
REM ============================

REM ========================================================================================
REM aijb_DBSetup.bat
REM [Author]:TAKESHI SHIMODA(DNITS), KAZUMI SATO(DNITS)
REM
REM $ aijb_DBSetup.bat 5761 2013-10-24 06:21:38Z HIDETOSHI_NAKATANI@denso.co.jp $
REM
REM Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
REM ========================================================================================

set NLS_LANG=Japanese_Japan.AL32UTF8

set SCHEMA_NAME=CTC-SPS2
set SCHEMA_PASS=CTC-SPS2
set IP_ADDRESS=10.0.0.11
set INSTANCE_NAME=XE

REM ---- PRAMETERS -------------------------------------------------------------------------
REM ----------------------------------------------------------------------------------------
sqlplus /nolog @ddl\create_aijb_table.sql %SCHEMA_NAME% %SCHEMA_PASS% %IP_ADDRESS% %INSTANCE_NAME% > create_table.log

echo [REPORT]:AIJB-Table-creating completed.

sqlplus /nolog @dml\insert_aijb_initial_data.sql %SCHEMA_NAME% %SCHEMA_PASS% %IP_ADDRESS% %INSTANCE_NAME% > insert_initial_data.log

echo [REPORT]:AIJB-InitialData-Inserting completed.
echo Press any key.
pause > nul
