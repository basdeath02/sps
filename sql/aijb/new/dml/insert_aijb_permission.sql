/*************************************************
 insert_aijb_permission.sql

 [JP] AIJB_PERMISSION 初期データ登録用SQLです。
 [EN] AIJB_PERMISSION It is the SQL for the registration of the initial data.

 $ insert_aijb_permission.sql 7434 2014-03-24 02:11:23Z 815372040074 $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
Insert into AIJB_PERMISSION (PERMISSION_ID, PERMISSION_NAME) values ('10', 'User');
Insert into AIJB_PERMISSION (PERMISSION_ID, PERMISSION_NAME) values ('20', 'Monitoring Center');
Insert into AIJB_PERMISSION (PERMISSION_ID, PERMISSION_NAME) values ('30', 'Administrator');

commit;

