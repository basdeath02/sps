/*************************************************
 PROJECT		:AI-JB
 FILE-NAME		:insert_aijb_initial_data.sql
 UPDATE			:2012 4/19
 REGISTER		:TAKESHI SHIMODA
 
 $ insert_aijb_initial_data.sql 5046 2013-08-08 01:50:53Z HIDETOSHI_NAKATANI@denso.co.jp $
 
 Copyright (c) 2012 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
/*
 --PARAMETERS-------------------------------------
 =================================================
REM ARGUMENT[1]:SCHEMA_NAME
REM ARGUMENT[2]:SCHEMA_PASS
REM ARGUMENT[3]:IP_ADDRESS
REM ARGUMENT[4]:INSTANCE_NAME
 =================================================
*/

--MAIN-------------------------------------------
----DB-CONNECTION MAKING BEGINS
connect &1/&2@&3/&4

----INITIAL DATA INSERTING BEGINS
@dml\insert_aijb_permission.sql
@dml\insert_aijb_admin_user.sql

----DB-CONNECTION CLOSING BEGINS
exit

--MAIN END-----------------------------------------

-- END
