/*************************************************
 insert_aijb_admin_user.sql

 [JP] AIJB_USER_MSTR 初期データ登録用SQLです。
 [EN] AIJB_USER_MSTR It is the SQL for the registration of the initial data.

 $ insert_aijb_admin_user.sql 4391 2013-05-22 07:00:46Z hiroko_nagata@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
Insert into AIJB_USER_MSTR (USER_ID,USER_NAME,USER_KANA_NAME,DEPT_NAME,GEN_EXTENSION_TEL,GEN_OUTSIDE_TEL,GEN_EMAIL,EMG_TEL_1,EMG_TEL_2,EMG_TEL_3,EMG_EMAIL,MAIL_SEND_AVAILABLE_FLAG,USER_AVAILABLE_FLAG,USER_REGIST_YMD,USER_REGIST_NAME,USER_UPDATE_YMD,USER_UPDATE_NAME,PERMISSION_ID,PASSWORD) values ('admin','DENSO_IT','DENSO_IT','IT','0120-001-001','0120-001-001','aijb@dnitsol.com','0120-001-001','0120-001-001','0120-001-001','aijb@localhost.com','1','1',systimestamp,'DENSO_IT',systimestamp,'DENSO_IT','30','admin');

commit;

