/*************************************************
 aijb_job_history.sql

 [JP] AIJB_JOB_HISTORY テーブル作成用SQLです。
 [EN] SQL is used to create AIJB_JOB_HISTORY table.

 $ aijb_job_history.sql 5122 2013-08-23 03:05:48Z tomonori_kuzukawa@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
create table AIJB_JOB_HISTORY (
    JOB_ID VARCHAR2(12) NOT NULL, 
    CHANGE_YMD DATE NOT NULL,
    JOB_NAME VARCHAR2(200) NOT NULL,
    JOB_TYPE CHAR(1) NOT NULL,
    CALENDAR_ID CHAR(5),
    EXECUTE_TARGET_HOST VARCHAR2(200),
    EXECUTE_USER_ID VARCHAR2(100),
    LATEST_QUEUE_ID CHAR(20),
    EXECUTE_MODULE_NAME VARCHAR2(200),
    EXECUTE_ARG VARCHAR2(200),
    JOB_AVAILABLE_FLAG CHAR(1) NOT NULL,
    REQ_JOB_AVAILABLE_FLAG CHAR(1) NOT NULL,
    MULTI_EXECUTE_COUNT NUMBER(2),
    JOB_REGIST_YMD TIMESTAMP NOT NULL,
    JOB_REGIST_NAME VARCHAR2(20) NOT NULL,
    JOB_UPDATE_YMD TIMESTAMP NOT NULL,
    JOB_UPDATE_NAME VARCHAR2(20) NOT NULL,
    JOB_END_TIME CHAR(4),
    DELAY_JUDGE_EXE_TIME NUMBER(8) NOT NULL,
    DELAY_JUDGE_START_TIME NUMBER(8) NOT NULL,
    DELAY_JUDGE_START_LIMIT_TIME CHAR(4),
    DELAY_JUDGE_END_LIMIT_TIME CHAR(4),
    DEPENDENCE_JOB_ID VARCHAR2(300),
    JOB_AVAILABLE_START DATE NOT NULL,
    JOB_AVAILABLE_END DATE NOT NULL, 
    EXECUTE_TIME NUMBER(5)  NOT NULL,
    EXECUTE_PRIORITY CHAR(1)  NOT NULL,
    RESIDENT_START_INTERVAL_TIME NUMBER(8) NOT NULL,
    RESIDENT_REPEAT_INTERVAL_TIME NUMBER(8) NOT NULL,
    JOB_EXE_CYCLE_REPEAT_TIME NUMBER(4) NOT NULL,
    JOB_EXE_CYCLE_START_TIME CHAR(4),
    JOB_EXE_CYCLE_END_TIME CHAR(4),
    JOB_EXE_CYCLE_MONTH CHAR(12),
    JOB_EXE_CYCLE_WEEK CHAR(6),
    JOB_EXE_CYCLE_YOUBI CHAR(7),
    JOB_EXE_CYCLE_DAY VARCHAR2(31),
    SYSTEM_ID CHAR(4),
    NON_WORKDAY_FLAG CHAR(1),
    LAST_QUEUE_CREATE_DAY CHAR(8),
    EXCLUSION_JOB_ID1 VARCHAR2(12),  
    EXCLUSION_JOB_ID2 VARCHAR2(12),  
    EXCLUSION_JOB_ID3 VARCHAR2(12),  
    EXCLUSION_JOB_ID4 VARCHAR2(12),  
    EXCLUSION_JOB_ID5 VARCHAR2(12),  
    MULTI_EXE_QUEUE_FLAG CHAR(1),
    MEMO VARCHAR2(200),
    GROUP_ID VARCHAR2(12) NOT NULL,
    SUPPORT_FLAG CHAR(1) NOT NULL,
    OPERATOR_NOTE VARCHAR2(400),
    UPDATE_REASON VARCHAR2(400),
    CONSTRAINT "AIJB_JOBHISTORY_PK" PRIMARY KEY ("JOB_ID","CHANGE_YMD")
)
/

COMMENT ON TABLE AIJB_JOB_HISTORY IS 'Job Change History';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_ID IS 'Job ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.CHANGE_YMD IS 'Job Date And Time Of The Move History';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_NAME IS 'Job Name';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_TYPE IS 'Job Type';
COMMENT ON COLUMN AIJB_JOB_HISTORY.CALENDAR_ID IS 'Calendar ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXECUTE_TARGET_HOST IS 'Execution Host Name';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXECUTE_USER_ID IS 'Execution User ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.LATEST_QUEUE_ID IS 'Latest Generation Queue ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXECUTE_MODULE_NAME IS 'File Execution Path';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXECUTE_ARG IS 'Run-Time Arguments';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_AVAILABLE_FLAG IS 'Job Status';
COMMENT ON COLUMN AIJB_JOB_HISTORY.REQ_JOB_AVAILABLE_FLAG IS 'Valid Flag Requests Job';
COMMENT ON COLUMN AIJB_JOB_HISTORY.MULTI_EXECUTE_COUNT IS 'Possible Number Of Concurrent';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_REGIST_YMD IS 'Registration Date And Time';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_REGIST_NAME IS 'Registrant';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_UPDATE_YMD IS 'Last Date Modified';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_UPDATE_NAME IS 'Last Updated By';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_END_TIME IS 'End Time Job';
COMMENT ON COLUMN AIJB_JOB_HISTORY.DELAY_JUDGE_EXE_TIME IS 'End Delay (Relative)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.DELAY_JUDGE_START_TIME IS 'Start Delay (Relative)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.DELAY_JUDGE_START_LIMIT_TIME IS 'Relative Time Start Delay (Absolute)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.DELAY_JUDGE_END_LIMIT_TIME IS 'Relative Time End Delay (Absolute)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.DEPENDENCE_JOB_ID IS 'Predecessor Job ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_AVAILABLE_START IS 'Lifetime Job (From)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_AVAILABLE_END IS 'Lifetime Job (To)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXECUTE_TIME IS 'Standard Execution Time (s)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXECUTE_PRIORITY IS 'Execution Priority';
COMMENT ON COLUMN AIJB_JOB_HISTORY.RESIDENT_START_INTERVAL_TIME IS 'First Start Time Of The Process Resident (s)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.RESIDENT_REPEAT_INTERVAL_TIME IS 'Scheduled Start Time Of The Process Resident (s)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_REPEAT_TIME IS 'Processing Cycle (Time Interval)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_START_TIME IS 'Processing Cycle (Start Time)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_END_TIME IS 'Processing Cycle (End Time)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_MONTH IS 'Processing Cycle (Month)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_WEEK IS 'Processing Cycle (Weeks)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_YOUBI IS 'Processing Cycle (Day Of The Week)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.JOB_EXE_CYCLE_DAY IS 'Processing Cycle (Date)';
COMMENT ON COLUMN AIJB_JOB_HISTORY.SYSTEM_ID IS 'System ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.NON_WORKDAY_FLAG IS 'Flag Execution Non-Working Days';
COMMENT ON COLUMN AIJB_JOB_HISTORY.LAST_QUEUE_CREATE_DAY IS 'Final Queue Creation Date';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXCLUSION_JOB_ID1 IS 'Exclusive Job ID 1';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXCLUSION_JOB_ID2 IS 'Exclusive Job ID 2';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXCLUSION_JOB_ID3 IS 'Exclusive Job ID 3';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXCLUSION_JOB_ID4 IS 'Exclusive Job ID 4';
COMMENT ON COLUMN AIJB_JOB_HISTORY.EXCLUSION_JOB_ID5 IS 'Exclusive Job ID 5';
COMMENT ON COLUMN AIJB_JOB_HISTORY.MULTI_EXE_QUEUE_FLAG IS 'Flag Creating Multiple Concurrent Queue';
COMMENT ON COLUMN AIJB_JOB_HISTORY.MEMO IS 'Remarks';
COMMENT ON COLUMN AIJB_JOB_HISTORY.GROUP_ID IS 'Job Administrator Group ID';
COMMENT ON COLUMN AIJB_JOB_HISTORY.SUPPORT_FLAG IS 'Corresponding Abnormal Flag';
COMMENT ON COLUMN AIJB_JOB_HISTORY.OPERATOR_NOTE IS 'Note For Operation';
COMMENT ON COLUMN AIJB_JOB_HISTORY.UPDATE_REASON IS 'Update or Delete Reason';

/
