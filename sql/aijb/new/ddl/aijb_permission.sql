/*************************************************
 aijb_permission.sql

 [JP] AIJB_PERMISSION テーブル作成用SQLです。
 [EN] SQL is used to create AIJB_PERMISSION table.

 $ aijb_permission.sql 4391 2013-05-22 07:00:46Z hiroko_nagata@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
CREATE TABLE AIJB_PERMISSION
    (
        PERMISSION_ID VARCHAR2(2) NOT NULL, 
        PERMISSION_NAME VARCHAR2(20) NOT NULL, 
        CONSTRAINT PK_PERMISSION PRIMARY KEY (PERMISSION_ID)
    );

commit;

COMMENT ON TABLE AIJB_PERMISSION IS 'Authority Master';
COMMENT ON COLUMN AIJB_PERMISSION.PERMISSION_ID IS 'Authority ID';
COMMENT ON COLUMN AIJB_PERMISSION.PERMISSION_NAME IS 'Authority Name';
