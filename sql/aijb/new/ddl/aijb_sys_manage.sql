/*************************************************
 aijb_sys_manage.sql

 [JP] AIJB_SYS_MANAGE テーブル作成用SQLです。
 [EN] SQL is used to create AIJB_SYS_MANAGE table.

 $ aijb_sys_manage.sql 5912 2013-11-13 01:34:56Z hiroko_nagata@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
CREATE TABLE AIJB_SYS_MANAGE
(
  SYSTEM_ID CHAR(4) not null
  , SYSTEM_NAME VARCHAR2(100) not null
  , SYSTEM_AVAILABLE_FLAG CHAR(1) not null
  , ALL_PROC_STOP_FLAG CHAR(1) not null
  , ALL_PROC_STOP_DATE DATE
  , ALL_PROC_STOP_RELEASE_DATE DATE
  , JOB_HM_FROM CHAR(4) not null
  , SYS_REGIST_YMD TIMESTAMP(6) not null
  , SYS_REGIST_NAME VARCHAR2(20) not null
  , SYS_UPDATE_YMD TIMESTAMP(6) not null
  , SYS_UPDATE_NAME VARCHAR2(20) not null
  , CONSTRAINT AIJB_SYS_MANAGE_PK PRIMARY KEY (SYSTEM_ID) USING INDEX
)
/
COMMENT ON TABLE AIJB_SYS_MANAGE IS 'System Master';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYSTEM_ID IS 'System ID';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYSTEM_NAME IS 'System Name';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYSTEM_AVAILABLE_FLAG IS 'Valid Flag System';
COMMENT ON COLUMN AIJB_SYS_MANAGE.ALL_PROC_STOP_FLAG IS 'Flag Pending System';
COMMENT ON COLUMN AIJB_SYS_MANAGE.ALL_PROC_STOP_DATE IS 'Last Start Date Pending System';
COMMENT ON COLUMN AIJB_SYS_MANAGE.ALL_PROC_STOP_RELEASE_DATE IS 'Last Date Of Termination Pending System';
COMMENT ON COLUMN AIJB_SYS_MANAGE.JOB_HM_FROM IS 'Job Base Time(start)';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYS_REGIST_YMD IS 'Registration Date And Time';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYS_REGIST_NAME IS 'Registrant';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYS_UPDATE_YMD IS 'Updated By';
COMMENT ON COLUMN AIJB_SYS_MANAGE.SYS_UPDATE_NAME IS 'Last Updated By';
/