/*************************************************
 PROJECT		:AI-JB
 FILE-NAME		:create_table.sql
 UPDATE			:2011 11/09
 REGISTER		:TAKESHI SHIMODA
 
 $ create_aijb_table.sql 5761 2013-10-24 06:21:38Z HIDETOSHI_NAKATANI@denso.co.jp $
 
 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
/*
 --PARAMETERS-------------------------------------
 =================================================
REM ARGUMENT[1]:SCHEMA_NAME
REM ARGUMENT[2]:SCHEMA_PASS
REM ARGUMENT[3]:IP_ADDRESS
REM ARGUMENT[4]:INSTANCE_NAME
 =================================================
*/

--MAIN-------------------------------------------
----DB-CONNECTION MAKING BEGINS
connect &1/&2@&3/&4

----ddl\aijb-TABLE MAKING BEGINS
@ddl\aijb_calendar_date_manage.sql
@ddl\aijb_calendar_manage.sql
@ddl\aijb_job_history.sql
@ddl\aijb_job_manage.sql
@ddl\aijb_job_retainer_group.sql
@ddl\aijb_message_code_manage.sql
@ddl\aijb_message_log_history.sql
@ddl\aijb_message_log_manage.sql
@ddl\aijb_queue_execute_history.sql
@ddl\aijb_queue_history.sql
@ddl\aijb_queue_manage.sql
@ddl\aijb_resident_req_history.sql
@ddl\aijb_resident_req_manage.sql
@ddl\aijb_sys_manage.sql
@ddl\aijb_user_mstr.sql
@ddl\aijb_permission.sql

----VIEW MAKING BEGINS
@ddl\v_aijb_queue_inf_union_all.sql
@ddl\v_aijb_request_inf_union_all.sql
@ddl\vw_aijb_job_history_manage.sql
@ddl\vw_aijb_message_log_manage.sql

----DB-CONNECTION CLOSING BEGINS
exit

--MAIN END-----------------------------------------

-- END
