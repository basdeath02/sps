/*************************************************
 vw_aijb_message_log_manage.sql

 [JP] VW_AIJB_MESSAGE_LOG_MANAGE ビュー作成用SQLです。
 [EN] SQL is used to create the VW_AIJB_MESSAGE_LOG_MANAGE view.

 $ vw_aijb_message_log_manage.sql 5455 2013-09-19 05:21:16Z hiroko_nagata@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
CREATE OR REPLACE VIEW VW_AIJB_MESSAGE_LOG_MANAGE(
    MESSAGE_LOG_ID,
    TABLE_TYPE,
    MOVE_YMD,
    SOURCE_TYPE,
    INSTANCE_ID,
    MESSAGE_CODE,
    INFORMATION,
    CHECK_FLAG,
    REGIST_YMD,
    REGIST_NAME,
    UPDATE_YMD,
    UPDATE_NAME
    )AS
    SELECT
        MESSAGE_LOG_ID
        ,'1' AS TABLE_TYPE
        ,null AS MOVE_YMD
        ,SOURCE_TYPE
        ,INSTANCE_ID
        ,MESSAGE_CODE
        ,INFORMATION
        ,CHECK_FLAG
        ,REGIST_YMD
        ,REGIST_NAME
        ,UPDATE_YMD
        ,UPDATE_NAME
    FROM 
        AIJB_MESSAGE_LOG_MANAGE
    UNION ALL
    SELECT
        MESSAGE_LOG_ID
        ,'2' AS TABLE_TYPE
        ,MOVE_YMD
        ,SOURCE_TYPE
        ,INSTANCE_ID
        ,MESSAGE_CODE
        ,INFORMATION
        ,CHECK_FLAG
        ,REGIST_YMD
        ,REGIST_NAME
        ,UPDATE_YMD
        ,UPDATE_NAME
    FROM 
        AIJB_MESSAGE_LOG_HISTORY
/

----COMMENT MAKING BEGINS
COMMENT ON COLUMN VW_AIJB_MESSAGE_LOG_MANAGE.TABLE_TYPE IS '''1'':AIJB_MESSAGE_LOG_MANAGE  ''2'':AIJB_MESSAGE_LOG_HISTORY'
/
--MAIN END-----------------------------------------

-- END
