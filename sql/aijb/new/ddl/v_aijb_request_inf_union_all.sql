/*************************************************
 v_aijb_request_inf_union_all.sql

 [JP] V_AIJB_REQUEST_INF_UNION_ALL ビュー作成用SQLです。
 [EN] SQL is used to create V_AIJB_REQUEST_INF_UNION_ALL view.

 $ v_aijb_request_inf_union_all.sql 4456 2013-05-27 09:32:40Z takeshi_shimoda@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/

--MAIN-------------------------------------------
----VIEW MAKING BIGINS
CREATE OR REPLACE VIEW V_AIJB_REQ_MANAGE_UNION_ALL(
    REQUEST_ID,
    TABLE_TYPE,
    MOVE_YMD,
    REGIST_YMD,
    REGIST_NAME,
    EXECUTE_START_YMD,
    EXECUTE_END_YMD,
    EXECUTE_PRIORITY,
    REQUEST_STATUS,
    UPDATE_YMD,
    UPDATE_NAME,
    JOB_ID,
    JOB_NAME,
    JOB_TYPE,
    EXECUTE_ARG,
    MULTI_EXECUTE_COUNT
    )AS
    select
        REQUEST_ID
        ,'1' as TABLE_TYPE
        ,null as MOVE_YMD
        ,REGIST_YMD
        ,REGIST_NAME
        ,EXECUTE_START_YMD
        ,EXECUTE_END_YMD
        ,EXECUTE_PRIORITY
        ,REQUEST_STATUS
        ,UPDATE_YMD
        ,UPDATE_NAME
        ,JOB_ID
        ,JOB_NAME
        ,JOB_TYPE
        ,EXECUTE_ARG
        ,MULTI_EXECUTE_COUNT
    from 
        AIJB_RESIDENT_REQ_MANAGE
    union all
    select
        REQUEST_ID
        ,'2' as TABLE_TYPE
        ,MOVE_YMD
        ,REGIST_YMD
        ,REGIST_NAME
        ,EXECUTE_START_YMD
        ,EXECUTE_END_YMD
        ,EXECUTE_PRIORITY
        ,REQUEST_STATUS
        ,UPDATE_YMD
        ,UPDATE_NAME
        ,JOB_ID
        ,JOB_NAME
        ,JOB_TYPE
        ,EXECUTE_ARG
        ,MULTI_EXECUTE_COUNT
    from 
        AIJB_RESIDENT_REQ_HISTORY
/

----COMMENT MAKING BEGINS
COMMENT ON COLUMN V_AIJB_REQ_MANAGE_UNION_ALL.TABLE_TYPE IS '''1'':AIJB_RESIDENT_REQ_MANAGE  ''2'':AIJB_RESIDENT_REQ_HISTORY'
/
--MAIN END-----------------------------------------

-- END
