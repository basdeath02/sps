/*************************************************
 aijb_resident_req_history.sql

 [JP] AIJB_RESIDENT_REQ_HISTORY テーブル作成用SQLです。
 [EN] SQL is used to create AIJB_RESIDENT_REQ_HISTORY table.

 $ aijb_resident_req_history.sql 5761 2013-10-24 06:21:38Z HIDETOSHI_NAKATANI@denso.co.jp $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
CREATE TABLE AIJB_RESIDENT_REQ_HISTORY
(
    REQUEST_ID                     CHAR(20) NOT NULL,
    MOVE_YMD                       DATE DEFAULT sysdate NOT NULL,
    REGIST_YMD                     TIMESTAMP(6) NOT NULL,
    REGIST_NAME                    VARCHAR2(20) NOT NULL,
    EXECUTE_START_YMD              TIMESTAMP(6),
    EXECUTE_END_YMD                TIMESTAMP(6),
    EXECUTE_PRIORITY               CHAR(1) NOT NULL,
    REQUEST_STATUS                 CHAR(10) NOT NULL,
    UPDATE_YMD                     TIMESTAMP(6) NOT NULL,
    UPDATE_NAME                    VARCHAR2(20) NOT NULL,
    JOB_ID                         VARCHAR2(12) NOT NULL,
    JOB_NAME                       VARCHAR2(200) NOT NULL,
    JOB_TYPE                       CHAR(1) NOT NULL,
    EXECUTE_ARG                    VARCHAR2(200),
    MULTI_EXECUTE_COUNT            NUMBER(2,0),
    CONSTRAINT "AIJB_RESIDENT_REQ_HISTORY_PKC" PRIMARY KEY ("REQUEST_ID")
);

COMMENT ON TABLE AIJB_RESIDENT_REQ_HISTORY IS 'Resident Requests History   Table to store the pre-treatment of information requests';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.REQUEST_ID IS 'Requests ID';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.MOVE_YMD IS 'Date And Time Of The Move History';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.REGIST_YMD IS 'Registration Date And Time';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.REGIST_NAME IS 'Registrant';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.EXECUTE_START_YMD IS 'Start Date And Time';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.EXECUTE_END_YMD IS 'End Date And Time';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.EXECUTE_PRIORITY IS 'Execution Priority';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.REQUEST_STATUS IS 'Request Status';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.UPDATE_YMD IS 'Date Modified';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.UPDATE_NAME IS 'Updated By';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.JOB_ID IS 'Job ID';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.JOB_NAME IS 'Job Name';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.JOB_TYPE IS 'Job Type';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.EXECUTE_ARG IS 'Run-Time Arguments';
COMMENT ON COLUMN AIJB_RESIDENT_REQ_HISTORY.MULTI_EXECUTE_COUNT IS 'Possible Number Of Concurrent';
/
