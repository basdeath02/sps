/*************************************************
 aijb_user_mstr.sql

 [JP] AIJB_USER_MSTR テーブル作成用SQLです。
 [EN] SQL is used to create AIJB_USER_MSTR table.

 $ aijb_user_mstr.sql 4391 2013-05-22 07:00:46Z hiroko_nagata@dnitsol.com $

 Copyright (c) 2011 DENSO IT SOLUTIONS, All rights reserved.
 *************************************************/
create table AIJB_USER_MSTR (
USER_ID                  VARCHAR2(12)  NOT NULL,
USER_NAME                VARCHAR2(100) NOT NULL,
USER_KANA_NAME           VARCHAR2(100) NOT NULL, 
DEPT_NAME                VARCHAR2(300) NOT NULL,
PASSWORD                 VARCHAR2(20) NOT NULL,
GEN_EXTENSION_TEL        VARCHAR2(20)  NOT NULL,
GEN_OUTSIDE_TEL          VARCHAR2(20),
GEN_EMAIL                VARCHAR2(300) NOT NULL,
EMG_TEL_1                VARCHAR2(20),
EMG_TEL_2                VARCHAR2(20),
EMG_TEL_3                VARCHAR2(20),
EMG_EMAIL                VARCHAR2(300),
MAIL_SEND_AVAILABLE_FLAG CHAR(1),
USER_AVAILABLE_FLAG      CHAR(1)   NOT NULL,
USER_REGIST_YMD          TIMESTAMP    NOT NULL,
USER_REGIST_NAME         VARCHAR2(20)  NOT NULL,
USER_UPDATE_YMD          TIMESTAMP    NOT NULL,
USER_UPDATE_NAME         VARCHAR2(20) NOT NULL, 
PERMISSION_ID            VARCHAR2(2) NOT NULL,
    CONSTRAINT "AIJB_USER_MSTR_PK" PRIMARY KEY ("USER_ID")
)
/
COMMENT ON TABLE AIJB_USER_MSTR IS 'User Master';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_ID IS 'User ID';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_NAME IS 'User Name';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_KANA_NAME IS 'User Name (Kana)';
COMMENT ON COLUMN AIJB_USER_MSTR.DEPT_NAME IS 'Department Name That Belongs';
COMMENT ON COLUMN AIJB_USER_MSTR.PASSWORD IS 'Password';
COMMENT ON COLUMN AIJB_USER_MSTR.GEN_EXTENSION_TEL IS 'In-company Contacts (Extension Number)';
COMMENT ON COLUMN AIJB_USER_MSTR.GEN_OUTSIDE_TEL IS 'In-company Contacts (External Number)';
COMMENT ON COLUMN AIJB_USER_MSTR.GEN_EMAIL IS 'In-company Contacts (E-Mail Address)';
COMMENT ON COLUMN AIJB_USER_MSTR.EMG_TEL_1 IS 'Night/Holiday Contacts (External Number) 1';
COMMENT ON COLUMN AIJB_USER_MSTR.EMG_TEL_2 IS 'Night/Holiday Contacts (External Number) 2';
COMMENT ON COLUMN AIJB_USER_MSTR.EMG_TEL_3 IS 'Night/Holiday Contacts (External Number) 3';
COMMENT ON COLUMN AIJB_USER_MSTR.EMG_EMAIL IS 'Night/Holiday Contacts (E-Mail Address)';
COMMENT ON COLUMN AIJB_USER_MSTR.MAIL_SEND_AVAILABLE_FLAG IS 'Flag Send Mail';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_AVAILABLE_FLAG IS 'Valid Flag User Information';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_REGIST_YMD IS 'Registration Date And Time';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_REGIST_NAME IS 'Registrant';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_UPDATE_YMD IS 'Updated By';
COMMENT ON COLUMN AIJB_USER_MSTR.USER_UPDATE_NAME IS 'Last Updated By';
COMMENT ON COLUMN AIJB_USER_MSTR.PERMISSION_ID IS 'Authority ID';
/