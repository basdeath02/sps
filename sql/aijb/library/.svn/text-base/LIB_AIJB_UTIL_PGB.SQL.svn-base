create or replace
package BODY        LIB_AIJB_UTIL_PG
/*************************************************************************
 * [JP] システム名   ：AI-JB
 * [JP] パッケージ名 ：LIB_AIJB_UTIL_PG
 * [JP] 機能概要     ：AI-JB 共通部品(パッケージ本体)
 * [JP] 作成日       ：2011/09/13
 * 
 * [EN] System name         ：AI-JB
 * [EN] Package Name        ：LIB_AIJB_UTIL_PG
 * [EN] Functional overview ：Common parts AI-JB (package body)
 * [EN] Creation date       ：2011/09/13
 * -----------------------------------------------------------------------
 * [JP] 変更履歴     ：r2700 常駐リクエストジョブ リクエスト登録機能追加
 * 
 * [EN] Change history      ：r2700 Resident request job Add a request registration function
 *
 * $ LIB_AIJB_UTIL_PGB.SQL 6888 2014-02-13 08:04:02Z tomonori_kuzukawa@dnitsol.com $
 *
 *************************************************************************/
IS
    --
    PROCEDURE CREATE_JOB_QUEUE_PC
    (
        PI_JOB_ID         IN VARCHAR2,
        PI_RESIST_NAME    IN VARCHAR2,
        PI_APPLI_INFO     IN VARCHAR2,
        PI_CHILD_JOB_INFO IN VARCHAR2,
        PI_EXECUTE_ARG    IN VARCHAR2,
        PI_START_YMD      IN DATE,
        PO_QUEUE_ID OUT VARCHAR2 
    )
    IS
        PROGRAM_NM CONSTANT VARCHAR2(100) := GC_PROGRAM_NAME_PG || '.CREATE_JOB_QUEUE_PC';
        -- [JP] リクエストジョブのジョブ種別
        -- [EN] Job type of job request
        REQ_JOB_TYPE AIJB_JOB_MANAGE.JOB_TYPE%type := '2';
        -- [JP] ジョブ管理TBL のレコード
        -- [EN] Record of Job Management TBL
        lr_job_mgr AIJB_JOB_MANAGE%rowtype;
        -- [JP] キューID
        -- [EN] Queue ID
        queue_id AIJB_QUEUE_MANAGE.QUEUE_ID%type;
        -- [JP] ジョブ実行予定日時
        -- [EN] Date and time job is scheduled to run
        execute_start_schedule_ymd AIJB_QUEUE_MANAGE.EXECUTE_START_SCHEDULE_YMD%type;
        -- [JP] ジョブ基準年月日
        -- [EN] Date of job criteria
        job_ymd AIJB_QUEUE_MANAGE.JOB_YMD%type;
        -- [JP] 実行時引数
        -- [EN] Run-time arguments
        execute_arg AIJB_QUEUE_MANAGE.EXECUTE_ARG%type;
        -- [JP] ユーザ定義例外（主キー設定不正）
        -- [EN] User-defined exception (Incorrect setting primary key)
        CHECK_PARAMETER_EX EXCEPTION;
        -- [JP] ユーザ定義例外（ジョブ登録不正）
        -- [EN] User-defined exception (Unauthorized job Registration)
        NO_EXIST_JOB_MANAGE_EX EXCEPTION;
        -- [JP] ユーザ定義例外（ジョブ種別不正）
        -- [EN] User-defined exception (Unauthorized job type)
        INVALID_JOB_TYPE_EX EXCEPTION;
        --
        -- [JP] キュー登録者（PI_RESIST_NAME）の文字長
        -- [EN] Character length of registrant queue (PI_RESIST_NAME)
        RESIST_NAME_LENGTH CONSTANT NUMBER := 20;
        -- [JP] 実行時引数（PI_EXECUTE_ARG）の文字長
        -- [EN] Character length of run-time arguments (PI_EXECUTE_ARG)
        EXECUTE_ARG_LENGTH CONSTANT NUMBER := 200;
        -- [JP] アプリ固有情報（PI_APPLI_INFO）の文字長
        -- [EN] Character length of application-specific information (PI_APPLI_INFO)
        APPLI_INFO_LENGTH CONSTANT NUMBER := 200;
        -- [JP] 子ジョブ任意情報（PI_CHILD_JOB_INFO）の文字長
        -- [EN] Character length of any information of the child jobs (PI_CHILD_JOB_INFO)
        CHILD_JOB_INFO_LENGTH CONSTANT NUMBER := 200;
        --
        -- [JP] ユーザ定義例外（キュー登録者の設定不正）
        -- [EN] User-defined exception (Incorrect setting registrant queue)
        CHECK_QUEUE_REGIST_NAME_EX EXCEPTION;
        -- [JP] ユーザ定義例外（実行予定日時の設定不正）
        -- [EN] User-defined exception (Incorrect setting date and time of scheduled to run)
        CHECK_START_YMD_EX EXCEPTION;
        -- [JP] ユーザ定義例外（キュー登録者の文字長不正）
        -- [EN] User-defined exception (Incorrect character length of registrant queue)
        LENGTH_RESIST_NAME_EX EXCEPTION;
        -- [JP] ユーザ定義例外（実行時引数の文字長不正）
        -- [EN] User-defined exception (Incorrect character length of run-time arguments)
        LENGTH_EXECUTE_ARG_EX EXCEPTION;
        -- [JP] ユーザ定義例外（アプリ固有情報の文字長不正）
        -- [EN] User-defined exception (Incorrect character length of application-specific information)
        LENGTH_APPLI_INFO_EX EXCEPTION;
        -- [JP] ユーザ定義例外（子ジョブ任意情報の文字長不正）
        -- [EN] User-defined exception (Incorrect character length of any information of the child jobs)
        LENGTH_CHILD_JOB_INFO_EX EXCEPTION;
    BEGIN
        -- [JP] 入力パラメータチェック
        -- [EN] Check the parameter input
        -- [JP] 必須項目（PI_JOB_ID）が設定されていること
        -- [EN] Be set to required field (PI_JOB_ID)
        IF ( PI_JOB_ID IS NULL ) THEN
            RAISE CHECK_PARAMETER_EX;
        END IF;
        -- [JP] 必須項目（PI_RESIST_NAME）が設定されていること
        -- [EN] Be set to required field (PI_RESIST_NAME)
        IF ( PI_RESIST_NAME IS NULL ) THEN
            RAISE CHECK_QUEUE_REGIST_NAME_EX;
        END IF;
        -- [JP] 実行予定日時（PI_START_YMD）が設定されていること
        -- [EN] Be set to date and time of scheduled to run (PI_START_YMD)
        IF ( PI_START_YMD IS NULL ) THEN
            RAISE CHECK_START_YMD_EX;
        END IF;
        -- [JP] キュー登録者（PI_RESIST_NAME）は20バイトを超えないこと。
        -- [EN] registrant queue (PI_RESIST_NAME) can not exceed 20 bytes.
        IF ( lengthb(PI_RESIST_NAME) > RESIST_NAME_LENGTH ) THEN
            RAISE LENGTH_RESIST_NAME_EX;
        END IF;
        -- [JP] 実行時引数（PI_EXECUTE_ARG）は200バイトを超えないこと。
        -- [EN] Run-time arguments (PI_EXECUTE_ARG) can not exceed 200 bytes.
        IF ( lengthb(PI_EXECUTE_ARG) > EXECUTE_ARG_LENGTH ) THEN
            RAISE LENGTH_EXECUTE_ARG_EX;
        END IF;
        -- [JP] アプリ固有情報（PI_APPLI_INFO）は200バイトを超えないこと。
        -- [EN] Application-specific information (PI_APPLI_INFO) can not exceed 200 bytes.
        IF ( lengthb(PI_APPLI_INFO) > APPLI_INFO_LENGTH ) THEN
            RAISE LENGTH_APPLI_INFO_EX;
        END IF;
        -- [JP] 子ジョブ任意情報（PI_CHILD_JOB_INFO）は200バイトを超えないこと。
        -- [EN] Any information of the child jobs (PI_CHILD_JOB_INFO) can not exceed 200 bytes.
        IF ( lengthb(PI_CHILD_JOB_INFO) > CHILD_JOB_INFO_LENGTH ) THEN
            RAISE LENGTH_CHILD_JOB_INFO_EX;
        END IF;
        -- [JP] ジョブ管理テーブルにジョブ情報が登録されていること
        -- [EN] Job information is registered in the Job Management Table
        select
            *
        into
            lr_job_mgr
        from
            AIJB_JOB_MANAGE t1
        where
            t1.JOB_ID = PI_JOB_ID
            and t1.JOB_AVAILABLE_START <= SYSDATE
            and t1.JOB_AVAILABLE_END >= SYSDATE
            and t1.JOB_AVAILABLE_FLAG in ('1', '4');
        --
        -- [JP] ジョブ種別がリクエストバッチであること
        -- [EN] Job type is a batch request
        IF ( lr_job_mgr.job_type != REQ_JOB_TYPE ) THEN
            RAISE INVALID_JOB_TYPE_EX;
        END IF;
        --
        -- [JP] 初期値の設定
        -- [EN] Setting initial value
        -- [JP] 実行予定日時の初期値を設定
        -- [EN] Setting initial value of date and time of scheduled to run
        execute_start_schedule_ymd := trunc(PI_START_YMD, 'MI');
        -- [JP] ジョブ基準日の初期値を設定(リクエストジョブではジョブ基準日を使用しないため、ジョブ基準日をNULLとする)
        -- [EN] Setting initial value of job base date(job base date is null it is not required for request job)
        job_ymd := NULL;
        -- [JP] 実行時引数の初期値を設定
        -- [EN] Setting initial value of run-time arguments
        execute_arg := lr_job_mgr.execute_arg;
        IF ( PI_EXECUTE_ARG IS NOT NULL ) THEN
            IF ( lr_job_mgr.execute_arg IS NULL ) THEN
                execute_arg := PI_EXECUTE_ARG;
            ELSE
                -- [JP] 実行時引数（PI_EXECUTE_ARG）とジョブマスタの実行時引数の合計が200バイトを超えないこと。
                -- [EN] The total run-time arguments (PI_EXECUTE_ARG) and run-time arguments of Job Master can not exceed 200 bytes.
                IF ( lengthb(lr_job_mgr.execute_arg || ',' || PI_EXECUTE_ARG) > EXECUTE_ARG_LENGTH ) THEN
                    RAISE LENGTH_EXECUTE_ARG_EX;
                END IF;
                execute_arg := lr_job_mgr.execute_arg || ',' || PI_EXECUTE_ARG;
            END IF;
        END IF;
        --
        --
        -- [JP] キューID をシーケンスから取得
        -- [EN] Gets the queue ID from the sequence
        select
            lpad(AIJB_QUEUE_MANAGE_SEQ.nextval, 20, '0')
        into
            queue_id
        from
            dual;
        --
        -- [JP] キューを作成する
        -- [EN] Create a queue
        insert
        into
            AIJB_QUEUE_MANAGE
            (
                QUEUE_ID
                , QUEUE_REGIST_YMD
                , QUEUE_REGIST_NAME
                , JOB_STATUS
                , CHILD_JOB_STATUS
                , CHILD_JOB_ID
                , EXECUTE_START_SCHEDULE_YMD
                , EXECUTE_START_YMD
                , EXECUTE_END_YMD
                , EXECUTE_PRIORITY
                , APPLI_INFO
                , MESSAGE_CODE
                , MESSAGE
                , STATUS_FLAG
                , CHILD_JOB_PROC_COUNT
                , CHILD_JOB_INFO
                , JOB_ID
                , JOB_NAME
                , JOB_TYPE
                , EXECUTE_TARGET_HOST
                , EXECUTE_USER_ID
                , EXECUTE_MODULE_NAME
                , EXECUTE_ARG
                , DEPENDENCE_JOB_ID
                , MULTI_EXECUTE_COUNT
                , EXECUTE_TIME
                , QUEUE_UPDATE_YMD
                , QUEUE_UPDATE_NAME
                , DELAY_JUDGE_EXE_TIME
                , DELAY_JUDGE_START_TIME
                , RESIDENT_START_INTERVAL_TIME
                , RESIDENT_REPEAT_INTERVAL_TIME
                , ADD_SOURCE
                , JOB_YMD
                , JOB_END_NOTICE_FLAG
                , EXECUTE_DELAY_NOTICE_FLAG
                , JOB_START_DELAY_NOTICE_FLAG
                , JOB_END_DELAY_NOTICE_FLAG
                , EXCLUSION_JOB_ID1
                , EXCLUSION_JOB_ID2
                , EXCLUSION_JOB_ID3
                , EXCLUSION_JOB_ID4
                , EXCLUSION_JOB_ID5
                , MULTI_EXE_QUEUE_FLAG
                , EXECUTE_START_FST_SCHEDULE_YMD
            )
            values
            (
                queue_id
                , SYSTIMESTAMP
                , PI_RESIST_NAME
                , '00'
                , '00'
                , 0
                , execute_start_schedule_ymd
                , ''
                , ''
                , lr_job_mgr.execute_priority
                , PI_APPLI_INFO
                , '00'
                , ''
                , '0000000000'
                , 0
                , PI_CHILD_JOB_INFO
                , PI_JOB_ID
                , lr_job_mgr.job_name
                , lr_job_mgr.job_type
                , lr_job_mgr.execute_target_host
                , lr_job_mgr.execute_user_id
                , lr_job_mgr.execute_module_name
                , execute_arg
                , lr_job_mgr.dependence_job_id
                , lr_job_mgr.multi_execute_count
                , lr_job_mgr.execute_time
                , SYSTIMESTAMP
                , PI_RESIST_NAME
                , lr_job_mgr.delay_judge_exe_time
                , lr_job_mgr.delay_judge_start_time
                , lr_job_mgr.resident_start_interval_time
                , lr_job_mgr.resident_repeat_interval_time
                , '2'
                , job_ymd
                , '0'
                , '0'
                , '0'
                , '0'
                , lr_job_mgr.exclusion_job_id1
                , lr_job_mgr.exclusion_job_id2
                , lr_job_mgr.exclusion_job_id3
                , lr_job_mgr.exclusion_job_id4
                , lr_job_mgr.exclusion_job_id5
                , lr_job_mgr.multi_exe_queue_flag
                , execute_start_schedule_ymd
            );
        --
        -- [JP] キューIDを出力パラメータにセット
        -- [EN] Set the queue ID in the output parameter
            PO_QUEUE_ID := queue_id;
        --
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20902, 'LIB_AIJB_UTIL_PG * NO_EXIST_JOB_MANAGE_EX ERROR');
        WHEN CHECK_PARAMETER_EX THEN
            RAISE_APPLICATION_ERROR(-20901, 'LIB_AIJB_UTIL_PG * CHECK_PARAMETER_EX ERROR');
        WHEN CHECK_QUEUE_REGIST_NAME_EX THEN
            RAISE_APPLICATION_ERROR(-20918, 'LIB_AIJB_UTIL_PG * CHECK_QUEUE_REGIST_NAME_EX ERROR');
        WHEN INVALID_JOB_TYPE_EX THEN
            RAISE_APPLICATION_ERROR(-20903, 'LIB_AIJB_UTIL_PG * INVALID_JOB_TYPE_EX ERROR');
        WHEN CHECK_START_YMD_EX THEN
            RAISE_APPLICATION_ERROR(-20904, 'LIB_AIJB_UTIL_PG * CHECK_START_YMD_EX ERROR');
        WHEN LENGTH_RESIST_NAME_EX THEN
            RAISE_APPLICATION_ERROR(-20905, 'LIB_AIJB_UTIL_PG * LENGTH_RESIST_NAME_EX ERROR');
        WHEN LENGTH_EXECUTE_ARG_EX THEN
            RAISE_APPLICATION_ERROR(-20906, 'LIB_AIJB_UTIL_PG * LENGTH_EXECUTE_ARG_EX ERROR');
        WHEN LENGTH_APPLI_INFO_EX THEN
            RAISE_APPLICATION_ERROR(-20907, 'LIB_AIJB_UTIL_PG * LENGTH_APPLI_INFO_EX ERROR');
        WHEN LENGTH_CHILD_JOB_INFO_EX THEN
            RAISE_APPLICATION_ERROR(-20908, 'LIB_AIJB_UTIL_PG * LENGTH_CHILD_JOB_INFO_EX ERROR');
        WHEN OTHERS THEN
            RAISE;
    END CREATE_JOB_QUEUE_PC;
    --
    --
    -- [JP] リクストジョブキュー登録(簡易版)
    -- [EN] Register request job queue (Simple version)
    PROCEDURE CREATE_JOB_QUEUE_PC
    (
        PI_JOB_ID         IN VARCHAR2,
        PI_RESIST_NAME    IN VARCHAR2,
        PI_APPLI_INFO     IN VARCHAR2,
        PI_CHILD_JOB_INFO IN VARCHAR2,
        PI_EXECUTE_ARG    IN VARCHAR2,
        PO_QUEUE_ID OUT VARCHAR2
    )
    IS
    BEGIN
    --
        CREATE_JOB_QUEUE_PC ( PI_JOB_ID, PI_RESIST_NAME, PI_APPLI_INFO, PI_CHILD_JOB_INFO, PI_EXECUTE_ARG, SYSDATE, PO_QUEUE_ID );
    --
    END CREATE_JOB_QUEUE_PC;
    --    
    -- [JP] 常駐リクエストジョブリクエスト登録
    -- [EN] Register resident requests job requests
    PROCEDURE CREATE_RESIDENT_REQ_PC
    (
        PI_JOB_ID         IN VARCHAR2,
        PI_REGIST_NAME    IN VARCHAR2,
        PO_REQUEST_ID OUT VARCHAR2
    )
    IS
        -- [JP] 定数定義
        -- [EN] Constant definition
        -- [JP] ログ出力用プロシージャ名
        -- [EN] Procedure name for log output
        PROGRAM_NM CONSTANT VARCHAR2(100) := GC_PROGRAM_NAME_PG || '.CREATE_RESIDENT_REQ_PC';
        -- [JP] リクエスト登録者（PI_REGIST_NAME）の文字長
        -- [EN] Character length of registrant request (PI_REGIST_NAME)
        REGIST_NAME_LENGTH CONSTANT NUMBER := 20;
        --
        -- [JP] 変数定義
        -- [EN] Variable definition
        -- [JP] リクエストジョブのジョブ種別
        -- [EN] Job type of job request
        REQ_JOB_TYPE AIJB_JOB_MANAGE.JOB_TYPE%type := '2';
        -- [JP] ジョブ管理TBL のレコード
        -- [EN] Record of Job Management TBL
        lr_job_mgr AIJB_JOB_MANAGE%rowtype;
        -- [JP] リクエストID
        -- [EN] Request ID
        request_id AIJB_RESIDENT_REQ_MANAGE.REQUEST_ID%type;
        --
        -- [JP] 例外定義
        -- [EN] Exception definitions
        -- [JP] ユーザ定義例外（必須パラメータ ジョブID入力不正）
        -- [EN] User-defined exception (Required parameters  Unauthorized enter the job ID)
        CHECK_PARAMETER_EX EXCEPTION;
        -- [JP] ユーザ定義例外（必須パラメータ リクエスト登録者入力不正）
        -- [EN] User-defined exception (Required parameters  Unauthorized enter the registrant request)
        CHECK_REGIST_NAME_EX EXCEPTION;
        -- [JP] ユーザ定義例外（リクエスト登録者の文字長不正）
        -- [EN] User-defined exception (Incorrect character length of registrant request)
        LENGTH_REGIST_NAME_EX EXCEPTION;
        -- [JP] ユーザ定義例外（ジョブ種別不正）
        -- [EN] User-defined exception (Unauthorized job type)
        INVALID_JOB_TYPE_EX EXCEPTION;
        -- [JP] ユーザ定義例外（実行時引数登録不正）
        -- [EN]User-defined exception (Unauthorized registration of run-time arguments)
        NULL_SERVICE_EX EXCEPTION;
        --
    BEGIN
        -- [JP] 入力パラメータチェック
        -- [EN] Check the parameter input
        -- [JP] 必須項目（PI_JOB_ID）が設定されていること
        -- [EN] Be set to required field (PI_JOB_ID)
        IF ( PI_JOB_ID IS NULL ) THEN
            RAISE CHECK_PARAMETER_EX;
        END IF;
        -- [JP] 必須項目（PI_REGIST_NAME）が設定されていること
        -- [EN] Be set to required field (PI_REGIST_NAME)
        IF ( PI_REGIST_NAME IS NULL ) THEN
            RAISE CHECK_REGIST_NAME_EX;
        END IF;
        -- [JP] リクエスト登録者（PI_REGIST_NAME）は20バイトを超えないこと。
        -- [EN] registrant request (PI_REGIST_NAME) can not exceed 20 bytes.
        IF ( lengthb(PI_REGIST_NAME) > REGIST_NAME_LENGTH ) THEN
            RAISE LENGTH_REGIST_NAME_EX;
        END IF;
        --
        -- [JP] ジョブID存在チェック
        -- [EN] Job ID check for the presence
        -- [JP] ジョブ管理テーブルにジョブ情報が登録されていること
        -- [EN] Job information is registered in the Job Management Table
        select
            *
        into
            lr_job_mgr
        from
            AIJB_JOB_MANAGE t1
        where
            t1.JOB_ID = PI_JOB_ID
            and t1.JOB_AVAILABLE_START <= SYSDATE
            and t1.JOB_AVAILABLE_END >= SYSDATE
            and t1.JOB_AVAILABLE_FLAG in ('1', '4');
        --
        -- [JP] ジョブ種別がリクエストバッチであること
        -- [EN] Job type is a batch request
        IF ( lr_job_mgr.job_type != REQ_JOB_TYPE ) THEN
            RAISE INVALID_JOB_TYPE_EX;
        END IF;
        -- [JP] 実行時引数が入力されていること
        -- [EN] Enter run-time arguments
        IF ( lr_job_mgr.execute_arg IS NULL ) THEN
            RAISE NULL_SERVICE_EX;
        END IF;
        --
        -- [JP] リクエストID をシーケンスから取得
        -- [EN] Gets the request ID from the sequence
        select
            lpad(AIJB_RESIDENT_REQ_MANAGE_SEQ.nextval, 20, '0')
        into
            request_id
        from
            dual;
        --
        -- [JP] リクエストを作成する
        -- [EN] Create a request
        insert
        into
            AIJB_RESIDENT_REQ_MANAGE
            (
                REQUEST_ID
                , REGIST_YMD
                , REGIST_NAME
                , EXECUTE_START_YMD
                , EXECUTE_END_YMD
                , EXECUTE_PRIORITY
                , REQUEST_STATUS
                , UPDATE_YMD
                , UPDATE_NAME
                , JOB_ID
                , JOB_NAME
                , JOB_TYPE
                , EXECUTE_ARG
                , MULTI_EXECUTE_COUNT
            )
            values
            (
                request_id
                , SYSTIMESTAMP
                , PI_REGIST_NAME
                , ''
                , ''
                , lr_job_mgr.execute_priority
                , '0000000000'
                , SYSTIMESTAMP
                , PI_REGIST_NAME
                , lr_job_mgr.job_id
                , lr_job_mgr.job_name
                , lr_job_mgr.job_type
                , lr_job_mgr.execute_arg
                , lr_job_mgr.multi_execute_count
            );
        --
        -- [JP] リクエストIDを出力パラメータにセット
        -- [EN] Set the request ID in the output parameter
            PO_REQUEST_ID := request_id;
        --
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20913, 'LIB_AIJB_UTIL_PG * NO_EXIST_JOB_MANAGE_EX ERROR');
        WHEN CHECK_PARAMETER_EX THEN
            RAISE_APPLICATION_ERROR(-20912, 'LIB_AIJB_UTIL_PG * CHECK_PARAMETER_EX ERROR');
        WHEN CHECK_REGIST_NAME_EX THEN
            RAISE_APPLICATION_ERROR(-20917, 'LIB_AIJB_UTIL_PG * CHECK_REGIST_NAME_EX ERROR');
        WHEN INVALID_JOB_TYPE_EX THEN
            RAISE_APPLICATION_ERROR(-20914, 'LIB_AIJB_UTIL_PG * INVALID_JOB_TYPE_EX ERROR');
        WHEN NULL_SERVICE_EX THEN
            RAISE_APPLICATION_ERROR(-20915, 'LIB_AIJB_UTIL_PG * NULL_SERVICE_EX ERROR');
        WHEN LENGTH_REGIST_NAME_EX THEN
            RAISE_APPLICATION_ERROR(-20916, 'LIB_AIJB_UTIL_PG * LENGTH_REGIST_NAME_EX ERROR');
        WHEN OTHERS THEN
            RAISE;
    END CREATE_RESIDENT_REQ_PC;
    --
--
END LIB_AIJB_UTIL_PG;
