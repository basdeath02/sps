/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 * 2015/07/28 CSI Akat            IN008 : import duplicate message
 * 2015/11/26 CSI Akat            IN044
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ChildJobBase;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.AnnounceMessageDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.service.CommonService;
import com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService;
import com.globaldenso.asia.sps.business.service.MiscellaneousService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
/**
 * This job has a purpose to read CSV file that upload by each DENSO<br />
 * company to the system. After user login to SPS system success<br />
 * the system will load a message to show at the announcement message<br />
 * section at "Main Screen" page.
 */
public class ImportMessageToSystemChildJob extends ChildJobBase {
    
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(ImportMessageToSystemChildJob.class);

    /** Call Service ImportMessageToSystemFacadeService. */
    private ImportMessageToSystemFacadeService importMessageToSystemFacadeService;

    /** Call Service MiscService. */
    private MiscellaneousService miscellaneousService;

    /** Call Service CommonService */
    private CommonService commonService;

    /**
     * Default constructor.
     */
    public ImportMessageToSystemChildJob() {

    }

    /**
     * <p>Setter method for importMessageToSystemFacadeService.</p>
     *
     * @param importMessageToSystemFacadeService Set for importMessageToSystemFacadeService
     */
    public void setImportMessageToSystemFacadeService(
        ImportMessageToSystemFacadeService importMessageToSystemFacadeService) {
        this.importMessageToSystemFacadeService = importMessageToSystemFacadeService;
    }

    /**
     * <p>
     * Setter method for miscellaneous service.
     * </p>
     * 
     * @param miscellaneousService Set for miscellaneous service
     */
    public void setMiscellaneousService(MiscellaneousService miscellaneousService) {
        this.miscellaneousService = miscellaneousService;
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * Auto-generated method stub : do nothing.
     * 
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void preBizProc(JobInputDomain arg0) throws ApplicationException
    {
        /** Delete expired message from SPS_M_ANNOUNCE_MESSAGE. */
        LoadContextPropertyForBatch.loadContextListener(LOG);
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        
        /** Get period delete expire message from MiscellaneousService. */
        // Start : [IN044] Call wrong method to get misc value
        //List<MiscellaneousDomain> miscDomains = null;
        //miscDomain.setMiscType(Constants.EXPIRE_MESSAGE);
        //miscDomain.setMiscCode(String.valueOf(Constants.ONE));
        //miscDomains = miscellaneousService.searchMisc(miscDomain);
        //String periodDelete = String.valueOf(Constants.ZERO);
        //if(null == miscDomains || Constants.ZERO == miscDomains.size()){
        //    periodDelete = String.valueOf(Constants.ZERO);
        //}else{
        //    periodDelete = miscDomains.get(Constants.ZERO).getMiscValue();
        //}
        //if(null == miscDomains || Constants.ZERO == miscDomains.size()){
        //    MessageUtil.getErrorMessageForBatchWithLabel(locale,
        //        SupplierPortalConstant.ERROR_CD_SP_E6_0001,
        //        null, null, LOG, Constants.TWO, false);
            
        //    MessageUtil.throwsApplicationMessage(locale, 
        //        SupplierPortalConstant.ERROR_CD_SP_E6_0001, null);
        //}else{
        
        miscDomain.setMiscType(Constants.EXPIRE_MESSAGE);
        miscDomain.setMiscCode(String.valueOf(Constants.ONE));
        Integer periodDelete = miscellaneousService.searchMiscValue(miscDomain);
        if(null == periodDelete){
            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                null, null, LOG, Constants.TWO, false);
            
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001, null);
        }else{
        // End : [IN044] Call wrong method to get misc value
        
            try{
                /** Call Façade service ImportMessageToSystemFacadeService deleteExpireMessage() to delete message expired. */
                SpsMAnnounceMessageCriteriaDomain spsMAnnounceMessageCriteriaDomain 
                    = new SpsMAnnounceMessageCriteriaDomain();
                
                // [IN044] : Incorrect logic to delete expire message
                //String periodDeleteDate = DateUtil.format(DateUtil.addDays(
                //    new java.sql.Date(this.commonService.searchSysDate().getTime()),
                //    Integer.parseInt(periodDelete)), DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH);
                int reduceDay = Constants.MINUS_ONE * periodDelete;
                String periodDeleteDate = DateUtil.format(DateUtil.addDays(
                    new java.sql.Date(this.commonService.searchSysDate().getTime()),
                    reduceDay), DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH);
                
                spsMAnnounceMessageCriteriaDomain.setEffectEndLessThanEqual(
                    DateUtil.parseToTimestamp(periodDeleteDate, 
                        DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH));
                importMessageToSystemFacadeService.deleteExpireMessage(
                    spsMAnnounceMessageCriteriaDomain);
            }catch(ApplicationException e){
                LOG.error(e.getMessage());
                throw e;
            }
        }
    }

    /**
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @SuppressWarnings("resource")
    @Override
    public void mainBizProc(JobInputDomain arg0) throws ApplicationException
    {
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        
        MessageUtil.getErrorMessageForBatchWithLabel(locale,
            SupplierPortalConstant.ERROR_CD_SP_I5_0001,
            null, null, LOG, Constants.ZERO,
            false);
        MessageUtil.getErrorMessageForBatchWithLabel(locale,
            SupplierPortalConstant.ERROR_CD_SP_I5_0002,
            null, null, LOG, Constants.ZERO,
            false);
       
        SpsMCompanyDensoCriteriaDomain spsMCompanyDensoCriteriaDomain 
            = new SpsMCompanyDensoCriteriaDomain();
        List<SpsMCompanyDensoDomain> spsMCompanyDensoDomains 
            = new ArrayList<SpsMCompanyDensoDomain>();
        
        /** Call Façade service ImportMessageToSystemFacadeService searchFileConfiguration to get  file configuration without condition. */
        spsMCompanyDensoDomains = importMessageToSystemFacadeService.searchFileConfiguration(
            spsMCompanyDensoCriteriaDomain);
        if(null == spsMCompanyDensoDomains){
            
            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                null, null, LOG, Constants.TWO, false);
            
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001, null);
            
            return;
        }else{
            String densoCode = Constants.EMPTY_STRING;
            String filePath = Constants.EMPTY_STRING;
            String tempPath = Constants.EMPTY_STRING;
            String csvFile = Constants.EMPTY_STRING;
            
            // [IN008] initial when start loop
            //List<AnnounceMessageDomain> announceMessageDomainList = 
            //    new ArrayList<AnnounceMessageDomain>();

            /** Get Message from CSV. */
            for(int i = Constants.ZERO; i < spsMCompanyDensoDomains.size(); ++i){
                
                // [IN008] initial when start loop
                List<AnnounceMessageDomain> announceMessageDomainList = 
                    new ArrayList<AnnounceMessageDomain>();

                densoCode = spsMCompanyDensoDomains.get(i).getDCd();
                filePath = spsMCompanyDensoDomains.get(i).getFilePath();
                tempPath = spsMCompanyDensoDomains.get(i).getTempPath();
                csvFile = filePath;
                StringBuffer bufferString = new StringBuffer();
                
                File oldfile = new File(filePath);
                String dotfileName =  oldfile.getName();
                String getPathTmp = Constants.EMPTY_STRING;
                
                MessageUtil.getErrorMessageForBatch(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I5_0003,
                    new String[] {csvFile}, LOG, Constants.ZERO, false);
                                
                BufferedReader input = null;
                String sCurrentLine;
                List<String> messageList = null;
                String[] messageListCheck = null;
                
                AnnounceMessageDomain announceMessageDomain = null;
                
                try{
                    /** Get record and validate the content from InputStream. */
//                    Change for get input stream from file path.
//                    input = new BufferedReader(new FileReader(csvFile));
                    InputStream inputStream = new FileInputStream(csvFile);
                    inputStream = FileUtil.checkUtf8BOMAndSkip(inputStream);
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    input = new BufferedReader(inputStreamReader);
                    
                    File checkSize = new File(csvFile);
                    
                    if(checkSize.exists()){
                        double  bytes = checkSize.length();
                        double kilobytes = bytes / 1024;
                        double megabytes = kilobytes / 1024;
                        
                        if(Constants.THREE < megabytes){
                            
                            StringBuffer fileSizeError = new StringBuffer();
                            fileSizeError.append(String.valueOf(Constants.THREE));
                            fileSizeError.append(Constants.SYMBOL_SPACE);
                            fileSizeError.append(String.valueOf(Constants.MEGABYTES));
                            
                            MessageUtil.getErrorMessageForBatch(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0015,
                                new String[] {fileSizeError.toString()}, 
                                LOG, Constants.ZERO, false);
                            continue;
                        }
                    }
                    
                    while (null != (sCurrentLine = input.readLine())) {
                        
                        announceMessageDomain = new AnnounceMessageDomain();
                        messageList = new ArrayList<String>();
                        
                        List<String> result = StringUtil.splitCsvData(sCurrentLine, 
                            Constants.THREE);
                        messageListCheck = sCurrentLine.split(Constants.SYMBOL_COMMA);
                        for (String temp : result) {
                            messageList.add(temp);
                        }
                        
                        if(Constants.ZERO == messageList.size()){
                            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E5_0004,
                                null, null, LOG, Constants.TWO,
                                false);
                            
                            continue;
                            
                        }
                        
                        /** Call Façade service ImportMessageToSystemFacadeService validateMessage() to validate content. */
                        List<String> messageError = new ArrayList<String>();
                        messageError = importMessageToSystemFacadeService.validateMessage(
                            messageList, messageListCheck);
                        if(Constants.ZERO != messageError.size()){
                            for (String temp : messageError) {
                                bufferString = new StringBuffer();
                                MessageUtil.getErrorMessageForBatchWithLabel(locale,
                                    temp, null, null, LOG, Constants.TWO, false);
                            }
                            continue;
                        }else{
                            announceMessageDomain.setMessageList(messageList);
                            announceMessageDomain.setFilePath(csvFile);
                            announceMessageDomain.setDCd(densoCode);
                            
                            announceMessageDomainList.add(announceMessageDomain);
                        }
                    }
                    
                    // [IN008] : continue if not have message to import
                    if (Constants.ZERO == announceMessageDomainList.size()) {
                        continue;
                    }
                    
                    String filename = dotfileName.substring(
                        Constants.ZERO, dotfileName.length() - Constants.FOUR);
                    
                    bufferString = new StringBuffer();
                    bufferString.append(tempPath);
                    bufferString.append(filename);
                    bufferString.append(Constants.SYMBOL_UNDER_SCORE);
                    
                    bufferString.append(DateUtil.format(commonService.searchSysDate(), 
                        DateUtil.PATTERN_YYYYMMDD_HHMMSS));
                    bufferString.append(Constants.SYMBOL_DOT);
                    bufferString.append(Constants.REPORT_CSV_FORMAT);
                    getPathTmp = bufferString.toString();
                    announceMessageDomainList.get(Constants.ZERO).setTempPath(getPathTmp);
                    
                    if(null != input){
                        input.close();
                    }
                    
                    if(oldfile.renameTo(new File(getPathTmp))){
                        oldfile.delete();
                        oldfile.deleteOnExit();
                    }else{
                        bufferString = new StringBuffer();
                        
                        MessageUtil.getErrorMessageForBatchWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E5_0018,
                            null, null, LOG, Constants.TWO, false);
                        
                        MessageUtil.throwsApplicationMessage(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E5_0018, null);
                    }
                    
                    /** Call Façade service ImportMessageToSystemFacadeService createMessage() to import message to system. */
                    importMessageToSystemFacadeService.createAnnouncementMessage(
                        announceMessageDomainList);
                    
                }catch(FileNotFoundException e){
                    MessageUtil.getErrorMessageForBatchWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E5_0003,
                        null, null, LOG, Constants.TWO, false);
                    continue;
                }catch(IOException e){
                    MessageUtil.getErrorMessageForBatchWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E5_0004,
                        null, null, LOG, Constants.TWO, false);
                    continue;
                }finally{
                    if(null != input){
                        try{
                            input.close();
                        }catch (IOException ioe){
                            LOG.error(ioe.getMessage());
                            continue;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void postBizProc(JobInputDomain arg0) throws ApplicationException {
        
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        
        /** Call Façade service ImportMessageToSystemFacadeService searchTempFileBackup to list of all expire file backup. */
        SpsTmpFileBackupCriteriaDomain spsTmpFileBackupCriteriaDomain = 
            new SpsTmpFileBackupCriteriaDomain();
        List<SpsTmpFileBackupDomain> spsTmpFileBackupDomainList = 
            new ArrayList<SpsTmpFileBackupDomain>();
        
        spsTmpFileBackupDomainList = this.importMessageToSystemFacadeService.
            searchTempFileBackup(spsTmpFileBackupCriteriaDomain);
        
        if(null != spsTmpFileBackupDomainList){
            for (int i = Constants.ZERO; i < spsTmpFileBackupDomainList.size(); ++i ) {
                Date date = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                
                long createMonth = spsTmpFileBackupDomainList.get(i).getCreateDatetime().getTime();
                Calendar calCreateMonth = Calendar.getInstance();
                cal.setTimeInMillis(createMonth);
                
                int sysMonth = cal.get(Calendar.MONTH);
                int dbMonth  = calCreateMonth.get(Calendar.MONTH);
                
                MiscellaneousDomain miscDomain = new MiscellaneousDomain();
                miscDomain.setMiscType(Constants.EXPIRE_MESSAGE);
                miscDomain.setMiscCode(Constants.STR_TWO);
                int month = miscellaneousService.searchMiscValue(miscDomain);
                if(month < dbMonth - sysMonth){
                    String getFilePath = spsTmpFileBackupDomainList.get(i).getFilePath();
                    spsTmpFileBackupCriteriaDomain.
                    setSeqNo(spsTmpFileBackupDomainList.get(i).getSeqNo());
                    
                    File currentFile = new File(getFilePath);
                    currentFile.delete();   
                    
                    /** Call Façade service ImportMessageToSystemFacadeService deleteTempFileBackupExpire(). */
                    this.importMessageToSystemFacadeService.
                        deleteTempFileBackupExpire(spsTmpFileBackupCriteriaDomain);
                }
            }
        }else{
            
            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                null, null, LOG, Constants.TWO, false);
            
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001, null);
            
        }
        
        MessageUtil.getErrorMessageForBatchWithLabel(locale,
            SupplierPortalConstant.ERROR_CD_SP_I5_0005,
            null, null, LOG, Constants.ZERO,
            false);
        
        MessageUtil.getErrorMessageForBatchWithLabel(locale,
            SupplierPortalConstant.ERROR_CD_SP_I5_0006,
            null, null, LOG, Constants.ZERO,
            false);
    }

}
