package com.globaldenso.asia.sps.jobcontrol;

import java.util.Locale;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.business.domain.JobManageDomain;
import com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.conf.ApplicationConfig;

/**
 * BatchJobRunner
 * */
public class BatchJobRunner_Purge {
    
    /** constructor */
    public BatchJobRunner_Purge() {
    }
    
    /**
     * @param args args
     * @throws Exception Exception
     * */
    public static void main(String[] args) throws Exception {
        
        /*Section1 Config File*/
        //Config No.1//
//        String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-transferPoDataFromCigma.xml";
        //Config No.2//
//        String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-transferDoDataFromCigma.xml";
        //Config No.3//
        //String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-transferInvoiceDataToJde.xml";
        //Config No.4//
        //String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-updatePaymentStatusFromJde.xml";
        //Config No.5//
        //String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-importMessageToSystem.xml";
        //Config No.6//
        //String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-poFoceAcknowledge.xml";
        //Config No.7//
        String configFile = "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-purgeDataProcess.xml";

        
        /*Section2 BeanId */
        //Config No.1//
//        String beanId = "residentBizProcExecute";
        //Config No.2//
//        String beanId = "residentBizProcExecute";
        //Config No.3//
        //String beanId = "BINV001ChildJob";
        //Config No.4//
        //String beanId = "BINV002ChildJob";
        //Config No.5//
        //String beanId = "importMessageToSystemChildJob";
        //Config No.6// 
        //String beanId = "poFoceAcknowledgeChildJob";
        //Config No.7//
        String beanId = "BADM002ChildJob";
        
        
        /*Section3 JobId */
        //Config No.1//
//        String jobId = "BORD004";
        //Config No.2//
//        String jobId = "BORD005";
        //Config No.3//
        //String jobId = "BORD004";
        //Config No.4//
        //String jobId = "BORD004";
        //Config No.5//
        //String jobId = "BORD004";
        //Config No.6// 
        //String jobId = "BORD004";
        //Config No.7//
        String jobId = "BORD004";
        
        /*Section4 executeArg */
        String executeArg = args[0];
        
        config();
        
        ClassPathXmlApplicationContext context =
            new ClassPathXmlApplicationContext( 
                new String[]{configFile} );

        
        /*Section5 job */
        //Config No.1//
//        TransferPoDataFromCigmaResidentJob job = 
//            (TransferPoDataFromCigmaResidentJob)context.getBean(beanId);
        //Config No.2//
//        TransferDoDataFromCigmaResidentJob job = 
//            (TransferDoDataFromCigmaResidentJob)context.getBean(beanId);
        //Config No.3//
        //TransferInvoiceDataToJdeChildJob job = 
        //    (TransferInvoiceDataToJdeChildJob)context.getBean(beanId);
        //Config No.4//
        //UpdatePaymentStatusFromJdeChildJob job = 
        //    (UpdatePaymentStatusFromJdeChildJob)context.getBean(beanId);
        //Config No.5//
        //ImportMessageToSystemChildJob job = 
        //    (ImportMessageToSystemChildJob)context.getBean(beanId);
        //Config No.6//
        //PoFoceAcknowledgeChildJob job = 
        //    (PoFoceAcknowledgeChildJob)context.getBean(beanId);
        //Config No.7//
        PurgeDataProcessChildJob job = 
            (PurgeDataProcessChildJob)context.getBean(beanId);
        
        
        
        JobManageDomain jobManageDomain = new JobManageDomain();
        jobManageDomain.setExecuteArg(executeArg);
        jobManageDomain.setJobId(jobId); // 0000
        QueueInfDomain queueInfDomain = new QueueInfDomain();
        
        JobInputDomain scheduleParam = new JobInputDomain();
        scheduleParam.setJobId(jobId);
        scheduleParam.setExecuteArg(executeArg);
        
        try {
        /*Section6 job param*/
        //Config No.1//            
//            job.jobCall(jobManageDomain, queueInfDomain);
        //Config No.2//    
//            job.jobCall(jobManageDomain, queueInfDomain);
        //Config No.3//          
        //    job.preBizProc(scheduleParam);
        //    job.mainBizProc(scheduleParam);
        //    job.postBizProc(scheduleParam);   
        //Config No.4//          
        //    job.preBizProc(scheduleParam);
        //    job.mainBizProc(scheduleParam);
        //    job.postBizProc(scheduleParam);             
        //Config No.5//          
        //    job.preBizProc(scheduleParam);
        //    job.mainBizProc(scheduleParam);
        //    job.postBizProc(scheduleParam);   
        //Config No.6//          
        //    job.preBizProc(scheduleParam);
        //    job.mainBizProc(scheduleParam);
        //    job.postBizProc(scheduleParam); 
        //Config No.7//          
            job.preBizProc(scheduleParam);
            job.mainBizProc(scheduleParam);
            job.postBizProc(scheduleParam); 

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * S
     * */
    private static void config(){
        try {
            Locale.setDefault(new Locale("en", "US"));
            String baseDirAppConf = "com.globaldenso.asia.sps.conf.ApplicationConfig";
            ContextParams.setBaseDirApplicationConf(baseDirAppConf);
            
            String baseDirGui = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_GUI.name());
            ContextParams.setBaseDirGui(baseDirGui);
            
            String baseDirReportGui = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_REPORT_GUI.name());
            ContextParams.setBaseDirReportGui(baseDirReportGui);
            
            String baseEmail = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_EMAIL.name());
            ContextParams.setBaseEmail(baseEmail);

            String baseDirMsg = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_MSG.name());
            ContextParams.setBaseDirMsg(baseDirMsg);

            
            String legendFileId = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.LEGEND_FILE_ID.name());
            ContextParams.setLegendFileId(legendFileId);
            
            String maxCsvFileSize = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.MAX_CSV_FILE_SIZE.name());
            ContextParams.setMaxCsvFileSize(Integer.valueOf(maxCsvFileSize));
            
            ContextParams.setJasperFilePath("C:/Eclipse_AIJ3/workspace/SPS/report/");
            
            String emailSmtp = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.EMAIL_SMTP.name());
            ContextParams.setEmailSmtp(emailSmtp);
            
            String defaultEmailFrom = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.DEFAULT_EMAIL_FROM.name());
            ContextParams.setDefaultEmailFrom(defaultEmailFrom);
            
            String defaultCigmaPoNo = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.DEFAULT_CIGMA_PO_NO.name());
            ContextParams.setDefaultCigmaPoNo(defaultCigmaPoNo);
            
            // Purge process send e-mail to person in charge
            String purgeEmailSendTo = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.PURGE_EMAIL_SEND_TO.name());
            ContextParams.setPurgeEmailSendTo(purgeEmailSendTo);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
