/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 * 2016/03/14 CSI Akat            [IN056-2]
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobManageDomain;
import com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.service.TransferDoDataToCigmaService;
import com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService;
import com.globaldenso.asia.sps.business.service.TransferPoDataToCigmaService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * This job has a purpose to read CSV file that upload by each DENSO<br />
 * company to the system. After user login to SPS system success<br />
 * the system will load a message to show at the announcement message<br />
 * section at "Main Screen" page.
 * */
public class TransferDoDataToCigmaJob extends ResidentBizProcExecute {
	/** The Constant LOG. */
	private static final Log LOG = LogFactory
			.getLog(TransferDoDataToCigmaJob.class);
	/** Cigma SPS Flag Error */
	private static final String SPS_FLAG_ERROR = Constants.STR_TWO;
	/** Cigma SPS Flag Normal */
	private static final String SPS_FLAG_NORMAL = Constants.STR_ZERO;

	/** The import message facade service. */
	private TransferDoDataToCigmaService transferDoDataToCigmaService;

	/**
	 * Default constructor.
	 * */
	public TransferDoDataToCigmaJob() {
		super();
	}

	/**
	 * <p>
	 * Setter method for transferPoDataFromCigmaFacadeService.
	 * </p>
	 * 
	 * @param transferPoDataFromCigmaFacadeService
	 *            Set for transferPoDataFromCigmaFacadeService
	 */
	public void setTransferDoDataToCigmaService(
			TransferDoDataToCigmaService transferDoDataToCigmaService) {
		this.transferDoDataToCigmaService = transferDoDataToCigmaService;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute#jobCall(com.globaldenso.ai.aijb.core.business.domain.JobManageDomain,
	 *      com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain)
	 */

	@Override
	protected boolean jobCall(JobManageDomain jobManageDomain,
			QueueInfDomain queueInfDomain) {
		LoadContextPropertyForBatch.loadContextListener(LOG);
		// super.jobCall(jobManageDomain, queueInfDomain);
		String jobId = null;
		if (null != jobManageDomain.getJobId()
				&& Constants.TWELVE < jobManageDomain.getJobId().length()) {
			jobId = jobManageDomain.getJobId().substring(Constants.ZERO,
					Constants.TWELVE);
		} else {
			jobId = jobManageDomain.getJobId();
		}
		boolean result = false;
		Locale locale = this.getDefaultLocale();
		// 1 Out Put Log
		LOG.info(MessageUtil.getApplicationMessageWithLabel(locale,
				SupplierPortalConstant.ERROR_CD_SP_I6_0014,
				SupplierPortalConstant.TRANSFER_DO_STEP_PROCESS_NAME));
		List<String> arguList = new ArrayList<String>();
		try {
			arguList.addAll(Arrays.asList(StringUtils.splitPreserveAllTokens(
					jobManageDomain.getExecuteArg(), Constants.SYMBOL_COMMA)));
			
			String strResult = null;
			// LOOP all data in <List>as400Server
			

		/*	SpsMCompanyDensoDomain company = new SpsMCompanyDensoDomain();
			company.setDCd(StringUtil
					.convertListToVarcharCommaSeperate(arguList));
			// Call Façade Service TransferDODataFromCIGMAFacadeService
			// searchAS400ServerList()
			List<As400ServerConnectionInformationDomain> as400Server = transferDoDataToCigmaService.searchAs400ServerList(company);

			if (null != as400Server) {
				if (Constants.ZERO == as400Server.size()) {
					// Log OutPut Log
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E5_0026,
									new String[] { company.getDCd() }));
				} else if (as400Server.size() < arguList.size()) {
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E5_0025,
									new String[] { company.getDCd() }));
				}
			} else {
				LOG.error(MessageUtil.getApplicationMessageHandledException(
						locale, SupplierPortalConstant.ERROR_CD_SP_E5_0026,
						new String[] { company.getDCd() }));
				// MessageUtil.throwsApplicationMessage(locale,
				// SupplierPortalConstant.ERROR_CD_SP_E5_0026, null
				// , new String[]{company.getDCd()} );
			}

			// LOOP all data in <List>as400Server
			if (null == as400Server) {
				return true;
			}*/
			//for (As400ServerConnectionInformationDomain server : as400Server) {
				try {

					SpsMDensoDensoRelationWithDODNDomain doCriteria = new SpsMDensoDensoRelationWithDODNDomain();
					doCriteria.setdCd(StringUtil.convertListToVarcharCommaSeperate(arguList));
					//doCriteria.setDatabaseName(server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
					List<SpsMDensoDensoRelationWithDODNDomain> doData = transferDoDataToCigmaService.searchDO(doCriteria);
                    if(null != doData){
                    	if(doData.size() >0){
                    		As400ServerConnectionInformationDomain server = new As400ServerConnectionInformationDomain();
                    			this.transferDoDataToCigmaService.transferDO(server, SPS_FLAG_ERROR,doData);
                    	}else{
   					     MessageUtil.getErrorMessageForBatch(
				                    locale, 
				                    SupplierPortalConstant.ERROR_CD_SP_E6_0060, 
				                    null, 
				                    LOG, 
				                    Constants.ZERO, 
				                    false);
                    		}
                    }else{
					     MessageUtil.getErrorMessageForBatch(
				                    locale, 
				                    SupplierPortalConstant.ERROR_CD_SP_E6_0060, 
				                    null, 
				                    LOG, 
				                    Constants.ZERO, 
				                    false);
					}
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
				}
			//} // end for each server.

			result = true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			Locale objLocale = LocaleUtility
					.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
			String message = MessageUtil.getApplicationMessageWithLabel(
					objLocale, SupplierPortalConstant.ERROR_CD_SP_I6_0015,
					SupplierPortalConstant.TRANSFER_DO_STEP_PROCESS_NAME);
			LOG.info(message);
		}

		return result;
	}

	/**
	 * Gets the content.
	 * 
	 * @return default locale
	 */
	private Locale getDefaultLocale() {
		Properties propApp = Props
				.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
		String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
		String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
		String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
		ContextParams.setDefaultLocale(strDefaultLocale);
		ContextParams.setBaseDirGui(strBaseDirGui);
		ContextParams.setBaseDirMsg(strBaseDirMsg);
		return LocaleUtil.getLocaleFromString(strDefaultLocale);
	}

	// public static void main(String[] args) {
	// TransferPoDataFromCigmaResidentJob childJob = new
	// TransferPoDataFromCigmaResidentJob();
	// JobManageDomain jobManageDomain = new JobManageDomain();
	// jobManageDomain.setExecuteArg( "DNTH" );
	// jobManageDomain.setJobId("BORD00040000");
	// QueueInfDomain queueInfDomain = new QueueInfDomain();
	// childJob.jobCall(jobManageDomain, queueInfDomain);
	// }
}
