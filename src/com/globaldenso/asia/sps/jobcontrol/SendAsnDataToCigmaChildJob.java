/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/26 CSI Parichat        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ChildJobBase;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.SendAsnDataToCigmaDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;
import com.globaldenso.asia.sps.business.service.SendAsnDataToCigmaFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * This batch job for Sending ASN data which mark 'Transfer Flag'<br />
 * at SPS table - ASN Header uncompleted to CIGMA DB
 * */
public class SendAsnDataToCigmaChildJob extends ChildJobBase {
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(SendAsnDataToCigmaChildJob.class);
    
    /** The send asn to cigma facade service. */
    private SendAsnDataToCigmaFacadeService sendAsnDataToCigmaFacadeService;
    
    /**
     * Default constructor.
     * */
    public SendAsnDataToCigmaChildJob() {
        
    }

    /**
     * <p>Setter method for send asn data to cigma facade service.</p>
     *
     * @param sendAsnDataToCigmaFacadeService Set for send asn data to cigma facade
     */
    public void setSendAsnDataToCigmaFacadeService(
        SendAsnDataToCigmaFacadeService sendAsnDataToCigmaFacadeService) {
        this.sendAsnDataToCigmaFacadeService = sendAsnDataToCigmaFacadeService;
    }

    /**
     * Auto-generated method stub : do nothing.
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void preBizProc(JobInputDomain arg0) throws ApplicationException
    {
        LoadContextPropertyForBatch.loadContextListener(LOG);
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        MessageUtil.getErrorMessageForBatchWithLabel(locale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0014,
            SupplierPortalConstant.LBL_SEND_ASN_DATA_TO_CIGMA, null, LOG, Constants.ZERO, false);
    }

    /**
     * 
     * @param arg0 job input domain
     * @throws ApplicationException 
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_UNCHECKED, Constants.SUPPRESS_WARNINGS_RAWTYPES})
    @Override
    public void mainBizProc(JobInputDomain arg0) throws ApplicationException{
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        SendingAsnDomain criteria = new SendingAsnDomain();
        SendingAsnDomain sendingAsnResult = null;
        
        List<String> dCdList = Arrays.asList(
            StringUtils.splitPreserveAllTokens(arg0.getExecuteArg(), Constants.SYMBOL_COMMA));
        
        criteria.setDCd(StringUtil.convertListToCharCommaSeperate(
            dCdList, SupplierPortalConstant.LENGTH_DENSO_CODE));
        
        try{
            sendingAsnResult = sendAsnDataToCigmaFacadeService.searchSendingAsn(criteria);
        }catch(Exception e){
            MessageUtil.getErrorMessageForBatch(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0004,
                new String[] {e.getClass().toString(), e.getMessage()}, LOG, Constants.TWO, true);
        }
        
        if(null == sendingAsnResult.getAsnInformationList() 
            || Constants.ZERO == sendingAsnResult.getAsnInformationList().size()){
            MessageUtil.getErrorMessageForBatch(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0012, null, LOG, Constants.TWO, true);
        }
        
        /** 2.1 Get unique SendingASNDomain. densoCode and set to <List> densoCode 
         *  2.2 Separate <List> ASNInfoDomain by set new <List> ASNInfoDomain for each same
         * */
        Map asnInfoMap = new HashMap();
        List<AsnInfoDomain> asnInfoTempList = null;
        List<String> densoCodeList = new ArrayList<String>();
        for(AsnInfoDomain asnInfo : sendingAsnResult.getAsnInformationList()){
            if(!asnInfoMap.containsKey(asnInfo.getSpsTAsnDomain().getDCd())){
                densoCodeList.add(asnInfo.getSpsTAsnDomain().getDCd());
                asnInfoTempList = new ArrayList<AsnInfoDomain>();
                asnInfoTempList.add(asnInfo);
                asnInfoMap.put(asnInfo.getSpsTAsnDomain().getDCd(), asnInfoTempList);
            }else{
                asnInfoTempList = (List<AsnInfoDomain>)asnInfoMap.get(
                    asnInfo.getSpsTAsnDomain().getDCd());
                asnInfoTempList.add(asnInfo);
            }
        }
        
        //3. Call Facade Service SendAsnDataToCigmaFacadeService  searchAS400ServerList()
        List<As400ServerConnectionInformationDomain> as400ServerList = null;
        try{
            String densoCode = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
            as400ServerList = sendAsnDataToCigmaFacadeService.searchAs400ServerList(densoCode);
        }catch(Exception e){
            MessageUtil.getErrorMessageForBatch(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0004,
                new String[] {e.getClass().toString(), e.getMessage()}, LOG, Constants.TWO, true);
        }
        
        //Warning there are no config for all company
        if(null == as400ServerList || Constants.ZERO == as400ServerList.size()){
            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                SupplierPortalConstant.LBL_DENSO_CODE, null, LOG, Constants.TWO, true);
        }
        //Warning there are no config for some company
        if(as400ServerList.size() != densoCodeList.size()){
            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0025,
                SupplierPortalConstant.LBL_DENSO_COMPANY, null, LOG, Constants.ONE, false);
        }
        SendAsnDataToCigmaDomain sendAsnDataToCigmaDomain = null; 
        for(As400ServerConnectionInformationDomain as400Server : as400ServerList)
        {
            List<AsnInfoDomain> asnInformationList = (List<AsnInfoDomain>)asnInfoMap
                .get(as400Server.getSpsMCompanyDensoDomain().getDCd());
            for(AsnInfoDomain asnInformation : asnInformationList){
                sendAsnDataToCigmaDomain = new SendAsnDataToCigmaDomain();
                sendAsnDataToCigmaDomain.setLocale(locale);
                sendAsnDataToCigmaDomain.setAsnInformationDomain(asnInformation);
                sendAsnDataToCigmaDomain.setAs400ServerInformation(as400Server);
                sendAsnDataToCigmaDomain.setLastUpdateDscId(arg0.getJobId());
                try{
                    sendAsnDataToCigmaFacadeService.transactSendAsn(
                        sendAsnDataToCigmaDomain);
                }catch(WebServiceCallerRestException e){
                    LOG.error(e.getMessage());
                    break;
                }catch(ApplicationException e){
                    LOG.error(e.getMessage());
                }catch(Exception e){
                    MessageUtil.getErrorMessageForBatch(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0004, new String[]{
                            MessageUtil.getLabelHandledException(locale,
                                SupplierPortalConstant.LBL_ERROR),
                            MessageUtil.getLabelHandledException(locale,
                                SupplierPortalConstant.LBL_INNER_TEXT_FROM_EXCEPTION)},
                                LOG, Constants.TWO, false);
                }
            }
        }
    }

    /**
     * Post Biz Proc
     * @param arg0 job input domain
     * @throws ApplicationException 
     */
    @Override
    public void postBizProc(JobInputDomain arg0) throws ApplicationException{
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        MessageUtil.getErrorMessageForBatchWithLabel(locale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0015, 
            SupplierPortalConstant.LBL_SEND_ASN_DATA_TO_CIGMA, null, LOG, Constants.ZERO, false);
    }
    
//    /**
//     * Main.
//     * 
//     * @param args the string[]
//     * */
//    public static void main(String[] args){
//        SendAsnDataToCigmaChildJob childJob = new SendAsnDataToCigmaChildJob();
//        String[] configLocations = {Constants.APPLICATION_CONTEXT_AIJB_JOB_XML,
//            "com/globaldenso/asia/sps/jobcontrol/applicationContext-aijb-sendAsnDataToCigma.xml"};
//        ClassPathXmlApplicationContext context
//            = new ClassPathXmlApplicationContext(configLocations);
//        
//        SendAsnDataToCigmaFacadeService sendAsnDataToCigmaFacadeService
//            = (SendAsnDataToCigmaFacadeService)context.getBean("sendAsnDataToCigmaFacadeService");
//        childJob.setSendAsnDataToCigmaFacadeService(sendAsnDataToCigmaFacadeService);
//        
//        JobInputDomain arg = new JobInputDomain();
//        arg.setExecuteArg("DNTH");
//        arg.setJobId(SupplierPortalConstant.JOB_ID_BORD003);
//        
//        try{
//            childJob.preBizProc(arg);
//        }catch(ApplicationException ex){
//            ex.printStackTrace();
//        }
//        try{
//            childJob.mainBizProc(arg);
//        }catch(ApplicationException ex){
//            ex.printStackTrace();
//        }
//        try{
//            childJob.postBizProc(arg);
//        }catch(ApplicationException ex){
//            ex.printStackTrace();
//        }
//    }
}
