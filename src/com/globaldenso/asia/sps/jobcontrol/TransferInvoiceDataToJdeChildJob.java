/*
 * ModifyDate Development company     Describe 
 * 2014/08/29 CSI Akat                Create
 * 2014/11/25 CSI Akat                IN040
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ChildJobBase;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceDetailDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.business.service.CommonService;
import com.globaldenso.asia.sps.business.service.TransferInvoiceToJdeFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * <p>The Class TransferInvoiceDataToJdeChildJob.</p>
 * <p>Batch process BINV001 Schedule Transfer Invoice Data to JDE.</p>
 * 
 * @author CSI
 */
public class TransferInvoiceDataToJdeChildJob extends ChildJobBase {

    /** LOG for output log. */
    private static final Log LOG = LogFactory.getLog(TransferInvoiceDataToJdeChildJob.class);

    /** The Facade for Transfer Invoice to JDE. */
    private TransferInvoiceToJdeFacadeService transferInvoiceToJdeFacadeService;

    /** The Common Service. */
    private CommonService commonService;
    
    /** The default constructor. */
    public TransferInvoiceDataToJdeChildJob() {
        super();
    }
    
    /**
     * <p>Setter method for transferInvoiceToJdeFacadeService.</p>
     *
     * @param transferInvoiceToJdeFacadeService Set for transferInvoiceToJdeFacadeService
     */
    public void setTransferInvoiceToJdeFacadeService(
        TransferInvoiceToJdeFacadeService transferInvoiceToJdeFacadeService) {
        this.transferInvoiceToJdeFacadeService = transferInvoiceToJdeFacadeService;
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * Batch Pre process output log and check parameter.
     * @param arg0 argument AI-JM
     * @throws ApplicationException an ApplicationException
     * */
    @Override
    public void preBizProc(JobInputDomain arg0) throws ApplicationException {
        LoadContextPropertyForBatch.loadContextListener(LOG);
        
        // 1. OutPut Log
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        MessageUtil.getErrorMessageForBatchWithLabel(objLocale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0014,
            SupplierPortalConstant.LBL_SCHEDULE_TRANSFER_INVOICE, null, LOG, Constants.ZERO,
            false);
        
        // 2. Set Parameter
        // ** have to do in main process
    }

    /**
     * Batch Main Process Transfer Invoice Data to JDE.
     * @param arg0 argument AI-JM
     * @throws ApplicationException an ApplicationException
     * */
    @Override
    public void mainBizProc(JobInputDomain arg0) throws ApplicationException {
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        String label = null;
        
        String param = arg0.getExecuteArg();

        String[] densoCodeArray = param.split(Constants.SYMBOL_COMMA);
        List<String> densoCodeList = new ArrayList<String>();
        for (String densoCode : densoCodeArray) {
            densoCodeList.add(densoCode.trim());
        }
        String densoCodeVarcharCsv = StringUtil.convertListToCharCommaSeperate(densoCodeList
            , Constants.FIVE);

        // 1. Get AS400 server list from TransferInvoiceToJDEFacadeService
        SpsMCompanyDensoDomain companyCriteria = new SpsMCompanyDensoDomain();
        companyCriteria.setDCd(densoCodeVarcharCsv);
        List<As400ServerConnectionInformationDomain> serverList = null;
        try {
            serverList = this.transferInvoiceToJdeFacadeService.searchAs400ServerList(
                companyCriteria);
        } catch (Exception e) {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_90_0001
                , new String[]{e.getClass().toString(), e.getMessage()}
                , LOG, Constants.TWO, true);
        }
        
        if (null == serverList || Constants.ZERO == serverList.size()) {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0026, new String[] {param}, LOG,
                Constants.TWO, true);
        }

        if (densoCodeArray.length != serverList.size()) {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0025, new String[] {param}, LOG,
                Constants.TWO, false);
        }
        
        List<String> asnNoList = null;
        // Check Duplicate ASN_NO before check ASN Receiving Status
        Set<String> asnNoSet = null;
        
        String asnNoIn = null;
        // 2. LOOP all data in <List> as400Server
        for (As400ServerConnectionInformationDomain server : serverList) {
            try {
                // 2.1 Get system date time
                Timestamp systemDatetime = this.commonService.searchSysDate();
                
                // 2.2 Get Invoice data from TransferInvoiceToJDEFacadeService
                InvoiceInformationDomain invoiceCriteria = new InvoiceInformationDomain();
                invoiceCriteria.setDCd(server.getSpsMCompanyDensoDomain().getDCd());
                List<InvoiceInformationDomain> invoiceList
                    = this.transferInvoiceToJdeFacadeService.searchInvoiceForTransferToJde(
                        invoiceCriteria);
                
                asnNoIn = null;
                if (null == invoiceList || Constants.ZERO == invoiceList.size()) {
                    MessageUtil.getErrorMessageForBatch(objLocale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0047,
                        new String[] {server.getSpsMCompanyDensoDomain().getDCd()}, LOG,
                        Constants.TWO, false);
                    continue;
                }
                
                for (InvoiceInformationDomain invoice : invoiceList) {
                    
                    // [IN040] If error in any invoice system must skip and continue to another
                    try {
                    
                        asnNoList = new ArrayList<String>();
                        // [IN040] Check Duplicate ASN_NO before check ASN Receiving Status
                        asnNoSet = new HashSet<String>();
                        
                        // 1) Check ASN Receiving status
                        for (InvoiceDetailDomain invoiceDetail
                            : invoice.getInvoiceDetailDomainList())
                        {
                            // [IN040] Check Duplicate ASN_NO before check ASN Receiving Status
                            //asnNoList.add(invoiceDetail.getSpsTInvoiceDetailDomain().getAsnNo());
                            asnNoSet.add(invoiceDetail.getSpsTInvoiceDetailDomain().getAsnNo());
                        }

                        // [IN040] Check Duplicate ASN_NO before check ASN Receiving Status
                        asnNoList.addAll(asnNoSet);
                        
                        asnNoIn = StringUtil.convertListToCharCommaSeperate(
                            asnNoList, Constants.SIXTEEN);
                        List<PseudoCigmaAsnDomain> asnReceivingList
                            = this.transferInvoiceToJdeFacadeService.searchAsnReceiving(
                                asnNoIn, server, objLocale);
                        if (null == asnReceivingList || Constants.ZERO == asnReceivingList.size())
                        {
                            label = MessageUtil.getLabelHandledException(objLocale,
                                SupplierPortalConstant.LBL_ASN_RECEIVING_NOT_EXIST);
                            MessageUtil.getErrorMessageForBatch(objLocale,
                                SupplierPortalConstant.ERROR_CD_SP_E6_0045,
                                new String[] {
                                    invoice.getSpsTInvoiceDomain().getInvoiceId().toString()
                                    , label}
                                , LOG, Constants.TWO, false);
                            continue;
                        }
                        
                        // 2) Transfer invoice data to SPS AP Interface Header and Detail File
                        boolean foundNotComplete = false;
                        for (PseudoCigmaAsnDomain asnReceiving : asnReceivingList) {
                            if (!Constants.ASN_STATUS_R.equals(
                                asnReceiving.getPseudoAsnStatus()))
                            {
                                foundNotComplete = true;
                                break;
                            }
                        }
                        if (foundNotComplete) {
                            continue;
                        }
                        
                        try {
                            this.transferInvoiceToJdeFacadeService.transactTransferDatatoJde(
                                invoice.getSpsTInvoiceDomain().getInvoiceId(), asnNoIn, systemDatetime,
                                server, invoice, arg0.getJobId());
                        } catch (ApplicationException applicationException) {
                            LOG.error(applicationException.getMessage());
                        } catch (Exception exception) {
                            String labelError = MessageUtil.getLabelHandledException(objLocale,
                                SupplierPortalConstant.LBL_ERROR);
                            MessageUtil.getErrorMessageForBatch(objLocale,
                                SupplierPortalConstant.ERROR_CD_SP_90_0004,
                                new String[] {labelError, exception.getMessage()},
                                LOG, Constants.TWO, false);
                        }

                    // [IN040] If error in any invoice system must skip and continue to another
                    } catch (ApplicationException applicationException) {
                        LOG.error(applicationException.getMessage());
                    } catch (Exception e) {
                        MessageUtil.getErrorMessageForBatch(objLocale,
                            SupplierPortalConstant.ERROR_CD_SP_90_0001
                            , new String[]{e.getClass().toString(), e.getMessage()}
                            , LOG, Constants.TWO, false);
                    }
                }
            } catch (ApplicationException applicationException) {
                LOG.error(applicationException.getMessage());
            } catch (Exception e) {
                MessageUtil.getErrorMessageForBatch(objLocale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}
                    , LOG, Constants.TWO, false);
            }
        }
    }

    /**
     * Batch Post process output log.
     * @param arg0 argument AI-JM
     * @throws ApplicationException an ApplicationException
     * */
    @Override
    public void postBizProc(JobInputDomain arg0) throws ApplicationException {
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        MessageUtil.getErrorMessageForBatchWithLabel(objLocale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0015,
            SupplierPortalConstant.LBL_SCHEDULE_TRANSFER_INVOICE, null, LOG, Constants.ZERO,
            false);
    }

}
