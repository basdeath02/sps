/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 * 2016/03/14 CSI Akat            [IN056-2]
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobManageDomain;
import com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * This job has a purpose to read CSV file that upload by each DENSO<br />
 * company to the system. After user login to SPS system success<br />
 * the system will load a message to show at the announcement message<br />
 * section at "Main Screen" page.
 * */
public class TransferPoDataFromCigmaResidentJob extends ResidentBizProcExecute {
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(TransferPoDataFromCigmaResidentJob.class);
    /** Cigma SPS Flag Error */
    private static final String SPS_FLAG_ERROR = Constants.STR_TWO;
    /** Cigma SPS Flag Normal */
    private static final String SPS_FLAG_NORMAL = Constants.STR_ZERO;
    
    /** The import message facade service. */
    private TransferPoDataFromCigmaFacadeService transferPoDataFromCigmaFacadeService;
    
    /**
     * Default constructor.
     * */
    public TransferPoDataFromCigmaResidentJob() {
        super();
    }

    /**
     * <p>Setter method for transferPoDataFromCigmaFacadeService.</p>
     *
     * @param transferPoDataFromCigmaFacadeService Set for transferPoDataFromCigmaFacadeService
     */
    public void setTransferPoDataFromCigmaFacadeService(
        TransferPoDataFromCigmaFacadeService transferPoDataFromCigmaFacadeService) {
        this.transferPoDataFromCigmaFacadeService = transferPoDataFromCigmaFacadeService;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute#jobCall(com.globaldenso.ai.aijb.core.business.domain.JobManageDomain, com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain)
     */
    
    @Override
    protected boolean jobCall(JobManageDomain jobManageDomain,
        QueueInfDomain queueInfDomain) {
        LoadContextPropertyForBatch.loadContextListener(LOG);
        //super.jobCall(jobManageDomain, queueInfDomain);
        String jobId = null;
        if( null != jobManageDomain.getJobId() 
            && Constants.TWELVE < jobManageDomain.getJobId().length()  ) {
            jobId = jobManageDomain.getJobId().substring(Constants.ZERO, Constants.TWELVE);
        }else{
            jobId = jobManageDomain.getJobId();
        }
        boolean result = false;
        Locale locale = this.getDefaultLocale();
        //      1   Out Put Log                                 
        LOG.info( MessageUtil.getApplicationMessageWithLabel(locale, 
            SupplierPortalConstant.ERROR_CD_SP_I6_0014, 
                SupplierPortalConstant.TRANSFER_PO_STEP_PROCESS_NAME ) );
        List<String> arguList = new ArrayList<String>();
        try {
            arguList.addAll( Arrays.asList( StringUtils
                .splitPreserveAllTokens(
                    jobManageDomain.getExecuteArg(), Constants.SYMBOL_COMMA) ) );
            SpsMCompanyDensoDomain company = new SpsMCompanyDensoDomain();
            company.setDCd( StringUtil.convertListToVarcharCommaSeperate(arguList) );
            // Call Façade Service TransferDODataFromCIGMAFacadeService  searchAS400ServerList()
            List<As400ServerConnectionInformationDomain> as400Server = 
                transferPoDataFromCigmaFacadeService.searchAs400ServerList(company);
            
            if( null != as400Server ){
                if(  Constants.ZERO == as400Server.size() ){
                    // Log OutPut Log
                    LOG.error(
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E5_0026
                            , new String[]{company.getDCd()} )
                    );
                }else if( as400Server.size() 
                    < arguList.size() ){
                    LOG.error(
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E5_0025
                            , new String[]{company.getDCd()} )
                    );
                }
            }else{
                LOG.error(
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E5_0026
                        , new String[]{company.getDCd()} )
                );
//                MessageUtil.throwsApplicationMessage(locale, 
//                    SupplierPortalConstant.ERROR_CD_SP_E5_0026, null
//                    , new String[]{company.getDCd()} );
            }
            String strResult = null;
            // LOOP all data in <List>as400Server
            if( null == as400Server ) {
                return true;
            }
//            as400Server = new ArrayList<As400ServerConnectionInformationDomain>();
            for(As400ServerConnectionInformationDomain server : as400Server) {
                try{
                    
                    // Start : [IN056] Transfer P/O for CIGMA by DENSO Company and CIGMA Vendor Code
                    // server.getSpsMCompanyDensoDomain().getDCd()
                    // [IN056-2] Not get Vendor code from Oracle, get from CIGMA
                    //List<SpsMAs400VendorDomain> vendorList
                    //    = this.transferPoDataFromCigmaFacadeService.searchCigmaVendorCd(
                    //        server.getSpsMCompanyDensoDomain().getDCd());
                    List<PseudoCigmaPoCoverPageInformationDomain> vendorList
                        = this.transferPoDataFromCigmaFacadeService.searchCigmaVendorCd(
                            server, SPS_FLAG_ERROR);

                    // [IN056-2] Not get Vendor code from Oracle, get from CIGMA
                    //for (SpsMAs400VendorDomain vendorDomain : vendorList) {
                    for (PseudoCigmaPoCoverPageInformationDomain vendorDomain : vendorList) {
                    // To be Continue [IN056]
                    
                        // 3. 3 Call Façade Service TransferDODataFromCIGMAFacadeService searchPurchaseOrder() to get D/O which are marked Error from previous batch running.
                        // List of TransferDODataFromCIGMA can find Supplier.
                        final List<List<TransferPoDataFromCigmaDomain>> transferList = 
                            new ArrayList<List<TransferPoDataFromCigmaDomain>>();

                        // [IN056] Add Vendor Code to criteria
                        //TransferPoDataFromCigmaResultDomain resultMap = 
                        //    transferPoDataFromCigmaFacadeService.searchPurchaseOrder(locale, 
                        //        server, SPS_FLAG_ERROR);
                        // [IN056-2] Not get Vendor code from Oracle, get from CIGMA
                        //TransferPoDataFromCigmaResultDomain resultMap = 
                        //    transferPoDataFromCigmaFacadeService.searchPurchaseOrder(locale, 
                        //        server, SPS_FLAG_ERROR, vendorDomain.getVendorCd());
                        TransferPoDataFromCigmaResultDomain resultMap = 
                            transferPoDataFromCigmaFacadeService.searchPurchaseOrder(locale, 
                                server, SPS_FLAG_ERROR, vendorDomain.getPseudoVendorCd());
                        
                        if(null != resultMap && Constants.ZERO == resultMap.getPoDataMap().size()) {
                            MessageUtil.getErrorMessageForBatch(
                                locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                                null, 
                                LOG, 
                                Constants.TWO, 
                                false);
        //                    continue;
                        } else {
                            List<TransferPoDataFromCigmaDomain> tmpList = null;
                            if( null != resultMap && null != resultMap.getPoDataMap() ) {
                                for(String cigmaPoNo : resultMap.getPoDataMap().keySet()){
                                    try {
                                        tmpList = transferPoDataFromCigmaFacadeService
                                            .transactCheckTransferPoFromCigma(locale, 
                                                resultMap.getPoDataMap().get(cigmaPoNo), 
                                                server, 
                                                true, 
                                                jobId,
                                                LOG);
                                        if( Constants.ZERO != tmpList.size() ) {
                                            transferList.add( tmpList );
                                        }
                                    } catch (Exception e) {
                                        LOG.error(e.getMessage(), e);
                                    }
                                }
                                
                                // 4    Separate CIGMA D/O Data which error from previous batch running to SPS system
            //                    SortUtil.sort(transferList, SortUtil.COMPARE_CIGMA_PO);
                                // Loop
                                for(List<TransferPoDataFromCigmaDomain> transferDomain 
                                    : transferList)
                                {
                                    try {
                                        // 4-1)     Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferErrorDOFromCIGMA() to save data in SPS system                      
                                        strResult = transferPoDataFromCigmaFacadeService
                                            .transactTransferErrorPoFromCigma(
                                                locale, 
                                                transferDomain, 
                                                resultMap.getCoverPageMap().get(
                                                    transferDomain.get(
                                                        Constants.ZERO).getCigmaPoNo()), 
                                                server, jobId);
                                        if( !Constants.EMPTY_STRING.equals(strResult) ){
                                            LOG.error( strResult );
                                        }
                                    } catch(WebServiceCallerRestException e){ 
                                        LOG.error(e.getMessage(), e);
                                        break;
                                    } catch(ApplicationException e){ 
                                        LOG.error(e.getMessage());
                                    } catch (Exception e) {
                                        LOG.error(e.getMessage(), e);
                                     // Log OutPut Log
                                    }
                                } // end for each
                            } // end check null.
                        }
                        
                        // 5    Call Façade Service TransferDODataFromCIGMAFacadeService searchPurchaseOrder() to get D/O which unread
                        transferList.clear();

                    /* Start : [IN056-2]
                     * - Separate loop flag Error and Flag Normal
                     * - Not get Vendor code from Oracle, get from CIGMA
                     * */ 
                    }
                    vendorList = this.transferPoDataFromCigmaFacadeService.searchCigmaVendorCd(
                            server, SPS_FLAG_NORMAL);

                    for (PseudoCigmaPoCoverPageInformationDomain vendorDomain : vendorList) {
                        final List<List<TransferPoDataFromCigmaDomain>> transferList = 
                            new ArrayList<List<TransferPoDataFromCigmaDomain>>();
                    // End : [IN056-2]
                        
                        // [IN056] Add Vendor Code to criteria
                        //resultMap = 
                        //    transferPoDataFromCigmaFacadeService.searchPurchaseOrder(locale, 
                        //        server, SPS_FLAG_NORMAL);
                        /* [IN056-2]
                         * - Separate loop flag Error and Flag Normal
                         * - Not get Vendor code from Oracle, get from CIGMA
                         * */ 
                        //resultMap = transferPoDataFromCigmaFacadeService.searchPurchaseOrder(locale
                        //    , server, SPS_FLAG_NORMAL, vendorDomain.getVendorCd());
                        TransferPoDataFromCigmaResultDomain resultMap
                            = transferPoDataFromCigmaFacadeService.searchPurchaseOrder(
                                locale, server, SPS_FLAG_NORMAL, vendorDomain.getPseudoVendorCd());
                        
                        if(null != resultMap && Constants.ZERO == resultMap.getPoDataMap().size()) {
                            MessageUtil.getErrorMessageForBatch(
                                locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                                null, 
                                LOG, 
                                Constants.TWO, 
                                false);
                        } else {
                            List<TransferPoDataFromCigmaDomain> tmpList = null;
                            if( null != resultMap && null != resultMap.getPoDataMap() ) {
                                for(String cigmaPoNo : resultMap.getPoDataMap().keySet()) {
                                    try {
                                        tmpList = transferPoDataFromCigmaFacadeService
                                            .transactCheckTransferPoFromCigma(locale, 
                                                resultMap.getPoDataMap().get(cigmaPoNo), 
                                                server, 
                                                false, 
                                                jobId,
                                                LOG);
                                        if( Constants.ZERO != tmpList.size() ){
                                            transferList.add( tmpList );
                                        }
                                    } catch (Exception e) {
                                        LOG.error(e.getMessage(), e);
                                    }
                                }
                                // 6    Separate unread CIGMA D/O Data to SPS system
            //                    SortUtil.sort(transferList, SortUtil.COMPARE_CIGMA_PO);
                                // Loop
                                for(List<TransferPoDataFromCigmaDomain> transferDomain
                                    : transferList)
                                {
                                    try {
                                        // 4-1)     Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferErrorDOFromCIGMA() to save data in SPS system                      
                                        strResult = transferPoDataFromCigmaFacadeService
                                            .transactTransferPoFromCigma(
                                                locale, 
                                                transferDomain, 
                                                resultMap.getCoverPageMap().get(
                                                    transferDomain.get(
                                                        Constants.ZERO).getCigmaPoNo()), 
                                                server, jobId);
                                        if( null != strResult 
                                            && !Constants.EMPTY_STRING.equals(strResult) ){
                                            LOG.error( strResult );
                                        }
                                    } catch(WebServiceCallerRestException e){ 
                                        LOG.error(e.getMessage(), e);
                                        break;
                                    } catch(ApplicationException e){ 
                                        LOG.error(e.getMessage());
                                    } catch (Exception e) {
                                     // Log OutPut Log
                                        LOG.error(e.getMessage(), e);
                                    }
                                } // end for each.
                            } // end check null.
                        }
                        
                    // Continue [IN056]
                    }
                    // End : [IN056] Transfer P/O for CIGMA by DENSO Company and CIGMA Vendor Code
                    
                    // 7    Call Façade Service TransferDODataFromCIGMAFacadeService searchChangePurchaseOrder() to get Change D/O which are marked Error from previous batch running.
                    final List<List<TransferChangePoDataFromCigmaDomain>> transferChangeList = 
                        new ArrayList<List<TransferChangePoDataFromCigmaDomain>>();
                    Map<String, List<TransferChangePoDataFromCigmaDomain>> resultChangeMap = 
                        transferPoDataFromCigmaFacadeService.searchChangePurchaseOrder(locale, 
                            server, SPS_FLAG_ERROR);
                    
                    if( null != resultChangeMap && Constants.ZERO == resultChangeMap.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
                    } else {
                        // 8    Keep Change CIGMA D/O Data which error from previous batch running to SPS system
                        if( null != resultChangeMap ) {
                            List<TransferChangePoDataFromCigmaDomain> tmpList = null;
                            for( String cigmaPoNo : resultChangeMap.keySet() ) {
                                List<TransferChangePoDataFromCigmaDomain> transferDomain 
                                    = resultChangeMap.get(cigmaPoNo);
                                try {
                                    tmpList = transferPoDataFromCigmaFacadeService
                                        .transactCheckTransferChangePoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            true, 
                                            jobId,
                                            LOG);
                                    if( null != tmpList && Constants.ZERO < tmpList.size() ) {
                                        transferChangeList.add(tmpList);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage(), e);
                                }
                            }
                            // Loop
                            for(List<TransferChangePoDataFromCigmaDomain> transferDomain 
                                : transferChangeList){
                                try {
                                    // 8-1)     Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferErrorChgPOFromCIGMA() to save data in SPS system
                                    transferPoDataFromCigmaFacadeService
                                        .transactTransferErrorChgPoFromCigma(locale, 
                                            transferDomain, 
                                            server, 
                                            jobId);
                                } catch(WebServiceCallerRestException e){ 
                                    LOG.error(e.getMessage(), e);
                                    break;
                                } catch(ApplicationException e){ 
                                    LOG.error(e.getMessage());
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage(), e);
                                }
                            } // end loop
                        } // end check null
                    }
                    
                    // 9    Call Façade Service TransferDODataFromCIGMAFacadeService searchChangePurchaseOrder() to get Change D/O which unread
                    transferChangeList.clear();
                    resultChangeMap = 
                        transferPoDataFromCigmaFacadeService.searchChangePurchaseOrder(locale, 
                            server, SPS_FLAG_NORMAL);
                    if( null != resultChangeMap && Constants.ZERO == resultChangeMap.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
                    } else {
                        if( null != resultChangeMap ) {
                            List<TransferChangePoDataFromCigmaDomain> tmpList = null;
                            for( String cigmaPoNo : resultChangeMap.keySet() ) {
                                List<TransferChangePoDataFromCigmaDomain> transferDomain 
                                    = resultChangeMap.get(cigmaPoNo);
                                try {
                                    tmpList = transferPoDataFromCigmaFacadeService
                                        .transactCheckTransferChangePoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            false, 
                                            jobId,
                                            LOG);
                                    if( null != tmpList && Constants.ZERO < tmpList.size() ){
                                        transferChangeList.add(tmpList);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage(), e);
                                }
                            }
                            
                            // 10   Keep unread CIGMA Chang D/O Data to SPS system
        //                    SortUtil.sort(transferChangeList, SortUtil.COMPARE_CIGMA_CHANGE_DO);
                            
                            // Loop
                            for(List<TransferChangePoDataFromCigmaDomain> transferDomain 
                                : transferChangeList){
                                try {
                                    // 10-1)        Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferChgDOFromCIGMA() to save data in SPS system
                                    transferPoDataFromCigmaFacadeService
                                        .transactTransferChgPoFromCigma(locale, 
                                            transferDomain, 
                                            server, 
                                            jobId);
                                } catch(WebServiceCallerRestException e){ 
                                    LOG.error(e.getMessage(), e);
                                    break;
                                } catch(ApplicationException e){ 
                                    LOG.error(e.getMessage());
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage(), e);
                                }
                            } // end loop
                        } // end check null.
                    }
                }catch(Exception ex){
                    LOG.error(ex.getMessage(), ex);
                }
            } // end for each server.
            // 11   Call Façade Service TransferDODataFromCIGMAFacadeService searchRecordLimit() without parameter
            MiscellaneousDomain abnormalLimit = 
                transferPoDataFromCigmaFacadeService.searchRecordLimit();
            if( null == abnormalLimit ){
                MessageUtil.getErrorMessageForBatch(
                    locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                    null, 
                    LOG, 
                    Constants.TWO, 
                    false);
            }else {
                // 12.  Get abnormal data for D/O  and send notification to supplier user
                // 12-1)        Call Façade Service TransferDODataFromCIGMAFacadeService searchCigmaPoError() without condition
                SpsCigmaPoErrorDomain criteria = new SpsCigmaPoErrorDomain();
                criteria.setMailFlg(STR_ONE);
                criteria.setDCd(company.getDCd());
                criteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_PO_FOR_SEND_TO_SUPPLIER);
                List<TransferPoErrorEmailDomain> transferPoErrorList = 
                    transferPoDataFromCigmaFacadeService.searchCigmaPoError(criteria);
                
                SpsCigmaPoErrorDomain criteriaDenso = new SpsCigmaPoErrorDomain();
                criteriaDenso.setMailFlg(STR_ONE);
                criteriaDenso.setDCd(company.getDCd());
                criteriaDenso.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_PO_FOR_SEND_TO_DENSO);
                List<TransferPoErrorEmailDomain> transferPoErrorListToDenso = 
                    transferPoDataFromCigmaFacadeService.searchCigmaPoError(criteriaDenso);
                
//                int countDoError = cigmaDoList.size();
                // 12-2)        Call Façade Service TransferDODataFromCIGMAFacadeService searchCigmaChgPoError() without condition
                SpsCigmaChgPoErrorDomain chgCriteria = new SpsCigmaChgPoErrorDomain();
                chgCriteria.setMailFlg(STR_ONE);
                chgCriteria.setDCd(company.getDCd());
                criteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_PO_FOR_SEND_TO_SUPPLIER);
                List<TransferChgPoErrorEmailDomain> transferChgPoErrorList = 
                    transferPoDataFromCigmaFacadeService.searchCigmaChgPoError(chgCriteria);
                
                SpsCigmaChgPoErrorDomain chgCriteriaDenso = new SpsCigmaChgPoErrorDomain();
                chgCriteriaDenso.setMailFlg(STR_ONE);
                chgCriteriaDenso.setDCd(company.getDCd());
                chgCriteriaDenso.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_PO_FOR_SEND_TO_DENSO);
                List<TransferChgPoErrorEmailDomain> transferChgPoErrorListToDenso = 
                    transferPoDataFromCigmaFacadeService.searchCigmaChgPoError(chgCriteriaDenso);
                
                if( Constants.ZERO == transferPoErrorList.size() 
                    && Constants.ZERO == transferChgPoErrorList.size() 
                    && Constants.ZERO == transferPoErrorListToDenso.size()
                    && Constants.ZERO == transferChgPoErrorListToDenso.size()){
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0014, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }else {
                    // 12-3)        If no any data from both of D/O Error and CHG D/O Error, then exit program
                    // 12-4)        Compare recordCount D/O Error with recordLimit to separate commit transaction by recordLimit
                    try{
                        // Already sort in Query, don't have to use SortUtil
                        //SortUtil.sort(transferPoErrorList
                        //    , SortUtil.COMPARE_TRANSFER_PO_ERROR_EMAIL);
                        //SortUtil.sort(transferChgPoErrorList
                        //    , SortUtil.COMPARE_TRANSFER_CHG_PO_ERROR_EMAIL);
                        transferPoDataFromCigmaFacadeService
                            .transactSendNotificationAbnormalDataPo(locale, 
                                transferPoErrorList,
                                transferPoErrorListToDenso,
                                transferChgPoErrorList,
                                transferChgPoErrorListToDenso,
                                Integer.parseInt(abnormalLimit.getMiscValue()),
                                LOG);
                    }catch(Exception ex){
                        LOG.error(ex.getMessage());
                    }
                }
            }
            result = true;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
            String message = MessageUtil.getApplicationMessageWithLabel(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0015,
                SupplierPortalConstant.TRANSFER_PO_STEP_PROCESS_NAME);
            LOG.info(message);
        }
        
        return result;
    }
    
    /**
     * Gets the content.
     * @return default locale
     */
    private Locale getDefaultLocale(){
        Properties propApp 
            = Props.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
        String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
        String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
        String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
        ContextParams.setDefaultLocale(strDefaultLocale);
        ContextParams.setBaseDirGui(strBaseDirGui);
        ContextParams.setBaseDirMsg(strBaseDirMsg);
        return LocaleUtil.getLocaleFromString(strDefaultLocale);
    }
    
//    public static void main(String[] args) {
//        TransferPoDataFromCigmaResidentJob childJob = new TransferPoDataFromCigmaResidentJob();
//        JobManageDomain jobManageDomain = new JobManageDomain();
//        jobManageDomain.setExecuteArg( "DNTH" );
//        jobManageDomain.setJobId("BORD00040000");
//        QueueInfDomain queueInfDomain = new QueueInfDomain();
//        childJob.jobCall(jobManageDomain, queueInfDomain);
//    }
}
