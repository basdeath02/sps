/*
 * ModifyDate Developmentcompany    Describe 
 * 2014/08/27 CSI Karnrawee         Create
 * 2018/05/18 CTC U. Rungsiwut      Update for purge process.
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ChildJobBase;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * This batch job for Purge Data<br />
 * on database.
 * */
public class PurgeDataProcessChildJob extends ChildJobBase {
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(PurgeDataProcessChildJob.class);
    
    /** The purge data process facade service. */
    private PurgeDataProcessFacadeService purgeDataProcessFacadeService = null;
    
    /**
     * Default constructor.
     * */
    public PurgeDataProcessChildJob() {
        
    }

    /**
     * <p>Setter method for purge data process facade service.</p>
     *
     * @param purgeDataProcessFacadeService Set for purge data process facade
     */
    public void setPurgeDataProcessFacadeService(
        PurgeDataProcessFacadeService purgeDataProcessFacadeService) {
        this.purgeDataProcessFacadeService = purgeDataProcessFacadeService;
    }
    
    /**
     * Auto-generated method stub : do nothing.
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void preBizProc(JobInputDomain arg0) throws ApplicationException {
        
        LoadContextPropertyForBatch.loadContextListener(LOG);
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        MessageUtil.getErrorMessageForBatchWithLabel(objLocale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0014,
            SupplierPortalConstant.LBL_PURGE_DATA_PROCESS, null, LOG, Constants.ZERO, false);
    }

    /**
     * 
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void mainBizProc(JobInputDomain arg0) throws ApplicationException{
        Locale locale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        
        List<String> param = null;
        List<String> companyCodeList = null;
        String densoCompanyCode = null;
        String parentJobId = arg0.getJobId();
        
        if(StringUtil.checkNullOrEmpty(arg0.getExecuteArg())){
            StringBuffer message = new StringBuffer();
            message.append(MessageUtil.getLabelHandledException(
                locale, SupplierPortalConstant.LBL_COMPANY_CODE_LIST));
            message.append(Constants.SYMBOL_SPACE).append(Constants.WORD_AND);
            message.append(Constants.SYMBOL_SPACE);
            message.append(MessageUtil.getLabelHandledException(
                locale, SupplierPortalConstant.LBL_PARRENT_JOB_ID));
            
            MessageUtil.getErrorMessageForBatch(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                new String[] {message.toString()},
                LOG, Constants.TWO, false);
        }else{
            param = Arrays.asList(StringUtils.splitPreserveAllTokens(
                arg0.getExecuteArg(), Constants.SYMBOL_PIPE));
            if(param.size() <= Constants.TWO){
                densoCompanyCode = param.get(Constants.ZERO);
            }
            if(StringUtil.checkNullOrEmpty(densoCompanyCode)){
                MessageUtil.getErrorMessageForBatchWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_COMPANY_CODE_LIST, null, LOG,
                    Constants.TWO, false);
            }
            if(StringUtil.checkNullOrEmpty(parentJobId)){
                MessageUtil.getErrorMessageForBatchWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_PARRENT_JOB_ID, null, LOG,
                    Constants.TWO, false);
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoCompanyCode)
            && !StringUtil.checkNullOrEmpty(parentJobId))
        {
            List<SpsMCompanyDensoDomain> spsMCompanyDensoList = null;
            companyCodeList = Arrays.asList(StringUtils.splitPreserveAllTokens(
                param.get(Constants.ZERO), Constants.SYMBOL_COMMA));
            densoCompanyCode = StringUtil.convertListToCharCommaSeperate(
                    companyCodeList, SupplierPortalConstant.LENGTH_DENSO_CODE);
            try{
                spsMCompanyDensoList
                    = purgeDataProcessFacadeService.searchDensoCompanyByCodeList(densoCompanyCode);
            }catch(Exception e){
                MessageUtil.getErrorMessageForBatch(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0004, new String[] {
                        e.getClass().toString(), e.getMessage()}, LOG, Constants.TWO, true);
            }
            
            if(null == spsMCompanyDensoList || Constants.ZERO == spsMCompanyDensoList.size()){
                MessageUtil.getErrorMessageForBatchWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_COMPANY, null, LOG, Constants.TWO, true);
            }
            if(spsMCompanyDensoList.size() != companyCodeList.size()){
                MessageUtil.getErrorMessageForBatchWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_SOME_DENSO_COMPANY,
                    null, LOG, Constants.ONE, false);
            }
            
            for(SpsMCompanyDensoDomain densoItem : spsMCompanyDensoList){
                LOG.info(StringUtil.appendsString(
                    MessageUtil.getLabelHandledException(locale,
                        SupplierPortalConstant.LBL_START_DENSO_COMPANY_CODE),
                        Constants.SYMBOL_SPACE, densoItem.getDCd()));
                
                densoItem.setLocale(locale);
//                Remove the condition which relate P/O number
//                List<PurgingPoDomain> purgingPoList 
//                    = this.purgeDataProcessFacadeService.searchAllPurgingOrderByCompany(densoItem);
//                if(null != purgingPoList && !purgingPoList.isEmpty()){
//                    for(PurgingPoDomain poItem : purgingPoList){
//                        
//                        // [IN012] If P/O not complete, not purge
//                        if (!poItem.getIsComplete()) {
//                            continue;
//                        }
//                        
//                        LOG.info(StringUtil.appendsString(
//                            MessageUtil.getLabelHandledException(locale,
//                                SupplierPortalConstant.LBL_START_PURGING_ORDER_BY_PO_NUMBER),
//                            poItem.getPurchaseOrderId()));
//                        try{
//                            poItem.setLocale(locale);
//                            this.purgeDataProcessFacadeService.transactPurgeSystemData(
//                                poItem, parentJobId);
//                        }catch(ApplicationException ae){
//                            LOG.info(ae.getMessage());
//                        }catch (Exception e) {
//                            MessageUtil.getErrorMessageForBatch(locale, 
//                                SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
//                                    MessageUtil.getLabelHandledException(locale,
//                                        SupplierPortalConstant.LBL_ERROR),
//                                    MessageUtil.getLabelHandledException(locale,
//                                        SupplierPortalConstant.LBL_INNER_TEXT_FROM_EXCEPTION)},
//                                LOG, Constants.TWO, false);
//                        }
//                    }
//                }else{
//                    MessageUtil.getErrorMessageForBatchWithLabel(locale,
//                        SupplierPortalConstant.ERROR_CD_SP_E6_0043,
//                        SupplierPortalConstant.LBL_ORDER, null, LOG, Constants.ONE, false);
//                }
//                
//                List<SpsTInvoiceDomain> invoiceList = this.purgeDataProcessFacadeService
//                        .searchAllPurgingInvoiceByCompany(densoItem);
//                
//                if(null != invoiceList && !invoiceList.isEmpty()){
//                    for(SpsTInvoiceDomain invoiceItem : invoiceList){
//                        try{
//                            this.purgeDataProcessFacadeService
//                                .transactPurgeInvoiceData(invoiceItem, densoItem, parentJobId);
//                        }catch(ApplicationException ae){
//                            LOG.info(ae.getMessage());
//                        }catch (Exception e) {
//                            MessageUtil.getErrorMessageForBatch(locale,
//                                SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
//                                    MessageUtil.getLabelHandledException(locale, 
//                                        SupplierPortalConstant.LBL_ERROR),
//                                    MessageUtil.getLabelHandledException(locale, 
//                                        SupplierPortalConstant.LBL_INNER_TEXT_FROM_EXCEPTION)},
//                                LOG, Constants.TWO, false);
//                        }
//                    }
//                    try{
//                        this.purgeDataProcessFacadeService.transactPurgeInvoiceCoverPageRunNoData(
//                            densoItem);
//                    }catch(ApplicationException ae){
//                        LOG.info(ae.getMessage());
//                    }catch (Exception e) {
//                        MessageUtil.getErrorMessageForBatch(locale,
//                            SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
//                                MessageUtil.getLabelHandledException(locale, 
//                                    SupplierPortalConstant.LBL_ERROR),
//                                MessageUtil.getLabelHandledException(locale, 
//                                    SupplierPortalConstant.LBL_INNER_TEXT_FROM_EXCEPTION)},
//                            LOG, Constants.TWO, false);
//                    }
//                }else{
//                    MessageUtil.getErrorMessageForBatchWithLabel(locale,
//                        SupplierPortalConstant.ERROR_CD_SP_E6_0043,
//                        SupplierPortalConstant.LBL_INVOICE, null, LOG, Constants.ONE, false);
//                }
                try {
                    this.purgeDataProcessFacadeService.transactPurgeData(densoItem, parentJobId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void postBizProc(JobInputDomain arg0) throws ApplicationException {
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        MessageUtil.getErrorMessageForBatchWithLabel(objLocale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0015, 
            SupplierPortalConstant.LBL_PURGE_DATA_PROCESS, null, LOG, Constants.ZERO, false);
    }
}
