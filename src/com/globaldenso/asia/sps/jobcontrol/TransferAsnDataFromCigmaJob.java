/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 * 2016/03/14 CSI Akat            [IN056-2]
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobManageDomain;
import com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.service.TransferAsnDataFromCigmaService;
import com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService;
import com.globaldenso.asia.sps.business.service.TransferPoDataToCigmaService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * This job has a purpose to read CSV file that upload by each DENSO<br />
 * company to the system. After user login to SPS system success<br />
 * the system will load a message to show at the announcement message<br />
 * section at "Main Screen" page.
 * */
public class TransferAsnDataFromCigmaJob extends ResidentBizProcExecute {
	/** The Constant LOG. */
	private static final Log LOG = LogFactory
			.getLog(TransferAsnDataFromCigmaJob.class);
	/** Cigma SPS Flag Error */
	private static final String SPS_FLAG_ERROR = Constants.STR_TWO;
	/** Cigma SPS Flag Normal */
	private static final String SPS_FLAG_NORMAL = Constants.STR_ZERO;

	/** The import message facade service. */
	private TransferAsnDataFromCigmaService transferAsnDataFromCigmaService;

	/**
	 * Default constructor.
	 * */
	public TransferAsnDataFromCigmaJob() {
		super();
	}

	/**
	 * <p>
	 * Setter method for transferPoDataFromCigmaFacadeService.
	 * </p>
	 * 
	 * @param transferPoDataFromCigmaFacadeService
	 *            Set for transferPoDataFromCigmaFacadeService
	 */
	
	public TransferAsnDataFromCigmaService getTransferAsnDataFromCigmaService() {
		return transferAsnDataFromCigmaService;
	}

	public void setTransferAsnDataFromCigmaService(
			TransferAsnDataFromCigmaService transferAsnDataFromCigmaService) {
		this.transferAsnDataFromCigmaService = transferAsnDataFromCigmaService;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute#jobCall(com.globaldenso.ai.aijb.core.business.domain.JobManageDomain,
	 *      com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain)
	 */

	@Override
	protected boolean jobCall(JobManageDomain jobManageDomain,
			QueueInfDomain queueInfDomain) {
		LoadContextPropertyForBatch.loadContextListener(LOG);
		// super.jobCall(jobManageDomain, queueInfDomain);
		String jobId = null;
		if (null != jobManageDomain.getJobId()
				&& Constants.TWELVE < jobManageDomain.getJobId().length()) {
			jobId = jobManageDomain.getJobId().substring(Constants.ZERO,
					Constants.TWELVE);
		} else {
			jobId = jobManageDomain.getJobId();
		}
		boolean result = false;
		Locale locale = this.getDefaultLocale();
		// 1 Out Put Log
		LOG.info(MessageUtil.getApplicationMessageWithLabel(locale,
				SupplierPortalConstant.ERROR_CD_SP_I6_0014,
				SupplierPortalConstant.TRANSFER_ASN_STEP_PROCESS_NAME));
		List<String> arguList = new ArrayList<String>();
		try {
			arguList.addAll(Arrays.asList(StringUtils.splitPreserveAllTokens(
					jobManageDomain.getExecuteArg(), Constants.SYMBOL_COMMA)));
			
			String strResult = null;

				try {

					SpsMDensoDensoRelationWithASNDNDomain asnCriteria = new SpsMDensoDensoRelationWithASNDNDomain();
					asnCriteria.setdCd(StringUtil.convertListToVarcharCommaSeperate(arguList));
					//poCriteria.setDatabaseName(server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
					//List<SpsMDensoDensoRelationWithASNDNDomain> listData = transferAsnDataFromCigmaService.searchData(asnCriteria);
					
					List<SpsMDensoDensoRelationWithASNDNDomain> listData = transferAsnDataFromCigmaService.searchDataGroup(asnCriteria);
					
					As400ServerConnectionInformationDomain server = new As400ServerConnectionInformationDomain();
					// Tranfer all ASN data
					if (null != listData) {
						if(listData.size() >0){   
						    this.transferAsnDataFromCigmaService.transferData(server, SPS_FLAG_ERROR,listData);						    
						}else{
						     MessageUtil.getErrorMessageForBatch(
					                    locale, 
					                    SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
					                    null, 
					                    LOG, 
					                    Constants.ZERO, 
					                    false);
						}
					}else{
					     MessageUtil.getErrorMessageForBatch(
				                    locale, 
				                    SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
				                    null, 
				                    LOG, 
				                    Constants.ZERO, 
				                    false);
					}
					
		
		
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
				}
			result = true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			Locale objLocale = LocaleUtility
					.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
			String message = MessageUtil.getApplicationMessageWithLabel(
					objLocale, SupplierPortalConstant.ERROR_CD_SP_I6_0015,
					SupplierPortalConstant.TRANSFER_ASN_STEP_PROCESS_NAME);
			LOG.info(message);
		}

		return result;
	}


	/**
	 * Gets the content.
	 * 
	 * @return default locale
	 */
	private Locale getDefaultLocale() {
		Properties propApp = Props
				.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
		String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
		String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
		String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
		ContextParams.setDefaultLocale(strDefaultLocale);
		ContextParams.setBaseDirGui(strBaseDirGui);
		ContextParams.setBaseDirMsg(strBaseDirMsg);
		return LocaleUtil.getLocaleFromString(strDefaultLocale);
	}

	// public static void main(String[] args) {
	// TransferPoDataFromCigmaResidentJob childJob = new
	// TransferPoDataFromCigmaResidentJob();
	// JobManageDomain jobManageDomain = new JobManageDomain();
	// jobManageDomain.setExecuteArg( "DNTH" );
	// jobManageDomain.setJobId("BORD00040000");
	// QueueInfDomain queueInfDomain = new QueueInfDomain();
	// childJob.jobCall(jobManageDomain, queueInfDomain);
	// }
}
