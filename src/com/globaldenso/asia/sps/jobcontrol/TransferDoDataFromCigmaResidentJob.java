/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_CR_LF;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0063;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobManageDomain;
import com.globaldenso.ai.aijb.core.business.domain.QueueInfDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ResidentBizProcExecute;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferOneWayKanbanDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;
import com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * This job has a purpose to read CSV file that upload by each DENSO<br />
 * company to the system. After user login to SPS system success<br />
 * the system will load a message to show at the announcement message<br />
 * section at "Main Screen" page.
 * */
public class TransferDoDataFromCigmaResidentJob extends ResidentBizProcExecute {
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(TransferDoDataFromCigmaResidentJob.class);
    /** Cigma SPS Flag Error */
    private static final String SPS_FLAG_ERROR = Constants.STR_TWO;
    /** Cigma SPS Flag Normal */
    private static final String SPS_FLAG_NORMAL = Constants.STR_ZERO;
    
    private static final String SPS_FLAG_BACK_ORDER_INIT = Constants.STR_THREE;
    
    private static final String SPS_FLAG_BACK_ORDER_COMPLETE = Constants.STR_FOUR;
    
    private static final String SPS_FLAG_BACK_ORDER_ERROR = Constants.STR_FIVE;
    
    /** The import message facade service. */
    private TransferDoDataFromCigmaFacadeService transferDoDataFromCigmaFacadeService;
    
//    /** The common service. */
//    private CommonService commonService;
    
    /**
     * Default constructor.
     * */
    public TransferDoDataFromCigmaResidentJob() {
        super();
    }

//    /**
//     * <p>Setter method for commonService.</p>
//     *
//     * @param commonService Set for commonService
//     */
//    public void setCommonService(CommonService commonService) {
//        this.commonService = commonService;
//    }

    /**
     * <p>Setter method for transferDoDataFromCigmaFacadeService.</p>
     *
     * @param transferDoDataFromCigmaFacadeService Set for transferDoDataFromCigmaFacadeService
     */
    public void setTransferDoDataFromCigmaFacadeService(
        TransferDoDataFromCigmaFacadeService transferDoDataFromCigmaFacadeService) {
        this.transferDoDataFromCigmaFacadeService = transferDoDataFromCigmaFacadeService;
    }

    @Override
    protected boolean jobCall(JobManageDomain jobManageDomain,
        QueueInfDomain queueInfDomain) {
        LoadContextPropertyForBatch.loadContextListener(LOG);
        //super.jobCall(jobManageDomain, queueInfDomain);
        String jobId = null;
        if( null != jobManageDomain.getJobId() 
            && Constants.TWELVE < jobManageDomain.getJobId().length()  ) {
            jobId = jobManageDomain.getJobId().substring(Constants.ZERO, Constants.TWELVE);
        }else{
            jobId = jobManageDomain.getJobId();
        }
        boolean result = false;
        Locale locale = this.getDefaultLocale();
        //1. Out Put Log
        List<String> arguList = new ArrayList<String>();
        LOG.info( MessageUtil.getApplicationMessageWithLabel(locale, 
            SupplierPortalConstant.ERROR_CD_SP_I6_0014, 
                SupplierPortalConstant.TRANSFER_DO_STEP_PROCESS_NAME ) );
        try {
            //2. Read configuration for calling CIGMA REST from Denso company

            arguList.addAll( Arrays.asList( StringUtils
                .splitPreserveAllTokens(
                    jobManageDomain.getExecuteArg(), Constants.SYMBOL_COMMA) ) );
            SpsMCompanyDensoDomain company = new SpsMCompanyDensoDomain();
            company.setDCd( StringUtil.convertListToVarcharCommaSeperate(arguList) ); // ***  'DIAT'  or 'SDM', 'DIAT"
            List<As400ServerConnectionInformationDomain> as400Server = 
                transferDoDataFromCigmaFacadeService.searchAs400ServerList(company);
            
            if( null != as400Server ){
                if(  Constants.ZERO == as400Server.size() ){
                    // Log OutPut Log
                    LOG.error(
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E5_0026
                            , new String[]{company.getDCd()} )
                    );
                    return false;
                }else if( as400Server.size() < arguList.size() ){
                    LOG.error(
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E5_0025
                            , new String[]{company.getDCd()} )
                    );
                }
            }else{
                LOG.error(
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E5_0026
                        , new String[]{company.getDCd()} )
                );
                return false;
            }
            // LOOP all data in <List>as400Server
//            if( null == as400Server ) {
//                return;
//            }
//            as400Server = new ArrayList<As400ServerConnectionInformationDomain>();
            for(As400ServerConnectionInformationDomain server : as400Server) {
                try{
                    // 3. Call Façade Service TransferDODataFromCIGMAFacadeService searchDeliveryOrder() to get D/O which are marked Error from previous batch running.
                    // List of TransferDODataFromCIGMA can find Supplier.
                    final List<List<TransferDoDataFromCigmaDomain>> transferList = 
                        new ArrayList<List<TransferDoDataFromCigmaDomain>>();
                    Map<String, List<TransferDoDataFromCigmaDomain>> resultMap = 
                        transferDoDataFromCigmaFacadeService.searchDeliveryOrder(locale, 
                            server, SPS_FLAG_ERROR);
                    if( null != resultMap && Constants.ZERO == resultMap.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
    //                    continue;
                    } else {
                        if( null != resultMap ) {
                            List<TransferDoDataFromCigmaDomain> tmpList = null;
                            for(String cigmaDoNo : resultMap.keySet()){
                                List<TransferDoDataFromCigmaDomain> transferDomain
                                    = resultMap.get(cigmaDoNo);
                                try {
                                    tmpList = transferDoDataFromCigmaFacadeService
                                        .transactCheckTransferDoFromCigma(
                                            locale,
                                            transferDomain,
                                            server,
                                            true,
                                            jobId,
                                            LOG);
                                    if( null != tmpList && Constants.ZERO < tmpList.size() ){
                                        transferList.add( tmpList );
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            
                            // 4. Separate CIGMA D/O Data which error from previous batch running to SPS system
    //                        SortUtil.sort(transferList, SortUtil.COMPARE_CIGMA_DO);
                            // Loop
                            for(List<TransferDoDataFromCigmaDomain> transferDomain : transferList){
                                try {
                                    // 4-1)     Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferErrorDOFromCIGMA() to save data in SPS system                      
                                    transferDoDataFromCigmaFacadeService
                                        .transactTransferErrorDoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            jobId);
                                } catch(WebServiceCallerRestException e){ 
                                    LOG.error(e.getMessage(), e);
                                    break;
                                } catch(ApplicationException e){ 
                                    LOG.error(e.getMessage());
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(
    //                                    MessageUtil.getApplicationMessageHandledException(locale, 
    //                                        SupplierPortalConstant.ERROR_CD_SP_80_0004
    //                                        , new String[]{
    //                                            e.getClass().getCanonicalName(), 
    //                                            e.getMessage()
    //                                        } )
                                        e.getMessage(), e
                                    );
                                }
                            } // end for each.
                        } // end check null.
                    }                
                    // 5  Call Facade Service TransferDODataFromCIGMAFacadeService searchDeliveryOrder() to get D/O which unread
                    transferList.clear();
                    resultMap = 
                        transferDoDataFromCigmaFacadeService.searchDeliveryOrder(locale, 
                            server, SPS_FLAG_NORMAL);
                    if( null != resultMap && Constants.ZERO == resultMap.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
    //                    continue;
                    } else {
                        if( null != resultMap ) {
                            List<TransferDoDataFromCigmaDomain> tmpList = null;
                            for(String cigmaDoNo : resultMap.keySet()){
                                List<TransferDoDataFromCigmaDomain> transferDomain
                                    = resultMap.get(cigmaDoNo);
                                try {
                                    tmpList = transferDoDataFromCigmaFacadeService
                                        .transactCheckTransferDoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            false, 
                                            jobId,
                                            LOG);
                                    if( null != tmpList && Constants.ZERO < tmpList.size() ){
                                        transferList.add( tmpList );
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            // 6    Separate unread CIGMA D/O Dat4a to SPS system
    //                        SortUtil.sort(transferList, SortUtil.COMPARE_CIGMA_DO);
                            // Loop
                            for(List<TransferDoDataFromCigmaDomain> transferDomain : transferList){
                                try {
                                    // 4-1)     Call Façade Service TransferDODataFromCIGMAFacadeService 
                                    //              transactTransferErrorDOFromCIGMA() to save data 
                                    //              in SPS system                      
                                    transferDoDataFromCigmaFacadeService
                                        .transactTransferDoFromCigma(locale, 
                                            transferDomain, 
                                            server, 
                                            jobId);
                                } catch(WebServiceCallerRestException e){ 
                                    LOG.error(e.getMessage(), e);
                                    break;
                                } catch(ApplicationException e){ 
                                    LOG.error(e.getMessage());
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(
    //                                    MessageUtil.getApplicationMessageHandledException(locale, 
    //                                        SupplierPortalConstant.ERROR_CD_SP_80_0004
    //                                        , new String[]{
    //                                            e.getClass().getCanonicalName(), 
    //                                            e.getMessage()
    //                                        } )
                                        e.getMessage(), e
                                    );
                                }
                            } // end for each.
                        } // end check null.
                    }
                    
                    //*********back order (bo)*********
                    //3 = init bo
                    //4 = complete bo
                    //5 = error bo
                    
                    transferList.clear();
                    resultMap = transferDoDataFromCigmaFacadeService.searchDeliveryOrder(locale, server, SPS_FLAG_BACK_ORDER_ERROR);
                    if(null != resultMap && Constants.ZERO == resultMap.size()){
                        //error message
                        LOG.info("Do data back order status = '5' ");
                    }
                    else{
                        if(null != resultMap){
                            for(String cigmaDoNo : resultMap.keySet()){
                                List<TransferDoDataFromCigmaDomain> transferDomain = resultMap.get(cigmaDoNo);
                                List<TransferDoDataFromCigmaDomain> tmp = null;
                                try {
                                    //validate vendor
                                    //validate part no
                                    //validate do exist and shipping status != CPS
                                    tmp = transferDoDataFromCigmaFacadeService.transactCheckTransferDoBackOrderFromCigma(locale, transferDomain, server, jobId, LOG);
                                    if(tmp != null && tmp.size() > Constants.ZERO){
                                        transferList.add(tmp);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            for(List<TransferDoDataFromCigmaDomain> transferDomain : transferList){
                                try {
                                    //update
                                    transferDoDataFromCigmaFacadeService.transactTransferDoBackOrderFromCigma(locale, transferDomain, server, jobId);
                                    
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                        }
                    }
                    
                    transferList.clear();
                    resultMap = transferDoDataFromCigmaFacadeService.searchDeliveryOrder(locale, server, SPS_FLAG_BACK_ORDER_INIT);
                    if(null != resultMap && Constants.ZERO == resultMap.size()){
                        //error message
                    }
                    else{
                        if(null != resultMap){
                            for(String cigmaDoNo : resultMap.keySet()){
                                List<TransferDoDataFromCigmaDomain> transferDomain = resultMap.get(cigmaDoNo);
                                List<TransferDoDataFromCigmaDomain> tmp = null;
                                try {
                                    //validate vendor
                                    //validate part no
                                    //validate do exist and shipping status != CPS
                                    tmp = transferDoDataFromCigmaFacadeService.transactCheckTransferDoBackOrderFromCigma(locale, transferDomain, server, jobId, LOG);
                                    if(tmp != null && tmp.size() > Constants.ZERO){
                                        transferList.add(tmp);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            for(List<TransferDoDataFromCigmaDomain> transferDomain : transferList){
                                try {
                                    //update
                                    transferDoDataFromCigmaFacadeService.transactTransferDoBackOrderFromCigma(locale, transferDomain, server, jobId);
                                    
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                        }
                    }                   
                    //*********back order (bo)*********
                    
                    // For check previous revision is error or not
                    Set<String> originalDoErrorSet = new HashSet<String>();
                    
                    // 7    Call Facade Service TransferDODataFromCIGMAFacadeService searchChangeDeliveryOrder() to get Change D/O which are marked Error from previous batch running.
                    final List<List<TransferChangeDoDataFromCigmaDomain>> transferChangeList = 
                        new ArrayList<List<TransferChangeDoDataFromCigmaDomain>>();
                    Map<String, List<TransferChangeDoDataFromCigmaDomain>> resultChangeMap = 
                        transferDoDataFromCigmaFacadeService.searchChangeDeliveryOrder(locale, 
                            server, SPS_FLAG_ERROR);
                    if( null != resultChangeMap && Constants.ZERO == resultChangeMap.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
    //                    continue;
                    } else {
                        if( null != resultChangeMap ) {
                            for(String cigmaDoNo : resultChangeMap.keySet() ) {
                                List<TransferChangeDoDataFromCigmaDomain> transferDomain 
                                    = resultChangeMap.get(cigmaDoNo);
                                List<TransferChangeDoDataFromCigmaDomain> tmpList = null;
                                try {
                                    tmpList = transferDoDataFromCigmaFacadeService
                                        .transactCheckTransferChangeDoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server,
                                            originalDoErrorSet,
                                            true, 
                                            jobId,
                                            LOG);
                                    if( null != tmpList && Constants.ZERO < tmpList.size() ){
                                        transferChangeList.add(tmpList);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            
                            // 8    Keep Change CIGMA D/O Data which error from previous batch running to SPS system
    //                        SortUtil.sort(transferChangeList, SortUtil.COMPARE_CIGMA_CHANGE_DO);
                            
                            // Loop
                            for(List<TransferChangeDoDataFromCigmaDomain> transferDomain 
                                : transferChangeList){
                                try {
                                    // 8-1)     Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferErrorChgDOFromCIGMA() to save data in SPS system
                                    String message = transferDoDataFromCigmaFacadeService
                                        .transactTransferErrorChgDoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            jobId);
                                    if( !Constants.EMPTY_STRING.equals(message) ){
                                        LOG.error( message );
                                    }
                                } catch(WebServiceCallerRestException e){ 
                                    LOG.error(e.getMessage());
                                    break;
                                } catch(ApplicationException e){ 
                                    LOG.error(e.getMessage());
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(
                                        MessageUtil.getApplicationMessageHandledException(locale, 
                                            SupplierPortalConstant.ERROR_CD_SP_80_0004
                                            , new String[]{
                                                e.getClass().getCanonicalName(), 
                                                e.getMessage()
                                            } )
                                    );
                                }
                            } // end loop
                        } // end check null.
                    }
                    
                    // 9    Call Façade Service TransferDODataFromCIGMAFacadeService searchChangeDeliveryOrder() to get Change D/O which unread
                    transferChangeList.clear();
                    resultChangeMap = 
                        transferDoDataFromCigmaFacadeService.searchChangeDeliveryOrder(locale, 
                            server, SPS_FLAG_NORMAL);
                    if( null != resultChangeMap && Constants.ZERO == resultChangeMap.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
    //                    continue;
                    } else {
                        if( null != resultChangeMap ) {
                            for(String cigmaDoNo : resultChangeMap.keySet() ) {
                                List<TransferChangeDoDataFromCigmaDomain> transferDomain 
                                    = resultChangeMap.get(cigmaDoNo);
                                List<TransferChangeDoDataFromCigmaDomain> tmpList = null;
                                try {
                                    tmpList = transferDoDataFromCigmaFacadeService
                                        .transactCheckTransferChangeDoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            originalDoErrorSet,
                                            false, 
                                            jobId,
                                            LOG);
                                    if( null != tmpList && Constants.ZERO < tmpList.size() ){
                                        transferChangeList.add(tmpList);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            
                            // 10   Keep unread CIGMA Chang D/O Data to SPS system
    //                        SortUtil.sort(transferChangeList, SortUtil.COMPARE_CIGMA_CHANGE_DO);
                            
                            // Loop
                            for(List<TransferChangeDoDataFromCigmaDomain> transferDomain 
                                : transferChangeList){
                                try {
                                    // 10-1)        Call Façade Service TransferDODataFromCIGMAFacadeService transactTransferChgDOFromCIGMA() to save data in SPS system
                                    String message = transferDoDataFromCigmaFacadeService
                                        .transactTransferChgDoFromCigma(
                                            locale, 
                                            transferDomain, 
                                            server, 
                                            jobId);
                                    if( !Constants.EMPTY_STRING.equals(message) ){
                                        LOG.error( message );
                                    }
                                } catch(WebServiceCallerRestException e){ 
                                    LOG.error(e.getMessage());
                                    break;
                                } catch(ApplicationException e){ 
                                    LOG.error(e.getMessage());
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error( e.getMessage() );
    //                                LOG.error(
    //                                    MessageUtil.getApplicationMessageHandledException(locale, 
    //                                        SupplierPortalConstant.ERROR_CD_SP_80_0004
    //                                        , new String[]{
    //                                            e.getClass().getCanonicalName(), 
    //                                            e.getMessage()
    //                                        } )
    //                                );
                                }
                            } // end loop
                        } // end check null.
                    }
                    
                    //*********back order (bo)*********
                    //3 = init bo
                    //4 = complete bo
                    //5 = error bo
                    
                    transferChangeList.clear();
                    resultChangeMap = transferDoDataFromCigmaFacadeService.searchChangeDeliveryOrder(locale, server, SPS_FLAG_BACK_ORDER_ERROR);
                    if(null != resultChangeMap && Constants.ZERO == resultChangeMap.size()){
                        //error message
                    }
                    else{
                        if(null != resultChangeMap){
                            for(String cigmaDoNo : resultChangeMap.keySet()){
                                List<TransferChangeDoDataFromCigmaDomain> transferDomain = resultChangeMap.get(cigmaDoNo);
                                List<TransferChangeDoDataFromCigmaDomain> tmp = null;
                                try {
                                    //validate vendor
                                    //validate part no
                                    //validate do exist and shipping status != CPS
                                    tmp = transferDoDataFromCigmaFacadeService.transactCheckTransferChangeDoBackOrderFromCigma(locale, transferDomain, server, jobId, LOG);
                                    if(tmp != null && tmp.size() > Constants.ZERO){
                                        transferChangeList.add(tmp);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            for(List<TransferChangeDoDataFromCigmaDomain> transferDomain : transferChangeList){
                                try {
                                    //update
                                    transferDoDataFromCigmaFacadeService.transactTransferChangeDoBackOrderFromCigma(locale, transferDomain, server, jobId);
                                    
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                        }
                    }
                    
                    transferChangeList.clear();
                    resultChangeMap = transferDoDataFromCigmaFacadeService.searchChangeDeliveryOrder(locale, server, SPS_FLAG_BACK_ORDER_INIT);
                    if(null != resultChangeMap && Constants.ZERO == resultChangeMap.size()){
                        //error message
                    }
                    else{
                        if(null != resultChangeMap){
                            for(String cigmaDoNo : resultChangeMap.keySet()){
                                List<TransferChangeDoDataFromCigmaDomain> transferDomain = resultChangeMap.get(cigmaDoNo);
                                List<TransferChangeDoDataFromCigmaDomain> tmp = null;
                                try {
                                    //validate vendor
                                    //validate part no
                                    //validate do exist and shipping status != CPS
                                    tmp = transferDoDataFromCigmaFacadeService.transactCheckTransferChangeDoBackOrderFromCigma(locale, transferDomain, server, jobId, LOG);
                                    if(tmp != null && tmp.size() > Constants.ZERO){
                                        transferChangeList.add(tmp);
                                    }
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                            for(List<TransferChangeDoDataFromCigmaDomain> transferDomain : transferChangeList){
                                try {
                                    //update
                                    transferDoDataFromCigmaFacadeService.transactTransferChangeDoBackOrderFromCigma(locale, transferDomain, server, jobId);
                                    
                                } catch (Exception e) {
                                 // Log OutPut Log
                                    LOG.error(e.getMessage());
                                }
                            }
                        }
                    }                   
                    //*********back order (bo)*********
                    
                    
                    // SPS Phase II
                    // 11   Call Façade Service TransferDoDataFromCigmaFacadeService searchOnewayKanbanTag() to get Kanban tag which already to transfer to SPS.
                    Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>>
                        resultKanbanMapSeparateByVendorCode = 
                            transferDoDataFromCigmaFacadeService.searchOnewayKanbanTagSeparateByVendorCode(locale, 
                                server, SupplierPortalConstant.SPS_KANBAN_FLAG_TRANSFER);
                    if( null != resultKanbanMapSeparateByVendorCode
                            && Constants.ZERO == resultKanbanMapSeparateByVendorCode.size() ) {
                        MessageUtil.getErrorMessageForBatch(
                            locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                            null, 
                            LOG, 
                            Constants.TWO, 
                            false);
                    } else {
                        List<String> listDoIdUpdate = new ArrayList<String>();
                        List<String> listDoNoUpdate = new ArrayList<String>();
						for (String cigmaDoNoOriginal : resultKanbanMapSeparateByVendorCode
								.keySet()) {
							List<TransferOneWayKanbanDataFromCigmaDomain> transferDomainList = resultKanbanMapSeparateByVendorCode
									.get(cigmaDoNoOriginal);
							for (TransferOneWayKanbanDataFromCigmaDomain transferDomainSeparateByVendorCd : transferDomainList) {
								final List<List<TransferOneWayKanbanDataFromCigmaDomain>> transferKanbanList = new ArrayList<List<TransferOneWayKanbanDataFromCigmaDomain>>();
								Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> resultKanbanMap = transferDoDataFromCigmaFacadeService
										.searchOnewayKanbanTag(
												locale,
												server,
												SupplierPortalConstant.SPS_KANBAN_FLAG_TRANSFER,
												transferDomainSeparateByVendorCd
														.getCigmaDoNo(),
												transferDomainSeparateByVendorCd
														.getsCd());
								if (null != resultKanbanMap
										&& Constants.ZERO == resultKanbanMap
												.size()) {
									MessageUtil
											.getErrorMessageForBatch(
													locale,
													SupplierPortalConstant.ERROR_CD_SP_E6_0001,
													null, LOG, Constants.TWO,
													false);
								} else {
									// 12 Keep Kanban tag which already to
									// transfer to SPS.
									if (null != resultKanbanMap) {
										for (String cigmaDoNo : resultKanbanMap
												.keySet()) {
											List<TransferOneWayKanbanDataFromCigmaDomain> transferDomain = resultKanbanMap
													.get(cigmaDoNo);
											List<TransferOneWayKanbanDataFromCigmaDomain> tmpList = null;
											try {
												tmpList = transferDoDataFromCigmaFacadeService
														.transactCheckTransferOneWayKanbanFromCigma(
																locale,
																transferDomain,
																server,
																originalDoErrorSet,
																false, jobId,
																LOG);
												if (null != tmpList
														&& Constants.ZERO < tmpList
																.size()) {
													transferKanbanList
															.add(tmpList);
												}
											} catch (Exception e) {
												// Log OutPut Log
												LOG.error("[One Way Kanban Tag] "
														.concat(e.getMessage()));
											}
										}
									} // resultKanbanMap is null
								} // End If

								for (List<TransferOneWayKanbanDataFromCigmaDomain> transferDomain : transferKanbanList) {
									try {
										// 12-1) Call Façade Service
										// TransferDoDataFromCigmaFacadeService
										// transactTransferOneWayKanbanFromCigma()
										// to save data in SPS system
										String message = transferDoDataFromCigmaFacadeService
												.transactTransferOneWayKanbanFromCigma(
														locale, transferDomain,
														server, jobId, listDoIdUpdate);
										if(!listDoNoUpdate.contains(cigmaDoNoOriginal.trim())){
											listDoNoUpdate.add(cigmaDoNoOriginal.trim());
						                }
										if (!Constants.EMPTY_STRING
												.equals(message)) {
											LOG.error(message);
										}
									} catch (WebServiceCallerRestException e) {
										LOG.error("[One Way Kanban Tag] "
												.concat(e.getMessage()));
										break;
									} catch (ApplicationException e) {
										LOG.error("[One Way Kanban Tag] "
												.concat(e.getMessage()));
									} catch (Exception e) {
										LOG.error("[One Way Kanban Tag] "
												.concat(e.getMessage()));
									}
								}
							}
						}
						
//                        20230323 Rungsiwut move from service transactTransferOneWayKanbanFromCigma to call 1 time.
						if (listDoIdUpdate.size() > Constants.ZERO) {
							String originalCigmaDoNo = StringUtil
									.convertListToVarcharCommaSeperate(listDoNoUpdate);
							System.out.println(originalCigmaDoNo);
							CommonWebServiceUtil
									.kanbanResourceUpdateKanbanTag(
											server,
											originalCigmaDoNo,
											Constants.EMPTY_STRING,
											SupplierPortalConstant.SPS_KANBAN_FLAG_TRANSFERED,
											jobId);

							// 3. Update ONE_WAY_KANBAN_TAG_FLAG = '1' on
							// SPS_T_DO
							// to notify e-mail to user
							try {
								String originalCigmaDoId = StringUtil
										.convertListToVarcharCommaSeperate(listDoIdUpdate);
								System.out.println(originalCigmaDoId);
								KanbanTagDomain kanbanTag = new KanbanTagDomain();
								kanbanTag.setDoNo(originalCigmaDoId);
								transferDoDataFromCigmaFacadeService
										.updateOneWayKanbanTagFlag(kanbanTag);
							} catch (ApplicationException e) {
								LOG.error("[One Way Kanban Tag] ".concat(e
										.getMessage()));
							}
						}
						
                    }
                }catch(Exception ex){
                    LOG.error(ex.getMessage(), ex);
                }
            } // end for each server.
            // 13   Call Façade Service TransferDODataFromCIGMAFacadeService searchRecordLimit() without parameter
            MiscellaneousDomain abnormalLimit = 
                transferDoDataFromCigmaFacadeService.searchRecordLimit();
//            abnormalLimit = null;
            if( null == abnormalLimit ){
                MessageUtil.getErrorMessageForBatch(
                    locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                    null, 
                    LOG, 
                    Constants.TWO, 
                    false);
            }else {
                // 14.  Get abnormal data for D/O  and send notification to supplier user
                // 14-1)        Call Façade Service TransferDODataFromCIGMAFacadeService searchCigmaDoError() without condition

                // Search CIGMA DO for send to Supplier
                SpsCigmaDoErrorDomain doCriteria = new SpsCigmaDoErrorDomain();
                doCriteria.setMailFlg(Constants.STR_ONE);
                doCriteria.setDCd(company.getDCd());
                doCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_SUPPLIER);
                doCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_FOR_SEND_TO_SUPPLIER);
                List<TransferDoErrorEmailDomain> cigmaDoToSupplierList
                    = transferDoDataFromCigmaFacadeService.searchCigmaDoError(doCriteria);

                // Search CIGMA DO for send to DENSO
                doCriteria = new SpsCigmaDoErrorDomain();
                doCriteria.setMailFlg(Constants.STR_ONE);
                doCriteria.setDCd(company.getDCd());
                doCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_DENSO);
                doCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_FOR_SEND_TO_DENSO);
                List<TransferDoErrorEmailDomain> cigmaDoToDensoList
                    = transferDoDataFromCigmaFacadeService.searchCigmaDoError(doCriteria);
                
                // Search CIGMA DO for send to Admin
                doCriteria = new SpsCigmaDoErrorDomain();
                doCriteria.setMailFlg(Constants.STR_ONE);
                doCriteria.setDCd(company.getDCd());
                doCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_DENSO);
                doCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_FOR_SEND_TO_ADMIN);
                List<TransferDoErrorEmailDomain> cigmaDoToAdminList
                    = transferDoDataFromCigmaFacadeService.searchCigmaDoError(doCriteria);
                
                // 14-2) Call Façade Service TransferDODataFromCIGMAFacadeService searchCigmaChgDoError() without condition
                // Search CIGMA CHG DO for send to Supplier
                SpsCigmaChgDoErrorDomain chgDoCriteria = new SpsCigmaChgDoErrorDomain();
                chgDoCriteria.setMailFlg(Constants.STR_ONE);
                chgDoCriteria.setDCd(company.getDCd());
                chgDoCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_SUPPLIER);
                chgDoCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_FOR_SEND_TO_SUPPLIER);
                List<TransferChgDoErrorEmailDomain> cigmaChgDoToSupplierList
                    = transferDoDataFromCigmaFacadeService.searchCigmaChgDoError(chgDoCriteria);

                // Search CIGMA CHG DO for send to DENSO
                chgDoCriteria = new SpsCigmaChgDoErrorDomain();
                chgDoCriteria.setMailFlg(Constants.STR_ONE);
                chgDoCriteria.setDCd(company.getDCd());
                chgDoCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_DENSO);
                chgDoCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_FOR_SEND_TO_DENSO);
                List<TransferChgDoErrorEmailDomain> cigmaChgDoToDensoList
                    = transferDoDataFromCigmaFacadeService.searchCigmaChgDoError(chgDoCriteria);

                // Search CIGMA CHG DO for send to Admin
                chgDoCriteria = new SpsCigmaChgDoErrorDomain();
                chgDoCriteria.setMailFlg(Constants.STR_ONE);
                chgDoCriteria.setDCd(company.getDCd());
                chgDoCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_DENSO);
                chgDoCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_FOR_SEND_TO_ADMIN);
                List<TransferChgDoErrorEmailDomain> cigmaChgDoToAdminList
                    = transferDoDataFromCigmaFacadeService.searchCigmaChgDoError(chgDoCriteria);
                
                //Start [CR20230129] change group send mail DO Backorder
                // Search CIGMA DO BackOrder for send to Admin
                doCriteria = new SpsCigmaDoErrorDomain();
                doCriteria.setMailFlg(Constants.STR_ONE);
                doCriteria.setDCd(company.getDCd());
                doCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_DENSO);
                doCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_DOBACKORDER_FOR_SEND_TO_ADMIN);
                List<TransferDoErrorEmailDomain> cigmaDoBackOrderToAdminList
                    = transferDoDataFromCigmaFacadeService.searchCigmaDoError(doCriteria);

                // Search CIGMA CHG DO BackOrder for send to Admin
                chgDoCriteria = new SpsCigmaChgDoErrorDomain();
                chgDoCriteria.setMailFlg(Constants.STR_ONE);
                chgDoCriteria.setDCd(company.getDCd());
                chgDoCriteria.setPreferredOrder(
                    SupplierPortalConstant.SORT_ERROR_FOR_SEND_TO_DENSO);
                chgDoCriteria.setErrorTypeFlg(
                    SupplierPortalConstant.ERROR_TYPE_FLG_DOBACKORDER_FOR_SEND_TO_ADMIN);
                List<TransferChgDoErrorEmailDomain> cigmaChgDoBackOrderToAdminList
                    = transferDoDataFromCigmaFacadeService.searchCigmaChgDoError(chgDoCriteria);
                //End [CR20230129] change group send mail DO Backorder
                
                // 14-3)        If no any data from both of D/O Error and CHG D/O Error, then exit program
                // 14-4)        Compare recordCount D/O Error with recordLimit to separate commit transaction by recordLimit
                // 14-5)        Compare recordCount CHG D/O Error with recordLimit to separate commit transaction by recordLimit
                // 14-6)       Send list of D/O Error and CHG D/O Error to Supllier user by e-mail
                if( (null == cigmaDoToSupplierList
                        || Constants.ZERO == cigmaDoToSupplierList.size()) 
                    && ( null == cigmaDoToDensoList
                        || Constants.ZERO == cigmaDoToDensoList.size()) 
                    && ( null == cigmaChgDoToSupplierList 
                        || Constants.ZERO == cigmaChgDoToSupplierList.size())
                    && ( null == cigmaChgDoToDensoList 
                        || Constants.ZERO == cigmaChgDoToDensoList.size()) 
                    && ( null == cigmaDoToAdminList
                        || Constants.ZERO == cigmaDoToAdminList.size())
                    && (null == cigmaChgDoToAdminList
                        || Constants.ZERO == cigmaChgDoToAdminList.size())
                    && (null == cigmaDoBackOrderToAdminList
                        || Constants.ZERO == cigmaDoBackOrderToAdminList.size())
                    && (null == cigmaChgDoBackOrderToAdminList
                        || Constants.ZERO == cigmaChgDoBackOrderToAdminList.size())
                        )
                {
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0014, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }else {
                    try{
                        transferDoDataFromCigmaFacadeService
                            .transactSendNotificationAbnormalDataDo(
                                locale, 
                                cigmaDoToSupplierList,
                                cigmaDoToDensoList,
                                cigmaDoToAdminList,
                                cigmaChgDoToSupplierList, 
                                cigmaChgDoToDensoList,
                                cigmaChgDoToAdminList,
                                cigmaDoBackOrderToAdminList,
                                cigmaChgDoBackOrderToAdminList,
                                Integer.parseInt(abnormalLimit.getMiscValue()),
                                LOG);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }
            }
            
            // 18.  Get BackOrder for notify to Supplier.
            
            SpsTDoDetailDomain spsTDoDeatilDomain = new SpsTDoDetailDomain();
            List<DeliveryOrderDomain> backOrderList = transferDoDataFromCigmaFacadeService.searchMailBackOrder(spsTDoDeatilDomain);

            if( null != backOrderList && Constants.ZERO == backOrderList.size() ){
                //Out Put Log.
                MessageUtil.getErrorMessageForBatch(
                    locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0085, 
                    null, 
                    LOG, 
                    Constants.TWO, 
                    false);
            }else{
                MiscellaneousDomain notiBackorderLimit = transferDoDataFromCigmaFacadeService.searchUrgentRecordLimit();
                
                if( null != notiBackorderLimit && null != notiBackorderLimit.getMiscValue() ){
                    try {
                        int inotiBackorderLimit = Integer.parseInt(notiBackorderLimit.getMiscValue());
                        transferDoDataFromCigmaFacadeService.transactSendNotificationChangeBackOrder(
                                locale, 
                                backOrderList, 
                                inotiBackorderLimit,
                                LOG);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }else{
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }
                /*
                MiscellaneousDomain urgentLimit = transferDoDataFromCigmaFacadeService.searchUrgentRecordLimit();
                
                if( null != urgentLimit && null != urgentLimit.getMiscValue() ){
                    try {
                        int iUrgentLimit = Integer.parseInt(urgentLimit.getMiscValue());
                        transferDoDataFromCigmaFacadeService
                            .transactSendNotificationChangeOrder(
                                locale, 
                                urgentList, 
                                iUrgentLimit,
                                LOG);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }else{
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }
                */
            }
            
            // 15.  Get urgent order to notify Change Order
            UrgentOrderDomain criteriaUrgent = new UrgentOrderDomain();
            criteriaUrgent.setUrgentOrderFlgHeader( Constants.ONE );
            criteriaUrgent.setUrgentOrderFlgDetail( Constants.ONE );
            criteriaUrgent.setLatestRevFlg( Constants.ONE );
            criteriaUrgent.setMailFlg( Constants.ZERO );
            
            List<DeliveryOrderDomain> urgentList = transferDoDataFromCigmaFacadeService.searchUrgentOrder(criteriaUrgent);
            if( null != urgentList && Constants.ZERO == urgentList.size() ){
                //Out Put Log.
                MessageUtil.getErrorMessageForBatch(
                    locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0008, 
                    null, 
                    LOG, 
                    Constants.TWO, 
                    false);
            }else{
                // 16.  Get record limit for notification Change Order
                MiscellaneousDomain urgentLimit = transferDoDataFromCigmaFacadeService
                    .searchUrgentRecordLimit();
                
                if( null != urgentLimit && null != urgentLimit.getMiscValue() ){
                    // 17. Send notification urgent order to user and update alert flag
                    try {
                        int iUrgentLimit = Integer.parseInt(urgentLimit.getMiscValue());
                        transferDoDataFromCigmaFacadeService
                            .transactSendNotificationChangeOrder(
                                locale, 
                                urgentList, 
                                iUrgentLimit,
                                LOG);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }else{
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }
            }
            
            // SPS Phase II
            // 18   Get D/O notify to Supplier.
            SpsTDoDomain spsTDoDomain = new SpsTDoDomain();
            spsTDoDomain.setNotificationMailDoFlag(Constants.STR_ZERO);
            List<SpsTDoDomain> spsTDoDomainList = 
                transferDoDataFromCigmaFacadeService.searchNotifyDeliveryOrderToSupplier(spsTDoDomain);
            // 18-1 Get information to notify to supplier.
            if( null != spsTDoDomainList && Constants.ZERO == spsTDoDomainList.size() ){
                MessageUtil.getErrorMessageForBatch(
                    locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0060, 
                    null, 
                    LOG, 
                    Constants.ZERO, 
                    false);
            }else{
                // 18-2 Notify the e-mail to supplier.
                MiscellaneousDomain notificationLimit = transferDoDataFromCigmaFacadeService
                    .searchUrgentRecordLimit();
                if( null != notificationLimit && null != notificationLimit.getMiscValue() ){
                    try {
                        int iNotificationLimit = Integer.parseInt(notificationLimit.getMiscValue());
                        transferDoDataFromCigmaFacadeService
                            .transactNotifyDeliveryOrderToSupplier(
                                locale, 
                                spsTDoDomainList, 
                                iNotificationLimit,
                                LOG,
                                spsTDoDomain);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }else {
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }
            }
         // 17.  Get Tag notify to Supplier.
            spsTDoDomain = new SpsTDoDomain();
            spsTDoDomain.setNotificationMailKanbanFlag(Constants.STR_ZERO);
            spsTDoDomainList = 
                transferDoDataFromCigmaFacadeService.searchNotifyDeliveryOrderToSupplier(spsTDoDomain);
            // 19-1 Get information to notify to supplier.
            if( null != spsTDoDomainList && Constants.ZERO == spsTDoDomainList.size() ){
                MessageUtil.getErrorMessageForBatch(
                    locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0061, 
                    null, 
                    LOG, 
                    Constants.ZERO, 
                    false);
            }else{
                // 19-2 Notify the e-mail to supplier.
                MiscellaneousDomain notificationLimit = transferDoDataFromCigmaFacadeService
                    .searchUrgentRecordLimit();
                if( null != notificationLimit && null != notificationLimit.getMiscValue() ){
                    try {
                        int iNotificationLimit = Integer.parseInt(notificationLimit.getMiscValue());
                        transferDoDataFromCigmaFacadeService
                            .transactNotifyDeliveryOrderToSupplier(
                                locale, 
                                spsTDoDomainList, 
                                iNotificationLimit,
                                LOG,
                                spsTDoDomain);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }else {
                    MessageUtil.getErrorMessageForBatch(
                        locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        null, 
                        LOG, 
                        Constants.TWO, 
                        false);
                }
            }
            result = true;
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
            String message = MessageUtil.getApplicationMessageWithLabel(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0015,
                SupplierPortalConstant.TRANSFER_DO_STEP_PROCESS_NAME);
            LOG.info(message);
        }
        return result;
    }
    
    /**
     * Gets the content.
     * @return default locale
     */
    private Locale getDefaultLocale(){
        Properties propApp 
            = Props.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
        String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
        String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
        String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
        ContextParams.setDefaultLocale(strDefaultLocale);
        ContextParams.setBaseDirGui(strBaseDirGui);
        ContextParams.setBaseDirMsg(strBaseDirMsg);
        return LocaleUtil.getLocaleFromString(strDefaultLocale);
    }
    
}
