/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat       Create
 * 2015/12/17 CSI Akat           [IN050]
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ChildJobBase;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.PoForceAcknowledgeDomain;
import com.globaldenso.asia.sps.business.service.PoForceAcknowledgeFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * <p>
 * Update Acknowledge Flag when Supplier does not confirm in time.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class PoFoceAcknowledgeChildJob extends ChildJobBase {
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(PoFoceAcknowledgeChildJob.class);

    /** Call Service POForceAcknowledgeFacadeService. */
    private PoForceAcknowledgeFacadeService poForceAcknowledgeFacadeService;

    /**
     * Default constructor.
     */
    public PoFoceAcknowledgeChildJob() {

    }

    /**
     * <p>
     * Setter method for poForceAcknowledgeFacadeService.
     * </p>
     * 
     * @param poForceAcknowledgeFacadeService Set for
     *            poForceAcknowledgeFacadeService
     */
    public void setPoForceAcknowledgeFacadeService(
        PoForceAcknowledgeFacadeService poForceAcknowledgeFacadeService) {
        this.poForceAcknowledgeFacadeService = poForceAcknowledgeFacadeService;
    }

    /**
     * Auto-generated method stub : do nothing.
     * 
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void preBizProc(JobInputDomain arg0) throws ApplicationException {
    }

    /**
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void mainBizProc(JobInputDomain arg0) throws ApplicationException {
        LoadContextPropertyForBatch.loadContextListener(LOG);
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        
        MessageUtil.getErrorMessageForBatch(locale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0014,
            new String[] {getLabel(SupplierPortalConstant.LBL_PO_FORCE_ACKNOWLEDGE, locale)}, LOG,
            Constants.ZERO, false);
        
        String updateUser = arg0.getJobId();
        PoForceAcknowledgeDomain poForceAcknowledgeDomain = new PoForceAcknowledgeDomain();
        SpsTPoCriteriaDomain poCriteria = null;
        SpsTPoDetailDomain PoDetailDomain = null;
        SpsTPoDetailCriteriaDomain poDetailCriteriaDomain = null;
        
        poCriteria = new SpsTPoCriteriaDomain();
        // [IN050] force acknowledge both Firm and Forecast period
        //poCriteria.setPeriodType(String.valueOf(Constants.ZERO));
        poCriteria.setPoStatus(Constants.ISS_STATUS);
        poCriteria.setForceAckDateLessThanEqual(new java.sql.Date(System.currentTimeMillis()));
        
        
        //Call Facade Service POForceAcknowledgeFacadeService searchForceAcknowledgePO().
        List<SpsTPoDomain> spsTPoDomainList
            = poForceAcknowledgeFacadeService.searchForceAcknowledgePo(poCriteria);
        
        if (null == spsTPoDomainList || Constants.ZERO == spsTPoDomainList.size()) {
            MessageUtil.getErrorMessageForBatchWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0016, null, null, LOG,
                Constants.ZERO, false);
        }
        
        if (Constants.ZERO != spsTPoDomainList.size()) {
            
            List<SpsTPoDomain> poList = new ArrayList<SpsTPoDomain>();
            List<SpsTPoCriteriaDomain> pocriList = new ArrayList<SpsTPoCriteriaDomain>();
            List<SpsTPoDetailDomain> podetailList = new ArrayList<SpsTPoDetailDomain>();
            List<SpsTPoDetailCriteriaDomain> podetailcriList = 
                new ArrayList<SpsTPoDetailCriteriaDomain>();
            
            try {
                
                /**
                 * Call Facade Service POForceAcknowledgeFacadeService for
                 * update P/O Header and Detail to "Force Acknowledge".
                 */
                for (SpsTPoDomain poDomain : spsTPoDomainList) {
                    poDomain.setPoStatus(SupplierPortalConstant.FAC);
                    poDomain.setLastUpdateDscId(updateUser);
                    poList.add(poDomain);
                    
                    poCriteria = new SpsTPoCriteriaDomain();
                    poCriteria.setPoId(poDomain.getPoId());
                    poCriteria.setLastUpdateDatetime(poDomain.getLastUpdateDatetime());
                    pocriList.add(poCriteria);
                    
                    PoDetailDomain = new SpsTPoDetailDomain();
                    PoDetailDomain.setPnStatus(SupplierPortalConstant.FAC);
                    PoDetailDomain.setLastUpdateDscId(updateUser);
                    podetailList.add(PoDetailDomain);
                    
                    poDetailCriteriaDomain = new SpsTPoDetailCriteriaDomain();
                    poDetailCriteriaDomain.setPoId(poDomain.getPoId());
                    podetailcriList.add(poDetailCriteriaDomain);
                }
                poForceAcknowledgeDomain.setSpsTPoDomainList(poList);
                poForceAcknowledgeDomain.setSpsTPoCriteriaDomainList(pocriList);
                poForceAcknowledgeDomain.setSpsTPoDetailDomainList(podetailList);
                poForceAcknowledgeDomain.setSpsTPoDetailCriteriaDomainList(podetailcriList);
                
                poForceAcknowledgeFacadeService
                    .transactForceAcknowledgePo(poForceAcknowledgeDomain);
                
            } catch (ApplicationException e) {
                MessageUtil.writeLog(LOG, Constants.TWO, e.getMessage());
                MessageUtil.throwsApplicationMessage(locale, null, e.getMessage());
            } catch (Exception e) {
                MessageUtil.getErrorMessageForBatch(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0004,
                    new String[] {getLabel(SupplierPortalConstant.LBL_ERROR, locale),
                        e.getMessage()}, LOG, Constants.TWO, true);
            }
        }
        
        MessageUtil.getErrorMessageForBatch(locale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0015,
            new String[] {getLabel(SupplierPortalConstant.LBL_PO_FORCE_ACKNOWLEDGE, locale)},
            LOG, Constants.ZERO, false);
    }

    /**
     * @param arg0 job input domain
     * @throws ApplicationException ApplicationException
     */
    @Override
    public void postBizProc(JobInputDomain arg0) throws ApplicationException {
    }

    /**
     * <p>Get Label method.</p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {
        String labelString = MessageUtil.getLabelHandledException(locale, key);
        return labelString;
    }
}
