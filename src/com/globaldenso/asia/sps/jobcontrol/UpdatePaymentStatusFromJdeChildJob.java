/*
 * ModifyDate Development company     Describe 
 * 2014/08/26 CSI Akat                Create
 * 2015/12/16 CSI Akat                [IN037]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.jobcontrol;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aijb.core.business.domain.JobInputDomain;
import com.globaldenso.ai.aijb.core.job.jobbase.ChildJobBase;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.UpdateInvoiceInformationDomain;
import com.globaldenso.asia.sps.business.service.UpdatePaymentStatusFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.LoadContextPropertyForBatch;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * <p>The Class UpdatePaymentStatusFromJdeChildJob.</p>
 * <p>Batch process BINV002 Schedule Update Payment Status from JDE to SPS.</p>
 * 
 * @author CSI
 */
public class UpdatePaymentStatusFromJdeChildJob extends ChildJobBase {

    /** LOG for output log. */
    private static final Log LOG = LogFactory.getLog(UpdatePaymentStatusFromJdeChildJob.class);
    
    /** The Facade Update Payment Status. */
    private UpdatePaymentStatusFacadeService updatePaymentStatusFacadeService;
    
    /** The default constructor. */
    public UpdatePaymentStatusFromJdeChildJob() {
        super();
    }

    /**
     * <p>Setter method for updatePaymentStatusFacadeService.</p>
     *
     * @param updatePaymentStatusFacadeService Set for updatePaymentStatusFacadeService
     */
    public void setUpdatePaymentStatusFacadeService(
        UpdatePaymentStatusFacadeService updatePaymentStatusFacadeService) {
        this.updatePaymentStatusFacadeService = updatePaymentStatusFacadeService;
    }

    /**
     * Batch Pre process output log and check parameter.
     * @param arg0 argument AI-JM
     * @throws ApplicationException an ApplicationException
     * */
    @Override
    public void preBizProc(JobInputDomain arg0) throws ApplicationException {
        LoadContextPropertyForBatch.loadContextListener(LOG);
        
        // 1. OutPut Log
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        MessageUtil.getErrorMessageForBatchWithLabel(objLocale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0014,
            SupplierPortalConstant.LBL_UPDATE_PAYMENT_STATUS_FROM_JDE, null, LOG, Constants.ZERO,
            false);
        
        String[] param = arg0.getExecuteArg().split(Constants.REGX_PIPE);

        // 2. If {parameter In 2} ='9999/99/99' and {parameter In 2} is not validate date format 
        String scopeDate = Constants.EMPTY_STRING;
        if (Constants.TWO == param.length) {
            scopeDate = param[Constants.ONE];
        }
        if (!SupplierPortalConstant.PARAM_DATE_NOT_SPECIFIED.equals(scopeDate)
            && !scopeDate.matches(Constants.REGX_DATEFORMAT))
        {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[] {scopeDate}, LOG,
                Constants.TWO, true);
        }
    }
    
    /**
     * Batch Main Process Update payment status from JDE.
     * @param arg0 argument AI-JM
     * @throws ApplicationException an ApplicationException
     * */
    @Override
    public void mainBizProc(JobInputDomain arg0) throws ApplicationException {
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);

        String[] param = arg0.getExecuteArg().split(Constants.REGX_PIPE);

        String[] densoCodeArray = param[Constants.ZERO].split(Constants.SYMBOL_COMMA);
        List<String> densoCodeList = new ArrayList<String>();
        for (String densoCode : densoCodeArray) {
            densoCodeList.add(densoCode.trim());
        }
        String densoCodeVarcharCsv = StringUtil.convertListToCharCommaSeperate(densoCodeList
            , Constants.FIVE);
        
        // 1. Call Façade Service UpdatePaymentStatusFacadeService searchAS400ServerList()
        SpsMCompanyDensoDomain companyCriteria = new SpsMCompanyDensoDomain();
        companyCriteria.setDCd(densoCodeVarcharCsv);
        List<As400ServerConnectionInformationDomain> serverList = null;
        try {
            serverList = this.updatePaymentStatusFacadeService.searchAs400ServerList(companyCriteria);
        } catch (Exception e) {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_90_0001
                , new String[]{e.getClass().toString(), e.getMessage()}
                , LOG, Constants.TWO, true);
        }

        if (Constants.ZERO == serverList.size()) {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0026, new String[] {param[Constants.ZERO]},
                LOG, Constants.TWO, true);
        }
        
        if (densoCodeArray.length != serverList.size()) {
            MessageUtil.getErrorMessageForBatch(objLocale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0025, new String[] {param[Constants.ZERO]},
                LOG, Constants.TWO, false);
        }
        
        // 2. LOOP all data in as400Server
        UpdateInvoiceInformationDomain updateParam = null;
        String scopeDate = null;
        if (SupplierPortalConstant.PARAM_DATE_NOT_SPECIFIED.equals(param[Constants.ONE])) {
            Calendar today = Calendar.getInstance();
            today.add(Calendar.DATE, Constants.MINUS_ONE);
            Date yesterday = new Date(today.getTimeInMillis());
            scopeDate = DateUtil.convertDateGregorianDateToJulianDate(yesterday);
        } else {
            scopeDate = DateUtil.convertStringGregorianDateToJulianDate(param[Constants.ONE]);
        }
        
        for (As400ServerConnectionInformationDomain server : serverList) {
            // 2.1 Call Façade Service UpdatePaymentStatusFacadeService updatePaymentStatusService()
            updateParam = new UpdateInvoiceInformationDomain();
            // [IN037] separate REST between Order and ASN/Invoice
            //updateParam.setSpsMAs400LparDomain(server.getSpsMAs400LparDomain());
            updateParam.setSpsMAs400LparDomain(server.getAsnInvoiceLpar());
            updateParam.setSpsMAs400SchemaDomain(server.getSpsMAs400SchemaDomain());
            updateParam.setDataScopeDate(scopeDate);
            updateParam.setDscId(arg0.getJobId());
            updateParam.setDCd(server.getSpsMCompanyDensoDomain().getDCd());
            
            try {
                this.updatePaymentStatusFacadeService.updatePaymentStatus(updateParam);
            } catch (ApplicationException applicationException) {
                LOG.error(applicationException.getMessage());
            } catch (Exception exception) {
                String labelError = MessageUtil.getLabelHandledException(objLocale,
                    SupplierPortalConstant.LBL_ERROR);
                MessageUtil.getErrorMessageForBatch(objLocale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0004,
                    new String[] {labelError, exception.getMessage()},
                    LOG, Constants.TWO, false);
            }
        }
    }

    /**
     * Batch Post process output log.
     * @param arg0 argument AI-JM
     * @throws ApplicationException an ApplicationException
     * */
    @Override
    public void postBizProc(JobInputDomain arg0) throws ApplicationException {
        Locale objLocale = LocaleUtility.langToLocale(Constants.BATCH_DEFAULT_LOCALE);
        MessageUtil.getErrorMessageForBatchWithLabel(objLocale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0015,
            SupplierPortalConstant.LBL_UPDATE_PAYMENT_STATUS_FROM_JDE, null, LOG, Constants.ZERO,
            false);
    }
}
