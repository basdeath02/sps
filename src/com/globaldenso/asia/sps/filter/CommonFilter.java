/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * 1.When user try to access my web so program will check session value first and then if session timeout, 
 *   session value is null or anything. Program will redirect to login page.
 * 2.When user not have permission to access that page. Program will check and redirect to permission page.
 * 3.Control session value.
 * The Class CommonFilter.
 * @author CSI
 */
public class CommonFilter implements Filter {

    /** The log. */
    private static final Log LOG = LogFactory.getLog(CommonFilter.class);

    /** The constants for word 'method'. */
    private static final String WORD_METHOD = "method";

    /** The constants for word 'userName'. */
    private static final String WORD_USERNAME = "userName";

    /** The constants for word 'dscId'. */
    private static final String PRM_DSCID = "dscId";
    
    /** The constants for Login Action name. */
    private static final String LOGIN_ACTION_NAME = "LoginAction.do";

    /** The constants for Main Screen Action name. */
    private static final String MAIN_SCREEN_ACTION_NAME = "MainScreenAction.do";

    /** The constants for Login Revision 01 JSP file. */
    private static final String LOGIN_REV01_JSP = "/Wcom001_Login_rev01.jsp";

    /** The constants for Login JSP file. */
    private static final String LOGIN_JSP = "/Wcom001_Login.jsp";

    /** The constants for Permission JSP file. */
    private static final String PERMISSION_JSP = "/Permission.jsp";

    /** The constants for Timeout JSP file. */
    private static final String TIMEOUT_JSP = "/Timeout.jsp";

    /** The constants for Index HTML file. */
    private static final String INDEX_HTML = "/index.html";

    /** The constants for Error JSP file. */
    private static final String ERROR_JSP = "error.jsp";

    /** The constants for Logout JSP file. */
    private static final String LOGOUT_JSP = "/Wcom006_Logout.jsp";
    
    /** The Parameter name 'errormessage'. */
    private static final String PRM_ERROR_MSG = "errormessage";
    
    /** The encoding. */
    protected String encoding = null;
    
    /** The filter config. */
    protected FilterConfig filterConfig = null;
    
    /** The ignore. */
    protected boolean ignore = true;
    
    /**
     * Instantiates a new common filter.
     */
    public CommonFilter() {
        super();
    }

    /**
     * destroy
     */
    public void destroy() {
    }

    /**
     * Do filter.
     * 
     * @param req the request
     * @param sresponse the response
     * @param chain the chain
     * @throws IOException IOException
     * @throws ServletException ServletException
     */
    public void doFilter(ServletRequest req, ServletResponse sresponse, FilterChain chain) 
        throws IOException, ServletException {

        /** Local variable declaration. */
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) sresponse;
        /** Set encoding. */
        if(ignore || (null == request.getCharacterEncoding())) {
            String encoding = selectEncoding(request);
            Locale locale = this.getLocaleFromDensoContext();
            LOG.debug(MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0017, new String[]{encoding}));
            if(null != encoding) {
                request.setCharacterEncoding(encoding);
                response.setCharacterEncoding(encoding);
            }
        }
        
        String url = request.getRequestURI();
        String requestedActionMethod = request.getParameter(WORD_METHOD);
        
        // TODO development phase
        String userNameParam = Constants.EMPTY_STRING; //request.getParameter(WORD_USERNAME);
        
        String uidHttpHeader = request.getHeader(Constants.HTTP_HEADER_KEY_UID);
        String uidHttpParam = request.getParameter(Constants.HTTP_HEADER_KEY_UID);
        
        LOG.info(StringUtil.appendsString(Constants.HTTP_HEADER_KEY_UID,
            Constants.SYMBOL_COLON, uidHttpHeader));
        
        if(!StringUtil.checkNullOrEmpty(uidHttpHeader)){
            userNameParam = uidHttpHeader;
        } else if (!StringUtil.checkNullOrEmpty(uidHttpParam)) {
            userNameParam = uidHttpParam;
        } else {
            userNameParam = request.getParameter(WORD_USERNAME);
        }
        
        if ( ((Constants.MINUS_ONE < url.indexOf(LOGIN_ACTION_NAME)) 
            && (SupplierPortalConstant.METHOD_DO_LOGIN.equals(requestedActionMethod)))
            || ((Constants.MINUS_ONE < url.indexOf(MAIN_SCREEN_ACTION_NAME)) 
            && (SupplierPortalConstant.METHOD_DO_LOGOUT.equals(requestedActionMethod)))
            || ((Constants.MINUS_ONE < url.indexOf(LOGIN_ACTION_NAME)) 
            && (SupplierPortalConstant.METHOD_DO_CHANGE_LOCALE.equals(requestedActionMethod)))
            || ((Constants.MINUS_ONE < url.indexOf(MAIN_SCREEN_ACTION_NAME)) 
            && (SupplierPortalConstant.METHOD_DO_CHANGE_LOCALE.equals(requestedActionMethod)))
            || ((Constants.MINUS_ONE < url.indexOf(LOGIN_ACTION_NAME)) 
            && (SupplierPortalConstant.METHOD_DO_INITIAL.equals(requestedActionMethod)))
            || ((Constants.MINUS_ONE < url.indexOf(MAIN_SCREEN_ACTION_NAME)) 
            && (SupplierPortalConstant.METHOD_DO_INITIAL.equals(requestedActionMethod)))
            || (Constants.MINUS_ONE < url.indexOf(LOGIN_REV01_JSP))
            || (Constants.MINUS_ONE < url.indexOf(LOGIN_JSP))
            || (Constants.MINUS_ONE < url.indexOf(TIMEOUT_JSP))
            || (Constants.MINUS_ONE < url.indexOf(PERMISSION_JSP))
            || (Constants.MINUS_ONE < url.indexOf(INDEX_HTML))
            || (Constants.MINUS_ONE < url.indexOf(ERROR_JSP))
            || (Constants.MINUS_ONE < url.indexOf(LOGOUT_JSP))
            || (Constants.MINUS_ONE < url.substring(url.length() - 1, url.length()).indexOf(
                Constants.SYMBOL_SLASH)))
        {
            try {
                chain.doFilter(request, response);
            } catch (Exception e) {
                this.redirectToErrorScreen(e, response);
            }
            return;
        }
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        
        if (null == userLoginDomain || (!userLoginDomain.getDscId().equals(userNameParam) 
            && null != userNameParam)) {

            // TODO Development Phase
            //try {
            //    extendDoFilter(request, response, chain);
            //} catch (Exception e) {
            //    this.redirectToErrorScreen(e, response);
            //}
            
            // CONTEXT_PATH/WCOM002Action.do?method=doInitial
            StringBuffer redirectBuffer = new StringBuffer();
            redirectBuffer.append(request.getContextPath()).append(Constants.SYMBOL_SLASH)
                .append(MAIN_SCREEN_ACTION_NAME).append(Constants.SYMBOL_QUESTION_MARK)
                .append(WORD_METHOD).append(Constants.SYMBOL_EQUAL)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL);

            // TODO development phase
            if (null != userNameParam) {
                redirectBuffer.append(Constants.SYMBOL_AMPERSAND)
                    // param name
                    .append(PRM_DSCID)
                    .append(Constants.SYMBOL_EQUAL)
                    // value
                    .append(userNameParam);
            } else {
                //response.sendRedirect(request.getContextPath() + LOGIN_REV01_JSP);
                response.sendRedirect(Constants.SPS_URL);
                return;
            }
            
            response.sendRedirect(redirectBuffer.toString());
            
            return;
        }
        
        // Check user permission
        boolean canAccess = false;
        for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
            if (null == roleScreen || null == roleScreen.getSpsMScreenDomain()
                || null == roleScreen.getSpsMScreenDomain().getScreenName())
            {
                continue;
            }
            if(Constants.MINUS_ONE
                != url.indexOf(roleScreen.getSpsMScreenDomain().getScreenName()))
            {
                canAccess = true;
            }
        }
        
        if(!canAccess){

            Locale locale = this.getLocaleFromDensoContext();
            String message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E9_0003);
            StringBuffer redirectBuffer = new StringBuffer();
            redirectBuffer.append(request.getContextPath()).append(Constants.SYMBOL_SLASH)
                .append(MAIN_SCREEN_ACTION_NAME).append(Constants.SYMBOL_QUESTION_MARK)
                .append(WORD_METHOD).append(Constants.SYMBOL_EQUAL)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                
                // param name
                .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_TYPE)
                .append(Constants.SYMBOL_EQUAL)
                // value
                .append(Constants.MESSAGE_FAILURE)
                
                // param name
                .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_MESSAGE)
                .append(Constants.SYMBOL_EQUAL)
                // value
                .append(message)
                
                ;
            response.sendRedirect(redirectBuffer.toString());
            return;
        }
        
        // Check enter direct URL
        if(StringUtil.checkNullOrEmpty(requestedActionMethod) 
            && !ServletFileUpload.isMultipartContent(request))
        {
            StringBuffer redirectBuffer = new StringBuffer();
            redirectBuffer.append(request.getContextPath()).append(Constants.SYMBOL_SLASH)
                .append(MAIN_SCREEN_ACTION_NAME).append(Constants.SYMBOL_QUESTION_MARK)
                .append(WORD_METHOD).append(Constants.SYMBOL_EQUAL)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                .append(Constants.SYMBOL_AMPERSAND)
                .append(PRM_DSCID)
                .append(Constants.SYMBOL_EQUAL)
                .append(userLoginDomain.getDscId());
            response.sendRedirect(redirectBuffer.toString());
            return;
        }
        
        try {
            extendDoFilter(request, response, chain);
        } catch (Exception e) {
            this.redirectToErrorScreen(e, response);
        }
        return;
    }
    
    /**
     * Extend do filter.
     * 
     * @param request the request
     * @param response the response
     * @param chain the chain
     * @throws Exception the exception
     * @throws ServletException the servlet exception
     */
    public void extendDoFilter(javax.servlet.http.HttpServletRequest request, 
        javax.servlet.http.HttpServletResponse response, FilterChain chain) 
        throws Exception, ServletException {
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            Locale locale = this.getLocaleFromDensoContext();
            LOG.error(MessageUtil.getApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E5_0029, 
                new String[] {e.getClass().getName(), e.getMessage()}));
            throw e;
        }
    }

    /**
     * @param filterConfig filterConfig
     * @throws ServletException ServletException
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        encoding = filterConfig.getInitParameter(Constants.WORD_ENCODING);
        String value = filterConfig.getInitParameter(Constants.WORD_IGNORE);
        if (null == value) {
            ignore = true;
        } else if (value.equalsIgnoreCase(String.valueOf(true))) {
            ignore = true;
        } else if (value.equalsIgnoreCase(Constants.WORD_YES)) {
            ignore = true;
        } else {
            ignore = false;
        }
    }

    /**
     * Select encoding.
     * 
     * @param request the request
     * @return the string
     */
    protected String selectEncoding(ServletRequest request) {
        return encoding;
    }
    
    /**
     * Get Locale information from DensoContext general area or default Locale.
     * @return locale
     * */
    private Locale getLocaleFromDensoContext() {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        if (null == locale) {
            locale = LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        }
        return locale;
    }
    
    /**
     * Print LOG and redirect to error.jsp.
     * @param e - Exception to print log
     * @param response - HTTP servlet response
     * @throws IOException may throw when send redirect
     * */
    private void redirectToErrorScreen(Exception e, HttpServletResponse response)
        throws IOException
    {
        Locale locale = this.getLocaleFromDensoContext();
        LOG.error(MessageUtil.getApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E5_0028, 
            new String[] {e.getClass().getName(), e.getMessage()}));
        LOG.error(MessageUtil.getApplicationMessageHandledException(locale,
            SupplierPortalConstant.ERROR_CD_SP_I6_0018));
        StringBuffer redirectBuffer = new StringBuffer();
        redirectBuffer.append(ERROR_JSP).append(Constants.SYMBOL_QUESTION_MARK)
            .append(PRM_ERROR_MSG).append(Constants.SYMBOL_EQUAL).append(e.getMessage());
        response.sendRedirect(redirectBuffer.toString());
    }
}