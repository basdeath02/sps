/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain;

/**
 * <p>The Interface Invoice Dao.</p>
 * <p>Service for invoice about search and update invoice status.</p>
 * <ul>
 * <li>Method search  : searchCountInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpUploadInvoiceDao {
    
    /**
     * Search temporary upload invoice detail.
     * Search temporary upload invoice detail by criteria.
     * 
     * @param tmpUploadInvoiceDomain the Temporary Upload Invoice Domain
     * @return the list of Temporary Upload Invoice Domain
     */
    public List<TmpUploadInvoiceDomain> searchTmpUploadInvoice(
        TmpUploadInvoiceDomain tmpUploadInvoiceDomain);
}