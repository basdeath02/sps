/*
 * ModifyDate Development company    Describe
 * 2014/08/19 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;

/**
 * <p>The Interface Upload Asn Dao.</p>
 * <p>DAO for ASN Uploading about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchTempUploadAsn</li>
 * </ul>
 *
 * @author CSI
 */
public interface UploadAsnDao {
    
    /**
     * Search temp upload asn.
     * <p>Search temp upload asn information for maintenance.</p>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the list of asn maintenance return domain
     */
    public List<AsnMaintenanceReturnDomain> searchTmpUploadAsn(
        AsnMaintenanceDomain asnMaintenanceDomain);
    
}