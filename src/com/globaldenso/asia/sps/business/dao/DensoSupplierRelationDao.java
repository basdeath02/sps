/*
 * ModifyDate Development company     Describe 
 * 2014/07/16 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;

/**
 * <p>The Interface DensoSupplierRelationDao.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchRelationByRoleDenso</li>
 * <li>Method search : searchRelationByRoleSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoSupplierRelationDao {
    
    /**
     * Search DENSO company and Supplier company relation by User DENSO role
     * @param dataScopeControlDemain data scope control
     * @return list of DENSO Supplier Relation
     * */
    public List<DensoSupplierRelationDomain> searchRelationByRoleDenso(
        DataScopeControlDomain dataScopeControlDemain);
    
    /**
     * Search DENSO company and Supplier company relation by User Supplier role
     * @param dataScopeControlDemain data scope control
     * @return list of DENSO Supplier Relation
     * */
    public List<DensoSupplierRelationDomain> searchRelationByRoleSupplier(
        DataScopeControlDomain dataScopeControlDemain);

}
