/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/02/24 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.math.BigDecimal;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;

/**
 * <p>The Interface purchase Order Information Dao.</p>
 * <p>Service for purchase Order Information about search data from criteria.</p>
 * <ul>
 * <li>Method search  : SearchPurchaseOrderReport</li>
 * <li>Method search  : SearchCountPurchaseOrder</li>
 * <li>Method search  : SearchPurchaseOrder</li>
 * <li>Method search  : SearchCountPurchaseOrderHeader</li>
 * <li>Method search  : SearchPurchaseOrderHeader</li>
 * <li>Method search  : SearchCountPurchaseOrderDetail</li>
 * <li>Method search  : SearchPurchaseOrderDetail</li>
 * <li>Method search  : SearchLastUpdateDatetime</li>
 * <li>Method search  : SearchPurchaseOrderByDo</li>
 * <li>Method update  : UpdatePurchaseOrder</li>
 * <li>Method update  : SearchPoFirmWaitToAckForSUser</li>
 * <li>Method update  : SearchPoForcastWaitToAckForSUser</li>
 * <li>Method update  : SearchPoFirmWaitToAckForDUser</li>
 * <li>Method update  : SearchPoForcastWaitToAckForDUser</li>
 * <li>Method update  : SearchPurgingPurchaseOrder</li>
 * <li>Method search  : searchChangePurchaseOrder</li>
 * <li>Method search  : searchCountChangePurchaseOrder</li>
 * <li>Method search : searchCountEffectParts</li>
 * <li>Method update : updateRejectNotReplyDue</li>
 * </ul>
 *
 * @author CSI
 */
public interface PurchaseOrderDao {
    
    /**
     * 
     * <p>Search Purchase Order Report.</p>
     *
     * @param input SpsTPoDomain
     * @return List of Map
     */
    public List<PurchaseOrderReportDomain> searchPurchaseOrderReport ( 
        SpsTPoDomain input); 
    /**
     * 
     * <p>Search Change Material Release Report.</p>
     *
     * @param input P/O Domain
     * @return List of Change Material Release
     */
    public List<ChangeMaterialReleaseReportDomain> searchChangeMaterialReleaseReport ( 
        PurchaseOrderInformationDomain input); 
    
    /**
     * Search count purchase Order.
     * Search count purchase Order by criteria from screen.
     *  
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the Integer
     */
    public Integer searchCountPurchaseOrder(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain);
    
    /**
     * Search purchase Order.
     * Search purchase Order by criteria from screen.
     * 
     * @param purchaseOrderInformationDomain the purchase Order Domain
     * @return the list of purchase Order Information Domain
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain);
    
    /**
     * Search count purchase Order Detail.
     * Search count purchase Order Detail by criteria from screen.
     *  
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain.
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderDetail(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain);

    /**
     * Search count purchase Order Detail that has PN Status PENDING and match to criteria
     * from screen.
     *  
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain.
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderDetailPending(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain);
    
    /**
     * Search purchase Order Detail.
     * Search purchase Order Detail by criteria from screen.
     * 
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain.
     * @return the list of purchase Order Detail Domain.
     */
    public List<PurchaseOrderDetailDomain> searchPurchaseOrderDetail(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain);
    
    /**
     * Search purchase Order Detail.
     * Search purchase Order Detail by criteria from screen.
     * 
     * @param purchaseOrderNotificationDomain the purchase Order Notification Domain.
     * @return the list of purchase Order Notification Detail Domain.
     */
    public List<PurchaseOrderNotificationDomain> searchPurchaseOrderNotification(
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain);
    
    /**
     * Search purchase Order Detail.
     * Search purchase Order Detail by criteria from screen.
     * 
     * @param purchaseOrderDueDomain the purchase order due Domain.
     * @return the list of purchase Order Acknowledge Domain.
     */
    public List<PurchaseOrderDueDomain> searchPurchaseOrderDue(
        PurchaseOrderDueDomain purchaseOrderDueDomain);
    
    /**
     * <p>Search Po Firm Wait To Ack For SUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Forcast Wait To Ack For SUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Firm Wait To Ack For DUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Forcast Wait To Ack For DUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain);
    
    //[IN072]
    
    /**
     * <p>Search Po Forecast Accept For SUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastDensoAcceptForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    
    /**
     * <p>Search Po Forecast Reject For SUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastDensoRejectForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Forecast Pending Accept/Reject For DUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastDensoPendingForDUser(
        TaskListDomain taskListCriteriaDomain);
    
    
    
    /**
     * Search count purchase Order Header.
     * Search count purchase Order Header by criteria from screen.
     *  
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderHeader(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain);
    
    /**
     * Search purchase Order Header.
     * Search purchase Order Header by criteria from screen.
     * 
     * @param purchaseOrderInformationDomain the purchase Order Domain
     * @return the list of purchase Order Information Domain
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrderHeader(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain);
    /**
     * 
     * <p>Search PurchaseOrderByDo.</p>
     *
     * @param criteria PurchaseOrderInformationDomain
     * @return List of PurchaseOrderInformationDomain
     */
    public List<PurchaseOrderInformationDomain> 
    searchPurchaseOrderByDo(PurchaseOrderInformationDomain criteria);
    
    /**
     * 
     * <p>the create P/O and return PO_ID method.</p>
     *
     * @param tpoDomain P/O
     * @return BigDecimal id
     */
    public BigDecimal createPurchaseOrder( SpsTPoDomain tpoDomain );
    
    /**
     * 
     * <p>the search exists method.</p>
     *
     * @param poDomain PurchaseOrderDomain
     * @return List of SpsTPoDomain P/O
     */
    public List<SpsTPoDomain> searchExistPurchaseOrder( PurchaseOrderDomain poDomain );
    
    /**
     * Search Purging Purchase Order.
     * Search Purchase Order for Purging.
     * 
     * @param purgeDataProcess the Purge Data Process Domain
     * @return the list of SPS Transaction PO Domain
     */
    public List<PurgingPoDomain> searchPurgingPurchaseOrder(PurgingPoDomain purgeDataProcess);

    /**
     * Search change purchase Order.
     * Search change purchase Order by criteria from screen.
     * 
     * @param purchaseOrderInformationDomain the purchase Order Domain
     * @return the list of purchase Order Information Domain
     */
    public List<PurchaseOrderInformationDomain> searchChangePurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain);
    
    /**
     * Search count change purchase Order.
     * Search count change purchase Order by criteria from screen.
     *  
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the Integer
     */
    public Integer searchCountChangePurchaseOrder(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain);
    
    /**
     * 
     * <p>Search Purchase Order Kanban Report.</p>
     *
     * @param input SpsTPoDomain
     * @return List of Map
     */
    public List<PurchaseOrderReportDomain> searchPurchaseOrderKanbanReport ( 
        SpsTPoDomain input); 
    
    
    // [IN055] Validate Supplier Information before delete
    /**
     * Search Count Effect Parts
     * Search DENSO Parts that effected to Supplier Information
     * @param poDomain search criteria
     * @return Number of effected parts
     * */
    public Integer searchCountEffectParts(PurchaseOrderDomain poDomain);
    
    // [IN054] Update all Supplier Promised Due that not reply to Reject
    /**
     * Update all Supplier Promised Due that not reply to Reject
     * @param spsTPoDomain P/O
     * @return Integer effect record
     */
    public Integer updateRejectNotReplyDue( SpsTPoDomain spsTPoDomain );
    /**
     * <p>Delete Purging P/O.</p>
     * <ul>
     * <li>Delete P/O data for purging.</li>
     * 
     * @param poId the PoId
     * @return the amount of SPS Transaction P/O Domain
     */
    public int deletePurgingPo(String poId);
    
    /**
     * 
     * <p>Purge data from SPS_T_PO.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deletePurchaseOrder(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria);
    
}