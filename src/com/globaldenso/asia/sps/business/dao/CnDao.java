/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.CnInformationDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;

/**
 * <p>The Interface CN Dao.</p>
 * <p>Service for CN about search CN information.</p>
 * <ul>
 * <li>Method search  : searchPriceDifferenceInformationDetail</li>
 * <li>Method search  : searchCn</li>
 * <li>Method search  : searchCountCn</li>
 * <li>Method search  : createCn</li>
 * <li>Method search  : searchCnCoverPage</li>
 * <li>Method search  : searchCnForTransferToJde</li>
 * </ul>
 *
 * @author CSI
 */
public interface CnDao {
    
    /**
     * search CN
     * search CN by criteria from search.
     * 
     * @param priceDifferenceInformationDomain the Price Difference Information Criteria Domain
     * @return List<PriceDifferenceInformationDomain>
    */
    public List<PriceDifferenceInformationDomain> searchCn(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain);
    
    /**
     * search Count CN
     * search Count CN by criteria from search.
     * 
     * @param priceDifferenceInformationDomain the Price Difference Information Criteria Domain
     * @return Integer
     */
    public Integer searchCountCn(PriceDifferenceInformationDomain priceDifferenceInformationDomain);
    
    /**
     * <p>Create CN data.
     * <ul>
     * <li>Insert the CN data into SPS_T_CN table.</li>
     * </ul>
     * 
     * @param spsTCnDomain the SPS Transaction CN Domain
     * @return the integer
     */
    public Integer createCn(SpsTCnDomain spsTCnDomain);
    
    /**
     * Search credit note cover page.
     * Search credit note data in order to create Invoice Cover Report.
     * 
     * @param cnInformationDomain the CN Information Domain
     * @return the list of CN Information Domain
     */
    public List<CnInformationDomain> searchCnCoverPage(CnInformationDomain cnInformationDomain);
    
    /**
     * Search CN for transfer to JDE.
     * @param spsTCnCriteriaDomain search criteria
     * @return List of CN to transfer to JDE
     * */
    public List<CnInformationDomain> searchCnForTransferToJde(
        SpsTCnCriteriaDomain spsTCnCriteriaDomain);
}