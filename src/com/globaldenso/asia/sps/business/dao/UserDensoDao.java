/*
 * ModifyDate Development company     Describe 
 * 2014/06/03 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;
import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;

/**
 * <p>The Interface DENSO User Dao.</p>
 * <p>Service for DENSO User about manipulate data from criteria.</p>
 * <ul>
 * <li>Method search  : searchCountDensoUser</li>
 * <li>Method search  : searchDensoUser</li>
 * <li>Method search  : searchDensoUserIncludeRole</li>
 * </ul>
 *
 * @author CSI
 */
public interface UserDensoDao {
    
    /**
     * <p>Search count DENSO User.</p>
     * Search count DENSO User by criteria from search.
     *  
     * @param densoUserInfoDomain the criteria for count DENSO user items.
     * @return the number of record count.
     */
    public Integer searchCountUserDenso(DensoUserInformationDomain densoUserInfoDomain);
    
    /**
     * <p>Search count DENSO User detail.</p>
     * Search DENSO User by criteria from search.
     * 
     * @param densoUserInfoDomain the criteria for search DENSO user information.
     * @return the list of DENSO user information.
     */
    public List<DensoUserInformationDomain> searchUserDenso(DensoUserInformationDomain 
        densoUserInfoDomain);
    
    /**
     * <p>Search Count DENSO User Include Role.</p>
     * 
     * @param densoUserInfoDomain the criteria for search DENSO user information with role.
     * @return the list of DENSO user information including role.
     */
    public Integer searchCountUserDensoIncludeRole(DensoUserInformationDomain densoUserInfoDomain);
    
    /**
     * <p>Search DENSO User Include Role.</p>
     * 
     * @param densoUserInfoDomain the criteria for search DENSO user information with role.
     * @return the list of DENSO user information including role.
     */
    public List<DensoUserInformationDomain> searchUserDensoIncludeRole(DensoUserInformationDomain 
        densoUserInfoDomain);
    
}