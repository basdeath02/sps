/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadDomain;

/**
 * <p>The Interface File Upload Dao.</p>
 * <p>Service for File Upload about search file data.</p>
 * <ul> 
 * <li>Method search  : searchFileUploadInformation</li>
 * </ul>
 *
 * @author CSI
 */
public interface FileUploadDao {
    
    /**
     * Search supplier code and supplier plant code for combo box that relate user role.
     * 
     * @param fileUploadDomain the file upload domain
     * @return the list of SpsTFileUploadDomain.
     */
    public List<SpsTFileUploadDomain> searchFileUploadInformation(FileUploadDomain 
        fileUploadDomain);
    
    /**
     * Search count file of supplier code and supplier plant code that relate user role.
     * 
     * @param fileUploadDomain the file upload domain
     * @return integer
     */
    public Integer searchCount (FileUploadDomain fileUploadDomain);
    
}