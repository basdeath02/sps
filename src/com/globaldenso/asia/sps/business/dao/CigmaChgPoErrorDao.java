/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;

/**
 * <p>The Interface CigmaPoErrorDao.</p>
 * <p>For SPS_CIGMA_CHG_PO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferChgPoError</li>
 * </ul>
 *
 * @author CSI
 */
public interface CigmaChgPoErrorDao {
    
    /**
     * Search transfer Change P/O error.
     * 
     * @param spsCigmaChgPoErrorDomain the SPS_CIGMA_CHG_PO_ERROR
     * @return List<TransferPoErrorEmailDomain>
    */
    public List<TransferChgPoErrorEmailDomain> searchTransferChgPoError(
        SpsCigmaChgPoErrorDomain spsCigmaChgPoErrorDomain);
}
