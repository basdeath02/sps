/*
 * ModifyDate Development company    Describe
 * 2014/06/25 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;

/**
 * <p>The Interface ASN Dao.</p>
 * <p>Service for ASN about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchSendingAsn</li>
 * <li>Method update  : updateTransferCIGMAFlag</li>
 * <li>Method search  : searchCountASNInformation</li>
 * <li>Method search  : searchAsnInformationDetail</li>
 * <li>Method search  : searchAsnDetail</li>
 * <li>Method search  : searchSumShippingQty</li>
 * <li>Method search  : searchSumShippingQtyByPart</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnDao {
    
    /**
     * Search Sending ASN.
     * Search Sending ASN data from transfer flag .
     * 
     * @param sendingAsnDomain the sending asn domain
     * @return the asn info domain
     */
    public List<AsnInfoDomain> searchSendingAsn(SendingAsnDomain sendingAsnDomain);
    
    /**
     * Search ASN detail.
     * Search ASN detail information by asn no.
     * 
     * @param asnInformationDomain the ASN Information Criteria Domain
     * @return the list of asn information domain.
     */
    public List<AsnInformationDomain> searchAsnDetail (AsnInformationDomain asnInformationDomain);
    
    /**
     * Search ASN Progress Information detail.
     * Search ASN Progress information detail by criteria from search.
     * 
     * @param asnProgressInformationDomain the ASN Progress Information Criteria Domain
     * @return the list of ASN Progress Information Domain
     */
    public List<AsnProgressInformationReturnDomain> searchAsnInformation(
        AsnProgressInformationDomain asnProgressInformationDomain);
    
    /**
     * 
     * <p>Search Count ASN Progress Information detail.</p>
     *
     * @param asnProgressInformationDomain the ASN Progress Information Domain
     * @return count
     */
    public int searchCountAsnInformation(
        AsnProgressInformationDomain asnProgressInformationDomain);
    
    /**
     * 
     * <p>Search exist ASN.</p>
     *
     * @param asnDomain the asn domain
     * @return the list of asn domain.
     */
    public List<AsnDomain> searchExistAsn(AsnDomain asnDomain);
    
    /**
     * 
     * <p>Search ASN for group invoice.</p>
     *
     * @param asnDomain the asn domain
     * @return the list of asn domain.
     */
    public List<AsnDomain> searchAsnForGroupInvoice(AsnDomain asnDomain);
    
    /**
     * Search ASN Information to generate Advanced Ship Notice Report.
     * @param spsTAsnDomain domain for SPS_T_ASN
     * @return List of ASN Info Domain
     * */
    public List<AsnInfoDomain> searchAsnReport(SpsTAsnDomain spsTAsnDomain);
    
    /**
     * Search purging asn order.
     * Search asn order for purging.
     * 
     * @param purgingDoDomain the Purging DO domain
     * @return the list of Purging DO domain
     */
    public List<PurgingAsnDomain> searchPurgingAsnOrder(PurgingDoDomain purgingDoDomain);

    // [IN012] For check delete P/O, D/O
    /**
     * Search count not purging asn order.
     * Search number of ASN that not have to purge.
     * 
     * @param purgingDoDomain the Purging DO domain
     * @return number of ASN that not have to purge
     */
    public Integer searchCountNotPurgingAsn(PurgingDoDomain purgingDoDomain);
    
    /**
     * Search sum shipping qty.
     * Search sum shipping quantity of each asn no.
     * 
     * @param spsTAsnDetailDomain the Sps T Asn Detail Domain
     * @return the list of Sps T Asn Detail Domain
     */
    public List<SpsTAsnDetailDomain> searchSumShippingQty(SpsTAsnDetailDomain spsTAsnDetailDomain);
    
    /**
     * Search sum shipping qty by part.
     * 
     * @param spsTAsnDetailDomain the SpsTAsnDetailDomain
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain searchSumShippingQtyByPart(
        SpsTAsnDetailDomain spsTAsnDetailDomain);
    /**
     * <p>Delete Purging ASN.</p>
     * <ul>
     * <li>Delete ASN data for purging.</li>
     * 
     * @param asnNo the asnNo
     * @return the amount of SPS Transaction ASN Domain
     */
    public int deletePurgingAsn(String asnNo);
}