/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain;

/**
 * <p>The Interface User Role Dao.</p>
 * <p>Service for Supplier User Role about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchUserRoleByDscId</li>
 * <li>Method delete  : deleteUserRoleByDscId</li>
 * </ul>
 *
 * @author CSI
 */
public interface UserRoleDao {
     
    /**
     * <p>Search user role information by DSC ID.</p>
     * 
     * @param userRoleDetailDomain the User role detail domain that keep criteria for search.
     * @return the list of User Role detail Domain.
     */
    public List<UserRoleDetailDomain> searchUserRoleByDscId(UserRoleDetailDomain 
        userRoleDetailDomain);
    
    /**
     * <p>Delete user role information by DSC ID.</p>
     * 
     * @param userRoleCriteriaDomain the User role detail domain that keep criteria for delete.
     * @return the count of delete record.
     */
    public int deleteUserRoleByDscId(SpsMUserRoleCriteriaDomain
        userRoleCriteriaDomain);
    
    /**
     * Search all Role Type for current login user.
     * @param userRoleScreen : search criteria (DSC_ID, current datetime)
     * @return list of Role Type name
     * */
    public List<SpsMRoleTypeDomain> searchUserRoleType(UserRoleScreenDomain userRoleScreen);
    
}