/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.business.domain.UserMenuDomain;

/**
 * <p>The Interface MenuDao.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchMenuByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public interface MenuDao {

    /**
     * Search Menu by User Permission from SPS_M_ROLE_USER_DENSO, SPS_M_ROLE_MENU and SPS_M_MENU.
     * @param userMenuDomain menu search criteria
     * @return list of SpsMMenuDomain
     * */
    public List<SpsMMenuDomain> searchMenuByUserPermission(UserMenuDomain userMenuDomain);
    
}
