/*
 * ModifyDate Development company     Describe 
 * 2014/06/06 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;


import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;

/**
 * <p>The Interface User Dao.</p>
 * <p>Service for Supplier User about search data from criteria.</p>
 * <ul>
 * <li>Method update  : UpdateUser</li>
 * <li>Method delete  : DeleteUser</li>
 * <li>Method create  : CreateUser</li>
 * <li>Method search  : searchEmailUserDenso</li>
 * </ul>
 *
 * @author CSI
 */
public interface UserDao {
    
 
    /**
     * <p>search department name</p>
     * <ul>
     * <li>search distinct all of department name.</li>
     * </ul>
     * 
     * @return the list of String.
     */
    public List<SpsMUserDomain> searchDepartmentName();
    
    /**
     * Search Email User DENSO.
     * 
     * @param userDensoDomain the user DENSO domain
     * @return the list of user domain
     */
    public List<SpsMUserDomain> searchEmailUserDenso (SpsMUserDensoDomain userDensoDomain);
    
}