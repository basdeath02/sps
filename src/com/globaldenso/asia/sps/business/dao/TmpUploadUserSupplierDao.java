/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import com.globaldenso.asia.sps.business.domain.TmpUserSupplierDomain;

/**
 * <p>The Interface Temporary User Supplier Dao.</p>
 * <p>Dao for Temporary User Supplier about search from CSV file.</p>
 * <ul>
 * <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpUploadUserSupplierDao {
    
 
    /**
     * <p>Search Temporary Upload Supplier One Record.</p>
     * 
     * @param tmpUserSupplierDomain that contain upload supplier user information.
     * @return the Supplier user information from upload.
     */
    public TmpUserSupplierDomain searchTmpUploadSupplierOneRecord(TmpUserSupplierDomain 
       tmpUserSupplierDomain);

}