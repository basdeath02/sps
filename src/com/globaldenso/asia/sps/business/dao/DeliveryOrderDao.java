/*
 * ModifyDate Development company    Describe 
 * 2014/07/22 CSI Parichat           Create
 * 2015/08/24 CSI Akat               [IN012]
 * 2015/08/22 Netband U.Rungsiwut     SPS phase II
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.math.BigDecimal;
import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DoCreatedAsnDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanDeliveryOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;

/**
 * <p>The Interface DeliveryOrderDao.</p>
 * <p>Service for delivery order about search data from criteria.</p>
 * <ul>
 * <li>Method search  : SearchCountAcknowledgedDo</li>
 * <li>Method search  : SearchAcknowledgedDo</li>
 * <li>Method search  : SearchCountDeliveryOrder</li>
 * <li>Method search  : SearchDeliveryOrder</li>
 * <li>Method search  : searchCountDoDetail</li>
 * <li>Method search  : SearchDoDetail</li>
 * <li>Method search  : SearchCountKanbanOrder</li>
 * <li>Method search  : SearchKanbanOrder</li>
 * <li>Method search  : SearchDoGroupAsn</li>
 * <li>Method search  : SearchCountKanbanOrderHeader</li>
 * <li>Method search  : SearchKanbanOrderHeader</li>
 * <li>Method search  : SearchCountDeliveryOrderHeader</li>
 * <li>Method search  : SearchDeliveryOrderHeader</li>
 * <li>Method search  : SearchUrgentOrderForSupplierUser</li>
 * <li>Method search  : SearchExistDo</li>
 * <li>Method search  : SearchDeliveryOrderReport</li>
 * <li>Method search  : SearchKanbanDeliveryOrderReport</li>
 * <li>Method search  : searchPurgingUrgentDeliveryOrder</li>
 * <li>Method search  : searchOneWayKanbanTagReportInformation</li>
 * </ul>
 * 
 * @author CSI
 */
public interface DeliveryOrderDao {
    
    /**
     * Search count acknowledged do information detail.
     * <p>Search count acknowledged do information by criteria from search.</p>
     *  
     * @param acknowledgedDoInformationDomain the acknowledged do information domain
     * @return the integer
     */
    public Integer searchCountAcknowledgedDo(AcknowledgedDoInformationDomain
        acknowledgedDoInformationDomain);
    
    /**
     * Search acknowledged do information detail.
     * <p>Search acknowledged do information by criteria from search.</p>
     * 
     * @param acknowledgedDoInformationDomain the acknowledged do information domain
     * @return the list
     */
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDoInformationDomain);
    
    /**
     * <p>Search Count Delivery Order.</p>
     *
     * @param deliveryOrderInformationDomain the delivery order information domain
     * @return the Integer
     */
    public Integer searchCountDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Delivery Order.</p>
     *
     * @param deliveryOrderInformationDomain the delivery order information domain
     * @return the list
     */
    public List<DeliveryOrderDetailDomain> searchDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /** Search Count Do Detail.
     * <p>Search Count Do Detail by criteria from other screens (WSHP001, WSHP004, WSHP008).</p>
     *
     * @param inquiryDoDetailDomain the inquiry do detail domain
     * @return the integer
     */
    public Integer searchCountDoDetail(InquiryDoDetailDomain inquiryDoDetailDomain);
    
    /** Search Do Detail.
     * <p>Search Do Detail by criteria from other screens (WSHP001, WSHP004, WSHP008).</p>
     *
     * @param inquiryDoDetailDomain the inquiry do detail domain
     * @return the list
     */
    public List<InquiryDoDetailReturnDomain> searchDoDetail(
        InquiryDoDetailDomain inquiryDoDetailDomain);
    
    /**
     * <p>
     * search Count KANBAN Order method.
     * </p>
     * 
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return Integer
     */
    public Integer searchCountKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);

    /**
     * <p>
     * search KANBAN Order method.
     * </p>
     * 
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return List
     */
    public List<KanbanOrderInformationResultDomain> searchKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);

    
    /**
     * Search do group asn information.
     * <p>Search delivery order data for grouping asn.</p>
     *  
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the list of asn maintenance return domain
     */
    public List<AsnMaintenanceReturnDomain> searchDoGroupAsn(
        AsnMaintenanceDomain asnMaintenanceDomain);
    
    /**
     * Search completed ship do.
     * <p>Search completed ship delivery order information.</p>
     *  
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the list of asn maintenance return domain
     */
    public List<AsnMaintenanceReturnDomain> searchCompleteShipDo(
        AsnMaintenanceDomain asnMaintenanceDomain);
    
    // [IN012] : search distinct P/N Shipment Status from D/O
    /**
     * Search distinct P/N Shipment Status from D/O
     * 
     * @param doDomain criteria
     * @return list of distinct P/N Shipment Status
     * */
    public List<String> searchDistinctPnStatus(SpsTDoDomain doDomain);
    
    /**
     * <p>Search Count Kanban Order Header method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return Integer
     */
    public Integer searchCountKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);
    
    /**
     * <p>Search Kanban Order Header method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return List
     */
    public List<KanbanOrderInformationResultDomain> searchKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);
    
    /**
     * <p>Search Count Delivery Order Header method.</p>
     *
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return Integer
     */
    public Integer searchCountDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Delivery Order Header method.</p>
     *
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return List
     */
    public List<DeliveryOrderDetailDomain> searchDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Urgent Order For Supplier User method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchUrgentOrderForSupplierUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Exist Do.</p>
     *
     * @param deliveryOrderInformationDomain the delivery order information domain
     * @return List
     */
    public List<DeliveryOrderDetailDomain> searchExistDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Delivery Order Report method.</p>
     *
     * @param deliveryOrderSubDetailDomain deliveryOrderSubDetailDomain
     * @return List
     */
    public List<DeliveryOrderReportDomain> searchDeliveryOrderReport(
        DeliveryOrderSubDetailDomain deliveryOrderSubDetailDomain);
    
    /**
     * <p>Search Kanban Delivery Order Report method.</p>
     *
     * @param kanbanOrderInformationDODetailReturnDomain kanbanOrderInformationDODetailReturnDomain
     * @return List
     */
    public List<KanbanDeliveryOrderReportDomain> searchKanbanDeliveryOrderReport(
        KanbanOrderInformationDoDetailReturnDomain kanbanOrderInformationDODetailReturnDomain);
    
    /**
     * Search count back order information.
     * Search count back order information by criteria from search.
     * 
     * @param backOrderInformationDomain the back order information domain
     * @return the integer
     */
    public Integer searchCountBackOrder(BackOrderInformationDomain backOrderInformationDomain);
    
    /**
     * Search back order information detail.
     * Search back order information detail by criteria from search.
     * 
     * @param backOrderInformationDomain the back order information domain
     * @return the list of file upload domain
     */
    public List<BackOrderInformationDomain> searchBackOrder(
        BackOrderInformationDomain backOrderInformationDomain);
    
    /**
     * 
     * <p>Create Delivery Order.</p>
     *
     * @param domain SpsTDoDomain
     * @return BigDecimal doId
     * @throws ApplicationException other error
     */
    public BigDecimal createDeliveryOrder(SpsTDoDomain domain) throws ApplicationException;
    
    /**
     * 
     * <p>searchExistDeliveryOrder.</p>
     *
     * @param parameter DeliveryOrderDomain
     * @return List of SpsTDoDomain
     */
    public List<SpsTDoDomain> searchExistDeliveryOrder(DeliveryOrderDomain parameter);
    /**
     * 
     * <p>searchUrgentOrder method.</p>
     *
     * @param criteria UrgentOrderDomain
     * @return List of DeliveryOrderDomain
     */
    public List<DeliveryOrderDomain> searchUrgentOrder(UrgentOrderDomain criteria);
    /**
     * <p>Search ASN from DO.</p>
     *
     * @param criteria DeliveryOrderDomain
     * @return List of SpsTAsnDomain
     */
    public List<DoCreatedAsnDomain> searchDoCreatedAsn(DeliveryOrderDomain criteria);
    
    /**
     * Search purging delivery order.
     * Search delivery order for purging.
     * 
     * @param purgingPoDomain the Purging PO domain
     * @return the list of Purging DO domain
     */
    public List<PurgingDoDomain> searchPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain);

    // [IN012] for check delete P/O
    /**
     * <p>Search count not purging delivery order.</p>
     * <ul>
     * <li>Search count number of Delivery Order that not have to purge.</li>
     * 
     * 
     * @param purgingPoDomain the Purging PO domain
     * @return number of Delivery Order that not have to purge
     */
    public Integer searchCountNotPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain);
    
    /**
     * Search purging urgent delivery order.
     * Search purging urgent delivery order by criteria from batch.
     * 
     * @param purgingPoDomain the Purging PO domain
     * @return the list of Purging DO domain
     */
    public List<PurgingDoDomain> searchPurgingUrgentDeliveryOrder(PurgingPoDomain purgingPoDomain);
    
    /**
     * Search delivery order acknowledgement.
     * <p>Search P/O status of SPS D/O no.</p>
     * 
     * @param deliveryOrderInformationDomain the deliveryOrderInformation domain
     * @return the SPS master PO domain
     */
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search count not purging delivery order.</p>
     * <ul>
     * <li>Search count number of Delivery Order that not have to purge.</li>
     * 
     * 
     * @param deliveryOrderInformationDomain the deliveryOrderInformationDomain
     * @return number of resultCode
     */
    public Integer updateDateTimeKanbanPdf(DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * Search One Way Kanban Tag Report Information.
     * <p>Search One Way Kanban Tag Report Information</p>
     * 
     * @param deliveryOrderInformationDomain the DeliveryOrderInformationDomain
     * @return OneWayKanbanTagReportDomain
     */
    public List<OneWayKanbanTagReportDomain> searchOneWayKanbanTagReportInformation(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);

    /**
     * search Notify Delivery Order To Supplier.
     * <p>search Notify Delivery Order To Supplier</p>
     * 
     * @param spsTDoDomain the SpsTDoDomain domain
     * @return the List of SpsTDoDomain domain
     */
    public List<SpsTDoDomain> searchNotifyDeliveryOrderToSupplier(SpsTDoDomain spsTDoDomain);

    /**
     * 
     * <p>createKanbanTag.</p>
     *
     * @param domain KanbanTagDomain
     * @return BigDecimal doId
     * @throws ApplicationException other error
     */
    public BigDecimal createKanbanTag(KanbanTagDomain domain) throws ApplicationException;
    /**
     * 
     * <p>updateOneWayKanbanTagFlag.</p>
     *
     * @param domain KanbanTagDomain
     * @return Integer
     * @throws ApplicationException other error
     */
    public Integer updateOneWayKanbanTagFlag(KanbanTagDomain domain) throws ApplicationException;
    /**
     * 
     * <p>Search Batch Number.</p>
     *
     * @return Integer
     * @throws ApplicationException other error
     */
    public Integer searchBatchNumber() throws ApplicationException;
    /**
     * 
     * <p>Create SPS_T_BHT_TRANSMIT_LOG .</p>
     *
     * @param receiveByScanDomain ReceiveByScanDomain
     * @return BigDecimal doId
     * @throws ApplicationException other error
     */
    public BigDecimal createTransmitLog(ReceiveByScanDomain receiveByScanDomain) throws ApplicationException;
    /**
     * 
     * <p>Check the message information is exist in SPS_T_BHT_TRANSMIT_LOG table.</p>
     *
     * @param receiveByScanDomain ReceiveByScanDomain
     * @return List of ReceiveByScanDomain
     * @throws ApplicationException other error
     */
    public List<ReceiveByScanDomain> isExistTransmitLog(ReceiveByScanDomain receiveByScanDomain) throws ApplicationException;
    /**
     * <p>Delete Purging D/O.</p>
     * <ul>
     * <li>Delete D/O data for purging.</li>
     * 
     * @param doId the doId
     * @return the amount of SPS Transaction Invoice Domain
     */
    public int deletePurgingDo(String doId);
    
    /**
     * 
     * <p>searchExistDeliveryOrderBackOrder.</p>
     *
     * @param parameter DeliveryOrderDomain
     * @return List of SpsTDoDomain
     */
    public List<SpsTDoDomain> searchExistDeliveryOrderBackOrder(DeliveryOrderDomain parameter);
    
    /**
     * 
     * <p>searchExistDeliveryOrderDetailBackOrder.</p>
     *
     * @param parameter DeliveryOrderDomain
     * @return List of SpsTDoDetailDomain
     */
    public List<SpsTDoDetailDomain> searchExistDeliveryOrderBackOrderDetail(DeliveryOrderDomain parameter);
    
    
    public Integer updateBackOrderDetail(SpsTDoDetailDomain parameters);
    
    public Integer updateBackOrderHeader(SpsTDoDomain parameters);
    
    /**
     * 
     * <p>searchMailBackOrder method.</p>
     *
     * @param criteria SpsTDoDetailDomain
     * @return List of DeliveryOrderDomain
     */
    public List<DeliveryOrderDomain> searchMailBackOrder(SpsTDoDetailDomain criteria);
    
    /**
     * 
     * <p>Purge data from SPS_T_BHT_TRANSMIT_LOG.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deleteBhtTransmitLog(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria);
    
    /**
     * 
     * <p>Purge data from SPS_T_DO.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deleteDeliveryOrder(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria);
    
    //public boolean createKanbanTagList(List<KanbanTagDomain> kanbanList) throws ApplicationException;
    public Integer insertKanbanTag(KanbanTagDomain domain) throws ApplicationException;
}
