/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceCoverPageDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;

/**
 * <p>The Interface Invoice Dao.</p>
 * <p>Service for invoice about search and update invoice status.</p>
 * <ul>
 * <li>Method search  : searchCountInvoiceInformation</li>
 * <li>Method search  : searchInvoiceInformationDetail</li>
 * <li>Method update  : updateInvoiceStatus</li>
 * </ul>
 *
 * @author CSI
 */
public interface InvoiceDao {
    
    /**
     * Search invoice information detail.
     * Search invoice information detail by criteria from search.
     * 
     * @param invoiceInformationDomain the Invoice Information Domain
     * @return the list of Invoice Information Domain
     */
    public List<InvoiceInformationDomain> searchInvoiceInformation(InvoiceInformationDomain 
        invoiceInformationDomain);
    
    /**
     * Search count invoice information
     * Search count invoice information by criteria from search.
     * 
     * @param invoiceInformationDomain the Invoice Information Domain
     * @return Integer
     */
    public Integer searchCountInvoiceInformation(InvoiceInformationDomain invoiceInformationDomain);
    
    /**
     * Create invoice.
     * Create invoice invoice data.
     * 
     * @param spsTInvoiceDomain the SPS Table Invoice Domain
     * @return the integer
     */
    public Integer createInvoice(SpsTInvoiceDomain spsTInvoiceDomain);
    
    /**
     * Search invoice cover page.
     * Search invoice cover page data in order to create report.
     * 
     * @param invoiceCoverPageDomain the Invoice cover page Domain
     * @return the list of Invoice Information Domain
     */
    public List<InvoiceCoverPageDomain> searchInvoiceCoverPage(InvoiceCoverPageDomain 
        invoiceCoverPageDomain);
    
    // [IN012] ASN can create by many Invoice (in case cancel invoice)
    ///**
    // * Search invoice by ASN.
    // * Search invoice data by ASN No.
    // * 
    // * @param purgingAsnDomain the Purging ASN Domain
    // * @return the SPS Transaction Invoice Domain
    // */
    //public SpsTInvoiceDomain searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain);
    /**
     * Search invoice by ASN.
     * Search invoice data by ASN No.
     * 
     * @param purgingAsnDomain the Purging ASN Domain
     * @return the SPS Transaction Invoice Domain
     */
    public List<SpsTInvoiceDomain> searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain);
    
    /**
     * Search Purging Invoice.
     * Search invoice data for purging.
     * 
     * @param purgingPoDomain the Purging PO Domain
     * @return the list of SPS Transaction Invoice Domain
     */
    public List<SpsTInvoiceDomain> searchPurgingInvoice(PurgingPoDomain purgingPoDomain);
    
    /**
     * Search Invoice for Transfer to JDE.
     * @param spsTInvoiceCriteriaDomain search criteria
     * @return List of Invoice to check for transfer.
     * */
    public List<InvoiceInformationDomain> searchInvoiceForTransferToJde(
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain);
    

    
    /**
     * <p>Delete Purging Invoice.</p>
     * <ul>
     * <li>Delete invoice data for purging.</li>
     * 
     * @param invoiceId the invoiceId
     * @return the amount of SPS Transaction Invoice Domain
     */
    public int deletePurgingInvoice(String invoiceId);
    
    /**
     * 
     * <p>Purge data from SPS_T_INVOICE.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deleteInvoice(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria);
    
}