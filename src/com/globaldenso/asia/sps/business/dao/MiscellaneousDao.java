/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;

/**
 * <p>The Interface Misc Dao.</p>
 * <p>Service for misc about search misc data.</p>
 * <ul>
 * <li>Method search  : searchMiscValue</li>
 * <li>Method search  : searchMisc</li>
 * </ul>
 *
 * @author CSI
 */
public interface MiscellaneousDao {
    
    /**
     * Search misc value.
     * Search misc value by misc type and misc code.
     * 
     * @param miscDomain the misc domain
     * @return the integer
     */
    public Integer searchMiscValue(MiscellaneousDomain miscDomain);
    
    /**
     * Search misc value for combo box.
     * Search misc value by misc type.
     * 
     * @param miscDomain the misc domain
     * @return the list of MiscDomain.
     */
    public List<MiscellaneousDomain> searchMisc(MiscellaneousDomain miscDomain);
    
}