/*
 * ModifyDate Development company     Describe 
 * 2014/05/28 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;

/**
 * <p>The Interface  User Supplier Dao.</p>
 * <p>Service for Supplier User about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchCountSupplierUser</li>
 * <li>Method search  : searchSupplierUser</li>
 * <li>Method search  : searchUserEmailBySupplierCode</li>
 * <li>Method search  : searchUserSupplierIncludeRole</li>
 * <li>Method search  : searchEmailUserSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public interface UserSupplierDao {
    
    /**
     * Search count Supplier User item detail.
     * Search count Supplier User by criteria from search.
     *  
     * @param supplierUserInfoDomain the Supplier User Info domain
     * @return the Integer
     */
    public Integer searchCountUserSupplier(SupplierUserInformationDomain supplierUserInfoDomain);
    
    /**
     * Search Supplier User item detail.
     * Search Supplier User by criteria from search.
     * 
     * @param supplierUserInfoDomain that keep criteria from search.
     * @return the List of supplier user information.
     */
    public List<SupplierUserInformationDomain> searchUserSupplier(SupplierUserInformationDomain 
        supplierUserInfoDomain);
    
    /**
     * Search User Supplier Include Role.
     * Search User Supplier Include Role by criteria for export CSV file.
     * 
     * @param supplierUserInfoDomain that keep criteria of searching.
     * @return the List of supplier user information including role.
     */
    public List<SupplierUserInformationDomain> searchUserSupplierIncludeRole(
        SupplierUserInformationDomain supplierUserInfoDomain);
    
    /**
     * <p>Search Supplier User Email By Not Found Flag</p>
     * <ul>
     * <li>Search Supplier User by criteria.</li>
     * </ul>
     * 
     * @param supplierUserDetailDomain the It keep criteria information to search.
     * @return SupplierUserDetailDomain that keep list of supplier user information.
     */
    public List<SupplierUserInformationDomain> searchUserSupplierEmailByNotFoundFlag(
        UserSupplierDetailDomain supplierUserDetailDomain);
    
    /**
     * <p>Search Supplier User Email By Urgent Flag</p>
     * <ul>
     * <li>Search Supplier User by criteria.</li>
     * </ul>
     * 
     * @param supplierUserDetailDomain the It keep criteria information to search.
     * @return SupplierUserDetailDomain that keep list of supplier user information.
     */
    public List<SupplierUserInformationDomain> searchUserSupplierEmailByUrgentFlag(
        UserSupplierDetailDomain supplierUserDetailDomain);
    
    /**
    * Search list of supplier user's email by supplier code.
    * 
    * @param supplierUserDomain the Supplier User domain
    * @return the lit
    */
    public List<String> searchUserEmailBySupplierCode(UserSupplierDomain supplierUserDomain);
    
    /**
     * Search Email User Supplier.
     * Search Email User Supplier by criteria.
     * 
     * @param userSupplierDetailDomain the user supplier detail domain
     * @return the list of spsMUserDomain
     */
    public List<SpsMUserDomain> searchEmailUserSupplier(
        UserSupplierDetailDomain userSupplierDetailDomain);
    
    /**
     * <p>Search Supplier User Email By Urgent Flag For Backorder</p>
     * <ul>
     * <li>Search Supplier User by criteria.</li>
     * </ul>
     * 
     * @param supplierUserDetailDomain the It keep criteria information to search.
     * @return SupplierUserDetailDomain that keep list of supplier user information.
     */
    public List<SupplierUserInformationDomain> searchUserSupplierEmailByUrgentFlagForBackorder(
        UserSupplierDetailDomain supplierUserDetailDomain);
}