/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The interface CompanySupplierDao.</p>
 * <p>DAO for Company Supplier.</p>
 * <ul>
 * <li>Method search  : searchCompanySupplierByRole</li>
 * <li>Method search  : searchCompanySupplierByRelation</li>
 * <li>Method search  : searchCompanySupplierNameByRelation</li>
 * <li>Method search  : seachCompanySupplierByCode</li>
 * <li>Method search  : searchExistCompanySupplierInformation</li>
 * </ul>
 *
 * @author CSI
 */
public interface CompanySupplierDao {
    
    /**
     * Search Supplier company by user role.
     * 
     * @param plantSupplierWithScope plant supplier information with data scope control
     * @return List of Company Supplier
     * */
    public List<CompanySupplierDomain> searchCompanySupplierByRole
        (PlantSupplierWithScopeDomain plantSupplierWithScope);
    
    /**
     * Search Supplier company by user Relation.
     * 
     * @param plantSupplierWithScope plant supplier information with data scope control
     * @return List of Company Supplier
     * */
    public List<CompanySupplierDomain> searchCompanySupplierByRelation
        (PlantSupplierWithScopeDomain plantSupplierWithScope);
    
    /**
     * Search company supplier name by Relation.
     * 
     * @param companySupplierWithScopeDomain company Supplier with scope
     * @return List of Company Supplier
     * */
    public List<CompanySupplierDomain> searchCompanySupplierNameByRelation
        (CompanySupplierWithScopeDomain companySupplierWithScopeDomain);
    
    /**
     * Search Supplier information by Supplier company code.
     * 
     * @param companySupplierCriteria the Company Supplier Domain
     * @return the Company Supplier Domain
     * */
    public CompanySupplierDomain searchCompanySupplierByCode(
        CompanySupplierDomain companySupplierCriteria);
    
    /**
     * Search Exist Company Supplier Information.
     * 
     * @param companySupplierDomain the Company Supplier Domain
     * @return the Company Supplier Domain
     * */
    public Integer searchExistCompanySupplierInformation(
        CompanySupplierDomain companySupplierDomain);
    
    /**
     * Search Supplier Exist company by user Relation.
     * 
     * @param companySupplierWithScopeDomain Supplier company with scope
     * @return count of company supplier.
     * */
    public Integer searchExistCompanySupplierByRelation
        (CompanySupplierWithScopeDomain companySupplierWithScopeDomain);
    
    /**
     * Search Supplier Exist company by user Relation.
     * 
     * @param companySupplierWithScopeDomain Supplier company with scope
     * @return count of company supplier.
     * */
    public Integer searchExistCompanySupplierByRole
        (CompanySupplierWithScopeDomain companySupplierWithScopeDomain);
}
