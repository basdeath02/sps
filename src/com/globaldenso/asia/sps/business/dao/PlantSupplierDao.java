/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The interface PlantSupplierDao.</p>
 * <p>DAO for Plant Supplier.</p>
 * <ul>
 * <li>Method search  : searchSupplierPlantByRole</li>
 * <li>Method search  : searchSupplierPlantByRelation</li>
 * </ul>
 *
 * @author CSI
 */
public interface PlantSupplierDao {
    
    /**
     * Search Plant Supplier by user role.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return List of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplierByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Plant Supplier relate with user role.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return List of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplierByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Plant Supplier Information by user role.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return List of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplierInformationByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Plant Supplier Information by relation.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return List of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplierInformationByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Exist Plant Supplier relate with user role.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return count of plant supplier.
     * */
    public Integer searchExistPlantSupplierByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Exist Plant Supplier relate with user role.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return count of plant supplier.
     * */
    public Integer searchExistPlantSupplierByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
}
