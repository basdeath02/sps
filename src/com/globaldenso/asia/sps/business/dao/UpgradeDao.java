/*
 * ModifyDate Development company     Describe 
 * 2017/09/26 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import com.globaldenso.asia.sps.business.domain.BhtUpgradeDomain;

/**
 * <p>The Interface UResourceServices.</p>
 * <p>Service for Upgrade Resource about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchBhtUpgrade</li>
 * </ul>
 *
 * @author Netband
 */
public interface UpgradeDao {
    
    /**
     * searchBhtUpgrade.
     * 
     * @param bhtUpgradeDomain the BhtUpgradeDomain
     * @return the BhtUpgradeDomain
     */
    public BhtUpgradeDomain searchBhtUpgrade(BhtUpgradeDomain bhtUpgradeDomain);
}