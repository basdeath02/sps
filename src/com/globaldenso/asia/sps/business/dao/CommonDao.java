/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;


import java.sql.Timestamp;

import com.globaldenso.asia.sps.business.domain.CommonDomain;

/**
 * <p>The Common Dao.</p>
 * <p>Service for common function.</p>
 * <ul>
 * <li>Method search  : searchSysDate</li>
 * </ul>
 *
 * @author CSI
 */
public interface CommonDao {
 
 
    /**
     * <p>Search system date.</p>
     * <ul>
     * <li>
     * Search the current date from server.(DB Server)
     * </li>
     * </ul>
     *  
     * @return the Timestamp
     */
    public Timestamp searchSysDate();
    
    /**
     * <p>Search system date.</p>
     * <ul>
     * <li>
     * Search the current date from server.(DB Server)
     * </li>
     * </ul>
     *  
     * @param commonDomain the Common domain
     * @return the Timestamp
     */
    public Timestamp searchSysDate(CommonDomain commonDomain);
}