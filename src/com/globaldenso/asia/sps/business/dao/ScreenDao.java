/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain;

/**
 * <p>The Interface ScreenDao.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchScreenByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public interface ScreenDao {

    /**
     * Search for screen and Plant that user has permission (user is DENSO).
     * @param roleUserScreenDomain Role User Screen domain
     * @return List of screen and plant
     * */
    public List<RoleScreenDomain> searchScreenByUserPermission(
        UserRoleScreenDomain roleUserScreenDomain);
}
