/*
 * ModifyDate Development company     Describe 
 * 2014/04/24 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface CompanyDensoDao.</p>
 * <p>Provide operator about Company DENSO.</p>
 * <ul>
 * <li>Method search  : searchCompanyDensoDetail</li>
 * <li>Method search  : searchDENSOCompanyByRole</li>
 * <li>Method search  : searchDENSOCompanyByRelation</li>
 * <li>Method search  : searchAS400ServerConnection</li>
 * <li>Method search  : searchDensoCompanyByCodeList</li>
 * </ul>
 *
 * @author CSI
 */
public interface CompanyDensoDao {
    
    /**
     * <p>Search detail of company DENSO.</p>
     * <p>Search detail of company DENSO data result</p>
     * <ul>
     * <li>Get detail of company DENSO.</li>
     * </ul>
     *  
     * @return the list
     */
    public List<CompanyDensoDomain> searchCompanyDensoDetail();
    
    /**
     * Search DENSO company by user role.
     * 
     * @param plantDensoWithScope plant information with data scope control
     * @return List of DENSO company
     * */
    public List<CompanyDensoDomain> searchCompanyDensoByRole
        (PlantDensoWithScopeDomain plantDensoWithScope);
    
    /**
     * Search DENSO company by Supplier company that user has role.
     * 
     * @param plantDensoWithScope plant information with data scope control
     * @return List of DENSO company
     * */
    public List<CompanyDensoDomain> searchCompanyDensoByRelation
        (PlantDensoWithScopeDomain plantDensoWithScope);
    
    /**
     * Search AS400 Service Connection.
     * 
     * @param companyDensoDomain the company DENSO domain
     * @return List of AS400 Server Connection Information domain
     * */
    public  List<As400ServerConnectionInformationDomain> searchAs400ServerConnection(
        SpsMCompanyDensoDomain companyDensoDomain);
    
    /**
     * Search DENSO Company By Code List.
     * 
     * @param companyDensoDomain the SPS master company DENSO domain
     * @return the list of SPS master company DENSO domain
     * */
    public  List<SpsMCompanyDensoDomain> searchCompanyDensoByCodeList(
        SpsMCompanyDensoDomain companyDensoDomain);
    
    /**
     * Search Supplier Exist company by user Relation.
     * 
     * @param companyDensoWithScopeDomain Supplier company with scope
     * @return count of company supplier.
     * */
    public Integer searchExistCompanyDensoByRelation
        (CompanyDensoWithScopeDomain companyDensoWithScopeDomain);
    
    /**
     * Search Supplier Exist company by user Relation.
     * 
     * @param companyDensoWithScopeDomain Supplier company with scope
     * @return count of company supplier.
     * */
    public Integer searchExistCompanyDensoByRole
        (CompanyDensoWithScopeDomain companyDensoWithScopeDomain);
}