/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The interface PlantDensoDao.</p>
 * <p>DAO for Plant DENSO.</p>
 * <ul>
 * <li>Method search  : searchDensoPlantByRole</li>
 * <li>Method search  : searchDensoPlantByRelation</li>
 * </ul>
 *
 * @author CSI
 */
public interface PlantDensoDao {

    /**
     * Search Plant DENSO by user role.
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return List of Plant DENSO
     * */
    public List<PlantDensoDomain> searchPlantDensoByRole(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search Plant DENSO relate with user role.
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return List of Plant DENSO
     * */
    public List<PlantDensoDomain> searchPlantDensoByRelation(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search Plant DENSO Information by user role.
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return List of Plant DENSO
     * */
    public List<PlantDensoDomain> searchPlantDensoInformationByRole(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search Plant DENSO Information relate with user role.
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return List of Plant DENSO
     * */
    public List<PlantDensoDomain> searchPlantDensoInformationByRelation(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search Exist Plant DENSO  relate with user role.
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return count of Plant DENSO
     * */
    public Integer searchExistPlantDensoByRelation(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search Exist Plant DENSO  relate with user role.
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return count of Plant DENSO
     * */
    public Integer searchExistPlantDensoByRole(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
}
