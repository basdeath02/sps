/*
 * ModifyDate Development company     Describe 
 * 2014/08/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain;

/**
 * <p>The Interface Temporary Supplier Info Dao.</p>
 * <p>Dao for Temporary Supplier Info about search from CSV file.</p>
 * <ul>
 * <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpSupplierInfoDao {
    
 
    /**
     * <p>Search Temporary Upload Supplier Info One Record.</p>
     * 
     * @param tmpSupplierInfoDomain that contain upload supplier information.
     * @return the Supplier information from upload.
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoOneRecord(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain);
    
    /**
     * <p>Search Temporary Upload Supplier Info Company name.</p>
     * 
     * @param tmpSupplierInfoDomain that contain upload supplier information.
     * @return the Supplier information from upload.
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoCompanyName(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain);
    
    /**
     * <p>Search Temporary Upload Supplier Info One Plant.</p>
     * 
     * @param tmpSupplierInfoDomain that contain upload supplier information.
     * @return the list of plant supplier domain.
     */
    public List<PlantSupplierDomain> searchTmpUploadSupplierInfoPlant(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain);

}