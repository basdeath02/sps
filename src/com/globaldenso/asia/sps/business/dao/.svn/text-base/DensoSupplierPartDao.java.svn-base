/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.dao;


import java.util.List;

import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain;

/**
 * <p>The Interface DENSO Supplier Part Dao.</p>
 * <p>Service for DENSO Supplier Part about search.</p>
 * <ul>
 * <li>Method search  : searchDensoSupplierPart</li>
 * <li>Method search  : searchDensoSupplierPartByRole</li>
 * <li>Method search  : searchDensoSupplierPartByRelation</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoSupplierPartDao {
    
 
    /**
     * Search DENSO Supplier Part.
     * 
     * @param densoSupplierPartDomain the It keep a criteria to search DENSO supplier part.
     * @return the DensoSupplierPartDomain.
     */
    public DensoSupplierPartDomain searchDensoSupplierPart(DensoSupplierPartDomain 
        densoSupplierPartDomain);
    
    /**
     * Search DENSO Supplier Part By Role.
     * 
     * @param dataScopeControlDomain the It keep a criteria to search DENSO Supplier Part By Role.
     * @return the DensoSupplierPartDomain.
     */
    public List<DensoSupplierPartDomain> searchDensoSupplierPartByRole(DataScopeControlDomain 
        dataScopeControlDomain);
      
    /**
     * Search DENSO Supplier Part By Relation.
     * 
     * @param dataScopeControlDomain the It keep a criteria to search DENSO Supplier Part 
     * By Relation.
     * @return the list of DensoSupplierPartDomain.
     */
    public List<DensoSupplierPartDomain> searchDensoSupplierPartByRelation(DataScopeControlDomain 
        dataScopeControlDomain );
    
}