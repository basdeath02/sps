/*
 * ModifyDate Development company     Describe 
 * 2014/08/29 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CnInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

import java.sql.Timestamp;

/**
 * <p>The Interface TransferInvoiceToJdeFacadeService.</p>
 * <p>Facade for batch process BINV001.</p>
 * 
 * @author CSI
 */
public class TransferInvoiceToJdeFacadeServiceImpl implements TransferInvoiceToJdeFacadeService {

    /** The Service for Company DENSO information. */
    private CompanyDensoService companyDensoService;
    
    /** The Invoice service. */
    private InvoiceService invoiceService;
    
    /** The CN Service. */
    private CnService cnService;
    
    /** The service for SPS_T_INVOICE. */
    private SpsTInvoiceService spsTInvoiceService;
    
    /** The default constructor. */
    public TransferInvoiceToJdeFacadeServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for companyDensoService.</p>
     *
     * @param companyDensoService Set for companyDensoService
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * <p>Setter method for invoiceService.</p>
     *
     * @param invoiceService Set for invoiceService
     */
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    /**
     * <p>Setter method for cnService.</p>
     *
     * @param cnService Set for cnService
     */
    public void setCnService(CnService cnService) {
        this.cnService = cnService;
    }

    /**
     * <p>Setter method for spsTInvoiceService.</p>
     *
     * @param spsTInvoiceService Set for spsTInvoiceService
     */
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService) {
        this.spsTInvoiceService = spsTInvoiceService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferInvoiceToJdeFacadeService#searchAs400ServerList(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain companyDensoDomain) throws ApplicationException
    {
        List<As400ServerConnectionInformationDomain> serverList = null;
        serverList = this.companyDensoService.searchAs400ServerList(companyDensoDomain);
        return serverList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferInvoiceToJdeFacadeService#searchInvoiceForTransferToJde(com.globaldenso.asia.sps.auto.business.domain.InvoiceInformationDomain)
     */
    public List<InvoiceInformationDomain> searchInvoiceForTransferToJde(
        InvoiceInformationDomain invoiceInformationDomain)
    {
        List<InvoiceInformationDomain> result = null;
        
        SpsTInvoiceCriteriaDomain criteria = new SpsTInvoiceCriteriaDomain();
        criteria.setDCd(invoiceInformationDomain.getDCd());
        criteria.setTransferJdeFlag(Constants.STR_ZERO);
        
        StringBuffer statusBuffer = new StringBuffer();
        statusBuffer.append(Constants.SYMBOL_SINGLE_QUOTE)
            .append(Constants.INVOICE_STATUS_ISS)
            .append(Constants.SYMBOL_SINGLE_QUOTE)
            .append(Constants.SYMBOL_COMMA)
            .append(Constants.SYMBOL_SINGLE_QUOTE)
            .append(Constants.INVOICE_STATUS_ISC)
            .append(Constants.SYMBOL_SINGLE_QUOTE);
        criteria.setInvoiceStatus(statusBuffer.toString());
        
        result = this.invoiceService.searchInvoiceForTransferToJde(criteria);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferInvoiceToJdeFacadeService#searchAsnReceiving(java.lang.String,com.globaldenso.asia.sps.auto.business.domain.As400ServerConnectionInformationDomain, java.util.Locale)
     */
    public List<PseudoCigmaAsnDomain> searchAsnReceiving(String asnNoIn,
        As400ServerConnectionInformationDomain as400Server, Locale locale)
        throws ApplicationException
    {
        List<PseudoCigmaAsnDomain> asnReceivingList = null;
        asnReceivingList = CommonWebServiceUtil.asnResourceSearchAsnReceiving(
            asnNoIn, Constants.EMPTY_STRING, as400Server, locale);
        return asnReceivingList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferInvoiceToJdeFacadeService#transactTransferDatatoJde(java.match.BigDecimal,java.lang.String,com.globaldenso.asia.sps.auto.business.domain.As400ServerConnectionInformationDomain,com.globaldenso.asia.sps.auto.business.domain.InvoiceInformationDomain, java.lang.String)
     */
    public void transactTransferDatatoJde(BigDecimal invoiceId, String asnNoIn,
        Timestamp systemDatetime, As400ServerConnectionInformationDomain as400Server,
        InvoiceInformationDomain invoiceInformationDomain, String batchJobId)
        throws ApplicationException
    {
        String label = null;
        Locale locale = invoiceInformationDomain.getLocale();
        
        // 1. Get CN Information from CnService
        SpsTCnCriteriaDomain cnCriteria = new SpsTCnCriteriaDomain();
        cnCriteria.setInvoiceId(invoiceId);
        cnCriteria.setCnStatus(Constants.ISS_STATUS);
        List<CnInformationDomain> cnList = this.cnService.searchCnForTransferToJde(cnCriteria);
        
        // 2. Update Transfer JDE Flag in invoice header from SpsTInvoiceService
        SpsTInvoiceDomain invoiceValue = new SpsTInvoiceDomain();
        invoiceValue.setTransferJdeFlag(Constants.STR_ONE);
        invoiceValue.setInvoiceStatus(Constants.INVOICE_STATUS_RAP);
        invoiceValue.setLastUpdateDscId(batchJobId);
        invoiceValue.setLastUpdateDatetime(systemDatetime);
        SpsTInvoiceCriteriaDomain invoiceCriteria = new SpsTInvoiceCriteriaDomain();
        invoiceCriteria.setInvoiceId(invoiceId);
        invoiceCriteria.setLastUpdateDatetime(
            invoiceInformationDomain.getSpsTInvoiceDomain().getLastUpdateDatetime());
        int effectRecord = this.spsTInvoiceService.updateByCondition(invoiceValue, invoiceCriteria);
        if (Constants.ONE != effectRecord) {
            label = MessageUtil.getLabelHandledException(locale,
                SupplierPortalConstant.LBL_DATA_ALREADY_USED);
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0045,
                null, new String[]{invoiceId.toString(), label});
        }
        
        // 3. Call service InvoiceResource to transfer Invoice Data to CIGMA AS/400 Server
        int recordCount = Constants.ZERO;
        recordCount = CommonWebServiceUtil.apInterfaceResourceTransactTransferInvoice(invoiceId,
            asnNoIn, batchJobId, systemDatetime, as400Server, invoiceInformationDomain, cnList);
        if (Constants.ZERO == recordCount) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0009,
                null, new String[]{invoiceId.toString()});
        }
    }
    
}
