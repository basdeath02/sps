/*
 * ModifyDate Development company     Describe 
 * 2014/08/29 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;

/**
 * <p>The Interface TransferInvoiceToJdeFacadeService.</p>
 * <p>Facade for batch process BINV001.</p>
 * 
 * <ul>
 * <li>Method search : searchAS400ServerList</li>
 * <li>Method search : searchInvoiceForTransferToJde</li>
 * <li>Method search : searchAsnReceiving</li>
 * <li>Method insert/update : transactTransferDatatoJde</li>
 * </ul>
 * 
 * @author CSI
 */
public interface TransferInvoiceToJdeFacadeService {
    
    /**
     * Search AS400 service list.
     * 
     * @param companyDensoDomain the common denso domain
     * @return List of AS400 server connection information domain
     * @throws ApplicationException ApplicationException
     * */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain companyDensoDomain) throws ApplicationException;
    
    /**
     * Search Invoice for Transfer to JDE.
     * @param invoiceInformationDomain criteria domain
     * @return List of Invoice to check for transfer.
     * */
    public List<InvoiceInformationDomain> searchInvoiceForTransferToJde(
        InvoiceInformationDomain invoiceInformationDomain);
    
    /**
     * Call Web Service for get ASN Receiving.
     * @param asnNoIn ASN Number to get (many ASN Number comma separate).
     * @param as400Server AS400 server information
     * @param locale for get Error Message
     * @return List of ASN from CIGMA
     * @throws ApplicationException an ApplicationException
     * */
    public List<PseudoCigmaAsnDomain> searchAsnReceiving(String asnNoIn,
        As400ServerConnectionInformationDomain as400Server, Locale locale)
        throws ApplicationException;
    
    /**
     * Transaction transfer data to JDE.
     * <ul>
     * <li>Get CN information by invoiceId and cnStatus is Issued</li>
     * <li>Update Invoice's Transfer to JDE flag</li>
     * <li>Transfer data to JDE</li>
     * </ul>
     * @param invoiceId current transaction invoice ID
     * @param asnNoIn ASN Number to get (many ASN Number comma separate).
     * @param systemDatetime update datetime for batch running current round
     * @param as400Server AS400 server information
     * @param invoiceInformationDomain invoice to transfer
     * @param batchJobId Job ID from AIJM.
     * @throws ApplicationException an ApplicationException
     * */
    public void transactTransferDatatoJde(BigDecimal invoiceId, String asnNoIn,
        Timestamp systemDatetime, As400ServerConnectionInformationDomain as400Server,
        InvoiceInformationDomain invoiceInformationDomain, String batchJobId)
        throws ApplicationException;
    
}
