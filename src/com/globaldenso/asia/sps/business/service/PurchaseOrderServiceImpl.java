/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2015/08/05 CSI Akat                [IN009]
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/02/24 CSI Akat                [IN054]
 * 2018/06/25 CTC U. Rungsiwut        Fix issue for firm type 'D' and 'M' it is not start on Monday.
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JasperPrint;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.business.dao.PurchaseOrderDao;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDueDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportRegionDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.THREE;
import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;
import static com.globaldenso.asia.sps.common.constant.Constants.FIVE;
import static com.globaldenso.asia.sps.common.constant.Constants.SIX;
import static com.globaldenso.asia.sps.common.constant.Constants.SEVEN;
import static com.globaldenso.asia.sps.common.constant.Constants.EIGHT;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ZERO;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;

//import static com.globaldenso.asia.sps.common.utils.MessageUtil.getLabel;

/**
 * <p>The Class Purchase Order Information Facade Service Implement.</p>
 * <p>Manage data of Supplier User.</p>
 * <ul>
 * <li>Method search : searchCountPurchaseOrder</li>
 * <li>Method search : searchPurchaseOrder</li>
 * <li>Method search : searchCountPurchaseOrderHeader</li>
 * <li>Method search : searchPurchaseOrderHeader</li>
 * <li>Method search : searchCountPurchaseOrderDetail</li>
 * <li>Method search : searchPurchaseOrderDetail</li>
 * <li>Method search : searchPurchaseOrderNotification</li>
 * <li>Method search : searchPurchaseOrderDue</li>
 * <li>Method update : searchPoFirmWaitToAckForSUser</li>
 * <li>Method update : searchPoForcastWaitToAckForSUser</li>
 * <li>Method update : searchPoFirmWaitToAckForDUser</li>
 * <li>Method update : searchPoForcastWaitToAckForDUser</li>
 * <li>Method update : searchPurgingPurchaseOrder</li>
 * <li>Method search  : searchChangePurchaseOrder</li>
 * <li>Method search  : searchCountChangePurchaseOrder</li>
 * <li>Method search : searchCountEffectParts</li>
 * <li>Method update : updateRejectNotReplyDue</li>
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderServiceImpl extends AbstractReportServicesImpl
    implements PurchaseOrderService {
    
    

    /** The purchase Order Dao */
    private PurchaseOrderDao purchaseOrderDao;
    
    /**
     * Instantiates a new Supplier User service impl.
     */
    public PurchaseOrderServiceImpl(){
        super();
    }
    
    /**
     * Sets the purchase Order Dao.
     * 
     * @param purchaseOrderDao the new purchase Order Dao.
     */
    public void setPurchaseOrderDao(
        PurchaseOrderDao purchaseOrderDao) {
        this.purchaseOrderDao = purchaseOrderDao;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain)
     */
    public List<PurchaseOrderReportDomain> searchPurchaseOrderReport ( 
        SpsTPoDomain input){
        return purchaseOrderDao.searchPurchaseOrderReport(input);
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchChangeMaterialReleaseReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<ChangeMaterialReleaseReportDomain> searchChangeMaterialReleaseReport ( 
        PurchaseOrderInformationDomain input) {
        return purchaseOrderDao.searchChangeMaterialReleaseReport(input);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#generatePo(java.util.List, java.util.List)
     */
    public InputStream generatePo(Locale locale,
        List<PurchaseOrderCoverPageReportDomain> coverList,
        List<PurchaseOrderReportDomain> reportList) throws Exception {
        
        // Generate P/O Cover Page
        Map<String, Object> parameters = new HashMap<String, Object>();
        
        int iTotal = reportList.size();
        String strTotal = EMPTY_STRING;
        if( iTotal % TWO == ZERO ){
            strTotal = StringUtil.appendsString( Constants.SYMBOL_SLASH, 
                String.valueOf( ( iTotal / TWO ) + ONE ) );
        } else {
            strTotal = StringUtil.appendsString( Constants.SYMBOL_SLASH, 
                String.valueOf( (iTotal / TWO) + TWO ) );
        }
        String strCoverPage = StringUtil.appendsString( Constants.STR_ONE, strTotal );
        for( PurchaseOrderCoverPageReportDomain coverDomain : coverList ){
            coverDomain.setIssueDate( 
                DateUtil.format(coverDomain.getPoIssueDate(), PATTERN_YYYYMMDD_SLASH) );
            coverDomain.setDeliveryOrderDueDate(
                DateUtil.format(coverDomain.getDeliveryOrderDate(), PATTERN_YYYYMMDD_SLASH) );
            coverDomain.setPageNo( strCoverPage );
            
            if(Constants.STR_ONE.equals(coverDomain.getShowDensoPlantFlg())){
                String dPcd = StringUtil.appendsString(
                    Constants.SYMBOL_SUBTRACT, coverDomain.getDPcd());
                coverDomain.setDPcd(dPcd);
            }else{
                coverDomain.setDPcd(Constants.EMPTY_STRING);
            }
        }
        
        Date forceAckDate = coverList.get(Constants.ZERO).getForceAckDate();
        parameters.put("acknowledgeDays", DateUtil.format(forceAckDate, PATTERN_YYYYMMDD_SLASH));
        
//        System.out.println( new java.io.File(".").getAbsolutePath() );
//        System.out.println( "Report Path : " + getReportPathFixed(ZERO) );
        JasperPrint jpPoCover = this.generateReport(getReportPathFixed(ZERO),
            coverList , parameters);
        
        JasperPrint jpPoTermsAndCondition = null;
        String poTermsAndConditionFlag = coverList.get(Constants.ZERO).getPoTermsAndConditionFlg();
        if(!STR_ZERO.equals(poTermsAndConditionFlag)){
            String filePath = StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.PO_TERMS_AND_CONDITION_REPORT, poTermsAndConditionFlag,
                Constants.SYMBOL_DOT, Constants.REPORT_JASPER_FORMAT);
            jpPoTermsAndCondition = this.generateReport(filePath, coverList, parameters);
        }
        
        // Generate P/O
        // setting label
        parameters = new HashMap<String, Object>();
        String controlDPcdFlag = coverList.get(Constants.ZERO).getShowDensoPlantFlg();
        List<PurchaseOrderReportDomain> poList
            = this.convertPoReport(reportList, strTotal, controlDPcdFlag);
        
        JasperPrint jpPo = null;
        if( ZERO == poList.size() ){
            List<PurchaseOrderReportDomain> emptyList = new ArrayList<PurchaseOrderReportDomain>();
            emptyList.add(new PurchaseOrderReportDomain());
            jpPo = generateReport(
                getReportPathFixed(TWO), 
                emptyList,
                parameters);
        } else {
            jpPo = generateReport(
                getReportPathFixed(ONE), 
                poList, 
                parameters);
        }
        
        // Generate PDF.
        List<JasperPrint> jpList = new ArrayList<JasperPrint>();
        jpList.add(jpPoCover);
        if(null != jpPoTermsAndCondition){
            jpList.add(jpPoTermsAndCondition);
        }
        jpList.add(jpPo);
        return generate(jpList);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#generateChgPo(java.util.List)
     */
    public InputStream generateChgPo(Locale locale,
        List<ChangeMaterialReleaseReportDomain> reportList) throws Exception {
        Map<String, Object> parameters = new HashMap<String, Object>();
        for(ChangeMaterialReleaseReportDomain reportDomain : reportList ){
            reportDomain.setEta( DateUtil.format(reportDomain.getEtaDate(),
                PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtd( DateUtil.format(reportDomain.getEtdDate(), 
                PATTERN_YYYYMMDD_SLASH));
            reportDomain.setIssueDate( DateUtil.format(reportDomain.getPoIssueDate(), 
                PATTERN_YYYYMMDD_SLASH));
            reportDomain.setDueDateFrom( DateUtil.format(reportDomain.getDueDateFromDate(), 
                PATTERN_YYYYMMDD_SLASH));
            reportDomain.setDueDateTo( DateUtil.format(reportDomain.getDueDateToDate(), 
                PATTERN_YYYYMMDD_SLASH));
            reportDomain.setOrderType( 
                SupplierPortalConstant.PO_ORDER_TYPE.get(reportDomain.getOrderMethod()) );
            reportDomain.setCd( 
                SupplierPortalConstant.PO_PERIOD_TYPE.get(reportDomain.getPeriodType()) );
            reportDomain.setReportType( 
                SupplierPortalConstant.PO_DWM.get(reportDomain.getReportType()) );
        }
        
        JasperPrint jpChgPo = generateReport(getReportPathFixed(THREE), 
            reportList , parameters);
        
        return generate(jpChgPo);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        List<PurchaseOrderInformationDomain> result = 
            (List<PurchaseOrderInformationDomain>)purchaseOrderDao
            .searchPurchaseOrder(purchaseOrderInformationDomain);
        
        this.formatPurchaseOrderForCsv(result);
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        Integer count = purchaseOrderDao.searchCountPurchaseOrder(
            purchaseOrderInformationDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderDetail(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public List<PurchaseOrderDetailDomain> searchPurchaseOrderDetail(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain)
    {
        List<PurchaseOrderDetailDomain> result = 
            (List<PurchaseOrderDetailDomain>)purchaseOrderDao
            .searchPurchaseOrderDetail(purchaseOrderAcknowledgementDomain);
        
        // FIX : Start : wrong dateformat
        for (PurchaseOrderDetailDomain poDetail : result) {
            try {
                if (null != poDetail.getStartPeriodDate()) {
                    poDetail.setStartPeriodDateShow(DateUtil.format(
                        poDetail.getStartPeriodDate(), PATTERN_YYYYMMDD_SLASH));
                }
                
                if (null != poDetail.getEndPeriodDate()) {
                    poDetail.setEndPeriodDateShow(DateUtil.format(
                        poDetail.getEndPeriodDate(), PATTERN_YYYYMMDD_SLASH));
                }
            } catch (Exception e) {
                continue;
            }
        }
        // FIX : End : wrong dateformat
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderDetail(com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain)
     */
    public List<PurchaseOrderNotificationDomain> searchPurchaseOrderNotification(
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain)
    {
        List<PurchaseOrderNotificationDomain> result = 
            (List<PurchaseOrderNotificationDomain>)purchaseOrderDao
            .searchPurchaseOrderNotification(purchaseOrderNotificationDomain);

        // [IN009] Rounding Mode use HALF_UP
        //DecimalFormat decimalFormat = new DecimalFormat(Constants.NO_DECIMAL_NUMBER_FORMAT);
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)defaultNumberFormat;
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setMinimumFractionDigits(Constants.ZERO);
        decimalFormat.setMaximumFractionDigits(Constants.TWO);
        
        for (PurchaseOrderNotificationDomain notification : result) {
            try {
                if (null != notification.getSpsTPoDue().getDueDate()) {
                    notification.setStringDueDate(DateUtil.format(
                        notification.getSpsTPoDue().getDueDate(), PATTERN_YYYYMMDD_SLASH));
                }
                if (null != notification.getSpsTPoDue().getSpsProposedDueDate()) {
                    notification.setStringProposedDueDate(DateUtil.format(
                        notification.getSpsTPoDue().getSpsProposedDueDate(), PATTERN_YYYYMMDD_SLASH));
                }
                if (null != notification.getSpsTPo().getPoIssueDate()) {
                    notification.setStringPoIssueDate(DateUtil.format(
                        notification.getSpsTPo().getPoIssueDate(), PATTERN_YYYYMMDD_SLASH));
                }
                if (null != notification.getSpsTPoDue().getEtd()) {
                    notification.setStringEtd(DateUtil.format(notification.getSpsTPoDue().getEtd(),
                        PATTERN_YYYYMMDD_SLASH));
                }
                if (null != notification.getSpsTPoDue().getOrderQty()) {
                    notification.setStringOrderQty(decimalFormat.format(
                        notification.getSpsTPoDue().getOrderQty()));
                }
                if (null != notification.getSpsTPoDue().getSpsProposedQty()) {
                    notification.setStringProposedQty(decimalFormat.format(
                        notification.getSpsTPoDue().getSpsProposedQty()));
                }
            } catch (Exception e) {
                continue;
            }
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderDetail(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public List<PurchaseOrderDueDomain> searchPurchaseOrderDue(
        PurchaseOrderDueDomain purchaseOrderDueDomain)
    {
        List<PurchaseOrderDueDomain> result = 
            (List<PurchaseOrderDueDomain>)purchaseOrderDao
            .searchPurchaseOrderDue(purchaseOrderDueDomain);
        
        // FIX : Start : wrong dateformat
        for (PurchaseOrderDueDomain poDue : result) {
            try {
                if (null != poDue.getSpsTPoDueDomain().getEtd()) {
                    poDue.setEtdShow(DateUtil.format(
                        poDue.getSpsTPoDueDomain().getEtd(), PATTERN_YYYYMMDD_SLASH));
                }
            } catch (Exception e) {
                continue;
            }
        }
        // FIX : End : wrong dateformat
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountPurchaseOrderInformation(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public Integer searchCountPurchaseOrderDetail(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain)
    {
        Integer count = purchaseOrderDao.searchCountPurchaseOrderDetail(
            purchaseOrderAcknowledgementDomain);
        return count;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountPurchaseOrderDetailPending(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public Integer searchCountPurchaseOrderDetailPending(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain)
    {
        Integer count = purchaseOrderDao.searchCountPurchaseOrderDetailPending(
            purchaseOrderAcknowledgementDomain);
        return count;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPoFirmWaitToAckForSUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoFirmWaitToAckForSUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPoForcastWaitToAckForSUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoForcastWaitToAckForSUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }
    
    // [IN072] Count Purchase Order Forecast : Denso Accept/Reject for Suppiler User
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderService#searchPoForcastDensoAcceptForSUser(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public List<MainScreenResultDomain> searchPoForcastDensoAcceptForSUser(
        TaskListDomain taskListCriteriaDomain) 
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoForcastDensoAcceptForSUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderService#searchPoForcastDensoRejectForSUser(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public List<MainScreenResultDomain> searchPoForcastDensoRejectForSUser(
        TaskListDomain taskListCriteriaDomain) 
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoForcastDensoRejectForSUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }
    
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderService#searchPoForcastDensoPendingForDUser(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public List<MainScreenResultDomain> searchPoForcastDensoPendingForDUser(
        TaskListDomain taskListCriteriaDomain) 
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoForcastDensoPendingForDUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPoFirmWaitToAckForDUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoFirmWaitToAckForDUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPoForcastWaitToAckForDUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain
            = purchaseOrderDao.searchPoForcastWaitToAckForDUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderHeader(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrderHeader(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        List<PurchaseOrderInformationDomain> result = 
            (List<PurchaseOrderInformationDomain>)purchaseOrderDao
            .searchPurchaseOrderHeader(purchaseOrderInformationDomain);
        
        // FIX : wrong dateformat
        for (PurchaseOrderInformationDomain poInfo : result) {
            try {
                if (null != poInfo.getSpsTPoDomain().getPoIssueDate()) {
                    poInfo.setPoIssuedDateShow(DateUtil.format(
                        poInfo.getSpsTPoDomain().getPoIssueDate(), PATTERN_YYYYMMDD_SLASH));
                }
            } catch (Exception e) {
                continue;
            }
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountPurchaseOrderHeader(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrderHeader(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        Integer count = purchaseOrderDao.searchCountPurchaseOrderHeader(
            purchaseOrderInformationDomain);
        return count;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderByDo(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrderByDo(
        PurchaseOrderInformationDomain criteria)
    {
        return purchaseOrderDao.searchPurchaseOrderByDo(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#createPurchaseOrder(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public BigDecimal createPurchaseOrder(SpsTPoDomain tpoDomain) {
        return purchaseOrderDao.createPurchaseOrder(tpoDomain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchExistPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain)
     */
    public SpsTPoDomain searchExistPurchaseOrder(PurchaseOrderDomain poDomain) {
        List<SpsTPoDomain> resultList = purchaseOrderDao.searchExistPurchaseOrder(poDomain);
        if( null != resultList && 0 < resultList.size() ){
            return resultList.get(0);
        }else {
            return null;
        }
    }
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurgingPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurgingPODomain)
     */
    public List<PurgingPoDomain> searchPurgingPurchaseOrder(PurgingPoDomain purgingPODomain) {
        List<PurgingPoDomain> purgingPOList = null;
        purgingPOList = (List<PurgingPoDomain>)this.purchaseOrderDao.searchPurgingPurchaseOrder(
            purgingPODomain);
        return purgingPOList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchChangePurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<PurchaseOrderInformationDomain> searchChangePurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        List<PurchaseOrderInformationDomain> result = 
            (List<PurchaseOrderInformationDomain>)purchaseOrderDao
            .searchChangePurchaseOrder(purchaseOrderInformationDomain);
        
        this.formatPurchaseOrderForCsv(result);
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountChangePurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountChangePurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        Integer count = purchaseOrderDao.searchCountChangePurchaseOrder(
            purchaseOrderInformationDomain);
        return count;
    }
    
//    /**
//     * 
//     * <p>Calculate Diff Day dat.</p>
//     *
//     * @param d1 date
//     * @param d2 date
//     * @return diff
//     */
//    private int diffDay(Date d1, Date d2){
//        if( null == d1 || null == d2 ){
//            return ZERO;
//        }
//        return (int)( ( d1.getTime() / Constants.DAY_MILLIS ) 
//            - ( d2.getTime() / Constants.DAY_MILLIS ) );
//    }
    
    /**
     * 
     * <p>Fill Label.</p>
     *
     * @param src PurchaseOrderReportDomain
     * @param dest PurchaseOrderReportDomain
     * @param reportDPcdFlag flag to control DENSO Plant Code
     */
    private void fillLabel(PurchaseOrderReportDomain src, PurchaseOrderReportDomain dest,
        String reportDPcdFlag)
    {
        dest.setPoIssueDate( 
            DateUtil.format( src.getIssueDate() , PATTERN_YYYYMMDD_SLASH) );
        dest.setSCd( src.getSCd() );
        dest.setSupplierName( src.getSupplierName() );
        dest.setSPcd( src.getSPcd() );
        dest.setTransportMode( src.getTransportMode() );
        dest.setIssueDateFrom( 
            DateUtil.format( src.getIssueDateFromDate() , PATTERN_YYYYMMDD_SLASH) );
        dest.setReleaseNo( src.getReleaseNo() );
        //dest.setSpsPoNo( src.getSpsPoNo() );
        //dest.setCigmaPoNo( src.getCigmaPoNo() );
        dest.setSpsPoNo( StringUtil.appendsString(src.getSPcd().trim(), src.getRefCigmaPoNo()) );
        dest.setCigmaPoNo( src.getRefCigmaPoNo() );
        dest.setIssueDateTo( 
            DateUtil.format( src.getIssueDateToDate() , PATTERN_YYYYMMDD_SLASH) );
        dest.setDPn( src.getDPn() );
        dest.setSPn( src.getSPn() );
        dest.setItemDescription( src.getItemDescription() );
        dest.setPlannerCode( src.getPlannerCode() );
        dest.setReceivingDock( src.getReceivingDock() );
        dest.setQtyBox( src.getQtyBox() );
        dest.setUnitOfMeasure( src.getUnitOfMeasure() );
        dest.setVariableQtyCode( src.getVariableQtyCode() );
        dest.setPhaseInOutCode( src.getPhaseInOutCode() );
        dest.setUp( 
            StringUtil.toCurrencyFormat( src.getUnitPrice().toString() ) );
        dest.setCurrencyCode( src.getCurrencyCode() );
        dest.setOrderLot( src.getOrderLot() );
        dest.setOrderMethod( 
            SupplierPortalConstant.PO_ORDER_TYPE.get(src.getOrderMethodDb()) );
        
        dest.setDCd( src.getDCd() );
        dest.setCompanyDensoName( src.getCompanyDensoName() );
        
        if(Constants.STR_ONE.equals( reportDPcdFlag )){
            dest.setDPcd( StringUtil.appendsString(Constants.SYMBOL_SUBTRACT, src.getDPcd()) );
        }else{
            dest.setDPcd( Constants.EMPTY_STRING );
        }
        
        dest.setMon( EMPTY_STRING );
        dest.setTue( EMPTY_STRING );
        dest.setWed( EMPTY_STRING );
        dest.setThu( EMPTY_STRING );
        dest.setFri( EMPTY_STRING );
        dest.setSat( EMPTY_STRING );
        dest.setSun( EMPTY_STRING );
        
        dest.setMonDate( EMPTY_STRING );
        dest.setTueDate( EMPTY_STRING );
        dest.setWedDate( EMPTY_STRING );
        dest.setThuDate( EMPTY_STRING );
        dest.setFriDate( EMPTY_STRING );
        dest.setSatDate( EMPTY_STRING );
        dest.setSunDate( EMPTY_STRING );
        
    }
    
    /**
     * 
     * <p>Convert P/O Report.</p>
     *
     * @param reportList List of PurchaseOrderReportDomain
     * @param strTotal strTotal
     * @param controlDPcdFlag control DENSO Plant Code flag
     * @return List of PurchaseOrderReportDomain
     */
    private List<PurchaseOrderReportDomain> convertPoReport(
        List<PurchaseOrderReportDomain> reportList, String strTotal, String controlDPcdFlag)
    {
//        return reportList;
        List<PurchaseOrderReportDomain> resultList = new ArrayList<PurchaseOrderReportDomain>();

        if ( ZERO < reportList.size() ) {
            PurchaseOrderReportDomain [] region2 = new PurchaseOrderReportDomain[SIX];
            PurchaseOrderReportDomain [] region3 = new PurchaseOrderReportDomain[FOUR];
            
            String strPgNo = null;
//            boolean bNotValue = true;
            int item = ONE;
            for(int iRecord = ZERO, length = reportList.size(); 
                iRecord < length; 
                iRecord++ , ++item )
            {
                PurchaseOrderReportDomain currentPart = reportList.get(iRecord);
                Calendar endFirm = null;
                if (null != currentPart.getEndFirmDate()) {
                    endFirm = Calendar.getInstance();
                    endFirm.setTimeInMillis(currentPart.getEndFirmDate().getTime());
                }
                
                if( Math.abs(item) % TWO == ONE ){
                    strPgNo = String.valueOf( (item / TWO) + TWO );
                } else {
                    strPgNo = String.valueOf( (item / TWO) + ONE );
                }
                
                strPgNo = StringUtil.appendsString( strPgNo, strTotal );
                
                // Set page header value to all record
                for (int j = ZERO; j < SIX; ++j) {
                    region2[j] = new PurchaseOrderReportDomain();
                    region2[j].setPageNo( strPgNo );
                    region2[j].setRegion( Constants.STR_TWO );
                    fillLabel(reportList.get(iRecord), region2[j], controlDPcdFlag);
                    resultList.add(region2[j]);
                }
                for (int j = ZERO; j < FOUR; ++j) {
                    region3[j] = new PurchaseOrderReportDomain();
                    region3[j].setPageNo( strPgNo );
                    region3[j].setRegion( Constants.STR_THREE );
                    fillLabel(reportList.get(iRecord), region3[j], controlDPcdFlag);
                    resultList.add(region3[j]);
                }
                
                List<PurchaseOrderReportRegionDomain> reportRegionList
                    = reportList.get(iRecord).getPoReportRegionList();

                List<PurchaseOrderReportDueDomain> dueDList
                    = new ArrayList<PurchaseOrderReportDueDomain>();
                List<PurchaseOrderReportDueDomain> dueWList
                    = new ArrayList<PurchaseOrderReportDueDomain>();
                List<PurchaseOrderReportDueDomain> dueMList
                    = new ArrayList<PurchaseOrderReportDueDomain>();
                
                for (int iRegion = ZERO; iRegion < reportRegionList.size(); iRegion++) {
                    if (STR_ZERO.equals(reportRegionList.get(iRegion).getReportTypeFlg())) {
                        dueDList = reportRegionList.get(iRegion).getPoDueList();
                    } else if (Constants.STR_ONE.equals(
                        reportRegionList.get(iRegion).getReportTypeFlg()))
                    {
                        dueWList = reportRegionList.get(iRegion).getPoDueList();
                    } else if (Constants.STR_TWO.equals(
                        reportRegionList.get(iRegion).getReportTypeFlg()))
                    {
                        dueMList = reportRegionList.get(iRegion).getPoDueList();
                    }
                }

                // Fill data Report type D : 42 records (from P/O DueDate From)
                Set<String> cdSet = new HashSet<String>();
                int dueDIndex = ZERO;
                Calendar periodDate = Calendar.getInstance();
                periodDate.setTimeInMillis(
                    reportList.get(iRecord).getIssueDateFromDate().getTime());
                // Additional for fixed case DM firm it is not start on Monday.
                int dayOfWeek1 = periodDate.get(Calendar.DAY_OF_WEEK);
                if(dayOfWeek1 == ONE){
                    periodDate.add(Calendar.DATE, ONE);                    
                } else if(dayOfWeek1 > TWO){
                    periodDate.add(Calendar.DATE, Constants.MINUS_ONE * (dayOfWeek1 - TWO));
                }
                // End additional code
                for(int iW = ZERO; iW < SIX; iW++) {
                    java.util.Date currentEtd = new java.util.Date(periodDate.getTimeInMillis());
                    
                    // Set DWM
                    if (Constants.DPO_TYPE.equals(currentPart.getPoType())
                        || Constants.DKO_TYPE.equals(currentPart.getPoType()))
                    {
                        if (ZERO == iW) {
                            region2[iW].setDwm( Constants.STR_D );
                        } else if (null == endFirm) {
                            region2[iW].setDwm( Constants.STR_W );
                        } else {
                            Calendar firstDayOfWeek = Calendar.getInstance();
                            firstDayOfWeek.setTimeInMillis(periodDate.getTimeInMillis());
                            // End Firm before start this week
                            if (ZERO < firstDayOfWeek.compareTo(endFirm)) {
                                region2[iW].setDwm( Constants.STR_W );
                            } else {
                                // End Firm after start this week
                                region2[iW].setDwm( Constants.STR_D );
                            }
                        }
                    } else {
                        region2[iW].setDwm( Constants.STR_D );
                    }
                    
                    region2[iW].setDate(DateUtil.format(currentEtd
                        , DateUtil.PATTERN_YYYYMMDD_SLASH));
                    
                    // Loop 7 Days (Mon - Sun)
                    cdSet.clear();
                    BigDecimal sumWeek = new BigDecimal(ZERO);
                    for (int dayOfWeek = ZERO; dayOfWeek < SEVEN; dayOfWeek++) {
                        BigDecimal orderQty = Constants.BIG_DECIMAL_ZERO;
                        String qty = EMPTY_STRING;
                        String date = EMPTY_STRING;
                        
                        if (ZERO == iW) {
                            qty = STR_ZERO;
                            date = EMPTY_STRING;
                        }
                        
                        currentEtd = new java.util.Date(periodDate.getTimeInMillis());
                        PurchaseOrderReportDueDomain currentDue = null;
                        if (dueDIndex < dueDList.size()) {
                            currentDue = dueDList.get(dueDIndex);
                        }
                        java.util.Date dueEtd = null;
                        if (null != currentDue) {
                            dueEtd = new java.util.Date(
                                currentDue.getEtd().getTime());
                        }
                        
                        // If current ETD equal to current Due ETD, use current Due
                        if (null != dueEtd && ZERO == currentEtd.compareTo(dueEtd)) {
                            cdSet.add(SupplierPortalConstant.PO_PERIOD_TYPE.get(
                                currentDue.getPeriodType()));
                            
                            orderQty = currentDue.getOrderQty();
                            
                            qty = String.valueOf(orderQty);
                            if (null == currentDue.getDueDate()) {
                                date = StringUtil.appendsString(
                                    Constants.SYMBOL_OPEN_BRACKET,
                                    DateUtil.format(dueEtd, PATTERN_YYYYMMDD_SLASH)
                                        , Constants.SYMBOL_CLOSE_BRACKET);
                            } else {
                                date = StringUtil.appendsString(
                                    Constants.SYMBOL_OPEN_BRACKET,
                                    DateUtil.format(currentDue.getDueDate(), PATTERN_YYYYMMDD_SLASH)
                                        , Constants.SYMBOL_CLOSE_BRACKET);
                            }
                            
                            dueDIndex++;
                        }
                        
                        sumWeek = sumWeek.add(orderQty);
                        
                        // Show quantity and due date when record is 'D'
                        if (Constants.STR_D.equals(region2[iW].getDwm())) {
                            if (ZERO == dayOfWeek) {
                                region2[iW].setMon( qty );
                                region2[iW].setMonDate( date );
                            } else if (ONE == dayOfWeek) {
                                region2[iW].setTue( qty );
                                region2[iW].setTueDate( date );
                            } else if (TWO == dayOfWeek) {
                                region2[iW].setWed( qty );
                                region2[iW].setWedDate( date );
                            } else if (THREE == dayOfWeek) {
                                region2[iW].setThu( qty );
                                region2[iW].setThuDate( date );
                            } else if (FOUR == dayOfWeek) {
                                region2[iW].setFri( qty );
                                region2[iW].setFriDate( date );
                            } else if (FIVE == dayOfWeek) {
                                region2[iW].setSat( qty );
                                region2[iW].setSatDate( date );
                            } else if (SIX == dayOfWeek) {
                                region2[iW].setSun( qty );
                                region2[iW].setSunDate( date );
                            }
                        }
                        
                        // To next day
                        periodDate.add(Calendar.DATE, ONE);
                    }
                    
                    region2[iW].setQty( String.valueOf(sumWeek));
                    
                    // Set C, D or C*
                    if( ONE < cdSet.size() ){
                        region2[iW].setCd( Constants.STR_C_STAR );
                    //} else if( ONE == cdSet.size() ){
                    //    region2[iW].setCd( cdSet.iterator().next() );
                    } else {
                        if (null == endFirm) {
                            region2[iW].setCd( Constants.STR_D );
                        } else {
                            Calendar firstDayOfWeek = Calendar.getInstance();
                            firstDayOfWeek.setTimeInMillis(periodDate.getTimeInMillis());
                            firstDayOfWeek.add(Calendar.DATE, Constants.MINUS_SEVEN);

                            // End Firm before start this week
                            if (ZERO < firstDayOfWeek.compareTo(endFirm)) {
                                region2[iW].setCd( Constants.STR_D );
                            } else {
                                // End Firm after start this week
                                
                                if(Constants.DKO_TYPE.equals(currentPart.getPoType())){
                                    region2[iW].setCd(Constants.STR_K );
                                }else{
                                    region2[iW].setCd( Constants.STR_C );
                                }
                            }
                        }
                    }
                } // for.
                
                /* Fill data Report type W : first 4 records (from P/O DueDate From + 42 and
                 * increase by 7)
                 * */
                int dueWIndex = ZERO;
                for(int iW = ZERO; iW < EIGHT; iW++ ) {
                    String qty = STR_ZERO;
                    String date = EMPTY_STRING;
                    String cd = EMPTY_STRING;
                    
                    java.util.Date currentEtd =  new java.util.Date(periodDate.getTimeInMillis());
                    PurchaseOrderReportDueDomain currentDue = null;
                    if (dueWIndex < dueWList.size()) {
                        currentDue = dueWList.get(dueWIndex);
                    }
                    java.util.Date dueEtd = null;
                    if (null != currentDue) {
                        dueEtd = new java.util.Date(
                            currentDue.getEtd().getTime());
                    }
                    
                    if (null != dueEtd && ZERO == currentEtd.compareTo(dueEtd)) {
                        qty = String.valueOf(currentDue.getOrderQty());
                        date = DateUtil.format(currentDue.getEtd(), PATTERN_YYYYMMDD_SLASH);
                        cd = SupplierPortalConstant.PO_PERIOD_TYPE.get(currentDue.getPeriodType());
                        dueWIndex++;
                    } else {
                        date = DateUtil.format(currentEtd, PATTERN_YYYYMMDD_SLASH) ;
                        if (null == endFirm) {
                            cd = Constants.STR_D;
                        } else if (ZERO < periodDate.compareTo(endFirm)) {
                            cd = Constants.STR_D;
                        } else {
                            cd = Constants.STR_C;
                        }
                    }
                    
                    if (iW < FOUR) {
                        region3[iW].setDate(date);
                        region3[iW].setCd(cd);
                        region3[iW].setDwm(SupplierPortalConstant.PO_DWM.get(Constants.STR_ONE));
                        region3[iW].setQty(qty);
                    } else {
                        region3[iW - FOUR].setDate2(date);
                        region3[iW - FOUR].setCd2(cd);
                        region3[iW - FOUR].setDwm2(
                            SupplierPortalConstant.PO_DWM.get(Constants.STR_ONE));
                        region3[iW - FOUR].setQty2(qty);
                    }

                    // To next Week
                    periodDate.add(Calendar.DATE, SEVEN);
                } // for.
                
                /* Fill data Report M : 4 records (from first date of month in P/O DueDate From
                 * increase by month)
                 * */
                int dueMIndex = ZERO;
                Calendar monthPeriod = Calendar.getInstance();
                monthPeriod.setTimeInMillis(
                    reportList.get(iRecord).getIssueDateFromDate().getTime());
                for(int iM = ZERO; iM < FOUR; iM++ ){
                    
                    java.util.Date currentMonth = new java.util.Date(monthPeriod.getTimeInMillis());
                    String defaultMonth = DateUtil.format(currentMonth
                        , DateUtil.PATTERN_YYYYMM_SLASH);
                    
                    String monthShow = defaultMonth;
                    String qty = STR_ZERO;
                    
                    PurchaseOrderReportDueDomain currentDue = null;
                    if (dueMIndex < dueMList.size()) {
                        currentDue = dueMList.get(dueMIndex);
                    }
                    String dueMonth = null;
                    if (null != currentDue) {
                        java.util.Date dueEtd = new java.util.Date(
                            currentDue.getEtd().getTime());
                        dueMonth = DateUtil.format(dueEtd, DateUtil.PATTERN_YYYYMM_SLASH);
                    }

                    if (null != dueMonth && defaultMonth.equals(dueMonth)) {
                        monthShow = dueMonth;
                        qty = String.valueOf(currentDue.getOrderQty());
                        dueMIndex++;
                    }

                    region3[iM].setMonth(monthShow);
                    region3[iM].setMonthdwm(SupplierPortalConstant.PO_DWM.get(Constants.STR_TWO));
                    region3[iM].setMonthQty(qty);
                    
                    // To next month
                    monthPeriod.add(Calendar.MONTH, ONE);
                } // for.
            } // end for all data
            
        } // end if.

        return resultList;
    }
    
    /**
     * <p>Get Report Path method.</p>
     *
     * @param reportType int
     * @return Path
     * @throws Exception error
     */
    private String getReportPathFixed(int reportType) throws Exception {
        
        if(ZERO == reportType ){
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.PO_COVER_PAGE_REPORT);
        } else if(ONE == reportType ) {
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.PURCHASE_ORDER_REPORT);
        } else if(TWO == reportType ) {
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.NO_PURCHASE_ORDER_REPORT);
        } else {
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.CHANGE_MATERIAL_RELEASE_REPORT);
        }
       
    }
    
    /**
     * Format datetime and number for show in CSV file.
     * @param result result from DAO
     * */
    private void formatPurchaseOrderForCsv(List<PurchaseOrderInformationDomain> result) {
        DecimalFormat defaultFormat = new DecimalFormat(Constants.DEFAULT_NUMBER_FORMAT);
        DecimalFormat noDecimalFormat = new DecimalFormat(Constants.NO_DECIMAL_NUMBER_FORMAT);
        DecimalFormat sixDecimalFormat = new DecimalFormat(Constants.SIX_DECIMAL_NUMBER_FORMAT);
        SpsTPoDomain sactPo = null;
        SpsTPoDetailDomain sactPoDetail = null;
        SpsTPoDueDomain sactPoDue = null;
        for (PurchaseOrderInformationDomain po : result) {
            po.setPreviousSpsPoNo(EMPTY_STRING);
            try {
                sactPo = po.getSpsTPoDomain();
                if (null != sactPo) {

                    if (null != sactPo.getPoIssueDate()) {
                        po.setStringPoIssueDate(DateUtil.format(sactPo.getPoIssueDate(),
                            PATTERN_YYYYMMDD_SLASH));
                    }
                    if (null != sactPo.getReleaseNo()) {
                        po.setStringReleaseNo(noDecimalFormat.format(sactPo.getReleaseNo()));
                    }
                }
                for (PurchaseOrderDetailDomain poDetail : po.getPurchaseOrderDetailList()) {
                    sactPoDetail = poDetail.getSpsTPoDetailDomain();
                    if (null != sactPoDetail) {
                        if (null != sactPoDetail.getOrderLot()) {
                            poDetail.setStringOrderLot(noDecimalFormat.format(
                                sactPoDetail.getOrderLot()));
                        }
                        if (null != sactPoDetail.getUnitPrice()) {
                            poDetail.setStringUnitPrice(sixDecimalFormat.format(
                                sactPoDetail.getUnitPrice()));
                        }
                        if(null != sactPoDetail.getQtyBox()){
                            poDetail.setStringQtyBox(defaultFormat.format(
                                sactPoDetail.getQtyBox()));
                        }
                    }
                    sactPoDue = poDetail.getSpsTPoDueDomain();
                    if (null != sactPoDue) {
                        if (null != sactPoDue.getDueDate()) {
                            poDetail.setStringDueDate(DateUtil.format(sactPoDue.getDueDate(),
                                PATTERN_YYYYMMDD_SLASH));
                        }
                        if (null != sactPoDue.getEtd()) {
                            poDetail.setStringEtd(DateUtil.format(sactPoDue.getEtd(),
                                PATTERN_YYYYMMDD_SLASH));
                        }
                        if (null != sactPoDue.getOrderQty()) {
                            poDetail.setStringOrderQty(defaultFormat.format(
                                sactPoDue.getOrderQty()));
                        }
                        if (null != sactPoDue.getPreviousQty()) {
                            poDetail.setStringPreviousQty(defaultFormat.format(
                                sactPoDue.getPreviousQty()));
                        }
                        if (null != sactPoDue.getDifferenceQty()) {
                            poDetail.setStringDifferenceQty(defaultFormat.format(
                                sactPoDue.getDifferenceQty()));
                        }
                    }
                }
            } catch (Exception e) {
                continue;
            }
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchPurchaseOrderKanbanReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain)
     */
    public List<PurchaseOrderReportDomain> searchPurchaseOrderKanbanReport ( 
        SpsTPoDomain input){
        return purchaseOrderDao.searchPurchaseOrderKanbanReport(input);
    }
    
    // [IN055] Validate Supplier Information before delete
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountEffectParts(com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain)
     */
    public Integer searchCountEffectParts(PurchaseOrderDomain poDomain) {
        return this.purchaseOrderDao.searchCountEffectParts(poDomain);
    }
    
    // [IN054] Update all Supplier Promised Due that not reply to Reject
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderService#updateRejectNotReplyDue(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public Integer updateRejectNotReplyDue( SpsTPoDomain spsTPoDomain ) {
        return this.purchaseOrderDao.updateRejectNotReplyDue(spsTPoDomain);
    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceService#deletePurgingPo(String)
     */
    public int deletePurgingPo(String poId){
        return this.purchaseOrderDao.deletePurgingPo(poId);
    }

	public Integer deletePurchaseOrder(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException {
        return purchaseOrderDao.deletePurchaseOrder(purgingBhtTransmitLogCriteria);
	}
}