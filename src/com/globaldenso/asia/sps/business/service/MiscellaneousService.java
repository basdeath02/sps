/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;

/**
 * <p>The Interface file upload service.</p>
 * <p>Service for file upload about search and insert file upload information.</p>
 * <ul>
 * <li>Method search  : searchCountFileNameByCode</li>
 * </ul>
 *
 * @author CSI
 */
public interface MiscellaneousService {
    
    /**
     * <p>Search misc value.</p>
     * <ul>
     * <li>Search misc value by misc type and misc code.</li>
     * </ul>
     * 
     * @param miscDomain the misc domain
     * @return the integer
     */
    public Integer searchMiscValue(MiscellaneousDomain miscDomain);
    
    /**
     * Search misc value for combo box.
     * Search misc value by misc type.
     * 
     * @param miscDomain the misc domain
     * @return the list of MiscDomain.
     */
    public List<MiscellaneousDomain> searchMisc(MiscellaneousDomain miscDomain);
}