/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;

/**
 * <p>The Interface UploadAsnService.</p>
 * <p>Service for ASN Uploading about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchTmpUploadAsn</li>
 * </ul>
 *
 * @author CSI
 */
public interface UploadAsnService {
    
    /**
     * Search temp upload asn.
     * <p>Search temp upload asn information for maintenance.</p>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the list of asn maintenance return domain
     * @throws ApplicationException ApplicationException
     */
    public List<AsnMaintenanceReturnDomain> searchTmpUploadAsn(
        AsnMaintenanceDomain asnMaintenanceDomain) throws ApplicationException;
}