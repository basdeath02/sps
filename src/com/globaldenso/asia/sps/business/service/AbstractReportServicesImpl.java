/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.common.constant.Constants;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * 
 * <p>Abstract class for Service that can generate report (ASN, PO etc.)</p>
 *
 * @author CSI
 */
//* @param <T> type of Result Domain Report
//* @param <T2> type of Criteria Domain Report
public abstract class AbstractReportServicesImpl/*<T, T2>*/ {
    
    /** The File Management service. */
    private FileManagementService fileManagementService;
    /**
     * 
     * <p>get Report Path.</p>
     *
     * @return Report name
     * @throws Exception 
     */
    protected String getReportPath() throws Exception{
        return null;
    }
//    /**
//     * 
//     * <p>Convert to Result Domain Report.</p>
//     *
//     * @param input criteria report
//     * @return list of Result Domain report.
//     * @throws Exception 
//     */
//    protected abstract List<T> getDataSource(T2 input) throws Exception;
    
    /**
     * 
     * <p>get Cover Page Report path.</p>
     *
     * @return Report name
     * @throws Exception 
     */
    protected String getCoverPageReportPath() throws Exception{
        return null;
    }
    
    /**
     * 
     * <p>Create File Upload.</p>
     *
     * @param fileData InputStream
     * @param fileName String
     * @param user create user
     * @return File ID
     * @throws Exception 
     */
    protected String createFileUpload(InputStream fileData, String fileName, String user) 
        throws Exception{
        return fileManagementService.createFileUpload(
                    fileData,
                    fileName,
                    Constants.SAVE_LIMIT_TERM,
                    user);
    }
    
    /**
     * 
     * <p>Generate PDF Stream.</p>
     *
     * @return inputStream InputStream 
     * @param print JasperPrint
     * @throws Exception 
     */
    protected static final InputStream generate(JasperPrint print) 
        throws Exception {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, print); 
        exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, output);
        exporter.exportReport();
        
        return new ByteArrayInputStream(output.toByteArray());
    }
    
    /**
     * 
     * <p>Generate PDF Stream from multi report.</p>
     *
     * @return inputStream InputStream
     * @param printList list of JasperPrint
     * @throws Exception 
     */
    protected static final InputStream generate(List<JasperPrint> printList) throws Exception {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, printList); 
        exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, output);
        exporter.exportReport(); 
        return new ByteArrayInputStream(output.toByteArray());
    }
    
    /**
     * 
     * <p>generate JasperPrint.</p>
     *
     * @param path PathReport
     * @param input List of Result Report Domain
     * @param parameters parameter
     * @return JasperPrint
     * @throws Exception 
     */
    @SuppressWarnings("rawtypes")
    protected JasperPrint generateReport(String path, List input, Map<String, Object> parameters)
        throws Exception {
        JasperPrint print = JasperFillManager.fillReport(path, parameters, 
            new JRBeanCollectionDataSource(input)); 
        return print;
    } // end generateReport.

    /**
     * <p>Setter method for fileManagementService.</p>
     *
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    
}
