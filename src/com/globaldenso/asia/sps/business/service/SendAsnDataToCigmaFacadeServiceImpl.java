/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnService;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.SendAsnDataToCigmaDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The Class SendAsnDataToCigmaFacadeServiceImpl.</p>
 * <p>Manage Send Asn Data to Cigma for batch.</p>
 * <li>Method search : searchSendingAsn</li>
 * <li>Method search : searchAs400ServerList</li>
 * <li>Method update : transactSendAsn</li>
 * @author CSI
 * @version 1.00
 */
public class SendAsnDataToCigmaFacadeServiceImpl implements SendAsnDataToCigmaFacadeService{
    
    /** The asn service. */
    private AsnService asnService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The spsTAsn service. */
    private SpsTAsnService spsTAsnService;
    
    /**
     * Instantiates a new send asn to cigma facade service impl.
     */
    public SendAsnDataToCigmaFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the asn service.
     * 
     * @param asnService the new asn service
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Sets the common service.
     * 
     * @param commonService the new common service
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets the company denso service.
     * 
     * @param companyDensoService the new company denso service
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * Sets the spsTAsn service.
     * 
     * @param spsTAsnService the new spsTAsn service
     */
    public void setSpsTAsnService(SpsTAsnService spsTAsnService) {
        this.spsTAsnService = spsTAsnService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SendAsnDataToCigmaFacadeService#searchSendingAsn(com.globaldenso.asia.sps.business.domain.SendingAsnDomain)
     */
    public SendingAsnDomain searchSendingAsn(SendingAsnDomain sendingAsnDomain)
    {
        SendingAsnDomain sendingAsnResult = new SendingAsnDomain();
        List<AsnInfoDomain> asnInfoList = asnService.searchSendingAsn(sendingAsnDomain);
        sendingAsnResult.setAsnInformationList(asnInfoList);
        return sendingAsnResult;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SendAsnDataToCigmaFacadeService#searchAs400ServerList(java.lang.String)
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        String companyDensoCode)
    {
        List<As400ServerConnectionInformationDomain> serverConnectionInfoList = null;
        SpsMCompanyDensoDomain companyDensoDomain = new SpsMCompanyDensoDomain();
        companyDensoDomain.setDCd(companyDensoCode);
        serverConnectionInfoList = companyDensoService.searchAs400ServerList(companyDensoDomain);
        
        return serverConnectionInfoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SendAsnDataToCigmaFacadeService#transactSendAsn(com.globaldenso.asia.sps.business.domain.SendAsnDataToCigmaDomain)
     */
    public void transactSendAsn(SendAsnDataToCigmaDomain sendAsnDataToCigmaDomin) 
        throws ApplicationException, WebServiceCallerRestException
    {
        Locale locale = sendAsnDataToCigmaDomin.getLocale();
        Timestamp currentDateTime = commonService.searchSysDate();
        AsnInfoDomain asnInformation = sendAsnDataToCigmaDomin.getAsnInformationDomain();
        SpsTAsnDomain asnDomain = new SpsTAsnDomain();
        SpsTAsnCriteriaDomain asnCriteria = new SpsTAsnCriteriaDomain();
        
        //Call service ASNBatchResource to transfer ASN Data to CIGMA AS/400 Server
        String recordCount = CommonWebServiceUtil.asnBatchResourceTransactSendAsn(
            sendAsnDataToCigmaDomin);
        if(!StringUtil.checkNullOrEmpty(recordCount)){
            if(Constants.ZERO == Integer.valueOf(recordCount)){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0009,
                    new String[]{asnInformation.getSpsTAsnDomain().getAsnNo()});
            }
        } else {
            asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_YES);
            asnDomain.setLastUpdateDscId(sendAsnDataToCigmaDomin.getLastUpdateDscId());
            asnDomain.setLastUpdateDatetime(currentDateTime);
            
            asnCriteria.setAsnNo(asnInformation.getSpsTAsnDomain().getAsnNo());
            asnCriteria.setLastUpdateDatetime(
                asnInformation.getSpsTAsnDomain().getLastUpdateDatetime());
            
            int rowUpdate = spsTAsnService.updateByCondition(asnDomain, asnCriteria);
            if(Constants.ZERO == rowUpdate){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0010,
                        new String[]{asnInformation.getSpsTAsnDomain().getAsnNo()});
                }
            }
        }
    }