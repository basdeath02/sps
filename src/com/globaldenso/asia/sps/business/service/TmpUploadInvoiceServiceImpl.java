/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain;
import com.globaldenso.asia.sps.business.dao.TmpUploadInvoiceDao;
import com.globaldenso.asia.sps.common.utils.DateUtil;

/**
 * <p>The Class TmpUploadInvoiceServiceImpl.</p>
 * <p>Service for temporary upload invoice.</p>
 * <ul>
 * <li>Method search  : searchTmpUploadInvoice</li>
 * </ul>
 *
 * @author CSI
 */
public class TmpUploadInvoiceServiceImpl implements TmpUploadInvoiceService {
    
    /** The temporary upload invoice dao. */
    private TmpUploadInvoiceDao tmpUploadInvoiceDao;

    /**
     * Instantiates a new invoice service impl.
     */
    public TmpUploadInvoiceServiceImpl(){
        super();
    }
    
    /**
     * Sets the temporary upload invoice dao.
     * 
     * @param tmpUploadInvoiceDao the temporary upload invoice dao.
     */
    public void setTmpUploadInvoiceDao(TmpUploadInvoiceDao tmpUploadInvoiceDao) {
        this.tmpUploadInvoiceDao = tmpUploadInvoiceDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadInvoiceService#searchTmpUploadInvoice(com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain)
     */
    public List<TmpUploadInvoiceDomain> searchTmpUploadInvoice(
        TmpUploadInvoiceDomain tmpUploadInvoiceDomain){
        
        List<TmpUploadInvoiceDomain> tmpUploadInvoiceList = null;
        tmpUploadInvoiceList = this.tmpUploadInvoiceDao.searchTmpUploadInvoice(
            tmpUploadInvoiceDomain);
        for(TmpUploadInvoiceDomain tmpUploadInvoiceItem:tmpUploadInvoiceList){
            tmpUploadInvoiceItem.setInvoiceDate(
                DateUtil.format(tmpUploadInvoiceItem.getInvoiceDateTd(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            tmpUploadInvoiceItem.setCnDate(
                DateUtil.format(tmpUploadInvoiceItem.getCnDateTd(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            for(AsnDomain asnItem:tmpUploadInvoiceItem.getAsnDtailList()){
                asnItem.setPlanEta(
                    DateUtil.format(asnItem.getSpsTmpUploadInvoiceDomain().getPlanEta(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
                
            }
        }
        return tmpUploadInvoiceList;
    } 
}