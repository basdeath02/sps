/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.UpdateInvoiceInformationDomain;

/**
 * <p>The Interface UpdatePaymentStatusFacadeService.</p>
 * <p>Interface for Update Payment Status From JDE batch.</p>
 * <ul>
 * <li>Method search  : searchAS400ServerList</li>
 * <li>Method update  : updatePaymentStatus</li>
 * </ul>
 *
 * @author CSI
 */
public interface UpdatePaymentStatusFacadeService {
    
    /**
     * Search AS400 service list.
     * 
     * @param companyDensoDomain the common denso domain
     * @return List of AS400 server connection information domain
     * @throws ApplicationException ApplicationException
     * */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain companyDensoDomain) throws ApplicationException;
    
    /**
     * Update Payment Status in SPS_T_INVOICE by get data from JDE.
     * @param updateInvoiceInformationDomain Update Invoice Information Domain
     * @throws ApplicationException an ApplicationException
     * */
    public void updatePaymentStatus(
        UpdateInvoiceInformationDomain updateInvoiceInformationDomain) throws ApplicationException;
}
