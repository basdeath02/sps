/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain;

/**
 * <p>The Interface SupplierUserRoleAssignmentFacadeService.</p>
 * <p>Service for Supplier User Role about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search  : initial</li>
 * <li>Method search  : initialPopup</li>
 * <li>Method insert  : createSupplierUserRole</li>
 * <li>Method update  : deleteSupplierUserRole</li>
 * <li>Method update  : updateSupplierUserRole</li>
 * </ul>
 *
 * @author CSI
 */
public interface SupplierUserRoleAssignmentFacadeService {
    
    /**
     * <p>Initial supplier user role.</p>
     * <ul>
     * <li>Search Supplier User Role Item Detail for display on supplier User Role screen.</li>
     * </ul>
     * 
     * @param supplierUserRoleAssignmentDomain that keep target user DSC-ID and data scope control 
     * to get the data from database.
     * @return the supplierUserRoleAssignmentDomain that keep Supplier user information to show 
     * on the top of screen.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserRoleAssignmentDomain searchInitial(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain) throws ApplicationException;
    
    /**
     * <p>Initial supplier user role pop up.</p>
     * <ul>
     * <li>Search Supplier User Role Item Detail for display on supplier User Role screen.</li>
     * </ul>
     * 
     * @param supplierUserRoleAssignmentDomain that keep user type, and scope of data to get 
     * from database.
     * @return the supplierUserRoleAssignmentDomain that It keep the list of RoleDomain and 
     * list of supplier plant to show on the combo box.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserRoleAssignmentDomain searchInitialPopup(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain) throws ApplicationException;
   
    /**
     * <p>Delete Supplier User Role</p>
     * <ul>
     * <li>Delete Supplier User Role by criteria.</li>
     * </ul>
     * 
     * @param supplierUserRoleAssignmentDomain that keep selected user role to delete.
     * @throws ApplicationException the ApplicationException.
     */
    public void deleteSupplierUserRole(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain)throws ApplicationException;
    
    /**
     * <p>Create Supplier User Role.</p>
     * <ul>
     * <li>create detail Supplier User Role data.
     * then click OK button.</li>
     * </ul>
     * 
     * @param supplierUserRoleAssignmentDomain that keep role information to insert to the system.
     * @return SupplierUserRoleAssignmentDomain that keep list of error message.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserRoleAssignmentDomain createUserRole(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain)throws ApplicationException;
    
    /**
     * <p>update User Role.
     * <ul>
     * <li>update Supplier User Role data.
     * then click OK button.</li>
     * </ul>
     * 
     *  @param supplierUserRoleAssignmentDomain that keep role information to update to the system.
     * @return SupplierUserRoleAssignmentDomain that keep list of error message.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserRoleAssignmentDomain updateSupplierUserRole(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain)throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}