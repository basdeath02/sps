/*
 * ModifyDate Development company     Describe 
 * 2014/03/03 CSI Parichat            Create
 * 2015/08/05 CSI Akat                [IN009]
 * 2015/09/10 CSI Akat                [IN014]
 * 2016/01/15 CSI Akat                [IN039]
 * 2016/03/02 CSI Akat                [FIX] Preview Mode must not show ASN NO
 * 2017/04/26 NB Napol                [SPS007] Change ASN Location from CIGMA to SPS
 * 
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JasperPrint;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.qrcodegenerate.QrcodeGenerator;
import com.globaldenso.ai.library.qrcodegenerate.exception.QrcodeGenerateException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain;
import com.globaldenso.asia.sps.business.dao.AsnDao;
import com.globaldenso.asia.sps.business.dao.MiscellaneousDao;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDetailDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnReportDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>The class AsnServiceImpl.</p>
 * <p>Service for ASN about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchSendingAsn</li>
 * <li>Method update  : searchAsnDetail</li>
 * <li>Method search  : searchAsnInformation</li>
 * <li>Method search  : searchCountAsnInformation</li>
 * <li>Method search  : searchExistAsn</li>
 * <li>Method search  : searchAsnReport</li>
 * <li>Method search  : searchAsnForGroupInvoice</li>
 * <li>Method create  : createViewAsnReport</li>
 * <li>Method search  : searchPurgingAsnOrder</li>
 * <li>Method search  : searchSumShippingQty</li>
 * <li>Method search  : searchSumShippingQtyByPart</li>
 * </ul>
 *
 * @author CSI
 */
public class AsnServiceImpl extends AbstractReportServicesImpl implements AsnService {
    
    /** The ASN Dao. */
    private AsnDao asnDao;
    
    /** The DAO for SPS_M_COMPANY_SUPPLIER. */
    private SpsMCompanySupplierDao spsMCompanySupplierDao;
    
    /** The DAO for SPS_T_DO_KANBAN_SEQ. */
    private SpsTDoKanbanSeqDao spsTDoKanbanSeqDao;
    
    /** The DAO for SPS_M_COMPANY_DENSO. */
    private SpsMCompanyDensoDao spsMCompanyDensoDao;
    
    /** The DAO for Miscellaneous. */
    private MiscellaneousDao miscDao;
    //[SPS007] Add Dao for 
    /** The DAO for SPS_M_PLANT_SUPPLIER. */
    private SpsMPlantSupplierDao spsMPlantSupplierDao;
    
    /** The DAO for SpsTDoDao. */
    private SpsTDoDao spsTDoDao;
    
    /** The DAO for spsTDoDetailDao. */
    private SpsTDoDetailDao spsTDoDetailDao;
    /**
     * Instantiates a new ASN service impl.
     */
    public AsnServiceImpl(){
        super();
    }
    
    /**
     * Sets the asn dao.
     * 
     * @param asnDao the asn dao.
     */
    public void setAsnDao(AsnDao asnDao) {
        this.asnDao = asnDao;
    }

    /**
     * <p>Setter method for spsTDoKanbanSeqDao.</p>
     *
     * @param spsTDoKanbanSeqDao Set for spsTDoKanbanSeqDao
     */
    public void setSpsTDoKanbanSeqDao(SpsTDoKanbanSeqDao spsTDoKanbanSeqDao) {
        this.spsTDoKanbanSeqDao = spsTDoKanbanSeqDao;
    }

    /**
     * <p>Setter method for spsMCompanySupplierDao.</p>
     *
     * @param spsMCompanySupplierDao Set for spsMCompanySupplierDao
     */
    public void setSpsMCompanySupplierDao(
        SpsMCompanySupplierDao spsMCompanySupplierDao) {
        this.spsMCompanySupplierDao = spsMCompanySupplierDao;
    }

    /**
     * <p>Setter method for spsMCompanyDensoDao.</p>
     *
     * @param spsMCompanyDensoDao Set for spsMCompanyDensoDao
     */
    public void setSpsMCompanyDensoDao(SpsMCompanyDensoDao spsMCompanyDensoDao) {
        this.spsMCompanyDensoDao = spsMCompanyDensoDao;
    }
    //[SPS007 Initial DAO]
    
    /**
     * <p>Setter method for spsMPlantSupplierDao.</p>
     *
     * @param spsMPlantSupplierDao Set for spsMPlantSupplierDao
     */
    public void setSpsMPlantSupplierDao(SpsMPlantSupplierDao spsMPlantSupplierDao) {
        this.spsMPlantSupplierDao = spsMPlantSupplierDao;
    }
    
    /**
     * <p>Setter method for spsTDoDao.</p>
     *
     * @param spsTDoDao Set for spsTDoDao
     */
    public void setSpsTDoDao(SpsTDoDao spsTDoDao) {
        this.spsTDoDao = spsTDoDao;
    }
    
    /**
     * <p>Setter method for spsTDoDetailDao.</p>
     *
     * @param spsTDoDetailDao Set for spsTDoDetailDao
     */
    public void setSpsTDoDetailDao(SpsTDoDetailDao spsTDoDetailDao) {
        this.spsTDoDetailDao = spsTDoDetailDao;
    }

    /**
     * <p>Setter method for miscDao.</p>
     *
     * @param miscDao Set for miscDao
     */
    public void setMiscDao(MiscellaneousDao miscDao) {
        this.miscDao = miscDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.eps.business.service.AsnService#searchSendingAsn(com.globaldenso.eps.business.domain.SendingAsnDomain)
     */
    public List<AsnInfoDomain> searchSendingAsn(SendingAsnDomain sendingAsnDomain){
        List<AsnInfoDomain> asnInfoList = null;
        asnInfoList = asnDao.searchSendingAsn(sendingAsnDomain);
        return asnInfoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchAsnDetail(com.globaldenso.asia.sps.business.domain.AsnInformationDomain)
     */
    public List<AsnInformationDomain> searchAsnDetail(AsnInformationDomain asnInformationDomain){
        List<AsnInformationDomain> asnInformationList = null;
        asnInformationList = asnDao.searchAsnDetail(asnInformationDomain);
        return asnInformationList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchAsnInformation(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public List<AsnProgressInformationReturnDomain> searchAsnInformation(
        AsnProgressInformationDomain asnProgressInformationDomain){
        List<AsnProgressInformationReturnDomain> result = 
            asnDao.searchAsnInformation(asnProgressInformationDomain);
        return result;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchCountAsnInformation(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public int searchCountAsnInformation(AsnProgressInformationDomain asnProgressInformationDomain){
        int recordCount = Constants.ZERO;
        recordCount = asnDao.searchCountAsnInformation(asnProgressInformationDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchExistAsn(com.globaldenso.asia.sps.business.domain.AsnDomain)
     */
    public List<AsnDomain> searchExistAsn(AsnDomain asnDomain){
        List<AsnDomain> asnList = null;
        asnList = this.asnDao.searchExistAsn(asnDomain);
        return asnList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchAsnReport(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain)
     */
    public InputStream searchAsnReport(SpsTAsnDomain spsTAsnDomain) throws Exception {
        List<AsnInfoDomain> asnInfoList = this.asnDao.searchAsnReport(spsTAsnDomain);
        List<AsnReportDomain> eachRcvList = null;
        Set<String> orderMethodSet = new HashSet<String>();
        
        DecimalFormat defaultFormat = new DecimalFormat(Constants.DEFAULT_NUMBER_FORMAT);
        DecimalFormat noDecimalFormat = new DecimalFormat(Constants.NO_DECIMAL_NUMBER_FORMAT);
        
        SpsTAsnDomain spsTAsn = null;
        SpsTAsnDetailDomain spsTAsnDetail = null;
        SpsTDoDomain spsTDo = null;
        SpsTDoDetailDomain spsTDoDetail = null;
        AsnReportDomain record = null;
        
        String etd = Constants.EMPTY_STRING;
        String etdSchedule = Constants.EMPTY_STRING;
        String eta = Constants.EMPTY_STRING;
        String etaSchedule = Constants.EMPTY_STRING;
        String stringNumberOfPallet = Constants.EMPTY_STRING;
        String stringIssueDate = Constants.EMPTY_STRING;
        String stringLastModified = Constants.EMPTY_STRING;
        String qrType = Constants.EMPTY_STRING;

        StringBuffer kanbanBuffer = new StringBuffer();
        StringBuffer densoBuffer = new StringBuffer();

        String currentRcv = Constants.EMPTY_STRING;
        Map<String, List<AsnReportDomain>> rcvMap
            = new LinkedHashMap<String, List<AsnReportDomain>>();
        
        for (AsnInfoDomain asnInfo : asnInfoList) {
            spsTAsn = asnInfo.getSpsTAsnDomain();
            
            qrType = asnInfo.getQrType();
            
            if (null != spsTAsn.getActualEtd()) {
                etd = DateUtil.format(
                    new Date(spsTAsn.getActualEtd().getTime()), DateUtil.PATTERN_YYYYMMDD_SLASH);
                etdSchedule = DateUtil.format(
                    new Date(spsTAsn.getActualEtd().getTime()), DateUtil.PATTERN_HHMM_COLON);
            }
            
            if (null != spsTAsn.getPlanEta()) {
                eta = DateUtil.format(
                    new Date(spsTAsn.getPlanEta().getTime()), DateUtil.PATTERN_YYYYMMDD_SLASH);
                etaSchedule = DateUtil.format(
                    new Date(spsTAsn.getPlanEta().getTime()), DateUtil.PATTERN_HHMM_COLON);
            }
            
            if (null != spsTAsn.getNumberOfPallet()) {
                stringNumberOfPallet = defaultFormat.format(spsTAsn.getNumberOfPallet());
            }
            
            if (null != spsTAsn.getCreateDatetime()) {
                stringIssueDate = DateUtil.format(new Date(spsTAsn.getCreateDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH); 
            }
            
            if (null != spsTAsn.getLastUpdateDatetime()) {
                stringLastModified = DateUtil.format(new Date(
                    spsTAsn.getLastUpdateDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            }
            
            if (null != asnInfo.getDensoName() && null != asnInfo.getDCd()) {
                densoBuffer.setLength(Constants.ZERO);
                densoBuffer.append(asnInfo.getDensoName()).append(Constants.SYMBOL_SPACE)
                   .append(Constants.SYMBOL_OPEN_BRACKET).append(asnInfo.getDCd())
                   .append(Constants.SYMBOL_CLOSE_BRACKET);
            }

            for (AsnInfoDetailDomain asnInfoDetail : asnInfo.getAsnInfoDetailList()) {
                spsTAsnDetail = asnInfoDetail.getSpsTAsnDetailDomain();
                spsTDo = asnInfoDetail.getSpsTDoDomain();
                spsTDoDetail = asnInfoDetail.getSpsTDoDetailDomain();
                
                //[SPS007 Use S_CD and S_PCD to get Supplier Plant Address]
                // Get Supplier Plant Location
                SpsMPlantSupplierCriteriaDomain supplierPlantCriteria
                    = new SpsMPlantSupplierCriteriaDomain();
                supplierPlantCriteria.setSCd(spsTAsn.getSCd());
                supplierPlantCriteria.setSPcd(spsTAsn.getSPcd());
                
                SpsMPlantSupplierDomain supplierPlant
                    = this.spsMPlantSupplierDao.searchByKey(supplierPlantCriteria);
               
                
                
                record = new AsnReportDomain();
                
                currentRcv = this.checkNullToEmpty(spsTAsnDetail.getRcvLane());
                eachRcvList = rcvMap.get(currentRcv);
                if (null == eachRcvList) {
                    eachRcvList = new ArrayList<AsnReportDomain>();
                    rcvMap.put(currentRcv, eachRcvList);
                }
                eachRcvList.add(record);
                
                record.setRcvLane(currentRcv);
                
                record.setAsnNo(this.checkNullToEmpty(spsTAsn.getAsnNo()));
                record.setSCd(this.checkNullToEmpty(spsTAsn.getSCd()));
                record.setVendorCd(this.checkNullToEmpty(spsTAsn.getVendorCd()));
                record.setSPcd(this.checkNullToEmpty(spsTAsn.getSPcd()));
                record.setDPcd(this.checkNullToEmpty(spsTAsn.getDPcd()));
                record.setTripNo(this.checkNullToEmpty(spsTAsn.getTripNo()));
                record.setRouteNo(this.checkNullToEmpty(spsTDo.getTruckRoute()));
                
                if (null != spsTDo.getTruckSeq()) {
                    record.setDelNo(noDecimalFormat.format(spsTDo.getTruckSeq()));
                } else {
                    record.setDelNo(Constants.EMPTY_STRING);
                }
                //[SPS007] Change DO Location to SPS_M_PLANT_SUPPLIER
                record.setSupplierLocation(this.checkNullToEmpty(supplierPlant.getPlantName()));
                record.setWarehouse(this.checkNullToEmpty(spsTDo.getWhPrimeReceiving()));
                record.setDockCode(this.checkNullToEmpty(spsTDo.getDockCode()));
                record.setTrans(this.checkNullToEmpty(spsTDo.getTm()));
                record.setCycle(this.checkNullToEmpty(spsTDo.getCycle()));
                
                if (Constants.STR_ONE.equals(spsTDo.getOrderMethod())) {
                    record.setOrderMethod(Constants.WORD_KANBAN_UPPER);
                } else {
                    record.setOrderMethod(Constants.WORD_FIXED_UPPER);
                }
                
                if (!Strings.judgeBlank(spsTDo.getOrderMethod())) {
                    orderMethodSet.add(spsTDo.getOrderMethod());
                }
                
                record.setSupplierName(this.checkNullToEmpty(asnInfo.getSupplierName()));
                record.setEtdDate(etd);
                record.setEtdTime(etdSchedule);
                record.setEtaDate(eta);
                record.setEtaTime(etaSchedule);
                record.setIssueDate(stringIssueDate);
                record.setLastModified(stringLastModified);
                record.setNumberOfPallet(stringNumberOfPallet);
                
                record.setCtrlNo(this.checkNullToEmpty(spsTDoDetail.getCtrlNo()));
                record.setDescription(this.checkNullToEmpty(spsTDoDetail.getItemDesc()));
                record.setQtyBox(spsTDoDetail.getQtyBox());
                record.setNoOfBoxes(spsTAsnDetail.getShippingBoxQty());
                
                // [IN009] Rounding Mode use HALF_UP, ShippingBoxQty use CEILING
                //record.setStringQtyBox(noDecimalFormat.format(spsTDoDetail.getQtyBox()));
                //record.setStringNoOfBoxes(noDecimalFormat.format(
                //    spsTAsnDetail.getShippingBoxQty()));
                record.setStringQtyBox(NumberUtil.formatRoundingHalfUp(
                    noDecimalFormat, spsTDoDetail.getQtyBox(), Constants.ZERO));
                record.setStringNoOfBoxes(noDecimalFormat.format(
                    spsTAsnDetail.getShippingBoxQty().setScale(
                        Constants.ZERO, RoundingMode.CEILING)));
                
                record.setDPn(this.checkNullToEmpty(spsTAsnDetail.getDPn()));
                record.setSPn(this.checkNullToEmpty(spsTAsnDetail.getSPn()));
                record.setUm(this.checkNullToEmpty(spsTAsnDetail.getUnitOfMeasure()));
                record.setSpsDoNo(this.checkNullToEmpty(spsTAsnDetail.getSpsDoNo()));
                record.setCigmaDoNo(this.checkNullToEmpty(spsTAsnDetail.getCigmaDoNo()));
                record.setRsn(this.checkNullToEmpty(spsTAsnDetail.getChangeReasonCd()));
                
                if (null != spsTAsnDetail.getReviseShippingQtyFlag()
                    && Constants.STR_ONE.equals(spsTAsnDetail.getReviseShippingQtyFlag()))
                {
                    record.setFlag(Constants.SYMBOL_STAR);
                } else {
                    record.setFlag(Constants.EMPTY_STRING);
                }
                // Change from DO_DETAIL.CURRENT_ORDER_QTY to ASN_DETAIL.SHIPPING_QTY
                if (null != spsTAsnDetail.getShippingQty()) {
                    record.setCurrentOrderQty(defaultFormat.format(
                        spsTAsnDetail.getShippingQty()));
                } else {
                    record.setCurrentOrderQty(Constants.EMPTY_STRING);
                }

                if (null != asnInfoDetail.getKanbanSeqNo()
                    && !Constants.EMPTY_STRING.equals(asnInfoDetail.getKanbanSeqNo().trim()))
                {
                    kanbanBuffer.setLength(Constants.ZERO);
                    kanbanBuffer.append(Constants.SYMBOL_OPEN_BRACKET)
                        .append(asnInfoDetail.getKanbanSeqNo())
                        .append(Constants.SYMBOL_CLOSE_BRACKET);
                    record.setKanbanSeqNo(kanbanBuffer.toString());
                } else {
                    record.setKanbanSeqNo(Constants.EMPTY_STRING);
                }
            }
        }

        List<AsnReportDomain> recordList = new ArrayList<AsnReportDomain>();
        
        // Generate QR code for each RCV Lane
        String receiveByScan = asnInfoList.get(Constants.ZERO).getAsnInfoDetailList().get(Constants.ZERO).getSpsTDoDomain().getReceiveByScan();
        this.generateForEachRcvLane(recordList, rcvMap, qrType, receiveByScan, asnInfoList.get(Constants.ZERO).getDCd());
        
        // Set QR Warning
        String qrWarning = null;
        if (Constants.STR_ONE.equals(qrType)) {
            qrWarning = Constants.EMPTY_STRING;
        } else if (Constants.STR_TWO.equals(qrType)) {
            qrWarning = MessageUtil.getReportLabel(
                spsTAsnDomain.getLocale(), SupplierPortalConstant.LBL_RSHP001_QR_REMARK_TYPE2);
        } else if (Constants.STR_THREE.equals(qrType)) {
            qrWarning = MessageUtil.getReportLabel(
                spsTAsnDomain.getLocale(), SupplierPortalConstant.LBL_RSHP001_QR_REMARK_TYPE3);
        }
        for (AsnReportDomain asnDetail : recordList) {
            List<AsnReportDomain> rcvRecordList = rcvMap.get(asnDetail.getRcvLane());
            asnDetail.setRcvLaneCount(rcvRecordList.size());
            asnDetail.setQrWarning(qrWarning);
        }

        Map<String, Object> parameters = this.getAsnReportParameter(spsTAsnDomain.getLocale());

        // Set Order Method
        this.setOrderMethodForAsnReport(parameters, orderMethodSet);

        parameters.put(SupplierPortalConstant.RPT_PRM_DENSO_COMPANY, densoBuffer.toString());
        parameters.put(SupplierPortalConstant.RPT_PRM_TOTAL_RECORD,
            Integer.valueOf(recordList.size()));

        this.generateRsnCaption(parameters);
        
        JasperPrint jpPage = generateReport(getReportPath(), recordList, parameters);
        
        List<JasperPrint> printList = new ArrayList<JasperPrint>();
        printList.add(jpPage);
        return generate(printList);
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#createViewAsnReport(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    public InputStream createViewAsnReport(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException, Exception
    {
        List<AsnReportDomain> eachRcvList = null;
        Set<String> orderMethodSet = new HashSet<String>();

        DecimalFormat defaultFormat = new DecimalFormat(Constants.DEFAULT_NUMBER_FORMAT);
        DecimalFormat noDecimalFormat = new DecimalFormat(Constants.NO_DECIMAL_NUMBER_FORMAT);
        DecimalFormat decimalFormat = new DecimalFormat(Constants.DECIMAL_FORMAT);
        
        SpsTAsnDetailDomain spsTAsnDetail = null;
        SpsTDoDomain spsTDo = null;
        SpsTDoDetailDomain spsTDoDetail = null;
        SpsTAsnDomain spsTAsn = null;
        AsnReportDomain record = null;
        BigDecimal zero = new BigDecimal(Constants.ZERO);
        
        // Get Supplier Name
        AsnMaintenanceReturnDomain firstAsn
            = asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO);
        SpsMCompanySupplierCriteriaDomain supplierCriteria
            = new SpsMCompanySupplierCriteriaDomain();
        supplierCriteria.setSCd(firstAsn.getSCd());
        SpsMCompanySupplierDomain supplier
            = this.spsMCompanySupplierDao.searchByKey(supplierCriteria);
        
        //[SPS007]
        // Get Supplier Plant Location
        SpsMPlantSupplierCriteriaDomain supplierPlantCriteria
            = new SpsMPlantSupplierCriteriaDomain();
        supplierPlantCriteria.setSCd(firstAsn.getSCd());
        supplierPlantCriteria.setSPcd(firstAsn.getSPcd());
        
        SpsMPlantSupplierDomain supplierPlant
            = this.spsMPlantSupplierDao.searchByKey(supplierPlantCriteria);
       
        
        // Get DENSO company
        SpsMCompanyDensoCriteriaDomain densoCriteria = new SpsMCompanyDensoCriteriaDomain();
        densoCriteria.setDCd(firstAsn.getDCd());
        SpsMCompanyDensoDomain denso = this.spsMCompanyDensoDao.searchByKey(densoCriteria);
        StringBuffer densoBuffer = new StringBuffer();
        densoBuffer.append(denso.getCompanyName()).append(Constants.SYMBOL_SPACE)
           .append(Constants.SYMBOL_OPEN_BRACKET).append(denso.getDCd())
           .append(Constants.SYMBOL_CLOSE_BRACKET);
        
        String tripNo = this.checkNullToEmpty(asnMaintenanceDomain.getTripNo());
        
        String etd = this.checkNullToEmpty(asnMaintenanceDomain.getActualEtdDate());
        String etdSchedule = this.checkNullToEmpty(asnMaintenanceDomain.getActualEtdTime());
        
        String eta = this.checkNullToEmpty(asnMaintenanceDomain.getPlanEtaDate());
        String etaSchedule = this.checkNullToEmpty(asnMaintenanceDomain.getPlanEtaTime());
        String stringNumberOfPallet = this.checkNullToEmpty(asnMaintenanceDomain.getNoOfPallet());
        
        StringBuffer kanbanBuffer = new StringBuffer();
        
        String currentRcv = Constants.EMPTY_STRING;
        Map<String, List<AsnReportDomain>> rcvMap
            = new LinkedHashMap<String, List<AsnReportDomain>>();

        BigDecimal totalShipQtyByPart = new BigDecimal(Constants.ZERO);
        String chgReasonCdByPart = Constants.EMPTY_STRING;
        String bufferChgReasonCd = Constants.EMPTY_STRING;
        BigDecimal remainQty = null;
        
        // [IN014] To keep previous record
        AsnReportDomain previousRecord = null;
        
        // [IN039] Shiping box calculate by FIFO
        AsnReportDomain previousRecordForBox = null;
        BigDecimal totalShipBoxByPart = new BigDecimal(Constants.ZERO);
        
        for (AsnMaintenanceReturnDomain asnMaintenance : asnMaintenanceDomain.getDoGroupAsnList()) {
            spsTAsnDetail = asnMaintenance.getAsnDetailDomain();
            spsTDo = asnMaintenance.getGroupDoDomain();
            spsTDoDetail = asnMaintenance.getGroupDoDetailDomain();
            spsTAsn = asnMaintenance.getAsnDomain();
            BigDecimal shippingQty = zero;
            BigDecimal shippingBoxQty = zero;
            
            if (Constants.STR_ONE.equals(asnMaintenance.getRecordGroup())) {
                
                // [IN014] Start : Change Parts No. and totalShipQtyByPart still not zero
                if (null != previousRecord 
                    && null != previousRecord.getTempShippingQty()
                    && totalShipQtyByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
                {
                    BigDecimal previousShipping = previousRecord.getTempShippingQty()
                        .add(totalShipQtyByPart);
                    previousRecord.setCurrentOrderQty(defaultFormat.format(previousShipping));
                    previousRecord.setCurrentOrderQtyNoComma(
                        decimalFormat.format(previousShipping));
                    // [IN039] Start : Shiping box calculate by FIFO
                    //BigDecimal previousShippingBoxQty = previousShipping.divide(
                    //    previousRecord.getQtyBox(), Constants.ZERO, RoundingMode.CEILING);
                    //previousRecord.setNoOfBoxes(previousShippingBoxQty);
                    //previousRecord.setStringNoOfBoxes(noDecimalFormat.format(
                    //    previousShippingBoxQty.setScale(Constants.ZERO, RoundingMode.CEILING)));
                    previousRecord = null;
                }
                // [IN014] End : Change Parts No. and totalShipQtyByPart still not zero
                
                // [IN039] Start : Shiping box calculate by FIFO
                if (null != previousRecordForBox 
                    && totalShipBoxByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
                {
                    BigDecimal previousShippingBoxQty = previousRecordForBox.getNoOfBoxes()
                        .add(totalShipBoxByPart);
                    previousRecordForBox.setNoOfBoxes(previousShippingBoxQty);
                    previousRecordForBox.setStringNoOfBoxes(noDecimalFormat.format(
                        previousShippingBoxQty.setScale(Constants.ZERO, RoundingMode.CEILING)));
                    previousRecordForBox = null;
                }
                // [IN039] End : Shiping box calculate by FIFO
                
                totalShipQtyByPart = asnMaintenance.getShippingQtyByPart();
                chgReasonCdByPart = asnMaintenance.getChangeReasonCd();
                bufferChgReasonCd = asnMaintenance.getChangeReasonCd();

                // [IN039] Shiping box calculate by FIFO
                totalShipBoxByPart = NumberUtil.toBigDecimalDefaultZero(
                    asnMaintenance.getShippingBoxQtyByPartInput());
            } else {
                chgReasonCdByPart = bufferChgReasonCd;
            }
            
            if (Constants.STR_ZERO.equals(asnMaintenanceDomain.getTemporaryMode())) {
                remainQty = asnMaintenance.getAsnReceivedQty();
            } else {
                remainQty = asnMaintenance.getTotalRemainingQty();
            }
            
            if(Constants.ZERO < totalShipQtyByPart.compareTo(BigDecimal.ZERO)) {
                if(remainQty.compareTo(totalShipQtyByPart) <= Constants.ZERO)
                {
                    shippingQty = remainQty;
                    totalShipQtyByPart = totalShipQtyByPart.subtract(remainQty);
                    chgReasonCdByPart = Constants.EMPTY_STRING;
                } else {
                    shippingQty = totalShipQtyByPart;
                    totalShipQtyByPart = new BigDecimal(Constants.ZERO);
                }
                // Start : [IN039] Use Shipping Box Quantity use FIFO not calculate each row
                //shippingBoxQty = shippingQty.divide(asnMaintenance.getAsnDetailDomain().getQtyBox(),
                //    Constants.ZERO, RoundingMode.CEILING);
                BigDecimal calculateShipBox = shippingQty.divide(
                    asnMaintenance.getAsnDetailDomain().getQtyBox()
                        , Constants.ZERO, RoundingMode.CEILING);
                
                if(calculateShipBox.compareTo(totalShipBoxByPart) <= Constants.ZERO
                    && Constants.ZERO != BigDecimal.ZERO.compareTo(totalShipQtyByPart))
                {
                    shippingBoxQty = calculateShipBox;
                    totalShipBoxByPart = totalShipBoxByPart.subtract(calculateShipBox);
                } else {
                    shippingBoxQty = totalShipBoxByPart;
                    totalShipBoxByPart = new BigDecimal(Constants.ZERO);
                }
                // End : [IN039] Use Shipping Box Quantity use FIFO not calculate each row
            } else {
                continue;
            }
            
            record = new AsnReportDomain();
            
            // [IN014] Keep previous record
            previousRecord = record;
            
            // [IN039] Keep previous record for calculate shipping box
            previousRecordForBox = record;
            
            currentRcv = this.checkNullToEmpty(spsTAsnDetail.getRcvLane());
            eachRcvList = rcvMap.get(currentRcv);
            if (null == eachRcvList) {
                eachRcvList = new ArrayList<AsnReportDomain>();
                rcvMap.put(currentRcv, eachRcvList);
            }
            eachRcvList.add(record);
            
            record.setRcvLane(currentRcv);
            
            // [FIX] Preview Mode must not show ASN No
            //record.setAsnNo(this.checkNullToEmpty(asnMaintenanceDomain.getAsnNo()));
            record.setAsnNo(this.checkNullToEmpty(Constants.EMPTY_STRING));
            
            record.setSCd(this.checkNullToEmpty(asnMaintenance.getSCd()));
            record.setSPcd(this.checkNullToEmpty(asnMaintenance.getSPcd()));
            record.setDPcd(this.checkNullToEmpty(asnMaintenance.getDPcd()));
            record.setDPn(this.checkNullToEmpty(asnMaintenance.getDPn()));
            record.setSPn(this.checkNullToEmpty(asnMaintenance.getSPn()));

            record.setVendorCd(spsTAsn.getVendorCd());
            record.setSupplierName(this.checkNullToEmpty(supplier.getCompanyName()));
            record.setEtdDate(etd);
            record.setEtdTime(etdSchedule);
            record.setEtaDate(eta);
            record.setEtaTime(etaSchedule);
            record.setTripNo(tripNo);
            record.setNumberOfPallet(stringNumberOfPallet);
            record.setIssueDate(Constants.EMPTY_STRING);
            record.setLastModified(Constants.EMPTY_STRING);
            record.setFlag(Constants.EMPTY_STRING);
            
            record.setRouteNo(this.checkNullToEmpty(spsTDo.getTruckRoute()));
            if (null != spsTDo.getTruckSeq()) {
                record.setDelNo(noDecimalFormat.format(spsTDo.getTruckSeq()));
            } else {
                record.setDelNo(Constants.EMPTY_STRING);
            }
            //[SPS007]
            record.setSupplierLocation(this.checkNullToEmpty(supplierPlant.getPlantName()));
            
            record.setWarehouse(this.checkNullToEmpty(spsTDo.getWhPrimeReceiving()));
            record.setDockCode(this.checkNullToEmpty(spsTDo.getDockCode()));
            record.setTrans(this.checkNullToEmpty(spsTDo.getTm()));
            record.setCycle(this.checkNullToEmpty(spsTDo.getCycle()));
            
            if (Constants.STR_ONE.equals(spsTDo.getOrderMethod())) {
                record.setOrderMethod(Constants.WORD_KANBAN_UPPER);
            } else {
                record.setOrderMethod(Constants.WORD_FIXED_UPPER);
            }
            
            if (!Strings.judgeBlank(spsTDo.getOrderMethod())) {
                orderMethodSet.add(spsTDo.getOrderMethod());
            }
            
            record.setCtrlNo(this.checkNullToEmpty(spsTDoDetail.getCtrlNo()));
            record.setDescription(this.checkNullToEmpty(spsTDoDetail.getItemDesc()));
            
            record.setUm(this.checkNullToEmpty(spsTAsnDetail.getUnitOfMeasure()));
            record.setQtyBox(spsTAsnDetail.getQtyBox());
            record.setNoOfBoxes(shippingBoxQty);
            
            // [IN009] Rounding Mode use HALF_UP
            //record.setStringQtyBox(noDecimalFormat.format(spsTAsnDetail.getQtyBox()));
            //record.setStringNoOfBoxes(noDecimalFormat.format(shippingBoxQty));
            record.setStringQtyBox(NumberUtil.formatRoundingHalfUp(
                noDecimalFormat, spsTAsnDetail.getQtyBox(), Constants.ZERO));
            record.setStringNoOfBoxes(noDecimalFormat.format(shippingBoxQty.setScale(
                Constants.ZERO, RoundingMode.CEILING)));
            
            if (null != shippingQty) {
                record.setCurrentOrderQty(defaultFormat.format(shippingQty));
                record.setCurrentOrderQtyNoComma(decimalFormat.format(shippingQty));
                
                // [IN014] Start : Change Parts No. and totalShipQtyByPart still not zero
                record.setTempShippingQty(shippingQty);
            }
            
            record.setSpsDoNo(this.checkNullToEmpty(spsTAsnDetail.getSpsDoNo()));
            record.setCigmaDoNo(this.checkNullToEmpty(spsTAsnDetail.getCigmaDoNo()));
            record.setRsn(this.checkNullToEmpty(chgReasonCdByPart));
            
            // Get KANBAN SEQ list.
            SpsTDoKanbanSeqCriteriaDomain kanbanCriteria = new SpsTDoKanbanSeqCriteriaDomain();
            kanbanCriteria.setDoId(spsTAsnDetail.getDoId());
            kanbanCriteria.setSPn(asnMaintenance.getSPn());
            List<SpsTDoKanbanSeqDomain> kanbanSeqList
                = this.spsTDoKanbanSeqDao.searchByCondition(kanbanCriteria);
            List<BigDecimal> kanbanSeqNoList = new ArrayList<BigDecimal>();
            int index = Constants.ZERO;
            boolean notYetAdd = true;
            for (SpsTDoKanbanSeqDomain kanbanSeq : kanbanSeqList) {
                if (null == kanbanSeq.getKanbanSeqNo()) {
                    continue;
                }
                notYetAdd = true;
                index = Constants.ZERO;
                for (BigDecimal kanbanSeqNo : kanbanSeqNoList) {
                    if (kanbanSeq.getKanbanSeqNo().compareTo(kanbanSeqNo) <= Constants.ZERO) {
                        kanbanSeqNoList.add(index, kanbanSeq.getKanbanSeqNo());
                        notYetAdd = false;
                        break;
                    }
                    index++;
                }
                if (notYetAdd) {
                    kanbanSeqNoList.add(kanbanSeq.getKanbanSeqNo());
                }
            }
            
            kanbanBuffer.setLength(Constants.ZERO);
            if (Constants.ZERO < kanbanSeqNoList.size()) {
                kanbanBuffer.append(Constants.SYMBOL_OPEN_BRACKET);
                for (BigDecimal kanbanSeqNo : kanbanSeqNoList) {
                    kanbanBuffer.append(kanbanSeqNo.toString()).append(Constants.SYMBOL_COMMA);
                }
                kanbanBuffer.setLength(kanbanBuffer.length() - Constants.ONE);
                kanbanBuffer.append(Constants.SYMBOL_CLOSE_BRACKET);
            }
            
            record.setKanbanSeqNo(kanbanBuffer.toString());
            
        }
        
        // [IN014] Start : End Loop and total ship quantity of last Parts No. is not zero
        if (null != previousRecord 
            && null != previousRecord.getTempShippingQty()
            && totalShipQtyByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
        {
            BigDecimal previousShipping = previousRecord.getTempShippingQty()
                .add(totalShipQtyByPart);
            previousRecord.setCurrentOrderQty(defaultFormat.format(previousShipping));
            previousRecord.setCurrentOrderQtyNoComma(
                decimalFormat.format(previousShipping));
            // [IN039] Start : Shiping box calculate by FIFO
            //BigDecimal previousShippingBoxQty = previousShipping.divide(
            //    previousRecord.getQtyBox(), Constants.ZERO, RoundingMode.CEILING);
            //previousRecord.setNoOfBoxes(previousShippingBoxQty);
            //previousRecord.setStringNoOfBoxes(noDecimalFormat.format(
            //    previousShippingBoxQty.setScale(Constants.ZERO, RoundingMode.CEILING)));
            previousRecord = null;
        }
        // [IN014] End : End Loop and total ship quantity of last Parts No. is not zero
        
        // [IN039] Start : End Loop and total ship box quantity of last Parts No. is not zero
        if (null != previousRecordForBox 
            && totalShipBoxByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
        {
            BigDecimal previousShippingBoxQty = previousRecordForBox.getNoOfBoxes()
                .add(totalShipBoxByPart);
            previousRecordForBox.setNoOfBoxes(previousShippingBoxQty);
            previousRecordForBox.setStringNoOfBoxes(noDecimalFormat.format(
                previousShippingBoxQty.setScale(Constants.ZERO, RoundingMode.CEILING)));
            previousRecordForBox = null;
        }
        // [IN039] End : End Loop and total ship box quantity of last Parts No. is not zero
        
        List<AsnReportDomain> recordList = new ArrayList<AsnReportDomain>();
        
        // Generate QR code for each RCV Lane
        String receiveByScan = asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO).getGroupDoDomain().getReceiveByScan();
        this.generateForEachRcvLane(recordList, rcvMap, denso.getQrType(), receiveByScan, asnMaintenanceDomain.getDCd());
        
        // Set QR Warning
        String qrWarning = null;
        if (Constants.STR_ONE.equals(denso.getQrType())) {
            qrWarning = Constants.EMPTY_STRING;
        } else if (Constants.STR_TWO.equals(denso.getQrType())) {
            qrWarning = MessageUtil.getReportLabel(asnMaintenanceDomain.getLocale(),
                SupplierPortalConstant.LBL_RSHP001_QR_REMARK_TYPE2);
        } else if (Constants.STR_THREE.equals(denso.getQrType())) {
            qrWarning = MessageUtil.getReportLabel(asnMaintenanceDomain.getLocale(),
                SupplierPortalConstant.LBL_RSHP001_QR_REMARK_TYPE3);
        }
        for (AsnReportDomain asnDetail : recordList) {
            List<AsnReportDomain> rcvRecordList = rcvMap.get(asnDetail.getRcvLane());
            asnDetail.setRcvLaneCount(rcvRecordList.size());
            asnDetail.setQrWarning(qrWarning);
        }

        Map<String, Object> parameters = this.getAsnReportParameter(
            asnMaintenanceDomain.getLocale());

        // Set Order Method
        this.setOrderMethodForAsnReport(parameters, orderMethodSet);

        parameters.put(SupplierPortalConstant.RPT_PRM_DENSO_COMPANY, densoBuffer.toString());
        parameters.put(SupplierPortalConstant.RPT_PRM_TOTAL_RECORD,
            Integer.valueOf(recordList.size()));

        this.generateRsnCaption(parameters);
        
        SortUtil.sort(recordList, SortUtil.COMPARE_ASN_REPORT);
        
        JasperPrint jpPage = generateReport(getReportPath(), recordList, parameters);
        
        List<JasperPrint> printList = new ArrayList<JasperPrint>();
        printList.add(jpPage);
        return generate(printList);
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchAsnForGroupInvoice(com.globaldenso.asia.sps.business.domain.AsnDomain)
     */
    public List<AsnDomain> searchAsnForGroupInvoice(AsnDomain asnDomain){
        List<AsnDomain> asnList = null;
        asnList = this.asnDao.searchAsnForGroupInvoice(asnDomain);
        for(AsnDomain item:asnList){
            item.setPlanEta(
                DateUtil.format(item.getSpsTAsnDomain().getPlanEta(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
        }
        return asnList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchPurgingAsnOrder(com.globaldenso.asia.sps.business.domain.PurgingDODomain)
     */
    public List<PurgingAsnDomain> searchPurgingAsnOrder(PurgingDoDomain purgingDoDomain){
        List<PurgingAsnDomain> purgingAsnList = null;
        purgingAsnList = this.asnDao.searchPurgingAsnOrder(purgingDoDomain);
        return purgingAsnList;
    }
    
    // [IN012] For check delete P/O, D/O
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchCountNotPurgingAsn(com.globaldenso.asia.sps.business.domain.PurgingDODomain)
     */
    public Integer searchCountNotPurgingAsn(PurgingDoDomain purgingDoDomain){
        Integer result = null;
        result = this.asnDao.searchCountNotPurgingAsn(purgingDoDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchSumShippingQty(com.globaldenso.asia.sps.business.domain.SpsTAsnDetailDomain)
     */
    public List<SpsTAsnDetailDomain> searchSumShippingQty(SpsTAsnDetailDomain spsTAsnDetailDomain){
        List<SpsTAsnDetailDomain> SpsTAsnDetailList = null;
        SpsTAsnDetailList = this.asnDao.searchSumShippingQty(spsTAsnDetailDomain);
        return SpsTAsnDetailList;
    }
    
    /**
     * Get path of Jasper report file for ASN Report.
     * @return Jasper Report file path for ASN Report
     * @throws Exception and Exception
     * */
    @Override
    protected String getReportPath() throws Exception {
        return StringUtil.appendsString(ContextParams.getJasperFilePath(),
            SupplierPortalConstant.ASN_REPORT_JASPER_FILE_NAME);
    }
    
    /** Get all label from Report Label Properties file and set to RSHP001 report parameter map.
     * @param locale for select language
     * @return parameter mapping.
     * */
    private Map<String, Object> getAsnReportParameter(Locale locale) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put(SupplierPortalConstant.RPT_PRM_REPORT_NAME, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_REPORT_NAME));
        param.put(SupplierPortalConstant.RPT_PRM_ASN_NO, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_ASN_NO));
        param.put(SupplierPortalConstant.RPT_PRM_NO, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_NO));
        param.put(SupplierPortalConstant.RPT_PRM_CODE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_CODE));
        param.put(SupplierPortalConstant.RPT_PRM_ROUTE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_ROUTE));
        param.put(SupplierPortalConstant.RPT_PRM_DEL, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_DEL));
        param.put(SupplierPortalConstant.RPT_PRM_SUPPLIER, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SUPPLIER));
        param.put(SupplierPortalConstant.RPT_PRM_SUP_NAME, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SUP_NAME));
        param.put(SupplierPortalConstant.RPT_PRM_SUP_LOC, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SUP_LOC));
        param.put(SupplierPortalConstant.RPT_PRM_SUP_PLANT, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SUP_PLANT));
        param.put(SupplierPortalConstant.RPT_PRM_PAGE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_PAGE));
        param.put(SupplierPortalConstant.RPT_PRM_G_PAGE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_G_PAGE));
        param.put(SupplierPortalConstant.RPT_PRM_A_ETD, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_A_ETD));
        param.put(SupplierPortalConstant.RPT_PRM_SCHEDULE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SCHEDULE));
        param.put(SupplierPortalConstant.RPT_PRM_TIME, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_TIME));
        param.put(SupplierPortalConstant.RPT_PRM_P_ETA, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_P_ETA));
        param.put(SupplierPortalConstant.RPT_PRM_PLANT, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_PLANT));
        param.put(SupplierPortalConstant.RPT_PRM_WARE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_WARE));
        param.put(SupplierPortalConstant.RPT_PRM_HOUSE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_HOUSE));
        param.put(SupplierPortalConstant.RPT_PRM_DOCK, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_DOCK));
        param.put(SupplierPortalConstant.RPT_PRM_RCV, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_RCV));
        param.put(SupplierPortalConstant.RPT_PRM_LANE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_LANE));
        param.put(SupplierPortalConstant.RPT_PRM_TRANS, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_TRANS));
        param.put(SupplierPortalConstant.RPT_PRM_CYCLE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_CYCLE));
        param.put(SupplierPortalConstant.RPT_PRM_TRIP_NO, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_TRIP_NO));
        param.put(SupplierPortalConstant.RPT_PRM_ISSUE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_ISSUE));
        param.put(SupplierPortalConstant.RPT_PRM_DATE, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_DATE));
        param.put(SupplierPortalConstant.RPT_PRM_LAST, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_LAST));
        param.put(SupplierPortalConstant.RPT_PRM_MODIFIED, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_MODIFIED));
        param.put(SupplierPortalConstant.RPT_PRM_CTRL, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_CTRL));
        param.put(SupplierPortalConstant.RPT_PRM_F, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_F));
        param.put(SupplierPortalConstant.RPT_PRM_D_PART_NUMBER, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_D_PART_NUMBER));
        param.put(SupplierPortalConstant.RPT_PRM_S_PART_NUMBER, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_S_PART_NUMBER));
        param.put(SupplierPortalConstant.RPT_PRM_DESCRIPTION, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_DESCRIPTION));
        param.put(SupplierPortalConstant.RPT_PRM_UM, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_UM));
        param.put(SupplierPortalConstant.RPT_PRM_ORDER, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_ORDER));
        param.put(SupplierPortalConstant.RPT_PRM_METHOD, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_METHOD));
        param.put(SupplierPortalConstant.RPT_PRM_QUANTITY, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_QUANTITY));
        param.put(SupplierPortalConstant.RPT_PRM_BOX, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_BOX));
        param.put(SupplierPortalConstant.RPT_PRM_NO_OF, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_NO_OF));
        param.put(SupplierPortalConstant.RPT_PRM_CURRENT, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_CURRENT));
        param.put(SupplierPortalConstant.RPT_PRM_QTY, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_QTY));
        param.put(SupplierPortalConstant.RPT_PRM_SPS_DO_NO, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SPS_DO_NO));
        param.put(SupplierPortalConstant.RPT_PRM_CIGMA_DO_NO, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_CIGMA_DO_NO));
        param.put(SupplierPortalConstant.RPT_PRM_DRIVER, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_DRIVER));
        param.put(SupplierPortalConstant.RPT_PRM_RECV, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_RECV));
        param.put(SupplierPortalConstant.RPT_PRM_CHECK, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_CHECK));
        param.put(SupplierPortalConstant.RPT_PRM_TOTAL_NO_OF, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_TOTAL_NO_OF));
        param.put(SupplierPortalConstant.RPT_PRM_FOR, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_FOR));
        param.put(SupplierPortalConstant.RPT_PRM_BOXES, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_BOXES));
        param.put(SupplierPortalConstant.RPT_PRM_PALLET, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_PALLET));
        param.put(SupplierPortalConstant.RPT_PRM_SUP_DEL, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SUP_DEL));
        param.put(SupplierPortalConstant.RPT_PRM_DENSO_REC, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_DENSO_REC));
        param.put(SupplierPortalConstant.RPT_PRM_SHIPPING, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_SHIPPING));
        param.put(SupplierPortalConstant.RPT_PRM_RSN, MessageUtil.getReportLabel(
            locale, SupplierPortalConstant.LBL_RSHP001_RSN));
        return param;
    }
    
    /**
     * Generate RSN Caption for ASN report.
     * @param param - Parameter Map for Jasper Report
     * */
    private void generateRsnCaption(Map<String, Object> param) {
        StringBuffer result1 = new StringBuffer();
        StringBuffer result2 = new StringBuffer();
        
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_CHANGE_REASON_CB);
        List<MiscellaneousDomain> rsnList = miscDao.searchMisc(miscDomain);
        if (null == rsnList || Constants.ZERO == rsnList.size()) {
            return ;
        }
        
        MiscellaneousDomain rsn = null;
        for (int i = 0 ; i < rsnList.size(); i++) {
            rsn = rsnList.get(i);
            if (Constants.ZERO == i % Constants.TWO) {
                result1.append(rsn.getMiscCode()).append(Constants.SYMBOL_SPACE)
                    .append(Constants.SYMBOL_COLON).append(Constants.SYMBOL_SPACE)
                    .append(rsn.getMiscValue()).append(Constants.SYMBOL_CR_LF);
            } else {
                result2.append(rsn.getMiscCode()).append(Constants.SYMBOL_SPACE)
                    .append(Constants.SYMBOL_COLON).append(Constants.SYMBOL_SPACE)
                    .append(rsn.getMiscValue()).append(Constants.SYMBOL_CR_LF);
            }
        }

        param.put(SupplierPortalConstant.RPT_PRM_RSN_CAPTION_1, result1.toString());
        param.put(SupplierPortalConstant.RPT_PRM_RSN_CAPTION_2, result2.toString());
    }
    
    
    /**
     * Check target String if null or empty will return Empty String.
     * @param target the target to check
     * @return target or alternate
     * */
    private String checkNullToEmpty(String target) {
        if (Strings.judgeBlank(target)) {
            return Constants.EMPTY_STRING;
        } else {
            return target.trim();
        }
    }
    
    /**
     * Generate QR Code, Del Number and Route Number.
     * @param recordList : list of record in Report
     * @param rcvMap : Map of record devide by RCV Lane
     * @param qrType : type of QR Code to generate
     * @param receiveByScan : Is generate QR for Kanban tag ?
     * @param dCd : To distinct company
     * @throws QrcodeGenerateException for Generate QR Code
     * */
    private void generateForEachRcvLane(List<AsnReportDomain> recordList,
        Map<String, List<AsnReportDomain>> rcvMap, String qrType, String receiveByScan, String dCd) throws QrcodeGenerateException
    {
        InputStream qrCodeReceiveByScan1 = null;
        InputStream qrCodeReceiveByScan2 = null;
        InputStream qrCodeReceiveByScan3 = null;
        InputStream qrCodeReceiveByScan4 = null;
        
        for (List<AsnReportDomain> rcvRecordList : rcvMap.values()) {
            InputStream qrCode = this.generateQrCode(qrType, rcvRecordList);
            /* Start generate ASN QR code for Receive By Scan*/
            if(rcvRecordList.size() > SupplierPortalConstant.MAX_RECORD_FOR_RECEIVE_BY_SCAN && receiveByScan.equals(Constants.STR_Y)){
                break;
            }
            List<AsnReportDomain> rcvLaneCount = rcvMap.get(rcvRecordList.get(Constants.ZERO).getRcvLane());
            if(receiveByScan.equals(Constants.STR_Y) && qrType.equals(Constants.STR_ONE)){
                qrCodeReceiveByScan1 = this.generateASNQrCode(rcvRecordList, rcvLaneCount.size(), Constants.ZERO, Constants.ONE * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, dCd);
                qrCodeReceiveByScan2 = this.generateASNQrCode(rcvRecordList, rcvLaneCount.size(), Constants.ONE * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, Constants.TWO * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, dCd);
                qrCodeReceiveByScan3 = this.generateASNQrCode(rcvRecordList, rcvLaneCount.size(), Constants.TWO * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, Constants.THREE * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, dCd);
                qrCodeReceiveByScan4 = this.generateASNQrCode(rcvRecordList, rcvLaneCount.size(), Constants.THREE * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, Constants.FOUR * SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN, dCd);
            }
            /* End generate ASN QR code for Receive By Scan*/
            Set<String> routeDelSet = new HashSet<String>();
            rcvRecordList.get(Constants.ZERO).setQrCode(qrCode);
            if(null != qrCodeReceiveByScan4){
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan1(qrCodeReceiveByScan1);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan2(qrCodeReceiveByScan2);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan3(qrCodeReceiveByScan3);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan4(qrCodeReceiveByScan4);
            }else if(null == qrCodeReceiveByScan1){
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan1(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan2(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan3(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan4(null);
            }else if(null == qrCodeReceiveByScan2){
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan1(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan2(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan3(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan4(qrCodeReceiveByScan1);
            } else if(null == qrCodeReceiveByScan3){
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan1(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan2(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan3(qrCodeReceiveByScan1);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan4(qrCodeReceiveByScan2);
            } else {
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan1(null);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan2(qrCodeReceiveByScan1);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan3(qrCodeReceiveByScan2);
                rcvRecordList.get(Constants.ZERO).setQrCodeReceiveByScan4(qrCodeReceiveByScan3);
            }
            
            for (AsnReportDomain asnRecord : rcvRecordList) {
                /*Insufficiency for allocate memory.
                asnRecord.setQrCode(qrCode);
                asnRecord.setQrCodeReceiveByScan1(qrCodeReceiveByScan1);
                asnRecord.setQrCodeReceiveByScan2(qrCodeReceiveByScan2);
                asnRecord.setQrCodeReceiveByScan3(qrCodeReceiveByScan3);
                asnRecord.setQrCodeReceiveByScan4(qrCodeReceiveByScan4);*/
                recordList.add(asnRecord);

                routeDelSet.add(StringUtil.appendsString(asnRecord.getRouteNo(),
                    Constants.SYMBOL_COLON, asnRecord.getDelNo()));
            }
            if (Constants.ONE != routeDelSet.size()) {
                for (AsnReportDomain asnRecord : rcvRecordList) {
                    asnRecord.setRouteNo(Constants.EMPTY_STRING);
                    asnRecord.setDelNo(Constants.EMPTY_STRING);
                }
            }
        }
    }

    /**
     * Generate QR Code for ASN.
     * @param recordList -  List of all record data
     * @param rcvLaneCount - Count of each RCV Lane
     * @param from - the index of recordList to start
     * @param to - the index of recordList to finish
     * @param dCd : To distinct company
     * @return Data in QR Code image
     * @throws QrcodeGenerateException 
     * */
    private InputStream generateASNQrCode(List<AsnReportDomain> recordList, Integer rcvLaneCount, Integer from, Integer to, String dCd) 
        throws QrcodeGenerateException{
        if(recordList.size() < from){
            return null;
        }
        
        StringBuffer content = new StringBuffer();
        Integer order = to / SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN;
        Integer total = recordList.size() / SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN;
        if(recordList.size() % SupplierPortalConstant.MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN > 0){
            total += 1;
        }
        List<SpsTDoDomain> spsTDoDomain = null;
        
        AsnReportDomain firstRecord = recordList.get(Constants.ZERO);
        SpsTDoCriteriaDomain criteria = new SpsTDoCriteriaDomain();
        //criteria.setCigmaDoNo(firstRecord.getCigmaDoNo());
        criteria.setSpsDoNo(firstRecord.getSpsDoNo());
        criteria.setVendorCd(firstRecord.getVendorCd());
        criteria.setLatestRevisionFlg(Constants.STR_ONE);
        //Thalerngsak add Start
        criteria.setDCd(dCd);
        //Thalerngsak add End
        if(firstRecord.getOrderMethod().equals(Constants.WORD_FIXED_UPPER)){
            criteria.setDataType(Constants.DDO_TYPE);
        } else {
            criteria.setDataType(Constants.KDO_TYPE);
        }
        try {
            spsTDoDomain = spsTDoDao.searchByCondition(criteria);
        } catch (ApplicationException e) {
            e.printStackTrace();
        }
        content.append(SupplierPortalConstant.QR_CODE_ASN_SYSTEM_ID)
            .append(SupplierPortalConstant.QR_CODE_ASN_TYPE)
            .append(fillData(firstRecord.getAsnNo().trim(),
                SupplierPortalConstant.LENGTH_ASN_NO))
            .append(fillData(firstRecord.getTrans().trim(),
                SupplierPortalConstant.LENGTH_TM))
            .append(fillData(firstRecord.getEtaDate().replaceAll(
                Constants.SYMBOL_SLASH, Constants.EMPTY_STRING).trim(),
                SupplierPortalConstant.LENGTH_ETD))
            .append(fillData(firstRecord.getEtdDate().replaceAll(
                Constants.SYMBOL_SLASH, Constants.EMPTY_STRING).trim(),
                SupplierPortalConstant.LENGTH_ETD))
            .append(fillData(firstRecord.getRcvLane().trim(),
                SupplierPortalConstant.LENGTH_RCV_LANE))
            .append(fillData(spsTDoDomain.get(Constants.ZERO).getDCd().trim(),
                SupplierPortalConstant.LENGTH_DENSO_COMPANY_CODE))
            .append(fillData(firstRecord.getDPcd().trim(),
                SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE))
            .append(fillData(firstRecord.getVendorCd().trim(),
                SupplierPortalConstant.LENGTH_SUPPLIER_CODE))
            .append(fillDataNumber(rcvLaneCount.toString().trim(),
                SupplierPortalConstant.LENGTH_RCV_LANE_COUNT,
                Constants.ZERO))
            .append(fillData(total.toString().trim(),
                SupplierPortalConstant.LENGTH_TOTAL_QR_CODE_ASN))
            .append(fillData(order.toString().trim(),
                SupplierPortalConstant.LENGTH_ORDER_QR_CODE_ASN))
            .append(fillData(spsTDoDomain.get(Constants.ZERO).getWhPrimeReceiving().trim(),
                SupplierPortalConstant.LENGTH_WAREHOUSE));
        //for (AsnReportDomain record : recordList) {
        for(Integer i = from; i < to; i++){
            if(recordList.size() <= i){
//                content.append(fillData(Constants.EMPTY_STRING, SupplierPortalConstant.LENGTH_CIGMA_DO_NO))
//                    .append(fillData(Constants.EMPTY_STRING, SupplierPortalConstant.LENGTH_DENSO_PART_NO))
//                    .append(fillData(Constants.EMPTY_STRING, SupplierPortalConstant.LENGTH_SHIPPING_QTY))
//                    .append(fillData(Constants.EMPTY_STRING, SupplierPortalConstant.LENGTH_KANBAN_TYPE));
                
            } else {
                AsnReportDomain record = recordList.get(i);
                
                //criteria.setCigmaDoNo(record.getCigmaDoNo());
//                criteria.setSpsDoNo(record.getSpsDoNo());
//                try {
//                    spsTDoDomain = spsTDoDao.searchByCondition(criteria);
//                } catch (ApplicationException e) {
//                    e.printStackTrace();
//                }
                criteria = new SpsTDoCriteriaDomain();
                criteria.setSpsDoNo(record.getSpsDoNo());
                criteria.setVendorCd(record.getVendorCd());
                criteria.setLatestRevisionFlg(Constants.STR_ONE);
                //Thalerngsak add Start
                criteria.setDCd(dCd);
                //Thalerngsak add End
                if(record.getOrderMethod().equals(Constants.WORD_FIXED_UPPER)){
                    criteria.setDataType(Constants.DDO_TYPE);
                } else {
                    criteria.setDataType(Constants.KDO_TYPE);
                }
                try {
                    spsTDoDomain = spsTDoDao.searchByCondition(criteria);
                } catch (ApplicationException e) {
                    e.printStackTrace();
                }
                SpsTDoDetailCriteriaDomain criteriaDetail = new SpsTDoDetailCriteriaDomain();
                List<SpsTDoDetailDomain> spsTDoDetailDomain = null;
                criteriaDetail.setDoId(spsTDoDomain.get(Constants.ZERO).getDoId());
                criteriaDetail.setDPn(record.getDPn());
                try {
                    spsTDoDetailDomain = spsTDoDetailDao.searchByCondition(criteriaDetail);
                } catch (ApplicationException e) {
                    e.printStackTrace();
                }
                content.append(fillData(record.getCigmaDoNo(), SupplierPortalConstant.LENGTH_CIGMA_DO_NO))
                    .append(fillData(record.getDPn(), SupplierPortalConstant.LENGTH_DENSO_PART_NO))
                    .append(fillDataNumber(record.getCurrentOrderQty(), SupplierPortalConstant.LENGTH_SHIPPING_QTY, SupplierPortalConstant.LENGTH_QTY_FLOATING))
                    .append(fillDataNumber(spsTDoDetailDomain.get(Constants.ZERO).getKanbanType(), SupplierPortalConstant.LENGTH_KANBAN_TYPE, Constants.ZERO));
//                BigDecimal shippingQty = new BigDecimal(record.getCurrentOrderQty().replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
//                BigDecimal qtyBox = record.getQtyBox();
//                BigDecimal result = shippingQty.remainder(qtyBox);
//                if(Constants.ZERO 
//                    < result.compareTo(new BigDecimal(Constants.ZERO)) 
//                    && !spsTDoDetailDomain.get(Constants.ZERO).getKanbanType().equals(Constants.FOUR)){
//                    content.append(fillData(Constants.STR_ONE, SupplierPortalConstant.LENGTH_IS_PARTIAL));
//                } else {
//                    content.append(fillData(Constants.STR_ZERO, SupplierPortalConstant.LENGTH_IS_PARTIAL));
//                }
            }
        }
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        QrcodeGenerator.generateQrcode(content.toString(), Constants.STR_M,
            Constants.HTTP_HEADER_UTF_8, Constants.QR_CODE_SIZE, baos);

        return new ByteArrayInputStream(baos.toByteArray());
    }

    /**
     * Generate QR Code and set to parameter map.
     * @param qrType - type of QR Code to generate
     * @param recordList -  List of all record data
     * @return Data in QR Code impage
     * @throws QrcodeGenerateException 
     * */
    private InputStream generateQrCode(String qrType, List<AsnReportDomain> recordList)
        throws QrcodeGenerateException
    {
        StringBuffer content = new StringBuffer();
        
        if (Constants.ZERO == recordList.size() || Constants.STR_ONE.equals(qrType)) {
            return null;
        }
        
        AsnReportDomain firstRecord = recordList.get(Constants.ZERO);
        
        if (Constants.STR_TWO.equals(qrType)) {
            String actualEtd = firstRecord.getEtdDate().replaceAll(
                Constants.SYMBOL_SLASH, Constants.EMPTY_STRING);
            if (SupplierPortalConstant.LENGTH_ETD < actualEtd.length()) {
                actualEtd = actualEtd.substring(Constants.ZERO, SupplierPortalConstant.LENGTH_ETD);
            }
            
            // Doc ID(1) + ASN No.(16) + CIGMA Vendor Code(6) + Plan Code(2) + Actual ETD(8)
            content.append(Constants.STR_A)
                .append(fillData(firstRecord.getAsnNo(), SupplierPortalConstant.LENGTH_ASN_NO))
                .append(fillData(firstRecord.getVendorCd(),
                    SupplierPortalConstant.LENGTH_CIGMA_VENDOR_CD))
                .append(fillData(firstRecord.getDPcd(), SupplierPortalConstant.LENGTH_PLANT_CODE))
                .append(fillData(actualEtd, SupplierPortalConstant.LENGTH_ETD));
            
        } else if (Constants.STR_THREE.equals(qrType)) {
            String issueDate = firstRecord.getIssueDate().replaceAll(
                Constants.SYMBOL_SLASH, Constants.EMPTY_STRING);
            if (SupplierPortalConstant.LENGTH_ISSUE_DATE < issueDate.length()) {
                issueDate = issueDate.substring(Constants.ZERO,
                    SupplierPortalConstant.LENGTH_ISSUE_DATE);
            }
            
            // ASN No.(16) + CIGMA Vendor Code(6) + ASN Issued date(8)
            content.append(fillData(firstRecord.getAsnNo(), SupplierPortalConstant.LENGTH_ASN_NO))
                .append(fillData(firstRecord.getVendorCd(),
                    SupplierPortalConstant.LENGTH_CIGMA_VENDOR_CD))
                .append(fillData(issueDate, SupplierPortalConstant.LENGTH_ISSUE_DATE));
        }
        
        List<String> doNoList = new ArrayList<String>();
        int partCount = 0;
        
        for (AsnReportDomain record : recordList) {
            if (Constants.STR_TWO.equals(qrType)) {
                if (!doNoList.contains(record.getCigmaDoNo())) {
                    doNoList.add(record.getCigmaDoNo());
                    if (SupplierPortalConstant.MAX_DO_NO_FOR_QR == doNoList.size()) {
                        break;
                    }
                }
            } else if (Constants.STR_THREE.equals(qrType)) {
                content
                    .append(fillData(record.getDPn(), SupplierPortalConstant.LENGTH_DENSO_PART_NO))
                    .append(fillDataNumber(record.getCurrentOrderQty(),
                        SupplierPortalConstant.LENGTH_QTY,
                        SupplierPortalConstant.LENGTH_QTY_FLOATING));
                partCount++;
                if (SupplierPortalConstant.MAX_PART_FOR_QR == partCount) {
                    break;
                }
            }
        }
        if (Constants.STR_TWO.equals(qrType)) {
            for (String doNo : doNoList) {
                content.append(fillData(doNo, SupplierPortalConstant.LENGTH_CIGMA_DO_NO));
            }
        }
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        QrcodeGenerator.generateQrcode(content.toString(), Constants.STR_M,
            Constants.HTTP_HEADER_UTF_8, Constants.QR_CODE_SIZE, baos);

        return new ByteArrayInputStream(baos.toByteArray());
    }
    
    /**
     * Set Order Method for Advance Shipping Notice report header.
     * @param parameters - Jasper Report parameter map
     * @param orderMethodSet - Set of order method in report
     * */
    private void setOrderMethodForAsnReport(Map<String, Object> parameters
        , Set<String> orderMethodSet)
    {
        StringBuffer orderMethodBuffer = new StringBuffer();
        if (Constants.TWO == orderMethodSet.size()) {
            orderMethodBuffer.append(Constants.WORD_FIXED).append(Constants.SYMBOL_SPACE)
                .append(Constants.SYMBOL_PLUS).append(Constants.SYMBOL_SPACE)
                .append(Constants.WORD_KANBAN).append(Constants.SYMBOL_SPACE)
                .append(Constants.WORD_ORDER);
        } else {
            if (Constants.STR_ZERO.equals(orderMethodSet.iterator().next())) {
                orderMethodBuffer.append(Constants.WORD_FIXED);
            } else {
                orderMethodBuffer.append(Constants.WORD_KANBAN);
            }
            orderMethodBuffer.append(Constants.SYMBOL_SPACE).append(Constants.WORD_ORDER);
        }
        parameters.put(SupplierPortalConstant.RPT_PRM_ORDER_METHOD,
            orderMethodBuffer.toString());
    }
    
    /**
     * If parameter input shorter than parameter len. Append input with space.
     * @param input - String input
     * @param len - target length
     * @return String that lenght equal to parameter len
     * */
    private String fillData(String input, int len) {
        StringBuffer result = new StringBuffer();
        result.append(input);
        for (int i = input.length(); i < len; i++) {
            result.append(Constants.SYMBOL_SPACE);
        }
        return result.toString();
    }

    /**
     * Split input to decimal part and floating part. If decimal part shorter than 
     * allLen - floatLen, fill with zero.<br />
     * If floating part shorter than floatLen, fill with zero.
     * @param input - String input
     * @param allLen - target length for whole string
     * @param floatLen - target length for floating part
     * @return String that lenght equal to parameter allLen
     * */
    private String fillDataNumber(String input, int allLen, int floatLen) {
        StringBuffer result = new StringBuffer();
        StringBuffer regexDot = new StringBuffer();
        // [.]
        regexDot.append(Constants.SYMBOL_OPEN_SQUARE_BRACKET).append(Constants.SYMBOL_DOT)
            .append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET);
        
        input = input.replaceAll(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
        
        String[] splitedInput = input.split(regexDot.toString());
        String decimal = splitedInput[Constants.ZERO];
        String floating = Constants.EMPTY_STRING;
        if (Constants.TWO == splitedInput.length) {
            floating = splitedInput[Constants.ONE];
        }
        
        int decLen = allLen - floatLen;
        int padding = decLen - decimal.length();
        for (int i = Constants.ZERO; i < padding; i++) {
            result.append(Constants.STR_ZERO);
        }
        result.append(decimal);
        
        result.append(floating);
        for (int j = floating.length(); j < floatLen; j++) {
            result.append(Constants.STR_ZERO);
        }
        
        return result.toString();
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnService#searchSumShippingQtyByPart(com.globaldenso.asia.sps.business.domain.SpsTAsnDetailDomain)
     */
    public SpsTAsnDetailDomain searchSumShippingQtyByPart(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        SpsTAsnDetailDomain asnDetailResult = null;
        asnDetailResult = asnDao.searchSumShippingQtyByPart(spsTAsnDetailDomain);
        return asnDetailResult;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceService#deletePurgingAsn(String)
     */
    public int deletePurgingAsn(String asnNo){
        return this.asnDao.deletePurgingAsn(asnNo);
    }
}