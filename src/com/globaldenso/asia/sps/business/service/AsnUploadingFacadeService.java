/*
 * ModifyDate Development company    Describe 
 * 2014/08/15 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.io.IOException;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.AsnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;

/**
 * <p>The Interface AsnUploadingFacadeService.</p>
 * <p>Facade for ASN uploading.</p>
 * <ul>
 * <li>Method delete  : deleteTmpUploadAsn</li>
 * <li>Method update  : transactUploadAsn</li>
 * <li>Method search  : searchTmpUploadAsnCsv</li>
 * <li>Method search  : searchByReturn</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnUploadingFacadeService {
    
    /**
     * <p>Delete Tmp Upload ASN</p>
     * <ul>
     * <li>Delete temp upload asn information</li>
     * </ul>
     * 
     * @param asnUploadingDomain the asn uploading domain
     * @return the int
     * @throws Exception Exception
     */
    public int deleteTmpUploadAsn(AsnUploadingDomain asnUploadingDomain) throws Exception;
    
    /**
     * <p>Transact Upload ASN</p>
     * <ul>
     * <li>Transact Upload ASN Information</li>
     * </ul>
     * 
     * @param asnUploadingCriteria the asn uploading domain
     * @return the asn uploading domain
     * @throws ApplicationException ApplicationException
     * @throws IOException IOException
     */
    public AsnUploadingDomain transactUploadAsn(AsnUploadingDomain asnUploadingCriteria) 
        throws ApplicationException, IOException;
    
    /**
     * <p>Search Tmp Upload Asn Csv</p>
     * <ul>
     * <li>Search tmp upload asn information for exporting to csv.</li>
     * </ul>
     * 
     * @param asnUploadingDomain the asn uploading domain
     * @throws ApplicationException ApplicationException
     * @throws Exception Exception
     */
    public void searchTmpUploadAsnCsv(AsnUploadingDomain asnUploadingDomain) 
        throws ApplicationException, Exception;
    
    /**
     * <p>Search by Return</p>
     * <ul>
     * <li>Search by Return for paging and return from wshp004 screen.</li>
     * </ul>
     * 
     * @param asnUploadingCriteria the asn uploading domain
     * @return the asn uploading domain
     * @throws ApplicationException ApplicationException
     */
    public AsnUploadingDomain searchByReturn(AsnUploadingDomain asnUploadingCriteria)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
    
    /**
     * Search DENSO Supplier Relation.
     * @param dataScopeControlDomain dataScopeControlDomain
     * @throws ApplicationException contain application message
     * */
    public void searchDensoSupplierRelation(
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException;
}