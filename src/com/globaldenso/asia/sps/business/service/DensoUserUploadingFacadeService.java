/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.io.IOException;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain;


/**
 * <p>The Interface DensoUserUploadingFacadeService.</p>
 * <p>Service for DENSO User Uploading about manipulate data form CSV file.</p>
 * <ul>
 * <li>Method search : initial</li>
 * <li>Method search : searchUploadErrorList</li>
 * <li>Method insert : transactUploadDensoUser</li>
 * <li>Method insert : transactRegisterUploadItem</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoUserUploadingFacadeService {
    
    /**
     * <p>initial for Supplier user uploading.</p>
     * <ul>
     * <li>Clear all data in temporary table.</li>
     * </ul>
     * @param densoUserUploadingDomain that keep information to clear the temporary data.
     * @throws ApplicationException the ApplicationException.
     */
    public void transactInitial(DensoUserUploadingDomain
        densoUserUploadingDomain) throws ApplicationException;
    
    /**
    * <p>Search Upload Error List.</p>
    * <ul>
    * <li>Clear the temporary data.</li>
    * </ul>
    * @param densoUserUploadingDomain It keep criteria to get data from temporary table.
    * @return list the list of Error information.
    * @throws ApplicationException the ApplicationException.
    */
    public  DensoUserUploadingDomain searchUploadErrorList(DensoUserUploadingDomain 
        densoUserUploadingDomain)throws ApplicationException;
        
    /**
     * <p>upload DENSO User.</p>
     * <ul>
     * <li>Validate DENSO User Item Detail from CSV file then display error result on screen.</li>
     * </ul>
     * 
     * @param densoUserUploadingDomain that keep all information for upload CSV file.
     * @return SupplierUserUploadingDomain that keep a summary of upload to show on screen.
     * @throws ApplicationException the ApplicationException
     * @throws IOException the IOException
     */
    public DensoUserUploadingDomain transactUploadDensoUser(DensoUserUploadingDomain 
        densoUserUploadingDomain) throws ApplicationException, IOException;
   
    /**
     * <p>Transact Register Upload Item.</p>
     * <ul>
     * <li>Insert supplier user detail from CSV file.</li>
     * </ul>
     * 
     * @param densoUserUploadingDomain that keep current user DSC ID and session code to move 
     * data from temporary to actual table.
     * @throws ApplicationException ApplicationException
     */
    public void transactRegisterUploadItem(DensoUserUploadingDomain densoUserUploadingDomain)
        throws ApplicationException;
    
    /**
     * <p>Delete Temporary Upload Item.</p>
     * <ul>
     * <li>Delete Temporary Upload Item before upload new item.</li>
     * </ul>
     * 
     * @param userDscId the Current user DSC ID.
     * @param sessionCode the session code.
     * @throws ApplicationException ApplicationException
     */
    public void deleteUploadTempTable(String userDscId, String sessionCode)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}