/*
 * ModifyDate Development company       Describe 
 * 2014/08/04 CSI Parichat              Create
 * 2015/08/26 CSI Akat                  [IN012]
 * 2015/09/02 CSI Akat                  [IN014]
 * 2016/01/05 CSI Akat                  [IN039]
 * 2016/02/03 CSI Akat                  [IN058] 
 * 2017/08/30 Netband U.Rungsiwut       Modify
 * 2018/04/11 Netband U.Rungsiwut       Generate ASN PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.FileManagementException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The class AsnMaintenanceFacadeServiceImpl.</p>
 * <p>Facade for ASN maintenance.</p>
 * <ul>
 * <li>Method search  : searchInitialWshp001</li>
 * <li>Method search  : searchInitialWshp008</li>
 * <li>Method search  : searchInitialWshp006</li>
 * <li>Method update  : transactSaveAndSendAsn</li>
 * <li>Method update  : transactCancelAsnAndSend</li>
 * <li>Method create  : createViewAsnReport</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchDownloadAsnReport</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method search  : searchRoleCanOperate</li>
 * <li>Method delete  : deleteTmpUploadAsn</li>
 * <li>Method search  : searchSendingEmail</li>
 * <li>Method search  : transactAllowReviseQty</li>
 * </ul>
 * 
 * @author CSI
 */
public class AsnMaintenanceFacadeServiceImpl implements AsnMaintenanceFacadeService {
    
    /** The delivery order service. */
    private DeliveryOrderService deliveryOrderService;
    
    /** The record limit service. */
    private RecordLimitService recordLimitService;
    
    /** The misc service. */
    private MiscellaneousService miscService;
    
    /** The unit of measure service. */
    private SpsMUnitOfMeasureService spsMUnitOfMeasureService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The asn runno service. */
    private SpsTAsnRunnoService spsTAsnRunnoService;
    
    /** The asn service. */
    private SpsTAsnService spsTAsnService;
    
    /** The asn detail service. */
    private SpsTAsnDetailService spsTAsnDetailService;
    
    /** The do service. */
    private SpsTDoService spsTDoService;
    
    /** The do detail service. */
    private SpsTDoDetailService spsTDoDetailService;
    
    /** The asn service. */
    private AsnService asnService;
    
    /** The user service. */
    private UserService userService;
    
    /** The user supplier service. */
    private UserSupplierService userSupplierService;
    
    /** The company service. */
    private CompanyDensoService companyDensoService; 
    
    /** The file management service. */
    private FileManagementService fileManagementService;
    
    /** The mail service. */
    private MailService mailService;
    
    /** The upload asn service. */
    private UploadAsnService uploadAsnService;
    
    /** The tmp upload asn service. */
    private SpsTmpUploadAsnService spsTmpUploadAsnService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new asn maintenance facade service impl.
     */
    public AsnMaintenanceFacadeServiceImpl(){
        super();
    }

    /**
     * Set the delivery order service.
     * 
     * @param deliveryOrderService the delivery order service to set
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }
    
    /**
     * Set the record limit service.
     * 
     * @param recordLimitService the record limit service to set
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    
    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the unit of measure service.
     * 
     * @param spsMUnitOfMeasureService the unit of measure service to set
     */
    public void setSpsMUnitOfMeasureService(
        SpsMUnitOfMeasureService spsMUnitOfMeasureService) {
        this.spsMUnitOfMeasureService = spsMUnitOfMeasureService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the asn runno service.
     * 
     * @param spsTAsnRunnoService the asn runno service to set
     */
    public void setSpsTAsnRunnoService(SpsTAsnRunnoService spsTAsnRunnoService) {
        this.spsTAsnRunnoService = spsTAsnRunnoService;
    }
    
    /**
     * Set the asn service.
     * 
     * @param spsTAsnService the asn service to set
     */
    public void setSpsTAsnService(SpsTAsnService spsTAsnService) {
        this.spsTAsnService = spsTAsnService;
    }

    /**
     * Set the asn detail service.
     * 
     * @param spsTAsnDetailService the asn details service to set
     */
    public void setSpsTAsnDetailService(SpsTAsnDetailService spsTAsnDetailService) {
        this.spsTAsnDetailService = spsTAsnDetailService;
    }
    
    /**
     * Set the do service.
     * 
     * @param spsTDoService the do service to set
     */
    public void setSpsTDoService(SpsTDoService spsTDoService) {
        this.spsTDoService = spsTDoService;
    }

    /**
     * Set the do detail service.
     * 
     * @param spsTDoDetailService the do detail service to set
     */
    public void setSpsTDoDetailService(SpsTDoDetailService spsTDoDetailService) {
        this.spsTDoDetailService = spsTDoDetailService;
    }
    
    /**
     * Set the asn service.
     * 
     * @param asnService the asn service to set
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Set the user service.
     * 
     * @param userService the user service to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    
    /**
     * Set the user supplier service.
     * 
     * @param userSupplierService the user service to set
     */
    public void setUserSupplierService(UserSupplierService userSupplierService) {
        this.userSupplierService = userSupplierService;
    }

    /**
     * Sets the company denso service.
     * 
     * @param companyDensoService the new company denso service
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * Sets the file management service.
     * 
     * @param fileManagementService the new file management service
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Sets the mail service.
     * 
     * @param mailService the new mail service
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
    
    /**
     * Sets the upload asn service.
     * 
     * @param uploadAsnService the new upload asn service
     */
    public void setUploadAsnService(UploadAsnService uploadAsnService) {
        this.uploadAsnService = uploadAsnService;
    }
    
    /**
     * Sets the tmp upload asn service.
     * 
     * @param spsTmpUploadAsnService the new tmp upload asn service
     */
    public void setSpsTmpUploadAsnService(
        SpsTmpUploadAsnService spsTmpUploadAsnService) {
        this.spsTmpUploadAsnService = spsTmpUploadAsnService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchInitialWshp001(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    public AsnMaintenanceDomain searchInitialWshp001(
        AsnMaintenanceDomain asnMaintenanceDomain) throws ApplicationException
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        AsnMaintenanceDomain criteria = null;
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        MiscellaneousDomain recordLimit = new MiscellaneousDomain();
        SpsMMiscDomain changeReasonCb = null;
        AsnMaintenanceReturnDomain asnDetail = null;
        List<SpsMMiscDomain> changeReasonCbList = new ArrayList<SpsMMiscDomain>();
        List<String> doIdList = new ArrayList<String>();
        List<AsnMaintenanceReturnDomain> asnDetailList 
            = new ArrayList<AsnMaintenanceReturnDomain>();
        
        if(Constants.ZERO < asnMaintenanceDomain.getDoInfoList().size()){
            for(AcknowledgedDoInformationReturnDomain doInfo : asnMaintenanceDomain.getDoInfoList())
            {
                if(StringUtil.checkNullOrEmpty(doInfo.getDeliveryOrderDomain().getDoId())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                        SupplierPortalConstant.LBL_SPS_DO_NO);
                }
                doIdList.add(String.valueOf(doInfo.getDeliveryOrderDomain().getDoId()));
            }
        }
        
        recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP004_RLM);
        recordLimit = recordLimitService.searchRecordLimit(recordLimit);
        if(null == recordLimit || StringUtil.checkNullOrEmpty(recordLimit.getMiscValue())){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        
        if(Integer.valueOf(recordLimit.getMiscValue())
            <  asnMaintenanceDomain.getDoInfoList().size()){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0049,
                new String[] {recordLimit.getMiscValue()});
        }
        
        if(Constants.ZERO < doIdList.size()){
            criteria = new AsnMaintenanceDomain();
            String doId = StringUtil.convertListToVarcharCommaSeperate(doIdList);
            criteria.setDoId(doId);
            // All status except 'CPS'
            
            // [IN012] search not include CPS and CCL status
            //criteria.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
            criteria.setShipmentStatusComplete(Constants.SHIPMENT_STATUS_CPS);
            criteria.setShipmentStatusCancel(Constants.SHIPMENT_STATUS_CCL);
            
            List<AsnMaintenanceReturnDomain> doGroupAsnResultList 
                = deliveryOrderService.searchDoGroupAsn(criteria);
            if(null != doGroupAsnResultList && Constants.ZERO < doGroupAsnResultList.size()){
                for(AsnMaintenanceReturnDomain doGroupAsn : doGroupAsnResultList){
                    asnDetail = new AsnMaintenanceReturnDomain();
                    asnDetail.setChangeReasonCdBackUp(Constants.EMPTY_STRING);
                    asnDetail.getAsnDomain().setSCd(doGroupAsn.getGroupDoDomain().getSCd());
                    asnDetail.getAsnDomain().setSPcd(doGroupAsn.getGroupDoDomain().getSPcd());
                    asnDetail.getAsnDomain().setDCd(doGroupAsn.getGroupDoDomain().getDCd());
                    asnDetail.getAsnDomain().setDPcd(doGroupAsn.getGroupDoDomain().getDPcd());
                    asnDetail.getAsnDomain().setCreateDatetime(
                        doGroupAsn.getGroupDoDomain().getCreateDatetime());
                    asnDetail.getAsnDomain().setLastUpdateDatetime(
                        doGroupAsn.getGroupDoDomain().getLastUpdateDatetime());
                    asnDetail.getAsnDomain().setVendorCd(
                        doGroupAsn.getGroupDoDomain().getVendorCd());
                    asnDetail.setSCd(doGroupAsn.getGroupDoDomain().getSCd());
                    asnDetail.setSPcd(doGroupAsn.getGroupDoDomain().getSPcd());
                    asnDetail.setDCd(doGroupAsn.getGroupDoDomain().getDCd());
                    asnDetail.setDPcd(doGroupAsn.getGroupDoDomain().getDPcd());
                    asnDetail.getGroupDoDomain().setLastUpdateDatetime(doGroupAsn.getGroupDoDomain()
                        .getLastUpdateDatetime());
                    asnDetail.getGroupDoDomain().setShipDatetime(doGroupAsn.getGroupDoDomain()
                        .getShipDatetime());
                    asnDetail.getGroupDoDomain().setOrderMethod(doGroupAsn.getGroupDoDomain()
                        .getOrderMethod());
                    asnDetail.getGroupDoDomain().setTruckRoute(doGroupAsn.getGroupDoDomain()
                        .getTruckRoute());
                    asnDetail.getGroupDoDomain().setTruckSeq(doGroupAsn.getGroupDoDomain()
                        .getTruckSeq());
                    asnDetail.getGroupDoDomain().setSupplierLocation(doGroupAsn.getGroupDoDomain()
                        .getSupplierLocation());
                    asnDetail.getGroupDoDomain().setWhPrimeReceiving(doGroupAsn.getGroupDoDomain()
                        .getWhPrimeReceiving());
                    asnDetail.getGroupDoDomain().setDockCode(doGroupAsn.getGroupDoDomain()
                        .getDockCode());
                    asnDetail.getGroupDoDomain().setTm(doGroupAsn.getGroupDoDomain().getTm());
                    asnDetail.getGroupDoDomain().setCycle(doGroupAsn.getGroupDoDomain().getCycle());
                    asnDetail.getGroupDoDomain().setReceiveByScan(doGroupAsn.getGroupDoDomain().getReceiveByScan());
                    asnDetail.setReceiveByScan(doGroupAsn.getReceiveByScan());
                    asnDetail.getGroupDoDetailDomain().setCtrlNo(doGroupAsn.getGroupDoDetailDomain()
                        .getCtrlNo());
                    asnDetail.getGroupDoDetailDomain().setItemDesc(doGroupAsn
                        .getGroupDoDetailDomain().getItemDesc());
                    asnDetail.getGroupDoDetailDomain().setLastUpdateDatetime(doGroupAsn
                        .getGroupDoDetailDomain().getLastUpdateDatetime());
                    asnDetail.setSPn(doGroupAsn.getGroupDoDetailDomain().getSPn());
                    asnDetail.setDPn(doGroupAsn.getGroupDoDetailDomain().getDPn());
                    asnDetail.getAsnDetailDomain().setSPn(doGroupAsn.getGroupDoDetailDomain()
                        .getSPn());
                    asnDetail.getAsnDetailDomain().setDPn(doGroupAsn.getGroupDoDetailDomain()
                        .getDPn());
                    asnDetail.getAsnDetailDomain().setDoId(doGroupAsn.getGroupDoDetailDomain()
                        .getDoId());
                    asnDetail.getAsnDetailDomain().setRcvLane(doGroupAsn.getGroupDoDetailDomain()
                        .getRcvLane());
                    asnDetail.getAsnDetailDomain().setQtyBox(doGroupAsn.getGroupDoDetailDomain()
                        .getQtyBox());
                    asnDetail.getAsnDetailDomain().setUnitOfMeasure(doGroupAsn
                        .getGroupDoDetailDomain().getUnitOfMeasure());
                    asnDetail.getAsnDetailDomain().setLastUpdateDatetime(doGroupAsn
                        .getGroupDoDetailDomain().getLastUpdateDatetime());
                    asnDetail.getAsnDetailDomain().setSpsDoNo(doGroupAsn.getGroupDoDomain()
                        .getSpsDoNo());
                    asnDetail.getAsnDetailDomain().setRevision(doGroupAsn.getGroupDoDomain()
                        .getRevision());
                    asnDetail.getAsnDetailDomain().setCigmaDoNo(doGroupAsn.getGroupDoDomain()
                        .getCigmaDoNo());
                    asnDetail.getAsnDetailDomain().setAllowReviseFlag(Constants.STR_ZERO);
                    asnDetail.getGroupDoDetailDomain().setChgCigmaDoNo(doGroupAsn
                        .getGroupDoDetailDomain().getChgCigmaDoNo());
                    asnDetail.getGroupDoDetailDomain().setCurrentOrderQty(doGroupAsn
                        .getGroupDoDetailDomain().getCurrentOrderQty());
                    asnDetail.getGroupDoDetailDomain().setKanbanType(doGroupAsn
                        .getGroupDoDetailDomain().getKanbanType());
                    asnDetail.setTotalShippedQty(doGroupAsn.getTotalShippedQty());
                    asnDetail.setRowCount(doGroupAsn.getRowCount());
                    asnDetail.setRecordGroup(doGroupAsn.getRecordGroup());
                    asnDetail.setUtc(doGroupAsn.getUtc());
                    asnDetail.setRevisingFlag(Constants.STR_ZERO);
                    asnDetail.setAsnStatus(Constants.EMPTY_STRING);
                    asnDetail.setAsnPnReceivingStatus(Constants.EMPTY_STRING);
                    asnDetail.setAsnReceivedQty(new BigDecimal(Constants.STR_ZERO));
                    asnDetail.getUserDomain().setFirstName(Constants.EMPTY_STRING);
                    asnDetail.getUserDomain().setMiddleName(Constants.EMPTY_STRING);
                    asnDetail.getUserDomain().setLastName(Constants.EMPTY_STRING);
                    asnDetail.getAs400VendorDomain().setVendorCd(Constants.EMPTY_STRING);
                    //set default cigma domain
                    asnDetail.getCigmaAsnDomain().setPseudoAsnNo(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoAsnStatus(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoReceivingLane(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoLastUpdate(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoLastUpdateTime(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoPnLastUpdate(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoPnLastUpdateTime(Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoPnReceivingStatus(
                        Constants.EMPTY_STRING);
                    asnDetail.getCigmaAsnDomain().setPseudoReceivedQty(Constants.STR_ZERO);
                    
                    BigDecimal totalRemainingQty = doGroupAsn.getGroupDoDetailDomain()
                        .getCurrentOrderQty().subtract(doGroupAsn.getTotalShippedQty());
                    asnDetail.getAsnDetailDomain().setShippingQty(totalRemainingQty);
                    asnDetail.getGroupDoDomain().setOneWayKanbanTagFlag(doGroupAsn.getGroupDoDomain()
                        .getOneWayKanbanTagFlag());
                    asnDetail.getGroupDoDomain().setTagOutput(doGroupAsn.getGroupDoDomain()
                        .getTagOutput());
                    asnDetailList.add(asnDetail);
                }
            }
        }
        
        if(Constants.ZERO < asnDetailList.size()){
            this.doSetAsnListToShow(asnDetailList, asnMaintenanceDomain);
        }else{
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }
        
        miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_CHANGE_REASON_CB);
        List<MiscellaneousDomain> chgReasonList = miscService.searchMisc(miscDomain);
        if(null == chgReasonList || Constants.ZERO == chgReasonList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_CHANGE_REASON);
        }else{
            for(MiscellaneousDomain chgReason : chgReasonList){
                changeReasonCb = new SpsMMiscDomain();
                changeReasonCb.setMiscCd(chgReason.getMiscCode());
                changeReasonCb.setMiscValue(chgReason.getMiscValue());
                changeReasonCbList.add(changeReasonCb);
            }
        }
        asnMaintenanceDomain.setDoGroupAsnList(asnDetailList);
        asnMaintenanceDomain.setChangeReasonList(changeReasonCbList);
        asnMaintenanceDomain.setSkipValidateLotSizeFlag(Constants.STR_ZERO);
        return asnMaintenanceDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchInitialWshp008(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    public AsnMaintenanceDomain searchInitialWshp008(AsnMaintenanceDomain asnMaintenanceDomain) 
        throws ApplicationException
    {
        Iterator asnItr = null;
        AsnInformationDomain asnInformationCriteria = null;
        SpsMMiscDomain changeReasonCb = null;
        SpsMCompanyDensoDomain companyDenso = new SpsMCompanyDensoDomain();
        List<AsnInformationDomain> asnInformationList = null;
        List<AsnMaintenanceReturnDomain> asnList = null;
        List<SpsMMiscDomain> changeReasonCbList = new ArrayList<SpsMMiscDomain>();
        
        Locale locale = asnMaintenanceDomain.getLocale();
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getAsnNo())){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                SupplierPortalConstant.LBL_ASN_NO);
        }
        asnInformationCriteria = new AsnInformationDomain();
        asnInformationCriteria.getSpsTAsnDomain().setAsnNo(asnMaintenanceDomain.getAsnNo());
        asnInformationCriteria.getSpsTAsnDomain().setDCd(asnMaintenanceDomain.getDCd());
        asnInformationList = asnService.searchAsnDetail(asnInformationCriteria);
        if(null == asnInformationList || asnInformationList.isEmpty()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }else{
            asnList = this.setNewAsnDetailInformationList(asnInformationList, null);
        }
        
        //Connect to CIGMA
        String densoCode = Constants.EMPTY_STRING;
        String asnNo = Constants.EMPTY_STRING;
        asnItr = asnList.iterator();
        List<String> densoCodeStrList = new ArrayList<String>();
        List<String> asnNoStrList = new ArrayList<String>();
        while(asnItr.hasNext()){
            AsnMaintenanceReturnDomain temp = (AsnMaintenanceReturnDomain)asnItr.next();
            if(StringUtil.checkNullOrEmpty(densoCode)){
                densoCode = temp.getDCd();
                densoCodeStrList.add(densoCode);
            }else{
                if(!densoCode.equals(temp.getDCd())){
                    densoCode = temp.getDCd();
                    densoCodeStrList.add(densoCode);
                }
            }
            if(StringUtil.checkNullOrEmpty(asnNo)){
                asnNo = temp.getAsnDomain().getAsnNo();
                asnNoStrList.add(asnNo);
            }else{
                if(!asnNo.equals(temp.getAsnDomain().getAsnNo())){
                    asnNo = temp.getAsnDomain().getAsnNo();
                    asnNoStrList.add(asnNo);
                }
            }
        }
        
        densoCode = StringUtil.convertListToVarcharCommaSeperate(densoCodeStrList);
        companyDenso.setDCd(densoCode);
        List<As400ServerConnectionInformationDomain> schemaResultList 
            = companyDensoService.searchAs400ServerList(companyDenso);
        
        if(null == schemaResultList || schemaResultList.isEmpty()){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0026);
        }
        
        for(As400ServerConnectionInformationDomain as400Server : schemaResultList)
        {
            asnNo = StringUtil.convertListToVarcharCommaSeperate(asnNoStrList);
            List<PseudoCigmaAsnDomain> cigmaAsnList = CommonWebServiceUtil.
                asnResourceSearchAsnReceiving(asnNo, null, as400Server, locale);
            
            for(AsnMaintenanceReturnDomain asn : asnList){
                String asnStatus = Constants.EMPTY_STRING;
                String pnReceivingStatus = Constants.EMPTY_STRING;
                BigDecimal asnReceivedQty = new BigDecimal(Constants.ZERO);
                PseudoCigmaAsnDomain cigmaAsnResult = new PseudoCigmaAsnDomain();
                //set default cigma asn information for numberFormat exception
                cigmaAsnResult.setPseudoReceivedQty(Constants.STR_ZERO);
                cigmaAsnResult.setPseudoLastUpdate(Constants.STR_ZERO);
                cigmaAsnResult.setPseudoLastUpdateTime(Constants.STR_ZERO);
                cigmaAsnResult.setPseudoPnLastUpdate(Constants.STR_ZERO);
                cigmaAsnResult.setPseudoPnLastUpdateTime(Constants.STR_ZERO);
                for(PseudoCigmaAsnDomain cigmaAsn : cigmaAsnList){
                    String cigmaCurDoNo = asn.getGroupDoDetailDomain().getChgCigmaDoNo();
                    if(StringUtil.checkNullOrEmpty(cigmaCurDoNo)){
                        cigmaCurDoNo = asn.getAsnDetailDomain().getCigmaDoNo();
                    }
                    if(asn.getAsnDomain().getAsnNo().trim().equals(cigmaAsn.getPseudoAsnNo().trim())
                        && asn.getAsnDetailDomain().getDPn().equals(cigmaAsn.getPseudoDPn().trim())
                        && cigmaCurDoNo.equals(cigmaAsn.getPseudoCigmaCurDoNo().trim()))
                    {
                        cigmaAsnResult = cigmaAsn;
                        asnReceivedQty = new BigDecimal(cigmaAsn.getPseudoReceivedQty());
                        asnStatus = cigmaAsn.getPseudoAsnStatus();
                        pnReceivingStatus = cigmaAsn.getPseudoPnReceivingStatus();
                        break;
                    }
                }
                if(!Constants.ASN_STATUS_CCL.equals(asn.getAsnDomain().getAsnStatus())){
                    if(!StringUtil.checkNullOrEmpty(asnStatus)){
                        if(Constants.ASN_STATUS_N.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_ISS;
                        }else if(Constants.ASN_STATUS_D.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_CCL;
                        }else if(Constants.ASN_STATUS_P.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_PTR;
                        }else{
                            asnStatus = Constants.ASN_STATUS_RCP;
                        }
                    }else{
                        asnStatus = asn.getAsnDomain().getAsnStatus();
                    }
                }else{
                    asnStatus = Constants.ASN_STATUS_CCL;
                }
                
                if(!Constants.ASN_STATUS_CCL.equals(asn.getAsnDomain().getAsnStatus())){
                    if(!StringUtil.checkNullOrEmpty(pnReceivingStatus)){
                        if(Constants.ASN_STATUS_N.equals(pnReceivingStatus)){
                            pnReceivingStatus = Constants.ASN_STATUS_ISS;
                        }else if(Constants.ASN_STATUS_D.equals(pnReceivingStatus)){
                            pnReceivingStatus = Constants.ASN_STATUS_CCL;
                        }else if(Constants.ASN_STATUS_P.equals(pnReceivingStatus)){
                            pnReceivingStatus = Constants.ASN_STATUS_PTR;
                        }else{
                            pnReceivingStatus = Constants.ASN_STATUS_RCP;
                        }
                    }else{
                        pnReceivingStatus = Constants.ASN_STATUS_ISS;
                    }
                }else{
                    pnReceivingStatus = Constants.ASN_STATUS_CCL;
                }
                asn.setAsnPnReceivingStatus(pnReceivingStatus);
                asn.setAsnStatus(asnStatus);
                asn.setAsnReceivedQty(asnReceivedQty);
                asn.setCigmaAsnDomain(cigmaAsnResult);
            }
        }
        
        if(Constants.ZERO < asnList.size()){
            this.doSetAsnListToShow(asnList, asnMaintenanceDomain);
            
            List<Boolean> reviseCompleteList = new ArrayList<Boolean>();
            AsnMaintenanceReturnDomain asnTmp = null;
            String revisingFlag = Constants.STR_ZERO;
            for(AsnMaintenanceReturnDomain asn : asnList){
                //Set revise shipping qty complete flag by RCV Lane
                if(Constants.STR_ONE.equals(asn.getRecordGroupByLane())){
                    if(null == asnTmp){
                        asnTmp = asn;
                    }else{
                        if(reviseCompleteList.contains(Boolean.FALSE)){
                            asnTmp.setReviseCompleteFlag(Constants.STR_ZERO);
                        }else{
                            asnTmp.setReviseCompleteFlag(Constants.STR_ONE);
                        }
                        asnTmp = asn;
                    }
                    reviseCompleteList.clear();
                }
                
                if(Constants.STR_ONE.equals(asn.getRecordGroup())){
                    if(Constants.STR_ONE.equals(asn.getAsnDetailDomain().getAllowReviseFlag())
                        && Constants.ZERO < asn.getShippingQtyByPart().compareTo(
                            asn.getAsnReceivedQtyByPart())){
                        revisingFlag = Constants.STR_ONE;
                    }else{
                        revisingFlag = Constants.STR_ZERO;
                    }
                    
                    if(Constants.ZERO == asn.getShippingQtyByPart().compareTo(
                        asn.getAsnReceivedQtyByPart())){
                        reviseCompleteList.add(Boolean.TRUE);
                    }else{
                        reviseCompleteList.add(Boolean.FALSE);
                    }
                }
                //Set revise shipping qty flag
                asn.setRevisingFlag(revisingFlag);
            }
            if(reviseCompleteList.contains(Boolean.FALSE)){
                asnTmp.setReviseCompleteFlag(Constants.STR_ZERO);
            }else{
                asnTmp.setReviseCompleteFlag(Constants.STR_ONE);
            }
            reviseCompleteList.clear();
        }
        
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_CHANGE_REASON_CB);
        List<MiscellaneousDomain> chgReasonList = miscService.searchMisc(miscDomain);
        if(null == chgReasonList || Constants.ZERO == chgReasonList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_CHANGE_REASON);
        }else{
            for(MiscellaneousDomain chgReason : chgReasonList){
                changeReasonCb = new SpsMMiscDomain();
                changeReasonCb.setMiscCd(chgReason.getMiscCode());
                changeReasonCb.setMiscValue(chgReason.getMiscValue());
                changeReasonCbList.add(changeReasonCb);
            }
        }
        
        AsnMaintenanceReturnDomain asn = asnList.get(Constants.ZERO);
        String actualEtd = DateUtil.format(new Date(asn.getAsnDomain().getActualEtd().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        String planEta = DateUtil.format(new Date(asn.getAsnDomain().getPlanEta().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        String lastModified = DateUtil.format(new Date(
            asn.getAsnDomain().getLastUpdateDatetime().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH); 
        String actualEtdDate = actualEtd.substring(Constants.ZERO, Constants.TEN);
        String actualEtdTime = actualEtd.substring(Constants.ELEVEN);
        String planEtaDate = planEta.substring(Constants.ZERO, Constants.TEN);
        String planEtaTime = planEta.substring(Constants.ELEVEN);
        
        asnMaintenanceDomain.setAsnRcvStatus(asn.getAsnStatus());
        asnMaintenanceDomain.setInvoiceStatus(StringUtil.nullToEmpty(asn.getInvoiceStatus()));
        asnMaintenanceDomain.setLastModified(lastModified);
        asnMaintenanceDomain.setActualEtdDate(actualEtdDate);
        asnMaintenanceDomain.setActualEtdTime(actualEtdTime);
        asnMaintenanceDomain.setPlanEtaDate(planEtaDate);
        asnMaintenanceDomain.setPlanEtaTime(planEtaTime);
        asnMaintenanceDomain.setNoOfPallet(String.valueOf(asn.getAsnDomain().getNumberOfPallet()));
        asnMaintenanceDomain.setTripNo(asn.getAsnDomain().getTripNo());
        asnMaintenanceDomain.setDoGroupAsnList(asnList);
        asnMaintenanceDomain.setChangeReasonList(changeReasonCbList);
        asnMaintenanceDomain.setSkipValidateLotSizeFlag(Constants.STR_ZERO);
        return asnMaintenanceDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchInitialWshp006(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    public AsnMaintenanceDomain searchInitialWshp006(AsnMaintenanceDomain asnMaintenanceDomain) 
        throws ApplicationException
    {
        MiscellaneousDomain criteria = null;
        SpsMMiscDomain changeReasonCb = null;
        List<SpsMMiscDomain> changeReasonCbList = new ArrayList<SpsMMiscDomain>();
        Locale locale = asnMaintenanceDomain.getLocale();
        
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getAsnNo())){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                SupplierPortalConstant.LBL_ASN_NO);
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getSessionId())){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                SupplierPortalConstant.LBL_SESSION_ID);
        }
        
        //Get Delivery Order data for grouping ASN from upload temporary table
        AsnMaintenanceDomain asnCriteria = new AsnMaintenanceDomain();
        asnCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
        asnCriteria.setSessionId(asnMaintenanceDomain.getSessionId());
        List<AsnMaintenanceReturnDomain> tempUploadAsnList
            = uploadAsnService.searchTmpUploadAsn(asnCriteria);
        if(null == tempUploadAsnList || tempUploadAsnList.isEmpty()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }
        
        //Gets doId from tmpUploadAsn
        Set<String> doIdSet = new TreeSet<String>();
        for(AsnMaintenanceReturnDomain tmpUploadAsnItem : tempUploadAsnList){
            doIdSet.add(tmpUploadAsnItem.getAsnDetailDomain().getDoId().toString());
        }
        
        criteria = new MiscellaneousDomain();
        criteria.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        criteria.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP004_RLM);
        MiscellaneousDomain recordLimit = recordLimitService.searchRecordLimit(criteria);
        if(null == recordLimit || StringUtil.checkNullOrEmpty(recordLimit.getMiscValue())){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        if(Integer.valueOf(recordLimit.getMiscValue()) < doIdSet.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0049,
                new String[] {recordLimit.getMiscValue()});
        }
        
        for(AsnMaintenanceReturnDomain tempUploadAsn : tempUploadAsnList){
            tempUploadAsn.setSCd(tempUploadAsn.getAsnDomain().getSCd());
            tempUploadAsn.setSPcd(tempUploadAsn.getAsnDomain().getSPcd());
            tempUploadAsn.setDCd(tempUploadAsn.getAsnDomain().getDCd());
            tempUploadAsn.setDPcd(tempUploadAsn.getAsnDomain().getDPcd());
            tempUploadAsn.setSPn(tempUploadAsn.getAsnDetailDomain().getSPn());
            tempUploadAsn.setDPn(tempUploadAsn.getAsnDetailDomain().getDPn());
            tempUploadAsn.setRevisingFlag(Constants.STR_ZERO);
            tempUploadAsn.setAsnStatus(Constants.EMPTY_STRING);
            tempUploadAsn.setAsnPnReceivingStatus(Constants.EMPTY_STRING);
            tempUploadAsn.setAsnReceivedQty(new BigDecimal(Constants.STR_ZERO));
            tempUploadAsn.getAsnDetailDomain().setAllowReviseFlag(Constants.STR_ZERO);
            //set default cigma domain
            tempUploadAsn.getCigmaAsnDomain().setPseudoAsnNo(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoAsnStatus(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoReceivingLane(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoLastUpdate(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoLastUpdateTime(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoPnLastUpdate(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoPnLastUpdateTime(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoPnReceivingStatus(Constants.EMPTY_STRING);
            tempUploadAsn.getCigmaAsnDomain().setPseudoReceivedQty(Constants.STR_ZERO);
            tempUploadAsn.setChangeReasonCdBackUp(Constants.EMPTY_STRING);
            /*BigDecimal totalRemainingQty = tempUploadAsn.getGroupDoDetailDomain()
                .getCurrentOrderQty().subtract(tempUploadAsn.getTotalShippedQty());*/
        }
        
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_CHANGE_REASON_CB);
        List<MiscellaneousDomain> chgReasonList = miscService.searchMisc(miscDomain);
        if(null != chgReasonList && Constants.ZERO < chgReasonList.size())
        {
            for(MiscellaneousDomain chgReason : chgReasonList){
                changeReasonCb = new SpsMMiscDomain();
                changeReasonCb.setMiscCd(chgReason.getMiscCode());
                changeReasonCb.setMiscValue(chgReason.getMiscValue());
                changeReasonCbList.add(changeReasonCb);
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_CHANGE_REASON);
            
        }
        if(Constants.ZERO < tempUploadAsnList.size()){
            this.doSetAsnListToShow(tempUploadAsnList, asnMaintenanceDomain);
        }
        AsnMaintenanceReturnDomain asn = tempUploadAsnList.get(Constants.ZERO);
        String actualEtd = DateUtil.format(new Date(asn.getAsnDomain().getActualEtd().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        String planEta = DateUtil.format(new Date(asn.getAsnDomain().getPlanEta().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        String lastModified = DateUtil.format(new Date(
            asn.getAsnDomain().getLastUpdateDatetime().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        String actualEtdDate = actualEtd.substring(Constants.ZERO, Constants.TEN);
        String actualEtdTime = actualEtd.substring(Constants.ELEVEN);
        String planEtaDate = planEta.substring(Constants.ZERO, Constants.TEN);
        String planEtaTime = planEta.substring(Constants.ELEVEN);
        
        asnMaintenanceDomain.setAsnRcvStatus(asn.getAsnStatus());
        asnMaintenanceDomain.setInvoiceStatus(StringUtil.nullToEmpty(asn.getInvoiceStatus()));
        asnMaintenanceDomain.setLastModified(lastModified);
        asnMaintenanceDomain.setActualEtdDate(actualEtdDate);
        asnMaintenanceDomain.setActualEtdTime(actualEtdTime);
        asnMaintenanceDomain.setPlanEtaDate(planEtaDate);
        asnMaintenanceDomain.setPlanEtaTime(planEtaTime);
        asnMaintenanceDomain.setNoOfPallet(String.valueOf(asn.getAsnDomain().getNumberOfPallet()));
        asnMaintenanceDomain.setTripNo(asn.getAsnDomain().getTripNo());
        asnMaintenanceDomain.setDoGroupAsnList(tempUploadAsnList);
        asnMaintenanceDomain.setChangeReasonList(changeReasonCbList);
        asnMaintenanceDomain.setSkipValidateLotSizeFlag(Constants.STR_ZERO);
        return asnMaintenanceDomain;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#transactSaveAndSendAsn(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    public boolean transactSaveAndSendAsn(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException, Exception
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        String hasRunNo = null;
        
        Map doIdMap = new HashMap();
        StringBuffer planEtaStr = new StringBuffer();
        StringBuffer actualEtdStr = new StringBuffer();
        StringBuffer actualEtaStr = new StringBuffer();
        
        AsnInformationDomain asnInformationCriteria = null;
        
        // [IN012] change D/O check status method
        //AsnMaintenanceDomain asnMaintenanceCriteria = null;
        SpsTDoDomain doStatusCriteria = null;
        
        SpsTAsnRunnoCriteriaDomain asnRunNoCriteria = null;
        SpsTDoDomain doDomain = null;
        SpsTAsnDomain asnDomain = null;
        SpsTDoCriteriaDomain doCriteria = null;
        SpsMCompanyDensoDomain companyDenso = new SpsMCompanyDensoDomain();
        SpsTAsnRunnoDomain asnRunNoDomain = null;
        SpsTDoDetailDomain doDetailDomain = null;
        SpsTAsnCriteriaDomain asnCriteria = null;
        SpsTAsnDetailDomain asnDetailDomain = null;
        SpsTDoDetailCriteriaDomain doDetailCriteria = null;
        SpsTAsnDetailCriteriaDomain asnDetailCriteria = null;
        List<SpsTDoDomain> doList = null;
        List<SpsTDoCriteriaDomain> doCriteriaList = null;
        List<AsnMaintenanceReturnDomain> doGroupAsnList = null;
        List<AsnMaintenanceReturnDomain> doGroupAsnResultList
            = new ArrayList<AsnMaintenanceReturnDomain>();
        List<AsnMaintenanceReturnDomain> asnShortShipList
            = new ArrayList<AsnMaintenanceReturnDomain>();
        List<AsnInformationDomain> asnInformationList = null;
        List<SpsTAsnDetailCriteriaDomain> asnDetailCriteriaList = null;
        List<SpsTAsnDetailCriteriaDomain> deletedAsnDetailCriteriaList = null;
        
        // [IN012] change D/O check status method
        //List<AsnMaintenanceDomain> criteriaDomain = new ArrayList<AsnMaintenanceDomain>();
        List<SpsTDoDomain> criteriaDomain = new ArrayList<SpsTDoDomain>();
        
        List<SpsTAsnDetailDomain> asnDetailList = new ArrayList<SpsTAsnDetailDomain>();
        List<SpsTDoDetailDomain> doDetailList = new ArrayList<SpsTDoDetailDomain>();
        List<SpsTDoDetailCriteriaDomain> doDetailCriteriaList 
            = new ArrayList<SpsTDoDetailCriteriaDomain>();
        List<ApplicationMessageDomain> warningMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        boolean resultValidateLotSize = this.validateLotSizeNotMatch(asnMaintenanceDomain);
        if(!resultValidateLotSize){
            asnMaintenanceDomain.setErrorMessageList(null);
            return false;
        }
        
        Map unitOfMeasureMap = this.generateUnitOfMeasureMap(locale);
        List<ApplicationMessageDomain> errorMessageList
            = this.validate(asnMaintenanceDomain, unitOfMeasureMap);
        
        if(Constants.ZERO < errorMessageList.size()){
            asnMaintenanceDomain.setErrorMessageList(errorMessageList);
            return false;
        }
        
        doGroupAsnList = asnMaintenanceDomain.getDoGroupAsnList();
        
        Timestamp updateDateTime = commonService.searchSysDate();
        int runNo = Constants.ZERO;
        String runningNoStr = Constants.EMPTY_STRING;
        StringBuffer asnNo = new StringBuffer();
        AsnMaintenanceReturnDomain doGroupAsn = doGroupAsnList.get(Constants.ZERO);
        
        if(Constants.STR_ONE.equals(asnMaintenanceDomain.getTemporaryMode()))
        {
            if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getAsnNo())){
                asnRunNoCriteria = new SpsTAsnRunnoCriteriaDomain();
                asnRunNoCriteria.setSCd(doGroupAsn.getSCd());
                asnRunNoCriteria.setDCd(doGroupAsn.getDCd());
                asnRunNoCriteria.setVendorCd(doGroupAsn.getAsnDomain().getVendorCd());
                asnRunNoCriteria.setSPcd(doGroupAsn.getSPcd()
                    .substring(Constants.ZERO, Constants.ONE));
                asnRunNoCriteria.setDPcd(doGroupAsn.getDPcd());
                asnRunNoCriteria.setAsnDate(new java.sql.Date(updateDateTime.getTime()));
                asnRunNoDomain = spsTAsnRunnoService.searchByKey(asnRunNoCriteria);
                if(null != asnRunNoDomain 
                    && !StringUtil.checkNullOrEmpty(asnRunNoDomain.getRunno()))
                {
                    runNo = Integer.valueOf(asnRunNoDomain.getRunno().toString()) + Constants.ONE;
                    hasRunNo = Constants.STR_ONE;
                }else{
                    runNo = Constants.ONE;
                    hasRunNo = Constants.STR_ZERO;
                }
                runningNoStr = String.format(Constants.RUNNING_NO_FORMAT, runNo);
                asnNo.append(doGroupAsn.getSPcd().charAt(Constants.ZERO));
                asnNo.append(DateUtil.getYear(updateDateTime).charAt(Constants.THREE));
                asnNo.append(this.getDigitOfMonth(updateDateTime));
                asnNo.append(updateDateTime.toString().substring(Constants.EIGHT, Constants.TEN));
                asnNo.append(doGroupAsn.getDPcd());
                asnNo.append(Constants.STR_M);
                asnNo.append(runningNoStr);
                asnNo.append(doGroupAsn.getAsnDomain().getVendorCd());
            }else{
                asnNo.append(asnMaintenanceDomain.getAsnNo());
            }
            asnMaintenanceDomain.setAsnNo(asnNo.toString());
            
            asnDomain = new SpsTAsnDomain();
            asnDomain.setAsnNo(asnNo.toString());
            asnDomain.setAsnStatus(Constants.ASN_STATUS_ISS);
            asnDomain.setVendorCd(doGroupAsn.getAsnDomain().getVendorCd());
            asnDomain.setSCd(doGroupAsn.getSCd());
            asnDomain.setSPcd(doGroupAsn.getSPcd());
            asnDomain.setDCd(doGroupAsn.getDCd());
            asnDomain.setDPcd(doGroupAsn.getDPcd());
            Date planEtdDate = this.getMinimumOfShipDateTime(doGroupAsnList);
            asnDomain.setPlanEtd(new Timestamp(planEtdDate.getTime()));
            planEtaStr.append(asnMaintenanceDomain.getPlanEtaDate())
                .append(Constants.SYMBOL_SPACE)
                .append(asnMaintenanceDomain.getPlanEtaTime());
            actualEtdStr.append(asnMaintenanceDomain.getActualEtdDate())
                .append(Constants.SYMBOL_SPACE)
                .append(asnMaintenanceDomain.getActualEtdTime());
            actualEtaStr.append(asnMaintenanceDomain.getPlanEtaDate())
                .append(Constants.SYMBOL_SPACE)
                .append(asnMaintenanceDomain.getPlanEtaTime());
            Date planEtaDate = DateUtil.parseToUtilDate(planEtaStr.toString(),
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            Date actualEtdDate = DateUtil.parseToUtilDate(actualEtdStr.toString(),
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            Date actualEtaDate = DateUtil.parseToUtilDate(actualEtaStr.toString(),
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            asnDomain.setPlanEta(new Timestamp(planEtaDate.getTime()));
            asnDomain.setActualEtd(new Timestamp(actualEtdDate.getTime()));
            asnDomain.setActualEta(new Timestamp(actualEtaDate.getTime()));
            asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_YES);
            
            String noOfPallet = asnMaintenanceDomain.getNoOfPallet().replace(
                Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
            asnDomain.setNumberOfPallet(new BigDecimal(noOfPallet));
            asnDomain.setTripNo(asnMaintenanceDomain.getTripNo().toUpperCase());
            asnDomain.setPdfFileId(null);
            asnDomain.setCreatedInvoiceFlag(Constants.STR_ZERO);
            asnDomain.setCreateDscId(asnMaintenanceDomain.getDscId());
            asnDomain.setCreateDatetime(updateDateTime);
            asnDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            asnDomain.setLastUpdateDatetime(updateDateTime);
            asnDomain.setReceiveByScan(doGroupAsn.getReceiveByScan());
            
            spsTAsnService.create(asnDomain);
            asnCriteria = new SpsTAsnCriteriaDomain();
            asnCriteria.setAsnNo(asnNo.toString());
            asnCriteria.setDCd(doGroupAsn.getDCd());
            int rowCountAsn = spsTAsnService.searchCount(asnCriteria);
            if(Constants.ZERO == rowCountAsn){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            BigDecimal totalShipQtyByPart = new BigDecimal(Constants.ZERO);
            String chgReasonCdByPart = Constants.EMPTY_STRING;
            String actualChgReasonCd = Constants.EMPTY_STRING;
            
            // [IN039] : Allow user to revise Shipping Box Quantity
            BigDecimal totalShipBoxByPart = new BigDecimal(Constants.ZERO);
            SpsTAsnDetailDomain previousAsnDetailForBox = null;
            
            // [IN014] previous Parts
            SpsTAsnDetailDomain previousAsnDetail = null;
            
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
                
                if(Constants.STR_ONE.equals(asnList.getRecordGroup())){
                    
                    // [IN014] Start : Change Parts No. and totalShipQtyByPart still not zero
                    if (null != previousAsnDetail 
                        && totalShipQtyByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
                    {
                        BigDecimal previousShipping
                            = previousAsnDetail.getShippingQty().add(totalShipQtyByPart);
                        previousAsnDetail.setShippingQty(previousShipping);
                        // [IN039] : Use Shipping Box Quantity use FIFO not calculate each row
                        //BigDecimal previousShippingBoxQty = previousShipping.divide(
                        //    previousAsnDetail.getQtyBox(), Constants.ZERO, RoundingMode.CEILING);
                        //previousAsnDetail.setShippingBoxQty(previousShippingBoxQty);
                        previousAsnDetail = null;
                    }
                    // [IN014] End : Change Parts No. and totalShipQtyByPart still not zero
                    
                    // [IN039] Start : Change Parts No. and totalShipBoxByPart still not zero
                    if (null != previousAsnDetailForBox 
                        && totalShipBoxByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
                    {
                        BigDecimal previousShipBox = previousAsnDetailForBox.getShippingBoxQty()
                            .add(totalShipBoxByPart);
                        previousAsnDetailForBox.setShippingBoxQty(previousShipBox);
                        previousAsnDetailForBox = null;
                    }
                    // [IN039] End : Change Parts No. and totalShipBoxByPart still not zero
                    
                    
                    totalShipQtyByPart = asnList.getShippingQtyByPart();
                    chgReasonCdByPart = asnList.getChangeReasonCd();
                    
                    // [IN039] : Allow user to revise Shipping Box Quantity
                    totalShipBoxByPart = NumberUtil.toBigDecimalDefaultZero(
                        asnList.getShippingBoxQtyByPartInput());
                }
                
                if(Constants.ZERO < totalShipQtyByPart.compareTo(
                    new BigDecimal(Constants.ZERO)))
                {
                    BigDecimal shippingQty = new BigDecimal(Constants.ZERO);
                    BigDecimal shippingBoxQty = new BigDecimal(Constants.ZERO);
                    asnDetailDomain = new SpsTAsnDetailDomain();
                    asnDetailDomain.setAsnNo(asnNo.toString());
                    asnDetailDomain.setDCd(asnList.getDCd());
                    asnDetailDomain.setSPn(asnList.getSPn());
                    asnDetailDomain.setDPn(asnList.getDPn());
                    asnDetailDomain.setDoId(asnList.getAsnDetailDomain().getDoId());
                    asnDetailDomain.setSpsDoNo(asnList.getAsnDetailDomain().getSpsDoNo());
                    asnDetailDomain.setRevision(asnList.getAsnDetailDomain().getRevision());
                    asnDetailDomain.setCigmaDoNo(asnList.getAsnDetailDomain().getCigmaDoNo());
                    
                    if(asnList.getTotalRemainingQty().compareTo(totalShipQtyByPart)
                        <= Constants.ZERO)
                    {
                        shippingQty = asnList.getTotalRemainingQty();
                        totalShipQtyByPart = totalShipQtyByPart.subtract(
                            asnList.getTotalRemainingQty());
                        actualChgReasonCd = Constants.EMPTY_STRING;
                    }else{
                        shippingQty = totalShipQtyByPart;
                        totalShipQtyByPart = new BigDecimal(Constants.ZERO);
                        actualChgReasonCd = chgReasonCdByPart;
                    }
                    
                    asnDetailDomain.setShippingQty(shippingQty);
                    asnDetailDomain.setShippingRoundQty(
                        shippingQty.setScale(Constants.ZERO, RoundingMode.DOWN));
                    asnDetailDomain.setQtyBox(asnList.getAsnDetailDomain().getQtyBox());

                    // Start : [IN039] Use Shipping Box Quantity use FIFO not calculate each row
                    //shippingBoxQty = shippingQty.divide(asnList.getAsnDetailDomain().getQtyBox(),
                    //    Constants.ZERO, RoundingMode.CEILING);
                    BigDecimal calculateShipBox
                        = shippingQty.divide(asnList.getAsnDetailDomain().getQtyBox()
                            , Constants.ZERO, RoundingMode.CEILING);
                    
                    if(calculateShipBox.compareTo(totalShipBoxByPart) <= Constants.ZERO
                        && Constants.ZERO != BigDecimal.ZERO.compareTo(totalShipQtyByPart))
                    {
                        shippingBoxQty = calculateShipBox;
                        totalShipBoxByPart = totalShipBoxByPart.subtract(calculateShipBox);
                    } else {
                        shippingBoxQty = totalShipBoxByPart;
                        totalShipBoxByPart = new BigDecimal(Constants.ZERO);
                    }
                    // End : [IN039] Use Shipping Box Quantity use FIFO not calculate each row
                    
                    asnDetailDomain.setShippingBoxQty(shippingBoxQty);
                    
                    asnDetailDomain.setRcvLane(asnList.getAsnDetailDomain().getRcvLane());
                    asnDetailDomain.setUnitOfMeasure(
                        asnList.getAsnDetailDomain().getUnitOfMeasure());
                    asnDetailDomain.setChangeReasonCd(actualChgReasonCd);
                    asnDetailDomain.setReviseShippingQtyFlag(Constants.STR_ZERO);
                    asnDetailDomain.setAllowReviseFlag(Constants.STR_ZERO);
                    asnDetailDomain.setCreateDscId(asnMaintenanceDomain.getDscId());
                    asnDetailDomain.setCreateDatetime(updateDateTime);
                    asnDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    asnDetailDomain.setLastUpdateDatetime(updateDateTime);
                    asnDetailDomain.setKanbanType(asnList.getGroupDoDetailDomain().getKanbanType());
                    asnDetailList.add(asnDetailDomain);

                    // [IN014] Remember current parts
                    previousAsnDetail = asnDetailDomain;
                    
                    // [IN039] Remember current parts for Shipping Box
                    previousAsnDetailForBox = asnDetailDomain;
                    
                    asnList.getAsnDetailDomain().setShippingQty(shippingQty);
                    asnList.getAsnDetailDomain().setShippingBoxQty(shippingBoxQty);
                    doGroupAsnResultList.add(asnList);
                }else if(Constants.ZERO == totalShipQtyByPart.compareTo(
                    new BigDecimal(Constants.ZERO))){
                    asnList.getAsnDetailDomain().setChangeReasonCd(chgReasonCdByPart);
                    asnList.getAsnDetailDomain().setShippingQty(new BigDecimal(Constants.ZERO));
                    asnList.getAsnDetailDomain().setShippingBoxQty(new BigDecimal(Constants.ZERO));
                    
                    if(unitOfMeasureMap.containsKey(
                        asnList.getAsnDetailDomain().getUnitOfMeasure()))
                    {
                        String displayDecimalPointFlag = (String)unitOfMeasureMap.get(
                            asnList.getAsnDetailDomain().getUnitOfMeasure());
                        
                        String shippingQtyStr = null;
                        if(Constants.DISPLAY_DECIMAL_FLAG_NO.equals(displayDecimalPointFlag)){
                            shippingQtyStr = StringUtil.toNumberFormat(
                                asnList.getAsnDetailDomain().getShippingQty().toString());
                        }else{
                            shippingQtyStr = StringUtil.toCurrencyFormat(
                                asnList.getAsnDetailDomain().getShippingQty().toString());
                        }
                        asnList.setShippingQtyStr(shippingQtyStr);
                        asnShortShipList.add(asnList);
                    }else{
                        MessageUtil.getApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                            SupplierPortalConstant.LBL_UNIT_OF_MEASURE);
                    }
                }
            }

            // [IN014] Start : End Loop and total ship quantity of last Parts No. is not zero
            if (null != previousAsnDetail 
                && totalShipQtyByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
            {
                BigDecimal previousShipping
                    = previousAsnDetail.getShippingQty().add(totalShipQtyByPart);
                previousAsnDetail.setShippingQty(previousShipping);
                // [IN039] : Use Shipping Box Quantity use FIFO not calculate each row
                //BigDecimal previousShippingBoxQty = previousShipping.divide(
                //    previousAsnDetail.getQtyBox(), Constants.ZERO, RoundingMode.CEILING);
                //previousAsnDetail.setShippingBoxQty(previousShippingBoxQty);
                previousAsnDetail = null;
            }
            // [IN014] End : End Loop and total ship quantity of last Parts No. is not zero
            
            // [IN039] Start : End Loop and total ship box quantity of last Parts No. is not zero
            if (null != previousAsnDetailForBox 
                && totalShipBoxByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
            {
                BigDecimal previousShipBox = previousAsnDetailForBox.getShippingBoxQty()
                    .add(totalShipBoxByPart);
                previousAsnDetailForBox.setShippingBoxQty(previousShipBox);
                previousAsnDetailForBox = null;
            }
            // [IN039] End : End Loop and total ship box quantity of last Parts No. is not zero

            if(doGroupAsnResultList.isEmpty()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0037,
                    SupplierPortalConstant.LBL_CREATE_ASN_NO_SHIPPING_QTY);
            }else{
                doGroupAsnList = new ArrayList<AsnMaintenanceReturnDomain>();
                doGroupAsnList = doGroupAsnResultList;
            }
            
            if(null != asnShortShipList && !asnShortShipList.isEmpty()){
                Map chgReasonMap = generateChangeReasonMap(locale);
                for(AsnMaintenanceReturnDomain asnShortShipItem : asnShortShipList){
                    String chgReasonName = (String)chgReasonMap.get(
                        asnShortShipItem.getAsnDetailDomain().getChangeReasonCd());
                    asnShortShipItem.setChangeReasonName(chgReasonName);
                }
                asnMaintenanceDomain.setAsnShortShipList(asnShortShipList);
            }
            
            spsTAsnDetailService.create(asnDetailList);
            asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
            asnDetailCriteria.setAsnNo(asnNo.toString());
            asnDetailCriteria.setDCd(doGroupAsn.getDCd());
            int rowCountAsnDetail = spsTAsnDetailService.searchCount(asnDetailCriteria);
            if(rowCountAsnDetail != doGroupAsnList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038);
            }
            
//            Generate ASN Report
//            2018-04-11 Don't generate BLOB
//            this.generateAndUpdatePdfFile(locale, asnNo.toString(), doGroupAsn.getDCd(),
//                updateDateTime, asnMaintenanceDomain.getDscId());
//            
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList)
            {
                doDetailDomain = new SpsTDoDetailDomain();
                doDetailCriteria = new SpsTDoDetailCriteriaDomain();
                // [IN014] Shipping Qty can more than Total Remain Qty
                //if(Constants.ZERO == asnList.getAsnDetailDomain().getShippingQty().compareTo(
                //    asnList.getTotalRemainingQty())){
                if(Constants.ZERO <= asnList.getAsnDetailDomain().getShippingQty().compareTo(
                    asnList.getTotalRemainingQty()))
                {
                    
                    doDetailDomain.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                }else{
                    if(Constants.ZERO == asnList.getAsnDetailDomain().getShippingQty().compareTo(
                        new BigDecimal(Constants.ZERO))){
                        doDetailDomain.setPnShipmentStatus(Constants.SHIPMENT_STATUS_ISS);
                    }else{
                        doDetailDomain.setPnShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
                    }
                }
                doDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                doDetailDomain.setLastUpdateDatetime(updateDateTime);
                doDetailCriteria.setDoId(asnList.getAsnDetailDomain().getDoId());
                doDetailCriteria.setDPn(asnList.getDPn());
                doDetailCriteria.setLastUpdateDatetime(asnList.getAsnDetailDomain()
                    .getLastUpdateDatetime());
                doDetailList.add(doDetailDomain);
                doDetailCriteriaList.add(doDetailCriteria);
            }
            int rowCountUpdate = spsTDoDetailService.updateByCondition(doDetailList,
                doDetailCriteriaList);
            if(rowCountUpdate != doGroupAsnList.size()){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
                String key = asnList.getAsnDetailDomain().getDoId().toString();
                if(!doIdMap.containsKey(key)){
                    doIdMap.put(key, asnList.getAsnDomain().getLastUpdateDatetime());
                }
            }
            
            Iterator itr = doIdMap.keySet().iterator();
            while(itr.hasNext()){
                String doId = (String)itr.next();

                // [IN012] change D/O check status method
                //asnMaintenanceCriteria = new AsnMaintenanceDomain();
                //asnMaintenanceCriteria.setDoId(doId);
                //asnMaintenanceCriteria.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                //asnMaintenanceCriteria.setLastUpdateDatetime((Timestamp)doIdMap.get(doId));
                //criteriaDomain.add(asnMaintenanceCriteria);
                doStatusCriteria = new SpsTDoDomain();
                doStatusCriteria.setDoId(new BigDecimal(doId));
                doStatusCriteria.setLastUpdateDatetime((Timestamp)doIdMap.get(doId));
                criteriaDomain.add(doStatusCriteria);
                
            }

            // [IN012] Start : change D/O check status method
            //List<AsnMaintenanceDomain> completedShipDoResultList
            //    = deliveryOrderService.searchCompleteShipDo(criteriaDomain);
            //if(Constants.ZERO < completedShipDoResultList.size()){
            //    doList = new ArrayList<SpsTDoDomain>();
            //    doCriteriaList = new ArrayList<SpsTDoCriteriaDomain>();
            //    for(AsnMaintenanceDomain completedShipDo : completedShipDoResultList){
            //        doDomain = new SpsTDoDomain();
            //        doCriteria = new SpsTDoCriteriaDomain();
            //        if(Constants.ZERO < completedShipDo.getDoGroupAsnList().size()){
            //            doDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
            //            doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            //            doDomain.setLastUpdateDatetime(updateDateTime);
            //            doCriteria.setDoId(new BigDecimal(completedShipDo.getDoId()));
            //            doCriteria.setLastUpdateDatetime(completedShipDo.getLastUpdateDatetime());
            //        }else{
            //            doDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
            //            doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            //            doDomain.setLastUpdateDatetime(updateDateTime);
            //            doCriteria.setDoId(new BigDecimal(completedShipDo.getDoId()));
            //            doCriteria.setLastUpdateDatetime(completedShipDo.getLastUpdateDatetime());
            //        }
            //        doList.add(doDomain);
            //        doCriteriaList.add(doCriteria);
            //    }
            //    int rowUpdate = spsTDoService.updateByCondition(doList, doCriteriaList);
            //    if(rowUpdate != criteriaDomain.size()){
            //        MessageUtil.throwsApplicationMessage(locale, 
            //            SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
            //            SupplierPortalConstant.ERROR_CD_SP_90_0002);
            //    }
            //}
            List<SpsTDoDomain> doUpdateStatusList
                = this.deliveryOrderService.searchExamineDoStatus(criteriaDomain);
            if (!doUpdateStatusList.isEmpty()) {
                doList = new ArrayList<SpsTDoDomain>();
                doCriteriaList = new ArrayList<SpsTDoCriteriaDomain>();
                for (SpsTDoDomain doUpdateStatus : doUpdateStatusList) {
                    doDomain = new SpsTDoDomain();
                    doCriteria = new SpsTDoCriteriaDomain();
                    
                    doDomain.setShipmentStatus(doUpdateStatus.getShipmentStatus());
                    doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    doDomain.setLastUpdateDatetime(updateDateTime);
                    doCriteria.setDoId(doUpdateStatus.getDoId());
                    doCriteria.setLastUpdateDatetime(doUpdateStatus.getLastUpdateDatetime());

                    doList.add(doDomain);
                    doCriteriaList.add(doCriteria);
                }
                
                int rowUpdate = spsTDoService.updateByCondition(doList, doCriteriaList);
                if(rowUpdate != criteriaDomain.size()){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            // [IN012] End : change D/O check status method
            
            asnInformationCriteria = new AsnInformationDomain();
            asnInformationCriteria.getSpsTAsnDomain().setAsnNo(asnNo.toString());
            asnInformationCriteria.getSpsTAsnDomain().setDCd(doGroupAsn.getDCd());
            asnInformationList = asnService.searchAsnDetail(asnInformationCriteria);
            if(null == asnInformationList || asnInformationList.isEmpty()){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            // Set cigma asn domain to map
            Map tempCigmaAsnMap = new HashMap();
            StringBuffer key = new StringBuffer();
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
                key = new StringBuffer();
                key.append(asnMaintenanceDomain.getAsnNo().trim())
                    .append(Constants.SYMBOL_UNDER_SCORE)
                    .append(asnList.getDPn()).append(Constants.SYMBOL_UNDER_SCORE)
                    .append(asnList.getAsnDetailDomain().getSpsDoNo());
                if(!tempCigmaAsnMap.containsKey(key.toString())){
                    tempCigmaAsnMap.put(key.toString(), asnList.getCigmaAsnDomain());
                }
            }
            
            AsnInformationDomain asnInformation = asnInformationList.get(Constants.ZERO);
            asnMaintenanceDomain.setDoGroupAsnList(
                this.setNewAsnDetailInformationList(asnInformationList, tempCigmaAsnMap));
            
            if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
                this.doSetAsnListToShow(asnMaintenanceDomain.getDoGroupAsnList(), 
                    asnMaintenanceDomain);
            }
            
            if(!StringUtil.checkNullOrEmpty(hasRunNo)){
                if(Constants.STR_ONE.equals(hasRunNo)){
                    asnRunNoDomain = new SpsTAsnRunnoDomain();
                    asnRunNoCriteria = new SpsTAsnRunnoCriteriaDomain();
                    asnRunNoDomain.setRunno(new BigDecimal(runNo));
                    asnRunNoDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    asnRunNoDomain.setLastUpdateDatetime(updateDateTime);
                    asnRunNoCriteria.setSCd(doGroupAsn.getSCd());
                    asnRunNoCriteria.setDCd(doGroupAsn.getDCd());
                    asnRunNoCriteria.setVendorCd(doGroupAsn.getAsnDomain().getVendorCd());
                    asnRunNoCriteria.setSPcd(doGroupAsn.getSPcd()
                        .substring(Constants.ZERO, Constants.ONE));
                    asnRunNoCriteria.setDPcd(doGroupAsn.getDPcd());
                    asnRunNoCriteria.setAsnDate(new java.sql.Date(updateDateTime.getTime()));
                    int countUpdateAsnRunNo = spsTAsnRunnoService.
                        updateByCondition(asnRunNoDomain, asnRunNoCriteria);
                    if(Constants.ZERO == countUpdateAsnRunNo){
                        MessageUtil.throwsApplicationMessage(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                            SupplierPortalConstant.ERROR_CD_SP_90_0002);
                    }
                }else{
                    asnRunNoDomain = new SpsTAsnRunnoDomain();
                    asnRunNoDomain.setSCd(doGroupAsn.getSCd());
                    asnRunNoDomain.setDCd(doGroupAsn.getDCd());
                    asnRunNoDomain.setVendorCd(doGroupAsn.getAsnDomain().getVendorCd());
                    asnRunNoDomain.setSPcd(doGroupAsn.getSPcd()
                        .substring(Constants.ZERO, Constants.ONE));
                    asnRunNoDomain.setDPcd(doGroupAsn.getDPcd());
                    asnRunNoDomain.setAsnDate(new java.sql.Date(updateDateTime.getTime()));
                    asnRunNoDomain.setRunno(new BigDecimal(runNo));
                    asnRunNoDomain.setCreateDscId(asnMaintenanceDomain.getDscId());
                    asnRunNoDomain.setCreateDatetime(updateDateTime);
                    asnRunNoDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    asnRunNoDomain.setLastUpdateDatetime(updateDateTime);
                    spsTAsnRunnoService.create(asnRunNoDomain);
                    
                    asnRunNoCriteria = new SpsTAsnRunnoCriteriaDomain();
                    asnRunNoCriteria.setSCd(doGroupAsn.getSCd());
                    asnRunNoCriteria.setDCd(doGroupAsn.getDCd());
                    asnRunNoCriteria.setVendorCd(doGroupAsn.getAsnDomain().getVendorCd());
                    asnRunNoCriteria.setSPcd(doGroupAsn.getSPcd()
                        .substring(Constants.ZERO, Constants.ONE));
                    asnRunNoCriteria.setDPcd(doGroupAsn.getDPcd());
                    asnRunNoCriteria.setAsnDate(new java.sql.Date(updateDateTime.getTime()));
                    int countAsnRunNo = spsTAsnRunnoService.searchCount(asnRunNoCriteria);
                    if(Constants.ZERO == countAsnRunNo){
                        MessageUtil.throwsApplicationMessage(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                            SupplierPortalConstant.ERROR_CD_SP_90_0002);
                    }
                }
            }
            
            //Start For Add Transportation Method to getDoGroupAsnList.
            Integer i = Constants.ZERO;
            while(i < asnMaintenanceDomain.getDoGroupAsnList().size()){
                asnMaintenanceDomain.getDoGroupAsnList().get(i).getGroupDoDetailDomain().setKanbanType(doGroupAsnList.get(i).getGroupDoDetailDomain().getKanbanType());
                asnMaintenanceDomain.getDoGroupAsnList().get(i).getGroupDoDomain().setReceiveByScan(doGroupAsnList.get(i).getGroupDoDomain().getReceiveByScan());
                asnMaintenanceDomain.getDoGroupAsnList().get(i).getGroupDoDomain().setTm(doGroupAsnList.get(i).getGroupDoDomain().getTm());
                i++;
            }
         // End For Add Transportation Method to getDoGroupAsnList.
            
            //Connect to CIGMA
            Iterator asnItr = doGroupAsnList.iterator();
            Set<String> dCdSet = new TreeSet<String>();
            while(asnItr.hasNext()){
                AsnMaintenanceReturnDomain temp = (AsnMaintenanceReturnDomain)asnItr.next();
                dCdSet.add(temp.getDCd());
            }
            companyDenso.setDCd(StringUtil.convertListToVarcharCommaSeperate(dCdSet));
            List<As400ServerConnectionInformationDomain> schemaResultList
                = companyDensoService.searchAs400ServerList(companyDenso);
            if(null == schemaResultList || schemaResultList.isEmpty()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026);
            }
            
            //This screen data has 1 DENSO Company Code
            As400ServerConnectionInformationDomain as400Server
                = schemaResultList.get(Constants.ZERO);
            Integer createRecord = Constants.ZERO;
            
            String recordResult = CommonWebServiceUtil.asnResourceCreateAsn(
                asnMaintenanceDomain.getDoGroupAsnList(), as400Server,
                asnMaintenanceDomain.getDscId(), Constants.STR_CREATE);
            if(!StringUtil.checkNullOrEmpty(recordResult)){
                createRecord = Integer.valueOf(recordResult);
            }
            
            if(createRecord != asnInformationList.size()){
//                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
//                    MessageUtil.getApplicationMessageHandledException(locale,
//                        SupplierPortalConstant.ERROR_CD_SP_E6_0002)));
                
                asnDomain = new SpsTAsnDomain();
                asnCriteria = new SpsTAsnCriteriaDomain();
                asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_NO);
                asnCriteria.setAsnNo(asnNo.toString());
                asnCriteria.setDCd(asnInformation.getSpsTAsnDomain().getDCd());
                asnCriteria.setLastUpdateDatetime(
                    asnInformation.getSpsTAsnDomain().getLastUpdateDatetime());
                int countUpdateTransferFlag 
                    = spsTAsnService.updateByCondition(asnDomain, asnCriteria);
                if(Constants.ZERO == countUpdateTransferFlag){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
//                20230329 Rungsiwut change to throw error to screen.
				MessageUtil.throwsApplicationMessage(locale,
						SupplierPortalConstant.ERROR_CD_SP_E6_0038,
						SupplierPortalConstant.ERROR_CD_SP_E6_0038);
            }
            
            if(warningMessageList.isEmpty()){
                for(AsnMaintenanceReturnDomain asnInfo : asnMaintenanceDomain.getDoGroupAsnList()){
                    asnInfo.getCigmaAsnDomain().setPseudoAsnStatus(Constants.ASN_STATUS_N);
                    asnInfo.getCigmaAsnDomain().setPseudoPnReceivingStatus(Constants.ASN_STATUS_N);
                    asnInfo.setAsnPnReceivingStatus(Constants.ASN_STATUS_ISS);
                    asnInfo.setAsnStatus(Constants.ASN_STATUS_ISS);
                }
            }
            
            //Set lastUpdateDatetime to CigmaAsnDomain
            for(AsnMaintenanceReturnDomain asnList : asnMaintenanceDomain.getDoGroupAsnList()){
                asnList.getCigmaAsnDomain().setPseudoLastUpdate(
                    DateUtil.format(new Date(updateDateTime.getTime()), DateUtil.PATTERN_YYYYMMDD));
                asnList.getCigmaAsnDomain().setPseudoLastUpdateTime(
                    DateUtil.format(new Date(updateDateTime.getTime()), DateUtil.PATTERN_HHMMSS));
                asnList.getCigmaAsnDomain().setPseudoPnLastUpdate(
                    DateUtil.format(new Date(updateDateTime.getTime()), DateUtil.PATTERN_YYYYMMDD));
                asnList.getCigmaAsnDomain().setPseudoPnLastUpdateTime(
                    DateUtil.format(new Date(updateDateTime.getTime()), DateUtil.PATTERN_HHMMSS));
            }
            
            //set to form
            if(Constants.ZERO < warningMessageList.size()){
                asnMaintenanceDomain.setWarningMessageList(warningMessageList);
            }
            
            asnMaintenanceDomain.setTemporaryMode(Constants.STR_ZERO);
            asnMaintenanceDomain.setAsnRcvStatus(Constants.ASN_STATUS_ISS);
            asnMaintenanceDomain.setLastModified(DateUtil.format(
                new Date(updateDateTime.getTime()), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_ASNMAINTENANCE_RESULT,
                asnMaintenanceDomain);
        }else{ //0 : Display exist ASN Grouping
            int sizeRevisingQty = Constants.ZERO;
            List<AsnMaintenanceReturnDomain> deletedAsnList
                = new ArrayList<AsnMaintenanceReturnDomain>();
            
            Map sumShippingQtyByPartMap = new HashMap();
            asnDetailList = new ArrayList<SpsTAsnDetailDomain>();
            asnDetailCriteriaList = new ArrayList<SpsTAsnDetailCriteriaDomain>();
            deletedAsnDetailCriteriaList = new ArrayList<SpsTAsnDetailCriteriaDomain>();
            AsnMaintenanceReturnDomain deletedAsnTmp = null;
            BigDecimal totalShippingQty = new BigDecimal(Constants.ZERO);
            BigDecimal receivedQtyByPart = new BigDecimal(Constants.ONE);
            String chgReasonCdByPart = Constants.EMPTY_STRING;
            String actualChgReasonCd = Constants.EMPTY_STRING;
            
            // [IN039] : Allow user to revise Shipping Box Quantity
            BigDecimal totalShipBoxByPart = new BigDecimal(Constants.ZERO);
            SpsTAsnDetailDomain previousAsnDetailForBox = null;
            SpsTAsnDetailDomain previousAsnDetail = null;
            
            for (AsnMaintenanceReturnDomain asnList : doGroupAsnList) {
                // [IN058] Keep Total Shipping Qty
                if(Constants.STR_ONE.equals(asnList.getRecordGroup())){
                    totalShippingQty = totalShippingQty.add(asnList.getShippingQtyByPart());
                }
                
                // [IN039] Skip all in case not revising
                if(!Constants.STR_ONE.equals(asnList.getRevisingFlag())){
                    continue;
                }
                
                if(Constants.STR_ONE.equals(asnList.getRecordGroup())){
                    // [IN039] Start : Change Parts No. and totalShipBoxByPart still not zero
                    if (null != previousAsnDetail 
                        && receivedQtyByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
                    {
                        BigDecimal previousShipping
                            = previousAsnDetail.getShippingQty().add(receivedQtyByPart);
                        previousAsnDetail.setShippingQty(previousShipping);
                        previousAsnDetail = null;
                    }
                    if (null != previousAsnDetailForBox 
                        && totalShipBoxByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
                    {
                        BigDecimal previousShipBox = previousAsnDetailForBox.getShippingBoxQty()
                            .add(totalShipBoxByPart);
                        previousAsnDetailForBox.setShippingBoxQty(previousShipBox);
                        previousAsnDetailForBox = null;
                    }
                    // [IN039] End : Change Parts No. and totalShipBoxByPart still not zero
                    
                    receivedQtyByPart = asnList.getAsnReceivedQtyByPart();
                    chgReasonCdByPart = asnList.getChangeReasonCd();
                    totalShippingQty = totalShippingQty.add(asnList.getShippingQtyByPart());

                    // [IN039] : Allow user to revise Shipping Box Quantity
                    totalShipBoxByPart = NumberUtil.toBigDecimalDefaultZero(
                        asnList.getShippingBoxQtyByPartInput());
                }
                
                // [IN039] Skip all in case not revising
                //if(Constants.STR_ONE.equals(asnList.getRevisingFlag())){
                    //Revise shipping qty by FIFO
                    
                BigDecimal sumShippingQtyByPart = new BigDecimal(Constants.ZERO);
                SpsTAsnDetailDomain asnDetail = new SpsTAsnDetailDomain();
                asnDetail.setAsnNo(asnList.getAsnDomain().getAsnNo());
                asnDetail.setDPn(asnList.getDPn());
                asnDetail.setRcvLane(asnList.getAsnDetailDomain().getRcvLane());
                asnDetail.setDoId(asnList.getAsnDetailDomain().getDoId());
                SpsTAsnDetailDomain asnDetailResultByPart
                    = asnService.searchSumShippingQtyByPart(asnDetail);
                if(null != asnDetailResultByPart && !StringUtil.checkNullOrEmpty(
                    asnDetailResultByPart.getShippingQty())){
                    sumShippingQtyByPart = asnDetailResultByPart.getShippingQty();
                }
                String key = StringUtil.appendsString(asnList.getDPn(),
                    asnList.getAsnDetailDomain().getRcvLane(),
                    asnList.getAsnDetailDomain().getDoId().toString());
                sumShippingQtyByPartMap.put(key, sumShippingQtyByPart);
                
                if(Constants.ZERO < receivedQtyByPart.compareTo(
                    new BigDecimal(Constants.ZERO)))
                {
                    sizeRevisingQty += Constants.ONE;
                    BigDecimal shippingQty = new BigDecimal(Constants.ZERO);
                    BigDecimal shippingBoxQty = new BigDecimal(Constants.ZERO);
                    asnDetailDomain = new SpsTAsnDetailDomain();
                    asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
                    
                    shippingQty = asnList.getAsnReceivedQty();
                    receivedQtyByPart = receivedQtyByPart.subtract(
                        asnList.getAsnReceivedQty());
                    
                    // Start : [IN039] Use Shipping Box Quantity use FIFO not calculate each row
                    //shippingBoxQty = shippingQty.divide(
                    //    asnList.getAsnDetailDomain().getQtyBox(),
                    //    Constants.ZERO, RoundingMode.CEILING);
                    BigDecimal calculateShipBox = shippingQty.divide(
                        asnList.getAsnDetailDomain().getQtyBox(),
                        Constants.ZERO, RoundingMode.CEILING);
                    
                    if(calculateShipBox.compareTo(totalShipBoxByPart) <= Constants.ZERO
                        && Constants.ZERO != BigDecimal.ZERO.compareTo(receivedQtyByPart))
                    {
                        shippingBoxQty = calculateShipBox;
                        totalShipBoxByPart = totalShipBoxByPart.subtract(calculateShipBox);
                    } else {
                        shippingBoxQty = totalShipBoxByPart;
                        totalShipBoxByPart = new BigDecimal(Constants.ZERO);
                    }
                    // End : [IN039] Use Shipping Box Quantity use FIFO not calculate each row
                    
                    asnDetailDomain.setShippingQty(shippingQty);
                    asnDetailDomain.setShippingRoundQty(shippingQty.setScale(
                        Constants.ZERO, RoundingMode.DOWN));
                    
                    BigDecimal actualShippingQty = sumShippingQtyByPart.add(shippingQty);
                    
                    if(Constants.ZERO == actualShippingQty.compareTo(
                        asnList.getGroupDoDetailDomain().getCurrentOrderQty())){
                        actualChgReasonCd = Constants.EMPTY_STRING;
                    }else{
                        actualChgReasonCd = chgReasonCdByPart;
                    }
                    
                    asnDetailDomain.setQtyBox(asnList.getAsnDetailDomain().getQtyBox());
                    asnDetailDomain.setShippingBoxQty(shippingBoxQty);
                    asnDetailDomain.setChangeReasonCd(actualChgReasonCd);
                    asnDetailDomain.setReviseShippingQtyFlag(Constants.STR_ONE);
                    asnDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    asnDetailDomain.setLastUpdateDatetime(updateDateTime);
                    asnDetailCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
                    asnDetailCriteria.setDCd(asnList.getDCd());
                    asnDetailCriteria.setDPn(asnList.getDPn());
                    asnDetailCriteria.setDoId(asnList.getAsnDetailDomain().getDoId());
                    asnDetailCriteria.setLastUpdateDatetime(asnList.getAsnDetailDomain()
                        .getLastUpdateDatetime());
                    asnDetailList.add(asnDetailDomain);
                    asnDetailCriteriaList.add(asnDetailCriteria);

                    // [IN039] Remember current parts for Shipping Box
                    previousAsnDetail = asnDetailDomain;
                    previousAsnDetailForBox = asnDetailDomain;
                    
                    asnList.getAsnDetailDomain().setShippingQty(shippingQty);
                    asnList.getAsnDetailDomain().setShippingBoxQty(shippingBoxQty);
                }else{
                    //Set ASN detail to list for delete
                    asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
                    asnDetailCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
                    asnDetailCriteria.setDCd(asnList.getDCd());
                    asnDetailCriteria.setDPn(asnList.getDPn());
                    asnDetailCriteria.setDoId(asnList.getAsnDetailDomain().getDoId());
                    asnDetailCriteria.setLastUpdateDatetime(asnList.getAsnDetailDomain()
                        .getLastUpdateDatetime());
                    deletedAsnDetailCriteriaList.add(asnDetailCriteria);
                    
                    asnList.getAsnDetailDomain().setShippingQty(
                        new BigDecimal(Constants.ZERO));
                    asnList.getAsnDetailDomain().setShippingBoxQty(
                        new BigDecimal(Constants.ZERO));
                    
                    deletedAsnTmp = new AsnMaintenanceReturnDomain();
                    deletedAsnTmp.setCigmaAsnDomain(asnList.getCigmaAsnDomain());
                    deletedAsnTmp.getAsnDomain().setVendorCd(
                        asnList.getAsnDomain().getVendorCd());
                    deletedAsnTmp.getAsnDomain().setAsnNo(asnList.getAsnDomain().getAsnNo());
                    deletedAsnTmp.getAsnDomain().setDCd(asnList.getAsnDomain().getDCd());
                    deletedAsnTmp.getAsnDomain().setDPcd(asnList.getAsnDomain().getDPcd());
                    deletedAsnTmp.getAsnDomain().setLastUpdateDatetime(
                        asnList.getAsnDomain().getLastUpdateDatetime());
                    deletedAsnTmp.getAsnDetailDomain().setCigmaDoNo(
                        asnList.getAsnDetailDomain().getCigmaDoNo());
                    deletedAsnTmp.getAsnDetailDomain().setDPn(asnList.getDPn());
                    deletedAsnTmp.getAsnDetailDomain().setRcvLane(
                        asnList.getAsnDetailDomain().getRcvLane());
                    deletedAsnTmp.getAsnDetailDomain().setShippingQty(
                        asnList.getAsnDetailDomain().getShippingQty());
                    deletedAsnTmp.getAsnDetailDomain().setLastUpdateDatetime(
                        asnList.getAsnDetailDomain().getLastUpdateDatetime());
                    
                    // [Cannot Cancel ASN Detail that Revision != 00]
                    deletedAsnTmp.getGroupDoDetailDomain().setChgCigmaDoNo(
                        asnList.getGroupDoDetailDomain().getChgCigmaDoNo());
                    
                    deletedAsnTmp.setReviseAsnDetailStatus(Constants.STR_CANCEL);
                    deletedAsnList.add(deletedAsnTmp);
                }

                // [IN039] Skip all in case not revising
                //}
            }

            // [IN039] Start : End Loop and total ship box quantity of last Parts No. is not zero
            if (null != previousAsnDetail 
                && receivedQtyByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
            {
                BigDecimal previousShipping
                    = previousAsnDetail.getShippingQty().add(receivedQtyByPart);
                previousAsnDetail.setShippingQty(previousShipping);
                previousAsnDetail = null;
            }
            
            if (null != previousAsnDetailForBox 
                && totalShipBoxByPart.compareTo(BigDecimal.ZERO) != Constants.ZERO)
            {
                BigDecimal previousShipBox = previousAsnDetailForBox.getShippingBoxQty()
                    .add(totalShipBoxByPart);
                previousAsnDetailForBox.setShippingBoxQty(previousShipBox);
                previousAsnDetailForBox = null;
            }
            // [IN039] End : End Loop and total ship box quantity of last Parts No. is not zero

            if(Constants.ZERO == totalShippingQty.compareTo(new BigDecimal(Constants.ZERO))){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0037,
                    SupplierPortalConstant.LBL_CREATE_ASN_NO_SHIPPING_QTY);
            }
            
            int rowUpdateAsnDetail 
                = spsTAsnDetailService.updateByCondition(asnDetailList, asnDetailCriteriaList);
            if(rowUpdateAsnDetail != sizeRevisingQty){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            for(SpsTAsnDetailCriteriaDomain deletedAsnDetail : deletedAsnDetailCriteriaList){
                int rowDeleteAsnDeteil
                    = spsTAsnDetailService.deleteByCondition(deletedAsnDetail);
                if(Constants.ZERO == rowDeleteAsnDeteil){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0038,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            
            asnDomain = new SpsTAsnDomain();
            String noOfPallet = asnMaintenanceDomain.getNoOfPallet().replace(
                Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
            asnDomain.setNumberOfPallet(new BigDecimal(noOfPallet));
            asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_YES);
            asnDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            asnDomain.setLastUpdateDatetime(updateDateTime);
            asnCriteria = new SpsTAsnCriteriaDomain();
            asnCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
            asnCriteria.setDCd(doGroupAsnList.get(Constants.ZERO).getAsnDomain().getDCd());
            asnCriteria.setLastUpdateDatetime(doGroupAsnList.get(Constants.ZERO).getAsnDomain()
                .getLastUpdateDatetime());
            int rowUpdateAsn = spsTAsnService.updateByCondition(asnDomain, asnCriteria);
            if(Constants.ZERO == rowUpdateAsn){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
//            Re-generate report
//            2018-04-11 Don't generate BLOB
//            this.generateAndUpdatePdfFile(locale, asnMaintenanceDomain.getAsnNo(),
//                doGroupAsn.getDCd(), updateDateTime, asnMaintenanceDomain.getDscId());
            
            doDetailList = new ArrayList<SpsTDoDetailDomain>();
            doDetailCriteriaList = new ArrayList<SpsTDoDetailCriteriaDomain>();
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
                if(Constants.STR_ONE.equals(asnList.getRevisingFlag())){
                    doDetailDomain = new SpsTDoDetailDomain();
                    String key = StringUtil.appendsString(asnList.getDPn(),
                        asnList.getAsnDetailDomain().getRcvLane(),
                        asnList.getAsnDetailDomain().getDoId().toString());
                    BigDecimal sumShippingQtyByPart = (BigDecimal)sumShippingQtyByPartMap.get(key);
                    
                    BigDecimal shippingQty = sumShippingQtyByPart.add(
                        asnList.getAsnDetailDomain().getShippingQty());
                    if(Constants.ZERO == shippingQty.compareTo(new BigDecimal(Constants.ZERO))){
                        doDetailDomain.setPnShipmentStatus(Constants.SHIPMENT_STATUS_ISS);
                    }else{

                        // [IN014] Shipping Qty can more than Total Remain Qty
                        //if(Constants.ZERO == shippingQty.compareTo(
                        //    asnList.getGroupDoDetailDomain().getCurrentOrderQty())){
                        if(Constants.ZERO <= shippingQty.compareTo(
                            asnList.getGroupDoDetailDomain().getCurrentOrderQty()))
                        {
                            doDetailDomain.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                        }else{
                            doDetailDomain.setPnShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
                        }
                    }
                    doDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    doDetailDomain.setLastUpdateDatetime(updateDateTime);
                    
                    doDetailCriteria = new SpsTDoDetailCriteriaDomain();
                    doDetailCriteria.setDoId(asnList.getAsnDetailDomain().getDoId());
                    doDetailCriteria.setDPn(asnList.getDPn());
                    doDetailCriteria.setLastUpdateDatetime(
                        asnList.getGroupDoDetailDomain().getLastUpdateDatetime());
                    doDetailList.add(doDetailDomain);
                    doDetailCriteriaList.add(doDetailCriteria);
                }
            }
            int rowUpdateDoDetail = spsTDoDetailService.updateByCondition(doDetailList, 
                doDetailCriteriaList);
            if(rowUpdateDoDetail != doDetailList.size()){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            //Check all part no. in D/O are complete shipped or not?
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
                String key = asnList.getAsnDetailDomain().getDoId().toString();
                if(!doIdMap.containsKey(key)){
                    doIdMap.put(key, asnList.getGroupDoDomain().getLastUpdateDatetime());
                }
            }
            
            Iterator itr = doIdMap.keySet().iterator();
            while(itr.hasNext()){
                String doId = (String)itr.next();
                
                // [IN012] change D/O check status method
                //asnMaintenanceCriteria = new AsnMaintenanceDomain();
                //asnMaintenanceCriteria.setDoId(doId);
                //asnMaintenanceCriteria.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                //asnMaintenanceCriteria.setLastUpdateDatetime((Timestamp)doIdMap.get(doId));
                //criteriaDomain.add(asnMaintenanceCriteria);
                doStatusCriteria = new SpsTDoDomain();
                doStatusCriteria.setDoId(new BigDecimal(doId));
                doStatusCriteria.setLastUpdateDatetime((Timestamp)doIdMap.get(doId));
                criteriaDomain.add(doStatusCriteria);
                
            }
            
            // [IN012] Start : change D/O check status method
            //List<AsnMaintenanceDomain> completedShipDoResultList
            //    = deliveryOrderService.searchCompleteShipDo(criteriaDomain);
            //if(Constants.ZERO < completedShipDoResultList.size()){
            //    doList = new ArrayList<SpsTDoDomain>();
            //    doCriteriaList = new ArrayList<SpsTDoCriteriaDomain>();
            //    for(AsnMaintenanceDomain completedShipDo : completedShipDoResultList){
            //        doDomain = new SpsTDoDomain();
            //        doCriteria = new SpsTDoCriteriaDomain();
            //        if(Constants.ZERO < completedShipDo.getDoGroupAsnList().size()){
            //            doDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
            //            doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            //            doDomain.setLastUpdateDatetime(updateDateTime);
            //            doCriteria.setDoId(new BigDecimal(completedShipDo.getDoId()));
            //            doCriteria.setLastUpdateDatetime(completedShipDo.getLastUpdateDatetime());
            //        }else{
            //            doDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
            //            doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            //            doDomain.setLastUpdateDatetime(updateDateTime);
            //            doCriteria.setDoId(new BigDecimal(completedShipDo.getDoId()));
            //            doCriteria.setLastUpdateDatetime(completedShipDo.getLastUpdateDatetime());
            //        }
            //        doList.add(doDomain);
            //        doCriteriaList.add(doCriteria);
            //    }
            //    int rowUpdate = spsTDoService.updateByCondition(doList, doCriteriaList);
            //    if(rowUpdate != criteriaDomain.size()){
            //        MessageUtil.throwsApplicationMessage(locale, 
            //            SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
            //            SupplierPortalConstant.ERROR_CD_SP_90_0002);
            //    }
            //}
            List<SpsTDoDomain> doUpdateStatusList
                = this.deliveryOrderService.searchExamineDoStatus(criteriaDomain);
            if (!doUpdateStatusList.isEmpty()) {
                doList = new ArrayList<SpsTDoDomain>();
                doCriteriaList = new ArrayList<SpsTDoCriteriaDomain>();
                for (SpsTDoDomain doUpdateStatus : doUpdateStatusList) {
                    doDomain = new SpsTDoDomain();
                    doCriteria = new SpsTDoCriteriaDomain();
                    
                    doDomain.setShipmentStatus(doUpdateStatus.getShipmentStatus());
                    doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                    doDomain.setLastUpdateDatetime(updateDateTime);
                    doCriteria.setDoId(doUpdateStatus.getDoId());
                    doCriteria.setLastUpdateDatetime(doUpdateStatus.getLastUpdateDatetime());

                    doList.add(doDomain);
                    doCriteriaList.add(doCriteria);
                }
                
                int rowUpdate = spsTDoService.updateByCondition(doList, doCriteriaList);
                if(rowUpdate != criteriaDomain.size()){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            // [IN012] End : change D/O check status method
            
            asnInformationCriteria = new AsnInformationDomain();
            asnInformationCriteria.getSpsTAsnDomain().setAsnNo(asnMaintenanceDomain.getAsnNo());
            asnInformationCriteria.getSpsTAsnDomain().setDCd(doGroupAsn.getDCd());
            asnInformationList = asnService.searchAsnDetail(asnInformationCriteria);
            if(null == asnInformationList || asnInformationList.isEmpty()){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            // Set cigma asn domain to map
            Map tempCigmaAsnMap = new HashMap();
            Set<String> asnRevisingKeyList = new TreeSet<String>();
            StringBuffer key = new StringBuffer();
            for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
                key = new StringBuffer();
                key.append(asnMaintenanceDomain.getAsnNo().trim())
                    .append(Constants.SYMBOL_UNDER_SCORE)
                    .append(asnList.getDPn()).append(Constants.SYMBOL_UNDER_SCORE)
                    .append(asnList.getAsnDetailDomain().getSpsDoNo());
                if(!tempCigmaAsnMap.containsKey(key.toString())){
                    tempCigmaAsnMap.put(key.toString(), asnList.getCigmaAsnDomain());
                }
                
                if(Constants.STR_ONE.equals(asnList.getRevisingFlag())){
                    asnRevisingKeyList.add(key.toString());
                }
            }
            
            asnMaintenanceDomain.setDoGroupAsnList(
                this.setNewAsnDetailInformationList(asnInformationList, tempCigmaAsnMap));
            
            if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
                this.doSetAsnListToShow(asnMaintenanceDomain.getDoGroupAsnList(),
                    asnMaintenanceDomain);
                
                //Set revise shipping qty complete flag by RCV Lane
                List<Boolean> reviseCompleteList = new ArrayList<Boolean>();
                AsnMaintenanceReturnDomain asnTmp = null;
                for(AsnMaintenanceReturnDomain asn : asnMaintenanceDomain.getDoGroupAsnList()){
                    if(Constants.STR_ONE.equals(asn.getRecordGroupByLane())){
                        if(null == asnTmp){
                            asnTmp = asn;
                        }else{
                            if(reviseCompleteList.contains(Boolean.FALSE)){
                                asnTmp.setReviseCompleteFlag(Constants.STR_ZERO);
                            }else{
                                asnTmp.setReviseCompleteFlag(Constants.STR_ONE);
                            }
                            asnTmp = asn;
                        }
                        reviseCompleteList.clear();
                    }
                    
                    if(Constants.STR_ONE.equals(asn.getRecordGroup())){
                        if(Constants.ZERO == asn.getShippingQtyByPart().compareTo(
                            asn.getAsnReceivedQtyByPart())){
                            reviseCompleteList.add(Boolean.TRUE);
                        }else{
                            reviseCompleteList.add(Boolean.FALSE);
                        }
                    }
                }
                if(reviseCompleteList.contains(Boolean.FALSE)){
                    asnTmp.setReviseCompleteFlag(Constants.STR_ZERO);
                }else{
                    asnTmp.setReviseCompleteFlag(Constants.STR_ONE);
                }
                reviseCompleteList.clear();
            }
            
            AsnInformationDomain asnInformation = asnInformationList.get(Constants.ZERO);
            //Connect to CIGMA
            Iterator asnItr = asnMaintenanceDomain.getDoGroupAsnList().iterator();
            Set<String> dCdSet = new TreeSet<String>();
            while(asnItr.hasNext()){
                AsnMaintenanceReturnDomain temp = (AsnMaintenanceReturnDomain)asnItr.next();
                temp.setReviseAsnDetailStatus(Constants.STR_UPDATE);
                dCdSet.add(temp.getDCd());
            }
            
            companyDenso.setDCd(StringUtil.convertListToVarcharCommaSeperate(dCdSet));
            List<As400ServerConnectionInformationDomain> schemaResultList 
                = companyDensoService.searchAs400ServerList(companyDenso);
            if(null == schemaResultList || schemaResultList.isEmpty()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026);
            }
            
            //merge new ASN detail revising list and deleted ASN detail list
            List<AsnMaintenanceReturnDomain> asnRevisingList
                = new ArrayList<AsnMaintenanceReturnDomain>();
            
            for(AsnMaintenanceReturnDomain asn : asnMaintenanceDomain.getDoGroupAsnList()){
                String asnKey = StringUtil.appendsString(asnMaintenanceDomain.getAsnNo().trim(),
                    Constants.SYMBOL_UNDER_SCORE, asn.getDPn(), Constants.SYMBOL_UNDER_SCORE,
                    asn.getAsnDetailDomain().getSpsDoNo());
                if(asnRevisingKeyList.contains(asnKey)){
                    asnRevisingList.add(asn);
                }
            }
            
            List<AsnMaintenanceReturnDomain> asnDetailTmpList
                = new ArrayList<AsnMaintenanceReturnDomain>();
            asnDetailTmpList.addAll(asnRevisingList);
            asnDetailTmpList.addAll(deletedAsnList);
            
            //This screen data has 1 DENSO Company Code
            As400ServerConnectionInformationDomain as400Server 
                = schemaResultList.get(Constants.ZERO);
            Integer updateRecord = Constants.ZERO;
            String recordResult = CommonWebServiceUtil.asnResourceUpdateAsn(
                asnDetailTmpList, as400Server, asnMaintenanceDomain.getDscId(),
                Constants.STR_UPDATE);
            if(!StringUtil.checkNullOrEmpty(recordResult)){
                updateRecord = Integer.valueOf(recordResult);
            }
            
            if(updateRecord != asnDetailTmpList.size()){
//                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
//                    MessageUtil.getApplicationMessageHandledException(locale, 
//                        SupplierPortalConstant.ERROR_CD_SP_E6_0002)));

//              20230329 Rungsiwut change to throw error to screen.
				MessageUtil.throwsApplicationMessage(locale,
						SupplierPortalConstant.ERROR_CD_SP_E6_0038,
						SupplierPortalConstant.ERROR_CD_SP_E6_0038);
            } else {
                // [Cannot Revise Shipping 2nd time if not return from ASN Maintenance Screen]
                AsnMaintenanceReturnDomain asnHeader = asnDetailTmpList.get(Constants.ZERO);
                String updateDateHeader = DateUtil.format(
                    new Date(asnHeader.getAsnDomain().getLastUpdateDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD);
                String updateTimeHeader = DateUtil.format(
                    new Date(asnHeader.getAsnDomain().getLastUpdateDatetime().getTime()),
                    DateUtil.PATTERN_HHMMSS);
                
                for(AsnMaintenanceReturnDomain asnHead : asnMaintenanceDomain.getDoGroupAsnList()) {
                    asnHead.getCigmaAsnDomain().setPseudoLastUpdate(updateDateHeader);
                    asnHead.getCigmaAsnDomain().setPseudoLastUpdateTime(updateTimeHeader);
                }

                for(AsnMaintenanceReturnDomain asnDetail : asnDetailTmpList){
                    asnDetail.getCigmaAsnDomain().setPseudoPnLastUpdate(updateDateHeader);
                    asnDetail.getCigmaAsnDomain().setPseudoPnLastUpdateTime(updateTimeHeader);
                }
                // [Cannot Revise Shipping 2nd time if not return from ASN Maintenance Screen]
            }
            
            if(Constants.ZERO < warningMessageList.size()){
                asnDomain = new SpsTAsnDomain();
                asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_NO);
                asnDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                asnDomain.setLastUpdateDatetime(updateDateTime);
                asnCriteria = new SpsTAsnCriteriaDomain();
                asnCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
                asnCriteria.setDCd(asnInformation.getSpsTAsnDomain().getDCd());
                asnCriteria.setLastUpdateDatetime(asnInformation.getSpsTAsnDomain()
                    .getLastUpdateDatetime());
                int countUpdateTransferFlag = spsTAsnService
                    .updateByCondition(asnDomain, asnCriteria);
                if(Constants.ZERO == countUpdateTransferFlag){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            
            if(Constants.ZERO < warningMessageList.size()){
                asnMaintenanceDomain.setWarningMessageList(warningMessageList);
            }
            
            asnMaintenanceDomain.setTemporaryMode(Constants.STR_ZERO);
            AsnMaintenanceReturnDomain asnInfo
                = asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO);
            asnMaintenanceDomain.setAsnRcvStatus(asnInfo.getAsnStatus());
            asnMaintenanceDomain.setLastModified(DateUtil.format(
                new Date(updateDateTime.getTime()), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
            DensoContext.get().putGeneralArea(
                SupplierPortalConstant.SESSION_ASNMAINTENANCE_RESULT, asnMaintenanceDomain);
        }
        return true;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#transactCancelAsnAndSend(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    public void transactCancelAsnAndSend(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        Timestamp updateDateTime = null;
        AsnInformationDomain asnInformationDomain = null;
        SpsMCompanyDensoDomain companyDenso = null;
        SpsTAsnDomain asnDomain = null;
        SpsTDoDomain doDomain = null;
        SpsTDoDetailDomain doDetail = null;
        SpsTAsnCriteriaDomain asnCriteriaDomain = null;
        SpsTDoCriteriaDomain doCriteria = null;
        SpsTDoDetailCriteriaDomain doDetailCriteria = null;
        
        // [IN012] change D/O check status method
        //AsnMaintenanceDomain asnMaintenanceCriteria = null;
        SpsTDoDomain doStatusCriteria = null;
        
        List<SpsTDoDomain> doList = null;
        List<SpsTDoCriteriaDomain> doCriteriaList = null;
        List<SpsTDoDetailDomain> doDetailList = null;
        List<SpsTDoDetailCriteriaDomain> doDetailCriteriaList = null;
        List<AsnMaintenanceReturnDomain> asnList = null;

        // [IN012] change D/O check status method
        //List<AsnMaintenanceDomain> criteriaDomain = new ArrayList<AsnMaintenanceDomain>();
        List<SpsTDoDomain> criteriaDomain = new ArrayList<SpsTDoDomain>();
        
        List<ApplicationMessageDomain> warningMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Map doIdMap = new HashMap();
        
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getAsnNo())){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                SupplierPortalConstant.LBL_ASN_NO);
        }
        
        if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
            asnList = asnMaintenanceDomain.getDoGroupAsnList();
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                SupplierPortalConstant.LBL_ASN_DATA);
        }
        
        updateDateTime = commonService.searchSysDate();
        
        asnDomain = new SpsTAsnDomain();
        asnDomain.setAsnStatus(Constants.ASN_STATUS_CCL);
        asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_YES);
        asnDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
        asnDomain.setLastUpdateDatetime(updateDateTime);
        asnCriteriaDomain = new SpsTAsnCriteriaDomain();
        asnCriteriaDomain.setAsnNo(asnMaintenanceDomain.getAsnNo());
        asnCriteriaDomain.setDCd(asnList.get(Constants.ZERO).getAsnDomain().getDCd());
        asnCriteriaDomain.setLastUpdateDatetime(
            asnList.get(Constants.ZERO).getAsnDomain().getLastUpdateDatetime());
        int rowCountUpdateAsn = spsTAsnService.updateByCondition(asnDomain, asnCriteriaDomain);
        if(Constants.ZERO == rowCountUpdateAsn){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0039, 
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        doDetailList = new ArrayList<SpsTDoDetailDomain>();
        doDetailCriteriaList = new ArrayList<SpsTDoDetailCriteriaDomain>();
        for(AsnMaintenanceReturnDomain asn : asnList){
            doDetail = new SpsTDoDetailDomain();
            if(Constants.ZERO == asn.getAsnDetailDomain().getShippingQty().compareTo(
                asn.getTotalShippedQty())){
                doDetail.setPnShipmentStatus(Constants.SHIPMENT_STATUS_ISS);
            }else{
                doDetail.setPnShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
            }
            doDetail.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            doDetail.setLastUpdateDatetime(updateDateTime);
            doDetailCriteria = new SpsTDoDetailCriteriaDomain();
            doDetailCriteria.setDoId(asn.getAsnDetailDomain().getDoId());
            doDetailCriteria.setDPn(asn.getDPn());
            doDetailCriteria.setLastUpdateDatetime(asn.getGroupDoDetailDomain()
                .getLastUpdateDatetime());
            doDetailList.add(doDetail);
            doDetailCriteriaList.add(doDetailCriteria);
        }
        int rowCountUpdateDoDetail = spsTDoDetailService.updateByCondition(doDetailList, 
            doDetailCriteriaList);
        if(Constants.ZERO == rowCountUpdateDoDetail){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0039, 
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        //Check all part no. in D/O are complete shipped or not!!
        for(AsnMaintenanceReturnDomain asn : asnList){
            String key = asn.getAsnDetailDomain().getDoId().toString();
            if(!doIdMap.containsKey(key)){
                doIdMap.put(key, asn.getGroupDoDomain().getLastUpdateDatetime());
            }
        }
        
        Iterator itr = doIdMap.keySet().iterator();
        while(itr.hasNext()){
            String doId = (String)itr.next();
            
            // [IN012] change D/O check status method
            //asnMaintenanceCriteria = new AsnMaintenanceDomain();
            //asnMaintenanceCriteria.setDoId(String.valueOf(doId));
            //asnMaintenanceCriteria.setLastUpdateDatetime((Timestamp)doIdMap.get(doId));
            //asnMaintenanceCriteria.setShipmentStatus(Constants.SHIPMENT_STATUS_ISS);
            //criteriaDomain.add(asnMaintenanceCriteria);
            doStatusCriteria = new SpsTDoDomain();
            doStatusCriteria.setDoId(new BigDecimal(doId));
            doStatusCriteria.setLastUpdateDatetime((Timestamp)doIdMap.get(doId));
            criteriaDomain.add(doStatusCriteria);
            
        }

        // [IN012] Start : change D/O check status method
        //List<AsnMaintenanceDomain> completedShipDoResultList
        //    = deliveryOrderService.searchCompleteShipDo(criteriaDomain);
        //if(Constants.ZERO < completedShipDoResultList.size()){
        //    doList = new ArrayList<SpsTDoDomain>();
        //    doCriteriaList = new ArrayList<SpsTDoCriteriaDomain>();
        //    for(AsnMaintenanceDomain completedShipDo : completedShipDoResultList){
        //        doDomain = new SpsTDoDomain();
        //        doCriteria = new SpsTDoCriteriaDomain();
        //        if(Constants.ZERO == completedShipDo.getDoGroupAsnList().size()){
        //            doDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_ISS);
        //            doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
        //            doDomain.setLastUpdateDatetime(updateDateTime);
        //            doCriteria.setDoId(new BigDecimal(completedShipDo.getDoId()));
        //            doCriteria.setLastUpdateDatetime(completedShipDo.getLastUpdateDatetime());
        //        }else{
        //            doDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
        //            doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
        //            doDomain.setLastUpdateDatetime(updateDateTime);
        //            doCriteria.setDoId(new BigDecimal(completedShipDo.getDoId()));
        //            doCriteria.setLastUpdateDatetime(completedShipDo.getDoGroupAsnList()
        //                .get(Constants.ZERO).getGroupDoDomain().getLastUpdateDatetime());
        //        }
        //        doList.add(doDomain);
        //        doCriteriaList.add(doCriteria);
        //    }
        //    int rowUpdateDo = spsTDoService.updateByCondition(doList, doCriteriaList);
        //    if(rowUpdateDo != criteriaDomain.size()){
        //        MessageUtil.throwsApplicationMessage(locale, 
        //            SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
        //            SupplierPortalConstant.ERROR_CD_SP_90_0002);
        //    }
        //}
        List<SpsTDoDomain> doUpdateStatusList
            = this.deliveryOrderService.searchExamineDoStatus(criteriaDomain);
        if (!doUpdateStatusList.isEmpty()) {
            doList = new ArrayList<SpsTDoDomain>();
            doCriteriaList = new ArrayList<SpsTDoCriteriaDomain>();
            for (SpsTDoDomain doUpdateStatus : doUpdateStatusList) {
                doDomain = new SpsTDoDomain();
                doCriteria = new SpsTDoCriteriaDomain();
                
                doDomain.setShipmentStatus(doUpdateStatus.getShipmentStatus());
                doDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                doDomain.setLastUpdateDatetime(updateDateTime);
                doCriteria.setDoId(doUpdateStatus.getDoId());
                doCriteria.setLastUpdateDatetime(doUpdateStatus.getLastUpdateDatetime());

                doList.add(doDomain);
                doCriteriaList.add(doCriteria);
            }
            
            int rowUpdate = spsTDoService.updateByCondition(doList, doCriteriaList);
            if(rowUpdate != criteriaDomain.size()){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
        }
        // [IN012] End : change D/O check status method
        
        AsnMaintenanceReturnDomain doGroupAsn = asnList.get(Constants.ZERO);
        
        asnInformationDomain = new AsnInformationDomain();
        asnInformationDomain.getSpsTAsnDomain().setAsnNo(asnMaintenanceDomain.getAsnNo());
        asnInformationDomain.getSpsTAsnDomain().setDCd(doGroupAsn.getDCd());
        List<AsnInformationDomain> asnInformationList
            = asnService.searchAsnDetail(asnInformationDomain);
        if(null == asnInformationList || asnInformationList.isEmpty()){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        // Set cigma asn domain to map
        Map tempCigmaAsnMap = new HashMap();
        StringBuffer key = new StringBuffer();
        for(AsnMaintenanceReturnDomain asn : asnList){
            key = new StringBuffer();
            key.append(asnMaintenanceDomain.getAsnNo().trim())
                .append(Constants.SYMBOL_UNDER_SCORE)
                .append(asn.getDPn()).append(Constants.SYMBOL_UNDER_SCORE)
                .append(asn.getAsnDetailDomain().getSpsDoNo());
            if(!tempCigmaAsnMap.containsKey(key.toString())){
                tempCigmaAsnMap.put(key.toString(), asn.getCigmaAsnDomain());
            }
        }
        
        asnMaintenanceDomain.setDoGroupAsnList(
            this.setNewAsnDetailInformationList(asnInformationList, tempCigmaAsnMap));
        
        if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
            this.doSetAsnListToShow(asnMaintenanceDomain.getDoGroupAsnList(), 
                asnMaintenanceDomain);
        }
        
        AsnInformationDomain asnInformation = asnInformationList.get(Constants.ZERO);
        
        //Connect to CIGMA
        Iterator asnItr = asnList.iterator();
        Set<String> dCdSet = new TreeSet<String>();
        
        while(asnItr.hasNext()){
            AsnMaintenanceReturnDomain temp = (AsnMaintenanceReturnDomain)asnItr.next();
            dCdSet.add(temp.getDCd());
        }
        
        companyDenso = new SpsMCompanyDensoDomain();
        companyDenso.setDCd(StringUtil.convertListToVarcharCommaSeperate(dCdSet));
        List<As400ServerConnectionInformationDomain> schemaResultList 
            = companyDensoService.searchAs400ServerList(companyDenso);
        if(null == schemaResultList || schemaResultList.isEmpty()){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0026);
        }
        
        As400ServerConnectionInformationDomain as400Server = schemaResultList.get(Constants.ZERO);
        Integer updateRecord = Constants.ZERO;
        
        String recordResult = CommonWebServiceUtil.asnResourceUpdateAsn(
            asnMaintenanceDomain.getDoGroupAsnList(), as400Server,
            asnMaintenanceDomain.getDscId(), Constants.STR_CANCEL);
        if(!StringUtil.checkNullOrEmpty(recordResult)){
            updateRecord = Integer.valueOf(recordResult);
        }
        
        if(Constants.ZERO == updateRecord){
//            warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_E6_0002)));
            
            SpsTAsnCriteriaDomain asnCriteria = null;
            asnDomain.setTransferCigmaFlg(Constants.TRANSFER_CIGMA_FLAG_NO);
            asnDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
            asnDomain.setLastUpdateDatetime(updateDateTime);
            asnCriteria = new SpsTAsnCriteriaDomain();
            asnCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
            asnCriteria.setDCd(asnInformation.getSpsTAsnDomain().getDCd());
            asnCriteria.setLastUpdateDatetime(
                asnInformation.getSpsTAsnDomain().getLastUpdateDatetime());
            
            int countUpdateTransferFlag = spsTAsnService.updateByCondition(asnDomain, asnCriteria);
            if(Constants.ZERO == countUpdateTransferFlag){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0038, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
//          20230329 Rungsiwut change to throw error to screen.
			MessageUtil.throwsApplicationMessage(locale,
					SupplierPortalConstant.ERROR_CD_SP_E6_0038,
					SupplierPortalConstant.ERROR_CD_SP_E6_0038);
        }
        
        for(AsnMaintenanceReturnDomain asnInfo : asnMaintenanceDomain.getDoGroupAsnList()){
            asnInfo.getCigmaAsnDomain().setPseudoAsnStatus(Constants.ASN_STATUS_D);
            asnInfo.getCigmaAsnDomain().setPseudoPnReceivingStatus(Constants.ASN_STATUS_D);
            asnInfo.setAsnPnReceivingStatus(Constants.ASN_STATUS_CCL);
            asnInfo.setAsnStatus(Constants.ASN_STATUS_CCL);
        }
        
        asnMaintenanceDomain.setWarningMessageList(warningMessageList);
        asnMaintenanceDomain.setAsnRcvStatus(Constants.ASN_STATUS_CCL);
        asnMaintenanceDomain.setLastModified(DateUtil.format(
            new Date(updateDateTime.getTime()), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchSendingEmail(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    public boolean searchSendingEmail(AsnMaintenanceDomain asnMaintenanceDomain, String mode)
        throws ApplicationException
    {
        boolean result = true;
        SpsMUserDensoDomain spsMUserDensoDomain = null;
        List<SpsMUserDomain> userDomainList = null;
        List<String> toList = new ArrayList<String>();
        
        Timestamp updateDateTime = asnMaintenanceDomain.getLastUpdateDatetime();
        AsnMaintenanceReturnDomain asnInformation
            = asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO);
        
        try{
            SendEmailDomain sendEmailDomain = this.getEmailContent(
                asnMaintenanceDomain, mode, updateDateTime);
            if(null != sendEmailDomain){
                //Search User List 
                spsMUserDensoDomain = new SpsMUserDensoDomain();
                spsMUserDensoDomain.setDCd(asnInformation.getDCd());
                spsMUserDensoDomain.setDPcd(asnInformation.getDPcd());
                if(Constants.STR_CREATE.equals(mode) || Constants.STR_CANCEL.equals(mode)){
                    spsMUserDensoDomain.setEmlCreateCancelAsnFlag(Constants.STR_ONE);
                }else if(Constants.STR_UPDATE.equals(mode)){
                    spsMUserDensoDomain.setEmlReviseAsnFlag(Constants.STR_ONE);
                }
                
                userDomainList = userService.searchEmailUserDenso(spsMUserDensoDomain);
                if(null == userDomainList || userDomainList.isEmpty()){
                    return false;
                }else{
                    for (SpsMUserDomain user : userDomainList) {
                        if (!Strings.judgeBlank(user.getEmail())
                            && !toList.contains(user.getEmail())){
                            toList.add(user.getEmail());
                        }
                    }
                }
                sendEmailDomain.setToList(toList);
                result = mailService.sendEmail(sendEmailDomain);
            }
        }catch(Exception e){
            result = false;
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#createViewAsnReport(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    public InputStream createViewAsnReport(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException, Exception
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        InputStream reportFile = null;
        
        Map unitOfMeasureMap = this.generateUnitOfMeasureMap(locale);
        boolean resultValidateLotSize = this.validateLotSizeNotMatch(asnMaintenanceDomain);
        if(!resultValidateLotSize){
            asnMaintenanceDomain.setErrorMessageList(null);
            return null;
        }
        
        List<ApplicationMessageDomain> errorMessageList
            = this.validate(asnMaintenanceDomain, unitOfMeasureMap);
        
        if(Constants.ZERO < errorMessageList.size()){
            asnMaintenanceDomain.setErrorMessageList(errorMessageList);
            return null;
        }
        
        try {
            reportFile = this.asnService.createViewAsnReport(asnMaintenanceDomain);
        }catch(Exception e){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0014);
        }
        return reportFile;
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    public FileManagementDomain searchFileName(AsnMaintenanceDomain asnMaintenanceDomain) 
        throws ApplicationException
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        FileManagementDomain fileManagement = null;
        try{
            fileManagement = fileManagementService.searchFileDownload(
                asnMaintenanceDomain.getFileId(), false, null);
        }catch(FileManagementException ex){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }catch(IOException io){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        return fileManagement;
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchDownloadAsnReport(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain, OutputStream)
     */
    public void searchDownloadAsnReport(AsnMaintenanceDomain asnMaintenanceDomain,
        OutputStream outputStream) throws ApplicationException
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        try{
            fileManagementService.searchFileDownload(asnMaintenanceDomain.getFileId(), true,
                outputStream);
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009, 
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain, OutputStream)
     */
    public void searchLegendInfo(AsnMaintenanceDomain asnMaintenanceDomain,
        OutputStream outputStream) throws ApplicationException
    {
        Locale locale = asnMaintenanceDomain.getLocale();
        try{
            fileManagementService.searchFileDownload(asnMaintenanceDomain.getFileId(),
                true, outputStream);
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009, 
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }
    
    /** {@inheritDoc} 
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#deleteTmpUploadAsn(com.globaldenso.asia.sps.business.domain.AsnUploadingDomain)
     */
    public int deleteTmpUploadAsn(AsnMaintenanceDomain asnMaintenanceDomain) throws Exception
    {
        int recordDelete = Constants.ZERO;
        SpsTmpUploadAsnCriteriaDomain criteria = new SpsTmpUploadAsnCriteriaDomain();
        criteria.setSessionId(asnMaintenanceDomain.getSessionId());
        criteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
        recordDelete = spsTmpUploadAsnService.deleteByCondition(criteria);
        return recordDelete;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#searchRoleCanOperate(java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#transactAllowReviseQty(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    public boolean transactAllowReviseQty(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException {
        Locale locale = asnMaintenanceDomain.getLocale();
        int rowUpdateAsnDetail = Constants.ZERO;
        boolean result = true;
        AsnInformationDomain asnInformationCriteria = null;
        SpsTAsnDetailDomain asnDetailDomain = null;
        SpsTAsnDetailCriteriaDomain asnDetailCriteria = null;
        List<AsnInformationDomain> asnInformationList = null;
        List<AsnMaintenanceReturnDomain> doGroupAsnList = null;
        
        boolean sendMailAllowRevise = false;
        boolean sendMailDisallowRevise = false;
        Map allowReviseOperationMap = new HashMap(); //Allow revise operation flag; 1: allow, 2: disallow.
        
        if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
            doGroupAsnList = asnMaintenanceDomain.getDoGroupAsnList();
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                SupplierPortalConstant.LBL_ASN_DATA);
        }
        Timestamp updateDateTime = commonService.searchSysDate();
        AsnMaintenanceReturnDomain asnDetail = doGroupAsnList.get(Constants.ZERO);
        
//        //Set all record to default (allowReviseFlag = '0')
//        asnDetailDomain = new SpsTAsnDetailDomain();
//        asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
//        asnDetailDomain.setAllowReviseFlag(Constants.STR_ZERO);
//        asnDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
//        asnDetailDomain.setLastUpdateDatetime(updateDateTime);
//        asnDetailCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
//        asnDetailCriteria.setDCd(asnDetail.getDCd());
//        rowUpdateAsnDetail = spsTAsnDetailService.updateByCondition(
//            asnDetailDomain, asnDetailCriteria);
//        if(asnMaintenanceDomain.getDoGroupAsnList().size() != rowUpdateAsnDetail){
//            MessageUtil.throwsApplicationMessageWithLabel(locale,
//                SupplierPortalConstant.ERROR_CD_SP_E6_0034, 
//                SupplierPortalConstant.LBL_UPDATE_ALLOW_REVISE_FLAG);
//        }
        
        for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
            if(Constants.STR_ONE.equals(asnList.getRecordGroupByLane()))
            {
                if(Constants.STR_ONE.equals(asnList.getAsnDetailDomain().getAllowReviseFlag()))
                {
                    if(!asnList.getAllowReviseFlagBackUp().equals(
                        asnList.getAsnDetailDomain().getAllowReviseFlag())){
                        asnDetailDomain = new SpsTAsnDetailDomain();
                        asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
                        asnDetailDomain.setAllowReviseFlag(Constants.STR_ONE);
                        asnDetailDomain.setLastUpdateDatetime(updateDateTime);
                        asnDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                        asnDetailCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
                        asnDetailCriteria.setDCd(asnList.getDCd());
                        asnDetailCriteria.setRcvLane(asnList.getAsnDetailDomain().getRcvLane());
                        
                        rowUpdateAsnDetail = spsTAsnDetailService.updateByCondition(
                            asnDetailDomain, asnDetailCriteria);
                        if(Constants.ZERO == rowUpdateAsnDetail){
                            MessageUtil.throwsApplicationMessageWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E6_0034, 
                                SupplierPortalConstant.LBL_UPDATE_ALLOW_REVISE_FLAG);
                        }
                        
                        allowReviseOperationMap.put(
                            asnList.getAsnDetailDomain().getRcvLane(), Constants.STR_ONE);
                        sendMailAllowRevise = true;
                    }
                }else{ // allow revise flag = '0'
                    if(!asnList.getAllowReviseFlagBackUp().equals(
                        asnList.getAsnDetailDomain().getAllowReviseFlag())){
                        asnDetailDomain = new SpsTAsnDetailDomain();
                        asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
                        asnDetailDomain.setAllowReviseFlag(Constants.STR_ZERO);
                        asnDetailDomain.setLastUpdateDatetime(updateDateTime);
                        asnDetailDomain.setLastUpdateDscId(asnMaintenanceDomain.getDscId());
                        asnDetailCriteria.setAsnNo(asnMaintenanceDomain.getAsnNo());
                        asnDetailCriteria.setDCd(asnList.getDCd());
                        asnDetailCriteria.setRcvLane(asnList.getAsnDetailDomain().getRcvLane());
                        
                        rowUpdateAsnDetail = spsTAsnDetailService.updateByCondition(
                            asnDetailDomain, asnDetailCriteria);
                        if(Constants.ZERO == rowUpdateAsnDetail){
                            MessageUtil.throwsApplicationMessageWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E6_0034, 
                                SupplierPortalConstant.LBL_UPDATE_ALLOW_REVISE_FLAG);
                        }
                        allowReviseOperationMap.put(
                            asnList.getAsnDetailDomain().getRcvLane(), Constants.STR_TWO);
                        sendMailDisallowRevise = true;
                    }
                }
            }
        }
        
        //Search ASN detail
        asnInformationCriteria = new AsnInformationDomain();
        asnInformationCriteria.getSpsTAsnDomain().setAsnNo(asnMaintenanceDomain.getAsnNo());
        asnInformationCriteria.getSpsTAsnDomain().setDCd(asnDetail.getDCd());
        asnInformationList = asnService.searchAsnDetail(asnInformationCriteria);
        
        // Set cigma asn domain to map
        Map tempCigmaAsnMap = new HashMap();
        StringBuffer key = new StringBuffer();
        for(AsnMaintenanceReturnDomain asnList : doGroupAsnList){
            key = new StringBuffer();
            key.append(asnMaintenanceDomain.getAsnNo().trim())
                .append(Constants.SYMBOL_UNDER_SCORE)
                .append(asnList.getDPn()).append(Constants.SYMBOL_UNDER_SCORE)
                .append(asnList.getAsnDetailDomain().getSpsDoNo());
            if(!tempCigmaAsnMap.containsKey(key.toString())){
                tempCigmaAsnMap.put(key.toString(), asnList.getCigmaAsnDomain());
            }
        }
        
        if(Constants.ZERO < asnInformationList.size()){
            asnMaintenanceDomain.setDoGroupAsnList(
                this.setNewAsnDetailInformationList(asnInformationList, tempCigmaAsnMap));
            this.doSetAsnListToShow(asnMaintenanceDomain.getDoGroupAsnList(), asnMaintenanceDomain);
        }
        
        //Set revise shipping qty flag
        String revisingFlag = Constants.STR_ZERO;
        List<Boolean> reviseCompleteList = new ArrayList<Boolean>();
        AsnMaintenanceReturnDomain asnTmp = null;
        
        for(AsnMaintenanceReturnDomain asn : asnMaintenanceDomain.getDoGroupAsnList()){
            //Set revise shipping qty complete flag by RCV Lane
            if(Constants.STR_ONE.equals(asn.getRecordGroupByLane())){
                if(null == asnTmp){
                    asnTmp = asn;
                }else{
                    if(reviseCompleteList.contains(Boolean.FALSE)){
                        asnTmp.setReviseCompleteFlag(Constants.STR_ZERO);
                    }else{
                        asnTmp.setReviseCompleteFlag(Constants.STR_ONE);
                    }
                    asnTmp = asn;
                }
                reviseCompleteList.clear();
            }
            
            if(Constants.STR_ONE.equals(asn.getRecordGroup())){
                if(Constants.STR_ONE.equals(asn.getAsnDetailDomain().getAllowReviseFlag())
                    && Constants.ZERO < asn.getShippingQtyByPart().compareTo(
                        asn.getAsnReceivedQtyByPart())){
                    revisingFlag = Constants.STR_ONE;
                }else{
                    revisingFlag = Constants.STR_ZERO;
                }
                
                if(Constants.ZERO == asn.getShippingQtyByPart().compareTo(
                    asn.getAsnReceivedQtyByPart())){
                    reviseCompleteList.add(Boolean.TRUE);
                }else{
                    reviseCompleteList.add(Boolean.FALSE);
                }
            }
            asn.setRevisingFlag(revisingFlag);
        }
        if(reviseCompleteList.contains(Boolean.FALSE)){
            asnTmp.setReviseCompleteFlag(Constants.STR_ZERO);
        }else{
            asnTmp.setReviseCompleteFlag(Constants.STR_ONE);
        }
        reviseCompleteList.clear();
        
        //send mail
        try{
            List<SpsMUserDomain> userList = null;
            if(sendMailAllowRevise || sendMailDisallowRevise){
                AsnMaintenanceReturnDomain asn
                    = asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO);
                UserSupplierDetailDomain userSupplierDomain = new UserSupplierDetailDomain();
                SpsMUserSupplierDomain spsMUserSupplierDomain = new SpsMUserSupplierDomain();
                SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                
                spsMUserSupplierDomain.setSCd(asn.getSCd());
                spsMUserSupplierDomain.setSPcd(asn.getSPcd());
                spsMUserSupplierDomain.setEmlAllowReviseFlag(Constants.STR_ONE);
                spsMUserDomain.setIsActive(Constants.STR_ONE);
                
                userSupplierDomain.setSpsMUserSupplierDomain(spsMUserSupplierDomain);
                userSupplierDomain.setSpsMUserDomain(spsMUserDomain);
                
                userList = userSupplierService.searchEmailUserSupplier(userSupplierDomain);
                if(null == userList || userList.isEmpty()){
                    return false;
                }
            }
            if(sendMailAllowRevise){
                SendEmailDomain sendEmailDomain = this.getEmailAllowReviseContent(
                    asnMaintenanceDomain, userList, Constants.STR_CREATE, allowReviseOperationMap);
                result = mailService.sendEmail(sendEmailDomain);
            }
            if(result && sendMailDisallowRevise){
                SendEmailDomain sendEmailDomain = this.getEmailAllowReviseContent(
                    asnMaintenanceDomain, userList, Constants.STR_CANCEL, allowReviseOperationMap);
                result = mailService.sendEmail(sendEmailDomain);
            }
        }catch(Exception e){
            result = false;
        }
        return result;
    }
    
    /**
     * Validate ASN
     * @return the list of error message
     * @param asnMaintenanceDomain the asnMaintenance domain
     * @param unitOfMeasureMap the unit of measure map
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    private List<ApplicationMessageDomain> validate(AsnMaintenanceDomain asnMaintenanceDomain
        , Map unitOfMeasureMap) throws Exception
    {
        boolean isValidActualEtdDate = true;
        boolean isValidPlanEtaDate = true;
        boolean isValidAcutalEtdTime = true;
        boolean isValidPlanEtaTime = true;
        
        List<ApplicationMessageDomain> errorMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        Locale locale = asnMaintenanceDomain.getLocale();
        
        asnMaintenanceDomain.setUnmatchedLotSizeFlag(Constants.STR_ZERO);
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getTemporaryMode()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_TEMPORARY_MODE)));
        }
        if(null == asnMaintenanceDomain.getDoGroupAsnList() 
            || Constants.ZERO == asnMaintenanceDomain.getDoGroupAsnList().size())
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_ASN_DATA)));
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getActualEtdDate()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_ACTUAL_ETD_DATE)));
            isValidActualEtdDate = false;
        }else{
            if(!DateUtil.isValidDate(asnMaintenanceDomain.getActualEtdDate(),
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_ACTUAL_ETD_DATE)));
                isValidActualEtdDate = false;
            }
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getActualEtdTime()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_ACTUAL_ETD_TIME)));
            isValidAcutalEtdTime = false;
        }else{
            if(!asnMaintenanceDomain.getActualEtdTime().matches(Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_ACTUAL_ETD_TIME)));
                isValidAcutalEtdTime = false;
            }
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getPlanEtaDate()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_PLAN_ETA_DATE)));
            isValidPlanEtaDate = false;
        }else{
            if(!DateUtil.isValidDate(
                asnMaintenanceDomain.getPlanEtaDate(), DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_PLAN_ETA_DATE)));
                isValidPlanEtaDate = false;
            }
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getPlanEtaTime()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_PLAN_ETA_TIME)));
            isValidPlanEtaTime = false;
        }else{
            if(!asnMaintenanceDomain.getPlanEtaTime().matches(Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_PLAN_ETA_TIME)));
                isValidPlanEtaTime = false;
            }
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getNoOfPallet()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_NUMBER_PALLET)));
        }else{
            String noOfPalletStr = asnMaintenanceDomain.getNoOfPallet()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
            if(!StringUtil.isNumeric(noOfPalletStr))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_NUMBER_PALLET)));
            }else{
                
                //*** Check Digit of No. of Pallet
                boolean isNumberFormat = true;
                if(asnMaintenanceDomain.getNoOfPallet().contains(Constants.SYMBOL_DOT)){
                    if(!StringUtil.isNumberFormat(noOfPalletStr, Constants.THIRTEEN, Constants.TWO)){
                        isNumberFormat = false;
                    }
                }else{
                    BigDecimal noOfPallet = new BigDecimal(noOfPalletStr);
                    if(Constants.ELEVEN < noOfPallet.precision()){
                        isNumberFormat = false;
                    }
                }
                
                if(!isNumberFormat){
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        MessageUtil.getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_NUMBER_PALLET),
                                    String.valueOf(Constants.FORMAT_DIGIT_NUMBER_OF_PALLET)})));
                }
            }
        }
        if(StringUtil.checkNullOrEmpty(asnMaintenanceDomain.getTripNo()))
        {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                    SupplierPortalConstant.LBL_TRIP_NO)));
        }else{
            if(SupplierPortalConstant.LENGTH_TRIP_NO < asnMaintenanceDomain.getTripNo().length())
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_TRIP_NO),
                            Constants.STR_THREE})));
            }
        }
        if(isValidActualEtdDate && isValidPlanEtaDate && isValidAcutalEtdTime && isValidPlanEtaTime)
        {
            int result = DateUtil.compareDate(asnMaintenanceDomain.getActualEtdDate(), 
                asnMaintenanceDomain.getPlanEtaDate());
            if(Constants.ZERO < result){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_ACTUAL_ETD), 
                                MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_PLAN_ETA)})));
            }else if(Constants.ZERO == result){
                Date d1 = DateUtil.parseToUtilDate(
                    asnMaintenanceDomain.getActualEtdTime(), DateUtil.PATTERN_HHMM_COLON);
                Date d2 = DateUtil.parseToUtilDate(
                    asnMaintenanceDomain.getPlanEtaTime(), DateUtil.PATTERN_HHMM_COLON);
                if(d1.after(d2)){
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_ACTUAL_ETD),
                                    MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_PLAN_ETA)})));
                }
            }
        }
        
        for(AsnMaintenanceReturnDomain doGroupAsn : asnMaintenanceDomain.getDoGroupAsnList())
        {
            if(Constants.STR_ONE.equals(doGroupAsn.getRecordGroup())){
                
                if(unitOfMeasureMap.containsKey(doGroupAsn.getAsnDetailDomain().getUnitOfMeasure()))
                {
                    String displayDecimalPointFlag = (String)unitOfMeasureMap.get(
                        doGroupAsn.getAsnDetailDomain().getUnitOfMeasure());
                    
                    if(Constants.DISPLAY_DECIMAL_FLAG_NO.equals(displayDecimalPointFlag)){
                        if(Constants.ZERO < doGroupAsn.getShippingQtyByPart().scale()){
                            errorMessageList.add(
                                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                                    MessageUtil.getApplicationMessageHandledException(locale,
                                        SupplierPortalConstant.ERROR_CD_SP_E7_0033,
                                        new String[] {doGroupAsn.getSPn()})));
                        }
                    }
                }else{
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                        SupplierPortalConstant.LBL_UNIT_OF_MEASURE);
                }
            }
        }
        
        if(Constants.STR_ONE.equals(asnMaintenanceDomain.getTemporaryMode())){
            boolean isLessThanEqualRemaining = true;
            boolean isNumberFormat = true;
            // [IN039] validate shipping box
            boolean isShippingBoxNotZero = true;
            boolean isShippingBoxIsZero = true;
            boolean isShippingBoxNotLong = true;
            boolean isShippingBoxNotNull = true;
            
            for(AsnMaintenanceReturnDomain doGroupAsn : asnMaintenanceDomain.getDoGroupAsnList())
            {
                if(Constants.STR_ONE.equals(doGroupAsn.getRecordGroup())){
                    
                    //validate Shipping QTY format
                    if(isNumberFormat){
                        if(String.valueOf(doGroupAsn.getShippingQtyByPart()).contains(
                            Constants.SYMBOL_DOT)){
                            if(!StringUtil.isNumberFormat(
                                String.valueOf(doGroupAsn.getShippingQtyByPart()),
                                Constants.NINE, Constants.TWO)){
                                isNumberFormat = false;
                            }
                        }else{
                            if(Constants.SEVEN < doGroupAsn.getShippingQtyByPart().precision()){
                                isNumberFormat = false;
                            }
                        }
                        if(!isNumberFormat){
                            errorMessageList.add(new ApplicationMessageDomain(
                                Constants.MESSAGE_FAILURE, 
                                MessageUtil.getApplicationMessageHandledException(locale,
                                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                                        MessageUtil.getLabelHandledException(locale,
                                            SupplierPortalConstant.LBL_SHIPPING_QTY),
                                            String.valueOf(Constants.FORMAT_DIGIT_SHIPPING_QTY)})));
                        }
                    }
                    
                    /* [IN014] Start : ShippingQty can more than OrderQty but not different
                     * more than one.
                     * */ 
                    //if(Constants.ZERO < doGroupAsn.getShippingQtyByPart().compareTo(
                    //    doGroupAsn.getTotalRemainingQtyByPart()) && isLessThanEqualRemaining)
                    //{
                    BigDecimal diffShippingRemain = doGroupAsn.getShippingQtyByPart().subtract(
                        doGroupAsn.getTotalRemainingQtyByPart());
                    
                    // shipping - remain >= 1
                    if(diffShippingRemain.compareTo(Constants.BIG_DECIMAL_ONE) >= Constants.ZERO 
                        && isLessThanEqualRemaining)
                    {
                    
                    /* [IN014] Start : ShippingQty can more than OrderQty but not different
                     * more than one.
                     * */ 
                        
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                                SupplierPortalConstant.LBL_SHIPQTY_GREATER_REMAINQTY)));
                        isLessThanEqualRemaining = false;
                    }
                    
                    // Start : [IN039] Validate Shipping Box Qty SHIPPING_BOX_QTY
                    if(StringUtil.checkNullOrEmpty(doGroupAsn.getShippingBoxQtyByPartInput())) {
                        if (isShippingBoxNotNull) {
                            errorMessageList.add(new ApplicationMessageDomain(
                                Constants.MESSAGE_FAILURE, 
                                    MessageUtil.getApplicationMessageWithLabel(locale, 
                                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, 
                                    SupplierPortalConstant.LBL_SHIPPING_BOX_QTY)));
                            isShippingBoxNotNull = false;
                        }
                        continue;
                    }
                    
                    if (null != doGroupAsn.getShippingBoxQtyByPartInput()) {
                        doGroupAsn.setShippingBoxQtyByPartInput(
                            doGroupAsn.getShippingBoxQtyByPartInput().replaceAll(
                                Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
                    }
                    
                    if (isShippingBoxNotLong 
                        && Constants.ELEVEN < doGroupAsn.getShippingBoxQtyByPartInput().length())
                    {
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageHandledException(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                                    MessageUtil.getLabelHandledException(locale,
                                        SupplierPortalConstant.LBL_SHIPPING_BOX_QTY),
                                        String.valueOf(Constants.FORMAT_DIGIT_SHIPPING_BOX_QTY)})));
                        isShippingBoxNotLong = false;
                    }
                    
                    BigDecimal shippingBox = NumberUtil.toBigDecimalDefaultZero(
                        doGroupAsn.getShippingBoxQtyByPartInput());
                    if (isShippingBoxNotZero 
                        && Constants.ZERO != BigDecimal.ZERO.compareTo(
                            doGroupAsn.getShippingQtyByPart())
                        && Constants.ZERO == BigDecimal.ZERO.compareTo(shippingBox))
                    {
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageHandledException(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0040)));
                        isShippingBoxNotZero = false;
                    }
                    
                    if (isShippingBoxIsZero
                        && Constants.ZERO == BigDecimal.ZERO.compareTo(
                            doGroupAsn.getShippingQtyByPart())
                        && Constants.ZERO != BigDecimal.ZERO.compareTo(shippingBox))
                    {
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageHandledException(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0041)));
                        isShippingBoxIsZero = false;
                    }
                    // End : [IN039] Validate Shipping Box Qty
                }
            }
        }
        
        //validate change reason cd.
        if(Constants.STR_ONE.equals(asnMaintenanceDomain.getTemporaryMode())){
            for(AsnMaintenanceReturnDomain doGroupAsn : asnMaintenanceDomain.getDoGroupAsnList())
            {
                if(Constants.STR_ONE.equals(doGroupAsn.getRecordGroup())){
                    if(doGroupAsn.getShippingQtyByPart().compareTo(
                        doGroupAsn.getTotalRemainingQtyByPart()) < Constants.ZERO
                        && StringUtil.checkNullOrEmpty(doGroupAsn.getChangeReasonCd()))
                    {
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                                SupplierPortalConstant.LBL_CHANGE_REASON)));
                        break;
                    }
                }
            }
        }else{
            for(AsnMaintenanceReturnDomain doGroupAsn : asnMaintenanceDomain.getDoGroupAsnList())
            {
                if(Constants.STR_ONE.equals(doGroupAsn.getRecordGroup())){
                    if(Constants.STR_ONE.equals(doGroupAsn.getRevisingFlag())){
                        if(StringUtil.checkNullOrEmpty(doGroupAsn.getChangeReasonCd()))
                        {
                            errorMessageList.add(new ApplicationMessageDomain(
                                Constants.MESSAGE_FAILURE,
                                MessageUtil.getApplicationMessageWithLabel(locale,
                                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                                    SupplierPortalConstant.LBL_CHANGE_REASON)));
                            break;
                        }
                    }
                }
            }
        }
        
        if(Constants.STR_ZERO.equals(asnMaintenanceDomain.getTemporaryMode())){
            for(AsnMaintenanceReturnDomain asnList : asnMaintenanceDomain.getDoGroupAsnList()){
                if(Constants.STR_ONE.equals(asnList.getRecordGroup())
                    && Constants.STR_ONE.equals(asnList.getRevisingFlag()))
                {
                    if(Constants.ZERO != asnList.getShippingQtyByPart().compareTo(
                        asnList.getAsnReceivedQtyByPart())){
                        
                        String displayDecimalPointFlag = (String)unitOfMeasureMap.get(
                            asnList.getAsnDetailDomain().getUnitOfMeasure());
                        
                        String receivedQtyStr = null;
                        if(Constants.DISPLAY_DECIMAL_FLAG_NO.equals(displayDecimalPointFlag)){
                            receivedQtyStr = StringUtil.toNumberFormat(
                                asnList.getAsnReceivedQtyByPart().toString());
                        }else{
                            receivedQtyStr = StringUtil.toCurrencyFormat(
                                asnList.getAsnReceivedQtyByPart().toString());
                        }
                        
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE,
                            MessageUtil.getApplicationMessageHandledException(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0017,
                                new String[] {receivedQtyStr})));
                        break;
                    }
                }
            }
        }
        return errorMessageList;
    }
    
    /**
     * Validate Unmatched Lot Size
     * @return boolean, true: matched with lot size, false: not matched with lot size
     * @param asnMaintenanceDomain the asnMaintenance domain
     */
    private boolean validateLotSizeNotMatch(AsnMaintenanceDomain asnMaintenanceDomain){
        boolean resultValidate = true;
        boolean isUnmatchedLotSize = false;
        String lotSizeNotMatchedFlag = Constants.EMPTY_STRING;
        if(Constants.STR_ZERO.equals(asnMaintenanceDomain.getSkipValidateLotSizeFlag())){
            if(Constants.STR_ONE.equals(asnMaintenanceDomain.getTemporaryMode())){
                if(null != asnMaintenanceDomain.getDoGroupAsnList()
                    && Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size())
                {
                    for(AsnMaintenanceReturnDomain asnItem
                        : asnMaintenanceDomain.getDoGroupAsnList())
                    {
                        if(Constants.STR_ONE.equals(asnItem.getRecordGroup())){
                            BigDecimal shippingQty = asnItem.getShippingQtyByPart();
                            BigDecimal qtyBox = asnItem.getAsnDetailDomain().getQtyBox();
                            BigDecimal result = shippingQty.remainder(qtyBox);
                            if(Constants.ZERO < result.compareTo(new BigDecimal(Constants.ZERO))){
                                isUnmatchedLotSize = true;
                                lotSizeNotMatchedFlag = Constants.STR_ONE;
                            }else{
                                lotSizeNotMatchedFlag = Constants.STR_ZERO;
                            }
                        }
                        asnItem.setLotSizeNotMatchFlag(lotSizeNotMatchedFlag);
                    }
                }
            }else{
                if(null != asnMaintenanceDomain.getDoGroupAsnList()
                    && Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size())
                {
                    for(AsnMaintenanceReturnDomain asnItem
                        : asnMaintenanceDomain.getDoGroupAsnList()){
                        if(Constants.STR_ONE.equals(asnItem.getRecordGroup())){
                            lotSizeNotMatchedFlag = Constants.STR_ZERO;
                            if(Constants.STR_ONE.equals(asnItem.getRevisingFlag())){
                                BigDecimal shippingQty = asnItem.getShippingQtyByPart();
                                BigDecimal qtyBox = asnItem.getAsnDetailDomain().getQtyBox();
                                BigDecimal result = shippingQty.remainder(qtyBox);
                                if(Constants.ZERO 
                                    < result.compareTo(new BigDecimal(Constants.ZERO)))
                                {
                                    isUnmatchedLotSize = true;
                                    lotSizeNotMatchedFlag = Constants.STR_ONE;
                                }
                            }
                        }
                        asnItem.setLotSizeNotMatchFlag(lotSizeNotMatchedFlag);
                    }
                }
            }
            
            if(isUnmatchedLotSize){
                asnMaintenanceDomain.setUnmatchedLotSizeFlag(Constants.STR_ONE);
                resultValidate = false;
            }else{
                asnMaintenanceDomain.setUnmatchedLotSizeFlag(Constants.STR_ZERO);
            }
        }
        return resultValidate;
    }
    
    /**
     * Gets digit of month
     * @return the string
     * @param updateDateTime the time stamp
     */
    private String getDigitOfMonth(Timestamp updateDateTime)
    {
        String digitOfMonth = Constants.EMPTY_STRING;
        String monthStr = DateUtil.getMonth(updateDateTime);
        BigDecimal month = new BigDecimal(monthStr);
        if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.ONE))){
            digitOfMonth = Constants.STR_ONE;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.TWO))){
            digitOfMonth = Constants.STR_TWO;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.THREE))){
            digitOfMonth = Constants.STR_THREE;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.FOUR))){
            digitOfMonth = Constants.STR_FOUR;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.FIVE))){
            digitOfMonth = Constants.STR_FIVE;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.SIX))){
            digitOfMonth = Constants.STR_SIX;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.SEVEN))){
            digitOfMonth = Constants.STR_SEVEN;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.EIGHT))){
            digitOfMonth = Constants.STR_EIGHT;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.NINE))){
            digitOfMonth = Constants.STR_NINE;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.TEN))){
            digitOfMonth = Constants.STR_X;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.ELEVEN))){
            digitOfMonth = Constants.STR_Y;
        }else if(Constants.ZERO == month.compareTo(new BigDecimal(Constants.TWELVE))){
            digitOfMonth = Constants.STR_Z;
        }
        return digitOfMonth;
    }
    
    /**
     * Gets minimum of shipDateTime for planEtd
     * @return the date
     * @param doGroupAsnList the list of asn maintenance return domain
     */
    private Date getMinimumOfShipDateTime(List<AsnMaintenanceReturnDomain> doGroupAsnList)
    {
        Date minDate = null;
        for(AsnMaintenanceReturnDomain doGroupAsn : doGroupAsnList){
            Date shipDate = DateUtil.parseToUtilDate(
                doGroupAsn.getGroupDoDomain().getShipDatetime().toString(),
                DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH);
            if(null == minDate){
                minDate = shipDate;
            }else{
                if(shipDate.before(minDate)){
                    minDate = shipDate;
                }
            }
        }
        return minDate;
    }
    
    /**
     * Gets Email content
     * @return the send email domain
     * @param asnMaintenanceDomain the asn maintenance domain
     * @param mode the string
     * @param updateDatetime the time stamp
     */
    private SendEmailDomain getEmailContent(AsnMaintenanceDomain asnMaintenanceDomain,
        String mode, Timestamp updateDatetime)
    {
        String subject = Constants.EMPTY_STRING;
        String headerText2 = Constants.EMPTY_STRING;
        StringBuffer userName =  new StringBuffer();
        int rowNo = Constants.ONE;
        StringBuffer emailBuffer = new StringBuffer();
        boolean isShortShipmentAsn = false;
        
        Locale locale = asnMaintenanceDomain.getLocale();
        List<AsnMaintenanceReturnDomain> asnList = new ArrayList<AsnMaintenanceReturnDomain>();
        asnList.addAll(asnMaintenanceDomain.getDoGroupAsnList());
        
        if(Constants.STR_CREATE.equals(mode)){
            subject = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_SUBJECT_CREATE, null);
            headerText2 = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CREATE, null);
        }else if(Constants.STR_UPDATE.equals(mode)){
            subject = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_SUBJECT_UPDATE, null);
            headerText2 = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_UPDATE, null);
        }else{
            subject = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_SUBJECT_CANCEL, null);
            headerText2 = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CANCEL, null);
        }
        String headerText1 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE1, null);
        String headerText3 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE3, null);
        String headerText4 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE4, null);
        
        String headerTable1 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE1, null);
        String headerTable2 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE2, null);
        
        String detailNormal = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE1, null);
        String detailShortShipment = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE2, null);
        String detailTableEnd = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE3, null);
        String remark = MessageUtil.getEmailLabelHandledException(locale,
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_REMARK, null);
        String footer1 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_FOOTER_LINE1, null);
        String footer2 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_FOOTER_LINE2, null);
        
        //Prepare parameter
        AsnMaintenanceReturnDomain asnDetailDomain = asnList.get(Constants.ZERO);
        
        String lastModified = DateUtil.format(new Date(
            asnDetailDomain.getAsnDomain().getLastUpdateDatetime().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        emailBuffer.append(subject).append(asnDetailDomain.getAsnDomain().getAsnNo());
        subject = emailBuffer.toString();
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_ASNNO,
            asnMaintenanceDomain.getAsnNo());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SCD, 
            asnDetailDomain.getAsnDomain().getVendorCd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SPCD, 
            asnDetailDomain.getSPcd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_DCD, 
            asnDetailDomain.getDCd());
        headerText3 = headerText3.replace(SupplierPortalConstant.REPLACE_DPCD, 
            asnDetailDomain.getDPcd());
        headerText3 = headerText3.replace(SupplierPortalConstant.REPLACE_LAST_MODIFIED, 
            lastModified + Constants.SYMBOL_SPACE + asnMaintenanceDomain.getUtc());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_ACTUAL_ETD, 
            asnMaintenanceDomain.getActualEtdDate());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_PLAN_ETA, 
            asnMaintenanceDomain.getPlanEtaDate());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_NOOFPALLET, 
            asnMaintenanceDomain.getNoOfPallet());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_TRIPNO, 
            asnMaintenanceDomain.getTripNo());
        String newLine = null;
        String detailAllLine = null;
        
        
        if(Constants.STR_CREATE.equals(mode)){
            if(null != asnMaintenanceDomain.getAsnShortShipList()
                && !asnMaintenanceDomain.getAsnShortShipList().isEmpty()){
                
                for(AsnMaintenanceReturnDomain asnShortShipItem
                    : asnMaintenanceDomain.getAsnShortShipList())
                {
                    asnShortShipItem.getUserDomain().setFirstName(
                        asnDetailDomain.getUserDomain().getFirstName());
                    asnShortShipItem.getUserDomain().setMiddleName(
                        asnDetailDomain.getUserDomain().getMiddleName());
                    asnShortShipItem.getUserDomain().setLastName(
                        asnDetailDomain.getUserDomain().getLastName());
                }
                asnList.addAll(asnMaintenanceDomain.getAsnShortShipList());
                SortUtil.sort(asnList, SortUtil.COMPARE_ASN_MAINTENANCE);
            }
        }
        
        
        emailBuffer.setLength(Constants.ZERO);
        String rowNumber = Constants.EMPTY_STRING;
        String rcvLane = Constants.EMPTY_STRING;
        String dpartNo = Constants.EMPTY_STRING;
        String spsDoNo = Constants.EMPTY_STRING;
        String revision = Constants.EMPTY_STRING;
        String shippingQty = Constants.EMPTY_STRING;
        String um = Constants.EMPTY_STRING;
        String qtyBox = Constants.EMPTY_STRING;
        String shippingBoxQty = Constants.EMPTY_STRING;
        String orderQty = Constants.EMPTY_STRING;
        StringBuffer changeReason = new StringBuffer();
        for(AsnMaintenanceReturnDomain asn : asnList){
            if(Constants.ZERO == asn.getTotalRemainingQty().compareTo(
                new BigDecimal(Constants.ZERO))){
                newLine = detailNormal;
            }else{
                newLine = detailShortShipment;
                isShortShipmentAsn = true;
            }
//            if(asn.getAsnDetailDomain().getShippingQty().compareTo(
//                asn.getGroupDoDetailDomain().getCurrentOrderQty()) < Constants.ZERO){
//            }else{
//            }
            userName.setLength(Constants.ZERO);
            rowNumber = Constants.EMPTY_STRING;
            rcvLane = Constants.EMPTY_STRING;
            dpartNo = Constants.EMPTY_STRING;
            spsDoNo = Constants.EMPTY_STRING;
            revision = Constants.EMPTY_STRING;
            shippingQty = Constants.EMPTY_STRING;
            orderQty = Constants.EMPTY_STRING;
            um = Constants.EMPTY_STRING;
            qtyBox = Constants.EMPTY_STRING;
            shippingBoxQty = Constants.EMPTY_STRING;
            changeReason.setLength(Constants.ZERO);
            
            rowNumber = String.valueOf(rowNo);
            rcvLane = asn.getAsnDetailDomain().getRcvLane();
            dpartNo = asn.getDPn();
            spsDoNo = asn.getAsnDetailDomain().getSpsDoNo();
            revision = asn.getAsnDetailDomain().getRevision();
//            shippingQty = asn.getAsnDetailDomain().getShippingQty().toString();
            shippingQty = asn.getShippingQtyStr();
            orderQty = asn.getOrderQtyStr();
            um = asn.getAsnDetailDomain().getUnitOfMeasure();
            qtyBox = asn.getAsnDetailDomain().getQtyBox().toString();
            shippingBoxQty = asn.getAsnDetailDomain().getShippingBoxQty().toString();
            if(!StringUtil.checkNullOrEmpty(asn.getAsnDetailDomain().getChangeReasonCd())){
                changeReason.append(asn.getAsnDetailDomain().getChangeReasonCd())
                    .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_COLON)
                    .append(Constants.SYMBOL_SPACE).append(asn.getChangeReasonName());
            }
            userName.append(asn.getUserDomain().getFirstName())
                .append(Constants.SYMBOL_SPACE)
                .append(StringUtil.nullToEmpty(asn.getUserDomain().getMiddleName()))
                .append(Constants.SYMBOL_SPACE)
                .append(asn.getUserDomain().getLastName());
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_USERNAME,
                StringUtil.rightPad(userName.toString(), Constants.THIRTY_ONE));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_ROWNO,
                StringUtil.rightPad(rowNumber, Constants.SIX));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_RCVLANE,
                StringUtil.rightPad(rcvLane,  Constants.THIRTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DPARTNO,
                StringUtil.rightPad(dpartNo, Constants.FIFTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSDONO,
                StringUtil.rightPad(spsDoNo, Constants.FOURTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_REVISION,
                StringUtil.rightPad(revision, Constants.FIVE));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_CURRENT_QTY, orderQty);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SHIPPING_QTY,
                StringUtil.rightPad(shippingQty, Constants.FIFTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_UM,
                StringUtil.rightPad(um, Constants.SIX));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_QTYBOX,
                StringUtil.rightPad(qtyBox, Constants.FOURTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SHIPPINGBOX_QTY,
                StringUtil.rightPad(shippingBoxQty, Constants.FOURTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_CHANGE_REASON,
                changeReason.toString());
            emailBuffer.append(newLine);
            rowNo += Constants.ONE;
        }
        
        if(Constants.STR_CREATE.equals(mode) && !isShortShipmentAsn){
            return null;
        }
        
        detailAllLine = emailBuffer.toString();
        String detail = null;
        emailBuffer.setLength(Constants.ZERO);
        emailBuffer
            .append(headerText1)
            .append(Constants.SYMBOL_NEWLINE)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText2)
            .append(Constants.SYMBOL_NEWLINE)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText3)
            .append(headerText4)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerTable1)
            .append(headerTable2)
            .append(detailAllLine)
            .append(detailTableEnd);
        if(isShortShipmentAsn){
            emailBuffer.append(remark);
        }
        emailBuffer.append(Constants.SYMBOL_NEWLINE)
            .append(footer1).append(Constants.SYMBOL_NEWLINE)
            .append(footer2).append(Constants.SYMBOL_NEWLINE);
        detail = emailBuffer.toString();
        
        SendEmailDomain sendEmail = new SendEmailDomain();
        sendEmail.setEmailFrom(ContextParams.getDefaultEmailFrom());
        sendEmail.setEmailTo(Constants.EMPTY_STRING);
        sendEmail.setToList(null);
        sendEmail.setHeader(subject);
        sendEmail.setContents(detail);
        sendEmail.setEmailSmtp(ContextParams.getEmailSmtp());
        return sendEmail;
    }
    
    /**
     * Gets email Allow Revise content
     * @return the send email domain
     * @param asnMaintenanceDomain the asn maintenance domain
     * @param userList the list of user domain 
     * @param mode the string
     * @param allowReviseOperationMap the map operation flag 1: allow, 2: disallow
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    private SendEmailDomain getEmailAllowReviseContent(AsnMaintenanceDomain asnMaintenanceDomain, 
        List<SpsMUserDomain> userList, String mode, Map allowReviseOperationMap)
    {
        String subject = Constants.EMPTY_STRING;
        String headerText2 = Constants.EMPTY_STRING;
        String remark = Constants.EMPTY_STRING;
        StringBuffer userName =  new StringBuffer();
        int rowNo = Constants.ONE;
        StringBuffer emailBuffer = new StringBuffer();
        List<String> toList = new ArrayList<String>();
        
        Locale locale = asnMaintenanceDomain.getLocale();
        List<AsnMaintenanceReturnDomain> asnList = asnMaintenanceDomain.getDoGroupAsnList();
        
        for (SpsMUserDomain user : userList) {
            if (!Strings.judgeBlank(user.getEmail()) && !toList.contains(user.getEmail())) {
                toList.add(user.getEmail());
            }
        }
        
        if(Constants.STR_CREATE.equals(mode)){
            subject = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_SUBJECT_ALLOW, null);
            headerText2 = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_ALLOW, null);
            remark = MessageUtil.getEmailLabelHandledException(locale,
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_REMARK_ALLOW, null);
        }else if(Constants.STR_CANCEL.equals(mode)){
            subject = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_SUBJECT_DISALLOW, null);
            headerText2 = MessageUtil.getEmailLabelHandledException(locale, 
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_DISALLOW, null);
            remark = MessageUtil.getEmailLabelHandledException(locale,
                SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_REMARK_DISALLOW, null);
        }
        
        String headerText1 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE1, null);
        String headerText3 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE3, null);
        String headerText4 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE4, null);
        
        String headerTable1 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE1, null);
        String headerTable2 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE2, null);
        
        String detailNormal = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE1, null);
        String detailAllowed = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE2, null);
        String detailTableEnd = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE3, null);
        String footer1 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_FOOTER_LINE1, null);
        String footer2 = MessageUtil.getEmailLabelHandledException(locale, 
            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_FOOTER_LINE2, null);
        
        //Prepare parameter
        AsnMaintenanceReturnDomain asnDetailDomain = asnList.get(Constants.ZERO);
        
        String lastModified = DateUtil.format(new Date(
            asnDetailDomain.getAsnDomain().getLastUpdateDatetime().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
        subject = StringUtil.appendsString(subject, asnDetailDomain.getAsnDomain().getAsnNo());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_ASNNO,
            asnMaintenanceDomain.getAsnNo());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SCD, 
            asnDetailDomain.getAsnDomain().getVendorCd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SPCD, 
            asnDetailDomain.getSPcd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_DCD, 
            asnDetailDomain.getDCd());
        headerText3 = headerText3.replace(SupplierPortalConstant.REPLACE_DPCD, 
            asnDetailDomain.getDPcd());
        headerText3 = headerText3.replace(SupplierPortalConstant.REPLACE_LAST_MODIFIED, 
            lastModified + Constants.SYMBOL_SPACE + asnMaintenanceDomain.getUtc());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_ACTUAL_ETD, 
            asnMaintenanceDomain.getActualEtdDate());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_PLAN_ETA, 
            asnMaintenanceDomain.getPlanEtaDate());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_NOOFPALLET, 
            asnMaintenanceDomain.getNoOfPallet());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_TRIPNO, 
            asnMaintenanceDomain.getTripNo());
        String newLine = null;
        String detailAllLine = null;
        
        emailBuffer.setLength(Constants.ZERO);
        String rowNumber = Constants.EMPTY_STRING;
        String rcvLane = Constants.EMPTY_STRING;
        String dpartNo = Constants.EMPTY_STRING;
        String spsDoNo = Constants.EMPTY_STRING;
        String revision = Constants.EMPTY_STRING;
        String shippingQty = Constants.EMPTY_STRING;
        String um = Constants.EMPTY_STRING;
        String qtyBox = Constants.EMPTY_STRING;
        String shippingBoxQty = Constants.EMPTY_STRING;
        String orderQty = Constants.EMPTY_STRING;
        StringBuffer changeReason = new StringBuffer();
        
        for(AsnMaintenanceReturnDomain asn : asnList){
            userName.setLength(Constants.ZERO);
            userName.append(asn.getUserDomain().getFirstName())
                .append(Constants.SYMBOL_SPACE)
                .append(StringUtil.nullToEmpty(asn.getUserDomain().getMiddleName()))
                .append(Constants.SYMBOL_SPACE)
                .append(asn.getUserDomain().getLastName());
            
            if(Constants.STR_CREATE.equals(mode) && Constants.STR_ONE.equals(
                allowReviseOperationMap.get(asn.getAsnDetailDomain().getRcvLane()))){
                newLine = detailAllowed;
            }else if(Constants.STR_CANCEL.equals(mode) && Constants.STR_TWO.equals(
                allowReviseOperationMap.get(asn.getAsnDetailDomain().getRcvLane()))){
                newLine = detailAllowed;
            }else{
                userName.setLength(Constants.ZERO);
                newLine = detailNormal;
            }
            
            rowNumber = Constants.EMPTY_STRING;
            rcvLane = Constants.EMPTY_STRING;
            dpartNo = Constants.EMPTY_STRING;
            spsDoNo = Constants.EMPTY_STRING;
            revision = Constants.EMPTY_STRING;
            shippingQty = Constants.EMPTY_STRING;
            orderQty = Constants.EMPTY_STRING;
            um = Constants.EMPTY_STRING;
            qtyBox = Constants.EMPTY_STRING;
            shippingBoxQty = Constants.EMPTY_STRING;
            changeReason.setLength(Constants.ZERO);
            
            rowNumber = String.valueOf(rowNo);
            rcvLane = asn.getAsnDetailDomain().getRcvLane();
            dpartNo = asn.getDPn();
            spsDoNo = asn.getAsnDetailDomain().getSpsDoNo();
            revision = asn.getAsnDetailDomain().getRevision();
            shippingQty = asn.getShippingQtyStr();
            orderQty = asn.getOrderQtyStr();
            um = asn.getAsnDetailDomain().getUnitOfMeasure();
            qtyBox = asn.getAsnDetailDomain().getQtyBox().toString();
            shippingBoxQty = asn.getAsnDetailDomain().getShippingBoxQty().toString();
            if(!StringUtil.checkNullOrEmpty(asn.getAsnDetailDomain().getChangeReasonCd())){
                changeReason.append(asn.getAsnDetailDomain().getChangeReasonCd())
                    .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_COLON)
                    .append(Constants.SYMBOL_SPACE).append(asn.getChangeReasonName());
            }
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_USERNAME,
                userName.toString());
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_ROWNO, rowNumber);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_RCVLANE, rcvLane);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DPARTNO, dpartNo);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSDONO, spsDoNo);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_REVISION, revision);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_CURRENT_QTY, orderQty);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SHIPPING_QTY, shippingQty);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_UM, um);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_QTYBOX, qtyBox);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SHIPPINGBOX_QTY,
                shippingBoxQty);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_CHANGE_REASON,
                changeReason.toString());
            emailBuffer.append(newLine);
            rowNo += Constants.ONE;
        }
        
        detailAllLine = emailBuffer.toString();
        String detail = null;
        emailBuffer.setLength(Constants.ZERO);
        emailBuffer
            .append(headerText1)
            .append(Constants.SYMBOL_NEWLINE)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText2)
            .append(Constants.SYMBOL_NEWLINE)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText3)
            .append(headerText4)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerTable1)
            .append(headerTable2)
            .append(detailAllLine)
            .append(detailTableEnd)
            .append(remark);
        emailBuffer.append(Constants.SYMBOL_NEWLINE)
            .append(footer1).append(Constants.SYMBOL_NEWLINE)
            .append(footer2).append(Constants.SYMBOL_NEWLINE);
        detail = emailBuffer.toString();
        
        SendEmailDomain sendEmail = new SendEmailDomain();
        sendEmail.setEmailFrom(ContextParams.getDefaultEmailFrom());
        sendEmail.setEmailTo(Constants.EMPTY_STRING);
        sendEmail.setToList(toList);
        sendEmail.setHeader(subject);
        sendEmail.setContents(detail);
        sendEmail.setEmailSmtp(ContextParams.getEmailSmtp());
        return sendEmail;
    }
    
    /**
     * Gets new ASN detail information list
     * 
     * @return the list
     * @param tempCigmaAsnMap the map
     * @param asnInfoList the list of asn information domain
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    private List<AsnMaintenanceReturnDomain> setNewAsnDetailInformationList(
        List<AsnInformationDomain> asnInfoList, Map<String, PseudoCigmaAsnDomain> tempCigmaAsnMap)
    {
        StringBuffer key = new StringBuffer();
        AsnInformationDomain asnInfo = null;
        AsnMaintenanceReturnDomain asn = null;
        PseudoCigmaAsnDomain cigmaAsnDomain = null;
        List<AsnMaintenanceReturnDomain> asnList = new ArrayList<AsnMaintenanceReturnDomain>();
        Iterator itr = asnInfoList.iterator();
        while(itr.hasNext()){
            asnInfo = (AsnInformationDomain)itr.next();
            asn = new AsnMaintenanceReturnDomain();
            asn.setChangeReasonCdBackUp(Constants.EMPTY_STRING);
            asn.getAsnDomain().setAsnNo(asnInfo.getSpsTAsnDomain().getAsnNo());
            asn.getAsnDomain().setAsnStatus(asnInfo.getSpsTAsnDomain().getAsnStatus());
            asn.getAsnDomain().setVendorCd(asnInfo.getSpsTAsnDomain().getVendorCd());
            asn.getAsnDomain().setSCd(asnInfo.getSpsTAsnDomain().getSCd());
            asn.getAsnDomain().setSPcd(asnInfo.getSpsTAsnDomain().getSPcd());
            asn.getAsnDomain().setDCd(asnInfo.getSpsTAsnDomain().getDCd());
            asn.getAsnDomain().setDPcd(asnInfo.getSpsTAsnDomain().getDPcd());
            asn.getAsnDomain().setActualEtd(asnInfo.getSpsTAsnDomain().getActualEtd());
            asn.getAsnDomain().setPlanEta(asnInfo.getSpsTAsnDomain().getPlanEta());
            asn.getAsnDomain().setTripNo(asnInfo.getSpsTAsnDomain().getTripNo());
            asn.getAsnDomain().setNumberOfPallet(asnInfo.getSpsTAsnDomain().getNumberOfPallet());
            asn.getAsnDomain().setCreatedInvoiceFlag(asnInfo.getSpsTAsnDomain()
                .getCreatedInvoiceFlag());
            asn.getAsnDomain().setPdfFileId(asnInfo.getSpsTAsnDomain().getPdfFileId());
            asn.setSCd(asnInfo.getSpsTAsnDomain().getSCd());
            asn.setSPcd(asnInfo.getSpsTAsnDomain().getSPcd());
            asn.setDCd(asnInfo.getSpsTAsnDomain().getDCd());
            asn.setDPcd(asnInfo.getSpsTAsnDomain().getDPcd());
            asn.getGroupDoDomain().setLastUpdateDatetime(asnInfo.getDoLastUpdateDatetime());
            asn.getGroupDoDomain().setShipDatetime(asnInfo.getShipDatetime());
            asn.getGroupDoDomain().setDeliveryDatetime(asnInfo.getDeliveryDatetime());
            asn.getGroupDoDomain().setOrderMethod(asnInfo.getOrderMethod());
            //asn.getGroupDoDomain().setTm();
            asn.getAsnDomain().setCreateDatetime(asnInfo.getSpsTAsnDomain().getCreateDatetime());
            asn.getAsnDomain().setLastUpdateDatetime(asnInfo.getSpsTAsnDomain()
                .getLastUpdateDatetime());
            asn.getAsnDetailDomain().setSPn(asnInfo.getSpsTAsnDetailDomain().getSPn());
            asn.getAsnDetailDomain().setDPn(asnInfo.getSpsTAsnDetailDomain().getDPn());
            asn.setSPn(asnInfo.getSpsTAsnDetailDomain().getSPn());
            asn.setDPn(asnInfo.getSpsTAsnDetailDomain().getDPn());
            asn.getAsnDetailDomain().setDoId(asnInfo.getSpsTAsnDetailDomain().getDoId());
            asn.getAsnDetailDomain().setRcvLane(asnInfo.getSpsTAsnDetailDomain().getRcvLane());
            asn.getAsnDetailDomain().setQtyBox(asnInfo.getSpsTAsnDetailDomain().getQtyBox());
            asn.getAsnDetailDomain().setUnitOfMeasure(asnInfo.getSpsTAsnDetailDomain()
                .getUnitOfMeasure());
            asn.getAsnDetailDomain().setLastUpdateDatetime(asnInfo.getSpsTAsnDetailDomain()
                .getLastUpdateDatetime());
            asn.getAsnDetailDomain().setSpsDoNo(asnInfo.getSpsTAsnDetailDomain().getSpsDoNo());
            asn.getAsnDetailDomain().setRevision(asnInfo.getSpsTAsnDetailDomain().getRevision());
            asn.getAsnDetailDomain().setCigmaDoNo(asnInfo.getSpsTAsnDetailDomain().getCigmaDoNo());
            asn.getGroupDoDetailDomain().setChgCigmaDoNo(asnInfo.getChgCigmaDoNo());
            asn.getGroupDoDetailDomain().setCurrentOrderQty(asnInfo.getCurrentOrderQty());
            asn.getGroupDoDetailDomain().setLastUpdateDatetime(asnInfo.getPnDoLastUpdateDatetime());
            asn.getAsnDetailDomain().setShippingQty(asnInfo.getSpsTAsnDetailDomain()
                .getShippingQty());
            asn.getAsnDetailDomain().setShippingBoxQty(asnInfo.getSpsTAsnDetailDomain()
                .getShippingBoxQty());
            asn.getAsnDetailDomain().setChangeReasonCd(asnInfo.getSpsTAsnDetailDomain()
                .getChangeReasonCd());
            asn.getAsnDetailDomain().setAllowReviseFlag(asnInfo.getSpsTAsnDetailDomain()
                .getAllowReviseFlag());
            asn.setAllowReviseFlagBackUp(asnInfo.getSpsTAsnDetailDomain().getAllowReviseFlag());
            asn.setChangeReasonName(asnInfo.getChangeReasonName());
            asn.setTotalShippedQty(asnInfo.getTotalShippedQty());
            asn.setRowCount(asnInfo.getRowCount());
            asn.setRecordGroup(asnInfo.getRecordGroup());
            asn.setRevisingFlag(Constants.STR_ZERO);
            asn.setReviseCompleteFlag(Constants.STR_ZERO);
            asn.setUtc(asnInfo.getSpsMCompanyDensoDomain().getUtc().trim());
            asn.setInvoiceStatus(StringUtil.nullToEmpty(asnInfo.getSpsMMiscDomain().getMiscCd()));
            asn.getUserDomain().setFirstName(asnInfo.getSpsMUserDomain().getFirstName());
            asn.getUserDomain().setMiddleName(asnInfo.getSpsMUserDomain().getMiddleName());
            asn.getUserDomain().setLastName(asnInfo.getSpsMUserDomain().getLastName());
            if(null == asn.getGroupDoDomain().getTruckSeq()){
                asn.getGroupDoDomain().setTruckSeq(new BigDecimal(Constants.ZERO));
            }
            
            //set cigma asn domain
            if(null != tempCigmaAsnMap && !tempCigmaAsnMap.isEmpty()){
                key = new StringBuffer();
                key.append(asn.getAsnDomain().getAsnNo().trim())
                    .append(Constants.SYMBOL_UNDER_SCORE)
                    .append(asn.getAsnDetailDomain().getDPn()).append(Constants.SYMBOL_UNDER_SCORE)
                    .append(asn.getAsnDetailDomain().getSpsDoNo());
                if(tempCigmaAsnMap.containsKey(key.toString())){
                    cigmaAsnDomain = (PseudoCigmaAsnDomain)tempCigmaAsnMap.get(key.toString());
                }
                
                String asnStatus = Constants.EMPTY_STRING;
                String pnReceivingStatus = Constants.EMPTY_STRING;
                BigDecimal asnReceivedQty = new BigDecimal(cigmaAsnDomain.getPseudoReceivedQty());
                if(Constants.ASN_STATUS_CCL.equals(asn.getAsnDomain().getAsnStatus())){
                    asnStatus = Constants.ASN_STATUS_CCL;
                }else{
                    if(!StringUtil.checkNullOrEmpty(cigmaAsnDomain.getPseudoAsnStatus())){
                        if(Constants.ASN_STATUS_N.equals(cigmaAsnDomain.getPseudoAsnStatus())){
                            asnStatus = Constants.ASN_STATUS_ISS;
                        }else if(Constants.ASN_STATUS_D.equals(
                            cigmaAsnDomain.getPseudoAsnStatus())){
                            asnStatus = Constants.ASN_STATUS_CCL;
                        }else if(Constants.ASN_STATUS_P.equals(
                            cigmaAsnDomain.getPseudoAsnStatus())){
                            asnStatus = Constants.ASN_STATUS_PTR;
                        }else{
                            asnStatus = Constants.ASN_STATUS_RCP;
                        }
                    }else{
                        asnStatus = asn.getAsnDomain().getAsnStatus();
                    }
                }
                
                if(Constants.ASN_STATUS_CCL.equals(asn.getAsnDomain().getAsnStatus())){
                    pnReceivingStatus = Constants.ASN_STATUS_CCL;
                }else{
                    if(!StringUtil.checkNullOrEmpty(cigmaAsnDomain.getPseudoPnReceivingStatus())){
                        if(Constants.ASN_STATUS_N.equals(
                            cigmaAsnDomain.getPseudoPnReceivingStatus())){
                            pnReceivingStatus = Constants.ASN_STATUS_ISS;
                        }else if(Constants.ASN_STATUS_D.equals(
                            cigmaAsnDomain.getPseudoPnReceivingStatus())){
                            pnReceivingStatus = Constants.ASN_STATUS_CCL;
                        }else if(Constants.ASN_STATUS_P.equals(
                            cigmaAsnDomain.getPseudoPnReceivingStatus())){
                            pnReceivingStatus = Constants.ASN_STATUS_PTR;
                        }else{
                            pnReceivingStatus = Constants.ASN_STATUS_RCP;
                        }
                    }else{
                        pnReceivingStatus = asn.getAsnDomain().getAsnStatus();
                    }
                }
                
                asn.setAsnStatus(asnStatus);
                asn.setAsnReceivedQty(asnReceivedQty);
                asn.setAsnPnReceivingStatus(pnReceivingStatus);
                asn.setCigmaAsnDomain(cigmaAsnDomain);
            }
            asnList.add(asn);
        }
        return asnList;
    }
    
    /**
     * Do set asn list to show on screen
     * 
     * @param asnDetailList the list
     * @param asnMaintenanceDomain the asn maintenance domain
     * @throws ApplicationException 
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    private void doSetAsnListToShow(List<AsnMaintenanceReturnDomain> asnDetailList, 
        AsnMaintenanceDomain asnMaintenanceDomain) throws ApplicationException
    {
        boolean firstFlag = true;
        int rowCountByLane = Constants.ZERO;
        StringBuffer utcStr = new StringBuffer();
        String displayDecimalPointFlag = Constants.EMPTY_STRING;
        String chgReasonCd = Constants.EMPTY_STRING;
        AsnMaintenanceReturnDomain doGroupAsn = null;
        AsnMaintenanceReturnDomain tempDoGroupAsn = null;
        AsnMaintenanceReturnDomain asnDetailTmp = null;
        BigDecimal shippingBoxQty = new BigDecimal(Constants.ZERO);
        BigDecimal shippingQtyByPart = new BigDecimal(Constants.ZERO);
        BigDecimal asnReceivedQtyByPart = new BigDecimal(Constants.ZERO);
        BigDecimal totalRemainingQtyByPart = new BigDecimal(Constants.ZERO);
        // [IN039] If ASN already saved, get shipping box qty from Database
        BigDecimal shippingBoxByPart = BigDecimal.ZERO;
//        List<AsnMaintenanceReturnDomain> asnDetailTmpList
//            = new ArrayList<AsnMaintenanceReturnDomain>();
        List<String> rcvLaneGroup = new ArrayList<String>();
        List<String> allowReviseFlagList = new ArrayList<String>();
        SpsMUnitOfMeasureDomain unitOfMeasureResult = null;
        SpsMUnitOfMeasureCriteriaDomain unitOfMeasureCriteria = null;
        
        Iterator itr = asnDetailList.iterator();
        while(itr.hasNext()){
            doGroupAsn = (AsnMaintenanceReturnDomain)itr.next();
            if(firstFlag){
                utcStr.append(Constants.SYMBOL_OPEN_BRACKET).append(Constants.WORD_UTC)
                    .append(doGroupAsn.getUtc().trim())
                    .append(Constants.SYMBOL_CLOSE_BRACKET);
                asnMaintenanceDomain.setUtc(utcStr.toString());
                firstFlag = false;
            }
            doGroupAsn.setRecordGroupByLane(Constants.EMPTY_STRING);
            if(rcvLaneGroup.contains(doGroupAsn.getAsnDetailDomain().getRcvLane())){
                rowCountByLane += Constants.ONE;
                asnDetailTmp.setRowCountByRcvLane(String.valueOf(rowCountByLane));
            }else{
                rowCountByLane = Constants.ONE;
                asnDetailTmp = doGroupAsn;
                asnDetailTmp.setRecordGroupByLane(Constants.STR_ONE);
                asnDetailTmp.setRowCountByRcvLane(String.valueOf(rowCountByLane));
                rcvLaneGroup.add(doGroupAsn.getAsnDetailDomain().getRcvLane());
                
                if(Constants.STR_ONE.equals(doGroupAsn.getAsnDetailDomain().getAllowReviseFlag())){
                    allowReviseFlagList.add(doGroupAsn.getAsnDetailDomain().getRcvLane());
                }
            }
            
            doGroupAsn.setAsnReceivedQtyByPart(new BigDecimal(Constants.ZERO));
            doGroupAsn.setTotalRemainingQty(doGroupAsn.getGroupDoDetailDomain()
                .getCurrentOrderQty().subtract(doGroupAsn.getTotalShippedQty()));
            
            // [IN019] If total remain is less than zero set to zero
            if (Constants.ZERO < BigDecimal.ZERO.compareTo(doGroupAsn.getTotalRemainingQty())) {
                doGroupAsn.setTotalRemainingQty(BigDecimal.ZERO);
            }
            
            doGroupAsn.setQtyBoxStr(StringUtil.toNumberFormat(doGroupAsn.
                getAsnDetailDomain().getQtyBox().toString()));
            doGroupAsn.setOrderQtyStr(StringUtil.toNumberFormat(doGroupAsn.
                getGroupDoDetailDomain().getCurrentOrderQty().toString()));
            
            unitOfMeasureCriteria = new SpsMUnitOfMeasureCriteriaDomain();
            unitOfMeasureCriteria.setUnitOfMeasureId(
                doGroupAsn.getAsnDetailDomain().getUnitOfMeasure());
            unitOfMeasureResult = spsMUnitOfMeasureService.searchByKey(unitOfMeasureCriteria);
            if(null == unitOfMeasureResult){
                MessageUtil.throwsApplicationMessageWithLabel(asnMaintenanceDomain.getLocale(),
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_UNIT_OF_MEASURE);
            }
            
            String shippingQtyStr = Constants.EMPTY_STRING;
            if(Constants.DISPLAY_DECIMAL_FLAG_NO.equals(
                unitOfMeasureResult.getDisplayDecimalPointFlg()))
            {
                doGroupAsn.setTotalRemainingQtyStr(StringUtil.toNumberFormat(doGroupAsn.
                    getTotalRemainingQty().toString()));
                doGroupAsn.setTotalShippedQtyStr(StringUtil.toNumberFormat(doGroupAsn.
                    getTotalShippedQty().toString()));
                shippingQtyStr = StringUtil.toNumberFormat(
                    doGroupAsn.getAsnDetailDomain().getShippingQty().toString());
                doGroupAsn.setAsnReceivedQtyStr(StringUtil.toNumberFormat(
                    doGroupAsn.getAsnReceivedQty().toString()));
            }else{
                doGroupAsn.setTotalRemainingQtyStr(StringUtil.toCurrencyFormat(doGroupAsn.
                    getTotalRemainingQty().toString()));
                doGroupAsn.setTotalShippedQtyStr(StringUtil.toCurrencyFormat(doGroupAsn.
                    getTotalShippedQty().toString()));
                shippingQtyStr = StringUtil.toCurrencyFormat(
                    doGroupAsn.getAsnDetailDomain().getShippingQty().toString());
                doGroupAsn.setAsnReceivedQtyStr(StringUtil.toCurrencyFormat(
                    doGroupAsn.getAsnReceivedQty().toString()));
            }
            
            // [IN039] : allow user to input Shipping Box Qty for unit KG or MT
            doGroupAsn.setEditShippingBoxQtyFlg(unitOfMeasureResult.getEditShippingBoxFlg());
            
            //set shipping qty with display decimal
            doGroupAsn.setShippingQtyStr(shippingQtyStr);
            doGroupAsn.getAsnDetailDomain().setShippingQty(new BigDecimal(
                shippingQtyStr.replaceAll(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING)));
            
            //calculate shipping box qty
            // Start : [IN039] If ASN already saved, get shipping box qty from Database
            //shippingBoxQty = doGroupAsn.getAsnDetailDomain().getShippingQty()
            //    .divide(doGroupAsn.getAsnDetailDomain().getQtyBox(), Constants.ZERO,
            //        RoundingMode.CEILING);
            boolean savedAsn = false;
            if (null == doGroupAsn.getAsnDetailDomain().getShippingBoxQty()) {
                shippingBoxQty = doGroupAsn.getAsnDetailDomain().getShippingQty()
                    .divide(doGroupAsn.getAsnDetailDomain().getQtyBox(), Constants.ZERO,
                        RoundingMode.CEILING);
            } else {
                shippingBoxQty = doGroupAsn.getAsnDetailDomain().getShippingBoxQty();
                savedAsn = true;
            }
            // End : [IN039] If ASN already saved, get shipping box qty from Database
            doGroupAsn.getAsnDetailDomain().setShippingBoxQty(shippingBoxQty);
            
            if(null == tempDoGroupAsn){
//                asnDetailTmpList.add(doGroupAsn);
                tempDoGroupAsn = doGroupAsn;
                chgReasonCd = StringUtil.nullToEmpty(
                    doGroupAsn.getAsnDetailDomain().getChangeReasonCd());
                asnReceivedQtyByPart = tempDoGroupAsn.getAsnReceivedQty();
                totalRemainingQtyByPart = tempDoGroupAsn.getTotalRemainingQty();
                shippingQtyByPart = tempDoGroupAsn.getAsnDetailDomain().getShippingQty();
                displayDecimalPointFlag = unitOfMeasureResult.getDisplayDecimalPointFlg();
                // [IN039] If ASN already saved, get shipping box qty from Database
                if (savedAsn) {
                    shippingBoxByPart = doGroupAsn.getAsnDetailDomain().getShippingBoxQty();
                }
            }else if(Constants.STR_ONE.equals(doGroupAsn.getRecordGroup())){
                tempDoGroupAsn.setChangeReasonCd(chgReasonCd);
                // [IN039] If ASN already saved, get shipping box qty from Database
                //this.setQtyForDisplayByPart(shippingQtyByPart, totalRemainingQtyByPart,
                //    asnReceivedQtyByPart, displayDecimalPointFlag, tempDoGroupAsn);
                this.setQtyForDisplayByPart(shippingQtyByPart, totalRemainingQtyByPart,
                    asnReceivedQtyByPart, displayDecimalPointFlag, tempDoGroupAsn,
                    shippingBoxByPart);
                
//                asnDetailTmpList.clear();
//                asnDetailTmpList.add(doGroupAsn);
                chgReasonCd = StringUtil.nullToEmpty(
                    doGroupAsn.getAsnDetailDomain().getChangeReasonCd());
                totalRemainingQtyByPart = doGroupAsn.getTotalRemainingQty();
                asnReceivedQtyByPart = doGroupAsn.getAsnReceivedQty();
                shippingQtyByPart = doGroupAsn.getAsnDetailDomain().getShippingQty();
                displayDecimalPointFlag = unitOfMeasureResult.getDisplayDecimalPointFlg();
                // [IN039] If ASN already saved, get shipping box qty from Database
                if (savedAsn) {
                    shippingBoxByPart = doGroupAsn.getAsnDetailDomain().getShippingBoxQty();
                }
                tempDoGroupAsn = doGroupAsn;
            }else{
//                asnDetailTmpList.add(doGroupAsn);
                chgReasonCd = StringUtil.nullToEmpty(
                    doGroupAsn.getAsnDetailDomain().getChangeReasonCd());
                asnReceivedQtyByPart = asnReceivedQtyByPart.add(doGroupAsn.getAsnReceivedQty());
                totalRemainingQtyByPart = totalRemainingQtyByPart.add(
                    doGroupAsn.getTotalRemainingQty());
                shippingQtyByPart = shippingQtyByPart.add(
                    doGroupAsn.getAsnDetailDomain().getShippingQty());
                // [IN039] If ASN already saved, get shipping box qty from Database
                if (savedAsn) {
                    shippingBoxByPart = shippingBoxByPart.add(
                        doGroupAsn.getAsnDetailDomain().getShippingBoxQty());
                }
            }
        }
        tempDoGroupAsn.setChangeReasonCd(chgReasonCd);
        // [IN039] If ASN already saved, get shipping box qty from Database
        //this.setQtyForDisplayByPart(shippingQtyByPart, totalRemainingQtyByPart,
        //    asnReceivedQtyByPart, displayDecimalPointFlag, tempDoGroupAsn);
        this.setQtyForDisplayByPart(shippingQtyByPart, totalRemainingQtyByPart,
            asnReceivedQtyByPart, displayDecimalPointFlag, tempDoGroupAsn, shippingBoxByPart);
        
        //Check active revise shipping qty button
        if(allowReviseFlagList.isEmpty()){
            asnMaintenanceDomain.setAllowReviseQtyFlag(Constants.STR_ZERO);
        }else{
            asnMaintenanceDomain.setAllowReviseQtyFlag(Constants.STR_ONE);
        }
    }
    
    /**
     * Method for setting QTY by part no.
     * @param shippingQtyByPart the shippingQty by part
     * @param remainingQtyByPart the total remainingQty by part
     * @param asnReceivedQtyByPart the total asnReceivedQty by part
     * @param displayDecimalPointFlag the display decimal flag
     * @param tempDoGroupAsn the asnMaintenanceReturn domain
     * @param shippingBoxByPart the total shipping box by part
     * */
    private void setQtyForDisplayByPart(BigDecimal shippingQtyByPart,
        BigDecimal remainingQtyByPart, BigDecimal asnReceivedQtyByPart,
        String displayDecimalPointFlag, AsnMaintenanceReturnDomain tempDoGroupAsn,
        BigDecimal shippingBoxByPart)
    {
        // [IN039] If ASN already saved, get shipping box qty from Database
        //BigDecimal shippingBoxQtyByPart = shippingQtyByPart.divide(
        //    tempDoGroupAsn.getAsnDetailDomain().getQtyBox(), Constants.ZERO, RoundingMode.CEILING);
        BigDecimal shippingBoxQtyByPart = null;
        if (Constants.ZERO == BigDecimal.ZERO.compareTo(shippingBoxByPart)) {
            shippingBoxQtyByPart = shippingQtyByPart.divide(
                tempDoGroupAsn.getAsnDetailDomain().getQtyBox()
                , Constants.ZERO, RoundingMode.CEILING);
        } else {
            shippingBoxQtyByPart = shippingBoxByPart;
        }
        
        tempDoGroupAsn.setAsnReceivedQtyByPart(asnReceivedQtyByPart);
        tempDoGroupAsn.setTotalRemainingQtyByPart(remainingQtyByPart);
        tempDoGroupAsn.setShippingBoxQtyByPart(shippingBoxQtyByPart);
        tempDoGroupAsn.setShippingBoxQtyByPartStr(StringUtil.toNumberFormat(
            tempDoGroupAsn.getShippingBoxQtyByPart().toString()));

        // [IN039] Initial input for shipping box qty
        if (Strings.judgeBlank(tempDoGroupAsn.getShippingBoxQtyByPartInput())) {
            tempDoGroupAsn.setShippingBoxQtyByPartInput(
                tempDoGroupAsn.getShippingBoxQtyByPartStr());
        }
        
        tempDoGroupAsn.setShippingQtyByPart(shippingQtyByPart);
        if(Constants.DISPLAY_DECIMAL_FLAG_NO.equals(displayDecimalPointFlag)){
            tempDoGroupAsn.setShippingQtyByPartStr(StringUtil.toNumberFormat(
                shippingQtyByPart.toString()));
        }else{
            tempDoGroupAsn.setShippingQtyByPartStr(StringUtil.toCurrencyFormat(
                shippingQtyByPart.toString()));
        }
    }



    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService#generateAndUpdatePdfFile()
     */
    public FileManagementDomain generateAndUpdatePdfFile(Locale locale, String asnNo, String dCd
        , Timestamp updateDateTime, String dscId) throws ApplicationException, Exception
    {
        updateDateTime = commonService.searchSysDate();
        SpsTAsnDomain reportParam = new SpsTAsnDomain();
        reportParam.setLocale(locale);
        reportParam.setAsnNo(asnNo.trim());
        reportParam.setDCd(dCd);
        InputStream reportData = null;
        FileManagementDomain resultDomain = new FileManagementDomain();
        try{
            reportData = this.asnService.searchAsnReport(reportParam);
            
            StringBuffer asnFileName = new StringBuffer();
            /* Change the filename of ASN report.
             * asnFileName.append(SupplierPortalConstant.ADVANCE_SHIP_NOTICE);
             */
            String asnTempleteFilename = getAsnTempleteFilename(dCd); 
            asnFileName.append(asnTempleteFilename);
            /* End to change the filename of ASN report.*/
            asnFileName.append(asnNo);
            asnFileName.append(Constants.SYMBOL_DOT);
            asnFileName.append(Constants.REPORT_PDF_FORMAT);
//            
//            String fileId = this.fileManagementService.createFileUpload(reportData,
//                asnFileName.toString(), Constants.SAVE_LIMIT_TERM, dscId);
//            reportParam.setAsnNo(asnNo);
//            reportParam.setDCd(dCd);
//            reportParam.setPdfFileId(fileId);
//            int rowUpdate = this.spsTAsnService.update(reportParam);
//            
//            if(Constants.ZERO == rowUpdate){
//                MessageUtil.throwsApplicationMessage(locale, 
//                    SupplierPortalConstant.ERROR_CD_SP_E6_0038,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
//            }
            resultDomain = new FileManagementDomain();
            resultDomain.setFileData(reportData);
            resultDomain.setFileName(asnFileName.toString());
        }catch(Exception e){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0014);
        }
        return resultDomain;
    }
    
    /**
     * Generate Change Reason Map for ASN short ship list.
     * @param locale locale to get report properties
     * @return map the chg reason code map
     * @throws ApplicationException an ApplicationException
     * */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    private Map generateChangeReasonMap(Locale locale) throws ApplicationException{
        Map chgReasonMap = new HashMap();
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_CHANGE_REASON_CB);
        List<MiscellaneousDomain> chgReasonList = miscService.searchMisc(miscDomain);
        if (null == chgReasonList || Constants.ZERO == chgReasonList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_CHANGE_REASON);
        }
        for(MiscellaneousDomain chgReasonItem : chgReasonList){
            chgReasonMap.put(chgReasonItem.getMiscCode(), chgReasonItem.getMiscValue());
        }
        return chgReasonMap;
    }
    
    /**
     * Generate Unit of Measure map for display decimal point format.
     * @param locale locale to get report properties
     * @return map the chg reason code map
     * @throws ApplicationException an ApplicationException
     * */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    private Map generateUnitOfMeasureMap(Locale locale)  throws ApplicationException{
        Map unitOfMeasureMap = new HashMap();
        SpsMUnitOfMeasureCriteriaDomain criteria = new SpsMUnitOfMeasureCriteriaDomain();
        criteria.setUnitOfMeasureId(Constants.EMPTY_STRING);
        List<SpsMUnitOfMeasureDomain> umList
            = spsMUnitOfMeasureService.searchByCondition(criteria);
        
        if(null == umList || Constants.ZERO == umList.size()){
            MessageUtil.getApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_UNIT_OF_MEASURE);
        }
        
        for(SpsMUnitOfMeasureDomain um : umList){
            unitOfMeasureMap.put(um.getUnitOfMeasureId(), um.getDisplayDecimalPointFlg());
        }
        return unitOfMeasureMap;
    }
    /**
     * Generate the templete filename of ASN from SPS_M_COMPANY_DENSO.
     * @param dCd to get the templete filename of ASN
     * @return asnTempleteFilename
     * @throws ApplicationException an ApplicationException
     * @throws Exception an Exception
     * */
    public String getAsnTempleteFilename(String dCd)
        throws ApplicationException, Exception {
        String asnTempleteFilename = null;
        SpsMCompanyDensoDomain companyDensoDomain = new SpsMCompanyDensoDomain();
        List<String> dCdList = new ArrayList<String>();
        
        try{
            dCdList.add(dCd);
            companyDensoDomain.setDCd(StringUtil.convertListToVarcharCommaSeperate(dCdList));
            List<SpsMCompanyDensoDomain> companyDensoDomainList = this.companyDensoService.searchCompanyDensoByCodeList(companyDensoDomain);
            asnTempleteFilename = companyDensoDomainList.get(Constants.ZERO).getAsnTempleteFilename();
        } finally {
            if(StringUtil.checkNullOrEmpty(asnTempleteFilename)){
                return SupplierPortalConstant.ADVANCE_SHIP_NOTICE;
            }
        }
        return asnTempleteFilename;
    }
}