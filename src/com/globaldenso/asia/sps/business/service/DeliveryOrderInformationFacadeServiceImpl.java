/*
 * ModifyDate Development company       Describe 
 * 2014/07/29 CSI Chatchai              Create
 * 2015/09/17 CSI Akat                  FIX wrong dateformat
 * 2015/12/09 CSI Akat                  [IN042]
 * 2017/08/22 Netband U.Rungsiwut       Modify
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change D/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>
 * Supplier and DENSO can inquire Delivery Order data (D/O), be able to download
 * them by CSV. User can also download D/O report in PDF format.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class DeliveryOrderInformationFacadeServiceImpl implements
    DeliveryOrderInformationFacadeService {

    /**
     * <p>
     * Call Service DensoSupplierRelationService.
     * </p>
     */
    private DensoSupplierRelationService densoSupplierRelationService;
    /**
     * <p>
     * Call Service CompanySupplierService.
     * </p>
     */
    private CompanySupplierService companySupplierService;

    /**
     * <p>
     * Call Service CompanyDensoService.
     * </p>
     */
    private CompanyDensoService densoCompanyService;

    /**
     * <p>
     * Call Service MiscService.
     * </p>
     */
    private MiscellaneousService miscService;

    /**
     * <p>
     * Call Service DeliveryOrderService.
     * </p>
     */
    private DeliveryOrderService deliveryOrderService;

    /**
     * <p>
     * Call Service RecordLimitService.
     * </p>
     */
    private RecordLimitService recordLimitService;

    /**
     * <p>
     * Call Service PlantSupplierService.
     * </p>
     */
    private PlantSupplierService plantSupplierService;

    /**
     * <p>
     * Call Service PlantDensoService.
     * </p>
     */
    private PlantDensoService plantDensoService;

    /**
     * <p>
     * Call Service FileManagementService.
     * </p>
     */
    private FileManagementService fileManagementService;

    /**
     * <p>
     * Call Service CommonService.
     * </p>
     */
    private CommonService commonService;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * <p>
     * The Constructor.
     * </p>
     */
    public DeliveryOrderInformationFacadeServiceImpl() {
        super();
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationService.
     * </p>
     * 
     * @param densoSupplierRelationService Set for densoSupplierRelationService
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * <p>
     * Setter method for companySupplierService.
     * </p>
     * 
     * @param companySupplierService Set for companySupplierService
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }

    /**
     * <p>
     * Setter method for densoCompanyService.
     * </p>
     * 
     * @param densoCompanyService Set for densoCompanyService
     */
    public void setDensoCompanyService(CompanyDensoService densoCompanyService) {
        this.densoCompanyService = densoCompanyService;
    }

    /**
     * <p>
     * Setter method for miscService.
     * </p>
     * 
     * @param miscService Set for miscService
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }

    /**
     * <p>
     * Setter method for deliveryOrderService.
     * </p>
     * 
     * @param deliveryOrderService Set for deliveryOrderService
     */
    public void setDeliveryOrderService(
        DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }

    /**
     * <p>
     * Setter method for recordLimitService.
     * </p>
     * 
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * <p>
     * Setter method for plantSupplierService.
     * </p>
     * 
     * @param plantSupplierService Set for plantSupplierService
     */
    public void setPlantSupplierService(
        PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>
     * Setter method for plantDensoService.
     * </p>
     * 
     * @param plantDensoService Set for plantDensoService
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }

    /**
     * <p>
     * Setter method for fileManagementService.
     * </p>
     * 
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(
        FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * <p>
     * Setter method for commonService.
     * </p>
     * 
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#initial(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public DeliveryOrderInformationDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException {

        DeliveryOrderInformationDomain deliveryOrderInformationDomain = 
            new DeliveryOrderInformationDomain();
        Locale locale = dataScopeControlDomain.getLocale();

        dataScopeControlDomain
            .setDensoSupplierRelationDomainList(densoSupplierRelationService
                .searchDensoSupplierRelation(dataScopeControlDomain));

        /** Get relation between DENSO and Supplier. */
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            this.densoSupplierRelationService
            .searchDensoSupplierRelation(dataScopeControlDomain);
        if (null == densoSupplierRelationList
            || densoSupplierRelationList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                Constants.EMPTY_STRING,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
        deliveryOrderInformationDomain
            .setDensoSupplierRelationList(densoSupplierRelationList);

        /** Get supplier company information from SupplierCompanyService. */
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<CompanySupplierDomain> companySupplierDomain = null;
        companySupplierDomain = companySupplierService
            .searchCompanySupplier(plantSupplierWithScope);
        if (null == companySupplierDomain
            || Constants.ZERO == companySupplierDomain.size()) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE, locale)});
        }

        /** Get list of supplier plant information from SupplierPlantService. */
        List<PlantSupplierDomain> plantSupplierList = new ArrayList<PlantSupplierDomain>();
        plantSupplierList = plantSupplierService.searchPlantSupplier(
            plantSupplierWithScope);
        if (null == plantSupplierList || plantSupplierList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        /** Get DENSO company information from DensoCompanyService. */
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoDomain = null;
        companyDensoDomain = densoCompanyService.searchCompanyDenso(plantDensoWithScope);
        if (null == companyDensoDomain
            || Constants.ZERO == companyDensoDomain.size()) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }
        
        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoList = new ArrayList<PlantDensoDomain>();
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScope);
        if (null == plantDensoList || plantDensoList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }

        /** Get information for Revision Combo box from MiscellaneousService. */
        List<MiscellaneousDomain> miscRevision = new ArrayList<MiscellaneousDomain>();
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_REVISION_CB);
        miscRevision = miscService.searchMisc(miscDomain);

        StringBuffer addString = null;
        if (null == miscRevision || miscRevision.isEmpty()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_REVISION, locale)});
        }

        /**
         * Get information for Shipment Status Combo box from
         * MiscellaneousService.
         */
        List<MiscellaneousDomain> miscShipmentStatusCb = new ArrayList<MiscellaneousDomain>();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SHIP_STS_CB);
        miscShipmentStatusCb = miscService.searchMisc(miscDomain);

        if (Constants.ZERO < miscShipmentStatusCb.size()) {
            for (MiscellaneousDomain shipmentStatus : miscShipmentStatusCb) {
                addString = new StringBuffer();
                addString.append(shipmentStatus.getMiscCode());
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(Constants.SYMBOL_COLON);
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(shipmentStatus.getMiscValue());
                shipmentStatus.setMiscValue(addString.toString());
            }
        } else {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SHIPMENT_STATUS, locale)});
        }

        /**
         * Get information for Transport Mode Combo box from
         * MiscellaneousService.
         */
        List<MiscellaneousDomain> miscTransportModeCb = new ArrayList<MiscellaneousDomain>();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_TRANS_CB);
        miscTransportModeCb = miscService.searchMisc(miscDomain);

        if (Constants.ZERO < miscTransportModeCb.size()) {
            for (MiscellaneousDomain transportMode : miscTransportModeCb) {
                addString = new StringBuffer();
                addString.append(transportMode.getMiscCode());
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(Constants.SYMBOL_COLON);
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(transportMode.getMiscValue());
                transportMode.setMiscValue(addString.toString());
            }
        } else {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_TRANSPORTATION_MODE, locale)});
        }

        deliveryOrderInformationDomain.setCompanySupplierList(companySupplierDomain);
        deliveryOrderInformationDomain.setPlantSupplierList(plantSupplierList);
        deliveryOrderInformationDomain.setCompanyDensoList(companyDensoDomain);
        deliveryOrderInformationDomain.setPlantDensoList(plantDensoList);
        deliveryOrderInformationDomain.setMiscRevision(miscRevision);
        deliveryOrderInformationDomain.setMiscShipmentStatusCb(miscShipmentStatusCb);
        deliveryOrderInformationDomain.setTransportModeCb(miscTransportModeCb);

        /** Return DeliveryOrderInformationDomain to Action with return value. */
        return deliveryOrderInformationDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchDeliveryOrderInformation(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public DeliveryOrderInformationResultDomain searchDeliveryOrderInformation(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException {

        DeliveryOrderInformationResultDomain deliveryOrderInformationResultDomain = 
            new DeliveryOrderInformationResultDomain();
        List<ApplicationMessageDomain> errorMessageList = null;
        Locale locale = deliveryOrderInformationDomain.getLocale();
        Integer recordCount = Constants.ZERO;
        Integer recordCountDisplay = Constants.ZERO;

        /** Validate input parameter. */

        errorMessageList = this.validateCriteria(deliveryOrderInformationDomain);
        
        if (Constants.ZERO < errorMessageList.size()) {
            deliveryOrderInformationResultDomain.setErrorMessageList(errorMessageList);
            return deliveryOrderInformationResultDomain;
        }
        
        deliveryOrderInformationDomain = toUpperCase(deliveryOrderInformationDomain);
        
        /** Count Delivery Order return record from DeliveryOrderService. */
        recordCount = deliveryOrderService.searchCountDeliveryOrder(deliveryOrderInformationDomain);

        if (Constants.ZERO == recordCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /**
         * Get maximum record limit from RecordLimitService (Max record include
         * D/O detail).
         */
        MiscellaneousDomain recordLimit = new MiscellaneousDomain();
        recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD006_RLM);
        recordLimit = recordLimitService.searchRecordLimit(recordLimit);
        if (null == recordLimit) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /** Check If recordCount > recordLimit */
        if (Integer.valueOf(recordLimit.getMiscValue()) < recordCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {recordLimit.getMiscValue()});
        }

        /** Get maximum record per page from RecordLimitService. */
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD006_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimitPerPage(recordLimitPerPage);
        if (null == recordLimitPerPage) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /**
         * Count Delivery Order return record from DeliveryOrderService to
         * display at screen by D/O header data.
         */
        recordCountDisplay = deliveryOrderService
            .searchCountDeliveryOrderHeader(deliveryOrderInformationDomain);

        if (Constants.ZERO == recordCountDisplay) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /** Calculate rownum for query */
        deliveryOrderInformationDomain.setMaxRowPerPage(Integer
            .valueOf(recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(deliveryOrderInformationDomain, recordCountDisplay);

        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = null;

        //Get Delivery Order data to display at screen from DeliveryOrderService.
        deliveryOrderDetailDomain = deliveryOrderService
            .searchDeliveryOrderHeader(deliveryOrderInformationDomain);

        /** Set <List> doInformation to return Domain. */
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomainPage = 
            new ArrayList<DeliveryOrderDetailDomain>();
        for (DeliveryOrderDetailDomain doDetail : deliveryOrderDetailDomain) {
            String utc = doDetail
                .getDeliveryOrderSubDetailDomain().getUtc()
                .replace(Constants.SYMBOL_SPACE, Constants.EMPTY_STRING);

            Timestamp deliveryDate = doDetail
                .getDeliveryOrderSubDetailDomain().getDeliveryDatetime();
            Timestamp shipDate = doDetail
                .getDeliveryOrderSubDetailDomain().getShipDatetime();

            String deliveryDateFormat = DateUtil.format(deliveryDate,
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            String shipDateFormat = DateUtil.format(shipDate,
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);

            String deliveryDateUtc = StringUtil.appendsString(
                deliveryDateFormat, Constants.SYMBOL_OPEN_BRACKET,
                Constants.WORD_UTC, utc, Constants.SYMBOL_CLOSE_BRACKET);

            String shipDateUtc = StringUtil.appendsString(shipDateFormat,
                Constants.SYMBOL_OPEN_BRACKET, Constants.WORD_UTC, utc,
                Constants.SYMBOL_CLOSE_BRACKET);

            doDetail.getDeliveryOrderSubDetailDomain()
                .setDeliveryDatetimeUtc(deliveryDateUtc);
            doDetail.getDeliveryOrderSubDetailDomain()
                .setShipDatetimeUtc(shipDateUtc);
            
            // FIX : wrong dateformat
            if (null != doDetail.getDeliveryOrderSubDetailDomain().getDoIssueDate()) {
                doDetail.getDeliveryOrderSubDetailDomain().setDoIssueDateShow(
                    DateUtil.format(doDetail.getDeliveryOrderSubDetailDomain().getDoIssueDate(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
            }
            //SPS phase II additional requirement 2018-03-27
            //Get the receiving status on CIGMA
            List<String> densoCodeList = new ArrayList<String>();
            densoCodeList.add(doDetail.getDeliveryOrderSubDetailDomain().getDCd());
            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
            
            SpsMCompanyDensoDomain companyDensoDomain = new SpsMCompanyDensoDomain();
            companyDensoDomain.setDCd(densoCodeStr);
            List<As400ServerConnectionInformationDomain> schemaResultList 
                = densoCompanyService.searchAs400ServerList(companyDensoDomain);
            if(null == schemaResultList || Constants.ZERO == schemaResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026);
            }
            if(densoCodeList.size() != schemaResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0025);
            }
            
            for(As400ServerConnectionInformationDomain as400Server : schemaResultList){
                String cigmaDoNumber = doDetail.getDeliveryOrderSubDetailDomain().getCigmaDoNo();
                
                String receiveStatus = 
                    CommonWebServiceUtil.cigmaDoResourceSearchDoReceiving(cigmaDoNumber,
                        doDetail.getDeliveryOrderSubDetailDomain().getShippingTotal(),
                        as400Server,
                        locale);
                doDetail.getDeliveryOrderSubDetailDomain().setReceiveStatus(receiveStatus);
            }
            
            deliveryOrderDetailDomainPage.add(doDetail);
        }
        deliveryOrderInformationResultDomain
            .setDeliveryOrderDetailDomain(deliveryOrderDetailDomainPage);
        return deliveryOrderInformationResultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchDeliveryOrderInformationCSV(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public DeliveryOrderInformationResultDomain searchDeliveryOrderInformationCsv(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException {

        DeliveryOrderInformationResultDomain deliveryOrderInformationResultDomain = 
            new DeliveryOrderInformationResultDomain();
        List<ApplicationMessageDomain> errorMessageList = null;
        Locale locale = deliveryOrderInformationDomain.getLocale();
        CommonDomain commonDomain = new CommonDomain();

        /** Validate input parameter. */
        try {
            errorMessageList = this.validateCriteria(deliveryOrderInformationDomain);
            if (Constants.ZERO < errorMessageList.size()) {
                deliveryOrderInformationResultDomain.setErrorMessageList(errorMessageList);
                return deliveryOrderInformationResultDomain;
            }
            
            deliveryOrderInformationDomain = toUpperCase(deliveryOrderInformationDomain);

            /** Count Delivery Order return record from DeliveryOrderService. */
            int maxRecord = Constants.ZERO;
            maxRecord = deliveryOrderService.searchCountDeliveryOrder(
                deliveryOrderInformationDomain);

            if (Constants.ZERO == maxRecord) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }

            /** Get maximum record limit from RecordLimitService. */
            MiscellaneousDomain recordLimit = new MiscellaneousDomain();
            recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
            recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD006_RLM);
            recordLimit = recordLimitService.searchRecordLimit(recordLimit);
            if (null == recordLimit) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }

            /** Check maxRecord is over recordLimit. */
            if (Integer.valueOf(recordLimit.getMiscValue()) < maxRecord) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {recordLimit.getMiscValue()});
            }
            List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = null;

            /** Get Delivery Order data from DeliveryOrderService. */
            deliveryOrderDetailDomain = deliveryOrderService
                .searchDeliveryOrder(deliveryOrderInformationDomain);

            deliveryOrderInformationResultDomain
                .setDeliveryOrderDetailDomain(deliveryOrderDetailDomain);

            /** Create CSV file and set file to InputStream. */
            final String[] headerArr = new String[] {
                SupplierPortalConstant.LBL_ORDER_METHOD,
                SupplierPortalConstant.LBL_S_CD,
                SupplierPortalConstant.LBL_S_PCD,
                SupplierPortalConstant.LBL_D_CD,
                SupplierPortalConstant.LBL_D_PCD,
                SupplierPortalConstant.LBL_ISSUE_DATE,
                SupplierPortalConstant.LBL_DELIVERY_DATE,
                SupplierPortalConstant.LBL_DELIVERY_TIME,
                SupplierPortalConstant.LBL_SHIP_DATE,
                SupplierPortalConstant.LBL_SHIP_TIME,
                SupplierPortalConstant.LBL_TM,
                SupplierPortalConstant.LBL_ROUTE,
                SupplierPortalConstant.LBL_DEL,
                SupplierPortalConstant.LBL_ORIGINAL_SPS_DO,
                SupplierPortalConstant.LBL_PREVIOUS_SPS_DO,
                SupplierPortalConstant.LBL_CURRENT_SPS_DO_NO,
                SupplierPortalConstant.LBL_REVISION,
                SupplierPortalConstant.LBL_DO_SHIP_STATUS,
                SupplierPortalConstant.LBL_D_PN,
                SupplierPortalConstant.LBL_ITEM_DESC,
                SupplierPortalConstant.LBL_S_PN,
                SupplierPortalConstant.LBL_CURRENT_CIGMA_DO_NO,
                SupplierPortalConstant.LBL_PREVIOUS_QTY,
                SupplierPortalConstant.LBL_ORDER_QTY,
                SupplierPortalConstant.LBL_DIFFERENCE_QTY,
                SupplierPortalConstant.LBL_QTY_BOX,
                SupplierPortalConstant.LBL_NO_OF_BOX,
                SupplierPortalConstant.LBL_UNIT_OF_MEASURE,
                SupplierPortalConstant.LBL_RSN,
                SupplierPortalConstant.LBL_MODEL,
                SupplierPortalConstant.LBL_CTRL_NO,
                SupplierPortalConstant.LBL_WH_PRIME_RECEIVING,
                SupplierPortalConstant.LBL_DOCKCODE,
                SupplierPortalConstant.LBL_RECEIVING_LANE,
                SupplierPortalConstant.LBL_CYCLE,
                SupplierPortalConstant.LBL_KANBAN_SEQ,
                SupplierPortalConstant.LBL_KANBAN_TYPE,
                SupplierPortalConstant.LBL_RECEIVE_BY_SCAN};
            
            List<Map<String, Object>> resultDetail = this.doMapDetail(
                deliveryOrderDetailDomain, headerArr);
            
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            StringBuffer resultString = new StringBuffer();
            resultString = commonService.createCsvString(commonDomain);
            deliveryOrderInformationResultDomain.setResultString(resultString);
        } catch (IOException e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0012,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return deliveryOrderInformationResultDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchSelectedCompanySupplier(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

        dataScopeControlDomain.setUserType(plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain
            .setUserRoleDomainList(plantSupplierWithScopeDomain
                .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        if(!StringUtil.checkNullOrEmpty(
            plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd()))
        {
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd().toUpperCase());
        }
       
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
            dataScopeControlDomain.getUserRoleDomainList());
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of supplier plant information from SupplierPlantService. */
        List<PlantSupplierDomain> plantSupplierList = new ArrayList<PlantSupplierDomain>();
        plantSupplierList = plantSupplierService.searchPlantSupplier(
            plantSupplierWithScopeDomain);

        if (null == plantSupplierList || plantSupplierList.size() <= Constants.ZERO) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        return plantSupplierList;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

        dataScopeControlDomain.setUserType(plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain
            .setUserRoleDomainList(plantSupplierWithScopeDomain
                .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        if(!StringUtil.checkNullOrEmpty(
            plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd()))
        {
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd().toUpperCase());
        }
       
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
            dataScopeControlDomain.getUserRoleDomainList());
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        
        /** Get supplier company information from SupplierCompanyService. */
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<CompanySupplierDomain> companySupplierDomain = null;
        companySupplierDomain = companySupplierService
            .searchCompanySupplier(plantSupplierWithScopeDomain);
        if (null == companySupplierDomain
            || Constants.ZERO == companySupplierDomain.size()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE, locale)});
        }

        return companySupplierDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchSelectedCompanyDenso(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        Locale locale = plantDensoWithScopeDomain.getLocale();

        dataScopeControlDomain.setUserType(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getDensoSupplierRelationDomainList());

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));

        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        if(!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            dataScopeControlDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setUserRoleDomainList(dataScopeControlDomain.getUserRoleDomainList());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoList = new ArrayList<PlantDensoDomain>();
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if (null == plantDensoList || plantDensoList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        return plantDensoList;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        Locale locale = plantDensoWithScopeDomain.getLocale();

        dataScopeControlDomain.setUserType(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getDensoSupplierRelationDomainList());

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));

        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        if(!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            dataScopeControlDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setUserRoleDomainList(dataScopeControlDomain.getUserRoleDomainList());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get DENSO company information from DensoCompanyService. */
        List<CompanyDensoDomain> companyDensoDomain = null;
        companyDensoDomain = densoCompanyService.searchCompanyDenso(plantDensoWithScopeDomain);
        if (null == companyDensoDomain
            || Constants.ZERO == companyDensoDomain.size()) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }
        
        return companyDensoDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#findFileName(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public FileManagementDomain searchFileName(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) throws ApplicationException
    {
        /**
         * Find file name by call Service FileManagementService
         * searchFileDownload() by pass 3 parameters as following.
         */
        FileManagementDomain resultDomain = null;
        try {
            resultDomain = fileManagementService.searchFileDownload(
                deliveryOrderInformationDomain.getPdfFileId(), false, null);
        } catch (IOException e) {
            Locale locale = deliveryOrderInformationDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        deliveryOrderInformationDomain.setPdfFileName(resultDomain.getFileName());
        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchDeliveryOrderReport(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain,
     *      java.io.OutputStream)
     */
    public FileManagementDomain searchDeliveryOrderReport(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain,
        OutputStream output) throws ApplicationException
    {
        /**
         * Get PDF file from call Service FileManagementService
         * searchFileDownload() by pass 3 parameters as following.
         */
        FileManagementDomain resultDomain = null;
        try {
            resultDomain = fileManagementService.searchFileDownload(
                deliveryOrderInformationDomain.getPdfFileId(), true, output);
        } catch (IOException e) {
            Locale locale = deliveryOrderInformationDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain,
     *      java.io.OutputStream)
     */
    public FileManagementDomain searchLegendInfo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain,
        OutputStream output) throws ApplicationException
    {
        /** Get PDF file from FileManagementService. */
        FileManagementDomain resultDomain = null;
        try {
            resultDomain = fileManagementService.searchFileDownload(
                deliveryOrderInformationDomain.getPdfFileId(), true, output);
        } catch (IOException e) {
            Locale locale = deliveryOrderInformationDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#validateCriteria(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    private List<ApplicationMessageDomain> validateCriteria(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException {

        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = deliveryOrderInformationDomain.getLocale();
        boolean issueDateFrom = false;
        boolean issueDateTo = false;
        boolean deliveryDateFrom = false;
        boolean deliveryDateTo = false;
        boolean shipDateFrom = false;
        boolean shipDateTo = false;

        /** Search criteria are empty. */
        if (StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getVendorCd())
            || StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getSPcd())
            || StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDCd())
            || StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDPcd())
            || StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getIssueDateFrom())
            || StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getIssueDateTo()))
        {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil
                    .getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        } else {
            if (deliveryOrderInformationDomain.getVendorCd().equals(Constants.MISC_CODE_ALL)) {
                deliveryOrderInformationDomain.setVendorCd(Constants.EMPTY_STRING);
            }
            if (deliveryOrderInformationDomain.getSPcd().equals(
                Constants.MISC_CODE_ALL)) {
                deliveryOrderInformationDomain.setSPcd(Constants.EMPTY_STRING);
            }
            if (deliveryOrderInformationDomain.getDCd().equals(
                Constants.MISC_CODE_ALL)) {
                deliveryOrderInformationDomain.setDCd(Constants.EMPTY_STRING);
            }
            if (deliveryOrderInformationDomain.getDPcd().equals(
                Constants.MISC_CODE_ALL)) {
                deliveryOrderInformationDomain.setDPcd(Constants.EMPTY_STRING);
            }
        }

        /** Check format of Issue Date From. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getIssueDateFrom())) {
            if (!DateUtil.isValidDate(
                deliveryOrderInformationDomain.getIssueDateFrom(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_ISSUE_DATE_FROM,
                                locale)})));
                issueDateFrom = false;
            } else {
                issueDateFrom = true;
            }
        }

        /** Check format of Issue Date To. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getIssueDateTo())) {
            if (!DateUtil.isValidDate(
                deliveryOrderInformationDomain.getIssueDateTo(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_ISSUE_DATE_TO,
                                locale)})));
                issueDateTo = false;
            } else {
                issueDateTo = true;
            }
        }

        /** Check format of Delivery Date From. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDeliveryDateFrom())) {
            if (!DateUtil.isValidDate(
                deliveryOrderInformationDomain.getDeliveryDateFrom(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_DELIVERY_DATE_FROM,
                                locale)})));
                deliveryDateFrom = false;
            } else {
                deliveryDateFrom = true;
            }
        }

        /** Check format of Delivery Date To. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDeliveryDateTo())) {
            if (!DateUtil.isValidDate(
                deliveryOrderInformationDomain.getDeliveryDateTo(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_DELIVERY_DATE_TO,
                                locale)})));
                deliveryDateTo = false;
            } else {
                deliveryDateTo = true;
            }
        }

        /** Check format and compare from to Delivery Time. */
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDeliveryTimeFrom())
            && !StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDeliveryTimeTo()))
        {
            boolean isValidDeliveryTimeFrom = true;
            boolean isValidDeliveryTimeTo = true;
            if(!deliveryOrderInformationDomain.getDeliveryTimeFrom().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_TIME_FROM)));
                isValidDeliveryTimeFrom = false;
            }
            if(!deliveryOrderInformationDomain.getDeliveryTimeTo().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_TIME_TO)));
                isValidDeliveryTimeTo = false;
            }
            if(isValidDeliveryTimeFrom && isValidDeliveryTimeTo){
                Date d1 = DateUtil.parseToUtilDate(
                    deliveryOrderInformationDomain.getDeliveryTimeFrom()
                    , DateUtil.PATTERN_HHMM_COLON);
                Date d2 = DateUtil.parseToUtilDate(
                    deliveryOrderInformationDomain.getDeliveryTimeTo()
                    , DateUtil.PATTERN_HHMM_COLON);
                if(d1.after(d2)){
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_DELIVERY_TIME_FROM),
                                    MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_DELIVERY_TIME_TO)})));
                }
            }
        }else{
            if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDeliveryTimeFrom())
                || !StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDeliveryTimeTo()))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                        SupplierPortalConstant.LBL_BOTH_DELIVERY_TIME_FROM_AND_TO)));
            }
        }
        
        /** Check format of Ship Date From. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getShipDateFrom())) {
            if (!DateUtil.isValidDate(
                deliveryOrderInformationDomain.getShipDateFrom(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_SHIP_DATE_FORM,
                                locale)})));
                shipDateFrom = false;
            } else {
                shipDateFrom = true;
            }
        }

        /** Check format of Ship Date To. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getShipDateTo())) {
            if (!DateUtil.isValidDate(
                deliveryOrderInformationDomain.getShipDateTo(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                                new String[] {getLabel(
                                    SupplierPortalConstant.LBL_SHIP_DATE_TO,
                                    locale)})));
                shipDateTo = false;
            } else {
                shipDateTo = true;
            }
        }

        /** Check Issue Date From is rather than Issue Date To. */
        if (issueDateFrom && issueDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                deliveryOrderInformationDomain.getIssueDateFrom(),
                deliveryOrderInformationDomain.getIssueDateTo())) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                            new String[] {
                                getLabel(
                                    SupplierPortalConstant.LBL_ISSUE_DATE_FROM,
                                    locale),
                                getLabel(
                                    SupplierPortalConstant.LBL_ISSUE_DATE_TO,
                                    locale)})));
            }

        }

        /** Check Delivery Date From is rather than Delivery Date To. */
        if (deliveryDateFrom && deliveryDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                deliveryOrderInformationDomain.getDeliveryDateFrom(),
                deliveryOrderInformationDomain.getDeliveryDateTo())) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                        new String[] {
                            getLabel(
                                SupplierPortalConstant.LBL_DELIVERY_DATE_FROM,
                                locale),
                            getLabel(
                                SupplierPortalConstant.LBL_DELIVERY_DATE_TO,
                                locale)})));
            }
        }

        /** Check Ship Date From is rather than Ship Date To. */
        if (shipDateFrom && shipDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                deliveryOrderInformationDomain.getShipDateFrom(),
                deliveryOrderInformationDomain.getShipDateTo())) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                            new String[] {
                                getLabel(
                                    SupplierPortalConstant.LBL_SHIP_DATE_FORM,
                                    locale),
                                getLabel(
                                    SupplierPortalConstant.LBL_SHIP_DATE_TO,
                                    locale)})));
            }
        }

        /** Check Length of Route No. is exceed the length of field in table. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getRouteNo())) {
            if (Constants.SIX < deliveryOrderInformationDomain.getRouteNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                            new String[] {
                                getLabel(SupplierPortalConstant.LBL_ROUTE_NO,
                                    locale), String.valueOf(Constants.SIX)})));
            }
        }

        /**
         * Check Length of Truck Sequence is exceed the length of field in
         * table.
         */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDel())) {
            if (Constants.THREE < deliveryOrderInformationDomain.getDel().length()) {
                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                                new String[] {
                                    getLabel(
                                        SupplierPortalConstant.RPT_PRM_DEL,
                                        locale),
                                    String.valueOf(Constants.THREE)})));
            }
        }

        /** Length of SPS D/O No. is exceed the length of field in table. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getSpsDoNo())) {
            if (Constants.ELEVEN < deliveryOrderInformationDomain.getSpsDoNo().length()) {
                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                                new String[] {
                                    getLabel(SupplierPortalConstant.LBL_DO_NO,
                                        locale),
                                    String.valueOf(Constants.ELEVEN)})));
            }
        }

        /** Length of CIGMA D/O No. is exceed the length of field in table. */
        if (!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getCigmaDoNo())) {
            if (Constants.TEN < deliveryOrderInformationDomain.getCigmaDoNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                            new String[] {
                                getLabel(
                                    SupplierPortalConstant.LBL_CIGMA_DO_NO,
                                    locale), String.valueOf(Constants.TEN)})));
            }
        }
        return errorMessageList;
    }

    /**
     * Map Detail.
     * 
     * @param deliveryOrderDetailDomain deliveryOrderDetailDomain
     * @param header the List
     * @return resultList
     */
    private List<Map<String, Object>> doMapDetail(
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain, String[] header)
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> deliveryOrderMap = null;
        DeliveryOrderSubDetailDomain doHeader = null;
        String itemDesc = null;
        String doIssueDate = null;
        String deliveryDate = null;
        String deliveryTime = null;
        String shipDate = null;
        String shipTime = null;
        
        for (DeliveryOrderDetailDomain deliveryOrderDetailDomaintmp : deliveryOrderDetailDomain)
        {
            for (DeliveryOrderDoDetailDomain doDetail
                : deliveryOrderDetailDomaintmp.getDeliveryOrderDoDetailDomain())
            {
                doIssueDate = Constants.EMPTY_STRING;
                deliveryDate = Constants.EMPTY_STRING;
                deliveryTime = Constants.EMPTY_STRING;
                shipDate = Constants.EMPTY_STRING;
                shipTime = Constants.EMPTY_STRING;
                itemDesc = Constants.EMPTY_STRING;
                doHeader = deliveryOrderDetailDomaintmp.getDeliveryOrderSubDetailDomain();
                
                if(null != doHeader.getDoIssueDate()){
                    doIssueDate = DateUtil.format(doHeader.getDoIssueDate(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                }
                
                if(null != doHeader.getDeliveryDatetime()){
                    deliveryDate = DateUtil.format(doHeader.getDeliveryDatetime(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                    deliveryTime = DateUtil.format(doHeader.getDeliveryDatetime(),
                        DateUtil.PATTERN_HHMM_COLON);
                }
                
                if(null != doHeader.getShipDatetime()){
                    shipDate = DateUtil.format(doHeader.getShipDatetime(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                    shipTime = DateUtil.format(doHeader.getShipDatetime(),
                        DateUtil.PATTERN_HHMM_COLON);
                }
                
                if(!StringUtil.checkNullOrEmpty(doDetail.getItemDesc())){
                    itemDesc = StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE,
                        doDetail.getItemDesc(), Constants.SYMBOL_DOUBLE_QUOTE);
                }
                
                deliveryOrderMap = new HashMap<String, Object>();
                deliveryOrderMap.put(header[Constants.ZERO],
                    StringUtil.checkNullToEmpty(doHeader.getOrderMethod()));
                deliveryOrderMap.put(header[Constants.ONE],
                    StringUtil.checkNullToEmpty(doHeader.getVendorCd()));
                deliveryOrderMap.put(header[Constants.TWO],
                    StringUtil.checkNullToEmpty(doHeader.getSPcd()));
                deliveryOrderMap.put(header[Constants.THREE],
                    StringUtil.checkNullToEmpty(doHeader.getDCd()));
                deliveryOrderMap.put(header[Constants.FOUR],
                    StringUtil.checkNullToEmpty(doHeader.getDPcd()));
                deliveryOrderMap.put(header[Constants.FIVE], doIssueDate);
                deliveryOrderMap.put(header[Constants.SIX], deliveryDate);
                deliveryOrderMap.put(header[Constants.SEVEN], deliveryTime);
                deliveryOrderMap.put(header[Constants.EIGHT], shipDate);
                deliveryOrderMap.put(header[Constants.NINE], shipTime);
                deliveryOrderMap.put(header[Constants.TEN],
                    StringUtil.checkNullToEmpty(doHeader.getTm()));
                deliveryOrderMap.put(header[Constants.ELEVEN],
                    StringUtil.checkNullToEmpty(doHeader.getTruckRoute()));
                deliveryOrderMap.put(header[Constants.TWELVE],
                    StringUtil.checkNullToEmpty(doHeader.getDel()));
                
                /* Start : [ADDITIONAL] Mr. Sittichai request for add revision to Original D/O No.
                 * and Previous D/O No.
                 * */ 
                //deliveryOrderMap.put(header[Constants.THIRTEEN],
                //    StringUtil.checkNullToEmpty(doHeader.getSpsDoNo()));
                //deliveryOrderMap.put(header[Constants.FOURTEEN],
                //    StringUtil.checkNullToEmpty(doHeader.getPreviousSpsDoNo()));
                deliveryOrderMap.put(header[Constants.THIRTEEN],
                    StringUtil.appendsString(StringUtil.checkNullToEmpty(doHeader.getSpsDoNo())
                        , Constants.DEFAULT_REVISION));
                if (Strings.judgeBlank(doHeader.getPreviousSpsDoNo())) {
                    deliveryOrderMap.put(header[Constants.FOURTEEN], Constants.EMPTY_STRING);
                } else {
                    String previousSpsDo = doHeader.getPreviousSpsDoNo().trim();
                    if (previousSpsDo.length() <= Constants.NINE) {
                        deliveryOrderMap.put(header[Constants.FOURTEEN]
                            , StringUtil.appendsString(previousSpsDo, Constants.DEFAULT_REVISION));
                    } else {
                        deliveryOrderMap.put(header[Constants.FOURTEEN], previousSpsDo);
                    }
                }
                // END : [ADDITIONAL]
                
                deliveryOrderMap.put(header[Constants.FIFTEEN],
                    StringUtil.checkNullToEmpty(doHeader.getCurrentSpsDoNo()));
                deliveryOrderMap.put(header[Constants.SIXTEEN], doHeader.getRevision());
                deliveryOrderMap.put(header[Constants.SEVENTEEN], doHeader.getShipmentStatus());
                deliveryOrderMap.put(header[Constants.EIGHTEEN], doDetail.getDPn());
                deliveryOrderMap.put(header[Constants.NINETEEN], itemDesc);
                deliveryOrderMap.put(header[Constants.TWENTY], doDetail.getSPn());
                
                /* [IN042] : Current CIGMA D/O No. is CHG CIGMA D/O No. in DO_DETAIL (if empty use
                 * CIGMA_DO_NO)
                 * */ 
                //deliveryOrderMap.put(header[Constants.TWENTY_ONE], doHeader.getCurrentCigmaDoNo());
                deliveryOrderMap.put(header[Constants.TWENTY_ONE], doHeader.getCigmaDoNo());
                if (!Strings.judgeBlank(doDetail.getChgCigmaDoNo())) {
                    deliveryOrderMap.put(header[Constants.TWENTY_ONE], doDetail.getChgCigmaDoNo());
                }
                
                deliveryOrderMap.put(header[Constants.TWENTY_TWO],
                    StringUtil.checkNullToEmpty(doDetail.getPreviousQty()));
                deliveryOrderMap.put(header[Constants.TWENTY_THREE],
                    StringUtil.checkNullToEmpty(doDetail.getCurrentOrderQty()));
                
                if(Constants.DEFAULT_REVISION.equals(doDetail.getPnRevision())) {
                    deliveryOrderMap.put(header[Constants.TWENTY_FOUR], Constants.STR_ZERO);
                }else{
                    BigDecimal diffQty = new BigDecimal(Constants.ZERO);
                    BigDecimal orderQty = NumberUtil.toBigDecimalDefaultZero(
                        doDetail.getCurrentOrderQty());
                    BigDecimal previousQty = NumberUtil.toBigDecimalDefaultZero(
                        doDetail.getPreviousQty());
                    
                    diffQty = orderQty.subtract(previousQty);
                    deliveryOrderMap.put(header[Constants.TWENTY_FOUR], diffQty.toString());
                }
                
                deliveryOrderMap.put(header[Constants.TWENTY_FIVE],
                    StringUtil.checkNullToEmpty(doDetail.getQtyBox()));
                deliveryOrderMap.put(header[Constants.TWENTY_SIX],
                    StringUtil.checkNullToEmpty(doDetail.getNoOfBoxes()));
                deliveryOrderMap.put(header[Constants.TWENTY_SEVEN],
                    StringUtil.checkNullToEmpty(doDetail.getUnitOfMeasure()));
                deliveryOrderMap.put(header[Constants.TWENTY_EIGHT],
                    StringUtil.checkNullToEmpty(doDetail.getChgReason()));
                deliveryOrderMap.put(header[Constants.TWENTY_NINE],
                    StringUtil.checkNullToEmpty(doDetail.getModel()));
                deliveryOrderMap.put(header[Constants.THIRTY],
                    StringUtil.checkNullToEmpty(doDetail.getCtrlNo()));
                deliveryOrderMap.put(header[Constants.THIRTY_ONE],
                    StringUtil.checkNullToEmpty(doHeader.getWhPrimeReceiving()));
                deliveryOrderMap.put(header[Constants.THIRTY_TWO],
                    StringUtil.checkNullToEmpty(doHeader.getDockCode()));
                deliveryOrderMap.put(header[Constants.THIRTY_THREE],
                    StringUtil.checkNullToEmpty(doDetail.getRcvLane()));
                deliveryOrderMap.put(header[Constants.THIRTY_FOUR],
                    StringUtil.checkNullToEmpty(doHeader.getCycle()));
                if(StringUtil.checkNullOrEmpty(doDetail.getKanbanTagSequence())){
                    deliveryOrderMap.put(header[Constants.THIRTY_FIVE], Constants.EMPTY_STRING);
                } else {
                    deliveryOrderMap.put(header[Constants.THIRTY_FIVE],
                        StringUtil.checkNullToEmpty(
                            StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE, 
                                doDetail.getKanbanTagSequence(), 
                                Constants.SYMBOL_DOUBLE_QUOTE)));
                }
                deliveryOrderMap.put(header[Constants.THIRTY_SIX],
                    StringUtil.checkNullToEmpty(doDetail.getKanbanType()));
                deliveryOrderMap.put(header[Constants.THIRTY_SEVEN],
                    StringUtil.checkNullToEmpty(doHeader.getReceiveByScan()));
                
                resultList.add(deliveryOrderMap);
            }
        }
        return resultList;
    }
    
   
    /**
     * <p>Converts all of the characters in this String to upper case using the rules of the default locale.</p>
     *
     * @param deliveryOrderInformationDomain DeliveryOrderInformation Domain
     * @return DeliveryOrderInformationDomain
     */
    private DeliveryOrderInformationDomain toUpperCase(DeliveryOrderInformationDomain 
        deliveryOrderInformationDomain){
        
        deliveryOrderInformationDomain.setVendorCd(
            deliveryOrderInformationDomain.getVendorCd().toUpperCase());
       
        deliveryOrderInformationDomain.setSPcd(
            deliveryOrderInformationDomain.getSPcd().toUpperCase());
      
        deliveryOrderInformationDomain.setDCd(
            deliveryOrderInformationDomain.getDCd().toUpperCase());
        
        deliveryOrderInformationDomain.setDPcd(
            deliveryOrderInformationDomain.getDPcd().toUpperCase());
        
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getRevision())){
            deliveryOrderInformationDomain.setRevision(deliveryOrderInformationDomain.
                getRevision().toUpperCase());
        }
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getShipmentStatus())){
            deliveryOrderInformationDomain.setShipmentStatus(deliveryOrderInformationDomain.
                getShipmentStatus().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getTransportMode())){
            deliveryOrderInformationDomain.setTransportMode(deliveryOrderInformationDomain.
                getTransportMode().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getRouteNo())){
            deliveryOrderInformationDomain.setRouteNo(deliveryOrderInformationDomain.
                getRouteNo().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getDel())){
            deliveryOrderInformationDomain.setDel(deliveryOrderInformationDomain.
                getDel().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getSpsDoNo())){
            deliveryOrderInformationDomain.setSpsDoNo(deliveryOrderInformationDomain.
                getSpsDoNo().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(deliveryOrderInformationDomain.getCigmaDoNo())){
            deliveryOrderInformationDomain.setCigmaDoNo(deliveryOrderInformationDomain.
                getCigmaDoNo().toUpperCase());
        }
        
        return deliveryOrderInformationDomain;
    }

    /**
     * <p>
     * Get Label method.
     * </p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {
        String labelString = MessageUtil.getLabelHandledException(locale, key);
        return labelString;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#updateDateTimeKanbanPdf(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer updateDateTimeKanbanPdf(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException {
        /** Count Delivery Order return record from DeliveryOrderService. */
        Integer resultCode = deliveryOrderService.updateDateTimeKanbanPdf(deliveryOrderInformationDomain);
        return resultCode;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#createOneWayKanbanTagReport(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public InputStream createOneWayKanbanTagReport(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException {
        
        InputStream report = null;
        Locale locale = deliveryOrderInformationDomain.getLocale();
        List<OneWayKanbanTagReportDomain> oneWayKanbanTagReportDomain = deliveryOrderService
            .searchOneWayKanbanTagReportInformation(deliveryOrderInformationDomain);
        try {
            report = deliveryOrderService.createOneWayKanbanTagReport(oneWayKanbanTagReportDomain);
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0017);
        }
        return report;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchGenerateDo(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public FileManagementDomain searchGenerateDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) throws ApplicationException
    {
        BigDecimal doId = new BigDecimal(deliveryOrderInformationDomain.getPdfFileId());
        Locale locale = deliveryOrderInformationDomain.getLocale();
//        Generate Report.
        InputStream isReportData = null;
        StringBuffer filenameReport = new StringBuffer();
        try {
            DeliveryOrderSubDetailDomain doReport = new DeliveryOrderSubDetailDomain();
            doReport.setDoId( doId.toString() );
            isReportData = deliveryOrderService.searchDeliveryOrderReport(doReport);
            filenameReport.append(SupplierPortalConstant.DELIVERY_ORDER);
            filenameReport.append(doId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(
                locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0013,
                null );
        }
        FileManagementDomain resultDomain = new FileManagementDomain();
        resultDomain = new FileManagementDomain();
        resultDomain.setFileData(isReportData);
        resultDomain.setFileName(filenameReport.toString());
        return resultDomain;
    }
}
