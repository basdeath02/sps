/*
 * ModifyDate Development company     Describe 
 * 2015/03/11 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaDoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDomain;

/**
 * <p>The Class CigmaDoErrorServiceImpl.</p>
 * <p>For SPS_CIGMA_DO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferDoError</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaDoErrorServiceImpl implements CigmaDoErrorService {

    /** The DAO for SPS_CIGMA_DO_ERROR. */
    private CigmaDoErrorDao cigmaDoErrorDao;
    
    /** The default constructor. */
    public CigmaDoErrorServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for cigmaDoErrorDao.</p>
     *
     * @param cigmaDoErrorDao Set for cigmaDoErrorDao
     */
    public void setCigmaDoErrorDao(CigmaDoErrorDao cigmaDoErrorDao) {
        this.cigmaDoErrorDao = cigmaDoErrorDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CigmaDoErrorService#searchTransferDoError(
     * com.globaldenso.asia.sps.auto.business.domain.CigmaDoErrorDomain)
     */
    public List<TransferDoErrorEmailDomain> searchTransferDoError(
        SpsCigmaDoErrorDomain spsCigmaDoErrorDomain)
    {
        List<TransferDoErrorEmailDomain> result = null;
        result = this.cigmaDoErrorDao.searchTransferDoError(spsCigmaDoErrorDomain);
        return result;
    }

}
