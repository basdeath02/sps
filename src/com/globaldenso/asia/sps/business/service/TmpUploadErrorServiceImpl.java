/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.dao.TmpUploadErrorDao;


/**
 * <p>The Class TempUploadErrorServiceImpl.</p>
 * <p>Manage data of Temporary Upload Error.</p>
 * <ul>
 * <li>Method search  : searchCountWarning</li>
 * <li>Method search  : searchCountIncorrect</li>
 * <li>Method search  : searchGroupOfValidationError</li>
 * <li>Method search  : searchCountGroupOfValidationError</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class TmpUploadErrorServiceImpl implements TmpUploadErrorService {
    


    /** The  Temporary Upload Error Dao. */
    private TmpUploadErrorDao tmpUploadErrorDao;
    
    /**
     * Instantiates a new Temporary Upload Error Service implement.
     */
    public TmpUploadErrorServiceImpl(){
        super();
    }
    
    /**
     * Sets the Temporary Upload Error Dao.
     * 
     * @param tmpUploadErrorDao the new Temporary Upload Error Dao.
     */
    public void setTmpUploadErrorDao(TmpUploadErrorDao tmpUploadErrorDao) {
        this.tmpUploadErrorDao = tmpUploadErrorDao;
    }


    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadErrorService#searchCountWarning
     * (com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    public int searchCountWarning(GroupUploadErrorDomain groupUploadErrorDomain){
        int countRecord = tmpUploadErrorDao.searchCountWarning(groupUploadErrorDomain);
        return countRecord;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadErrorService#searchCountIncorrect
     * (com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    public int searchCountIncorrect(GroupUploadErrorDomain groupUploadErrorDomain){
        int countRecord = tmpUploadErrorDao.searchCountIncorrect(groupUploadErrorDomain);
        return countRecord;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadErrorService#SearchTempUploadError
     * (com.globaldenso.asia.sps.business.domain.TempUploadErrorDomain)
     */
    public List<GroupUploadErrorDomain> searchGroupOfValidationError(GroupUploadErrorDomain 
        groupUploadErrorDomain){
        List<GroupUploadErrorDomain> result = (List<GroupUploadErrorDomain>)tmpUploadErrorDao
            .searchGroupOfValidationError(groupUploadErrorDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadErrorService#searchCountGroupOfValidationError
     * (com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    public int searchCountGroupOfValidationError(GroupUploadErrorDomain groupUploadErrorDomain){
        int countRecord = tmpUploadErrorDao.searchCountGroupOfValidationError(
            groupUploadErrorDomain);
        return countRecord;
    }
    
}