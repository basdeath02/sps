/*
 * ModifyDate Development company     Describe 
 * 2014/06/18 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadResultDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface CNDNDownloadingFacadeService.</p>
 * <p>Facade for CNDNDownloadingFacadeService.</p>
 * <ul>
 * <li>Method search  : searchCountFileUploadDetail</li>
 * <li>Method search  : searchFileUploadDetail</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchFileDownload</li>
 * </ul>
 *
 * @author CSI
 */
public interface CnDnDownloadingFacadeService {
    
    
    
    /**
     * <p>Search file upload detail.</p>
     * <ul>
     * <li>Search file upload item detail for display on CN/DN Downloading screen.</li>
     * </ul>
     * 
     * @param fileUploadCriteriaDomain the file upload criteria domain
     * @return the list of File Upload Domain.
     * @throws Exception Exception
     */
    public FileUploadResultDomain searchFileDownloadInformation(
        FileUploadCriteriaDomain fileUploadCriteriaDomain)
        throws Exception;
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search file name for validation duplicate file name.</li>
     * </ul>
     * 
     * @param fileUploadDomain the file upload domain
     * @return the string.
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(FileUploadDomain fileUploadDomain) throws ApplicationException;
        
    /**
     * <p>Search file download.</p>
     * <ul>
     * <li>Search file download from file was uploaded.</li>
     * </ul>
     * 
     * @param fileUploadDomain the file upload domain
     * @param output the output stream
     * 
     * @throws ApplicationException ApplicationException
     */
    public void searchFileDownloadCsv(FileUploadDomain fileUploadDomain, OutputStream output) 
        throws ApplicationException;
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search file name for validation duplicate file name.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the Data Scope Control Domain
     * @return List<SpsMCompanySupplierDomain>
     * @throws ApplicationException ApplicationException
     */
    public FileUploadResultDomain searchInitial(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException;
    
    /**
    * <p>Search file name.</p>
    * <ul>
    * <li>Search file name for validation duplicate file name.</li>
    * </ul>
    * 
    * @param plantSupplierWithScopeDomain the Data Scope Control Domain
    * @return List<SpsMCompanySupplierDomain>
    * @throws ApplicationException ApplicationException
    */
    public List<SpsMMiscDomain> searchSelectedCompanySupplier(PlantSupplierWithScopeDomain 
        plantSupplierWithScopeDomain)throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScope PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Company Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}