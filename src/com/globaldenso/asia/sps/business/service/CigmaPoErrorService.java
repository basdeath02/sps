/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 2016/03/16 CSI Akat                [IN068]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Date;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;

/**
 * <p>The Interface CigmaPoErrorService.</p>
 * <p>For SPS_CIGMA_PO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferPoError</li>
 * <li>Method search  : searchMinIssueDate</li>
 * </ul>
 *
 * @author CSI
 */
public interface CigmaPoErrorService {

    /**
     * Search transfer P/O error.
     * 
     * @param spsCigmaPoErrorDomain the SPS_CIGMA_PO_ERROR
     * @return List<TransferPoErrorEmailDomain>
    */
    public List<TransferPoErrorEmailDomain> searchTransferPoError(
        SpsCigmaPoErrorDomain spsCigmaPoErrorDomain);

    // [IN068] add method for search minimum issue date
    /**
     * Search Minimum Issue Date for error Supplier Information Not Found.
     * @param spsCigmaPoErrorDomain the domain for SPS_CIGMA_PO_ERROR
     * @return minimum Issue Date
     * */
    public Date searchMinIssueDate(SpsCigmaPoErrorDomain spsCigmaPoErrorDomain);
    
}
