/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface PlantSupplierService.</p>
 * <p>Service for Plant Service about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchSupplierPlant</li>
 * </ul>
 *
 * @author CSI
 */
public interface PlantSupplierService {

    /**
     * Search Supplier Plant by user role (or relate to user role).
     * @param plantSupplierWithScopeDomain Plant Supplier with scope
     * @return List Of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Supplier Plant by user role (or relate to user role).
     * @param plantSupplierWithScopeDomain Plant Supplier with scope
     * @return List Of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplierNew(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Supplier Plant Information by user role (or relate to user role).
     * @param plantSupplierWithScopeDomain Plant Supplier with scope
     * @return List Of Plant Supplier
     * */
    public List<PlantSupplierDomain> searchPlantSupplierInformation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
    
    /**
     * Search Exist Plant Supplier relate with user role.
     * 
     * @param plantSupplierWithScopeDomain plant supplier with scope
     * @return count of plant supplier.
     * */
    public Integer searchExistPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain);
}
