/*
 * ModifyDate Development company     Describe 
 * 2015/03/11 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDomain;

/**
 * <p>The Interface CigmaDoErrorService.</p>
 * <p>For SPS_CIGMA_DO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferDoError</li>
 * </ul>
 *
 * @author CSI
 */
public interface CigmaDoErrorService {

    /**
     * Search transfer D/O error.
     * 
     * @param spsCigmaDoErrorDomain the SPS_CIGMA_DO_ERROR
     * @return List<TransferDoErrorEmailDomain>
    */
    public List<TransferDoErrorEmailDomain> searchTransferDoError(
        SpsCigmaDoErrorDomain spsCigmaDoErrorDomain);
    
}
