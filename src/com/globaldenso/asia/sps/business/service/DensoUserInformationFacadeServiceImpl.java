/*
 * ModifyDate Development company     Describe 
 * 2014/06/06 CSI Phakaporn           Create
 * 2014/08/06 CSI Phakaporn           Modified
 * 2016/02/10 CSI Akat                [IN049]
 * 
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class DensoUserInformationFacadeServiceImpl.</p>
 * <p>Manage data of DENSO User.</p>
 * <ul>
 * <li>Method search  : searchDensoSupplierRelation</li>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchinitialWithCriteria</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchUserDenso</li>
 * <li>Method update  : deleteUserDenso</li>
 * <li>Method search  : searchUserDensoCSV</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class DensoUserInformationFacadeServiceImpl implements DensoUserInformationFacadeService {
    
    /** The DENSO user service. */
    private UserDensoService userDensoService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The DENSO supplier relation service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The Company DENSO service. */
    private CompanyDensoService companyDensoService; 
    
    /** The Plant DENSO service. */
    private PlantDensoService plantDensoService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The SPS M User Service. */
    private SpsMUserService spsMUserService;
    
    /** The SPS M User Role Service. */
    private SpsMUserRoleService spsMUserRoleService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new DENSO service implement.
     */
    public DensoUserInformationFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the DENSO user service.
     * 
     * @param userDensoService the new DENSO user service.
     */
    public void setUserDensoService(UserDensoService userDensoService) {
        this.userDensoService = userDensoService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets the DENSO supplier Relation service.
     * 
     * @param densoSupplierRelationService the new DENSO supplier Relation service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    /**
     * Sets the Company DENSO service.
     * 
     * @param companyDensoService the new Company DENSO service.
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    /**
     * Sets The SPS M User Service.
     * 
     * @param spsMUserService the new SPS M User Service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    /**
     * Sets The Plant DENSO Service.
     * 
     * @param plantDensoService the new Plant DENSO Service.
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    /**
     * Sets The SPS M User Role Service.
     * 
     * @param spsMUserRoleService the new SPS M User Role Service.
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#searchDensoSupplierRelation
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public DataScopeControlDomain searchDensoSupplierRelation(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException
    {
        List<DensoSupplierRelationDomain> resultList = new ArrayList<DensoSupplierRelationDomain>();
        Locale locale = dataScopeControlDomain.getLocale();
        
        /*Get relation between DENSO and Supplier*/
        resultList = densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(Constants.ZERO < resultList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(resultList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        return dataScopeControlDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#searchInitial
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public DensoUserInformationDomain searchInitial(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException
    {
        List<CompanyDensoDomain> companyDensoList = null;
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = dataScopeControlDomain.getLocale();
        DensoUserInformationDomain result = new DensoUserInformationDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        if(Constants.ZERO < dataScopeControlDomain.getDensoSupplierRelationDomainList().size()){
            result.setDataScopeControlDomain(dataScopeControlDomain);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        /*Get DENSO company information*/
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoList = companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == companyDensoList || companyDensoList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        /*Get plant DENSO information*/
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoList = this.plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null == plantDensoList || plantDensoList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        result.setCompanyDensoList(companyDensoList);
        result.setPlantDensoList(plantDensoList);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#initialWithCriteria
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public DensoUserInformationDomain searchInitialWithCriteria(
        DensoUserInformationDomain densoUserInfoDomain) throws ApplicationException
    {
        Locale locale = densoUserInfoDomain.getLocale();
        List<DensoUserInformationDomain> resultList = new ArrayList<DensoUserInformationDomain>();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        List<CompanyDensoDomain> companyDensoList = new ArrayList<CompanyDensoDomain>();
        List<PlantDensoDomain> plantDensoList = new ArrayList<PlantDensoDomain>();
        DataScopeControlDomain dataScopeControlDomain = densoUserInfoDomain
            .getDataScopeControlDomain();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getDscId())){
            densoUserInfoDomain.setDscId(densoUserInfoDomain.getDscId().trim());
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getEmployeeCode())){
            densoUserInfoDomain.setEmployeeCode(densoUserInfoDomain.getEmployeeCode().trim());
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getFirstName())){
            densoUserInfoDomain.setFirstName(densoUserInfoDomain.getFirstName().trim());
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getMiddleName())){
            densoUserInfoDomain.setMiddleName(densoUserInfoDomain.getMiddleName().trim());
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getLastName())){
            densoUserInfoDomain.setLastName(densoUserInfoDomain.getLastName().trim());
        }
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList =
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        if(Constants.ZERO < dataScopeControlDomain.getDensoSupplierRelationDomainList().size()){
            densoUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /*Get list of DENSO company information*/
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoList = companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == companyDensoList || companyDensoList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }else{
            densoUserInfoDomain.setCompanyDensoList(companyDensoList);
        }
        
        /*Get list of DENSO plant information*/
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(densoUserInfoDomain.getDCd());
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(Constants.ZERO < plantDensoList.size()){
            densoUserInfoDomain.setPlantDensoList(plantDensoList);
        }
        
        /*Get a number of records count by criteria value*/
        int maxRecord = userDensoService.searchCountUserDenso(densoUserInfoDomain);
        if(maxRecord <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.LBL_DENSO_USER);
        }
        
        /* Get Maximum record limit*/
        recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_RLM);
        recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
        if(null == recordsLimit){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Check max record*/
        if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{recordsLimit.getMiscValue()});
        }
        
        /* Get Maximum record per page limit*/
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Calculate Row number for query*/
        densoUserInfoDomain.setMaxRowPerPage(Integer.valueOf(
            recordLimitPerPage.getMiscValue()));
        densoUserInfoDomain.setPageNumber(densoUserInfoDomain.getPageNumber());
        SpsPagingUtil.calcPaging(densoUserInfoDomain, maxRecord);
        
        /*Get list of supplier user information*/
        resultList = userDensoService.searchUserDenso(densoUserInfoDomain);
        if(resultList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.LBL_DENSO_USER);
        }else{
            for(DensoUserInformationDomain densoUser : resultList){
                Date lastUpdateDatetime = new Date(
                    densoUser.getSpsMUserDensoDomain().getLastUpdateDatetime().getTime());
                densoUser.setLastUpdateDatetimeScreen(DateUtil.format(
                    lastUpdateDatetime, patternTimestamp));
            }
            densoUserInfoDomain.setUserDensoInfoList(resultList);
        }
        return densoUserInfoDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#searchSelectedCompanyDenso
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        DensoUserInformationDomain densoUserInfoDomain) throws ApplicationException {
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = densoUserInfoDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain = 
            densoUserInfoDomain.getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(densoUserInfoDomain.getDCd());
        plantDensoWithScopeDomain.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoWithScopeDomain.setDataScopeControlDomain(
            densoUserInfoDomain.getDataScopeControlDomain());
        
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if (plantDensoList.size() <= Constants.ZERO) {
            plantDensoList = new ArrayList<PlantDensoDomain>();
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScope.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == result || result.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#searchUserDenso(com.globaldenso.asia.sps.business.domain.densoUserInfoDomain)
     */
    public DensoUserInformationDomain searchUserDenso(
        DensoUserInformationDomain densoUserInformationDomain)throws ApplicationException
    {
        Locale locale = densoUserInformationDomain.getLocale();
        List<DensoUserInformationDomain> resultList = new ArrayList<DensoUserInformationDomain>();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        try{
            errorMessageList = validateInputValue(densoUserInformationDomain);
            
            /*Throw list of error message.*/
            if(Constants.ZERO < errorMessageList.size()){
                densoUserInformationDomain.setErrorMessageList(errorMessageList);
                return densoUserInformationDomain;
            }
            
            if(!StringUtil.checkNullOrEmpty(densoUserInformationDomain.getDscId())){
                densoUserInformationDomain.setDscId(densoUserInformationDomain.getDscId());
            }
            if(Constants.MISC_CODE_ALL.equals(densoUserInformationDomain.getDCd())){
                densoUserInformationDomain.setDCd(null);
            }else{
                densoUserInformationDomain.setDCd(densoUserInformationDomain.getDCd());
            }
            if(Constants.MISC_CODE_ALL.equals(densoUserInformationDomain.getDPcd())){
                densoUserInformationDomain.setDPcd(null);
            }else{
                densoUserInformationDomain.setDPcd(densoUserInformationDomain.getDPcd());
            }
            
            /*Get a number of records count by criteria value*/
            int maxRecord = userDensoService.searchCountUserDenso(densoUserInformationDomain);
            if(maxRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.LBL_DENSO_USER);
            }
            
            /* Get Maximum record limit*/
            recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_RLM);
            recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
            if(null == recordsLimit){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Check max record*/
            if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{recordsLimit.getMiscValue()});
            }
            
            /* Get Maximum record per page limit*/
            recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_PLM);
            recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Calculate Row number for query*/
            densoUserInformationDomain.setMaxRowPerPage(Integer.valueOf(
                recordLimitPerPage.getMiscValue()));
            densoUserInformationDomain.setPageNumber(densoUserInformationDomain.getPageNumber());
            SpsPagingUtil.calcPaging(densoUserInformationDomain, maxRecord);
            
            /*Get list of supplier user information*/
            resultList = userDensoService.searchUserDenso(densoUserInformationDomain);
            if(resultList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.LBL_DENSO_USER);
            }else{
                for(DensoUserInformationDomain densoUser : resultList){
                    Date lastUpdateDatetime = new Date(
                        densoUser.getSpsMUserDensoDomain().getLastUpdateDatetime().getTime());
                    densoUser.setLastUpdateDatetimeScreen(
                        DateUtil.format(lastUpdateDatetime, patternTimestamp));
                }
                densoUserInformationDomain.setUserDensoInfoList(resultList);
            }
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception ex){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return densoUserInformationDomain;
    }
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#deleteUserDenso
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public DensoUserInformationDomain deleteUserDenso(DensoUserInformationDomain 
        densoUserInformationDomain) throws ApplicationException
    {
        List<DensoUserInformationDomain> densoUserSelectedList = 
            densoUserInformationDomain.getUserDensoInfoList();
        List<SpsMUserCriteriaDomain> spsMUserCriteriaDomainList =
            new ArrayList<SpsMUserCriteriaDomain>(); 
        List<SpsMUserDomain> spsMUserDomainList = new ArrayList<SpsMUserDomain>();
        List<DensoUserInformationDomain> densoUserList = 
            new ArrayList<DensoUserInformationDomain>();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        List<SpsMUserRoleDomain> spsMUserRoleDomainList = new ArrayList<SpsMUserRoleDomain>();
        List<SpsMUserRoleCriteriaDomain> spsMUserRoleCriteriaDomainList =
            new ArrayList<SpsMUserRoleCriteriaDomain>(); 
        
        Locale locale = densoUserInformationDomain.getLocale();
        
        /*Get relation between DENSO and Supplier*/
//      DataScopeControlDomain dataScopeControlDomain
//          = densoUserInformationDomain.getDataScopeControlDomain();
//      DataScopeControlDomain dataScopeControlCriteria = new DataScopeControlDomain();
//      dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
//      dataScopeControlCriteria.setUserRoleDomainList(
//      dataScopeControlDomain.getUserRoleDomainList());
//      dataScopeControlCriteria.setDensoSupplierRelationDomainList(null);
        
        int countUser = 0;
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        try{
//            List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
//                densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlCriteria);
//            dataScopeControlDomain.setDensoSupplierRelationDomainList(
//                densoSupplierRelationDomainList);
//            if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() < Constants.ZERO){
//                MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
//                    SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
//            }
            
            /*set domain and criteria for update*/
            for(DensoUserInformationDomain densoUserSelected : densoUserSelectedList){
                if(!Strings.judgeBlank(densoUserSelected.getDscId())){
                    SpsMUserCriteriaDomain spsMUserCriteria = new SpsMUserCriteriaDomain();
                    spsMUserCriteria.setDscId(densoUserSelected.getDscId());
                    spsMUserCriteria.setIsActive(Constants.IS_ACTIVE);
                    countUser = spsMUserService.searchCount(spsMUserCriteria);
                    if(Constants.ZERO == countUser){
                        MessageUtil.throwsApplicationMessageWithLabel(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_DELETE_SELECTED_ITEMS);
                    }
                    Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(densoUserSelected
                        .getLastUpdateDatetimeScreen(), patternTimestamp);
                    
                    SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                    spsMUserDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                    spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    spsMUserDomain.setLastUpdateDscId(densoUserInformationDomain.getUpdateUser());
                    spsMUserDomainList.add(spsMUserDomain);
                    
                    spsMUserCriteria.setLastUpdateDatetime(lastUpdateDatetime);
                    spsMUserCriteriaDomainList.add(spsMUserCriteria);
                    
                    //Set criteria to SPS_M_USER_ROLE
                    SpsMUserRoleCriteriaDomain spsUserRoleCriteria = new 
                        SpsMUserRoleCriteriaDomain();
                    spsUserRoleCriteria.setDscId(densoUserSelected.getDscId());
                    spsUserRoleCriteria.setIsActive(Constants.IS_ACTIVE);
                    spsMUserRoleCriteriaDomainList.add(spsUserRoleCriteria);
                    
                    SpsMUserRoleDomain spsMUserRoleDomain = new SpsMUserRoleDomain();
                    spsMUserRoleDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                    spsMUserRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    spsMUserRoleDomain.setLastUpdateDscId(densoUserInformationDomain
                        .getUpdateUser());
                    spsMUserRoleDomainList.add(spsMUserRoleDomain);
                }
            }
            /*Update DENSO User Information*/
            if(!spsMUserCriteriaDomainList.isEmpty()){
                int updateRecord = spsMUserService.updateByCondition(spsMUserDomainList, 
                    spsMUserCriteriaDomainList);
                if(updateRecord != spsMUserCriteriaDomainList.size()){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0034, 
                        SupplierPortalConstant.LBL_DELETE_SELECTED_ITEMS);
                }else{
                    spsMUserRoleService.updateByCondition(spsMUserRoleDomainList,
                        spsMUserRoleCriteriaDomainList);
                }
            }else{
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0012,
                    SupplierPortalConstant.LBL_DELETE);
            }
            
            /*search for initial after delete*/
            if(Constants.MISC_CODE_ALL.equals(densoUserInformationDomain.getDCd())){
                densoUserInformationDomain.setDCd(null);
            }else{
                densoUserInformationDomain.setDCd(densoUserInformationDomain.getDCd());
            }
            if(Constants.MISC_CODE_ALL.equals(densoUserInformationDomain.getDPcd())){
                densoUserInformationDomain.setDPcd(null);
            }else{
                densoUserInformationDomain.setDPcd(densoUserInformationDomain.getDPcd());
            }
            densoUserInformationDomain.setIsActive(Constants.IS_ACTIVE);
            int maxRecord = userDensoService.searchCountUserDenso(densoUserInformationDomain);
            if(maxRecord <= Constants.ZERO){
                densoUserInformationDomain.setTotalCount(maxRecord);
                return densoUserInformationDomain;
            }
            
            /* Get Maximum record limit*/
            recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_RLM);
            recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
            if(null == recordsLimit){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Check max record*/
            if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{recordsLimit.getMiscValue()});
            }
            
            /* Get Maximum record per page limit*/
            recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_PLM);
            recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Calculate Row number for query*/
            densoUserInformationDomain.setMaxRowPerPage(Integer.valueOf(
                recordLimitPerPage.getMiscValue()));
            densoUserInformationDomain.setPageNumber(densoUserInformationDomain.getPageNumber());
            SpsPagingUtil.calcPaging(densoUserInformationDomain, maxRecord);
            
            /*Get list of DENSO user information*/
            densoUserList = userDensoService.searchUserDenso(densoUserInformationDomain);
            if(densoUserList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_USER);
            }else{
                for(DensoUserInformationDomain densoUser : densoUserList){
                    Date lastUpdateDatetime = new Date(
                        densoUser.getSpsMUserDensoDomain().getLastUpdateDatetime().getTime());
                    densoUser.setLastUpdateDatetimeScreen(
                        DateUtil.format(lastUpdateDatetime, patternTimestamp));
                }
                densoUserInformationDomain.setUserDensoInfoList(densoUserList);
            }
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception ex){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return densoUserInformationDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService#searchUserDensoCsv
com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public DensoUserInformationDomain searchUserDensoCsv(DensoUserInformationDomain 
        densoUserInformationDomain) throws ApplicationException
    {
        Locale locale = densoUserInformationDomain.getLocale();
        StringBuffer resultString = new StringBuffer();
        CommonDomain commonDomain = new CommonDomain();
        DensoUserInformationDomain resultDomain = new DensoUserInformationDomain();
        DensoUserInformationDomain densoUserCriteria = new DensoUserInformationDomain();
        List<DensoUserInformationDomain> userDensoInfoList
            = new ArrayList<DensoUserInformationDomain>();
        
//      DataScopeControlDomain dataScopeControlDomain
//          = densoUserInformationDomain.getDataScopeControlDomain();
//        DataScopeControlDomain dataScopeControlCriteria = new DataScopeControlDomain();
//        List<DensoUserInformationDomain> densoUserResultList
//            = new ArrayList<DensoUserInformationDomain>();
        
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        
        try{
            errorMessageList = validateInputValue(densoUserInformationDomain);
            /*Throw list of error message.*/
            if(Constants.ZERO < errorMessageList.size()){
                densoUserInformationDomain.setErrorMessageList(errorMessageList);
                return densoUserInformationDomain;
            }
            
            /*Get relation between DENSO and Supplier*/
            if(Constants.MISC_CODE_ALL.equals(densoUserInformationDomain.getDCd())){
                densoUserInformationDomain.setDCd(null);
            }else{
                densoUserInformationDomain.setDCd(densoUserInformationDomain.getDCd());
            }
            if(Constants.MISC_CODE_ALL.equals(densoUserInformationDomain.getDPcd())){
                densoUserInformationDomain.setDPcd(null);
            }else{
                densoUserInformationDomain.setDPcd(densoUserInformationDomain.getDPcd());
            }
            
//            dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
//            dataScopeControlCriteria.setUserRoleDomainList(dataScopeControlDomain
//                .getUserRoleDomainList());
//            dataScopeControlCriteria.setDensoSupplierRelationDomainList(null);
//            List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
//                densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlCriteria);
//            dataScopeControlDomain.setDensoSupplierRelationDomainList(
//                densoSupplierRelationDomainList);
//            if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO)
//            {
//                MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
//                    SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
//            }
            
            /*Set criteria values*/
            if(StringUtil.checkNullOrEmpty(densoUserInformationDomain.getDscId())){
                densoUserCriteria.setDscId(densoUserInformationDomain.getDscId());
            }else{
                densoUserCriteria.setDscId(densoUserInformationDomain.getDscId());
            }
            densoUserCriteria.setDCd(densoUserInformationDomain.getDCd());
            densoUserCriteria.setDPcd(densoUserInformationDomain.getDPcd());
            densoUserCriteria.setEmployeeCode(densoUserInformationDomain.getEmployeeCode());
            densoUserCriteria.setFirstName(densoUserInformationDomain.getFirstName());
            densoUserCriteria.setMiddleName(densoUserInformationDomain.getMiddleName());
            densoUserCriteria.setLastName(densoUserInformationDomain.getLastName());
            densoUserCriteria.setRegisterDateFrom(densoUserInformationDomain.getRegisterDateFrom());
            densoUserCriteria.setRegisterDateTo(densoUserInformationDomain.getRegisterDateTo());
            densoUserCriteria.setDensoAuthenList(densoUserInformationDomain.getDensoAuthenList());
            densoUserCriteria.setIsActive(Constants.IS_ACTIVE);
            
//            dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
//            dataScopeControlCriteria.setUserRoleDomainList(dataScopeControlDomain
//                .getUserRoleDomainList());
//            dataScopeControlCriteria.setDensoSupplierRelationDomainList(dataScopeControlDomain
//                .getDensoSupplierRelationDomainList());
//            densoUserCriteria.setDataScopeControlDomain(dataScopeControlCriteria);
            
            /*Get list of supplier user information*/
            int max = userDensoService.searchCountUserDensoIncludeRole(densoUserCriteria);
            densoUserCriteria.setRowNumFrom(Constants.ZERO);
            densoUserCriteria.setRowNumTo(max);
            
            /** Get maximum record limit from RecordLimitService. */
            MiscellaneousDomain recordLimit = new MiscellaneousDomain();
            recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
            recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM005_RLM);
            recordLimit = recordLimitService.searchRecordLimit(recordLimit);
            if(null == recordLimit){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /** Check maxRecord is over recordLimit. */
            if(Integer.valueOf(recordLimit.getMiscValue()) < max){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0020,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {recordLimit.getMiscValue()});
            }
            
            userDensoInfoList = userDensoService.searchUserDensoIncludeRole(densoUserCriteria);
            if(userDensoInfoList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I6_0006,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Create CSV String*/
            /*Validate*/
            for(DensoUserInformationDomain userDenso : userDensoInfoList){
                if(null != userDenso.getSpsMUserRoleDomain()){
                    if(!StringUtil.checkNullOrEmpty(
                        userDenso.getSpsMUserRoleDomain().getEffectStart())){
                        userDenso.setEffectStartScreen(DateUtil.format(
                            userDenso.getSpsMUserRoleDomain().getEffectStart(), pattern));
                    }else{
                        userDenso.setEffectStartScreen(Constants.EMPTY_STRING);
                    }
                    if(!StringUtil.checkNullOrEmpty(
                        userDenso.getSpsMUserRoleDomain().getEffectEnd())){
                        userDenso.setEffectEndScreen(DateUtil.format(
                            userDenso.getSpsMUserRoleDomain().getEffectEnd(), pattern));
                    }else{
                        userDenso.setEffectEndScreen(Constants.EMPTY_STRING);
                    }
                }
                userDenso.setRoleFlag(Constants.STR_N);
                userDenso.setInformationFlag(Constants.STR_N);
            }
            
            final String[] headerArr = new String[] {
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_DSC_ID),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EMPLOYEE_CODE),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_PCD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_DEPARTMENT_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_FIRST_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_MIDDLE_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_LAST_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_TELEPHONE),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EMAIL),
                MessageUtil.getReportLabel(locale,
                    SupplierPortalConstant.LBL_EML_CREATE_CANCEL_ASN),
                MessageUtil.getReportLabel(locale,
                    SupplierPortalConstant.LBL_EML_REVISE_SHIPPING_QTY),
                MessageUtil.getReportLabel(locale,
                    SupplierPortalConstant.LBL_EML_CREATE_INV_WITH_CN),
                MessageUtil.getReportLabel(locale,
                    SupplierPortalConstant.LBL_EML_ABNORMAL_TRANSFER_1),
                MessageUtil.getReportLabel(locale,
                    SupplierPortalConstant.LBL_EML_ABNORMAL_TRANSFER_2),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_D_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_D_PCD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EFFECT_START),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EFFECT_END),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_INFO_FLAG),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_FLAG)};
            
            List<Map<String, Object>> resultDetail = this
                .doMapDensoUserInfoList(userDensoInfoList, headerArr);
            
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            resultString = commonService.createCsvString(commonDomain);
            if(null == resultString){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012,  
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }else{
                resultDomain.setResultString(resultString);
            }
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception ex){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * 
     * <p>Validate input value.</p>
     *
     * @param densoUserInfoDomain that keep all input from the screen.
     * @return the List of validation error message
     * @throws Exception the Exception.
     */
    private List<ApplicationMessageDomain> validateInputValue(DensoUserInformationDomain 
        densoUserInfoDomain)throws Exception
    {
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = densoUserInfoDomain.getLocale();
        String message = new String();
        boolean isValidDateFrom = false;
        boolean isValidDateTo = false;
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        String dscId = Constants.EMPTY_STRING;
        String firstName = Constants.EMPTY_STRING;
        String middleName = Constants.EMPTY_STRING;
        String lastName = Constants.EMPTY_STRING;
        String registerDateFrom = Constants.EMPTY_STRING;
        String registerDateTo = Constants.EMPTY_STRING;
        String employeeCd = Constants.EMPTY_STRING;
        
        /*Validate criteria input for search*/
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getDscId())){
            dscId = densoUserInfoDomain.getDscId();
            if(Constants.MAX_DSC_ID_LENGTH < dscId.length()){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_DSC_ID),
                            String.valueOf(Constants.MAX_DSC_ID_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getEmployeeCode())){
            employeeCd = densoUserInfoDomain.getEmployeeCode();
            if(Constants.MAX_EMPLOYEE_CD_LENGTH < employeeCd.length()){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_EMPLOYEE_CODE),
                            String.valueOf(Constants.MAX_EMPLOYEE_CD_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getFirstName())){
            firstName = densoUserInfoDomain.getFirstName();
            if(Constants.MAX_FIRST_NAME_LENGTH < firstName.length()){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_FIRST_NAME),
                            String.valueOf(Constants.MAX_FIRST_NAME_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getMiddleName())){
            middleName = densoUserInfoDomain.getMiddleName();
            if(Constants.MAX_MIDDLE_NAME_LENGTH < middleName.length()){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_MIDDLE_NAME),
                            String.valueOf(Constants.MAX_MIDDLE_NAME_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getLastName())){
            lastName = densoUserInfoDomain.getLastName();
            if(Constants.MAX_LAST_NAME_LENGTH < lastName.length()){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_LAST_NAME),
                            String.valueOf(Constants.MAX_LAST_NAME_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getRegisterDateFrom())){
            registerDateFrom = densoUserInfoDomain.getRegisterDateFrom();
            if(!DateUtil.isValidDate(registerDateFrom, pattern)){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_REGISTER_DATE_FROM)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getRegisterDateTo())){
            registerDateTo = densoUserInfoDomain.getRegisterDateTo();
            if(!DateUtil.isValidDate(registerDateTo, pattern)){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_REGISTER_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getRegisterDateFrom())
            && !StringUtil.checkNullOrEmpty(densoUserInfoDomain.getRegisterDateTo())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO < DateUtil.compareDate(densoUserInfoDomain.getRegisterDateFrom(), 
                densoUserInfoDomain.getRegisterDateTo())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_REGISTER_DATE_FROM),
                        MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_REGISTER_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
        }
        if(StringUtil.checkNullOrEmpty(dscId)
            && StringUtil.checkNullOrEmpty(employeeCd)
            && StringUtil.checkNullOrEmpty(densoUserInfoDomain.getDCd())
            && StringUtil.checkNullOrEmpty(densoUserInfoDomain.getDPcd())
            && StringUtil.checkNullOrEmpty(firstName)
            && StringUtil.checkNullOrEmpty(middleName)
            && StringUtil.checkNullOrEmpty(lastName)
            && StringUtil.checkNullOrEmpty(registerDateTo)
            && StringUtil.checkNullOrEmpty(registerDateFrom)){
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0011);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
        }else{
            if(StringUtil.checkNullOrEmpty(densoUserInfoDomain.getDCd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_DENSO_COMPANY_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(densoUserInfoDomain.getDPcd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_DENSO_PLANT_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        return errorMessageList;
    }
    
    /**
     * Do Map DENSO User Information List.
     * 
     * @param densoUserList the DENSO User List
     * @param header the List
     * @return resultList 
     */
    private List<Map<String, Object>> doMapDensoUserInfoList(List<DensoUserInformationDomain>
    densoUserList, String[] header){
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> densoUserMap = null;
        StringBuffer email = new StringBuffer();
        for(DensoUserInformationDomain densoUser : densoUserList){
            densoUserMap = new HashMap<String, Object>();
            email = new StringBuffer();
            if(!StringUtil.checkNullOrEmpty(densoUser.getUserDomain().getEmail())){
                email.append(Constants.SYMBOL_DOUBLE_QUOTE).append(densoUser.getUserDomain()
                    .getEmail()).append(Constants.SYMBOL_DOUBLE_QUOTE);
            }
            
            densoUserMap.put(header[Constants.ZERO], densoUser.getSpsMUserDensoDomain().getDscId());
            densoUserMap.put(header[Constants.ONE],
                densoUser.getSpsMUserDensoDomain().getEmployeeCd());
            densoUserMap.put(header[Constants.TWO], StringUtil.checkNullToEmpty(
                densoUser.getSpsMUserDensoDomain().getDCd()));
            densoUserMap.put(header[Constants.THREE], StringUtil.checkNullToEmpty(
                densoUser.getSpsMUserDensoDomain().getDPcd()));
            densoUserMap.put(header[Constants.FOUR], StringUtil.checkNullToEmpty(
                densoUser.getUserDomain().getDepartmentName()));
            densoUserMap.put(header[Constants.FIVE], densoUser.getUserDomain().getFirstName());
            densoUserMap.put(header[Constants.SIX], StringUtil.checkNullToEmpty(
                densoUser.getUserDomain().getMiddleName()));
            densoUserMap.put(header[Constants.SEVEN], densoUser.getUserDomain().getLastName());
            densoUserMap.put(header[Constants.EIGHT], StringUtil.checkNullToEmpty(
                densoUser.getUserDomain().getTelephone()));
            densoUserMap.put(header[Constants.NINE], email.toString());
            densoUserMap.put(header[Constants.TEN], densoUser.getSpsMUserDensoDomain()
                .getEmlCreateCancelAsnFlag());
            densoUserMap.put(header[Constants.ELEVEN], densoUser.getSpsMUserDensoDomain()
                .getEmlReviseAsnFlag());
            densoUserMap.put(header[Constants.TWELVE], densoUser.getSpsMUserDensoDomain()
                .getEmlCreateInvoiceFlag());

            // Start : [IN049] Change Column Name
            //densoUserMap.put(header[Constants.THIRTEEN], densoUser.getSpsMUserDensoDomain()
            //    .getEmlAbnormalTransfer1Flag());
            //densoUserMap.put(header[Constants.FOURTEEN], densoUser.getSpsMUserDensoDomain()
            //    .getEmlAbnormalTransfer2Flag());
            densoUserMap.put(header[Constants.THIRTEEN], densoUser.getSpsMUserDensoDomain()
                .getEmlPedpoDoalcasnUnmatpnFlg());
            densoUserMap.put(header[Constants.FOURTEEN], densoUser.getSpsMUserDensoDomain()
                .getEmlAbnormalTransferFlg());
            // End : [IN049] Change Column Name
            
            if(null != densoUser.getSpsMUserRoleDomain()){
                densoUserMap.put(header[Constants.FIFTEEN], StringUtil.checkNullToEmpty(
                    densoUser.getSpsMUserRoleDomain().getRoleCd()));
                densoUserMap.put(header[Constants.SIXTEEN], StringUtil.checkNullToEmpty(
                    densoUser.getSpsMUserRoleDomain().getDCd()));
                densoUserMap.put(header[Constants.SEVENTEEN], StringUtil.checkNullToEmpty(
                    densoUser.getSpsMUserRoleDomain().getDPcd()));
            }else{
                densoUserMap.put(header[Constants.FIFTEEN], Constants.EMPTY_STRING);
                densoUserMap.put(header[Constants.SIXTEEN], Constants.EMPTY_STRING);
                densoUserMap.put(header[Constants.SEVENTEEN], Constants.EMPTY_STRING);
            }
            densoUserMap.put(header[Constants.EIGHTEEN], StringUtil.nullToEmpty(
                densoUser.getEffectStartScreen()));
            densoUserMap.put(header[Constants.NINETEEN], StringUtil.nullToEmpty(
                densoUser.getEffectEndScreen()));
            densoUserMap.put(header[Constants.TWENTY], densoUser.getRoleFlag());
            densoUserMap.put(header[Constants.TWENTY_ONE], densoUser.getInformationFlag());
            resultList.add(densoUserMap);
        }
        return resultList;
    }

}