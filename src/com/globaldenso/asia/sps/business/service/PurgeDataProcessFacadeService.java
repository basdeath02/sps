/*
 * ModifyDate Development company     Describe 
 * 2014/08/26 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;

/**
 * <p>The Interface PurgeDataProcessFacadeService.</p>
 * <p>Facade for PurgeDataProcessFacadeService.</p>
 * <ul>
 * <li>Method search   : searchDensoCompanyByCodeList</li>
 * <li>Method transact : transactPurgeSystemData</li>
 * </ul>
 *
 * @author CSI
 */
public interface PurgeDataProcessFacadeService {
    
    /**
     * <p>Search DENSO company by code list.</p>
     * <ul>
     * <li>Search DENSO company by code list for purge data process batch.</li>
     * </ul>
     * 
     * @param densoCompanyCode the String
     * @return the list of SPS Master Company DENSO Domain
     */
    public List<SpsMCompanyDensoDomain> searchDensoCompanyByCodeList(String densoCompanyCode);
    
    /**
     * <p>Search all purging order by company.</p>
     * <ul>
     * <li>Search all purging order by company for purge data process batch.</li>
     * </ul>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @return the list of SPS Master Company DENSO Domain
     * @throws ApplicationException ApplicationException
     */
    public List<PurgingPoDomain> searchAllPurgingOrderByCompany(
        SpsMCompanyDensoDomain spsMCompanyDenso)throws ApplicationException;
    
    /**
     * <p>Search transact purge system data.</p>
     * <ul>
     * <li>Purging system data for purge data process batch.</li>
     * </ul>
     * 
     * @param poItem the Purging PO Domain
     * @param parentJobId the String
     * @throws ApplicationException ApplicationException
     * @throws Exception Exception
     */
    public void transactPurgeSystemData(PurgingPoDomain poItem, String parentJobId)
        throws ApplicationException, Exception;
    
    /**
     * <p>Search all purging invoice by company.</p>
     * <ul>
     * * <li>Search invoice by code list for purge data process batch.</li>
     * </ul>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @return the list of SPS Transaction Invoice Domain
     * @throws ApplicationException ApplicationException
     */
    public List<SpsTInvoiceDomain> searchAllPurgingInvoiceByCompany(
        SpsMCompanyDensoDomain spsMCompanyDenso) throws ApplicationException;
    
    /**
     * <p>Transact purge invoice data.</p>
     * <ul>
     * * <li>Purging invoice data</li>
     * </ul>
     * 
     * @param invoiceItem the SPS transaction invoice Domain
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @param parentJobId the String
     * @throws ApplicationException ApplicationException
     * @throws Exception Exception
     */
    public void transactPurgeInvoiceData(
        SpsTInvoiceDomain invoiceItem, SpsMCompanyDensoDomain spsMCompanyDenso, String parentJobId)
        throws ApplicationException, Exception;
    
    /**
     * <p>Transact Purge Invoice Cover Page Run No Data.</p>
     * <ul>
     * * <li>Purging invoice cover page run no data</li>
     * </ul>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @throws ApplicationException ApplicationException
     */
    public void transactPurgeInvoiceCoverPageRunNoData(SpsMCompanyDensoDomain spsMCompanyDenso)
        throws ApplicationException;

    /**
     * <p>Transact Purge Data.</p>
     * <ul>
     * * <li>Purging data</li>
     * </ul>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @param parentJobId the string parentJobId
     * @throws ApplicationException ApplicationException
     * @throws Exception Exception
     */
    public void transactPurgeData(SpsMCompanyDensoDomain spsMCompanyDenso, String parentJobId)
        throws ApplicationException, Exception;
}