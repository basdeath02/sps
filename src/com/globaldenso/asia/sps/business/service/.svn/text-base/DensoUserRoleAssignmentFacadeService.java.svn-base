/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface DensoUserRoleAssignmentFacadeService.</p>
 * <p>Service for DENSO User Role about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchInitialPopup</li>
 * <li>Method insert  : searchSelectedCompanyDenso</li>
 * <li>Method update  : searchSelectedPlantDenso</li>
 * <li>Method update  : deleteUserRole</li>
 * <li>Method update  : createUserRole</li>
 * <li>Method update  : updateUserRole</li>
 * <li>Method update  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoUserRoleAssignmentFacadeService {
    
    /**
     * <p>Initial DENSO User Role Detail.</p>
     * <ul>
     * <li>Search DENSO User Role Item Detail for display on DENSO User Role screen.</li>
     * </ul>
     * 
     * @param densoUserRoleAssignDomain that Keep target user DSC ID and
     * data scope control for get data to show on screen.
     * @return DensoUserRoleAssignDomain that Keep a target user information with assigned roles.
     * @throws ApplicationException ApplicationException
     */
    public DensoUserRoleAssignDomain searchInitial(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain) throws ApplicationException;
   
    /**
     * <p>Initial DENSO User Role Detail.</p>
     * <ul>
     * <li>Search DENSO User Role Item Detail for display on DENSO User Role Pop-up screen.</li>
     * </ul>
     * 
     * @param densoUserRoleAssignDomain that Keep data scope to get data to show on screen.
     * @return DensoUserRoleAssignDomain that Keep all combo box values to show on screen.
     * @throws ApplicationException ApplicationException
     */
    public DensoUserRoleAssignDomain searchInitialPopup(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain) throws ApplicationException;
    
    /**
     * <p>Change select DENSO company.</p>
     * <ul>
     * <li>Change select DENSO company.</li>
     * </ul>
     * 
     * @return the List of DENSO plant to show in combo box.
     * @param densoUserRoleAssignDomain that  keep criteria and scope of data 
     * for search DENSO plant information.
     * @throws ApplicationException the ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        DensoUserRoleAssignDomain densoUserRoleAssignDomain)throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Delete DENSO User Role</p>
     * <ul>
     * <li>Delete DENSO User Role by criteria.</li>
     * </ul>
     * 
     * @param densoUserRoleAssignDomain that keep selected user role to delete.
     * @throws ApplicationException the ApplicationException.
     */
    public void deleteUserRole(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain)throws ApplicationException;
    
    /**
     * <p>Create DENSO User Role.</p>
     * <ul>
     * <li>create detail DENSO User Role data.
     * then click OK button.</li>
     * </ul>
     * 
     * @param densoUserRoleAssignDomain that keep role information to insert to the system.
     * @return densoUserRoleAssignDomain that keep list of error message.
     * @throws ApplicationException the ApplicationException.
     */
    public DensoUserRoleAssignDomain createUserRole(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain)throws ApplicationException;
    
    /**
     * <p>update DENSO User Role.
     * <ul>
     * <li>update DENSO User Role data.
     * then click OK button.</li>
     * </ul>
     * 
     *  @param densoUserRoleAssignDomain that keep role information to update to the system.
     * @return densoUserRoleAssignDomain that keep list of error message.
     * @throws ApplicationException the ApplicationException.
     */
    public DensoUserRoleAssignDomain updateUserRole(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain)throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}