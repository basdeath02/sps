/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 2015/08/04 CSI Akat                [IN009]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceCoverPageDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.dao.InvoiceDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>The Class InvoiceServiceImpl.</p>
 * <p>Service for invoice about search and update invoice status.</p>
 * <ul>
 * <li>Method search  : searchCountInvoiceInformation</li>
 * <li>Method search  : searchInvoiceInformation</li>
 * <li>Method create  : createInvoice</li>
 * <li>Method search  : searchInvoiceCoverPage</li>
 * <li>Method search  : searchInvoiceByAsn</li>
 * <li>Method search  : searchPurgingInvoice</li>
 * <li>Method search  : searchInvoiceForTransferToJde</li>
 * </ul>
 *
 * @author CSI
 */
public class InvoiceServiceImpl implements InvoiceService {
    
    /** The invoice dao. */
    private InvoiceDao invoiceDao;

    /**
     * Instantiates a new invoice service impl.
     */
    public InvoiceServiceImpl(){
        super();
    }
    
    /**
     * Sets the invoice dao.
     * 
     * @param invoiceDao the invoice dao.
     */
    public void setInvoiceDao(InvoiceDao invoiceDao) {
        this.invoiceDao = invoiceDao;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceService#searchCountInvoiceInformation(com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public Integer searchCountInvoiceInformation(InvoiceInformationDomain invoiceInformationDomain){
        
        Integer rowCount = Constants.ZERO;
        rowCount = this.invoiceDao.searchCountInvoiceInformation(invoiceInformationDomain);
        return rowCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceService#searchInvoiceInformation(com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public List<InvoiceInformationDomain> searchInvoiceInformation(
        InvoiceInformationDomain invoiceInformationDomain){
        
        List<InvoiceInformationDomain> result = null;
        
        result = this.invoiceDao.searchInvoiceInformation(invoiceInformationDomain);
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        for(InvoiceInformationDomain item:result){
            int scale = 0;
            if(!StringUtil.checkNullOrEmpty(item.getDecimalDisp())){
                scale = Integer.valueOf(item.getDecimalDisp());
            }
            decimalFormat.setMinimumFractionDigits(scale);
            
            item.setInvoiceDate(
                DateUtil.format(item.getSpsTInvoiceDomain().getInvoiceDate(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            
            // [IN009] Rounding Mode use HALF_UP
            //item.setBaseAmount(
            //    decimalFormat.format(item.getSpsTInvoiceDomain().getSBaseAmount()));
            //item.setVatAmount(
            //    decimalFormat.format(item.getSpsTInvoiceDomain().getSVatAmount()));
            //item.setTotalAmount(
            //    decimalFormat.format(item.getSpsTInvoiceDomain().getSTotalAmount()));
            item.setBaseAmount(NumberUtil.formatRoundingHalfUp(
                decimalFormat, item.getSpsTInvoiceDomain().getSBaseAmount(), scale));
            item.setVatAmount(NumberUtil.formatRoundingHalfUp(
                decimalFormat, item.getSpsTInvoiceDomain().getSVatAmount(), scale));
            item.setTotalAmount(NumberUtil.formatRoundingHalfUp(
                decimalFormat, item.getSpsTInvoiceDomain().getSTotalAmount(), scale));
            
            if(!StringUtil.checkNullOrEmpty(item.getSpsTInvoiceDomain().getPaymentDate())){
                item.setPaymentDate(
                    DateUtil.format(item.getSpsTInvoiceDomain().getPaymentDate(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
            }
            if(!StringUtil.checkNullOrEmpty(item.getSpsTCnDomain().getCnDate())){
                item.setCnDate(
                    DateUtil.format(item.getSpsTCnDomain().getCnDate(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
            }
        }
        
        return result;
    } 
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceService#createInvoice(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain)
     */
    public Integer createInvoice(SpsTInvoiceDomain spsTInvoiceDomain){
        
        Integer invoiceId = null;
        invoiceId = this.invoiceDao.createInvoice(spsTInvoiceDomain);
        return invoiceId;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceService#searchInvoiceCoverPage(com.globaldenso.asia.sps.auto.business.domain.InvoiceCoverPageDomain)
     */
    public List<InvoiceCoverPageDomain> searchInvoiceCoverPage(
        InvoiceCoverPageDomain invoiceCoverPageDomain){
        
        List<InvoiceCoverPageDomain> result = null;
        
        result = this.invoiceDao.searchInvoiceCoverPage(invoiceCoverPageDomain);
        return result;
    } 

    // [IN012] ASN can create by many Invoice (in case cancel invoice)
    ///**
    // * {@inheritDoc}
    // * @see com.globaldenso.asia.sps.business.service.InvoiceService#searchInvoiceByAsn(com.globaldenso.asia.sps.auto.business.domain.PurgingAsnDomain)
    // */
    //public SpsTInvoiceDomain searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain){
    //    SpsTInvoiceDomain spsTInvoiceDomain = null;
    //    spsTInvoiceDomain = this.invoiceDao.searchInvoiceByAsn(purgingAsnDomain);
    //    return spsTInvoiceDomain;
    //} 
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceService#searchInvoiceByAsn(com.globaldenso.asia.sps.auto.business.domain.PurgingAsnDomain)
     */
    public List<SpsTInvoiceDomain> searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain){
        List<SpsTInvoiceDomain> invoiceList = null;
        invoiceList = this.invoiceDao.searchInvoiceByAsn(purgingAsnDomain);
        return invoiceList;
    } 
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceService#searchPurgingInvoice(com.globaldenso.asia.sps.business.domain.PurgingPODomain)
     */
    public List<SpsTInvoiceDomain> searchPurgingInvoice(PurgingPoDomain purgingPoDomain){
        
        List<SpsTInvoiceDomain> spsTInvoiceList = null;
        spsTInvoiceList = this.invoiceDao.searchPurgingInvoice(purgingPoDomain);
        return spsTInvoiceList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceService#searchInvoiceForTransferToJde(com.globaldenso.asia.sps.business.domain.SpsTInvoiceCriteriaDomain)
     */
    public List<InvoiceInformationDomain> searchInvoiceForTransferToJde(
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain)
    {
        List<InvoiceInformationDomain> invoiceInfoList = null;
        invoiceInfoList = this.invoiceDao.searchInvoiceForTransferToJde(spsTInvoiceCriteriaDomain);
        return invoiceInfoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceService#deletePurgingInvoice(String)
     */
    public int deletePurgingInvoice(String invoiceId){
        return this.invoiceDao.deletePurgingInvoice(invoiceId);
    }
}