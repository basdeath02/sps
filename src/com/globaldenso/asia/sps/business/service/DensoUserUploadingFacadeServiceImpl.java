/*
 * ModifyDate Development company     Describe 
 * 2014/07/30 CSI Phakaporn           Create
 * 2014/08/15 CSI Phakaporn           Modified method
 * 2016/02/10 CSI Akat                [IN049]
 * 
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain;
import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.TmpUserDensoDomain;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * <p>The Interface DensoUserUploadingFacadeServiceImpl.</p>
 * <p>Service for Supplier User Uploading about manipulate data form CSV file.</p>
 * <ul>
 * <li>Method search : initial</li>
 * <li>Method search : searchUploadErrorList</li>
 * <li>Method insert : transactUploadDensoUser</li>
 * <li>Method insert : transactRegisterUploadItem</li>
 * <li>Method search : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public class DensoUserUploadingFacadeServiceImpl implements DensoUserUploadingFacadeService {
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The Temporary Upload Error service.*/
    private SpsTmpUploadErrorService spsTmpUploadErrorService;
    
    /** The Company Supplier service.*/
    private CompanyDensoService companyDensoService;
    
    /** The Plant Supplier service.*/
    private PlantDensoService  plantDensoService;
    
    /** The user role service.*/
    private SpsMUserRoleService spsMUserRoleService;
    
    /** The user service.*/
    private SpsMUserService spsMUserService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The Temporary Upload Error service. */
    private TmpUploadErrorService tmpUploadErrorService;
    
    /** The Role service. */
    private SpsMRoleService spsMRoleService;
    
    /** The Temporary Upload User DENSO service. */
    private TmpUploadUserDensoService tmpUploadUserDensoService;
    
    /** The Company DENSO service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The Plant DENSO service. */
    private SpsMPlantDensoService spsMPlantDensoService;
    
    /** The User DENSO service. */
    private SpsMUserDensoService spsMUserDensoService;
    
    /** The Temporary User DENSO service. */
    private SpsTmpUserDensoService spsTmpUserDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /**
     * Instantiates a new DENSO User Uploading Facade service impl.
     */
    public DensoUserUploadingFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
 
    /**
     * Sets the Temporary Upload Error Service.
     * 
     * @param tmpUploadErrorService the Temporary Upload Error Service.
     */ 
    public void setTmpUploadErrorService(
        TmpUploadErrorService tmpUploadErrorService) {
        this.tmpUploadErrorService = tmpUploadErrorService;
    }

    /**
     * Sets the Temporary Upload Error Service.
     * 
     * @param spsTmpUploadErrorService the Temporary Upload Error Service.
     */ 
    public void setSpsTmpUploadErrorService(
        SpsTmpUploadErrorService spsTmpUploadErrorService) {
        this.spsTmpUploadErrorService = spsTmpUploadErrorService;
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service.
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    /**
     * Set the Company DENSO service.
     * 
     * @param companyDensoService the Company DENSO service.
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * Set the plant DENSO service.
     * 
     * @param spsMPlantDensoService the plant DENSO service.
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }
    /**
     * Set the user role service.
     * 
     * @param spsMUserRoleService the user role service.
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }
    /**
     * Set the user service.
     * 
     * @param spsMUserService the user service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    
    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    /**
     * Sets the role service.
     * 
     * @param spsMRoleService the new role service.
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * Sets the Company DENSO service.
     * 
     * @param spsMCompanyDensoService the new Company DENSO service.
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    /**
     * Set the plant DENSO service.
     * 
     * @param plantDensoService the plant DENSO service.
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    /**
     * Set the User DENSO service.
     * 
     * @param spsMUserDensoService the User DENSO service.
     */
    public void setSpsMUserDensoService(SpsMUserDensoService spsMUserDensoService) {
        this.spsMUserDensoService = spsMUserDensoService;
    }

    /**
     * Set the User DENSO service.
     * 
     * @param spsTmpUserDensoService the User DENSO service.
     */
    public void setSpsTmpUserDensoService(
        SpsTmpUserDensoService spsTmpUserDensoService) {
        this.spsTmpUserDensoService = spsTmpUserDensoService;
    }
    /**
     * Set the Upload User DENSO service.
     * 
     * @param tmpUploadUserDensoService the Upload User DENSO service.
     */
    public void setTmpUploadUserDensoService(
        TmpUploadUserDensoService tmpUploadUserDensoService) {
        this.tmpUploadUserDensoService = tmpUploadUserDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeServiceImpl#searchInitial
     * (com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain)
     */
    public void transactInitial(DensoUserUploadingDomain densoUserUploadingDomain) 
        throws ApplicationException {
        String userDscId = densoUserUploadingDomain.getUserDscId();
        String sessionCode = densoUserUploadingDomain.getSessionCode();

        /*Call method deleteUploadTempTable for clear all data in temporary table*/
        deleteUploadTempTable(userDscId, sessionCode);

    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeServiceImpl#transactUploadDensoUser
     * (com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain)
     */
    public DensoUserUploadingDomain transactUploadDensoUser(DensoUserUploadingDomain 
        densoUserUploadingDomain) throws ApplicationException, IOException
    {
        DataScopeControlDomain dataScopeControlDomain = densoUserUploadingDomain
            .getDataScopeControlDomain();
        FileManagementDomain fileManagementDomain = densoUserUploadingDomain
            .getFileManagementDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        boolean dscIdValidateFlag = false;
        boolean roleCodeValidateFlag = false;
        boolean roleDensoCompanyCodeValidateflag = false;
        boolean roleDensoPlantCodeValidateFlag = false;
        boolean informationValidationComplete = true;
        boolean roleValidationComplete = true;
        boolean isExist = false;
//        Boolean isNumber = false;
        boolean effectStartValidateFlag = false;
        boolean effectEndValidateFlag = false;
        boolean isExistDensoCompany = false;
        boolean isExistRoleDensoCompany = false;
        boolean returnValue = false;
//        Boolean isLetter = false;
        String line = new String();
        String companyDensoCode = new String();
        String plantDensoCode = new String();
        String dscId = new String();
        String roleCode = new String();
        String isActive = new String();
        String userDscId = densoUserUploadingDomain.getUserDscId();
        String sessionCd = densoUserUploadingDomain.getSessionCode();
        int lineNo = Constants.ZERO;
        String errorCode = new String();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Timestamp currentTimestamp = commonService.searchSysDate();
        Date currentDate = new Date(currentTimestamp.getTime());
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferReader = null;
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        try {
            /*Get each record and validate the content */
            inputStream = fileManagementDomain.getFileData();
            inputStream = FileUtil.checkUtf8BOMAndSkip(inputStream);
            inputStreamReader = new InputStreamReader(inputStream);
            bufferReader = new BufferedReader(inputStreamReader);
            
            /*LOOP each CSV record until end of file*/
            while(!StringUtil.checkNullOrEmpty(line = bufferReader.readLine())){
                lineNo++;
                dscIdValidateFlag = false;
                roleCodeValidateFlag   =  false;
                roleDensoCompanyCodeValidateflag  =  false;
                roleDensoPlantCodeValidateFlag = false;
                informationValidationComplete    = true;
                roleValidationComplete =   true;
                isExist = false;
//                isNumber = false;
                effectStartValidateFlag = false;
                effectEndValidateFlag = false;
                isExistDensoCompany = false;
                isExistRoleDensoCompany = false;
                
                List<String> splitItem = StringUtil.splitCsvData(line, Constants.TWENTY_TWO);
                splitItem.set(Constants.ZERO,
                    splitItem.get(Constants.ZERO).toUpperCase()); //Dsc Id
                splitItem.set(Constants.ONE,
                    splitItem.get(Constants.ONE).toUpperCase()); //Employee Code
                splitItem.set(Constants.TWO,
                    splitItem.get(Constants.TWO).toUpperCase()); //DENSO Company Code
                splitItem.set(Constants.THREE,
                    splitItem.get(Constants.THREE).toUpperCase()); //DENSO Plant Code
                splitItem.set(Constants.FOUR,
                    splitItem.get(Constants.FOUR).toUpperCase()); //Department Name
                splitItem.set(Constants.FIVE,
                    splitItem.get(Constants.FIVE).toUpperCase()); //First Name
                splitItem.set(Constants.SIX,
                    splitItem.get(Constants.SIX).toUpperCase()); //Middle Name
                splitItem.set(Constants.SEVEN,
                    splitItem.get(Constants.SEVEN).toUpperCase()); //Last Name
                splitItem.set(Constants.EIGHT,
                    splitItem.get(Constants.EIGHT).toUpperCase()); //Telephone
                splitItem.set(Constants.NINE,
                    splitItem.get(Constants.NINE).toUpperCase()); //Email
                splitItem.set(Constants.TEN,
                    splitItem.get(Constants.TEN).toUpperCase()); //Create ASN(Short Ship) & Cancel ASN
                splitItem.set(Constants.ELEVEN,
                    splitItem.get(Constants.ELEVEN).toUpperCase()); //Revise Shipping Qty
                splitItem.set(Constants.TWELVE,
                    splitItem.get(Constants.TWELVE).toUpperCase()); //Create Invoice with CN
                splitItem.set(Constants.THIRTEEN,
                    splitItem.get(Constants.THIRTEEN).toUpperCase()); //Pending PO, DO Create ASN, Supp Info not Found
                splitItem.set(Constants.FOURTEEN,
                    splitItem.get(Constants.FOURTEEN).toUpperCase()); //AB Normal Data Transfer
                splitItem.set(Constants.FIFTEEN,
                    splitItem.get(Constants.FIFTEEN).toUpperCase()); //Role Code
                splitItem.set(Constants.SIXTEEN,
                    splitItem.get(Constants.SIXTEEN).toUpperCase()); //Role DENSO Code
                splitItem.set(Constants.SEVENTEEN,
                    splitItem.get(Constants.SEVENTEEN).toUpperCase()); //Role DENSO Plant Code
                splitItem.set(Constants.EIGHTEEN,
                    splitItem.get(Constants.EIGHTEEN).toUpperCase()); //Effective Start Date
                splitItem.set(Constants.NINETEEN,
                    splitItem.get(Constants.NINETEEN).toUpperCase()); //Effective End Date
                splitItem.set(Constants.TWENTY,
                    splitItem.get(Constants.TWENTY).toUpperCase()); //Information Flag
                splitItem.set(Constants.TWENTY_ONE,
                    splitItem.get(Constants.TWENTY_ONE).toUpperCase()); //Role Flag
                
                /**** Check number of record elements*/
                if(Constants.TWENTY_TWO != splitItem.size()){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR001;
                    createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                    informationValidationComplete = false;
                    continue;
                }

                /**** Check information flag missing code*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWENTY))
                    && !Constants.STR_A.equals(splitItem.get(Constants.TWENTY))
                    && !Constants.STR_U.equals(splitItem.get(Constants.TWENTY))
                    && !Constants.STR_D.equals(splitItem.get(Constants.TWENTY))
                    && !Constants.STR_N.equals(splitItem.get(Constants.TWENTY))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR039;
                    createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                    informationValidationComplete = false;
                }

                /*Check information flag "N" value*/
                if(!(Constants.STR_N.equals(splitItem.get(Constants.TWENTY)))){

                    /**** Check NULL  DSC ID*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                        /**** Check DSC ID length*/
                        if(Constants.MAX_DSC_ID_LENGTH < splitItem.get(Constants.ZERO).length())
                        {
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR003;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }

                        /**Changed DSC ID Validation refer to 23/10/2014 12:00 am.*/
                        /**** Check DSC ID in numeric format*//*
                            isNumber = splitItem.get(Constants.ZERO).matches(
                                Constants.REGX_NUMBER_FORMAT);
                            if(!isNumber){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR006;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                informationValidationComplete = false;
                            }*/

                        /**** Check DSC ID duplicate (Information flag is "A")*/
                        if(Constants.STR_A.equals(splitItem.get(Constants.TWENTY))){
                            /**** Check existing user by DSC ID in master data*/
                            dscId = splitItem.get(Constants.ZERO);
                            isExist = searchExistUser(dscId);
                            if(isExist){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR004;
                                createItemUploadError(densoUserUploadingDomain, errorCode, 
                                    lineNo);
                                informationValidationComplete = false;
                            }else{
                                dscIdValidateFlag = true;
                            }
                        }

                        /**** Check DSC ID  not found in system (Information flag is "U")*/
                        if(Constants.STR_U.equals(splitItem.get(Constants.TWENTY))){
                            /**** Check existing user by DSC ID in master data */
                            dscId = splitItem.get(Constants.ZERO);
                            isExist = searchExistUser(dscId);
                            if(!isExist){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                createItemUploadError(densoUserUploadingDomain, errorCode,
                                    lineNo);
                                informationValidationComplete = false;
                            }else{
                                dscIdValidateFlag = true;
                            }
                        }

                        /**** Check DSC ID  not found in system (Information flag is "D")*/
                        if(Constants.STR_D.equals(splitItem.get(Constants.TWENTY))){
                            /**** Check existing user by DSC ID in master data */
                            dscId = splitItem.get(Constants.ZERO);
                            isExist = searchExistUser(dscId);
                            if(!isExist){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                informationValidationComplete = false;
                            }else{
                                dscIdValidateFlag = true;
                            }
                        }
                        
                        //Check Duplicate DSC ID in temp
                        if(dscIdValidateFlag){
                            SpsTmpUserDensoCriteriaDomain criteria
                                = new SpsTmpUserDensoCriteriaDomain();
                            int countRecord = 0;
                            criteria.setDscId(dscId);
                            criteria.setUserDscId(userDscId);
                            criteria.setSessionCd(sessionCd);
                            countRecord = spsTmpUserDensoService.searchCount(criteria);
                            if(Constants.ZERO < countRecord){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR004;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                informationValidationComplete = false;
                                dscIdValidateFlag = false;
                            }
                        }
                        
                    }else if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR002;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }
                    
                    /** Check NULL Employee code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ONE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR057;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }else{
                        /**** Check Employee code length*/
                        if(Constants.MAX_EMPLOYEE_CD_LENGTH < splitItem.get(Constants.ONE).length())
                        {
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR058;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }
                    }
                    
                    /**** Check NULL DENSO company code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR059;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check DENSO company code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                        && Constants.FIVE < splitItem.get(Constants.TWO).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR060;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check DENSO company code*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                        && splitItem.get(Constants.TWO).length() <= Constants.FIVE){
                        /**** Check existing DENSO company in system*/
                        companyDensoCode = StringUtil.addSpace(splitItem.get(Constants.TWO),
                            Constants.FIVE);
                        isExistDensoCompany = searchExistDensoCompany(
                            companyDensoCode);
                        if(isExistDensoCompany){
                            /**Check existing DENSO company in system by current user data scope*/
                            companyDensoCode = StringUtil.addSpace(splitItem.get(Constants.TWO),
                                Constants.FIVE);
                            boolean isExistDensoCompanyByScope = 
                                searchExistDensoCompanyWithScope(companyDensoCode,
                                    dataScopeControlDomain);
                            /** Current user not have a right to working on this DENSO company.*/
                            if(!isExistDensoCompanyByScope){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR062;
                                createItemUploadError(densoUserUploadingDomain, errorCode, 
                                    lineNo);
                                informationValidationComplete = false;
                            }else{
                                roleDensoCompanyCodeValidateflag = true;
                            }
                        }else{
                            /**** Check existing DENSO company not found in the master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR061;
                            createItemUploadError(densoUserUploadingDomain, errorCode, 
                                lineNo);
                            informationValidationComplete = false;
                        }
                    }

                    /**** Check NULL DENSO plant code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.THREE))){
                        /*errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR063;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;*/
                        splitItem.set(Constants.THREE, Constants.STR_NINETY_NINE);
                    }

                    /**** Check DENSO plant code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.THREE))
                        && Constants.TWO < splitItem.get(Constants.THREE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR064;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check DENSO plant code*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                        && isExistDensoCompany
                        && !StringUtil.checkNullOrEmpty(splitItem.get(Constants.THREE))
                        && splitItem.get(Constants.THREE).length() <= Constants.TWO){
                        /**Check existing DENSO plant in system*/
                        companyDensoCode = StringUtil.addSpace(splitItem.get(Constants.TWO),
                            Constants.FIVE);
                        plantDensoCode = StringUtil.addSpace(splitItem.get(Constants.THREE),
                            Constants.TWO);
                        boolean isExistDensoPlant = searchExistDensoPlant(
                            companyDensoCode, plantDensoCode);
                        if(isExistDensoPlant){
                            /**Check existing DENSO plant in system by current user data scope*/
                            companyDensoCode = StringUtil.addSpace(splitItem.get(Constants.TWO),
                                Constants.FIVE);
                            plantDensoCode = StringUtil.addSpace(splitItem.get(Constants.THREE),
                                Constants.TWO);
                            boolean isExistDensoPlantByScope = 
                                searchExistDensoPlantWithScope(companyDensoCode, 
                                    plantDensoCode, dataScopeControlDomain);
                            if(!isExistDensoPlantByScope){
                                /**Current user not have a right to working on this DENSO plant.*/
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR066;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                informationValidationComplete = false;
                            }
                        }else{
                            /**** Check existing DENSO plant not found in the master data*/
                            if(!Constants.STR_NINETY_NINE.equals(splitItem.get(Constants.THREE))){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR065;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                informationValidationComplete = false;
                            }
                        }
                    }

                    /**** Check NULL department name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOUR))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR018;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                    }

                    /**** Check department name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOUR))
                        && Constants.MAX_DEPARTMENT_LENGTH < splitItem.get(
                            Constants.FOUR).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR019;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL first name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIVE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR020;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check first name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIVE))
                        && Constants.MAX_FIRST_NAME_LENGTH < splitItem.get(
                            Constants.FIVE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR021;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check first name is not including by numeric*/
                    /*isLetter = splitItem.get(Constants.FIVE).matches(
                            Constants.REGX_LETTER_FORMAT);
                        if(!isLetter){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR022;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }*/

                    /**** Check NULL middle name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR023;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                    }

                    /**** Check middle name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))
                        && Constants.MAX_MIDDLE_NAME_LENGTH < splitItem.get(
                            Constants.SIX).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR024;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check middle name is not including by numeric*/
                    /*isLetter = splitItem.get(Constants.SIX).matches(
                            Constants.REGX_LETTER_FORMAT);
                        if(!isLetter){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR025;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }*/

                    /**** Check NULL last name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR026;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check last name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVEN))
                        && Constants.MAX_LAST_NAME_LENGTH < splitItem.get(Constants.SEVEN)
                            .length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR027;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check last name is not including by numeric*/
                    /*isLetter = splitItem.get(Constants.SEVEN).matches(
                            Constants.REGX_LETTER_FORMAT);
                        if(!isLetter){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR028;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }*/

                    /**** Check NULL telephone number*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHT))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR029;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                    }

                    /**** Check telephone number length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHT))
                        && Constants.MAX_TELEPHONE_LENGTH < splitItem.get(
                            Constants.EIGHT).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR030;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL email address*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR031;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check email address length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINE))
                        && Constants.MAX_EMAIL_LENGTH < splitItem.get(Constants.NINE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR032;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }else{
                        /**** Check format for each email address*/
                        String[] strEmail = StringUtils.splitPreserveAllTokens(
                            splitItem.get(Constants.NINE), Constants.SYMBOL_COMMA);
                        for(int i = Constants.ZERO; i < strEmail.length; i++){
                            boolean isEmail = 
                                strEmail[i].matches(Constants.REGX_EMAIL_FORMAT);
                            if(!isEmail){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR033;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                informationValidationComplete = false;
                            }
                        }
                    }
                    
                    /**** Check NULL email to : Create ASN Fixed*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR067;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        splitItem.set(Constants.TEN, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to : Create ASN Fixed*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TEN))
                        && !Constants.STR_ONE .equals(splitItem.get(Constants.TEN))
                        && !Constants.STR_ZERO.equals(splitItem.get(Constants.TEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR068;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL email to : Create ASN KANBAN*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ELEVEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR069;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        splitItem.set(Constants.ELEVEN, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to : Create ASN KANBAN*/
                    if(!StringUtil.checkNullOrEmpty( splitItem.get(Constants.ELEVEN))
                        && !Constants.STR_ONE.equals(splitItem.get(Constants.ELEVEN))
                        && !Constants.STR_ZERO.equals(splitItem.get(Constants.ELEVEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR070;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL email to : Pending purchase order*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWELVE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR071;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        splitItem.set(Constants.TWELVE, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to : Pending purchase order*/
                    if(!StringUtil.checkNullOrEmpty( splitItem.get(Constants.TWELVE))
                        && !Constants.STR_ONE .equals(splitItem.get(Constants.TWELVE))
                        && !Constants.STR_ZERO.equals(splitItem.get(Constants.TWELVE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR072;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL email to :  Change Ship QTY*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.THIRTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR073;
                        createItemUploadError(densoUserUploadingDomain, errorCode,
                            lineNo);
                        splitItem.set(Constants.THIRTEEN, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to :  Change Ship QTY*/
                    if(!StringUtil.checkNullOrEmpty( splitItem.get(Constants.THIRTEEN))
                        && !Constants.STR_ONE .equals(splitItem.get(Constants.THIRTEEN))
                        && !Constants.STR_ZERO.equals(splitItem.get(Constants.THIRTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR074;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL email to :  D/O Created to ASN*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOURTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR075;
                        createItemUploadError(densoUserUploadingDomain, errorCode,
                            lineNo);
                        splitItem.set(Constants.FOURTEEN, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to :  D/O Created to ASN*/
                    if(!StringUtil.checkNullOrEmpty( splitItem.get(Constants.FOURTEEN))
                        && !Constants.STR_ONE .equals(splitItem.get(Constants.FOURTEEN))
                        && !Constants.STR_ZERO.equals(splitItem.get(Constants.FOURTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR076;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL information flag*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWENTY))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR038;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                }/*End: Check information flag "N" value*/

                /**** Check NULL role flag*/
                if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWENTY_ONE))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR053;
                    createItemUploadError(densoUserUploadingDomain, errorCode, 
                        lineNo);
                    roleValidationComplete = false;
                }

                /**** Check role flag missing code*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWENTY_ONE))
                    && (!Constants.STR_A.equals(splitItem.get(Constants.TWENTY_ONE))
                        && !Constants.STR_U.equals(splitItem.get(Constants.TWENTY_ONE))
                        && !Constants.STR_T.equals(splitItem.get(Constants.TWENTY_ONE))
                        && !Constants.STR_N.equals(splitItem.get(Constants.TWENTY_ONE)))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR054;
                    createItemUploadError(densoUserUploadingDomain, errorCode, 
                        lineNo);
                    roleValidationComplete = false;
                }

                /**** Check role flag "N" value*/
                if(!(Constants.STR_N.equals(splitItem.get(Constants.TWENTY_ONE)))){

                    if(Constants.STR_N.equals(splitItem.get(Constants.TWENTY))){

                        /**** Check NULL  DSC ID*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                            /**** Check DSC ID length*/
                            if(Constants.MAX_DSC_ID_LENGTH < splitItem.get(Constants.ZERO).length())
                            {
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR003;
                                createItemUploadError(densoUserUploadingDomain, errorCode, 
                                    lineNo);
                                informationValidationComplete = false;
                            }

                            /**Changed DSC ID Validation refer to 23/10/2014 12:00 am.*/
                            /**** Check DSC ID in numeric format*/
                            /*isNumber = splitItem.get(Constants.ZERO).matches(
                                    Constants.REGX_NUMBER_FORMAT);
                                if(!isNumber){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR006;
                                    createItemUploadError(densoUserUploadingDomain, errorCode, 
                                        lineNo);
                                    informationValidationComplete = false;
                                }*/

                            /**** Check DSC ID not found in system.(Role flag is "A")*/
                            if(Constants.STR_A.equals(splitItem.get(Constants.TWENTY_ONE))){
                             /**** Check existing user by DSC ID in master data*/
                                dscId = splitItem.get(Constants.ZERO);
                                isExist = searchExistUser(dscId);
                                if(!isExist){
                                    SpsTmpUserDensoCriteriaDomain criteria
                                        = new SpsTmpUserDensoCriteriaDomain();
                                    int countRecord = 0;
                                    criteria.setDscId(dscId);
                                    criteria.setInformationFlag(Constants.STR_A);
                                    criteria.setUserDscId(userDscId);
                                    criteria.setSessionCd(sessionCd);
                                    
                                    countRecord = spsTmpUserDensoService.searchCount(criteria);
                                    if(Constants.ZERO < countRecord){
                                        dscIdValidateFlag = true;
                                    }else{
                                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                        createItemUploadError(
                                            densoUserUploadingDomain, errorCode, lineNo);
                                        informationValidationComplete = false;
                                    }
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }

                            /**** Check DSC ID  not found in system.(Role flag is "U")*/
                            if(Constants.STR_U.equals(splitItem.get(Constants.TWENTY_ONE))){
                                /**** Check existing user by DSC ID in master data */
                                dscId = splitItem.get(Constants.ZERO);
                                isExist = searchExistUser(dscId);
                                if(!isExist){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                    createItemUploadError(densoUserUploadingDomain, errorCode, 
                                        lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }

                            /**** Check DSC ID  not found in system (Role flag is "T")*/
                            if(Constants.STR_T.equals(splitItem.get(Constants.TWENTY_ONE))){
                                /**** Check existing user by DSC ID in master data */
                                dscId = splitItem.get(Constants.ZERO);
                                isExist = searchExistUser(dscId);
                                if(!isExist){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                    createItemUploadError(densoUserUploadingDomain, errorCode, 
                                        lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }
                        }else if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR002;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }
                    }

                    /**** Check role code length*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIFTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR040;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        roleValidationComplete = false;
                    }

                    /**** Check role code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIFTEEN))
                        && Constants.TWO < splitItem.get(Constants.FIFTEEN).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR041;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        roleValidationComplete = false;
                    }

                    /**** Check existing role in master data*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIFTEEN))){
                        /*Check existing role code in the role master data */
                        SpsMRoleCriteriaDomain roleCriteriaDomain = 
                            new SpsMRoleCriteriaDomain();
                        roleCriteriaDomain.setRoleCd(splitItem.get(Constants.FIFTEEN));
                        int recordCount = spsMRoleService.searchCount(roleCriteriaDomain);
                        if(Constants.ZERO < recordCount){
                            roleCodeValidateFlag = true;
                        }else{
                            /**** not found role code in master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR042;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                        }
                    }
                    /**** Check NULL assign role DENSO Company code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR077;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        roleValidationComplete = false;
                    }

                    /**** Check assign role DENSO Company code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))
                        && Constants.FIVE < splitItem.get(Constants.SIXTEEN).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR078;
                        createItemUploadError(densoUserUploadingDomain, errorCode, 
                            lineNo);
                        roleValidationComplete = false;
                    }

                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))
                        && splitItem.get(Constants.SIXTEEN).length() <= Constants.FIVE){
                        /**** Check existing DENSO company in system*/
                        companyDensoCode = StringUtil.addSpace(splitItem.get(Constants.SIXTEEN),
                            Constants.FIVE);
                        isExistRoleDensoCompany = searchExistDensoCompany(
                            companyDensoCode);
                        if(isExistRoleDensoCompany){
                            /**Check existing DENSO company in system by current user data scope*/
                            companyDensoCode = StringUtil.addSpace(splitItem.get(
                                Constants.SIXTEEN), Constants.FIVE);
                            boolean isExistDensoCompanyByScope = 
                                searchExistDensoCompanyWithScope(companyDensoCode,
                                    dataScopeControlDomain);
                            /** Current user not have a right to working on this DENSO company.*/
                            if(!isExistDensoCompanyByScope){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR080;
                                createItemUploadError(densoUserUploadingDomain, errorCode, 
                                    lineNo);
                                roleValidationComplete = false;
                            }else{
                                roleDensoCompanyCodeValidateflag = true;
                            }
                        }else{
                            /**** Check existing DENSO company not found in the master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR079;
                            createItemUploadError(densoUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                        }
                    }

                    /**** Check NULL assign role DENSO plant code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVENTEEN))){
//                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR081;
//                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
//                        roleValidationComplete = false;
                        splitItem.set(Constants.SEVENTEEN, Constants.STR_NINETY_NINE);
                    }

                    /**** Check assign role DENSO plant code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVENTEEN))
                        && Constants.TWO < splitItem.get(Constants.SEVENTEEN).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR082;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        roleValidationComplete = false;
                    }

                    /**** Check assign role DENSO plant code*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))
                        && isExistRoleDensoCompany
                        && !StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVENTEEN))
                        && splitItem.get(Constants.SEVENTEEN).length() <= Constants.TWO){
                        /*Check existing DENSO plant in system*/
                        companyDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SIXTEEN), Constants.FIVE);
                        plantDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SEVENTEEN), Constants.TWO);
                        boolean isExistDensoPlant = searchExistDensoPlant(
                            companyDensoCode, plantDensoCode);
                        if(isExistDensoPlant){
                            /*Check existing DENSO plant in system by current user data scope*/
                            companyDensoCode = StringUtil.addSpace(splitItem.get(
                                Constants.SIXTEEN), Constants.FIVE);
                            plantDensoCode = StringUtil.addSpace(splitItem.get(
                                Constants.SEVENTEEN), Constants.TWO);
                            boolean isExistDensoPlantByScope = 
                                searchExistDensoPlantWithScope(companyDensoCode, 
                                    plantDensoCode, dataScopeControlDomain);
                            if(isExistDensoPlantByScope){
                                roleDensoPlantCodeValidateFlag = true;
                            }else{
                                /**Current user not have a right to working on this DENSO plant.*/
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR084;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                roleValidationComplete = false;
                            }
                        }else{
                            /** Check existing DENSO plant not found in the master data*/
                            if(!Constants.STR_NINETY_NINE.equals(
                                splitItem.get(Constants.SEVENTEEN))){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR083;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                roleValidationComplete = false;
                            }else{
                                roleDensoPlantCodeValidateFlag = true;
                            }
                        }
                    }
                    /**** Check effective start date Null value*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR047;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        effectStartValidateFlag = false;
                        String effectiveStart = setEffectiveStartDate();
                        splitItem.set(Constants.EIGHTEEN, effectiveStart);
                    }else{
                        /**** Check format effective start date*/
                        if(!DateUtil.isValidDate(splitItem.get(Constants.EIGHTEEN), pattern)){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR048;
                            createItemUploadError(densoUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                            effectStartValidateFlag = false;
                        }else{
                            effectStartValidateFlag = true;
                        }
                    }

                    /**** Check effective end date Null value*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINETEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR049;
                        createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                        effectStartValidateFlag = false;
                        String effectiveEnd = setEffectiveEndDate();
                        splitItem.set(Constants.NINETEEN, effectiveEnd);
                    }else{
                        /**** Check format effective end date*/
                        if(!DateUtil.isValidDate(splitItem.get(Constants.NINETEEN), pattern)){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR050;
                            createItemUploadError(densoUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                            effectEndValidateFlag = false;
                        }else{
                            effectEndValidateFlag = true;
                        }
                    }

                    /**** Check effective start date more than effective end date*/
                    if(effectEndValidateFlag && effectStartValidateFlag){
                        if(Constants.ZERO < DateUtil.compareDate(splitItem.get(
                            Constants.EIGHTEEN), splitItem.get(Constants.NINETEEN))){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR051;
                            createItemUploadError(densoUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                        }
                    }

                    /**** Check effective end date less than current date*/
                    if(effectEndValidateFlag && effectStartValidateFlag){
                        if(DateUtil.compareDate(splitItem.get(Constants.NINETEEN), 
                            DateUtil.format(currentDate, pattern)) < Constants.ZERO ){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR052;
                            createItemUploadError(densoUserUploadingDomain, errorCode, 
                                lineNo);
                        }
                    }

                    /**** Check already assigned user role*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWENTY_ONE))
                        && dscIdValidateFlag && roleCodeValidateFlag
                        && roleDensoPlantCodeValidateFlag
                        && Constants.STR_A.equals(splitItem.get(Constants.TWENTY_ONE))){
                        /*Check existing user role in system*/
                        dscId     =   splitItem.get(Constants.ZERO);
                        roleCode  =   splitItem.get(Constants.FIFTEEN);
                        companyDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SIXTEEN), Constants.FIVE);
                        plantDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SEVENTEEN), Constants.TWO);
                        isActive             =    Constants.IS_ACTIVE;
                        returnValue = searchExistUserRole(dscId, roleCode, 
                            companyDensoCode, plantDensoCode, isActive);
                        boolean isExistUserRoleTmp = searchExistUserRoleTmpData(dscId, roleCode,
                            companyDensoCode, plantDensoCode, userDscId, sessionCd);
                        if(returnValue){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR055;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                        }else{
                            if(isExistUserRoleTmp){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR055;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                roleValidationComplete = false;
                            }
                        }
                    }

                    /**Check user role not found in system(in case of flag is update "U")*/
                    if(dscIdValidateFlag && roleCodeValidateFlag
                        && roleDensoCompanyCodeValidateflag
                        && roleDensoPlantCodeValidateFlag
                        && Constants.STR_U.equals(splitItem.get(Constants.TWENTY_ONE))){
                        /*Check existing user role in system*/
                        dscId     =   splitItem.get(Constants.ZERO);
                        roleCode  =   splitItem.get(Constants.FIFTEEN);
                        companyDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SIXTEEN), Constants.FIVE);
                        plantDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SEVENTEEN), Constants.TWO);
                        isActive  =  Constants.IS_ACTIVE;

                        returnValue = searchExistUserRole(dscId, roleCode,
                            companyDensoCode, plantDensoCode, isActive);
                        boolean isExistUserRoleTmp = searchExistUserRoleTmpData(dscId, roleCode,
                            companyDensoCode, plantDensoCode, userDscId, sessionCd);
                        if(!returnValue){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR056;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                        }else{
                            if(isExistUserRoleTmp){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR055;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                roleValidationComplete = false;
                            }
                        }
                    }

                    /**Check user role not found in system(in case of role flag is terminate"T")*/
                    if(dscIdValidateFlag && roleCodeValidateFlag
                        && roleDensoCompanyCodeValidateflag
                        && roleDensoPlantCodeValidateFlag
                        && Constants.STR_T.equals(splitItem.get(Constants.TWENTY_ONE))){
                        /*Check existing user role in system*/
                        dscId     =   splitItem.get(Constants.ZERO);
                        roleCode  =   splitItem.get(Constants.FIFTEEN);
                        companyDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SIXTEEN), Constants.FIVE);
                        plantDensoCode = StringUtil.addSpace(splitItem.get(
                            Constants.SEVENTEEN), Constants.TWO);
                        isActive  =  Constants.IS_ACTIVE;

                        returnValue = searchExistUserRole(dscId, roleCode,
                            companyDensoCode, plantDensoCode, isActive);
                        boolean isExistUserRoleTmp = searchExistUserRoleTmpData(dscId, roleCode,
                            companyDensoCode, plantDensoCode, userDscId, sessionCd);
                        if(!returnValue){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR056;
                            createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                        }else{
                            if(isExistUserRoleTmp){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR055;
                                createItemUploadError(densoUserUploadingDomain, errorCode, lineNo);
                                roleValidationComplete = false;
                            }
                        }
                    }
                }/*End:Check role flag "N" value*/

                List<SpsMUserDomain> userList = new ArrayList<SpsMUserDomain>();
                List<SpsMUserDensoDomain> densoList = new ArrayList<SpsMUserDensoDomain>();
                List<SpsMUserRoleDomain> userRoleList = new ArrayList<SpsMUserRoleDomain>();
                /**** Check Information validation and role validation is passed*/
                if(informationValidationComplete && roleValidationComplete){
                    if(Constants.STR_U.equals(splitItem.get(Constants.TWENTY))
                        || Constants.STR_D.equals(splitItem.get(Constants.TWENTY))){
                        if(isExist){
                            /*Get last update date from user master data*/
                            SpsMUserCriteriaDomain userCriteriaDomain = 
                                new SpsMUserCriteriaDomain();
                            userCriteriaDomain.setDscId(splitItem.get(Constants.ZERO));
                            userList = spsMUserService.searchByCondition(userCriteriaDomain);

                            /*Get last update date from DENSO user master data from */
                            SpsMUserDensoCriteriaDomain densoCriteriaDomain = 
                                new SpsMUserDensoCriteriaDomain();
                            densoCriteriaDomain.setDscId(splitItem.get(Constants.ZERO));
                            densoList = spsMUserDensoService.searchByCondition(
                                densoCriteriaDomain);
                        }else{
                            SpsMUserDensoDomain userDenso = new SpsMUserDensoDomain();
                            SpsMUserDomain userDomain = new SpsMUserDomain();
                            userDenso.setLastUpdateDatetime(commonService.searchSysDate());
                            userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                            userList.add(userDomain);
                            densoList.add(userDenso);
                        }
                    }

                    if(Constants.STR_U.equals(splitItem.get(Constants.TWENTY_ONE))
                        || Constants.STR_T.equals(splitItem.get(Constants.TWENTY_ONE))){
                        if(returnValue){
                            /*Get last update date from user role master data*/
                            SpsMUserRoleCriteriaDomain roleCriteriaDomain = 
                                new SpsMUserRoleCriteriaDomain();
                            roleCriteriaDomain.setDscId(splitItem.get(Constants.ZERO));
                            roleCriteriaDomain.setRoleCd(splitItem.get(Constants.FIFTEEN));
                            roleCriteriaDomain.setDCd(StringUtil.addSpace(splitItem.get(
                                Constants.SIXTEEN), Constants.FIVE));
                            roleCriteriaDomain.setDPcd(StringUtil.addSpace(splitItem.get(
                                Constants.SEVENTEEN), Constants.TWO));
                            roleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                            userRoleList = spsMUserRoleService.searchByCondition(
                                roleCriteriaDomain);
                        }else{
                            SpsMUserRoleDomain userRole = new SpsMUserRoleDomain();
                            userRole.setLastUpdateDatetime(commonService.searchSysDate());
                            userRoleList.add(userRole);
                        }
                    }

                    /*Insert all record items to the temporary table*/
                    SpsTmpUserDensoDomain tmpUserDensoDomain = 
                        new SpsTmpUserDensoDomain();
                    Timestamp effectStart = null;
                    Timestamp effectEnd = null;
                    String roleCd = new String();
                    String roleDCd = new String();
                    String roleDPcd = new String();

                    if(!(Constants.STR_N.equals(splitItem.get(Constants.TWENTY_ONE)))){
                        /*Set format effective date if not null*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHTEEN))){
                            Date effectStartUtil = DateUtil.parseToUtilDate(splitItem
                                .get(Constants.EIGHTEEN), pattern);
                            effectStart = new Timestamp(effectStartUtil.getTime());
                        }
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINETEEN))){
                            Date effectEndUtil = DateUtil.parseToUtilDate(splitItem
                                .get(Constants.NINETEEN), pattern);
                            effectEnd = new Timestamp(effectEndUtil.getTime());
                        }
                        roleCd = splitItem.get(Constants.FIFTEEN);
                        roleDCd = splitItem.get(Constants.SIXTEEN);
                        roleDPcd = splitItem.get(Constants.SEVENTEEN);
                    }else{
                        effectStart = null;
                        effectEnd = null;
                        roleCd = null;
                        roleDCd = null;
                        roleDPcd = null;
                    }

                    /*Set user and user DENSO last update date time temporary upload table*/
                    if(Constants.STR_U.equals(splitItem.get(Constants.TWENTY))
                        || Constants.STR_D.equals(splitItem.get(Constants.TWENTY))){

                        SpsMUserDomain userDomain = userList.get(Constants.ZERO);
                        SpsMUserDensoDomain userDenso = densoList.get(
                            Constants.ZERO);
                        tmpUserDensoDomain.setDensoLastUpdate(userDenso
                            .getLastUpdateDatetime());
                        tmpUserDensoDomain.setUserLastUpdate(
                            userDomain.getLastUpdateDatetime());

                    }

                    /*Set role last update date time to temporary upload table*/
                    if(Constants.STR_U.equals(splitItem.get(Constants.TWENTY_ONE))
                        || Constants.STR_T.equals(splitItem.get(Constants.TWENTY_ONE))){
                        SpsMUserRoleDomain userRole = userRoleList.get(Constants.ZERO);
                        tmpUserDensoDomain.setRoleLastUpdate(userRole
                            .getLastUpdateDatetime());
                    }

                    tmpUserDensoDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
                    tmpUserDensoDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
                    tmpUserDensoDomain.setLineNo(BigDecimal.valueOf(lineNo));
                    tmpUserDensoDomain.setDscId(splitItem.get(Constants.ZERO));
                    tmpUserDensoDomain.setEmployeeCd(splitItem.get(Constants.ONE));
                    tmpUserDensoDomain.setDCd(splitItem.get(Constants.TWO));
                    tmpUserDensoDomain.setDPcd(splitItem.get(Constants.THREE));
                    tmpUserDensoDomain.setDepartmentCd(splitItem.get(Constants.FOUR));
                    tmpUserDensoDomain.setFirstName(splitItem.get(Constants.FIVE));
                    tmpUserDensoDomain.setMiddleName(splitItem.get(Constants.SIX));
                    tmpUserDensoDomain.setLastName(splitItem.get(Constants.SEVEN));
                    tmpUserDensoDomain.setTelephone(splitItem.get(Constants.EIGHT));
                    tmpUserDensoDomain.setEmail(splitItem.get(Constants.NINE));
                    tmpUserDensoDomain.setEmlCreateCancelAsnFlag(splitItem.get(Constants.TEN));
                    tmpUserDensoDomain.setEmlReviseAsnFlag(splitItem.get(Constants.ELEVEN));
                    tmpUserDensoDomain.setEmlCreateInvoiceFlag(splitItem.get(Constants.TWELVE));
                    
                    // Start : [IN049] Change column name
                    //tmpUserDensoDomain.setEmlAbnormalTransfer1Flag(
                    //    splitItem.get(Constants.THIRTEEN));
                    //tmpUserDensoDomain.setEmlAbnormalTransfer2Flag(
                    //    splitItem.get(Constants.FOURTEEN));
                    tmpUserDensoDomain.setEmlPedpoDoalcasnUnmatpnFlg(
                        splitItem.get(Constants.THIRTEEN));
                    tmpUserDensoDomain.setEmlAbnormalTransferFlg(
                        splitItem.get(Constants.FOURTEEN));
                    // End : [IN049] Change column name
                    
                    tmpUserDensoDomain.setRoleCd(roleCd);
                    tmpUserDensoDomain.setRoleDCd(roleDCd);
                    tmpUserDensoDomain.setRoleDPcd(roleDPcd);
                    tmpUserDensoDomain.setEffectStart(effectStart);
                    tmpUserDensoDomain.setEffectEnd(effectEnd);
                    tmpUserDensoDomain.setInformationFlag(splitItem.get(Constants.TWENTY));
                    tmpUserDensoDomain.setRoleFlag(splitItem.get(Constants.TWENTY_ONE));
                    tmpUserDensoDomain.setIsActualRegister(Constants.IS_NOT_ACTIVE);
                    tmpUserDensoDomain.setUploadDatetime(commonService.searchSysDate());
                    tmpUserDensoDomain.setToActualDatetime(null);

                    spsTmpUserDensoService.create(tmpUserDensoDomain); 
                }/* End: Check Information validation and role validation is passed*/
            }/* End: LOOP*/
            
            /*Calculate total record*/
            int totalRecord = lineNo;
            int numberOfWarning = 0;
            int numberOfError = 0;
            int numberOfCorrect = 0;
            
            /*Check Existing data in TmpUploadUserSupplier table.*/
            SpsTmpUserDensoCriteriaDomain tmpUserDensoCriteriaDomain =
                new SpsTmpUserDensoCriteriaDomain();
            tmpUserDensoCriteriaDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
            tmpUserDensoCriteriaDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
            int countUserDenso = spsTmpUserDensoService.searchCount(
                tmpUserDensoCriteriaDomain);
            if(Constants.ZERO < countUserDenso){
                /*Get number of correct record*/
                numberOfCorrect = countUserDenso;
            }
            
            /*Check Existing data in TmpUploadError table.*/
            SpsTmpUploadErrorCriteriaDomain uploadErrorCriteriaDomain = new 
                SpsTmpUploadErrorCriteriaDomain();
            uploadErrorCriteriaDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
            uploadErrorCriteriaDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
            int count = spsTmpUploadErrorService.searchCount(uploadErrorCriteriaDomain);
            if(Constants.ZERO < count){
                /*Get number of warning record*/
                GroupUploadErrorDomain groupUploadErrorDomain = new GroupUploadErrorDomain();
                groupUploadErrorDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(densoUserUploadingDomain.getSessionCode());
                groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
                numberOfWarning = tmpUploadErrorService.searchCountWarning(groupUploadErrorDomain);
                
                /*Get number of incorrect record*/
                groupUploadErrorDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(densoUserUploadingDomain.getSessionCode());
                groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
                numberOfError = tmpUploadErrorService.searchCountIncorrect(groupUploadErrorDomain);
                
                /*Get number of correct record*/
                SpsTmpUserDensoCriteriaDomain densoCriteriaDomain = 
                    new SpsTmpUserDensoCriteriaDomain();
                densoCriteriaDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
                densoCriteriaDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
                numberOfCorrect = spsTmpUserDensoService.searchCount(densoCriteriaDomain);
                
                /*Calculate total record*/
                totalRecord = lineNo;
                
                int maxRecord = tmpUploadErrorService.searchCountGroupOfValidationError(
                    groupUploadErrorDomain);
                
                /* Get Maximum record limit*/
                MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
                recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
                recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM008_RLM);
                recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
                if(null == recordsLimit){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                /*Check max record*/
                if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[]{recordsLimit.getMiscValue()});
                }
                
                /* Get Maximum record per page limit*/
                MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
                recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
                recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM008_PLM);
                recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
                if(null == recordLimitPerPage){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                /*Calculate row number for query*/
                densoUserUploadingDomain.setPageNumber(densoUserUploadingDomain.getPageNumber());
                densoUserUploadingDomain.setMaxRowPerPage(Integer.valueOf(
                    recordLimitPerPage.getMiscValue()));
                SpsPagingUtil.calcPaging(densoUserUploadingDomain, maxRecord);
                
                /*Get list of validation error and warning and group by code number*/
                List<GroupUploadErrorDomain> result = new ArrayList<GroupUploadErrorDomain>();
                groupUploadErrorDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(densoUserUploadingDomain.getSessionCode());
                groupUploadErrorDomain.setRowNumFrom(densoUserUploadingDomain.getRowNumFrom());
                groupUploadErrorDomain.setRowNumTo(densoUserUploadingDomain.getRowNumTo());
                result = tmpUploadErrorService.searchGroupOfValidationError(groupUploadErrorDomain);
                densoUserUploadingDomain.setUploadErrorDetailDomain(result);
            }
            
            /*Set data to domain*/
            densoUserUploadingDomain.setTotalRecord(String.valueOf(totalRecord));
            densoUserUploadingDomain.setCorrectRecord(String.valueOf(numberOfCorrect));
            densoUserUploadingDomain.setInCorrectRecord(String.valueOf(numberOfError));
            densoUserUploadingDomain.setWarningRecord(String.valueOf(numberOfWarning));
            
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }finally{
            if(null != inputStream){
                inputStream.close();
            }
            if(null != inputStreamReader){
                inputStreamReader.close();
            }
            if(null != bufferReader){
                bufferReader.close();
            }
        }
        return densoUserUploadingDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeServiceImpl#transactRegisterUploadItem
     * (com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain)
     */
    public void transactRegisterUploadItem(DensoUserUploadingDomain densoUserUploadingDomain)
        throws ApplicationException{
        Locale locale = densoUserUploadingDomain.getLocale();
        
        try{
            /*Get record count in temporary table*/
            SpsTmpUserDensoCriteriaDomain tmpUserDensoCriteriaDomain = 
                new SpsTmpUserDensoCriteriaDomain();
            tmpUserDensoCriteriaDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
            tmpUserDensoCriteriaDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
            int recordCount = spsTmpUserDensoService.searchCount(tmpUserDensoCriteriaDomain);
            if(recordCount <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0039,
                    SupplierPortalConstant.LBL_DENSO_USER);
            }
            
            /*Get each record from temporary table*/
            for(int i = Constants.ZERO; i < recordCount; i++){
                
                /*Get temporary data each record to process*/
                TmpUserDensoDomain criteriaDomain = new TmpUserDensoDomain();
                criteriaDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
                criteriaDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
                TmpUserDensoDomain tempUploadDensoUserDomain = tmpUploadUserDensoService
                    .searchTmpUploadDensoOneRecord(criteriaDomain);
                if(null == tempUploadDensoUserDomain){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0039,
                        SupplierPortalConstant.LBL_DENSO_USER);
                }
                
                /*Supplier user information Operation by flag values.*/
                if(Constants.STR_A.equals(tempUploadDensoUserDomain.getInformationFlag())){
                    try{
                        /*Create new user information to the system*/
                        SpsMUserDomain userDomain = new SpsMUserDomain();
                        userDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                        userDomain.setFirstName(tempUploadDensoUserDomain.getFirstName());
                        if(!StringUtil.checkNullOrEmpty(tempUploadDensoUserDomain.getMiddleName())){
                            userDomain.setMiddleName(tempUploadDensoUserDomain.getMiddleName());
                        }
                        userDomain.setLastName(tempUploadDensoUserDomain.getLastName());
                        if(!StringUtil.checkNullOrEmpty(
                            tempUploadDensoUserDomain.getDepartmentCd())){
                            userDomain.setDepartmentName(
                                tempUploadDensoUserDomain.getDepartmentCd());
                        }
                        userDomain.setEmail(tempUploadDensoUserDomain.getEmail());
                        userDomain.setTelephone(tempUploadDensoUserDomain.getTelephone());
                        userDomain.setIsActive(Constants.IS_ACTIVE);
                        userDomain.setUserType(Constants.STR_D);
                        userDomain.setCreateDscId(densoUserUploadingDomain.getUserDscId());
                        userDomain.setCreateDatetime(commonService.searchSysDate());
                        userDomain.setLastUpdateDscId(densoUserUploadingDomain.getUserDscId());
                        userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                        
                        //Check existing inActive user in user table.
                        SpsMUserCriteriaDomain userCriteria = new SpsMUserCriteriaDomain();
                        SpsMUserDensoCriteriaDomain userDensoCriteria = 
                            new SpsMUserDensoCriteriaDomain();
                        SpsMUserRoleCriteriaDomain spsMUserRoleCriteria = 
                            new SpsMUserRoleCriteriaDomain();
                        userCriteria.setDscId(tempUploadDensoUserDomain.getDscId());
                        userCriteria.setIsActive(Constants.IS_NOT_ACTIVE);
                        userDensoCriteria.setDscId(tempUploadDensoUserDomain.getDscId());
                        spsMUserRoleCriteria.setDscId(tempUploadDensoUserDomain.getDscId());
                        spsMUserRoleCriteria.setIsActive(Constants.IS_NOT_ACTIVE);
                        int countExist = spsMUserService.searchCount(userCriteria);
                        if(Constants.ZERO < countExist){
                            //Delete existing inActive user in table.
                            spsMUserService.deleteByCondition(userCriteria);
                            spsMUserDensoService.deleteByCondition(userDensoCriteria);
                            spsMUserRoleService.deleteByCondition(spsMUserRoleCriteria);
                        }
                        spsMUserService.create(userDomain);
                    }catch(Exception e){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_REGISTER_USER_INFO);
                    }
                    
                    try{
                        /*Create new DENSO user information to the system*/
                        SpsMUserDensoDomain userDensoDomain = new SpsMUserDensoDomain();
                        userDensoDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                        userDensoDomain.setEmployeeCd(tempUploadDensoUserDomain.getEmployeeCd());
                        userDensoDomain.setDCd(tempUploadDensoUserDomain.getDCd());
                        userDensoDomain.setDPcd(tempUploadDensoUserDomain.getDPcd());
                        userDensoDomain.setEmlCreateCancelAsnFlag(
                            tempUploadDensoUserDomain.getEmlCreateCancelAsnFlag());
                        userDensoDomain.setEmlReviseAsnFlag(
                            tempUploadDensoUserDomain.getEmlReviseAsnFlag());
                        userDensoDomain.setEmlCreateInvoiceFlag(
                            tempUploadDensoUserDomain.getEmlCreateInvoiceFlag());
                        
                        // Start : [IN049] Change column name
                        //userDensoDomain.setEmlAbnormalTransfer1Flag(
                        //    tempUploadDensoUserDomain.getEmlAbnormalTransfer1Flag());
                        //userDensoDomain.setEmlAbnormalTransfer2Flag(
                        //    tempUploadDensoUserDomain.getEmlAbnormalTransfer2Flag());
                        userDensoDomain.setEmlPedpoDoalcasnUnmatpnFlg(
                            tempUploadDensoUserDomain.getEmlPedpoDoalcasnUnmatpnFlg());
                        userDensoDomain.setEmlAbnormalTransferFlg(
                            tempUploadDensoUserDomain.getEmlAbnormalTransferFlg());
                        // End : [IN049] Change column name
                        
                        userDensoDomain.setCreateDscId(densoUserUploadingDomain.getUserDscId());
                        userDensoDomain.setCreateDatetime(commonService.searchSysDate());
                        userDensoDomain.setLastUpdateDscId(densoUserUploadingDomain.getUserDscId());
                        userDensoDomain.setLastUpdateDatetime(commonService.searchSysDate());
                        spsMUserDensoService.create(userDensoDomain);
                    }catch(Exception e){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_REGISTER_DENSO_USER_INFO);
                    }
                    
                }else if(Constants.STR_U.equals(tempUploadDensoUserDomain.getInformationFlag())){
                    /*Update DENSO user information to the system*/
                    SpsMUserDensoCriteriaDomain userDensoCriteriaDomain = 
                        new SpsMUserDensoCriteriaDomain();
                    SpsMUserDensoDomain userDensoDomain = new SpsMUserDensoDomain();
                    userDensoDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userDensoDomain.setEmployeeCd(tempUploadDensoUserDomain.getEmployeeCd());
                    userDensoDomain.setDCd(tempUploadDensoUserDomain.getDCd());
                    userDensoDomain.setDPcd(tempUploadDensoUserDomain.getDPcd());
                    userDensoDomain.setEmlCreateCancelAsnFlag(
                        tempUploadDensoUserDomain.getEmlCreateCancelAsnFlag());
                    userDensoDomain.setEmlReviseAsnFlag(
                        tempUploadDensoUserDomain.getEmlReviseAsnFlag());
                    userDensoDomain.setEmlCreateInvoiceFlag(
                        tempUploadDensoUserDomain.getEmlCreateInvoiceFlag());
                    
                    // Start : [IN049] Change column name
                    //userDensoDomain.setEmlAbnormalTransfer1Flag(
                    //    tempUploadDensoUserDomain.getEmlAbnormalTransfer1Flag());
                    //userDensoDomain.setEmlAbnormalTransfer2Flag(
                    //    tempUploadDensoUserDomain.getEmlAbnormalTransfer2Flag());
                    userDensoDomain.setEmlPedpoDoalcasnUnmatpnFlg(
                        tempUploadDensoUserDomain.getEmlPedpoDoalcasnUnmatpnFlg());
                    userDensoDomain.setEmlAbnormalTransferFlg(
                        tempUploadDensoUserDomain.getEmlAbnormalTransferFlg());
                    // End : [IN049] Change column name
                    
                    userDensoDomain.setLastUpdateDscId(densoUserUploadingDomain.getUserDscId());
                    userDensoDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    /*Set criteria Domain*/
                    userDensoCriteriaDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userDensoCriteriaDomain.setLastUpdateDatetime(tempUploadDensoUserDomain
                        .getDensoLastUpdate());
                    
                    int updateRecord = spsMUserDensoService.updateByCondition(userDensoDomain, 
                        userDensoCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_UPDATE_DENSO_USER_INFO);
                    }
                    /*Update user information to the system*/
                    SpsMUserCriteriaDomain userCriteriaDomain = new SpsMUserCriteriaDomain();
                    SpsMUserDomain userDomain = new SpsMUserDomain();
                    userDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userDomain.setFirstName(tempUploadDensoUserDomain.getFirstName());
                    if(!StringUtil.checkNullOrEmpty(tempUploadDensoUserDomain.getMiddleName())){
                        userDomain.setMiddleName(tempUploadDensoUserDomain.getMiddleName());
                    }
                    userDomain.setLastName(tempUploadDensoUserDomain.getLastName());
                    if(StringUtil.checkNullOrEmpty(tempUploadDensoUserDomain.getDepartmentCd())){
                        userDomain.setDepartmentName(tempUploadDensoUserDomain.getDepartmentCd());
                    }
                    userDomain.setEmail(tempUploadDensoUserDomain.getEmail());
                    userDomain.setTelephone(tempUploadDensoUserDomain.getTelephone());
                    userDomain.setIsActive(Constants.IS_ACTIVE);
                    userDomain.setUserType(Constants.STR_D);
                    userDomain.setLastUpdateDscId(densoUserUploadingDomain.getUserDscId());
                    userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userCriteriaDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                    userCriteriaDomain.setLastUpdateDatetime(
                        tempUploadDensoUserDomain.getUserLastUpdate());
                    
                    updateRecord = spsMUserService.updateByCondition(
                        userDomain, userCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_UPDATE_USER_INFO);
                    }
                }else if(Constants.STR_D.equals(tempUploadDensoUserDomain.getInformationFlag())){
                    /*Update user information (active flag to false) to the system*/
                    SpsMUserCriteriaDomain userCriteriaDomain = new SpsMUserCriteriaDomain();
                    SpsMUserDomain userDomain = new SpsMUserDomain();
                    userDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                    userDomain.setLastUpdateDscId(tempUploadDensoUserDomain.getUserDscId());
                    userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userCriteriaDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userCriteriaDomain.setLastUpdateDatetime(tempUploadDensoUserDomain
                        .getUserLastUpdate());
                    
                    int updateRecord = spsMUserService.updateByCondition(userDomain, 
                        userCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_DELETE_USER_INFO);
                    }
                }
                /*Supplier user role Operation by flag values.*/
                if(Constants.STR_A.equals(tempUploadDensoUserDomain.getRoleFlag())){
                    /*Assign new role to supplier user*/
                    try{
                        SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
                        userRoleDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                        userRoleDomain.setRoleCd(tempUploadDensoUserDomain.getRoleCd());
                        userRoleDomain.setDCd(tempUploadDensoUserDomain.getRoleDCd());
                        userRoleDomain.setDPcd(tempUploadDensoUserDomain.getRoleDPcd());
                        userRoleDomain.setIsActive(Constants.IS_ACTIVE);
                        userRoleDomain.setEffectStart(tempUploadDensoUserDomain.getEffectStart());
                        userRoleDomain.setEffectEnd(tempUploadDensoUserDomain.getEffectEnd());
                        userRoleDomain.setCreateDscId(tempUploadDensoUserDomain.getUserDscId());
                        userRoleDomain.setCreateDatetime(commonService.searchSysDate());
                        userRoleDomain.setLastUpdateDscId(tempUploadDensoUserDomain.getUserDscId());
                        userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());

                        //Check existing inActive user role in table.
                        SpsMUserRoleCriteriaDomain spsMUserRoleCriteria = 
                            new SpsMUserRoleCriteriaDomain();
                        spsMUserRoleCriteria.setDscId(tempUploadDensoUserDomain.getDscId());
                        spsMUserRoleCriteria.setIsActive(Constants.IS_NOT_ACTIVE);
                        int countRecord = spsMUserRoleService.searchCount(spsMUserRoleCriteria);
                        if(Constants.ZERO < countRecord){
                          //Delete existing inActive user role in table.
                            spsMUserRoleService.deleteByCondition(spsMUserRoleCriteria);
                        }
                        spsMUserRoleService.create(userRoleDomain);
                    }catch(Exception e){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_REGISTER_USER_ROLE);
                    }
                }else if(Constants.STR_U.equals(tempUploadDensoUserDomain.getRoleFlag())){
                    /*Update existing supplier user role*/
                    SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = 
                        new SpsMUserRoleCriteriaDomain();
                    SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
                    userRoleDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userRoleDomain.setRoleCd(tempUploadDensoUserDomain.getRoleCd());
                    userRoleDomain.setDCd(tempUploadDensoUserDomain.getRoleDCd());
                    userRoleDomain.setDPcd(tempUploadDensoUserDomain.getRoleDPcd());
                    userRoleDomain.setIsActive(Constants.IS_ACTIVE);
                    userRoleDomain.setEffectStart(tempUploadDensoUserDomain.getEffectStart());
                    userRoleDomain.setEffectEnd(tempUploadDensoUserDomain.getEffectEnd());
                    userRoleDomain.setLastUpdateDscId(tempUploadDensoUserDomain.getUserDscId());
                    userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userRoleCriteriaDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userRoleCriteriaDomain.setRoleCd(tempUploadDensoUserDomain.getRoleCd());
                    userRoleCriteriaDomain.setDCd(tempUploadDensoUserDomain.getRoleDCd());
                    userRoleCriteriaDomain.setDPcd(tempUploadDensoUserDomain.getRoleDPcd());
                    userRoleCriteriaDomain.setLastUpdateDatetime(tempUploadDensoUserDomain
                        .getRoleLastUpdate());

                    int updateRecord = spsMUserRoleService.updateByCondition(userRoleDomain, 
                        userRoleCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_UPDATE_USER_ROLE);
                    }
                }else if(Constants.STR_T.equals(tempUploadDensoUserDomain.getRoleFlag())){
                    /*Set supplier user role to expired*/
                    SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = 
                        new SpsMUserRoleCriteriaDomain();
                    SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
                    userRoleDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userRoleDomain.setRoleCd(tempUploadDensoUserDomain.getRoleCd());
                    userRoleDomain.setDCd(tempUploadDensoUserDomain.getRoleDCd());
                    userRoleDomain.setDPcd(tempUploadDensoUserDomain.getRoleDPcd());
                    userRoleDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                    userRoleDomain.setEffectStart(tempUploadDensoUserDomain.getEffectStart());
                    userRoleDomain.setEffectEnd(tempUploadDensoUserDomain.getEffectEnd());
                    userRoleDomain.setLastUpdateDscId(tempUploadDensoUserDomain.getUserDscId());
                    userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userRoleCriteriaDomain.setDscId(tempUploadDensoUserDomain.getDscId());
                    userRoleCriteriaDomain.setRoleCd(tempUploadDensoUserDomain.getRoleCd());
                    userRoleCriteriaDomain.setDCd(tempUploadDensoUserDomain.getRoleDCd());
                    userRoleCriteriaDomain.setDPcd(tempUploadDensoUserDomain.getRoleDPcd());
                    userRoleCriteriaDomain.setLastUpdateDatetime(tempUploadDensoUserDomain
                        .getRoleLastUpdate());
                    
                    int updateRecord = spsMUserRoleService.updateByCondition(userRoleDomain,
                        userRoleCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_DELETE_USER_ROLE);
                    }
                }
                /*Set flag (register to actual table) in temporary table*/
                SpsTmpUserDensoDomain tmpUserDensoDomain = new SpsTmpUserDensoDomain();
                /*Set value to Domain*/
                tmpUserDensoDomain.setIsActualRegister(Constants.IS_ACTIVE);
                tmpUserDensoDomain.setToActualDatetime(commonService.searchSysDate());
                /*Set value to CriteriaDomain*/
                tmpUserDensoCriteriaDomain.setUserDscId(tempUploadDensoUserDomain.getUserDscId());
                tmpUserDensoCriteriaDomain.setSessionCd(tempUploadDensoUserDomain
                    .getSessionCd());
                tmpUserDensoCriteriaDomain.setLineNo(tempUploadDensoUserDomain.getLineNo());
                
                int updateRecord = spsTmpUserDensoService.updateByCondition(
                    tmpUserDensoDomain, tmpUserDensoCriteriaDomain);
                if(updateRecord != Constants.ONE){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                        SupplierPortalConstant.LBL_UPDATE_MOVE_TO_ACTUAL_FLAG);
                }
            } /*END LOOP*/
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeServiceImpl#searchUploadErrorList
     * (com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain)
     */
    public  DensoUserUploadingDomain searchUploadErrorList(DensoUserUploadingDomain 
        densoUserUploadingDomain)throws ApplicationException{
        Locale locale = densoUserUploadingDomain.getLocale();
        List<GroupUploadErrorDomain> result = new ArrayList<GroupUploadErrorDomain>();
        
        /*Get number of warning record*/
        GroupUploadErrorDomain groupUploadErrorDomain = new GroupUploadErrorDomain();
        groupUploadErrorDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(densoUserUploadingDomain.getSessionCode());
        groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
        int numberOfWarning = tmpUploadErrorService.searchCountWarning(groupUploadErrorDomain);

        /*Get number of incorrect record */
        groupUploadErrorDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(densoUserUploadingDomain.getSessionCode());
        groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
        int numberOfError = tmpUploadErrorService.searchCountIncorrect(groupUploadErrorDomain);

        /*Get number of correct record*/
        SpsTmpUserDensoCriteriaDomain densoCriteriaDomain = new SpsTmpUserDensoCriteriaDomain();
        densoCriteriaDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
        densoCriteriaDomain.setSessionCd(densoUserUploadingDomain.getSessionCode());
        int numberOfCorrect = spsTmpUserDensoService.searchCount(densoCriteriaDomain);

        /*Calculate total record*/
        int totalRecord = numberOfCorrect + numberOfWarning + numberOfError;

        int maxRecord = tmpUploadErrorService.searchCountGroupOfValidationError(
            groupUploadErrorDomain);
        /* Get Maximum record limit*/
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM008_RLM);
        recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
        if(null == recordsLimit){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Check max record*/
        if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{recordsLimit.getMiscValue()});
        }
        
        /* Get Maximum record per page limit*/
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM008_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /*Calculate row number for query*/
        densoUserUploadingDomain.setPageNumber(densoUserUploadingDomain.getPageNumber());
        densoUserUploadingDomain.setMaxRowPerPage(Integer.valueOf(
            recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(densoUserUploadingDomain, maxRecord);

        /*Get list of validation error and warning and group by code number*/
        groupUploadErrorDomain.setUserDscId(densoUserUploadingDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(densoUserUploadingDomain.getSessionCode());
        groupUploadErrorDomain.setRowNumFrom(densoUserUploadingDomain.getRowNumFrom());
        groupUploadErrorDomain.setRowNumTo(densoUserUploadingDomain.getRowNumTo());
        result = tmpUploadErrorService.searchGroupOfValidationError(groupUploadErrorDomain);

        densoUserUploadingDomain.setTotalRecord(String.valueOf(totalRecord));
        densoUserUploadingDomain.setCorrectRecord(String.valueOf(numberOfCorrect));
        densoUserUploadingDomain.setInCorrectRecord(String.valueOf(numberOfError));
        densoUserUploadingDomain.setWarningRecord(String.valueOf(numberOfWarning));
        densoUserUploadingDomain.setUploadErrorDetailDomain(result);
            
        return densoUserUploadingDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeServiceImpl#deleteUploadTempTable
     * (String userDscId, String sessionCode)
     */
    public void deleteUploadTempTable(String userDscId, String sessionCode)
        throws ApplicationException{
        
        SpsTmpUploadErrorCriteriaDomain criteriaDomain = new SpsTmpUploadErrorCriteriaDomain();
        SpsTmpUserDensoCriteriaDomain densoCriteriaDomain = 
            new SpsTmpUserDensoCriteriaDomain();
        /*Set Upload Date time Less Than Equal to (CommonService.searchSysDate() - 2 day)*/
        Timestamp currentTimestamp = commonService.searchSysDate();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Calendar prevDate = Calendar.getInstance();
        prevDate.setTime(currentTimestamp);
        prevDate.add(Calendar.DATE, Constants.MINUS_TWO);
        String previousDate = DateUtil.format(prevDate.getTime(), patternTimestamp);
        Timestamp uploadDatetime = DateUtil.parseToTimestamp(previousDate, patternTimestamp);

        /*1.Delete temporary table that keep upload error items.*/
        if(!StringUtil.checkNullOrEmpty(userDscId) && !StringUtil.checkNullOrEmpty(sessionCode))
        {
            criteriaDomain.setSessionCd(sessionCode);
            criteriaDomain.setUserDscId(userDscId);
        }else{
            criteriaDomain.setUploadDatetimeLessThanEqual(uploadDatetime);
        }
        spsTmpUploadErrorService.deleteByCondition(criteriaDomain);

        /*2.Delete temporary table that keep upload DENSO user information*/
        if(!StringUtil.checkNullOrEmpty(userDscId) && !StringUtil.checkNullOrEmpty(sessionCode))
        {
            densoCriteriaDomain.setSessionCd(sessionCode);
            densoCriteriaDomain.setUserDscId(userDscId);
        }else{
            densoCriteriaDomain.setUploadDatetimeLessThanEqual(uploadDatetime);
        }
        spsTmpUserDensoService.deleteByCondition(densoCriteriaDomain); 

    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeServiceImpl#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * 
     * <p>Search Exist DENSO Company.</p>
     *
     * @param densoCompanyCode the DENSO Company code to check existing DENSO company.
     * @return boolean that keep true = found DENSO company code in master data 
     * and false = not found DENSO company code in master data.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistDensoCompany(String densoCompanyCode)
        throws ApplicationException{
        
        /*Check existing DENSO company in the master data*/
        SpsMCompanyDensoCriteriaDomain criteriaDomain = new SpsMCompanyDensoCriteriaDomain();

        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMCompanyDensoService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }

    }
    
    /**
     * 
     * <p>Search Exist DENSO Company With Scope.</p>
     *
     * @param densoCompanyCode the DENSO Company code to check existing DENSO company.
     * @param dataScopeControlDomain the Limitation to search condition.
     * @return boolean that keep true = found DENSO company code in master data 
     * and false = not found DENSO company code in master data.
     */
    private boolean searchExistDensoCompanyWithScope(String densoCompanyCode, 
        DataScopeControlDomain dataScopeControlDomain){
        /*Check existing DENSO company in the master data*/ 
        CompanyDensoWithScopeDomain companyDensoWithScopeDomain = 
            new CompanyDensoWithScopeDomain();
        CompanyDensoDomain companyDensoDomain = new CompanyDensoDomain();
        companyDensoDomain.setIsActive(Constants.IS_ACTIVE);
        companyDensoDomain.setDCd(densoCompanyCode);
        companyDensoWithScopeDomain.setCompanyDensoDomain(companyDensoDomain);
        companyDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        Integer count = companyDensoService.searchExistCompanyDenso(
            companyDensoWithScopeDomain);
        if(Constants.ZERO < count){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist DENSO Plant.</p>
     *
     * @param densoCompanyCode the DENSO company code.
     * @param densoPlantCode the DENSO plant code.
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistDensoPlant(String densoCompanyCode, String densoPlantCode)
        throws ApplicationException{
        /*Check existing DENSO plant in the master data*/
        SpsMPlantDensoCriteriaDomain criteriaDomain = new SpsMPlantDensoCriteriaDomain();

        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setDPcd(densoPlantCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMPlantDensoService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
        
    }
    
    /**
     * 
     * <p>Search Exist DENSO Plant With Scope.</p>
     *
     * @param densoCompanyCode the DENSO Company code to check existing DENSO company.
     * @param densoPlantCode the DENSO plant code
     * @param dataScopeControlDomain the Limitation to search condition.
     * @return boolean that keep true = found DENSO plant code in master data 
     * and false = not found DENSO plant code in master data.
     */
    private boolean searchExistDensoPlantWithScope(String densoCompanyCode, 
        String densoPlantCode, DataScopeControlDomain dataScopeControlDomain){
        /*Check existing Supplier plant by current user data scope in the master data */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = 
            new PlantDensoWithScopeDomain();
        PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
        plantDensoDomain.setDCd(densoCompanyCode);
        plantDensoDomain.setDPcd(densoPlantCode);
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        int countRecord = plantDensoService
            .searchExistPlantDenso(plantDensoWithScopeDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    

    /**
     * 
     * <p>Search Exist User Role.</p>
     *
     * @param dscId the User DSC ID number.
     * @param roleCode the Role code.
     * @param densoCompanyCode the DENSO company code.
     * @param densoPlantCode the DENSO plant code.
     * @param isActive the user role active flag.
     * @return boolean the true = use existing in master data, false = user  not in master data.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistUserRole(String dscId, String roleCode, String densoCompanyCode, 
        String densoPlantCode, String isActive)throws ApplicationException{
        
        /*Check existing user role in the master data*/
        SpsMUserRoleCriteriaDomain criteriaDomain = new SpsMUserRoleCriteriaDomain();

        criteriaDomain.setDscId(dscId);
        criteriaDomain.setRoleCd(roleCode);
        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setDPcd(densoPlantCode);
        criteriaDomain.setIsActive(isActive);
        int countRecord = spsMUserRoleService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }

    }
    
    /**
     * 
     * <p>create Item Upload Error.</p>
     *
     * @param uploadDomain It keep current user information to create error data to temporary table.
     * @param errorCode the Error code from validation.
     * @param lineNo the Line no from validation.
     * @throws ApplicationException the ApplicationException.
     */
    private void createItemUploadError(DensoUserUploadingDomain uploadDomain, 
        String errorCode, int lineNo)throws ApplicationException{
        /*Create error from upload validation to temporary table*/
        SpsTmpUploadErrorDomain errorDomain = new SpsTmpUploadErrorDomain();

        errorDomain.setUserDscId(uploadDomain.getUserDscId());
        errorDomain.setSessionCd(uploadDomain.getSessionCode());
        errorDomain.setLineNo(BigDecimal.valueOf(lineNo));
        errorDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
        errorDomain.setMiscCd(errorCode);
        errorDomain.setFunctionCode(Constants.STR_ONE);
        errorDomain.setUploadDatetime(commonService.searchSysDate());
        spsTmpUploadErrorService.create(errorDomain);
        
    }
    
    /**
     * 
     * <p>search Exist User.</p>
     *
     * @param dscId the DSC ID number for search user information.
     * @return boolean the true = user DSC ID existing in master data, 
     * false = user DSC ID not in master data.
     * @throws ApplicationException the ApplicationException.
     */
    
    private boolean searchExistUser(String dscId)throws ApplicationException{
        /*Check existing user in master date*/
        SpsMUserCriteriaDomain criteriaDomain = new SpsMUserCriteriaDomain();

        criteriaDomain.setDscId(dscId);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMUserService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>set Effective end date.</p>
     *
     * @return effectiveEndDate
     * @throws Exception the Exception.
     */
    private String setEffectiveEndDate()throws Exception{
        String effectiveEnd = new String();
        Timestamp currentTimestamp = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        
        //Search limit of effect end date
        MiscellaneousDomain endDateLimit = new MiscellaneousDomain();
        endDateLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
        endDateLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_ADD_DTE);
        endDateLimit = recordLimitService.searchRecordLimit(endDateLimit);
        
        Integer effectDateLimit = Integer.valueOf(endDateLimit.getMiscValue());
        
        Calendar effectDate = Calendar.getInstance();
        effectDate.setTime(currentTimestamp);
        effectDate.add(Calendar.DATE, effectDateLimit);
        
        effectiveEnd = DateUtil.format(effectDate.getTime(), pattern);

        return effectiveEnd;
    }
    
    /**
     * 
     * <p>set Effective start date.</p>
     *
     * @return effectiveEndDate
     * @throws Exception the Exception.
     */
    private String setEffectiveStartDate()throws Exception{
        Timestamp currentTimestamp = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Date effectStartDate = new Date(currentTimestamp.getTime());
        String effectiveStart = DateUtil.format(effectStartDate, pattern);
        return effectiveStart;
    }
    
    /**
     * 
     * <p>search Exist User Temporary data.</p>
     *
     * @param dscId the User DSC ID number.
     * @param roleCode the Role code.
     * @param densoCompanyCode the DENSO company code.
     * @param densoPlantCode the DENSO plant code.
     * @param userDscId the User DSC ID number.
     * @param sessionCd the session code.
     * @return boolean the true = user DSC ID existing in master data, 
     * false = user DSC ID not in master data.
     * @throws ApplicationException the applicationException.
     */
    private boolean searchExistUserRoleTmpData(String dscId, String roleCode, String 
        densoCompanyCode, String densoPlantCode, String userDscId, String sessionCd)throws 
        ApplicationException{
        /*Check existing user in Temporary date*/
        SpsTmpUserDensoCriteriaDomain criteriaDomain = new SpsTmpUserDensoCriteriaDomain();
        int countRecord = 0;
        criteriaDomain.setDscId(dscId);
        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setRoleCd(roleCode);
        criteriaDomain.setRoleDPcd(densoPlantCode);
        criteriaDomain.setUserDscId(userDscId);
        criteriaDomain.setSessionCd(sessionCd);
        
        countRecord = spsTmpUserDensoService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }

    } 

}