/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 2015/08/04 CSI Akat                [IN009]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.CnInformationDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.business.dao.CnDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;

/**
 * <p>The Class CNServiceImpl.</p>
 * <p>Service for file upload about search and insert file upload information.</p>
 * <ul>
 * <li>Method search  : searchCountCn</li>
 * <li>Method search  : searchCn</li>
 * <li>Method search  : searchCnCoverPage</li>
 * <li>Method search  : searchCnForTransferToJde</li>
 * </ul>
 *
 * @author CSI
 */
public class CnServiceImpl implements CnService {
    
    /** The cn dao. */
    private CnDao cnDao;

    /**
     * Instantiates a new cn service impl.
     */
    public CnServiceImpl(){
        super();
    }
    
    /**
     * Sets the cn dao.
     * 
     * @param cnDao the cn dao.
     */
    public void setCnDao(CnDao cnDao) {
        this.cnDao = cnDao;
    }
    
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CNService
     * #searchCountCn
     * (com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain)
     */
    public Integer searchCountCn(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain){
        
        return this.cnDao.
            searchCountCn(priceDifferenceInformationDomain);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CNService#searchCN
     * (com.globaldenso.asia.sps.business.domain.priceDifferenceInformationDomain)
     */
    public List<PriceDifferenceInformationDomain> searchCn(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain){
        
        List<PriceDifferenceInformationDomain> result = null;
        
        result = cnDao.searchCn(priceDifferenceInformationDomain);
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        for(PriceDifferenceInformationDomain item: result){
            DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
            decimalFormat.setGroupingUsed(true);
            decimalFormat.setMinimumFractionDigits(Constants.FOUR);
            decimalFormat.setMaximumFractionDigits(Constants.FOUR);
            
            item.setInvoiceDate(
                DateUtil.format(item.getSpsTInvoiceDomain().getInvoiceDate(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            item.setCnDate(
                DateUtil.format(item.getSpsTCnDomain().getCnDate(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            
            // [IN009] Rounding Mode use HALF_UP
            //item.setSupplierPriceUnit(
            //    decimalFormat.format(item.getSpsTCnDetailDomain().getSPriceUnit()));
            //item.setDensoPriceUnit(
            //    decimalFormat.format(item.getSpsTCnDetailDomain().getDPriceUnit()));
            //item.setDifferencePriceUnit(
            //    decimalFormat.format(item.getSpsTCnDetailDomain().getDiffPriceUnit()));
            item.setSupplierPriceUnit(NumberUtil.formatRoundingHalfUp(
                decimalFormat, item.getSpsTCnDetailDomain().getSPriceUnit(), Constants.FOUR));
            item.setDensoPriceUnit(NumberUtil.formatRoundingHalfUp(
                decimalFormat, item.getSpsTCnDetailDomain().getDPriceUnit(), Constants.FOUR));
            item.setDifferencePriceUnit(NumberUtil.formatRoundingHalfUp(
                decimalFormat, item.getSpsTCnDetailDomain().getDiffPriceUnit(), Constants.FOUR));
                
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CNService#createCN
     * (com.globaldenso.asia.sps.business.domain.SpsTCnDomain)
     */
    public Integer createCn(SpsTCnDomain spsTCnDomain){
        
        Integer cnId = null;
        cnId = this.cnDao.createCn(spsTCnDomain);
        return cnId;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CNService#searchCnCoverPage
     * (com.globaldenso.asia.sps.business.domain.CNInformationDomain)
     */
    public List<CnInformationDomain> searchCnCoverPage(CnInformationDomain cnInformationDomain){
        
        List<CnInformationDomain> result = null;
        result = this.cnDao.searchCnCoverPage(cnInformationDomain);
        return result;
    } 

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CNService#searchCnForTransferToJde
     * (com.globaldenso.asia.sps.auto.business.domain.SpsTCnCriteriaDomain)
     */
    public List<CnInformationDomain> searchCnForTransferToJde(
        SpsTCnCriteriaDomain spsTCnCriteriaDomain)
    {
        List<CnInformationDomain> result = null;
        result = this.cnDao.searchCnForTransferToJde(spsTCnCriteriaDomain);
        return result;
    }
}