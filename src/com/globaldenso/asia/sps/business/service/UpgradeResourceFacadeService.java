/*
 * ModifyDate Development company     Describe 
 * 2017/09/26 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.BhtUpgradeDomain;

/**
 * <p>The Interface UResourceServices.</p>
 * <p>Service for Upgrade Resource about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchBhtUpgrade</li>
 * </ul>
 *
 * @author Netband
 */
public interface UpgradeResourceFacadeService {

    /**
     * <p>searchBhtUpgrade.</p>
     * 
     * @param bhtUpgradeDomain the BhtUpgradeDomain 
     * @return the bhtUpgradeDomain
     * @throws ApplicationException ApplicationException
     */
    public BhtUpgradeDomain searchBhtUpgrade
        (BhtUpgradeDomain bhtUpgradeDomain) throws ApplicationException;
    
}
