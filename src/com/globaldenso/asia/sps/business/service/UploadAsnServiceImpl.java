/*
 * ModifyDate Development company    Describe 
 * 2014/08/19 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.dao.UploadAsnDao;

/**
 * <p>The Class UploadAsnServiceImpl.</p>
 * <p>Service for ASN Uploading about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchTmpUploadAsn</li>
 * </ul>
 * @author CSI
 */
public class UploadAsnServiceImpl implements UploadAsnService {
    
    /** The upload asn dao. */
    private UploadAsnDao uploadAsnDao;

    /**
     * Instantiates a new upload asn service impl.
     */
    public UploadAsnServiceImpl(){
        super();
    }
    
    /**
     * Sets the upload asn dao.
     * 
     * @param uploadAsnDao the upload asn dao
     */
    public void setUploadAsnDao(UploadAsnDao uploadAsnDao) {
        this.uploadAsnDao = uploadAsnDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UploadAsnService#searchTmpUploadAsn(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    public List<AsnMaintenanceReturnDomain> searchTmpUploadAsn(
        AsnMaintenanceDomain asnMaintenanceDomain) throws ApplicationException {
        List<AsnMaintenanceReturnDomain> asnInformationResultList = null;
        asnInformationResultList = uploadAsnDao.searchTmpUploadAsn(asnMaintenanceDomain);
        return asnInformationResultList;
    } 
}