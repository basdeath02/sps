/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;

/**
 * <p>The Interface MenuService.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchMenuByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public interface MenuService {
    
    /**
     * Search Menu by User Permission from SPS_M_ROLE_USER_DENSO, SPS_M_ROLE_MENU and SPS_M_MENU.
     * @param dscId User's DSC_ID
     * @param isActive is menu active
     * @param currentDatetime current date time
     * @return list of SpsMMenuDomain
     * */
    public List<SpsMMenuDomain> searchMenuByUserPermission(
        String dscId, String isActive, Timestamp currentDatetime);
    
}
