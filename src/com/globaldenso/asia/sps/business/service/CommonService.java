/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.io.IOException;
import java.sql.Timestamp;
import com.globaldenso.asia.sps.business.domain.CommonDomain;


/**
 * <p>The Interface CommonService.</p>
 * <p>Service for User about common function in system.</p>
 * <ul>
 * <li>Method search: searchSysDate</li>
 * </ul>
 *
 * @author CSI
 */
public interface CommonService {
    
    /**
     * <p>Search system date.</p>
     * <ul>
     * <li>
     * Search the current date from server.(DB Server)
     * </li>
     * </ul>
     *  
     * @return the Timestamp
     */
    public Timestamp searchSysDate();
    
    /**
     * <p>Search system date.</p>
     * <ul>
     * <li>
     * Search the current date from server.(DB Server)
     * </li>
     * </ul>
     *  
     * @param commonDomain the Common domain
     * @return the Timestamp
     */
    public Timestamp searchSysDate(CommonDomain commonDomain);
    
    /**
     * <p>Creates the CSV.</p>
     * <ul>
     * <li>Program will prepare data for export dynamic master table.</li>
     * </ul>
     * 
     * @param commonDomain the Common domain
     * @return the commonDomain
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public StringBuffer createCsvString(CommonDomain commonDomain)throws IOException;
}