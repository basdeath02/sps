/*
 * ModifyDate Development company     Describe 
 * 2014/08/16 CSI Arnon               Create
 * 2016/02/04 CSI Akat                [IN056]
 * 2016/03/14 CSI Akat                [IN056-2]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;

/**
 * <p>The class TransferPoDataToCigmaServiceImpl.</p>
 * <p>Facade for TransferPoDataToCigmaServiceImpl.</p>
 * <ul>
 * </ul>
 *
 * @author CSI
 */
public interface TransferAsnDataFromCigmaService{
    /**
     * 
     * <p>Search As400 Server List.</p>
     *
     * @param criteria company
     * @return List
     * @throws Exception exception
     */
	
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
            SpsMCompanyDensoDomain criteria) throws Exception;
        
    
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchData(
    		SpsMDensoDensoRelationWithASNDNDomain criteria) throws Exception;
    
    public void transferData(
            As400ServerConnectionInformationDomain as400Server, String spsFlag,List<SpsMDensoDensoRelationWithASNDNDomain> data) throws Exception;
    
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchDataGroup(SpsMDensoDensoRelationWithASNDNDomain criteria) throws Exception;
    
}
