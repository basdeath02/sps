/*
 * ModifyDate Development company       Describe 
 * 2014/07/29 CSI Chatchai              Create
 * 2015/10/05 CSI Akat                  [IN020]
 * 2015/12/09 CSI Akat                  [IN042]
 * 2015/12/25 CSI Akat                  [ADDITIONAL]
 * 2017/08/30 Netband U.Rungsiwut       Modify
 * 2018/04/11 Netband U.Rungsiwut       Generate kanban PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailListReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultMasterDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>
 * Supplier and DENSO can inquire KANBAN Order data(D/O), be able to download
 * them by CSV . User can also download D/O report in PDF format.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationFacadeServiceImpl implements
    KanbanOrderInformationFacadeService {

    /**
     * <p>
     * Call Service DensoSupplierRelationService.
     * </p>
     */
    private DensoSupplierRelationService densoSupplierRelationService;
    /**
     * <p>
     * Call Service CompanySupplierService.
     * </p>
     */
    private CompanySupplierService companySupplierService;

    /**
     * <p>
     * Call Service CompanyDensoService.
     * </p>
     */
    private CompanyDensoService densoCompanyService;

    /**
     * <p>
     * Call Service MiscService.
     * </p>
     */
    private MiscellaneousService miscService;

    /**
     * <p>
     * Call Service DeliveryOrderService.
     * </p>
     */
    private DeliveryOrderService deliveryOrderService;

    /**
     * <p>
     * Call Service RecordLimitService.
     * </p>
     */
    private RecordLimitService recordLimitService;

    /**
     * <p>
     * Call Service PlantSupplierService.
     * </p>
     */
    private PlantSupplierService plantSupplierService;

    /**
     * <p>
     * Call Service PlantDensoService.
     * </p>
     */
    private PlantDensoService plantDensoService;

    /**
     * <p>
     * Call Service FileManagementService.
     * </p>
     */
    private FileManagementService fileManagementService;

    /**
     * <p>
     * Call Service CommonService.
     * </p>
     */
    private CommonService commonService;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * <p>
     * The Constructor.
     * </p>
     */
    public KanbanOrderInformationFacadeServiceImpl() {
        super();
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationService.
     * </p>
     * 
     * @param densoSupplierRelationService Set for densoSupplierRelationService
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * <p>
     * Setter method for companySupplierService.
     * </p>
     * 
     * @param companySupplierService Set for companySupplierService
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }

    /**
     * <p>
     * Setter method for densoCompanyService.
     * </p>
     * 
     * @param densoCompanyService Set for densoCompanyService
     */
    public void setDensoCompanyService(CompanyDensoService densoCompanyService) {
        this.densoCompanyService = densoCompanyService;
    }

    /**
     * <p>
     * Setter method for miscService.
     * </p>
     * 
     * @param miscService Set for miscService
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }

    /**
     * <p>
     * Setter method for deliveryOrderService.
     * </p>
     * 
     * @param deliveryOrderService Set for deliveryOrderService
     */
    public void setDeliveryOrderService(
        DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }

    /**
     * <p>
     * Setter method for recordLimitService.
     * </p>
     * 
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * <p>
     * Setter method for plantSupplierService.
     * </p>
     * 
     * @param plantSupplierService Set for plantSupplierService
     */
    public void setPlantSupplierService(
        PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>
     * Setter method for plantDensoService.
     * </p>
     * 
     * @param plantDensoService Set for plantDensoService
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }

    /**
     * <p>
     * Setter method for fileManagementService.
     * </p>
     * 
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(
        FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * <p>
     * Setter method for commonService.
     * </p>
     * 
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#searchInitial(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public KanbanOrderInformationDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException
    {
        KanbanOrderInformationDomain kanbanOrderInformationDomain = 
            new KanbanOrderInformationDomain();
        Locale locale = dataScopeControlDomain.getLocale();

        /** Get relation between DENSO and Supplier. */
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            this.densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if (null == densoSupplierRelationList
            || densoSupplierRelationList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
        kanbanOrderInformationDomain.setDensoSupplierRelationList(densoSupplierRelationList);

        /** Get supplier company information from SupplierCompanyService. */
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanySupplierDomain> companySupplierDomainList = null;
        companySupplierDomainList = companySupplierService.searchCompanySupplier(
            plantSupplierWithScope);
        if (null == companySupplierDomainList
            || Constants.ZERO == companySupplierDomainList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }

        /** Get list of supplier plant information from SupplierPlantService. */
        List<PlantSupplierDomain> plantSupplierDomain = null;
        plantSupplierDomain = plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        if (null == plantSupplierDomain || plantSupplierDomain.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE, locale)});
        }

        /** Get DENSO company information from DENSOCompanyService. */
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoDomainList = null;
        companyDensoDomainList = densoCompanyService.searchCompanyDenso(plantDensoWithScope);
        if (null == companyDensoDomainList
            || Constants.ZERO == companyDensoDomainList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoDomain = null;
        plantDensoDomain = plantDensoService.searchPlantDenso(plantDensoWithScope);
        if (null == plantDensoDomain || plantDensoDomain.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }
        
        /** Get information for Revision Combo box from MiscellaneousService. */
        List<MiscellaneousDomain> miscRevisionList = new ArrayList<MiscellaneousDomain>();
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_REVISION_CB);
        miscRevisionList = miscService.searchMisc(miscDomain);

        StringBuffer addString = null;
        if (null == miscRevisionList || miscRevisionList.isEmpty()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_REVISION);
        }

        /**
         * Get information for Shipment Status Combo box from
         * MiscellaneousService.
         */
        List<MiscellaneousDomain> miscShipmentStatusCbList = new ArrayList<MiscellaneousDomain>();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SHIP_STS_CB);
        miscShipmentStatusCbList = miscService.searchMisc(miscDomain);

        if (Constants.ZERO < miscShipmentStatusCbList.size()) {
            for (MiscellaneousDomain shipmentStatus : miscShipmentStatusCbList) {
                addString = new StringBuffer();
                addString.append(shipmentStatus.getMiscCode());
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(Constants.SYMBOL_COLON);
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(shipmentStatus.getMiscValue());
                shipmentStatus.setMiscValue(addString.toString());
            }
        } else {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SHIPMENT_STATUS);
        }

        /**
         * Get information for Transport Mode Combo box from
         * MiscellaneousService.
         */
        List<MiscellaneousDomain> miscTransportModeCbList = new ArrayList<MiscellaneousDomain>();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_TRANS_CB);
        miscTransportModeCbList = miscService.searchMisc(miscDomain);

        if (Constants.ZERO < miscTransportModeCbList.size()) {
            for (MiscellaneousDomain transportMode : miscTransportModeCbList) {
                addString = new StringBuffer();
                addString.append(transportMode.getMiscCode());
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(Constants.SYMBOL_COLON);
                addString.append(Constants.SYMBOL_SPACE);
                addString.append(transportMode.getMiscValue());
                transportMode.setMiscValue(addString.toString());
            }
        } else {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_TRANSPORTATION_MODE);
        }

        kanbanOrderInformationDomain.setCompanySupplierList(companySupplierDomainList);
        kanbanOrderInformationDomain.setCompanyDensoList(companyDensoDomainList);
        kanbanOrderInformationDomain.setPlantSupplierList(plantSupplierDomain);
        kanbanOrderInformationDomain.setPlantDensoList(plantDensoDomain);
        kanbanOrderInformationDomain.setRevisionList(miscRevisionList);
        kanbanOrderInformationDomain.setShipmentStatusList(miscShipmentStatusCbList);
        kanbanOrderInformationDomain.setTransportModeList(miscTransportModeCbList);
        return kanbanOrderInformationDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#searchKANBANOrderInformation(com.globaldenso.asia.sps.business.domain.KANBANOrderInformationDomain)
     */
    public KanbanOrderInformationResultMasterDomain searchKanbanOrderInformation(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException {

        KanbanOrderInformationResultMasterDomain kanbanOrderInformationResultMasterDomain = 
            new KanbanOrderInformationResultMasterDomain();
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomainList = null;
        List<ApplicationMessageDomain> errorMessageList = null;
        Locale locale = kanbanOrderInformationDomain.getLocale();
        Integer recordCount = Constants.ZERO;
        Integer recordCountDisplay = Constants.ZERO;

        /** Validate input parameter. */

        errorMessageList = this.validateCriteria(kanbanOrderInformationDomain);
        if (Constants.ZERO < errorMessageList.size()) {
            kanbanOrderInformationResultMasterDomain.setErrorMessageList(errorMessageList);
            return kanbanOrderInformationResultMasterDomain;
        }
        
        kanbanOrderInformationDomain = toUpperCase(kanbanOrderInformationDomain);

        /** Count KANBAN Delivery Order return record from DeliveryOrderService. */
        recordCount = deliveryOrderService.searchCountKanbanOrder(kanbanOrderInformationDomain);
        if (Constants.ZERO == recordCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /** Get maximum record limit from RecordLimitService. */
        MiscellaneousDomain recordLimitParam = new MiscellaneousDomain();
        recordLimitParam.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
        recordLimitParam.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD007_RLM);
        MiscellaneousDomain recordLimit = recordLimitService.searchRecordLimit(recordLimitParam);
        if (null == recordLimit) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /** Check If recordCount > recordLimit. */
        if (Integer.valueOf(recordLimit.getMiscValue()) < recordCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {recordLimit.getMiscValue()});
        }

        /** Get maximum record per page from RecordLimitService. */
        MiscellaneousDomain recordLimitPerPageParam = new MiscellaneousDomain();
        recordLimitPerPageParam.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPageParam.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD007_PLM);
        MiscellaneousDomain recordLimitPerPage
            = recordLimitService.searchRecordLimitPerPage(recordLimitPerPageParam);
        if (null == recordLimitPerPage) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /**
         * Count KANBAN Delivery Order return record from DeliveryOrderService
         * to display at screen by D/O header data.
         */
        recordCountDisplay = deliveryOrderService
            .searchCountKanbanOrderHeader(kanbanOrderInformationDomain);

        if (Constants.ZERO == recordCountDisplay) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /** Calculate rownum for query. */
        kanbanOrderInformationDomain.setMaxRowPerPage(Integer
            .valueOf(recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(kanbanOrderInformationDomain,
            recordCountDisplay);

        /**
         * Get KANBAN Delivery Order data to display at screen from
         * DeliveryOrderService.
         */
        kanbanOrderInformationResultDomainList = deliveryOrderService
            .searchKanbanOrderHeader(kanbanOrderInformationDomain);

        /** Set <List> KANBANInformation to return Domain. */
        List<KanbanOrderInformationResultDomain> KanbanOrderInformationResultDomainPage = 
            new ArrayList<KanbanOrderInformationResultDomain>();
        for (KanbanOrderInformationResultDomain kanbanOrderResult
            : kanbanOrderInformationResultDomainList)
        {
            String utc = kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain().getUtc()
                .replace(Constants.SYMBOL_SPACE, Constants.EMPTY_STRING);

            Timestamp shipDatetime = kanbanOrderResult
                .getKanbanOrderInformationDoDetailReturnDomain().getShipDatetime();
            Timestamp deliveryDate = kanbanOrderResult
                .getKanbanOrderInformationDoDetailReturnDomain().getDeliveryDatetime();

            String shipDateFormat = DateUtil.format(shipDatetime,
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            String deliveryDateFormat = DateUtil.format(deliveryDate,
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);

            String shipDateUtc = StringUtil.appendsString(shipDateFormat,
                Constants.SYMBOL_OPEN_BRACKET, Constants.WORD_UTC, utc,
                Constants.SYMBOL_CLOSE_BRACKET);

            String deliveryDateUtc = StringUtil.appendsString(
                deliveryDateFormat, Constants.SYMBOL_OPEN_BRACKET,
                Constants.WORD_UTC, utc, Constants.SYMBOL_CLOSE_BRACKET);

            kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain()
                .setShipDatetimeUtc(shipDateUtc);
            kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain()
                .setDeliveryDatetimeUtc(deliveryDateUtc);

            // FIX : wrong dateformat
            if (null != kanbanOrderResult
                .getKanbanOrderInformationDoDetailReturnDomain().getDoIssueDate())
            {
                kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain()
                    .setDoIssueDateShow(DateUtil.format(kanbanOrderResult
                        .getKanbanOrderInformationDoDetailReturnDomain()
                            .getDoIssueDate(), DateUtil.PATTERN_YYYYMMDD_SLASH));
            }
          //SPS phase II additional requirement 2018-03-27
            //Get the receiving status on CIGMA
            List<String> densoCodeList = new ArrayList<String>();
            densoCodeList.add(kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain().getDCd());
            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
            
            SpsMCompanyDensoDomain companyDensoDomain = new SpsMCompanyDensoDomain();
            companyDensoDomain.setDCd(densoCodeStr);
            List<As400ServerConnectionInformationDomain> schemaResultList 
                = densoCompanyService.searchAs400ServerList(companyDensoDomain);
            if(null == schemaResultList || Constants.ZERO == schemaResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026);
            }
            if(densoCodeList.size() != schemaResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0025);
            }
            
            for(As400ServerConnectionInformationDomain as400Server : schemaResultList){
                String cigmaDoNumber = kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain().getCigmaDoNo();
                
                String receiveStatus = 
                    CommonWebServiceUtil.cigmaDoResourceSearchDoReceiving(cigmaDoNumber,
                        kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain().getShippingTotal(),
                        as400Server,
                        locale);
                kanbanOrderResult.getKanbanOrderInformationDoDetailReturnDomain().setReceiveStatus(receiveStatus);
            }
            
            KanbanOrderInformationResultDomainPage.add(kanbanOrderResult);
        }
        kanbanOrderInformationResultMasterDomain.setKanbanOrderInformationResultDomain(
            KanbanOrderInformationResultDomainPage);
        return kanbanOrderInformationResultMasterDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#searchKanbanOrderInformationCSV(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    public KanbanOrderInformationResultMasterDomain searchKanbanOrderInformationCsv(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException {
        KanbanOrderInformationResultMasterDomain kanbanOrderInformationResultMasterDomain = 
            new KanbanOrderInformationResultMasterDomain();
        List<KanbanOrderInformationResultDomain> KanbanOrderInformationResultDomainList = null;
        List<ApplicationMessageDomain> errorMessageList = null;
        CommonDomain commonDomain = new CommonDomain();
        Locale locale = kanbanOrderInformationDomain.getLocale();
        
        /** Validate input parameter. */
        try {
            errorMessageList = this.validateCriteria(kanbanOrderInformationDomain);
            
            if (Constants.ZERO < errorMessageList.size()) {
                kanbanOrderInformationResultMasterDomain.setErrorMessageList(errorMessageList);
                return kanbanOrderInformationResultMasterDomain;
            }
            kanbanOrderInformationDomain = toUpperCase(kanbanOrderInformationDomain);
            
            /** Count KANBAN Order return record from DeliveryOrderService. */
            int maxRecord = Constants.ZERO;
            maxRecord = deliveryOrderService.searchCountKanbanOrder(kanbanOrderInformationDomain);
            
            if (Constants.ZERO == maxRecord) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /** Get maximum record limit from RecordLimitService. */
            MiscellaneousDomain recordLimit = new MiscellaneousDomain();
            recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
            recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD007_RLM);
            recordLimit = recordLimitService.searchRecordLimit(recordLimit);
            if (null == recordLimit) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /** Check maxRecord is over recordLimit. */
            if (Integer.valueOf(recordLimit.getMiscValue()) < maxRecord) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {recordLimit.getMiscValue()});
            }
            
            /** Get KANBAN Order data from DeliveryOrderService. */
            KanbanOrderInformationResultDomainList = deliveryOrderService
                .searchKanbanOrder(kanbanOrderInformationDomain);
            
            kanbanOrderInformationResultMasterDomain
                .setKanbanOrderInformationResultDomain(KanbanOrderInformationResultDomainList);
            
            /** Create CSV file and set file to InputStream. */
            final String[] headerArr = new String[] {
                SupplierPortalConstant.LBL_ORDER_METHOD,
                SupplierPortalConstant.LBL_S_CD,
                SupplierPortalConstant.LBL_S_PCD,
                SupplierPortalConstant.LBL_D_CD,
                SupplierPortalConstant.LBL_D_PCD,
                SupplierPortalConstant.LBL_ISSUE_DATE,
                SupplierPortalConstant.LBL_DELIVERY_DATE,
                SupplierPortalConstant.LBL_DELIVERY_TIME,
                SupplierPortalConstant.LBL_SHIP_DATE,
                SupplierPortalConstant.LBL_SHIP_TIME,
                SupplierPortalConstant.LBL_TM,
                SupplierPortalConstant.LBL_ROUTE,
                SupplierPortalConstant.LBL_DEL,
                SupplierPortalConstant.LBL_ORIGINAL_SPS_DO,
                SupplierPortalConstant.LBL_PREVIOUS_SPS_DO,
                SupplierPortalConstant.LBL_CURRENT_SPS_DO_NO,
                SupplierPortalConstant.LBL_REVISION,
                SupplierPortalConstant.LBL_DO_SHIP_STATUS,
                SupplierPortalConstant.LBL_D_PN,
                SupplierPortalConstant.LBL_ITEM_DESC,
                SupplierPortalConstant.LBL_S_PN,
                SupplierPortalConstant.LBL_CURRENT_CIGMA_DO_NO,
                SupplierPortalConstant.LBL_PREVIOUS_QTY,
                SupplierPortalConstant.LBL_ORDER_QTY,
                SupplierPortalConstant.LBL_DIFFERENCE_QTY,
                SupplierPortalConstant.LBL_QTY_BOX,
                SupplierPortalConstant.LBL_NO_OF_BOX,
                SupplierPortalConstant.LBL_UNIT_OF_MEASURE,
                SupplierPortalConstant.LBL_RSN,
                SupplierPortalConstant.LBL_MODEL,
                SupplierPortalConstant.LBL_CTRL_NO,
                SupplierPortalConstant.LBL_WH_PRIME_RECEIVING,
                SupplierPortalConstant.LBL_DOCKCODE,
                SupplierPortalConstant.LBL_RECEIVING_LANE,
                SupplierPortalConstant.LBL_CYCLE,
                SupplierPortalConstant.LBL_KANBAN_SEQ,
                SupplierPortalConstant.LBL_KANBAN_TYPE,
                SupplierPortalConstant.LBL_RECEIVE_BY_SCAN};
            
            List<Map<String, Object>> resultDetail = this.doMapDetail(
                KanbanOrderInformationResultDomainList, headerArr);
            
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            StringBuffer resultString = new StringBuffer();
            resultString = commonService.createCsvString(commonDomain);
            
            kanbanOrderInformationResultMasterDomain.setResultString(resultString);
        } catch (IOException e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0012,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return kanbanOrderInformationResultMasterDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#findFileName(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    public FileManagementDomain searchFileName(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException {

        /**
         * Find file name by call Service FileManagementService
         * searchFileDownload() by pass 3 parameters as following.
         */
        FileManagementDomain resultDomain = null;
        try {
            resultDomain = fileManagementService.searchFileDownload(
                kanbanOrderInformationDomain.getPdfFileId(), false, null);
        } catch (IOException e) {
            Locale locale = kanbanOrderInformationDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        kanbanOrderInformationDomain.setPdfFileName(resultDomain.getFileName());

        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#searchKanbanOrderReport(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain,
     *      java.io.OutputStream)
     */
    public FileManagementDomain searchKanbanOrderReport(
        KanbanOrderInformationDomain kanbanOrderInformationDomain,
        OutputStream output) throws ApplicationException {

        /**
         * Get data by call Service FileManagementService searchFileDownload()
         * by pass 3 parameters as following.
         */
        FileManagementDomain resultDomain = null;
        try {
            resultDomain = fileManagementService.searchFileDownload(
                kanbanOrderInformationDomain.getPdfFileId(), true, output);
        } catch (IOException e) {
            Locale locale = kanbanOrderInformationDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#changeSelectSupplierCompany(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException
    {
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

        dataScopeControlDomain.setUserType(plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(
            plantSupplierWithScopeDomain.getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        
        if(!StringUtil.checkNullOrEmpty(plantSupplierWithScopeDomain.getPlantSupplierDomain()
            .getVendorCd()))
        {
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd().toUpperCase());
        }
        
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
            dataScopeControlDomain.getUserRoleDomainList());
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of supplier plant information from SupplierPlantService. */
        List<PlantSupplierDomain> spsMPlantSupplierDomain = null;
        spsMPlantSupplierDomain = plantSupplierService.searchPlantSupplier(
            plantSupplierWithScopeDomain);
        if (null == spsMPlantSupplierDomain || spsMPlantSupplierDomain.size() <= Constants.ZERO) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE, locale)});
        }
        return spsMPlantSupplierDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

        dataScopeControlDomain.setUserType(plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain
            .setUserRoleDomainList(plantSupplierWithScopeDomain
                .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        if(!StringUtil.checkNullOrEmpty(
            plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd()))
        {
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd().toUpperCase());
        }
       
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
            dataScopeControlDomain.getUserRoleDomainList());
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        
        /** Get supplier company information from SupplierCompanyService. */
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<CompanySupplierDomain> companySupplierDomain = null;
        companySupplierDomain = companySupplierService
            .searchCompanySupplier(plantSupplierWithScopeDomain);
        if (null == companySupplierDomain
            || Constants.ZERO == companySupplierDomain.size()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE, locale)});
        }

        return companySupplierDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#changeSelectDensoCompany(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException
    {
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getDensoSupplierRelationDomainList());

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));

        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantDensoWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        
        if(!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            dataScopeControlDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
            dataScopeControlDomain.getUserRoleDomainList());
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
        plantDensoDomain = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if (null == plantDensoDomain
            || plantDensoDomain.size() <= Constants.ZERO) {
            Locale locale = plantDensoWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }
        return plantDensoDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        Locale locale = plantDensoWithScopeDomain.getLocale();

        dataScopeControlDomain.setUserType(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getDensoSupplierRelationDomainList());

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));

        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        if(!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            dataScopeControlDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setUserRoleDomainList(dataScopeControlDomain.getUserRoleDomainList());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get DENSO company information from DensoCompanyService. */
        List<CompanyDensoDomain> companyDensoDomain = null;
        companyDensoDomain = densoCompanyService.searchCompanyDenso(plantDensoWithScopeDomain);
        if (null == companyDensoDomain
            || Constants.ZERO == companyDensoDomain.size()) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }
        
        return companyDensoDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain,
     *      java.io.OutputStream)
     */
    public FileManagementDomain searchLegendInfo(
        KanbanOrderInformationDomain kanbanOrderInformationDomain, OutputStream output)
        throws ApplicationException
    {
        /** Get PDF file from FileManagementService. */
        FileManagementDomain resultDomain = null;
        try {
            resultDomain = fileManagementService.searchFileDownload(
                kanbanOrderInformationDomain.getPdfFileId(), true, output);
        } catch (IOException e) {
            Locale locale = kanbanOrderInformationDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }

    /**
     * <p>
     * Validate Criteria method.
     * </p>
     * 
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    private List<ApplicationMessageDomain> validateCriteria(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException {

        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = kanbanOrderInformationDomain.getLocale();
        boolean issueDateFrom = false;
        boolean issueDateTo = false;
        boolean deliveryDateFrom = false;
        boolean deliveryDateTo = false;
        boolean shipDateFrom = false;
        boolean shipDateTo = false;

        /** Search criteria are empty. */
        if (StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getVendorCd())
            || StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getSPcd())
            || StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDCd())
            || StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDPcd())
            || StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getIssueDateFrom())
            || StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getIssueDateTo()))
        {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil
                    .getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        } else {
            if (kanbanOrderInformationDomain.getVendorCd().equals(Constants.MISC_CODE_ALL)) {
                kanbanOrderInformationDomain.setVendorCd(Constants.EMPTY_STRING);
            }
            if (kanbanOrderInformationDomain.getSPcd().equals(Constants.MISC_CODE_ALL)) {
                kanbanOrderInformationDomain.setSPcd(Constants.EMPTY_STRING);
            }
            if (kanbanOrderInformationDomain.getDCd().equals(Constants.MISC_CODE_ALL)) {
                kanbanOrderInformationDomain.setDCd(Constants.EMPTY_STRING);
            }
            if (kanbanOrderInformationDomain.getDPcd().equals(Constants.MISC_CODE_ALL)) {
                kanbanOrderInformationDomain.setDPcd(Constants.EMPTY_STRING);
            }
        }

        /** Check format of Issue Date From. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getIssueDateFrom())) {
            if (!DateUtil.isValidDate(kanbanOrderInformationDomain.getIssueDateFrom()
                , DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                    , MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_ISSUE_DATE_FROM, locale)})));
                issueDateFrom = false;
            } else {
                issueDateFrom = true;
            }
        }

        /** Check format of Issue Date To. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getIssueDateTo())) {
            if (!DateUtil.isValidDate(kanbanOrderInformationDomain.getIssueDateTo()
                , DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_ISSUE_DATE_TO, locale)})));
                issueDateTo = false;
            } else {
                issueDateTo = true;
            }
        }

        /** Check format of Delivery Date From. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDeliveryDateFrom())) {
            if (!DateUtil.isValidDate(kanbanOrderInformationDomain.getDeliveryDateFrom()
                , DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_DELIVERY_DATE_FROM, locale)})));
                deliveryDateFrom = false;
            } else {
                deliveryDateFrom = true;
            }
        }

        /** Check format of Delivery Date To. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDeliveryDateTo())) {
            if (!DateUtil.isValidDate(kanbanOrderInformationDomain.getDeliveryDateTo()
                , DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_DELIVERY_DATE_TO, locale)})));
                deliveryDateTo = false;
            } else {
                deliveryDateTo = true;
            }
        }

        /** Check format and compare from to Delivery Time. */
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDeliveryTimeFrom())
            && !StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDeliveryTimeTo()))
        {
            boolean isValidDeliveryTimeFrom = true;
            boolean isValidDeliveryTimeTo = true;
            if(!kanbanOrderInformationDomain.getDeliveryTimeFrom().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_TIME_FROM)));
                isValidDeliveryTimeFrom = false;
            }
            if(!kanbanOrderInformationDomain.getDeliveryTimeTo().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_TIME_TO)));
                isValidDeliveryTimeTo = false;
            }
            if(isValidDeliveryTimeFrom && isValidDeliveryTimeTo){
                Date d1 = DateUtil.parseToUtilDate(
                    kanbanOrderInformationDomain.getDeliveryTimeFrom()
                    , DateUtil.PATTERN_HHMM_COLON);
                Date d2 = DateUtil.parseToUtilDate(
                    kanbanOrderInformationDomain.getDeliveryTimeTo()
                    , DateUtil.PATTERN_HHMM_COLON);
                if(d1.after(d2)){
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_DELIVERY_TIME_FROM),
                                    MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_DELIVERY_TIME_TO)})));
                }
            }
        }else{
            if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDeliveryTimeFrom())
                || !StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDeliveryTimeTo()))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                        SupplierPortalConstant.LBL_BOTH_DELIVERY_TIME_FROM_AND_TO)));
            }
        }
        
        /** Check format of Ship Date From. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getShipDateFrom())) {
            if (!DateUtil.isValidDate(kanbanOrderInformationDomain.getShipDateFrom()
                , DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_SHIP_DATE_FORM, locale)})));
                shipDateFrom = false;
            } else {
                shipDateFrom = true;
            }
        }

        /** Check format of Ship Date To. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain
            .getShipDateTo())) {
            if (!DateUtil.isValidDate(
                kanbanOrderInformationDomain.getShipDateTo(),
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {

                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                                new String[] {getLabel(
                                    SupplierPortalConstant.LBL_SHIP_DATE_TO,
                                    locale)})));
                shipDateTo = false;

            } else {
                shipDateTo = true;
            }
        }

        /** Check Issue Date From is rather than Issue Date To. */
        if (issueDateFrom && issueDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                kanbanOrderInformationDomain.getIssueDateFrom(),
                kanbanOrderInformationDomain.getIssueDateTo())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                            new String[] {
                                getLabel(
                                    SupplierPortalConstant.LBL_ISSUE_DATE_FROM,
                                    locale),
                                getLabel(
                                    SupplierPortalConstant.LBL_ISSUE_DATE_TO,
                                    locale)})));

            }

        }

        /** Check Delivery Date From is rather than Delivery Date To. */
        if (deliveryDateFrom && deliveryDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                kanbanOrderInformationDomain.getDeliveryDateFrom(),
                kanbanOrderInformationDomain.getDeliveryDateTo())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                        new String[] {
                            getLabel(
                                SupplierPortalConstant.LBL_DELIVERY_DATE_FROM,
                                locale),
                            getLabel(
                                SupplierPortalConstant.LBL_DELIVERY_DATE_TO,
                                locale)})));

            }
        }

        /** Check Ship Date From is rather than Ship Date To. */
        if (shipDateFrom && shipDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                kanbanOrderInformationDomain.getShipDateFrom(),
                kanbanOrderInformationDomain.getShipDateTo())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                            new String[] {
                                getLabel(
                                    SupplierPortalConstant.LBL_SHIP_DATE_FORM,
                                    locale),
                                getLabel(
                                    SupplierPortalConstant.LBL_SHIP_DATE_TO,
                                    locale)})));

            }
        }

        /** Check Length of Route No. is exceed the length of field in table. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain
            .getRouteNo())) {
            if (Constants.SIX < kanbanOrderInformationDomain.getRouteNo()
                .length()) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                            new String[] {
                                getLabel(SupplierPortalConstant.LBL_ROUTE_NO,
                                    locale), String.valueOf(Constants.SIX)})));

            }
        }

        /**
         * Check Length of Truck Sequence is exceed the length of field in
         * table.
         */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDel())) {
            if (Constants.THREE < kanbanOrderInformationDomain.getDel()
                .length()) {

                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                                new String[] {
                                    getLabel(
                                        SupplierPortalConstant.RPT_PRM_DEL,
                                        locale),
                                    String.valueOf(Constants.THREE)})));

            }
        }

        /** Length of SPS D/O No. is exceed the length of field in table. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain
            .getSpsDoNo())) {
            if (Constants.ELEVEN < kanbanOrderInformationDomain.getSpsDoNo()
                .length()) {

                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                                new String[] {
                                    getLabel(SupplierPortalConstant.LBL_DO_NO,
                                        locale),
                                    String.valueOf(Constants.ELEVEN)})));

            }
        }

        /** Length of CIGMA D/O No. is exceed the length of field in table. */
        if (!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain
            .getCigmaDoNo())) {
            if (Constants.TEN < kanbanOrderInformationDomain.getCigmaDoNo()
                .length()) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                            new String[] {
                                getLabel(
                                    SupplierPortalConstant.LBL_CIGMA_DO_NO,
                                    locale), String.valueOf(Constants.TEN)})));

            }
        }
        return errorMessageList;
    }

    /**
     * <p>
     * Map Detail.
     * </p>
     * 
     * @param kanbanOrderInformationResultDomain
     *            kanbanOrderInformationResultDomain
     * @param header header
     * @return List
     */
    private List<Map<String, Object>> doMapDetail(
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain,
        String[] header)
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> kanbanOrderMap = null;
        KanbanOrderInformationDoDetailReturnDomain doHeader = null;
        String doIssueDate = null;
        String deliveryDate = null;
        String deliveryTime = null;
        String shipDate = null;
        String shipTime = null;
        String itemDesc = null;
        
        for (KanbanOrderInformationResultDomain kanbanHeaderTmp
            : kanbanOrderInformationResultDomain) {
            for (KanbanOrderInformationDoDetailListReturnDomain doDetail
                : kanbanHeaderTmp.getKanbanOrderInformationDoDetailListReturnDomain())
            {
                doIssueDate = Constants.EMPTY_STRING;
                deliveryDate = Constants.EMPTY_STRING;
                deliveryTime = Constants.EMPTY_STRING;
                shipDate = Constants.EMPTY_STRING;
                shipTime = Constants.EMPTY_STRING;
                itemDesc = Constants.EMPTY_STRING;
                doHeader = kanbanHeaderTmp.getKanbanOrderInformationDoDetailReturnDomain();
                
                if(null != doHeader.getDoIssueDate()){
                    doIssueDate = DateUtil.format(doHeader.getDoIssueDate(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                }
                
                if(null != doHeader.getDeliveryDatetime()){
                    deliveryDate = DateUtil.format(doHeader.getDeliveryDatetime(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                    deliveryTime = DateUtil.format(doHeader.getDeliveryDatetime(),
                        DateUtil.PATTERN_HHMM_COLON);
                }
                
                if(null != doHeader.getShipDatetime()){
                    shipDate = DateUtil.format(doHeader.getShipDatetime(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                    shipTime = DateUtil.format(doHeader.getShipDatetime(),
                        DateUtil.PATTERN_HHMM_COLON);
                }
                
                if(!StringUtil.checkNullOrEmpty(doDetail.getItemDesc())){
                    itemDesc = StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE,
                        doDetail.getItemDesc(), Constants.SYMBOL_DOUBLE_QUOTE);
                }
                
                kanbanOrderMap = new HashMap<String, Object>();
                kanbanOrderMap.put(header[Constants.ZERO],
                    StringUtil.checkNullToEmpty(doHeader.getOrderMethod()));
                kanbanOrderMap.put(header[Constants.ZERO],
                    StringUtil.checkNullToEmpty(doHeader.getOrderMethod()));
                kanbanOrderMap.put(header[Constants.ONE],
                    StringUtil.checkNullToEmpty(doHeader.getVendorCd()));
                kanbanOrderMap.put(header[Constants.TWO],
                    StringUtil.checkNullToEmpty(doHeader.getSPcd()));
                kanbanOrderMap.put(header[Constants.THREE],
                    StringUtil.checkNullToEmpty(doHeader.getDCd()));
                kanbanOrderMap.put(header[Constants.FOUR],
                    StringUtil.checkNullToEmpty(doHeader.getDPcd()));
                kanbanOrderMap.put(header[Constants.FIVE], doIssueDate);
                kanbanOrderMap.put(header[Constants.SIX], deliveryDate);
                kanbanOrderMap.put(header[Constants.SEVEN], deliveryTime);
                kanbanOrderMap.put(header[Constants.EIGHT], shipDate);
                kanbanOrderMap.put(header[Constants.NINE], shipTime);
                kanbanOrderMap.put(header[Constants.TEN],
                    StringUtil.checkNullToEmpty(doHeader.getTm()));
                kanbanOrderMap.put(header[Constants.ELEVEN],
                    StringUtil.checkNullToEmpty(doHeader.getTruckRoute()));
                kanbanOrderMap.put(header[Constants.TWELVE],
                    StringUtil.checkNullToEmpty(doHeader.getDel()));
                
                /* Start : [ADDITIONAL] Mr. Sittichai request for add revision to Original D/O No.
                 * and Previous D/O No.
                 * */ 
                //kanbanOrderMap.put(header[Constants.THIRTEEN],
                //    StringUtil.checkNullToEmpty(doHeader.getSpsDoNo()));
                //kanbanOrderMap.put(header[Constants.FOURTEEN],
                //    StringUtil.checkNullToEmpty(doHeader.getPreviousSpsDoNo()));
                kanbanOrderMap.put(header[Constants.THIRTEEN],
                    StringUtil.appendsString(StringUtil.checkNullToEmpty(doHeader.getSpsDoNo())
                        , Constants.DEFAULT_REVISION));
                if (Strings.judgeBlank(doHeader.getPreviousSpsDoNo())) {
                    kanbanOrderMap.put(header[Constants.FOURTEEN], Constants.EMPTY_STRING);
                } else {
                    String previousSpsDo = doHeader.getPreviousSpsDoNo().trim();
                    if (previousSpsDo.length() <= Constants.NINE) {
                        kanbanOrderMap.put(header[Constants.FOURTEEN]
                            , StringUtil.appendsString(previousSpsDo, Constants.DEFAULT_REVISION));
                    } else {
                        kanbanOrderMap.put(header[Constants.FOURTEEN], previousSpsDo);
                    }
                }
                // END : [ADDITIONAL]
                 
                kanbanOrderMap.put(header[Constants.FIFTEEN],
                    StringUtil.checkNullToEmpty(doHeader.getCurrentSpsDoNo()));
                kanbanOrderMap.put(header[Constants.SIXTEEN], doHeader.getRevision());
                kanbanOrderMap.put(header[Constants.SEVENTEEN], doHeader.getShipmentStatus());
                kanbanOrderMap.put(header[Constants.EIGHTEEN], doDetail.getDPn());
                kanbanOrderMap.put(header[Constants.NINETEEN], itemDesc);
                kanbanOrderMap.put(header[Constants.TWENTY], doDetail.getSPn());
                
                /* [IN042] : Current CIGMA D/O No. is CHG CIGMA D/O No. in DO_DETAIL (if empty use
                 * CIGMA_DO_NO)
                 * */ 
                //kanbanOrderMap.put(header[Constants.TWENTY_ONE], doHeader.getCurrentCigmaDoNo());
                kanbanOrderMap.put(header[Constants.TWENTY_ONE], doHeader.getCigmaDoNo());
                if (!Strings.judgeBlank(doDetail.getChgCigmaDoNo())) {
                    kanbanOrderMap.put(header[Constants.TWENTY_ONE], doDetail.getChgCigmaDoNo());
                }
                
                kanbanOrderMap.put(header[Constants.TWENTY_TWO],
                    StringUtil.checkNullToEmpty(doDetail.getPreviousQty()));
                kanbanOrderMap.put(header[Constants.TWENTY_THREE],
                    StringUtil.checkNullToEmpty(doDetail.getCurrentOrderQty()));
                
                if(Constants.DEFAULT_REVISION.equals(doDetail.getPnRevision())) {
                    kanbanOrderMap.put(header[Constants.TWENTY_FOUR], Constants.STR_ZERO);
                }else{
                    BigDecimal diffQty = new BigDecimal(Constants.ZERO);
                    BigDecimal orderQty = NumberUtil.toBigDecimalDefaultZero(
                        doDetail.getCurrentOrderQty());
                    BigDecimal previousQty = NumberUtil.toBigDecimalDefaultZero(
                        doDetail.getPreviousQty());
                    
                    diffQty = orderQty.subtract(previousQty);
                    kanbanOrderMap.put(header[Constants.TWENTY_FOUR], diffQty.toString());
                }
                
                kanbanOrderMap.put(header[Constants.TWENTY_FIVE],
                    StringUtil.checkNullToEmpty(doDetail.getQtyBox()));
                kanbanOrderMap.put(header[Constants.TWENTY_SIX],
                    StringUtil.checkNullToEmpty(doDetail.getNoOfBoxes()));
                kanbanOrderMap.put(header[Constants.TWENTY_SEVEN],
                    StringUtil.checkNullToEmpty(doDetail.getUnitOfMeasure()));
                kanbanOrderMap.put(header[Constants.TWENTY_EIGHT],
                    StringUtil.checkNullToEmpty(doDetail.getChgReason()));
                kanbanOrderMap.put(header[Constants.TWENTY_NINE],
                    StringUtil.checkNullToEmpty(doDetail.getModel()));
                kanbanOrderMap.put(header[Constants.THIRTY],
                    StringUtil.checkNullToEmpty(doDetail.getCtrlNo()));
                kanbanOrderMap.put(header[Constants.THIRTY_ONE],
                    StringUtil.checkNullToEmpty(doHeader.getWhPrimeReceiving()));
                kanbanOrderMap.put(header[Constants.THIRTY_TWO],
                    StringUtil.checkNullToEmpty(doHeader.getDockCode()));
                kanbanOrderMap.put(header[Constants.THIRTY_THREE],
                    StringUtil.checkNullToEmpty(doDetail.getRcvLane()));
                kanbanOrderMap.put(header[Constants.THIRTY_FOUR],
                    StringUtil.checkNullToEmpty(doHeader.getCycle()));
                /*
                if (null !=  doDetail.getKanbanSeq() && !doDetail.getKanbanSeq().isEmpty()) {
                    StringBuffer buffer = new StringBuffer();
                    for (int i = Constants.ZERO; i < doDetail.getKanbanSeq().size(); ++i) {
                        if (Constants.ZERO == i) {
                            buffer.append(doDetail.getKanbanSeq().get(i).getKanbanSeq());
                        } else {
                            buffer.append(Constants.SYMBOL_COMMA);
                            
                            // [IN020] avoid EXCEL auto format
                            buffer.append(Constants.SYMBOL_SPACE);
                            
                            buffer.append(doDetail.getKanbanSeq().get(i).getKanbanSeq());
                        }
                    }
                    kanbanOrderMap.put(header[Constants.THIRTY_FIVE],
                        StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE, buffer.toString(),
                            Constants.SYMBOL_DOUBLE_QUOTE));
                }else{
                    kanbanOrderMap.put(header[Constants.THIRTY_FIVE], Constants.EMPTY_STRING);
                }
                */
                if(StringUtil.checkNullOrEmpty(doDetail.getKanbanTagSequence())){
                    kanbanOrderMap.put(header[Constants.THIRTY_FIVE], Constants.EMPTY_STRING);
                } else {
                    kanbanOrderMap.put(header[Constants.THIRTY_FIVE],
                        StringUtil.checkNullToEmpty(
                            StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE, 
                                doDetail.getKanbanTagSequence(), 
                                Constants.SYMBOL_DOUBLE_QUOTE)));
                }
                kanbanOrderMap.put(header[Constants.THIRTY_SIX],
                    StringUtil.checkNullToEmpty(doDetail.getKanbanType()));
                kanbanOrderMap.put(header[Constants.THIRTY_SEVEN],
                    StringUtil.checkNullToEmpty(doHeader.getReceiveByScan()));
                resultList.add(kanbanOrderMap);
            }
        }
        return resultList;
    }

    /**
     * <p>Converts all of the characters in this String to upper case using the rules of the default locale.</p>
     *
     * @param kanbanOrderInformationDomain KanbanOrderInformation Domain
     * @return kanbanOrderInformationDomain
     */
    private KanbanOrderInformationDomain toUpperCase(KanbanOrderInformationDomain 
        kanbanOrderInformationDomain){
        
        kanbanOrderInformationDomain.setVendorCd(kanbanOrderInformationDomain.
            getVendorCd().toUpperCase());
       
        kanbanOrderInformationDomain.setSPcd(kanbanOrderInformationDomain.
            getSPcd().toUpperCase());
      
        kanbanOrderInformationDomain.setDCd(kanbanOrderInformationDomain.
            getDCd().toUpperCase());
        
        kanbanOrderInformationDomain.setDPcd(kanbanOrderInformationDomain.
            getDPcd().toUpperCase());
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getRevision())){
            kanbanOrderInformationDomain.setRevision(kanbanOrderInformationDomain.
                getRevision().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.
            getShipmentStatus())){
            kanbanOrderInformationDomain.setShipmentStatus(kanbanOrderInformationDomain.
                getShipmentStatus().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.
            getTransportMode())){
            kanbanOrderInformationDomain.setTransportMode(kanbanOrderInformationDomain.
                getTransportMode().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.
            getRouteNo())){
            kanbanOrderInformationDomain.setRouteNo(kanbanOrderInformationDomain.
                getRouteNo().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getDel())){
            kanbanOrderInformationDomain.setDel(
                kanbanOrderInformationDomain.getDel().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getSpsDoNo())){
            kanbanOrderInformationDomain.setSpsDoNo(kanbanOrderInformationDomain.
                getSpsDoNo().toUpperCase());
        }
        
        if(!StringUtil.checkNullOrEmpty(kanbanOrderInformationDomain.getCigmaDoNo())){
            kanbanOrderInformationDomain.setCigmaDoNo(kanbanOrderInformationDomain.
                getCigmaDoNo().toUpperCase());
        }
        
        return kanbanOrderInformationDomain;
    }
    
    /**
     * <p>
     * Get Label method.
     * </p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {
        String labelString = MessageUtil.getLabelHandledException(locale, key);
        return labelString;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#updateDateTimeKanbanPdf(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer updateDateTimeKanbanPdf(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException {
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
        deliveryOrderInformationDomain.setDoId(kanbanOrderInformationDomain.getDoId());
        /** Count Delivery Order return record from DeliveryOrderService. */
        Integer resultCode = deliveryOrderService.updateDateTimeKanbanPdf(deliveryOrderInformationDomain);
        return resultCode;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#createOneWayKanbanTagReport(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public InputStream createOneWayKanbanTagReport(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException {
        
        InputStream report = null;
        Locale locale = kanbanOrderInformationDomain.getLocale();
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
        deliveryOrderInformationDomain.setDoId(kanbanOrderInformationDomain.getDoId());
        List<OneWayKanbanTagReportDomain> oneWayKanbanTagReportDomain = deliveryOrderService
            .searchOneWayKanbanTagReportInformation(deliveryOrderInformationDomain);
        try {
            report = deliveryOrderService.createOneWayKanbanTagReport(oneWayKanbanTagReportDomain);
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0017);
        }
        return report;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchGenerateDo(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public FileManagementDomain searchGenerateDo(
        KanbanOrderInformationDomain kanbanOrderInformationDomain) throws ApplicationException
    {
        BigDecimal doId = new BigDecimal(kanbanOrderInformationDomain.getPdfFileId());
        Locale locale = kanbanOrderInformationDomain.getLocale();
//        Generate Report.
        InputStream isReportData = null;
        StringBuffer filenameReport = new StringBuffer();
        try {
            KanbanOrderInformationDoDetailReturnDomain kanbanReport = new KanbanOrderInformationDoDetailReturnDomain();
            kanbanReport.setDoId( doId.toString() );
            isReportData = deliveryOrderService.searchKanbanDeliveryOrderReport( kanbanReport );
            filenameReport.append(SupplierPortalConstant.KANBAN);
            filenameReport.append(doId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(
                locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0013,
                null );
        }
        FileManagementDomain resultDomain = new FileManagementDomain();
        resultDomain = new FileManagementDomain();
        resultDomain.setFileData(isReportData);
        resultDomain.setFileName(filenameReport.toString());
        return resultDomain;
    }
}
