/*
 * ModifyDate Development company    Describe 
 * 2014/06/12 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.dao.CompanyDensoDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;


/**
 * <p>The Class CompanyDensoServiceImpl.</p>
 * <p>Manage data of Company Denso.</p>
 * <ul>
 * <li>Method search  : searchCompanyDensoDetail</li>
 * <li>Method search  : searchDensoCompany</li>
 * <li>Method search  : searchAS400ServerList</li>
 * <li>Method search  : searchDensoCompanyByCodeList</li>
 * </ul>
 *
 * @author CSI
 */
public class CompanyDensoServiceImpl implements CompanyDensoService {
    
    /** The company denso dao. */
    private CompanyDensoDao companyDensoDao;
    
    /**
     * Instantiates a new PO service impl.
     */
    public CompanyDensoServiceImpl(){
        super();
    }
    
    /**
     * Sets the company denso dao.
     * 
     * @param companyDensoDao the new company denso dao
     */
    public void setCompanyDensoDao(CompanyDensoDao companyDensoDao) {
        this.companyDensoDao = companyDensoDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanyDensoService#searchCompanyDensoDetail
     * (com.globaldenso.asia.sps.business.domain.CompanyDensoDomain)
     */
    public List<CompanyDensoDomain> searchCompanyDensoDetail(){
        List<CompanyDensoDomain> result =  companyDensoDao.searchCompanyDensoDetail();
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanyDensoService#searchDensoCompany
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScope)
    {
        List<CompanyDensoDomain> companyDensoList = null;
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(
            plantDensoWithScope.getDataScopeControlDomain().getUserType()))
        {
            companyDensoList = this.companyDensoDao.searchCompanyDensoByRole(plantDensoWithScope);
        } else {
            companyDensoList = this.companyDensoDao.searchCompanyDensoByRelation(
                plantDensoWithScope);
        }
        return companyDensoList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanyDensoService#searchAs400ServerList
     * (com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain companyDensoDomain)
    {
        List<As400ServerConnectionInformationDomain> as400ServerInformationList = null;
        as400ServerInformationList = this.companyDensoDao.searchAs400ServerConnection(
            companyDensoDomain);
        return as400ServerInformationList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanyDensoService#searchDensoCompanyByCodeList
     * (com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public List<SpsMCompanyDensoDomain> searchCompanyDensoByCodeList(
        SpsMCompanyDensoDomain companyDensoDomain){
        
        List<SpsMCompanyDensoDomain> spsMCompanyDensoList = null;
        spsMCompanyDensoList = this.companyDensoDao.searchCompanyDensoByCodeList(
            companyDensoDomain);
        return spsMCompanyDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanySupplierService#searchExistCompanyDenso
     * (com.globaldenso.asia.sps.business.domain.companyDensoWithScopeDomain)
     */
    public Integer searchExistCompanyDenso(
        CompanyDensoWithScopeDomain companyDensoWithScopeDomain)
    {
        Integer recordCount = 0;
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(
            companyDensoWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            recordCount = this.companyDensoDao.searchExistCompanyDensoByRole(
                companyDensoWithScopeDomain);
        }else{
            recordCount = this.companyDensoDao.searchExistCompanyDensoByRelation(
                companyDensoWithScopeDomain);
        }
        return recordCount;
    }
}