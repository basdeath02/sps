/*
 * ModifyDate Development company     Describe 
 * 2014/06/06 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.dao.UserDao;


/**
 * <p>The Class UserServiceImpl.</p>
 * <p>Manage data of User.</p>
 * <ul>
 * <li>Method delete  : deleteSupplierUserDetail</li>
 * <li>Method update  : updateSupplierUserDetail</li>
 * <li>Method search  : searchEmailUserDenso</li>
 * </ul>
 *
 * @author CSI
 */
public class UserServiceImpl implements UserService {
    
    /** The  user dao. */
    private UserDao userDao;
    
    /**
     * Instantiates a new User service impl.
     */
    public UserServiceImpl(){
        super();
    }
    
    /**
     * Sets the user dao.
     * 
     * @param userDao the new user dao
     */
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserService#updateUser(com.globaldenso.asia.sps.business.domain.supplierUserDomain)
     */
    public List<SpsMUserDomain> searchDepartmentName(){
        List<SpsMUserDomain> departmentList = new ArrayList<SpsMUserDomain>();
        departmentList = userDao.searchDepartmentName();
        return departmentList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserService#searchEmailUserDenso(com.globaldenso.asia.sps.business.domain.supplierUserDomain)
     */
    public List<SpsMUserDomain> searchEmailUserDenso(SpsMUserDensoDomain userDensoDomain){
        List<SpsMUserDomain> userResultList = null;
        userResultList = userDao.searchEmailUserDenso(userDensoDomain);
        return userResultList;
    }
}