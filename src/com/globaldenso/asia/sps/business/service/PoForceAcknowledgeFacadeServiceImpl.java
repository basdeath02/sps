/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Chatchai        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.business.domain.PoForceAcknowledgeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * <p>
 * Update Acknowledge Flag when Supplier didn’t confirm in time.
 * </p>
 * <p>
 * Facade for POForceAcknowledgeFacadeServiceImpl.
 * </p>
 * <ul>
 * <li>Method create : updateForceAcknowledgePO</li>
 * <li>Method create : transactUpload</li>
 * <li>Method create : searchForceAcknowledgePO</li>
 * </ul>
 * 
 * @author CSI
 */
public class PoForceAcknowledgeFacadeServiceImpl implements
    PoForceAcknowledgeFacadeService {

    /** Call Service SpsTPoService. */
    private SpsTPoService spsTPoService;

    /** Call Service SpsTPoDetailService. */
    private SpsTPoDetailService spsTPoDetailService;

    /** Call Service CommonService. */
    private CommonService commonService;

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService#transactUpload(com.globaldenso.asia.sps.business.domain.FileManagementDomain,
     *      com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public PoForceAcknowledgeFacadeServiceImpl() {
        super();
    }

    /**
     * <p>
     * Setter method for spsTPoService.
     * </p>
     * 
     * @param spsTPoService Set for spsTPoService
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }

    /**
     * <p>
     * Setter method for spsTPoDetailService.
     * </p>
     * 
     * @param spsTPoDetailService Set for spsTPoDetailService
     */
    public void setSpsTPoDetailService(SpsTPoDetailService spsTPoDetailService) {
        this.spsTPoDetailService = spsTPoDetailService;
    }

    /**
     * <p>
     * Setter method for commonService.
     * </p>
     * 
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNService#searchCountPriceDifferenceInformation(com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationCriteriaDomain)
     */
    public List<SpsTPoDomain> searchForceAcknowledgePo(
        SpsTPoCriteriaDomain poCriteria) throws ApplicationException {
        /** Call service SpsTPoService searchByCondition(). */
        List<SpsTPoDomain> spsTPoDomainList = spsTPoService
            .searchByCondition(poCriteria);
        return spsTPoDomainList;

    }

    /**
     * {@inheritDoc}
     * 
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService#transactUpload(com.globaldenso.asia.sps.business.domain.FileManagementDomain,
     *      com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public void transactForceAcknowledgePo(
        PoForceAcknowledgeDomain poForceAcknowledgeDomain)
        throws ApplicationException {

        Locale locale = poForceAcknowledgeDomain.getLocale();

        List<SpsTPoDomain> poList = poForceAcknowledgeDomain
            .getSpsTPoDomainList();
        List<SpsTPoCriteriaDomain> poCriList = poForceAcknowledgeDomain
            .getSpsTPoCriteriaDomainList();

        List<SpsTPoDetailDomain> poDetailList = poForceAcknowledgeDomain
            .getSpsTPoDetailDomainList();
        List<SpsTPoDetailCriteriaDomain> poDetailCriList = poForceAcknowledgeDomain
            .getSpsTPoDetailCriteriaDomainList();

        /** Call service SpsTPoService update() for update P/O header with key --> PO_ID by pass parameter with 1 arg (<List>Domain). */
        Timestamp lastUpdateDatetime = commonService.searchSysDate();

        int recordCount = Constants.ZERO;

        for (int i = Constants.ZERO; i < poList.size(); ++i) {
            SpsTPoDomain tPo = poList.get(i);
            tPo.setLastUpdateDatetime(lastUpdateDatetime);
            SpsTPoCriteriaDomain tPoCri = poCriList.get(i);
            recordCount = spsTPoService.updateByCondition(tPo, tPoCri);
            if (Constants.ONE != recordCount) {
                MessageUtil.throwsApplicationMessage(
                    locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {getLabel(
                        SupplierPortalConstant.LBL_UPDATE_USER_INFO, locale)});
            }
        }

        /** Call service SpsTPoDetailService updateByCondition() for update P/O detail with condition --> PO_ID by pass parameter with 2 arg (Domain, CriteriaDomain). */
        for (int i = Constants.ZERO; i < poDetailList.size(); ++i) {
            SpsTPoDetailDomain tpoDetail = poDetailList.get(i);
            tpoDetail.setLastUpdateDatetime(lastUpdateDatetime);
            SpsTPoDetailCriteriaDomain tpoDetailCri = poDetailCriList.get(i);
            spsTPoDetailService.updateByCondition(tpoDetail, tpoDetailCri);
        }

    }

    /**
     * <p>
     * Get Label method.
     * </p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {

        String labelString = MessageUtil.getLabelHandledException(locale, key);

        return labelString;
    }
}
