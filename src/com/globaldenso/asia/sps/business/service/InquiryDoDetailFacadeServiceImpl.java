/*
 * ModifyDate Development company   Describe 
 * 2014/07/29 CSI Parichat          Create
 * 2015/07/15 CSI Akat              [IN004]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.FileManagementException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;


/**
 * <p>The Class InquiryDoDetailFacadeServiceImpl.</p>
 * <p>Manage data of Inquiry do detail.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method search  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public class InquiryDoDetailFacadeServiceImpl implements InquiryDoDetailFacadeService {

    /** The record limit service. */
    private RecordLimitService recordLimitService;
    
    /** The delivery order service. */
    private DeliveryOrderService deliveryOrderService;
    
    /** The company service. */
    private CompanyDensoService companyDensoService;
    
    /** The file management service. */
    private FileManagementService fileManagementService;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new inquiry do detail service implement.
     */
    public InquiryDoDetailFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * Sets the delivery order service.
     * 
     * @param deliveryOrderService the new delivery order service
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }
    
    /**
     * Sets the company denso service.
     * 
     * @param companyDensoService the new company denso service
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * Set the file management service.
     * 
     * @param fileManagementService the file management service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InquiryDODetailFacadeService#searchInitial(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain)
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_UNCHECKED, Constants.SUPPRESS_WARNINGS_RAWTYPES})
    public void searchInitial(InquiryDoDetailDomain inquiryDODetailDomain) 
        throws ApplicationException
    {
        String densoCode = Constants.EMPTY_STRING;
        Map doDetailMap = new HashMap();
        SpsMCompanyDensoDomain companyDenso = new SpsMCompanyDensoDomain();
        MiscellaneousDomain recordLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        
        Set<String> dCdList = new TreeSet<String>();
        List<InquiryDoDetailReturnDomain> doDetailReturnList 
            = new ArrayList<InquiryDoDetailReturnDomain>();
        List<InquiryDoDetailReturnDomain> doDetailReturnTempList = null;
        List<InquiryDoDetailReturnDomain> inquiryDoDetailReturnList = null;
        Locale locale = inquiryDODetailDomain.getLocale();
        
        if(StringUtil.checkNullOrEmpty(inquiryDODetailDomain.getDoId())){
            MessageUtil.throwsApplicationMessageWithLabel(locale
                , SupplierPortalConstant.ERROR_CD_SP_E7_0007, SupplierPortalConstant.LBL_SPS_DO_NO);
        }
        
        Integer recordCount = deliveryOrderService.searchCountDoDetail(inquiryDODetailDomain);
        if(Constants.ZERO == recordCount){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }
        
        recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP003_RLM);
        recordLimit = recordLimitService.searchRecordLimit(recordLimit);
        if(null == recordLimit || StringUtil.checkNullOrEmpty(recordLimit.getMiscValue())){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        
        int recLimit = Integer.valueOf(recordLimit.getMiscValue());
        if(recLimit < recordCount){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                new String[]{recordLimit.getMiscValue()});
        }
        
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP003_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimitPerPage(recordLimitPerPage);
        if(null == recordLimitPerPage
            || StringUtil.checkNullOrEmpty(recordLimitPerPage.getMiscValue())){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        
        inquiryDoDetailReturnList = deliveryOrderService.searchDoDetail(inquiryDODetailDomain);
        for(InquiryDoDetailReturnDomain inquiryDODetail : inquiryDoDetailReturnList){
            densoCode = inquiryDODetail.getDoDomain().getDCd();
            if(doDetailMap.containsKey(densoCode)){
                List<InquiryDoDetailReturnDomain> doDetailList 
                    = (List<InquiryDoDetailReturnDomain>)doDetailMap.get(densoCode);
                doDetailList.add(inquiryDODetail);
            }else{
                doDetailReturnTempList = new ArrayList<InquiryDoDetailReturnDomain>();
                doDetailReturnTempList.add(inquiryDODetail);
                doDetailMap.put(densoCode, doDetailReturnTempList);
                dCdList.add(densoCode);
            }
        }
        
        List<As400ServerConnectionInformationDomain> schemaResultList = null;
        if(Constants.ZERO < dCdList.size()){
            companyDenso.setDCd(StringUtil.convertListToVarcharCommaSeperate(dCdList));
            schemaResultList = companyDensoService.searchAs400ServerList(companyDenso);
            if(null == schemaResultList || Constants.ZERO == schemaResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale
                    , SupplierPortalConstant.ERROR_CD_SP_E5_0026);
            }
            if(dCdList.size() != schemaResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale
                    , SupplierPortalConstant.ERROR_CD_SP_E5_0025);
            }
        }
        
        List<PseudoCigmaAsnDomain> cigmaAsnResultList = null;
        if( null != schemaResultList ) {
            for(As400ServerConnectionInformationDomain as400Server : schemaResultList){
                Set<String> asnNoList = new TreeSet<String>();
                String dCd = as400Server.getSpsMCompanyDensoDomain().getDCd();
                doDetailReturnTempList = (List<InquiryDoDetailReturnDomain>)doDetailMap.get(dCd);
                for(InquiryDoDetailReturnDomain doDetailReturnTemp : doDetailReturnTempList){
                    // [IN004] : check null before put in map
                    //asnNoList.add(doDetailReturnTemp.getAsnDetailDomain().getAsnNo());
                    if (null != doDetailReturnTemp.getAsnDetailDomain().getAsnNo()) {
                        asnNoList.add(doDetailReturnTemp.getAsnDetailDomain().getAsnNo());
                    }
                }

                // [IN004] : size of list before put in map
                if (Constants.ZERO != asnNoList.size()) {
                    String asnNoIn = StringUtil.convertListToVarcharCommaSeperate(asnNoList);
                    cigmaAsnResultList = CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                        asnNoIn, Constants.EMPTY_STRING, as400Server, locale);
                }
                if(null == cigmaAsnResultList || Constants.ZERO == cigmaAsnResultList.size()){
                    cigmaAsnResultList = new ArrayList<PseudoCigmaAsnDomain>();
                }
            }
        }
        
        //Merge data
        for(InquiryDoDetailReturnDomain doDetailReturnTemp : doDetailReturnTempList){
            String asnStatus = Constants.EMPTY_STRING;
            BigDecimal inTransitQty = new BigDecimal(Constants.ZERO);
            BigDecimal receivedQty = new BigDecimal(Constants.ZERO);
            for(PseudoCigmaAsnDomain cigmaAsnResult : cigmaAsnResultList)
            {
                String cigmaCurDoNo = doDetailReturnTemp.getChgCigmaDoNo();
                if(StringUtil.checkNullOrEmpty(cigmaCurDoNo)){
                    cigmaCurDoNo = doDetailReturnTemp.getDoDomain().getCigmaDoNo();
                }
                // [IN004] : check null before trim
                if(null != doDetailReturnTemp.getAsnDetailDomain().getAsnNo()
                    && doDetailReturnTemp.getAsnDetailDomain().getAsnNo().trim().equals(
                        cigmaAsnResult.getPseudoAsnNo().trim()) 
                    // [IN004] change result map field
                    //&& doDetailReturnTemp.getAsnDetailDomain().getDPn().equals(
                    //    cigmaAsnResult.getPseudoDPn().trim())
                    && doDetailReturnTemp.getDoDetailDomain().getDPn().equals(
                        cigmaAsnResult.getPseudoDPn().trim())
                        
                    && cigmaCurDoNo.equals(cigmaAsnResult.getPseudoCigmaCurDoNo().trim()))
                {
                    receivedQty = new BigDecimal(cigmaAsnResult.getPseudoReceivedQty());
                    asnStatus = cigmaAsnResult.getPseudoAsnStatus();
                    break;
                }
            }
            
            // [IN004] : Check D/O already create ASN
            if (null != doDetailReturnTemp.getAsnDomain().getAsnStatus()) {
                if(!Constants.ASN_STATUS_CCL.equals(
                    doDetailReturnTemp.getAsnDomain().getAsnStatus()))
                {
                    if(!StringUtil.checkNullOrEmpty(asnStatus)){
                        if(Constants.ASN_STATUS_N.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_ISS;
                        }else if(Constants.ASN_STATUS_D.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_CCL;
                        }else if(Constants.ASN_STATUS_P.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_PTR;
                        }else if(Constants.ASN_STATUS_R.equals(asnStatus)){
                            asnStatus = Constants.ASN_STATUS_RCP;
                        }
                        doDetailReturnTemp.getAsnDomain().setAsnStatus(asnStatus);
                    }
                }
                doDetailReturnTemp.setReceivedQty(receivedQty);
                if(Constants.ASN_STATUS_CCL.equals(
                    doDetailReturnTemp.getAsnDomain().getAsnStatus()))
                {
                    inTransitQty = new BigDecimal(Constants.ZERO);
                }else{
                    inTransitQty = doDetailReturnTemp.getAsnDetailDomain().getShippingQty()
                        .subtract(doDetailReturnTemp.getReceivedQty());
                }
                doDetailReturnTemp.setInTransitQty(inTransitQty);
                
                String shippingQtyStr = StringUtil.toCurrencyFormat(String.valueOf(
                    doDetailReturnTemp.getAsnDetailDomain().getShippingQty()));
                String receivedQtyStr = StringUtil.toCurrencyFormat(String.valueOf(
                    doDetailReturnTemp.getReceivedQty()));
                String inTransitQtyStr = StringUtil.toCurrencyFormat(String.valueOf(
                    doDetailReturnTemp.getInTransitQty()));
                doDetailReturnTemp.setShippingQtyStr(shippingQtyStr);
                doDetailReturnTemp.setReceivingQtyStr(receivedQtyStr);
                doDetailReturnTemp.setInTransitQtyStr(inTransitQtyStr);
            } else {
                // [IN004] : D/O doesn't create ASN yet
                doDetailReturnTemp.setShippingQtyStr(Constants.EMPTY_STRING);
                doDetailReturnTemp.setReceivingQtyStr(Constants.EMPTY_STRING);
                doDetailReturnTemp.setInTransitQtyStr(Constants.EMPTY_STRING);
            }
            
            // [IN004] add field
            String orderQtyStr = StringUtil.toCurrencyFormat(String.valueOf(
                doDetailReturnTemp.getDoDetailDomain().getCurrentOrderQty()));
            doDetailReturnTemp.setOrderQtyStr(orderQtyStr);
            // [IN004] change result map field
            //doDetailReturnTemp.setDPn(doDetailReturnTemp.getAsnDetailDomain().getDPn());
            //doDetailReturnTemp.setSPn(doDetailReturnTemp.getAsnDetailDomain().getSPn());
            doDetailReturnTemp.setDPn(doDetailReturnTemp.getDoDetailDomain().getDPn());
            doDetailReturnTemp.setSPn(doDetailReturnTemp.getDoDetailDomain().getSPn());
            
            doDetailReturnList.add(doDetailReturnTemp);
        }
        
        //Calculate rownum for query
        inquiryDODetailDomain.setMaxRowPerPage(Integer.valueOf(recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(inquiryDODetailDomain, doDetailReturnList.size());
        int rowNumFrom = inquiryDODetailDomain.getRowNumFrom() - Constants.ONE;
        int rowNumTo = inquiryDODetailDomain.getRowNumTo();
        
        //Set <List> doDetailInformation to return Domain
        List<InquiryDoDetailReturnDomain> doDetailInformationList 
            = new ArrayList<InquiryDoDetailReturnDomain>();
        for(int i = rowNumFrom; i <  rowNumTo; i++){
            doDetailInformationList.add(doDetailReturnList.get(i));
        }
        inquiryDODetailDomain.setInquiryDoDetailReturnList(doDetailInformationList);
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InquiryDODetailFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain)
     */
    public FileManagementDomain searchFileName(InquiryDoDetailDomain inquiryDoDetailDomain)
        throws ApplicationException
    {
        Locale locale = inquiryDoDetailDomain.getLocale();
        FileManagementDomain resultDomain = null;
        try{
            resultDomain = fileManagementService.searchFileDownload(
                inquiryDoDetailDomain.getPdfFileId(), false, null);
        }catch(FileManagementException ex){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }catch(IOException io){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        return resultDomain;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InquiryDODetailFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain, java.io.OutputStream)
     */
    public void searchLegendInfo(InquiryDoDetailDomain inquiryDoDetailDomain,
        OutputStream outputStream) throws ApplicationException
    {
        Locale locale = inquiryDoDetailDomain.getLocale();
        try{
            fileManagementService.searchFileDownload(inquiryDoDetailDomain.getPdfFileId(),
                true, outputStream);
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
}