/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserRegistrationDomain;

/**
 * <p>The Interface SupplierUserRegistrationFacadeService.</p>
 * <p>Service for Supplier User about search data from criteria and create and update data.</p>
 * <ul>
 * <li>Method search  : initialRegister</li>
 * <li>Method search  : initialEdit</li>
 * <li>Method insert  : transacCreateSupplierUser</li>
 * <li>Method update  : transacUpdateSupplierUser</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchSelectedPlantSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public interface SupplierUserRegistrationFacadeService {
     
    /**
     * <p>Initial data for Register.</p>
     * <ul>
     * <li>Search Information for combo loading.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain that keep the user role to query data.
     * @return SupplierUserRegistrationDomain the keep Information for combo loading.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserRegistrationDomain searchInitialRegister(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>Initial data for Edit.</p>
     * <ul>
     * <li>Update detail Supplier User data.Operator change value on update page 
     * then click Register button.</li>
     * </ul>
     * 
     * @param supplierUserRegistrationDomain that keep DSC ID for load information 
     * and DataScopeControlDomain for Role Inquiry.
     * @return the SupplierUserRegistrationDomain that keep Company supplier information domain.
     * @throws ApplicationException the ApplicationException
     */
    public SupplierUserRegistrationDomain searchInitialEdit(SupplierUserRegistrationDomain 
        supplierUserRegistrationDomain) throws ApplicationException;
    
    /**
     * <p>Transact Create SupplierUser</p>
     * <ul>
     * <li>Insert detail Supplier User data.Operator input value on register page 
     * then click register button.</li>
     * </ul>
     * 
     * @param supplierUserRegistrationDomain that keep User and Supplier Information.
     * @return the SupplierUserRegistrationDomain that keep List of Error Message.
     * @throws  ApplicationException the ApplicationException
     */
    public SupplierUserRegistrationDomain transactCreateSupplierUser(SupplierUserRegistrationDomain 
        supplierUserRegistrationDomain) throws ApplicationException;
    
    /**
     * <p>Transact Update SupplierUser</p>
     * <ul>
     * <li>Update detail Supplier User data.Operator input value on register page 
     * then click register button.</li>
     * </ul>
     * 
     * @param supplierUserRegistrationDomain that keep User and Supplier Information.
     * @return the SupplierUserRegistrationDomain that keep List of Error Message.
     * @throws ApplicationException the ApplicationException
     */
    public SupplierUserRegistrationDomain transactUpdateSupplierUser(SupplierUserRegistrationDomain 
        supplierUserRegistrationDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select supplier company code.</p>
     * <ul>
     * <li>Search list of supplier plant to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return SupplierUserRegistrationDomain the List of supplier plant and vendor code.
     * @throws ApplicationException the ApplicationException
     */
    public SupplierUserRegistrationDomain  searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant supplier code.</p>
     * <ul>
     * <li>Search list of company supplier to show in combo box.</li>
     * </ul>
     * 
     * @param companySupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Plant Supplier
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}