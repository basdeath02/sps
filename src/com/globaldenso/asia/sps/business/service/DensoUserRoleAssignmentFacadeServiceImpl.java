/*
 * ModifyDate Development company     Describe 
 * 2014/06/16 CSI Phakaporn           Create
 * 2016/01/15 CSI Akat                [IN053]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.UserDensoDetailDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class DensoUserRoleAssignmentFacadeServiceImpl.</p>
 * <p>Manage data of Supplier User Role.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchInitialPopup</li>
 * <li>Method insert  : searchSelectedCompanyDenso</li>
 * <li>Method update  : searchSelectedPlantDenso</li>
 * <li>Method update  : deleteUserRole</li>
 * <li>Method update  : createUserRole</li>
 * <li>Method update  : updateUserRole</li>
 * <li>Method update  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public class DensoUserRoleAssignmentFacadeServiceImpl implements
    DensoUserRoleAssignmentFacadeService{
    
    /** The common service. */
    private CommonService commonService;
    
    /** The user role service. */
    private UserRoleService userRoleService;
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The User Service. */
    private SpsMUserService spsMUserService;
    
    /** The User Service. */
    private SpsMUserDensoService spsMUserDensoService;
    
    /** The User Role Service. */
    private SpsMUserRoleService spsMUserRoleService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The Role Service. */
    private SpsMRoleService spsMRoleService;
    
    /** The Plant DENSO service. */
    private PlantDensoService plantDensoService;
    
    /** The DENSO Company Service. */
    private CompanyDensoService companyDensoService;
    
    /** The DENSO Company Service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The DENSO Plant Service. */
    private SpsMPlantDensoService spsMPlantDensoService;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /**
     * Instantiates a new DensoUserRoleAssignmentFacade service impl.
     */
    public DensoUserRoleAssignmentFacadeServiceImpl(){
        super();
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets the User Role Service.
     * 
     * @param userRoleService the new User Role Service.
     */
    public void setUserRoleService(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    /**
     * Sets the User Service.
     * 
     * @param spsMUserService the new User Service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    /**
     * Sets the User Role Service.
     * 
     * @param spsMUserRoleService the new User Role Service.
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }
    /**
     * Sets the record Limit Service.
     * 
     * @param recordLimitService the new record Limit Service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    /**
     * Sets the Role Service.
     * 
     * @param spsMRoleService the new Role Service.
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    /**
     * Sets the User DENSO Service.
     * 
     * @param spsMUserDensoService the new User DENSO Service.
     */
    public void setSpsMUserDensoService(SpsMUserDensoService spsMUserDensoService) {
        this.spsMUserDensoService = spsMUserDensoService;
    }
    
    /**
     * Sets The Plant DENSO Service.
     * 
     * @param plantDensoService the new Plant DENSO Service.
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    /**
     * Set the company DENSO service.
     * 
     * @param companyDensoService the company DENSO service.
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    /**
     * Set the company DENSO service.
     * 
     * @param spsMCompanyDensoService the company DENSO service.
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * Sets The Plant DENSO Service.
     * 
     * @param spsMPlantDensoService the new Plant DENSO Service.
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#searchInitial
     * (com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain)
     */
    public DensoUserRoleAssignDomain searchInitial(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain) throws ApplicationException
    {
        Locale locale = densoUserRoleAssignDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain = densoUserRoleAssignDomain
            .getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = new 
            ArrayList<DensoSupplierRelationDomain>();
        SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
        List<UserRoleDetailDomain> userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
        SpsMUserRoleCriteriaDomain spsMUserRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        UserRoleDetailDomain userRoleDetailDomain = new UserRoleDetailDomain();
        List<UserRoleDetailDomain> userRoleResultlist = new ArrayList<UserRoleDetailDomain>();
        int patternDate = DateUtil.PATTERN_YYYYMMDD_SLASH;
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        try{
            /*call private method for get relation between DENSO and supplier*/
            densoSupplierRelationDomainList = searchDensoSupplierRelation(dataScopeControlDomain);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
            
            if(!Constants.MODE_REGISTER.equals(densoUserRoleAssignDomain.getMode())){
                /*call private method for Get Supplier user information*/
                spsMUserDomain.setDscId(densoUserRoleAssignDomain.getUserDensoDetailDomain()
                    .getUserRoleDetailDomain().getDscId());
                UserDensoDetailDomain densoUserDetailDomain = searchDensoUserInformation(
                    spsMUserDomain);
                if(Constants.ZERO < densoUserDetailDomain.getErrorMessageList().size()){
                    densoUserRoleAssignDomain.setErrorMessageList(densoUserDetailDomain
                        .getErrorMessageList());
                    return densoUserRoleAssignDomain;
                }
                densoUserRoleAssignDomain.setUserDensoDetailDomain(densoUserDetailDomain);
                
                /*Get a number of records by DSC ID.*/
                spsMUserRoleCriteriaDomain.setDscId(densoUserRoleAssignDomain
                    .getUserDensoDetailDomain().getSpsMUserDomain().getDscId());
                spsMUserRoleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                int maxRecord = spsMUserRoleService.searchCount(spsMUserRoleCriteriaDomain);
                
                /* Get Maximum record limit*/
                recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
                recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM008_RLM);
                recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
                if(null == recordsLimit){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                /*Check max record*/
                if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[]{recordsLimit.getMiscValue()});
                }
                
                /* Get Maximum record per page limit*/
                recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
                recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM008_PLM);
                recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
                if(null == recordLimitPerPage){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }

                /*Calculate Rownum for query*/
                userRoleDetailDomain.setPageNumber(densoUserRoleAssignDomain.getPageNumber());
                userRoleDetailDomain.setMaxRowPerPage(Integer.valueOf(
                    recordLimitPerPage.getMiscValue()));
                SpsPagingUtil.calcPaging(userRoleDetailDomain, maxRecord);
                
                /*Get target user roles information*/
                userRoleDetailDomain.setDscId(densoUserRoleAssignDomain
                    .getUserDensoDetailDomain().getSpsMUserDomain().getDscId());
                userRoleDetailDomain.setIsActive(Constants.IS_ACTIVE);
                
                /*call private method for get list of assigned user roles*/
                userRoleDetailList = searchUserRoleAssigned(userRoleDetailDomain);
                
                /*set date format for display on screen*/
                for(UserRoleDetailDomain userRoleDomain : userRoleDetailList){
                    UserRoleDetailDomain userRoleResult = new UserRoleDetailDomain();
                    Date effectEndDate = new Date(userRoleDomain.getEffectEnd().getTime());
                    Date effectStartDate = new Date(userRoleDomain.getEffectStart().getTime());
                    Date createDate = new Date(userRoleDomain.getCreateDatetime().getTime());
                    Date lastUpdateDatetime = new Date(userRoleDomain.getLastUpdateDatetime()
                        .getTime());
                    
                    userRoleResult.setSeqNoScreen(String.valueOf(userRoleDomain.getSeqNo()));
                    userRoleResult.setSeqNo(userRoleDomain.getSeqNo());
                    userRoleResult.setEffectEndScreen(DateUtil.format(effectEndDate, patternDate));
                    userRoleResult.setEffectStartScreen(DateUtil.format(effectStartDate, 
                        patternDate));
                    userRoleResult.setCreateDateScreen(DateUtil.format(createDate, patternDate));
                    userRoleResult.setDCd(userRoleDomain.getDCd());
                    userRoleResult.setDPcd(userRoleDomain.getDPcd());
                    userRoleResult.setFirstName(userRoleDomain.getFirstName());
                    userRoleResult.setMiddleName(userRoleDomain.getMiddleName());
                    userRoleResult.setLastName(userRoleDomain.getLastName());
                    userRoleResult.setRoleCd(userRoleDomain.getRoleCd());
                    userRoleResult.setRoleName(userRoleDomain.getRoleName());
                    userRoleResult.setLastUpdateDatetimeScreen(
                        DateUtil.format(lastUpdateDatetime, patternTimestamp));
                    userRoleResultlist.add(userRoleResult);
                }
                densoUserRoleAssignDomain.getUserDensoDetailDomain()
                    .setUserRoleDetailList(userRoleResultlist);
                densoUserRoleAssignDomain.getUserDensoDetailDomain()
                    .setUserRoleDetailDomain(userRoleDetailDomain);
            }
            
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return densoUserRoleAssignDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#initialPopup
     * (com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain)
     */
    public DensoUserRoleAssignDomain searchInitialPopup(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain) throws ApplicationException
    {
        Locale locale = densoUserRoleAssignDomain.getLocale();
        DensoUserRoleAssignDomain result = new DensoUserRoleAssignDomain();
        DataScopeControlDomain dataScopeControlDomain = densoUserRoleAssignDomain
            .getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            new ArrayList<DensoSupplierRelationDomain>();
        List<CompanyDensoDomain> companyDensoList = new ArrayList<CompanyDensoDomain>();
        List<PlantDensoDomain> plantDensoList = new ArrayList<PlantDensoDomain>();
        
        /*Get relation between DENSO and Supplier*/
        densoSupplierRelationDomainList = searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        result.setDataScopeControlDomain(dataScopeControlDomain);
        
        /*Get all roles from SpsMRoleService*/
        SpsMRoleCriteriaDomain criteria = new SpsMRoleCriteriaDomain();
        List<SpsMRoleDomain> spsMRoleList = spsMRoleService.searchByCondition(criteria);
        if(Constants.ZERO < spsMRoleList.size()){
            result.getUserDensoDetailDomain().setSpsMRoleList(spsMRoleList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_ROLE);
        }
        /*Get DENSO plant list */
        companyDensoList = searchDensoCompany(dataScopeControlDomain);
        result.setCompanyDensoList(companyDensoList);
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(plantDensoList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        result.setPlantDensoList(plantDensoList);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#searchSelectedCompanyDenso
     * (com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        DensoUserRoleAssignDomain densoUserRoleAssignDomain) throws ApplicationException
    {
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = densoUserRoleAssignDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain = 
            densoUserRoleAssignDomain.getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
            densoUserRoleAssignDomain.getDCd());
        plantDensoWithScopeDomain.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoWithScopeDomain.setDataScopeControlDomain(
            densoUserRoleAssignDomain.getDataScopeControlDomain());
        
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(null == plantDensoList || plantDensoList.isEmpty()){
            plantDensoList = new ArrayList<PlantDensoDomain>();
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScope.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == result || Constants.ZERO == result.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#deleteUserRole(com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain)
     */
    public  void deleteUserRole(DensoUserRoleAssignDomain densoUserRoleAssignDomain)
        throws ApplicationException
    {
        List<UserRoleDetailDomain> userRoleDetailSelectedList = densoUserRoleAssignDomain
            .getUserDensoDetailDomain().getUserRoleDetailList();
        List<SpsMUserRoleCriteriaDomain> criteriaList = new ArrayList<SpsMUserRoleCriteriaDomain>();
        List<SpsMUserRoleDomain> domainList = new ArrayList<SpsMUserRoleDomain>();
        Locale locale = densoUserRoleAssignDomain.getLocale();
        
        /*set effect end date to (CommonService.searchSysDate() - 1 day)*/
        Timestamp currentTimestamp = commonService.searchSysDate();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Calendar prevDate = Calendar.getInstance();
        prevDate.setTime(currentTimestamp);
        prevDate.add(Calendar.DATE, Constants.MINUS_ONE);
        String previousDate = DateUtil.format(prevDate.getTime(), patternTimestamp);
        Timestamp effectEnd = DateUtil.parseToTimestamp(previousDate, patternTimestamp);

        /*Update user role active flag to inactive and effective end date to expire*/
        for(UserRoleDetailDomain userRoleSelected : userRoleDetailSelectedList){
            SpsMUserRoleDomain roleDomain = new SpsMUserRoleDomain();
            roleDomain.setRoleCd(userRoleSelected.getRoleCd());
            roleDomain.setDPcd(userRoleSelected.getDPcd());
            roleDomain.setIsActive(Constants.IS_NOT_ACTIVE);
            roleDomain.setEffectEnd(effectEnd);
            roleDomain.setLastUpdateDscId(densoUserRoleAssignDomain.getUpdateUser());
            roleDomain.setLastUpdateDatetime(currentTimestamp);
            domainList.add(roleDomain);
            
            /*Check existing user role from spsMUserRoleService*/
            SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
            userRoleCriteriaDomain.setSeqNo(userRoleSelected.getSeqNo());
            int countUserRole = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
            if(countUserRole <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_USER_ROLE);
            }
            Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(userRoleSelected
                .getLastUpdateDatetimeScreen(), patternTimestamp);
            SpsMUserRoleCriteriaDomain roleCriteria = new SpsMUserRoleCriteriaDomain();
            roleCriteria.setSeqNo(userRoleSelected.getSeqNo());
            roleCriteria.setLastUpdateDatetime(lastUpdateDatetime);
            criteriaList.add(roleCriteria);
        }
        int updateRecord = spsMUserRoleService.updateByCondition(domainList, criteriaList);
        if(updateRecord != criteriaList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_DELETE_USER_ROLE);
        }

    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#createUserRole
     * (com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain)
     */
    public DensoUserRoleAssignDomain createUserRole(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain)throws ApplicationException{
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        DensoUserRoleAssignDomain result = new DensoUserRoleAssignDomain();
        SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Locale locale = densoUserRoleAssignDomain.getLocale();
        UserRoleDetailDomain userRoleDetailDomain = densoUserRoleAssignDomain
            .getUserDensoDetailDomain().getUserRoleDetailDomain();
        try{
            /*Validate all input items.*/
            errorMessageList = validateInputValue(densoUserRoleAssignDomain);
            /*In case of found an error message return the message to ActionForm*/
            if(Constants.ZERO < errorMessageList.size()){
                densoUserRoleAssignDomain.setErrorMessageList(errorMessageList);
                return densoUserRoleAssignDomain;
            }
            
            Date effectStartUtil = DateUtil.parseToUtilDate(densoUserRoleAssignDomain
                .getEffectStartScreen(), pattern);
            Date effectEndUtil = DateUtil.parseToUtilDate(
                densoUserRoleAssignDomain.getEffectEndScreen(), pattern);
            Timestamp effectStart = new Timestamp(effectStartUtil.getTime());
            Timestamp effectEnd = new Timestamp(effectEndUtil.getTime());

            if(!userRoleDetailDomain.getRoleCd().equals(densoUserRoleAssignDomain
                .getRoleCode())){
                /*Check existing assigned user role from SPSMUserRoleService*/
                SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = 
                    new SpsMUserRoleCriteriaDomain();
                userRoleCriteriaDomain.setDscId(userRoleDetailDomain.getDscId());
                userRoleCriteriaDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
                userRoleCriteriaDomain.setDCd(userRoleDetailDomain.getDCd());
                userRoleCriteriaDomain.setDPcd(userRoleDetailDomain.getDPcd());
                userRoleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                int recordCount = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
                if(Constants.ZERO < recordCount){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0024,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            
            /*Create new user role.*/
            userRoleDomain.setDscId(userRoleDetailDomain.getDscId());
            userRoleDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
            userRoleDomain.setDCd(userRoleDetailDomain.getDCd());
            userRoleDomain.setDPcd(userRoleDetailDomain.getDPcd());
            userRoleDomain.setIsActive(Constants.IS_ACTIVE);
            userRoleDomain.setEffectStart(effectStart);
            userRoleDomain.setEffectEnd(effectEnd);
            userRoleDomain.setCreateDscId(userRoleDetailDomain.getCreateDscId());
            userRoleDomain.setCreateDatetime(commonService.searchSysDate());
            userRoleDomain.setLastUpdateDscId(userRoleDetailDomain.getCreateDscId());
            userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            spsMUserRoleService.create(userRoleDomain);
            
            result.setIsActive(Constants.IS_ACTIVE);
            
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        return result;
       
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#updateUserRole(com.globaldenso.asia.sps.business.domain.DensoUserRoleAssignDomain)
     */
    public DensoUserRoleAssignDomain updateUserRole(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain) throws ApplicationException{
        Locale locale = densoUserRoleAssignDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
        SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
        UserRoleDetailDomain userRoleDetailDomain = densoUserRoleAssignDomain
            .getUserDensoDetailDomain().getUserRoleDetailDomain();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;

        try{
            /*Validate all input items.*/
            errorMessageList = validateInputValue(densoUserRoleAssignDomain);
            /*In case of found an error message return the message to ActionForm*/
            if(Constants.ZERO < errorMessageList.size()){
                densoUserRoleAssignDomain.setErrorMessageList(errorMessageList);
                return densoUserRoleAssignDomain;
            }
            
            Date effectStartUtil = DateUtil.parseToUtilDate(densoUserRoleAssignDomain
                .getEffectStartScreen(), pattern);
            Date effectEndUtil = DateUtil.parseToUtilDate(
                densoUserRoleAssignDomain.getEffectEndScreen(), pattern);
            Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(userRoleDetailDomain
                .getLastUpdateDatetimeScreen(), patternTimestamp );
            Timestamp effectStart = new Timestamp(effectStartUtil.getTime());
            Timestamp effectEnd = new Timestamp(effectEndUtil.getTime());
            
            if(!userRoleDetailDomain.getRoleCd().equals(densoUserRoleAssignDomain
                .getRoleCode())){
                /*Check existing assigned user role from SPSMUserRoleService*/
                userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
                userRoleCriteriaDomain.setDscId(userRoleDetailDomain.getDscId());
                userRoleCriteriaDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
                userRoleCriteriaDomain.setDCd(userRoleDetailDomain.getDCd());
                userRoleCriteriaDomain.setDPcd(userRoleDetailDomain.getDPcd());
                userRoleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                int recordCount = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
                if(Constants.ZERO < recordCount){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0024,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            
            /*Check existing user role from spsMUserRoleService*/
            userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
            userRoleCriteriaDomain.setSeqNo(userRoleDetailDomain.getSeqNo());
            int countUserRole = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
            if(countUserRole <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_USER_ROLE);
            }
            
            userRoleDomain.setDscId(userRoleDetailDomain.getDscId());
            userRoleDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
            userRoleDomain.setDCd(userRoleDetailDomain.getDCd());
            userRoleDomain.setDPcd(userRoleDetailDomain.getDPcd());
            userRoleDomain.setIsActive(Constants.IS_ACTIVE);
            userRoleDomain.setEffectStart((Timestamp)effectStart);
            userRoleDomain.setEffectEnd((Timestamp)effectEnd);
            
            // [IN053] Assign Role show assign by used create user, change to update user
            //userRoleDomain.setLastUpdateDscId(userRoleDetailDomain.getCreateDscId());
            userRoleDomain.setLastUpdateDscId(userRoleDetailDomain.getLastUpdateDscId());
            
            userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            userRoleCriteriaDomain.setSeqNo(userRoleDetailDomain.getSeqNo());
            userRoleCriteriaDomain.setLastUpdateDatetime(lastUpdateDatetime);
            
            int updateRecord = spsMUserRoleService.updateByCondition(userRoleDomain,
                userRoleCriteriaDomain);
            if(updateRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                    SupplierPortalConstant.LBL_UPDATE_USER_ROLE);
            }
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        return densoUserRoleAssignDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRoleAssignmentFacadeService#searchRoleCanOperate(java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * 
     * <p>Get relation between DENSO and Supplier.</p>
     *
     * @param dataScopeControlDomain that keep data scope criteria.
     * @return list the keep list of relation between DENSO and Supplier.
     * @throws ApplicationException the ApplicationException from error message.
     */
    private List<DensoSupplierRelationDomain> searchDensoSupplierRelation(DataScopeControlDomain
        dataScopeControlDomain)throws ApplicationException{
        Locale locale = dataScopeControlDomain.getLocale();
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = new 
            ArrayList<DensoSupplierRelationDomain>();
        /*Get relation between DENSO and Supplier*/
        densoSupplierRelationDomainList = densoSupplierRelationService
            .searchDensoSupplierRelation(dataScopeControlDomain);
        
        if(densoSupplierRelationDomainList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        return densoSupplierRelationDomainList;
    }
    
    /**
     * 
     * <p>Get DENSO User Information.</p>
     *
     * @param spsMUserDomain that keep criteria for search.
     * @return DensoUserDetailDomain that keep DENSO user detail.
     * @throws ApplicationException the ApplicationException from error message.
     */
   
    private UserDensoDetailDomain searchDensoUserInformation(SpsMUserDomain spsMUserDomain)
        throws ApplicationException{
        
        Locale locale = spsMUserDomain.getLocale();
        SpsMUserCriteriaDomain spsMUserCriteriaDomain = new SpsMUserCriteriaDomain();
        SpsMUserDensoCriteriaDomain spsMUserDensoCriteriaDomain = 
            new SpsMUserDensoCriteriaDomain();
        List<SpsMUserDensoDomain> spsMUserDensoDomainList = 
            new ArrayList<SpsMUserDensoDomain>();
        UserDensoDetailDomain densoUserDetailDomain = new UserDensoDetailDomain();
        List<SpsMUserDomain> spsMUserDomainList = new ArrayList<SpsMUserDomain>();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        String message = new String();
        spsMUserCriteriaDomain.setDscId(spsMUserDomain.getDscId());
        spsMUserCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        spsMUserDensoCriteriaDomain.setDscId(spsMUserDomain.getDscId());
        
        try{
            /*Get user information*/
            spsMUserDomainList = spsMUserService.searchByCondition(spsMUserCriteriaDomain);
            if(Constants.ZERO < spsMUserDomainList.size()){
                densoUserDetailDomain.setSpsMUserDomain(spsMUserDomainList.get(Constants.ZERO));
            }else{
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_USER)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            
            /*Get DENSO user information*/
            spsMUserDensoDomainList = spsMUserDensoService.searchByCondition(
                spsMUserDensoCriteriaDomain);
            if(Constants.ZERO < spsMUserDensoDomainList.size()){
                densoUserDetailDomain.setSpsMUserDensoDomain(spsMUserDensoDomainList
                    .get(Constants.ZERO));
            }else{
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_DENSO_USER)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        densoUserDetailDomain.setErrorMessageList(errorMessageList);
        return densoUserDetailDomain;
    }
    
    /**
     * 
     * <p>Get User Role Information.</p>
     *
     * @param userRoleDetailDomain that keep criteria for search.
     * @return list of UserRoleDetailDomain that keep user role detail.
     * @throws ApplicationException the ApplicationException from error message.
     */
    private List<UserRoleDetailDomain>  searchUserRoleAssigned(UserRoleDetailDomain 
        userRoleDetailDomain)throws ApplicationException{
        List<UserRoleDetailDomain> resultList = new ArrayList<UserRoleDetailDomain>();

        resultList = userRoleService.searchUserRoleByDscId(userRoleDetailDomain);
        
        return resultList;
    }
    
    /**
     * <p>Search DENSO Company.</p>
     *
     * @param dataScopeControlDomain that keep criteria for search.
     * @return list of CompanyDensoDomain that keep company DENSO.
     * @throws ApplicationException the ApplicationException from error message.
     */
    private List<CompanyDensoDomain> searchDensoCompany(DataScopeControlDomain 
        dataScopeControlDomain)throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        List<CompanyDensoDomain> companyDensoDomainList  = new ArrayList<CompanyDensoDomain>();
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoDomainList = companyDensoService.searchCompanyDenso(plantDensoWithScopeDomain);
        
        if(null == companyDensoDomainList || companyDensoDomainList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return companyDensoDomainList;
    }
    
    /**
     * <p>Validate input value.</p>
     *
     * @param densoUserRoleAssignDomain that keep criteria for search.
     * @return list of ApplicationMessageDomain that keep error message.
     * @throws Exception the Exception.
     */
    private List<ApplicationMessageDomain> validateInputValue(DensoUserRoleAssignDomain 
        densoUserRoleAssignDomain) throws Exception{
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = densoUserRoleAssignDomain.getLocale();
        String message = new String();
        boolean isValidDateFrom = false;
        boolean isValidDateTo = false;
        Timestamp currentDatetime = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Date currentDate = new Date(currentDatetime.getTime());
        
        /*Validate input*/
        if(StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain
            .getUserDensoDetailDomain().getUserRoleDetailDomain().getRoleCd())
            || StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain
                .getUserDensoDetailDomain().getUserRoleDetailDomain().getDPcd())
            || StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain
                .getUserDensoDetailDomain().getUserRoleDetailDomain().getDCd())
            || StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectEndScreen())
            || StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectStartScreen()))
        {
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                SupplierPortalConstant.ERROR_CD_SP_W6_0002, new String[]{MessageUtil.getLabel(
                    locale, SupplierPortalConstant.LBL_ASSIGN_NEW_USER_ROLE)});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                message));
        }
        
        if(!StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectEndScreen())){
            if(!DateUtil.isValidDate(densoUserRoleAssignDomain.getEffectEndScreen(), pattern)){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_EFFECTIVE_END)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectStartScreen())){
            if(!DateUtil.isValidDate(densoUserRoleAssignDomain.getEffectStartScreen(), pattern)){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_EFFECTIVE_START)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectStartScreen())
            && !StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectEndScreen())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO  < DateUtil.compareDate(densoUserRoleAssignDomain
                .getEffectStartScreen(), densoUserRoleAssignDomain.getEffectEndScreen())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_W6_0003);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(densoUserRoleAssignDomain.getEffectStartScreen())){
            if(Constants.ZERO < DateUtil.compareDate(DateUtil.format(currentDate, pattern),
                densoUserRoleAssignDomain.getEffectEndScreen())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                    SupplierPortalConstant.ERROR_CD_SP_W6_0004);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
        }
        /*Check values existing in master data*/
        List<ApplicationMessageDomain> masterErrorList = checkExistingMaster(
            densoUserRoleAssignDomain.getUserDensoDetailDomain()
                .getUserRoleDetailDomain());
        if(Constants.ZERO <= masterErrorList.size()){
            for(ApplicationMessageDomain errorMessage : masterErrorList){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    errorMessage.getMessage()));
            }
        }
        return errorMessageList;
    }
    
    /**
     * 
     * <p>Check Existing Master.</p>
     *
     * @param userRoleDetailDomain that keep a data that need to check existing in master data.
     * @return list of ApplicationMessageDomain that keep error message.
     * @throws Exception the Exception.
     */
    private List<ApplicationMessageDomain> checkExistingMaster(UserRoleDetailDomain 
        userRoleDetailDomain) throws Exception{
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = userRoleDetailDomain.getLocale();
        String message = new String();
        
        /*Check existing role code in master data*/
        SpsMRoleCriteriaDomain roleCriteriaDomain = new SpsMRoleCriteriaDomain();
        roleCriteriaDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
        roleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int recordCount = spsMRoleService.searchCount(roleCriteriaDomain);
        if(recordCount <= Constants.ZERO){
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
                    MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_ROLE)});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                message));
        }
        /*Check existing DENSO company code in master data*/
        SpsMCompanyDensoCriteriaDomain companyDensoCriteriaDomain = 
            new SpsMCompanyDensoCriteriaDomain();
        companyDensoCriteriaDomain.setDCd(userRoleDetailDomain.getDCd());
        companyDensoCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int companyCount = spsMCompanyDensoService.searchCount(companyDensoCriteriaDomain);
        if(companyCount <= Constants.ZERO){
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
                    MessageUtil.getLabel(locale, 
                        SupplierPortalConstant.LBL_DENSO_COMPANY_CODE)});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                message));
        }
        /*Check existing DENSO plant code in master data*/
        if(!Constants.STR_NINETY_NINE.equals(userRoleDetailDomain.getDPcd())){
            SpsMPlantDensoCriteriaDomain plantDensoCriteriaDomain = 
                new SpsMPlantDensoCriteriaDomain();
            plantDensoCriteriaDomain.setDCd(userRoleDetailDomain.getDCd());
            plantDensoCriteriaDomain.setDPcd(userRoleDetailDomain.getDPcd());
            plantDensoCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
            int plantCount = spsMPlantDensoService.searchCount(plantDensoCriteriaDomain);
            if(plantCount <= Constants.ZERO){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
                        MessageUtil.getLabel(locale,
                            SupplierPortalConstant.LBL_DENSO_PLANT_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
        }
        
        return errorMessageList;
    }
}