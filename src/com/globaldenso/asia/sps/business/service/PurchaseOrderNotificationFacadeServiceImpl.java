/*
 * ModifyDate Development company     Describe 
 * 2014/08/18 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>The Class Purchase Order Notification Facade Service Implement.</p>
 * <p>Service for Purchase Order Notification about search data 
 * from Purchase Order Acknowledgement screen.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderNotificationFacadeServiceImpl implements
    PurchaseOrderNotificationFacadeService
{
    
    /** The service for Purchase Order. */
    PurchaseOrderService purchaseOrderService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /** The Default Constructor. */
    public PurchaseOrderNotificationFacadeServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for purchaseOrderService.</p>
     *
     * @param purchaseOrderService Set for purchaseOrderService
     */
    public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderNotificationFacadeService#searchInitial(com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain)
     */
    public List<PurchaseOrderNotificationDomain> searchInitial(
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain)
        throws ApplicationException
    {
        List<PurchaseOrderNotificationDomain> result = null;
        Locale locale = purchaseOrderNotificationDomain.getLocale();
        
        // 1. Validate input parameter which pass from WORD003 screen
        if (null == purchaseOrderNotificationDomain.getSpsTPo().getPoId()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                SupplierPortalConstant.LBL_PO_NO);
        }
        
        // 2. Get Purchase Order Notification data from PurchaseOrderService
        purchaseOrderNotificationDomain.setMarkPendingFlag(Constants.STR_ONE);
        result = this.purchaseOrderService.searchPurchaseOrderNotification(
            purchaseOrderNotificationDomain);
        if (null == result || Constants.ZERO == result.size()) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                null);
        }
        
        // 3. Set <List> poDueInformation to return Domain
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
}
