/*
 * ModifyDate Development company     Describe 
 * 2014/04/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;
import com.globaldenso.asia.sps.business.dao.UserDensoDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class DENSOUserServiceImpl.</p>
 * <p>Manage data of DENSO User.</p>
 * <ul>
 * <li>Method search  : searchCountDensoUser</li>
 * <li>Method search  : searchDensoUser</li>
 * <li>Method search  : searchDensoUserIncludeRole</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class UserDensoServiceImpl implements UserDensoService {
    
    /** The DENSO user dao. */
    private UserDensoDao userDensoDao;

    /**
     * Instantiates a new PO service impl.
     */
    public UserDensoServiceImpl(){
        super();
    }
    
    /**
     * Sets the DENSO user dao.
     * 
     * @param userDensoDao the new DENSO user dao
     */
    public void setUserDensoDao(UserDensoDao userDensoDao) {
        this.userDensoDao = userDensoDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserDensoService#searchCountDensoUser(com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public Integer searchCountUserDenso(DensoUserInformationDomain densoUserInfoDomain){
        
        /* Concatenates string name for use 'like' condition in query example. %name%. */
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setLastName(lastName);
        }
        
        Integer count = userDensoDao.searchCountUserDenso(densoUserInfoDomain);
        return count;
    } 
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserDensoService#searchDensoUser(com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public List<DensoUserInformationDomain> searchUserDenso(DensoUserInformationDomain 
        densoUserInfoDomain){

        /* Concatenates string name for use 'like' condition in query example. %name%. */
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setLastName(lastName);
        }

        List<DensoUserInformationDomain> result = (List<DensoUserInformationDomain>)userDensoDao
            .searchUserDenso(densoUserInfoDomain); 
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserDensoService#searchCountDensoUserIncludeRole(com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public Integer searchCountUserDensoIncludeRole(DensoUserInformationDomain densoUserInfoDomain){
        
        /* Concatenates string name for use 'like' condition in query example. %name%. */
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setLastName(lastName);
        }
        
        Integer count = userDensoDao.searchCountUserDensoIncludeRole(densoUserInfoDomain);
        return count;
    } 
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserDensoService#searchDensoUserIncludeRole(com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public List<DensoUserInformationDomain> searchUserDensoIncludeRole(DensoUserInformationDomain 
        densoUserInfoDomain){

        /* Concatenates string name for use 'like' condition in query example. %name%. */
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(densoUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                densoUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            densoUserInfoDomain.setLastName(lastName);
        }
        
        List<DensoUserInformationDomain> result = (List<DensoUserInformationDomain>)userDensoDao
            .searchUserDensoIncludeRole(densoUserInfoDomain); 
        return result;
    }

}