/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.business.dao.ScreenDao;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain;

/**
 * <p>The Interface ScreenService.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchScreenByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public class ScreenServiceImpl implements ScreenService {

    /** The Screen Dao. */
    private ScreenDao screenDao;
    
    /** The default constructor. */
    public ScreenServiceImpl() {
        super();
    }

    /**
     * Set method for screenDao.
     * @param screenDao the screenDao to set
     */
    public void setScreenDao(ScreenDao screenDao) {
        this.screenDao = screenDao;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ScreenService#searchScreenByUserPermission(java.lang.String,java.lang.String,java.lang.String,java.sql.Timestamp)
     * */
    public List<RoleScreenDomain> searchScreenByUserPermission(
        String dscId, String isRoleUserActive, String isScreenActive, Timestamp currentDatetime)
    {
        UserRoleScreenDomain roleUserScreen = new UserRoleScreenDomain();
        roleUserScreen.getSpsMUserRoleDomain().setDscId(dscId);
        roleUserScreen.getSpsMUserRoleDomain().setIsActive(isRoleUserActive);
        roleUserScreen.getSpsMScreenDomain().setIsActive(isScreenActive);
        roleUserScreen.setCurrentDatetime(currentDatetime);
        return this.screenDao.searchScreenByUserPermission(roleUserScreen);
    }
    
}
