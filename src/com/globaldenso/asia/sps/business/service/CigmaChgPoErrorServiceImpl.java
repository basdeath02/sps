/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaChgPoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;

/**
 * <p>The Class CigmaChgPoErrorService.</p>
 * <p>For SPS_CIGMA_CHG_PO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferChgPoError</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaChgPoErrorServiceImpl implements CigmaChgPoErrorService {

    /** The DAO for SPS_CIGMA_CHG_PO_ERROR. */
    private CigmaChgPoErrorDao cigmaChgPoErrorDao;
    
    /** The default constructor. */
    public CigmaChgPoErrorServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for cigmaChgPoErrorDao.</p>
     *
     * @param cigmaChgPoErrorDao Set for cigmaChgPoErrorDao
     */
    public void setCigmaChgPoErrorDao(CigmaChgPoErrorDao cigmaChgPoErrorDao) {
        this.cigmaChgPoErrorDao = cigmaChgPoErrorDao;
    }

    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaChgPoErrorService#searchTransferChgPoError(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain)
     */
    public List<TransferChgPoErrorEmailDomain> searchTransferChgPoError(
        SpsCigmaChgPoErrorDomain spsCigmaChgPoErrorDomain)
    {
        List<TransferChgPoErrorEmailDomain> result = null;
        result = this.cigmaChgPoErrorDao.searchTransferChgPoError(spsCigmaChgPoErrorDomain);
        return result;
    }

}
