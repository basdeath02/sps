/*
 * ModifyDate Development company     Describe 
 * 2014/07/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.asia.sps.business.domain.TmpUserDensoDomain;

/**
 * <p>The Interface TempUploadUserDensoService.</p>
 * <p>Service for Temporary User DENSO about search from CSV file.</p>
 * <ul>
 *  <li>Method search  : searchTempUploadDensoOneRecord</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpUploadUserDensoService {
       
    /**
     * <p>Search Temporary Upload DENSO One Record.</p>
     * 
     * @param tmpUserDensoDomain that contain upload DENSO user information.
     * @return the DENSO user information from upload.
     */
    public TmpUserDensoDomain searchTmpUploadDensoOneRecord(TmpUserDensoDomain 
        tmpUserDensoDomain);

}