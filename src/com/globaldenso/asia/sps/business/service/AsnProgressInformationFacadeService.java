/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface AsnProgressInformationFacadeService.</p>
 * <p>Service for ASN progress information about search data from criteria.</p>
 * <ul>
 * <li>Method search  : initial</li>
 * <li>Method search  : searchAsnProgressInformation</li>
 * <li>Method search  : searchAsnProgressInformationCsv</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchSelectedPlantDenso</li>
 * <li>Method search  : searchSelectedPlantSupplier</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : validateGroupAsn</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnProgressInformationFacadeService {
    /**
     * <p>Method for Asn Progress Initial.</p>
     *
     * @param asnProgressInformationDomain 
     * @return AsnProgressInformationDomain
     * @throws ApplicationException applicationException
     */
    public AsnProgressInformationDomain searchInitial(AsnProgressInformationDomain 
        asnProgressInformationDomain) throws ApplicationException;
    
    /**
     * <p>Method for search Asn Progress.</p>
     *
     * @param asnProgressInformationDomain 
     * @return List of AsnProgressInformationReturnDomain
     * @throws ApplicationException applicationException
     */
    public List<AsnProgressInformationReturnDomain> searchAsnProgressInformation (
        AsnProgressInformationDomain asnProgressInformationDomain) throws ApplicationException;
    
    /**
     * <p>Method for search Asn Progress CSV Data.</p>
     *
     * @param asnProgressInformationDomain 
     * @return List of AsnProgressInformationReturnDomain
     * @throws ApplicationException applicationException
     */
    public List<AsnProgressInformationReturnDomain> searchAsnProgressInformationCsv (
        AsnProgressInformationDomain asnProgressInformationDomain) throws ApplicationException;
    
    /**
     * <p>Method for search Supplier.</p>
     *
     * @param plantSupplierWithScopeDomain the plantSupplierWithScope domain
     * @return List of plantSupplier domain
     * @throws ApplicationException applicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Method for search plant denso.</p>
     *
     * @param plantDensoWithScopeDomain the plantDensoWithScope domain
     * @return List of plantDenso domain
     * @throws ApplicationException applicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant supplier code.</p>
     * <ul>
     * <li>Search list of company supplier to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Plant Supplier
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Method for search File name.</p>
     *
     * @param asnProgressInformationDomain 
     * @return FileManagementDomain 
     * @throws ApplicationException applicationException
     */
    public FileManagementDomain searchFileName(
        AsnProgressInformationDomain asnProgressInformationDomain) throws ApplicationException;
    
    /**
     * 
     * <p>Method for search Legend Infomation.</p>
     *
     * @param asnProgressInformationDomain 
     * @param outputStream 
     * @return FileManagementDomain
     * @throws ApplicationException applicationException
     */
    public FileManagementDomain searchLegendInfo(
        AsnProgressInformationDomain asnProgressInformationDomain, 
        OutputStream outputStream) throws ApplicationException;
    
    /**
     * 
     * <p>Method for validate Asn Progress criteria.</p>
     *
     * @param asnProgressInformationDomain 
     * @return boolean
     * @throws ApplicationException applicationException
     */
    public boolean searchValidate(
        AsnProgressInformationDomain asnProgressInformationDomain) throws ApplicationException;
    
    /**
     * 
     * <p>Method for Convert data Asn Progress to array string[csv].</p>
     *
     * @param asnProgressInformationReturnDomains List of AsnProgressInformationReturnDomain
     * @param locale 
     * @return List of Map
     * @throws ApplicationException applicationException
     * @throws IOException ioException
     */
    public String convertToCsvData(
        List<AsnProgressInformationReturnDomain> asnProgressInformationReturnDomains, 
            Locale locale) throws ApplicationException, IOException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}