/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 2014/08/12 CSI Phakaporn           Modified method
 * 2015/09/16 CSI Akat                [IN015]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserUploadingDomain;
import com.globaldenso.asia.sps.business.domain.TmpUserSupplierDomain;
import com.globaldenso.asia.sps.common.utils.MessageUtil;



/**
 * <p>The Interface SupplierUserUploadingFacadeService.</p>
 * <p>Service for Supplier User Uploading about manipulate data form CSV file.</p>
 * <ul>
 * <li>Method search : initial</li>
 * <li>Method search : searchUploadErrorList</li>
 * <li>Method insert : transactUploadSupplierUser</li>
 * <li>Method insert : transactRegisterUploadItem</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class SupplierUserUploadingFacadeServiceImpl implements SupplierUserUploadingFacadeService {
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The Temporary Upload Error service.*/
    private SpsTmpUploadErrorService spsTmpUploadErrorService;
    
    /** The Company Supplier service.*/
    private SpsMCompanySupplierService spsMCompanySupplierService;
    
    /** The Company Supplier service.*/
    private CompanySupplierService companySupplierService;
    
    /** The Plant Supplier service.*/
    private SpsMPlantSupplierService spsMPlantSupplierService;
    
    /** The Plant Supplier service.*/
    private PlantSupplierService  plantSupplierService;
    
    /** The Temporary User Supplier service. */
    private SpsTmpUserSupplierService spsTmpUserSupplierService;
    
    /** The user role service.*/
    private SpsMUserRoleService spsMUserRoleService;
    
    /** The user service.*/
    private SpsMUserService spsMUserService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The Temporary Upload Error service. */
    private TmpUploadErrorService tmpUploadErrorService;
    
    /** The Role service. */
    private SpsMRoleService spsMRoleService;
    
    /** The user supplier service.*/
    private SpsMUserSupplierService spsMUserSupplierService;
    
    /** The Temporary User Supplier service. */
    private TmpUploadUserSupplierService tmpUploadUserSupplierService;
    
    /** The Company DENSO service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /**
     * Instantiates a new Supplier User Uploading Facade service impl.
     */
    public SupplierUserUploadingFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
 
    /**
     * Sets the Temporary Upload Error Service.
     * 
     * @param tmpUploadErrorService the Temporary Upload Error Service.
     */ 
    public void setTmpUploadErrorService(
        TmpUploadErrorService tmpUploadErrorService) {
        this.tmpUploadErrorService = tmpUploadErrorService;
    }

    /**
     * Sets the Temporary User Supplier Service.
     * 
     * @param tmpUploadUserSupplierService the Temporary User Supplier Service.
     */ 
    public void setTmpUploadUserSupplierService(
        TmpUploadUserSupplierService tmpUploadUserSupplierService) {
        this.tmpUploadUserSupplierService = tmpUploadUserSupplierService;
    }

    /**
     * Sets the Temporary Upload Error Service.
     * 
     * @param spsTmpUploadErrorService the Temporary Upload Error Service.
     */ 
    public void setSpsTmpUploadErrorService(
        SpsTmpUploadErrorService spsTmpUploadErrorService) {
        this.spsTmpUploadErrorService = spsTmpUploadErrorService;
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service.
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    /**
     * Set the company supplier service.
     * 
     * @param spsMCompanySupplierService the company supplier service.
     */
    public void setSpsMCompanySupplierService(
        SpsMCompanySupplierService spsMCompanySupplierService) {
        this.spsMCompanySupplierService = spsMCompanySupplierService;
    }
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service.
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    /**
     * Set the plant supplier service.
     * 
     * @param spsMPlantSupplierService the plant supplier service.
     */
    public void setSpsMPlantSupplierService(
        SpsMPlantSupplierService spsMPlantSupplierService) {
        this.spsMPlantSupplierService = spsMPlantSupplierService;
    }
    /**
     * Set the plant supplier service.
     * 
     * @param plantSupplierService the plant supplier service.
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * Set the Temporary user supplier service.
     * 
     * @param spsTmpUserSupplierService the Temporary user supplier service.
     */
    public void setSpsTmpUserSupplierService(
        SpsTmpUserSupplierService spsTmpUserSupplierService) {
        this.spsTmpUserSupplierService = spsTmpUserSupplierService;
    }
    /**
     * Set the user role service.
     * 
     * @param spsMUserRoleService the user role service.
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }
    /**
     * Set the user service.
     * 
     * @param spsMUserService the user service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    
    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    /**
     * Sets the role service.
     * 
     * @param spsMRoleService the new role service.
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    /**
     * Sets the user supplier service.
     * 
     * @param spsMUserSupplierService the new user supplier service.
     */
    public void setSpsMUserSupplierService(
        SpsMUserSupplierService spsMUserSupplierService) {
        this.spsMUserSupplierService = spsMUserSupplierService;
    }
    /**
     * Sets the Company DENSO service.
     * 
     * @param spsMCompanyDensoService the new Company DENSO service.
     */
    public void setSpsMCompanyDensoService(SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserUploadingFacadeServiceImpl#searchInitial
     * (com.globaldenso.asia.sps.business.domain.SupplierUserUploadingDomain)
     */
    public void transactInitial(SupplierUserUploadingDomain supplierUserUploadingDomain)
        throws ApplicationException {
        String userDscId = supplierUserUploadingDomain.getUserDscId();
        String sessionCode = supplierUserUploadingDomain.getSessionCode();
        /*Call method deleteUploadTempTable for clear all data in temporary table*/
        deleteUploadTempTable(userDscId, sessionCode);
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserUploadingFacadeServiceImpl#transactUploadSupplierUser
     * (com.globaldenso.asia.sps.business.domain.supplierUserInfoDomain)
     */
    public SupplierUserUploadingDomain transactUploadSupplierUser(SupplierUserUploadingDomain 
        supplierUserUploadingDomain) throws ApplicationException, IOException
    {
        DataScopeControlDomain dataScopeControlDomain
            = supplierUserUploadingDomain.getDataScopeControlDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        FileManagementDomain fileManagementDomain
            = supplierUserUploadingDomain.getFileManagementDomain();
        boolean dscIdValidateFlag = false;
        boolean roleCodeValidateFlag   =  false;
        boolean roleSupplierPlantCodeValidateFlag  =  false;
        boolean informationValidationComplete    = true;
        boolean roleValidationComplete =   true;
        boolean isExist = false;
        boolean isExistTmp = false;
        boolean effectStartValidateFlag = false;
        boolean effectEndValidateFlag = false;
        boolean returnValue = false;
        boolean isExistSupplierCompany = false;
//        Boolean isLetter = false;
//        Boolean isNumber = false;
        String line = new String();
        String companySupplierCode = new String();
        String plantSupplierCode = new String();
        String dscId = new String();
        String roleCode = new String();
        String isActive = new String();
        int lineNo = Constants.ZERO;
        String errorCode = new String();
        String userDscId = supplierUserUploadingDomain.getUserDscId();
        String sessionCd = supplierUserUploadingDomain.getSessionCode();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Timestamp currentTimestamp = commonService.searchSysDate();
        Date currentDate = new Date(currentTimestamp.getTime());
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferReader = null;
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        try {
            /*Get each record and validate the content */
            inputStream = fileManagementDomain.getFileData();
            inputStream = FileUtil.checkUtf8BOMAndSkip(inputStream);
            inputStreamReader = new InputStreamReader(inputStream);
            bufferReader = new BufferedReader(inputStreamReader);
            
            /*LOOP each CSV record until end of file*/
            while(!StringUtil.checkNullOrEmpty(line = bufferReader.readLine())){
                lineNo++;
                /*initial flag validate*/
                dscIdValidateFlag = false;
                roleCodeValidateFlag   =  false;
                roleSupplierPlantCodeValidateFlag  =  false;
                informationValidationComplete    = true;
                roleValidationComplete =   true;
                isExist = false;
                isExistTmp = false;
                effectStartValidateFlag = false;
                effectEndValidateFlag = false;
                //isNumber = false;
                returnValue = false;
                isExistSupplierCompany = false;
                
                List<String> splitItem = StringUtil.splitCsvData(line, Constants.TWENTY);
                splitItem.set(Constants.ZERO,
                    splitItem.get(Constants.ZERO).toUpperCase()); //DSC Id
                splitItem.set(Constants.ONE,
                    splitItem.get(Constants.ONE).toUpperCase()); //DENSO Owner
                splitItem.set(Constants.TWO,
                    splitItem.get(Constants.TWO).toUpperCase()); //SCD
                splitItem.set(Constants.THREE,
                    splitItem.get(Constants.THREE).toUpperCase()); //SPCD
                splitItem.set(Constants.FOUR,
                    splitItem.get(Constants.FOUR).toUpperCase()); //Department name
                splitItem.set(Constants.FIVE,
                    splitItem.get(Constants.FIVE).toUpperCase()); //First Name
                splitItem.set(Constants.SIX,
                    splitItem.get(Constants.SIX).toUpperCase()); //Middle Name
                splitItem.set(Constants.SEVEN,
                    splitItem.get(Constants.SEVEN).toUpperCase()); //Last Name
                splitItem.set(Constants.EIGHT,
                    splitItem.get(Constants.EIGHT).toUpperCase());  //Telephone
                splitItem.set(Constants.NINE,
                    splitItem.get(Constants.NINE).toUpperCase()); //Email
                splitItem.set(Constants.TEN,
                    splitItem.get(Constants.TEN).toUpperCase()); //Urgent Order Flag
                splitItem.set(Constants.ELEVEN,
                    splitItem.get(Constants.ELEVEN).toUpperCase()); //Supplier Not Found Flag
                splitItem.set(Constants.TWELVE,
                    splitItem.get(Constants.TWELVE).toUpperCase()); //Allow Revise Flag
                splitItem.set(Constants.THIRTEEN,
                    splitItem.get(Constants.THIRTEEN).toUpperCase()); //Cancel Invoice Flag
                splitItem.set(Constants.FOURTEEN,
                    splitItem.get(Constants.FOURTEEN).toUpperCase()); //Role Code
                splitItem.set(Constants.FIFTEEN,
                    splitItem.get(Constants.FIFTEEN).toUpperCase()); //Role Supplier Plant Code
                splitItem.set(Constants.SIXTEEN,
                    splitItem.get(Constants.SIXTEEN).toUpperCase()); //Effective Start Date
                splitItem.set(Constants.SEVENTEEN,
                    splitItem.get(Constants.SEVENTEEN).toUpperCase()); //Effective End Date
                splitItem.set(Constants.EIGHTEEN,
                    splitItem.get(Constants.EIGHTEEN).toUpperCase()); //Information Flag
                splitItem.set(Constants.NINETEEN,
                    splitItem.get(Constants.NINETEEN).toUpperCase()); //Role Flag
                
                /**** Check number of record elements*/
                if(Constants.TWENTY != splitItem.size()){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR001;
                    createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    informationValidationComplete = false;
                    continue;
                }

                /**** Check NULL information flag*/
                if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHTEEN))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR038;
                    createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    informationValidationComplete = false;
                }

                /**** Check information flag missing code*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHTEEN))
                    && (!Constants.STR_A.equals(splitItem.get(Constants.EIGHTEEN))
                        && !Constants.STR_U.equals(splitItem.get(Constants.EIGHTEEN))
                        && !Constants.STR_D.equals(splitItem.get(Constants.EIGHTEEN))
                        && !Constants.STR_N.equals(splitItem.get(Constants.EIGHTEEN)))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR039;
                    createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    informationValidationComplete = false;
                }
                
                /*Check information flag "N" value*/
                if(!(Constants.STR_N.equals(splitItem.get(Constants.EIGHTEEN)))){
                    
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                        /**** Check DSC ID length*/
                        if(Constants.MAX_DSC_ID_LENGTH < splitItem.get(Constants.ZERO).length()){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR003;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }
                        
                        /**** Check DSC ID duplicate (Information flag is "A")*/
                        if(Constants.STR_A.equals(splitItem.get(Constants.EIGHTEEN))){
                            /**** Check existing user by DSC ID in master data*/
                            dscId = splitItem.get(Constants.ZERO);
                            isExist = searchExistUserMasterData(dscId);
                            // [IN015] search user exist in Temporary data with any flag
                            //isExistTmp = searchExistUserTmpData(dscId, splitItem.get(
                            //    Constants.EIGHTEEN), userDscId, sessionCd);
                            isExistTmp = searchExistUserTmpData(dscId, null, userDscId, sessionCd);
                            
                            if(isExist){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR004;
                                createItemUploadError(supplierUserUploadingDomain, errorCode,
                                    lineNo);
                                informationValidationComplete = false;
                            }else{
                                /**** Check existing user by DSC ID in Temporary data*/
                                // [IN015] search user exist in Temporary data with any flag
                                if(isExistTmp){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR004;
                                    createItemUploadError(supplierUserUploadingDomain, 
                                        errorCode, lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }
                        }

                        /**** Check DSC ID  not found in system (Information flag is "U")*/
                        if(Constants.STR_U.equals(splitItem.get(Constants.EIGHTEEN))){
                            /** Check existing user by DSC ID in master and Temporary data */
                            dscId = splitItem.get(Constants.ZERO);
                            isExist = searchExistUserMasterData(dscId);
                            // [IN015] check user exist in Temporary data with flag A or U or D
                            //isExistTmp = true;
                            isExistTmp = searchExistUserTmpData(dscId, 
                                Constants.STR_A, userDscId, sessionCd);
                            if (!isExistTmp) {
                                isExistTmp = searchExistUserTmpData(dscId, 
                                    Constants.STR_U, userDscId, sessionCd);
                            }
                            if (!isExistTmp) {
                                isExistTmp = searchExistUserTmpData(dscId, 
                                    Constants.STR_D, userDscId, sessionCd);
                            }
                            
                            if(!isExist){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                createItemUploadError(supplierUserUploadingDomain, errorCode,
                                    lineNo);
                                informationValidationComplete = false;
                            }else{
                                // [IN015] check user exist in Temporary data with flag A or U or D
                                /**** Check existing user by DSC ID in Temporary data*/
                                if(isExistTmp){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR004;
                                    createItemUploadError(supplierUserUploadingDomain, 
                                        errorCode, lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }
                        }

                        /**** Check DSC ID  not found in system (Information flag is "D")*/
                        if(Constants.STR_D.equals(splitItem.get(Constants.EIGHTEEN))){
                            /** Check existing user by DSC ID in master and Temporary data */
                            dscId = splitItem.get(Constants.ZERO);
                            isExist = searchExistUserMasterData(dscId);
                            // [IN015] search user exist in Temporary data with any flag
                            //isExistTmp = true;
                            isExistTmp = searchExistUserTmpData(dscId, null, userDscId, sessionCd);
                            
                            /* [IN015] If data exist in master -> error not exist
                             * If data exist in Temporary data -> error duplicate
                             * */
                            //if(!isExist && !isExistTmp){
                            //    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                            //    createItemUploadError(supplierUserUploadingDomain, errorCode,
                            //        lineNo);
                            //    informationValidationComplete = false;
                            //}else{
                            //    dscIdValidateFlag = true;
                            //}
                            if(!isExist){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                createItemUploadError(supplierUserUploadingDomain, errorCode,
                                    lineNo);
                                informationValidationComplete = false;
                            }else{
                                /**** Check existing user by DSC ID in Temporary data*/
                                if(isExistTmp){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR004;
                                    createItemUploadError(supplierUserUploadingDomain, 
                                        errorCode, lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }
                            
                        }
                    }else{
                        /**** Check NULL  DSC ID*/
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR002;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL DENSO Owner*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ONE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR007;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }else{
                        /**** Check DENSO Owner length*/
                        if(Constants.FIVE < splitItem.get(Constants.ONE).length()){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR008;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }else{
                            splitItem.set(Constants.ONE,
                                StringUtil.rightPad(splitItem.get(Constants.ONE), Constants.FIVE));
                        }
                    }

                    /**** Check DENSO Owner existing in master data*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ONE))){
                        SpsMCompanyDensoCriteriaDomain companyDensoDomain =
                            new SpsMCompanyDensoCriteriaDomain();
                        String dOwner = StringUtil.addSpace(splitItem.get(Constants.ONE), 
                            Constants.FIVE);
                        companyDensoDomain.setDCd(dOwner);
                        int recordCount = spsMCompanyDensoService.searchCount(
                            companyDensoDomain);
                        if(Constants.ZERO == recordCount){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR009;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            informationValidationComplete = false;
                        }
                    }

                    /**** Check NULL Supplier company code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR010;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }else{
                        /**** Check Supplier company code length*/
                        if(Constants.SIX < splitItem.get(Constants.TWO).length()){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR011;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }else{
                            splitItem.set(Constants.TWO,
                                StringUtil.rightPad(splitItem.get(Constants.TWO), Constants.SIX));
                        }
                    }

                    /**** Check Supplier company code*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                        && splitItem.get(Constants.TWO).length() <= Constants.SIX){
                        /**** Check existing Supplier company in system*/
                        companySupplierCode = StringUtil.addSpace(splitItem.get(Constants.TWO),
                            Constants.SIX);
                        isExistSupplierCompany = searchExistSupplierCompany(
                            companySupplierCode);
                        if(isExistSupplierCompany){
                            /**Check existing Supplier company in system by current user data scope*/
                            companySupplierCode = StringUtil.addSpace(splitItem.get(
                                Constants.TWO), Constants.SIX);
                            boolean isExistSupplierCompanyByScope = 
                                searchExistSupplierCompanyWithScope(companySupplierCode, 
                                    dataScopeControlDomain);
                            /** Current user not have a right to working on this DENSO company.*/
                            if(!isExistSupplierCompanyByScope){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR013;
                                createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                    lineNo);
                                informationValidationComplete = false;
                            }
                        }else{
                            /**** Check existing Supplier company not found in the master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR012;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            informationValidationComplete = false;
                        }
                    }
                    
                    /**** Check NULL Supplier plant code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.THREE))){
                        /*errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR014;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;*/
                        splitItem.set(Constants.THREE, Constants.UNDEFINED_FOR_SUPPLIER);
                    }
                    
                    /**** Check Supplier plant code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.THREE))
                        && Constants.ONE < splitItem.get(Constants.THREE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR015;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }else{
                        splitItem.set(Constants.THREE, StringUtil.rightPad(
                            splitItem.get(Constants.THREE), Constants.ONE));
                        
                        /**** Check Supplier plant code*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                            && isExistSupplierCompany)
                        {
                            /**Check existing Supplier plant in system*/
                            companySupplierCode = StringUtil.addSpace(
                                splitItem.get(Constants.TWO), Constants.SIX);
                            plantSupplierCode = splitItem.get(Constants.THREE);
                            boolean isExistSupplierPlant = searchExistSupplierPlant(
                                companySupplierCode, plantSupplierCode);
                            if(isExistSupplierPlant){
                                /**Check existing Supplier plant in system by current user data scope*/
                                companySupplierCode = StringUtil.addSpace(
                                    splitItem.get(Constants.TWO), Constants.SIX);
                                plantSupplierCode = splitItem.get(Constants.THREE);
                                boolean isExistSupplierPlantByScope = 
                                    searchExistSupplierPlantWithScope(companySupplierCode, 
                                        plantSupplierCode, dataScopeControlDomain);
                                if(!isExistSupplierPlantByScope){
                                    /**Current user not have a right to working on this Supplier plant.*/
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR017;
                                    createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                        lineNo);
                                    informationValidationComplete = false;
                                }
                            }else{
                                /**** Check existing supplier plant not found in the master data*//*
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR016;
                                    createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                        lineNo);
                                    informationValidationComplete = false;*/
                            }
                        }
                    }
                    
                    /**** Check NULL department name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOUR))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR018;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    }

                    /**** Check department name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOUR))
                        && Constants.MAX_DEPARTMENT_LENGTH < splitItem.get(Constants.FOUR)
                            .length())
                    {
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR019;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check NULL first name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIVE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR020;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check first name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIVE))
                        && Constants.MAX_FIRST_NAME_LENGTH < splitItem.get(Constants.FIVE)
                            .length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR021;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check first name is not including by numeric*/
                    /*isLetter = splitItem.get(Constants.FIVE).matches(
                            Constants.REGX_LETTER_FORMAT);
                        if(!isLetter){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR022;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }*/

                    /**** Check NULL middle name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR023;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    }

                    /**** Check middle name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))
                        && Constants.MAX_MIDDLE_NAME_LENGTH < splitItem.get(Constants.SIX)
                            .length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR024;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check middle name is not including by numeric*/
                    /*isLetter = splitItem.get(Constants.SIX).matches(
                            Constants.REGX_LETTER_FORMAT);
                        if(!isLetter){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR025;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }*/

                    /**** Check NULL last name*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR026;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check last name length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVEN))
                        && Constants.MAX_LAST_NAME_LENGTH < splitItem.get(Constants.SEVEN)
                            .length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR027;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }

                    /**** Check last name is not including by numeric*/
                    /*isLetter = splitItem.get(Constants.SEVEN).matches(
                            Constants.REGX_LETTER_FORMAT);
                        if(!isLetter){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR028;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }*/
                    
                    /**** Check NULL telephone number*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHT))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR029;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    }
                    
                    /**** Check telephone number length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.EIGHT))
                        && Constants.MAX_TELEPHONE_LENGTH < splitItem.get(Constants.EIGHT).length())
                    {
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR030;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }
                    
                    /**** Check NULL email address*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR031;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }
                    
                    /**** Check email address length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINE))
                        && Constants.MAX_EMAIL_LENGTH < splitItem.get(Constants.NINE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR032;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }
                    
                    /**** Check format for each email address*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINE))
                        && splitItem.get(Constants.NINE).length() <= Constants.MAX_EMAIL_LENGTH)
                    {
                        String[] strEmail = StringUtils.splitPreserveAllTokens(
                            splitItem.get(Constants.NINE), Constants.SYMBOL_COMMA);
                        for(int i = Constants.ZERO; i < strEmail.length; i++){
                            boolean isEmail = 
                                strEmail[i].matches(Constants.REGX_EMAIL_FORMAT);
                            if(!isEmail){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR033;
                                createItemUploadError(supplierUserUploadingDomain, errorCode,
                                    lineNo);
                                informationValidationComplete = false;
                            }
                        }
                    }
                    
                    /**** Check NULL email to : Urgent Order*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR034;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, 
                            lineNo);
                        splitItem.set(Constants.TEN, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to : Urgent Order*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TEN))){
                        if(!splitItem.get(Constants.TEN).equals(Constants.STR_ONE)
                            && !splitItem.get(Constants.TEN).equals(Constants.STR_ZERO)){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR035;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            informationValidationComplete = false;
                        }
                    }

                    /**** Check NULL email to : Supplier information not found*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ELEVEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR036;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        splitItem.set(Constants.ELEVEN, Constants.STR_ZERO);
                    }

                    /**** Check data out of range email to : Supplier information not found*/
                    if(!StringUtil.checkNullOrEmpty( splitItem.get(Constants.ELEVEN))
                        && !splitItem.get(Constants.ELEVEN).equals(Constants.STR_ONE)
                        && !splitItem.get(Constants.ELEVEN).equals(Constants.STR_ZERO)){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR037;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        informationValidationComplete = false;
                    }
                    
                    /**** Check NULL email to : Allow revise*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWELVE))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR098;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        splitItem.set(Constants.TWELVE, Constants.STR_ZERO);
                    }else{
                        /**** Check data out of range email to : Allow revise*/
                        if(!splitItem.get(Constants.TWELVE).equals(Constants.STR_ZERO)
                            && !splitItem.get(Constants.TWELVE).equals(Constants.STR_ONE)){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR099;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }
                    }
                    
                    /**** Check NULL email to : Cancel Invoice*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.THIRTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR100;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        splitItem.set(Constants.THIRTEEN, Constants.STR_ZERO);
                    }else{
                        /**** Check data out of range email to : Cancel Invoice*/
                        if(!splitItem.get(Constants.THIRTEEN).equals(Constants.STR_ZERO)
                            && !splitItem.get(Constants.THIRTEEN).equals(Constants.STR_ONE)){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR101;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            informationValidationComplete = false;
                        }
                    }
                }/*End: Check information flag "N" value*/

                /**** Check NULL role flag*/
                if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINETEEN))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR053;
                    createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    roleValidationComplete = false;
                }

                /**** Check role flag missing code*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINETEEN))
                    && (!Constants.STR_A.equals(splitItem.get(Constants.NINETEEN))
                        && !Constants.STR_U.equals(splitItem.get(Constants.NINETEEN))
                        && !Constants.STR_T.equals(splitItem.get(Constants.NINETEEN))
                        && !Constants.STR_N.equals(splitItem.get(Constants.NINETEEN)))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR054;
                    createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                    roleValidationComplete = false;
                }
                
                /**** Check role flag "N" value*/
                if(!(Constants.STR_N.equals(splitItem.get(Constants.NINETEEN)))){
                    if(Constants.STR_N.equals(splitItem.get(Constants.EIGHTEEN))){
                        
                        /**** Check NULL  DSC ID*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                            /**** Check DSC ID length*/
                            if(Constants.MAX_DSC_ID_LENGTH < splitItem.get(Constants.ZERO).length())
                            {
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR003;
                                createItemUploadError(
                                    supplierUserUploadingDomain, errorCode, lineNo);
                                roleValidationComplete = false;
                                dscIdValidateFlag = false;
                            }else{
                                dscIdValidateFlag = true;
                            }
                            
                            /**Changed DSC ID Validation refer to 23/10/2014 12:00 am.*/
                            /**** Check DSC ID in numeric format*/
                            /*isNumber = splitItem.get(Constants.ZERO).matches(
                                    Constants.REGX_NUMBER_FORMAT);
                                if(!isNumber){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR006;
                                    createItemUploadError(supplierUserUploadingDomain, errorCode,
                                        lineNo);
                                    roleValidationComplete = false;
                                    dscIdValidateFlag = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }*/

                            /**** Check DSC ID not found in system (Role flag is "A")*/
                            /*if(Constants.STR_A.equals(splitItem.get(Constants.NINETEEN))){
                             *//**** Check existing user by DSC ID in master data*//*
                                    dscId = splitItem.get(Constants.ZERO);
                                    isExist = searchExistUserMasterData(dscId);
                                    if(!isExist){
                                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                        createItemUploadError(supplierUserUploadingDomain, 
                                            errorCode,
                                            lineNo);
                                        informationValidationComplete = false;
                                    }
                                }*/

                            /**** Check DSC ID  not found in system (Role flag is "U")*/
                            if(Constants.STR_U.equals(splitItem.get(Constants.NINETEEN))){
                                /** Check existing user by DSC ID in master and Temporary data */
                                dscId = splitItem.get(Constants.ZERO);
                                isExist = searchExistUserMasterData(dscId);
                                if(!isExist){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                    createItemUploadError(supplierUserUploadingDomain, 
                                        errorCode,
                                        lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }

                            /**** Check DSC ID  not found in system (Role flag is "T")*/
                            if(Constants.STR_T.equals(splitItem.get(Constants.NINETEEN))){
                                /** Check existing user by DSC ID in master and Temporary data */
                                dscId = splitItem.get(Constants.ZERO);
                                isExist = searchExistUserMasterData(dscId);
                                if(!isExist){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR005;
                                    createItemUploadError(supplierUserUploadingDomain, 
                                        errorCode,
                                        lineNo);
                                    informationValidationComplete = false;
                                }else{
                                    dscIdValidateFlag = true;
                                }
                            }
                        }else if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR002;
                            createItemUploadError(supplierUserUploadingDomain, errorCode,
                                lineNo);
                            roleValidationComplete = false;
                            dscIdValidateFlag = false;
                        }

                        /**** Check NULL Supplier company code*/
                        if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR010;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            informationValidationComplete = false;
                        }

                        /**** Check Supplier company code length*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                            && Constants.SIX < splitItem.get(Constants.TWO).length()){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR011;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            informationValidationComplete = false;
                        }
                        
                        /**** Check Supplier company code*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                            && splitItem.get(Constants.TWO).length() <= Constants.SIX){
                            /**** Check existing Supplier company in system*/
                            companySupplierCode = StringUtil.addSpace(splitItem.get(
                                Constants.TWO),
                                Constants.SIX);
                            isExistSupplierCompany = searchExistSupplierCompany(
                                companySupplierCode);
                            if(isExistSupplierCompany){
                                /**Check existing Supplier company in system by current user data scope*/
                                companySupplierCode = StringUtil.addSpace(splitItem.get(
                                    Constants.TWO), Constants.SIX);
                                boolean isExistSupplierCompanyByScope = 
                                    searchExistSupplierCompanyWithScope(companySupplierCode, 
                                        dataScopeControlDomain);
                                /** Current user not have a right to working on this DENSO company.*/
                                if(!isExistSupplierCompanyByScope){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR013;
                                    createItemUploadError(supplierUserUploadingDomain, 
                                        errorCode, lineNo);
                                    informationValidationComplete = false;
                                }
                            }else{
                                /**** Check existing Supplier company not found in the master data*/
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR012;
                                createItemUploadError(supplierUserUploadingDomain, errorCode,
                                    lineNo);
                                informationValidationComplete = false;
                            }
                        }
                    }
                    
                    /**** Check NULL role code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOURTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR040;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        roleValidationComplete = false;
                    }
                    
                    /**** Check role code length*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOURTEEN))
                        && Constants.TWO  < splitItem.get(Constants.FOURTEEN).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR041;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        roleValidationComplete = false;
                    }else{
                        /**** Check existing role in master data*/
                        SpsMRoleCriteriaDomain roleCriteriaDomain = 
                            new SpsMRoleCriteriaDomain();
                        roleCriteriaDomain.setRoleCd(splitItem.get(Constants.FOURTEEN));
                        int recordCount = spsMRoleService.searchCount(roleCriteriaDomain);
                        if(Constants.ZERO < recordCount){
                            roleCodeValidateFlag = true;
                        }else{
                            /**** not found role code in master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR042;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                        }
                    }
                    
                    /**** Check NULL assign role Supplier plant code*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.FIFTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR043;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        roleValidationComplete = false;
                    }else{
                        /**** Check assign role Supplier plant code length*/
                        if(Constants.ONE < splitItem.get(Constants.FIFTEEN).length()){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR044;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                        }else{
                            splitItem.set(Constants.FIFTEEN, StringUtil.rightPad(
                                splitItem.get(Constants.FIFTEEN), Constants.ONE));
                            
                            /**** Check assign role Supplier plant code*/
                            if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))
                                && isExistSupplierCompany){
                                companySupplierCode = StringUtil.addSpace(
                                    splitItem.get(Constants.TWO), Constants.SIX);
                                plantSupplierCode = splitItem.get(Constants.FIFTEEN);
                                boolean isExistSupplierPlant = searchExistSupplierPlant(
                                    companySupplierCode, plantSupplierCode);
                                if(isExistSupplierPlant){
                                    /*Check existing Supplier plant in system by current user data scope*/
                                    companySupplierCode = StringUtil.addSpace(
                                        splitItem.get(Constants.TWO), Constants.SIX);
                                    plantSupplierCode = splitItem.get(Constants.FIFTEEN);
                                    boolean isExistSupplierPlantByScope = 
                                        searchExistSupplierPlantWithScope(companySupplierCode, 
                                            plantSupplierCode, dataScopeControlDomain);
                                    if(isExistSupplierPlantByScope){
                                        roleSupplierPlantCodeValidateFlag = true;
                                    }else{
                                        /**Current user not have a right to working on this supplier plant.*/
                                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR046;
                                        createItemUploadError(supplierUserUploadingDomain,
                                            errorCode, lineNo);
                                        roleValidationComplete = false;
                                    }
                                }else{
                                    /** Check existing Supplier plant not found in the master data*/
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR045;
                                    createItemUploadError(supplierUserUploadingDomain, errorCode,
                                        lineNo);
                                    roleValidationComplete = false;
                                }
                            }
                        }
                    }
                    
                    /**** Check effective start date Null value*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR047;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        effectStartValidateFlag = false;
                        String effectiveStart = setEffectiveStartDate();
                        splitItem.set(Constants.SIXTEEN, effectiveStart);
                    }else{
                        /**** Check format effective start date*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))
                            && !DateUtil.isValidDate(splitItem.get(Constants.SIXTEEN), pattern)){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR048;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                            effectStartValidateFlag = false;
                        }else{
                            effectStartValidateFlag = true;
                        }
                    }
                    
                    /**** Check effective end date Null value*/
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVENTEEN))){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR049;
                        createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        effectEndValidateFlag = false;
                        String effectiveEnd = setEffectiveEndDate();
                        splitItem.set(Constants.SEVENTEEN, effectiveEnd);
                    }else{
                        /**** Check format effective end date*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVENTEEN))
                            && !DateUtil.isValidDate(splitItem.get(Constants.SEVENTEEN), pattern))
                        {
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR050;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                            effectEndValidateFlag = false;
                        }else{
                            effectEndValidateFlag = true;
                        }
                    }
                    
                    /**** Check effective start date more than effective end date*/
                    if(effectEndValidateFlag && effectStartValidateFlag){
                        if(Constants.ZERO < DateUtil.compareDate(splitItem.get(
                            Constants.SIXTEEN), splitItem.get(Constants.SEVENTEEN))){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR051;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                            roleValidationComplete = false;
                        }
                    }
                    
                    if(effectEndValidateFlag){
                        /**** Check effective end date less than current date*/
                        if(DateUtil.compareDate(splitItem.get(Constants.SEVENTEEN), 
                            DateUtil.format(currentDate, pattern)) < Constants.ZERO){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR052;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, lineNo);
                        }
                    }
                    
                    /**** Check already assigned user role*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.NINETEEN))
                        && dscIdValidateFlag && roleCodeValidateFlag
                        && roleSupplierPlantCodeValidateFlag
                        && Constants.STR_A.equals(splitItem.get(Constants.NINETEEN))){
                        /*Check existing user role in system*/
                        dscId = splitItem.get(Constants.ZERO);
                        roleCode = splitItem.get(Constants.FOURTEEN);
                        companySupplierCode  =   StringUtil.addSpace(
                            splitItem.get(Constants.TWO), Constants.SIX);
                        plantSupplierCode = splitItem.get(Constants.FIFTEEN);
                        isActive = Constants.IS_ACTIVE;
                        returnValue = searchExistUserRole(dscId, roleCode, 
                            companySupplierCode, plantSupplierCode, isActive);
                        boolean isExistUserRoleTmp = searchExistUserRoleTmpData(dscId, roleCode,
                            companySupplierCode, plantSupplierCode, userDscId, sessionCd);
                        if(returnValue){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR055;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                        }else{
                            if(isExistUserRoleTmp){
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR055;
                                createItemUploadError(supplierUserUploadingDomain, errorCode,
                                    lineNo);
                                roleValidationComplete = false;
                            }
                        }
                    }
                    
                    /**Check user role not found in system(in case of flag is update "U")*/
                    if(dscIdValidateFlag && roleCodeValidateFlag
                        && roleSupplierPlantCodeValidateFlag
                        && Constants.STR_U.equals(splitItem.get(Constants.NINETEEN))){
                        /*Check existing user role in system*/
                        dscId = splitItem.get(Constants.ZERO);
                        roleCode = splitItem.get(Constants.FOURTEEN);
                        companySupplierCode  =   StringUtil.addSpace(
                            splitItem.get(Constants.TWO), Constants.SIX);
                        plantSupplierCode = splitItem.get(Constants.FIFTEEN);
                        isActive =  Constants.IS_ACTIVE;
                        
                        returnValue = searchExistUserRole(dscId, roleCode,
                            companySupplierCode, plantSupplierCode, isActive);
                        boolean isExistUserRoleTmp = searchExistUserRoleTmpData(dscId, roleCode,
                            companySupplierCode, plantSupplierCode, userDscId, sessionCd);
                        if(!returnValue && !isExistUserRoleTmp){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR056;
                            createItemUploadError(supplierUserUploadingDomain, errorCode,
                                lineNo);
                            roleValidationComplete = false;
                        }
                    }
                    
                    /**Check user role not found in system(in case of flag is terminate"T")*/
                    if(dscIdValidateFlag && roleCodeValidateFlag
                        && roleSupplierPlantCodeValidateFlag
                        && Constants.STR_T.equals(splitItem.get(Constants.NINETEEN))){
                        /*Check existing user role in system*/
                        dscId = splitItem.get(Constants.ZERO);
                        roleCode = splitItem.get(Constants.FOURTEEN);
                        companySupplierCode  =   StringUtil.addSpace(
                            splitItem.get(Constants.TWO), Constants.SIX);
                        plantSupplierCode = splitItem.get(Constants.FIFTEEN);
                        isActive = Constants.IS_ACTIVE;
                        
                        returnValue = searchExistUserRole(dscId, roleCode,
                            companySupplierCode, plantSupplierCode, isActive);
                        boolean isExistUserRoleTmp = searchExistUserRoleTmpData(dscId, roleCode,
                            companySupplierCode, plantSupplierCode, userDscId, sessionCd);
                        if(!returnValue && !isExistUserRoleTmp){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR056;
                            createItemUploadError(supplierUserUploadingDomain, errorCode, 
                                lineNo);
                            roleValidationComplete = false;
                        }
                    }
                }/*End:Check role flag "N" value*/
                
                List<SpsMUserRoleDomain> userRoleList = new ArrayList<SpsMUserRoleDomain>();
                List<SpsMUserDomain> userList = new ArrayList<SpsMUserDomain>();
                List<SpsMUserSupplierDomain> supplierList = 
                    new ArrayList<SpsMUserSupplierDomain>();
                /**** Check Information validation and role validation is passed*/
                if(informationValidationComplete && roleValidationComplete){
                    if(Constants.STR_U.equals(splitItem.get(Constants.EIGHTEEN))
                        || Constants.STR_D.equals(splitItem.get(Constants.EIGHTEEN))){
                        if(isExist){
                            /*Get last update date from user master data*/
                            SpsMUserCriteriaDomain userCriteriaDomain = 
                                new SpsMUserCriteriaDomain();
                            userCriteriaDomain.setDscId(splitItem.get(Constants.ZERO));
                            userList = spsMUserService.searchByCondition(userCriteriaDomain);
                            
                            /*Get last update date from supplier user master data from */
                            SpsMUserSupplierCriteriaDomain supplierCriteriaDomain = 
                                new SpsMUserSupplierCriteriaDomain();
                            supplierCriteriaDomain.setDscId(splitItem.get(Constants.ZERO));
                            supplierList = spsMUserSupplierService
                                .searchByCondition(supplierCriteriaDomain);
                        }else{
                            SpsMUserSupplierDomain userSupplier = new SpsMUserSupplierDomain();
                            SpsMUserDomain userDomain = new SpsMUserDomain();
                            userSupplier.setLastUpdateDatetime(commonService.searchSysDate());
                            userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                            userList.add(userDomain);
                            supplierList.add(userSupplier);
                        }
                    }
                    
                    if(Constants.STR_U.equals(splitItem.get(Constants.NINETEEN))
                        || Constants.STR_T.equals(splitItem.get(Constants.NINETEEN))){
                        if(returnValue){
                            /*Get last update date from user role master data*/
                            SpsMUserRoleCriteriaDomain roleCriteriaDomain = 
                                new SpsMUserRoleCriteriaDomain();
                            roleCriteriaDomain.setDscId(splitItem.get(Constants.ZERO));
                            roleCriteriaDomain.setRoleCd(splitItem.get(Constants.FOURTEEN));
                            companySupplierCode = StringUtil.addSpace(splitItem.get(
                                Constants.TWO), Constants.SIX);
                            roleCriteriaDomain.setSCd(companySupplierCode);
                            roleCriteriaDomain.setSPcd(splitItem.get(Constants.FIFTEEN));
                            roleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                            userRoleList = spsMUserRoleService.searchByCondition(
                                roleCriteriaDomain);
                        }else{
                            SpsMUserRoleDomain userRole = new SpsMUserRoleDomain();
                            userRole.setLastUpdateDatetime(commonService.searchSysDate());
                            userRoleList.add(userRole);
                        }
                    }

                    /*Insert all record items to the temporary table*/
                    SpsTmpUserSupplierDomain tmpUserSupplierDomain =
                        new SpsTmpUserSupplierDomain();
                    Timestamp effectStart = null;
                    Timestamp effectEnd = null;
                    String roleCd = new String();
                    String roleSPcd = new String();
                    if(!(Constants.STR_N.equals(splitItem.get(Constants.NINETEEN)))){

                        /*Set format effective date if not null*/
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIXTEEN))){
                            Date effectStartUtil = DateUtil.parseToUtilDate(
                                splitItem.get(Constants.SIXTEEN), pattern);
                            effectStart = new Timestamp(effectStartUtil.getTime());
                        }
                        if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SEVENTEEN))){
                            Date effectEndUtil = DateUtil.parseToUtilDate(
                                splitItem.get(Constants.SEVENTEEN), pattern);
                            effectEnd = new Timestamp(effectEndUtil.getTime());
                        }

                        roleCd = splitItem.get(Constants.FOURTEEN);
                        roleSPcd = splitItem.get(Constants.FIFTEEN);
                    }else{
                        effectStart = null;
                        effectEnd = null;
                        roleCd = null;
                        roleSPcd = null;
                    }
                    /*Set user and user supplier last update date time temporary upload table*/
                    if(Constants.STR_U.equals(splitItem.get(Constants.EIGHTEEN))
                        || Constants.STR_D.equals(splitItem.get(Constants.EIGHTEEN))){

                        SpsMUserDomain userDomain = userList.get(Constants.ZERO);
                        SpsMUserSupplierDomain userSupplier = supplierList.get(
                            Constants.ZERO);
                        tmpUserSupplierDomain.setSupplierLastUpdate(userSupplier
                            .getLastUpdateDatetime());
                        tmpUserSupplierDomain.setUserLastUpdate(
                            userDomain.getLastUpdateDatetime());

                    }

                    /*Set role last update date time to temporary upload table*/
                    if(Constants.STR_U.equals(splitItem.get(Constants.NINETEEN))
                        || Constants.STR_T.equals(splitItem.get(Constants.NINETEEN))){
                        SpsMUserRoleDomain userRole = userRoleList.get(Constants.ZERO);
                        tmpUserSupplierDomain.setRoleLastUpdate(userRole.getLastUpdateDatetime());
                    }

                    tmpUserSupplierDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
                    tmpUserSupplierDomain.setSessionCd(supplierUserUploadingDomain
                        .getSessionCode());
                    tmpUserSupplierDomain.setLineNo(BigDecimal.valueOf(lineNo));
                    tmpUserSupplierDomain.setDscId(splitItem.get(Constants.ZERO));
                    tmpUserSupplierDomain.setDOwner(splitItem.get(Constants.ONE));
                    tmpUserSupplierDomain.setSCd(splitItem.get(Constants.TWO));
                    tmpUserSupplierDomain.setSPcd(splitItem.get(Constants.THREE));
                    tmpUserSupplierDomain.setDepartmentCd(splitItem.get(Constants.FOUR));
                    tmpUserSupplierDomain.setFirstName(splitItem.get(Constants.FIVE));
                    tmpUserSupplierDomain.setMiddleName(splitItem.get(Constants.SIX));
                    tmpUserSupplierDomain.setLastName(splitItem.get(Constants.SEVEN));
                    tmpUserSupplierDomain.setTelephone(splitItem.get(Constants.EIGHT));
                    tmpUserSupplierDomain.setEmail(splitItem.get(Constants.NINE));
                    tmpUserSupplierDomain.setEmlUrgentOrderFlag(splitItem.get(Constants.TEN));
                    tmpUserSupplierDomain.setEmlSInfoNotfoundFlag(splitItem.get(Constants.ELEVEN));
                    tmpUserSupplierDomain.setEmlAllowReviseFlag(splitItem.get(Constants.TWELVE));
                    tmpUserSupplierDomain.setEmlCancelInvoiceFlag(
                        splitItem.get(Constants.THIRTEEN));
                    tmpUserSupplierDomain.setRoleCd(roleCd);
                    tmpUserSupplierDomain.setRoleSPcd(roleSPcd);
                    tmpUserSupplierDomain.setEffectStart(effectStart);
                    tmpUserSupplierDomain.setEffectEnd(effectEnd);
                    tmpUserSupplierDomain.setInformationFlag(splitItem.get(Constants.EIGHTEEN));
                    tmpUserSupplierDomain.setRoleFlag(splitItem.get(Constants.NINETEEN));
                    tmpUserSupplierDomain.setIsActualRegister(Constants.IS_NOT_ACTIVE);
                    tmpUserSupplierDomain.setUploadDatetime(commonService.searchSysDate());
                    tmpUserSupplierDomain.setToActualDatetime(null);
                    spsTmpUserSupplierService.create(tmpUserSupplierDomain);
                }/* End: Check Information validation and role validation is passed*/
            }/* End: LOOP*/
            
            /*Calculate total record*/
            int totalRecord = lineNo;
            int numberOfWarning = 0;
            int numberOfError = 0;
            int numberOfCorrect = 0;
            
            /*Check Existing data in TmpUploadUserSupplier table.*/
            SpsTmpUserSupplierCriteriaDomain tmpUserSupplierCriteriaDomain =
                new SpsTmpUserSupplierCriteriaDomain();
            tmpUserSupplierCriteriaDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
            tmpUserSupplierCriteriaDomain.setSessionCd(supplierUserUploadingDomain
                .getSessionCode());
            int countUserSupplier = spsTmpUserSupplierService.searchCount(
                tmpUserSupplierCriteriaDomain);
            if(Constants.ZERO < countUserSupplier){
                /*Get number of correct record*/
                numberOfCorrect
                    = spsTmpUserSupplierService.searchCount(tmpUserSupplierCriteriaDomain);
            }
            
            /*Check Existing data in TmpUploadError table.*/
            SpsTmpUploadErrorCriteriaDomain uploadErrorCriteriaDomain = new 
                SpsTmpUploadErrorCriteriaDomain();
            uploadErrorCriteriaDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
            uploadErrorCriteriaDomain.setSessionCd(supplierUserUploadingDomain.getSessionCode());
            int count = spsTmpUploadErrorService.searchCount(uploadErrorCriteriaDomain);
            if(Constants.ZERO < count){
                /*Get number of warning record*/
                GroupUploadErrorDomain groupUploadErrorDomain = new GroupUploadErrorDomain();
                groupUploadErrorDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(supplierUserUploadingDomain.getSessionCode());
                groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
                numberOfWarning = tmpUploadErrorService.searchCountWarning(groupUploadErrorDomain);
                
                /*Get number of incorrect record*/
                groupUploadErrorDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(supplierUserUploadingDomain.getSessionCode());
                groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
                numberOfError = tmpUploadErrorService.searchCountIncorrect(groupUploadErrorDomain);
                
                int maxRecord = tmpUploadErrorService.searchCountGroupOfValidationError(
                    groupUploadErrorDomain);
                
                /* Get Maximum record limit*/
                MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
                recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
                recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM004_RLM);
                recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
                if(null == recordsLimit){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                /*Check max record*/
                if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[]{recordsLimit.getMiscValue()});
                }
                
                /* Get Maximum record per page limit*/
                MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
                recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
                recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM003_PLM);
                recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
                if(null == recordLimitPerPage){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                /*Calculate row number for query*/
                supplierUserUploadingDomain.setPageNumber(supplierUserUploadingDomain
                    .getPageNumber());
                supplierUserUploadingDomain.setMaxRowPerPage(Integer.valueOf(
                    recordLimitPerPage.getMiscValue()));
                SpsPagingUtil.calcPaging(supplierUserUploadingDomain, maxRecord);
                
                /*Get list of validation error and warning and group by code number*/
                List<GroupUploadErrorDomain> result = new ArrayList<GroupUploadErrorDomain>();
                groupUploadErrorDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(supplierUserUploadingDomain.getSessionCode());
                groupUploadErrorDomain.setRowNumFrom(supplierUserUploadingDomain.getRowNumFrom());
                groupUploadErrorDomain.setRowNumTo(supplierUserUploadingDomain.getRowNumTo());
                result = tmpUploadErrorService.searchGroupOfValidationError(groupUploadErrorDomain);
                supplierUserUploadingDomain.setUploadErrorDetailDomain(result);
            }
            /*Set data to domain*/
            supplierUserUploadingDomain.setTotalRecord(String.valueOf(totalRecord));
            supplierUserUploadingDomain.setCorrectRecord(String.valueOf(numberOfCorrect));
            supplierUserUploadingDomain.setInCorrectRecord(String.valueOf(numberOfError));
            supplierUserUploadingDomain.setWarningRecord(String.valueOf(numberOfWarning));
            
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }finally{
            if(null != inputStream){
                inputStream.close();
            }
            if(null != inputStreamReader){
                inputStreamReader.close();
            }
            if(null != bufferReader){
                bufferReader.close();
            }
        }
        return supplierUserUploadingDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserUploadingFacadeServiceImpl#transactRegisterUploadItem
     * (com.globaldenso.asia.sps.business.domain.SupplierUserUploadingDomain)
     */
    public void transactRegisterUploadItem(SupplierUserUploadingDomain supplierUserUploadingDomain)
        throws ApplicationException{
        Locale locale = supplierUserUploadingDomain.getLocale();
        
        try{
            /*Get record count in temporary table*/
            SpsTmpUserSupplierCriteriaDomain tmpUserSupplierCriteriaDomain = 
                new SpsTmpUserSupplierCriteriaDomain();
            tmpUserSupplierCriteriaDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
            tmpUserSupplierCriteriaDomain.setSessionCd(supplierUserUploadingDomain
                .getSessionCode());
            int recordCount = spsTmpUserSupplierService.searchCount(tmpUserSupplierCriteriaDomain);
            if(recordCount <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0039,
                    SupplierPortalConstant.LBL_SUPPLIER_USER);
            }
            
            /*Get each record from temporary table*/
            for(int i = Constants.ZERO; i < recordCount; i++){
                
                /*Get temporary data each record to process*/
                TmpUserSupplierDomain criteriaDomain = new TmpUserSupplierDomain();
                criteriaDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
                criteriaDomain.setSessionCode(supplierUserUploadingDomain.getSessionCode());
                TmpUserSupplierDomain tempUploadSupplierUserDomain = tmpUploadUserSupplierService
                    .searchTmpUploadSupplierOneRecord(criteriaDomain);
                if(null == tempUploadSupplierUserDomain){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0039,
                        SupplierPortalConstant.LBL_SUPPLIER_USER);
                }
                
                /*Supplier user information Operation by flag values.*/
                if(Constants.STR_A.equals(tempUploadSupplierUserDomain.getInformationFlag())){
                    try{
                        /*Create new user information to the system*/
                        SpsMUserDomain userDomain = new SpsMUserDomain();
                        userDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                        userDomain.setFirstName(tempUploadSupplierUserDomain.getFirstName());
                        if(!StringUtil.checkNullOrEmpty(
                            tempUploadSupplierUserDomain.getMiddleName())){
                            userDomain.setMiddleName(tempUploadSupplierUserDomain.getMiddleName());
                        }
                        userDomain.setLastName(tempUploadSupplierUserDomain.getLastName());
                        if(!StringUtil.checkNullOrEmpty(tempUploadSupplierUserDomain
                            .getDepartmentCode())){
                            userDomain.setDepartmentName(tempUploadSupplierUserDomain
                                .getDepartmentCode());
                        }
                        userDomain.setEmail(tempUploadSupplierUserDomain.getEmail());
                        userDomain.setTelephone(tempUploadSupplierUserDomain.getTelephone());
                        userDomain.setIsActive(Constants.IS_ACTIVE);
                        userDomain.setUserType(Constants.STR_S);
                        /*userType not exist in table.*/
                        userDomain.setCreateDscId(supplierUserUploadingDomain.getUserDscId());
                        userDomain.setCreateDatetime(commonService.searchSysDate());
                        userDomain.setLastUpdateDscId(supplierUserUploadingDomain.getUserDscId());
                        userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                        
                        //Check existing inActive user in user table.
                        SpsMUserCriteriaDomain userCriteria = new SpsMUserCriteriaDomain();
                        SpsMUserSupplierCriteriaDomain userSupplierCriteria = 
                            new SpsMUserSupplierCriteriaDomain();
                        SpsMUserRoleCriteriaDomain spsMUserRoleCriteria = 
                            new SpsMUserRoleCriteriaDomain();
                        userCriteria.setDscId(tempUploadSupplierUserDomain.getDscId());
                        userCriteria.setIsActive(Constants.IS_NOT_ACTIVE);
                        userSupplierCriteria.setDscId(tempUploadSupplierUserDomain.getDscId());
                        spsMUserRoleCriteria.setDscId(tempUploadSupplierUserDomain.getDscId());
                        spsMUserRoleCriteria.setIsActive(Constants.IS_NOT_ACTIVE);
                        int countExist = spsMUserService.searchCount(userCriteria);
                        if(Constants.ZERO < countExist){
                            //Delete existing inActive user in table.
                            spsMUserService.deleteByCondition(userCriteria);
                            spsMUserSupplierService.deleteByCondition(userSupplierCriteria);
                            spsMUserRoleService.deleteByCondition(spsMUserRoleCriteria);
                        }
                        spsMUserService.create(userDomain);
                    }catch(Exception e){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_REGISTER_USER_INFO);
                    }
                    
                    try{
                        /*Create new Supplier user information to the system*/
                        SpsMUserSupplierDomain userSupplierDomain = new SpsMUserSupplierDomain();
                        userSupplierDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                        userSupplierDomain.setDOwner(tempUploadSupplierUserDomain.getDensoOwner());
                        userSupplierDomain.setSCd(tempUploadSupplierUserDomain.getSCd());
                        userSupplierDomain.setSPcd(tempUploadSupplierUserDomain.getSPcd());
                        userSupplierDomain.setEmlUrgentOrderFlag(tempUploadSupplierUserDomain
                            .getEmlUrgentOrder());
                        userSupplierDomain.setEmlSInfoNotfoundFlag(tempUploadSupplierUserDomain
                            .getEmlSInfoNotfound());
                        userSupplierDomain.setEmlAllowReviseFlag(
                            tempUploadSupplierUserDomain.getEmlAllowRevise());
                        userSupplierDomain.setEmlCancelInvoiceFlag(
                            tempUploadSupplierUserDomain.getEmlCancelInvoice());
                        userSupplierDomain.setCreateDscId(supplierUserUploadingDomain
                            .getUserDscId());
                        userSupplierDomain.setCreateDatetime(commonService.searchSysDate());
                        userSupplierDomain.setLastUpdateDscId(supplierUserUploadingDomain
                            .getUserDscId());
                        userSupplierDomain.setLastUpdateDatetime(commonService.searchSysDate());
                        spsMUserSupplierService.create(userSupplierDomain);
                    }catch(Exception e){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_REGISTER_SUPPLIER_USER_INFO);
                    }
                }else if(Constants.STR_U.equals(tempUploadSupplierUserDomain.getInformationFlag())){
                    /*Update Supplier user information to the system*/
                    SpsMUserSupplierCriteriaDomain userSupplierCriteriaDomain = 
                        new SpsMUserSupplierCriteriaDomain();
                    SpsMUserSupplierDomain userSupplierDomain = new SpsMUserSupplierDomain();
                    userSupplierDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userSupplierDomain.setDOwner(tempUploadSupplierUserDomain.getDensoOwner());
                    userSupplierDomain.setSCd(tempUploadSupplierUserDomain
                        .getSCd());
                    userSupplierDomain.setSPcd(tempUploadSupplierUserDomain.getSPcd());
                    userSupplierDomain.setEmlUrgentOrderFlag(tempUploadSupplierUserDomain
                        .getEmlUrgentOrder());
                    userSupplierDomain.setEmlSInfoNotfoundFlag(tempUploadSupplierUserDomain
                        .getEmlSInfoNotfound());
                    userSupplierDomain.setEmlAllowReviseFlag(
                        tempUploadSupplierUserDomain.getEmlAllowRevise());
                    userSupplierDomain.setEmlCancelInvoiceFlag(
                        tempUploadSupplierUserDomain.getEmlCancelInvoice());
                    userSupplierDomain.setLastUpdateDscId(supplierUserUploadingDomain
                        .getUserDscId());
                    userSupplierDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    /*Set criteria Domain*/
                    userSupplierCriteriaDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userSupplierCriteriaDomain.setLastUpdateDatetime(tempUploadSupplierUserDomain
                        .getSupplierLastUpdate());
                    
                    int updateRecord = spsMUserSupplierService.updateByCondition(userSupplierDomain,
                        userSupplierCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_UPDATE_SUPPLIER_USER_INFO);
                    }
                    
                    /*Update user information to the system*/
                    SpsMUserCriteriaDomain userCriteriaDomain = new SpsMUserCriteriaDomain();
                    SpsMUserDomain userDomain = new SpsMUserDomain();
                    userDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userDomain.setFirstName(tempUploadSupplierUserDomain.getFirstName());
                    if(!StringUtil.checkNullOrEmpty(
                        tempUploadSupplierUserDomain.getMiddleName())){
                        userDomain.setMiddleName(tempUploadSupplierUserDomain.getMiddleName());
                    }
                    userDomain.setLastName(tempUploadSupplierUserDomain.getLastName());
                    if(!StringUtil.checkNullOrEmpty(tempUploadSupplierUserDomain
                        .getDepartmentCode())){
                        userDomain.setDepartmentName(tempUploadSupplierUserDomain
                            .getDepartmentCode());
                    }
                    userDomain.setEmail(tempUploadSupplierUserDomain.getEmail());
                    userDomain.setTelephone(tempUploadSupplierUserDomain.getTelephone());
                    userDomain.setIsActive(Constants.IS_ACTIVE);
                    userDomain.setUserType(Constants.STR_S);
                    userDomain.setLastUpdateDscId(supplierUserUploadingDomain
                        .getUserDscId());
                    userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    userCriteriaDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                    userCriteriaDomain.setLastUpdateDatetime(tempUploadSupplierUserDomain
                        .getUserLastUpdate());
                    
                    updateRecord = spsMUserService.updateByCondition(userDomain,
                        userCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_UPDATE_USER_INFO);
                    }
                }else if(Constants.STR_D.equals(tempUploadSupplierUserDomain.getInformationFlag())){
                    
                    /*Update user information (active flag to false) to the system*/
                    SpsMUserCriteriaDomain userCriteriaDomain = new SpsMUserCriteriaDomain();
                    SpsMUserDomain userDomain = new SpsMUserDomain();
                    userDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                    userDomain.setLastUpdateDscId(tempUploadSupplierUserDomain.getUserDscId());
                    userDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userCriteriaDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userCriteriaDomain.setLastUpdateDatetime(tempUploadSupplierUserDomain
                        .getUserLastUpdate());
                    userCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                    
                    int updateRecord = spsMUserService.updateByCondition(userDomain, 
                        userCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_DELETE_USER_INFO);
                    }
                }
                /*Supplier user role Operation by flag values.*/
                if(Constants.STR_A.equals(tempUploadSupplierUserDomain.getRoleFlag())){
                    /*Assign new role to supplier user*/
                    try{
                        SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
                        userRoleDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                        userRoleDomain.setRoleCd(tempUploadSupplierUserDomain.getRoleCode());
                        userRoleDomain.setSCd(tempUploadSupplierUserDomain.getSCd());
                        userRoleDomain.setSPcd(tempUploadSupplierUserDomain
                            .getRoleSupplierPlantCode());
                        userRoleDomain.setIsActive(Constants.IS_ACTIVE);
                        userRoleDomain.setEffectStart(tempUploadSupplierUserDomain
                            .getEffectStart());
                        userRoleDomain.setEffectEnd(tempUploadSupplierUserDomain.getEffectEnd());
                        userRoleDomain.setCreateDscId(tempUploadSupplierUserDomain.getUserDscId());
                        userRoleDomain.setCreateDatetime(commonService.searchSysDate());
                        userRoleDomain.setLastUpdateDscId(tempUploadSupplierUserDomain
                            .getUserDscId());
                        userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());

                        //Check existing inActive user role in table.
                        SpsMUserRoleCriteriaDomain spsMUserRoleCriteria = 
                            new SpsMUserRoleCriteriaDomain();
                        spsMUserRoleCriteria.setDscId(tempUploadSupplierUserDomain.getDscId());
                        spsMUserRoleCriteria.setIsActive(Constants.IS_NOT_ACTIVE);
                        int countRecord = spsMUserRoleService.searchCount(spsMUserRoleCriteria);
                        if(Constants.ZERO < countRecord){
                          //Delete existing inActive user role in table.
                            spsMUserRoleService.deleteByCondition(spsMUserRoleCriteria);
                        }
                        spsMUserRoleService.create(userRoleDomain);
                    }catch(Exception e){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_REGISTER_USER_ROLE);
                    }
                }else if(Constants.STR_U.equals(tempUploadSupplierUserDomain.getRoleFlag())){
                    /*Update existing supplier user role*/
                    SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = 
                        new SpsMUserRoleCriteriaDomain();
                    SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
                    userRoleDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userRoleDomain.setRoleCd(tempUploadSupplierUserDomain.getRoleCode());
                    userRoleDomain.setSCd(tempUploadSupplierUserDomain.getSCd());
                    userRoleDomain.setSPcd(tempUploadSupplierUserDomain.getRoleSupplierPlantCode());
                    userRoleDomain.setIsActive(Constants.IS_ACTIVE);
                    userRoleDomain.setEffectStart(tempUploadSupplierUserDomain.getEffectStart());
                    userRoleDomain.setEffectEnd(tempUploadSupplierUserDomain.getEffectEnd());
                    userRoleDomain.setLastUpdateDscId(tempUploadSupplierUserDomain.getUserDscId());
                    userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userRoleCriteriaDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userRoleCriteriaDomain.setRoleCd(tempUploadSupplierUserDomain.getRoleCode());
                    userRoleCriteriaDomain.setSCd(tempUploadSupplierUserDomain.getSCd());
                    userRoleCriteriaDomain.setSPcd(tempUploadSupplierUserDomain
                        .getRoleSupplierPlantCode());
                    userRoleCriteriaDomain.setLastUpdateDatetime(tempUploadSupplierUserDomain
                        .getRoleLastUpdate());

                    int updateRecord = spsMUserRoleService.updateByCondition(userRoleDomain, 
                        userRoleCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_UPDATE_USER_ROLE);
                    }
                }else if(Constants.STR_T.equals(tempUploadSupplierUserDomain.getRoleFlag())){
                    /*Set supplier user role to expired*/
                    SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = 
                        new SpsMUserRoleCriteriaDomain();
                    SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
                    userRoleDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userRoleDomain.setRoleCd(tempUploadSupplierUserDomain.getRoleCode());
                    userRoleDomain.setSCd(tempUploadSupplierUserDomain.getSCd());
                    userRoleDomain.setSPcd(tempUploadSupplierUserDomain.getRoleSupplierPlantCode());
                    userRoleDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                    userRoleDomain.setEffectStart(tempUploadSupplierUserDomain.getEffectStart());
                    userRoleDomain.setEffectEnd(tempUploadSupplierUserDomain.getEffectEnd());
                    userRoleDomain.setLastUpdateDscId(tempUploadSupplierUserDomain.getUserDscId());
                    userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    userRoleCriteriaDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                    userRoleCriteriaDomain.setRoleCd(tempUploadSupplierUserDomain.getRoleCode());
                    userRoleCriteriaDomain.setSCd(tempUploadSupplierUserDomain.getSCd());
                    userRoleCriteriaDomain.setSPcd(tempUploadSupplierUserDomain
                        .getRoleSupplierPlantCode());
                    userRoleCriteriaDomain.setLastUpdateDatetime(tempUploadSupplierUserDomain
                        .getRoleLastUpdate());
                    
                    int updateRecord = spsMUserRoleService.updateByCondition(userRoleDomain, 
                        userRoleCriteriaDomain);
                    if(updateRecord != Constants.ONE){
                        MessageUtil.throwsApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                            SupplierPortalConstant.LBL_DELETE_USER_ROLE);
                    }
                }
                /*Set flag (register to actual table) in temporary table*/
                
                SpsTmpUserSupplierDomain tmpUserSupplierDomain = new SpsTmpUserSupplierDomain();
                /*Set value to Domain*/
                tmpUserSupplierDomain.setIsActualRegister(Constants.IS_ACTIVE);
                tmpUserSupplierDomain.setToActualDatetime(commonService.searchSysDate());
                /*Set value to CriteriaDomain*/
                tmpUserSupplierCriteriaDomain.setDscId(tempUploadSupplierUserDomain.getDscId());
                tmpUserSupplierCriteriaDomain.setSessionCd(tempUploadSupplierUserDomain
                    .getSessionCode());
                tmpUserSupplierCriteriaDomain.setLineNo(tempUploadSupplierUserDomain.getLineNo());

                int updateRecord = spsTmpUserSupplierService.updateByCondition(
                    tmpUserSupplierDomain, tmpUserSupplierCriteriaDomain);
                if(updateRecord != Constants.ONE){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                        SupplierPortalConstant.LBL_UPDATE_MOVE_TO_ACTUAL_FLAG);
                }
            } /*END LOOP*/
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserUploadingFacadeServiceImpl#searchUploadErrorList
     * (com.globaldenso.asia.sps.business.domain.SupplierUserUploadingDomain)
     */
    public  SupplierUserUploadingDomain searchUploadErrorList(SupplierUserUploadingDomain 
        supplierUserUploadingDomain)throws ApplicationException{
        List<GroupUploadErrorDomain> result = new ArrayList<GroupUploadErrorDomain>();
        Locale locale = supplierUserUploadingDomain.getLocale();
        /*Get number of warning record*/
        GroupUploadErrorDomain groupUploadErrorDomain = new GroupUploadErrorDomain();
        groupUploadErrorDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(supplierUserUploadingDomain.getSessionCode());
        groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
        int numberOfWarning = tmpUploadErrorService.searchCountWarning(groupUploadErrorDomain);

        /*Get number of incorrect record */
        groupUploadErrorDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(supplierUserUploadingDomain.getSessionCode());
        groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
        int numberOfError = tmpUploadErrorService.searchCountIncorrect(groupUploadErrorDomain);

        /*Get number of correct record*/
        SpsTmpUserSupplierCriteriaDomain supplierCriteriaDomain = new 
            SpsTmpUserSupplierCriteriaDomain();
        supplierCriteriaDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
        supplierCriteriaDomain.setSessionCd(supplierUserUploadingDomain.getSessionCode());
        int numberOfCorrect = spsTmpUserSupplierService.searchCount(supplierCriteriaDomain);

        /*Calculate total record*/
        int totalRecord = numberOfCorrect + numberOfWarning + numberOfError;

        int maxRecord = tmpUploadErrorService.searchCountGroupOfValidationError(
            groupUploadErrorDomain);
        
        /* Get Maximum record limit*/
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM004_RLM);
        recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
        if(null == recordsLimit){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Check max record*/
        if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{recordsLimit.getMiscValue()});
        }
        
        /* Get Maximum record per page limit*/
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM003_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        /*Calculate row number for query*/
        supplierUserUploadingDomain.setPageNumber(supplierUserUploadingDomain.getPageNumber());
        supplierUserUploadingDomain.setMaxRowPerPage(Integer.valueOf(
            recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(supplierUserUploadingDomain, maxRecord);

        /*Get list of validation error and warning and group by code number*/
        groupUploadErrorDomain.setUserDscId(supplierUserUploadingDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(supplierUserUploadingDomain.getSessionCode());
        groupUploadErrorDomain.setRowNumFrom(supplierUserUploadingDomain.getRowNumFrom());
        groupUploadErrorDomain.setRowNumTo(supplierUserUploadingDomain.getRowNumTo());
        result = tmpUploadErrorService.searchGroupOfValidationError(groupUploadErrorDomain);

        supplierUserUploadingDomain.setTotalRecord(String.valueOf(totalRecord));
        supplierUserUploadingDomain.setCorrectRecord(String.valueOf(numberOfCorrect));
        supplierUserUploadingDomain.setInCorrectRecord(String.valueOf(numberOfError));
        supplierUserUploadingDomain.setWarningRecord(String.valueOf(numberOfWarning));
        supplierUserUploadingDomain.setUploadErrorDetailDomain(result);
            
        return supplierUserUploadingDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserUploadingFacadeServiceImpl#deleteUploadTempTable
     * (String userDscId, String sessionCode)
     */
    public void deleteUploadTempTable(String userDscId, String sessionCode)
        throws ApplicationException{
        
        SpsTmpUploadErrorCriteriaDomain criteriaDomain = new SpsTmpUploadErrorCriteriaDomain();
        SpsTmpUserSupplierCriteriaDomain supplierCriteriaDomain = 
            new SpsTmpUserSupplierCriteriaDomain();
        /*Set Upload Date time Less Than Equal to (CommonService.searchSysDate() - 2 day)*/
        Timestamp currentTimestamp = commonService.searchSysDate();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Calendar prevDate = Calendar.getInstance();
        prevDate.setTime(currentTimestamp);
        prevDate.add(Calendar.DATE, Constants.MINUS_TWO);
        String previousDate = DateUtil.format(prevDate.getTime(), patternTimestamp);
        Timestamp uploadDatetime = DateUtil.parseToTimestamp(previousDate, patternTimestamp);

        /*1.Delete temporary table that keep upload error items.*/
        if(!StringUtil.checkNullOrEmpty(userDscId) && !StringUtil.checkNullOrEmpty(sessionCode)){
            criteriaDomain.setSessionCd(sessionCode);
            criteriaDomain.setUserDscId(userDscId);
        }else{
            criteriaDomain.setUploadDatetimeLessThanEqual(uploadDatetime);
        }
        spsTmpUploadErrorService.deleteByCondition(criteriaDomain);

        /*2.Delete temporary table that keep upload Supplier user information*/
        if(!StringUtil.checkNullOrEmpty(userDscId) && !StringUtil.checkNullOrEmpty(sessionCode)){
            supplierCriteriaDomain.setSessionCd(sessionCode);
            supplierCriteriaDomain.setUserDscId(userDscId);
        }else{
            supplierCriteriaDomain.setUploadDatetimeLessThanEqual(uploadDatetime);
        }
        spsTmpUserSupplierService.deleteByCondition(supplierCriteriaDomain);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * 
     * <p>search Exist Supplier Company.</p>
     *
     * @param supplierCompanyCode the Supplier Company code to check existing supplier company.
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistSupplierCompany(String supplierCompanyCode)
        throws ApplicationException{
        /*Check existing Supplier company in the master data*/
        SpsMCompanySupplierCriteriaDomain criteriaDomain = new SpsMCompanySupplierCriteriaDomain();

        criteriaDomain.setSCd(supplierCompanyCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMCompanySupplierService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist Supplier Company With Scope.</p>
     *
     * @param supplierCompanyCode the Supplier Company code to check existing supplier company.
     * @param dataScopeControlDomain the Limitation to search condition.
     * @return boolean that keep true = found and false = not found.
     */
    private boolean searchExistSupplierCompanyWithScope(String supplierCompanyCode, 
        DataScopeControlDomain dataScopeControlDomain){
        /*Check existing Supplier company in the master data*/
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain = 
            new CompanySupplierWithScopeDomain();
        CompanySupplierDomain companySupplierDomain = new CompanySupplierDomain();
        companySupplierDomain.setIsActive(Constants.IS_ACTIVE);
        companySupplierDomain.setSCd(supplierCompanyCode);
        companySupplierWithScopeDomain.setCompanySupplierDomain(companySupplierDomain);
        companySupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        Integer count = companySupplierService.searchExistCompanySupplier(
            companySupplierWithScopeDomain);
        if(Constants.ZERO < count){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist Supplier Company With Scope.</p>
     *
     * @param supplierCompanyCode the Supplier Company code to check existing supplier company.
     * @param supplierPlantCode the Supplier plant code
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException. 
     */
    private boolean searchExistSupplierPlant(String supplierCompanyCode, String supplierPlantCode)
        throws ApplicationException{
        /*Check existing Supplier plant in the master data*/
        SpsMPlantSupplierCriteriaDomain criteriaDomain = new SpsMPlantSupplierCriteriaDomain();

        criteriaDomain.setSCd(supplierCompanyCode);
        criteriaDomain.setSPcd(supplierPlantCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);

        int countRecord = spsMPlantSupplierService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }

    }
    
    /**
     * 
     * <p>Search Exist Supplier Plant With Scope.</p>
     *
     * @param supplierCompanyCode the Supplier Company code to check existing supplier company.
     * @param supplierPlantCode the Supplier plant code
     * @param dataScopeControlDomain the Limitation to search condition.
     * @return boolean that keep true = found and false = not found.
     */
    private boolean searchExistSupplierPlantWithScope(String supplierCompanyCode, 
        String supplierPlantCode, DataScopeControlDomain dataScopeControlDomain){
        /*Check existing Supplier plant by current user data scope in the master data */
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        plantSupplierDomain.setSCd(supplierCompanyCode);
        plantSupplierDomain.setSPcd(supplierPlantCode);
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        Integer count = plantSupplierService.searchExistPlantSupplier(
            plantSupplierWithScopeDomain);
        if(Constants.ZERO < count){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist User Role.</p>
     *
     * @param dscId the User DSC ID number.
     * @param roleCode the Role code.
     * @param supplierCompanyCode the supplier company code.
     * @param supplierPlantCode the supplier plant code.
     * @param isActive the user role active flag.
     * @return boolean the true = use existing in master data, false = user  not in master data.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistUserRole(String dscId, String roleCode, String supplierCompanyCode,
        String supplierPlantCode, String isActive)throws ApplicationException{
        
        /*Check existing user role in the master data*/
        SpsMUserRoleCriteriaDomain criteriaDomain = new SpsMUserRoleCriteriaDomain();

        criteriaDomain.setDscId(dscId);
        criteriaDomain.setRoleCd(roleCode);
        criteriaDomain.setSCd(supplierCompanyCode);
        criteriaDomain.setSPcd(supplierPlantCode);
        criteriaDomain.setIsActive(isActive);
        int countRecord = spsMUserRoleService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }

    }
    
    /**
     * 
     * <p>create Item Upload Error.</p>
     *
     * @param uploadDomain It keep current user information to create error data to temporary table.
     * @param errorCode the Error code from validation.
     * @param lineNo the Line no from validation.
     * @throws ApplicationException the ApplicationException.
     */
    private void createItemUploadError(SupplierUserUploadingDomain uploadDomain, 
        String errorCode, int lineNo)throws ApplicationException{
        /*Create error from upload validation to temporary table*/
        SpsTmpUploadErrorDomain errorDomain = new SpsTmpUploadErrorDomain();

        errorDomain.setUserDscId(uploadDomain.getUserDscId());
        errorDomain.setSessionCd(uploadDomain.getSessionCode());
        errorDomain.setLineNo(BigDecimal.valueOf(lineNo));
        errorDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
        errorDomain.setMiscCd(errorCode);
        errorDomain.setFunctionCode(Constants.STR_ONE);
        errorDomain.setUploadDatetime(commonService.searchSysDate());
        
        SpsTmpUploadErrorCriteriaDomain uploadErrorCriteriaDomain = new 
            SpsTmpUploadErrorCriteriaDomain();
        uploadErrorCriteriaDomain.setUserDscId(uploadDomain.getUserDscId());
        uploadErrorCriteriaDomain.setSessionCd(uploadDomain.getSessionCode());
        uploadErrorCriteriaDomain.setLineNo(BigDecimal.valueOf(lineNo));
        uploadErrorCriteriaDomain.setMiscCd(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
        uploadErrorCriteriaDomain.setMiscType(errorCode);
        int count = spsTmpUploadErrorService.searchCount(uploadErrorCriteriaDomain);
        if(count <= Constants.ZERO){
            spsTmpUploadErrorService.create(errorDomain);
        }
        
    }
    
    /**
     * 
     * <p>search Exist User.</p>
     *
     * @param dscId the DSC ID number for search user information.
     * @return boolean the true = user DSC ID existing in master data, 
     * false = user DSC ID not in master data.
     * @throws ApplicationException the ApplicationException.
     */
    
    private boolean searchExistUserMasterData(String dscId)throws ApplicationException{
        /*Check existing user in master date*/
        SpsMUserCriteriaDomain criteriaDomain = new SpsMUserCriteriaDomain();

        criteriaDomain.setDscId(dscId);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMUserService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>search Exist User Temporary data.</p>
     *
     * @param dscId the DSC ID number for search user information.
     * @param informationFlag the Information flag for search user information.
     * @param userDscId the User DSC ID number.
     * @param sessionCd the session code.
     * @return boolean the true = user DSC ID existing in master data, 
     * false = user DSC ID not in master data.
     * @throws ApplicationException the ApplicationException.
     */
    
    private boolean searchExistUserTmpData(String dscId, String informationFlag, String userDscId, 
        String sessionCd) throws ApplicationException{
        /*Check existing user in Temporary date*/
        SpsTmpUserSupplierCriteriaDomain criteriaDomain = new SpsTmpUserSupplierCriteriaDomain();
        int countRecord = 0;
        criteriaDomain.setDscId(dscId);
        criteriaDomain.setUserDscId(userDscId);
        criteriaDomain.setSessionCd(sessionCd);
        
        // [IN015] search with parameter informationFlag only
        //if(!Constants.STR_A.equals(informationFlag)){
        //    /*Check existing user in Temporary data when information flag A*/
        //    criteriaDomain.setInformationFlag(Constants.STR_A);
        //    countRecord = spsTmpUserSupplierService.searchCount(criteriaDomain);
        //    if(Constants.ZERO < countRecord){
        //        return true;
        //    }else{
                /*Check existing user in Temporary data when information flag U*/
        //        criteriaDomain.setInformationFlag(Constants.STR_U);
        //        countRecord = spsTmpUserSupplierService.searchCount(criteriaDomain);
        //        if(Constants.ZERO < countRecord){
        //            return true;
        //        }else{
        //            return false;
        //        }
        //    }
        //}else{
        //    countRecord = spsTmpUserSupplierService.searchCount(criteriaDomain);
        //    if(Constants.ZERO < countRecord){
        //        return true;
        //    }else{
        //        return false;
        //    }
        //}
        criteriaDomain.setInformationFlag(informationFlag);
        countRecord = spsTmpUserSupplierService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>search Exist User Temporary data.</p>
     *
     * @param dscId the User DSC ID number.
     * @param roleCode the Role code.
     * @param supplierCompanyCode the supplier company code.
     * @param supplierPlantCode the supplier plant code.
     * @param userDscId the User DSC ID number.
     * @param sessionCd the session code.
     * @return boolean the true = user DSC ID existing in master data, 
     * false = user DSC ID not in master data.
     * @throws ApplicationException the applicationException.
     */
    private boolean searchExistUserRoleTmpData(String dscId, String roleCode, String
        supplierCompanyCode, String supplierPlantCode, String userDscId, String sessionCd)throws
        ApplicationException{
        /*Check existing user in Temporary date*/
        SpsTmpUserSupplierCriteriaDomain criteriaDomain = new SpsTmpUserSupplierCriteriaDomain();
        int countRecord = 0;
        criteriaDomain.setDscId(dscId);
        criteriaDomain.setRoleCd(roleCode);
        criteriaDomain.setSCd(supplierCompanyCode);
        criteriaDomain.setRoleSPcd(supplierPlantCode);
        criteriaDomain.setUserDscId(userDscId);
        criteriaDomain.setSessionCd(sessionCd);
        
        countRecord = spsTmpUserSupplierService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }

    } 
    
    /**
     * 
     * <p>set Effective end date.</p>
     *
     * @return effectiveEndDate
     * @throws Exception the Exception.
     */
    private String setEffectiveEndDate()throws Exception{
        String effectiveEnd = new String();
        Timestamp currentTimestamp = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        
        //Search limit of effect end date
        MiscellaneousDomain endDateLimit = new MiscellaneousDomain();
        endDateLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
        endDateLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_ADD_DTE);
        endDateLimit = recordLimitService.searchRecordLimit(endDateLimit);
        
        Integer effectDateLimit = Integer.valueOf(endDateLimit.getMiscValue());
        
        Calendar effectDate = Calendar.getInstance();
        effectDate.setTime(currentTimestamp);
        effectDate.add(Calendar.DATE, effectDateLimit);
        
        effectiveEnd = DateUtil.format(effectDate.getTime(), pattern);

        return effectiveEnd;
    }
    
    /**
     * 
     * <p>set Effective start date.</p>
     *
     * @return effectiveEndDate
     * @throws Exception the Exception.
     */
    private String setEffectiveStartDate()throws Exception{
        Timestamp currentTimestamp = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Date effectStartDate = new Date(currentTimestamp.getTime());
        String effectiveStart = DateUtil.format(effectStartDate, pattern);
        return effectiveStart;
    }

}