/*
 * ModifyDate Development company     Describe 
 * 2014/06/11 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.business.domain.CnDnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface CNDNUploadingFacadeService.</p>
 * <p>Facade for CNDNUploadingFacadeService.</p>
 * <ul>
 * <li>Method create  : transactUpload</li>
 * </ul>
 *
 * @author CSI
 */
public interface CnDnUploadingFacadeService {
    
    /**
     * <p>Upload CN/DN data and create upload information </p>
     * <ul>
     * <li>Call createFileUpload method of FileManagementService class in order to upload data 
     * into database and Call createFileUpload method of FileUploadService class in order to
     * insert information data into database</li>
     * </ul>
     * 
     * @param fileManagementDomain the file management domain
     * @param spsTFileUploadDomain the file upload domain
     * @param fileSize the file upload size
     * @param maxCsvFileSize the file upload max size
     * @return the string
     * @throws ApplicationException ApplicationException
     */
    public CnDnUploadingDomain transactUploadCnDn(FileManagementDomain fileManagementDomain,
        SpsTFileUploadDomain spsTFileUploadDomain, int fileSize , int maxCsvFileSize) 
        throws ApplicationException;
    

    /**
     * <p>Upload CN/DN data and create upload information </p>
     * <ul>
     * <li>Call initial method of CNDNUploadingFacadeService class in order to upload data 
     * into database and Call initial method of FileUploadService class in order to
     * insert information data into database</li>
     * </ul>
     * 
   
     *@param dataScopeControlDomain the file upload domain
     * @return the DataScopeControlDomain
     * @throws ApplicationException ApplicationException
     */
    public CnDnUploadingDomain searchInitial(DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException ;
    /**
     * <p>Upload CN/DN data and create upload information </p>
     * <ul>
     * <li>Call changeSelectSupplierCompany method of changeSelectSupplierCompany class in order 
     * to upload data into database and Call initial method of FileUploadService class in order to
     * insert information data into database</li>
     * </ul>
     * 
   
     *@param plantSupplierWithScopeDomain the file upload domain
     * @return the DataScopeControlDomain
     *  @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier
    (PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScope PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope) throws ApplicationException;

    /**
     * <p>
     * Search Selected Company Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}