/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationReturnDomain;

/**
 * <p>The Interface SupplierUserFacadeService.</p>
 * <p>Service for Supplier User about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search  : initial</li>
 * <li>Method search  : initialWithCriteria</li>
 * <li>Method delete  : searchSelectedCompanySupplier</li>
 * <li>Method delete  : searchSelectedPlantSupplier</li>
 * <li>Method create  : searchSupplierUser</li>
 * <li>Method create  : deleteSupplierUser</li>
 * <li>Method create  : downloadSupplierUser</li>
 * </ul>
 *
 * @author CSI
 */
public interface SupplierUserInformationFacadeService {
    
    /**
     * <p>Initial Supplier User Information Screen.</p>
     * <ul>
     * <li>Search Company supplier detail for initial supplier user information screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the It keep user type, user role, commercial relation between
     * DENSO and Supplier , and selected Supplier Company.
     * @return the DataScopeControlDomain that keep DENSO and Supplier relation.
     * @throws ApplicationException ApplicationException
     */
    public DataScopeControlDomain searchDensoSupplierRelation(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>Initial Supplier User Information Screen.</p>
     * <ul>
     * <li>Search Company supplier detail for initial supplier user information screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the DataScopeControlDomain
     * @return the SupplierUserInfoDomain that keep list of supplier Company.
     * @throws ApplicationException ApplicationException
     */
    public SupplierUserInformationDomain searchInitial(DataScopeControlDomain
        dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>Initial Supplier User Information Screen with criteria.</p>
     * <ul>
     * <li>Search for initial screen when return from WADM002 and WADM004.</li>
     * </ul>
     * 
     * @param supplierUserInfoDomain the SupplierUserInfoDomain.
     * @return the SupplierUserInfoDomain.
     * @throws ApplicationException ApplicationException
     */
    public SupplierUserInformationDomain searchInitialWithCriteria(SupplierUserInformationDomain
        supplierUserInfoDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select supplier company code.</p>
     * <ul>
     * <li>Search list of supplier plant to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return SpsMPlantSupplierDomain the List of supplier plant to show in combo box
     * @throws ApplicationException the ApplicationException
     */
    public List<PlantSupplierDomain>  searchSelectedCompanySupplier(PlantSupplierWithScopeDomain
        plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant supplier code.</p>
     * <ul>
     * <li>Search list of company supplier to show in combo box.</li>
     * </ul>
     * 
     * @param companySupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Plant Supplier
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Search Supplier User.</p>
     * <ul>
     * <li>Search Supplier User for display on Inquiry Supplier User screen.</li>
     * </ul>
     * 
     * @param supplierUserInformationReturnDomain It keep the criteria to search supplier
     * user information.
     * @return the SupplierUserInformationReturnDomain that keep list of supplier user 
     * and error message list.
     * @throws ApplicationException the Exception.
     */
    public SupplierUserInformationReturnDomain searchUserSupplier(
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain)
        throws ApplicationException;
   
    /**
     * <p>Delete Supplier User</p>
     * <ul>
     * <li>Update supplier user(s) to inactive by criteria.</li>
     * </ul>
     * 
     * @param supplierUserInfoDomain It keep the criteria to delete supplier user information.
     * @return SupplierUserInformationReturnDomain that keep list of supplier user 
     * and error message list.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserInformationReturnDomain deleteUserSupplier(SupplierUserInformationDomain
        supplierUserInfoDomain)throws ApplicationException;
    
    /**
     * <p>Download Supplier User</p>
     * <ul>
     * <li>Load data to CSV file by criteria for export file.</li>
     * </ul>
     * 
     * @param supplierUserInfoDomain It keep the criteria to delete supplier user information.
     * @return SupplierUserInformationReturnDomain that keep stream of CSV file error message list.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierUserInformationReturnDomain searchUserSupplierCsv(SupplierUserInformationDomain
        supplierUserInfoDomain)throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}