/*
 * ModifyDate Development company       Describe 
 * 2014/08/04 CSI Parichat              Create
 * 2018/04/11 Netband U.Rungsiwut       Generate ASN PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;

/**
 * <p>The Interface AsnMaintenanceFacadeService.</p>
 * <p>Facade for ASN Maintenance.</p>
 * <ul>
 * <li>Method search  : searchInitialWshp001</li>
 * <li>Method search  : searchInitialWshp008</li>
 * <li>Method search  : searchInitialWshp006</li>
 * <li>Method update  : transactSaveAndSendAsn</li>
 * <li>Method update  : transactCancelAsnAndSend</li>
 * <li>Method create  : createViewAsnReport</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchDownloadAsnReport</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method delete  : deleteTmpUploadAsn</li>
 * <li>Method search  : searchSendingEmail</li>
 * <li>Method search  : transactAllowReviseQty</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnMaintenanceFacadeService {
    
    /**
     * <p>Search Initial WSHP001.</p>
     * <ul>
     * <li>Initial from WSHP001 screen.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the asn maintenance domain.
     * @throws ApplicationException ApplicationException
     */
    public AsnMaintenanceDomain searchInitialWshp001(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException;
    
    /**
     * <p>Search Initial WSHP008.</p>
     * <ul>
     * <li>Initial from WSHP008 screen.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the asn maintenance domain.
     * @throws ApplicationException ApplicationException
     */
    public AsnMaintenanceDomain searchInitialWshp008(AsnMaintenanceDomain asnMaintenanceDomain) 
        throws ApplicationException;
    
    /**
     * <p>Search Initial WSHP006.</p>
     * <ul>
     * <li>Initial from WSHP006 screen.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the asn maintenance domain.
     * @throws ApplicationException ApplicationException
     */
    public AsnMaintenanceDomain searchInitialWshp006(AsnMaintenanceDomain asnMaintenanceDomain) 
        throws ApplicationException;
    
    /**
     * <p>Transact Save and Send Asn.</p>
     * <ul>
     * <li>Save detail data of ASN and set ASN status to "Issued".</li>
     * <li>Send ASN data to Cigma system.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the boolean
     * @throws ApplicationException ApplicationException
     * @throws Exception Exception
     */
    public boolean transactSaveAndSendAsn(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException, Exception;
    
    /**
     * <p>Transact Cancel Asn and Send.</p>
     * <ul>
     * <li>Cancel sending ASN data and set ASN status to "Cancel".</li>
     * <li>Cancel ASN data from CIGMA system</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @throws ApplicationException ApplicationException
     */
    public void transactCancelAsnAndSend(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException; 
    
    /**
     * <p>Create view asn report.</p>
     * <ul>
     * <li>Create ASN report PDF file (Without ASN no.)</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return ASN Report data
     * @throws ApplicationException ApplicationException
     * @throws Exception Exception
     */
    public InputStream createViewAsnReport(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException, Exception;
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search PDF file name from PDF file id.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the file management domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(AsnMaintenanceDomain asnMaintenanceDomain) 
        throws ApplicationException;
    
    /**
     * <p>Search download asn report.</p>
     * <ul>
     * <li>Search download asn report from PDF file id.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @param outputStream the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchDownloadAsnReport (AsnMaintenanceDomain asnMaintenanceDomain,
        OutputStream outputStream) throws ApplicationException;
    
    /**
     * <p>Search legend info.</p>
     * <ul>
     * <li>Search download legend information from file id.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @param outputStream the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(AsnMaintenanceDomain asnMaintenanceDomain,
        OutputStream outputStream) throws ApplicationException;
    
    /**
     * <p>Delete temp upload asn.</p>
     * <ul>
     * <li>Delete temp upload asn from criteria.</li>
     * </ul>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @return the int
     * @throws Exception Exception
     */
    public int deleteTmpUploadAsn(AsnMaintenanceDomain asnMaintenanceDomain)throws Exception;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
    
    /**
     * Search Sending Email and return by throw ApplicationException.
     * <p>Send ASN data to Denso by email.</p>
     * @param asnMaintenanceDomain the asn maintenance domain
     * @param mode the string
     * @return boolean the result of sending email
     * @throws ApplicationException ApplicationException
     * */
    public boolean searchSendingEmail(AsnMaintenanceDomain asnMaintenanceDomain, String mode)
        throws ApplicationException;
    
    /**
     * Transact Allow Revise Qty
     * <p>Allow supplier user to revise shipping qty by DENSO user.</p>
     * @param asnMaintenanceDomain the asn maintenance domain
     * @throws ApplicationException ApplicationException
     * @return boolean, return true: if allow revise process and send mail success; false: if allow revise process success but send mail fail.
     * */
    public boolean transactAllowReviseQty(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException;

    /**
     * Generate ASN Report filename from SPS_T_COMPANY_DENSO.
     * @param dCd the company denso code
     * @return ASN Templete Filename
     * @throws ApplicationException an ApplicationException
     * @throws Exception an Exception
     * */
    public String getAsnTempleteFilename(String dCd) throws ApplicationException, Exception;

    /**
     * Generate ASN Report and update file ID to SPS_T_ASN.
     * @param locale locale to get report properties
     * @param asnNo an ASN_NO
     * @param dCd the company denso code
     * @param updateDateTime Last Update Datetime
     * @param dscId Last Update DSC_ID
     * @return FileManagementDomain ASN PDF
     * @throws ApplicationException an ApplicationException
     * @throws Exception an Exception
     * */
    public FileManagementDomain generateAndUpdatePdfFile(Locale locale, String asnNo, String dCd
        , Timestamp updateDateTime, String dscId) throws ApplicationException, Exception;
}