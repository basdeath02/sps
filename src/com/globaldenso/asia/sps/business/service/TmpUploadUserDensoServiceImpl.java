/*
 * ModifyDate Development company     Describe 
 * 2014/07/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.asia.sps.business.domain.TmpUserDensoDomain;
import com.globaldenso.asia.sps.business.dao.TmpUploadUserDensoDao;


/**
 * <p>The Class TempUserSupplierServiceImpl.</p>
 * <p>Manage data of Temporary User Supplier.</p>
 * <ul>
 * <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class TmpUploadUserDensoServiceImpl implements TmpUploadUserDensoService {

    

    /** The  Temporary User DENSO Dao. */
    private TmpUploadUserDensoDao tmpUploadUserDensoDao;
    
    /**
     * Instantiates a new Temporary User Supplier Service implement.
     */
    public TmpUploadUserDensoServiceImpl(){
        super();
    }
    
    /**
     * Sets the Temporary User DENSO Dao.
     * 
     * @param tmpUploadUserDensoDao the new Temporary User DENSO Dao.
     */
    public void setTmpUploadUserDensoDao(TmpUploadUserDensoDao tmpUploadUserDensoDao) {
        this.tmpUploadUserDensoDao = tmpUploadUserDensoDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadUserDensoService#searchTempUploadDensoOneRecord(com.globaldenso.asia.sps.business.domain.TmpUserDensoDomain)
     */
    public TmpUserDensoDomain searchTmpUploadDensoOneRecord(TmpUserDensoDomain tmpUserDensoDomain){
        TmpUserDensoDomain result = (TmpUserDensoDomain)tmpUploadUserDensoDao
            .searchTmpUploadDensoOneRecord(tmpUserDensoDomain);
        return result;
    }
    
}