/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Map;

import com.globaldenso.asia.sps.business.dao.CommonDao;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.utils.MessageUtil;



/**
 * <p>The Class CommonServiceImpl.</p>
 * <p>Manage data of common function.</p>
 * <ul>
 * <li>Method search  : searchSysDate</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class CommonServiceImpl implements CommonService{


    /** The common dao. */
    private CommonDao commonDao;
    
    /**
     * Instantiates a new Common service implement.
     */
    public CommonServiceImpl(){
        super();
    }
    
    /**
     * Sets the common dao.
     * 
     * @param commonDao the Common dao
     */
    public void setCommonDao(CommonDao commonDao) {
        this.commonDao = commonDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CommonService#searchSysDate()
     */
    public Timestamp searchSysDate(){
        Timestamp currentDateTime = commonDao.searchSysDate();
        return currentDateTime;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CommonService#searchSysDate(com.globaldenso.eps.business.domain.CommonDomain)
     */
    public Timestamp searchSysDate(CommonDomain commonDomain){
        Timestamp currentDateTime = commonDao.searchSysDate(commonDomain);
        return currentDateTime;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CommonService#createCSVString(com.globaldenso.eps.business.domain.CommonDomain)
     */
    public StringBuffer createCsvString(CommonDomain commonDomain)throws IOException
    {
        StringBuffer sb = new StringBuffer();
        Map<String, Object> result = null;
        /** set header */
        if(commonDomain.getHeaderFlag()){
            for(String h : commonDomain.getHeaderArr()){
                sb.append(MessageUtil.getReportLabel(commonDomain.getLocale(), h));
                sb.append(Constants.SYMBOL_COMMA);
            }
            if(Constants.ZERO < sb.length()){
                sb.setLength(sb.length() - Constants.ONE);
                sb.append(Constants.SYMBOL_CR_LF);
            }
        }
        /** set detail */
        for(int i = Constants.ZERO; i < commonDomain.getResultList().size(); i++){
            result = commonDomain.getResultList().get(i);
            for(String h : commonDomain.getHeaderArr()){
                sb.append(result.get(h));
                sb.append(Constants.SYMBOL_COMMA);
            }
            if(Constants.ZERO < sb.length()){
                sb.setLength(sb.length() - Constants.ONE);
                sb.append(Constants.SYMBOL_CR_LF);
            }
        }
        return sb;
    }
    
}