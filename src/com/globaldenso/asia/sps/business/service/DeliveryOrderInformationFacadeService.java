/*
 * ModifyDate Development company       Describe 
 * 2014/07/29 CSI Chatchai              Create
 * 2017/08/22 Netband U.Rungsiwut       Modify
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change D/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>
 * The Class DeliveryOrderInformationFacadeService.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public interface DeliveryOrderInformationFacadeService {
    /**
     * <p>
     * Delivery Order Information Domain.
     * </p>
     * 
     * @param DataScopeControlDomain the Delivery Order Information Domain
     * @return the String
     * @throws ApplicationException ApplicationException
     */

    public DeliveryOrderInformationDomain searchInitial(
        DataScopeControlDomain DataScopeControlDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Delivery Order Information method.
     * </p>
     * 
     * @param deliveryOrderInformationDomain the deliveryOrderInformationDomain
     * @return the list
     * @throws ApplicationException ApplicationException
     */
    public DeliveryOrderInformationResultDomain searchDeliveryOrderInformation(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Delivery Order Information Csv method.
     * </p>
     * 
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return the list
     * @throws ApplicationException ApplicationException
     */
    public DeliveryOrderInformationResultDomain searchDeliveryOrderInformationCsv(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Company Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScopeDomain PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScope PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Company Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search File Name method.
     * </p>
     * 
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return DeliveryOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Delivery Order Report method.
     * </p>
     * 
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @param output output
     * @return DeliveryOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchDeliveryOrderReport(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * <p>
     * Search Legend Info.
     * </p>
     * 
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @param output output
     * @return DeliveryOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchLegendInfo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;

    /**
     * <p>
     * Search update DateTime KanbanPdf method.
     * </p>
     * 
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return resultCode
     * @throws ApplicationException ApplicationException
     */
    public Integer updateDateTimeKanbanPdf(DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException;
    
    /**
     * Generate One Way Kanban Tag Report Information and return by throw ApplicationException.
     * @param deliveryOrderInformationDomain : DeliveryOrderInformationDomain
     * @return InputStream createOneWayKanbanTagReport
     * @throws ApplicationException contain application message
     * */
    public InputStream createOneWayKanbanTagReport(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Generate D/O PDF method.
     * </p>
     * 
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return DeliveryOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchGenerateDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        throws ApplicationException;
}
