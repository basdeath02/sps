/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 2015/08/04 CSI Akat                [IN009]
 * 2015/10/21 CSI Akat                [IN030]
 * 2015/10/27 CSI Akat                [IN036]
 * 2015/12/09 CSI Akat                [IN047]
 * 2018/11/30 CTC P.Pawan             [IN1777]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import static com.globaldenso.asia.sps.common.constant.Constants.REGX_DECIMAL_NUMBER_FORMAT_NEG;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPurchasePriceMasterDomain;
import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceResultDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The class InvoiceInformationUploadingFacadeServiceImpl.</p>
 * <p>Facade for InvoiceInformationUploadingFacadeServiceImpl.</p>
 * <ul>
 * <li>Method delete   : deleteTmpUploadInvoice</li>
 * <li>Method search   : searchByReturn</li>
 * <li>Method create   : transactInvoice</li>
 * <li>Method search   : searchTmpUploadInvoiceCsv</li>
 * <li>Method search   : searchDensoSupplierRelation</li>
 * <li>Method validate : searchValidateGroupInvoice</li>
 * <li>Method search   : downloadInvoiceInformation</li>
 * <li>Method search   : changeSelectDensoCompany</li>
 * </ul>
 *
 * @author CSI
 */
public class InvoiceInformationUploadingFacadeServiceImpl 
    implements InvoiceInformationUploadingFacadeService {
    
    /** Index of Uploading CSV, Column 'DCd'. */
    private static final int INDEX_DCD = 0;
    
    /** Index of Uploading CSV, Column 'DPcd'. */
    private static final int INDEX_DPCD = 1;
    
    /** Index of Uploading CSV, Column 'Vendor Cd'. */
    private static final int INDEX_VENDOR_CD = 2;
    
    /** Index of Uploading CSV, Column 'SPcd'. */
    private static final int INDEX_SPCD = 3;
    
    /** Index of Uploading CSV, Column 'Supplier Tax Id'. */
    private static final int INDEX_S_TAX_ID = 4;
    
    /** Index of Uploading CSV, Column 'Invoice No'. */
    private static final int INDEX_INVOICE_NO = 5;
    
    /** Index of Uploading CSV, Column 'Invoice Date'. */
    private static final int INDEX_INVOICE_DATE = 6;
    
    /** Index of Uploading CSV, Column 'Vat Type'. */
    private static final int INDEX_VAT_TYPE = 7;
    
    /** Index of Uploading CSV, Column 'Vat Rate'. */
    private static final int INDEX_VAT_RATE = 8;
    
    /** Index of Uploading CSV, Column 'Supplier Currency'. */
    private static final int INDEX_S_CURRENCY = 9;
    
    /** Index of Uploading CSV, Column 'Supplier Base Amount'. */
    private static final int INDEX_S_BASE_AMOUNT = 10;
    
    /** Index of Uploading CSV, Column 'Invoice Vat Amount'. */
    private static final int INDEX_INVOICE_VAT_AMOUNT = 11;
    
    /** Index of Uploading CSV, Column 'Total Amount'. */
    private static final int INDEX_TOTAL_AMOUNT = 12;
    
    /** Index of Uploading CSV, Column 'Credit Note No'. */
    private static final int INDEX_CN_NO = 13;
    
    /** Index of Uploading CSV, Column 'Create Note Date'. */
    private static final int INDEX_CN_DATE = 14;
    
    /** Index of Uploading CSV, Column 'Supplier CN Base Amount'. */
    private static final int INDEX_CN_BASE_AMOUNT = 15;
    
    /** Index of Uploading CSV, Column 'Supplier CN Vat Amount'. */
    private static final int INDEX_CN_VAT_AMOUNT = 16;
    
    /** Index of Uploading CSV, Column 'Supplier CN Total Amount'. */
    private static final int INDEX_CN_TOTAL_AMOUNT = 17;
    
    /** Index of Uploading CSV, Column 'Asn No'. */
    private static final int INDEX_ASN_NO = 18;
    
    /** Index of Uploading CSV, Column 'Trip No'. */
    private static final int INDEX_TRIP_NO = 19;

    // Start : [IN036] Insert D/O No. and Revision, all index add 2
    ///** Index of Uploading CSV, Column 'Plan Eta'. */
    //private static final int INDEX_PLAN_ETA = 20;
    ///** Index of Uploading CSV, Column 'Actual Etd'. */
    //private static final int INDEX_ACTUAL_ETD = 21;
    ///** Index of Uploading CSV, Column 'DPn'. */
    //private static final int INDEX_DPN = 22;
    ///** Index of Uploading CSV, Column 'SPn'. */
    //private static final int INDEX_SPN = 23;
    ///** Index of Uploading CSV, Column 'Shipping Qty'. */
    //private static final int INDEX_SHIPPING_QTY = 24;
    ///** Index of Uploading CSV, Column 'Unit Of Measure'. */
    //private static final int INDEX_S_UM = 25;
    ///** Index of Uploading CSV, Column 'Unit Of Measure'. */
    //private static final int INDEX_S_UNIT_PRICE = 26;
    
    /** Index of Uploading CSV, Column 'DO N0.'. */
    private static final int INDEX_DO_NO = 20;
    
    /** Index of Uploading CSV, Column 'Revision'. */
    private static final int INDEX_REVISION = 21;
    
    /** Index of Uploading CSV, Column 'Plan Eta'. */
    private static final int INDEX_PLAN_ETA = 22;
    
    /** Index of Uploading CSV, Column 'Actual Etd'. */
    private static final int INDEX_ACTUAL_ETD = 23;
    
    /** Index of Uploading CSV, Column 'DPn'. */
    private static final int INDEX_DPN = 24;
    
    /** Index of Uploading CSV, Column 'SPn'. */
    private static final int INDEX_SPN = 25;
    
    /** Index of Uploading CSV, Column 'Shipping Qty'. */
    private static final int INDEX_SHIPPING_QTY = 26;
    
    /** Index of Uploading CSV, Column 'Unit Of Measure'. */
    private static final int INDEX_S_UM = 27;
    
    /** Index of Uploading CSV, Column 'Unit Of Measure'. */
    private static final int INDEX_S_UNIT_PRICE = 28;
    // End : [IN036] Insert D/O No. and Revision, all index add 2
    
    /** The sps temp upload invoice service. */
    private SpsTmpUploadInvoiceService spsTmpUploadInvoiceService = null;
    
    /** The sps master company denso service. */
    private SpsMCompanyDensoService spsMCompanyDensoService = null;
    
    /** The sps master plant denso service. */
    private SpsMPlantDensoService spsMPlantDensoService = null;
    
    /** The sps transaction invoice service. */
    private SpsTInvoiceService spsTInvoiceService = null;
    
    /** The sps master as400 vendor service. */
    private SpsMAs400VendorService spsMAs400VendorService = null;
    
    /** The cn service. */
    private CnService cnService = null;
    
    /** The asn service. */
    private AsnService asnService = null;
    
    /** The common service. */
    private CommonService commonService = null;
    
    /** The misc service. */
    private MiscellaneousService miscService = null;
    
    /** The Denso Supplier Relation service. */
    private DensoSupplierRelationService densoSupplierRelationService = null; 
    
    /** The Supplier company service. */
    private CompanySupplierService companySupplierService = null;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public InvoiceInformationUploadingFacadeServiceImpl(){
        super();
    }

    /**
     * Set the sps temp upload invoice service.
     * 
     * @param spsTmpUploadInvoiceService the sps temp upload invoice service to set
     */
    public void setSpsTmpUploadInvoiceService(
        SpsTmpUploadInvoiceService spsTmpUploadInvoiceService){
        this.spsTmpUploadInvoiceService = spsTmpUploadInvoiceService;
    }
    
    /**
     * Set the sps master company denso service.
     * 
     * @param spsMCompanyDensoService the sps master company denso service to set
     */
    public void setSpsMCompanyDensoService(SpsMCompanyDensoService spsMCompanyDensoService){
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * Set the sps master plant denso service.
     * 
     * @param spsMPlantDensoService the sps master plant denso service to set
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService){
        this.spsMPlantDensoService = spsMPlantDensoService;
    }
    
    /**
     * Sets the sps transaction invoice service.
     * 
     * @param spsTInvoiceService the sps transaction invoice service.
     */ 
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService) {
        this.spsTInvoiceService = spsTInvoiceService;
    }
    
    /**
     * Sets the sps master as400 vendor service.
     * 
     * @param spsMAs400VendorService the sps master as400 vendor service.
     */ 
    public void setSpsMAs400VendorService(SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }
    
    /**
     * Sets the cn service.
     * 
     * @param cnService the cn service.
     */ 
    public void setCnService(CnService cnService) {
        this.cnService = cnService;
    }
    
    /**
     * Sets the asn service.
     * 
     * @param asnService the asn service.
     */ 
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService#deleteTmpUploadInvoice(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain)
     */
    public void deleteTmpUploadInvoice(InvoiceInformationUploadingDomain 
        invoiceInformationUploadingDomain) throws ApplicationException{
        
        SpsTmpUploadInvoiceCriteriaDomain criteria = new SpsTmpUploadInvoiceCriteriaDomain();
        if(null != invoiceInformationUploadingDomain){
            criteria.setSessionId(invoiceInformationUploadingDomain.getSessionId());
            criteria.setUploadDscId(invoiceInformationUploadingDomain.getDscId());
        }
        else{
            CommonDomain commonDomain = new CommonDomain();
            commonDomain.setOperation(Constants.STR_MINUS_TWO);
            criteria.setUploadDatetimeLessThanEqual(this.commonService.searchSysDate(commonDomain));
        }
        this.spsTmpUploadInvoiceService.deleteByCondition(criteria);
    } 
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService#searchByReturn(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain)
     */
    public InvoiceInformationUploadingDomain searchByReturn(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain) 
        throws ApplicationException {
        
        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain = 
            new InvoiceInformationUploadingDomain();

        this.searchTmpUploadInvoice(invoiceInformationUploadingDomain, 
            invoiceInformationUploadingReturnDomain);
        return invoiceInformationUploadingReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService#transactInvoice(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain)
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_UNCHECKED, Constants.SUPPRESS_WARNINGS_RAWTYPES})
    public InvoiceInformationUploadingDomain transactInvoice(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain, 
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException
    {
        Locale locale = invoiceInformationUploadingDomain.getLocale();
        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain
            = new InvoiceInformationUploadingDomain();
        
        Iterator itr = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader br = null;
        String line = null;
        boolean supplierCodeAreSame = true;
        boolean densoCodeAreSame = true;
        boolean densoPlantCodeAreSame = true;
        boolean supplierTaxIdAreSame = true;
        boolean invoiceNoAreSame = true;
        boolean invoiceDateAreSame = true;
        List<String> splitItem = null;
        List<ApplicationMessageDomain> errorMsgList = new ArrayList<ApplicationMessageDomain>();
        List<List<String>> invoiceInfoUploadingList = new ArrayList<List<String>>();
        Map<String, BigDecimal> sumShippingQtyMap = new HashMap<String, BigDecimal>();
        
        try{
//            invoiceInformationUploadingReturnDomain
//                = this.searchUserAuthority(dataScopeControlDomain);
            
            inputStream = invoiceInformationUploadingDomain.getFileManagementDomain().getFileData();
            inputStream = FileUtil.checkUtf8BOMAndSkip(inputStream);
            inputStreamReader = new InputStreamReader(inputStream);
            br = new BufferedReader(inputStreamReader);
            
            while((line = br.readLine()) != null){
                // [IN036] add more 2 column
                //splitItem = StringUtil.splitCsvData(line, Constants.TWENTY_SEVEN);
                splitItem = StringUtil.splitCsvData(line, Constants.TWENTY_NINE);
                this.convertValueToUpperCase(splitItem);
                invoiceInfoUploadingList.add(splitItem);
            }
        }catch(IOException oe){
            MessageUtil.throwsApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0010);
        }finally{
            try{
                if(null != inputStream){
                    inputStream.close();
                }
                if(null != inputStreamReader){
                    inputStreamReader.close();
                }
                if(null != br){
                    br.close();
                }
            }catch(IOException ioe){
                MessageUtil.throwsApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_80_0010);
            }
        }
        
        if(Constants.ZERO < invoiceInfoUploadingList.size()){
            String supplierCode = null;
            String densoCode = null;
            String densoPlantCode = null;
            String supplierTaxId = null;
            String invoiceNo = null;
            String invoiceDate = null;
            
            itr = invoiceInfoUploadingList.iterator();
            
            while(itr.hasNext() && (supplierCodeAreSame || densoCodeAreSame || densoPlantCodeAreSame
                || supplierTaxIdAreSame || invoiceNoAreSame || invoiceDateAreSame))
            {
                //Validate for Error
                splitItem = (List<String>)itr.next();
                if(StringUtil.checkNullOrEmpty(supplierCode)){
                    supplierCode = splitItem.get(INDEX_VENDOR_CD);
                }else{
                    if(!supplierCode.equals(splitItem.get(INDEX_VENDOR_CD)) && supplierCodeAreSame){
                        supplierCodeAreSame = false;
                    }
                }
                
                if(StringUtil.checkNullOrEmpty(densoCode)){
                    densoCode = splitItem.get(INDEX_DCD);
                }else{
                    if(!densoCode.equals(splitItem.get(INDEX_DCD)) && densoCodeAreSame){
                        densoCodeAreSame = false;
                    }
                }
                
                if(StringUtil.checkNullOrEmpty(densoPlantCode)){
                    densoPlantCode = splitItem.get(INDEX_DPCD);
                }else{
                    if(!densoPlantCode.equals(splitItem.get(INDEX_DPCD)) && densoPlantCodeAreSame){
                        densoPlantCodeAreSame = false;
                    }
                }
                
                if(StringUtil.checkNullOrEmpty(supplierTaxId)){
                    supplierTaxId = splitItem.get(INDEX_S_TAX_ID);
                }else{
                    if(!supplierTaxId.equals(splitItem.get(INDEX_S_TAX_ID))
                        && supplierTaxIdAreSame){
                        supplierTaxIdAreSame = false;
                    }
                }
                
                if(StringUtil.checkNullOrEmpty(invoiceNo)){
                    invoiceNo = splitItem.get(INDEX_INVOICE_NO);
                }else{
                    if(!invoiceNo.equals(splitItem.get(INDEX_INVOICE_NO)) && invoiceNoAreSame){
                        invoiceNoAreSame = false;
                    }
                }
                
                if(StringUtil.checkNullOrEmpty(invoiceDate)){
                    invoiceDate = splitItem.get(INDEX_INVOICE_DATE);
                }else{
                    if(!invoiceDate.equals(splitItem.get(INDEX_INVOICE_DATE))
                        && invoiceDateAreSame){
                        invoiceDateAreSame = false;
                    }
                }
                
                if(sumShippingQtyMap.containsKey(splitItem.get(INDEX_ASN_NO))){
                    BigDecimal sumShippingQty 
                        = sumShippingQtyMap.get(splitItem.get(INDEX_ASN_NO)).add(
                            NumberUtil.toBigDecimalDefaultZero(splitItem.get(INDEX_SHIPPING_QTY)));
                    sumShippingQtyMap.put(splitItem.get(INDEX_ASN_NO), sumShippingQty);
                }else{
                    sumShippingQtyMap.put(splitItem.get(INDEX_ASN_NO),
                        NumberUtil.toBigDecimalDefaultZero(splitItem.get(INDEX_SHIPPING_QTY)));
                }
            }
        }
        
        splitItem = invoiceInfoUploadingList.get(Constants.ZERO);
        if(!supplierCodeAreSame){
            errorMsgList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                    SupplierPortalConstant.LBL_S_CD_NOT_SAME)));
        }
        
        if(!densoCodeAreSame){
            errorMsgList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                    SupplierPortalConstant.LBL_D_CD_NOT_SAME)));
        }else{
            SpsMCompanyDensoCriteriaDomain companyDensoCriteria
                = new SpsMCompanyDensoCriteriaDomain();
            companyDensoCriteria.setDCd(StringUtil.rightPad(
                splitItem.get(INDEX_DCD), SupplierPortalConstant.LENGTH_DENSO_CODE));
            SpsMCompanyDensoDomain companyDenso
                = spsMCompanyDensoService.searchByKey(companyDensoCriteria);
            if(null == companyDenso
                || StringUtil.checkNullOrEmpty(companyDenso.getGroupInvoiceDiffPlantDenso())){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
            }else{
                if(SupplierPortalConstant.GROUP_INVOICE_NOT_ALLOW_DIFF_DPCD.equals(
                    companyDenso.getGroupInvoiceDiffPlantDenso())){
                    if(!densoPlantCodeAreSame){
                        errorMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                                SupplierPortalConstant.LBL_D_PCD_NOT_SAME)));
                    }
                }
            }
        }
        
        if(!supplierTaxIdAreSame){
            errorMsgList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                    SupplierPortalConstant.LBL_S_TAX_ID_NOT_SAME)));
        }
        
        if(!invoiceNoAreSame){
            errorMsgList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                    SupplierPortalConstant.LBL_INVOICE_NO_NOT_SAME)));
        }
        
        if(!invoiceDateAreSame){
            errorMsgList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                    SupplierPortalConstant.LBL_INVOICE_DATE_NOT_SAME)));
        }
        
        if(Constants.ZERO < sumShippingQtyMap.size()){
            boolean sumShippingQtyAreEqual = true;
            SpsTAsnDetailDomain criteria = new SpsTAsnDetailDomain();
            criteria.setAsnNo(
                StringUtil.convertListToVarcharCommaSeperate(sumShippingQtyMap.keySet()));
            List<SpsTAsnDetailDomain> spsTAsnDetailList
                = this.asnService.searchSumShippingQty(criteria);
            for(SpsTAsnDetailDomain asnDetailItem : spsTAsnDetailList){
                String asnNo = asnDetailItem.getAsnNo();
                if(Constants.ZERO != sumShippingQtyMap.get(asnNo)
                    .compareTo(asnDetailItem.getShippingQty()))
                {
                    sumShippingQtyAreEqual = false;
                    break;
                }
            }
            if(!sumShippingQtyAreEqual){
                errorMsgList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0008,
                        SupplierPortalConstant.LBL_SHIPPING_QTY_NOT_EQUAL)));
            }
        }
        
        if(Constants.ZERO < errorMsgList.size()){
            invoiceInformationUploadingDomain.setErrorMessageList(errorMsgList);
            return null;
        }
        
        //Validate file content in the detail level
        boolean isValidDCd = false;
        boolean isValidVendorCd = false;
        boolean isValidSPcd = false;
        boolean isValidAsn = false;
        int csvLineNo = Constants.ONE;
        List<String> errorCodeList = null;
        Map<String, BigDecimal> sUnitPriceMap = new HashMap<String, BigDecimal>();
        itr = invoiceInfoUploadingList.iterator();
        
        List<SpsMPlantSupplierDomain> supplierAuthenList
            = invoiceInformationUploadingDomain.getSupplierAuthenList();
        List<SpsMPlantDensoDomain> densoAuthenList
            = invoiceInformationUploadingDomain.getDensoAuthenList();
        
        String asnStatusStr = StringUtil.convertListToVarcharCommaSeperate(
            Constants.ASN_STATUS_ISS, Constants.ASN_STATUS_RCP, Constants.ASN_STATUS_PTR);
        
        while(itr.hasNext()){
            splitItem = (List<String>)itr.next();
            errorCodeList = new ArrayList<String>();
            
            isValidDCd = false;
            isValidVendorCd = false;
            isValidSPcd = false;
            isValidAsn = false;
            
            SpsTmpUploadInvoiceDomain spsTmpUploadInvoiceDomain = null;
            String utc = null;
            
            boolean isValidDCdAuthen = false;
            boolean isValidSCdAuthen = false;
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DCD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F01);
            }else{
                if(SupplierPortalConstant.LENGTH_DENSO_CODE < splitItem.get(INDEX_DCD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F01);
                }else{
                    splitItem.set(INDEX_DCD, StringUtil.rightPad(splitItem.get(INDEX_DCD),
                        SupplierPortalConstant.LENGTH_DENSO_CODE));
                    
                    SpsMCompanyDensoDomain criteria = new SpsMCompanyDensoDomain();
                    criteria.setDCd(StringUtil.convertListToVarcharCommaSeperate(
                        splitItem.get(INDEX_DCD)));
                    criteria.setIsActive(Constants.STR_ONE);
                    List<SpsMCompanyDensoDomain> result 
                        = this.companyDensoService.searchCompanyDensoByCodeList(criteria);
                    if(null == result || Constants.ZERO == result.size()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
                    }else{
                        utc = result.get(Constants.ZERO).getUtc();
                        isValidDCd = true;
                    }
                    
                    
                    //*** Check user have authority in this DENSO code
                    for(SpsMPlantDensoDomain densoAuthen : densoAuthenList){
                        if(densoAuthen.getDCd().equals(splitItem.get(INDEX_DCD))){
                            isValidDCdAuthen = true;
                            break;
                        }
                    }
                    if(!isValidDCdAuthen){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A01);
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DPCD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F02);
            }else{
                if(SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE
                    < splitItem.get(INDEX_DPCD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F02);
                }else{
                    splitItem.set(INDEX_DPCD, StringUtil.rightPad(splitItem.get(INDEX_DPCD),
                        SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE));
                    
                    if(isValidDCd){
                        SpsMPlantDensoCriteriaDomain criteria = new SpsMPlantDensoCriteriaDomain();
                        criteria.setDCd(splitItem.get(INDEX_DCD));
                        criteria.setDPcd(splitItem.get(INDEX_DPCD));
                        criteria.setIsActive(Constants.STR_ONE);
                        int recordCount = this.spsMPlantDensoService.searchCount(criteria);
                        if(Constants.ZERO == recordCount){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                        }
                        
                        //*** Check user have authority in this DENSO plant code
                        if(isValidDCdAuthen){
                            boolean isValidDPcdAuthen = false;
                            for(SpsMPlantDensoDomain densoAuthen : densoAuthenList){
                                if(densoAuthen.getDCd().equals(splitItem.get(INDEX_DCD))
                                    && densoAuthen.getDPcd().equals(splitItem.get(INDEX_DPCD))){
                                    isValidDPcdAuthen = true;
                                    break;
                                }
                            }
                            if(!isValidDPcdAuthen){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A02);
                            }
                        }
                    }
                }
            }
            
            String sCd = Constants.EMPTY_STRING;
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_VENDOR_CD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F03);
            }else{
                if(SupplierPortalConstant.LENGTH_VENDOR_CD
                    < splitItem.get(INDEX_VENDOR_CD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F03);
                }else{
                    if(isValidDCd){
                        SpsMAs400VendorCriteriaDomain criteria 
                            = new SpsMAs400VendorCriteriaDomain();
                        criteria.setDCd(splitItem.get(INDEX_DCD));
                        criteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        criteria.setIsActive(Constants.STR_ONE);
                        List<SpsMAs400VendorDomain> vendorList
                            = this.spsMAs400VendorService.searchByCondition(criteria);
                        if(null == vendorList || vendorList.isEmpty()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M03);
                        }else{
                            isValidVendorCd = true;
                            sCd = vendorList.get(Constants.ZERO).getSCd();
                        }
                        
                        //*** Check user have authority in this company supplier code
                        for(SpsMPlantSupplierDomain supplierAuthen : supplierAuthenList){
                            if(supplierAuthen.getSCd().equals(sCd)){
                                isValidSCdAuthen = true;
                                break;
                            }
                        }
                        if(!isValidSCdAuthen){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A03);
                        }
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SPCD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F04);
            }else{
                if(SupplierPortalConstant.LENGTH_SUPPLIER_PLANT_CODE
                    < splitItem.get(INDEX_SPCD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F04);
                }else{
                    if(isValidVendorCd){
                        CompanySupplierDomain criteria = new CompanySupplierDomain();
                        criteria.setDCd(splitItem.get(INDEX_DCD));
                        criteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        criteria.setSPcd(splitItem.get(INDEX_SPCD));
                        criteria.setIsActive(Constants.STR_ONE);
                        int recordCount = this.companySupplierService
                            .searchExistCompanySupplierInformation(criteria);
                        if(Constants.ZERO == recordCount){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M04);
                        }else{
                            isValidSPcd = true;
                        }
                        
                        //*** Check user have authority in this supplier plant code
                        if(isValidSCdAuthen){
                            boolean isValidSPcdAuthen = false;
                            for(SpsMPlantSupplierDomain supplierAuthen : supplierAuthenList){
                                if(supplierAuthen.getSCd().equals(sCd)
                                    && supplierAuthen.getSPcd().equals(splitItem.get(INDEX_SPCD))){
                                    isValidSPcdAuthen = true;
                                    break;
                                }
                            }
                            if(!isValidSPcdAuthen){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A04);
                            }
                        }
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_S_TAX_ID))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
            }else{
                if(Constants.TWENTY < splitItem.get(INDEX_S_TAX_ID).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
                }else{
                    if(isValidSPcd){
                        CompanySupplierDomain criteria = new CompanySupplierDomain();
                        criteria.setDCd(splitItem.get(INDEX_DCD));
                        criteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        criteria.setSPcd(splitItem.get(INDEX_SPCD));
                        criteria.setSTaxId(splitItem.get(INDEX_S_TAX_ID));
                        criteria.setIsActive(Constants.STR_ONE);
                        int recordCount = this.companySupplierService
                            .searchExistCompanySupplierInformation(criteria);
                        
                        if(Constants.ZERO == recordCount){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M05);
                        }
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_INVOICE_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
            }else{
                if(Constants.TWENTY_FIVE < splitItem.get(INDEX_INVOICE_NO).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
                }else{
                    if(isValidVendorCd){
                        SpsTInvoiceCriteriaDomain criteria = new SpsTInvoiceCriteriaDomain();
                        criteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        criteria.setInvoiceNo(splitItem.get(INDEX_INVOICE_NO));
                        List<SpsTInvoiceDomain> result
                            = this.spsTInvoiceService.searchByCondition(criteria);
                        Iterator<SpsTInvoiceDomain> iterator = result.iterator();
                        while(iterator.hasNext()){
                            SpsTInvoiceDomain item = iterator.next();
                            if(Constants.INVOICE_STATUS_DCL.equals(item.getInvoiceStatus())){
                                iterator.remove();
                            }
                        }
                        if(Constants.ZERO < result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_D06);
                        }
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_INVOICE_DATE))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
            }else{
                if(!DateUtil.isValidDate(splitItem.get(INDEX_INVOICE_DATE),
                    DateUtil.PATTERN_YYYYMMDD_SLASH)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
                }
            }
            
            // TODO
            // [IN059] check all VAT amount with VAT Rate
            String vatType = null;
            String vatRate = null;
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_VAT_TYPE))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
            }else{
                if(!Constants.STR_ZERO.equals(splitItem.get(INDEX_VAT_TYPE))
                    && !Constants.STR_ONE.equals(splitItem.get(INDEX_VAT_TYPE))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
                }

                // Start : [IN059] check all VAT amount with VAT Rate
                else {
                    vatType = splitItem.get(INDEX_VAT_TYPE);
                    if (Constants.STR_ZERO.equals(vatType)) {
                        vatRate = Constants.STR_ZERO;
                    }
                }
                // End : [IN059] check all VAT amount with VAT Rate
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_VAT_RATE))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
            }else{
                if(!splitItem.get(INDEX_VAT_RATE).matches(Constants.REGX_NUMBER_FORMAT)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
                }else{
                    if(Constants.THREE < splitItem.get(INDEX_VAT_RATE).length()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
                    }

                    // [IN059] check all VAT amount with VAT Rate
                    else {
                        if (Constants.STR_ONE.equals(vatType)) {
                            vatRate = splitItem.get(INDEX_VAT_RATE);
                        }
                    }
                    // End : [IN059] check all VAT amount with VAT Rate
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_S_CURRENCY))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
            }else{
                if(Constants.TWO < splitItem.get(INDEX_S_CURRENCY).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
                }else{
                    if(isValidVendorCd){
                        CompanySupplierDomain criteria = new CompanySupplierDomain();
                        criteria.setDCd(splitItem.get(INDEX_DCD));
                        criteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        criteria.setCurrencyCd(splitItem.get(INDEX_S_CURRENCY));
                        criteria.setIsActive(Constants.STR_ONE);
                        int recordCount = this.companySupplierService
                            .searchExistCompanySupplierInformation(criteria);
                        if(Constants.ZERO == recordCount){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M10);
                        }
                    }
                }
            }
            
            boolean isNumberOfBaseAmount = false;
            boolean isNumberOfVatAmount = false;
            boolean isNumberOfTotalAmount = false;
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_S_BASE_AMOUNT))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
            }else{
                // [FIX] not allow input negative value
                //if(!StringUtil.isNumeric(splitItem.get(INDEX_S_BASE_AMOUNT))){
                if(!StringUtil.isPositiveNumeric(splitItem.get(INDEX_S_BASE_AMOUNT))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
                }else{
                    if(splitItem.get(INDEX_S_BASE_AMOUNT).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(splitItem.get(INDEX_S_BASE_AMOUNT),
                            Constants.FIFTEEN, Constants.FOUR)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
                        }else{
                            isNumberOfBaseAmount = true;
                        }
                    }else{
                        if(Constants.ELEVEN < splitItem.get(INDEX_S_BASE_AMOUNT).length()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
                        }else{
                            isNumberOfBaseAmount = true;
                        }
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_INVOICE_VAT_AMOUNT))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
            }else{
                // [FIX] not allow input negative value
                //if(!StringUtil.isNumeric(splitItem.get(INDEX_INVOICE_VAT_AMOUNT))){
                if(!StringUtil.isPositiveNumeric(splitItem.get(INDEX_INVOICE_VAT_AMOUNT))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
                }else{
                    if(splitItem.get(INDEX_INVOICE_VAT_AMOUNT).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(splitItem.get(INDEX_INVOICE_VAT_AMOUNT),
                            Constants.FIFTEEN, Constants.FOUR)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
                        }else{
                            isNumberOfVatAmount = true;
                        }
                    }else{
                        if(Constants.ELEVEN < splitItem.get(INDEX_INVOICE_VAT_AMOUNT).length()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
                        }else{
                            isNumberOfVatAmount = true;
                        }
                    }
                }
            }
            
            // TODO
            // Start : [IN059] check Invoice VAT amount with VAT rate
            BigDecimal vatRateBd = null;
            if (null != vatRate) {
                vatRateBd = Constants.ZERO_POINT_ZERO_ONE.multiply(new BigDecimal(vatRate));
            }
            if(isNumberOfBaseAmount && isNumberOfVatAmount && null != vatRateBd){
                BigDecimal calculatedInvVat = vatRateBd.multiply(
                    new BigDecimal(splitItem.get(INDEX_S_BASE_AMOUNT)));
                calculatedInvVat = calculatedInvVat.setScale(Constants.TWO, RoundingMode.HALF_UP);
                BigDecimal inputInvVat = new BigDecimal(splitItem.get(INDEX_INVOICE_VAT_AMOUNT));
                if (Constants.ZERO != calculatedInvVat.compareTo(inputInvVat)) {
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
                }
            }
            // End : [IN059] check Invoice VAT amount with VAT rate
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_TOTAL_AMOUNT))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F13);
            }else{
                // [FIX] not allow input negative value
                //if(!StringUtil.isNumeric(splitItem.get(INDEX_TOTAL_AMOUNT))){
                if(!StringUtil.isPositiveNumeric(splitItem.get(INDEX_TOTAL_AMOUNT))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F13);
                }else{
                    if(splitItem.get(INDEX_TOTAL_AMOUNT).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(splitItem.get(INDEX_TOTAL_AMOUNT),
                            Constants.FIFTEEN, Constants.FOUR)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F13);
                        }else{
                            isNumberOfTotalAmount = true;
                        }
                    }else{
                        if(Constants.ELEVEN < splitItem.get(INDEX_TOTAL_AMOUNT).length()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F13);
                        }else{
                            isNumberOfTotalAmount = true;
                        }
                    }
                }
            }
            
            if(isNumberOfBaseAmount && isNumberOfVatAmount && isNumberOfTotalAmount
                && !(Constants.ZERO == new BigDecimal(splitItem.get(INDEX_TOTAL_AMOUNT))
                    .compareTo(new BigDecimal(splitItem.get(INDEX_S_BASE_AMOUNT))
                        .add(new BigDecimal(splitItem.get(INDEX_INVOICE_VAT_AMOUNT)))))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T13);
            }
            
            if(Constants.TWENTY_FIVE < splitItem.get(INDEX_CN_NO).length()){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F14);
            }else{
                if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))){
                    if(isValidVendorCd){
                        PriceDifferenceInformationDomain criteria
                            = new PriceDifferenceInformationDomain();
                        criteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        criteria.setCnNo(splitItem.get(INDEX_CN_NO));
                        List<PriceDifferenceInformationDomain> result
                            = this.cnService.searchCn(criteria);
                        Iterator<PriceDifferenceInformationDomain> iterator = result.iterator();
                        while(iterator.hasNext()) {
                            PriceDifferenceInformationDomain item = iterator.next();
                            if(Constants.CCL_STATUS.equals(item.getSpsTCnDomain().getCnStatus())){
                                iterator.remove();
                            }
                        }
                        if(Constants.ZERO < result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_D14);
                        }
                    }
                }
            }
            
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_DATE))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F15);
            }
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && (!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_DATE))
                && !DateUtil.isValidDate(splitItem.get(INDEX_CN_DATE),
                    DateUtil.PATTERN_YYYYMMDD_SLASH))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F15);
            }
            
            boolean isNumberOfCnBaseAmount = false;
            boolean isNumberOfCnVatAmount = false;
            boolean isNumberOfCnTotalAmount = false;
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_BASE_AMOUNT))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
            }
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && (!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_BASE_AMOUNT)))){
                if(!splitItem.get(INDEX_CN_BASE_AMOUNT).matches(REGX_DECIMAL_NUMBER_FORMAT_NEG)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
                }else{
                    if(Constants.BIG_DECIMAL_ZERO.compareTo(
                        new BigDecimal(splitItem.get(INDEX_CN_BASE_AMOUNT))) <= Constants.ZERO){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
                    }else{
                        String strNumberWithoutSymbol = splitItem.get(INDEX_CN_BASE_AMOUNT)
                            .replace(Constants.SYMBOL_SUBTRACT, Constants.SYMBOL_SPACE);
                        if(splitItem.get(INDEX_CN_BASE_AMOUNT).contains(Constants.SYMBOL_DOT)){
                            if(!StringUtil.isNumberFormat(strNumberWithoutSymbol,
                                Constants.FIFTEEN, Constants.FOUR)){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
                            }else{
                                isNumberOfCnBaseAmount = true;
                            }
                        }else{
                            if(Constants.ELEVEN < strNumberWithoutSymbol.length()){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
                            }else{
                                isNumberOfCnBaseAmount = true;
                            }
                        }
                    }
                }
            }
            
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_VAT_AMOUNT))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
            }
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && (!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_VAT_AMOUNT)))){
                
                // [IN059] CN VAT amount can be zero
                //if(!splitItem.get(INDEX_CN_VAT_AMOUNT).matches(REGX_DECIMAL_NUMBER_FORMAT_NEG)){
                if(!splitItem.get(INDEX_CN_VAT_AMOUNT).matches(REGX_DECIMAL_NUMBER_FORMAT_NEG)
                    && !Constants.STR_ZERO_TWO_DIGIT.equals(splitItem.get(INDEX_CN_VAT_AMOUNT)))
                {
                    
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
                }else{

                    // [IN059] CN VAT amount can be zero
                    //if(Constants.BIG_DECIMAL_ZERO.compareTo(
                    //    new BigDecimal(splitItem.get(INDEX_CN_VAT_AMOUNT))) <= Constants.ZERO){
                    if(Constants.BIG_DECIMAL_ZERO.compareTo(
                        new BigDecimal(splitItem.get(INDEX_CN_VAT_AMOUNT))) < Constants.ZERO){
                        
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
                    }else{
                        String strNumberWithoutSymbol = splitItem.get(INDEX_CN_VAT_AMOUNT)
                            .replace(Constants.SYMBOL_SUBTRACT, Constants.SYMBOL_SPACE);
                        if(splitItem.get(INDEX_CN_VAT_AMOUNT).contains(Constants.SYMBOL_DOT)){
                            if(!StringUtil.isNumberFormat(strNumberWithoutSymbol,
                                Constants.FIFTEEN, Constants.FOUR)){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
                            }else{
                                isNumberOfCnVatAmount = true;
                            }
                        }else{
                            if(Constants.ELEVEN < strNumberWithoutSymbol.length()){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
                            }else{
                                isNumberOfCnVatAmount = true;
                            }
                        }
                    }
                }
            }
            
            // TODO
            // Start : [IN059] Validate CN VAT with VAT Rate
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && isNumberOfCnBaseAmount && isNumberOfCnVatAmount && null != vatRateBd)
            {
                BigDecimal calculatedCnVat = vatRateBd.multiply(
                    new BigDecimal(splitItem.get(INDEX_CN_BASE_AMOUNT)));
                calculatedCnVat = calculatedCnVat.setScale(Constants.TWO, RoundingMode.HALF_UP);
                BigDecimal inputCnVat = new BigDecimal(splitItem.get(INDEX_CN_VAT_AMOUNT));
                if (Constants.ZERO != calculatedCnVat.compareTo(inputCnVat)) {
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T17);
                }
            }
            // End : [IN059] Validate CN VAT with VAT Rate
            
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_TOTAL_AMOUNT))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
            }
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO))
                && (!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_TOTAL_AMOUNT)))){
                if(!splitItem.get(INDEX_CN_TOTAL_AMOUNT).matches(REGX_DECIMAL_NUMBER_FORMAT_NEG)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
                }else{
                    if(Constants.BIG_DECIMAL_ZERO.compareTo(
                        new BigDecimal(splitItem.get(INDEX_CN_TOTAL_AMOUNT))) <= Constants.ZERO){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
                    }else{
                        String strNumberWithoutSymbol = splitItem.get(INDEX_CN_TOTAL_AMOUNT)
                            .replace(Constants.SYMBOL_SUBTRACT, Constants.SYMBOL_SPACE);
                        if(splitItem.get(INDEX_CN_TOTAL_AMOUNT).contains(Constants.SYMBOL_DOT)){
                            if(!StringUtil.isNumberFormat(strNumberWithoutSymbol,
                                Constants.FIFTEEN, Constants.FOUR)){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
                            }else{
                                isNumberOfCnTotalAmount = true;
                            }
                        }else{
                            if(Constants.ELEVEN < strNumberWithoutSymbol.length()){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
                            }else{
                                isNumberOfCnTotalAmount = true;
                            }
                        }
                    }
                }
            }
            
            if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_NO)) 
                && isNumberOfCnBaseAmount && isNumberOfCnVatAmount && isNumberOfCnTotalAmount
                && !(Constants.ZERO == new BigDecimal(splitItem.get(INDEX_CN_TOTAL_AMOUNT))
                    .compareTo(new BigDecimal(splitItem.get(INDEX_CN_BASE_AMOUNT))
                        .add(new BigDecimal(splitItem.get(INDEX_CN_VAT_AMOUNT)))))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T18);
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_ASN_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
            }else{
                if(splitItem.get(INDEX_ASN_NO).length() < Constants.ELEVEN
                    || Constants.SIXTEEN < splitItem.get(INDEX_ASN_NO).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
                }else{
                    AsnDomain criteria = new AsnDomain();
                    criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                    criteria.setAsnStatus(asnStatusStr);
                    criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);
                    
                    criteria.setSupplierAuthenList(
                        invoiceInformationUploadingDomain.getSupplierAuthenList());
                    criteria.setDensoAuthenList(
                        invoiceInformationUploadingDomain.getDensoAuthenList());
                    
                    List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                    if(Constants.ZERO == result.size()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T19);
                    }else{
                        isValidAsn = true;
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_TRIP_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
            }else{
                if(!StringUtil.isPositiveInteger(splitItem.get(INDEX_TRIP_NO))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
                }else{
                    if(SupplierPortalConstant.LENGTH_TRIP_NO
                        < splitItem.get(INDEX_TRIP_NO).length()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
                    }else{
                        splitItem.set(INDEX_TRIP_NO, StringUtil.rightPad(
                            splitItem.get(INDEX_TRIP_NO), SupplierPortalConstant.LENGTH_TRIP_NO));
                        
                        if(isValidAsn){
                            AsnDomain criteria = new AsnDomain();
                            criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                            criteria.setTripNo(splitItem.get(INDEX_TRIP_NO));
                            criteria.setAsnStatus(asnStatusStr);
                            criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                            criteria.setSupplierAuthenList(
                                invoiceInformationUploadingDomain.getSupplierAuthenList());
                            criteria.setDensoAuthenList(
                                invoiceInformationUploadingDomain.getDensoAuthenList());
                            
                            List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                            if(Constants.ZERO == result.size()){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T20);
                            }
                        }
                    }
                }
            }
            
            // Start : [IN036] Insert D/O No. and Revision Column number add 2.
            boolean isValidDoNo = false;
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DO_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F21);
            }else{
                if(splitItem.get(INDEX_DO_NO).length() < Constants.NINE
                    || Constants.ELEVEN < splitItem.get(INDEX_DO_NO).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F21);
                }else{
                    if(isValidAsn){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T21);
                        }else{
                            isValidDoNo = true;
                        }
                    }
                }
            }
            
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_REVISION))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F22);
            }else{
                if(Constants.TWO != splitItem.get(INDEX_REVISION).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F22);
                }else{
                    if(isValidDoNo){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                        criteria.setRevision(splitItem.get(INDEX_REVISION));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T22);
                        }
                    }
                }
            }
            
            boolean isValidPlanEta = false;
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_PLAN_ETA))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F23);
            }else{
                if(!DateUtil.isValidDate(splitItem.get(INDEX_PLAN_ETA), 
                    DateUtil.PATTERN_YYYYMMDD_SLASH)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F23);
                }else{
                    if(isValidAsn){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setPlanEta(splitItem.get(INDEX_PLAN_ETA));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);
                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T23);
                        }else{
                            isValidPlanEta = true;
                        }
                    }
                }
            }

            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_ACTUAL_ETD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F24);
            }else{
                if(!DateUtil.isValidDate(splitItem.get(INDEX_ACTUAL_ETD),
                    DateUtil.PATTERN_YYYYMMDD_SLASH)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F24);
                }else{
                    if(isValidAsn){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setActualEtd(splitItem.get(INDEX_ACTUAL_ETD));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);
                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T24);
                        }
                    }
                }
            }

            boolean isValidDPn = false;
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DPN))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F25);
            }else{
                if(SupplierPortalConstant.LENGTH_DPN < splitItem.get(INDEX_DPN).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F25);
                }else{
                    if(isValidAsn && isValidDoNo){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                        criteria.setDPn(splitItem.get(INDEX_DPN));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T25);
                        }else{
                            isValidDPn = true;
                        }
                    }
                }
            }

            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SPN))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F26);
            }else{
                if(SupplierPortalConstant.LENGTH_SUPPLIER_PART_NO
                    < splitItem.get(INDEX_SPN).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F26);
                }else{
                    if(isValidAsn && isValidDoNo){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                        criteria.setSPn(splitItem.get(INDEX_SPN));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T26);
                        }
                    }
                }
            }

            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SHIPPING_QTY))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F27);
            }else{
                boolean isValidShippingQty = true;
                // [FIX] not allow input negative value
                //if(!StringUtil.isNumeric(splitItem.get(INDEX_SHIPPING_QTY))){
                if(!StringUtil.isPositiveNumeric(splitItem.get(INDEX_SHIPPING_QTY))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F27);
                    isValidShippingQty = false;
                }else{
                    if(splitItem.get(INDEX_SHIPPING_QTY).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(splitItem.get(INDEX_SHIPPING_QTY),
                            Constants.NINE, Constants.TWO)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F27);
                            isValidShippingQty = false;
                        }
                    }else{
                        if(Constants.SEVEN < splitItem.get(INDEX_SHIPPING_QTY).length()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F27);
                            isValidShippingQty = false;
                        }
                    }
                }
                if(isValidAsn && isValidDoNo && isValidDPn && isValidShippingQty){
                    AsnDomain criteria = new AsnDomain();
                    criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                    criteria.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                    criteria.setDPn(splitItem.get(INDEX_DPN));
                    criteria.setShippingQty(splitItem.get(INDEX_SHIPPING_QTY));
                    criteria.setAsnStatus(asnStatusStr);
                    criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                    criteria.setSupplierAuthenList(
                        invoiceInformationUploadingDomain.getSupplierAuthenList());
                    criteria.setDensoAuthenList(
                        invoiceInformationUploadingDomain.getDensoAuthenList());
                    
                    List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                    if(Constants.ZERO == result.size()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T27);
                    }
                }
            }

            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_S_UM))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F28);
            }else{
                if(SupplierPortalConstant.LENGTH_UM < splitItem.get(INDEX_S_UM).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F28);
                }else{
                    if(isValidAsn && isValidDoNo && isValidDPn){
                        AsnDomain criteria = new AsnDomain();
                        criteria.setAsnNo(splitItem.get(INDEX_ASN_NO));
                        criteria.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                        criteria.setDPn(splitItem.get(INDEX_DPN));
                        criteria.setUnitOfMeasure(splitItem.get(INDEX_S_UM));
                        criteria.setAsnStatus(asnStatusStr);
                        criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);

                        criteria.setSupplierAuthenList(
                            invoiceInformationUploadingDomain.getSupplierAuthenList());
                        criteria.setDensoAuthenList(
                            invoiceInformationUploadingDomain.getDensoAuthenList());
                        
                        List<AsnDomain> result = this.asnService.searchExistAsn(criteria);
                        if(Constants.ZERO == result.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T28);
                        }
                    }
                }
            }

            // [IN047] allow user upload Supplier Unit Price = 0
            //if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_S_UNIT_PRICE))
            //    || Constants.STR_ZERO.equals(splitItem.get(INDEX_S_UNIT_PRICE))){
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_S_UNIT_PRICE))) {
                
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F29);
            }else{
                // [FIX] not allow input negative value
                //if(!StringUtil.isNumeric(splitItem.get(INDEX_S_UNIT_PRICE))){
                if(!StringUtil.isPositiveNumeric(splitItem.get(INDEX_S_UNIT_PRICE))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F29);
                }else{
                    boolean isValidSUnitPrice = true;
                    if(splitItem.get(INDEX_S_UNIT_PRICE).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(splitItem.get(INDEX_S_UNIT_PRICE),
                            Constants.THIRTEEN, Constants.FOUR)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F29);
                            isValidSUnitPrice = false;
                        }
                    }else{
                        if(Constants.NINE < splitItem.get(INDEX_S_UNIT_PRICE).length()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F29);
                            isValidSUnitPrice = false;
                        }
                    }
                    
                    if(isValidPlanEta && isValidSUnitPrice && isValidDPn
                        && isValidDCd && isValidVendorCd)
                    {
                        if(sUnitPriceMap.containsKey(splitItem.get(INDEX_DPN))){
                            BigDecimal sUnitPrice = sUnitPriceMap.get(splitItem.get(INDEX_DPN));
                            if(Constants.ZERO != sUnitPrice.compareTo(
                                NumberUtil.toBigDecimalDefaultZero(
                                    splitItem.get(INDEX_S_UNIT_PRICE)))){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T29);
                            }
                        }else{
                            sUnitPriceMap.put(splitItem.get(INDEX_DPN),
                                NumberUtil.toBigDecimalDefaultZero(
                                    splitItem.get(INDEX_S_UNIT_PRICE)));
                        }
                        
                        if(!errorCodeList.contains(SupplierPortalConstant.UPLOAD_ERROR_CODE_T29)){
                            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(
                                splitItem.get(INDEX_DCD));
                            SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400
                                = new SpsMCompanyDensoDomain();
                            companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
                            List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
                                = this.companyDensoService.searchAs400ServerList(
                                    companyDensoCriteriaForSearchAS400);
                            
                            if(null == spsMAs400SchemaList || spsMAs400SchemaList.isEmpty()){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M29);
                            } else{
                                List<PseudoCigmaPurchasePriceMasterDomain> pseudoCigmaAsnList 
                                    = CommonWebServiceUtil
                                    .purchasePriceMasterResourceSearchUnitPrice(
                                        StringUtil.convertListToVarcharCommaSeperate(
                                            splitItem.get(INDEX_VENDOR_CD)),
                                        StringUtil.convertListToVarcharCommaSeperate(
                                            splitItem.get(INDEX_DPN)),
                                        StringUtil.convertListToVarcharCommaSeperate(
                                            splitItem.get(INDEX_PLAN_ETA).replace(
                                                Constants.SYMBOL_SLASH, Constants.EMPTY_STRING)),
                                        spsMAs400SchemaList.get(Constants.ZERO));
                                
                                // Start : [IN030] Price Master can return more than 1 record
                                //if(Constants.ZERO == pseudoCigmaAsnList.size()
                                //    || Constants.MINUS_ONE == NumberUtil.toBigDecimalDefaultZero(
                                //        splitItem.get(INDEX_S_UNIT_PRICE)).compareTo(
                                //            NumberUtil.toBigDecimalDefaultZero(
                                //                pseudoCigmaAsnList.get(Constants.ZERO)
                                //                    .getPseudoUnitPrice()))){
                                //    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T27);
                                //}
                                BigDecimal price = BigDecimal.ZERO;
                                if (null != pseudoCigmaAsnList 
                                    && Constants.ZERO != pseudoCigmaAsnList.size()) 
                                {
                                    price = NumberUtil.toBigDecimalDefaultZero(
                                        pseudoCigmaAsnList.get(Constants.ZERO)
                                            .getPseudoUnitPrice());
                                }
                                if(Constants.ZERO == pseudoCigmaAsnList.size()
                                    || Constants.MINUS_ONE == NumberUtil.toBigDecimalDefaultZero(
                                        splitItem.get(INDEX_S_UNIT_PRICE)).compareTo(price))
                                {
                                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T29);
                                }
                                // End : [IN030] Price Master can return more than 1 record
                            }
                        }
                    }
                }
            }
            
            // End : [IN036] Insert D/O No. and Revision Column number add 2.
            
            spsTmpUploadInvoiceDomain = new SpsTmpUploadInvoiceDomain();
            if(Constants.ZERO < errorCodeList.size()){
                String errorCodeStr = Constants.EMPTY_STRING;
                for(String errorCode : errorCodeList){
                    errorCodeStr = StringUtil.appendsString(
                        errorCodeStr, errorCode, Constants.SYMBOL_COMMA, Constants.SYMBOL_SPACE);
                }
                errorCodeStr = errorCodeStr.trim();
                
                spsTmpUploadInvoiceDomain.setSessionId(
                    invoiceInformationUploadingDomain.getSessionId());
                spsTmpUploadInvoiceDomain.setVendorCd(null);
                spsTmpUploadInvoiceDomain.setSPcd(null);
                spsTmpUploadInvoiceDomain.setSTaxId(null);
                spsTmpUploadInvoiceDomain.setDCd(null);
                spsTmpUploadInvoiceDomain.setDPcd(null);
                spsTmpUploadInvoiceDomain.setInvoiceNo(null);
                spsTmpUploadInvoiceDomain.setInvoiceDate(null);
                spsTmpUploadInvoiceDomain.setVatType(null);
                spsTmpUploadInvoiceDomain.setVatRate(null);
                spsTmpUploadInvoiceDomain.setCurrencyCd (null);
                spsTmpUploadInvoiceDomain.setBaseAmount(null);
                spsTmpUploadInvoiceDomain.setVatAmount(null);
                spsTmpUploadInvoiceDomain.setTotalAmount(null);
                spsTmpUploadInvoiceDomain.setCnNo(null);
                spsTmpUploadInvoiceDomain.setCnDate(null);
                spsTmpUploadInvoiceDomain.setCnBaseAmount(null);
                spsTmpUploadInvoiceDomain.setCnVatAmount(null);
                spsTmpUploadInvoiceDomain.setCnTotalAmount(null);
                spsTmpUploadInvoiceDomain.setUploadResult(Constants.STR_ONE);
                spsTmpUploadInvoiceDomain.setErrorCode(errorCodeStr.substring(
                    Constants.ZERO, errorCodeStr.length() - Constants.ONE));
                spsTmpUploadInvoiceDomain.setCsvLineNo(new BigDecimal(csvLineNo));
                spsTmpUploadInvoiceDomain.setAsnNo(null);
                spsTmpUploadInvoiceDomain.setTripNo(null);
                
                // [IN036] add DO No. and Revision
                spsTmpUploadInvoiceDomain.setSpsDoNo(null);
                spsTmpUploadInvoiceDomain.setRevision(null);
                
                spsTmpUploadInvoiceDomain.setPlanEta(null);
                spsTmpUploadInvoiceDomain.setActualEtd(null);
                spsTmpUploadInvoiceDomain.setUtc(null);
                spsTmpUploadInvoiceDomain.setDPn(null);
                spsTmpUploadInvoiceDomain.setSPn(null);
                spsTmpUploadInvoiceDomain.setShippingQty(null);
                spsTmpUploadInvoiceDomain.setSUnitOfMeasure(null);
                spsTmpUploadInvoiceDomain.setSPriceUnit(null);
                spsTmpUploadInvoiceDomain.setUploadDscId(
                    invoiceInformationUploadingDomain.getDscId());
                spsTmpUploadInvoiceDomain.setUploadDatetime(this.commonService.searchSysDate());
            }else{
                spsTmpUploadInvoiceDomain.setSessionId(
                    invoiceInformationUploadingDomain.getSessionId());
                spsTmpUploadInvoiceDomain.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                spsTmpUploadInvoiceDomain.setSPcd(splitItem.get(INDEX_SPCD));
                spsTmpUploadInvoiceDomain.setSTaxId(splitItem.get(INDEX_S_TAX_ID));
                spsTmpUploadInvoiceDomain.setDCd(splitItem.get(INDEX_DCD));
                spsTmpUploadInvoiceDomain.setDPcd(splitItem.get(INDEX_DPCD));
                spsTmpUploadInvoiceDomain.setInvoiceNo(splitItem.get(INDEX_INVOICE_NO));
                spsTmpUploadInvoiceDomain.setInvoiceDate(DateUtil.parseToSqlDate(
                    splitItem.get(INDEX_INVOICE_DATE), DateUtil.PATTERN_YYYYMMDD_SLASH));
                spsTmpUploadInvoiceDomain.setVatType(splitItem.get(INDEX_VAT_TYPE));
                spsTmpUploadInvoiceDomain.setVatRate(new BigDecimal(splitItem.get(INDEX_VAT_RATE)));
                spsTmpUploadInvoiceDomain.setCurrencyCd (splitItem.get(INDEX_S_CURRENCY));
                spsTmpUploadInvoiceDomain.setBaseAmount(new BigDecimal(
                    splitItem.get(INDEX_S_BASE_AMOUNT)));
                spsTmpUploadInvoiceDomain.setVatAmount(new BigDecimal(
                    splitItem.get(INDEX_INVOICE_VAT_AMOUNT)));
                spsTmpUploadInvoiceDomain.setTotalAmount(new BigDecimal(
                    splitItem.get(INDEX_TOTAL_AMOUNT)));
                spsTmpUploadInvoiceDomain.setCnNo(StringUtil.checkNullToEmpty(
                    splitItem.get(INDEX_CN_NO)));
                if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_DATE))){
                    spsTmpUploadInvoiceDomain.setCnDate(DateUtil.parseToSqlDate(
                        splitItem.get(INDEX_CN_DATE), DateUtil.PATTERN_YYYYMMDD_SLASH));
                }
                if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_BASE_AMOUNT))){
                    spsTmpUploadInvoiceDomain.setCnBaseAmount(null);
                }else{
                    spsTmpUploadInvoiceDomain.setCnBaseAmount(
                        new BigDecimal(splitItem.get(INDEX_CN_BASE_AMOUNT)));
                }
                if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_VAT_AMOUNT))){
                    spsTmpUploadInvoiceDomain.setCnVatAmount(null);
                }else{
                    spsTmpUploadInvoiceDomain.setCnVatAmount(
                        new BigDecimal(splitItem.get(INDEX_CN_VAT_AMOUNT)));
                }
                if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_CN_TOTAL_AMOUNT))){
                    spsTmpUploadInvoiceDomain.setCnTotalAmount(null);
                }else{
                    spsTmpUploadInvoiceDomain.setCnTotalAmount(
                        new BigDecimal(splitItem.get(INDEX_CN_TOTAL_AMOUNT)));
                }
                spsTmpUploadInvoiceDomain.setUploadResult(Constants.STR_ZERO);
                spsTmpUploadInvoiceDomain.setErrorCode(Constants.EMPTY_STRING);
                spsTmpUploadInvoiceDomain.setCsvLineNo(new BigDecimal(csvLineNo));
                spsTmpUploadInvoiceDomain.setAsnNo(splitItem.get(INDEX_ASN_NO));
                spsTmpUploadInvoiceDomain.setTripNo(splitItem.get(INDEX_TRIP_NO));
                
                // [IN036] add DO No. and Revision
                spsTmpUploadInvoiceDomain.setSpsDoNo(splitItem.get(INDEX_DO_NO));
                spsTmpUploadInvoiceDomain.setRevision(splitItem.get(INDEX_REVISION));
                
                spsTmpUploadInvoiceDomain.setPlanEta(DateUtil.parseToTimestamp(
                    splitItem.get(INDEX_PLAN_ETA), DateUtil.PATTERN_YYYYMMDD_SLASH));
                spsTmpUploadInvoiceDomain.setActualEtd(DateUtil.parseToTimestamp(
                    splitItem.get(INDEX_ACTUAL_ETD), DateUtil.PATTERN_YYYYMMDD_SLASH));
                spsTmpUploadInvoiceDomain.setUtc(utc);
                spsTmpUploadInvoiceDomain.setDPn(splitItem.get(INDEX_DPN));
                spsTmpUploadInvoiceDomain.setSPn(splitItem.get(INDEX_SPN));
                spsTmpUploadInvoiceDomain.setShippingQty(new BigDecimal(
                    splitItem.get(INDEX_SHIPPING_QTY)));
                spsTmpUploadInvoiceDomain.setSUnitOfMeasure(splitItem.get(INDEX_S_UM));
                spsTmpUploadInvoiceDomain.setSPriceUnit(new BigDecimal(
                    splitItem.get(INDEX_S_UNIT_PRICE)));
                spsTmpUploadInvoiceDomain.setUploadDscId(
                    invoiceInformationUploadingDomain.getDscId());
                spsTmpUploadInvoiceDomain.setUploadDatetime(this.commonService.searchSysDate());
            }
            csvLineNo += Constants.ONE;
            this.spsTmpUploadInvoiceService.create(spsTmpUploadInvoiceDomain);
        }
        
        this.searchTmpUploadInvoice(invoiceInformationUploadingDomain,
            invoiceInformationUploadingReturnDomain);
        return invoiceInformationUploadingReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService#searchTmpUploadInvoiceCsv(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain)
     */
    public InvoiceInformationUploadingDomain searchTmpUploadInvoiceCsv(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain) 
        throws ApplicationException {
        
        Locale locale = invoiceInformationUploadingDomain.getLocale();
        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain = 
            new InvoiceInformationUploadingDomain();
        
        SpsTmpUploadInvoiceCriteriaDomain criteria = new SpsTmpUploadInvoiceCriteriaDomain();
        criteria.setSessionId(invoiceInformationUploadingDomain.getSessionId());
        criteria.setUploadDscId(invoiceInformationUploadingDomain.getDscId());
        List<SpsTmpUploadInvoiceDomain> spsTmpUploadInvoiceDomainList 
            = this.spsTmpUploadInvoiceService.searchByCondition(criteria);
        if(null == spsTmpUploadInvoiceDomainList 
            || spsTmpUploadInvoiceDomainList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);  
        }
        
        int recordCount = this.spsTmpUploadInvoiceService.searchCount(criteria);
        criteria.setUploadResult(Constants.STR_ZERO);
        int iCorrectRecord = this.spsTmpUploadInvoiceService.searchCount(criteria);
        criteria.setUploadResult(Constants.STR_ONE);
        int iErrorRecord = this.spsTmpUploadInvoiceService.searchCount(criteria);
        invoiceInformationUploadingReturnDomain.setTotalRecord(String.valueOf(recordCount));
        invoiceInformationUploadingReturnDomain.setCorrectRecord(String.valueOf(iCorrectRecord));
        invoiceInformationUploadingReturnDomain.setWarningRecord(Constants.STR_ZERO);
        invoiceInformationUploadingReturnDomain.setErrorRecord(String.valueOf(iErrorRecord));
        
        if(Constants.ONE < spsTmpUploadInvoiceDomainList.size()){
            
            int i = Constants.ZERO;
            String invoiceNo = null;
            boolean invoiceNoAreSame = false;
            
            do{
                invoiceNo = StringUtil.nullToEmpty(
                    spsTmpUploadInvoiceDomainList.get(i).getInvoiceNo());
                i++;
                if(!invoiceNo.equals(StringUtil.nullToEmpty(
                    spsTmpUploadInvoiceDomainList.get(i).getInvoiceNo()))){
                    invoiceNoAreSame = false;
                }
                
            }while(i < spsTmpUploadInvoiceDomainList.size() && invoiceNoAreSame);
            
            if(invoiceNoAreSame){
                invoiceInformationUploadingReturnDomain.setInvoiceNo(
                    spsTmpUploadInvoiceDomainList.get(Constants.ZERO).getInvoiceNo());
            }else{
                invoiceInformationUploadingReturnDomain.setInvoiceNo(null);
            }
        }else if(Constants.ONE == spsTmpUploadInvoiceDomainList.size()){
            invoiceInformationUploadingReturnDomain.setInvoiceNo(
                spsTmpUploadInvoiceDomainList.get(Constants.ZERO).getInvoiceNo());
        }
        
        invoiceInformationUploadingReturnDomain.setTmpUploadInvoiceResultList(
            this.getTmpUploadInvoiceDomainList(spsTmpUploadInvoiceDomainList));
        
        String fileName = StringUtil.appendsString(
            Constants.UPLOAD_INVOICE_RESULT_LIST,
            Constants.SYMBOL_UNDER_SCORE,
            DateUtil.format(this.commonService.searchSysDate(), DateUtil.PATTERN_YYYYMMDD_HHMM));
        
        final String[] headerArr = new String[] {
            SupplierPortalConstant.LBL_INVOICE_NO,
            SupplierPortalConstant.LBL_TOTAL_RECORD,
            SupplierPortalConstant.LBL_CORRECT_RECORD,
            SupplierPortalConstant.LBL_ERROR_RECORD,
            SupplierPortalConstant.LBL_D_CD,
            SupplierPortalConstant.LBL_D_PCD,
            SupplierPortalConstant.LBL_S_CD,
            SupplierPortalConstant.LBL_S_PCD,
            SupplierPortalConstant.LBL_SUPPLIER_TAX_ID,
            SupplierPortalConstant.LBL_WARNING_RECORD,
            SupplierPortalConstant.LBL_CSV_LINE_NO,
            SupplierPortalConstant.LBL_UPLOAD_RESULT,
            SupplierPortalConstant.LBL_ASN_NO,
            SupplierPortalConstant.LBL_PLAN_ETA,
            SupplierPortalConstant.LBL_ACTUAL_ETD,
            SupplierPortalConstant.LBL_D_PN,
            SupplierPortalConstant.LBL_S_PN,
            SupplierPortalConstant.LBL_SHIPPING_QTY,
            SupplierPortalConstant.LBL_S_UM, 
            SupplierPortalConstant.LBL_ERROR_MESSAGE};
        
        
        List<Map<String, Object>> resultDetail = this.getInvoiceInformationUploadingMap(
            invoiceInformationUploadingReturnDomain, headerArr);
        CommonDomain commonDomain = new CommonDomain();
        commonDomain.setResultList(resultDetail);
        commonDomain.setHeaderArr(headerArr);
        commonDomain.setHeaderFlag(true);
        
        try {
            invoiceInformationUploadingReturnDomain.setCsvResult(
                this.commonService.createCsvString(commonDomain));
            invoiceInformationUploadingReturnDomain.setFileName(fileName);
            
        } catch (IOException ioe) {
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0012); 
        }
        
        return invoiceInformationUploadingReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService#searchDensoSupplierRelation(
     * com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public void searchDensoSupplierRelation(DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException {
        
        Locale locale = dataScopeControlDomain.getLocale();
        List<DensoSupplierRelationDomain> densoSupplierRelationList;
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                Constants.DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService#searchValidateGroupInvoice(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain)
     */
    public InvoiceInformationUploadingDomain searchValidateGroupInvoice(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain) 
        throws ApplicationException {
        
        Locale locale = invoiceInformationUploadingDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        List<ApplicationMessageDomain> warningMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain 
            = new InvoiceInformationUploadingDomain();
        
        boolean planEtaDateAreSame = true;
        boolean planEtaMonthAreSame = true;
        boolean planEtaWeekAreSame = true;
        
        SpsTmpUploadInvoiceCriteriaDomain criteriaSearch = new SpsTmpUploadInvoiceCriteriaDomain();
        criteriaSearch.setSessionId(invoiceInformationUploadingDomain.getSessionId());
        criteriaSearch.setUploadDscId(invoiceInformationUploadingDomain.getDscId());
        List<SpsTmpUploadInvoiceDomain> spsTmpUploadInvoiceDomainList 
            = this.spsTmpUploadInvoiceService.searchByCondition(criteriaSearch);
        
        if(Constants.ONE < spsTmpUploadInvoiceDomainList.size()){
            
            Iterator<SpsTmpUploadInvoiceDomain> iterator = spsTmpUploadInvoiceDomainList.iterator();
            Set<String> planEtaDateList = new TreeSet<String>();
            Set<String> planEtaMonthList = new TreeSet<String>();
            Set<String> planEtaWeekList = new TreeSet<String>();
            Calendar planEtaDateCalendar = Calendar.getInstance();
            String planEtaDate = null;
            String planEtaMonth = null;
            String planEtaWeek = null;
            
            while(iterator.hasNext() && (planEtaDateAreSame || planEtaMonthAreSame 
                || planEtaWeekAreSame)){
                
                SpsTmpUploadInvoiceDomain tmpUploadInvoiceItem = iterator.next();
                
                if(null != tmpUploadInvoiceItem.getPlanEta()){
                    planEtaDateCalendar.setTimeInMillis(tmpUploadInvoiceItem.getPlanEta()
                        .getTime());
                    planEtaDate = DateUtil.format(tmpUploadInvoiceItem.getPlanEta(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                    planEtaMonth = String.valueOf(planEtaDateCalendar.get(Calendar.MONTH));
                    planEtaWeek = String.valueOf(planEtaDateCalendar.get(Calendar.WEEK_OF_YEAR));
                }else{
                    planEtaDate = Constants.EMPTY_STRING;
                    planEtaMonth = Constants.EMPTY_STRING;
                    planEtaWeek = Constants.EMPTY_STRING;
                }
                if(planEtaDateAreSame && planEtaDateList.add(planEtaDate)
                    && Constants.ONE < planEtaDateList.size()){
                    planEtaDateAreSame = false;
                }
                if(planEtaMonthAreSame && planEtaMonthList.add(planEtaMonth)
                    && Constants.ONE < planEtaDateList.size()){
                    planEtaMonthAreSame = false;
                }
                if(planEtaWeekAreSame && planEtaWeekList.add(planEtaWeek)
                    && Constants.ONE < planEtaDateList.size()){
                    planEtaWeekAreSame = false;
                }                
            }
        }
        
        SpsMCompanyDensoCriteriaDomain criteria = new SpsMCompanyDensoCriteriaDomain();
        criteria.setDCd(invoiceInformationUploadingDomain.getDCd());
        SpsMCompanyDensoDomain  spsMCompanyDensoDomain =
            this.spsMCompanyDensoService.searchByKey(criteria);
        if(SupplierPortalConstant.GROUP_INVOICE_ERROR_TYPE.equals(
            spsMCompanyDensoDomain.getGroupInvoiceErrorType())){
            if(SupplierPortalConstant.GROUP_INVOICE_DATE_TYPE.equals(
                spsMCompanyDensoDomain.getGroupInvoiceType())
                && !planEtaDateAreSame){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                        SupplierPortalConstant.LBL_PLANETA_DATE)));
            }else if(SupplierPortalConstant.GROUP_INVOICE_MONTH_TYPE.equals(spsMCompanyDensoDomain
                .getGroupInvoiceType()) 
                && !planEtaMonthAreSame){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                        SupplierPortalConstant.LBL_PLANETA_MONTH)));
            }else if(SupplierPortalConstant.GROUP_INVOICE_WEEK_TYPE.equals(spsMCompanyDensoDomain
                .getGroupInvoiceType())
                && !planEtaWeekAreSame){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                        SupplierPortalConstant.LBL_PLANETA_WEEK)));
            }
        }else if(SupplierPortalConstant.GROUP_INVOICE_WARNING_TYPE.equals(spsMCompanyDensoDomain
            .getGroupInvoiceErrorType())){
            if(SupplierPortalConstant.GROUP_INVOICE_DATE_TYPE.equals(spsMCompanyDensoDomain
                .getGroupInvoiceType())
                && !planEtaDateAreSame){
                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_W2_0001, 
                        SupplierPortalConstant.LBL_PLANETA_DATE)));
            }else if(SupplierPortalConstant.GROUP_INVOICE_MONTH_TYPE.equals(spsMCompanyDensoDomain
                .getGroupInvoiceType())
                && !planEtaMonthAreSame){
                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_W2_0001, 
                        SupplierPortalConstant.LBL_PLANETA_MONTH)));
            }else if(SupplierPortalConstant.GROUP_INVOICE_WEEK_TYPE.equals(spsMCompanyDensoDomain
                .getGroupInvoiceType())
                && !planEtaWeekAreSame){
                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_W2_0001, 
                        SupplierPortalConstant.LBL_PLANETA_WEEK)));
            }
        }
        
        if(Constants.ZERO < errorMessageList.size()){
            invoiceInformationUploadingReturnDomain.setErrorMessageList(errorMessageList);
        }
        
        if(Constants.ZERO < warningMessageList.size()){
            invoiceInformationUploadingReturnDomain.setWarningMessageList(warningMessageList);
        }
        
        return invoiceInformationUploadingReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
//    /**
//     * <p>Search user authority.</p>
//     * <ul>
//     * <li>Search user authority.</li>
//     * </ul>
//     * 
//     * @param dataScopeControlDomain the Data Scope Control Domain
//     * @return the Invoice Information Uploading Domain
//     * @throws ApplicationException 
//     */
//    private InvoiceInformationUploadingDomain searchUserAuthority(
//        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException
//    {
//        List<CompanySupplierDomain> companySupplierList = null;
//        List<CompanyDensoDomain> companyDensoList = null;
//        List<PlantDensoDomain> companyDensoPlantList = null;
//        List<PlantSupplierDomain> companySupplierPlantList = null;
//        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = null;
//        PlantDensoWithScopeDomain plantDensoWithScopeDomain = null;
//        List<String> tempStrList = null;
//        Map<String, List<String>> tempPlantMap = null;
//        
//        Locale locale = dataScopeControlDomain.getLocale();
//        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain 
//            = new InvoiceInformationUploadingDomain();
//
//        plantSupplierWithScopeDomain = new PlantSupplierWithScopeDomain();
//        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
//        companySupplierList = this.companySupplierService.searchCompanySupplier(
//            plantSupplierWithScopeDomain);
//        if(Constants.ZERO == companySupplierList.size()){
//            MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
//                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
//        }
//        tempStrList = new ArrayList<String>();
//        for(CompanySupplierDomain item : companySupplierList){
//            tempStrList.add(item.getVendorCd());
//        }
//        invoiceInformationUploadingReturnDomain.setCompanySupplierList(tempStrList);
//        
//        companySupplierPlantList = this.plantSupplierService.searchPlantSupplierInformation(
//            plantSupplierWithScopeDomain);
//        if(Constants.ZERO == companySupplierPlantList.size()){
//            MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
//                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
//        }
//        tempPlantMap = new HashMap<String, List<String>>();
//        for(PlantSupplierDomain item : companySupplierPlantList){
//            if(tempPlantMap.containsKey(item.getSPcd())){
//                tempPlantMap.get(item.getSPcd()).add(item.getVendorCd());
//            }else{
//                List<String> vendorCodeList = new ArrayList<String>();
//                vendorCodeList.add(item.getVendorCd());
//                tempPlantMap.put(item.getSPcd(), vendorCodeList);
//            }
//        }
//        invoiceInformationUploadingReturnDomain.setPlantSupplierList(tempPlantMap);
//
//        plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
//        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
//        companyDensoList = this.companyDensoService.searchCompanyDenso(plantDensoWithScopeDomain);
//        if(Constants.ZERO == companyDensoList.size()){
//            MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
//                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
//        }
//        tempStrList = new ArrayList<String>();
//        for(CompanyDensoDomain item : companyDensoList){
//            tempStrList.add(item.getDCd());
//        }
//        invoiceInformationUploadingReturnDomain.setCompanyDensoList(tempStrList);
//        
//        companyDensoPlantList = this.plantDensoService.searchPlantDensoInformation(
//            plantDensoWithScopeDomain);
//        if(Constants.ZERO == companyDensoPlantList.size()){
//            MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
//                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
//        }
//        tempPlantMap = new HashMap<String, List<String>>();
//        for(PlantDensoDomain item : companyDensoPlantList){
//            if(tempPlantMap.containsKey(item.getDPcd())){
//                tempPlantMap.get(item.getDPcd()).add(item.getDCd());
//            }else{
//                List<String> densoPlantList = new ArrayList<String>();
//                densoPlantList.add(item.getDCd());
//                tempPlantMap.put(item.getDPcd(), densoPlantList);
//            }
//        }
//        invoiceInformationUploadingReturnDomain.setPlantDensoList(tempPlantMap);
//        return invoiceInformationUploadingReturnDomain;
//    }
    
    /**
     * <p>Search temp upload invoice.</p>
     * <ul>
     * <li>Search data from temporary upload invoice table.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingDomain the Invoice Information Uploading Domain
     * @param invoiceInformationUploadingReturnDomain the Invoice Information Uploading Domain
     * @throws ApplicationException 
     */
    private void searchTmpUploadInvoice(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain,
        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain)
        throws ApplicationException{
        
        Locale locale = invoiceInformationUploadingDomain.getLocale();
        
        boolean invoiceNoAreSame = true;
        boolean supplierCodeAreSame = true;
        boolean supplierTaxIdAreSame = true;
        boolean densoCodeAreSame = true;
        boolean densoPlantCodeAreSame = true;
        
        SpsTmpUploadInvoiceCriteriaDomain criteriaSearchCount 
            = new SpsTmpUploadInvoiceCriteriaDomain();
        criteriaSearchCount.setSessionId(invoiceInformationUploadingDomain.getSessionId());
        criteriaSearchCount.setUploadDscId(invoiceInformationUploadingDomain.getDscId());
        int recordCount = this.spsTmpUploadInvoiceService.searchCount(criteriaSearchCount);
        if(Constants.ZERO == recordCount){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);   
        }
        
        MiscellaneousDomain miscLimitPerPageDomain = new MiscellaneousDomain();
        miscLimitPerPageDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        miscLimitPerPageDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV003_PLM);
        Integer recordLimitPerPage = miscService.searchMiscValue(miscLimitPerPageDomain);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        invoiceInformationUploadingDomain.setMaxRowPerPage(recordLimitPerPage);
        
        SpsPagingUtil.calcPaging(invoiceInformationUploadingDomain, recordCount);
        SpsTmpUploadInvoiceCriteriaDomain criteriaSearchForPaging 
            = new SpsTmpUploadInvoiceCriteriaDomain();
        criteriaSearchForPaging.setSessionId(invoiceInformationUploadingDomain.getSessionId());
        criteriaSearchForPaging.setUploadDscId(invoiceInformationUploadingDomain.getDscId());
        criteriaSearchForPaging.setRowNumFrom(invoiceInformationUploadingDomain.getRowNumFrom());
        criteriaSearchForPaging.setRowNumTo(invoiceInformationUploadingDomain.getRowNumTo());
        List<SpsTmpUploadInvoiceDomain> spsTmpUploadInvoiceDomainList 
            = this.spsTmpUploadInvoiceService.searchByConditionForPaging(criteriaSearchForPaging);
        
        SpsTmpUploadInvoiceCriteriaDomain criteriaSearchCountCorrectRecord 
            = new SpsTmpUploadInvoiceCriteriaDomain();
        criteriaSearchCountCorrectRecord.setSessionId(invoiceInformationUploadingDomain
            .getSessionId());
        criteriaSearchCountCorrectRecord.setUploadDscId(invoiceInformationUploadingDomain
            .getDscId());
        criteriaSearchCountCorrectRecord.setUploadResult(Constants.STR_ZERO);
        int iCorrectRecord = this.spsTmpUploadInvoiceService.searchCount(
            criteriaSearchCountCorrectRecord);
    
        SpsTmpUploadInvoiceCriteriaDomain criteriaSearchCountErrorRecord 
            = new SpsTmpUploadInvoiceCriteriaDomain();
        criteriaSearchCountErrorRecord.setSessionId(invoiceInformationUploadingDomain
            .getSessionId());
        criteriaSearchCountErrorRecord.setUploadDscId(invoiceInformationUploadingDomain.getDscId());
        criteriaSearchCountErrorRecord.setUploadResult(Constants.STR_ONE);
        int iErrorRecord = this.spsTmpUploadInvoiceService.searchCount(
            criteriaSearchCountErrorRecord);
        
        invoiceInformationUploadingReturnDomain.setTotalRecord(String.valueOf(recordCount));
        invoiceInformationUploadingReturnDomain.setCorrectRecord(String.valueOf(iCorrectRecord));
        invoiceInformationUploadingReturnDomain.setWarningRecord(Constants.STR_ZERO);
        invoiceInformationUploadingReturnDomain.setErrorRecord(String.valueOf(iErrorRecord));
        
        if(Constants.ZERO < spsTmpUploadInvoiceDomainList.size()){
            
            Iterator<SpsTmpUploadInvoiceDomain> iterator = spsTmpUploadInvoiceDomainList.iterator();
            Set<String> invoiceNoList = new TreeSet<String>();
            Set<String> supplierCodeList = new TreeSet<String>();
            Set<String> supplierTaxIdList = new TreeSet<String>();
            Set<String> densoCodeList = new TreeSet<String>();
            Set<String> densoPlantCodeList = new TreeSet<String>();
            String invoiceNo = null;
            String supplierCode = null;
            String supplierTaxId = null;
            String densoCode = null;
            String densoPlantCode = null;
            
            while(iterator.hasNext() && (invoiceNoAreSame || supplierCodeAreSame 
                || supplierTaxIdAreSame || densoCodeAreSame || densoPlantCodeAreSame)){
                
                SpsTmpUploadInvoiceDomain tmpUploadInvoiceItem = iterator.next();
                if(null != tmpUploadInvoiceItem.getInvoiceNo()){
                    invoiceNo = tmpUploadInvoiceItem.getInvoiceNo();
                }else{
                    invoiceNo = Constants.EMPTY_STRING;
                }
                if(null != tmpUploadInvoiceItem.getVendorCd()){
                    supplierCode = tmpUploadInvoiceItem.getVendorCd();
                }else{
                    supplierCode = Constants.EMPTY_STRING;
                }
                if(null != tmpUploadInvoiceItem.getSTaxId()){
                    supplierTaxId = tmpUploadInvoiceItem.getSTaxId();
                }else{
                    supplierTaxId = Constants.EMPTY_STRING;
                }
                if(null != tmpUploadInvoiceItem.getDCd()){
                    densoCode = tmpUploadInvoiceItem.getDCd();
                }else{
                    densoCode = Constants.EMPTY_STRING;
                }
                if(null != tmpUploadInvoiceItem.getDPcd()){
                    densoPlantCode = tmpUploadInvoiceItem.getDPcd();
                }else{
                    densoPlantCode = Constants.EMPTY_STRING;
                }
                if(invoiceNoAreSame && invoiceNoList.add(invoiceNo)
                    && Constants.ONE < invoiceNoList.size()){
                    invoiceNoAreSame = false;
                }  
                if(supplierCodeAreSame && supplierCodeList.add(supplierCode)
                    && Constants.ONE < supplierCodeList.size()){
                    supplierCodeAreSame = false;
                } 
                if(supplierTaxIdAreSame && supplierTaxIdList.add(supplierTaxId)
                    && Constants.ONE < supplierTaxIdList.size()){
                    supplierTaxIdAreSame = false;
                } 
                if(densoCodeAreSame && densoCodeList.add(densoCode)
                    && Constants.ONE < densoCodeList.size()){
                    densoCodeAreSame = false;
                } 
                if(densoPlantCodeAreSame && densoPlantCodeList.add(densoPlantCode)
                    && Constants.ONE < densoPlantCodeList.size()){
                    densoPlantCodeAreSame = false;
                } 
            }
            
            if(invoiceNoAreSame){
                invoiceInformationUploadingReturnDomain.setInvoiceNo(spsTmpUploadInvoiceDomainList
                    .get(Constants.ZERO).getInvoiceNo());
            }else{
                invoiceInformationUploadingReturnDomain.setInvoiceNo(null);
            }
            if(supplierCodeAreSame){
                invoiceInformationUploadingReturnDomain.setVendorCd(
                    spsTmpUploadInvoiceDomainList.get(Constants.ZERO).getVendorCd());
            }else{
                invoiceInformationUploadingReturnDomain.setVendorCd(null);
            }
            if(supplierTaxIdAreSame){
                invoiceInformationUploadingReturnDomain.setSupplierTaxId(
                    spsTmpUploadInvoiceDomainList.get(Constants.ZERO).getSTaxId());
            }else{
                invoiceInformationUploadingReturnDomain.setSupplierTaxId(null);
            }
            if(densoCodeAreSame){
                invoiceInformationUploadingReturnDomain.setDCd(spsTmpUploadInvoiceDomainList
                    .get(Constants.ZERO).getDCd());
            }else{
                invoiceInformationUploadingReturnDomain.setDCd(null);
            }
            if(densoPlantCodeAreSame){
                invoiceInformationUploadingReturnDomain.setDPcd(
                    spsTmpUploadInvoiceDomainList.get(Constants.ZERO).getDPcd());
            }else{
                invoiceInformationUploadingReturnDomain.setDPcd(null);
            }
        }
        
        invoiceInformationUploadingReturnDomain.setTmpUploadInvoiceResultList(
            this.getTmpUploadInvoiceDomainList(spsTmpUploadInvoiceDomainList));
        
    }
    
    /**
     * <p>Get invoice information uploading map.</p>
     * <ul>
     * <li>Preparation data for create CSV file.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingReturnDomain the Invoice Information Uploading Domain
     * @param header the array of string
     * @return the list of map string with object
     */
    private List<Map<String, Object>> getInvoiceInformationUploadingMap(
        InvoiceInformationUploadingDomain invoiceInformationUploadingReturnDomain, String[] header)
    {
        List<Map<String, Object>> invoiceInformationUploadingMapList 
            = new ArrayList<Map<String, Object>>();
        List<TmpUploadInvoiceResultDomain> tmpUploadInvoiceResultList 
            = invoiceInformationUploadingReturnDomain.getTmpUploadInvoiceResultList();
        
        String errorCode = null;
        Map<String, Object>  invoiceInformationUploadingMap = null;
        Map<String, String> uploadResultsMap = new HashMap<String, String>();
        
        uploadResultsMap.put(Constants.STR_ZERO, Constants.STR_OK);
        uploadResultsMap.put(Constants.STR_ONE, Constants.STR_NG);
        
        for(TmpUploadInvoiceResultDomain record : tmpUploadInvoiceResultList){
            invoiceInformationUploadingMap = new HashMap<String, Object>();
            errorCode = Constants.EMPTY_STRING;
            
            invoiceInformationUploadingMap.put(header[Constants.ZERO], 
                StringUtil.checkNullToEmpty(record.getSpsTmpUploadInvoice().getInvoiceNo()));
            invoiceInformationUploadingMap.put(header[Constants.ONE], StringUtil.checkNullToEmpty(
                invoiceInformationUploadingReturnDomain.getTotalRecord()));
            invoiceInformationUploadingMap.put(header[Constants.TWO], StringUtil.checkNullToEmpty(
                invoiceInformationUploadingReturnDomain.getCorrectRecord()));
            invoiceInformationUploadingMap.put(header[Constants.THREE], StringUtil.checkNullToEmpty(
                invoiceInformationUploadingReturnDomain.getErrorRecord()));
            invoiceInformationUploadingMap.put(header[Constants.FOUR], StringUtil.checkNullToEmpty(
                record.getSpsTmpUploadInvoice().getDCd()));
            invoiceInformationUploadingMap.put(header[Constants.FIVE], StringUtil.checkNullToEmpty(
                record.getSpsTmpUploadInvoice().getDPcd()));
            invoiceInformationUploadingMap.put(header[Constants.SIX], StringUtil.checkNullToEmpty(
                record.getSpsTmpUploadInvoice().getVendorCd()));
            invoiceInformationUploadingMap.put(header[Constants.SEVEN], StringUtil.checkNullToEmpty(
                record.getSpsTmpUploadInvoice().getSPcd()));
            invoiceInformationUploadingMap.put(header[Constants.EIGHT], StringUtil.checkNullToEmpty(
                record.getSpsTmpUploadInvoice().getSTaxId()));
            invoiceInformationUploadingMap.put(header[Constants.NINE], StringUtil.checkNullToEmpty(
                invoiceInformationUploadingReturnDomain.getWarningRecord()));
            invoiceInformationUploadingMap.put(header[Constants.TEN],
                record.getSpsTmpUploadInvoice().getCsvLineNo());
            invoiceInformationUploadingMap.put(header[Constants.ELEVEN],
                uploadResultsMap.get(record.getSpsTmpUploadInvoice().getUploadResult()));
            invoiceInformationUploadingMap.put(header[Constants.TWELVE],
                StringUtil.checkNullToEmpty(record.getSpsTmpUploadInvoice().getAsnNo()));
            invoiceInformationUploadingMap.put(header[Constants.THIRTEEN],
                StringUtil.checkNullToEmpty(record.getPlanEta()));
            invoiceInformationUploadingMap.put(header[Constants.FOURTEEN],
                StringUtil.checkNullToEmpty(record.getActualEtd()));
            invoiceInformationUploadingMap.put(header[Constants.FIFTEEN],
                StringUtil.checkNullToEmpty(record.getSpsTmpUploadInvoice().getDPn()));
            invoiceInformationUploadingMap.put(header[Constants.SIXTEEN],
                StringUtil.checkNullToEmpty(record.getSpsTmpUploadInvoice().getSPn()));
            invoiceInformationUploadingMap.put(header[Constants.SEVENTEEN],
                StringUtil.checkNullToEmpty(record.getSpsTmpUploadInvoice().getShippingQty()));
            invoiceInformationUploadingMap.put(header[Constants.EIGHTEEN],
                StringUtil.checkNullToEmpty(record.getSpsTmpUploadInvoice().getSUnitOfMeasure()));
            
            if(!StringUtil.checkNullOrEmpty(record.getSpsTmpUploadInvoice().getErrorCode())){
                errorCode = StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE,
                    record.getSpsTmpUploadInvoice().getErrorCode(), Constants.SYMBOL_DOUBLE_QUOTE);
            }
            
            invoiceInformationUploadingMap.put(header[Constants.NINETEEN], errorCode);
            invoiceInformationUploadingMapList.add(invoiceInformationUploadingMap);
        }
        return invoiceInformationUploadingMapList;
    }
    
    /**
     * <p>Get tmp upload invoice domain list.</p>
     * <ul>
     * <li>Getter data from setting date time format for show on screen.</li>
     * </ul>
     * 
     * @param spsTmpUploadInvoiceDomainList the list of the SPS Temp Upload Invoice Domain 
     * @return list of the SPS Upload Invoice Domain 
     */
    private List<TmpUploadInvoiceResultDomain> getTmpUploadInvoiceDomainList(
        List<SpsTmpUploadInvoiceDomain> spsTmpUploadInvoiceDomainList) {
        List<TmpUploadInvoiceResultDomain> tmpUploadInvoiceDomainList 
            = new ArrayList<TmpUploadInvoiceResultDomain>();
        
        StringBuffer utcActualETD = null;
        StringBuffer utcPlanETA = null;
        StringBuffer utc = null;
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        
        for(SpsTmpUploadInvoiceDomain item : spsTmpUploadInvoiceDomainList){
            TmpUploadInvoiceResultDomain tmpUploadInvoiceResultDomain 
                = new TmpUploadInvoiceResultDomain();
            tmpUploadInvoiceResultDomain.setSpsTmpUploadInvoice(item);
            
            if(null != item.getUtc()){
                utc = new StringBuffer();
                utc.append(Constants.SYMBOL_OPEN_BRACKET);
                utc.append(Constants.SYMBOL_SPACE);
                utc.append(Constants.WORD_UTC);
                utc.append(item.getUtc());
                utc.append(Constants.SYMBOL_CLOSE_BRACKET);
                
                utcActualETD = new StringBuffer();
                utcActualETD.append(
                    DateUtil.format(item.getActualEtd(), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
                utcActualETD.append(utc);
                tmpUploadInvoiceResultDomain.setActualEtd(utcActualETD.toString());
                
                utcPlanETA = new StringBuffer();
                utcPlanETA.append(
                    DateUtil.format(item.getPlanEta(), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
                utcPlanETA.append(utc);
                tmpUploadInvoiceResultDomain.setPlanEta(utcPlanETA.toString());
                
            }
            
            if(null != item.getShippingQty()){
                // [IN009] Rounding Mode use HALF_UP
                //tmpUploadInvoiceResultDomain.setShippingQty(
                //    decimalFormat.format(item.getShippingQty()));
                tmpUploadInvoiceResultDomain.setShippingQty(NumberUtil.formatRoundingHalfUp(
                    decimalFormat, item.getShippingQty(), Constants.TWO));
            }
            
            tmpUploadInvoiceDomainList.add(tmpUploadInvoiceResultDomain);
        }
        return tmpUploadInvoiceDomainList;
    }
    
    /**
     * <p>Convert value to Upper Case.</p>
     * 
     * @param splitItem the list of string
     */
    private void convertValueToUpperCase(List<String> splitItem){
        splitItem.set(INDEX_DCD, splitItem.get(INDEX_DCD).toUpperCase());
        splitItem.set(INDEX_DPCD, splitItem.get(INDEX_DPCD).toUpperCase());
        splitItem.set(INDEX_VENDOR_CD, splitItem.get(INDEX_VENDOR_CD).toUpperCase());
        splitItem.set(INDEX_SPCD, splitItem.get(INDEX_SPCD).toUpperCase());
        splitItem.set(INDEX_S_TAX_ID, splitItem.get(INDEX_S_TAX_ID).toUpperCase());
        splitItem.set(INDEX_INVOICE_NO, splitItem.get(INDEX_INVOICE_NO).toUpperCase());
        splitItem.set(INDEX_INVOICE_DATE, splitItem.get(INDEX_INVOICE_DATE).toUpperCase());
        splitItem.set(INDEX_VAT_TYPE, splitItem.get(INDEX_VAT_TYPE).toUpperCase());
        splitItem.set(INDEX_VAT_RATE, splitItem.get(INDEX_VAT_RATE).toUpperCase());
        splitItem.set(INDEX_S_CURRENCY, splitItem.get(INDEX_S_CURRENCY).toUpperCase());
        splitItem.set(INDEX_S_BASE_AMOUNT, splitItem.get(INDEX_S_BASE_AMOUNT).toUpperCase());
        splitItem.set(INDEX_INVOICE_VAT_AMOUNT, splitItem.get(INDEX_INVOICE_VAT_AMOUNT).toUpperCase());
        splitItem.set(INDEX_TOTAL_AMOUNT, splitItem.get(INDEX_TOTAL_AMOUNT).toUpperCase());
        splitItem.set(INDEX_CN_NO, splitItem.get(INDEX_CN_NO).toUpperCase());
        splitItem.set(INDEX_CN_DATE, splitItem.get(INDEX_CN_DATE).toUpperCase());
        splitItem.set(INDEX_CN_BASE_AMOUNT, splitItem.get(INDEX_CN_BASE_AMOUNT).toUpperCase());
        splitItem.set(INDEX_CN_VAT_AMOUNT, splitItem.get(INDEX_CN_VAT_AMOUNT).toUpperCase());
        splitItem.set(INDEX_CN_TOTAL_AMOUNT, splitItem.get(INDEX_CN_TOTAL_AMOUNT).toUpperCase());
        splitItem.set(INDEX_ASN_NO, splitItem.get(INDEX_ASN_NO).toUpperCase());
        splitItem.set(INDEX_TRIP_NO, splitItem.get(INDEX_TRIP_NO).toUpperCase());
        
        // [IN036] Convert SPS_DO_NO and REVISION to upper case
        splitItem.set(INDEX_DO_NO, splitItem.get(INDEX_DO_NO).toUpperCase());
        splitItem.set(INDEX_REVISION, splitItem.get(INDEX_REVISION).toUpperCase());
        
        splitItem.set(INDEX_PLAN_ETA, splitItem.get(INDEX_PLAN_ETA).toUpperCase());
        splitItem.set(INDEX_ACTUAL_ETD, splitItem.get(INDEX_ACTUAL_ETD).toUpperCase());
        splitItem.set(INDEX_DPN, splitItem.get(INDEX_DPN).toUpperCase());
        splitItem.set(INDEX_SPN, splitItem.get(INDEX_SPN).toUpperCase());
        splitItem.set(INDEX_SHIPPING_QTY, splitItem.get(INDEX_SHIPPING_QTY).toUpperCase());
        splitItem.set(INDEX_S_UM, splitItem.get(INDEX_S_UM).toUpperCase());
        splitItem.set(INDEX_S_UNIT_PRICE, splitItem.get(INDEX_S_UNIT_PRICE).toUpperCase());
    }
}
