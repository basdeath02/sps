/*
 * ModifyDate Development company     Describe 
 * 2014/06/18 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadForDisplayDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadResultDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>
 * The class CNDNDownloadingFacadeServiceImpl.
 * </p>
 * <p>
 * Facade for CNDNDownloadingFacadeServiceImpl.
 * </p>
 * <ul>
 * <li>Method search : searchCountFileUploadDetail</li>
 * <li>Method search : searchFileUploadDetail</li>
 * <li>Method search : searchFileName</li>
 * <li>Method search : searchFileDownload</li>
 * </ul>
 * 
 * @author CSI
 */
public class CnDnDownloadingFacadeServiceImpl implements
    CnDnDownloadingFacadeService {

    /** The file management service. */
    private FileManagementService fileManagementService;

    /** The densoSupplierRelation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;

    /** The companySupplier Service. */
    private CompanySupplierService companySupplierService;

    /** The plantSupplier Service. */
    private PlantSupplierService plantSupplierService;

    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;

    /** The File Upload Service. */
    private FileUploadService fileUploadService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * <p>
     * Call Service CompanyDensoService.
     * </p>
     */
    private CompanyDensoService densoCompanyService;
    
    /**
     * <p>
     * Call Service PlantDensoService.
     * </p>
     */
    private PlantDensoService plantDensoService;

    /**
     * Instantiates a new download facade service implement.
     */
    public CnDnDownloadingFacadeServiceImpl() {
        super();
    }

    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(
        FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationService.
     * </p>
     * 
     * @param densoSupplierRelationService Set for densoSupplierRelationService
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * <p>
     * Setter method for companySupplierService.
     * </p>
     * 
     * @param companySupplierService Set for companySupplierService
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }

    /**
     * <p>
     * Setter method for plantSupplierService.
     * </p>
     * 
     * @param plantSupplierService Set for plantSupplierService
     */
    public void setPlantSupplierService(
        PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>
     * Setter method for recordLimitService.
     * </p>
     * 
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * <p>
     * Setter method for fileUploadService.
     * </p>
     * 
     * @param fileUploadService Set for fileUploadService
     */
    public void setFileUploadService(FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    /**
     * <p>Setter method for densoCompanyService.</p>
     *
     * @param densoCompanyService Set for densoCompanyService
     */
    public void setDensoCompanyService(CompanyDensoService densoCompanyService) {
        this.densoCompanyService = densoCompanyService;
    }

    /**
     * <p>Setter method for plantDensoService.</p>
     *
     * @param plantDensoService Set for plantDensoService
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.CNDNDownloadingFacadeService
     *      #searchFileDownload
     *      (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public FileUploadResultDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException {

        FileUploadResultDomain fileUploadResultDomain = new FileUploadResultDomain();
        Locale locale = dataScopeControlDomain.getLocale();

        try {

            dataScopeControlDomain
                .setDensoSupplierRelationDomainList(densoSupplierRelationService
                    .searchDensoSupplierRelation(dataScopeControlDomain));

            /** Get relation between DENSO and Supplier. */
            List<DensoSupplierRelationDomain> densoSupplierRelationList = 
                this.densoSupplierRelationService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            if (null == densoSupplierRelationList
                || densoSupplierRelationList.size() <= Constants.ZERO) {
                MessageUtil.throwsApplicationMessage(
                    locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    Constants.EMPTY_STRING,
                    new String[] {getLabel(
                        SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                        locale)});
            }
            fileUploadResultDomain
                .setDensoSupplierRelationList(densoSupplierRelationList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
            
            /** Get supplier company information from SupplierCompanyService. */
            PlantSupplierWithScopeDomain plantSupplierWithScopeDomain
                = new PlantSupplierWithScopeDomain();
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            List<CompanySupplierDomain> companySupplierDomain = null;
            companySupplierDomain = companySupplierService
                .searchCompanySupplier(plantSupplierWithScopeDomain);
            if (null == companySupplierDomain
                || Constants.ZERO == companySupplierDomain.size()) {
                MessageUtil.throwsApplicationMessage(
                    locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {getLabel(
                        SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE,
                        locale)});
            }
            MiscellaneousDomain miscDomainSupplier = null;
            List<MiscellaneousDomain> companySupplierList = new ArrayList<MiscellaneousDomain>();
            for (CompanySupplierDomain supplier : companySupplierDomain) {
                miscDomainSupplier = new MiscellaneousDomain();
                miscDomainSupplier.setMiscCode(supplier.getVendorCd());
                miscDomainSupplier.setMiscValue(supplier.getVendorCd());
                companySupplierList.add(miscDomainSupplier);
            }
            fileUploadResultDomain.setCompanySupplierList(companySupplierList);
            
            // Get Plant Supplier
            List<PlantSupplierDomain> plantSupplierDomainList = plantSupplierService
                .searchPlantSupplier(plantSupplierWithScopeDomain);
            if (null == plantSupplierDomainList 
                || Constants.ZERO == plantSupplierDomainList.size()) {
                MessageUtil
                    .throwsApplicationMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {getLabel(
                            SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE,
                            locale)});
            }
            List<SpsMMiscDomain> plantSupplierDomainMiscList = new ArrayList<SpsMMiscDomain>();
            for (PlantSupplierDomain company : plantSupplierDomainList) {
                SpsMMiscDomain companyCode = new SpsMMiscDomain();
                companyCode.setMiscCd(company.getSPcd());
                companyCode.setMiscValue(company.getSPcd());
                plantSupplierDomainMiscList.add(companyCode);
            }
            fileUploadResultDomain.setPlantSupplierList(plantSupplierDomainMiscList);
            
            /** Get DENSO company information from DensoCompanyService. */
            PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
            plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            List<CompanyDensoDomain> companyDensoDomain = null;
            companyDensoDomain = densoCompanyService
                .searchCompanyDenso(plantDensoWithScopeDomain);
            if (null == companyDensoDomain
                || Constants.ZERO == companyDensoDomain.size()) {
                MessageUtil
                    .throwsApplicationMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {getLabel(
                            SupplierPortalConstant.LBL_DENSO_COMPANY_CODE,
                            locale)});
            }
            MiscellaneousDomain miscDomainDenso = null;
            List<MiscellaneousDomain> companyDensoList = new ArrayList<MiscellaneousDomain>();
            for (CompanyDensoDomain company : companyDensoDomain) {
                miscDomainDenso = new MiscellaneousDomain();
                miscDomainDenso.setMiscCode(company.getDCd());
                miscDomainDenso.setMiscValue(company.getDCd());
                companyDensoList.add(miscDomainDenso);
            }
            fileUploadResultDomain.setCompanyDensoList(companyDensoList);

            /** Get list of DENSO plant information from DENSOPlantService. */
            List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
            plantDensoDomain = plantDensoService
                .searchPlantDenso(plantDensoWithScopeDomain);
            if (null == plantDensoDomain
                || plantDensoDomain.size() <= Constants.ZERO) {
                MessageUtil.throwsApplicationMessage(
                    locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {getLabel(
                        SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
            }
            fileUploadResultDomain.setPlantDensoList(plantDensoDomain);
            
        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception exception) {
            throw new ApplicationException(exception.getMessage());
        }
        return fileUploadResultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNDownloadingFacadeService#searchFileName
     *      (com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public FileManagementDomain searchFileName(FileUploadDomain fileUploadDomain)
        throws ApplicationException {

        Locale locale = fileUploadDomain.getLocale();
        FileManagementDomain resultDomain = null;

        try {
            resultDomain = fileManagementService.searchFileDownload(
                fileUploadDomain.getFileId(), false, null);

        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                SupplierPortalConstant.ERROR_CD_SP_90_0001);
        }

        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNDownloadingFacadeService
     *      #searchFileDownloadCsv
     *      (com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public void searchFileDownloadCsv(FileUploadDomain fileUploadDomain,
        OutputStream output) throws ApplicationException {

        Locale locale = fileUploadDomain.getLocale();

        try {
            fileManagementService.searchFileDownload(
                fileUploadDomain.getFileId(), true, output);
            output.flush();
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @throws Exception
     * @see com.globaldenso.asia.sps.business.service.CNDNDownloadingFacadeService
     *      #searchFileUploadInformation
     *      (com.globaldenso.asia.sps.business.domain.FileUploadCriteriaDomain)
     *      (com.globaldenso.asia.sps.business.domain.MiscellaneousDomain)
     */
    public FileUploadResultDomain searchFileDownloadInformation(
        FileUploadCriteriaDomain fileUploadCriteriaDomain) throws Exception {
        Locale locale = fileUploadCriteriaDomain.getLocale();
        List<FileUploadForDisplayDomain> fileUploadForDisplayDomainList = 
            new ArrayList<FileUploadForDisplayDomain>();
        FileUploadResultDomain fileUploadResult = new FileUploadResultDomain();
        FileUploadDomain fileUploadDomainCriteria = new FileUploadDomain();
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();

        List<ApplicationMessageDomain> errorMessageList = 
            validateCriteria(fileUploadCriteriaDomain);

        if (Constants.ZERO < errorMessageList.size()) {
            fileUploadResult.setErrorMessageList(errorMessageList);
            return fileUploadResult;
        }

        int recordCount = Constants.ZERO;

        if (Constants.ZERO < errorMessageList.size()) {
            fileUploadResult.setErrorMessageList(errorMessageList);
            return fileUploadResult;
        }

        String dateform = fileUploadCriteriaDomain.getUpdateDateFrom();

        String dateTo = fileUploadCriteriaDomain.getUpdateDateTo();

        fileUploadDomainCriteria
            .setCreateDatetimeGreaterThanEqual(dateform);
        fileUploadDomainCriteria.setCreateDatetimeLessThanEqual(dateTo);
        fileUploadDomainCriteria.setVendorCd(fileUploadCriteriaDomain
            .getVendorCd());
        fileUploadDomainCriteria
            .setSPcd(fileUploadCriteriaDomain.getSPcd());
        
        fileUploadDomainCriteria.setDCd(fileUploadCriteriaDomain.getDCd());
        fileUploadDomainCriteria.setDPcd(fileUploadCriteriaDomain.getDPcd());
      
        if(!StringUtil.checkNullOrEmpty(fileUploadCriteriaDomain.getFileName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String fileName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                fileUploadCriteriaDomain.getFileName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            fileUploadDomainCriteria.setFileName(fileName);
        }

        fileUploadDomainCriteria.setDensoAuthenList(
            fileUploadCriteriaDomain.getDensoAuthenList());
        fileUploadDomainCriteria.setSupplierAuthenList(
            fileUploadCriteriaDomain.getSupplierAuthenList());
        
        int record = fileUploadService
            .searchCount(fileUploadDomainCriteria);
        if (Constants.ZERO == record) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        } else {
            recordCount = record;
        }
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV007_RLM);
        MiscellaneousDomain recordLimit = recordLimitService
            .searchRecordLimit(miscDomain);
        if (null == recordLimit) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        miscDomain
            .setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV007_PLM);
        MiscellaneousDomain recordLimitPerPage = recordLimitService
            .searchRecordLimitPerPage(miscDomain);
        if (null == recordLimitPerPage) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        if (Integer.valueOf(recordLimit.getMiscValue()) < recordCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {recordLimit.getMiscValue()});
        }

        fileUploadCriteriaDomain.setMaxRowPerPage(Integer
            .parseInt(recordLimitPerPage.getMiscValue()));

        SpsPagingUtil.calcPaging(fileUploadCriteriaDomain, recordCount);

        fileUploadDomainCriteria.setRowNumFrom(fileUploadCriteriaDomain
            .getRowNumFrom());
        fileUploadDomainCriteria.setRowNumTo(fileUploadCriteriaDomain
            .getRowNumTo());
        fileUploadDomainCriteria.
            setPreferredOrder(SupplierPortalConstant.CN_DN_DOWNLOADING_SQL);
        List<SpsTFileUploadDomain> spsTFileUploadDomainList = fileUploadService
            .searchFileUploadInformation(fileUploadDomainCriteria);
        fileUploadForDisplayDomainList = new ArrayList<FileUploadForDisplayDomain>();
        FileUploadForDisplayDomain fileForDisplay = null;

        for (SpsTFileUploadDomain fileUpload : spsTFileUploadDomainList) {
            fileForDisplay = new FileUploadForDisplayDomain();

            FileUploadDomain fileUploadDomain = new FileUploadDomain();
            fileUploadDomain.setVendorCd(fileUpload.getVendorCd());
            fileUploadDomain.setSPcd(fileUpload.getSPcd());
            fileUploadDomain.setFileName(fileUpload.getFileName());
            fileUploadDomain.setDCd(fileUpload.getDCd());
            fileUploadDomain.setDPcd(fileUpload.getDPcd());

            String date_to_string = DateUtil.format(
                fileUpload.getCreateDatetime(),
                DateUtil.PATTERN_YYYYMMDD_SLASH);
            fileUploadDomain.setLastUpdateDate(date_to_string);
            fileUploadDomain.setComment(fileUpload.getFileComment());
            fileUploadDomain.setFileId(fileUpload.getFileId());
            
            fileForDisplay.setFileUploadDomain(fileUploadDomain);
            fileUploadForDisplayDomainList.add(fileForDisplay);

        }
        fileUploadResult
            .setFileUploadForDisplayDomainList(fileUploadForDisplayDomainList);

        return fileUploadResult;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService
     *      #searchSelectedCompanySupplier
     *      (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<SpsMMiscDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {
        Locale locale = plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getLocale();
        List<SpsMMiscDomain> plantSupplierDomainMiscList = new ArrayList<SpsMMiscDomain>();

        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(
                plantSupplierWithScopeDomain.getDataScopeControlDomain());

        if (null == densoSupplierRelationDomainList) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(
            Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
        List<PlantSupplierDomain> plantSupplierDomainList = plantSupplierService
            .searchPlantSupplier(plantSupplierWithScopeDomain);

        if (null == plantSupplierDomainList 
            || Constants.ZERO == plantSupplierDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE,
                    locale)});
        }

        for (PlantSupplierDomain company : plantSupplierDomainList) {
            SpsMMiscDomain companyCode = new SpsMMiscDomain();
            companyCode.setMiscCd(company.getSPcd());
            companyCode.setMiscValue(company.getSPcd());
            plantSupplierDomainMiscList.add(companyCode);
        }

        return plantSupplierDomainMiscList;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService
     *      #searchSelectedPlantSupplier
     *      (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {
        Locale locale = plantSupplierWithScopeDomain.getDataScopeControlDomain().getLocale();

        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(
                plantSupplierWithScopeDomain.getDataScopeControlDomain());

        if (null == densoSupplierRelationDomainList) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(
            Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        List<CompanySupplierDomain> companySupplierDomain = null;
        companySupplierDomain = companySupplierService
            .searchCompanySupplier(plantSupplierWithScopeDomain);
        if (null == companySupplierDomain
            || Constants.ZERO == companySupplierDomain.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE,
                    locale)});
        }

        return companySupplierDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CnDnDownloadingFacadeService#searchSelectedCompanyDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        Locale locale = plantDensoWithScopeDomain.getLocale();

        /** Get relation between DENSO and Supplier. */
        List<DensoSupplierRelationDomain> densoSupplierRelation
            = densoSupplierRelationService.searchDensoSupplierRelation(
                plantDensoWithScopeDomain.getDataScopeControlDomain());

        if (null == densoSupplierRelation) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        if(!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelation);

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
        plantDensoDomain = plantDensoService
            .searchPlantDenso(plantDensoWithScopeDomain);
        if (null == plantDensoDomain || plantDensoDomain.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }

        return plantDensoDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CnDnDownloadingFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        Locale locale = plantDensoWithScopeDomain.getLocale();

        /** Get relation between DENSO and Supplier. */
        List<DensoSupplierRelationDomain> densoSupplierRelation
            = densoSupplierRelationService.searchDensoSupplierRelation(
                plantDensoWithScopeDomain.getDataScopeControlDomain());

        if (null == densoSupplierRelation) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        if(!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelation);

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<CompanyDensoDomain> companyDensoDomain = null;
        companyDensoDomain = densoCompanyService
            .searchCompanyDenso(plantDensoWithScopeDomain);
        if (null == companyDensoDomain
            || Constants.ZERO == companyDensoDomain.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }

        return companyDensoDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }

    /**
     * Get validate criteria.
     * 
     * @param fileUploadCriteriaDomain the file upload criteria domain
     * @return list of application message domain
     * @throws Exception Exception
     */
    private List<ApplicationMessageDomain> validateCriteria(
        FileUploadCriteriaDomain fileUploadCriteriaDomain) throws Exception {
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = fileUploadCriteriaDomain.getLocale();
        String updateDateFrom = null;
        String updateDateTo = null;
        boolean hasUpdateDateFrom = false;
        boolean hasUpdateDateTo = false;

        if (Strings.judgeBlank(fileUploadCriteriaDomain.getUpdateDateFrom())
            && Strings.judgeBlank(fileUploadCriteriaDomain.getUpdateDateTo())
            && Strings.judgeBlank(fileUploadCriteriaDomain.getVendorCd())
            && Strings.judgeBlank(fileUploadCriteriaDomain.getSPcd())
            && Strings.judgeBlank(fileUploadCriteriaDomain.getFileName())) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil
                    .getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        } else {
            if (fileUploadCriteriaDomain.getVendorCd().equals(
                Constants.MISC_CODE_ALL)) {
                fileUploadCriteriaDomain.setVendorCd(Constants.EMPTY_STRING);
            }
            if (fileUploadCriteriaDomain.getSPcd().equals(
                Constants.MISC_CODE_ALL)) {
                fileUploadCriteriaDomain.setSPcd(Constants.EMPTY_STRING);
            }
            if (fileUploadCriteriaDomain.getDCd().equals(
                Constants.MISC_CODE_ALL)) {
                fileUploadCriteriaDomain.setDCd(Constants.EMPTY_STRING);
            }
            if (fileUploadCriteriaDomain.getDPcd().equals(
                Constants.MISC_CODE_ALL)) {
                fileUploadCriteriaDomain.setDPcd(Constants.EMPTY_STRING);
            }
        }
        if (!StringUtil.checkNullOrEmpty(fileUploadCriteriaDomain
            .getUpdateDateFrom())) {
            updateDateFrom = fileUploadCriteriaDomain.getUpdateDateFrom();
            if (!DateUtil.isValidDate(updateDateFrom,
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getErrorMessage(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            SupplierPortalConstant.ERROR_CD_SP_90_0002,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_UPLOAD_DATE_FROM,
                                locale)})));
                hasUpdateDateFrom = false;
            } else {
                hasUpdateDateFrom = true;
            }
        }
        if (!StringUtil.checkNullOrEmpty(fileUploadCriteriaDomain
            .getUpdateDateTo())) {
            updateDateTo = fileUploadCriteriaDomain.getUpdateDateTo();
            if (!DateUtil.isValidDate(updateDateTo,
                DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getErrorMessage(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                            SupplierPortalConstant.ERROR_CD_SP_90_0002,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_UPLOAD_DATE_TO,
                                locale)})));
                hasUpdateDateTo = false;
            } else {
                hasUpdateDateTo = true;
            }
        }
        if (hasUpdateDateFrom && hasUpdateDateTo) {
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            try {
                dateFrom.setTime(DateUtil.parseToUtilDate(updateDateFrom,
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
                dateTo.setTime(DateUtil.parseToUtilDate(updateDateTo,
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            } catch (Exception e) {
                throw new ApplicationException(e.getMessage());
            }
            if (Constants.ZERO < DateUtil.compareDate(updateDateFrom,
                updateDateTo)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(
                                SupplierPortalConstant.LBL_UPLOAD_DATE_FROM,
                                locale),
                            getLabel(SupplierPortalConstant.LBL_UPLOAD_DATE_TO,
                                locale)})));

            }
        }
        if (!StringUtil
            .checkNullOrEmpty(fileUploadCriteriaDomain.getFileName())) {
            if (Constants.MAX_TEXTBOX_LENGTH < fileUploadCriteriaDomain
                .getFileName().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_FILE_NAME,
                                locale),
                            String.valueOf(Constants.MAX_TEXTBOX_LENGTH)})));
            } else {
                fileUploadCriteriaDomain.setFileName(fileUploadCriteriaDomain
                    .getFileName().trim().toUpperCase());
            }
        }
        return errorMessageList;
    }

    /**
     * Get Label method.
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {

        String labelString = MessageUtil.getLabelHandledException(locale, key);
        return labelString;
    }
}
