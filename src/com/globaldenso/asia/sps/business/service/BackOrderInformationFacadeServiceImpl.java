/*
 * ModifyDate Development company     Describe 
 * 2014/08/26 CSI Karnrawee           Create
 * 2015/07/15 CSI Akat                [IN004]
 * 2015/12/09 CSI Akat                [IN042]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The class BackOrderInformationFacadeServiceImpl.</p>
 * <p>Facade for BackOrderInformationFacadeServiceImpl.</p>
 * <ul>
 * <li>Method search   : searchInitial</li>
 * <li>Method search   : searchBackOrderInformation</li>
 * <li>Method search   : searchBackOrderInformationCsv</li>
 * <li>Method search   : searchSelectedCompanySupplier</li>
 * <li>Method search   : searchSelectedCompanyDenso</li>
 * <li>Method search   : searchFileName</li>
 * <li>Method search   : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public class BackOrderInformationFacadeServiceImpl implements BackOrderInformationFacadeService {

    /** The deliveryOrder service. */
    private DeliveryOrderService deliveryOrderService = null;
    
    /** The misc service. */
    private MiscellaneousService miscService = null;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The file management service. */
    private FileManagementService fileManagementService = null;
    
    /** The Denso Supplier Relation service. */
    private DensoSupplierRelationService densoSupplierRelationService = null; 
    
    /** The Supplier company service. */
    private CompanySupplierService companySupplierService = null;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The Plant Supplier Service. */
    private PlantSupplierService plantSupplierService = null; 
    
    /** The Plant Denso Service. */
    private PlantDensoService plantDensoService = null;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;

    // [IN004] : create link in every case, this service not use
    ///** The service for SPS_T_ASN_DETAIL. */
    //private SpsTAsnDetailService spsTAsnDetailService;
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public BackOrderInformationFacadeServiceImpl(){
        super();
    }

    /**
     * Set the delivery order Service.
     * 
     * @param deliveryOrderService the delivery order service to set
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }
    
    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Sets the common service.
     * 
     * @param commonService the new common service
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Set the plant supplier service.
     * 
     * @param plantSupplierService the plant supplier service to set
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }
    
    /**
     * Set the plant denso service.
     * 
     * @param plantDensoService the plant denso service to set
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    // [IN004] : create link in every case, this service not use
    ///**
    // * <p>Setter method for spsTAsnDetailService.</p>
    // *
    // * @param spsTAsnDetailService Set for spsTAsnDetailService
    // */
    //public void setSpsTAsnDetailService(SpsTAsnDetailService spsTAsnDetailService) {
    //    this.spsTAsnDetailService = spsTAsnDetailService;
    //}
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchInitial(
     * com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public BackOrderInformationReturnDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain)throws ApplicationException {
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        MiscellaneousDomain transportModeCbCriteria = null;
        
        Locale locale = dataScopeControlDomain.getLocale();
        BackOrderInformationReturnDomain backOrderInformationReturn 
            = new BackOrderInformationReturnDomain();
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(null == densoSupplierRelationList || densoSupplierRelationList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                Constants.DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
        
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanySupplierDomain> companySupplierList
            = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if(null == companySupplierList || companySupplierList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantSupplierDomain> plantSupplierList
            = this.plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        if(null == plantSupplierList || plantSupplierList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoList
            = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == companyDensoList || companyDensoList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantDensoDomain> plantDensoList
            = this.plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null ==  plantDensoList || plantDensoList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        transportModeCbCriteria = new MiscellaneousDomain();
        transportModeCbCriteria.setMiscType(SupplierPortalConstant.MISC_TYPE_TRANS_CB);
        List<MiscellaneousDomain> transportModeCbList
            = miscService.searchMisc(transportModeCbCriteria);
        if(null == transportModeCbList || Constants.ZERO == transportModeCbList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_TRANSPORTATION_MODE);
        }
        
        backOrderInformationReturn.setDensoSupplierRelationList(densoSupplierRelationList);
        backOrderInformationReturn.setCompanySupplierList(companySupplierList);
        backOrderInformationReturn.setPlantSupplierList(plantSupplierList);
        backOrderInformationReturn.setCompanyDensoList(companyDensoList);
        backOrderInformationReturn.setPlantDensoList(plantDensoList);
        backOrderInformationReturn.setTransportModeCbList(transportModeCbList);
        return backOrderInformationReturn;
    }
    

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchBackOrderInformation(
     * com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    public BackOrderInformationReturnDomain searchBackOrderInformation(
        BackOrderInformationDomain backOrderInformationCriteria)
        throws ApplicationException {
        
        Locale locale = backOrderInformationCriteria.getLocale();
        BackOrderInformationReturnDomain backOrderInformationReturn 
            = new BackOrderInformationReturnDomain();
        
        List<ApplicationMessageDomain> errorMessageList 
            = this.validateCriteria(backOrderInformationCriteria);
        
        if(null != errorMessageList && Constants.ZERO < errorMessageList.size()){
            backOrderInformationReturn.setErrorMessageList(errorMessageList);
        }else{
            Integer recordLimit = Constants.ZERO;
            Integer recordLimitPerPage = Constants.ZERO;
            
            List<BackOrderInformationDomain> backOrderInformationList
                = this.mergeBackOrderInformation(backOrderInformationCriteria, recordLimit);
            
            MiscellaneousDomain miscLimitPerPageDomain = new MiscellaneousDomain();
            miscLimitPerPageDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            miscLimitPerPageDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP007_PLM);
            recordLimitPerPage = miscService.searchMiscValue(miscLimitPerPageDomain);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032);
            }
            backOrderInformationCriteria.setMaxRowPerPage(recordLimitPerPage);
            
            if(Constants.ZERO < backOrderInformationList.size()){
                SpsPagingUtil.calcPaging(backOrderInformationCriteria, 
                    backOrderInformationList.size());
                backOrderInformationReturn.setBackOrderInformationList(
                    backOrderInformationList);
            }
        }
        return backOrderInformationReturn;
    }
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchBackOrderInformationCsv(
     * com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    public BackOrderInformationReturnDomain searchBackOrderInformationCsv(
        BackOrderInformationDomain backOrderInformationCriteria) throws ApplicationException {
        
        Locale locale = backOrderInformationCriteria.getLocale();
        BackOrderInformationReturnDomain backOrderInformationReturn 
            = new BackOrderInformationReturnDomain();
        
        Integer recordLimit = Constants.ZERO;
        List<ApplicationMessageDomain> errorMessageList = null;
        List<BackOrderInformationDomain> backOrderInformationForDisplay = null;
        
        errorMessageList = this.validateCriteria(backOrderInformationCriteria);
        if(null != errorMessageList && Constants.ZERO < errorMessageList.size()){
            backOrderInformationReturn.setErrorMessageList(errorMessageList);
        }else{
            
            String fileName = StringUtil.appendsString(
                Constants.BACK_ORDER_INFORMATION_LIST,
                Constants.SYMBOL_UNDER_SCORE,
                DateUtil.format(this.commonService.searchSysDate(), 
                    DateUtil.PATTERN_YYYYMMDD_HHMM));
            
            backOrderInformationForDisplay = this.mergeBackOrderInformation(
                backOrderInformationCriteria, recordLimit);
            
            final String[] headerArr = new String[] {
                SupplierPortalConstant.LBL_ISSUE_DATE,
                SupplierPortalConstant.LBL_S_CD,
                SupplierPortalConstant.LBL_S_PCD,
                SupplierPortalConstant.LBL_D_CD,
                SupplierPortalConstant.LBL_D_PCD,
                SupplierPortalConstant.LBL_DELIVERY_DATE,
                SupplierPortalConstant.LBL_DELIVERY_TIME,
                SupplierPortalConstant.LBL_CURRENT_SPS_DO_NO,
                SupplierPortalConstant.LBL_REVISION,
                SupplierPortalConstant.LBL_CYCLE,
                SupplierPortalConstant.LBL_ROUTE,
                SupplierPortalConstant.LBL_DEL,
                SupplierPortalConstant.LBL_CTRL_NO,
                SupplierPortalConstant.LBL_D_PN,
                SupplierPortalConstant.LBL_ITEM_DESC,
                SupplierPortalConstant.LBL_S_PN,
                SupplierPortalConstant.LBL_UNIT_OF_MEASURE,
                SupplierPortalConstant.LBL_ORDER_METHOD,
                SupplierPortalConstant.LBL_TM,
                SupplierPortalConstant.LBL_ORDER_QTY,
                SupplierPortalConstant.LBL_QTY_BOX,
                SupplierPortalConstant.LBL_DOCKCODE,
                SupplierPortalConstant.LBL_RECEIVING_LANE,
                SupplierPortalConstant.LBL_WH_PRIME_RECEIVING,
                SupplierPortalConstant.LBL_CURRENT_CIGMA_DO_NO,
                SupplierPortalConstant.LBL_TOTAL_SHIPPING_QTY,
                SupplierPortalConstant.LBL_TOTAL_RECEIVED_QTY,
                SupplierPortalConstant.LBL_TOTAL_IN_TRANSIT_QTY,
                SupplierPortalConstant.LBL_TOTAL_BACK_ORDER_QTY,
                SupplierPortalConstant.LBL_PN_SHIP_STATUS
            };
            
            List<Map<String, Object>> resultDetail = this.getBackOrderInformationMap(
                backOrderInformationForDisplay, headerArr);
            CommonDomain commonDomain = new CommonDomain();
            commonDomain.setLocale(locale);
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            try {
                backOrderInformationReturn.setCsvResult(
                    this.commonService.createCsvString(commonDomain));
                backOrderInformationReturn.setFileName(fileName);
            
            } catch (IOException ioe) {
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012);
            }
        }
        return backOrderInformationReturn;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchSelectedCompanySupplier(
     * com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)throws ApplicationException {
        
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<PlantSupplierDomain> companySupplierPlantList = null;

        DataScopeControlDomain dataScopeControlDomain = plantSupplierWithScopeDomain
            .getDataScopeControlDomain();
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companySupplierPlantList = this.plantSupplierService.searchPlantSupplier(
            plantSupplierWithScopeDomain);
        if(Constants.ZERO == companySupplierPlantList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        return companySupplierPlantList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchSelectedCompanyDenso(
     * com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException {
        
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<PlantDensoDomain> companyDensoPlantList = null;
        
        DataScopeControlDomain dataScopeControlDomain = plantDensoWithScopeDomain
            .getDataScopeControlDomain();
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoPlantList = this.plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(Constants.ZERO == companyDensoPlantList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        return companyDensoPlantList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope) throws ApplicationException
    {
        Locale locale = plantSupplierWithScope.getLocale();
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantSupplierWithScope.getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain = plantSupplierWithScope.getPlantSupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if(result.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        return result;
    }
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScope.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if (result.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchFileName(
     * com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    public FileManagementDomain searchFileName(BackOrderInformationDomain backOrderInformation) 
        throws ApplicationException {
        
        Locale locale = backOrderInformation.getLocale();
        FileManagementDomain resultDomain = null;
        
        try {
            resultDomain = this.fileManagementService.searchFileDownload(
                backOrderInformation.getFileId(), false, null);
            
        } catch (IOException e) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchFileDownload(
     * com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain,
     * OutputStream)
     */
    public void searchLegendInfo(BackOrderInformationDomain backOrderInformation, 
        OutputStream output) throws ApplicationException {
        
        Locale locale = backOrderInformation.getLocale();
        
        try {
            this.fileManagementService.searchFileDownload(backOrderInformation.getFileId(),
                true, output);
            
        }catch (IOException e) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * <p>Get back order information map.</p>
     * <ul>
     * <li>Preparation data for create CSV file.</li>
     * </ul>
     * 
     * @param backOrderInformationList the List of Back Order Information Domain
     * @param header the array of string
     * @return List<Map<String, Object>>
     */
    private List<Map<String, Object>> getBackOrderInformationMap(
        List<BackOrderInformationDomain> backOrderInformationList, String[] header){
        String issueDate = null;
        String itemDesc = null;
        List<Map<String, Object>> backOrderInformationeList 
            = new ArrayList<Map<String, Object>>();
        
        Map<String, Object>  backOrderInformationeMap = null;
        for(BackOrderInformationDomain record :backOrderInformationList){
            issueDate = Constants.EMPTY_STRING;
            itemDesc = Constants.EMPTY_STRING;
            
            backOrderInformationeMap = new HashMap<String, Object>();
            SpsTDoDomain doDomain = record.getSpsTDoDomain();
            SpsTDoDetailDomain doDetailDomain = record.getSpsTDoDetailDomain();
            
            if(null != doDomain.getDoIssueDate()){
                issueDate = DateUtil.format(
                    doDomain.getDoIssueDate(), DateUtil.PATTERN_YYYYMMDD_SLASH);
            }
            
            if(!StringUtil.checkNullOrEmpty(doDetailDomain.getItemDesc())){
                itemDesc = StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE,
                    doDetailDomain.getItemDesc(), Constants.SYMBOL_DOUBLE_QUOTE);
            }
            
            backOrderInformationeMap.put(header[Constants.ZERO], issueDate);
            backOrderInformationeMap.put(header[Constants.ONE], doDomain.getVendorCd());
            backOrderInformationeMap.put(header[Constants.TWO],
                StringUtil.checkNullToEmpty(doDomain.getSPcd()));
            backOrderInformationeMap.put(header[Constants.THREE],
                StringUtil.checkNullToEmpty(doDomain.getDCd()));
            backOrderInformationeMap.put(header[Constants.FOUR],
                StringUtil.checkNullToEmpty(doDomain.getDPcd()));
            backOrderInformationeMap.put(header[Constants.FIVE], 
                DateUtil.format(doDomain.getDeliveryDatetime(), DateUtil.PATTERN_YYYYMMDD_SLASH));
            backOrderInformationeMap.put(header[Constants.SIX], 
                DateUtil.format(doDomain.getDeliveryDatetime(), DateUtil.PATTERN_HHMM_COLON));
            backOrderInformationeMap.put(header[Constants.SEVEN], doDomain.getCurrentSpsDoNo());
            backOrderInformationeMap.put(header[Constants.EIGHT], doDomain.getRevision());
            backOrderInformationeMap.put(header[Constants.NINE],
                StringUtil.checkNullToEmpty(doDomain.getCycle()));
            backOrderInformationeMap.put(header[Constants.TEN],
                StringUtil.checkNullToEmpty(doDomain.getTruckRoute()));
            backOrderInformationeMap.put(header[Constants.ELEVEN],
                StringUtil.checkNullToEmpty(doDomain.getTruckSeq()));
            backOrderInformationeMap.put(header[Constants.TWELVE],
                StringUtil.checkNullToEmpty(doDetailDomain.getCtrlNo()));
            backOrderInformationeMap.put(header[Constants.THIRTEEN], doDetailDomain.getDPn());
            backOrderInformationeMap.put(header[Constants.FOURTEEN], itemDesc);
            backOrderInformationeMap.put(header[Constants.FIFTEEN], doDetailDomain.getSPn());
            backOrderInformationeMap.put(header[Constants.SIXTEEN],
                doDetailDomain.getUnitOfMeasure());
            backOrderInformationeMap.put(header[Constants.SEVENTEEN], doDomain.getOrderMethod());
            backOrderInformationeMap.put(header[Constants.EIGHTEEN],
                StringUtil.checkNullToEmpty(doDomain.getTm()));
            backOrderInformationeMap.put(header[Constants.NINETEEN],
                doDetailDomain.getCurrentOrderQty().toString());
            backOrderInformationeMap.put(header[Constants.TWENTY],
                doDetailDomain.getQtyBox().toString());
            backOrderInformationeMap.put(header[Constants.TWENTY_ONE],
                StringUtil.checkNullToEmpty(doDomain.getDockCode()));
            backOrderInformationeMap.put(header[Constants.TWENTY_TWO],
                StringUtil.checkNullToEmpty(doDetailDomain.getRcvLane()));
            backOrderInformationeMap.put(header[Constants.TWENTY_THREE],
                StringUtil.checkNullToEmpty(doDomain.getWhPrimeReceiving()));
            
            /* [IN042] : Current CIGMA D/O No. is CHG CIGMA D/O No. in DO_DETAIL (if empty use
             * CIGMA_DO_NO)
             * */ 
            //backOrderInformationeMap.put(header[Constants.TWENTY_FOUR],
            //    StringUtil.checkNullToEmpty(doDomain.getCurrentCigmaDoNo()));
            backOrderInformationeMap.put(header[Constants.TWENTY_FOUR],
                StringUtil.checkNullToEmpty(doDomain.getCigmaDoNo()));
            if (!Strings.judgeBlank(doDetailDomain.getChgCigmaDoNo())) {
                backOrderInformationeMap.put(header[Constants.TWENTY_FOUR],
                    StringUtil.checkNullToEmpty(doDetailDomain.getChgCigmaDoNo()));
            }
            
            backOrderInformationeMap.put(header[Constants.TWENTY_FIVE], 
                record.getSpsTAsnDetailDomain().getShippingRoundQty());
            backOrderInformationeMap.put(header[Constants.TWENTY_SIX], 
                StringUtil.checkNullToEmpty(record.getBdTotalReceivedQty()));
            backOrderInformationeMap.put(header[Constants.TWENTY_SEVEN], 
                StringUtil.checkNullToEmpty(StringUtil.removeComma(record.getTotalInTransitQty())));
            backOrderInformationeMap.put(header[Constants.TWENTY_EIGHT], 
                StringUtil.checkNullToEmpty(StringUtil.removeComma(record.getTotalBackOrderQty())));
            backOrderInformationeMap.put(header[Constants.TWENTY_NINE], 
                doDetailDomain.getPnShipmentStatus());
            backOrderInformationeList.add(backOrderInformationeMap);
        }
        return backOrderInformationeList;
    }
    
    
    /**
     * <p>Validate criteria.</p>
     * <ul>
     * <li>Validation criteria data before search data.</li>
     * </ul>
     * 
     * @param backOrderInformationCriteria the Back Order Information Domain
     * @return the List of Application Message Domain
     */
    private List<ApplicationMessageDomain> validateCriteria(
        BackOrderInformationDomain backOrderInformationCriteria){
        
        Locale locale = backOrderInformationCriteria.getLocale();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        
        String deliveryDateFrom = null;
        String deliveryDateTo = null;
        boolean hasDeliveryDateFrom = false;
        boolean hasDeliveryDateTo = false;
                
        if(StringUtil.checkNullOrEmpty(backOrderInformationCriteria.getDeliveryDateFrom())
            || StringUtil.checkNullOrEmpty(backOrderInformationCriteria.getDeliveryDateTo())
            || StringUtil.checkNullOrEmpty(backOrderInformationCriteria.getVendorCd())
            || StringUtil.checkNullOrEmpty(backOrderInformationCriteria.getSPcd())
            || StringUtil.checkNullOrEmpty(backOrderInformationCriteria.getDCd())
            || StringUtil.checkNullOrEmpty(backOrderInformationCriteria.getDPcd())){
            
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        }
        
        if(Constants.MISC_CODE_ALL.equals(backOrderInformationCriteria.getVendorCd())){
            backOrderInformationCriteria.setVendorCd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(backOrderInformationCriteria.getSPcd())){
            backOrderInformationCriteria.setSPcd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(backOrderInformationCriteria.getDCd())){
            backOrderInformationCriteria.setDCd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(backOrderInformationCriteria.getDPcd())){
            backOrderInformationCriteria.setDPcd(null);
        }
        
        if(!Strings.judgeBlank(backOrderInformationCriteria.getDeliveryDateFrom())){
            deliveryDateFrom = backOrderInformationCriteria.getDeliveryDateFrom();
            if(!DateUtil.isValidDate(deliveryDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_DATE_FROM)));
            }else{
                hasDeliveryDateFrom = true;
            }
        }
        if(!Strings.judgeBlank(backOrderInformationCriteria.getDeliveryDateTo())){
            deliveryDateTo = backOrderInformationCriteria.getDeliveryDateTo();
            if(!DateUtil.isValidDate(deliveryDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_DATE_TO)));
            }
            else{
                hasDeliveryDateTo = true;
            }
        }
        if(hasDeliveryDateFrom && hasDeliveryDateTo){
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            
            dateFrom.setTime(DateUtil.parseToSqlDate
                (deliveryDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH));
            dateTo.setTime(DateUtil.parseToSqlDate
                (deliveryDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH));
            
            if(dateTo.before(dateFrom)){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_DELIVERY_DATE_TO),
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_DELIVERY_DATE_FROM)})));
            }
        }
        
        if(!Strings.judgeBlank(backOrderInformationCriteria.getSpsDoNo())){
            if(Constants.MAX_SPS_DO_NO < backOrderInformationCriteria.getSpsDoNo().length()){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                        new String[]{MessageUtil.getLabelHandledException(
                            locale, SupplierPortalConstant.LBL_DO_NO),
                            String.valueOf(Constants.MAX_SPS_DO_NO)})));
            }
        }
        if(!Strings.judgeBlank(backOrderInformationCriteria.getAsnNo())){
            if(Constants.MAX_ASN_NO_LENGTH < backOrderInformationCriteria.getAsnNo().length()){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                        new String[]{MessageUtil.getLabelHandledException(
                            locale, SupplierPortalConstant.LBL_ASN_NO),
                            String.valueOf(Constants.MAX_ASN_NO_LENGTH)})));
            }
        }
        if(!Strings.judgeBlank(backOrderInformationCriteria.getDPn())){
            if(Constants.MAX_D_PART_NO_LENGTH 
                < backOrderInformationCriteria.getDPn().length()){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_DENSO_PART_NO),
                            String.valueOf(Constants.MAX_D_PART_NO_LENGTH)})));
            }
        }
        if(!Strings.judgeBlank(backOrderInformationCriteria.getSPn())){
            if(Constants.MAX_S_PART_NO_LENGTH 
                < backOrderInformationCriteria.getSPn().length()){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_SUPPLIER_PART_NO),
                            String.valueOf(Constants.MAX_S_PART_NO_LENGTH)})));
            }
        }
        
        if(Constants.ZERO < errorMessageList.size()){
            return errorMessageList;
        }else{
            return null;
        }
    }
    
    /**
     * <p>Merge back oder information.</p>
     * <ul>
     * <li>Merging back oder information data whit CIGMA data.</li>
     * </ul>
     * 
     * @param backOrderInformationCriteria the Back Order Information Domain
     * @param recordLimit of integer
     * @return List<Map<String, Object>>
     * @throws ApplicationException ApplicationException
     */
    private List<BackOrderInformationDomain> mergeBackOrderInformation(
        BackOrderInformationDomain backOrderInformationCriteria, Integer recordLimit)
        throws ApplicationException {
        
        Locale locale = backOrderInformationCriteria.getLocale();
        Integer recordCount = Constants.ZERO;
        
        MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
        miscLimitDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        miscLimitDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP007_RLM);
        recordLimit = miscService.searchMiscValue(miscLimitDomain);
        if(null == recordLimit){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        
        recordCount = this.deliveryOrderService.searchCountBackOrder(backOrderInformationCriteria);
        if(Constants.ZERO == recordCount){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }else if(recordLimit < recordCount){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                new String[]{String.valueOf(recordLimit)});
        }
        
        List<BackOrderInformationDomain> backOrderInformationList;
        backOrderInformationList = this.deliveryOrderService.searchBackOrder(
            backOrderInformationCriteria);
        if(null == backOrderInformationList || backOrderInformationList.isEmpty()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }
        
        Map<String, List<BackOrderInformationDomain>> backOrderResultMap 
            = new HashMap<String, List<BackOrderInformationDomain>>();
        Map<String, Set<String>> asnNoMap = new HashMap<String, Set<String>>();
        for(BackOrderInformationDomain backOrderItem : backOrderInformationList){
            
            String densoCode = backOrderItem.getSpsTDoDomain().getDCd();
            if(backOrderResultMap.containsKey(densoCode)){
                backOrderResultMap.get(densoCode).add(backOrderItem);
            }else{
                List<BackOrderInformationDomain> backOrderInformationListMap 
                    = new ArrayList<BackOrderInformationDomain>();
                backOrderInformationListMap.add(backOrderItem);
                backOrderResultMap.put(densoCode, backOrderInformationListMap);
            }
            
            if(null != backOrderItem.getSpsTAsnDomain()){
                String asnNo = backOrderItem.getSpsTAsnDomain().getAsnNo();
                if(asnNoMap.containsKey(densoCode)){
                    asnNoMap.get(densoCode).add(asnNo);
                }else{
                    Set<String> asnNoList = new TreeSet<String>();
                    asnNoList.add(asnNo);
                    asnNoMap.put(densoCode, asnNoList);
                }
            }
        }
        
        List<BackOrderInformationDomain> backOrderInformationResultList 
            = new ArrayList<BackOrderInformationDomain>();
        if(null == asnNoMap || Constants.ZERO == asnNoMap.size()){
            //All BackOrder information list has not ASN.
            backOrderInformationResultList.addAll(backOrderInformationList);
        }else{
            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(
                backOrderResultMap.keySet());
            SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
                = new SpsMCompanyDensoDomain();
            companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
            List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
                = this.companyDensoService.searchAs400ServerList(
                    companyDensoCriteriaForSearchAS400);
            if(Constants.ZERO == spsMAs400SchemaList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            if(spsMAs400SchemaList.size() != backOrderResultMap.keySet().size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0025,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            
            for(As400ServerConnectionInformationDomain as400Server : spsMAs400SchemaList)
            {
                List<BackOrderInformationDomain> backOrderList = 
                    backOrderResultMap.get(as400Server.getSpsMCompanyDensoDomain().getDCd());
                
                Set<String> asnNoList = asnNoMap.get(
                    as400Server.getSpsMCompanyDensoDomain().getDCd());
                if(null != asnNoList && !asnNoList.isEmpty()){
                    String asnNoStr = StringUtil.convertListToVarcharCommaSeperate(asnNoList);
                    List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
                        CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                            asnNoStr, null, as400Server, locale);
                    
                    Iterator<BackOrderInformationDomain> iterator = backOrderList.iterator();
                    while(iterator.hasNext()){
                        BackOrderInformationDomain backOrderItem = iterator.next();
                        String cigmaCurDoNo
                            = backOrderItem.getSpsTDoDetailDomain().getChgCigmaDoNo();
                        if(StringUtil.checkNullOrEmpty(cigmaCurDoNo)){
                            cigmaCurDoNo = backOrderItem.getSpsTDoDomain().getCigmaDoNo();
                        }
                        
                        BigDecimal totalReceivedQty = new BigDecimal(Constants.ZERO);
                        backOrderItem.setBdTotalReceivedQty(totalReceivedQty);
                        if(null != backOrderItem.getSpsTAsnDomain()){
                            for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList){
                                if(backOrderItem.getSpsTAsnDomain().getAsnNo().trim()
                                    .equals(cigmaAsnitem.getPseudoAsnNo().trim()))
                                {
                                    if(backOrderItem.getSpsTDoDetailDomain().getDPn().trim()
                                        .equals(cigmaAsnitem.getPseudoDPn().trim())
                                        && cigmaCurDoNo.equals(
                                            cigmaAsnitem.getPseudoCigmaCurDoNo().trim()))
                                    {
                                        totalReceivedQty
                                            = new BigDecimal(cigmaAsnitem.getPseudoReceivedQty());
                                        backOrderItem.setBdTotalReceivedQty(totalReceivedQty);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }else{
                    //Current DENSO Company does not have ASN data.
                    Iterator<BackOrderInformationDomain> iterator = backOrderList.iterator();
                    while(iterator.hasNext()){
                        BackOrderInformationDomain backOrderItem = iterator.next();
                        backOrderItem.setBdTotalReceivedQty(new BigDecimal(Constants.ZERO));
                    }
                }
                backOrderInformationResultList.addAll(backOrderList);
            }
//            if(backOrderResultMap.size() != asnNoMap.size()){
//                for(String backOrderKey : backOrderResultMap.keySet()){
//                    if(!asnNoMap.containsKey(backOrderKey)){
//                        backOrderInformationResultList.addAll(backOrderResultMap.get(backOrderKey));
//                    }
//                }
//            }
        }
        
        Map<String, BackOrderInformationDomain> sumQtyMap = 
            new HashMap<String, BackOrderInformationDomain>();
        //This part group data by DO No and Denso Part Number.
        for(BackOrderInformationDomain backOrderItem : backOrderInformationResultList)
        {
            String backOrderKey = StringUtil.appendsString(
                backOrderItem.getSpsTDoDomain().getSpsDoNo().trim(),
                backOrderItem.getSpsTDoDetailDomain().getDPn().trim());
            if(sumQtyMap.containsKey(backOrderKey))
            {
                SpsTAsnDetailDomain asnDetail
                    = sumQtyMap.get(backOrderKey).getSpsTAsnDetailDomain();
                BigDecimal totalShippingQty = asnDetail.getShippingRoundQty();
                totalShippingQty = totalShippingQty.add(
                    backOrderItem.getSpsTAsnDetailDomain().getShippingRoundQty());
                asnDetail.setShippingRoundQty(totalShippingQty);
                
                BigDecimal totalReceivedQty 
                    = sumQtyMap.get(backOrderKey).getBdTotalReceivedQty();
                totalReceivedQty = totalReceivedQty.add(backOrderItem.getBdTotalReceivedQty());
                sumQtyMap.get(backOrderKey).setBdTotalReceivedQty(totalReceivedQty);
            }else{
                // [IN004] : create link in every case
                //String linkAsnFlag = Constants.STR_ZERO;
                //SpsTAsnDetailCriteriaDomain criteria = new SpsTAsnDetailCriteriaDomain();
                //criteria.setDoId(backOrderItem.getSpsTDoDomain().getDoId());
                //int asnDetailCount = spsTAsnDetailService.searchCount(criteria);
                //if(Constants.ZERO < asnDetailCount){
                //    linkAsnFlag = Constants.STR_ONE;
                //}
                //backOrderItem.setLinkAsnFlag(linkAsnFlag);
                
                sumQtyMap.put(backOrderKey, backOrderItem);
            }
        }
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        List<BackOrderInformationDomain> backOrderInformationForDisplay =
            new ArrayList<BackOrderInformationDomain>();
        for(String backOrderKey : sumQtyMap.keySet()){
            BigDecimal totalInTransitQty = null;
            BigDecimal totalBackOrderQty = null;
                
            BackOrderInformationDomain backOrderItem = sumQtyMap.get(backOrderKey);
            backOrderItem.setShippingRoundQty(decimalFormat.format(
                backOrderItem.getSpsTAsnDetailDomain().getShippingRoundQty()));
            if(null == backOrderItem.getBdTotalReceivedQty())
            {
                backOrderItem.setTotalReceivedQty(
                    decimalFormat.format(Constants.BIG_DECIMAL_ZERO));
                backOrderItem.setTotalShipingQty(decimalFormat.format(
                    backOrderItem.getSpsTAsnDetailDomain().getShippingRoundQty()));
                totalInTransitQty = backOrderItem.getSpsTAsnDetailDomain().getShippingRoundQty();
                totalBackOrderQty = backOrderItem.getSpsTDoDetailDomain().getCurrentOrderQty();
                
                backOrderItem.setTotalInTransitQty(decimalFormat.format(totalInTransitQty));
                backOrderItem.setTotalBackOrderQty(decimalFormat.format(totalBackOrderQty));
                backOrderInformationForDisplay.add(backOrderItem);
            }else{
                if((Constants.ZERO != backOrderItem.getSpsTDoDetailDomain().getCurrentOrderQty().compareTo(backOrderItem.getBdTotalReceivedQty())) 
                    || backOrderItem.getSpsTDoDetailDomain().getBackorderFlg().equalsIgnoreCase("1"))
                {
                //if(Constants.ZERO != backOrderItem.getSpsTDoDetailDomain().getCurrentOrderQty()
                //    .compareTo(backOrderItem.getBdTotalReceivedQty()))
                //{
                    backOrderItem.setTotalShipingQty(decimalFormat.format(
                        backOrderItem.getSpsTAsnDetailDomain().getShippingRoundQty()));
                    backOrderItem.setTotalReceivedQty(decimalFormat.format(
                        backOrderItem.getBdTotalReceivedQty()));
                    totalInTransitQty = backOrderItem.getSpsTAsnDetailDomain()
                        .getShippingRoundQty().subtract(backOrderItem.getBdTotalReceivedQty());
                    totalBackOrderQty = backOrderItem.getSpsTDoDetailDomain()
                        .getCurrentOrderQty().subtract(backOrderItem.getBdTotalReceivedQty());
                    
                    backOrderItem.setTotalInTransitQty(decimalFormat.format(totalInTransitQty));
                    backOrderItem.setTotalBackOrderQty(decimalFormat.format(totalBackOrderQty));
                    backOrderInformationForDisplay.add(backOrderItem);
                }
            }
        }
        
        if(backOrderInformationForDisplay.isEmpty()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }
        SortUtil.sort(backOrderInformationForDisplay, SortUtil.COMPARE_BACK_ORDER_INFORMATION);
        backOrderResultMap.clear();
        asnNoMap.clear();
        sumQtyMap.clear();
        return backOrderInformationForDisplay;
    }
}
