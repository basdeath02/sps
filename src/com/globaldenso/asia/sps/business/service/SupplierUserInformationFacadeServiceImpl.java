/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationReturnDomain;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class SupplierUserFacadeServiceImpl.</p>
 * <p>Manage data of Supplier User.</p>
 * <ul>
 * <li>Method search  : initial</li>
 * <li>Method search  : initialWithCriteria</li>
 * <li>Method delete  : searchSelectedCompanySupplier</li>
 * <li>Method delete  : searchSelectedPlantSupplier</li>
 * <li>Method create  : searchSupplierUser</li>
 * <li>Method create  : deleteSupplierUser</li>
 * <li>Method create  : searchSupplierUserCSV</li>
 * </ul>
 *
 * @author CSI
 */
public class SupplierUserInformationFacadeServiceImpl implements 
    SupplierUserInformationFacadeService {

    /** The supplier user service. */
    private UserSupplierService userSupplierService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The Supplier Company Service. */
    private CompanySupplierService companySupplierService;
    
    /** The Supplier Plant Service. */
    private PlantSupplierService plantSupplierService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The SPS M User Service. */
    private SpsMUserService spsMUserService;
    
    /** The SPS M User Role Service. */
    private SpsMUserRoleService spsMUserRoleService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new Supplier User Facade service impl.
     */
    public SupplierUserInformationFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the supplier user service.
     * 
     * @param userSupplierService the new supplier user service
     */
    public void setUserSupplierService(UserSupplierService userSupplierService) {
        this.userSupplierService = userSupplierService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
 
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    /**
     * Sets the supplier company Service.
     * 
     * @param companySupplierService the new supplier company Service.
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    /**
     * Sets the supplier plant service.
     * 
     * @param plantSupplierService the new supplier plant service.
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }
    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    /**
     * Sets The SPS M User Service.
     * 
     * @param spsMUserService the new SPS M User Service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    /**
     * Sets The SPS M User Role Service.
     * 
     * @param spsMUserRoleService the new SPS M User Role Service.
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchDensoSupplierRelation
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public DataScopeControlDomain searchDensoSupplierRelation(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        
        if(Constants.ZERO < densoSupplierRelationDomainList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        return dataScopeControlDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchInitial
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public SupplierUserInformationDomain searchInitial(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        List<CompanySupplierDomain> companySupplierDomainList  = null;
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain
            = new CompanySupplierWithScopeDomain();
        SupplierUserInformationDomain supplierUserInfoDomain = new SupplierUserInformationDomain();
        
        /*Get supplier company information*/
        companySupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        companySupplierWithScopeDomain.getCompanySupplierDomain().setIsActive(Constants.IS_ACTIVE);
        
        companySupplierDomainList = companySupplierService
            .searchCompanySupplierNameByRelation(companySupplierWithScopeDomain);
        
        if(null == companySupplierDomainList || companySupplierDomainList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME);
        }
        
        /*Get list of supplier plant information*/
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierCriteria = new PlantSupplierDomain();
        plantSupplierCriteria.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScope.setPlantSupplierDomain(plantSupplierCriteria);
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<PlantSupplierDomain> plantSupplierList
            =  plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        
        if(null == plantSupplierList || plantSupplierList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        supplierUserInfoDomain.setCompanySupplierList(companySupplierDomainList);
        supplierUserInfoDomain.setPlantSupplierList(plantSupplierList);
        supplierUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        return supplierUserInfoDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#initialWithCriteria
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public SupplierUserInformationDomain searchInitialWithCriteria(SupplierUserInformationDomain 
        supplierUserInfoDomain) throws ApplicationException
    {
        Locale locale = supplierUserInfoDomain.getLocale();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        List<CompanySupplierDomain> companySupplierDomainList =
            supplierUserInfoDomain.getCompanySupplierList();
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain
            = new CompanySupplierWithScopeDomain();
        CompanyDensoWithScopeDomain densoCompanyWithScopeDomain = new CompanyDensoWithScopeDomain();
        DataScopeControlDomain dataScopeControlDomain
            = supplierUserInfoDomain.getDataScopeControlDomain();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getDscId())){
            supplierUserInfoDomain.setDscId(supplierUserInfoDomain.getDscId().trim());
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getFirstName())){
            supplierUserInfoDomain.setFirstName(supplierUserInfoDomain.getFirstName().trim());
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getMiddleName())){
            supplierUserInfoDomain.setMiddleName(supplierUserInfoDomain.getMiddleName().trim());
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getLastName())){
            supplierUserInfoDomain.setLastName(supplierUserInfoDomain.getLastName().trim());
        }
        
        supplierUserInfoDomain.setSupplierAuthenList(
            supplierUserInfoDomain.getSupplierAuthenList());
        
        /*Get supplier company information*/
        CompanyDensoDomain companyDensoDomain = densoCompanyWithScopeDomain.getCompanyDensoDomain();
        companyDensoDomain.setIsActive(Constants.IS_ACTIVE);
        densoCompanyWithScopeDomain.setCompanyDensoDomain(companyDensoDomain);
        densoCompanyWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        companySupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        companySupplierWithScopeDomain.getCompanySupplierDomain().setIsActive(Constants.IS_ACTIVE);
        
        companySupplierDomainList = companySupplierService
            .searchCompanySupplierNameByRelation(companySupplierWithScopeDomain);
        
        if(Constants.ZERO < companySupplierDomainList.size()){
            supplierUserInfoDomain.setCompanySupplierList(companySupplierDomainList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME);
        }
        
        /*Get list of supplier plant information*/
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain
            =  plantSupplierWithScopeDomain.getPlantSupplierDomain();
        plantSupplierDomain.setSCd(supplierUserInfoDomain.getSCd());
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        DataScopeControlDomain dataScopeControlPlant = new DataScopeControlDomain();
        dataScopeControlPlant.setUserType(dataScopeControlDomain.getUserType());
        dataScopeControlPlant.setUserRoleDomainList(dataScopeControlDomain.getUserRoleDomainList());
        dataScopeControlPlant.setDensoSupplierRelationDomainList(dataScopeControlDomain
            .getDensoSupplierRelationDomainList());
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlPlant);
        
        List<PlantSupplierDomain> plantSupplierDomainList
            =  plantSupplierService.searchPlantSupplier(plantSupplierWithScopeDomain);
        if(plantSupplierDomainList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,  
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        supplierUserInfoDomain.setPlantSupplierList(plantSupplierDomainList);
        
        /*Get a number of records count by criteria value*/
        int maxRecord = userSupplierService.searchCountUserSupplier(supplierUserInfoDomain);
        if(maxRecord <= Constants.ZERO){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /* Get Maximum record limit*/
        recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_RLM);
        recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
        if(null == recordsLimit){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Check max record*/
        if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{recordsLimit.getMiscValue()});
        }
        
        /* Get Maximum record per page limit*/
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Calculate Rownum for query*/
        supplierUserInfoDomain.setMaxRowPerPage(Integer.valueOf(
            recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(supplierUserInfoDomain, maxRecord);
        
        /*Get list of supplier user information*/
        List<SupplierUserInformationDomain> resultList
            = userSupplierService.searchUserSupplier(supplierUserInfoDomain);
        if(resultList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }else{
            List<SupplierUserInformationDomain> supplierUserList = 
                new ArrayList<SupplierUserInformationDomain>();
            for(SupplierUserInformationDomain supplierDomain : resultList){
                Date lastUpdateDatetime = new Date(
                    supplierDomain.getLastUpdateDatetime().getTime());
                supplierDomain.setUpdateDatetime(DateUtil.format(
                    lastUpdateDatetime, patternTimestamp));
                supplierUserList.add(supplierDomain);
            }
            supplierUserInfoDomain.setUserSupplierInfoList(supplierUserList);
        }
        return supplierUserInfoDomain;
    } 
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchSelectedCompanySupplier
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(PlantSupplierWithScopeDomain 
        plantSupplierWithScopeDomain) throws ApplicationException
    {
        List<PlantSupplierDomain> plantSupplierList = 
            new ArrayList<PlantSupplierDomain>();
        DataScopeControlDomain dataScopeControlDomain
            = plantSupplierWithScopeDomain.getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain
            = plantSupplierWithScopeDomain.getPlantSupplierDomain();
        PlantSupplierDomain plantSupplierCriteria = new PlantSupplierDomain();
        
        /* Get list of supplier plant information*/
        plantSupplierCriteria.setSCd(plantSupplierDomain.getSCd());
        plantSupplierCriteria.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierCriteria);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierList =  plantSupplierService.searchPlantSupplier(
            plantSupplierWithScopeDomain);
        if(null == plantSupplierList || plantSupplierList.isEmpty()){
            plantSupplierList = new ArrayList<PlantSupplierDomain>();
//            MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                SupplierPortalConstant.ERROR_CD_SP_E6_0031,   
//                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain) throws ApplicationException
    {
        Locale locale = companySupplierWithScopeDomain.getLocale();
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = companySupplierWithScopeDomain.getDataScopeControlDomain();
        CompanySupplierDomain companySupplier
            = companySupplierWithScopeDomain.getCompanySupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if (densoSupplierRelationDomainList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        /*Get list of company supplier information*/
        companySupplier.setIsActive(Constants.IS_ACTIVE);
        result = companySupplierService.searchCompanySupplierNameByRelation(
            companySupplierWithScopeDomain);
        
        if(null == result || result.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME);
        }
        return result;
    }
    

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchUserSupplier
     * (com.globaldenso.asia.sps.business.domain.supplierUserInfoDomain)
     */
    public SupplierUserInformationReturnDomain searchUserSupplier(
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain) 
        throws ApplicationException
    {
        SupplierUserInformationDomain supplierUserInfoDomain
            = supplierUserInformationReturnDomain.getSupplierUserInfomationDomain();
        Locale locale = supplierUserInfoDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain
            = supplierUserInfoDomain.getDataScopeControlDomain();
        SupplierUserInformationDomain supplierUserCriteria = new SupplierUserInformationDomain();
        DataScopeControlDomain dataScopeControlCriteria = new DataScopeControlDomain();
        List<SupplierUserInformationDomain> resultList = 
            new ArrayList<SupplierUserInformationDomain>();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        try{
            /*validate for create supplier user*/
            errorMessageList = validateInputValue(supplierUserInfoDomain);
            
            /*Throw list of error message.*/
            if(Constants.ZERO < errorMessageList.size()){
                supplierUserInformationReturnDomain.setErrorMessageList(errorMessageList);
                return supplierUserInformationReturnDomain;
            }
            
            /*Get Number of record count by criteria values*/
            supplierUserCriteria.setDscId(supplierUserInfoDomain.getDscId());
            if(Constants.MISC_CODE_ALL.equals(supplierUserInfoDomain.getSCd())){
                supplierUserCriteria.setSCd(null);
            }else{
                supplierUserCriteria.setSCd(supplierUserInfoDomain.getSCd());
            }
            if(Constants.MISC_CODE_ALL.equals(supplierUserInfoDomain.getSPcd())){
                supplierUserCriteria.setSPcd(null);
            }else{
                supplierUserCriteria.setSPcd(supplierUserInfoDomain.getSPcd());
            }
            supplierUserCriteria.setFirstName(supplierUserInfoDomain.getFirstName());
            supplierUserCriteria.setMiddleName(supplierUserInfoDomain.getMiddleName());
            supplierUserCriteria.setLastName(supplierUserInfoDomain.getLastName());
            supplierUserCriteria.setRegisterDateFrom(supplierUserInfoDomain.getRegisterDateFrom());
            supplierUserCriteria.setRegisterDateTo(supplierUserInfoDomain.getRegisterDateTo());
            supplierUserCriteria.setIsActive(Constants.IS_ACTIVE);
            supplierUserCriteria.setSupplierAuthenList(
                supplierUserInfoDomain.getSupplierAuthenList());
            
            dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
            dataScopeControlCriteria.setUserRoleDomainList(
                dataScopeControlDomain.getUserRoleDomainList());
            dataScopeControlCriteria.setDensoSupplierRelationDomainList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList());
            
            supplierUserCriteria.setDataScopeControlDomain(dataScopeControlCriteria);
            
            int maxRecord = userSupplierService.searchCountUserSupplier(supplierUserCriteria);
            if(maxRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /* Get Maximum record limit*/
            recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_RLM);
            recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
            if(null == recordsLimit){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Check max record*/
            if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{recordsLimit.getMiscValue()});
            }
            
            /* Get Maximum record per page limit*/
            recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_PLM);
            recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Calculate Rownum for query*/
            supplierUserCriteria.setMaxRowPerPage(Integer.valueOf(
                recordLimitPerPage.getMiscValue()));
            supplierUserCriteria.setPageNumber(supplierUserInfoDomain.getPageNumber());
            SpsPagingUtil.calcPaging(supplierUserCriteria, maxRecord);
            
            /*Get list of supplier user information*/
            resultList = userSupplierService.searchUserSupplier(supplierUserCriteria);
            if(resultList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }else{
                List<SupplierUserInformationDomain> supplierUserList = 
                    new ArrayList<SupplierUserInformationDomain>();
                for(SupplierUserInformationDomain supplierDomain : resultList){
                    Date lastUpdateDatetime = new Date(
                        supplierDomain.getLastUpdateDatetime().getTime());
                    supplierDomain.setUpdateDatetime(DateUtil.format(
                        lastUpdateDatetime, patternTimestamp));
                    supplierUserList.add(supplierDomain);
                }
                supplierUserInformationReturnDomain.setSupplierUserInfoDomainList(supplierUserList);
            }
            supplierUserInformationReturnDomain.setSupplierUserInfomationDomain(
                supplierUserCriteria);
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception ex){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        return supplierUserInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#deleteUserSupplier
     * (com.globaldenso.asia.sps.business.domain.supplierUserInfoDomain)
     */
    public SupplierUserInformationReturnDomain deleteUserSupplier(SupplierUserInformationDomain 
        supplierUserInfoDomain) throws ApplicationException
    {
        DataScopeControlDomain dataScopeControlDomain = supplierUserInfoDomain
            .getDataScopeControlDomain();
        List<SupplierUserInformationDomain> supplierUserSelectedList = supplierUserInfoDomain
            .getUserSupplierInfoList();
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain = 
            new SupplierUserInformationReturnDomain();
        DataScopeControlDomain dataScopeControlCriteria = new DataScopeControlDomain();
        List<SpsMUserCriteriaDomain> spsMUserCriteriaDomainList =
            new ArrayList<SpsMUserCriteriaDomain>(); 
        List<SpsMUserDomain> spsMUserDomainList = new ArrayList<SpsMUserDomain>();
        List<SpsMUserRoleDomain> spsMUserRoleDomainList = new ArrayList<SpsMUserRoleDomain>();
        List<SpsMUserRoleCriteriaDomain> spsMUserRoleCriteriaDomainList =
            new ArrayList<SpsMUserRoleCriteriaDomain>(); 
        List<SupplierUserInformationDomain> supplierUserList = 
            new ArrayList<SupplierUserInformationDomain>();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        int countUser = 0;
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Locale locale = supplierUserInfoDomain.getLocale();
        
        /*Get relation between DENSO and Supplier*/
        dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
        dataScopeControlCriteria.setUserRoleDomainList(dataScopeControlDomain
            .getUserRoleDomainList());
        dataScopeControlCriteria.setDensoSupplierRelationDomainList(null);
        
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlCriteria);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /*set domain and criteria for update*/
        for(SupplierUserInformationDomain supplierUserSelected : supplierUserSelectedList){
            if(!Strings.judgeBlank(supplierUserSelected.getDscId())){
                //Set criteria to SPS_M_USER
                SpsMUserCriteriaDomain spsMUserCriteria = new SpsMUserCriteriaDomain();
                spsMUserCriteria.setDscId(supplierUserSelected.getDscId());
                spsMUserCriteria.setIsActive(Constants.IS_ACTIVE);
                countUser = spsMUserService.searchCount(spsMUserCriteria);
                if(Constants.ZERO == countUser){
                    MessageUtil.throwsApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                        SupplierPortalConstant.LBL_DELETE_SELECTED_ITEMS);
                }
                Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(supplierUserSelected
                    .getUpdateDatetime(), patternTimestamp);
                
                SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                spsMUserDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
                spsMUserDomain.setLastUpdateDscId(supplierUserInfoDomain.getUpdateUser());
                spsMUserDomainList.add(spsMUserDomain);
                
                spsMUserCriteria.setLastUpdateDatetime(lastUpdateDatetime);
                spsMUserCriteriaDomainList.add(spsMUserCriteria);
                
                //Set criteria to SPS_M_USER_ROLE
                SpsMUserRoleCriteriaDomain spsUserRoleCriteria = new SpsMUserRoleCriteriaDomain();
                spsUserRoleCriteria.setDscId(supplierUserSelected.getDscId());
                spsUserRoleCriteria.setIsActive(Constants.IS_ACTIVE);
                spsMUserRoleCriteriaDomainList.add(spsUserRoleCriteria);
                
                SpsMUserRoleDomain spsMUserRoleDomain = new SpsMUserRoleDomain();
                spsMUserRoleDomain.setIsActive(Constants.IS_NOT_ACTIVE);
                spsMUserRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
                spsMUserRoleDomain.setLastUpdateDscId(supplierUserInfoDomain.getUpdateUser());
                spsMUserRoleDomainList.add(spsMUserRoleDomain);
            }
        }
        /*Update Supplier User Information*/
        if(spsMUserCriteriaDomainList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0012,
                SupplierPortalConstant.LBL_DELETE);
        }
        
        int updateRecord = spsMUserService.updateByCondition(spsMUserDomainList, 
            spsMUserCriteriaDomainList);
        if(updateRecord <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_DELETE_SELECTED_ITEMS);
        }else{
            spsMUserRoleService.updateByCondition(spsMUserRoleDomainList,
                spsMUserRoleCriteriaDomainList);
        }
        
        /*search for initial after delete*/
        if(Constants.MISC_CODE_ALL.equals(supplierUserInfoDomain.getSCd())){
            supplierUserInfoDomain.setSCd(null);
        }else{
            supplierUserInfoDomain.setSCd(supplierUserInfoDomain.getSCd());
        }
        if(Constants.MISC_CODE_ALL.equals(supplierUserInfoDomain.getSPcd())){
            supplierUserInfoDomain.setSPcd(null);
        }else{
            supplierUserInfoDomain.setSPcd(supplierUserInfoDomain.getSPcd());
        }
        supplierUserInfoDomain.setIsActive(Constants.IS_ACTIVE);
        
        int maxRecord = userSupplierService.searchCountUserSupplier(supplierUserInfoDomain);
        if(maxRecord <= Constants.ZERO){
            supplierUserInfoDomain.setTotalCount(maxRecord);
            supplierUserInformationReturnDomain.setSupplierUserInfomationDomain(
                supplierUserInfoDomain);
            return supplierUserInformationReturnDomain;
        }
        
        /* Get Maximum record limit*/
        recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_RLM);
        recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
        if(null == recordsLimit){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Check max record*/
        if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{recordsLimit.getMiscValue()});
        }
        
        /* Get Maximum record per page limit*/
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Calculate Rownum for query*/
        supplierUserInfoDomain.setMaxRowPerPage(Integer.valueOf(
            recordLimitPerPage.getMiscValue()));
        supplierUserInfoDomain.setPageNumber(supplierUserInfoDomain.getPageNumber());
        SpsPagingUtil.calcPaging(supplierUserInfoDomain, maxRecord);
        
        /*Get list of supplier user information*/
        supplierUserList = userSupplierService.searchUserSupplier(supplierUserInfoDomain);
        if(null == supplierUserList || supplierUserList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_SUPPLIER_USER);
        }else{
            for(SupplierUserInformationDomain supplierDomain : supplierUserList){
                Date lastUpdateDatetime = new Date(
                    supplierDomain.getLastUpdateDatetime().getTime());
                supplierDomain.setUpdateDatetime(DateUtil.format(
                    lastUpdateDatetime, patternTimestamp));
            }
        }
        supplierUserInformationReturnDomain.setSupplierUserInfoDomainList(supplierUserList);
        supplierUserInformationReturnDomain.setSupplierUserInfomationDomain(supplierUserInfoDomain);
        return supplierUserInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchSupplierUserCSV
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public SupplierUserInformationReturnDomain searchUserSupplierCsv(SupplierUserInformationDomain 
        supplierUserInfoDomain) throws ApplicationException
    {
        Locale locale = supplierUserInfoDomain.getLocale();
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain = 
            new SupplierUserInformationReturnDomain();
        StringBuffer resultString = new StringBuffer();
        DataScopeControlDomain dataScopeControlDomain = supplierUserInfoDomain
            .getDataScopeControlDomain();
        SupplierUserInformationDomain supplierUserCriteria = new SupplierUserInformationDomain();
        DataScopeControlDomain dataScopeControlCriteria = new DataScopeControlDomain();
        CommonDomain commonDomain = new CommonDomain();
        List<SupplierUserInformationDomain> supplierUserList = 
            new ArrayList<SupplierUserInformationDomain>();
        List<SupplierUserInformationDomain> supplierUserResultList = new 
            ArrayList<SupplierUserInformationDomain>();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        
        try{
            
            /*validate for create supplier user*/
            errorMessageList = new ArrayList<ApplicationMessageDomain>();
            errorMessageList = validateInputValue(supplierUserInfoDomain);
            /*Throw list of error message.*/
            if(Constants.ZERO < errorMessageList.size()){
                supplierUserInformationReturnDomain.setErrorMessageList(errorMessageList);
                return supplierUserInformationReturnDomain;
            }
            
            if(Constants.MISC_CODE_ALL.equals(supplierUserInfoDomain.getSCd())){
                supplierUserInfoDomain.setSCd(null);
            }else{
                supplierUserInfoDomain.setSCd(supplierUserInfoDomain.getSCd());
            }
            if(Constants.MISC_CODE_ALL.equals(supplierUserInfoDomain.getSPcd())){
                supplierUserInfoDomain.setSPcd(null);
            }else{
                supplierUserInfoDomain.setSPcd(supplierUserInfoDomain.getSPcd());
            }
            
            /*Get relation between DENSO and Supplier*/
            dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
            dataScopeControlCriteria.setUserRoleDomainList(dataScopeControlDomain
                .getUserRoleDomainList());
            dataScopeControlCriteria.setDensoSupplierRelationDomainList(null);
            
            List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
                densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlCriteria);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
            if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO)
            {
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
            }
            
            supplierUserInfoDomain.setIsActive(Constants.IS_ACTIVE);
            int maxRecord = userSupplierService.searchCountUserSupplier(supplierUserInfoDomain);
            if(maxRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I6_0006,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /* Get Maximum record limit*/
            recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
            recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM001_RLM);
            recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
            if(null == recordsLimit){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Check max record*/
            if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0020,  
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{recordsLimit.getMiscValue()});
            }
            
            /*Set criteria values*/
            supplierUserCriteria.setDscId(supplierUserInfoDomain.getDscId());
            supplierUserCriteria.setSCd(supplierUserInfoDomain.getSCd());
            supplierUserCriteria.setSPcd(supplierUserInfoDomain.getSPcd());
            supplierUserCriteria.setFirstName(supplierUserInfoDomain.getFirstName());
            supplierUserCriteria.setMiddleName(supplierUserInfoDomain.getMiddleName());
            supplierUserCriteria.setLastName(supplierUserInfoDomain.getLastName());
            supplierUserCriteria.setRegisterDateFrom(supplierUserInfoDomain.getRegisterDateFrom());
            supplierUserCriteria.setRegisterDateTo(supplierUserInfoDomain.getRegisterDateTo());
            supplierUserCriteria.setSupplierAuthenList(
                supplierUserInfoDomain.getSupplierAuthenList());
            supplierUserCriteria.setIsActive(Constants.IS_ACTIVE);
            
            dataScopeControlCriteria.setUserType(dataScopeControlDomain.getUserType());
            dataScopeControlCriteria.setUserRoleDomainList(dataScopeControlDomain
                .getUserRoleDomainList());
            dataScopeControlCriteria.setDensoSupplierRelationDomainList(dataScopeControlDomain
                .getDensoSupplierRelationDomainList());
            
            supplierUserCriteria.setDataScopeControlDomain(dataScopeControlCriteria);
            
            /*Get list of supplier user information*/
            supplierUserList = userSupplierService.searchUserSupplierIncludeRole(
                supplierUserCriteria);
            if(supplierUserList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I6_0006,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            supplierUserInformationReturnDomain.setSupplierUserInfoDomainList(supplierUserList);
            
            /*Create CSV String*/
            /*Validate*/
            for(SupplierUserInformationDomain supplierUser : supplierUserList){
                if(null != supplierUser.getUserSupplierDetailDomain().getUserRoleDetailDomain()){
                    if(!StringUtil.checkNullOrEmpty(supplierUser.getUserSupplierDetailDomain()
                        .getUserRoleDetailDomain().getEffectStart())){
                        supplierUser.setEffectStartScreen(DateUtil.format(supplierUser
                            .getUserSupplierDetailDomain().getUserRoleDetailDomain()
                            .getEffectStart(), pattern));
                    }else{
                        supplierUser.setEffectStartScreen(Constants.EMPTY_STRING);
                    }
                    if(!StringUtil.checkNullOrEmpty(supplierUser.getUserSupplierDetailDomain()
                        .getUserRoleDetailDomain().getEffectEnd())){
                        supplierUser.setEffectEndScreen(DateUtil.format(supplierUser
                            .getUserSupplierDetailDomain().getUserRoleDetailDomain()
                            .getEffectEnd(), pattern));
                    }else{
                        supplierUser.setEffectEndScreen(Constants.EMPTY_STRING);
                    }
                }
                supplierUser.setInformationFlag(Constants.STR_N);
                supplierUser.setRoleFlag(Constants.STR_N);
                supplierUserResultList.add(supplierUser);
            }
            
            final String[] headerArr = new String[] {
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_DSC_ID),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_OWNER),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_PCD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_DEPARTMENT_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_FIRST_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_MIDDLE_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_LAST_NAME),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_TELEPHONE),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EMAIL),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EML_URGENT_ORDER),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EML_S_INFO_NOTFOUND),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EML_ALLOW_REVISE),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EML_CANCEL_INVOICE),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_S_P),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EFFECT_START),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EFFECT_END),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_INFO_FLAG),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ROLE_FLAG)};
            List<Map<String, Object>> resultDetail
                = this.doMapSupplierInfoDetail(supplierUserResultList, headerArr);
            
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            resultString = commonService.createCsvString(commonDomain);
            if(null == resultString){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_80_0012,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            supplierUserInformationReturnDomain.setResultString(resultString);
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception ex){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return supplierUserInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /** Private method for validate
     * 
     * @param supplierUserInfoDomain the SupplierUserInformationDomain.
     * @return the list of error message.
     * @throws Exception the Exception
     */
    private List<ApplicationMessageDomain> validateInputValue(SupplierUserInformationDomain
        supplierUserInfoDomain) throws Exception
    {
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = supplierUserInfoDomain.getLocale();
        String message = new String();
        boolean isValidDateFrom = false;
        boolean isValidDateTo = false;
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        String dscId = new String();
        String firstName = new String();
        String middleName = new String();
        String lastName = new String();
        String registerDateFrom = new String();
        String registerDateTo = new String();
        
        /*Validate criteria*/
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getDscId())){
            dscId = supplierUserInfoDomain.getDscId().trim();
            if(!StringUtil.checkNullOrEmpty(dscId)){
                if(Constants.MAX_DSC_ID_LENGTH < supplierUserInfoDomain.getDscId().trim()
                    .length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_DSC_ID),
                                String.valueOf(Constants.MAX_DSC_ID_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }else{
                    /**Changed DSC ID Validation refer to 23/10/2014 12:00 am.*/
                    /*Boolean isNumber = supplierUserInfoDomain.getDscId().trim()
                        .matches(Constants.REGX_NUMBER_FORMAT);
                    if(!isNumber){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_DSC_ID)});
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, message));
                    }*/
                }
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getFirstName())){
            firstName = supplierUserInfoDomain.getFirstName().trim();
            if(!StringUtil.checkNullOrEmpty(firstName)){
                if(Constants.MAX_FIRST_NAME_LENGTH < supplierUserInfoDomain.getFirstName()
                    .trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_FIRST_NAME),
                                String.valueOf(Constants.MAX_FIRST_NAME_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
                /*Validate input character only.
                 * else{
                    Boolean isLetter = supplierUserInfoDomain.getFirstName().trim()
                        .matches(Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_FIRST_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, message));
                    }
                }*/
                supplierUserInfoDomain.setFirstName(supplierUserInfoDomain.getFirstName()
                    .toUpperCase());
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getMiddleName())){
            middleName = supplierUserInfoDomain.getMiddleName().trim();
            if(!StringUtil.checkNullOrEmpty(middleName)){
                if(Constants.MAX_MIDDLE_NAME_LENGTH < supplierUserInfoDomain.getMiddleName()
                    .trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_MIDDLE_NAME),
                                String.valueOf(Constants.MAX_MIDDLE_NAME_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                }
                /*Validate input character only.
                 * else{
                    Boolean isLetter = supplierUserInfoDomain.getMiddleName().trim()
                        .matches(Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_MIDDLE_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, message));
                    }
                }*/
                supplierUserInfoDomain.setMiddleName(supplierUserInfoDomain.getMiddleName()
                    .toUpperCase());
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getLastName())){
            lastName = supplierUserInfoDomain.getLastName().trim();
            if(!StringUtil.checkNullOrEmpty(lastName)){
                if(Constants.MAX_LAST_NAME_LENGTH < supplierUserInfoDomain.getLastName().trim()
                    .length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_LAST_NAME),
                                String.valueOf(Constants.MAX_LAST_NAME_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
                /*Validate input character only.
                 * else{
                    Boolean isLetter = supplierUserInfoDomain.getLastName().trim()
                        .matches(Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_LAST_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, message));
                    }
                }*/
                supplierUserInfoDomain.setLastName(supplierUserInfoDomain.getLastName()
                    .toUpperCase());
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getRegisterDateFrom())){
            registerDateFrom = supplierUserInfoDomain.getRegisterDateFrom().trim();
            if(!StringUtil.checkNullOrEmpty(registerDateFrom)){
                if(!DateUtil.isValidDate(supplierUserInfoDomain.getRegisterDateFrom(), pattern))
                {
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant
                                .LBL_REGISTER_DATE_FROM)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                    isValidDateFrom = false;
                }else{
                    isValidDateFrom = true;
                }
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getRegisterDateTo())){
            registerDateTo = supplierUserInfoDomain.getRegisterDateTo().trim();
            if(!StringUtil.checkNullOrEmpty(registerDateTo)){
                if(!DateUtil.isValidDate(supplierUserInfoDomain.getRegisterDateTo(), pattern)){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant
                                .LBL_REGISTER_DATE_TO)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                    isValidDateTo = false;
                }else{
                    isValidDateTo = true;
                }
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getRegisterDateFrom())
            && !StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getRegisterDateTo())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO < DateUtil.compareDate(supplierUserInfoDomain
                .getRegisterDateFrom(), 
                supplierUserInfoDomain.getRegisterDateTo())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                           MessageUtil.getLabel(locale, SupplierPortalConstant
                               .LBL_REGISTER_DATE_FROM), 
                           MessageUtil.getLabel(locale, SupplierPortalConstant
                               .LBL_REGISTER_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        
        if(StringUtil.checkNullOrEmpty(dscId) 
            && StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getSCd())
            && StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getSPcd())
            && StringUtil.checkNullOrEmpty(firstName)
            && StringUtil.checkNullOrEmpty(middleName)
            && StringUtil.checkNullOrEmpty(lastName)
            && StringUtil.checkNullOrEmpty(registerDateTo)
            && StringUtil.checkNullOrEmpty(registerDateFrom)){
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0011);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                message));
        }else{
            if(StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getSCd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil
                    .getLabel(locale, SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
            if(StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getSPcd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil
                    .getLabel(locale, SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
        }
        return errorMessageList;
    }
    
    /**
     * Do MapSupplierUserDetail.
     * 
     * @param supplierUserList the Supplier User List
     * @param header the List
     * @return resultList 
     */
    private List<Map<String, Object>> doMapSupplierInfoDetail(List<SupplierUserInformationDomain> 
    supplierUserList, String[] header)
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> supplierUserMap = null;
        StringBuffer email = new StringBuffer();
        for(SupplierUserInformationDomain supplierUser : supplierUserList){
            supplierUserMap = new HashMap<String, Object>();
            email = new StringBuffer();
            email.append(Constants.SYMBOL_DOUBLE_QUOTE).append(supplierUser.getUserDomain()
                .getEmail()).append(Constants.SYMBOL_DOUBLE_QUOTE);
            
            supplierUserMap.put(header[Constants.ZERO], supplierUser.getDscId());
            supplierUserMap.put(header[Constants.ONE], StringUtil.checkNullToEmpty(
                supplierUser.getUserSupplierDetailDomain()
                    .getSpsMUserSupplierDomain().getDOwner()));
            supplierUserMap.put(header[Constants.TWO], StringUtil.checkNullToEmpty(
                supplierUser.getSCd()));
            supplierUserMap.put(header[Constants.THREE], StringUtil.checkNullToEmpty(
                supplierUser.getSPcd()));
            supplierUserMap.put(header[Constants.FOUR], StringUtil.checkNullToEmpty(
                supplierUser.getUserDomain().getDepartmentName()));
            supplierUserMap.put(header[Constants.FIVE], StringUtil.checkNullToEmpty(
                supplierUser.getUserDomain().getFirstName()));
            supplierUserMap.put(header[Constants.SIX], StringUtil.checkNullToEmpty(
                supplierUser.getUserDomain().getMiddleName()));
            supplierUserMap.put(header[Constants.SEVEN], StringUtil.checkNullToEmpty(
                supplierUser.getUserDomain().getLastName()));
            supplierUserMap.put(header[Constants.EIGHT], StringUtil.checkNullToEmpty(
                supplierUser.getUserDomain().getTelephone()));
            supplierUserMap.put(header[Constants.NINE], email.toString());
            supplierUserMap.put(header[Constants.TEN], supplierUser.getEmlUrgentFlag());
            supplierUserMap.put(header[Constants.ELEVEN], supplierUser.getEmlInfoFlag());
            supplierUserMap.put(header[Constants.TWELVE], supplierUser.getEmlAllowRevise());
            supplierUserMap.put(header[Constants.THIRTEEN], supplierUser.getEmlCancelInvoice());
            supplierUserMap.put(header[Constants.FOURTEEN], StringUtil.checkNullToEmpty(
                supplierUser.getRoleCode()));
            supplierUserMap.put(header[Constants.FIFTEEN],  StringUtil.checkNullToEmpty(
                supplierUser.getUserSupplierDetailDomain().getUserRoleDetailDomain().getSPcd()));
            supplierUserMap.put(header[Constants.SIXTEEN], supplierUser.getEffectStartScreen());
            supplierUserMap.put(header[Constants.SEVENTEEN], supplierUser.getEffectEndScreen());
            supplierUserMap.put(header[Constants.EIGHTEEN], supplierUser.getInformationFlag());
            supplierUserMap.put(header[Constants.NINETEEN], supplierUser.getRoleFlag());
            resultList.add(supplierUserMap);
        }
        return resultList;
    }
}