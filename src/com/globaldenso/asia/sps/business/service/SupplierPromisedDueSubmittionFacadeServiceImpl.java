/*
 * ModifyDate Development company     Describe 
 * 2014/08/14 CSI Akat                Create
 * 2016/02/23 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;
import com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * Facade for screen WORD004 Supplier Promised Due Submittion.
 * 
 * <ul>
 * <li>Method search    : searchInitial</li>
 * </ul>
 * 
 * @author CSI
 * */
public class SupplierPromisedDueSubmittionFacadeServiceImpl implements
    SupplierPromisedDueSubmittionFacadeService
{
    /** The Purchase Order service. */
    private PurchaseOrderService purchaseOrderService;

    /** The user service. */
    private MiscellaneousService miscService;

    /** The Common Service. */
    private CommonService commonService;
    
    /** The Service for SPS_T_PO_DUE. */
    private SpsTPoDueService spsTPoDueService;
    
    /** The Service for SPS_T_PO_DETAIL. */
    private SpsTPoDetailService spsTPoDetailService;
    
    /** The Service for SPS_T_PO. */
    private SpsTPoService spsTPoService;

    /** The File Management Service. */
    private FileManagementService fileManagementService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;

    /** The default constructor. */
    public SupplierPromisedDueSubmittionFacadeServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for purchaseOrderService.</p>
     *
     * @param purchaseOrderService Set for purchaseOrderService
     */
    public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    /**
     * <p>Setter method for miscService.</p>
     *
     * @param miscService Set for miscService
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>Setter method for spsTPoDueService.</p>
     *
     * @param spsTPoDueService Set for spsTPoDueService
     */
    public void setSpsTPoDueService(SpsTPoDueService spsTPoDueService) {
        this.spsTPoDueService = spsTPoDueService;
    }

    /**
     * <p>Setter method for spsTPoDetailService.</p>
     *
     * @param spsTPoDetailService Set for spsTPoDetailService
     */
    public void setSpsTPoDetailService(SpsTPoDetailService spsTPoDetailService) {
        this.spsTPoDetailService = spsTPoDetailService;
    }

    /**
     * <p>Setter method for spsTPoService.</p>
     *
     * @param spsTPoService Set for spsTPoService
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }

    /**
     * <p>Setter method for fileManagementService.</p>
     *
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierPromisedDueSubmittionFacadeService#searchInitial(com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain)
     */
    public SupplierPromisedDueSubmittionDomain searchInitial(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException
    {
        Locale locale = supplierPromisedDueSubmittionDomain.getLocale();
        
        // 1. Validate input parameter which pass from WORD003 screen
        if (Strings.judgeBlank(supplierPromisedDueSubmittionDomain.getPoId())) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                SupplierPortalConstant.LBL_PO_NO);
        }
        if (Strings.judgeBlank(supplierPromisedDueSubmittionDomain.getSPn())) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                SupplierPortalConstant.LBL_SUPPLIER_PART_NO);
        }
        if (Strings.judgeBlank(supplierPromisedDueSubmittionDomain.getDPn())) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                SupplierPortalConstant.LBL_DENSO_PART_NO);
        }
        
        // 2. Get Purchase Order Due data from PurchaseOrderService
        PurchaseOrderDueDomain poDue = new PurchaseOrderDueDomain();
        poDue.getSpsTPoDomain().setPoId(new BigDecimal(
            supplierPromisedDueSubmittionDomain.getPoId()));
        poDue.setSPn(supplierPromisedDueSubmittionDomain.getSPn());
        poDue.setDPn(supplierPromisedDueSubmittionDomain.getDPn());
        
        // [IN054] If action mode Reply, get only mark pending P/O due
        //if (Constants.MODE_REPLY.equals(supplierPromisedDueSubmittionDomain.getActionMode())) {
        //    poDue.getSpsTPoDueDomain().setMarkPendingFlg(Constants.STR_ONE);
        //}
        
        List<PurchaseOrderDueDomain> poDueList
            = this.purchaseOrderService.searchPurchaseOrderDue(poDue);
        if (null == poDueList || Constants.ZERO == poDueList.size()) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001
                , null);
        }
        
        // 3. Get information for Pending Reason Combo box from MiscellaneousService
        MiscellaneousDomain misc = new MiscellaneousDomain();
        misc.setMiscType(SupplierPortalConstant.MISC_TYPE_PENDING_REASON_CB);
        List<MiscellaneousDomain> pendingReasonList = this.miscService.searchMisc(misc);
        if (null == pendingReasonList || Constants.ZERO == pendingReasonList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_REASON);
        }
        
        // Start : [IN054] Add new combobox
        MiscellaneousDomain miscAcceptReject = new MiscellaneousDomain();
        miscAcceptReject.setMiscType(SupplierPortalConstant.MISC_TYPE_ACCEPT_REJECT_CB);
        List<MiscellaneousDomain> acceptRejectList = this.miscService.searchMisc(miscAcceptReject);
        if (null == acceptRejectList || Constants.ZERO == acceptRejectList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_ACCEPT_REJECT);
        }
        supplierPromisedDueSubmittionDomain.setAcceptRejectCodeList(acceptRejectList);
        // End : [IN054] Add new combobox
            
        // 4. Set <List> poDueInformation to return Domain
        supplierPromisedDueSubmittionDomain.setPurchaseOrderDueList(poDueList);
        supplierPromisedDueSubmittionDomain.setPendingReasonCodeList(pendingReasonList);
        
        return supplierPromisedDueSubmittionDomain;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierPromisedDueSubmittionFacadeService#transactNewDue(com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain)
     */
    public List<ApplicationMessageDomain> transactNewDue(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException
    {
        Locale locale = supplierPromisedDueSubmittionDomain.getLocale();
        String updateDscId = supplierPromisedDueSubmittionDomain.getDscId();
        Timestamp updateDatetime = this.commonService.searchSysDate();
        Set<String> messageSet = new LinkedHashSet<String>();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        List<SpsTPoDueDomain> poDueValueList = new ArrayList<SpsTPoDueDomain>();
        List<SpsTPoDueCriteriaDomain> poDueCriteriaList = new ArrayList<SpsTPoDueCriteriaDomain>();
        BigDecimal poId = new BigDecimal(supplierPromisedDueSubmittionDomain.getPoId());
        SpsTPoDueDomain poDueValue = null;
        SpsTPoDueCriteriaDomain poDueCriteria = null;
        Date proposeDate = null;
        BigDecimal proposeQty = null;
        boolean cannotParse = false;
        
        // 1. Validate input parameter for update
        for (PurchaseOrderDueDomain poDue
            : supplierPromisedDueSubmittionDomain.getPurchaseOrderDueList())
        {
            cannotParse = false;
            if (Strings.judgeBlank(poDue.getStringProposedDate())
                || Strings.judgeBlank(poDue.getStringProposedQty())
                || Strings.judgeBlank(poDue.getSpsTPoDueDomain().getSpsPendingReasonCd()))
            {
                messageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0010));
                continue;
            }
            
            if(!DateUtil.isValidDate(poDue.getStringProposedDate(),
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                messageSet.add(MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_PROPOSED_DATE));
                continue;
            }
            
            try {
                proposeDate = DateUtil.parseToTimestamp(poDue.getStringProposedDate(),
                    DateUtil.PATTERN_YYYYMMDD_SLASH);
                proposeQty = new BigDecimal(poDue.getStringProposedQty().replaceAll(
                    Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
            } catch (Exception exception) {
                proposeDate = null;
                proposeQty = null;
                cannotParse = true;
            }
            
            if (cannotParse
                || null == proposeDate
                || (null != poDue.getSpsTPoDueDomain().getEtd()
                    && null != poDue.getSpsTPoDueDomain().getOrderQty() 
                    && Constants.ZERO == poDue.getSpsTPoDueDomain().getEtd().compareTo(proposeDate)
                    && poDue.getSpsTPoDueDomain().getOrderQty().equals(proposeQty)))
            {
                messageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0029));
                continue;
            }
            
            poDueValue = new SpsTPoDueDomain();
            poDueValue.setSpsProposedDueDate(new java.sql.Date(proposeDate.getTime()));
            poDueValue.setSpsProposedQty(proposeQty);
            poDueValue.setSpsPendingReasonCd(poDue.getSpsTPoDueDomain().getSpsPendingReasonCd());
            poDueValue.setMarkPendingFlg(Constants.STR_ONE);
            poDueValue.setLastUpdateDscId(updateDscId);
            poDueValue.setLastUpdateDatetime(updateDatetime);
            poDueValueList.add(poDueValue);
            
            poDueCriteria = new SpsTPoDueCriteriaDomain();
            poDueCriteria.setPoId(poId);
            poDueCriteria.setSPn(supplierPromisedDueSubmittionDomain.getSPn());
            poDueCriteria.setDPn(supplierPromisedDueSubmittionDomain.getDPn());
            poDueCriteria.setDueId(poDue.getSpsTPoDueDomain().getDueId());
            poDueCriteria.setLastUpdateDatetime(poDue.getSpsTPoDueDomain().getLastUpdateDatetime());
            poDueCriteriaList.add(poDueCriteria);
        }

        if (Constants.ZERO != messageSet.size()) {
            for (String message : messageSet) {
                applicationMessageList.add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
            return applicationMessageList;
        }
        
        if (Constants.ZERO == poDueValueList.size() || Constants.ZERO == poDueCriteriaList.size()) {
            return null;
        }
        
        // 2. Update Purchase Order Due from SpsTPoDueService
        int updatePoDueCount
            = this.spsTPoDueService.updateByCondition(poDueValueList, poDueCriteriaList);
        if (poDueValueList.size() != updatePoDueCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0036, null);
        }
        
        PurchaseOrderDueDomain firstPoDue
            = supplierPromisedDueSubmittionDomain.getPurchaseOrderDueList().get(Constants.ZERO);
        
        // 3. Update Purchase Order Detail from SpsTPoDetailService
        SpsTPoDetailDomain poDetail = new SpsTPoDetailDomain();
        poDetail.setPnStatus(SupplierPortalConstant.PO_STATUS_PENDING);
        poDetail.setLastUpdateDscId(updateDscId);
        poDetail.setLastUpdateDatetime(updateDatetime);
        
        SpsTPoDetailCriteriaDomain poDetailCriteira = new SpsTPoDetailCriteriaDomain();
        poDetailCriteira.setPoId(poId);
        poDetailCriteira.setSPn(supplierPromisedDueSubmittionDomain.getSPn());
        poDetailCriteira.setDPn(supplierPromisedDueSubmittionDomain.getDPn());
        poDetailCriteira.setLastUpdateDatetime(firstPoDue.getDetailUpdateDatetime());
        
        int updatePoDetailCount
            = this.spsTPoDetailService.updateByCondition(poDetail, poDetailCriteira);
        if (Constants.ONE != updatePoDetailCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0035, null);
        }
        
        // 4. Update Purchase Order Header from SpsTPoService
        SpsTPoDomain poHeader = new SpsTPoDomain();
        poHeader.setLastUpdateDscId(updateDscId);
        poHeader.setLastUpdateDatetime(updateDatetime);
        
        SpsTPoCriteriaDomain poHeaderCriteria = new SpsTPoCriteriaDomain();
        poHeaderCriteria.setPoId(poId);
        poHeaderCriteria.setLastUpdateDatetime(
            firstPoDue.getSpsTPoDomain().getLastUpdateDatetime());
        
        int updatePoCount = this.spsTPoService.updateByCondition(poHeader, poHeaderCriteria);
        if (Constants.ONE != updatePoCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0035, null);
        }
        return null;
    }
    
    // Start : [IN054] add new method
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierPromisedDueSubmittionFacadeService#transactNewDue(com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain)
     */
    public List<ApplicationMessageDomain> transactReplyDue(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException
    {
        Locale locale = supplierPromisedDueSubmittionDomain.getLocale();
        String updateDscId = supplierPromisedDueSubmittionDomain.getDscId();
        Timestamp updateDatetime = this.commonService.searchSysDate();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        List<SpsTPoDueDomain> poDueValueList = new ArrayList<SpsTPoDueDomain>();
        List<SpsTPoDueCriteriaDomain> poDueCriteriaList = new ArrayList<SpsTPoDueCriteriaDomain>();
        BigDecimal poId = new BigDecimal(supplierPromisedDueSubmittionDomain.getPoId());
        SpsTPoDueDomain poDueValue = null;
        SpsTPoDueCriteriaDomain poDueCriteria = null;
        
        // 1. Validate input parameter for update
        for (PurchaseOrderDueDomain poDue
            : supplierPromisedDueSubmittionDomain.getPurchaseOrderDueList())
        {
            if (Strings.judgeBlank(poDue.getSpsTPoDueDomain().getDensoReplyFlg())) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                        SupplierPortalConstant.LBL_ACCEPT_REJECT)));
                break;
            }
            
            poDueValue = new SpsTPoDueDomain();
            poDueValue.setDensoReplyFlg(poDue.getSpsTPoDueDomain().getDensoReplyFlg());
            poDueValue.setLastUpdateDscId(updateDscId);
            poDueValue.setLastUpdateDatetime(updateDatetime);
            poDueValueList.add(poDueValue);
            
            poDueCriteria = new SpsTPoDueCriteriaDomain();
            poDueCriteria.setPoId(poId);
            poDueCriteria.setSPn(supplierPromisedDueSubmittionDomain.getSPn());
            poDueCriteria.setDPn(supplierPromisedDueSubmittionDomain.getDPn());
            poDueCriteria.setDueId(poDue.getSpsTPoDueDomain().getDueId());
            poDueCriteria.setLastUpdateDatetime(poDue.getSpsTPoDueDomain().getLastUpdateDatetime());
            poDueCriteriaList.add(poDueCriteria);
        }

        if (Constants.ZERO != applicationMessageList.size()) {
            return applicationMessageList;
        }
        
        // 2. Update Purchase Order Due from SpsTPoDueService
        int updatePoDueCount
            = this.spsTPoDueService.updateByCondition(poDueValueList, poDueCriteriaList);
        if (poDueValueList.size() != updatePoDueCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0036, null);
        }
        
        PurchaseOrderDueDomain firstPoDue
            = supplierPromisedDueSubmittionDomain.getPurchaseOrderDueList().get(Constants.ZERO);
        
        // 3. Update Purchase Order Detail from SpsTPoDetailService
        SpsTPoDetailDomain poDetail = new SpsTPoDetailDomain();
        poDetail.setPnStatus(SupplierPortalConstant.PO_STATUS_REPLY);
        poDetail.setLastUpdateDscId(updateDscId);
        poDetail.setLastUpdateDatetime(updateDatetime);
        
        SpsTPoDetailCriteriaDomain poDetailCriteira = new SpsTPoDetailCriteriaDomain();
        poDetailCriteira.setPoId(poId);
        poDetailCriteira.setSPn(supplierPromisedDueSubmittionDomain.getSPn());
        poDetailCriteira.setDPn(supplierPromisedDueSubmittionDomain.getDPn());
        poDetailCriteira.setLastUpdateDatetime(firstPoDue.getDetailUpdateDatetime());
        
        int updatePoDetailCount
            = this.spsTPoDetailService.updateByCondition(poDetail, poDetailCriteira);
        if (Constants.ONE != updatePoDetailCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0035, null);
        }
        
        // 4. Update Purchase Order Header from SpsTPoService
        SpsTPoDomain poHeader = new SpsTPoDomain();
        poHeader.setLastUpdateDscId(updateDscId);
        poHeader.setLastUpdateDatetime(updateDatetime);
        
        SpsTPoCriteriaDomain poHeaderCriteria = new SpsTPoCriteriaDomain();
        poHeaderCriteria.setPoId(poId);
        poHeaderCriteria.setLastUpdateDatetime(
            firstPoDue.getSpsTPoDomain().getLastUpdateDatetime());
        
        int updatePoCount = this.spsTPoService.updateByCondition(poHeader, poHeaderCriteria);
        if (Constants.ONE != updatePoCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0035, null);
        }
        return null;
    }
    // End : [IN054] add new method
    

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierPromisedDueSubmittionFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain)
     */
    public FileManagementDomain searchFileName(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException
    {
        FileManagementDomain result = null;
        
        // 1. Find file name from FileManagementService
        try {
            result = this.fileManagementService.searchFileDownload(
                supplierPromisedDueSubmittionDomain.getFileId(), false, null);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(supplierPromisedDueSubmittionDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierPromisedDueSubmittionFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain, java.io.OutputStream)
     */
    public void searchLegendInfo(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain
        , OutputStream output) throws ApplicationException
    {
        // 1. Get PDF file from FileManagementService
        try {
            this.fileManagementService.searchFileDownload(
                supplierPromisedDueSubmittionDomain.getFileId(), true, output);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(supplierPromisedDueSubmittionDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
}
