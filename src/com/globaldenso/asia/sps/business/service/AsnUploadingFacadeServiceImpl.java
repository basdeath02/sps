/*
 * ModifyDate Development company    Describe 
 * 2014/06/12 CSI Parichat           Create
 * 2015/08/24 CSI Akat               [IN012]
 * 2015/10/15 CSI Akat               [IN034]
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.AsnUploadingReturnDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>The Class AsnUploadingFacadeServiceImpl.</p>
 * <p>Facade for ASN uploading.</p>
 * <ul>
 * <li>Method delete  : deleteTmpUploadAsn</li>
 * <li>Method update  : transactUploadAsn</li>
 * <li>Method search  : searchTmpUploadAsnCsv</li>
 * <li>Method search  : searchByReturn</li>
 * <li>Method search  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public class AsnUploadingFacadeServiceImpl implements AsnUploadingFacadeService {
    
    /** Index of Uploading CSV, Column 'DCd'. */
    private static final int INDEX_DCD = 0;
    
    /** Index of Uploading CSV, Column 'DPcd'. */
    private static final int INDEX_DPCD = 1;
    
    /** Index of Uploading CSV, Column 'Vendor Cd'. */
    private static final int INDEX_VENDOR_CD = 2;
    
    /** Index of Uploading CSV, Column 'SPcd'. */
    private static final int INDEX_SPCD = 3;
    
    /** Index of Uploading CSV, Column 'ASN No'. */
    private static final int INDEX_ASN_NO = 4;
    
    /** Index of Uploading CSV, Column 'Plan Eta'. */
    private static final int INDEX_PLAN_ETA = 5;
    
    /** Index of Uploading CSV, Column 'Delivery Time'. */
    private static final int INDEX_DELIVERY_TIME = 6;
    
    /** Index of Uploading CSV, Column 'Actual Etd'. */
    private static final int INDEX_ACTUAL_ETD = 7;
    
    /** Index of Uploading CSV, Column 'Shipping Time'. */
    private static final int INDEX_SHIPPING_TIME = 8;
    
    /** Index of Uploading CSV, Column 'No Of Pallet'. */
    private static final int INDEX_NO_OF_PALLET = 9;
    
    /** Index of Uploading CSV, Column 'Trip No'. */
    private static final int INDEX_TRIP_NO = 10;
    
    /** Index of Uploading CSV, Column 'SPS Do No'. */
    private static final int INDEX_SPS_DO_NO = 11;
    
    /** Index of Uploading CSV, Column 'Order Method'. */
    private static final int INDEX_ORDER_METHOD = 12;
    
    /** Index of Uploading CSV, Column 'Revision'. */
    private static final int INDEX_REVISION = 13;
    
    /** Index of Uploading CSV, Column 'TRANs'. */
    private static final int INDEX_TRANS = 14;
    
    /** Index of Uploading CSV, Column 'DPn'. */
    private static final int INDEX_DPN = 15;
    
    /** Index of Uploading CSV, Column 'SPn'. */
    private static final int INDEX_SPN = 16;
    
    /** Index of Uploading CSV, Column 'Rcv Lane'. */
    private static final int INDEX_RCV_LANE = 17;
    
    /** Index of Uploading CSV, Column 'Shipping Qty'. */
    private static final int INDEX_SHIPPING_QTY = 18;
    
    /** Index of Uploading CSV, Column 'Unit Of Measure'. */
    private static final int INDEX_UM = 19;
    
    /** The asn service. */
    private AsnService asnService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The delivery order service. */
    private DeliveryOrderService deliveryOrderService;
    
    /** The denso supplier relation service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The record limit service. */
    private RecordLimitService recordLimitService;
    
    /** The company denso service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The plant denso service. */
    private SpsMPlantDensoService spsMPlantDensoService;
    
    /** The plant supplier service. */
    private SpsMPlantSupplierService spsMPlantSupplierService;
    
    /** The temp upload asn service. */
    private SpsTmpUploadAsnService spsTmpUploadAsnService;
    
    /** The as400 vendor service. */
    private SpsMAs400VendorService spsMAs400VendorService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new ASN uploading facade service impl.
     */
    public AsnUploadingFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the asn service.
     * 
     * @param asnService the new asn service
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }

    /**
     * Sets the common service.
     * 
     * @param commonService the new common service
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets the delivery order service.
     * 
     * @param deliveryOrderService the new delivery order service
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }

    /**
     * Sets the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the new denso supplier relation service
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * Sets the company denso service.
     * 
     * @param spsMCompanyDensoService the new company denso service
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }

    /**
     * Sets the plant denso service.
     * 
     * @param spsMPlantDensoService the new plant denso service
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }

    /**
     * Sets the plant supplier service.
     * 
     * @param spsMPlantSupplierService the new plant supplier service
     */
    public void setSpsMPlantSupplierService(
        SpsMPlantSupplierService spsMPlantSupplierService) {
        this.spsMPlantSupplierService = spsMPlantSupplierService;
    }

    /**
     * Sets the temp upload asn service.
     * 
     * @param spsTmpUploadAsnService the new temp upload asn service
     */
    public void setSpsTmpUploadAsnService(SpsTmpUploadAsnService spsTmpUploadAsnService) {
        this.spsTmpUploadAsnService = spsTmpUploadAsnService;
    }
    
    /**
     * Sets the as400 vendor service.
     * 
     * @param spsMAs400VendorService the new as400 vendor service
     */
    public void setSpsMAs400VendorService(SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService#deleteTmpUploadAsn(com.globaldenso.asia.sps.business.domain.AsnUploadingDomain)
     */
    public int deleteTmpUploadAsn(AsnUploadingDomain asnUploadingDomain) throws Exception
    {
        int recordDelete = Constants.ZERO;
        SpsTmpUploadAsnCriteriaDomain criteria = new SpsTmpUploadAsnCriteriaDomain();
        
        if(!StringUtil.checkNullOrEmpty(asnUploadingDomain.getSessionId())
            && !StringUtil.checkNullOrEmpty(asnUploadingDomain.getUploadDscId())){
            criteria.setSessionId(asnUploadingDomain.getSessionId());
            criteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        }else{
            //CommonService.searchSysDate() - 2 days
            Timestamp currentDateTime = commonService.searchSysDate();
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDateTime);
            cal.add(Calendar.DATE, Constants.MINUS_TWO);
            criteria.setUploadDatetimeLessThanEqual(new Timestamp(cal.getTimeInMillis()));
        }
        recordDelete = spsTmpUploadAsnService.deleteByCondition(criteria);
        return recordDelete;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService#transactUploadAsn(com.globaldenso.asia.sps.business.domain.AsnUploadingDomain)
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_UNCHECKED, Constants.SUPPRESS_WARNINGS_RAWTYPES})
    public AsnUploadingDomain transactUploadAsn(AsnUploadingDomain asnUploadingCriteria) 
        throws ApplicationException, IOException
    {
        Locale locale = asnUploadingCriteria.getLocale();
        boolean isSameAsnNo = true;
        boolean isSameSupplierCode = true;
        boolean isSameSupplierPlantCode = true;
        boolean isSameDensoCode = true;
        boolean isSameDensoPlantCode = true;
        boolean isSameOrderMethod = true;
        boolean isSameTrans = true;
        boolean isTheSameReceiveByScan = true;
        boolean isTheSameWhPrimeReceiving = true;
        boolean isAmountPartGroupByRcvLaneOverQrCode = true;
        
        BufferedReader br = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        Iterator itr = null;
        
        List<String> errorCodeList = new ArrayList<String>();
        AsnUploadingDomain asnUploadingDomainResult = new AsnUploadingDomain();
        DeliveryOrderInformationDomain doCriteria = null;
        SpsMCompanyDensoCriteriaDomain companyDensoCriteria = null;
        SpsMPlantDensoCriteriaDomain plantDensoCriteria = null;
        SpsMPlantSupplierCriteriaDomain plantSupplierCriteria = null;
        SpsMAs400VendorCriteriaDomain vendorCriteria = null;
        SpsTmpUploadAsnDomain tmpUploadAsnDomain = null;
        SpsTmpUploadAsnCriteriaDomain tmpUploadAsnCriteria = null;
        List<String> splitItem = null;
        List<SpsTmpUploadAsnDomain> tmpUploadAsnDomainList = null;
        List<SpsTmpUploadAsnDomain> tmpUploadAsnInsertList = new ArrayList<SpsTmpUploadAsnDomain>();
        List<List<String>> asnInfoUploadingList = new ArrayList<List<String>>();
        List<ApplicationMessageDomain> errorMsgList = new ArrayList<ApplicationMessageDomain>();
        List<ApplicationMessageDomain> warningMsgList = new ArrayList<ApplicationMessageDomain>();
        
        AsnUploadingDomain asnUserAuthority = new AsnUploadingDomain();
        asnUserAuthority.setSupplierAuthenList(asnUploadingCriteria.getSupplierAuthenList());
        asnUserAuthority.setDensoAuthenList(asnUploadingCriteria.getDensoAuthenList());
        
        Map<String, Integer> quantityEachRcvLane = new HashMap<String, Integer>();
        List<String> rcvLane = new ArrayList<String>();
        List<String> rcvLaneError = new ArrayList<String>();
        
        /**
         * 2. Get each record and validate the content
         * 3. Validate file content in the header level
         **/
        try{
            inputStream = asnUploadingCriteria.getFileInputStream();
            inputStream = FileUtil.checkUtf8BOMAndSkip(inputStream);
            inputStreamReader = new InputStreamReader(inputStream);
            br = new BufferedReader(inputStreamReader);
            
            String line = null;
            while((line = br.readLine()) != null){
                List<String> lineItem = StringUtil.splitCsvData(line, Constants.TWENTY);
                lineItem.set(INDEX_DCD, lineItem.get(INDEX_DCD).toUpperCase());
                lineItem.set(INDEX_DPCD, lineItem.get(INDEX_DPCD).toUpperCase());
                lineItem.set(INDEX_VENDOR_CD, lineItem.get(INDEX_VENDOR_CD).toUpperCase());
                lineItem.set(INDEX_SPCD, lineItem.get(INDEX_SPCD).toUpperCase());
                lineItem.set(INDEX_ASN_NO, lineItem.get(INDEX_ASN_NO).toUpperCase());
                lineItem.set(INDEX_PLAN_ETA, lineItem.get(INDEX_PLAN_ETA).toUpperCase());
                lineItem.set(INDEX_DELIVERY_TIME, lineItem.get(INDEX_DELIVERY_TIME).toUpperCase());
                lineItem.set(INDEX_ACTUAL_ETD, lineItem.get(INDEX_ACTUAL_ETD).toUpperCase());
                lineItem.set(INDEX_SHIPPING_TIME, lineItem.get(INDEX_SHIPPING_TIME).toUpperCase());
                lineItem.set(INDEX_NO_OF_PALLET, lineItem.get(INDEX_NO_OF_PALLET).toUpperCase());
                lineItem.set(INDEX_TRIP_NO, lineItem.get(INDEX_TRIP_NO).toUpperCase());
                lineItem.set(INDEX_SPS_DO_NO, lineItem.get(INDEX_SPS_DO_NO).toUpperCase());
                lineItem.set(INDEX_ORDER_METHOD, lineItem.get(INDEX_ORDER_METHOD).toUpperCase());
                lineItem.set(INDEX_REVISION, lineItem.get(INDEX_REVISION).toUpperCase());
                lineItem.set(INDEX_TRANS, lineItem.get(INDEX_TRANS).toUpperCase());
                lineItem.set(INDEX_DPN, lineItem.get(INDEX_DPN).toUpperCase());
                lineItem.set(INDEX_SPN, lineItem.get(INDEX_SPN).toUpperCase());
                lineItem.set(INDEX_RCV_LANE, lineItem.get(INDEX_RCV_LANE).toUpperCase());
                lineItem.set(INDEX_SHIPPING_QTY, lineItem.get(INDEX_SHIPPING_QTY).toUpperCase());
                lineItem.set(INDEX_UM, lineItem.get(INDEX_UM).toUpperCase());
                asnInfoUploadingList.add(lineItem);
            }
        }finally{
            if(null != inputStream){
                inputStream.close();
            }
            if(null != inputStreamReader){
                inputStreamReader.close();
            }
            if(null != br){
                br.close();
            }
        }
        
        if(Constants.ZERO < asnInfoUploadingList.size())
        {
            String asnNo = Constants.EMPTY_STRING;
            String vendorCd = Constants.EMPTY_STRING;
            String sPcd = Constants.EMPTY_STRING;
            String dCd = Constants.EMPTY_STRING;
            String dPcd = Constants.EMPTY_STRING;
            String receiveByScan = Constants.EMPTY_STRING;
            String whPrimeReceiving = Constants.EMPTY_STRING;
            String orderMethod = Constants.EMPTY_STRING;
            String trans = Constants.EMPTY_STRING;
            itr = asnInfoUploadingList.iterator();
            while(itr.hasNext() && (isSameAsnNo || isSameSupplierCode || isSameSupplierPlantCode 
                || isSameDensoCode || isSameDensoPlantCode || isSameOrderMethod || isSameTrans))
            {
                splitItem = (List<String>)itr.next();
                //3.1 Validate for Error
                if(StringUtil.checkNullOrEmpty(asnNo)){
                    asnNo = splitItem.get(INDEX_ASN_NO);
                }else{
                    if(!asnNo.equals(splitItem.get(INDEX_ASN_NO)) && isSameAsnNo){
                        isSameAsnNo = false;
                        errorMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                SupplierPortalConstant.LBL_ASN_NO_NOT_SAME)));
                    }
                }
                if(StringUtil.checkNullOrEmpty(vendorCd)){
                    vendorCd = splitItem.get(INDEX_VENDOR_CD);
                }else{
                    if(!vendorCd.equals(splitItem.get(INDEX_VENDOR_CD)) && isSameSupplierCode){
                        isSameSupplierCode = false;
                        errorMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                SupplierPortalConstant.LBL_S_CD_NOT_SAME)));
                    } 
                }
                if(StringUtil.checkNullOrEmpty(sPcd)){
                    sPcd = splitItem.get(INDEX_SPCD);
                }else{
                    if(!sPcd.equals(splitItem.get(INDEX_SPCD)) && isSameSupplierPlantCode){
                        isSameSupplierPlantCode = false;
                        errorMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                SupplierPortalConstant.LBL_S_PCD_NOT_SAME)));
                    }       
                }
                if(StringUtil.checkNullOrEmpty(dCd)){
                    dCd = splitItem.get(INDEX_DCD);
                }else{
                    if(!dCd.equals(splitItem.get(INDEX_DCD)) && isSameDensoCode){
                        isSameDensoCode = false;
                        errorMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                SupplierPortalConstant.LBL_D_CD_NOT_SAME)));
                    }      
                }
                if(StringUtil.checkNullOrEmpty(dPcd)){
                    dPcd = splitItem.get(INDEX_DPCD);
                }else{
                    if(!dPcd.equals(splitItem.get(INDEX_DPCD)) && isSameDensoPlantCode){
                        isSameDensoPlantCode = false;
                        errorMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                SupplierPortalConstant.LBL_D_PCD_NOT_SAME)));
                    }   
                }
                
                DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
                deliveryOrderInformationDomain.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                deliveryOrderInformationDomain.setDPn(splitItem.get(INDEX_DPN));
                deliveryOrderInformationDomain.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                deliveryOrderInformationDomain.setUnitOfMeasure(splitItem.get(INDEX_UM));
                deliveryOrderInformationDomain.setSupplierAuthenList(asnUploadingCriteria.getSupplierAuthenList());
                deliveryOrderInformationDomain.setDensoAuthenList(asnUploadingCriteria.getDensoAuthenList());
                List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain
                    = deliveryOrderService.searchExistDo(deliveryOrderInformationDomain);
                DeliveryOrderDetailDomain chkReceiveByScanDomain = null;
                if(deliveryOrderDetailDomain != null && deliveryOrderDetailDomain.size() > 0){
                    chkReceiveByScanDomain = deliveryOrderDetailDomain.get(Constants.ZERO);
                    //Start *** warehouse is not same
                    if(StringUtil.checkNullOrEmpty(whPrimeReceiving)){
                        whPrimeReceiving = chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getWhPrimeReceiving();
                    }else{
                        if(!whPrimeReceiving.equals(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getWhPrimeReceiving()) 
                            && chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getReceiveByScan().equals(Constants.STR_Y)
                            && isTheSameWhPrimeReceiving){
                            isTheSameWhPrimeReceiving = false;
                            errorMsgList.add(new ApplicationMessageDomain(
                                Constants.MESSAGE_FAILURE, 
                                MessageUtil.getApplicationMessageWithLabel(locale, 
                                    SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                    SupplierPortalConstant.LBL_WH_PRIME_RECEIVING_NOT_SAME)));
                        }   
                    }
                    //End *** warehouse is not same
                    
                    //Start *** Receive by Scan is not same
                    if(StringUtil.checkNullOrEmpty(receiveByScan)){
                        receiveByScan = chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getReceiveByScan();
                    }else{
                        if(!receiveByScan.equals(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getReceiveByScan()) && isTheSameReceiveByScan){
                            isTheSameReceiveByScan = false;
                            errorMsgList.add(new ApplicationMessageDomain(
                                Constants.MESSAGE_FAILURE, 
                                MessageUtil.getApplicationMessageWithLabel(locale, 
                                    SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                                    SupplierPortalConstant.LBL_RECEIVE_BY_SCAN_NOT_SAME)));
                        }   
                    }
                    //End *** Receive by Scan is not same
                    
                    //*** check maximum record when Receive by Scan ="Y" ***
                    //This step to count from RCV Lane
                    if(!quantityEachRcvLane.containsKey(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getRcvLane())){
                        quantityEachRcvLane.put(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getRcvLane(), Constants.ONE);
                        rcvLane.add(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getRcvLane());
                    } else {
                        quantityEachRcvLane.put(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getRcvLane(), quantityEachRcvLane.get(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getRcvLane()) + 1);
                    }
                }
//                Start *** DO has not been printed One-way Kanban tag
//                DENSO user need to remove this condition
//                On 2/2/2018
//                if(chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getIsPrintOneWayKanbanTag().equals(Constants.STR_N)
//                    && chkReceiveByScanDomain.getDeliveryOrderSubDetailDomain().getReceiveByScan().equals(Constants.STR_Y)){
//                   MessageUtil.throwsApplicationMessage(locale,
//                        SupplierPortalConstant.ERROR_CD_SP_E7_0042,
//                        splitItem.get(INDEX_SPS_DO_NO));
//                }
//                End *** DO has not been printed One-way Kanban tag
                
                //3.3 Validate for warning
                if(StringUtil.checkNullOrEmpty(orderMethod)){
                    orderMethod = splitItem.get(INDEX_ORDER_METHOD);
                }else{
                    if(!orderMethod.equals(splitItem.get(INDEX_ORDER_METHOD)) && isSameOrderMethod)
                    {
                        isSameOrderMethod = false;
                    }
                }
                if(StringUtil.checkNullOrEmpty(trans)){
                    trans = splitItem.get(INDEX_TRANS);
                }else{
                    if(!trans.equals(splitItem.get(INDEX_TRANS)) && isSameTrans){
                        isSameTrans = false;
                        warningMsgList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_WARNING, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_W6_0011, 
                                SupplierPortalConstant.LBL_TRANS_NOT_SAME)));
                    }
                }
            }
            //Start *** check maximum record when Receive by Scan ="Y" ***
            if(receiveByScan.equals(Constants.STR_Y)){
                Integer i = Constants.ZERO;
                while(i < rcvLane.size()){
                    if(SupplierPortalConstant.MAX_RECORD_FOR_RECEIVE_BY_SCAN <= quantityEachRcvLane.get(rcvLane.get(i))){
                        isAmountPartGroupByRcvLaneOverQrCode = false;
                        rcvLaneError.add(rcvLane.get(i));
                    }
                    i++;
                }
                if(!isAmountPartGroupByRcvLaneOverQrCode){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0043,
                        SupplierPortalConstant.STR_MAX_RECORD_FOR_RECEIVE_BY_SCAN);
                }
                // End *** check maximum record when Receive by Scan ="Y" ***
            }
            
            //Validate order method
            if(isSameDensoCode && !StringUtil.checkNullOrEmpty(dCd) && !isSameOrderMethod){
                SpsMCompanyDensoCriteriaDomain criteria = new SpsMCompanyDensoCriteriaDomain();
                criteria.setDCd(StringUtil.rightPad(dCd, Constants.FIVE));
                SpsMCompanyDensoDomain companyDensoDomain
                    = spsMCompanyDensoService.searchByKey(criteria);
                if(null == companyDensoDomain
                    || StringUtil.checkNullOrEmpty(companyDensoDomain.getGroupAsnErrorType())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                        SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
                }
                if(SupplierPortalConstant.GROUP_ASN_WARNING_TYPE.equals(
                    companyDensoDomain.getGroupAsnErrorType())){
                    warningMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
                        MessageUtil.getApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_W6_0011,
                            SupplierPortalConstant.LBL_ORDER_METHOD_NOT_SAME)));
                }else{
                    errorMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                            SupplierPortalConstant.LBL_ORDER_METHOD_NOT_SAME)));
                }
            }
        }
        if(Constants.ZERO < errorMsgList.size()){
            asnUploadingCriteria.setErrorMessageList(errorMsgList);
            return null;
        }
        
        //4. Validate file content in the detail level
        Timestamp updateDateTime = commonService.searchSysDate();
        String sCd = Constants.EMPTY_STRING;
        
        boolean isValidDCd = false;
        boolean isValidVendorCd = false;
        boolean isValidAsnNo = true;
        boolean isValidDeliveryOrder = true;
        boolean isValidDPn = true;
        int csvLineNo = Constants.ONE;
        itr = asnInfoUploadingList.iterator();
        
        while(itr.hasNext())
        {
            splitItem = (List<String>)itr.next();
            errorCodeList = new ArrayList<String>();
            
            isValidDCd = false;
            isValidVendorCd = false;
            isValidAsnNo = true;
            isValidDeliveryOrder = true;
            isValidDPn = true;
            boolean isValidDCdAuthen = false;
            boolean isValidSCdAuthen = false;
            
            //*** Check blank DENSO code
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DCD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F01);
            }else{
                //*** Check DENSO code length
                if(SupplierPortalConstant.LENGTH_DENSO_CODE 
                    < splitItem.get(INDEX_DCD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F01);
                }else{
                    //Set Company Denso Code format to 5 digits
                    splitItem.set(INDEX_DCD,
                        String.format(Constants.DCD_FORMAT, splitItem.get(INDEX_DCD)));
                    
                    //*** Check exist DENSO company code in master data
                    companyDensoCriteria = new SpsMCompanyDensoCriteriaDomain();
                    companyDensoCriteria.setDCd(splitItem.get(INDEX_DCD));
                    companyDensoCriteria.setIsActive(Constants.IS_ACTIVE);
                    int countDensoCompany = spsMCompanyDensoService.searchCount(
                        companyDensoCriteria);
                    if(Constants.ZERO == countDensoCompany){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
                    }else{
                        isValidDCd = true;
                    }
                    
                    //*** Check user have authority in this DENSO code
                    for(SpsMPlantDensoDomain densoAuthen : asnUserAuthority.getDensoAuthenList()){
                        if(densoAuthen.getDCd().equals(splitItem.get(INDEX_DCD))){
                            isValidDCdAuthen = true;
                            break;
                        }
                    }
                    
                    if(!isValidDCdAuthen){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A01);
                    }
                }
            }
            
            //*** Check blank DENSO plant code
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DPCD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F02);
            }else{
                //*** Check DENSO plant code length
                if(SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE 
                    < splitItem.get(INDEX_DPCD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F02);
                }else{
                    //Set DENSO plant code format to 2 digits
                    splitItem.set(INDEX_DPCD, StringUtil.rightPad(
                        splitItem.get(INDEX_DPCD), Constants.TWO));
                    
                    //*** Check exist DENSO plant code in master data
                    if(isValidDCd){
                        plantDensoCriteria = new SpsMPlantDensoCriteriaDomain();
                        plantDensoCriteria.setDCd(splitItem.get(INDEX_DCD));
                        plantDensoCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        plantDensoCriteria.setIsActive(Constants.IS_ACTIVE);
                        int countDensoPlant = spsMPlantDensoService.searchCount(plantDensoCriteria);
                        if(Constants.ZERO == countDensoPlant){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                        }
                    }
                    
                    //*** Check user have authority in this DENSO plant code
                    if(isValidDCd && isValidDCdAuthen){
                        boolean isValidDPcdAuthen = false;
                        for(SpsMPlantDensoDomain densoAuthen 
                            : asnUserAuthority.getDensoAuthenList()){
                            if(densoAuthen.getDCd().equals(splitItem.get(INDEX_DCD))
                                && densoAuthen.getDPcd().equals(splitItem.get(INDEX_DPCD))){
                                isValidDPcdAuthen = true;
                                break;
                            }
                        }
                        if(!isValidDPcdAuthen){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A02);
                        }
                    }
                }
            }
            
            sCd = Constants.EMPTY_STRING;
            //*** Check blank vendor code
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_VENDOR_CD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F03);
            }else{
                //*** Check vendor code length
                if(SupplierPortalConstant.LENGTH_VENDOR_CD
                    < splitItem.get(INDEX_VENDOR_CD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F03);
                }else{
                    //*** Check exist vendor code in master data
                    if(isValidDCd){
                        vendorCriteria = new SpsMAs400VendorCriteriaDomain();
                        vendorCriteria.setDCd(splitItem.get(INDEX_DCD));
                        vendorCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        vendorCriteria.setIsActive(Constants.IS_ACTIVE);
                        List<SpsMAs400VendorDomain> vendorList
                            = spsMAs400VendorService.searchByCondition(vendorCriteria);
                        if(null == vendorList || vendorList.isEmpty()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M03);
                        }else{
                            isValidVendorCd = true;
                            sCd = StringUtil.nullToEmpty(vendorList.get(Constants.ZERO).getSCd());
                        }
                        
                        //*** Check user have authority in this company supplier code
                        for(SpsMPlantSupplierDomain supplierAuthen
                            : asnUserAuthority.getSupplierAuthenList()){
                            if(supplierAuthen.getSCd().equals(sCd)){
                                isValidSCdAuthen = true;
                                break;
                            }
                        }
                        if(!isValidSCdAuthen){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A03);
                        }
                    }
                }
            }
            //*** Check blank supplier plant code
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SPCD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F04);
            }else{
                //*** Check supplier plant code length
                if(SupplierPortalConstant.LENGTH_SUPPLIER_PLANT_CODE 
                    < splitItem.get(INDEX_SPCD).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F04);
                }else{
                    //*** Check exist supplier plant code in master data
                    if(isValidVendorCd){
                        plantSupplierCriteria = new SpsMPlantSupplierCriteriaDomain();
                        plantSupplierCriteria.setSCd(sCd);
                        plantSupplierCriteria.setSPcd(splitItem.get(INDEX_SPCD));
                        plantSupplierCriteria.setIsActive(Constants.IS_ACTIVE);
                        int countSupplierPlant = spsMPlantSupplierService.searchCount(
                            plantSupplierCriteria);
                        if(Constants.ZERO == countSupplierPlant){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M04);
                        }
                        
                        //*** Check user have authority in this supplier plant code
                        if(isValidSCdAuthen){
                            boolean isValidSPcdAuthen = false;
                            for(SpsMPlantSupplierDomain supplierAuthen
                                : asnUserAuthority.getSupplierAuthenList()){
                                if(supplierAuthen.getSCd().equals(sCd)
                                    && supplierAuthen.getSPcd().equals(splitItem.get(INDEX_SPCD))){
                                    isValidSPcdAuthen = true;
                                    break;
                                }
                            }
                            if(!isValidSPcdAuthen){
                                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A04);
                            }
                        }
                    }
                }
            }
            
            //*** Check blank ASN No.
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_ASN_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
                isValidAsnNo = false;
            }else{
                //*** Check ASN No. Length
                String asnNoWithFormat =  splitItem.get(INDEX_ASN_NO);
                if(asnNoWithFormat.length() < Constants.ELEVEN 
                    || Constants.SIXTEEN < asnNoWithFormat.length())
                {
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
                    isValidAsnNo = false;
                }else{
                    //*** Check ASN No. Format
                    if(!StringUtil.isNumeric(asnNoWithFormat.substring(
                        Constants.ONE, Constants.TWO)) 
                        || !asnNoWithFormat.substring(Constants.TWO, Constants.THREE).matches(
                            Constants.REGX_ASN_NO_MONTH_FORMAT)
                        || !asnNoWithFormat.substring(Constants.THREE, Constants.FIVE).matches(
                            Constants.REGX_ASN_NO_DATE_FORMAT)
                        || !Constants.STR_U.equals(asnNoWithFormat.substring(
                            Constants.SEVEN, Constants.EIGHT))
                        || !StringUtil.isNumeric(asnNoWithFormat.substring(
                            Constants.EIGHT, Constants.TEN)))
                    {
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
                        isValidAsnNo = false;
                    }
                    
                    String sPcdFirstDigit = Constants.EMPTY_STRING;
                    String densoPlant = splitItem.get(INDEX_DPCD);
                    String vendorCd = splitItem.get(INDEX_VENDOR_CD);
                    
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SPCD))){
                        sPcdFirstDigit = splitItem.get(INDEX_SPCD)
                            .substring(Constants.ZERO, Constants.ONE);
                    }
                    
                    if(!asnNoWithFormat.substring(Constants.ZERO, Constants.ONE)
                        .equals(sPcdFirstDigit)
                        || !asnNoWithFormat.substring(Constants.FIVE, Constants.SEVEN)
                            .equals(densoPlant)
                        || !asnNoWithFormat.substring(Constants.TEN).equals(vendorCd))
                    {
                        if(!errorCodeList.contains(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
                            isValidAsnNo = false;
                        }
                    }
                }
            }
            
            if(isValidAsnNo && isValidDCd){
                //*** Check ASN No. not found in ASN Information
                AsnDomain asnDomain = new AsnDomain();
                asnDomain.setAsnNo(splitItem.get(INDEX_ASN_NO));
                asnDomain.setDCd(splitItem.get(INDEX_DCD));
                asnDomain.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                asnDomain.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                List<AsnDomain> asnDomainResultList =  asnService.searchExistAsn(asnDomain);
                if(Constants.ZERO < asnDomainResultList.size()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_D05);
                    isValidAsnNo = false;
                }
            }
            
            //*** Check blank Plan ETA
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_PLAN_ETA))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
            }else{
                //*** Check Plan ETA data type
                if(!DateUtil.isValidDate(splitItem.get(INDEX_PLAN_ETA),
                    DateUtil.PATTERN_YYYYMMDD_SLASH)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
                }
            }
            
            //*** Check blank Delivery Time
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DELIVERY_TIME))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
            }else{
                //*** Check Delivery Time data type
                if(!splitItem.get(INDEX_DELIVERY_TIME).matches(Constants.REGX_TIME24HOURS_FORMAT)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
                }
            }
            
            //*** Check blank Actual Departure
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_ACTUAL_ETD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
            }else{
                //*** Check Actual Departure data type
                if(!DateUtil.isValidDate(splitItem.get(INDEX_ACTUAL_ETD),
                    DateUtil.PATTERN_YYYYMMDD_SLASH)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
                }
            }
            
            //*** Check blank Shipping Time
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SHIPPING_TIME))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
            }else{
                //*** Check Delivery Time data type
                if(!splitItem.get(INDEX_SHIPPING_TIME).matches(Constants.REGX_TIME24HOURS_FORMAT)){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
                }
            }
            
            //*** Check blank No of Pallet.
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_NO_OF_PALLET))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
            }else{
                //*** Check No. of Pallet data type
                if(!StringUtil.isNumeric(splitItem.get(INDEX_NO_OF_PALLET))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
                }else{
                    //*** Check Digit of No. of Pallet
                    if(splitItem.get(INDEX_NO_OF_PALLET).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(
                            splitItem.get(INDEX_NO_OF_PALLET), Constants.THIRTEEN, Constants.TWO)){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
                        }
                    }else{
                        BigDecimal noOfPallet = new BigDecimal(splitItem.get(INDEX_NO_OF_PALLET));
                        if(Constants.ELEVEN < noOfPallet.precision()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
                        }
                    }
                }
            }
            
            //*** Check blank Trip No.
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_TRIP_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
            }else{
                //*** Check Trip No length
                if(!StringUtil.isPositiveInteger(splitItem.get(INDEX_TRIP_NO))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
                }else{
                    if(SupplierPortalConstant.LENGTH_TRIP_NO 
                        < splitItem.get(INDEX_TRIP_NO).length()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
                    }
                }
            }
            
            //*** Check blank Current SPS D/O No.
            boolean isValidFormatSpsDoNo = false;
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SPS_DO_NO))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
                isValidDeliveryOrder = false;
            }else{
                if(SupplierPortalConstant.LENGTH_SPS_DO_NO 
                    < splitItem.get(INDEX_SPS_DO_NO).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
                    isValidDeliveryOrder = false;
                }else{
                    isValidFormatSpsDoNo = true;
                    
                    if(Constants.STR_ZERO.equals(splitItem.get(INDEX_ORDER_METHOD))
                        || Constants.STR_ONE.equals(splitItem.get(INDEX_ORDER_METHOD))){
                        //*** Check SPS D/O No. not found in D/O transaction
                        doCriteria = new DeliveryOrderInformationDomain();
                        doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        
                        // Start : [IN034] add more criteria to search D/O
                        doCriteria.setDCd(splitItem.get(INDEX_DCD));
                        doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        // End : [IN034] add more criteria to search D/O
                        
                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                            deliveryOrderService.searchExistDo(doCriteria);
                        if(null != deliveryOrderInfoList 
                            && Constants.ZERO < deliveryOrderInfoList.size())
                        {
                            for(DeliveryOrderDetailDomain deliveryOrderInfo : deliveryOrderInfoList)
                            {
                                DeliveryOrderSubDetailDomain doHeader 
                                    = deliveryOrderInfo.getDeliveryOrderSubDetailDomain();
                                
                                // [IN012] Shipment Status CPS and CCL cannot create ASN
                                //if(Constants.SHIPMENT_STATUS_CPS.equals(
                                //    doHeader.getShipmentStatus())){
                                if(Constants.SHIPMENT_STATUS_CPS.equals(
                                    doHeader.getShipmentStatus())
                                    || Constants.SHIPMENT_STATUS_CCL.equals(
                                        doHeader.getShipmentStatus()))
                                {
                                    
                                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
                                    isValidDeliveryOrder = false;
                                }
                            }
                        }else{
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
                            isValidDeliveryOrder = false;
                        }
                        
                        //** Check P/O status of SPS D/O No.
                        SpsTPoDomain poDomain =
                            deliveryOrderService.searchDeliveryOrderAcknowledgement(doCriteria);
                        if(null != poDomain && !StringUtil.checkNullOrEmpty(poDomain.getPoStatus()))
                        {
                            if(!Constants.PO_STATUS_ACKNOWLEDGE.equals(poDomain.getPoStatus())
                                && !Constants.PO_STATUS_FORCE_ACKNOWLEDGE.equals(
                                    poDomain.getPoStatus()))
                            {
                                if(!errorCodeList.contains(
                                    SupplierPortalConstant.UPLOAD_ERROR_CODE_T12)){
                                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
                                    isValidDeliveryOrder = false;
                                }
                            }
                        }
                    }
                }
            }
            
            //*** Check blank Order Method
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_ORDER_METHOD))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F13);
                isValidDeliveryOrder = false;
            }else{
                if(!Constants.STR_ZERO.equals(splitItem.get(INDEX_ORDER_METHOD)) 
                    && !Constants.STR_ONE.equals(splitItem.get(INDEX_ORDER_METHOD))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F13);
                    isValidDeliveryOrder = false;
                }else{
                    if(isValidFormatSpsDoNo){
                        //*** Check Order Method match with D/O transaction
                        doCriteria = new DeliveryOrderInformationDomain();
                        doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        
                        // Start : [IN034] add more criteria to search D/O
                        doCriteria.setDCd(splitItem.get(INDEX_DCD));
                        doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        // End : [IN034] add more criteria to search D/O
                        
                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                            deliveryOrderService.searchExistDo(doCriteria);
                        if(null == deliveryOrderInfoList || deliveryOrderInfoList.isEmpty()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T13);
                            isValidDeliveryOrder = false;
                        }
                    }
                }
            }
            
            //*** Check blank Revision
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_REVISION))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F14);
            }else{
                if(SupplierPortalConstant.LENGTH_REVISION 
                    < splitItem.get(INDEX_REVISION).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F14);
                }else{
                    if(isValidDeliveryOrder){
                        //*** Check Revision in not latest revision
                        doCriteria = new DeliveryOrderInformationDomain();
                        doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        
                        // Start : [IN034] add more criteria to search D/O
                        doCriteria.setDCd(splitItem.get(INDEX_DCD));
                        doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        // End : [IN034] add more criteria to search D/O
                        
                        doCriteria.setRevision(splitItem.get(INDEX_REVISION));
                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                            deliveryOrderService.searchExistDo(doCriteria);
                        if(null == deliveryOrderInfoList || deliveryOrderInfoList.isEmpty()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T14);
                        }
                    }
                }
            }
            
            //*** Check TRANs length
            if(Constants.ONE < splitItem.get(INDEX_TRANS).length()){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F15);
            }else{
                splitItem.set(INDEX_TRANS,
                    StringUtil.rightPad(splitItem.get(INDEX_TRANS), Constants.ONE));
                
                //*** Check TRANs match with D/O transaction
                if(isValidDeliveryOrder){
                    doCriteria = new DeliveryOrderInformationDomain();
                    doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                    doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                    
                    // Start : [IN034] add more criteria to search D/O
                    doCriteria.setDCd(splitItem.get(INDEX_DCD));
                    doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                    doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                    // End : [IN034] add more criteria to search D/O
                    
                    doCriteria.setTransportMode(splitItem.get(INDEX_TRANS));
                    doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                    doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                    List<DeliveryOrderDetailDomain> deliveryOrderInfoList 
                        = deliveryOrderService.searchExistDo(doCriteria);
                    if(null == deliveryOrderInfoList || deliveryOrderInfoList.isEmpty()){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T15);
                    }
                }
            }
            
            
            
            //*** Check blank DENNO Part No.
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_DPN))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
                isValidDPn = false;
            }else{
                //*** Check DENSO Part No. length
                if(SupplierPortalConstant.LENGTH_DENSO_PART_NO < splitItem.get(INDEX_DPN).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
                    isValidDPn = false;
                }else{
                    if(isValidDeliveryOrder){
                        doCriteria = new DeliveryOrderInformationDomain();
                        doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        
                        // Start : [IN034] add more criteria to search D/O
                        doCriteria.setDCd(splitItem.get(INDEX_DCD));
                        doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        // End : [IN034] add more criteria to search D/O
                        
                        doCriteria.setDPn(splitItem.get(INDEX_DPN));
                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                            deliveryOrderService.searchExistDo(doCriteria);
                        
                        if(null != deliveryOrderInfoList 
                            && Constants.ZERO < deliveryOrderInfoList.size()){
                            for(DeliveryOrderDetailDomain deliveryOrderInfo : deliveryOrderInfoList)
                            {
                                for(DeliveryOrderDoDetailDomain deliveryOrderDetail 
                                    : deliveryOrderInfo.getDeliveryOrderDoDetailDomain()){

                                    // [IN012] Shipment Status CPS and CCL cannot create ASN
                                    //if(Constants.SHIPMENT_STATUS_CPS.equals(
                                    //    deliveryOrderDetail.getPnShipmentStatus())){
                                    if(Constants.SHIPMENT_STATUS_CPS.equals(
                                        deliveryOrderDetail.getPnShipmentStatus())
                                        || Constants.SHIPMENT_STATUS_CCL.equals(
                                            deliveryOrderDetail.getPnShipmentStatus()))
                                    {
                                        
                                        errorCodeList.add(
                                            SupplierPortalConstant.UPLOAD_ERROR_CODE_T16);
                                        isValidDPn = false;
                                        break;
                                    }
                                }
                            }
                        }else{
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T16);
                            isValidDPn = false;
                        }
                    }
                }
            }
            
            //*** Check blank Supplier Part No.
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SPN))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
            }else{
                //*** Check Supplier Part No. length
                if(SupplierPortalConstant.LENGTH_SUPPLIER_PART_NO 
                    < splitItem.get(INDEX_SPN).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
                }else{
                    if(isValidDeliveryOrder){
                        //*** Check Supplier Part No. not found in D/O transaction
                        doCriteria = new DeliveryOrderInformationDomain();
                        doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        
                        // Start : [IN034] add more criteria to search D/O
                        doCriteria.setDCd(splitItem.get(INDEX_DCD));
                        doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        // End : [IN034] add more criteria to search D/O
                        
                        doCriteria.setSPn(splitItem.get(INDEX_SPN));
                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                            deliveryOrderService.searchExistDo(doCriteria);
                        if(null == deliveryOrderInfoList 
                            || Constants.ZERO == deliveryOrderInfoList.size()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T17);
                        }
                    }
                }
            }
            
            //*** Check RCV Lane length
            if(SupplierPortalConstant.LENGTH_RCV_LANE 
                < splitItem.get(INDEX_RCV_LANE).length()){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
            }else{
                splitItem.set(INDEX_RCV_LANE,
                    StringUtil.rightPad(splitItem.get(INDEX_RCV_LANE), Constants.THIRTEEN));
                
                //*** Check RCV Lane not match in D/O transaction data
                if(isValidDeliveryOrder && isValidDPn){
                    doCriteria = new DeliveryOrderInformationDomain();
                    doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                    doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                    
                    // Start : [IN034] add more criteria to search D/O
                    doCriteria.setDCd(splitItem.get(INDEX_DCD));
                    doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                    doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                    // End : [IN034] add more criteria to search D/O
                    
                    doCriteria.setDPn(splitItem.get(INDEX_DPN));
                    doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                    doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                    List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                        deliveryOrderService.searchExistDo(doCriteria);
                    
                    boolean hasRcvLane = false;
                    if(null != deliveryOrderInfoList 
                        && Constants.ZERO < deliveryOrderInfoList.size()){
                        for(DeliveryOrderDetailDomain deliveryOrderInfo : deliveryOrderInfoList)
                        {
                            for(DeliveryOrderDoDetailDomain deliveryOrderDetail 
                                : deliveryOrderInfo.getDeliveryOrderDoDetailDomain()){
                                if(deliveryOrderDetail.getRcvLane().equals(
                                    splitItem.get(INDEX_RCV_LANE)))
                                {
                                    hasRcvLane = true;
                                    break;
                                }
                            }
                        }
                    }
                    if(!hasRcvLane){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T18);
                    }
                }
            }
            
            //*** Check blank Shipping QTY.
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_SHIPPING_QTY))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
            }else{
                //*** Check Shipping QTY data type
                if(!StringUtil.isNumeric(splitItem.get(INDEX_SHIPPING_QTY))){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
                }else{
                    //*** Check Shipping QTY. length
                    boolean isNumberFormat = true;
                    BigDecimal shippingQty = new BigDecimal(splitItem.get(INDEX_SHIPPING_QTY));
                    if(splitItem.get(INDEX_SHIPPING_QTY).contains(Constants.SYMBOL_DOT)){
                        if(!StringUtil.isNumberFormat(
                            splitItem.get(INDEX_SHIPPING_QTY), Constants.NINE, Constants.TWO)){
                            isNumberFormat = false;
                        }
                    }else{
                        if(Constants.SEVEN < shippingQty.precision()){
                            isNumberFormat = false;
                        }
                    }
                    if(!isNumberFormat){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
                    }
                    //*** Check Shipping QTY < 0
                    if(shippingQty.compareTo(new BigDecimal(Constants.ZERO)) < Constants.ZERO){
                        errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T19);
                    }
                }
            }
            
            //*** Check blank U/M
            if(StringUtil.checkNullOrEmpty(splitItem.get(INDEX_UM))){
                errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
            }else{
                //*** Check U/M length
                if(SupplierPortalConstant.LENGTH_UM 
                    < splitItem.get(INDEX_UM).length()){
                    errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
                }else{
                    if(isValidDeliveryOrder && isValidDPn){
                        //*** Check U/M match with D/O transaction
                        doCriteria = new DeliveryOrderInformationDomain();
                        doCriteria.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        
                        // Start : [IN034] add more criteria to search D/O
                        doCriteria.setDCd(splitItem.get(INDEX_DCD));
                        doCriteria.setDPcd(splitItem.get(INDEX_DPCD));
                        doCriteria.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        // End : [IN034] add more criteria to search D/O
                        
                        doCriteria.setDPn(splitItem.get(INDEX_DPN));
                        doCriteria.setUnitOfMeasure(splitItem.get(INDEX_UM));
                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
                            deliveryOrderService.searchExistDo(doCriteria);
                        if(null == deliveryOrderInfoList || deliveryOrderInfoList.isEmpty()){
                            errorCodeList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T20);
                        }
                        AcknowledgedDoInformationDomain acknowledgedDOInformationDomain = new AcknowledgedDoInformationDomain();
                        acknowledgedDOInformationDomain.setDCd(fillData(splitItem.get(INDEX_DCD), SupplierPortalConstant.LENGTH_DENSO_CODE));
                        acknowledgedDOInformationDomain.setDPcd(fillData(splitItem.get(INDEX_DPCD), SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE));
                        acknowledgedDOInformationDomain.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                        acknowledgedDOInformationDomain.setSPcd(splitItem.get(INDEX_SPCD));
                        acknowledgedDOInformationDomain.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                        acknowledgedDOInformationDomain.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                        acknowledgedDOInformationDomain.setShipmentStatus(Constants.MISC_CODE_ALL);
                        acknowledgedDOInformationDomain.setSupplierAuthenList(asnUploadingCriteria.getSupplierAuthenList());
                        acknowledgedDOInformationDomain.setDensoAuthenList(asnUploadingCriteria.getDensoAuthenList());
                        
                        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInformationResultList = deliveryOrderService.
                            searchAcknowledgedDo(acknowledgedDOInformationDomain);
                        if(Constants.ZERO == acknowledgedDOInformationResultList.size()){
                            MessageUtil.throwsApplicationMessage(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0042,
                                splitItem.get(INDEX_SPS_DO_NO));
                        }
                    }
                }
            }
            
            StringBuffer planEtaStr = new StringBuffer();
            tmpUploadAsnDomain = new SpsTmpUploadAsnDomain();
            if(Constants.ZERO < errorCodeList.size()){
                String errorCodeStr = Constants.EMPTY_STRING;
                for(String errorCode : errorCodeList){
                    errorCodeStr = StringUtil.appendsString(
                        errorCodeStr, errorCode, Constants.SYMBOL_COMMA, Constants.SYMBOL_SPACE);
                }
                errorCodeStr = errorCodeStr.trim();
                
                tmpUploadAsnDomain.setSessionId(asnUploadingCriteria.getSessionId());
                tmpUploadAsnDomain.setCsvLineNo(String.valueOf(csvLineNo));
                tmpUploadAsnDomain.setIsErrorFlg(Constants.ERROR_FLAG_ERROR);
                tmpUploadAsnDomain.setErrorCode(errorCodeStr.substring(
                    Constants.ZERO, errorCodeStr.length() - Constants.ONE));
                tmpUploadAsnDomain.setUploadDscId(asnUploadingCriteria.getUploadDscId());
                tmpUploadAsnDomain.setUploadDatetime(updateDateTime);
            }else{
                tmpUploadAsnDomain.setSessionId(asnUploadingCriteria.getSessionId());
                tmpUploadAsnDomain.setCsvLineNo(String.valueOf(csvLineNo));
                tmpUploadAsnDomain.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
                tmpUploadAsnDomain.setSPcd(splitItem.get(INDEX_SPCD));
                tmpUploadAsnDomain.setDCd(splitItem.get(INDEX_DCD));
                tmpUploadAsnDomain.setDPcd(splitItem.get(INDEX_DPCD));
                tmpUploadAsnDomain.setSpsDoNo(splitItem.get(INDEX_SPS_DO_NO));
                tmpUploadAsnDomain.setRevision(splitItem.get(INDEX_REVISION));
                tmpUploadAsnDomain.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
                tmpUploadAsnDomain.setTm(splitItem.get(INDEX_TRANS));
                planEtaStr.append(splitItem.get(INDEX_PLAN_ETA));
                planEtaStr.append(Constants.SYMBOL_SPACE);
                planEtaStr.append(splitItem.get(INDEX_DELIVERY_TIME));
                Date planEtaDate = DateUtil.parseToUtilDate(
                    planEtaStr.toString(), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
                Date actualEtd = DateUtil.parseToUtilDate(
                    StringUtil.appendsString(splitItem.get(INDEX_ACTUAL_ETD),
                        Constants.SYMBOL_SPACE, splitItem.get(INDEX_SHIPPING_TIME)), 
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
                tmpUploadAsnDomain.setPlanEta(new Timestamp(planEtaDate.getTime()));
                tmpUploadAsnDomain.setActualEtd(new Timestamp(actualEtd.getTime()));
                tmpUploadAsnDomain.setTripNo(splitItem.get(INDEX_TRIP_NO));
                tmpUploadAsnDomain.setNumberOfPallet(new BigDecimal(
                    splitItem.get(INDEX_NO_OF_PALLET)));
                tmpUploadAsnDomain.setDPn(splitItem.get(INDEX_DPN));
                tmpUploadAsnDomain.setSPn(splitItem.get(INDEX_SPN));
                tmpUploadAsnDomain.setShippingQty(new BigDecimal(
                    splitItem.get(INDEX_SHIPPING_QTY)));
                tmpUploadAsnDomain.setUnitOfMeasure(splitItem.get(INDEX_UM));
                tmpUploadAsnDomain.setRcvLane(splitItem.get(INDEX_RCV_LANE));
                tmpUploadAsnDomain.setAsnNo(splitItem.get(INDEX_ASN_NO));
                tmpUploadAsnDomain.setIsErrorFlg(Constants.ERROR_FLAG_CORRECT);
                tmpUploadAsnDomain.setUploadDscId(asnUploadingCriteria.getUploadDscId());
                tmpUploadAsnDomain.setUploadDatetime(updateDateTime);
            }
            tmpUploadAsnInsertList.add(tmpUploadAsnDomain);
            csvLineNo += Constants.ONE;
        }//end while
        
        this.sortTmpUploadAsnList(tmpUploadAsnInsertList);
        /**
         * 4.2 Keep validate result to temporary table
         **/
        spsTmpUploadAsnService.create(tmpUploadAsnInsertList);
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingCriteria.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingCriteria.getUploadDscId());
        int countInsert = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        if(countInsert != tmpUploadAsnInsertList.size()){
            throw new ApplicationException(MessageUtil.getApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                SupplierPortalConstant.LBL_UPLOAD_CANNOT_INSERT_IN_TMP_TABLE));
        }
        
        /**
         * 5. Validate uploaded data
         **/
        splitItem = asnInfoUploadingList.get(Constants.ZERO);
        //** Check Duplicate DENSO Part No.
        String asnNo = splitItem.get(INDEX_ASN_NO);
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingCriteria.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingCriteria.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_CORRECT);
        tmpUploadAsnDomainList = spsTmpUploadAsnService.searchByCondition(tmpUploadAsnCriteria);
        if(!tmpUploadAsnDomainList.isEmpty()){
            StringBuffer spsDoNoAndDPnStr = new StringBuffer(Constants.EMPTY_STRING);
            List<String> spsDoNoAndDPnList = new ArrayList<String>();
            itr = tmpUploadAsnDomainList.iterator();
            while(itr.hasNext()){
                SpsTmpUploadAsnDomain tmpUploadAsn = (SpsTmpUploadAsnDomain)itr.next();
                
                spsDoNoAndDPnStr.append(tmpUploadAsn.getSpsDoNo())
                    .append(tmpUploadAsn.getRevision()).append(Constants.SYMBOL_UNDER_SCORE)
                    .append(tmpUploadAsn.getDPn());
                if(spsDoNoAndDPnList.contains(spsDoNoAndDPnStr.toString())){
                    errorMsgList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, 
                        MessageUtil.getApplicationMessageWithLabel(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0006, 
                            SupplierPortalConstant.LBL_UPLOAD_SAME_SPSDONO_DPN)));
                    break;
                }else{
                    spsDoNoAndDPnList.add(spsDoNoAndDPnStr.toString());
                }
                spsDoNoAndDPnStr.setLength(Constants.ZERO);
            }
        }
        asnUploadingDomainResult = this.searchTempUploadAsn(asnUploadingCriteria);
        asnUploadingDomainResult.setAsnNo(asnNo);
        asnUploadingDomainResult.setVendorCd(splitItem.get(INDEX_VENDOR_CD));
        asnUploadingDomainResult.setSPcd(splitItem.get(INDEX_SPCD));
        asnUploadingDomainResult.setDCd(splitItem.get(INDEX_DCD));
        asnUploadingDomainResult.setDPcd(splitItem.get(INDEX_DPCD));
        if(Constants.ZERO < errorMsgList.size()){
            asnUploadingDomainResult.setErrorMessageList(errorMsgList);
        }
        if(Constants.ZERO < warningMsgList.size()){
            asnUploadingDomainResult.setWarningMessageList(warningMsgList);
        }
        if(Constants.ZERO < asnUploadingDomainResult.getTmpUploadAsnResultList().size()){
            //set shipping qty for screen
            String shippingQty = Constants.EMPTY_STRING;
            for(AsnUploadingReturnDomain tmpUploadAsnResult 
                : asnUploadingDomainResult.getTmpUploadAsnResultList()){
                if(null != tmpUploadAsnResult.getTmpUploadAsnDomain().getShippingQty()){
                    shippingQty = StringUtil.toCurrencyFormat(
                        tmpUploadAsnResult.getTmpUploadAsnDomain().getShippingQty().toString());
                    tmpUploadAsnResult.setShippingQtyStr(shippingQty);
                    /* Start to get information from SPS_T_DO */
                    DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
                    deliveryOrderInformationDomain.setSpsDoNo(tmpUploadAsnResult.getTmpUploadAsnDomain().getSpsDoNo());
                    deliveryOrderInformationDomain.setDCd(tmpUploadAsnResult.getTmpUploadAsnDomain().getDCd());
                    deliveryOrderInformationDomain.setDPcd(tmpUploadAsnResult.getTmpUploadAsnDomain().getDPcd());
                    deliveryOrderInformationDomain.setVendorCd(tmpUploadAsnResult.getTmpUploadAsnDomain().getVendorCd());
                    deliveryOrderInformationDomain.setSPcd(tmpUploadAsnResult.getTmpUploadAsnDomain().getSPcd());
                    List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = deliveryOrderService.searchExistDo(deliveryOrderInformationDomain);
                    /* End to get information from SPS_T_DO */
                    if(deliveryOrderDetailDomain.size() > Constants.ZERO){
                        tmpUploadAsnResult.setReceiveByScan(deliveryOrderDetailDomain.get(Constants.ZERO).getDeliveryOrderSubDetailDomain().getReceiveByScan());
                        tmpUploadAsnResult.setWhPrimeReceiving(deliveryOrderDetailDomain.get(Constants.ZERO).getDeliveryOrderSubDetailDomain().getWhPrimeReceiving());
                    }
                }
            }
        }
        return asnUploadingDomainResult;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService#searchTmpUploadAsnCsv(com.globaldenso.asia.sps.business.domain.AsnUploadingDomain)
     */
    public void searchTmpUploadAsnCsv(AsnUploadingDomain asnUploadingDomain) 
        throws ApplicationException {
        Locale locale = asnUploadingDomain.getLocale();
        CommonDomain commonDomain = new CommonDomain();
        StringBuffer resultString = new StringBuffer();
        SpsTmpUploadAsnCriteriaDomain tmpUploadAsnCriteria = null;
        AsnUploadingReturnDomain asnUploadingReturn = null;
        List<AsnUploadingReturnDomain> asnUploadingReturnList = 
            new ArrayList<AsnUploadingReturnDomain>();
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        List<SpsTmpUploadAsnDomain> tmpUploadAsnList = spsTmpUploadAsnService
            .searchByCondition(tmpUploadAsnCriteria);
        if(null == tmpUploadAsnList || tmpUploadAsnList.isEmpty()){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }else{
            this.sortTmpUploadAsnList(tmpUploadAsnList);
        }
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        int recordCount = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_CORRECT);
        int iCorrectRecord = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria); 
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_WARNING);
        int iWarningRecord = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria); 
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_ERROR);
        int iErrorRecord = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        
        for(SpsTmpUploadAsnDomain tmpUploadAsn : tmpUploadAsnList){
            asnUploadingReturn =  new AsnUploadingReturnDomain();
            asnUploadingReturn.setTmpUploadAsnDomain(tmpUploadAsn);
            asnUploadingReturnList.add(asnUploadingReturn);
        }
        asnUploadingDomain.setTotalRecord(recordCount);
        asnUploadingDomain.setCorrectRecord(iCorrectRecord);
        asnUploadingDomain.setWarningRecord(iWarningRecord);
        asnUploadingDomain.setErrorRecord(iErrorRecord);
        asnUploadingDomain.setTmpUploadAsnResultList(asnUploadingReturnList);
        
        final String[] headerArr = new String[] {
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_CD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_PCD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_CD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_PCD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ASN_NO),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_TOTAL_RECORD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_CORRECT_RECORD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ERROR_RECORD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_WARNING_RECORD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_CSV_LINE_NO), 
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_UPLOAD_RESULT),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_CURRENT_SPS_DO_NO),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_REVISION), 
//            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ORDER_METHOD),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_RECEIVING_LANE),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_PN),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_PN),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_SHIPPING_QTY),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_UM),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_TM),
            MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_ERROR_MESSAGE)};
        
        List<Map<String, Object>> asnInfoCsvMapList 
            = this.doSetUploadingAsnInformationCsv(asnUploadingDomain, headerArr);
        commonDomain.setResultList(asnInfoCsvMapList);
        commonDomain.setHeaderArr(headerArr);
        commonDomain.setHeaderFlag(true);
        
        try{
            resultString = commonService.createCsvString(commonDomain);
        }catch(IOException ioe){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0012,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        asnUploadingDomain.setAsnInformationForCsv(resultString);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService#searchByReturn(com.globaldenso.asia.sps.business.domain.AsnUploadingDomain)
     */
    public AsnUploadingDomain searchByReturn(AsnUploadingDomain asnUploadingCriteria)
        throws ApplicationException
    {
        AsnUploadingDomain asnUploadingResult = null;
        asnUploadingResult = this.searchTempUploadAsn(asnUploadingCriteria);
        
        if(Constants.ZERO < asnUploadingResult.getTmpUploadAsnResultList().size()){
            AsnUploadingReturnDomain uploadAsnResult = asnUploadingResult
                .getTmpUploadAsnResultList().get(Constants.ZERO);
            asnUploadingResult.setAsnNo(uploadAsnResult.getTmpUploadAsnDomain().getAsnNo());
            asnUploadingResult.setVendorCd(uploadAsnResult.getTmpUploadAsnDomain().getVendorCd());
            asnUploadingResult.setSPcd(uploadAsnResult.getTmpUploadAsnDomain().getSPcd());
            asnUploadingResult.setDCd(uploadAsnResult.getTmpUploadAsnDomain().getDCd());
            asnUploadingResult.setDPcd(uploadAsnResult.getTmpUploadAsnDomain().getDPcd());
            
            /* Start to get information from SPS_T_DO */
            DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
            deliveryOrderInformationDomain.setSpsDoNo(uploadAsnResult.getTmpUploadAsnDomain().getSpsDoNo());
            deliveryOrderInformationDomain.setDCd(uploadAsnResult.getTmpUploadAsnDomain().getDCd());
            deliveryOrderInformationDomain.setDPcd(uploadAsnResult.getTmpUploadAsnDomain().getDPcd());
            deliveryOrderInformationDomain.setVendorCd(uploadAsnResult.getTmpUploadAsnDomain().getVendorCd());
            deliveryOrderInformationDomain.setSPcd(uploadAsnResult.getTmpUploadAsnDomain().getSPcd());
            List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = deliveryOrderService.searchExistDo(deliveryOrderInformationDomain);
            /* End to get information from SPS_T_DO */
            //set shipping qty for screen
            String shippingQty = Constants.EMPTY_STRING;
            for(AsnUploadingReturnDomain tmpUploadAsnResult 
                : asnUploadingResult.getTmpUploadAsnResultList()){
                if(null != tmpUploadAsnResult.getTmpUploadAsnDomain().getShippingQty()){
                    shippingQty = StringUtil.toCurrencyFormat(
                        tmpUploadAsnResult.getTmpUploadAsnDomain().getShippingQty().toString());
                    tmpUploadAsnResult.setShippingQtyStr(shippingQty);
                    tmpUploadAsnResult.setReceiveByScan(deliveryOrderDetailDomain.get(Constants.ZERO).getDeliveryOrderSubDetailDomain().getReceiveByScan());
                    tmpUploadAsnResult.setWhPrimeReceiving(deliveryOrderDetailDomain.get(Constants.ZERO).getDeliveryOrderSubDetailDomain().getWhPrimeReceiving());
                }
            }
        }
        return asnUploadingResult;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService#searchRoleCanOperate(java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService#searchDensoSupplierRelation(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public void searchDensoSupplierRelation(
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(null != densoSupplierRelationList && Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
    }
    
    /**
     * Search Temp Upload Asn
     * @param asnUploadingDomain the data asn uploading domain
     * @return the ans uploading domain
     * @throws ApplicationException ApplicationException
     * */
    private AsnUploadingDomain searchTempUploadAsn(AsnUploadingDomain asnUploadingDomain) 
        throws ApplicationException
    {
        Locale locale = asnUploadingDomain.getLocale();
        AsnUploadingDomain asnUploadingDomainResult = new AsnUploadingDomain();
        AsnUploadingReturnDomain asnUploadingResult = null;
        SpsTmpUploadAsnCriteriaDomain tmpUploadAsnCriteria = null;
        List<AsnUploadingReturnDomain> asnUploadingResultList
            = new ArrayList<AsnUploadingReturnDomain>();
        
        MiscellaneousDomain recordLimit = null;
        MiscellaneousDomain criteria = new MiscellaneousDomain();
        criteria.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        criteria.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP006_PLM);
        recordLimit = recordLimitService.searchRecordLimitPerPage(criteria);
        if(null == recordLimit || StringUtil.checkNullOrEmpty(recordLimit.getMiscValue())){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032);
        }
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        int countTempUploadAsn = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        if(Constants.ZERO == countTempUploadAsn){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001);
        }
        asnUploadingDomainResult.setPageNumber(asnUploadingDomain.getPageNumber());
        asnUploadingDomainResult.setMaxRowPerPage(Integer.valueOf(recordLimit.getMiscValue()));
        SpsPagingUtil.calcPaging(asnUploadingDomainResult, countTempUploadAsn);
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setRowNumFrom(asnUploadingDomainResult.getRowNumFrom());
        tmpUploadAsnCriteria.setRowNumTo(asnUploadingDomainResult.getRowNumTo());
        List<SpsTmpUploadAsnDomain> tmpUploadAsnList = spsTmpUploadAsnService.
            searchByConditionForPaging(tmpUploadAsnCriteria);
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_CORRECT);
        int iCorrectRecord = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_WARNING);
        int iWarningRecord = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        
        tmpUploadAsnCriteria = new SpsTmpUploadAsnCriteriaDomain();
        tmpUploadAsnCriteria.setSessionId(asnUploadingDomain.getSessionId());
        tmpUploadAsnCriteria.setUploadDscId(asnUploadingDomain.getUploadDscId());
        tmpUploadAsnCriteria.setIsErrorFlg(Constants.ERROR_FLAG_ERROR);
        int iErrorRecord = spsTmpUploadAsnService.searchCount(tmpUploadAsnCriteria);
        
        this.sortTmpUploadAsnList(tmpUploadAsnList);
        if(Constants.ZERO < tmpUploadAsnList.size()){
            for(SpsTmpUploadAsnDomain tmpUploadAsn : tmpUploadAsnList){
                asnUploadingResult = new AsnUploadingReturnDomain();
                asnUploadingResult.setTmpUploadAsnDomain(tmpUploadAsn);
                asnUploadingResultList.add(asnUploadingResult);
            }
        }
        asnUploadingDomainResult.setTotalRecord(countTempUploadAsn);
        asnUploadingDomainResult.setCorrectRecord(iCorrectRecord);
        asnUploadingDomainResult.setWarningRecord(iWarningRecord);
        asnUploadingDomainResult.setErrorRecord(iErrorRecord);
        asnUploadingDomainResult.setTmpUploadAsnResultList(asnUploadingResultList);
        return asnUploadingDomainResult;
    }
    
    /**
     * Sorting Temp upload asn list (sort by error code, csv line no);
     * @param tmpUploadAsnList the list of tmp upload asn domain
     */
    private void sortTmpUploadAsnList(List<SpsTmpUploadAsnDomain> tmpUploadAsnList)
    {
        boolean flag = true;
        BigDecimal csvLineNoCurrenct = new BigDecimal(Constants.ZERO);
        BigDecimal csvLineNoNext = new BigDecimal(Constants.ZERO);
        SpsTmpUploadAsnDomain tmpAsnCurrent = null;
        SpsTmpUploadAsnDomain tmpAsnNext = null;
        
        while(flag){
            flag = false;
            for(int i = Constants.ZERO; i < tmpUploadAsnList.size() - Constants.ONE; i++){
                tmpAsnCurrent = tmpUploadAsnList.get(i);
                tmpAsnNext = tmpUploadAsnList.get(i + Constants.ONE);
                int result = tmpAsnCurrent.getIsErrorFlg().compareTo(tmpAsnNext.getIsErrorFlg());
                result = result * Constants.MINUS_ONE;
                if(Constants.ZERO == result){
                    csvLineNoCurrenct = new BigDecimal(tmpAsnCurrent.getCsvLineNo());
                    csvLineNoNext = new BigDecimal(tmpAsnNext.getCsvLineNo());
                    result = csvLineNoCurrenct.compareTo(csvLineNoNext);
                }
                
                if(Constants.ZERO < result){
                    tmpUploadAsnList.remove(i);
                    tmpUploadAsnList.add(i, tmpAsnNext);
                    tmpUploadAsnList.remove(i + Constants.ONE);
                    tmpUploadAsnList.add(i + Constants.ONE, tmpAsnCurrent);
                    flag = true;
                }
            }
        }
    }
    
    /**
     * Do set uploading asn information Csv.
     * 
     * @param asnUploadingDomain the asn uploading domain
     * @param header the List
     * @return resultList 
     */
    private List<Map<String, Object>> doSetUploadingAsnInformationCsv(
        AsnUploadingDomain asnUploadingDomain, String[] header) {
        String errorResult = Constants.EMPTY_STRING;
        StringBuffer errorCode = null;
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        List<AsnUploadingReturnDomain> tmpUploadAsnList 
            = asnUploadingDomain.getTmpUploadAsnResultList();
        Map<String, Object> tmpUploadAsnMap = null;
        
        for(AsnUploadingReturnDomain tmpUploadAsn : tmpUploadAsnList){
            tmpUploadAsnMap = new HashMap<String, Object>();
            
            errorCode = new StringBuffer(Constants.EMPTY_STRING);
            tmpUploadAsnMap.put(header[Constants.ZERO], StringUtil.checkNullToEmpty(
                asnUploadingDomain.getVendorCd()));
            tmpUploadAsnMap.put(header[Constants.ONE], StringUtil.checkNullToEmpty(
                asnUploadingDomain.getSPcd()));
            tmpUploadAsnMap.put(header[Constants.TWO], StringUtil.checkNullToEmpty(
                asnUploadingDomain.getDCd()));
            tmpUploadAsnMap.put(header[Constants.THREE], StringUtil.checkNullToEmpty(
                asnUploadingDomain.getDPcd()));
            tmpUploadAsnMap.put(header[Constants.FOUR], StringUtil.checkNullToEmpty(
                asnUploadingDomain.getAsnNo()));
            tmpUploadAsnMap.put(header[Constants.FIVE], 
                String.valueOf(asnUploadingDomain.getTotalRecord()));
            tmpUploadAsnMap.put(header[Constants.SIX], 
                String.valueOf(asnUploadingDomain.getCorrectRecord()));
            tmpUploadAsnMap.put(header[Constants.SEVEN], 
                String.valueOf(asnUploadingDomain.getErrorRecord()));
            tmpUploadAsnMap.put(header[Constants.EIGHT], 
                String.valueOf(asnUploadingDomain.getWarningRecord()));
            tmpUploadAsnMap.put(header[Constants.NINE],
                tmpUploadAsn.getTmpUploadAsnDomain().getCsvLineNo());
            if(Constants.ERROR_FLAG_CORRECT.equals(tmpUploadAsn.getTmpUploadAsnDomain()
                .getIsErrorFlg())  || Constants.ERROR_FLAG_WARNING.equals(tmpUploadAsn
                    .getTmpUploadAsnDomain().getIsErrorFlg())){
                errorResult = Constants.STR_OK;
            }else{
                errorResult = Constants.STR_NG;
            }
            tmpUploadAsnMap.put(header[Constants.TEN], errorResult);
            tmpUploadAsnMap.put(header[Constants.ELEVEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getSpsDoNo()));
            tmpUploadAsnMap.put(header[Constants.TWELVE], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getRevision()));
//            tmpUploadAsnMap.put(header[Constants.THIRTEEN], StringUtil.checkNullToEmpty(
//                tmpUploadAsn.getTmpUploadAsnDomain().getOrderMethod()));
            tmpUploadAsnMap.put(header[Constants.THIRTEEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getRcvLane()));
            tmpUploadAsnMap.put(header[Constants.FOURTEEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getDPn()));
            tmpUploadAsnMap.put(header[Constants.FIFTEEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getSPn()));
            tmpUploadAsnMap.put(header[Constants.SIXTEEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getShippingQty()));
            tmpUploadAsnMap.put(header[Constants.SEVENTEEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getUnitOfMeasure()));
            tmpUploadAsnMap.put(header[Constants.EIGHTEEN], StringUtil.checkNullToEmpty(
                tmpUploadAsn.getTmpUploadAsnDomain().getTm()));
            if(null != tmpUploadAsn.getTmpUploadAsnDomain().getErrorCode() 
                && Constants.ZERO < tmpUploadAsn.getTmpUploadAsnDomain().getErrorCode().length()){
                errorCode = errorCode.append(Constants.SYMBOL_DOUBLE_QUOTE)
                    .append(tmpUploadAsn.getTmpUploadAsnDomain().getErrorCode())
                    .append(Constants.SYMBOL_DOUBLE_QUOTE);
            }
            tmpUploadAsnMap.put(header[Constants.NINETEEN], errorCode);
            resultList.add(tmpUploadAsnMap);
        }
        return resultList;
    }
    
    /**
     * If parameter input shorter than parameter len. Append input with space.
     * @param input - String input
     * @param len - target length
     * @return String that lenght equal to parameter len
     * */
    private String fillData(String input, int len) {
        StringBuffer result = new StringBuffer();
        result.append(input);
        for (int i = input.length(); i < len; i++) {
            result.append(Constants.SYMBOL_SPACE);
        }
        return result.toString();
    }
}