/*
 * ModifyDate Development company     Describe 
 * 2014/07/03 CSI Phakaporn           Create
 * 2016/02/17 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;

/**
 * <p>The Interface Purchase Order Acknowledgement Facade Service.</p>
 * <p>Service for Purchase Order Acknowledgement about search data 
 * from Purchase Order Information screen.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method insert/update : transactSaveAndSendPo</li>
 * </ul>
 *
 * @author CSI
 */
public interface PurchaseOrderAcknowledgementFacadeService {
    
    /**
     * <p>Search Purchase Order Information.</p>
     * <ul>
     * <li>Search Purchase Order Information for display on Purchase Order Acknowledge screen.</li>
     * </ul>
     * 
     * @param purchaseOrderAcknowledgementDomain the Purchase Order Acknowledge Domain.
     * @return the Purchase Order Detail Domain.
     * @throws ApplicationException ApplicationException
     */
    public List<PurchaseOrderDetailDomain> searchInitial(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain) throws ApplicationException;

    /**
     * Search count purchase Order Detail that has PN Status PENDING and match to criteria
     * from screen.
     *  
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain.
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderDetailPending(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain);
    
    /**
     * Call FileManagementService.serachFileDownload to get file name.
     * 
     * @param purchaseOrderAcknowledgementDomain the Purchase Order Acknowledgement Domain.
     * @return File Management Domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain)
        throws ApplicationException;

    /**
     * Call FileManagementService.serachFileDownload to send Legend File data to client.
     * 
     * @param purchaseOrderAcknowledgementDomain the Purchase Order Acknowledgement Domain.
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain,
        OutputStream output) throws ApplicationException;

    /**
     * <p>Transact save and send purchase order.</p>
     * <ul>
     * <li>Transact save and send purchase order for save and send mail on 
     * Purchase Order Acknowledge screen.</li>
     * </ul>
     * 
     * @param purchaseOrderAcknowledgementDomain the Purchase Order Information Domain.
     * @return the Purchase Order Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public PurchaseOrderAcknowledgementDomain transactSaveAndSendPo(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain) 
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
    
    // [IN054] Add new function DENSO Reply
    /**
     * <p>Transact DENSO Reply.</p>
     * <ul>
     * <li>Change P/O Status to RPL</li>
     * <li>Change PN Status from PED to RPL</li>
     * <li>Check is there are any Pending P/O Due that not reply yet, reject it.</li>
     * <li>Send email to inform Supplier User about Reply result.</li>
     * </li>
     * </ul>
     * 
     * @param purchaseOrderAcknowledgementDomain the Purchase Order Information Domain.
     * @return the Purchase Order Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public PurchaseOrderAcknowledgementDomain transactDensoReply(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain) 
        throws ApplicationException;
    
}