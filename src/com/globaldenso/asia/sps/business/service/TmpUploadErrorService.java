/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 2014/08/14 CSI Phakaporn           Modified method.
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.util.List;

import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;

/**
 * <p>The Interface TempUploadErrorService.</p>
 * <p>Service for Temporary Upload Error about search upload Error from validate CSV file.</p>
 * <ul>
 * <li>Method search  : searchCount</li>
 * <li>Method search  : searchGroupOfValidationError</li>
 * <li>Method search  : searchCountGroupOfValidationError</li>
 * </ul>
 * </ul>
 *
 * @author CSI
 */
public interface TmpUploadErrorService {
       
    /**
     * Search count warning items.
     * 
     * @param groupUploadErrorDomain the It keep a criteria to search count error items.
     * @return the number of records count.
     */
    public int searchCountWarning(GroupUploadErrorDomain groupUploadErrorDomain);
    
    /**
     * Search count In-correct items.
     * 
     * @param groupUploadErrorDomain the It keep a criteria to search count error items.
     * @return the number of records count.
     */
    public int searchCountIncorrect(GroupUploadErrorDomain groupUploadErrorDomain);
      
    /**
     * Search group of validation error.
     * 
     * @param groupUploadErrorDomain the It keep a criteria to search count error items
     * @return the list group of upload error items.
     */
    public List<GroupUploadErrorDomain> searchGroupOfValidationError(GroupUploadErrorDomain 
        groupUploadErrorDomain );
    
    /**
     * Search count group of validation error.
     * 
     * @param groupUploadErrorDomain the It keep a criteria to search count error items.
     * @return the number of records count.
     */
    public int searchCountGroupOfValidationError(GroupUploadErrorDomain groupUploadErrorDomain);
}