/*
 * ModifyDate Development company Describe 
 * 2014/09/01 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.dao.PurchaseOrderCoverPageDao;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;

/**
 * 
 * <p>The Interface purchase Order Cover Page Dao Implement.</p>
 * <p>Service for purchase Order Cover Page about search data from criteria.</p>
 *
 * <ul>
 *  <li>Method search  : searchPOCoverPageReport</li>
 * </ul>
 * @author CSI
 */
public class PurchaseOrderCoverPageServiceImpl implements PurchaseOrderCoverPageService {
    /** Purchase Order Cover Page DAO. */
    private PurchaseOrderCoverPageDao purchaseOrderCoverPageDao;
    /**
     * Instantiates a new Purchase Order Cover Page Dao Implement.
     */
    public PurchaseOrderCoverPageServiceImpl() {
        super();
    }
    
    /**
     * <p>Setter method for purchaseOrderCoverPageDao.</p>
     *
     * @param purchaseOrderCoverPageDao Set for purchaseOrderCoverPageDao
     */
    public void setPurchaseOrderCoverPageDao(
        PurchaseOrderCoverPageDao purchaseOrderCoverPageDao) {
        this.purchaseOrderCoverPageDao = purchaseOrderCoverPageDao;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderCoverPageDao#searchPoCoverPageReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<PurchaseOrderCoverPageReportDomain> searchPoCoverPageReport(
        PurchaseOrderInformationDomain input) {
        return purchaseOrderCoverPageDao.searchPoCoverPageReport(input);
    }
}
