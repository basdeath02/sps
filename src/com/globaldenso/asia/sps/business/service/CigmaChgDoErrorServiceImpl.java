/*
 * ModifyDate Development company     Describe 
 * 2015/03/11 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaChgDoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDomain;

/**
 * <p>The Class CigmaChgDoErrorService.</p>
 * <p>For SPS_CIGMA_CHG_DO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferChgDoError</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaChgDoErrorServiceImpl implements CigmaChgDoErrorService {

    /** The DAO for SPS_CIGMA_CHG_DO_ERROR. */
    private CigmaChgDoErrorDao cigmaChgDoErrorDao;
    
    /** The default constructor. */
    public CigmaChgDoErrorServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for cigmaChgDoErrorDao.</p>
     *
     * @param cigmaChgDoErrorDao Set for cigmaChgDoErrorDao
     */
    public void setCigmaChgDoErrorDao(CigmaChgDoErrorDao cigmaChgDoErrorDao) {
        this.cigmaChgDoErrorDao = cigmaChgDoErrorDao;
    }

    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaChgDoErrorService#searchTransferChgDoError(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain)
     */
    public List<TransferChgDoErrorEmailDomain> searchTransferChgDoError(
        SpsCigmaChgDoErrorDomain spsCigmaChgDoErrorDomain)
    {
        List<TransferChgDoErrorEmailDomain> result = null;
        result = this.cigmaChgDoErrorDao.searchTransferChgDoError(spsCigmaChgDoErrorDomain);
        return result;
    }

}
