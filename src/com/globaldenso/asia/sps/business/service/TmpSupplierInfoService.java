/*
 * ModifyDate Development company     Describe 
 * 2014/08/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain;

/**
 * <p>The Interface TempSupplierInfoService.</p>
 * <p>Service for Temporary Supplier Information about search from CSV file.</p>
 * <ul>
 *  <li>Method search  : searchTempUploadSupplierInfoOneRecord</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpSupplierInfoService {
       
    /**
     * <p>Search Temporary Upload Supplier Info One Record.</p>
     * 
     * @param tempSupplierInfoDomain that contain upload supplier information.
     * @return the Supplier information from upload.
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoOneRecord(TmpSupplierInfoDomain 
        tempSupplierInfoDomain);
    
    /**
     * <p>Search Temporary Upload Supplier Info Company name.</p>
     * 
     * @param tempSupplierInfoDomain that contain upload supplier information.
     * @return the Supplier information from upload.
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoCompanyName(TmpSupplierInfoDomain 
        tempSupplierInfoDomain);
    
    /**
     * <p>Search Temporary Upload Supplier Info Plant.</p>
     * 
     * @param tempSupplierInfoDomain that contain upload supplier information.
     * @return the list of plant supplier.
     */
    public List<PlantSupplierDomain> searchTmpUploadSupplierInfoPlant(TmpSupplierInfoDomain 
        tempSupplierInfoDomain);

}