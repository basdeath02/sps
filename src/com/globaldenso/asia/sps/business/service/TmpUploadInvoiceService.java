/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain;

/**
 * <p>The Interface  temporary upload invoice service.</p>
 * <p>Service for temporary upload invoice about search data.</p>
 * <ul>
 * <li>Method search  : searchCountInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpUploadInvoiceService {
    
    /**
     * <p>Search temporary upload invoice detail.</p>
     * <ul>
     * <li>Search temporary upload invoice item detail for display on Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param tmpUploadInvoiceDomain the Temporary Upload Invoice Domain
     * @return the list of Temporary Upload Invoice Domain.
     */
    public List<TmpUploadInvoiceDomain> searchTmpUploadInvoice(
        TmpUploadInvoiceDomain tmpUploadInvoiceDomain);

}