/*
 * ModifyDate Development company     Describe 
 * 2014/06/25 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;

/**
 * <p>The Interface record limit service.</p>
 * <p>Service about miscellaneous for search record limit.</p>
 * <ul>
 * <li>Method search  : searchRecordLimit</li>
 * <li>Method search  : searchRecordLimitPerPage</li>
 * </ul>
 *
 * @author CSI
 */
public interface RecordLimitService {
    
    /**
     * Search record limit.
     * Search record limit by criteria specify.
     * 
     * @param miscDomain the misc domain
     * @return miscDomain the misc domain
     */
    public MiscellaneousDomain searchRecordLimit(MiscellaneousDomain miscDomain);
    
    /**
     * Search record limit per page.
     * Search record limit per page by criteria specify.
     * 
     * @param miscDomain the misc domain
     * @return miscDomain the misc domain
     */
    public MiscellaneousDomain searchRecordLimitPerPage(MiscellaneousDomain miscDomain);
}