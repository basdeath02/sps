/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.io.IOException;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.SupplierUserUploadingDomain;


/**
 * <p>The Interface SupplierUserUploadingFacadeService.</p>
 * <p>Service for Supplier User Uploading about manipulate data form CSV file.</p>
 * <ul>
 * <li>Method search : initial</li>
 * <li>Method search : searchUploadErrorList</li>
 * <li>Method insert : transactUploadSupplierUser</li>
 * <li>Method insert : transactRegisterUploadItem</li>
 * </ul>
 *
 * @author CSI
 */
public interface SupplierUserUploadingFacadeService {
    
    /**
     * <p>initial for Supplier user uploading.</p>
     * <ul>
     * <li>Clear all data in temporary table.</li>
     * </ul>
     * @param supplierUserUploadingDomain that keep information to clear the temporary data.
     * @throws ApplicationException the ApplicationException.
     */
    public void transactInitial(SupplierUserUploadingDomain
        supplierUserUploadingDomain) throws ApplicationException;
    
    /**
    * <p>Search Upload Error List.</p>
    * <ul>
    * <li>Clear the temporary data.</li>
    * </ul>
    * @param supplierUserUploadingDomain It keep criteria to get data from temporary table.
    * @return list the list of Error information.
    * @throws ApplicationException the ApplicationException.
    */
    public  SupplierUserUploadingDomain searchUploadErrorList(SupplierUserUploadingDomain 
        supplierUserUploadingDomain)throws ApplicationException;
        
    /**
     * <p>upload Supplier User.</p>
     * <ul>
     * <li>Validate Supplier User Item Detail from CSV file then display error result on screen.</li>
     * </ul>
     * 
     * @param supplierUserUploadingDomain that keep all information for upload CSV file.
     * @return SupplierUserUploadingDomain that keep a summary of upload to show on screen.
     * @throws ApplicationException the ApplicationException
     * @throws IOException the IOException
     */
    public SupplierUserUploadingDomain transactUploadSupplierUser(SupplierUserUploadingDomain 
        supplierUserUploadingDomain) throws ApplicationException, IOException;
   
    /**
     * <p>Transact Register Upload Item.</p>
     * <ul>
     * <li>Insert supplier user detail from CSV file.</li>
     * </ul>
     * 
     * @param supplierUserUploadingDomain that keep current user DSC ID and session code to move 
     * data from temporary to actual table.
     * @throws ApplicationException ApplicationException
     */
    public void transactRegisterUploadItem(SupplierUserUploadingDomain supplierUserUploadingDomain)
        throws ApplicationException;
    
    /**
     * <p>Delete temporary upload item.</p>
     * <ul>
     * <li>Delete temporary upload item before upload new item.</li>
     * </ul>
     * 
     * @param userDscId the Current user DSC ID.
     * @param sessionCode the session code.
     * @throws ApplicationException ApplicationException
     */
    public void deleteUploadTempTable(String userDscId, String sessionCode)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}