/*
 * ModifyDate Development company       Describe 
 * 2014/08/20 CSI Parichat              Create
 * 2018/04/18 Netband U.Rungsiwut       Generate ASN PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AsnDetailInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface AsnDetailInformationFacadeService.</p>
 * <p>Facade for Asn Detail Information.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchAsnDetail</li>
 * <li>Method search  : searchAsnDetailCsv</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedPlantSupplier</li>
 * <li>Method search  : searchSelectedPlantDenso</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchAsnReport</li>
 * <li>Method search  : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnDetailInformationFacadeService {
    
    /**
     * <p>Search Initial.</p>
     * <ul>
     * <li>Initial for display on Asn Detail Information screen.</li>
     * </ul>
     * 
     * @param asnDetailInformationDomain the asn detail information domain
     * @return he asn detail information domain
     * @throws ApplicationException ApplicationException
     */
    public AsnDetailInformationDomain searchInitial (AsnDetailInformationDomain 
        asnDetailInformationDomain) throws ApplicationException;
    
    /**
     * <p>Search ASN Detail.</p>
     * <ul>
     * <li>Search ASN Detail Information.</li>
     * </ul>
     * 
     * @param asnDetailInformationCriteria the asn detail information domain
     * @return the asn detail information domain
     * @throws ApplicationException ApplicationException
     */
    public AsnDetailInformationDomain searchAsnDetail(AsnDetailInformationDomain 
        asnDetailInformationCriteria) throws ApplicationException;
    
    
    /**
     * <p>Search ASN Detail.</p>
     * <ul>
     * <li>Search ASN Detail Information.</li>
     * </ul>
     * 
     * @param asnDetailInformationCriteria the asn detail information domain
     * @return the asn detail information domain
     * @throws ApplicationException ApplicationException
     */
    public AsnDetailInformationDomain searchAsnDetailCsv(AsnDetailInformationDomain 
        asnDetailInformationCriteria) throws ApplicationException;
    
    /**
     * <p>Search Selected Company Supplier.</p>
     * <ul>
     * <li>Initial data when change select supplier company code</li>
     * <li>Search list of supplier plant to show in combo box</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return SpsMPlantSupplierDomain the List of supplier plant to show in combo box
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain>  searchSelectedCompanySupplier(PlantSupplierWithScopeDomain 
        plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Search Selected Company Denso.</p>
     * <ul>
     * <li>Initial data when change select DENSO company code</li>
     * <li>Search list of DENSO plant to show in combo box</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return the List of DENSO plant to show in combo box
     * @throws ApplicationException Application Exception
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant supplier code.</p>
     * <ul>
     * <li>Search list of company supplier to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Plant Supplier
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    
    /**
     * <p>Search File Name.</p>
     * Call FileManagementService.serachFileDownload to get file name.
     * 
     * @param asnDetailInformationDomain the ASN Detail Information Domain.
     * @return the file management domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        AsnDetailInformationDomain asnDetailInformationDomain) throws ApplicationException;
    
    /**
     * <p>Search Asn Report.</p>
     * Call FileManagementService.serachFileDownload to send ASN Report data to client.
     * 
     * @param asnDetailInformationDomain the ASN Detail Information Domain.
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchAsnReport(AsnDetailInformationDomain asnDetailInformationDomain,
        OutputStream output) throws ApplicationException;
    
    /**
     * <p>Search Legened Info.</p>
     * Call FileManagementService.serachFileDownload to send Legend File data to client.
     * 
     * @param asnDetailInformationDomain the ASN Detail Information Domain.
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(AsnDetailInformationDomain asnDetailInformationDomain,
        OutputStream output) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;

    /**
     * Generate ASN Report and update file ID to SPS_T_ASN.
     * @param locale locale to get report properties
     * @param asnNo an ASN_NO
     * @param dCd the company denso code
     * @param updateDateTime Last Update Datetime
     * @param dscId Last Update DSC_ID
     * @return FileManagementDomain ASN PDF
     * @throws ApplicationException an ApplicationException
     * @throws Exception an Exception
     * */
    public FileManagementDomain generateAndUpdatePdfFile(Locale locale, String asnNo, String dCd
        , Timestamp updateDateTime, String dscId) throws ApplicationException, Exception;
}