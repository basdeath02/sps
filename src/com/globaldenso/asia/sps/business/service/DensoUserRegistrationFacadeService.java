/*
 * ModifyDate Development company     Describe 
 * 2014/06/15 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface DensoUserRegistrationFacadeService.</p>
 * <p>Service for DENSO User about search data from criteria and create and update data.</p>
 * <ul>
 * <li>Method search  : searchInitialRegister</li>
 * <li>Method search  : searchInitialEdit</li>
 * <li>Method create  : transactRegisterNewUserDenso</li>
 * <li>Method update  : transactRegisterExistUserDenso</li>
 * <li>Method update  : searchSelectedCompanyDenso</li>
 * <li>Method update  : searchSelectedPlantDenso</li>
 * <li>Method update  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoUserRegistrationFacadeService {
     
    /**
     * <p>initial Screen mode Register</p>
     * <ul>
     * <li>Search DENSO User Item Detail for display on DENSO User Registration screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain that keep role information to query data to show on screen.
     * @return densoUserRegistrationDomain the keep all combo box values to show on screen.
     * @throws ApplicationException ApplicationException
     */
    public DensoUserRegistrationDomain searchInitialRegister(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>initial Screen mode Edit</p>
     * <ul>
     * <li>Search DENSO User Item Detail for display on DENSO User Registration screen.</li>
     * </ul>
     * 
     * @param densoUserRegistrationDomain that keep user DSC ID number and current user role.
     * @return densoUserRegistrationDomain the  keep a list for combo box and DENSO user 
     * information to show on screen
     * @throws ApplicationException ApplicationException
     */
    public DensoUserRegistrationDomain searchInitialEdit(DensoUserRegistrationDomain 
        densoUserRegistrationDomain) throws ApplicationException;
    
    /**
     * <p>Register New DENSO User</p>
     * <ul>
     * <li>Insert detail DENSO User data.Operator input value on register page
     * then click register button.</li>
     * </ul>
     * 
     * @param densoUserRegistrationDomain the keep all user information input data.
     * @return densoUserRegistrationDomain the list of validation error message.
     * @throws ApplicationException the application exception.
     */
    public DensoUserRegistrationDomain transactRegisterNewUserDenso(DensoUserRegistrationDomain
        densoUserRegistrationDomain) throws ApplicationException;
    
    /**
     * <p>Update DENSO User Detail.</p>
     * <ul>
     * <li>Update detail DENSO User data.Operator change value on update page 
     * then click Register button.</li>
     * </ul>
     * 
     * @param densoUserRegistrationDomain the keep all user information input data.
     * @return densoUserRegistrationDomain the list of validation error message.
     * @throws ApplicationException the application exception.
     */
    public DensoUserRegistrationDomain transactRegisterExistUserDenso(DensoUserRegistrationDomain
        densoUserRegistrationDomain) throws ApplicationException;
    
    /**
     * <p>Change select DENSO company.</p>
     * <ul>
     * <li>Change select DENSO company.</li>
     * </ul>
     * 
     * @return the list of PlantDensoDomain.
     * @param densoUserRegistrationDomain the DENSO user info domain.
     * @throws ApplicationException the ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        DensoUserRegistrationDomain densoUserRegistrationDomain)throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}