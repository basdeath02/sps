/*
 * ModifyDate Development company     Describe 
 * 2014/08/26 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import static com.globaldenso.asia.sps.common.utils.MessageUtil.getEmailLabel;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementCriterionDomain;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTCnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The class PurgeDataProcessFacadeServiceImpl.</p>
 * <p>Facade for PurgeDataProcessFacadeServiceImpl.</p>
 * <ul>
 * <li>Method search  : searchInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class PurgeDataProcessFacadeServiceImpl implements PurgeDataProcessFacadeService {
    
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(PurgeDataProcessFacadeService.class);
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** Mail Service */
    private MailService mailService;
    
    /** The purchase order service. */
    private PurchaseOrderService purchaseOrderService;
    
    /** The common service. */
    private CommonService commonService = null;
    
    /** The deliveryOrder service. */
    private DeliveryOrderService deliveryOrderService = null;
    
    /** The ASN service. */
    private AsnService asnService = null;
    
    /** The file management service. */
    private InvoiceService invoiceService = null;
    
    /** The SPS transaction detail service. */
    private SpsTAsnRunnoService spsTAsnRunnoService = null;
    
    /** The SPS transaction detail service. */
    private SpsTAsnService spsTAsnService = null;
    
    /** The SPS transaction detail service. */
    private SpsTAsnDetailService spsTAsnDetailService = null;
    
    /** The file management service. */
    private FileManagementService fileManagementService = null;
    
    /** The SPS transaction do kanban service. */
    private SpsTDoKanbanSeqService spsTDoKanbanSeqService = null;
    
    /** The SPS transaction do service. */
    private SpsTDoService spsTDoService = null;
    
    /** The SPS transaction do detail service. */
    private SpsTDoDetailService spsTDoDetailService = null;
    
    /** The SPS transaction po cover page service. */
    private SpsTPoCoverPageService spsTPoCoverPageService = null;
    
    /** The SPS transaction po cover page detail service. */
    private SpsTPoCoverPageDetailService spsTPoCoverPageDetailService = null;
    
    /** The SPS transaction po due service. */
    private SpsTPoDueService spsTPoDueService = null;
    
    /** The SPS transaction po service. */
    private SpsTPoService spsTPoService = null;
    
    /** The SPS transaction po detail service. */
    private SpsTPoDetailService spsTPoDetailService = null;
    
    /** The SPS transaction invoice service. */
    private SpsTInvoiceService spsTInvoiceService = null;
    
    /** The SPS transaction invoice detail service. */
    private SpsTInvoiceDetailService spsTInvoiceDetailService = null;
    
    /** The SPS transaction cn service. */
    private SpsTCnService spsTCnService = null;
    
    /** The SPS transaction cn detail service. */
    private SpsTCnDetailService spsTCnDetailService = null;
    
    /** The SPS transaction invoice cover page running no service. */
    private SpsTInvCoverpageRunnoService spsTInvCoverpageRunnoService = null;
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public PurgeDataProcessFacadeServiceImpl(){
        super();
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    /**
     * <p>Setter method for mailService.</p>
     *
     * @param mailService Set for mailService
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
    
    /**
     * Set the purchase order service.
     * 
     * @param purchaseOrderService the purchase order service to set
     */
    public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the delivery order Service.
     * 
     * @param deliveryOrderService the delivery order service to set
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }
    
    /**
     * Set the ASN Service.
     * 
     * @param asnService the ASN service to set
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Set the invoice service.
     * 
     * @param invoiceService the invoice service to set
     */
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    
    /**
     * Set the SPS transaction asn service.
     * 
     * @param spsTAsnService the SPS transaction asn service to set
     */
    public void setSpsTAsnService(SpsTAsnService spsTAsnService) {
        this.spsTAsnService = spsTAsnService;
    }
    
    /**
     * Set the SPS transaction asn run no service.
     * 
     * @param spsTAsnRunnoService the SPS transaction asn run no service to set
     */
    public void setSpsTAsnRunnoService(SpsTAsnRunnoService spsTAsnRunnoService) {
        this.spsTAsnRunnoService = spsTAsnRunnoService;
    }
    
    /**
     * Set the SPS transaction asn detail service.
     * 
     * @param spsTAsnDetailService the SPS transaction asn detail service to set
     */
    public void setSpsTAsnDetailService(SpsTAsnDetailService spsTAsnDetailService) {
        this.spsTAsnDetailService = spsTAsnDetailService;
    }
    
    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the SPS transaction do kanban service.
     * 
     * @param spsTDoKanbanSeqService the SPS transaction do kanban service to set
     */
    public void setSpsTDoKanbanSeqService(SpsTDoKanbanSeqService spsTDoKanbanSeqService) {
        this.spsTDoKanbanSeqService = spsTDoKanbanSeqService;
    }
    
    /**
     * Set the SPS transaction do service.
     * 
     * @param spsTDoService the SPS transaction do service to set
     */
    public void setSpsTDoService(SpsTDoService spsTDoService) {
        this.spsTDoService = spsTDoService;
    }
    
    /**
     * Set the SPS transaction do detail service.
     * 
     * @param spsTDoDetailService the SPS transaction do detail service to set
     */
    public void setSpsTDoDetailService(SpsTDoDetailService spsTDoDetailService) {
        this.spsTDoDetailService = spsTDoDetailService;
    }
    
    /**
     * Set theSPS transaction po cover page service.
     * 
     * @param spsTPoCoverPageService the SPS transaction po cover page service to set
     */
    public void setSpsTPoCoverPageService(SpsTPoCoverPageService spsTPoCoverPageService) {
        this.spsTPoCoverPageService = spsTPoCoverPageService;
    }
    
    /**
     * Set the SPS transaction po cover page detail service.
     * 
     * @param spsTPoCoverPageDetailService the SPS transaction po cover page detail service to set
     */
    public void setSpsTPoCoverPageDetailService(
        SpsTPoCoverPageDetailService spsTPoCoverPageDetailService) {
        this.spsTPoCoverPageDetailService = spsTPoCoverPageDetailService;
    }
    
    /**
     * Set the SPS transaction po due service.
     * 
     * @param spsTPoDueService the SPS transaction po due service to set
     */
    public void setSpsTPoDueService(SpsTPoDueService spsTPoDueService) {
        this.spsTPoDueService = spsTPoDueService;
    }
    
    /**
     * Set the SPS transaction po service.
     * 
     * @param spsTPoService the SPS transaction po service to set
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }
    
    /**
     * Set the SPS transaction po detail service.
     * 
     * @param spsTPoDetailService the SPS transaction po detail service to set
     */
    public void setSpsTPoDetailService(SpsTPoDetailService spsTPoDetailService) {
        this.spsTPoDetailService = spsTPoDetailService;
    }
    
    /**
     * Set the SPS transaction invoice service.
     * 
     * @param spsTInvoiceService the SPS transaction invoice service to set
     */
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService) {
        this.spsTInvoiceService = spsTInvoiceService;
    }
    
    /**
     * Set the SPS transaction invoice detail service.
     * 
     * @param spsTInvoiceDetailService the SPS transaction invoice detail service to set
     */
    public void setSpsTInvoiceDetailService(SpsTInvoiceDetailService spsTInvoiceDetailService) {
        this.spsTInvoiceDetailService = spsTInvoiceDetailService;
    }
    
    /**
     * Set the SPS transaction cn service.
     * 
     * @param spsTCnService the SPS transaction cn service to set
     */
    public void setSpsTCnService(SpsTCnService spsTCnService) {
        this.spsTCnService = spsTCnService;
    }
    
    /**
     * Set the SPS transaction invoice cover page running no service.
     * 
     * @param spsTInvCoverpageRunnoService the SPS transaction invoice cover page running no service to set
     */
    public void setSpsTInvCoverpageRunnoService(SpsTInvCoverpageRunnoService spsTInvCoverpageRunnoService) {
        this.spsTInvCoverpageRunnoService = spsTInvCoverpageRunnoService;
    }
    
    /**
     * Set the SPS transaction cn detail service.
     * 
     * @param spsTCnDetailService the SPS transaction cn detail service to set
     */
    public void setSpsTCnDetailService(SpsTCnDetailService spsTCnDetailService) {
        this.spsTCnDetailService = spsTCnDetailService;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#searchDensoCompanyByCodeList(
     * com.globaldenso.asia.sps.business.domain.SpsMCompanyDensoDomain)
     */
    public List<SpsMCompanyDensoDomain> searchDensoCompanyByCodeList(String densoCompanyCode){
        
        List<SpsMCompanyDensoDomain> densoCompanyCodeList = null;
        SpsMCompanyDensoDomain spsMCompanyDenso = new SpsMCompanyDensoDomain();
        spsMCompanyDenso.setDCd(densoCompanyCode);
        densoCompanyCodeList = this.companyDensoService.searchCompanyDensoByCodeList(
            spsMCompanyDenso);
        return densoCompanyCodeList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#searchAllPurgingOrderByCompany(
     * com.globaldenso.asia.sps.business.domain.SpsMCompanyDensoDomain)
     */
    public List<PurgingPoDomain> searchAllPurgingOrderByCompany(
        SpsMCompanyDensoDomain spsMCompanyDenso)throws ApplicationException{
        
        List<PurgingPoDomain> purgingPOList = this.searchPurgingPoByCompany(spsMCompanyDenso);
        if(null != purgingPOList && !purgingPOList.isEmpty()){
            Locale objLocale = spsMCompanyDenso.getLocale();
            this.searchPurgingDoByPo(purgingPOList, objLocale);
            this.searchPurgingUrgentDo(purgingPOList, spsMCompanyDenso, objLocale);
            this.searchAsnByDo(purgingPOList, objLocale);
            this.checkCompleteAsn(purgingPOList, objLocale);
        }
        
        return purgingPOList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#searchAllPurgingInvoiceByCompany(
     * com.globaldenso.asia.sps.business.domain.SpsMCompanyDensoDomain)
     */
    public List<SpsTInvoiceDomain> searchAllPurgingInvoiceByCompany(
        SpsMCompanyDensoDomain spsMCompanyDenso)throws ApplicationException{
        
        Locale objLocale = spsMCompanyDenso.getLocale();
        PurgingPoDomain purgingPoCriteria = this.setPurgingInvoiceCriteria(spsMCompanyDenso);
        
        List<SpsTInvoiceDomain> spsTInvoiceList
            = this.invoiceService.searchPurgingInvoice(purgingPoCriteria);
        if(null == spsTInvoiceList || spsTInvoiceList.isEmpty()){
            MessageUtil.getErrorMessageForBatchWithLabel(
                objLocale, SupplierPortalConstant.ERROR_CD_SP_E6_0043,
                SupplierPortalConstant.LBL_INVOICE,
                null, LOG, Constants.ONE, false);
        }
        return spsTInvoiceList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#transactPurgeSystemData(
     * com.globaldenso.asia.sps.business.domain.PurgingPoDomain,
     * the String)
     */
    public void transactPurgeSystemData(PurgingPoDomain poItem, String parentJobId)
        throws ApplicationException, Exception{
        
        Locale objLocale = poItem.getLocale();
        if(null != poItem.getPurgingDoList()){
            for(PurgingDoDomain doItem : poItem.getPurgingDoList()){
                for(PurgingAsnDomain asnItem : doItem.getPurgingAsnList()){
                    String asnNo = asnItem.getAsnNo();
                    
                    SpsTAsnDetailCriteriaDomain spsTAsnDetailCriteria 
                        = new SpsTAsnDetailCriteriaDomain();
                    spsTAsnDetailCriteria.setAsnNo(asnNo);
                    spsTAsnDetailCriteria.setDCd(asnItem.getDCd());
                    //Check Existing ASN No. table detail in master data.
                    int countAsnDetail = spsTAsnDetailService.searchCount(spsTAsnDetailCriteria);
                    if(Constants.ZERO < countAsnDetail){
                        int asnDetailDeleteRecord 
                            = this.spsTAsnDetailService.deleteByCondition(spsTAsnDetailCriteria);
                        if(asnDetailDeleteRecord <= Constants.ZERO){
                            MessageUtil.throwsApplicationMessage(objLocale, 
                                SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                                new String[]{StringUtil.appendsString(
                                    MessageUtil.getLabelHandledException(objLocale, 
                                        SupplierPortalConstant.LBL_ASN_DETAIL_PURGING_DATA), 
                                        asnNo)});
                        }
                    }
                    
                    SpsTAsnCriteriaDomain spsTAsnCriteria = new SpsTAsnCriteriaDomain();
                    spsTAsnCriteria.setAsnNo(asnNo);
                    spsTAsnCriteria.setDCd(asnItem.getDCd());
                    //Check Existing ASN No. table Header in master data.
                    int countAsnHeader = spsTAsnService.searchCount(spsTAsnCriteria);
                    if(Constants.ZERO < countAsnHeader){
                        int asnDeleteRecord  = 
                            this.spsTAsnService.deleteByCondition(spsTAsnCriteria);
                        if(asnDeleteRecord <= Constants.ZERO){
                            MessageUtil.throwsApplicationMessage(objLocale,
                                SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                                new String[] {StringUtil.appendsString(
                                        MessageUtil.getLabelHandledException(objLocale, 
                                            SupplierPortalConstant.LBL_ASN_HEADER_PURGING_DATA), 
                                            asnNo)});
                        }
                    }
                    
                    if(!StringUtil.checkNullOrEmpty(asnItem.getPdfFileId())){
                        FileManagementCriterionDomain criterion = new FileManagementCriterionDomain();
                        criterion.setFileId(asnItem.getPdfFileId());
                        int countFile = this.fileManagementService.searchCountFileInfo(criterion);
                        if(Constants.ZERO < countFile){
                            FileManagementDomain resultDomain = 
                                this.fileManagementService.searchFileDownload(
                                    asnItem.getPdfFileId(), false, null);
                            this.fileManagementService.deleteFile(asnItem.getPdfFileId(),
                                resultDomain.getLastUpdateDate(), parentJobId);
                        }
                    }
                    
                }
                
                String doId = doItem.getDeliveryOrderId();
                BigDecimal bdDoId = NumberUtil.toBigDecimal(doId);
                SpsTDoDetailCriteriaDomain spsTDoDetailCriteria 
                    = new SpsTDoDetailCriteriaDomain();
                spsTDoDetailCriteria.setDoId(bdDoId);
                //Check Existing DO Detail in master data.
                int countDoDetail = spsTDoDetailService.searchCount(spsTDoDetailCriteria);
                if(Constants.ZERO < countDoDetail){
                    int doDetailRecord 
                        = this.spsTDoDetailService.deleteByCondition(spsTDoDetailCriteria);
                    if(doDetailRecord <= Constants.ZERO){
                        MessageUtil.throwsApplicationMessage(objLocale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                            new String[] {StringUtil.appendsString(
                                MessageUtil.getLabelHandledException(objLocale, 
                                    SupplierPortalConstant
                                    .LBL_DELIVERY_ORDER_DETAIL_PURGING_DATA), doId)});
                    }
                }
                
                SpsTDoKanbanSeqCriteriaDomain spsTDoKanbanSeqCriteria
                    = new SpsTDoKanbanSeqCriteriaDomain();
                spsTDoKanbanSeqCriteria.setDoId(bdDoId);
                //Check Existing DO KANBAN in master data.
                int countDoKanban = spsTDoKanbanSeqService.searchCount(spsTDoKanbanSeqCriteria);
                if(Constants.ZERO < countDoKanban){
                    int doKanbanSeqRecord 
                        = this.spsTDoKanbanSeqService.deleteByCondition(spsTDoKanbanSeqCriteria);
                    if(doKanbanSeqRecord <= Constants.ZERO){
                        MessageUtil.throwsApplicationMessage(objLocale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0044, null,
                            new String[] {StringUtil.appendsString(
                                MessageUtil.getLabelHandledException(objLocale,
                                    SupplierPortalConstant
                                    .LBL_DELIVERY_KANBAN_SEQUENCE_PURGING_DATA), doId)});
                    }
                }
                
                SpsTDoCriteriaDomain spsTDoCriteria = new SpsTDoCriteriaDomain();
                spsTDoCriteria.setDoId(bdDoId);
                //Check Existing DO Header in master data.
                int countDoHeader = spsTDoService.searchCount(spsTDoCriteria);
                if(Constants.ZERO < countDoHeader){
                    int doRecord
                        = this.spsTDoService.deleteByCondition(spsTDoCriteria);
                    if(doRecord <= Constants.ZERO){
                        MessageUtil.throwsApplicationMessage(objLocale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0044, null,
                            new String[] {StringUtil.appendsString(
                                MessageUtil.getLabelHandledException(objLocale,
                                    SupplierPortalConstant
                                    .LBL_DELIVERY_ORDER_HEADER_PURGING_DATA), doId)});
                    }
                }
                
                
                if(!StringUtil.checkNullOrEmpty(doItem.getPdfFileId())){
                    FileManagementCriterionDomain criterion = new FileManagementCriterionDomain();
                    criterion.setFileId(doItem.getPdfFileId());
                    int countFile = this.fileManagementService.searchCountFileInfo(criterion);
                    if(Constants.ZERO < countFile){
                        FileManagementDomain resultDomain = this.fileManagementService.searchFileDownload(
                            doItem.getPdfFileId(), false, null);
                        this.fileManagementService.deleteFile(doItem.getPdfFileId(),
                            resultDomain.getLastUpdateDate(), parentJobId);
                    }
                }
            }
        }
        
        if(!poItem.getIsDummy()){
            
            String poId = poItem.getPurchaseOrderId();
            BigDecimal bdPoId = NumberUtil.toBigDecimal(poId);
            SpsTPoCoverPageDetailCriteriaDomain spsTPoCoverPageDetailCriteria 
                = new SpsTPoCoverPageDetailCriteriaDomain();
            spsTPoCoverPageDetailCriteria.setPoId(bdPoId);
            //Check Existing PO cover page Detail in master data.
            int countPoCoverPageDetail = spsTPoCoverPageDetailService.searchCount(
                spsTPoCoverPageDetailCriteria);
            if(Constants.ZERO < countPoCoverPageDetail){
                int poCoverPageDetailRecord
                    = this.spsTPoCoverPageDetailService.deleteByCondition(
                        spsTPoCoverPageDetailCriteria);
                if(poCoverPageDetailRecord <= Constants.ZERO){
                    MessageUtil.throwsApplicationMessage(objLocale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0044, null,
                        new String[]{StringUtil.appendsString(
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant
                                .LBL_PURCHASE_ORDER_COVER_PAGE_DETAIL_PURGING_DATA), poId)});
                }
            }
            
            SpsTPoCoverPageCriteriaDomain spsTPoCoverPageCriteria 
                = new SpsTPoCoverPageCriteriaDomain();
            spsTPoCoverPageCriteria.setPoId(bdPoId);
            //Check Existing PO cover page Header in master data.
            int countPoCoverPage = spsTPoCoverPageService.searchCount(spsTPoCoverPageCriteria);
            if(Constants.ZERO < countPoCoverPage){
                int poCoverPageRecord
                    = this.spsTPoCoverPageService.deleteByCondition(spsTPoCoverPageCriteria);
                if(poCoverPageRecord <= Constants.ZERO){
                    MessageUtil.throwsApplicationMessage(objLocale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                        new String[]{StringUtil.appendsString(
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant
                                .LBL_PURCHASE_ORDER_COVER_PAGE_HEADER_PURGING_DATA), poId)});
                }
            }
            
            SpsTPoDueCriteriaDomain spsTPoDueCriteria = new SpsTPoDueCriteriaDomain();
            spsTPoDueCriteria.setPoId(bdPoId);
            //Check Existing PO Due Header in master data.
            int countPoDueHeader = spsTPoDueService.searchCount(spsTPoDueCriteria);
            if(Constants.ZERO < countPoDueHeader){
                int poDueRecord = this.spsTPoDueService.deleteByCondition(spsTPoDueCriteria);
                if(poDueRecord <= Constants.ZERO){
                    MessageUtil.throwsApplicationMessage(objLocale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                        new String[] {StringUtil.appendsString(
                                MessageUtil.getLabelHandledException(objLocale, 
                                    SupplierPortalConstant.LBL_PURCHASE_ORDER_DUE_PURGING_DATA),
                                    poId)});
                }
            }
            
            SpsTPoDetailCriteriaDomain spsTPoDetailCriteria = new SpsTPoDetailCriteriaDomain();
            spsTPoDetailCriteria.setPoId(bdPoId);
            //Check Existing PO Detail in master data.
            int countPoDetail = spsTPoDetailService.searchCount(spsTPoDetailCriteria);
            if(Constants.ZERO < countPoDetail){
                int poDetailRecord = this.spsTPoDetailService.deleteByCondition(
                    spsTPoDetailCriteria);
                if(poDetailRecord <= Constants.ZERO){
                    MessageUtil.throwsApplicationMessage(objLocale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                        new String[]{StringUtil.appendsString(
                                MessageUtil.getLabelHandledException(objLocale, 
                                    SupplierPortalConstant.LBL_PURCHASE_ORDER_DETAIL_PURGING_DATA), 
                                    poId)});
                }
            }
            
            SpsTPoCriteriaDomain spsTpoCriteria = new SpsTPoCriteriaDomain();
            spsTpoCriteria.setPoId(bdPoId);
            //Check Existing PO Header in master data.
            int countPo = spsTPoService.searchCount(spsTpoCriteria);
            if(Constants.ZERO < countPo){
                int poRecord = this.spsTPoService.deleteByCondition(spsTpoCriteria);
                if(poRecord <= Constants.ZERO){
                    MessageUtil.throwsApplicationMessage(objLocale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                        new String[] {StringUtil.appendsString(
                                MessageUtil.getLabelHandledException(objLocale, 
                                    SupplierPortalConstant.LBL_PURCHASE_ORDER_PURGING_DATA), poId)});
                }
            }
            

            if(!StringUtil.checkNullOrEmpty(poItem.getPdfOriginalFileId())){
                FileManagementCriterionDomain criterionOriginal = new FileManagementCriterionDomain();
                criterionOriginal.setFileId(poItem.getPdfOriginalFileId());
                int countOriginalFile 
                    = this.fileManagementService.searchCountFileInfo(criterionOriginal);
                if(Constants.ZERO < countOriginalFile){
                    FileManagementDomain resultDomain = null;
                    resultDomain = this.fileManagementService.searchFileDownload(
                        poItem.getPdfOriginalFileId(), false, null);
                    this.fileManagementService.deleteFile(poItem.getPdfOriginalFileId(),
                        resultDomain.getLastUpdateDate(), parentJobId);
                }
            }
            
            
            if(!StringUtil.checkNullOrEmpty(poItem.getPdfChangeFileId())){
                FileManagementCriterionDomain criterionChange = new FileManagementCriterionDomain();
                criterionChange.setFileId(poItem.getPdfChangeFileId());
                int countChangeFile = this.fileManagementService.searchCountFileInfo(criterionChange);
                if(Constants.ZERO < countChangeFile){
                    FileManagementDomain resultDomain = null;
                    resultDomain = this.fileManagementService.searchFileDownload(
                        poItem.getPdfChangeFileId(), false, null);
                    this.fileManagementService.deleteFile(poItem.getPdfChangeFileId(),
                        resultDomain.getLastUpdateDate(), parentJobId);
                }
            }
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#transacPurgeInvoiceData(
     * com.globaldenso.asia.sps.business.domain.SpsTInvoiceDomain,
     * the string)
     */
    public void transactPurgeInvoiceData(
        SpsTInvoiceDomain invoiceItem, SpsMCompanyDensoDomain spsMCompanyDenso, String parentJobId)
        throws ApplicationException, Exception{
        
        Locale objLocale = invoiceItem.getLocale();
        
        BigDecimal bdInvoiceId = invoiceItem.getInvoiceId();
        String invoiceId = bdInvoiceId.toString();
        
        SpsTInvoiceDetailCriteriaDomain spsTInvoiceDetailCriteria
            = new SpsTInvoiceDetailCriteriaDomain();
        spsTInvoiceDetailCriteria.setInvoiceId(bdInvoiceId);
        int invoiceDetailRecord 
            = this.spsTInvoiceDetailService.deleteByCondition(spsTInvoiceDetailCriteria);
        if(invoiceDetailRecord <= Constants.ZERO){
            MessageUtil.throwsApplicationMessage(
                objLocale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0044, 
                null, 
                new String[]{
                    StringUtil.appendsString(
                        MessageUtil.getLabelHandledException(objLocale, 
                            SupplierPortalConstant.LBL_INVOICE_DETAIL_PURGING_DATA),
                            invoiceId
                    )
                }
            );
        }
        
        SpsTCnCriteriaDomain spsTCnCriteriaForSearch = new SpsTCnCriteriaDomain();
        spsTCnCriteriaForSearch.setInvoiceId(bdInvoiceId);
        List<SpsTCnDomain> spsTCnList 
            = this.spsTCnService.searchByCondition(spsTCnCriteriaForSearch);
        if(null != spsTCnList && !spsTCnList.isEmpty()){
            BigDecimal bdCnId = spsTCnList.get(Constants.ZERO).getCnId();
            String cnId = bdCnId.toString();
            
            SpsTCnDetailCriteriaDomain spsTCnDetailCriteria = new SpsTCnDetailCriteriaDomain();
            spsTCnDetailCriteria.setCnId(bdCnId);
            int cnDetailRecord = this.spsTCnDetailService.deleteByCondition(
                spsTCnDetailCriteria);
            if(cnDetailRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(
                    objLocale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0044, 
                    null, 
                    new String[]{
                        StringUtil.appendsString(
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant.LBL_CN_DETAIL_PURGING_DATA),
                                cnId
                        )
                    }
                );
            }
            SpsTCnCriteriaDomain spsTCnCriteria = new SpsTCnCriteriaDomain();
            spsTCnCriteria.setCnId(bdCnId);
            int cnRecord = this.spsTCnService.deleteByCondition(spsTCnCriteria);
            if(cnRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(
                    objLocale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0044, 
                    null, 
                    new String[]{
                        StringUtil.appendsString(
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant.LBL_CN_HEADER_PAGE_PURGING_DATA),
                                cnId
                        )
                    }
                );
            }
        }else{
            MessageUtil.getErrorMessageForBatchWithLabel(
                objLocale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0043, 
                SupplierPortalConstant.LBL_CN, null, LOG, Constants.TWO, false);
        }
        
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteria = new SpsTInvoiceCriteriaDomain();
        spsTInvoiceCriteria.setInvoiceId(bdInvoiceId);
        int invoiceRecord = this.spsTInvoiceService.deleteByCondition(spsTInvoiceCriteria);
        if(invoiceRecord <= Constants.ZERO){
            MessageUtil.throwsApplicationMessage(
                objLocale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0044, 
                null, 
                new String[]{
                    StringUtil.appendsString(
                        MessageUtil.getLabelHandledException(objLocale, 
                            SupplierPortalConstant.LBL_INVOICE_PURGING_DATA),
                            invoiceId
                    )
                }
            );
        }
        
        if(!StringUtil.checkNullOrEmpty(invoiceItem.getCoverPageFileId())){
            FileManagementCriterionDomain criterion = new FileManagementCriterionDomain();
            criterion.setFileId(invoiceItem.getCoverPageFileId());
            int countFile = this.fileManagementService.searchCountFileInfo(criterion);
            if(Constants.ZERO < countFile){
                FileManagementDomain resultDomain = 
                    this.fileManagementService.searchFileDownload(
                        invoiceItem.getCoverPageFileId(), false, null);
                this.fileManagementService.deleteFile(invoiceItem.getCoverPageFileId(), 
                    resultDomain.getLastUpdateDate(), parentJobId);
            }
            
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#transactPurgeInvoiceCoverPageRunNoData(
     * com.globaldenso.asia.sps.business.domain.SpsMCompanyDensoDomain)
     */
    public void transactPurgeInvoiceCoverPageRunNoData(SpsMCompanyDensoDomain spsMCompanyDenso)
        throws ApplicationException{
        
        Locale objLocale = spsMCompanyDenso.getLocale();
        
        SpsTInvCoverpageRunnoCriteriaDomain invoiceCoverpageRunnoCriteria 
            = new SpsTInvCoverpageRunnoCriteriaDomain();
        invoiceCoverpageRunnoCriteria.setDCd(spsMCompanyDenso.getDCd());
        invoiceCoverpageRunnoCriteria.setCreateDatetimeLessThanEqual(
            new Timestamp(this.calDateStartPurge(
                spsMCompanyDenso.getInvoiceKeepYear()).getTimeInMillis()));
        int invoiceCoverpageRunnoRecord 
            = this.spsTInvCoverpageRunnoService.deleteByCondition(invoiceCoverpageRunnoCriteria);
        if(invoiceCoverpageRunnoRecord <= Constants.ZERO){
            MessageUtil.throwsApplicationMessage(
                objLocale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0044, 
                null, 
                new String[]{
                    MessageUtil.getLabelHandledException(objLocale, 
                        SupplierPortalConstant.LBL_INVOICE_COVER_PAGE_PURGING_RUNNING_NO)
                }
            );
        }
    }
    
    /**
     * <p>Search purging PO by company.</p>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @return the list of Purging PO Domain
     * @throws ApplicationException ApplicationException
     */
    private List<PurgingPoDomain> searchPurgingPoByCompany(SpsMCompanyDensoDomain spsMCompanyDenso) 
        throws ApplicationException{
        
        PurgingPoDomain purgingPoCriteria = this.setPurgingPoCriteria(spsMCompanyDenso);
        
        List<PurgingPoDomain> purgingPOList 
            = this.purchaseOrderService.searchPurgingPurchaseOrder(purgingPoCriteria);
        if(null != purgingPOList && !purgingPOList.isEmpty()){
            for(PurgingPoDomain item : purgingPOList){
                item.setIsComplete(true);
                item.setDateStartPurge(purgingPoCriteria.getDateStartPurge());
            }
        }
        
        return purgingPOList;
    }
    
    /**
     * <p>Search purging DO by PO.</p>
     * 
     * @param purgingPOList the list of Purging PO Domain
     * @param objLocale the local
     * @throws ApplicationException ApplicationException
     */
    private void searchPurgingDoByPo(List<PurgingPoDomain> purgingPOList, Locale objLocale)
        throws ApplicationException{
        
        for(PurgingPoDomain poItem:purgingPOList){

            // [IN012] also purge Cancel D/O
            //purgingPoCriteria.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
            poItem.setShipmentStatusComplete(Constants.SHIPMENT_STATUS_CPS);
            poItem.setShipmentStatusCancel(Constants.SHIPMENT_STATUS_CCL);
            
            List<PurgingDoDomain> deliveryOrderList 
                = this.deliveryOrderService.searchPurgingDeliveryOrder(poItem);
            if(null == deliveryOrderList || deliveryOrderList.isEmpty()){
                
                // [IN012] P/O can delete new method
                //poItem.setIsComplete(true);
                
                MessageUtil.getErrorMessageForBatch(objLocale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0043,
                    new String[] {StringUtil.appendsString(
                            Constants.SYMBOL_OPEN_BRACKET,
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant.LBL_PO),
                            Constants.SYMBOL_COLON, poItem.getPurchaseOrderId(),
                            Constants.SYMBOL_CLOSE_BRACKET,
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant.LBL_DELIVERY_ORDER))},
                    LOG, Constants.TWO, false);
            }else{
                poItem.setPurgingDoList(deliveryOrderList);
            }
            
            // [IN012] Start : Search for D/O that not have to purge
            Integer countNotPurge
                = this.deliveryOrderService.searchCountNotPurgingDeliveryOrder(poItem);
            if (Constants.ZERO < countNotPurge) {
                poItem.setIsComplete(false);
            }
            // [IN012] End : Search for D/O that not have to purge
            
        }
    }
    
    /**
     * <p>Search purging urgent Do.</p>
     * 
     * @param purgingPOList the list of Purging PO Domain
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @param objLocale the local
     * @throws ApplicationException ApplicationException
     */
    private void searchPurgingUrgentDo(List<PurgingPoDomain> purgingPOList, 
        SpsMCompanyDensoDomain spsMCompanyDenso, Locale objLocale)throws ApplicationException{
        
        PurgingPoDomain purgingPoCriteria = this.setPurgingPoCriteria(spsMCompanyDenso);
        
        // [IN012] also purge Cancel D/O
        //purgingPoCriteria.setShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
        purgingPoCriteria.setShipmentStatusComplete(Constants.SHIPMENT_STATUS_CPS);
        purgingPoCriteria.setShipmentStatusCancel(Constants.SHIPMENT_STATUS_CCL);
        
        List<PurgingDoDomain> deliveryOrderList 
            = this.deliveryOrderService.searchPurgingUrgentDeliveryOrder(purgingPoCriteria);
        
        String currentDate = DateUtil.format(
            this.commonService.searchSysDate(), DateUtil.PATTERN_YYYYMMDD);
        int loopCounter = 1;
        for(PurgingDoDomain doItem:deliveryOrderList){
            PurgingPoDomain poItem = new PurgingPoDomain();
            poItem.setIsDummy(true);
            // [IN012] P/O dummy is also complete
            poItem.setIsComplete(true);
            
            // [IN012]
            poItem.setDateStartPurge(purgingPoCriteria.getDateStartPurge());
            
            poItem.setPurchaseOrderId(
                StringUtil.appendsString(currentDate, String.valueOf(loopCounter)));
            List<PurgingDoDomain> purgingDoList = new ArrayList<PurgingDoDomain>();
            purgingDoList.add(doItem);
            poItem.setPurgingDoList(purgingDoList);
            
            purgingPOList.add(poItem);
            loopCounter++;
        }
    }
    
    /**
     * <p>Search purging ASN by DO.</p>
     * 
     * @param purgingPOList the list of Purging PO Domain
     * @param objLocale the local
     * @throws ApplicationException ApplicationException
     */
    private void searchAsnByDo(List<PurgingPoDomain> purgingPOList, Locale objLocale)
        throws ApplicationException{
        
        for(PurgingPoDomain poItem : purgingPOList){
            if(null != poItem.getPurgingDoList()){
                for(PurgingDoDomain doItem : poItem.getPurgingDoList()){
                    
                    // [IN012] If D/O Shipment Status is Cancel, not check ASN
                    if (Constants.SHIPMENT_STATUS_CCL.equals(doItem.getDoShipStatus())) {
                        continue;
                    }
                    
                    doItem.setDateStartPurge(poItem.getDateStartPurge());
                    List<PurgingAsnDomain> purgingAsnList 
                        = this.asnService.searchPurgingAsnOrder(doItem);
                    if(null == purgingAsnList || purgingAsnList.isEmpty()){
                        
                        // [IN012] P/O can delete new method
                        //poItem.setIsComplete(false);
                        
                        MessageUtil.getErrorMessageForBatch(objLocale,
                            SupplierPortalConstant.ERROR_CD_SP_E6_0043,
                            new String[] {
                                StringUtil.appendsString(
                                    Constants.SYMBOL_OPEN_BRACKET,
                                    MessageUtil.getLabelHandledException(objLocale,
                                        SupplierPortalConstant.LBL_PO),
                                    Constants.SYMBOL_COLON,
                                    poItem.getPurchaseOrderId(),
                                    Constants.SYMBOL_COMMA,
                                    MessageUtil.getLabelHandledException(objLocale,
                                        SupplierPortalConstant.LBL_DO),
                                    Constants.SYMBOL_COLON,
                                    doItem.getDeliveryOrderId(),
                                    Constants.SYMBOL_CLOSE_BRACKET,
                                    MessageUtil.getLabelHandledException(objLocale,
                                        SupplierPortalConstant.LBL_ASN))},
                            LOG, Constants.TWO, false);
                    }else{
                        doItem.setPurgingAsnList(purgingAsnList);
                    }
                    
                    // [IN012] Start : Search for D/O that not have to purge
                    Integer countNotPurge = this.asnService.searchCountNotPurgingAsn(doItem);
                    if (Constants.ZERO < countNotPurge) {
                        poItem.setIsComplete(false);
                    }
                    // [IN012] End : Search for D/O that not have to purge
                    
                }
            }
        }
    }
    
    /**
     * <p>Search check complete ASN.</p>
     * 
     * @param purgingPOList the list of Purging PO Domain
     * @param objLocale the local
     * @throws ApplicationException ApplicationException
     */
    private void checkCompleteAsn(List<PurgingPoDomain> purgingPOList , Locale objLocale)
        throws ApplicationException{
        
        for(PurgingPoDomain poItem : purgingPOList){
            if(null != poItem && poItem.getIsComplete()){
                if(null != poItem.getPurgingDoList()){
                    for(PurgingDoDomain doItem : poItem.getPurgingDoList()){
                        if(null != doItem.getPurgingAsnList()
                            && !doItem.getPurgingAsnList().isEmpty())
                        {
                            for(PurgingAsnDomain asnItem:doItem.getPurgingAsnList()){
                                PurgingAsnDomain purgingAsnDomain = new PurgingAsnDomain();
                                purgingAsnDomain.setAsnNo(asnItem.getAsnNo());
                                purgingAsnDomain.setDCd(asnItem.getDCd());
                                boolean isComplete 
                                    = this.checkAsnRelateCloseInvoice(purgingAsnDomain);
                                if(!isComplete){
                                    poItem.setIsComplete(false);
                                    
                                    MessageUtil.getErrorMessageForBatch(objLocale,
                                        SupplierPortalConstant.ERROR_CD_SP_E6_0043,
                                        new String[] {
                                            StringUtil.appendsString(
                                                Constants.SYMBOL_OPEN_BRACKET,
                                                MessageUtil.getLabelHandledException(objLocale,
                                                    SupplierPortalConstant.LBL_PO),
                                                Constants.SYMBOL_COLON,
                                                poItem.getPurchaseOrderId(),
                                                Constants.SYMBOL_COMMA,
                                                MessageUtil.getLabelHandledException(objLocale,
                                                    SupplierPortalConstant.LBL_DO),
                                                Constants.SYMBOL_COLON,
                                                doItem.getDeliveryOrderId(),
                                                Constants.SYMBOL_COMMA,
                                                MessageUtil.getLabelHandledException(objLocale,
                                                    SupplierPortalConstant.LBL_ASN),
                                                Constants.SYMBOL_COLON,
                                                asnItem.getAsnNo(),
                                                Constants.SYMBOL_CLOSE_BRACKET)},
                                        LOG, Constants.TWO, false);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * <p>Search check ASN relate close invoice.</p>
     * 
     * @param purgingAsnDomain the Purging ASN Domain
     * @return boolean
     * @throws ApplicationException ApplicationException
     */
    private boolean checkAsnRelateCloseInvoice(PurgingAsnDomain purgingAsnDomain)
        throws ApplicationException
    {
        // [IN012] ASN can create by many Invoice (in case cancel invoice)
        //SpsTInvoiceDomain spsTInvoiceDomain 
        //    = this.invoiceService.searchInvoiceByAsn(purgingAsnDomain);
        //
        //if(null != spsTInvoiceDomain 
        //    && (Constants.INVOICE_STATUS_DCL.equals(spsTInvoiceDomain.getInvoiceStatus())
        //        || Constants.INVOICE_STATUS_PAY.equals(spsTInvoiceDomain.getInvoiceStatus()))){
        //    return true;
        //}else{
        //    return false;
        //} 
        
        List<SpsTInvoiceDomain> invoiceList
            = this.invoiceService.searchInvoiceByAsn(purgingAsnDomain);
        for (SpsTInvoiceDomain spsTInvoiceDomain : invoiceList) {
            if (!Constants.INVOICE_STATUS_DCL.equals(spsTInvoiceDomain.getInvoiceStatus())
                && !Constants.INVOICE_STATUS_PAY.equals(spsTInvoiceDomain.getInvoiceStatus()))
            {
                return false;
            }
        }
        return true;
        
    }
    
    /**
     * <p>Set purging PO criteria.</p>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @return boolean
     */
    private PurgingPoDomain setPurgingPoCriteria(SpsMCompanyDensoDomain spsMCompanyDenso) {
        
        PurgingPoDomain purgingPoCriteria = new PurgingPoDomain();
        purgingPoCriteria.setDCd(spsMCompanyDenso.getDCd());
        
        Calendar cal = this.calDateStartPurge(spsMCompanyDenso.getOrderKeepYear());
        purgingPoCriteria.setDateStartPurge(
            DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_SLASH));
            
        return purgingPoCriteria;
    }
    
    /**
     * <p>Set purging Bht Transmit Log criteria.</p>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @return PurgingBhtTransmitLogDomain
     */
	private PurgingBhtTransmitLogDomain setPurgingBhtTransmitLogCriteria(
			SpsMCompanyDensoDomain spsMCompanyDenso) {

		PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria = new PurgingBhtTransmitLogDomain();
		purgingBhtTransmitLogCriteria.setDCd(spsMCompanyDenso.getDCd());

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, Constants.MINUS_ONE);
		purgingBhtTransmitLogCriteria.setDateStartPurge(cal.getTime());

		return purgingBhtTransmitLogCriteria;
	}
    
    /**
     * <p>Set purging PO criteria.</p>
     * 
     * @param spsMCompanyDenso the SPS Master Company DENSO Domain
     * @return boolean
     */
    private PurgingPoDomain setPurgingInvoiceCriteria(SpsMCompanyDensoDomain spsMCompanyDenso) {
        
        PurgingPoDomain purgingPoCriteria = new PurgingPoDomain();
        purgingPoCriteria.setDCd(spsMCompanyDenso.getDCd());
        
        Calendar cal = this.calDateStartPurge(spsMCompanyDenso.getInvoiceKeepYear());
        purgingPoCriteria.setDateStartPurge(
            DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_SLASH));
            
        return purgingPoCriteria;
    }
    
    /**
     * <p>Cal date start purge.</p>
     * 
     * @param bdOrderKeepYear the big decimal of order keep year
     * @return Calendar
     */
    private Calendar calDateStartPurge(BigDecimal bdOrderKeepYear){
        
        int orderKeepYear = NumberUtil.toIntegerDefaultZero(bdOrderKeepYear) * Constants.MINUS_ONE;
        Timestamp currentDateTime = commonService.searchSysDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDateTime);
        cal.add(Calendar.YEAR, orderKeepYear);
        return cal;
    }

    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeService#transactPurgeData(
     * com.globaldenso.asia.sps.business.domain.SpsMCompanyDensoDomain)
     */
    public void transactPurgeData(SpsMCompanyDensoDomain spsMCompanyDenso,
        String parentJobId) throws ApplicationException, Exception {
        Locale objLocale = spsMCompanyDenso.getLocale();
        List<String> asnNoList = new ArrayList<String>();
        List<String> asnNoRcpList = new ArrayList<String>();
        int result = Constants.ZERO;
        boolean purgeResult = true;
        
//        Add purge process for SPS_T_BHT_TRANSMIT_LOG table
        PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria = this.setPurgingBhtTransmitLogCriteria(spsMCompanyDenso);
        
        String companyCode = spsMCompanyDenso.getDCd();
        try{
        	
//        	DELETE FROM SPS_T_BHT_TRANSMIT_LOG
            result += deliveryOrderService.deleteBhtTransmitLog(purgingBhtTransmitLogCriteria);
            if(result < Constants.ZERO){
                MessageUtil.throwsApplicationMessage(objLocale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                    new String[] {StringUtil.appendsString(
                            MessageUtil.getLabelHandledException(objLocale, 
                                SupplierPortalConstant.LBL_DELIVERY_KANBAN_SEQUENCE_PURGING_DATA))});
            }
//          End process to delete from SPS_T_BHT_TRANSMIT_LOG

//          DELETE FROM SPS_T_ASN
            List<String> asnStatusToDelete = new ArrayList<String>();
            asnStatusToDelete.add(Constants.ASN_STATUS_R);
            asnStatusToDelete.add(Constants.ASN_STATUS_D);
            List<PurgingAsnDomain> purgingAsnDomainList = null;
            
            List<String> asnNoGroup = new ArrayList<String>();
            List<String> asnNoRcpGroup = new ArrayList<String>();
//            
            PurgingDoDomain purgingDoDomainCriteria = new PurgingDoDomain();
            
            purgingDoDomainCriteria.setDateStartPurge(DateUtil.format(purgingBhtTransmitLogCriteria.getDateStartPurge(), DateUtil.PATTERN_YYYYMMDD_SLASH));
            purgingDoDomainCriteria.setDCd(companyCode);
            purgingAsnDomainList = asnService.searchPurgingAsnOrder(purgingDoDomainCriteria);
            if(purgingAsnDomainList.size() > Constants.ZERO){
                for(PurgingAsnDomain purgingAsnDomain : purgingAsnDomainList){
                    asnNoList.add(purgingAsnDomain.getAsnNo());
//                        Set the Maximum size of list to 300 because the web service to call check status of ASN has limit of string length = 7000
                    if(asnNoList.size() == Constants.THREE_HUNDREDS){
                        String asnNo = StringUtil.convertListToVarcharCommaSeperate(asnNoList);
                        asnNoGroup.add(asnNo);
                        asnNoList.clear();
                    }
                }
                if(asnNoList.size() > Constants.ZERO){
                    String asnNo = StringUtil.convertListToVarcharCommaSeperate(asnNoList);
                    asnNoGroup.add(asnNo);
                }
                String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(companyCode);
                SpsMCompanyDensoDomain companyDensoDomain = new SpsMCompanyDensoDomain();
                companyDensoDomain.setDCd(densoCodeStr);
                List<As400ServerConnectionInformationDomain> schemaResultList 
                    = companyDensoService.searchAs400ServerList(companyDensoDomain);
                if(null == schemaResultList || Constants.ZERO == schemaResultList.size()){
                    MessageUtil.throwsApplicationMessageHandledException(objLocale,
                        SupplierPortalConstant.ERROR_CD_SP_E5_0026);
                }
                if(Constants.ONE != schemaResultList.size()){
                    MessageUtil.throwsApplicationMessageHandledException(objLocale,
                        SupplierPortalConstant.ERROR_CD_SP_E5_0025);
                }
                for(String asnNoInGroup : asnNoGroup){
                    for(As400ServerConnectionInformationDomain as400Server : schemaResultList){
                        List<PseudoCigmaAsnDomain> cigmaAsnList = 
                            CommonWebServiceUtil.asnResourceSearchAsnReceiving(asnNoInGroup,
                                Constants.EMPTY_STRING, as400Server, objLocale);
                        for(PseudoCigmaAsnDomain pseudoCigmaAsnDomain : cigmaAsnList){
                            if(asnStatusToDelete.contains(pseudoCigmaAsnDomain.getPseudoAsnStatus()) 
                                && !asnNoRcpList.contains(pseudoCigmaAsnDomain.getPseudoAsnNo().trim())
                                && !asnNoRcpGroup.contains(pseudoCigmaAsnDomain.getPseudoAsnNo().trim())){
                                asnNoRcpList.add(pseudoCigmaAsnDomain.getPseudoAsnNo().trim());
                                if(asnNoRcpList.size() == Constants.ONE_THOUSAND ){
                                    String asnNoRcp = StringUtil.convertListToVarcharCommaSeperate(asnNoRcpList);
                                    asnNoRcpGroup.add(asnNoRcp);
                                    asnNoRcpList.clear();
                                }
                            }
                        }
                        if(asnNoRcpList.size() > Constants.ZERO){
                            String asnNoRcp = StringUtil.convertListToVarcharCommaSeperate(asnNoRcpList);
                            asnNoRcpList.clear();
                            asnNoRcpGroup.add(asnNoRcp);
                        }
                    }
                }
                if(Constants.ZERO < asnNoRcpGroup.size()){
                    for(String asnNoInGroup : asnNoRcpGroup){
                        result = asnService.deletePurgingAsn(asnNoInGroup);
                        if(result < Constants.ZERO){
                            MessageUtil.throwsApplicationMessage(objLocale,
                                SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                                new String[] {StringUtil.appendsString(
                                        MessageUtil.getLabelHandledException(objLocale, 
                                            SupplierPortalConstant.LBL_ASN_HEADER_PURGING_DATA))});
                        }
                    }
                }
            }
//            End process to delete from SPS_T_ASN
            
//          DEELTE FROM SPS_T_INVOICE
          result += invoiceService.deleteInvoice(purgingBhtTransmitLogCriteria);
          if(result < Constants.ZERO){
              MessageUtil.throwsApplicationMessage(objLocale,
                  SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                  new String[] {StringUtil.appendsString(
                          MessageUtil.getLabelHandledException(objLocale, 
                              SupplierPortalConstant.LBL_INVOICE_PURGING_DATA))});
          }
//        End process to delete from SPS_T_INVOICE
          
//          DELETE FROM SPS_T_DO
          result += deliveryOrderService.deleteDeliveryOrder(purgingBhtTransmitLogCriteria);
          if(result < Constants.ZERO){
              MessageUtil.throwsApplicationMessage(objLocale,
                  SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                  new String[] {StringUtil.appendsString(
                          MessageUtil.getLabelHandledException(objLocale, 
                              SupplierPortalConstant.LBL_DELIVERY_ORDER_HEADER_PURGING_DATA))});
          }
//        End process to delete from SPS_T_DO
          
//          DELETE FROM SPS_T_PO
          result += purchaseOrderService.deletePurchaseOrder(purgingBhtTransmitLogCriteria);
          if(result < Constants.ZERO){
              MessageUtil.throwsApplicationMessage(objLocale,
                  SupplierPortalConstant.ERROR_CD_SP_E6_0044, null, 
                  new String[] {StringUtil.appendsString(
                          MessageUtil.getLabelHandledException(objLocale, 
                              SupplierPortalConstant.LBL_PURCHASE_ORDER_PURGING_DATA))});
          }
//          End process to delete from SPS_T_PO
        }catch(Exception e){
            purgeResult = false;
        }
        
//      Process to send e-mail to notify the result of Purge process
        StringBuffer content = new StringBuffer();
        SendEmailDomain email = new SendEmailDomain();
        email.setHeader(getEmailLabel(objLocale, SupplierPortalConstant.PURGE_NOTIFICATION_SUBJECT));
        email.setEmailSmtp(ContextParams.getEmailSmtp());
        content.append(getEmailLabel(objLocale, SupplierPortalConstant.PURGE_NOTIFICATION_CONTENT_HEADER));
        if(purgeResult){
            content.append(
                getEmailLabel(objLocale, SupplierPortalConstant.PURGE_NOTIFICATION_CONTENT_COMPLETE).replaceAll(SupplierPortalConstant.MAIL_DCD_REPLACEMENT, companyCode));
        } else {
            content.append(
                getEmailLabel(objLocale, SupplierPortalConstant.PURGE_NOTIFICATION_CONTENT_INCOMPLETE).replaceAll(SupplierPortalConstant.MAIL_DCD_REPLACEMENT, companyCode));
        }
        content.append(getEmailLabel(objLocale, SupplierPortalConstant.PURGE_NOTIFICATION_CONTENT_FOOTER));
        email.setContents(content.toString());
        email.setEmailFrom(ContextParams.getDefaultEmailFrom());
        email.setEmailTo(ContextParams.getPurgeEmailSendTo());
        boolean sendMailResult = true;
        if (!mailService.sendEmail(email)) {
            sendMailResult = false;
        }
        if(!sendMailResult){
            MessageUtil.getErrorMessageForBatch(objLocale, SupplierPortalConstant.ERROR_CD_SP_E5_0002
                , null
                , LOG, Constants.TWO, false);
        }
    }
}
