/*
 * ModifyDate Development company    Describe 
 * 2014/07/10 CSI Parichat           Create
 * 2016/03/15 CSI Akat               [IN069]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.common.core.mailsend.JavaMailSend;
import com.globaldenso.ai.common.core.mailsend.SendMailInfoDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.jobcontrol.TransferPoDataFromCigmaResidentJob;

/**
 * <p>The Class MailServiceImpl.</p>
 * <p>Manage data send mail.</p>
 * <ul>
 * <li>Method search  : sendEmail</li>
 * </ul>
 *
 * @author CSI
 */
public class MailServiceImpl implements MailService {
    
    /**
     * Instantiates a new mail service impl.
     */
    public MailServiceImpl() {
        super();
    }

    /** {@inheritDoc}
     * @throws Exception 
     * @see com.globaldenso.eps.business.service.MailService#sendEmail(com.globaldenso.asia.sps.business.domain.SendEmailDomain)
     */
    public boolean sendEmail(SendEmailDomain sendEmailDomain) throws Exception {
        JavaMailSend javaMailSend = null;
        boolean isHaveAttach = false;

        SendMailInfoDomain sendMailInfoDomain = new SendMailInfoDomain();
        try {
            sendMailInfoDomain.setSmtpHostNm(sendEmailDomain.getEmailSmtp());
            sendMailInfoDomain.setSendFromMailAddress(sendEmailDomain.getEmailFrom());

            // [IN069] : able to send to one address
            //sendMailInfoDomain.setSendToMailAddress(
            //    StringUtil.convertListToStringCommaSeperate(sendEmailDomain.getToList()));
            if (null == sendEmailDomain.getToList() 
                || Constants.ZERO == sendEmailDomain.getToList().size()) 
            {
                sendMailInfoDomain.setSendToMailAddress(sendEmailDomain.getEmailTo());
            } else {
                sendMailInfoDomain.setSendToMailAddress(
                    StringUtil.convertListToStringCommaSeperate(sendEmailDomain.getToList()));
            }
            
            sendMailInfoDomain.setTitle(sendEmailDomain.getHeader());
            
            if(!isHaveAttach){
                sendMailInfoDomain.setMailFormat(Constants.MAIL_FORMAT_TEXT_HMTL);
                sendMailInfoDomain.setContents(sendEmailDomain.getContents());
            } else {
                sendMailInfoDomain.setMailFormat(Constants.MAIL_FORMAT_MULTIPART_MIXED);
                sendMailInfoDomain.setContents(sendEmailDomain.getContents());
            }
            javaMailSend = new JavaMailSend();
            javaMailSend.mailSend(sendMailInfoDomain);
        } catch (Exception ex){
            // [IN021] for investigate
            try {
                Log LOG = LogFactory.getLog(TransferPoDataFromCigmaResidentJob.class);
                LOG.error(" # Java mail send error LOG ########################################################################################################### ");
                LOG.error(ex.getClass().getName());
                LOG.error(ex.getMessage());
              //LOG.error("SmtpHostNm:" + sendMailInfoDomain.getSmtpHostNm());
              //LOG.error("SendFromMailAddress:" + sendMailInfoDomain.getSendFromMailAddress());
                LOG.error("SendToMailAddress:" + sendMailInfoDomain.getSendToMailAddress());
              //LOG.error("Title:" + sendMailInfoDomain.getTitle());
              //LOG.error("MailFormat:" + sendMailInfoDomain.getMailFormat());
                LOG.error("Contents:" + sendMailInfoDomain.getContents());
              //LOG.error("SmtpAuthFlg:" + sendMailInfoDomain.getSmtpAuthFlg());
              //LOG.error("SmtpAuthUserId:" + sendMailInfoDomain.getSmtpAuthUserId());
              //LOG.error("SmtpAuthPasswd:" + sendMailInfoDomain.getSmtpAuthPasswd());
                LOG.error(" # END ############################################################################################################################## ");
                
            } catch (Exception exc){
                
            }

            return false;
        }
        return true;
    }
    /*
    public static void main(String[] args) throws Exception {
        List<String> emailTo = new ArrayList<String>();
        
        emailTo.add( "def@ctcmail.com" );
        emailTo.add( "DMMD2@ctcmail.com" );
        SendEmailDomain domain = new SendEmailDomain();
        domain.setHeader("Test Error");
        domain.setEmailSmtp("10.0.0.3");
        domain.setEmailFrom("DMMD1@ctcmail.com");
        domain.setToList(emailTo);
        domain.setContents( "Contents" );
        MailServiceImpl mail = new MailServiceImpl();
        mail.sendEmail( domain );
    }
    */
}