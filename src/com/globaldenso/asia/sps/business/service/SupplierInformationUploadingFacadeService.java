/*
 * ModifyDate Development company     Describe 
 * 2014/08/22 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.io.IOException;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain;


/**
 * <p>The Interface SupplierInformationUploadingFacadeService.</p>
 * <p>Service for Supplier Information Uploading about manipulate data form CSV file.</p>
 * <ul>
 * <li>Method search : initial</li>
 * <li>Method search : searchUploadErrorList</li>
 * <li>Method insert : transactUploadSupplierUser</li>
 * <li>Method insert : transactRegisterUploadItem</li>
 * <li>Method search : searchSupplierInformationCSV</li>
 * </ul>
 *
 * @author CSI
 */
public interface SupplierInformationUploadingFacadeService {
    
    /**
     * <p>initial for Supplier Information uploading.</p>
     * <ul>
     * <li>Clear all data in temporary table.</li>
     * </ul>
     * @param supplierInfoUploadDomain that keep information to clear the temporary data.
     * @throws ApplicationException the ApplicationException.
     */
    public void transactInitial(SupplierInfoUploadDomain supplierInfoUploadDomain)
        throws ApplicationException;
    
    /**
    * <p>Search Upload Error List.</p>
    * <ul>
    * <li>Clear the temporary data.</li>
    * </ul>
    * @param supplierInfoUploadDomain It keep criteria to get data from temporary table.
    * @return list the list of Error information.
    * @throws ApplicationException the ApplicationException.
    */
    public  SupplierInfoUploadDomain searchUploadErrorList(SupplierInfoUploadDomain 
        supplierInfoUploadDomain)throws ApplicationException;
        
    /**
     * <p>upload Supplier Information.</p>
     * <ul>
     * <li>Validate Supplier Information from CSV file then display error result on screen.</li>
     * </ul>
     * 
     * @param supplierInfoUploadDomain that keep all information for upload CSV file.
     * @return SupplierUserUploadingDomain that keep a summary of upload to show on screen.
     * @throws ApplicationException the ApplicationException
     * @throws IOException the IOException
     */
    public SupplierInfoUploadDomain transactUploadSupplierInformation(SupplierInfoUploadDomain 
        supplierInfoUploadDomain) throws ApplicationException, IOException;
   
    /**
     * <p>Transact Register Upload Item.</p>
     * <ul>
     * <li>Insert supplier Information from CSV file.</li>
     * </ul>
     * 
     * @param supplierInfoUploadDomain that keep current user DSC ID and session code to move 
     * data from temporary to actual table.
     * @throws ApplicationException ApplicationException
     */
    public void transactRegisterSupplierInformation(SupplierInfoUploadDomain 
        supplierInfoUploadDomain)throws ApplicationException;
    
    /**
     * <p>Search Supplier Information CSV.</p>
     * <ul>
     * <li>Insert supplier user detail from CSV file.</li>
     * </ul>
     * 
     * @param supplierInfoUploadDomain that keep criteria to get data to export.
     * @return supplierInfoUploadDomain that keep stream of CSV File.
     * @throws ApplicationException the ApplicationException.
     */
    public SupplierInfoUploadDomain searchSupplierInformationCsv(SupplierInfoUploadDomain 
        supplierInfoUploadDomain) throws ApplicationException;
    
    /**
     * <p>Delete temporary upload item.</p>
     * <ul>
     * <li>Delete temporary upload item before upload new item.</li>
     * </ul>
     * 
     * @param userDscId the Current user DSC ID.
     * @param sessionCode the session code.
     * @throws ApplicationException ApplicationException
     */
    public void deleteUploadTempTable(String userDscId, String sessionCode)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}