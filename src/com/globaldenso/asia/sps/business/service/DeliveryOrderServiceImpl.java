/*
 * ModifyDate Development company    Describe 
 * 2014/07/22 CSI Parichat           Create
 * 2015/08/24 CSI Akat               [IN012]
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 2018/10/25 CTC P.Pawan            [IN1750]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.qrcodegenerate.QrcodeGenerator;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.dao.DeliveryOrderDao;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DoCreatedAsnDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanDeliveryOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>
 * The class DeliveryOrderServiceImpl.
 * </p>
 * <p>
 * Service for delivery order about search data from criteria.
 * </p>
 * <ul>
 * <li>Method search : searchCountAcknowledgedDO</li>
 * <li>Method search : searchAcknowledgedDO</li>
 * <li>Method search : searchCountDoDetail</li>
 * <li>Method search : searchDoDetail</li>
 * <li>Method search : searchCountDeliveryOrder</li>
 * <li>Method search : searchDeliveryOrder</li>
 * <li>Method search : searchCountKanbanOrder</li>
 * <li>Method search : searchKanbanOrder</li>
 * <li>Method search : searchDoGroupAsn</li>
 * <li>Method search : searchCountKanbanOrderHeader</li>
 * <li>Method search : searchKanbanOrderHeader</li>
 * <li>Method search : searchCountDeliveryOrderHeader</li>
 * <li>Method search : searchDeliveryOrderHeader</li>
 * <li>Method search : searchUrgentOrderForSupplierUser</li>
 * <li>Method search : searchExistsDo</li>
 * <li>Method search : searchDeliveryOrderReport</li>
 * <li>Method search : searchCountBackOrder</li>
 * <li>Method search : searchBackOrder</li>
 * <li>Method search : searchPurgingDeliveryOrder</li>
 * <li>Method search : searchPurgingUrgentDeliveryOrder</li>
 * <li>Method search : createOneWayKanbanTagReport</li>
 * </ul>
 * 
 * @author CSI
 */
public class DeliveryOrderServiceImpl extends AbstractReportServicesImpl 
    implements DeliveryOrderService {

    /** The Deliver Order Dao. */
    private DeliveryOrderDao deliveryOrderDao;

    /**
     * Instantiates a new delivery order service impl.
     */
    public DeliveryOrderServiceImpl() {
        super();
    }

    /**
     * Sets the delivery order dao.
     * 
     * @param deliveryOrderDao the delivery order dao.
     */
    public void setDeliveryOrderDao(DeliveryOrderDao deliveryOrderDao) {
        this.deliveryOrderDao = deliveryOrderDao;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.eps.business.service.DeliveryOrderService#searchCountAcknowledgedDO(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public Integer searchCountAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDOInformationDomain){
        Integer rowCount = Constants.ZERO;
        rowCount = deliveryOrderDao.searchCountAcknowledgedDo(acknowledgedDOInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.eps.business.service.DeliveryOrderService#searchAcknowledgedDO(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDOInformationDomain)
    {
        StringBuffer utcStr = null;
        StringBuffer deliveryDateStr = null;
        StringBuffer shipmentDateStr = null;
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInfoResultList
            = new ArrayList<AcknowledgedDoInformationReturnDomain>();
        
        acknowledgedDOInfoResultList = deliveryOrderDao
            .searchAcknowledgedDo(acknowledgedDOInformationDomain);
        if (null != acknowledgedDOInfoResultList
            && Constants.ZERO < acknowledgedDOInfoResultList.size())
        {
            for (AcknowledgedDoInformationReturnDomain acknowledgedDoInfo 
                : acknowledgedDOInfoResultList)
            {
                utcStr = new StringBuffer();
                deliveryDateStr = new StringBuffer();
                shipmentDateStr = new StringBuffer();
                utcStr.append(Constants.SYMBOL_OPEN_BRACKET)
                    .append(Constants.WORD_UTC)
                    .append(acknowledgedDoInfo.getUtc().trim())
                    .append(Constants.SYMBOL_CLOSE_BRACKET);
                deliveryDateStr.append(DateUtil.format(new Date(
                    acknowledgedDoInfo.getDeliveryOrderDomain().getDeliveryDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH)).append(utcStr);
                shipmentDateStr.append(DateUtil.format(new Date(
                    acknowledgedDoInfo.getDeliveryOrderDomain().getShipDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH)).append(utcStr);
                acknowledgedDoInfo.setDeliveryDateStr(deliveryDateStr.toString());
                acknowledgedDoInfo.setShipmentDateStr(shipmentDateStr.toString());
                if(null != acknowledgedDoInfo.getDeliveryOrderDomain().getDoIssueDate()){
                    acknowledgedDoInfo.setIssuedDateStr(DateUtil.format(new Date(
                        acknowledgedDoInfo.getDeliveryOrderDomain().getDoIssueDate().getTime()),
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
                }else{
                    acknowledgedDoInfo.setIssuedDateStr(Constants.EMPTY_STRING);
                }
            }
        }
        return acknowledgedDOInfoResultList;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountDeliveryOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer searchCountDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = deliveryOrderDao
            .searchCountDeliveryOrder(deliveryOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDeliveryOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public List<DeliveryOrderDetailDomain> searchDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = null;
        deliveryOrderDetailDomain = deliveryOrderDao
            .searchDeliveryOrder(deliveryOrderInformationDomain);

        return deliveryOrderDetailDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountDoDetail(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain)
     */
    public Integer searchCountDoDetail(InquiryDoDetailDomain inquiryDoDetailDomain) {
        Integer rowCountDo = Constants.ZERO;
        rowCountDo = deliveryOrderDao.searchCountDoDetail(inquiryDoDetailDomain);
        return rowCountDo;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDoDetail(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain)
     */
    public List<InquiryDoDetailReturnDomain> searchDoDetail(
        InquiryDoDetailDomain inquiryDODetailDomain){
        List<InquiryDoDetailReturnDomain> inquiryDoDetailResultList = null;
        inquiryDoDetailResultList = deliveryOrderDao
            .searchDoDetail(inquiryDODetailDomain);
        return inquiryDoDetailResultList;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountKanbanOrder(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    public Integer searchCountKanbanOrder(KanbanOrderInformationDomain kanbanOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = deliveryOrderDao.searchCountKanbanOrder(kanbanOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchKANBANOrder(com.globaldenso.asia.sps.business.domain.KANBANOrderInformationDomain)
     */
    public List<KanbanOrderInformationResultDomain> searchKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
    {
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain = null;
        kanbanOrderInformationResultDomain
            = deliveryOrderDao.searchKanbanOrder(kanbanOrderInformationDomain);
        return kanbanOrderInformationResultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDoGroupAsn(com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain)
     */
    public List<AsnMaintenanceReturnDomain> searchDoGroupAsn(
        AsnMaintenanceDomain asnMaintenanceDomain)
    {
        List<AsnMaintenanceReturnDomain> doGroupAsnResultList = null;
        doGroupAsnResultList = deliveryOrderDao.searchDoGroupAsn(asnMaintenanceDomain);
        return doGroupAsnResultList;
    }

    // [IN012] : not use
    ///**
    // * {@inheritDoc}
    // * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCompleteShipDo(com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain)
    // */
    //public List<AsnMaintenanceDomain> searchCompleteShipDo(
    //    List<AsnMaintenanceDomain> criteriaDomainList)
    //{
    //    AsnMaintenanceDomain asnMaintenance = null;
    //    List<AsnMaintenanceDomain> doResultList = new ArrayList<AsnMaintenanceDomain>();
    //    List<AsnMaintenanceReturnDomain> deliveryOrderList = null;
    //    for (AsnMaintenanceDomain criteria : criteriaDomainList) {
    //        deliveryOrderList = deliveryOrderDao.searchCompleteShipDo(criteria);
    //        if (null == deliveryOrderList) {
    //            deliveryOrderList = new ArrayList<AsnMaintenanceReturnDomain>();
    //        }
    //        asnMaintenance = new AsnMaintenanceDomain();
    //        asnMaintenance.setDoGroupAsnList(deliveryOrderList);
    //        asnMaintenance.setDoId(criteria.getDoId());
    //        asnMaintenance.setLastUpdateDatetime(criteria.getLastUpdateDatetime());
    //        doResultList.add(asnMaintenance);
    //    }
    //    return doResultList;
    //}
    
    // [IN012] : Create method for check D/O status
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchExamineDoStatus(java.util.List)
     */
    public List<SpsTDoDomain> searchExamineDoStatus(List<SpsTDoDomain> criteriaDomainList) {
        List<SpsTDoDomain> doResultList = new ArrayList<SpsTDoDomain>();
        SpsTDoDomain doHeader = null;
        for (SpsTDoDomain criteria : criteriaDomainList) {
            List<String> partsStatus 
                = deliveryOrderDao.searchDistinctPnStatus(criteria);
            String doStatus = Constants.EMPTY_STRING;
            
            if (Constants.THREE <= partsStatus.size()) {
                doStatus = Constants.SHIPMENT_STATUS_PTS;
            } else if (Constants.ONE == partsStatus.size()) {
                doStatus = partsStatus.get(Constants.ZERO);
            } else {
                if (Constants.SHIPMENT_STATUS_CCL.equals(partsStatus.get(Constants.ZERO))) {
                    doStatus = partsStatus.get(Constants.ONE);
                } else {
                    doStatus = Constants.SHIPMENT_STATUS_PTS;
                }
            }
            
            doHeader = new SpsTDoDomain();
            doHeader.setShipmentStatus(doStatus);
            doHeader.setDoId(criteria.getDoId());
            doHeader.setLastUpdateDatetime(criteria.getLastUpdateDatetime());
            doResultList.add(doHeader);
        }
        
        return doResultList;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountKanbanOrderHeader(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    public Integer searchCountKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = deliveryOrderDao.searchCountKanbanOrderHeader(kanbanOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchKanbanOrderHeader(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    public List<KanbanOrderInformationResultDomain> searchKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
    {
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain = null;
        kanbanOrderInformationResultDomain = deliveryOrderDao
            .searchKanbanOrderHeader(kanbanOrderInformationDomain);
        return kanbanOrderInformationResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountDeliveryOrderHeader(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer searchCountDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = deliveryOrderDao.searchCountDeliveryOrderHeader(deliveryOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDeliveryOrderHeader(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public List<DeliveryOrderDetailDomain> searchDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = null;
        deliveryOrderDetailDomain = deliveryOrderDao
            .searchDeliveryOrderHeader(deliveryOrderInformationDomain);
        return deliveryOrderDetailDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchUrgentOrderForSupplierUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    public List<MainScreenResultDomain> searchUrgentOrderForSupplierUser(
        TaskListDomain taskListCriteriaDomain){
        List<MainScreenResultDomain> mainScreenResultDomain = deliveryOrderDao
            .searchUrgentOrderForSupplierUser(taskListCriteriaDomain);
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchExistDo(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public List<DeliveryOrderDetailDomain> searchExistDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailResultList = null;
        deliveryOrderDetailResultList
            = deliveryOrderDao.searchExistDo(deliveryOrderInformationDomain);
        return deliveryOrderDetailResultList;
    }


    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDeliveryOrderReport(com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain)
     */
    public InputStream searchDeliveryOrderReport(
        DeliveryOrderSubDetailDomain deliveryOrderSubDetailDomain)
        throws Exception {
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<DeliveryOrderReportDomain> deliveryOrderReportDomain = null;
        deliveryOrderReportDomain = deliveryOrderDao
            .searchDeliveryOrderReport(deliveryOrderSubDetailDomain);
        
        Locale locale = deliveryOrderSubDetailDomain.getLocale();
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setMinimumFractionDigits(Constants.TWO);
        
        if(null == deliveryOrderReportDomain 
            || Constants.ZERO == deliveryOrderReportDomain.size()){
            return null;
        }
        
        for(DeliveryOrderReportDomain reportDomain : deliveryOrderReportDomain ){
            reportDomain.setIssueDateString(DateUtil.format(reportDomain.getIssueDate(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtdString(DateUtil.format(reportDomain.getEtd(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtaString(DateUtil.format(reportDomain.getEta(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtaTime(DateUtil.format(reportDomain.getEta(), 
                DateUtil.PATTERN_HHMM_COLON));
            reportDomain.setEtdTime(DateUtil.format(reportDomain.getEtd(), 
                DateUtil.PATTERN_HHMM_COLON));
            reportDomain.setDeliveryDateString(DateUtil.format(reportDomain.getEta(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setDeliveryTime(DateUtil.format(reportDomain.getEta(), 
                DateUtil.PATTERN_HHMM_COLON));
            reportDomain.setQuantityperBox(decimalFormat.format(reportDomain.
                getQuantityperBoxBigDec()).toString());
            reportDomain.setNoofBox(decimalFormat.format(reportDomain.
                getNoofBoxBigDec()).toString());
            reportDomain.setPreviousQty(decimalFormat.format(reportDomain
                .getPreviousQtyBigDec()).toString());
            reportDomain.setCurrentQty(decimalFormat.format(reportDomain.
                getCurrentQtyBigDec()).toString());
            BigDecimal currentQty = reportDomain.getCurrentQtyBigDec();
            BigDecimal previousQty = reportDomain.getPreviousQtyBigDec();
            reportDomain.setDiffQty(decimalFormat.format(currentQty.
                subtract(previousQty)).toString());
            
            reportDomain.setTxtDeliveryOrder(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_REPORT_NAME));
            reportDomain.setTxtRouteNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ROUTE_NO ));
            reportDomain.setTxtDelNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DEL_NO));
            reportDomain.setTxtSCd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUPPLIER_CODE));
            reportDomain.setTxtSupName(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUP_NAME));
            reportDomain.setTxtSupLoc(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUP_LOC));
            reportDomain.setTxtSupPlant(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUP_PLANT));
            reportDomain.setTxtIssueDate(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ISSUE_DATE));
            reportDomain.setTxtPage(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_PAGE));
            reportDomain.setTxtCurrrent(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CURRENT_SPS_DO_NO));
            reportDomain.setTxtOriginal(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ORIGINAL_SPS_DO_NO));
            reportDomain.setTxtEtd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ETD_SCHEDULE));
            reportDomain.setTxtShipDate(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SHIP_DATE));
            reportDomain.setTxtTime(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_TIME));
            reportDomain.setTxtEta(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ETA_SCHEDULE));
            reportDomain.setTxtDelDate(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DEL_DATE));
            reportDomain.setTxtSPcd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_PLANT_CODE));
            reportDomain.setTxtWareHouse(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_WARE_HOUSE));
            reportDomain.setTxtDockCode(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DOCK_CODE));
            reportDomain.setTxtRcvLane(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_RCV_LANE));
            reportDomain.setTxtTran(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_TRANS));
            reportDomain.setTxtCtrlNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CTRL_NO));
            reportDomain.setTxtdPn(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_D_PART_NUMBER));
            reportDomain.setTxtsPn(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_S_PART_NUMBER));
            reportDomain.setTxtDesc(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DESCRIPTION));
            reportDomain.setTxtUm(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_U_M));
            reportDomain.setTxtQtyPerBox(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_QUANTITY_BOX));
            reportDomain.setTxtNoOfBox(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_NO_OF_BOX));
            reportDomain.setTxtPreQty(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_PREVIOUS_QTY));
            reportDomain.setTxtCurrQty(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CURRENT_QTY));
            reportDomain.setTxtDiffQty(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DIFF_QTY));
            reportDomain.setTxtRsn(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_RSN));           
            reportDomain.setTxtCurrCigma(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CURRENT_CIGMA_DO_No));
            reportDomain.setTxtTitle(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_THIS));
            reportDomain.setTxtAs(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_AT));
            reportDomain.setTxtComment(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_COMMENT));
            reportDomain.setTxtBackOrder(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_BACK_ORDER));
            reportDomain.setTxtWrongOrder(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_WRONG_ORDER));
            reportDomain.setTxtProd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_PROD_SCHEDULE_CHANGE));
            reportDomain.setTxtCust(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CUST_URGENT_ORDER));
            reportDomain.setTxtQtyPro(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_QUALITY_PROBLEM));
            reportDomain.setTxtOth(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_OTHER));
            reportDomain.setTxtSupplier(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUPPLIER));
            reportDomain.setTxtSing(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SIGNATURE));
            
            StringBuffer dCd = new StringBuffer();
            dCd.append(Constants.SYMBOL_OPEN_BRACKET);
            dCd.append(reportDomain.getDCd());
            dCd.append(Constants.SYMBOL_CLOSE_BRACKET);
            
            reportDomain.setDCd(dCd.toString());
        }
        
        JasperPrint jpChangeMR = generateReport(getReportPathFixed(Constants.ZERO), 
            deliveryOrderReportDomain , parameters);
        return generate(jpChangeMR);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchKanbanDeliveryOrderReport(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDODetailReturnDomain)
     */
    public InputStream searchKanbanDeliveryOrderReport(
        KanbanOrderInformationDoDetailReturnDomain kanbanOrderInformationDODetailReturnDomain)
        throws Exception
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<KanbanDeliveryOrderReportDomain> kanbanDeliveryOrderReportDomain = null;
        kanbanDeliveryOrderReportDomain = deliveryOrderDao.searchKanbanDeliveryOrderReport(
            kanbanOrderInformationDODetailReturnDomain);
        
        Locale locale = kanbanOrderInformationDODetailReturnDomain.getLocale();
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setMinimumFractionDigits(Constants.TWO);
        
        if(null == kanbanDeliveryOrderReportDomain 
            || Constants.ZERO == kanbanDeliveryOrderReportDomain.size()){
            return null;
        }
        String kanbanSeqFlg = null;
        for(KanbanDeliveryOrderReportDomain reportDomain : kanbanDeliveryOrderReportDomain ){
            
            reportDomain.setIssueDateString(DateUtil.format(reportDomain.getIssueDate(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtdString(DateUtil.format(reportDomain.getEtd(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtaString(DateUtil.format(reportDomain.getEta(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            reportDomain.setEtaTime(DateUtil.format(reportDomain.getEta(), 
                DateUtil.PATTERN_HHMM_COLON));
            reportDomain.setEtdTime(DateUtil.format(reportDomain.getEtd(), 
                DateUtil.PATTERN_HHMM_COLON));
            
            if(null != reportDomain.getKanbanSequence()){
                StringBuffer buffer = new StringBuffer();
                buffer.append(Constants.SYMBOL_OPEN_BRACKET);
                buffer.append(reportDomain.getKanbanSequence());
                buffer.append(Constants.SYMBOL_CLOSE_BRACKET);
                reportDomain.setKanbanSequence(buffer.toString());
            }
            
            reportDomain.setQuantityPerBox(decimalFormat.format(reportDomain.
                getQuantityPerBoxBigDec()).toString());
            reportDomain.setNoOfBox(decimalFormat.format(reportDomain.
                getNoOfBoxBigDec()).toString());
            reportDomain.setCurrentQty(decimalFormat.format(reportDomain.
                getCurrentQtyBigDec()).toString());
            
            reportDomain.setTxtReportName(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD005_REPORT_NAME));
            reportDomain.setTxtRouteNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ROUTE_NO ));
            reportDomain.setTxtDelNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DEL_NO));
            reportDomain.setTxtSCd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUPPLIER_CODE));
            reportDomain.setTxtSupName(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUP_NAME));
            reportDomain.setTxtSupLoc(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUP_LOC));
            reportDomain.setTxtSupPlant(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUP_PLANT));
            reportDomain.setTxtIssueDate(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ISSUE_DATE));
            reportDomain.setTxtPage(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_PAGE));
            reportDomain.setTxtCurrrent(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CURRENT_SPS_DO_NO));
            reportDomain.setTxtOriginal(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ORIGINAL_SPS_DO_NO));
            reportDomain.setTxtEtd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ETD_SCHEDULE));
            reportDomain.setTxtShipDate(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SHIP_DATE));
            reportDomain.setTxtTime(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_TIME));
            reportDomain.setTxtEta(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_ETA_SCHEDULE));
            reportDomain.setTxtDelDate(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DEL_DATE));
            reportDomain.setTxtSPcd(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_PLANT_CODE));
            reportDomain.setTxtWareHouse(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_WARE_HOUSE));
            reportDomain.setTxtDockCode(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DOCK_CODE));
            reportDomain.setTxtRcvLane(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_RCV_LANE));
            reportDomain.setTxtTran(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_TRANS));
            reportDomain.setTxtCycle(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD005_CYCLE));
            reportDomain.setTxtCtrlNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CTRL_NO));
            reportDomain.setTxtdPn(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_D_PART_NUMBER));
            reportDomain.setTxtsPn(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_S_PART_NUMBER));
            reportDomain.setTxtDesc(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_DESCRIPTION));
            reportDomain.setTxtUm(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_U_M));
            reportDomain.setTxtKanbanSeqNo(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD005_KANBAN_SEQ_NO));
            reportDomain.setTxtQtyPerBox(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_QUANTITY_BOX));
            reportDomain.setTxtNoOfBox(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_NO_OF_BOX));
            reportDomain.setTxtCurrQty(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CURRENT_QTY));
            reportDomain.setTxtCurrCigma(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_CURRENT_CIGMA_DO_No));
            reportDomain.setTxtSupplier(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SUPPLIER));
            reportDomain.setTxtSing(MessageUtil.getReportLabel(
                locale, SupplierPortalConstant.RORD003_SIGNATURE));
            
            StringBuffer dCd = new StringBuffer();
            dCd.append(Constants.SYMBOL_OPEN_BRACKET);
            dCd.append(reportDomain.getDCd());
            dCd.append(Constants.SYMBOL_CLOSE_BRACKET);
            
            reportDomain.setDCd(dCd.toString());
            kanbanSeqFlg = reportDomain.getKanbanSeqFlag();
            
        }
        
        int pathFlag = Constants.ONE;
        if (Constants.STR_TWO.equals(kanbanSeqFlg)) {
            pathFlag = Constants.TWO;
        }
        
        JasperPrint jpChangeMR = generateReport(getReportPathFixed(pathFlag), 
            kanbanDeliveryOrderReportDomain , parameters);
        return generate(jpChangeMR);
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountBackOrder(com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    public Integer searchCountBackOrder(BackOrderInformationDomain backOrderInformationDomain){
        Integer rowCount = Constants.ZERO;
        rowCount = this.deliveryOrderDao.searchCountBackOrder(backOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchBackOrder(com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    public List<BackOrderInformationDomain> searchBackOrder(
        BackOrderInformationDomain backOrderInformationDomain)
    {
        List<BackOrderInformationDomain> backOrderInformationList = null;
        backOrderInformationList = this.deliveryOrderDao.
            searchBackOrder(backOrderInformationDomain);
        
        StringBuffer deliveryDatetime = null;
        StringBuffer utc = null;
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        
        for(BackOrderInformationDomain item : backOrderInformationList)
        {
            utc = new StringBuffer();
            utc.append(Constants.SYMBOL_OPEN_BRACKET);
            utc.append(Constants.SYMBOL_SPACE);
            utc.append(Constants.WORD_UTC);
            utc.append(item.getSpsMCompanyDensoDomain().getUtc());
            utc.append(Constants.SYMBOL_CLOSE_BRACKET);
            
            deliveryDatetime = new StringBuffer();
            deliveryDatetime.append(DateUtil.format(new Date(
                item.getSpsTDoDomain().getDeliveryDatetime().getTime()),
                DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
            deliveryDatetime.append(utc);
            item.setDeliveryDate(deliveryDatetime.toString());
            
            BigDecimal currentOrderQty = new BigDecimal(Constants.ZERO);
            if (null != item.getSpsTDoDetailDomain().getCurrentOrderQty()) {
                currentOrderQty = item.getSpsTDoDetailDomain().getCurrentOrderQty();
            }
            item.setCurrentOrderQty(decimalFormat.format(currentOrderQty));
        }
        return backOrderInformationList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchPurgingDeliveryOrder(com.globaldenso.asia.sps.business.domain.PurgingPODomain)
     */
    public List<PurgingDoDomain> searchPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain)
    {
        List<PurgingDoDomain> deliveryOrderList = null;
        deliveryOrderList = this.deliveryOrderDao.searchPurgingDeliveryOrder(purgingPoDomain);
        return deliveryOrderList;
    }

    // [IN012] for check delete P/O
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchCountNotPurgingDeliveryOrder(com.globaldenso.asia.sps.business.domain.PurgingPODomain)
     */
    public Integer searchCountNotPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain)
    {
        Integer result = null;
        result = this.deliveryOrderDao.searchCountNotPurgingDeliveryOrder(purgingPoDomain);
        return result;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#createDeliveryOrder(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    public BigDecimal createDeliveryOrder(SpsTDoDomain domain) throws ApplicationException {
        return deliveryOrderDao.createDeliveryOrder(domain);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchExistDeliveryOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain)
     */
    public List<SpsTDoDomain> searchExistDeliveryOrder(DeliveryOrderDomain paramters){
        List<SpsTDoDomain> doResultList = null;
        doResultList = deliveryOrderDao.searchExistDeliveryOrder(paramters);
        return doResultList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchUrgentOrder(com.globaldenso.asia.sps.business.domain.UrgentOrderDomain)
     */
    public List<DeliveryOrderDomain> searchUrgentOrder(UrgentOrderDomain criteria){
        List<DeliveryOrderDomain> doResultList = null;
        doResultList = deliveryOrderDao.searchUrgentOrder(criteria);
        return doResultList;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDoCreatedAsn(com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain)
     */
    public List<DoCreatedAsnDomain> searchDoCreatedAsn(DeliveryOrderDomain criteria){
        List<DoCreatedAsnDomain> asnList = deliveryOrderDao.searchDoCreatedAsn(criteria);
        return asnList;
    }
    
    /**
     * <p>Get Report Path method.</p>
     *
     * @param reportType int
     * @return Path
     */
    private String getReportPathFixed(int reportType){
        if(Constants.ZERO == reportType ){
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.DELIVERY_ORDER_REPORT);
        } else if (Constants.ONE == reportType ) {
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.KANBAN_DELOVERY_ORDER_REPORT);
        } else {
            return StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.KANBAN_DELOVERY_ORDER_REPORT_NO_SEQ);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchPurgingUrgentDeliveryOrder(com.globaldenso.asia.sps.business.domain.PurgingPoDomain)
     */
    public List<PurgingDoDomain> searchPurgingUrgentDeliveryOrder(PurgingPoDomain purgingPoDomain) {
        List<PurgingDoDomain> purgingUrgentDo = null;
        purgingUrgentDo = deliveryOrderDao.searchPurgingUrgentDeliveryOrder(purgingPoDomain);
        return purgingUrgentDo;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDeliveryOrderAcknowledgement(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        SpsTPoDomain poDomain = null;
        poDomain = deliveryOrderDao.searchDeliveryOrderAcknowledgement(
            deliveryOrderInformationDomain);
        return poDomain;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#updateDateTimeKanbanPdf(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer updateDateTimeKanbanPdf(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        Integer resultCode = Constants.ZERO;
        resultCode = deliveryOrderDao
            .updateDateTimeKanbanPdf(deliveryOrderInformationDomain);
        return resultCode;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchOneWayKanbanTagReportInformation(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public List<OneWayKanbanTagReportDomain> searchOneWayKanbanTagReportInformation(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        List<OneWayKanbanTagReportDomain> deliveryOrderInformationResultDomain = null;
        deliveryOrderInformationResultDomain = deliveryOrderDao.searchOneWayKanbanTagReportInformation(
            deliveryOrderInformationDomain);
        return deliveryOrderInformationResultDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#createOneWayKanbanTagReport(List of com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain)
     */
    public InputStream createOneWayKanbanTagReport(
        List<OneWayKanbanTagReportDomain> oneWayKanbanTagReportDomain)
        throws Exception {
        Map<String, Object> parameters = null;
        String currentDate = DateUtil.format(
            Calendar.getInstance().getTime(), DateUtil.PATTERN_YYYYMMDD);
        List<OneWayKanbanTagReportDomain> oneWayKanbanTagReport = new ArrayList<OneWayKanbanTagReportDomain>();
        List<OneWayKanbanTagReportDomain> oneWayKanbanInnerTagReport = new ArrayList<OneWayKanbanTagReportDomain>();
        //IN[1750] Number of Tag not sequence by P/No. ---Start---
        int runningNumber =0;
        for(Integer i = 0; i < oneWayKanbanTagReportDomain.size(); i++){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            QrcodeGenerator.generateQrcode(oneWayKanbanTagReportDomain.get(i).getQrContent(), Constants.STR_M,
                Constants.HTTP_HEADER_UTF_8, Constants.QR_CODE_SIZE, baos);
            
            oneWayKanbanTagReportDomain.get(i).setPrintDate(currentDate);
            if(i > 0 && 
                ( oneWayKanbanTagReportDomain.get(i).getdPn().equals(oneWayKanbanTagReportDomain.get(i-1).getdPn()) 
                 && oneWayKanbanTagReportDomain.get(i).getsPn().equals(oneWayKanbanTagReportDomain.get(i-1).getsPn()))
                 ){
                runningNumber++;
            }else{
                runningNumber = 1;
            }
            oneWayKanbanTagReportDomain.get(i).setNumberOfBox(runningNumber + "/" + oneWayKanbanTagReportDomain.get(i).getNumberOfBox());
            oneWayKanbanTagReportDomain.get(i).setQrContentInputStream(new ByteArrayInputStream(baos.toByteArray()));
            // IN[1796] Change One-way Kanban Tag field 'Location'. ---Start---
            if(oneWayKanbanTagReportDomain.get(i)!=null 
                && oneWayKanbanTagReportDomain.get(i).getWhLocation() != null
                && !oneWayKanbanTagReportDomain.get(i).getWhLocation().trim().equals(Constants.EMPTY_STRING)
                && oneWayKanbanTagReportDomain.get(i).getWhLocation().trim().length() > Constants.FIVE
                ){
                oneWayKanbanTagReportDomain.get(i).setWhLocation(oneWayKanbanTagReportDomain.get(i).getWhLocation().trim().substring(0,5));
            }
            // IN[1796] Change One-way Kanban Tag field 'Location'. ---End---
            if(oneWayKanbanTagReportDomain.get(i).getKanbanType().equals(Constants.STR_THREE)){
                oneWayKanbanInnerTagReport.add(oneWayKanbanTagReportDomain.get(i));
            } else {
                oneWayKanbanTagReport.add(oneWayKanbanTagReportDomain.get(i));
            }
        }
      //IN[1750] Number of Tag not sequence by P/No. ---END---
        List<JasperPrint> printList = new ArrayList<JasperPrint>();
        if(oneWayKanbanTagReport.size() > Constants.ZERO){
            JasperPrint jpPage1 = generateReport(StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.ONE_WAY_KANBAN_TAG_REPORT_JASPER_FILE_NAME), oneWayKanbanTagReport, parameters);
            System.out.println(StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.ONE_WAY_KANBAN_TAG_REPORT_JASPER_FILE_NAME));
            printList.add(jpPage1);
        }
        if(oneWayKanbanInnerTagReport.size() > Constants.ZERO){
            JasperPrint jpPage2 = generateReport(StringUtil.appendsString(ContextParams.getJasperFilePath(),
                SupplierPortalConstant.ONE_WAY_KANBAN_INNER_TAG_REPORT_JASPER_FILE_NAME), oneWayKanbanInnerTagReport, parameters);
            printList.add(jpPage2);
        }
        return generate(printList);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchNotifyDeliveryOrderToSupplier(com.globaldenso.asia.sps.business.domain.spsTDoDomain)
     */
    public List<SpsTDoDomain> searchNotifyDeliveryOrderToSupplier(SpsTDoDomain spsTDoDomain) {
        List<SpsTDoDomain> spsTDoDomainList = null;
        spsTDoDomainList = deliveryOrderDao.searchNotifyDeliveryOrderToSupplier(
            spsTDoDomain);
        return spsTDoDomainList;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#createKanbanTag(com.globaldenso.asia.sps.business.domain.KanbanTagDomain)
     */
    public BigDecimal createKanbanTag(KanbanTagDomain domain)
        throws ApplicationException {
    	return deliveryOrderDao.createKanbanTag(domain);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#updateOneWayKanbanTagFlag(com.globaldenso.asia.sps.business.domain.KanbanTagDomain)
     */
    public Integer updateOneWayKanbanTagFlag(KanbanTagDomain domain)
        throws ApplicationException {
        return deliveryOrderDao.updateOneWayKanbanTagFlag(domain);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchBatchNumber(void)
     */
    public Integer searchBatchNumber()
        throws ApplicationException {
        return deliveryOrderDao.searchBatchNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#createTransmitLog(com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain)
     */
    public BigDecimal createTransmitLog(ReceiveByScanDomain receiveByScanDomain)
        throws ApplicationException {
        return deliveryOrderDao.createTransmitLog(receiveByScanDomain);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#isExistTransmitLog(com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain)
     */
    public List<ReceiveByScanDomain> isExistTransmitLog(ReceiveByScanDomain receiveByScanDomain)
        throws ApplicationException {
        return deliveryOrderDao.isExistTransmitLog(receiveByScanDomain);
    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceService#deletePurgingInvoice(String)
     */
    public int deletePurgingDo(String doId){
        return this.deliveryOrderDao.deletePurgingDo(doId);
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchExistDeliveryOrderBackOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain)
     */
    public List<SpsTDoDomain> searchExistDeliveryOrderBackOrder(DeliveryOrderDomain paramters){
        List<SpsTDoDomain> doResultList = null;
        doResultList = deliveryOrderDao.searchExistDeliveryOrderBackOrder(paramters);
        return doResultList;
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchExistDeliveryOrderBackDetailOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain)
     */
    public List<SpsTDoDetailDomain> searchExistDeliveryOrderBackOrderDetail(DeliveryOrderDomain paramters){
        List<SpsTDoDetailDomain> doResultList = null;
        doResultList = deliveryOrderDao.searchExistDeliveryOrderBackOrderDetail(paramters);
        return doResultList;
    }
    
    
    public Integer updateBackOrderDetail(SpsTDoDetailDomain parameters) throws ApplicationException{
        return deliveryOrderDao.updateBackOrderDetail(parameters);
    }
    
    public Integer updateBackOrderHeader(SpsTDoDomain parameters) throws ApplicationException{
        return deliveryOrderDao.updateBackOrderHeader(parameters);
    }
    
    public List<DeliveryOrderDomain> searchMailBackOrder(SpsTDoDetailDomain criteria){
        List<DeliveryOrderDomain> doResultList = null;
        doResultList = deliveryOrderDao.searchMailBackOrder(criteria);
        return doResultList;
    }

	public Integer deleteBhtTransmitLog(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException {
        return deliveryOrderDao.deleteBhtTransmitLog(purgingBhtTransmitLogCriteria);
	}

	public Integer deleteDeliveryOrder(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException {
        return deliveryOrderDao.deleteDeliveryOrder(purgingBhtTransmitLogCriteria);
	}
	/*
	public boolean createKanbanTagList(List<KanbanTagDomain> kanbanList)
	        throws ApplicationException {
	        return deliveryOrderDao.createKanbanTagList(kanbanList);
	}
	*/
	public Integer insertKanbanTag(KanbanTagDomain domain)
	        throws ApplicationException {
	    	return deliveryOrderDao.insertKanbanTag(domain);
	}
}