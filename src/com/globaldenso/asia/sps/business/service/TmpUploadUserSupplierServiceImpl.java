/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import com.globaldenso.asia.sps.business.domain.TmpUserSupplierDomain;
import com.globaldenso.asia.sps.business.dao.TmpUploadUserSupplierDao;


/**
 * <p>The Class TempUserSupplierServiceImpl.</p>
 * <p>Manage data of Temporary User Supplier.</p>
 * <ul>
 * <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class TmpUploadUserSupplierServiceImpl implements TmpUploadUserSupplierService {

    /** The  Temporary User Supplier Dao. */
    private TmpUploadUserSupplierDao tmpUploadUserSupplierDao;
    
    /**
     * Instantiates a new Temporary User Supplier Service implement.
     */
    public TmpUploadUserSupplierServiceImpl(){
        super();
    }
    
    /**
     * Sets the Temporary User Supplier Dao.
     * 
     * @param tmpUploadUserSupplierDao the new Temporary User Supplier Dao.
     */
    public void setTmpUploadUserSupplierDao(
        TmpUploadUserSupplierDao tmpUploadUserSupplierDao) {
        this.tmpUploadUserSupplierDao = tmpUploadUserSupplierDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpUploadUserSupplierService#searchTempUploadSupplierOneRecord(com.globaldenso.asia.sps.business.domain.TmpUserSupplierDomain)
     */
    public TmpUserSupplierDomain searchTmpUploadSupplierOneRecord(TmpUserSupplierDomain 
        tmpUserSupplierDomain){
        TmpUserSupplierDomain result = (TmpUserSupplierDomain)tmpUploadUserSupplierDao
            .searchTmpUploadSupplierOneRecord(tmpUserSupplierDomain);
        return result;
    }
    
}