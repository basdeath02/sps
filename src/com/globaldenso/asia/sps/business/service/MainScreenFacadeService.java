/*
 * ModifyDate Development company     Describe 
 * 2014/08/15 CSI Chatchai                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;

/**
 * <p>
 * This screen is opening after login to the system success this page has two main informations  1) DENSO company announcement message  2) Task list of system operations.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public interface MainScreenFacadeService {
   
    /**
     * <p>Search Initial method.</p>
     *
     * @param dscId dscId
     * @param locale locale
     * @return UserLoginDomain
     * @throws ApplicationException ApplicationException
     */
    public UserLoginDomain searchInitial(String dscId, Locale locale) throws ApplicationException;
    
    /**
     * <p>Search User role method.</p>
     *
     * @param dscId dscId
     * @param locale locale
     * @throws ApplicationException ApplicationException
     */
    public void searchUserRole(String dscId, Locale locale) throws ApplicationException;
    
    /**
     * <p>Search Welcome Message method.</p>
     *
     * @param userLoginDomain userLoginDomain
     * @param locale locale
     * @param supplierAuthenList the list of supplier authen
     * @param densoAuthenList the list of denso authen
     * @return MainScreenDomain
     * @throws ApplicationException ApplicationException
     */
    public MainScreenDomain searchWelcomMessage(UserLoginDomain userLoginDomain,
        Locale locale, List<SpsMPlantSupplierDomain> supplierAuthenList,
        List<SpsMPlantDensoDomain> densoAuthenList) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}
