/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;

/**
 * <p>The Interface ScreenService.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchScreenByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public interface ScreenService {

    /**
     * Search for screen and Plant that user has permission (user is DENSO).
     * @param dscId user login id
     * @param isRoleUserActive is User Role active
     * @param isScreenActive is screen active
     * @param currentDatetime current datetime
     * @return List of screen and plant
     * */
    public List<RoleScreenDomain> searchScreenByUserPermission(
        String dscId, String isRoleUserActive, String isScreenActive, Timestamp currentDatetime);

}
