/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.business.dao.CompanySupplierDao;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * <p>The class CompanySupplierServiceImpl implements CompanySupplierService.</p>
 * <p>Service for Company Supplier.</p>
 * <ul>
 * <li>Method search  : searchCompanySupplier</li>
 * <li>Method search  : searchCompanySupplierNameByRelation</li>
 * <li>Method search  : seachCompanySupplierByCode</li>
 * <li>Method search  : searchExistCompanySupplierInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class CompanySupplierServiceImpl implements CompanySupplierService {

    /** The company Supplier DAO. */
    private CompanySupplierDao companySupplierDao;
    
    /** The default constructor. */
    public CompanySupplierServiceImpl() {
        super();
    }
    
    /**
     * The setter method for companySupplierDao.
     * 
     * @param companySupplierDao the new companySupplierDao to set.
     * */
    public void setCompanySupplierDao(CompanySupplierDao companySupplierDao) {
        this.companySupplierDao = companySupplierDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanySupplierService#searchSupplierCompany
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope) 
    {
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(
            plantSupplierWithScope.getDataScopeControlDomain().getUserType()))
        {
            return this.companySupplierDao.searchCompanySupplierByRole(plantSupplierWithScope);
        } else {
            return this.companySupplierDao.searchCompanySupplierByRelation(
                plantSupplierWithScope);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanySupplierService#searchCompanySupplierNameByRelation
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchCompanySupplierNameByRelation(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain){
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();
        result = this.companySupplierDao.searchCompanySupplierNameByRelation(
            companySupplierWithScopeDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanySupplierService#seachSupplierCompanyByCode
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierDomain)
     */
    public CompanySupplierDomain searchCompanySupplierByCode(
        CompanySupplierDomain companySupplierCriteria)
    {
        CompanySupplierDomain companySupplier = null;
        companySupplier = this.companySupplierDao.searchCompanySupplierByCode(
            companySupplierCriteria);
        return companySupplier;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanySupplierService#searchExistCompanySupplierInformation
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierDomain)
     */
    public Integer searchExistCompanySupplierInformation(
        CompanySupplierDomain companySupplierDomain)
    {
        Integer recordCount = null;
        recordCount = this.companySupplierDao.searchExistCompanySupplierInformation(
            companySupplierDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CompanySupplierService#searchExistCompanySupplier
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierDomain)
     */
    public Integer searchExistCompanySupplier(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain)
    {
        Integer recordCount = 0;
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(
            companySupplierWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            recordCount = this.companySupplierDao.searchExistCompanySupplierByRole(
                companySupplierWithScopeDomain);
        }else{
            recordCount = this.companySupplierDao.searchExistCompanySupplierByRelation(
                companySupplierWithScopeDomain);
        }
        return recordCount;
    }
}
