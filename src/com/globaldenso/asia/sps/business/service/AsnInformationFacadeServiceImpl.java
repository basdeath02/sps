/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The class ASNInformationFacadeServiceImpl.</p>
 * <p>Facade for ASNInformationFacadeServiceImpl.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchAsnInformation</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method search  : validateGroupInvoice</li>
 * </ul>
 *
 * @author CSI
 */
public class AsnInformationFacadeServiceImpl implements AsnInformationFacadeService {

    /** The ASN service. */
    private AsnService asnService = null;
    
    /** The misc service. */
    private MiscellaneousService miscService = null;
    
    /** The file management service. */
    private FileManagementService fileManagementService = null;
    
    /** The sps master company denso service. */
    private SpsMCompanyDensoService spsMCompanyDensoService = null;
    
    /** The Denso Supplier Relation service. */
    private DensoSupplierRelationService densoSupplierRelationService = null; 
    
    /** The Supplier company service. */
    private CompanySupplierService companySupplierService = null;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The Plant Supplier Service. */
    private PlantSupplierService plantSupplierService = null; 
    
    /** The Plant Denso Service. */
    private PlantDensoService plantDensoService = null;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public AsnInformationFacadeServiceImpl(){
        super();
    }

    /**
     * Set the ASN Service.
     * 
     * @param asnService the ASN service to set
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the sps master company denso service.
     * 
     * @param spsMCompanyDensoService the sps master company denso service to set
     */
    public void setSpsMCompanyDensoService(SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Set the plant supplier service.
     * 
     * @param plantSupplierService the plant supplier service to set
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }
    
    /**
     * Set the plant denso service.
     * 
     * @param plantDensoService the plant denso service to set
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchInitial(
     * com.globaldenso.asia.sps.business.domain.DataScopeControlDomain, 
     * com.globaldenso.asia.sps.business.domain.AsnInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    public AsnInformationReturnDomain searchInitial(DataScopeControlDomain dataScopeControlDomain, 
        AsnInformationDomain asnInformationDomain)throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        AsnInformationReturnDomain asnInformationReturnDomain = new AsnInformationReturnDomain();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<CompanySupplierDomain> companySupplierList = null;
        List<CompanyDensoDomain> companyDensoList = null;
        MiscellaneousDomain asnStatusMiscDomain = null;
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                Constants.DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
        
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companySupplierList = this.companySupplierService.searchCompanySupplier(
            plantSupplierWithScope);
        if(Constants.ZERO == companySupplierList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        asnInformationReturnDomain.setCompanySupplierList(companySupplierList);
        
        if(null != asnInformationDomain.getSpsTAsnDomain().getVendorCd()){
            plantSupplierWithScope.getPlantSupplierDomain().setVendorCd(
                asnInformationDomain.getSpsTAsnDomain().getVendorCd());
        }
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getVendorCd())){
            plantSupplierWithScope.getPlantSupplierDomain().setVendorCd(null);
        }
        
        List<PlantSupplierDomain> companySupplierPlantList = null;
        companySupplierPlantList = this.plantSupplierService
            .searchPlantSupplier(plantSupplierWithScope);
        if(null == companySupplierPlantList || Constants.ZERO == companySupplierPlantList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        asnInformationReturnDomain.setPlantSupplierList(companySupplierPlantList);
        
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoList = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(Constants.ZERO == companyDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        asnInformationReturnDomain.setCompanyDensoList(companyDensoList);
        
        if(null != asnInformationDomain.getSpsTAsnDomain().getDCd()){
            plantDensoWithScope.getPlantDensoDomain().setDCd(
                asnInformationDomain.getSpsTAsnDomain().getDCd());
        }
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getDCd())){
            plantDensoWithScope.getPlantDensoDomain().setDCd(null);
        }
        
        List<PlantDensoDomain> companyDensoPlantList = null;
        companyDensoPlantList = this.plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null == companyDensoPlantList
            || Constants.ZERO == companyDensoPlantList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        asnInformationReturnDomain.setPlantDensoList(companyDensoPlantList);
        
        asnStatusMiscDomain = new MiscellaneousDomain();
        asnStatusMiscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_ASN_STATUS);
        List<MiscellaneousDomain> asnStatusList = miscService.searchMisc(asnStatusMiscDomain);
        if(Constants.ZERO < asnStatusList.size()){
            Iterator asnStatusItr = asnStatusList.iterator();
            while(asnStatusItr.hasNext()){
                MiscellaneousDomain asnStatusItem = (MiscellaneousDomain)asnStatusItr.next();
                if(Constants.ASN_STATUS_CCL.equals(asnStatusItem.getMiscCode())){
                    asnStatusItr.remove();
                }
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_ASN_STATUS);
        }
        asnInformationReturnDomain.setAsnStatusList(asnStatusList);
        
        return asnInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchAsnInformation( 
     * com.globaldenso.asia.sps.business.domain.AsnInformationDomain)
     */
    public AsnInformationReturnDomain searchAsnInformation(
        AsnInformationDomain asnInformationDomain) throws ApplicationException
    {
        Locale locale = asnInformationDomain.getLocale();
        AsnInformationReturnDomain asnInformationReturnDomain = new AsnInformationReturnDomain();
        
        List<ApplicationMessageDomain> errorMessageList = this.validate(asnInformationDomain);
        if(null != errorMessageList && Constants.ZERO < errorMessageList.size()){
            asnInformationReturnDomain.setErrorMessageList(errorMessageList);
        }else{
            List<AsnProgressInformationReturnDomain> totalResult = null;
            List<ApplicationMessageDomain> warningMessageList 
                = new ArrayList<ApplicationMessageDomain>();
            if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getDCd())
                || Strings.judgeBlank(asnInformationDomain.getSpsTAsnDomain().getDCd()))
            {
                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0034, 
                        SupplierPortalConstant.LBL_DENSO_COMPANY)));
            }
            else{
                SpsMCompanyDensoCriteriaDomain spsMCompanyDensoCriteriaDomain = null;
                SpsMCompanyDensoDomain spsMCompanyDensoDomain = null;
                
                spsMCompanyDensoCriteriaDomain = new SpsMCompanyDensoCriteriaDomain();
                spsMCompanyDensoCriteriaDomain.setDCd(
                    asnInformationDomain.getSpsTAsnDomain().getDCd());
                spsMCompanyDensoDomain = this.spsMCompanyDensoService.searchByKey(
                    spsMCompanyDensoCriteriaDomain);
                if(SupplierPortalConstant.GROUP_INVOICE_DATE_TYPE.equals(
                    spsMCompanyDensoDomain.getGroupInvoiceType()) 
                    && !asnInformationDomain.getPlanEtaDateFrom().equals(
                        asnInformationDomain.getPlanEtaDateTo())){
                    warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_W2_0004,
                            new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_PLAN_ETA),
                                    asnInformationDomain.getSpsTAsnDomain().getDCd(),
                                    MessageUtil.getLabelHandledException(locale,
                                        SupplierPortalConstant.LBL_DATE)})));
                }else if(SupplierPortalConstant.GROUP_INVOICE_MONTH_TYPE.equals(
                    spsMCompanyDensoDomain.getGroupInvoiceType())
                    && (asnInformationDomain.getPlanEtaMonthFrom()
                        != asnInformationDomain.getPlanEtaMonthTo()
                    && asnInformationDomain.getPlanEtaWeekFrom()
                        != asnInformationDomain.getPlanEtaWeekTo()))
                {
                    warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_W2_0004,
                            new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_PLAN_ETA),
                                    asnInformationDomain.getSpsTAsnDomain().getDCd(),
                                    MessageUtil.getLabelHandledException(locale,
                                        SupplierPortalConstant.LBL_MONTH)})));
                }else if(SupplierPortalConstant.GROUP_INVOICE_WEEK_TYPE.equals(
                    spsMCompanyDensoDomain.getGroupInvoiceType())
                    && asnInformationDomain.getPlanEtaWeekFrom()
                        != asnInformationDomain.getPlanEtaWeekTo())
                {
                    warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_W2_0004,
                            new String[] {
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_PLAN_ETA),
                                    asnInformationDomain.getSpsTAsnDomain().getDCd(),
                                    MessageUtil.getLabelHandledException(locale,
                                        SupplierPortalConstant.LBL_WEEK)})));
                }
            }
            
            AsnProgressInformationDomain criteria = new AsnProgressInformationDomain();
            criteria.setVendorCd(asnInformationDomain.getSpsTAsnDomain().getVendorCd());
            criteria.setSPcd(asnInformationDomain.getSpsTAsnDomain().getSPcd());
            criteria.setDCd(asnInformationDomain.getSpsTAsnDomain().getDCd());
            criteria.setDPcd(asnInformationDomain.getSpsTAsnDomain().getDPcd());
            criteria.setPlanEtaFrom(asnInformationDomain.getPlanEtaDateFrom());
            criteria.setPlanEtaTo(asnInformationDomain.getPlanEtaDateTo());
            criteria.setPlanEtaTimeFrom(asnInformationDomain.getPlanEtaTimeFrom());
            criteria.setPlanEtaTimeTo(asnInformationDomain.getPlanEtaTimeTo());
            criteria.setAsnStatus(asnInformationDomain.getSpsTAsnDomain().getAsnStatus());
            criteria.setAsnNo(asnInformationDomain.getSpsTAsnDomain().getAsnNo());
            criteria.setActualEtdFrom(asnInformationDomain.getActualEtdDateFrom());
            criteria.setActualEtdTo(asnInformationDomain.getActualEtdDateTo());
            criteria.setSTaxId(asnInformationDomain.getSTaxId());
            criteria.setCreatedInvoiceFlag(Constants.STR_ZERO);
            criteria.setSupplierAuthenList(asnInformationDomain.getSupplierAuthenList());
            criteria.setDensoAuthenList(asnInformationDomain.getDensoAuthenList());
            
            int recordCount = asnService.searchCountAsnInformation(criteria);
            if(Constants.ZERO == recordCount){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
            miscLimitDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            miscLimitDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV002_RLM);
            Integer recordLimit = miscService.searchMiscValue(miscLimitDomain);
            if(null == recordLimit){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032);
            }else if(recordLimit < recordCount){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    new String[]{String.valueOf(recordLimit)});
            }
            
            MiscellaneousDomain miscLimitPerPageDomain = new MiscellaneousDomain();
            miscLimitPerPageDomain.setMiscType(
                SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            miscLimitPerPageDomain.setMiscCode(
                SupplierPortalConstant.MISC_CODE_WINV002_PLM);
            Integer recordLimitPerPage = miscService.searchMiscValue(miscLimitPerPageDomain);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032);
            }
            asnInformationDomain.setMaxRowPerPage(recordLimitPerPage);
            
            totalResult = asnService.searchAsnInformation(criteria);
            if(Constants.ZERO == totalResult.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            Set<String> asnKeyList = new TreeSet<String>();
            Set<String> densoCodeList = new TreeSet<String>();
            Map<String, List<AsnInformationDomain>> asnInformationResultMap 
                = new HashMap<String, List<AsnInformationDomain>>();
            Map<String, Set<String>> asnNoMap = new HashMap<String, Set<String>>();
            //This part group data by Denso Code.
            for(AsnProgressInformationReturnDomain asnProgressItem : totalResult){
                if(asnKeyList.add(StringUtil.appendsString(
                    asnProgressItem.getAsnNo(), asnProgressItem.getDCd()))){
                    
                    AsnInformationDomain asnItem = this.getAsnInformation(asnProgressItem);
                    if(asnInformationResultMap.containsKey(asnProgressItem.getDCd())){
                        
                        asnInformationResultMap.get(asnProgressItem.getDCd()).add(asnItem);
                        asnNoMap.get(asnProgressItem.getDCd()).add(asnProgressItem.getAsnNo());
                    }else{
                        
                        List<AsnInformationDomain> asnInformationList 
                            = new ArrayList<AsnInformationDomain>();
                        asnInformationList.add(asnItem);
                        asnInformationResultMap.put(asnProgressItem.getDCd(), asnInformationList);
                        
                        Set<String> asnNoList = new TreeSet<String>();
                        asnNoList.add(asnProgressItem.getAsnNo());
                        asnNoMap.put(asnProgressItem.getDCd(), asnNoList);
                    }
                    densoCodeList.add(asnProgressItem.getDCd());
                }
            }
            
            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
            SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
                = new SpsMCompanyDensoDomain();
            companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
            List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
                = this.companyDensoService.searchAs400ServerList(
                    companyDensoCriteriaForSearchAS400);
            if(Constants.ZERO == spsMAs400SchemaList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            if(spsMAs400SchemaList.size() != densoCodeList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0025,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            
            Map<String, String> ansStatusmap = new HashMap<String, String>();
            ansStatusmap.put(Constants.ASN_STATUS_N, Constants.ASN_STATUS_ISS);
            ansStatusmap.put(Constants.ASN_STATUS_D, Constants.ASN_STATUS_CCL);
            ansStatusmap.put(Constants.ASN_STATUS_P, Constants.ASN_STATUS_PTR);
            ansStatusmap.put(Constants.ASN_STATUS_R, Constants.ASN_STATUS_RCP);
            String asnStatusStr = StringUtil.convertListToVarcharCommaSeperate(
                asnInformationDomain.getSpsTAsnDomain().getAsnStatus());
            List<AsnInformationDomain> asnInformationResultList 
                = new ArrayList<AsnInformationDomain>();
            for(As400ServerConnectionInformationDomain as400Server : spsMAs400SchemaList){
                String asnNoStr = 
                    StringUtil.convertListToVarcharCommaSeperate(
                        asnNoMap.get(as400Server.getSpsMCompanyDensoDomain().getDCd()));
                List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
                    CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                        asnNoStr, asnStatusStr, as400Server, locale);
                
                List<AsnInformationDomain> asnInformationList 
                    = asnInformationResultMap.get(
                         as400Server.getSpsMCompanyDensoDomain().getDCd()); 
                Iterator<AsnInformationDomain> iterator = asnInformationList.iterator();
                while(iterator.hasNext()){
                    AsnInformationDomain asnItem = iterator.next();
                    if(Constants.ZERO < cigmaAsnDomainList.size()){
                        for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList){
                            if(asnItem.getSpsTAsnDomain().getAsnNo().trim().equals(
                                cigmaAsnitem.getPseudoAsnNo().trim()))
                            {
                                if(!Constants.ASN_STATUS_CCL.equals(
                                    asnItem.getSpsTAsnDomain().getAsnStatus()))
                                {
                                    asnItem.getSpsTAsnDomain().setAsnStatus(
                                        ansStatusmap.get(cigmaAsnitem.getPseudoAsnStatus()));
                                }
                                break;
                            }
                        }
                    }
                    if(Constants.ASN_STATUS_CCL.equals(asnItem.getSpsTAsnDomain().getAsnStatus())
                        || (null != asnInformationDomain.getSpsTAsnDomain().getAsnStatus()
                            && !asnInformationDomain.getSpsTAsnDomain().getAsnStatus().equals(
                                asnItem.getSpsTAsnDomain().getAsnStatus()))){
                        iterator.remove();
                    }
                }
                asnInformationResultList.addAll(asnInformationList);
            }
            if(Constants.ZERO == asnInformationResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            SpsPagingUtil.calcPaging(asnInformationDomain, asnInformationResultList.size());
            SortUtil.sort(asnInformationResultList, SortUtil.COMPARE_CREATE_INVOICE_BY_MANUAL);
            asnInformationReturnDomain.setAsnInformationDomainList(asnInformationResultList);
            asnInformationReturnDomain.setWarningMessageList(warningMessageList);
        }
        return asnInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchSelectedCompanyDenso( 
     * com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException{
        
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<PlantDensoDomain> companyDensoPlantList = null;
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            plantDensoWithScopeDomain.getDataScopeControlDomain());
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companyDensoPlantList = this.plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(null == companyDensoPlantList
            || Constants.ZERO == companyDensoPlantList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        return companyDensoPlantList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchSelectedPlantDenso( 
     * com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException{
        
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<CompanyDensoDomain> companyDensoList = null;
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            plantDensoWithScopeDomain.getDataScopeControlDomain());
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companyDensoList = this.companyDensoService.searchCompanyDenso(plantDensoWithScopeDomain);
        if(null == companyDensoList
            || Constants.ZERO == companyDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        return companyDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchSelectedCompanySupplier( 
     * com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)throws ApplicationException {
        
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<PlantSupplierDomain> companySupplierPlantList = null;

        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            plantSupplierWithScopeDomain.getDataScopeControlDomain());
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companySupplierPlantList = this.plantSupplierService
            .searchPlantSupplier(plantSupplierWithScopeDomain);
        if(null == companySupplierPlantList
            || Constants.ZERO == companySupplierPlantList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        
        return companySupplierPlantList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchSelectedCompanySupplier( 
     * com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)throws ApplicationException {
        
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<CompanySupplierDomain> companySupplierList = null;

        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            plantSupplierWithScopeDomain.getDataScopeControlDomain());
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companySupplierList = this.companySupplierService.searchCompanySupplier(
            plantSupplierWithScopeDomain);
        if(null == companySupplierList
            || Constants.ZERO == companySupplierList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        
        return companySupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchFileName( 
     * com.globaldenso.asia.sps.business.domain.AsnInformationDomain)
     */
    public FileManagementDomain searchFileName(AsnInformationDomain asnInformationDomain) 
        throws ApplicationException {
        
        Locale locale = asnInformationDomain.getLocale();
        FileManagementDomain resultDomain = null;
        
        try {
            
            resultDomain = fileManagementService.searchFileDownload(
                asnInformationDomain.getFileId(), false, null);
        } catch (IOException e) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);   
        }
        
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#searchLegendInfo
     * (com.globaldenso.asia.sps.business.domain.AsnInformationDomain,
     * OutputStream)
     */
    public void searchLegendInfo(AsnInformationDomain asnInformationDomain, OutputStream output) 
        throws ApplicationException {
        
        Locale locale = asnInformationDomain.getLocale();
        
        try {
            fileManagementService.searchFileDownload(asnInformationDomain.getFileId(), true, 
                output);
            
        } catch (IOException e) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ASNInformationFacadeService#validateGroupInvoice(
     * Map<java.lang.String, com.globaldenso.asia.sps.business.domain.ASNInformationDomain>, 
     * Locale)
     */
    public AsnInformationReturnDomain validateGroupInvoice(
        Map<String, AsnInformationDomain> asnSelectedMap, Locale locale)throws ApplicationException{
        
        AsnInformationReturnDomain asnInformationReturnDomain = new AsnInformationReturnDomain();
        
        if(Constants.ZERO == asnSelectedMap.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0012, 
                SupplierPortalConstant.LBL_GROUPING_INVOICE);
        }
        else {
            List<ApplicationMessageDomain> errorMessageList 
                = new ArrayList<ApplicationMessageDomain>();
            List<ApplicationMessageDomain> warningMessageList 
                = new ArrayList<ApplicationMessageDomain>();
            
            Iterator<String> keySetIterator = asnSelectedMap.keySet().iterator();
            Set<String> supplierTaxIdList = new TreeSet<String>();
            Set<String> densoCodeList = new TreeSet<String>();
            Set<String> densoPlantCodeList = new TreeSet<String>();
            Set<String> planEtaDateList = new TreeSet<String>();
            Set<String> planEtaMonthList = new TreeSet<String>();
            Set<String> planEtaWeekList = new TreeSet<String>();
            Calendar planEtaDateCalendar = Calendar.getInstance();
            
            String planEtaDate = null;
            String planEtaMonth = null;
            String planEtaWeek = null;
            boolean supplierTaxIdAreSame = true;
            boolean densoCodeAreSame = true;
            boolean densoPlantCodeAreSame = true;
            boolean planEtaDateAreSame = true;
            boolean planEtaMonthAreSame = true;
            boolean planEtaWeekAreSame = true;
            String key = null;
            
            while(keySetIterator.hasNext() && (supplierTaxIdAreSame || densoCodeAreSame 
                || densoPlantCodeAreSame || planEtaDateAreSame || planEtaMonthAreSame 
                || planEtaWeekAreSame)){
                
                key = keySetIterator.next();
                
                if(supplierTaxIdAreSame 
                    && supplierTaxIdList.add(asnSelectedMap.get(key).getSTaxId())
                    && Constants.ONE < supplierTaxIdList.size()){
                    supplierTaxIdAreSame = false;
                }
                if(densoCodeAreSame 
                    && densoCodeList.add(asnSelectedMap.get(key).getSpsTAsnDomain().getDCd())
                    && Constants.ONE < densoCodeList.size()){
                    densoCodeAreSame = false;
                }
                if(densoPlantCodeAreSame 
                    && densoPlantCodeList.add(asnSelectedMap.get(key).getSpsTAsnDomain().getDPcd())
                    && Constants.ONE < densoPlantCodeList.size()){
                    densoPlantCodeAreSame = false;
                }
                
                if(null != asnSelectedMap.get(key).getSpsTAsnDomain().getPlanEta()){
                    planEtaDateCalendar.setTimeInMillis(
                        asnSelectedMap.get(key).getSpsTAsnDomain().getPlanEta().getTime());
                    planEtaDate = DateUtil.format(planEtaDateCalendar.getTime(),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                    planEtaMonth = String.valueOf(planEtaDateCalendar.get(Calendar.MONTH));
                    planEtaWeek = String.valueOf(planEtaDateCalendar.get(Calendar.WEEK_OF_YEAR));
                }else{
                    planEtaDate = Constants.EMPTY_STRING;
                    planEtaMonth = Constants.EMPTY_STRING;
                    planEtaWeek = Constants.EMPTY_STRING;
                }
                if(planEtaDateAreSame && planEtaDateList.add(planEtaDate)
                    && Constants.ONE < planEtaDateList.size()){
                    planEtaDateAreSame = false;
                }
                if(planEtaMonthAreSame && planEtaMonthList.add(planEtaMonth)
                    && Constants.ONE < planEtaMonthList.size()){
                    planEtaMonthAreSame = false;
                }
                if(planEtaWeekAreSame && planEtaWeekList.add(planEtaWeek)
                    && Constants.ONE < planEtaWeekList.size()){
                    planEtaWeekAreSame = false;
                }                
            }
            
            if(supplierTaxIdAreSame){
                asnInformationReturnDomain.setVendorCd(
                    asnSelectedMap.get(key).getSpsTAsnDomain().getVendorCd());
                asnInformationReturnDomain.setSupplierPlantTaxId(
                    asnSelectedMap.get(key).getSTaxId());
            }else{
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                        SupplierPortalConstant.LBL_SUPPLIER_TAX_ID)));
            }
            if(!densoCodeAreSame){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                        SupplierPortalConstant.LBL_DENSO_CODE)));
            }else{
                asnInformationReturnDomain.setDCd(
                    asnSelectedMap.get(key).getSpsTAsnDomain().getDCd());
                
                SpsMCompanyDensoCriteriaDomain companyDensoCriteria
                    = new SpsMCompanyDensoCriteriaDomain();
                companyDensoCriteria.setDCd(asnSelectedMap.get(key).getSpsTAsnDomain().getDCd());
                SpsMCompanyDensoDomain companyDenso
                    = spsMCompanyDensoService.searchByKey(companyDensoCriteria);
                if(null == companyDenso
                    || StringUtil.checkNullOrEmpty(companyDenso.getGroupInvoiceDiffPlantDenso())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                        SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
                }else{
                    if(SupplierPortalConstant.GROUP_INVOICE_NOT_ALLOW_DIFF_DPCD.equals(
                        companyDenso.getGroupInvoiceDiffPlantDenso())){
                        if(!densoPlantCodeAreSame){
                            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                                MessageUtil.getApplicationMessageWithLabel(locale, 
                                    SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                                    SupplierPortalConstant.LBL_DENSO_PLANT_CODE)));
                        }else{
                            asnInformationReturnDomain.setDPcd(
                                asnSelectedMap.get(key).getSpsTAsnDomain().getDPcd());
                        }
                    }else{
                        if(densoPlantCodeAreSame){
                            asnInformationReturnDomain.setDPcd(
                                asnSelectedMap.get(key).getSpsTAsnDomain().getDPcd());
                        }else{
                            asnInformationReturnDomain.setDPcd(Constants.EMPTY_STRING);
                        }
                    }
                }
            }
            
            if(densoCodeAreSame){
                SpsMCompanyDensoCriteriaDomain spsMCompanyDensoCriteriaDomain
                    = new SpsMCompanyDensoCriteriaDomain();
                spsMCompanyDensoCriteriaDomain.setDCd(
                    asnSelectedMap.get(key).getSpsTAsnDomain().getDCd());
                SpsMCompanyDensoDomain spsMCompanyDensoDomain
                    = spsMCompanyDensoService.searchByKey(spsMCompanyDensoCriteriaDomain);
                if(SupplierPortalConstant.GROUP_INVOICE_ERROR_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceErrorType())){
                    if(SupplierPortalConstant.GROUP_INVOICE_DATE_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceType())
                        && !planEtaDateAreSame){
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                                SupplierPortalConstant.LBL_PLANETA_DATE)));
                    }else if(SupplierPortalConstant.GROUP_INVOICE_MONTH_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceType()) 
                        && !planEtaMonthAreSame){
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                                SupplierPortalConstant.LBL_PLANETA_MONTH)));
                    }else if(SupplierPortalConstant.GROUP_INVOICE_WEEK_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceType())
                        && !planEtaWeekAreSame){
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E7_0004, 
                                SupplierPortalConstant.LBL_PLANETA_WEEK)));
                    }
                }else if(SupplierPortalConstant.GROUP_INVOICE_WARNING_TYPE.equals(
                    spsMCompanyDensoDomain.getGroupInvoiceErrorType())){
                    if(SupplierPortalConstant.GROUP_INVOICE_DATE_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceType())
                        && !planEtaDateAreSame){
                        warningMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_WARNING, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_W2_0001, 
                                SupplierPortalConstant.LBL_PLANETA_DATE)));
                    }else if(SupplierPortalConstant.GROUP_INVOICE_MONTH_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceType())
                        && !planEtaMonthAreSame){
                        warningMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_WARNING, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_W2_0001, 
                                SupplierPortalConstant.LBL_PLANETA_MONTH)));
                    }else if(SupplierPortalConstant.GROUP_INVOICE_WEEK_TYPE.equals(
                        spsMCompanyDensoDomain.getGroupInvoiceType())
                        && !planEtaWeekAreSame){
                        warningMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_WARNING, 
                            MessageUtil.getApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_W2_0001, 
                                SupplierPortalConstant.LBL_PLANETA_WEEK)));
                    }
                }
            }
            asnInformationReturnDomain.setErrorMessageList(errorMessageList);
            asnInformationReturnDomain.setWarningMessageList(warningMessageList);
        }
        return asnInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * <p>Validate.</p>
     * <ul>
     * <li>Validation criteria data before search data.</li>
     * </ul>
     * 
     * @param asnInformationDomain the ASN Information Domain 
     * @return list of the Application Message Domain 
     */
    private List<ApplicationMessageDomain> validate(AsnInformationDomain asnInformationDomain){
        
        String planEtaDateFrom = null;
        String planEtaDateTo = null;
        String actualEtdDateFrom = null;
        String actualEtdDateTo = null;
        boolean hasPlanEtaDateFrom = false;
        boolean hasPlanEtaDateTo = false;
        boolean hasActualEtdDateFrom = false;
        boolean hasActualEtdDateTo = false;
        
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = asnInformationDomain.getLocale();
        
        if(Strings.judgeBlank(asnInformationDomain.getPlanEtaDateFrom())
            || Strings.judgeBlank(asnInformationDomain.getPlanEtaDateTo())
            || Strings.judgeBlank(asnInformationDomain.getSpsTAsnDomain().getVendorCd())
            || Strings.judgeBlank(asnInformationDomain.getSpsTAsnDomain().getSPcd())
            || Strings.judgeBlank(asnInformationDomain.getSpsTAsnDomain().getDCd())
            || Strings.judgeBlank(asnInformationDomain.getSpsTAsnDomain().getDPcd())){
            
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        }
        
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getVendorCd())){
            asnInformationDomain.getSpsTAsnDomain().setVendorCd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getSPcd())){
            asnInformationDomain.getSpsTAsnDomain().setSPcd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getDCd())){
            asnInformationDomain.getSpsTAsnDomain().setDCd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getDPcd())){
            asnInformationDomain.getSpsTAsnDomain().setDPcd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(asnInformationDomain.getSpsTAsnDomain().getAsnStatus())){
            asnInformationDomain.getSpsTAsnDomain().setAsnStatus(null);
        }
        
        if(!Strings.judgeBlank(asnInformationDomain.getPlanEtaDateFrom())){
            planEtaDateFrom = asnInformationDomain.getPlanEtaDateFrom();
            if(!DateUtil.isValidDate(planEtaDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_PLAN_ETA_DATE_FROM)));
            }else{
                hasPlanEtaDateFrom = true;
            }
        }
        if(!Strings.judgeBlank(asnInformationDomain.getPlanEtaDateTo())){
            planEtaDateTo = asnInformationDomain.getPlanEtaDateTo();
            if(!DateUtil.isValidDate(planEtaDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_PLAN_ETA_DATE_TO)));
            }else{
                hasPlanEtaDateTo = true;
            }
        }
        if(hasPlanEtaDateFrom && hasPlanEtaDateTo){
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            dateFrom.setTime(DateUtil.parseToSqlDate
                (planEtaDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH));
            dateTo.setTime(DateUtil.parseToSqlDate
                (planEtaDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH));
            asnInformationDomain.setPlanEtaMonthFrom(dateFrom.get(Calendar.MONTH));
            asnInformationDomain.setPlanEtaWeekFrom(dateFrom.get(Calendar.WEEK_OF_YEAR));
            
            asnInformationDomain.setPlanEtaMonthTo(dateTo.get(Calendar.MONTH));
            asnInformationDomain.setPlanEtaWeekTo(dateTo.get(Calendar.WEEK_OF_YEAR));
            
            if(dateTo.before(dateFrom)){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_PLAN_ETA_DATE_TO),
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_PLAN_ETA_DATE_FROM)})));
            }
        }
        
        /** Check format and compare from to Plan ETA Time. */
        if(!StringUtil.checkNullOrEmpty(asnInformationDomain.getPlanEtaTimeFrom())
            && !StringUtil.checkNullOrEmpty(asnInformationDomain.getPlanEtaTimeTo()))
        {
            boolean isValidDeliveryTimeFrom = true;
            boolean isValidDeliveryTimeTo = true;
            if(!asnInformationDomain.getPlanEtaTimeFrom().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_PLAN_ETA_TIME_FROM)));
                isValidDeliveryTimeFrom = false;
            }
            if(!asnInformationDomain.getPlanEtaTimeTo().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_PLAN_ETA_TIME_TO)));
                isValidDeliveryTimeTo = false;
            }
            if(isValidDeliveryTimeFrom && isValidDeliveryTimeTo){
                Date d1 = DateUtil.parseToUtilDate(
                    asnInformationDomain.getPlanEtaTimeFrom()
                    , DateUtil.PATTERN_HHMM_COLON);
                Date d2 = DateUtil.parseToUtilDate(
                    asnInformationDomain.getPlanEtaTimeTo()
                    , DateUtil.PATTERN_HHMM_COLON);
                if(d1.after(d2)){
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_PLAN_ETA_TIME_FROM),
                                    MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_PLAN_ETA_TIME_TO)})));
                }
            }
        }else{
            if(!StringUtil.checkNullOrEmpty(asnInformationDomain.getPlanEtaTimeFrom())
                || !StringUtil.checkNullOrEmpty(asnInformationDomain.getPlanEtaTimeTo()))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                        SupplierPortalConstant.LBL_BOTH_PLAN_ETA_TIME_FROM_AND_TO)));
            }
        }
        
        if(!Strings.judgeBlank(asnInformationDomain.getActualEtdDateFrom())){
            actualEtdDateFrom = asnInformationDomain.getActualEtdDateFrom();
            if(!DateUtil.isValidDate(actualEtdDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_FROM)));
            }else{
                hasActualEtdDateFrom = true;
            }
        }
        if(!Strings.judgeBlank(asnInformationDomain.getActualEtdDateTo())){
            actualEtdDateTo = asnInformationDomain.getActualEtdDateTo();
            if(!DateUtil.isValidDate(actualEtdDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_TO)));
            }else{
                hasActualEtdDateTo = true;
            }
        }
        if(hasActualEtdDateFrom && hasActualEtdDateTo){
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            dateFrom.setTime(DateUtil.parseToSqlDate
                (actualEtdDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH));
            dateTo.setTime(DateUtil.parseToSqlDate
                (actualEtdDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH));
            
            if(dateTo.before(dateFrom)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_TO),
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_FROM)})));
            }
        }
        
        if(!Strings.judgeBlank(asnInformationDomain.getSpsTAsnDomain().getAsnNo())){
            if(Constants.MAX_ASN_NO_LENGTH 
                < asnInformationDomain.getSpsTAsnDomain().getAsnNo().length()){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_ASN_NO),
                            String.valueOf(Constants.MAX_ASN_NO_LENGTH)})));
            }
        }
        if(!Strings.judgeBlank(asnInformationDomain.getSTaxId())){
            if(Constants.MAX_SUPPLIER_PLANT_TAX_ID_LENGTH 
                < asnInformationDomain.getSTaxId().length()){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_SUPPLIER_TAX_ID),
                            String.valueOf(Constants.MAX_SUPPLIER_PLANT_TAX_ID_LENGTH)})));
            }
        }
        
        return errorMessageList;
    }
    
    /**
     * <p>Get Asn Information.</p>
     * <ul>
     * <li>Getter data from setting date time format for show on screen.</li>
     * </ul>
     * 
     * @param asnProgressItem the ASN Progress Information Return Domain
     * @return list of the SPS Upload Invoice Domain 
     */
    private AsnInformationDomain getAsnInformation(
        AsnProgressInformationReturnDomain asnProgressItem){
        
        StringBuffer utcActualETD = null;
        StringBuffer utcPlanETA = null;
        StringBuffer utc = null;
        
        AsnInformationDomain asnItem = new AsnInformationDomain();
        asnItem.getSpsTAsnDomain().setAsnNo(asnProgressItem.getAsnNo());
        asnItem.getSpsTAsnDomain().setAsnStatus(asnProgressItem.getAsnStatus());
        asnItem.getSpsTAsnDomain().setTripNo(asnProgressItem.getTripNo());
        asnItem.getSpsTAsnDomain().setVendorCd(asnProgressItem.getVendorCd());
        asnItem.getSpsTAsnDomain().setSPcd(asnProgressItem.getSPcd());
        asnItem.setSTaxId(asnProgressItem.getsTaxId());
        asnItem.getSpsTAsnDomain().setDCd(asnProgressItem.getDCd());
        asnItem.getSpsTAsnDomain().setDPcd(asnProgressItem.getDPcd());
        asnItem.getSpsTAsnDomain().setPlanEta(asnProgressItem.getPlanEta());
        
        if(null != asnProgressItem.getUtc()){
            utc = new StringBuffer();
            utc.append(Constants.SYMBOL_OPEN_BRACKET);
            utc.append(Constants.SYMBOL_SPACE);
            utc.append(Constants.WORD_UTC);
            utc.append(asnProgressItem.getUtc());
            utc.append(Constants.SYMBOL_CLOSE_BRACKET);
            
            utcActualETD = new StringBuffer();
            utcActualETD.append(
                DateUtil.format(asnProgressItem.getActualEtd(),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
            utcActualETD.append(utc);
            asnItem.setActualEtd(utcActualETD.toString());
            
            utcPlanETA = new StringBuffer();
            utcPlanETA.append(
                DateUtil.format(asnProgressItem.getPlanEta(),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH));
            utcPlanETA.append(utc);
            asnItem.setPlanEta(utcPlanETA.toString());
        }
        return asnItem;
    }
}
