/*
 * ModifyDate Development company       Describe 
 * 2014/07/01 CSI Phakaporn             Create
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change P/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationResultDomain;

/**
 * <p>The Class Purchase Order Information Facade Service Implement.</p>
 * <p>Manage data of Purchase Order Information.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchPurchaseOrderInformation</li>
 * <li>Method search  : searchPurchaseOrderInformationCsv</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchPurchaseOrderReport</li>
 * <li>Method search  : searchChangeMaterialRelease</li>
 * <li>Method search  : searchPlantBySelectSupplierCompany</li>
 * <li>Method search  : searchPlantBySelectDensoCompany</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method update  : updateAcknowledgementPo</li>
 * </ul>
 *
 * @author CSI
 */
public interface PurchaseOrderInformationFacadeService {
    
    /**
     * <p>Search Purchase Order default Information.</p>
     * <ul>
     * <li>Search Purchase Order Information for display on Purchase Order Information screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the Data scope control domain
     * @return the Purchase Order Information.
     * @throws ApplicationException ApplicationException
     */
    public PurchaseOrderInformationDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>Search Purchase Order Information.</p>
     * <ul>
     * <li>Search Purchase Order Information for display on Purchase Order Information screen.</li>
     * </ul>
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @return the Purchase Order Information.
     * @throws ApplicationException ApplicationException
     */
    public PurchaseOrderInformationResultDomain searchPurchaseOrderInformation(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>Create Export</p>
     * <ul>
     * <li>Create CSV file for export.</li>
     * </ul>
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @param commonDomain the common domain
     * @return purchase order information
     * @throws ApplicationException ApplicationException
     */
    public PurchaseOrderInformationResultDomain searchPurchaseOrderInformationCsv(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain, CommonDomain commonDomain)
        throws ApplicationException;

    /**
     * Call FileManagementService.serachFileDownload to get file name.
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @return File Management Domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException;

    /**
     * Call FileManagementService.serachFileDownload to send Purchase Order Report data to client.
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchPurchaseOrderReport(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * Call FileManagementService.serachFileDownload to send Change Material Release data to client.
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchChangeMaterialRelease(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain,
        OutputStream output) throws ApplicationException;
    
    /**
     * <p>Initial data when change select supplier company code.</p>
     * <ul>
     * <li>Search list of supplier plant to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return SpsMPlantSupplierDomain the List of supplier plant to show in combo box
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain>  searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    
    /**
     * <p>Initial data when change select supplier company code.</p>
     * <ul>
     * <li>Search list of supplier plant to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Supplier Plant
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select DENSO company code.</p>
     * <ul>
     * <li>Search list of DENSO plant to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return the List of DENSO plant to show in combo box
     * @throws ApplicationException Application Exception
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;

    /**
     * <p>Initial data when change select DENSO company code.</p>
     * <ul>
     * <li>Search list of DENSO plant to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected DENSO plant
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * Call FileManagementService.serachFileDownload to send Legend File data to client.
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(PurchaseOrderInformationDomain purchaseOrderInformationDomain,
        OutputStream output) throws ApplicationException;
    
    /**
     * <p>Update Purchase Order Information for Acknowledge function.</p>
     * <ul>
     * <li>Update Purchase Order Information by criteria from screen.</li>
     * </ul>
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @return Update recrod count
     * @throws ApplicationException ApplicationException
     */
    public Integer updateAcknowledgementPo(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;

    /**
     * Generate Original Po.
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @return File Management Domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain generateOriginalPo(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException;

    /**
     * Generate Change Po.
     * 
     * @param purchaseOrderInformationDomain the Purchase Order Information Domain.
     * @return File Management Domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain generateChangePo(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException;
}