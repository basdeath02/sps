/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2015/07/16 CSI Akat                  [IN007]
 * 2015/08/25 CSI Akat                  [IN012]
 * 2015/10/06 CSI Akat                  [IN020]
 * 2015/11/16 CSI Akat                  [IN025]
 * 2016/01/25 CSI Akat                  [IN055]
 * 2016/02/10 CSI Akat                  [IN049]
 * 2016/03/15 CSI Akat                  [IN069]
 * 2016/03/16 CSI Akat                  [IN068]
 * 2016/04/08 CSI Akat                  [IN068]
 * 2016/04/18 CSI Akat                  [IN070]
 * 2016/04/27 CSI Akat                  [IN071]
 * 2016/06/01 NB Napol                  [IN075]
 * 2018/04/11 Netband U.Rungsiwut       Generate D/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.mail.search.SearchException;

import org.apache.commons.logging.Log;
import org.springframework.dao.DataIntegrityViolationException;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTCnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DoCreatedAsnDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaChangeDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaOneWayKanbanInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDetailDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDetailDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoKanbanlDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferOneWayKanbanDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;





//static import
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0001;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0002;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0001;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0007;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0021;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0022;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0023;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0024;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0025;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0026;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0027;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0046;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0051;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0062;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0063;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0079;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0080;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0081;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0082;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0083;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0084;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0016;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_VENDOR_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_PARTS_NO_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_DO_DUP;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_PO_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_DO_ORIGINAL_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_DO_ALREADY_CREATE_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_PREVIOUS_REV_ERROR;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TABLE_CIGMA_DO_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TABLE_CIGMA_CHG_DO_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CREATE_KANBAN_SEQUENCE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_SEARCH_DELIVERY_ORDER_REPORT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_SEARCH_KANBAN_DELIVERY_ORDER_REPORT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_SEARCH_EXISTS_DELIVERY_ORDER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CHECK_DO_IN_CREATE_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CHECK_PREVIOUS_REV_ERROR;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CREATE_KANBAN_TO_CREATE_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_COPY_KANBAN_TO_CREATE_NEW_REVISION;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_DO_STEP_SEARCH_KANBAN_TO_CREATE_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CREATE_DO_DETAIL_TO_CREATE_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_COPY_DO_DETAIL_TO_CREATE_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_CREATE_DO_IN_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_SEARCH_DO_TO_CREATE_NEW_REVISION;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_UPDATE_CURRENT_REVISION_DO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_DO_STEP_UPDATE_CHG_DO_STATUS;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NEWLINE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0015;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_DO_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_DO_1_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER3;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE4;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_DO_2_ERROR_SENDER_NAME;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_DO_2_ERROR_HEADER1;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_DO_2_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER;

//21082021 import constant for mail backorder
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_ERROR_SUBJECT_BACKORDER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_ERROR_REMARK_BACKORDER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_ERROR_DETAIL_BACKORDER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_DO_ERROR_SUBJECT_BACKORDER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_DO_ERROR_HEADER_BACKORDER;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_DO_ERROR_HEADER_BACKORDER_TABLE2;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_DO_ERROR_DETAIL_BACKORDER;

import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_SUBJECT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER4;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_DO_NOTI_BACKORDER_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_DO_NOTI_BACKORDER_SUBJECT;
//end mail backorder

import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_DO_URGENT_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER4;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER_TABLE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER_TABLE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_HEADER_TABLE4;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_URGENT_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_SCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_SPCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DPCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_NO_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_ASN_NO_LIST_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_SPS_DO_NO_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DELIVERY_DATE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DO_ISSUE_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_ISSUE_DATE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_REV_NO_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DELIVERY_TIME_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_TRUCK_ROUTE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_TRUCK_SEQ_REPLACEMENT;
//[IN075] Add Order Type
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_ORDER_TYPE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_SHIPMENT_STATUS_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_DO_DUPLICATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_PO_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_DO_ORIGINAL_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_DO_ALREADY_CREATE_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_PREVIOUS_REV_ERRO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTMATCH_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LIMIT_BATCH_RECORD_TYPE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LIMIT_MAX_REC_ORD_E_CODE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LIMIT_MAX_REC_ORD_C_CODE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_VENDOR_CD_REPLACEMENT;

// Start : [IN025] change email content
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER2_PONOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER2_DODUP;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_HEADER2_PREREVERROR;
// End : [IN025] change email content

// Start : [IN055] Change header column case Part not found
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_DO_ERROR_HEADER_TABLE3_PRT_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3_PRT_NOT_FOUND;
// End : [IN055] Change header column case Part not found

//[IN070] add list of email to send in content
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_PERSON_IN_CHARGE;
import static com.globaldenso.asia.sps.common.constant.Constants.DEFAULT_REVISION;
import static com.globaldenso.asia.sps.common.constant.Constants.DDO_TYPE;
import static com.globaldenso.asia.sps.common.constant.Constants.KDO_TYPE;
import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_THREE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_FOUR;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_FIVE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_Y;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_CR_LF;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NBSP;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.THREE;
import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;
import static com.globaldenso.asia.sps.common.constant.Constants.FIVE;
import static com.globaldenso.asia.sps.common.constant.Constants.SIX;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_HHMMSS;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_HHMM;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_HHMM_COLON;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToSqlDate;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToTimestamp;
import static com.globaldenso.asia.sps.common.utils.DateUtil.format;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.REVISION_FORMAT;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.increaseNumber;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.toBigDecimal;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.padZero;
import static com.globaldenso.asia.sps.common.utils.StringUtil.appendsString;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getLabelHandledException;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getEmailLabel;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.writeLog;
import static com.globaldenso.asia.sps.common.utils.StringUtil.nullToEmpty;

/* Import for SPS Phase II*/
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_SHIPMENT_DATE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_DO_NOTIFICATION_SUBJECT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_KANBAN_NOTIFICATION_SUBJECT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_DO_NOTIFICATION_CONTENT_HEADER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_DO_NOTIFICATION_CONTENT_DETAIL_HEADER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_DO_NOTIFICATION_CONTENT_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
.MAIL_DO_NOTIFICATION_CONTENT_FOOTER;

/**
 * <p>The class TransferDoDataFromCigmaFacadeServiceImpl.</p>
 * <p>Facade for TransferDoDataFromCigmaFacadeServiceImpl.</p>
 * <ul>
 * <li>Method search  : searchInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
//public class TransferDoDataFromCigmaFacadeServiceImpl{}

public class TransferDoDataFromCigmaFacadeServiceImpl 
    implements TransferDoDataFromCigmaFacadeService {
    
    /** Reuse Argument 1. */
    private String[] argument1 = new String[ONE];
    /** Reuse Argument 2. */
    private String[] argument2 = new String[TWO];
    /** Reuse Argument 3. */
    private String[] argument3 = new String[THREE];
    /** Reuse Argument 4. */
    private String[] argument4 = new String[FOUR];
    /** Reuse Argument 5. */
    private String[] argument5 = new String[FIVE];
    /** Reuse Argument 6. */
    private String[] argument6 = new String[SIX];

    /** Common Service */
    private CommonService commonService;
    /** Denso Company Services. */
    private CompanyDensoService companyDensoService;
    /** PurchaseOrderService */
    private PurchaseOrderService purchaseOrderService;
    /** DeliveryOrderService */
    private DeliveryOrderService deliveryOrderService;
    /** UserService */
    private UserService userService;
    /** SupplierUserService */
    private UserSupplierService userSupplierService;
    /** RecordLimitService */
    private RecordLimitService recordLimitService;
    /** FileManagementService */
    private FileManagementService fileManagementService;
    /** Mail Service */
    private MailService mailService;
    /** CIGMA D/O Error Service */
    private CigmaDoErrorService cigmaDoErrorService;
    /** CIGMA Change D/O Error Service */
    private CigmaChgDoErrorService cigmaChgDoErrorService;
    // [IN068] for get minimum issue date
    /** CIGMA P/O Error Service */
    private CigmaPoErrorService cigmaPoErrorService;
    
    private AsnService asnService;
    

    /** Service Auto */
    /** Transaction D/O Service */
    private SpsTDoService spsTDoService;
    /** Transaction D/O Detail Service */
    private SpsTDoDetailService spsTDoDetailService;
    /** Transaction D/O Kanban Seq Service */
    private SpsTDoKanbanSeqService spsTDoKanbanSeqService;
    /** Transaction Cigma D/O Error Service */
    private SpsCigmaDoErrorService spsCigmaDoErrorService;
    /** Transaction Cigma Change D/O Error Service */
    private SpsCigmaChgDoErrorService spsCigmaChgDoErrorService;
    /** Master Denso Supplier Part Service */
    private SpsMDensoSupplierPartsService spsMDensoSupplierPartsService;
    /**  SpsMAs400Vendor Service */
    private SpsMAs400VendorService spsMAs400VendorService; 
    // [INXXX] for update ASN Detail when change D/O
    /**  spsTAsnDetailService Service */
    private SpsTAsnDetailService spsTAsnDetailService;
    
    // [IN019] Start : Also update Invoice Detail and CN Detail when issue Change D/O
    /** Service for SPS_T_INOICE_DETAIL. */
    private SpsTInvoiceDetailService spsTInvoiceDetailService;
    
    /** Service for SPS_T_CN_DETAIL. */
    private SpsTCnDetailService spsTCnDetailService;
    // [IN019] End : Also update Invoice Detail and CN Detail when issue Change D/O
    
    // Start : [IN071] also update ASN Header, Invoice Header and CN Header
    /** Service for SPS_T_ASN */
    private SpsTAsnService spsTAsnService;
    
    /** Service for SPS_T_INVOICE */
    private SpsTInvoiceService spsTInvoiceService;
    
    /** Service for SPS_T_CN */
    private SpsTCnService spsTCnService;
    // End : [IN071] also update ASN Header, Invoice Header and CN Header
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public TransferDoDataFromCigmaFacadeServiceImpl(){
        super();
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>Setter method for companyDensoService.</p>
     *
     * @param companyDensoService Set for companyDensoService
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * <p>Setter method for spsMAs400VendorService.</p>
     *
     * @param spsMAs400VendorService Set for spsMAs400VendorService
     */
    public void setSpsMAs400VendorService(
        SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }

    /**
     * <p>Setter method for purchaseOrderService.</p>
     *
     * @param purchaseOrderService Set for purchaseOrderService
     */
    public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    /**
     * <p>Setter method for deliveryOrderService.</p>
     *
     * @param deliveryOrderService Set for deliveryOrderService
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }

    /**
     * <p>Setter method for userService.</p>
     *
     * @param userService Set for userService
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * <p>Setter method for userSupplierService.</p>
     *
     * @param userSupplierService Set for userSupplierService
     */
    public void setUserSupplierService(UserSupplierService userSupplierService) {
        this.userSupplierService = userSupplierService;
    }

    /**
     * <p>Setter method for recordLimitService.</p>
     *
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * <p>Setter method for fileManagementService.</p>
     *
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    /**
     * <p>Setter method for mailService.</p>
     *
     * @param mailService Set for mailService
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
    /**
     * <p>Setter method for spsTDoService.</p>
     *
     * @param spsTDoService Set for spsTDoService
     */
    public void setSpsTDoService(SpsTDoService spsTDoService) {
        this.spsTDoService = spsTDoService;
    }

    /**
     * <p>Setter method for spsTDoDetailService.</p>
     *
     * @param spsTDoDetailService Set for spsTDoDetailService
     */
    public void setSpsTDoDetailService(SpsTDoDetailService spsTDoDetailService) {
        this.spsTDoDetailService = spsTDoDetailService;
    }

    /**
     * <p>Setter method for spsTDoKanbanSeqService.</p>
     *
     * @param spsTDoKanbanSeqService Set for spsTDoKanbanSeqService
     */
    public void setSpsTDoKanbanSeqService(
        SpsTDoKanbanSeqService spsTDoKanbanSeqService) {
        this.spsTDoKanbanSeqService = spsTDoKanbanSeqService;
    }

    /**
     * <p>Setter method for spsCigmaDoErrorService.</p>
     *
     * @param spsCigmaDoErrorService Set for spsCigmaDoErrorService
     */
    public void setSpsCigmaDoErrorService(
        SpsCigmaDoErrorService spsCigmaDoErrorService) {
        this.spsCigmaDoErrorService = spsCigmaDoErrorService;
    }

    /**
     * <p>Setter method for spsCigmaChgDoErrorService.</p>
     *
     * @param spsCigmaChgDoErrorService Set for spsCigmaChgDoErrorService
     */
    public void setSpsCigmaChgDoErrorService(
        SpsCigmaChgDoErrorService spsCigmaChgDoErrorService) {
        this.spsCigmaChgDoErrorService = spsCigmaChgDoErrorService;
    }

    /**
     * <p>Setter method for spsMDensoSupplierPartsService.</p>
     *
     * @param spsMDensoSupplierPartsService Set for spsMDensoSupplierPartsService
     */
    public void setSpsMDensoSupplierPartsService(
        SpsMDensoSupplierPartsService spsMDensoSupplierPartsService) {
        this.spsMDensoSupplierPartsService = spsMDensoSupplierPartsService;
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchAs400ServerList(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public List<As400ServerConnectionInformationDomain> 
    searchAs400ServerList(SpsMCompanyDensoDomain criteria) 
        throws Exception {
        return companyDensoService.searchAs400ServerList(criteria);
    }

    /**
     * <p>Setter method for cigmaDoErrorService.</p>
     *
     * @param cigmaDoErrorService Set for cigmaDoErrorService
     */
    public void setCigmaDoErrorService(CigmaDoErrorService cigmaDoErrorService) {
        this.cigmaDoErrorService = cigmaDoErrorService;
    }

    /**
     * <p>Setter method for cigmaChgDoErrorService.</p>
     *
     * @param cigmaChgDoErrorService Set for cigmaChgDoErrorService
     */
    public void setCigmaChgDoErrorService(
        CigmaChgDoErrorService cigmaChgDoErrorService) {
        this.cigmaChgDoErrorService = cigmaChgDoErrorService;
    }

    /**
     * <p>Setter method for spsTAsnDetailService.</p>
     *
     * @param spsTAsnDetailService Set for spsTAsnDetailService
     */
    public void setSpsTAsnDetailService(SpsTAsnDetailService spsTAsnDetailService) {
        this.spsTAsnDetailService = spsTAsnDetailService;
    }

    /**
     * <p>Setter method for spsTInvoiceDetailService.</p>
     *
     * @param spsTInvoiceDetailService Set for spsTInvoiceDetailService
     */
    public void setSpsTInvoiceDetailService(
        SpsTInvoiceDetailService spsTInvoiceDetailService) {
        this.spsTInvoiceDetailService = spsTInvoiceDetailService;
    }

    /**
     * <p>Setter method for spsTCnDetailService.</p>
     *
     * @param spsTCnDetailService Set for spsTCnDetailService
     */
    public void setSpsTCnDetailService(SpsTCnDetailService spsTCnDetailService) {
        this.spsTCnDetailService = spsTCnDetailService;
    }

    // [IN068] for search minimum issue date
    /**
     * <p>Setter method for cigmaPoErrorService.</p>
     *
     * @param cigmaPoErrorService Set for cigmaPoErrorService
     */
    public void setCigmaPoErrorService(CigmaPoErrorService cigmaPoErrorService) {
        this.cigmaPoErrorService = cigmaPoErrorService;
    }

    // Start : [IN071] also update ASN Header, Invoice Header and CN Header
    /**
     * <p>Setter method for spsTAsnService.</p>
     * @param spsTAsnService Set for spsTAsnService
     */
    public void setSpsTAsnService(SpsTAsnService spsTAsnService) {
        this.spsTAsnService = spsTAsnService;
    }

    /**
     * <p>Setter method for spsTInvoiceService.</p>
     * @param spsTInvoiceService Set for spsTInvoiceService
     */
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService) {
        this.spsTInvoiceService = spsTInvoiceService;
    }

    /**
     * <p>Setter method for spsTCnService.</p>
     * @param spsTCnService Set for spsTCnService
     */
    public void setSpsTCnService(SpsTCnService spsTCnService) {
        this.spsTCnService = spsTCnService;
    }
    // End : [IN071] also update ASN Header, Invoice Header and CN Header
    
    /**
     * <p>Setter method for asnService.</p>
     *
     * @param asnService Set for asnService
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchDeliveryOrder(java.util.Locale, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public Map<String, List<TransferDoDataFromCigmaDomain>> searchDeliveryOrder(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag) throws Exception
    {
        // 1    Delete all error records, IF search D/O transfer error.
        // If search D/O transfer error (first step), delete all error record.
        if (Constants.STR_TWO.equals(spsFlag)) {
            this.deleteDoError(as400Server.getSpsMCompanyDensoDomain().getDCd(), locale);
        }
        
        // 2    Get CIGMA Delivery Order data which finished writing to data file  (check with Transmit data control file)
        List<PseudoCigmaDoInformationDomain> pseudoList = 
            CommonWebServiceUtil.searchCigmaDeliveryOrder(as400Server, spsFlag);
//        if( null == pseudoList || ZERO == pseudoList.size() ){
//            throwsErrorMessage(locale, ERROR_CD_SP_E6_0001, null);
//        }
        Map<String, List<TransferDoDataFromCigmaDomain>> resultMap
            = new HashMap<String, List<TransferDoDataFromCigmaDomain>>();
        if( ZERO < pseudoList.size() ){
            for(TransferDoDataFromCigmaDomain domain : convertToCigmaDo(pseudoList)){
                if( null == resultMap.get(domain.getCigmaDoNo()) ){
                    resultMap.put(domain.getCigmaDoNo(), 
                        new ArrayList<TransferDoDataFromCigmaDomain>());
                }
                resultMap.get(domain.getCigmaDoNo()).add(domain);
            }
        }
        return resultMap;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#checkTransferDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, boolean)
     */
    public List<TransferDoDataFromCigmaDomain> transactCheckTransferDoFromCigma(
        Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        boolean bError, 
        String jobId,
        Log log
    ) throws Exception {
        List<TransferDoDataFromCigmaDomain> resultList 
            = new ArrayList<TransferDoDataFromCigmaDomain>(transferList.size());
        
        // 1. Find Supplier Plant Code for each DENSO Part No and find SPS P/O No relate with D/O
        // initial criteria 1-1
        StringBuffer sbErrorMsg = new StringBuffer();
        Map<String, TransferDoDataFromCigmaDomain> headerMap 
            = new LinkedHashMap<String, TransferDoDataFromCigmaDomain>();
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = 
            new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
        for(TransferDoDataFromCigmaDomain transfer : transferList) {
            headerMap = new LinkedHashMap<String, TransferDoDataFromCigmaDomain>();

            List<TransferDoDataFromCigmaDomain> bufferList 
                = new ArrayList<TransferDoDataFromCigmaDomain>(transferList.size());
            // initial criteria 1-3
            // LOOP until end of cigmaDOInformation. <List>cigmaDODetail. dPartNo
            // 1-1) Get SPS SupplierCode from SpsMAs400VendorService.
            SpsMAs400VendorDomain vendorDo 
                = findSupplier(as400VendorCriteriaDomain, transfer.getVendorCd());
            // check is not found Supplier Code.
            if( null == vendorDo ){
                // (1) Keep CIGMA Error data into SPS_CIGMA_DO_ERROR 
                // (Keep all data from the same CIGMA D/O No)
                insertCigmaDoError( locale, 
                    as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, null,
                    STR_ZERO,
                    ERROR_TYPE_VENDOR_NOT_FOUND);
                
                sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCigmaDoNo(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                break;
            } // end check not found.
            else {
                transfer.setSCd( vendorDo.getSCd() );
            }
            
            TransferDoDataFromCigmaDomain header = null;
            String sPcd = null;
            
            SpsMDensoSupplierPartsCriteriaDomain partCriteria = 
                new SpsMDensoSupplierPartsCriteriaDomain();
            partCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            partCriteria.setDPcd(transfer.getPlantCode());
            partCriteria.setSCd(transfer.getSCd());
            boolean detailError = false;
            
            // 1-2) Get Supplier Information data from DENSOSupplierPartService
            for(TransferDoDetailDataFromCigmaDomain doDetail : transfer.getCigmaDoDetail()) {
                
                // [IN023] compare effective date by Delivery Date
                //partCriteria
                //    .setEffectDateLessThanEqual(new Timestamp(transfer.getIssueDate().getTime()));
                Timestamp deliveryDatetime = parseToTimestamp( 
                    appendsString(
                        transfer.getDeliveryDate(), 
                        transfer.getDeliveryTime() )
                    , PATTERN_YYYYMMDD_HHMM );
                partCriteria.setEffectDateLessThanEqual(deliveryDatetime);
                
                SpsMDensoSupplierPartsDomain partDo = findPart(partCriteria, doDetail.getPartNo()
                    , SupplierPortalConstant.SORT_PART_MASTER);
                
                // check is not found Supplier Part No
                if( null == partDo ) {
                    String cause = ERROR_TYPE_PARTS_NO_NOT_FOUND;
                    partCriteria.setEffectDateLessThanEqual(null);
                    SpsMDensoSupplierPartsDomain effect = this.findPart(partCriteria,
                        doDetail.getPartNo(), SupplierPortalConstant.SORT_PART_MASTER_ASC);
                        
                    if (null != effect) {
                        cause = ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
                    }
                    
                    insertCigmaDoError( locale, as400Server.getSpsMCompanyDensoDomain(), 
                        transfer, doDetail, STR_ONE, cause);
                        
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                        transfer.getCigmaDoNo(),
                        doDetail.getPartNo() ) )
                        .append( SYMBOL_CR_LF );
                    detailError = true;
                    continue;
                }
                
                if (detailError) {
                    continue;
                }
                
                sPcd = partDo.getSPcd();
                doDetail.setSPcd(sPcd);
                transfer.setSPcd(sPcd);
                
                if( null == headerMap.get(sPcd) ){
                    header = new TransferDoDataFromCigmaDomain();
                    header = cloneNew(transfer, header);
                    headerMap.put(sPcd, header);
                }
                
                header = headerMap.get( sPcd );
                header.setSPcd( sPcd );
                doDetail.setSPn( partDo.getSPn() );
                header.getCigmaDoDetail().add( doDetail );
            } // end for find Supplier Part No.
            
            if( EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
                
                // Check Duplicate transfer DO
                SpsTDoCriteriaDomain doCriteria = new SpsTDoCriteriaDomain();
                
                // [IN022] add more criteria for check duplicate D/O
                doCriteria.setVendorCd(transfer.getVendorCd());
                doCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
                
                doCriteria.setCigmaDoNo(transfer.getCigmaDoNo());
                doCriteria.setDPcd(transfer.getPlantCode());
                doCriteria.setDataType(transfer.getDataType());
                List<SpsTDoDomain> doList = 
                    spsTDoService.searchByCondition(doCriteria);
                if( null != doList && ZERO < doList.size() ){
                    for(TransferDoDetailDataFromCigmaDomain detail : transfer.getCigmaDoDetail()) {
                        insertCigmaDoError( locale, as400Server.getSpsMCompanyDensoDomain(), 
                            transfer, detail, STR_ONE, ERROR_TYPE_DO_DUP);
                    }
                    
                    sbErrorMsg.append( getMessage(locale, ERROR_CD_SP_E6_0051, 
                        transfer.getCigmaDoNo(),
                        transfer.getPlantCode(),
                        transfer.getDataType()))
                        .append( SYMBOL_CR_LF );
                    
                    continue;
                } // End Check duplicate transfer DO
                
                for (String strSpcd : headerMap.keySet()) {
                    bufferList.add( headerMap.get(strSpcd) );
                }
            
                if( !STR_Y.equals(transfer.getAddTruckRouteFlag()) ) {
                    // 1-3) Get P/O Id from PurchaseOrderService
                    PurchaseOrderInformationDomain poInfoCriteria 
                        = new PurchaseOrderInformationDomain();
                    poInfoCriteria.setSpsTPoDomain(new SpsTPoDomain());
                    poInfoCriteria.setSpsTPoDetailDomain(new SpsTPoDetailDomain());
                    poInfoCriteria.setSpsTPoDueDomain(new SpsTPoDueDomain());
                    
                    for( TransferDoDataFromCigmaDomain trn : bufferList ){
                        for(TransferDoDetailDataFromCigmaDomain transferDetail 
                            : trn.getCigmaDoDetail()){
                            poInfoCriteria.getSpsTPoDomain().setRefCigmaPoNo( trn.getCigmaPoNo() );
                            poInfoCriteria
                                .getSpsTPoDetailDomain().setDPn( transferDetail.getPartNo() );
                            poInfoCriteria.getSpsTPoDueDomain().setDueDate( 
                                parseToSqlDate(transfer.getDeliveryDate(), PATTERN_YYYYMMDD) );
                            List<PurchaseOrderInformationDomain> poInfoList = 
                                purchaseOrderService.searchPurchaseOrderByDo(poInfoCriteria);
                            if( null == poInfoList || ZERO == poInfoList.size() ){
                                continue;
                            } // end if.
                            if( ZERO == sbErrorMsg.length() ) {
                                trn.setPoId(toBigDecimal(
                                    poInfoList.get(ZERO).getSpsTPoDomain().getPoId().toString()));
                                break;
                            }
                        } // end for detail.
                        if( null == trn.getPoId() ) {
                            for(TransferDoDetailDataFromCigmaDomain detail
                                : transfer.getCigmaDoDetail())
                            {
                                insertCigmaDoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                                    trn, detail, STR_ONE, ERROR_TYPE_PO_NOT_FOUND);
                            }
                            
                            sbErrorMsg.append( getMessage(locale, ERROR_CD_SP_E6_0046, 
                                transfer.getCigmaDoNo()))
                                .append( SYMBOL_CR_LF );
                            
                            continue;
                        }
                    }
                } // end find reference Po ID.
                for(TransferDoDataFromCigmaDomain trn : bufferList){
                    resultList.add(trn);
                }
            } // end IF check DO
        } // End Loop by DO header
        
        if( ZERO < sbErrorMsg.length() ) {
            writeLog(log, TWO, sbErrorMsg.toString());
            resultList.clear();
            if( !bError ){
                String partNoError = null;
                try{
                    partNoError = CommonWebServiceUtil.updateCigmaDeliveryOrder( as400Server, 
                        transferList, STR_TWO, jobId );
                }catch(Exception e){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, STR_TWO, Constants.DO);
                }
                if( !StringUtil.checkNullOrEmpty(partNoError) ){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, STR_TWO, Constants.DO );
                }
            }
        }
        
        return resultList;
    } // end check.
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactTransferErrorDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public void transactTransferErrorDoFromCigma(Locale locale,
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception {
        transferDoFromCigma(locale, transferList, as400Server, true, jobId);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactTransferDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public void transactTransferDoFromCigma(Locale locale,
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception {
        transferDoFromCigma(locale, transferList, as400Server, false, jobId);
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchChangeDeliveryOrder(java.util.Locale, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public Map<String, List<TransferChangeDoDataFromCigmaDomain>> searchChangeDeliveryOrder(
        Locale locale, As400ServerConnectionInformationDomain server, String spsFlag)
        throws Exception
    {
        // If search D/O transfer error (first step), delete all error record.
        if (Constants.STR_TWO.equals(spsFlag)) {
            this.deleteChgDoError(server.getSpsMCompanyDensoDomain().getDCd(), locale);
        }
        
        List<PseudoCigmaChangeDoInformationDomain> pseudoList = 
            CommonWebServiceUtil.searchCigmaChangeDeliveryOrder(server, spsFlag);
//        if( null == pseudoList || ZERO == pseudoList.size() ){
//            throwsErrorMessage(locale, ERROR_CD_SP_E6_0001, null);
//        }
        Map<String, List<TransferChangeDoDataFromCigmaDomain>> resultMap
            = new LinkedHashMap<String, List<TransferChangeDoDataFromCigmaDomain>>();
        for( TransferChangeDoDataFromCigmaDomain domain : convertToCigmaChangeDo(pseudoList) ){
            if( null == resultMap.get(domain.getCurrentDelivery()) ){
                resultMap.put(domain.getCurrentDelivery(), 
                    new ArrayList<TransferChangeDoDataFromCigmaDomain>());
            }
            resultMap.get(domain.getCurrentDelivery()).add( domain );
        }
        return resultMap; //
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#checkTransferChangeDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, boolean)
     */
    public List<TransferChangeDoDataFromCigmaDomain> transactCheckTransferChangeDoFromCigma(
        Locale locale,
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        Set<String> originalDoErrorSet,
        boolean bError, 
        String jobId,
        Log log) throws Exception
    {
        List<TransferChangeDoDataFromCigmaDomain> resultList
            = new ArrayList<TransferChangeDoDataFromCigmaDomain>();
        StringBuffer sbErrorMsg = new StringBuffer();
        // 1. Find Supplier Plant Code for each DENSO Part No and find SPS P/O No relate with D/O
        // initial criteria 1-1
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = 
            new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        // initial criteria 1-3
        PurchaseOrderInformationDomain poInfoCriteria = new PurchaseOrderInformationDomain();
        poInfoCriteria.setSpsTPoDomain(new SpsTPoDomain());
        poInfoCriteria.setSpsTPoDetailDomain(new SpsTPoDetailDomain());
        poInfoCriteria.setSpsTPoDueDomain(new SpsTPoDueDomain());
        
        for(TransferChangeDoDataFromCigmaDomain transfer : transferList) {
            String sPcd = null;
            TransferChangeDoDataFromCigmaDomain header = null;
            Map<String, TransferChangeDoDataFromCigmaDomain> headerMap
                = new LinkedHashMap<String, TransferChangeDoDataFromCigmaDomain>();
            // LOOP until end of cigmaDOInformation. <List>cigmaDODetail. dPartNo
            // 1-1) Get SPS SupplierCode from SpsMAs400VendorService.
            as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            as400VendorCriteriaDomain.setVendorCd( transfer.getVendorCd() );
            List<SpsMAs400VendorDomain> vendorList = 
                spsMAs400VendorService.searchByCondition(as400VendorCriteriaDomain);
            // check is not found Supplier Code.
            if( null == vendorList || ZERO == vendorList.size() ){
                // (1) Keep CIGMA Error data into SPS_CIGMA_DO_ERROR (Keep all data from the same CIGMA D/O No)
                insertCigmaChgDoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, null, STR_ZERO, ERROR_TYPE_VENDOR_NOT_FOUND);

                sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCurrentDelivery(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                break;
            } // end check not found.
            transfer.setSCd( vendorList.get(ZERO).getSCd() );
            
            SpsMDensoSupplierPartsCriteriaDomain partCriteria = 
                new SpsMDensoSupplierPartsCriteriaDomain();
            partCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            partCriteria.setDPcd(transfer.getPlantCode());
            partCriteria.setSCd(transfer.getSCd());
            // 1-2) Get Supplier Information data from DENSOSupplierPartService
            for(TransferChangeDoDetailDataFromCigmaDomain doDetail : transfer.getCigmaDoDetail()) {
                
                // [IN023] compare effective date by Delivery Date
                //partCriteria.setEffectDateLessThanEqual( 
                //    new Timestamp(transfer.getIssueDate().getTime()) );
                Timestamp deliveryDatetime = parseToTimestamp( 
                    appendsString(
                        transfer.getDeliveryDate(), 
                        transfer.getDeliveryTime() )
                    , PATTERN_YYYYMMDD_HHMM );
                partCriteria.setEffectDateLessThanEqual(deliveryDatetime);
                
                SpsMDensoSupplierPartsDomain parts = this.findPart(partCriteria, 
                    doDetail.getPartNo(), SupplierPortalConstant.SORT_PART_MASTER);
                // check is not found Supplier Part No
                if( null == parts){
                    String cause = ERROR_TYPE_PARTS_NO_NOT_FOUND;
                    partCriteria.setEffectDateLessThanEqual(null);
                    SpsMDensoSupplierPartsDomain effect = this.findPart(partCriteria,
                        doDetail.getPartNo(), SupplierPortalConstant.SORT_PART_MASTER_ASC);
                        
                    if (null != effect) {
                        cause = ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
                    }
                    
                    insertCigmaChgDoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                        transfer, doDetail, STR_ONE, cause);
                    
                    originalDoErrorSet.add(transfer.getOriginalDelivery());
                    
                    sbErrorMsg.append( getMessage(
                        locale, 
                        ERROR_CD_SP_E6_0021, 
                        transfer.getCurrentDelivery(),
                        doDetail.getPartNo() ) );
                    continue;
                }
                
                sPcd = parts.getSPcd();
                doDetail.setSPcd(sPcd);
                transfer.setSPcd(sPcd);
                
                if( null == headerMap.get(sPcd) ){
                    header = new TransferChangeDoDataFromCigmaDomain();
                    header = cloneNew(transfer, header);
                    headerMap.put(sPcd, header);
                }
                
                header = headerMap.get( sPcd );
                header.setSPcd( sPcd );
                doDetail.setSPn(parts.getSPn() );
                header.getCigmaDoDetail().add( doDetail );
                
                SpsTDoDomain doDomain = null;
                // 2-1-1)
                DeliveryOrderDomain doParameter = new DeliveryOrderDomain();
                doParameter.setDoHeader( new SpsTDoDomain() );
                doParameter.setDoDetail( new SpsTDoDetailDomain() );
                doParameter.getDoHeader().setCigmaDoNo( transfer.getOriginalDelivery() );
                doParameter.getDoHeader().setSCd( transfer.getSCd() );
                
                // [IN022] Add more criteria for check exist D/O
                doParameter.getDoHeader().setVendorCd(transfer.getVendorCd());
                doParameter.getDoHeader().setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
                
                doParameter.getDoHeader().setDeliveryDatetime( parseToTimestamp(
                    appendsString(transfer.getDeliveryDate()
                        , transfer.getDeliveryTime()), PATTERN_YYYYMMDD_HHMM) );

                /* Search Original
                 * 01 : change quantity
                 * d_pn match
                 * data_type match
                 * s_pcd match
                 * */
                doParameter.getDoDetail().setDPn( doDetail.getPartNo() );
                doParameter.getDoHeader().setDataType(transfer.getDataType());
                doParameter.getDoHeader().setSPcd( transfer.getSPcd() );
                
                List<SpsTDoDomain> doList = 
                    deliveryOrderService.searchExistDeliveryOrder(doParameter);
                if( null != doList && ZERO < doList.size() ){
                    doDomain = doList.get( ZERO );
                }else{
                    
                    // [IN007] Case add new part search all at once
                    // Search Original D/O with same DataType ignore DPN
                    //doParameter.getDoDetail().setDPn( null );
                    //doList = deliveryOrderService.searchExistDeliveryOrder(doParameter);
                    //if( null == doList || ZERO == doList.size() ){
                    //    // [INYYY] Set Original D/O before next step
                    //    //doDomain = doList.get( ZERO );
                    //}else{
                    //    // Search Original D/O with different DataType
                    //    doParameter.getDoHeader().setDataType(null);
                    /* Search Original
                     * 02 : add new parts
                     * d_pn not specified
                     * data_type not specified
                     * s_pcd not specified
                     * */
                    doParameter.getDoDetail().setDPn(null);
                    doParameter.getDoHeader().setDataType(null);
                    doParameter.getDoHeader().setSPcd(null);
                    doList = deliveryOrderService.searchExistDeliveryOrder(doParameter);
                    // [IN007]
                    //if( null != doList && ZERO < doList.size() ){
                    //    // [INYYY] Set Original D/O before next step
                    //    //doDomain = doList.get( ZERO );
                    //} else {
                    if( null == doList || ZERO == doList.size() ){
                        insertCigmaChgDoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                            transfer, doDetail, STR_ONE, ERROR_TYPE_DO_ORIGINAL_NOT_FOUND);

                        originalDoErrorSet.add(transfer.getOriginalDelivery());
                        
                        sbErrorMsg.append( getMessage(locale, ERROR_CD_SP_E6_0027, 
                            transfer.getCurrentDelivery(),
                            doDetail.getSPn(),
                            getLabelHandledException(locale
                                , TRANSFER_DO_STEP_SEARCH_EXISTS_DELIVERY_ORDER) ) );
                        // Skip check ASN
                        continue;
                    }
                }
                
                // [IN007] If not case change quantity, not have to check ASN
                if (null != doDomain) {
                    
                    // 2-1-2) Check D/O already create ASN
                    AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
                    criteria.setDoId( doDomain.getDoId().toString() );
                    criteria.setDPn( doDetail.getPartNo() );
                    List<AsnMaintenanceReturnDomain> doGroupAsnResultList =
                        deliveryOrderService.searchDoGroupAsn(criteria);
                    if(ZERO < doGroupAsnResultList.size()){
                        for(AsnMaintenanceReturnDomain doGroupAsn : doGroupAsnResultList){
                            if ( ZERO < doGroupAsn.getTotalShippedQty().intValue() ) {
                                insertCigmaChgDoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                                    transfer, doDetail, STR_ONE, ERROR_TYPE_DO_ALREADY_CREATE_ASN);

                                originalDoErrorSet.add(transfer.getOriginalDelivery());
                                
                                sbErrorMsg.append( getMessage(locale, ERROR_CD_SP_E6_0027, 
                                    transfer.getCurrentDelivery(),
                                    EMPTY_STRING,
                                    getLabelHandledException(locale
                                        , TRANSFER_DO_STEP_CHECK_DO_IN_CREATE_ASN) ) ); 
                                break;
                            } // end if.
                        }
                    }
                }
            } // end for Denso Part No.
            
            if( EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
                
                if (originalDoErrorSet.contains(transfer.getOriginalDelivery())) {
                    for(TransferChangeDoDetailDataFromCigmaDomain detail
                        : transfer.getCigmaDoDetail())
                    {
                        insertCigmaChgDoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                            transfer, detail, STR_ONE, ERROR_TYPE_PREVIOUS_REV_ERROR);
                    }
                    
                    sbErrorMsg.append( getMessage(locale, ERROR_CD_SP_E6_0027, 
                        transfer.getCurrentDelivery(),
                        EMPTY_STRING,
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_CHECK_PREVIOUS_REV_ERROR) ) ); 
                } else {
                    for (String strSpcd : headerMap.keySet()) {
                        resultList.add( headerMap.get(strSpcd) );
                    }
                }
            }
        } // End for D/O Header
        if( !EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
            writeLog(log, TWO, sbErrorMsg.toString());
            resultList.clear();
            String partNoError = null;
            if( !bError ) {
                try{
                    partNoError = CommonWebServiceUtil.updateCigmaChangeDeliveryOrder( as400Server, 
                        transferList, STR_TWO, jobId );
                } catch(Exception e) {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, STR_TWO, Constants.CHG_DO);
                }
                if( !StringUtil.checkNullOrEmpty(partNoError) ) {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, STR_TWO, Constants.CHG_DO );
                }
            }
        }
        return resultList;
    } // end check.
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactTransferErrorChgDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public String transactTransferErrorChgDoFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) 
        throws Exception {
        return transferChgDoFromCigma(
            locale,
            transferList, 
            as400Server, 
            true, 
            jobId);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactTransferChgDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public String transactTransferChgDoFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) 
        throws Exception {
        return transferChgDoFromCigma(
            locale, 
            transferList, 
            as400Server, 
            false, 
            jobId);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchRecordLimit()
     */
    public MiscellaneousDomain searchRecordLimit() throws ApplicationException {
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(LIMIT_BATCH_RECORD_TYPE);
        miscDomain.setMiscCode(LIMIT_MAX_REC_ORD_E_CODE);
        return recordLimitService.searchRecordLimit(miscDomain);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchCigmaDoError()
     */
    public List<TransferDoErrorEmailDomain> searchCigmaDoError(SpsCigmaDoErrorDomain criteria)
        throws ApplicationException
    {
        return this.cigmaDoErrorService.searchTransferDoError(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchCigmaChgDoError()
     */
    public List<TransferChgDoErrorEmailDomain> searchCigmaChgDoError(
        SpsCigmaChgDoErrorDomain criteria) throws ApplicationException
    {
        return this.cigmaChgDoErrorService.searchTransferChgDoError(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactSendNotificationAbnormalDataDo(java.util.Locale, java.util.List, java.util.List, int)
     */
    public List<String> transactSendNotificationAbnormalDataDo(Locale locale,
        List<TransferDoErrorEmailDomain> sendDoErrorToSupplier,
        List<TransferDoErrorEmailDomain> sendDoErrorToDenso,
        List<TransferDoErrorEmailDomain> sendDoErrorToAdmin,
        List<TransferChgDoErrorEmailDomain> sendChgDoErrorToSupplier,
        List<TransferChgDoErrorEmailDomain> sendChgDoErrorToDenso,
        List<TransferChgDoErrorEmailDomain> sendChgDoErrorToAdmin,
        List<TransferDoErrorEmailDomain> sendDoBackOrderErrorToAdmin,
        List<TransferChgDoErrorEmailDomain> sendChgDoBackOrderErrorToAdmin,
        int limit, Log log) throws Exception
    {
        List<String> recordErrorList = new ArrayList<String>();
        String sCd = null;
        
        // [IN016] Send email to supplier by plant
        String sPcd = null; 
        String tmpSPcd = null;
        
        String cigmaDoNo = null;
        String errorType = null;
        int iCount = ZERO;
        boolean bSended = false;
        boolean isSkip = false;
        StringBuffer sbTitle = new StringBuffer( 
            getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_SUBJECT) );
        StringBuffer sbContent = new StringBuffer();
        StringBuffer sbFrom = new StringBuffer();
        List<String> emailTo = new ArrayList<String>();
        UserSupplierDetailDomain criSupplierUser = new UserSupplierDetailDomain();
        criSupplierUser.setSpsMUserDomain( new SpsMUserDomain() );
        criSupplierUser.setSpsMUserSupplierDomain( new SpsMUserSupplierDomain() );
        criSupplierUser.getSpsMUserDomain().setIsActive( STR_ONE );
        criSupplierUser.getSpsMUserDomain().setUserType( Constants.STR_S );
        criSupplierUser.getSpsMUserSupplierDomain().setEmlSInfoNotfoundFlag( STR_ONE );
        sbFrom.append( ContextParams.getDefaultEmailFrom() );

        /* ----------------------------------------------------------------------------------------
         * 1. DO Error Send to Supplier (G)
         * */
        if( null != sendDoErrorToSupplier ){
            for(TransferDoErrorEmailDomain domain : sendDoErrorToSupplier){
                isSkip = false;
                if( null == sCd ){
                    emailTo.clear();
                    
                    // [IN016] Send email to supplier by plant
                    //isSkip = findMail(criSupplierUser, domain.getSCd(), emailTo, false);
                    isSkip = findMail(criSupplierUser, domain.getSCd(), domain.getSPcd()
                        , emailTo, false);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {domain.getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    sCd = domain.getSCd();
                    
                    // [IN016] send email to supplier by plant
                    if (null == domain.getSPcd()) {
                        sPcd = Constants.STR_NULL;
                    } else {
                        sPcd = domain.getSPcd();
                    }
                    
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    sbContent = new StringBuffer();
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillDoErrorMail(locale, sbContent, domain);
                }

                // [IN016] Send email to supplier by plant
                //if( !sCd.equals( domain.getSCd() ) ) {
                if (null == domain.getSPcd()) {
                    tmpSPcd = Constants.STR_NULL;
                } else {
                    tmpSPcd = domain.getSPcd();
                }
                if(!sCd.equals( domain.getSCd()) || !sPcd.equals(tmpSPcd)) {
                    
                    if (!bSended) {
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                            MAIL_CIGMA_DO_ERROR_FOOTER, sCd, log);
                        bSended = true;
                        emailTo.clear();
                    }

                    // [IN016] Send email to supplier by plant
                    //isSkip = findMail(criSupplierUser, domain.getSCd(), emailTo, false);
                    isSkip = findMail(criSupplierUser, domain.getSCd(), domain.getSPcd()
                        , emailTo, false);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {domain.getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    
                    sCd = domain.getSCd();

                    // [IN016] send email to supplier by plant
                    if (null == domain.getSPcd()) {
                        sPcd = Constants.STR_NULL;
                    } else {
                        sPcd = domain.getSPcd();
                    }
                    
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillDoErrorMail(locale, sbContent, domain);
                }
                
                if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                    || !errorType.equals(domain.getErrorTypeFlg()))
                {
                    if( iCount == limit ){
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                            MAIL_CIGMA_DO_ERROR_FOOTER, sCd, log);
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, domain.getErrorTypeFlg());
                        bSended = true;
                        iCount = ZERO;
                    }else{
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                    }
                    
                    // [IN068] Group data by D_CD and Issue Date
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();

                    fillDoErrorMail(locale, sbContent, domain);
                    iCount++;
                }
                
                for (TransferDoErrorEmailDetailDomain detail
                    : domain.getTransferDoErrorEmailDetailList())
                {
                    fillDoDetil(sbContent, locale, errorType, domain, detail);
                    bSended = false;
                }
            } // end for each SPS DO ERROR
            if( !bSended && ZERO < sendDoErrorToSupplier.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                    MAIL_CIGMA_DO_ERROR_FOOTER, sCd, log);
            }
        } // end if SPS DO ERROR send to Supplier
        
        /* ----------------------------------------------------------------------------------------
         * 2. DO Error Send to Administrator (E)
         * */
        String dCd = null;
        String dPcd = null;
        cigmaDoNo = null;
        errorType = null;
        SpsMUserDensoDomain criAdmin = new SpsMUserDensoDomain();
        // [IN049] Change column name
        //criDensoUser.setEmlAbnormalTransfer2Flag( STR_ONE );
        //criAdmin.setEmlAbnormalTransferFlg( STR_ONE ); //21082021 Back order
        
        if( null != sendDoErrorToAdmin ){
            for(TransferDoErrorEmailDomain domain : sendDoErrorToAdmin) {
                criAdmin.setEmlAbnormalTransferFlg( STR_ONE );
                isSkip = false;
                if( null == dCd ){
                    isSkip = findDensoMail(criAdmin, domain.getDCd(),
                        domain.getDPcd(), emailTo);
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {
                                StringUtil.appendsString(domain.getDCd(), 
                                    Constants.SYMBOL_COLON, domain.getDPcd())
                            }
                            , log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    dCd = domain.getDCd();
                    dPcd = domain.getDPcd();
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    bSended = false;
                    sbContent = new StringBuffer();
                    
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillDoErrorMail(locale, sbContent, domain);
                }
                
                if( !dCd.equals( domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                    if (!bSended) {
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                            MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
                        bSended = true;
                        emailTo.clear();
                    }

                    isSkip = findDensoMail(criAdmin, domain.getDCd(),
                        domain.getDPcd(), emailTo);
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {
                                StringUtil.appendsString(domain.getDCd(), 
                                    Constants.SYMBOL_COLON, domain.getDPcd())
                            }
                            , log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    dCd = domain.getDCd();
                    dPcd = domain.getDPcd();
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillDoErrorMail(locale, sbContent, domain);
                }
                
                if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                    || !errorType.equals(domain.getErrorTypeFlg()))
                {
                    if( iCount == limit ){
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                            MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, domain.getErrorTypeFlg());
                        bSended = true;
                        iCount = ZERO;
                    }else{
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                    }
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    fillDoErrorMail(locale, sbContent, domain);
                    iCount++;
                }
                
                for (TransferDoErrorEmailDetailDomain detail
                    : domain.getTransferDoErrorEmailDetailList())
                {
                    fillDoDetil(sbContent, locale, errorType, domain, detail);
                    bSended = false;
                }
                
            } // end for each SPS DO ERROR
            if( !bSended && ZERO < sendDoErrorToAdmin.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                    MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
            }
        } // end if SPS DO ERROR send to Administrator
        
        /* ----------------------------------------------------------------------------------------
         * 3. DO Error Send to DENSO User (C)
         * */
        dCd = null;
        dPcd = null;
        cigmaDoNo = null;
        errorType = null;
        SpsMUserDensoDomain criDenso = new SpsMUserDensoDomain();
        // [IN049] Change column name
        //criAdminDenso.setEmlAbnormalTransfer1Flag( STR_ONE );
        criDenso.setEmlPedpoDoalcasnUnmatpnFlg( STR_ONE );

        if( null != sendDoErrorToDenso ){
            for(TransferDoErrorEmailDomain domain : sendDoErrorToDenso) {
                isSkip = false;
                if( null == dCd ){
                    isSkip = findDensoMail(criDenso, domain.getDCd(),
                        domain.getDPcd(), emailTo);
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {
                                StringUtil.appendsString(domain.getDCd(), 
                                    Constants.SYMBOL_COLON, domain.getDPcd())
                            }
                            , log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    dCd = domain.getDCd();
                    dPcd = domain.getDPcd();
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    bSended = false;
                    sbContent = new StringBuffer();
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillDoErrorMail(locale, sbContent, domain);
                }
                
                if( !dCd.equals( domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                    if (!bSended) {
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                            MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
                        bSended = true;
                        emailTo.clear();
                    }
                    
                    isSkip = findDensoMail(criDenso, domain.getDCd(),
                        domain.getDPcd(), emailTo);
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {
                                StringUtil.appendsString(domain.getDCd(), 
                                    Constants.SYMBOL_COLON, domain.getDPcd())
                            }
                            , log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    dCd = domain.getDCd();
                    dPcd = domain.getDPcd();
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillDoErrorMail(locale, sbContent, domain);
                }
                
                if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                    || !errorType.equals(domain.getErrorTypeFlg()))
                {
                    if( iCount == limit ){
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                            MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, domain.getErrorTypeFlg());
                        bSended = true;
                        iCount = ZERO;
                    }else{
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                    }
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    fillDoErrorMail(locale, sbContent, domain);
                    iCount++;
                }
                
                for (TransferDoErrorEmailDetailDomain detail
                    : domain.getTransferDoErrorEmailDetailList())
                {
                    fillDoDetil(sbContent, locale, errorType, domain, detail);
                    bSended = false;
                }
            } // end for each SPS DO ERROR
            if( !bSended && ZERO < sendDoErrorToDenso.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                    MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
            }
        } // end if SPS DO ERROR send to DENSO
        
        /* ----------------------------------------------------------------------------------------
         * 4. CHG DO Error Send to Supplier (G)
         * */
        sCd = null;
        
        // [IN016] send email to supplier by plant
        sPcd = null;
        
        cigmaDoNo = null;
        errorType = null;
        isSkip = false;
        emailTo.clear();
        sbTitle = new StringBuffer(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_2_ERROR_SUBJECT));
        
        if ( null != sendChgDoErrorToSupplier ) {
            for(TransferChgDoErrorEmailDomain domain : sendChgDoErrorToSupplier){
                isSkip = false;
                if( null == sCd ){
                    emailTo.clear();
                    
                    // [IN016] Send email to supplier by plant
                    //isSkip = findMail(criSupplierUser, domain.getSCd(), emailTo, false);
                    isSkip = findMail(criSupplierUser, domain.getSCd(), domain.getSPcd()
                        , emailTo, false);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {domain.getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }

                    sCd = domain.getSCd();
                    
                    // [IN016] send email to supplier by plant
                    if (null == domain.getSPcd()) {
                        sPcd = Constants.STR_NULL;
                    } else {
                        sPcd = domain.getSPcd();
                    }
                    
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    bSended = false;
                    sbContent = new StringBuffer();
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillChgDoErrorMail(locale, sbContent, domain, null);
                }
                
                // [IN016] Send email to supplier by plant
                //if( !sCd.equals( domain.getSCd() ) ) {
                if (null == domain.getSPcd()) {
                    tmpSPcd = Constants.STR_NULL;
                } else {
                    tmpSPcd = domain.getSPcd();
                }
                if(!sCd.equals( domain.getSCd()) || !sPcd.equals(tmpSPcd)) {
                
                    if (!bSended) {
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                            MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER, sCd, log);
                        bSended = true;
                        emailTo.clear();
                    }

                    // [IN016] Send email to supplier by plant
                    //isSkip = findMail(criSupplierUser, domain.getSCd(), emailTo, false);
                    isSkip = findMail(criSupplierUser, domain.getSCd(), domain.getSPcd()
                        , emailTo, false);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {domain.getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    sCd = domain.getSCd();
                    
                    // [IN016] send email to supplier by plant
                    if (null == domain.getSPcd()) {
                        sPcd = Constants.STR_NULL;
                    } else {
                        sPcd = domain.getSPcd();
                    }
                    
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    fillChgDoErrorMail(locale, sbContent, domain, null);
                }
                
                if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                    || !errorType.equals(domain.getErrorTypeFlg()))
                {
                    if( iCount == limit ){
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                            MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER, sCd, log);
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, domain.getErrorTypeFlg());
                        bSended = true;
                        iCount = ZERO;
                    }else{
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                    }
                    
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    fillChgDoErrorMail(locale, sbContent, domain, null);
                    iCount++;
                }
                
                for (TransferChgDoErrorEmailDetailDomain detail
                    : domain.getTransferChgDoErrorEmailDetailList())
                {
                    fillChgDoDetail(sbContent, locale, errorType, domain, detail);
                    bSended = false;
                }
            } // end for each SPS DO ERROR
            if( !bSended && ZERO < sendChgDoErrorToSupplier.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                    MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER, sCd, log);
            }
        } // end if SPS CHG DO ERROR send to Supplier
        
        /* ----------------------------------------------------------------------------------------
         * 5. CHG DO Error Send to Administrator (E)
         * */
        dCd = null;
        dPcd = null;
        cigmaDoNo = null;
        errorType = null;
        errorType = null;

        if (null != sendChgDoErrorToAdmin) {
            for (TransferChgDoErrorEmailDomain domain : sendChgDoErrorToAdmin) {
                isSkip = false;
                
                criAdmin.setEmlAbnormalTransferFlg( STR_ONE );
                if( null == dCd ){
                    isSkip = findDensoMail(criAdmin, domain.getDCd(),
                        domain.getDPcd(), emailTo);
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {
                                StringUtil.appendsString(domain.getDCd(), 
                                    Constants.SYMBOL_COLON, domain.getDPcd())
                            }
                            , log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    dCd = domain.getDCd();
                    dPcd = domain.getDPcd();
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    bSended = false;
                    sbContent = new StringBuffer();
                    sbTitle = new StringBuffer(
                        getEmailLabel(locale, MAIL_CIGMA_CHG_DO_2_ERROR_SUBJECT));
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    this.fillChgDoErrorMail(locale, sbContent, domain, null);
                }
                
                if( !dCd.equals( domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                    if (!bSended) {
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                            MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                        bSended = true;
                        emailTo.clear();
                    }

                    isSkip = findDensoMail(criAdmin, domain.getDCd(),
                        domain.getDPcd(), emailTo);
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                            , new String[] {
                                StringUtil.appendsString(domain.getDCd(), 
                                    Constants.SYMBOL_COLON, domain.getDPcd())
                            }
                            , log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    dCd = domain.getDCd();
                    dPcd = domain.getDPcd();
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    iCount = ONE;
                    // [IN025] change email content
                    //initialDoMail(locale, emailTo, sbContent);
                    initialDoMail(locale, emailTo, sbContent, errorType);
                    this.fillChgDoErrorMail(locale, sbContent, domain, null);
                }
                
                if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                    || !errorType.equals(domain.getErrorTypeFlg()))
                {
                    if( iCount == limit ){
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                            MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, domain.getErrorTypeFlg());
                        bSended = true;
                        iCount = ZERO;
                    }else{
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                    }
                    cigmaDoNo = domain.getCigmaDoNo();
                    errorType = domain.getErrorTypeFlg();
                    this.fillChgDoErrorMail(locale, sbContent, domain, null);
                    
                    iCount++;
                }
                
                for (TransferChgDoErrorEmailDetailDomain detail
                    : domain.getTransferChgDoErrorEmailDetailList())
                {
                    fillChgDoDetail(sbContent, locale, errorType, domain, detail);
                    bSended = false;
                }
            }
            if( !bSended && ZERO < sendChgDoErrorToAdmin.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                    MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
            }
        }

        /* ----------------------------------------------------------------------------------------
         * 6. CHG DO Error Send to DENSO User (C)
         * */
        dCd = null;
        dPcd = null;
        cigmaDoNo = null;
        errorType = null;
        StringBuffer sbContentHeader = null;
        StringBuffer sbContentDetail = null;
        List<String> asnNoList = new ArrayList<String>();
        DeliveryOrderDomain criAsnNo = new DeliveryOrderDomain();
        criAsnNo.setDoHeader(new SpsTDoDomain());
        criAsnNo.setDoDetail(new SpsTDoDetailDomain());

        if (null != sendChgDoErrorToDenso) {
            for (TransferChgDoErrorEmailDomain domain : sendChgDoErrorToDenso) {
                isSkip = false;
                if (ERROR_TYPE_DO_ALREADY_CREATE_ASN.equals(domain.getErrorTypeFlg())) {
                    // Error Type DO Already create ASN
                    if (null != errorType && !ERROR_TYPE_DO_ALREADY_CREATE_ASN.equals(errorType)) {
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                            MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                        dCd = null;
                        dPcd = null;
                        cigmaDoNo = null;
                        errorType = null;
                    }
                    errorType = domain.getErrorTypeFlg();
                    
                    if( null == dCd ){
                        isSkip = this.findDensoMail(criDenso, domain.getDCd(),
                            domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        iCount = ZERO;
                        sbTitle = new StringBuffer(
                            getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_SUBJECT));
                        sbContent = new StringBuffer();
                        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER1));
                        
                        // [IN070] add list of email to send in content
                        if (null != emailTo && ZERO != emailTo.size()) {
                            String emailToString = StringUtil.convertListToStringCommaSeperate(emailTo);
                            sbContent.append(appendsString(
                                SYMBOL_NEWLINE
                                , getEmailLabel(locale, MAIL_CIGMA_ERROR_PERSON_IN_CHARGE)
                                , emailToString
                                , Constants.SYMBOL_END_PARAGRAPH
                                , SYMBOL_NEWLINE));
                        }
                        
                        sbContentDetail = new StringBuffer();
                        sbContentHeader = new StringBuffer();
                    }

                    if( !dCd.equals(domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                        if (!bSended) {
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                                MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER, dCd, log);
                            bSended = true;
                        }

                        isSkip = this.findDensoMail(criDenso, domain.getDCd(),
                            domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        sbContent = new StringBuffer();
                        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER1));
                        
                        // [IN070] add list of email to send in content
                        if (null != emailTo && ZERO != emailTo.size()) {
                            String emailToString = StringUtil.convertListToStringCommaSeperate(emailTo);
                            sbContent.append(appendsString(
                                SYMBOL_NEWLINE
                                , getEmailLabel(locale, MAIL_CIGMA_ERROR_PERSON_IN_CHARGE)
                                , emailToString
                                , Constants.SYMBOL_END_PARAGRAPH
                                , SYMBOL_NEWLINE));
                        }
                        
                        sbContentDetail = new StringBuffer();
                        sbContentHeader = new StringBuffer();
                        asnNoList.clear();
                        iCount = Constants.ONE;
                    }
                    
                    asnNoList.clear();
                    sbContentDetail = new StringBuffer();
                    if( iCount == limit ){
                        sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                            MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER, dCd, log);
                        
                        bSended = true;
                        sbContent = new StringBuffer();
                        iCount = ZERO;
                    }
                    ++iCount;
                    
                    for (TransferChgDoErrorEmailDetailDomain detail
                        : domain.getTransferChgDoErrorEmailDetailList())
                    {
                        // [IN022] Add criteria for search ASN
                        criAsnNo.getDoHeader().setVendorCd(domain.getVendorCd());
                        criAsnNo.getDoHeader().setDCd(domain.getDCd());
                        
                        criAsnNo.getDoHeader().setCigmaDoNo(domain.getOriginalCigmaDoNo());
                        criAsnNo.getDoHeader().setSCd( domain.getSCd() );
                        criAsnNo.getDoHeader().setDeliveryDatetime(
                            new Timestamp(domain.getDeliveryDate().getTime()));
                        criAsnNo.getDoDetail().setDPn(detail.getCigmaChgDoError().getDPn());
                        criAsnNo.getDoHeader().setDataType(detail.getCigmaChgDoError()
                            .getDataType());
                        
                        List<DoCreatedAsnDomain> asnResultList = 
                            deliveryOrderService.searchDoCreatedAsn(criAsnNo);
                        if( !(null != asnResultList && ZERO < asnResultList.size()) ){
                            return recordErrorList;
                        }
                        mergeList(asnNoList, asnResultList);
                        
                        String wh = detail.getCigmaChgDoError().getWh();
                        if(StringUtil.checkNullOrEmpty(wh)){
                            wh = SYMBOL_NBSP;
                        }
                        
                        String dockCode = detail.getCigmaChgDoError().getDockCode();
                        if(StringUtil.checkNullOrEmpty(dockCode)){
                            dockCode = SYMBOL_NBSP;
                        }
                        
                        DoCreatedAsnDomain asnResult = asnResultList.get(ZERO);
                        String chgDoDetail = getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_DETAIL);
                        chgDoDetail = chgDoDetail.replaceAll(SupplierPortalConstant.REPLACE_USERNAME,
                            appendsString(
                                nullToEmpty(asnResult.getSpsMUserDomain().getFirstName())
                                , SYMBOL_NBSP
                                , nullToEmpty(asnResult.getSpsMUserDomain().getMiddleName())
                                , SYMBOL_NBSP
                                , nullToEmpty(asnResult.getSpsMUserDomain().getLastName())));
                        
                        chgDoDetail = chgDoDetail.replaceAll(
                            SupplierPortalConstant.REPLACE_DPARTNO,
                            detail.getCigmaChgDoError().getDPn());
                        chgDoDetail = chgDoDetail.replaceAll(
                            SupplierPortalConstant.REPLACE_SHIPPING_QTY, asnResult
                                .getSpsTAsnDetailDomain().getShippingQty().toString());
                        chgDoDetail = chgDoDetail.replaceAll(SupplierPortalConstant.REPLACE_WH, wh);
                        chgDoDetail = chgDoDetail.replaceAll(
                            SupplierPortalConstant.REPLACE_DOCKCODE, dockCode);
                        sbContentDetail.append(chgDoDetail);
                        bSended = false;
                    } // End loop Detail Email
                    
                    sbContentHeader = new StringBuffer();
                    this.fillChgDoErrorMail(locale, sbContentHeader, domain, asnNoList);
                    
                    sbContent.append( Constants.SYMBOL_END_TABLE );
                    sbContent.append( sbContentHeader.toString() );
                    sbContent.append( sbContentDetail.toString() );
                } else {
                    // Other Error Type
                    if (null != errorType && ERROR_TYPE_DO_ALREADY_CREATE_ASN.equals(errorType)) {
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                        sbContent.append( getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER) );
                        // 1-6) Call MailService sendEmail() to create e-mail
                        sendMail(locale, sbTitle.toString(), sbContent.toString(), 
                            sbFrom.toString(), emailTo, false, dCd, log);
                        
                        dCd = null;
                        dPcd = null;
                        cigmaDoNo = null;
                        errorType = null;
                    }
                    errorType = domain.getErrorTypeFlg();
                    if( null == dCd ){
                        isSkip = findDensoMail(criDenso, domain.getDCd(),
                            domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        iCount = ONE;
                        bSended = false;
                        sbContent = new StringBuffer();
                        sbTitle = new StringBuffer(
                            getEmailLabel(locale, MAIL_CIGMA_CHG_DO_2_ERROR_SUBJECT));
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, errorType);
                        this.fillChgDoErrorMail(locale, sbContent, domain, null);
                    }
                    
                    if( !dCd.equals( domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                        if (!bSended) {
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                                MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                            bSended = true;
                            emailTo.clear();
                        }
                        
                        isSkip = findDensoMail(criDenso, domain.getDCd(),
                            domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        iCount = ONE;
                        // [IN025] change email content
                        //initialDoMail(locale, emailTo, sbContent);
                        initialDoMail(locale, emailTo, sbContent, errorType);
                        this.fillChgDoErrorMail(locale, sbContent, domain, null);
                    }
                    
                    if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                        || !errorType.equals(domain.getErrorTypeFlg()))
                    {
                        if( iCount == limit ){
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                                MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                            // [IN025] change email content
                            //initialDoMail(locale, emailTo, sbContent);
                            initialDoMail(locale, emailTo, sbContent, domain.getErrorTypeFlg());
                            bSended = true;
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                        }
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        this.fillChgDoErrorMail(locale, sbContent, domain, null);
                        iCount++;
                    }
                    
                    for (TransferChgDoErrorEmailDetailDomain detail
                        : domain.getTransferChgDoErrorEmailDetailList())
                    {
                        fillChgDoDetail(sbContent, locale, errorType, domain, detail);
                        bSended = false;
                    }
                }
            }
            if( !bSended && ZERO < sendChgDoErrorToDenso.size() ){
                if( ERROR_TYPE_DO_ALREADY_CREATE_ASN.equals(errorType) ){
                    sbContent.append( Constants.SYMBOL_END_TABLE );
                    sbContent.append( getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER) );
                    // 1-6) Call MailService sendEmail() to create e-mail
                    sendMail(
                        locale,
                        sbTitle.toString(), 
                        sbContent.toString(), 
                        sbFrom.toString(), 
                        emailTo,
                        false, dCd, log);
                } else {
                    sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                        MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                }
            }
        }
        
        //Start [CR20230129] change group send mail DO Backorder
        /* ----------------------------------------------------------------------------------------
         * 7. DO Backorder Error Send to Administrator (E)
         * */
        dCd = null;
        dPcd = null;
        cigmaDoNo = null;
        errorType = null;
        criAdmin = new SpsMUserDensoDomain();
        // [IN049] Change column name
        //criDensoUser.setEmlAbnormalTransfer2Flag( STR_ONE );
        //criAdmin.setEmlAbnormalTransferFlg( STR_ONE ); //21082021 Back order
        
        if( null != sendDoBackOrderErrorToAdmin ){
            for(TransferDoErrorEmailDomain domain : sendDoBackOrderErrorToAdmin) {
                // 21082021 Backorder
                if(domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND) 
                    || domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND)
                    || domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST)
                    || domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN)){
                    
                    String doType = "ORI";
                    criAdmin.setEmlCreateCancelAsnFlag( STR_ONE );
                    isSkip = false;
                    if( null == dCd ){
                        isSkip = findDensoMail(criAdmin, domain.getDCd(), domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        iCount = ONE;
                        bSended = false;
                        sbContent = new StringBuffer();
                        
                        sbTitle = new StringBuffer(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_SUBJECT_BACKORDER));
                        
                        initialDoMailBackorder(locale, emailTo, sbContent, errorType, doType);
                        fillDoErrorMailBackorder(locale, sbContent, domain);
                    }
                    
                    if( !dCd.equals( domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                        if (!bSended) {
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
                            bSended = true;
                            emailTo.clear();
                        }

                        isSkip = findDensoMail(criAdmin, domain.getDCd(), domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        iCount = ONE;
                        
                        sbTitle = new StringBuffer(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_SUBJECT_BACKORDER));
                        initialDoMailBackorder(locale, emailTo, sbContent, errorType, doType);
                        fillDoErrorMailBackorder(locale, sbContent, domain);
                    }
                    
                    if (!cigmaDoNo.equals(domain.getCigmaDoNo()) || !errorType.equals(domain.getErrorTypeFlg()))
                    {
                        if( iCount == limit ){
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
                            // [IN025] change email content
                            //initialDoMail(locale, emailTo, sbContent);
                            initialDoMailBackorder(locale, emailTo, sbContent, domain.getErrorTypeFlg(), doType);
                            bSended = true;
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                        }
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        fillDoErrorMailBackorder(locale, sbContent, domain);
                        iCount++;
                    }
                    
                    for (TransferDoErrorEmailDetailDomain detail: domain.getTransferDoErrorEmailDetailList())
                    {
                        fillDoDetilBackorder(sbContent, locale, errorType, domain, detail);
                        bSended = false;
                    }
                    // 21082021 Backorder
                }
                
            } // end for each SPS DO Backorder ERROR
            if( !bSended && ZERO < sendDoBackOrderErrorToAdmin.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                    MAIL_CIGMA_DO_ERROR_FOOTER, dCd, log);
            }
        } // end if SPS DO Backorder ERROR send to Administrator
        
        /* ----------------------------------------------------------------------------------------
         * 8. CHG DO Backorder Error Send to Administrator (E)
         * */
        dCd = null;
        dPcd = null;
        cigmaDoNo = null;
        errorType = null;
        errorType = null;

        if (null != sendChgDoBackOrderErrorToAdmin) {
            for (TransferChgDoErrorEmailDomain domain : sendChgDoBackOrderErrorToAdmin) {
                isSkip = false;
                // 21082021 Backorder
                if(domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND) 
                    || domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND)
                    || domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST)
                    || domain.getErrorTypeFlg().trim().equals(ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN)){
                    
                    String doType = "CHG";
                    criAdmin.setEmlCreateCancelAsnFlag( STR_ONE );
                    if( null == dCd ){
                        isSkip = findDensoMail(criAdmin, domain.getDCd(), domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        iCount = ONE;
                        bSended = false;
                        sbContent = new StringBuffer();
                        sbTitle = new StringBuffer(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_ERROR_SUBJECT_BACKORDER));
                        initialDoMailBackorder(locale, emailTo, sbContent, errorType, doType);
                        fillChgDoErrorMailBackorder(locale, sbContent, domain);
                    }
                    
                    if( !dCd.equals( domain.getDCd()) || !dPcd.equals(domain.getDPcd())) {
                        if (!bSended) {
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,
                                MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                            bSended = true;
                            emailTo.clear();
                        }

                        isSkip = findDensoMail(criAdmin, domain.getDCd(),
                            domain.getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getDCd();
                        dPcd = domain.getDPcd();
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        iCount = ONE;
                        initialDoMailBackorder(locale, emailTo, sbContent, errorType, doType);
                        fillChgDoErrorMailBackorder(locale, sbContent, domain);
                    }
                    
                    if (!cigmaDoNo.equals(domain.getCigmaDoNo())
                        || !errorType.equals(domain.getErrorTypeFlg()))
                    {
                        if( iCount == limit ){
                            sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo,MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
                            initialDoMailBackorder(locale, emailTo, sbContent, domain.getErrorTypeFlg(), doType);
                            bSended = true;
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                        }
                        cigmaDoNo = domain.getCigmaDoNo();
                        errorType = domain.getErrorTypeFlg();
                        fillChgDoErrorMailBackorder(locale, sbContent, domain);
                        
                        iCount++;
                    }
                    
                    for (TransferChgDoErrorEmailDetailDomain detail : domain.getTransferChgDoErrorEmailDetailList())
                    {
                        fillChgDoDetilBackorder(sbContent, locale, errorType, domain, detail);
                        bSended = false;
                    }
                    
                }// end if Backorder
            }
            if( !bSended && ZERO < sendChgDoBackOrderErrorToAdmin.size() ){
                sendDoMail(locale, sbContent, sbTitle, sbFrom, emailTo, 
                    MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER, dCd, log);
            }
        }
        //End [CR20230129] change group send mail DO Backorder

        return recordErrorList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchUrgentRecordLimit()
     */
    public MiscellaneousDomain searchUrgentRecordLimit() throws ApplicationException {
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(LIMIT_BATCH_RECORD_TYPE);
        miscDomain.setMiscCode(LIMIT_MAX_REC_ORD_C_CODE);
        return recordLimitService.searchRecordLimit(miscDomain);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchUrgentOrder(com.globaldenso.asia.sps.business.domain.UrgentOrderDomain)
     */
    public List<DeliveryOrderDomain> searchUrgentOrder(UrgentOrderDomain criteria) 
        throws ApplicationException {
        return deliveryOrderService.searchUrgentOrder(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactSendNotificationChangeOrder(java.util.Locale, java.util.List, int)
     */
    public void transactSendNotificationChangeOrder(Locale locale
        , List<DeliveryOrderDomain> sendUrgentMail, int limit, Log log) 
        throws Exception {
//        List<String> recordErrorList = new ArrayList<String>();
        String sCd = null;
        
        // [IN016] Send email to supplier by plant
        String sPcd = null;
        
        String cigmaDoNo = null;
        int iCount = Constants.ZERO;
        boolean bSended = false;
        StringBuffer sbTitle = new StringBuffer( 
            getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_SUBJECT) );
        StringBuffer sbContent = new StringBuffer();
        StringBuffer sbFrom = new StringBuffer();
        List<String> emailTo = new ArrayList<String>();
        UserSupplierDetailDomain criSupplierUser = new UserSupplierDetailDomain();
        criSupplierUser.setSpsMUserDomain( new SpsMUserDomain() );
        criSupplierUser.setSpsMUserSupplierDomain( new SpsMUserSupplierDomain() );
        criSupplierUser.getSpsMUserDomain().setIsActive( STR_ONE );
        criSupplierUser.getSpsMUserDomain().setUserType( Constants.STR_S );
        criSupplierUser.getSpsMUserSupplierDomain().setEmlSInfoNotfoundFlag( STR_ONE );
        sbFrom.append( ContextParams.getDefaultEmailFrom() );
        if( null != sendUrgentMail){
            for(DeliveryOrderDomain domain : sendUrgentMail){
                //System.out.println(domain.getDoHeader().getDataType());
                
                boolean isSkip = false;
                if( null == sCd ){
                    
                    // [IN016] Send email to supplier by plant
                    //isSkip = findMail(criSupplierUser, domain.getDoHeader().getSCd(),
                    //    emailTo, true);
                    isSkip = findMail(criSupplierUser, domain.getDoHeader().getSCd()
                        , domain.getDoHeader().getSPcd(), emailTo, true);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0001
                            , new String[] {domain.getDoHeader().getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    sCd = domain.getDoHeader().getSCd();

                    // [IN016] Send email to supplier by plant
                    sPcd = domain.getDoHeader().getSPcd();
                    
                    cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                    updateMailFlag(locale, domain);
                    iCount = ONE;
                    initialCigmaUrgent(locale, sbContent, domain);
                    //System.out.println("WHATAAA");
                    fillUrgentMail(locale, sbContent, domain);

                // [IN016] Send email to supplier by plant
                //}else if( null != sCd && !sCd.equals( domain.getDoHeader().getSCd() ) ) {
                } else if(!sCd.equals( domain.getDoHeader().getSCd())
                    || !sPcd.equals(domain.getDoHeader().getSPcd())) 
                {
                    
                    if (!bSended) {
                        sendUrgentMail(locale, sbTitle, sbFrom, sbContent, emailTo, sCd, log);
                        bSended = true;
                        emailTo.clear();
                    }
                    
                    // [IN016] Send email to supplier by plant
                    //isSkip = findMail(criSupplierUser, domain.getDoHeader().getSCd(),
                    //    emailTo, true);
                    isSkip = findMail(criSupplierUser, domain.getDoHeader().getSCd()
                        , domain.getDoHeader().getSPcd(), emailTo, true);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0001
                            , new String[] {domain.getDoHeader().getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    sCd = domain.getDoHeader().getSCd();

                    // [IN016] Send email to supplier by plant
                    sPcd = domain.getDoHeader().getSPcd();
                    
                    cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                    updateMailFlag(locale, domain);
                    iCount = ONE;
                    initialCigmaUrgent(locale, sbContent, domain);
                    fillUrgentMail(locale, sbContent, domain);
                }else if( !cigmaDoNo.equals( domain.getDoHeader().getCigmaDoNo() ) ) {
                    if( iCount == limit ){
                        sendUrgentMail(locale, sbTitle, sbFrom, sbContent, emailTo, sCd, log);
                        bSended = true;
                        iCount = ZERO;
                        
                        initialCigmaUrgent(locale, sbContent, domain);
                    }else {
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                        sbContent.append( SYMBOL_NEWLINE );
                    }
                    
                    cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                    //sbContent.append( Constants.SYMBOL_START_PARAGRAPH );
                    //initialCigmaUrgent(locale, sbContent);
                    updateMailFlag(locale, domain);
                    fillUrgentMail(locale, sbContent, domain);
                    iCount++;
                }else {
                    updateMailFlag(locale, domain);
                }
                
                String userName = domain.getDoDetail().getLastUpdateDscId();
                if(StringUtil.checkNullOrEmpty(userName)){
                    userName = SYMBOL_NBSP;
                }
                
                String um = domain.getDoDetail().getUnitOfMeasure();
                if(StringUtil.checkNullOrEmpty(um)){
                    um = SYMBOL_NBSP;
                }
                
                String rsn = domain.getDoDetail().getChgReason();
                if(StringUtil.checkNullOrEmpty(rsn)){
                    rsn = SYMBOL_NBSP;
                }
                
                String wh = domain.getDoHeader().getWhPrimeReceiving();
                if(StringUtil.checkNullOrEmpty(wh)){
                    wh = SYMBOL_NBSP;
                }
                
                String dockCode = domain.getDoHeader().getDockCode();
                if(StringUtil.checkNullOrEmpty(dockCode)){
                    dockCode = SYMBOL_NBSP;
                }

                String chgCigmaDoNo = domain.getDoDetail().getChgCigmaDoNo();
                if(StringUtil.checkNullOrEmpty(chgCigmaDoNo)){
                    chgCigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                }
                
                String urgentDoDetail = getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_DETAIL);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_USERNAME, userName);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_DPARTNO, domain.getDoDetail().getDPn());
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_SPARTNO, domain.getDoDetail().getSPn());
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_UM, um);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_PREVIOUS_QTY,
                    domain.getDoDetail().getPreviousQty().toString());
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_CURRENT_QTY,
                    domain.getDoDetail().getCurrentOrderQty().toString());
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_DIFFERENCE_QTY,
                    domain.getDoDetail().getCurrentOrderQty().subtract(
                        domain.getDoDetail().getPreviousQty()).toString());
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_QTYBOX,
                    domain.getDoDetail().getQtyBox().toString());
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_RSN, rsn);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_CHG_CIGMA_DO_NO, chgCigmaDoNo);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_WH, wh);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_DOCKCODE, dockCode);
                urgentDoDetail = urgentDoDetail.replaceAll(
                    SupplierPortalConstant.REPLACE_RCVLANE, domain.getDoDetail().getRcvLane());
                sbContent.append(urgentDoDetail);
                bSended = false;
            } // end for each SPS DO URGENT
            if( !bSended && ZERO < sendUrgentMail.size() ){
                sendUrgentMail(locale, sbTitle, sbFrom, sbContent, emailTo, sCd, log);
            }
        } // end if SPS DO URGENT
        return ;
    }
    
    /**
     * 
     * <p>Insert CIGMA_DO_ERROR.</p>
     *
     * @param locale Locale
     * @param criteria SpsMCompanyDensoDomain
     * @param transfer TransferDoDataFromCigmaDomain
     * @param transferDetail TransferDoDetailDataFromCigmaDomain
     * @param mailFlag flag
     * @param errorType ERROR_TYPE_FLG
     * @throws ApplicationException exception
     */
    private void insertCigmaDoError( Locale locale,
        SpsMCompanyDensoDomain criteria,
        TransferDoDataFromCigmaDomain transfer, 
        TransferDoDetailDataFromCigmaDomain transferDetail,
        String mailFlag,
        String errorType
    ) throws ApplicationException {
        try {
            SpsCigmaDoErrorDomain errorDo = new SpsCigmaDoErrorDomain();
            errorDo.setCigmaDoNo( transfer.getCigmaDoNo().trim() );
            errorDo.setDataType( transfer.getDataType() );
            if( null != transfer.getSCd() ){
                errorDo.setSCd( transfer.getSCd() );
            }else{
                errorDo.setSCd( null );
            }
            errorDo.setIssueDate( new Date(transfer.getIssueDate().getTime()) );
            errorDo.setMailFlg( mailFlag );
            errorDo.setErrorTypeFlg(errorType);
            errorDo.setDCd( criteria.getDCd().trim() );
            errorDo.setDPcd( transfer.getPlantCode().trim() );
            errorDo.setVendorCd( transfer.getVendorCd() );
            errorDo.setDeliveryDate( 
                parseToSqlDate(transfer.getDeliveryDate(), PATTERN_YYYYMMDD) );
            errorDo.setDeliveryTime( transfer.getDeliveryTime() );
            errorDo.setUpdateByUserId( transfer.getUpdateByUserId().trim() );

            if (null != transferDetail) {
                errorDo.setSPcd( transferDetail.getSPcd() );
                errorDo.setDPn( transferDetail.getPartNo() );
                errorDo.setCurrentOrderQty( 
                    toBigDecimal(transferDetail.getCurrentOrderQty()) );
            }
            
            spsCigmaDoErrorService.create(errorDo);
        } catch (ApplicationException e) {
            String parnNo = EMPTY_STRING;
            String orderQty = EMPTY_STRING;
            
            if (null != transferDetail) {
                parnNo = transferDetail.getPartNo();
                orderQty = transferDetail.getCurrentOrderQty();
            }
            
            throwsErrorMessage(locale, 
                ERROR_CD_SP_E6_0025,
                Constants.DO, transfer.getCigmaDoNo(), parnNo, orderQty,
                TABLE_CIGMA_DO_NAME
            );
        }
    }
    
    /**
     * Delete error record in SPS_CIGMA_CHG_DO_ERROR.
     * @param dCd - DENSO company code
     * @param locale - for get message
     * @throws Exception - when catch exception
     * */
    private void deleteDoError(String dCd, Locale locale)
        throws Exception
    {
        SpsCigmaDoErrorCriteriaDomain errorDomain = new SpsCigmaDoErrorCriteriaDomain();
        errorDomain.setDCd(dCd);
        try {
            spsCigmaDoErrorService.deleteByCondition(errorDomain);
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0023, 
                EMPTY_STRING, EMPTY_STRING,
                TABLE_CIGMA_DO_NAME );
        }
    }
    
    /**
     * 
     * <p>Insert CIGMA_CHG_DO_ERROR.</p>
     *
     * @param locale Locale
     * @param criteria SpsMCompanyDensoDomain
     * @param transfer TransferChangeDoDataFromCigmaDomain
     * @param transferDetail TransferChangeDoDetailDataFromCigmaDomain
     * @param mailFlag flag
     * @param errorType type
     * @throws ApplicationException exception
     */
    private void insertCigmaChgDoError( Locale locale,
        SpsMCompanyDensoDomain criteria,
        TransferChangeDoDataFromCigmaDomain transfer,
        TransferChangeDoDetailDataFromCigmaDomain transferDetail,
        String mailFlag, 
        String errorType
    ) throws ApplicationException {

        try {
            SpsCigmaChgDoErrorDomain errorDo = new SpsCigmaChgDoErrorDomain();
            errorDo.setCigmaDoNo( transfer.getCurrentDelivery().trim() );
            errorDo.setDataType( transfer.getDataType() );
            if( null != transfer.getSCd() ){
                errorDo.setSCd( transfer.getSCd() );
            }else{
                errorDo.setSCd( null );
            }
            errorDo.setIssueDate( new Date(transfer.getIssueDate().getTime()) );
            errorDo.setOriginalCigmaDoNo( transfer.getOriginalDelivery() );
            errorDo.setPrevCigmaDoNo( transfer.getPrevDelivery() );
            
            
            errorDo.setMailFlg( mailFlag );
            errorDo.setErrorTypeFlg( errorType );
            errorDo.setVendorCd( transfer.getVendorCd() );
            errorDo.setDCd( criteria.getDCd() );
            errorDo.setDPcd( transfer.getPlantCode() );
            errorDo.setDeliveryDate( 
                parseToSqlDate(transfer.getDeliveryDate(), DateUtil.PATTERN_YYYYMMDD) );
            errorDo.setDeliveryTime( transfer.getDeliveryTime() );
            errorDo.setWh( transfer.getWhPrimeReceiving() );
            errorDo.setDockCode( transfer.getReceivingDock() );
            errorDo.setUpdateByUserId(transfer.getUpdateByUserId().trim());

            if (null != transferDetail) {
                errorDo.setDPn( transferDetail.getPartNo() );
                errorDo.setCurrentOrderQty(toBigDecimal(transferDetail.getCurrentOrderQty()));
                errorDo.setOriginalOrderQty(toBigDecimal(transferDetail.getOriginalOrderQty()));
                errorDo.setSPcd(transferDetail.getSPcd());
            }
            
            spsCigmaChgDoErrorService.create(errorDo);
            
        } catch (Exception e) {
            String parnNo = EMPTY_STRING;
            String orderQty = EMPTY_STRING;
            
            if (null != transferDetail) {
                parnNo = transferDetail.getPartNo();
                orderQty = transferDetail.getCurrentOrderQty();
            }
            
            throwsErrorMessage(locale, 
                ERROR_CD_SP_E6_0025, Constants.CHG_DO,
                transfer.getCurrentDelivery(), parnNo, orderQty,
                TABLE_CIGMA_CHG_DO_NAME
            );    
        }
    }
    
    /**
     * Delete error record in SPS_CIGMA_CHG_DO_ERROR.
     * @param dCd - DENSO company code
     * @param locale - for get message
     * @throws Exception - when catch exception
     * */
    private void deleteChgDoError(String dCd, Locale locale)
        throws Exception
    {
        SpsCigmaChgDoErrorCriteriaDomain cigmaErrorCriteria = 
            new SpsCigmaChgDoErrorCriteriaDomain();
        cigmaErrorCriteria.setDCd(dCd);
        try {
            spsCigmaChgDoErrorService.deleteByCondition( cigmaErrorCriteria );
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0026, 
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                TABLE_CIGMA_CHG_DO_NAME );
        }
    }
    
    /**
     * 
     * <p>Fill D/O Error Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param domain domain
     * @throws Exception exception
     */
    private void fillDoErrorMail(Locale locale, StringBuffer sbContent, 
        TransferDoErrorEmailDomain domain) throws Exception
    {
        if (ERROR_TYPE_VENDOR_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND));
        } else if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND)
                .replaceAll(SupplierPortalConstant.REPLACE_TRANSFER_PROCESS, Constants.DO));
        } else if (ERROR_TYPE_DO_DUP.equals(domain.getErrorTypeFlg())) {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_DO_DUPLICATE));
        } else if (ERROR_TYPE_PO_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_PO_NOTFOUND));
        } else if (ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(
            domain.getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale
                , MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE)
                .replaceAll("#transferProcess#", Constants.DO));
        }
        
        else if(ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND.equals(domain.getErrorTypeFlg())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND));
        }
        else if(ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND));
        }
        else if(ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST.equals(domain.getErrorTypeFlg())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTFOUND));
        }
        else if(ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN.equals(domain.getErrorTypeFlg())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTMATCH_ASN));
        }

        // #vendorCd# #sCd# #dCd# #issueDate#
        sbContent.append(
            getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER3)
                .replaceAll(MAIL_VENDOR_CD_REPLACEMENT, domain.getVendorCd())
                .replaceAll(MAIL_SCD_REPLACEMENT, domain.getSCd()) 
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDCd())
                .replaceAll(MAIL_ISSUE_DATE_REPLACEMENT, 
                    format(domain.getIssueDate(), PATTERN_YYYYMMDD_SLASH)));
        
        // [IN055] Change header column case Part not found
        //sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER_TABLE3));
        // [IN068] apply to Supplier Info Not found and Effective Date Not Match
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
        if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())
            || ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(domain.getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale
                , MAIL_CIGMA_DO_ERROR_HEADER_TABLE3_PRT_NOT_FOUND));
        } else {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER_TABLE3));
        }
        
    } // end fillDoErrorMail.
    
    /**
     * 
     * <p>Fill Chg D/O Error Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param domain domain
     * @param asnList List of String
     * @throws Exception exception
     */
    private void fillChgDoErrorMail(Locale locale, StringBuffer sbContent, 
        TransferChgDoErrorEmailDomain domain, List<String> asnList) throws Exception {
        if( ERROR_TYPE_DO_ALREADY_CREATE_ASN.equals(domain.getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER2));
            
            sbContent.append( appendsString(
                //getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE1)
                //, SYMBOL_NBSP
                getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_DO_ALREADY_CREATE_ASN)
                , SYMBOL_NEWLINE));
            
            sbContent.append( appendsString( 
                getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER3)
                    .replaceAll(MAIL_CIGMA_DO_NO_REPLACEMENT
                        , domain.getCigmaDoNo()) 
                    .replaceAll(MAIL_ASN_NO_LIST_REPLACEMENT, asnList.toString() )
                    .replaceAll(MAIL_DELIVERY_DATE_REPLACEMENT, 
                        format(domain.getDeliveryDate()
                            , PATTERN_YYYYMMDD_SLASH))
                    .replaceAll(MAIL_DELIVERY_TIME_REPLACEMENT, appendsString( 
                        domain.getDeliveryTime().substring(ZERO, Constants.TWO)
                        , Constants.STR_COLON
                        , domain.getDeliveryTime().substring(Constants.TWO
                            , Constants.FOUR) ) )
                , SYMBOL_NEWLINE) );
            
            sbContent.append( appendsString(
                getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE2)
                , getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE3)
                , getEmailLabel(locale, MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE4)));
            
        } else {
            if (ERROR_TYPE_VENDOR_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND));
            } else if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND)
                    .replaceAll(SupplierPortalConstant.REPLACE_TRANSFER_PROCESS, Constants.CHG_DO));
            } else if (ERROR_TYPE_DO_ORIGINAL_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale,
                    MAIL_CIGMA_ERROR_HEADER_DO_ORIGINAL_NOTFOUND));
            } else if (ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale
                    , MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE)
                    .replaceAll("#transferProcess#", Constants.CHG_DO));
            } else if (ERROR_TYPE_PREVIOUS_REV_ERROR.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_PREVIOUS_REV_ERRO));
            }
            else if (ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND));
            }
            else if (ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND));
            }
            else if (ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND));
            }
            else if (ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN.equals(domain.getErrorTypeFlg())) {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN));
            }
            
            
            // #vendorCd# #sCd# #dCd# #issueDate#
            sbContent.append(
                getEmailLabel(locale, MAIL_CIGMA_CHG_DO_2_ERROR_HEADER3)
                    .replaceAll(MAIL_VENDOR_CD_REPLACEMENT
                        , domain.getVendorCd())
                    .replaceAll(MAIL_SCD_REPLACEMENT, domain.getSCd()) 
                    .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDCd())
                    .replaceAll(MAIL_ISSUE_DATE_REPLACEMENT,
                        format(domain.getIssueDate()
                            , PATTERN_YYYYMMDD_SLASH)));
            
            // [IN055] Change header column case Part not found
            //sbContent.append(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3));
            // [IN068] apply to Supplier Info Not found and Effective Date Not Match
            //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())) {
            if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg())
                || ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(domain.getErrorTypeFlg()))
            {
                sbContent.append(getEmailLabel(locale
                    , MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3_PRT_NOT_FOUND));
            } else {
                sbContent.append(getEmailLabel(locale, MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3));
            }
            
        }
    }
    
    /**
     * 
     * <p>Fill Urgent Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param domain domain
     * @throws Exception exception
     */
    private void fillUrgentMail(Locale locale, StringBuffer sbContent, 
        DeliveryOrderDomain domain) throws Exception
    {
        String deliveryDate = format(domain.getDoHeader().getDeliveryDatetime()
            , PATTERN_YYYYMMDD_SLASH);
        String deliveryTime = format(domain.getDoHeader().getDeliveryDatetime()
            , PATTERN_HHMM_COLON);

        /* D.CD, D.P.CD, Route, Del
         * SPS D/O No, Rev, Delivery Date, Delivery Time, Issue Date
         * */
        sbContent.append(
            getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER4)
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDoHeader().getDCd())
                .replaceAll(MAIL_DPCD_REPLACEMENT, domain.getDoHeader().getDPcd())
                .replaceAll(MAIL_TRUCK_ROUTE_REPLACEMENT, domain.getDoHeader().getTruckRoute())
                .replaceAll(MAIL_TRUCK_SEQ_REPLACEMENT
                    , domain.getDoHeader().getTruckSeq().toString())
                .replaceAll(MAIL_ORDER_TYPE_REPLACEMENT
                    , domain.getDoHeader().getDataType())    
                .replaceAll(MAIL_SPS_DO_NO_REPLACEMENT, domain.getDoHeader().getSpsDoNo())
                .replaceAll(MAIL_REV_NO_REPLACEMENT, domain.getDoHeader().getRevision())
                .replaceAll(MAIL_DELIVERY_DATE_REPLACEMENT, deliveryDate)
                .replaceAll(MAIL_DELIVERY_TIME_REPLACEMENT, appendsString(
                    deliveryTime
                    , Constants.SYMBOL_SPACE
                    , Constants.SYMBOL_OPEN_BRACKET
                    , Constants.WORD_UTC, domain.getCompany().getUtc().trim()
                    , Constants.SYMBOL_CLOSE_BRACKET))
                .replaceAll(MAIL_DO_ISSUE_DATE, 
                    format(domain.getDoHeader().getDoIssueDate(), PATTERN_YYYYMMDD_SLASH))
        );
        
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER_TABLE1));
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER_TABLE2));
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER_TABLE3));
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER_TABLE4));
    }

    /**
     * 
     * <p>call send email method.</p>
     *
     * @param locale locale
     * @param title String
     * @param content Stringn
     * @param emailFrom String
     * @param emailTo List of mail
     * @param urgent urgent
     * @param companyCd company code
     * @param log to output error message
     * @throws Exception exception
     */
    private void sendMail(Locale locale, String title, String content, String emailFrom, 
        List<String> emailTo, boolean urgent, String companyCd, Log log) throws Exception
    {
        SendEmailDomain email = new SendEmailDomain();
        email.setHeader( title );
        email.setEmailSmtp(ContextParams.getEmailSmtp());
        email.setContents( content/*.substring( content.indexOf("Dear All") )*/ );
        email.setEmailFrom( emailFrom );

        // Start : [IN069] loop send email for each email in to list
        //email.setToList( emailTo );
        //boolean sendMailResult = mailService.sendEmail(email);
        boolean sendMailResult = true;
        for (String receiver : emailTo) {
            email.setEmailTo(receiver);
            if (!mailService.sendEmail(email)) {
                sendMailResult = false;
            }
        }
        // End : [IN069] loop send email for each email in to list
        
        if(!sendMailResult){
            if( urgent ) {
                throwsErrorMessage(locale, ERROR_CD_SP_E5_0001);
            } else {
                // If cannot send email, not throws Exception
                //throwsErrorMessage(locale, ERROR_CD_SP_E5_0002);
                MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                    , new String[] {companyCd}
                    , log, TWO, false);
            }
        }
    }
    
    /**
     * 
     * <p>convert Email.</p>
     *
     * @param userList List of SpsMUserDomain
     * @return List of String
     */
    private List<String> convertEmail(List<SpsMUserDomain> userList){
        List<String> resultList = new ArrayList<String>(userList.size());
        for(SpsMUserDomain user : userList){
            if( !resultList.contains( user.getEmail() ) ){
                resultList.add( user.getEmail() );
            }
        }
        return resultList;
    }
    
    /**
     * 
     * <p>Merge List.</p>
     *
     * @param dataList List of String
     * @param newList List of String
     */
    private void mergeList(List<String> dataList, List<DoCreatedAsnDomain> newList){
        for(DoCreatedAsnDomain data : newList ){
            if (null == data.getSpsTAsnDomain().getAsnNo()) {
                continue;
            }
            if( !dataList.contains(data.getSpsTAsnDomain().getAsnNo().trim()) ){
                dataList.add(data.getSpsTAsnDomain().getAsnNo().trim());
            }
        }
    }
    
    /**
     * 
     * <p>Convert To Cigma D/O.</p>
     *
     * @param doList List of PseudoCigmaDoInformationDomain
     * @return List of TransferDoDataFromCigmaDomain
     */
    private List<TransferDoDataFromCigmaDomain> convertToCigmaDo(
        List<PseudoCigmaDoInformationDomain> doList)
    {
        List<TransferDoDataFromCigmaDomain> resultList =
            new ArrayList<TransferDoDataFromCigmaDomain>();
        TransferDoDataFromCigmaDomain transfer = null;
        TransferDoDetailDataFromCigmaDomain detail = null;
        TransferDoKanbanlDataFromCigmaDomain kanban = null;
//        Long maxCreateDetailDateTime = null;
//        Long maxUpdateDetailDateTime = null;
//        Long tmpCreateDetailDateTime = null;
//        Long tmpUpdateDetailDateTime = null;
        
        for(PseudoCigmaDoInformationDomain pseudo : doList){
            if( null == transfer 
                || !pseudo.getPseudoDoNumber().equals(transfer.getCigmaDoNo())
                || !pseudo.getPseudoPlantCode().equals(transfer.getPlantCode())
                || !pseudo.getPseudoDataType().equals(transfer.getDataType()) 
            ) {
                
//                if( null != transfer ){
//                    transfer.setMaxCreateDateTime( maxCreateDetailDateTime.toString() );
//                    transfer.setMaxUpdateDateTime( maxCreateDetailDateTime.toString() );
//                }
                
                transfer = new TransferDoDataFromCigmaDomain();
                transfer.setDataType( pseudo.getPseudoDataType() ); // Data Type
                transfer.setCompanyName( pseudo.getPseudoCompanyName() ); // Company Name
                transfer.setCompanyAddress( pseudo.getPseudoCompanyAddress() ); // Company Address
                transfer.setTruckRoute( pseudo.getPseudoTruckRoute() ); // TRUCK ROUTE #
                transfer.setTruckSeq( pseudo.getPseudoTruckSeq() ); // TRUCK SEQ #
                transfer.setVendorCd( pseudo.getPseudoHeaderVendorCd() ); // Supplier Code
                transfer.setSupplierName( pseudo.getPseudoSupplierName() ); // Supplier Name
                transfer.setSupplierLocation( 
                    pseudo.getPseudoSupplierLocation() ); // Supplier Location
                transfer.setIssueDate( 
                    parseToSqlDate(
                        pseudo.getPseudoIssuedDate(), PATTERN_YYYYMMDD) ); // Issued Date
                transfer.setCigmaDoNo( pseudo.getPseudoDoNumber() ); // D/O Number
                transfer.setShipDate( pseudo.getPseudoShipDate() ); // Ship Date
                transfer.setShipTime(
                    padZero(Integer.parseInt(pseudo.getPseudoShipTime()), Constants.FOUR) ); // Ship Time
                transfer.setDeliveryDate( pseudo.getPseudoDeliveryDate() ); // Delivery Date
                transfer.setDeliveryTime( 
                    padZero(Integer.parseInt(pseudo.getPseudoDeliveryTime()), Constants.FOUR) ); // Delivery Time
                transfer.setPlantCode( pseudo.getPseudoPlantCode() ); // Plant Code
                transfer.setWhPrimeReceiving( 
                    pseudo.getPseudoWarehouseForPrimeReceiving() ); // Warehouse for Prime Receiving
//                transfer.setWhLocation( pseudo.getPseudoWerehouseLocation() ); // Werehouse Location
                transfer.setReceivingDock( pseudo.getPseudoReceivingDock() ); // Receiving Dock
                transfer.setReceivingGate( pseudo.getPseudoReceivingGate() ); // Receiving Gate
                transfer.setTm( pseudo.getPseudoTm() ); // TM
                transfer.setCycle( pseudo.getPseudoCycle() ); // Cycle
//                transfer.setControlNo( pseudo.getPseudoControlNo() ); // Control No.
                transfer.setTripNo( pseudo.getPseudoTripNo() ); // Trip No.
                transfer.setBacklog( pseudo.getPseudoBacklog() ); // Backlog
                transfer.setCigmaPoNo( pseudo.getPseudoPoNumber() ); // P/O Number
                transfer.setAddTruckRouteFlag( 
                    pseudo.getPseudoAddTruckRouteFlag() ); // Add Truck Route Flag
                
                
//                tmpCreateDetailDateTime = null;
//                tmpUpdateDetailDateTime = null;
                
                transfer.setCigmaDoDetail(new ArrayList<TransferDoDetailDataFromCigmaDomain>());
                transfer.setUpdateByUserId(pseudo.getPseudoUpdateByUserId());
                transfer.setScanReceiveFlag(pseudo.getPseudoScanReceiveFlag());
                transfer.setTagOutput(pseudo.getPseudoTagOutput());
                detail = new TransferDoDetailDataFromCigmaDomain();
                resultList.add(transfer);
                
//                maxCreateDetailDateTime = Long.parseLong( appendsString(detail.getCreateDate(), 
//                    detail.getCreateTime()) );
//                maxUpdateDetailDateTime = Long.parseLong( appendsString(detail.getUpdateDate(), 
//                    detail.getUpdateTime()) );
            }
            
            if( !pseudo.getPseudoDoPartNumber().equals(detail.getPartNo()) ) {
                detail = new TransferDoDetailDataFromCigmaDomain();
                transfer.getCigmaDoDetail().add(detail);
                detail.setPartNo( pseudo.getPseudoDoPartNumber() ); // Part Number
                detail.setPartName( pseudo.getPseudoPartName().trim() ); // Part Name
                detail.setUnitOfMeasure( pseudo.getPseudoUnitOfMeasure() ); // Unit of Measure
                detail.setLotSize( pseudo.getPseudoLotSize() ); // Lot size
                detail.setNoOfBoxes( pseudo.getPseudoNoOfBoxes() ); // No. of Boxes
                detail.setCurrentOrderQty( pseudo.getPseudoCurrentOrderQty() ); //Current Order Qty.
                detail.setRcvLane( pseudo.getPseudoRcvLane() ); // RCV Lane
                detail.setSpsFlag( pseudo.getPseudoSpsFlag() ); // SPS Flag
                detail.setWhLocation( pseudo.getPseudoWerehouseLocation() ); // Werehouse Location
                detail.setCreateBy( pseudo.getPseudoCreateByUserId() ); // Create by USER ID
                detail.setCreateDate( pseudo.getPseudoCreateDate() ); // Create Date
                detail.setCreateTime( 
                    padZero(Integer.parseInt(pseudo.getPseudoCreateTime()), Constants.SIX) ); // Create Time
                detail.setUpdateBy( pseudo.getPseudoUpdateByUserId() ); // Update by USER ID
                detail.setUpdateDate( pseudo.getPseudoUpdateDate() ); // Update Date
                detail.setUpdateTime( 
                    padZero(Integer.parseInt(pseudo.getPseudoUpdateTime()), Constants.SIX) ); // Update Time
                detail.setControlNo( pseudo.getPseudoControlNo() ); // Control No.
//                tmpCreateDetailDateTime = 
//                    Long.parseLong( appendsString(detail.getCreateDate(), 
//                        detail.getCreateTime()) );
//                if( maxCreateDetailDateTime < tmpCreateDetailDateTime ) {
//                    maxCreateDetailDateTime = tmpCreateDetailDateTime;
//                }
//                tmpUpdateDetailDateTime = 
//                    Long.parseLong( appendsString(detail.getUpdateDate(), 
//                        detail.getUpdateTime()) );
//                if( maxUpdateDetailDateTime < tmpUpdateDetailDateTime ) {
//                    maxUpdateDetailDateTime = tmpUpdateDetailDateTime;
//                }
                
                detail.setCigmaDoKanbanSeq( new ArrayList<TransferDoKanbanlDataFromCigmaDomain>() );
                detail.setKanbanType(pseudo.getPseudoKanbanType());
                detail.setRemark1(pseudo.getPseudoRemark1());
                detail.setRemark2(pseudo.getPseudoRemark2());
                detail.setRemark3(pseudo.getPseudoRemark3());
                detail.setdCustomerPartNo(pseudo.getPseudoDCustomerPartNo());
                detail.setsProcessCode(pseudo.getPseudoSProcessCode());
            }
            
            if (Strings.judgeBlank(pseudo.getPseudoKanbanSequenceNo())) {
                continue;
            }
            
            kanban = new TransferDoKanbanlDataFromCigmaDomain();
            detail.getCigmaDoKanbanSeq().add(kanban);
            kanban.setPlantCode( pseudo.getPseudoSPcd() ); // Plant Cd
            kanban.setVendorCd( pseudo.getPseudoKanbanVendorCd() ); // SUPPLIER CODE
            kanban.setDeliveryRunDate( pseudo.getPseudoDeliveryRunDate() ); // Delivery Run Date
            kanban.setDeliveryRunNo( pseudo.getPseudoDeliveryRunNo() ); // Delivery Run No.
            kanban.setPartNo( pseudo.getPseudoPartNumber() ); // PART NUMBER
            kanban.setKanbanSeqNo( pseudo.getPseudoKanbanSequenceNo() ); // Kanban Sequence No.
            kanban.setKanbanCtrlNo( pseudo.getPseudoKanbanControlNo() ); // Kanban Control No.
            kanban.setKanbanLotSize( pseudo.getPseudoKanbanLotSize() ); // Kanban Lot Size
            kanban.setTempKanbanTag( pseudo.getPseudoTemporaryKanbanTag() ); // Temporary Kanban Tag
            kanban.setCigmaDoNo( pseudo.getPseudoDeliveryOrderNumber() ); // Delivery Order Number
            kanban.setCigmaPoNo( pseudo.getPseudoPurchaseOrderNo() ); // PURCHASE ORDER NO.
            kanban.setDeliveryDueTime( pseudo.getPseudoDeliveryDueTime() ); // Delivery Due Time
            kanban.setDeliveryRunType( pseudo.getPseudoDeliveryRunType() ); // Delivery Run Type
            kanban.setRunScheduleDate( pseudo.getPseudoRunScheduleDate() ); // Run Schedule Date
            kanban.setQrRecvBatchNo( pseudo.getPseudoQrRecvBatchNo() ); // QR Recv Batch No.
            kanban.setQrScanTime( pseudo.getPseudoQrScanTime() ); // QR Scan Time
        }
        
//        if( ZERO < doList.size() ){
//            if( null == transfer.getMaxCreateDateTime() ){
//                transfer.setMaxCreateDateTime( maxCreateDetailDateTime.toString() );
//                transfer.setMaxUpdateDateTime( maxUpdateDetailDateTime.toString() );
//            }
//        }
        return resultList;
    }
    
    /**
     * 
     * <p>Get Next Revision.</p>
     *
     * @param doDetailList List of doDetailList
     * @param doDetail SpsTDoDetailDomain
     * @return String nextSeq
     */
    private String getNextRevision(List<SpsTDoDetailDomain> doDetailList, 
        SpsTDoDetailDomain doDetail){
        String strRevision = Constants.DEFAULT_REVISION;
        for(SpsTDoDetailDomain tdoDetail : doDetailList){
            if( tdoDetail.getDoId().equals( doDetail.getDoId() ) 
                && tdoDetail.getSPn().equals( doDetail.getSPn() )
                && tdoDetail.getDPn().equals( doDetail.getDPn() ) ){
                strRevision = tdoDetail.getPnRevision();
            }
        }
        return increaseNumber(strRevision, REVISION_FORMAT);
    }
    
    /**
     * 
     * <p>Convert To Cigma Change D/O.</p>
     *
     * @param doList List of PseudoCigmaChangeDoInformationDomain
     * @return List of TransferChangeDoDataFromCigmaDomain
     */
    private List<TransferChangeDoDataFromCigmaDomain> convertToCigmaChangeDo(
        List<PseudoCigmaChangeDoInformationDomain> doList)
    {
        List<TransferChangeDoDataFromCigmaDomain> resultList = 
            new ArrayList<TransferChangeDoDataFromCigmaDomain>();
        TransferChangeDoDataFromCigmaDomain transfer = null;
        TransferChangeDoDetailDataFromCigmaDomain detail = null;
        TransferDoKanbanlDataFromCigmaDomain kanban = null;
//        Long maxCreateDetailDateTime = null;
//        Long maxUpdateDetailDateTime = null;
//        Long tmpCreateDetailDateTime = null;
//        Long tmpUpdateDetailDateTime = null;
        
        for(PseudoCigmaChangeDoInformationDomain pseudo : doList){
            if( null == transfer 
                || !transfer.getCurrentDelivery().equals(pseudo.getPseudoCurrentDelivery())
                || !transfer.getPlantCode().equals(pseudo.getPseudoPlantCode())
                || !transfer.getDataType().equals(pseudo.getPseudoDataType()))
            {
//                if( null != transfer ){
//                    transfer.setMaxCreateDateTime( maxCreateDetailDateTime.toString() );
//                    transfer.setMaxUpdateDateTime( maxCreateDetailDateTime.toString() );
//                }
                transfer = new TransferChangeDoDataFromCigmaDomain();
                transfer.setDataType( pseudo.getPseudoDataType() ); // Data Type
                transfer.setCompanyName( pseudo.getPseudoCompanyName() ); // Company Name
                transfer.setCompanyAddress( pseudo.getPseudoCompanyAddress() ); // Company Address
                transfer.setTruckRoute( pseudo.getPseudoTruckRoute() ); // TRUCK ROUTE #
                transfer.setTruckSeq( pseudo.getPseudoTruckSeq() ); // TRUCK SEQ #
                transfer.setVendorCd( pseudo.getPseudoHeaderVendorCd() ); // Supplier Code
                transfer.setSupplierName( pseudo.getPseudoSupplierName() ); // Supplier Name
                transfer.setSupplierLocation( 
                    pseudo.getPseudoSupplierLocation() ); // Supplier Location
                transfer.setIssueDate( 
                    parseToSqlDate( 
                        pseudo.getPseudoIssuedDate(), PATTERN_YYYYMMDD) ); // Issued Date
                transfer.setCurrentDelivery( 
                    pseudo.getPseudoCurrentDelivery() ); // CURRENT DELIVERY #
                transfer.setShipDate( pseudo.getPseudoShipDate() ); // Ship Date
                transfer.setShipTime( 
                    padZero(
                        Integer.parseInt(
                            pseudo.getPseudoShipTime()), Constants.FOUR) ); // Ship Time
                transfer.setDeliveryDate( pseudo.getPseudoDeliveryDate() ); // Delivery Date
                transfer.setDeliveryTime( 
                    padZero(
                        Integer.parseInt(
                            pseudo.getPseudoDeliveryTime()), Constants.FOUR) ); // Delivery Time
                transfer.setPlantCode( pseudo.getPseudoPlantCode() ); // Plant Code
                transfer.setWhPrimeReceiving( 
                    pseudo.getPseudoWarehouseForPrimeReceiving() ); // Warehouse for Prime Receiving
                transfer.setOriginalDelivery( 
                    pseudo.getPseudoOriginalDelivery() ); // ORIGINAL DELIVERY #
                transfer.setReceivingDock( pseudo.getPseudoReceivingDock() ); // Receiving Dock
                transfer.setReceivingGate( pseudo.getPseudoReceivingGate() ); // Receiving Gate
                transfer.setTm( pseudo.getPseudoTm() ); // TM
                transfer.setCycle( pseudo.getPseudoCycle() ); // Cycle
//                transfer.setControlNo( pseudo.getPseudoControlNo() ); // Control No.
                transfer.setPrevDelivery( pseudo.getPseudoPrevDelivery() ); // PREV DELIVERY # 
                transfer.setUrgentFlag( pseudo.getPseudoUrgentFlag() ); // Urgent Flag
                
//                tmpCreateDetailDateTime = null;
//                tmpUpdateDetailDateTime = null;
                
                transfer
                    .setCigmaDoDetail( new ArrayList<TransferChangeDoDetailDataFromCigmaDomain>());
                transfer.setUpdateByUserId(pseudo.getPseudoUpdateByUserId());
                transfer.setScanReceiveFlag(pseudo.getPseudoScanReceiveFlag());
                transfer.setTagOutput(pseudo.getPseudoTagOutput());
                detail = new TransferChangeDoDetailDataFromCigmaDomain();
                resultList.add(transfer);
                
//                maxCreateDetailDateTime = Long.parseLong( appendsString(detail.getCreateDate(), 
//                    detail.getCreateTime()) );
//                maxUpdateDetailDateTime = Long.parseLong( appendsString(detail.getUpdateDate(), 
//                    detail.getUpdateTime()) );
            }
            
            if( !pseudo.getPseudoDoPartNumber().equals(detail.getPartNo()) ) {
                detail = new TransferChangeDoDetailDataFromCigmaDomain();
                transfer.getCigmaDoDetail().add(detail);
                detail.setPartNo( pseudo.getPseudoDoPartNumber() ); // Part Number
                detail.setLotSize( pseudo.getPseudoLotSize() ); // Lot size
                detail.setNoOfBoxes( pseudo.getPseudoNoOfBoxes() ); // No. of Boxes
                detail.setCurrentOrderQty( pseudo.getPseudoCurrentOrderQty() ); // CURRENT ORDER QTY
                detail.setOriginalOrderQty( 
                    pseudo.getPseudoOriginalOrderQty() ); // ORIGINAL ORDER QTY
                detail.setUnitOfMeasure( pseudo.getPseudoUnitOfMeasure() ); // Unit of Measure
                detail.setReasonForChange( pseudo.getPseudoReasonForChange() ); // REASON FOR CHANGE
                detail.setItemDescription( pseudo.getPseudoItemDescription() ); // Item Description
                detail.setModel( 
                    pseudo.getPseudoEngineerDrawingNoModel() ); // Engineer Drawing No(Model)
                detail.setSpsFlag( pseudo.getPseudoSpsFlag() ); // SPS Flag
                detail.setControlNo( pseudo.getPseudoControlNo() ); // Control No.
                detail.setCreateBy( pseudo.getPseudoCreateByUserId() ); // Create by USER ID
                detail.setCreateDate( pseudo.getPseudoCreateDate() ); // Create Date
                detail.setCreateTime( 
                    padZero(
                        Integer.parseInt(
                            pseudo.getPseudoCreateTime()), Constants.SIX) ); // Create Time
                detail.setUpdateBy( pseudo.getPseudoUpdateByUserId() ); // Update by USER ID
                detail.setUpdateDate( pseudo.getPseudoUpdateDate() ); // Update Date
                detail.setUpdateTime( 
                    padZero(
                        Integer.parseInt(
                            pseudo.getPseudoUpdateTime()), Constants.SIX) ); // Update Time
                
                detail.setRcvLane(Constants.EMPTY_STRING);
                detail.setWhLocation(Constants.EMPTY_STRING);
                if (null != pseudo.getPseudoValue1()) {
                    if (pseudo.getPseudoValue1().length() < Constants.TWENTY) {
                        detail.setWhLocation(pseudo.getPseudoValue1().substring(ZERO).trim());
                    } else if (Constants.TWENTY <= pseudo.getPseudoValue1().length()) {
                        detail.setWhLocation(pseudo.getPseudoValue1().substring(
                            ZERO, Constants.TWENTY).trim());
                    }
                    if (Constants.TWENTY < pseudo.getPseudoValue1().length()) {
                        detail.setRcvLane(
                            pseudo.getPseudoValue1().substring(Constants.TWENTY).trim());
                    }
                }
                
//                tmpCreateDetailDateTime = 
//                    Long.parseLong( appendsString(detail.getCreateDate(), 
//                        detail.getCreateTime()) );
//                if( maxCreateDetailDateTime < tmpCreateDetailDateTime ) {
//                    maxCreateDetailDateTime = tmpCreateDetailDateTime;
//                }
//                tmpUpdateDetailDateTime = 
//                    Long.parseLong( appendsString(detail.getUpdateDate(), 
//                        detail.getUpdateTime()) );
//                if( maxUpdateDetailDateTime < tmpUpdateDetailDateTime ) {
//                    maxUpdateDetailDateTime = tmpUpdateDetailDateTime;
//                }
                
                detail.setCigmaDoKanbanSeq( new ArrayList<TransferDoKanbanlDataFromCigmaDomain>() );
                detail.setKanbanType(pseudo.getPseudoKanbanType());
                detail.setRemark1(pseudo.getPseudoRemark1());
                detail.setRemark2(pseudo.getPseudoRemark2());
                detail.setRemark3(pseudo.getPseudoRemark3());
                detail.setdCustomerPartNo(pseudo.getPseudoDCustomerPartNo());
                detail.setsProcessCode(pseudo.getPseudoSProcessCode());
            }

            if (Strings.judgeBlank(pseudo.getPseudoKanbanSequenceNo())) {
                continue;
            }
            
            kanban = new TransferDoKanbanlDataFromCigmaDomain();
            detail.getCigmaDoKanbanSeq().add(kanban);
            kanban.setPlantCode( pseudo.getPseudoPlantCd() ); // Plant Cd
            kanban.setVendorCd( pseudo.getPseudoKanbanVendorCd() ); // SUPPLIER CODE
            kanban.setDeliveryRunDate( pseudo.getPseudoDeliveryRunDate() ); // Delivery Run Date
            kanban.setDeliveryRunNo( pseudo.getPseudoDeliveryRunNo() ); // Delivery Run No.
            kanban.setPartNo( pseudo.getPseudoPartNumber() ); // PART NUMBER
            kanban.setKanbanSeqNo( pseudo.getPseudoKanbanSequenceNo() ); // Kanban Sequence No.
            kanban.setKanbanCtrlNo( pseudo.getPseudoKanbanControlNo() ); // Kanban Control No.
            kanban.setKanbanLotSize( pseudo.getPseudoKanbanLotSize() ); // Kanban Lot Size
            kanban.setTempKanbanTag( 
                pseudo.getPseudoTemporaryKanbanTag() ); // Temporary Kanban Tag
            kanban.setCigmaDoNo( 
                pseudo.getPseudoDeliveryOrderNumber() ); // Delivery Order Number
            kanban.setCigmaPoNo( pseudo.getPseudoPurchaseOrderNo() ); // PURCHASE ORDER NO.
            kanban.setDeliveryDueTime( pseudo.getPseudoDeliveryDueTime() ); // Delivery Due Time
            kanban.setDeliveryRunType( pseudo.getPseudoDeliveryRunType() ); // Delivery Run Type
            kanban.setRunScheduleDate( pseudo.getPseudoRunScheduleDate() ); // Run Schedule Date
            kanban.setQrRecvBatchNo( pseudo.getPseudoQrRecvBatchNo() ); // QR Recv Batch No.
            kanban.setQrScanTime( pseudo.getPseudoQrScanTime() ); // QR Scan Time
                
        }
        
//        if( ZERO < doList.size() ){
//            if( null == transfer.getMaxCreateDateTime() ){
//                transfer.setMaxCreateDateTime( maxCreateDetailDateTime.toString() );
//                transfer.setMaxUpdateDateTime( maxUpdateDetailDateTime.toString() );
//            }
//        }
        
        return resultList;
        
    }
    
    /**
     * 
     * <p>insert TransferDoData.</p>
     *
     * @param locale locale
     * @param criteria criteria
     * @param transfer transferData
     * @return DO ID
     * @throws ApplicationException exception
     */
    private BigDecimal insertDo(Locale locale, SpsMCompanyDensoDomain criteria
        , TransferDoDataFromCigmaDomain transfer ) throws ApplicationException{
        BigDecimal doId = null;
        try {
            // 2-1-1) For the same cigmaDONo, supplierCode, supplierPlantCode, dataType insert data from DeliveryOrderService
            SpsTDoDomain doDomain = new SpsTDoDomain();
            
            String prefixSpsDo = transfer.getSPcd().substring(ZERO, ONE);
            doDomain.setSpsDoNo( appendsString(prefixSpsDo, transfer.getCigmaDoNo()));
            doDomain.setCigmaDoNo( transfer.getCigmaDoNo() );
            doDomain.setCurrentSpsDoNo(appendsString(
                prefixSpsDo, transfer.getCigmaDoNo(), Constants.DEFAULT_REVISION));
            doDomain.setCurrentCigmaDoNo(appendsString(
                transfer.getCigmaDoNo(), Constants.DEFAULT_REVISION));
            
            doDomain.setRevision( Constants.DEFAULT_REVISION );
            doDomain.setDoIssueDate( new Date(transfer.getIssueDate().getTime()) );
            doDomain.setShipmentStatus( Constants.ISS_TYPE );
            doDomain.setVendorCd( transfer.getVendorCd() );
            doDomain.setSCd( transfer.getSCd() );
            doDomain.setSPcd( transfer.getSPcd() );
            doDomain.setDCd( criteria.getDCd() );
            doDomain.setDPcd( transfer.getPlantCode() );
            if(Strings.judgeBlank(transfer.getTm())){
                doDomain.setTm(Constants.SYMBOL_SPACE);
            }else{
                doDomain.setTm(transfer.getTm());
            }
            doDomain.setDataType( transfer.getDataType() );
            if( DDO_TYPE.equals(transfer.getDataType()) ){
                doDomain.setOrderMethod( STR_ZERO );
            } else {
                doDomain.setOrderMethod( STR_ONE );
            }
            
            Timestamp createDate = parseToTimestamp( 
                appendsString(
                    transfer.getShipDate(), 
                    transfer.getShipTime() )
                , PATTERN_YYYYMMDD_HHMM );
            doDomain.setShipDatetime( createDate );
                
            createDate = parseToTimestamp( 
                appendsString(
                    transfer.getDeliveryDate(), 
                    transfer.getDeliveryTime() )
                , PATTERN_YYYYMMDD_HHMM );
            doDomain.setDeliveryDatetime( createDate );
            
            doDomain.setPdfFileId( null );
            doDomain.setTruckRoute( transfer.getTruckRoute() );
            doDomain.setTruckSeq( toBigDecimal(transfer.getTruckSeq()) );
            doDomain.setPoId( transfer.getPoId() );
            doDomain.setWhPrimeReceiving( transfer.getWhPrimeReceiving() );
//            doDomain.setWhLocation( transfer.getWhLocation() );
            doDomain.setLatestRevisionFlg( STR_ONE );
            if( STR_Y.equals( transfer.getAddTruckRouteFlag() ) ){
                doDomain.setUrgentOrderFlg( STR_ONE );
            }else{
                doDomain.setUrgentOrderFlg( STR_ZERO );
            }
            doDomain.setClearUrgentDate( DateUtil.addDays(doDomain.getDoIssueDate(), THREE) );
            if( STR_Y.equals( transfer.getAddTruckRouteFlag() ) ){
                doDomain.setNewTruckRouteFlg( STR_ONE );
            }else{
                doDomain.setNewTruckRouteFlg( STR_ZERO );
            }
            
            doDomain.setDockCode( transfer.getReceivingDock() );
            doDomain.setCycle( transfer.getCycle() );
            doDomain.setTripNo( toBigDecimal(transfer.getTripNo()) );
            doDomain.setBacklog( toBigDecimal(transfer.getBacklog()) );
            doDomain.setReceivingGate( transfer.getReceivingGate() );
            doDomain.setSupplierLocation( transfer.getSupplierLocation() );
            
            String createBy = null;
            String updateBy = null;
            Timestamp bufferMaxCreate = null;
            Timestamp bufferMaxUpdate = null;
            Timestamp bufferCreate = null;
            Timestamp bufferUpdate = null;
            for(TransferDoDetailDataFromCigmaDomain detail : transfer.getCigmaDoDetail() ){
                bufferCreate = parseToTimestamp( 
                    appendsString(
                        detail.getCreateDate(), 
                        detail.getCreateTime())
                    , PATTERN_YYYYMMDD_HHMMSS );
                bufferUpdate = parseToTimestamp( 
                    appendsString(
                        detail.getUpdateDate(), 
                        detail.getUpdateTime())
                    , PATTERN_YYYYMMDD_HHMMSS );
                
                if( null == bufferMaxCreate ) {
                    bufferMaxCreate = bufferCreate;
                    createBy = detail.getCreateBy();
                }
                if( null == bufferMaxUpdate ) {
                    bufferMaxUpdate = bufferUpdate;
                    updateBy = detail.getUpdateBy();
                }
                
                if( null != bufferMaxCreate && bufferMaxCreate.before( bufferCreate ) ){
                    bufferMaxCreate = bufferCreate;
                    createBy = detail.getCreateBy();
                }
                if( null != bufferMaxUpdate && bufferMaxUpdate.before( bufferUpdate ) ){
                    bufferMaxUpdate = bufferUpdate;
                    updateBy = detail.getUpdateBy();
                }
            }
            
            doDomain.setCreateDscId( createBy );
            doDomain.setCreateDatetime( bufferMaxCreate );
            doDomain.setLastUpdateDscId( updateBy );
            doDomain.setLastUpdateDatetime( commonService.searchSysDate() );
            doDomain.setReceiveByScan(transfer.getScanReceiveFlag());
            doDomain.setPdfFileKbTagPrintDate(null);
            doDomain.setOneWayKanbanTagFlag(Constants.STR_ZERO);
            doDomain.setNotificationMailDoFlag(Constants.STR_ZERO);
            doDomain.setNotificationMailKanbanFlag(Constants.STR_ZERO);
            doDomain.setTagOutput(transfer.getTagOutput());
            
            doId =  deliveryOrderService.createDeliveryOrder( doDomain );
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0022, 
                transfer.getCigmaDoNo(),
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER) );
        }
        
        return doId;
    }
    
    /**
     * 
     * <p>insert TransferDoDetail.</p>
     *
     * @param locale locale
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param doId DoId
     * @throws ApplicationException exception
     */
    private void insertDetail(Locale locale, TransferDoDataFromCigmaDomain transfer
        , TransferDoDetailDataFromCigmaDomain transferDetail, BigDecimal doId
    ) throws ApplicationException{
        Timestamp current = commonService.searchSysDate();
        try {
            SpsTDoDetailDomain doDetailDomain = new SpsTDoDetailDomain();
            doDetailDomain.setDoId( doId );
            doDetailDomain.setSPn( transferDetail.getSPn() );
            doDetailDomain.setDPn( transferDetail.getPartNo() );
            doDetailDomain.setPnRevision( DEFAULT_REVISION );
            doDetailDomain.setPnShipmentStatus( Constants.ISS_TYPE );
            doDetailDomain.setChgCigmaDoNo( null );
            doDetailDomain.setCtrlNo( transfer.getControlNo() );
            doDetailDomain.setCurrentOrderQty( 
                toBigDecimal(transferDetail.getCurrentOrderQty()) );
            doDetailDomain.setQtyBox( toBigDecimal(transferDetail.getLotSize() ) );
            doDetailDomain.setChgReason( null );
            doDetailDomain.setItemDesc( transferDetail.getPartName() );
            doDetailDomain.setUnitOfMeasure( transferDetail.getUnitOfMeasure() );
//            doDetailDomain.setModel( transferDetail.getModel() );
            doDetailDomain.setOriginalQty( toBigDecimal(transferDetail.getCurrentOrderQty() ) );
            doDetailDomain.setPreviousQty( toBigDecimal(STR_ZERO) );
            doDetailDomain.setNoOfBoxes( toBigDecimal(transferDetail.getNoOfBoxes()) );
            if(StringUtil.checkNullOrEmpty(transferDetail.getRcvLane())){
                doDetailDomain.setRcvLane( Constants.SYMBOL_SPACE );
            }else{
                doDetailDomain.setRcvLane( transferDetail.getRcvLane() );
            }
            doDetailDomain.setWhLocation( transferDetail.getWhLocation() );
            if( STR_Y.equals(transfer.getAddTruckRouteFlag()) ){
                doDetailDomain.setUrgentOrderFlg( STR_ONE );
            }else{
                doDetailDomain.setUrgentOrderFlg( STR_ZERO );
            }
            doDetailDomain.setMailFlg( STR_ZERO );
            
            doDetailDomain.setCreateDscId( transferDetail.getCreateBy() );
            doDetailDomain.setCreateDatetime( 
                parseToTimestamp( 
                    appendsString(
                        transferDetail.getCreateDate(), 
                        padZero(Integer.parseInt(transferDetail.getCreateTime()), Constants.SIX))
                    , PATTERN_YYYYMMDD_HHMMSS ) );
            doDetailDomain.setLastUpdateDscId( transferDetail.getUpdateBy() );
            doDetailDomain.setLastUpdateDatetime( current );
            doDetailDomain.setCtrlNo( transferDetail.getControlNo() );
            doDetailDomain.setKanbanType(transferDetail.getKanbanType());
            doDetailDomain.setRemark1(transferDetail.getRemark1());
            doDetailDomain.setRemark2(transferDetail.getRemark2());
            doDetailDomain.setRemark3(transferDetail.getRemark3());
            doDetailDomain.setcPn(transferDetail.getdCustomerPartNo());
            doDetailDomain.setsProcCd(transferDetail.getsProcessCode());
            
            spsTDoDetailService.create(doDetailDomain);
        
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0022, 
                transfer.getCigmaDoNo(), 
                transferDetail.getPartNo(), 
                getLabelHandledException(locale, TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER_DETAIL) );
        }
    }
    
    /**
     * 
     * <p>insert TransferDoKanban data.</p>
     *
     * @param locale locale
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param doId Do ID
     * @throws ApplicationException exception
     */
    private void insertKanban(Locale locale, TransferDoDataFromCigmaDomain transfer,
        TransferDoDetailDataFromCigmaDomain transferDetail, BigDecimal doId
    ) throws ApplicationException {
        try {
            int kanbanSeq = ONE;
            Timestamp current = commonService.searchSysDate();
            for( TransferDoKanbanlDataFromCigmaDomain transferKanban 
                : transferDetail.getCigmaDoKanbanSeq() ){
                SpsTDoKanbanSeqDomain doKanbanDomain = new SpsTDoKanbanSeqDomain();
                doKanbanDomain.setDoId( doId );
                doKanbanDomain.setDPn( transferDetail.getPartNo() );
                doKanbanDomain.setSPn( transferDetail.getSPn() );
                doKanbanDomain.setKanbanSeqId( toBigDecimal(kanbanSeq++) );
                doKanbanDomain.setDeliveryRunDate( 
                    parseToSqlDate(transferKanban.getDeliveryRunDate(), PATTERN_YYYYMMDD) );
                doKanbanDomain.setDeliveryRunNo( 
                    toBigDecimal( transferKanban.getDeliveryRunNo() ) );
                doKanbanDomain.setKanbanSeqNo( 
                    toBigDecimal(transferKanban.getKanbanSeqNo()) );
                doKanbanDomain.setKanbanCtrlNo( transferKanban.getKanbanCtrlNo() );
                doKanbanDomain.setKanbanLotSize( 
                    toBigDecimal(transferKanban.getKanbanLotSize()) );
                doKanbanDomain.setTempKanbanTag( transferKanban.getTempKanbanTag() );
                doKanbanDomain.setDeliveryDueTime( 
                    toBigDecimal(transferKanban.getDeliveryDueTime()) );
                doKanbanDomain.setDeliveryRunType( transferKanban.getDeliveryRunType() );
                doKanbanDomain.setRunScheduleDate( 
                    parseToSqlDate(transferKanban.getRunScheduleDate(), PATTERN_YYYYMMDD) );
                doKanbanDomain.setQrRcvBatchNo( 
                    toBigDecimal(transferKanban.getQrRecvBatchNo()) );
                doKanbanDomain.setQrScanTime( 
                    toBigDecimal(transferKanban.getQrScanTime()) );
                doKanbanDomain.setCreateDscId( transferDetail.getCreateBy() );
                doKanbanDomain.setCreateDatetime( 
                    parseToTimestamp( 
                        appendsString(
                            transferDetail.getCreateDate(), 
                            padZero(
                                Integer.parseInt(transferDetail.getCreateTime()), Constants.SIX))
                        , PATTERN_YYYYMMDD_HHMMSS ) );
                doKanbanDomain.setLastUpdateDscId( transferDetail.getUpdateBy() );
                doKanbanDomain.setLastUpdateDatetime( current );
                spsTDoKanbanSeqService.create(doKanbanDomain);    
            }
        } catch (Exception e) {
            throwsErrorMessage(locale, 
                ERROR_CD_SP_E6_0022, 
                transfer.getCigmaDoNo(), 
                transferDetail.getPartNo(), 
                getLabelHandledException(locale, TRANSFER_DO_STEP_CREATE_KANBAN_SEQUENCE) );
        }
    }
    
    /**
     * 
     * <p> Transfer D/O From Cigma.</p>
     *
     * @param locale locale
     * @param transferList TransferDoDataFromCigmaDomain
     * @param as400Server serverConfig
     * @param bError error
     * @param jobId jobId
     * @throws Exception exception
     */
    private void transferDoFromCigma(Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        boolean bError, 
        String jobId) throws Exception {
        for(TransferDoDataFromCigmaDomain transfer : transferList) {
            // 2-1) Separate CIGMA D/O to SPS D/O by Supplier Plant Code and Data Type
            BigDecimal doId = null;
    //        for(TransferDoDataFromCigmaDomain transfer : transferList){
            doId = insertDo(locale, as400Server.getSpsMCompanyDensoDomain(), transfer);
            
            // 2-1-2) LOOP-2 until end of <List> cigmaDOInformation. cigmaDODetail
            for(TransferDoDetailDataFromCigmaDomain transferDetail : transfer.getCigmaDoDetail()) {
                // insert TransferDoDetail
                insertDetail(locale, transfer, transferDetail, doId);
                if( !DDO_TYPE.equals(transfer.getDataType()) ){
                    // for each Kanban.
                    insertKanban(locale, transfer, transferDetail, doId);
                }
                if(transferDetail.getKanbanType().equals(Constants.STR_ONE)){
                    
                }
            } // end for detail.
//            2018-04-11 Don't generate BLOB
//            generateDeliveryOrder(
//                locale, 
//                transfer.getCigmaDoNo(), 
//                transfer.getDataType(), 
//                doId,
//                jobId,
//                false);
            
        }

        String partNoError = null;
        try{
            partNoError = CommonWebServiceUtil.updateCigmaDeliveryOrder( as400Server, 
                transferList, STR_ONE, jobId );
            if( !StringUtil.checkNullOrEmpty(partNoError) ){
                if( !bError ){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, STR_ONE, Constants.DO);
                }
            }
        }catch(Exception e){
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, STR_ONE, Constants.DO);
        }
    }
    
    /**
     * 
     * <p>insert TransferDoData.</p>
     *
     * @param locale locale
     * @param transfer transferData
     * @param doOldDomain SpsTDoDomain
     * @return DO ID
     * @throws ApplicationException exception
     */
    private BigDecimal insertChangeDo(Locale locale
        , TransferChangeDoDataFromCigmaDomain transfer
        , SpsTDoDomain doOldDomain) throws ApplicationException
    {
        BigDecimal newDoId = null;
        try {
            SpsTDoDomain doNewDomain = new SpsTDoDomain();
            // [IN007] : SPS_DO_NO generate by current version S_PCD
            String prefixSpsDo = transfer.getSPcd().substring(ZERO, ONE);
            //doNewDomain.setSpsDoNo( doOldDomain.getSpsDoNo() );
            if (!Strings.judgeBlank(transfer.getOriginalDelivery())) {
                doNewDomain.setSpsDoNo(appendsString(
                    prefixSpsDo, transfer.getOriginalDelivery()));
            } else {
                doNewDomain.setSpsDoNo( doOldDomain.getSpsDoNo() );
            }
            
            doNewDomain.setCigmaDoNo( doOldDomain.getCigmaDoNo() );

            // [IN007] : Move to create before generate SPS_DO_NO
            //String prefixSpsDo = transfer.getSPcd().substring(ZERO, ONE);
            if (!Strings.judgeBlank(transfer.getCurrentDelivery())) {
                doNewDomain.setCurrentSpsDoNo(appendsString(
                    prefixSpsDo, transfer.getCurrentDelivery()));
                doNewDomain.setCurrentCigmaDoNo(transfer.getCurrentDelivery());
                doNewDomain.setRevision(transfer.getCurrentDelivery().substring(
                    transfer.getCurrentDelivery().length() - Constants.TWO));
            } else {
                doNewDomain.setRevision(
                    increaseNumber(doOldDomain.getRevision(), REVISION_FORMAT) );
            }
            
            if (!Strings.judgeBlank(transfer.getPrevDelivery())) {
                doNewDomain.setPreviousSpsDoNo(appendsString(
                    prefixSpsDo, transfer.getPrevDelivery()));
                doNewDomain.setPreviousCigmaDoNo(transfer.getPrevDelivery());
            }
            
            doNewDomain.setDoIssueDate( doOldDomain.getDoIssueDate() );
            doNewDomain.setShipmentStatus( doOldDomain.getShipmentStatus() );
            doNewDomain.setVendorCd( doOldDomain.getVendorCd() );
            doNewDomain.setSCd( doOldDomain.getSCd() );
            
            // [IN007] : S_PCD get from Change data (current supplier information)
            //doNewDomain.setSPcd( doOldDomain.getSPcd() );
            doNewDomain.setSPcd( transfer.getSPcd() );
            
            doNewDomain.setDCd( doOldDomain.getDCd() );
            doNewDomain.setDPcd( transfer.getPlantCode() );
            if(Strings.judgeBlank(transfer.getTm())){
                doNewDomain.setTm(Constants.SYMBOL_SPACE);
            }else{
                doNewDomain.setTm(transfer.getTm());
            }
            doNewDomain.setDataType( transfer.getDataType() );
            doNewDomain.setOrderMethod( doOldDomain.getOrderMethod() );
            Timestamp createDate = parseToTimestamp( 
                appendsString(
                    transfer.getShipDate(), 
                    padZero(Integer.parseInt(transfer.getShipTime()), Constants.FOUR)) // SIX to FOUR
                , PATTERN_YYYYMMDD_HHMM );
            if( null == createDate ) {
                doNewDomain.setShipDatetime( doOldDomain.getShipDatetime() );
            }else {
                doNewDomain.setShipDatetime( createDate );
            }
                
            createDate = parseToTimestamp( 
                appendsString(
                    transfer.getDeliveryDate(), 
                    padZero(Integer.parseInt(transfer.getDeliveryTime()), Constants.FOUR)) // SIX to FOUR
                , PATTERN_YYYYMMDD_HHMM );
            doNewDomain.setDeliveryDatetime( createDate );
            doNewDomain.setPdfFileId( null );
            doNewDomain.setTruckRoute( transfer.getTruckRoute() );
            doNewDomain.setTruckSeq( toBigDecimal(transfer.getTruckSeq()) );
            doNewDomain.setPoId( doOldDomain.getPoId() );
            doNewDomain.setWhPrimeReceiving( transfer.getWhPrimeReceiving() );
//            doNewDomain.setWhLocation( doOldDomain.getWhLocation() );
            doNewDomain.setLatestRevisionFlg( STR_ONE );
            //[IN075] KANBANFIX
            //if( DCD_TYPE.equals(transfer.getDataType()) ){
            if( DDO_TYPE.equals(transfer.getDataType()) || KDO_TYPE.equals(transfer.getDataType())){
                doNewDomain.setUrgentOrderFlg( STR_ONE );
            //END [IN075] KANBANFIX    
            }else{
                doNewDomain.setUrgentOrderFlg( STR_ZERO );
            }
            //System.out.println(transfer.getDataType());
            doNewDomain.setClearUrgentDate( 
                DateUtil.addDays(doNewDomain.getDoIssueDate(), THREE) );
            doNewDomain.setNewTruckRouteFlg( doOldDomain.getNewTruckRouteFlg() );
            doNewDomain.setDockCode( transfer.getReceivingDock() );
            doNewDomain.setCycle( transfer.getCycle() );
            doNewDomain.setTripNo( doOldDomain.getTripNo() );
            doNewDomain.setBacklog( doOldDomain.getBacklog() );
            doNewDomain.setReceivingGate( transfer.getReceivingGate() );
            doNewDomain.setSupplierLocation( transfer.getSupplierLocation() );
            
            String createBy = null;
            String updateBy = null;
            Timestamp bufferMaxCreate = null;
            Timestamp bufferMaxUpdate = null;
            Timestamp bufferCreate = null;
            Timestamp bufferUpdate = null;
            for(TransferChangeDoDetailDataFromCigmaDomain detail : transfer.getCigmaDoDetail() ){
                bufferCreate = parseToTimestamp( 
                    appendsString(
                        detail.getCreateDate(), 
                        padZero(Integer.parseInt(detail.getCreateTime()), Constants.SIX))
                    , PATTERN_YYYYMMDD_HHMMSS );
                bufferUpdate = parseToTimestamp( 
                    appendsString(
                        detail.getUpdateDate(), 
                        padZero(Integer.parseInt(detail.getUpdateTime()), Constants.SIX))
                    , PATTERN_YYYYMMDD_HHMMSS );
                
                if( null == bufferMaxCreate ) {
                    bufferMaxCreate = bufferCreate;
                }
                if( null == bufferMaxUpdate ) {
                    bufferMaxUpdate = bufferUpdate;
                }
                
                if( null != bufferMaxCreate 
                    && null != bufferCreate
                    && bufferMaxCreate.before( bufferCreate ) ){
                    bufferMaxCreate = bufferCreate;
                    createBy = detail.getCreateBy();
                }
                if( null != bufferMaxUpdate 
                    && null != bufferUpdate
                    && bufferMaxUpdate.before( bufferUpdate ) ){
                    bufferMaxUpdate = bufferUpdate;
                    updateBy = detail.getUpdateBy();
                }
            }
            if( null != createBy ){
                doNewDomain.setCreateDscId( createBy );
            } else {
                doNewDomain.setCreateDscId( doOldDomain.getCreateDscId() );
            }
            
            if( null != bufferMaxCreate ){
                doNewDomain.setCreateDatetime( bufferMaxCreate );
            } else {
                doNewDomain.setCreateDatetime( doOldDomain.getCreateDatetime() );
            }
            
            if( null != createBy ){
                doNewDomain.setLastUpdateDscId( updateBy );
            } else {
                doNewDomain.setLastUpdateDscId( doOldDomain.getLastUpdateDscId() );
            }
            
            doNewDomain.setLastUpdateDatetime( commonService.searchSysDate() );
            doNewDomain.setReceiveByScan(transfer.getScanReceiveFlag());
            doNewDomain.setPdfFileKbTagPrintDate(null);
            doNewDomain.setOneWayKanbanTagFlag(Constants.STR_ZERO);
            doNewDomain.setNotificationMailDoFlag(Constants.STR_ZERO);
            doNewDomain.setNotificationMailKanbanFlag(Constants.STR_ZERO);
            doNewDomain.setTagOutput( transfer.getTagOutput() );
            
            newDoId = deliveryOrderService.createDeliveryOrder(doNewDomain);
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                transfer.getCurrentDelivery(),
                EMPTY_STRING,
                getLabelHandledException(locale
                    , TRANSFER_DO_STEP_CREATE_DO_IN_NEW_REVISION) );
        }
        return newDoId;
    }
    
    /**
     * 
     * <p>insert Change Detail.</p>
     *
     * @param locale locale
     * @param transfer transferData
     * @param newDoId BigDecimal
     * @param doDetailOldList List of SpsTDoDetailDomain
     * @param doDetailNewList List of SpsTDoDetailDomain
     * @throws ApplicationException exception
     */
    private void insertChangeDetail(Locale locale
        , TransferChangeDoDataFromCigmaDomain transfer
        , BigDecimal newDoId, List<SpsTDoDetailDomain> doDetailOldList
        , List<SpsTDoDetailDomain> doDetailNewList
    ) throws ApplicationException{
        Timestamp current = commonService.searchSysDate();
        try {
            for(SpsTDoDetailDomain doDetailOldDomain : doDetailOldList){
                SpsTDoDetailDomain doDetailNewDomain = new SpsTDoDetailDomain();
                doDetailNewDomain.setDoId( newDoId );
                doDetailNewDomain.setSPn( doDetailOldDomain.getSPn() );
                doDetailNewDomain.setDPn( doDetailOldDomain.getDPn() );
                doDetailNewDomain.setPnRevision( doDetailOldDomain.getPnRevision() );
                doDetailNewDomain.setPnShipmentStatus( 
                    doDetailOldDomain.getPnShipmentStatus() );
                doDetailNewDomain.setChgCigmaDoNo( doDetailOldDomain.getChgCigmaDoNo() );
                doDetailNewDomain.setCtrlNo( doDetailOldDomain.getCtrlNo() );
                doDetailNewDomain.setCurrentOrderQty( doDetailOldDomain.getCurrentOrderQty() );
                doDetailNewDomain.setQtyBox( doDetailOldDomain.getQtyBox() );
                doDetailNewDomain.setChgReason( doDetailOldDomain.getChgReason() );
                doDetailNewDomain.setItemDesc( doDetailOldDomain.getItemDesc() );
                doDetailNewDomain.setUnitOfMeasure( doDetailOldDomain.getUnitOfMeasure() );
                doDetailNewDomain.setModel( doDetailOldDomain.getModel() );
                doDetailNewDomain.setOriginalQty( doDetailOldDomain.getOriginalQty() );
                doDetailNewDomain.setPreviousQty( doDetailOldDomain.getCurrentOrderQty() );
                doDetailNewDomain.setNoOfBoxes( doDetailOldDomain.getNoOfBoxes() );
                if(StringUtil.checkNullOrEmpty(doDetailOldDomain.getRcvLane())){
                    doDetailNewDomain.setRcvLane( Constants.SYMBOL_SPACE );
                }else{
                    doDetailNewDomain.setRcvLane( doDetailOldDomain.getRcvLane() );
                }
                doDetailNewDomain.setUrgentOrderFlg( doDetailOldDomain.getUrgentOrderFlg() );
                doDetailNewDomain.setMailFlg( doDetailOldDomain.getMailFlg() );
                doDetailNewDomain.setCreateDscId( doDetailOldDomain.getCreateDscId() );
                doDetailNewDomain.setCreateDatetime( doDetailOldDomain.getCreateDatetime() );
                doDetailNewDomain.setLastUpdateDscId( doDetailOldDomain.getLastUpdateDscId() );
                doDetailNewDomain.setWhLocation( doDetailOldDomain.getWhLocation() );
//                doDetailNewDomain.setLastUpdateDatetime( 
//                    doDetailOldDomain.getLastUpdateDatetime() );
                doDetailNewDomain.setLastUpdateDatetime( current );
                doDetailNewDomain.setCtrlNo( doDetailOldDomain.getCtrlNo() );
                doDetailNewDomain.setKanbanType(doDetailOldDomain.getKanbanType());
                doDetailNewDomain.setRemark1(doDetailOldDomain.getRemark1());
                doDetailNewDomain.setRemark2(doDetailOldDomain.getRemark2());
                doDetailNewDomain.setRemark3(doDetailOldDomain.getRemark3());
                doDetailNewDomain.setcPn(doDetailOldDomain.getcPn());
                doDetailNewDomain.setsProcCd(doDetailOldDomain.getsProcCd());
                
                spsTDoDetailService.create(doDetailNewDomain);
                doDetailNewList.add(doDetailNewDomain);
            }
            
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                transfer.getCurrentDelivery(),
                EMPTY_STRING,
                getLabelHandledException(locale
                    , TRANSFER_DO_STEP_COPY_DO_DETAIL_TO_CREATE_NEW_REVISION) );
        }
    }
    
    /**
     * 
     * <p>update ChangeDetail.</p>
     *
     * @param locale locale
     * @param newDoId BigDecimal
     * @param doDetailNewList List of SpsTDoDetailDomain
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param newDetailList List of TransferChangeDoDetailDataFromCigmaDomain
     * @throws ApplicationException exception
     */
    private void updateChangeDetail(Locale locale, BigDecimal newDoId
        , List<SpsTDoDetailDomain> doDetailNewList
        , TransferChangeDoDataFromCigmaDomain transfer
        , TransferChangeDoDetailDataFromCigmaDomain transferDetail
        , List<TransferChangeDoDetailDataFromCigmaDomain> newDetailList
    ) throws ApplicationException {
        Timestamp current = commonService.searchSysDate();
        if( !newDetailList.contains( transferDetail ) ){
            try {
                SpsTDoDetailDomain doDetailDomain = new SpsTDoDetailDomain();
                doDetailDomain.setDoId( newDoId );
                doDetailDomain.setSPn( transferDetail.getSPn() );
                doDetailDomain.setDPn( transferDetail.getPartNo() );
                doDetailDomain.setPnRevision( getNextRevision(doDetailNewList, doDetailDomain) );
                doDetailDomain.setChgCigmaDoNo( transfer.getCurrentDelivery() );
                doDetailDomain.setCtrlNo( transferDetail.getControlNo() );
                doDetailDomain.setCurrentOrderQty( 
                    toBigDecimal(transferDetail.getCurrentOrderQty()) );
                
                // [IN012] check P/N Shipment Status from Current Order Quantity
                if (null == doDetailDomain.getCurrentOrderQty() 
                    || Constants.ZERO 
                        == BigDecimal.ZERO.compareTo(doDetailDomain.getCurrentOrderQty()))
                {
                    doDetailDomain.setPnShipmentStatus( Constants.SHIPMENT_STATUS_CCL );
                } else {
                    doDetailDomain.setPnShipmentStatus( Constants.SHIPMENT_STATUS_ISS );
                }
                
                doDetailDomain.setQtyBox( toBigDecimal(transferDetail.getLotSize()) );
                doDetailDomain.setChgReason( transferDetail.getReasonForChange() );
                doDetailDomain.setItemDesc( transferDetail.getItemDescription() );
                doDetailDomain.setUnitOfMeasure( transferDetail.getUnitOfMeasure() );
                doDetailDomain.setModel( transferDetail.getModel() );
                doDetailDomain.setOriginalQty( 
                    toBigDecimal(transferDetail.getOriginalOrderQty()) );
//                doDetailDomain.setPreviousQty( 
//                    toBigDecimal(transferDetail.getCurrentOrderQty()) );
                doDetailDomain.setNoOfBoxes( 
                    toBigDecimal(transferDetail.getNoOfBoxes()) );
                doDetailDomain.setUrgentOrderFlg( STR_ONE );
                doDetailDomain.setMailFlg( STR_ZERO );
                doDetailDomain.setLastUpdateDscId( transferDetail.getUpdateBy() );
    //            doDetailDomain.setLastUpdateDatetime( 
    //                parseToTimestamp( appendsString(
    //                    transferDetail.getUpdateDate(), transferDetail.getUpdateTime()) 
    //                    , YYYYMMDD_HHMM) );
                doDetailDomain.setLastUpdateDatetime( current );
                spsTDoDetailService.update(doDetailDomain);
            
            } catch (Exception e) {
                throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                    transfer.getCurrentDelivery(),
                    EMPTY_STRING,
                    getLabelHandledException(locale
                        , TRANSFER_DO_STEP_CREATE_DO_DETAIL_TO_CREATE_NEW_REVISION) );
            }
        }else {
            try {
                SpsTDoDetailDomain doDetailDomain = new SpsTDoDetailDomain();
                doDetailDomain.setDoId( newDoId );
                doDetailDomain.setSPn( transferDetail.getSPn() );
                doDetailDomain.setDPn( transferDetail.getPartNo() );
                doDetailDomain.setPnRevision( DEFAULT_REVISION );
                doDetailDomain.setChgCigmaDoNo( transfer.getCurrentDelivery() );
                doDetailDomain.setCtrlNo( transferDetail.getControlNo() );
                doDetailDomain.setCurrentOrderQty( 
                    toBigDecimal(transferDetail.getCurrentOrderQty()) );
                
                // [IN012] check P/N Shipment Status from Current Order Quantity
                //doDetailDomain.setPnShipmentStatus( Constants.ISS_TYPE );
                if (null == doDetailDomain.getCurrentOrderQty() 
                    || Constants.ZERO 
                        == BigDecimal.ZERO.compareTo(doDetailDomain.getCurrentOrderQty()))
                {
                    doDetailDomain.setPnShipmentStatus( Constants.SHIPMENT_STATUS_CCL );
                } else {
                    doDetailDomain.setPnShipmentStatus( Constants.SHIPMENT_STATUS_ISS );
                }
                
                doDetailDomain.setQtyBox( toBigDecimal(transferDetail.getLotSize() ) );
                doDetailDomain.setChgReason( transferDetail.getReasonForChange() );
                doDetailDomain.setItemDesc( transferDetail.getItemDescription() );
                doDetailDomain.setUnitOfMeasure( transferDetail.getUnitOfMeasure() );
                doDetailDomain.setModel( transferDetail.getModel() );
                doDetailDomain.setOriginalQty( toBigDecimal(transferDetail.getCurrentOrderQty() ) );
                doDetailDomain.setPreviousQty( toBigDecimal(STR_ZERO) );
                doDetailDomain.setNoOfBoxes( toBigDecimal(transferDetail.getNoOfBoxes()) );
                
                if(StringUtil.checkNullOrEmpty(transferDetail.getRcvLane())){
                    doDetailDomain.setRcvLane( Constants.SYMBOL_SPACE );
                }else{
                    doDetailDomain.setRcvLane( transferDetail.getRcvLane() );
                }
                doDetailDomain.setWhLocation( transferDetail.getWhLocation() );
                
//                if( STR_Y.equals(transfer.getAddTruckRouteFlag()) ){
//                    doDetailDomain.setUrgentOrderFlg( STR_ONE );
//                }else{
//                    doDetailDomain.setUrgentOrderFlg( STR_ZERO );
//                }
                doDetailDomain.setUrgentOrderFlg( STR_ONE ); //transfer.getUrgentFlag() );
                doDetailDomain.setMailFlg( STR_ZERO );
                
                doDetailDomain.setCreateDscId( transferDetail.getCreateBy() );
                doDetailDomain.setCreateDatetime( 
                    parseToTimestamp( 
                        appendsString(
                            transferDetail.getCreateDate(), 
                            padZero(Integer.parseInt(transferDetail.getCreateTime()), Constants.SIX))
                        , PATTERN_YYYYMMDD_HHMMSS ) );
                doDetailDomain.setLastUpdateDscId( transferDetail.getUpdateBy() );
                doDetailDomain.setLastUpdateDatetime( current );
                doDetailDomain.setCtrlNo( transferDetail.getControlNo() );
                doDetailDomain.setKanbanType(transferDetail.getKanbanType());
                doDetailDomain.setRemark1(transferDetail.getRemark1());
                doDetailDomain.setRemark2(transferDetail.getRemark2());
                doDetailDomain.setRemark3(transferDetail.getRemark3());
                doDetailDomain.setcPn(transferDetail.getdCustomerPartNo());
                doDetailDomain.setsProcCd(transferDetail.getsProcessCode());
                
                spsTDoDetailService.create(doDetailDomain);
            } catch (Exception e) {
                throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                    transfer.getCurrentDelivery(),
                    EMPTY_STRING,
                    getLabelHandledException(locale
                        , TRANSFER_DO_STEP_CREATE_DO_DETAIL_TO_CREATE_NEW_REVISION) );
            }
        }
    }
    
    /**
     * 
     * <p>insert Change Kanban.</p>
     *
     * @param locale locale
     * @param transfer transferData
     * @param newDoId Do ID
     * @param doKanbanOldList Lis of SpsTDoKanbanSeqDomain
     * @throws ApplicationException exception
     */
    private void insertChangeKanban(Locale locale, TransferChangeDoDataFromCigmaDomain transfer,
        BigDecimal newDoId, List<SpsTDoKanbanSeqDomain> doKanbanOldList
    ) throws ApplicationException {
        Timestamp current = commonService.searchSysDate();
        try {
            for(SpsTDoKanbanSeqDomain doKanbanOldDomain : doKanbanOldList){
                SpsTDoKanbanSeqDomain doKanbanNewDomain = new SpsTDoKanbanSeqDomain();
                doKanbanNewDomain.setDoId( newDoId );
                doKanbanNewDomain.setSPn( doKanbanOldDomain.getSPn() );
                doKanbanNewDomain.setDPn( doKanbanOldDomain.getDPn() );
                doKanbanNewDomain.setKanbanSeqId( doKanbanOldDomain.getKanbanSeqId() );
                doKanbanNewDomain.setDeliveryRunDate( doKanbanOldDomain.getDeliveryRunDate() );
                doKanbanNewDomain.setDeliveryRunNo( doKanbanOldDomain.getDeliveryRunNo() );
                doKanbanNewDomain.setKanbanSeqNo( doKanbanOldDomain.getKanbanSeqNo() );
                doKanbanNewDomain.setKanbanCtrlNo( doKanbanOldDomain.getKanbanCtrlNo() );
                doKanbanNewDomain.setKanbanLotSize( doKanbanOldDomain.getKanbanLotSize() );
                doKanbanNewDomain.setTempKanbanTag( doKanbanOldDomain.getTempKanbanTag() );
                doKanbanNewDomain.setDeliveryDueTime( doKanbanOldDomain.getDeliveryDueTime() );
                doKanbanNewDomain.setDeliveryRunType( doKanbanOldDomain.getDeliveryRunType() );
                doKanbanNewDomain.setRunScheduleDate( doKanbanOldDomain.getRunScheduleDate() );
                doKanbanNewDomain.setQrRcvBatchNo( doKanbanOldDomain.getQrRcvBatchNo() );
                doKanbanNewDomain.setQrScanTime( doKanbanOldDomain.getQrScanTime() );
                doKanbanNewDomain.setCreateDscId( doKanbanOldDomain.getCreateDscId() );
                doKanbanNewDomain.setCreateDatetime( doKanbanOldDomain.getCreateDatetime() );
                doKanbanNewDomain.setLastUpdateDscId( doKanbanOldDomain.getLastUpdateDscId() );
                doKanbanNewDomain.setLastUpdateDatetime( 
                    doKanbanOldDomain.getLastUpdateDatetime() );
                doKanbanNewDomain.setLastUpdateDatetime( current );
                spsTDoKanbanSeqService.create(doKanbanNewDomain);
            }
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                transfer.getCurrentDelivery(),
                EMPTY_STRING,
                getLabelHandledException(locale
                    , TRANSFER_DO_STEP_COPY_KANBAN_TO_CREATE_NEW_REVISION) );
        }
    }
    
    /**
     * 
     * <p>update ChangeKanban.</p>
     *
     * @param locale locale
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param newDoId Do ID
     * @param doDomain SpsTDoDomain
     * @param doKanbanOldList List of Kanban
     * @throws ApplicationException exception
     */
    private void updateChangeKanban(Locale locale
        , TransferChangeDoDataFromCigmaDomain transfer
        , TransferChangeDoDetailDataFromCigmaDomain transferDetail
        , BigDecimal newDoId, SpsTDoDomain doDomain
        , List<SpsTDoKanbanSeqDomain> doKanbanOldList
    ) throws ApplicationException {
        Timestamp current = commonService.searchSysDate();
        try {
            SpsTDoKanbanSeqCriteriaDomain doKanbanCriteriaUpdateDomain = 
                new SpsTDoKanbanSeqCriteriaDomain();
//          doKanbanCriteriaUpdateDomain.setSPn( transferDetail.getSPn() );
            doKanbanCriteriaUpdateDomain.setDPn( transferDetail.getPartNo() );
            doKanbanCriteriaUpdateDomain.setDoId( newDoId );
            int kanbanSeq = ONE;
            int kanbanSeqNext = doKanbanOldList.size() + ONE;
            for( TransferDoKanbanlDataFromCigmaDomain transferKanban 
                : transferDetail.getCigmaDoKanbanSeq() )
            {
                SpsTDoKanbanSeqDomain doKanbanDomain = new SpsTDoKanbanSeqDomain();
                doKanbanDomain.setDoId( newDoId );
                doKanbanDomain.setDPn( transferDetail.getPartNo() );
                doKanbanDomain.setSPn( transferDetail.getSPn() );
                doKanbanDomain.setKanbanSeqId( toBigDecimal(kanbanSeq++) );
                doKanbanDomain.setDeliveryRunDate( 
                    parseToSqlDate( transferKanban.getDeliveryRunDate(), PATTERN_YYYYMMDD) );
                doKanbanDomain.setDeliveryRunNo( 
                    toBigDecimal(transferKanban.getDeliveryRunNo()) );
                doKanbanDomain.setKanbanSeqNo( 
                    toBigDecimal(transferKanban.getKanbanSeqNo()) );
                doKanbanDomain.setKanbanCtrlNo( transferKanban.getKanbanCtrlNo() );
                doKanbanDomain.setKanbanLotSize( 
                    toBigDecimal(transferKanban.getKanbanLotSize()) );
                doKanbanDomain.setTempKanbanTag( transferKanban.getTempKanbanTag() );
                doKanbanDomain.setDeliveryDueTime( 
                    toBigDecimal(transferKanban.getDeliveryDueTime()) );
                doKanbanDomain.setDeliveryRunType( transferKanban.getDeliveryRunType() );
                doKanbanDomain.setRunScheduleDate( 
                    parseToSqlDate(
                        transferKanban.getRunScheduleDate(), PATTERN_YYYYMMDD) );
                doKanbanDomain.setQrRcvBatchNo( 
                    toBigDecimal(transferKanban.getQrRecvBatchNo()) );
                doKanbanDomain.setQrScanTime( 
                    toBigDecimal(transferKanban.getQrScanTime()) );
                doKanbanDomain.setCreateDscId( transferDetail.getCreateBy() );
                doKanbanDomain.setCreateDatetime( 
                    parseToTimestamp( appendsString(
                        transferDetail.getCreateDate(), transferDetail.getCreateTime())
                        , PATTERN_YYYYMMDD_HHMMSS) );
                doKanbanDomain.setLastUpdateDscId( transferDetail.getUpdateBy() );
//                doKanbanDomain.setLastUpdateDatetime( 
//                    parseToTimestamp( appendsString(
//                        transferDetail.getUpdateDate(), transferDetail.getUpdateTime())
//                        , YYYYMMDD_HHMM) );
                doKanbanDomain.setLastUpdateDatetime( current );
                
                boolean bFound = false;
                for( SpsTDoKanbanSeqDomain oldKanban : doKanbanOldList )
                {
                    if( oldKanban.getDPn().equals(doKanbanDomain.getDPn()) ){
                        bFound = true;
                        break;
                    }
                }
                
                if( bFound ) {
                    spsTDoKanbanSeqService.updateByCondition(
                        doKanbanDomain, doKanbanCriteriaUpdateDomain);
                }else {
                    kanbanSeq--;
                    doKanbanDomain.setKanbanSeqId( toBigDecimal(kanbanSeqNext++) );
                    spsTDoKanbanSeqService.create(doKanbanDomain);
                }
            }
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, transfer.getCurrentDelivery(),
                EMPTY_STRING, getLabelHandledException(locale,
                    TRANSFER_DO_STEP_CREATE_KANBAN_TO_CREATE_NEW_REVISION) );
        }
    }
    
    /**
     * 
     * <p>transactTransferChgDoFromCigma.</p>
     *
     * @param locale Locale
     * @param transferList TransferChangeDoDataFromCigmaDomain
     * @param as400Server serverConfig
     * @param bError error
     * @param jobId jobId
     * @return TransferChangeDoDataFromCigmaDomain
     * @throws Exception error
     */
    private String transferChgDoFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        boolean bError, 
        String jobId
    ) throws Exception{
        StringBuffer sbErrorMsg = new StringBuffer();
        for(TransferChangeDoDataFromCigmaDomain transfer : transferList) {
            // 2-1)
            SpsTDoDomain doDomain = null;
            boolean originalMatch = true;
            
            // [IN071] get current datetime for set Last Update Datetime
            Timestamp lastUpdateDatetime = commonService.searchSysDate();
            
            List<TransferChangeDoDetailDataFromCigmaDomain> newDetailList 
                = new ArrayList<TransferChangeDoDetailDataFromCigmaDomain>();
            for(TransferChangeDoDetailDataFromCigmaDomain transferDetail 
                : transfer.getCigmaDoDetail())
            {
                // 2-1-1)
                DeliveryOrderDomain doParameter = new DeliveryOrderDomain();
                doParameter.setDoHeader( new SpsTDoDomain() );
                doParameter.setDoDetail( new SpsTDoDetailDomain() );
                doParameter.getDoHeader().setCigmaDoNo( transfer.getOriginalDelivery() );
                doParameter.getDoHeader().setSCd( transfer.getSCd() );

                // [IN022] Add more criteria for check exist D/O
                doParameter.getDoHeader().setVendorCd(transfer.getVendorCd());
                doParameter.getDoHeader().setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
                
                doParameter.getDoHeader().setDeliveryDatetime( parseToTimestamp(
                    appendsString(transfer.getDeliveryDate()
                        , transfer.getDeliveryTime()), PATTERN_YYYYMMDD_HHMM) );
                
                /* Search Original
                 * 01 : change quantity
                 * d_pn match
                 * data_type match
                 * s_pcd match
                 * */
                doParameter.getDoDetail().setDPn( transferDetail.getPartNo() );
                doParameter.getDoHeader().setDataType(transfer.getDataType());
                doParameter.getDoHeader().setSPcd( transfer.getSPcd() );
                
                List<SpsTDoDomain> doList = 
                    deliveryOrderService.searchExistDeliveryOrder(doParameter);
                if( null != doList && ZERO < doList.size() ){
                    doDomain = doList.get( ZERO );
                }else{
                    // [IN007] Start : Change D/O's S_PCD can be different from the original
                    /* Search Original
                     * 02 : add new parts
                     * d_pn not specified
                     * data_type match
                     * s_pcd match
                     * */
                    doParameter.getDoDetail().setDPn( null );
                    doList = deliveryOrderService.searchExistDeliveryOrder(doParameter);
                    if( null != doList && ZERO < doList.size() ){
                        doDomain = doList.get( ZERO );
                        newDetailList.add(transferDetail);
                        continue; // Check next part
                    } else {
                        /* Search Original
                         * 03 : change data type
                         * d_pn not specified
                         * data_type not specified
                         * s_pcd match
                         * */
                        doParameter.getDoHeader().setDataType(null);
                        doList = deliveryOrderService.searchExistDeliveryOrder(doParameter);
                        if( null != doList && ZERO < doList.size() ){
                            doDomain = doList.get( ZERO );
                            newDetailList.addAll(transfer.getCigmaDoDetail());
                            originalMatch = false;
                            break; // Skip all check
                        } else {
                            /* Search Original
                             * 04 : change data type
                             * d_pn not specified
                             * data_type not specified
                             * s_pcd not specified
                             * */
                            doParameter.getDoHeader().setSPcd(null);
                            doList = deliveryOrderService.searchExistDeliveryOrder(doParameter);
                            if( null != doList && ZERO < doList.size() ){
                                doDomain = doList.get( ZERO );
                                newDetailList.addAll(transfer.getCigmaDoDetail());
                                originalMatch = false;
                                break; // Skip all check
                            }
                        }
                    }
                    // [IN007] End : Change D/O's S_PCD can be different from the original
                }
            } // End For each D/O detail
            
            SpsTDoCriteriaDomain doCriteriaLastestDomain = new SpsTDoCriteriaDomain();
            doCriteriaLastestDomain.setDoId( doDomain.getDoId() );
            if (originalMatch) {
                // 2-1-3) Update Original D/O
                SpsTDoDomain doLastestDomain = new SpsTDoDomain();
                doLastestDomain.setDoId( doDomain.getDoId() );
                doLastestDomain.setLatestRevisionFlg( STR_ZERO );
                doLastestDomain.setLastUpdateDscId( jobId ); // transfer.getMaxUpdateDateTime() );
                // [IN071] : MaxUpdateDateTime never set value
                //doLastestDomain.setLastUpdateDatetime( 
                //    parseToTimestamp(transfer.getMaxUpdateDateTime()
                //        , YYYYMMDD_HHMMSS));
                doLastestDomain.setLastUpdateDatetime(lastUpdateDatetime);
                
                if( ZERO == spsTDoService.updateByCondition(doLastestDomain
                    , doCriteriaLastestDomain) ) {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                        transfer.getCurrentDelivery(),
                        EMPTY_STRING,
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_UPDATE_CURRENT_REVISION_DO) ); 
                }
                
                // Start : [IN071] also update D/O Detail
                SpsTDoDetailCriteriaDomain detailCriteria = new SpsTDoDetailCriteriaDomain();
                detailCriteria.setDoId(doDomain.getDoId());
                SpsTDoDetailDomain detailValue = new SpsTDoDetailDomain();
                detailValue.setLastUpdateDatetime(lastUpdateDatetime);
                detailValue.setLastUpdateDscId(jobId);
                if( ZERO == this.spsTDoDetailService.updateByCondition(detailValue, detailCriteria))
                {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                        transfer.getCurrentDelivery(),
                        EMPTY_STRING,
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_UPDATE_CURRENT_REVISION_DO) ); 
                }
                // End : [IN071] also update D/O Detail
            }
            
            SpsTDoDomain doOldDomain = spsTDoService.searchByKey(doCriteriaLastestDomain);
            if( null == doOldDomain ){
                throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                    transfer.getCurrentDelivery(),
                    EMPTY_STRING,
                    getLabelHandledException(locale
                        , TRANSFER_DO_STEP_SEARCH_DO_TO_CREATE_NEW_REVISION) );
            }
            
            if (!originalMatch) {
                // New D/O status set to initial (ISS)
                doOldDomain.setShipmentStatus( Constants.ISS_TYPE );

                // New D/O OrderMethod copy from Change D/O
                if( DDO_TYPE.equals(transfer.getDataType()) ){
                    doOldDomain.setOrderMethod( STR_ZERO );
                } else {
                    doOldDomain.setOrderMethod( STR_ONE );
                }
            } else {
                /* Add new parts and Shipment status is Complete Shipped,
                 * change status to Partial Shipped
                 * */
                if (ZERO < newDetailList.size() 
                    && Constants.SHIPMENT_STATUS_CPS.equals(doOldDomain.getShipmentStatus()))
                {
                    doOldDomain.setShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
                }
            }
            
            // create Cigma Change Delivery Order.
            BigDecimal newDoId = this.insertChangeDo(locale, transfer, doOldDomain);
            
            // [INXXX] Start : update ASN Detail for changed D/O
            if (originalMatch) {
                SpsTAsnDetailDomain asnDetailValue = new SpsTAsnDetailDomain();
                asnDetailValue.setDoId(newDoId);
                // [IN071] also set last update datetime
                asnDetailValue.setLastUpdateDscId(jobId);
                asnDetailValue.setLastUpdateDatetime(lastUpdateDatetime);
                
                SpsTAsnDetailCriteriaDomain asnDetailCriteria = new SpsTAsnDetailCriteriaDomain();
                asnDetailCriteria.setDoId(doDomain.getDoId());
                // Start : [IN071] Check before update
                //spsTAsnDetailService.updateByCondition(asnDetailValue, asnDetailCriteria);
                List<SpsTAsnDetailDomain> asnDetailList
                    = spsTAsnDetailService.searchByCondition(asnDetailCriteria);
                if (!asnDetailList.isEmpty()) {
                    spsTAsnDetailService.updateByCondition(asnDetailValue, asnDetailCriteria);
                    
                    List<String> asnKeyList = new ArrayList<String>();
                    for (SpsTAsnDetailDomain asnDetail : asnDetailList) {
                        String asnKey = StringUtil.appendsString(
                            asnDetail.getAsnNo(), asnDetail.getDCd());
                        if (asnKeyList.contains(asnKey)) {
                            continue;
                        }
                        
                        SpsTAsnCriteriaDomain asnCriteria = new SpsTAsnCriteriaDomain();
                        asnCriteria.setAsnNo(asnDetail.getAsnNo());
                        asnCriteria.setDCd(asnDetail.getDCd());
                        SpsTAsnDomain asnValue = new SpsTAsnDomain();
                        asnValue.setLastUpdateDscId(jobId);
                        asnValue.setLastUpdateDatetime(lastUpdateDatetime);
                        this.spsTAsnService.updateByCondition(asnValue, asnCriteria);
                        asnKeyList.add(asnKey);
                    }
                }
                // End : [IN071] Check before update
                
                // [IN019] Start : Also update Invoice Detail and CN Detail when issue Change D/O
                SpsTInvoiceDetailDomain invoiceDetailValue = new SpsTInvoiceDetailDomain();
                invoiceDetailValue.setDoId(newDoId);
                // [IN071] also set last update datetime
                invoiceDetailValue.setLastUpdateDatetime(lastUpdateDatetime);
                invoiceDetailValue.setLastUpdateDscId(jobId);
                
                SpsTInvoiceDetailCriteriaDomain invoiceDetailCriteria
                    = new SpsTInvoiceDetailCriteriaDomain();
                invoiceDetailCriteria.setDoId(doDomain.getDoId());

                // Start : [IN071] Check before update
                //this.spsTInvoiceDetailService.updateByCondition(
                //    invoiceDetailValue, invoiceDetailCriteria);
                List<SpsTInvoiceDetailDomain> invoiceDetailList
                    = this.spsTInvoiceDetailService.searchByCondition(invoiceDetailCriteria);
                if (!invoiceDetailList.isEmpty()) {
                    this.spsTInvoiceDetailService.updateByCondition(
                        invoiceDetailValue, invoiceDetailCriteria);
                    
                    List<BigDecimal> invoiceKeyList = new ArrayList<BigDecimal>();
                    for (SpsTInvoiceDetailDomain invoiceDetail : invoiceDetailList) {
                        if (invoiceKeyList.contains(invoiceDetail.getInvoiceId())) {
                            continue;
                        }
                        
                        SpsTInvoiceCriteriaDomain invoiceCriteria = new SpsTInvoiceCriteriaDomain();
                        invoiceCriteria.setInvoiceId(invoiceDetail.getInvoiceId());
                        SpsTInvoiceDomain invoiceValue = new SpsTInvoiceDomain();
                        invoiceValue.setLastUpdateDatetime(lastUpdateDatetime);
                        invoiceValue.setLastUpdateDscId(jobId);
                        this.spsTInvoiceService.updateByCondition(invoiceValue, invoiceCriteria);
                        
                        invoiceKeyList.add(invoiceDetail.getInvoiceId());
                    }
                }
                // End : [IN071] Check before update
                
                
                SpsTCnDetailDomain cnDetailValue = new SpsTCnDetailDomain();
                cnDetailValue.setDoId(newDoId);
                // [IN071] also set last update datetime
                cnDetailValue.setLastUpdateDatetime(lastUpdateDatetime);
                cnDetailValue.setLastUpdateDscId(jobId);
                
                SpsTCnDetailCriteriaDomain cnDetailCriteria = new SpsTCnDetailCriteriaDomain();
                cnDetailCriteria.setDoId(doDomain.getDoId());

                // Start : [IN071] Check before update
                //this.spsTCnDetailService.updateByCondition(cnDetailValue, cnDetailCriteria);
                List<SpsTCnDetailDomain> cnDetailList
                    = this.spsTCnDetailService.searchByCondition(cnDetailCriteria);
                if (!cnDetailList.isEmpty()) {
                    this.spsTCnDetailService.updateByCondition(cnDetailValue, cnDetailCriteria);

                    List<BigDecimal> cnKeyList = new ArrayList<BigDecimal>();
                    for (SpsTCnDetailDomain cnDetail : cnDetailList) {
                        if (cnKeyList.contains(cnDetail.getCnId())) {
                            continue;
                        }
                        
                        SpsTCnCriteriaDomain cnCriteria = new SpsTCnCriteriaDomain();
                        cnCriteria.setCnId(cnDetail.getCnId());
                        SpsTCnDomain cnValue = new SpsTCnDomain();
                        cnValue.setLastUpdateDatetime(lastUpdateDatetime);
                        cnValue.setLastUpdateDscId(jobId);
                        this.spsTCnService.updateByCondition(cnValue, cnCriteria);
                        
                        cnKeyList.add(cnDetail.getCnId());
                    }
                }
                // End : [IN071] Check before update
                
                // [IN019] End : Also update Invoice Detail and CN Detail when issue Change D/O
            }
            // [INXXX] End : update ASN Detail for changed D/O
            
            // Copy D/O Detail from original D/O
            List<SpsTDoDetailDomain> doDetailNewList = null;
            if (originalMatch) {
                SpsTDoDetailCriteriaDomain doDetailCriteriaDomain = new SpsTDoDetailCriteriaDomain();
                doDetailCriteriaDomain.setDoId( doDomain.getDoId() );
                List<SpsTDoDetailDomain> doDetailOldList = 
                    spsTDoDetailService.searchByCondition(doDetailCriteriaDomain);
                if( null != doDetailOldList && ZERO < doDetailOldList.size() ){
                    doDetailNewList = new ArrayList<SpsTDoDetailDomain>(doDetailOldList.size());
                }else{
                    doDetailNewList = new ArrayList<SpsTDoDetailDomain>();
                }
                if( null != doDetailOldList && ZERO < doDetailOldList.size() ){
                    // for each insert
                    insertChangeDetail(locale, transfer, newDoId,
                        doDetailOldList, doDetailNewList );
                } else {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                        transfer.getCurrentDelivery(),
                        EMPTY_STRING,
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION) );
                }
            } else {
                doDetailNewList = new ArrayList<SpsTDoDetailDomain>();
            }
            
            if( Constants.KDO_TYPE.equals(transfer.getDataType())) {
                // Copy D/O KANBAN Sequence from original D/O
                List<SpsTDoKanbanSeqDomain> doKanbanOldList = null;
                if (originalMatch) {
                    SpsTDoKanbanSeqCriteriaDomain doKanbanCriteriaDomain 
                        = new SpsTDoKanbanSeqCriteriaDomain();
                    doKanbanCriteriaDomain.setDoId( doDomain.getDoId() );
                    doKanbanOldList = spsTDoKanbanSeqService.searchByCondition(doKanbanCriteriaDomain);
                    if( null != doKanbanOldList && ZERO < doKanbanOldList.size() ){
                        // for each insert kanban seq.
                        insertChangeKanban(locale, transfer, newDoId, doKanbanOldList);
                    }
                } else {
                    doKanbanOldList = new ArrayList<SpsTDoKanbanSeqDomain>();
                }
                
                // 2-1-4)
                for(TransferChangeDoDetailDataFromCigmaDomain transferDetail 
                    : transfer.getCigmaDoDetail()){
                    updateChangeDetail(locale, 
                        newDoId, 
                        doDetailNewList, 
                        transfer, 
                        transferDetail,
                        newDetailList);
                    // 2-1-5)
                    updateChangeKanban( locale, 
                        transfer, 
                        transferDetail, 
                        newDoId, 
                        doDomain,
                        doKanbanOldList );
                } // end 2-1-4)
            } else {
                // 2-1-4)
                for(TransferChangeDoDetailDataFromCigmaDomain transferDetail 
                    : transfer.getCigmaDoDetail()){
                    updateChangeDetail(locale, 
                        newDoId, 
                        doDetailNewList, 
                        transfer, 
                        transferDetail,
                        newDetailList);
                } // end 2-1-4)
            }
//            2018-04-11 Don't generate BLOB
//            generateDeliveryOrder(
//                locale, 
//                transfer.getOriginalDelivery(), 
//                transfer.getDataType(), 
//                newDoId,
//                jobId,
//                true);

            // [IN012] Start : Check D/O current Shipment Status
            try {
                List<SpsTDoDomain> criteriaDomain = new ArrayList<SpsTDoDomain>();
                SpsTDoDomain doStatusCriteria = new SpsTDoDomain();
                doStatusCriteria.setDoId(newDoId);
                criteriaDomain.add(doStatusCriteria);
                List<SpsTDoDomain> doUpdateStatusList
                    = this.deliveryOrderService.searchExamineDoStatus(criteriaDomain);
                if (!doUpdateStatusList.isEmpty()) {
                    SpsTDoDomain doUpdateStatus = new SpsTDoDomain();
                    SpsTDoCriteriaDomain doUpdateStatusCriteria = new SpsTDoCriteriaDomain();
                    
                    doUpdateStatus.setShipmentStatus(
                        doUpdateStatusList.get(ZERO).getShipmentStatus());
                    doUpdateStatusCriteria.setDoId(doUpdateStatusList.get(ZERO).getDoId());

                    int rowUpdate = spsTDoService.updateByCondition(
                        doUpdateStatus, doUpdateStatusCriteria);
                    if(rowUpdate != criteriaDomain.size()){
                        throwsErrorMessage(locale, ERROR_CD_SP_E6_0027,
                            transfer.getOriginalDelivery(), 
                            EMPTY_STRING, 
                            getLabelHandledException(locale, TRANSFER_DO_STEP_UPDATE_CHG_DO_STATUS));
                    }
                }
            } catch (Exception e) {
                throwsErrorMessage(locale, ERROR_CD_SP_E6_0027,
                    transfer.getOriginalDelivery(), 
                    EMPTY_STRING, 
                    getLabelHandledException(locale, TRANSFER_DO_STEP_UPDATE_CHG_DO_STATUS));
            }
            // [IN012] End : Check D/O current Shipment Status
            
        }
        
        String partNoError = null;
        try{
            partNoError = CommonWebServiceUtil.updateCigmaChangeDeliveryOrder( as400Server, 
                transferList, STR_ONE, jobId );
            if( !StringUtil.checkNullOrEmpty(partNoError) ){
                if( !bError ){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, 
                        STR_ONE,
                        Constants.CHG_DO );
                }
            }
        }catch(Exception e){
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0024, 
                STR_ONE,
                Constants.CHG_DO);
        }
        
        return sbErrorMsg.toString();
    }
    
    /**
     * 
     * <p>Get Argument Message.</p>
     *
     * @param args array String
     * @return Sring[]
     */
    private String[] getArgumentMessage(String...args){
        if( null != args && ZERO < args.length ){
            if( ONE == args.length ){
                argument1[ZERO] = args[ZERO];
                return argument1;
            }else if( TWO == args.length ){
                argument2[ZERO] = args[ZERO];
                argument2[ONE] = args[ONE];
                return argument2;
            }else if( THREE == args.length ){
                argument3[ZERO] = args[ZERO];
                argument3[ONE] = args[ONE];
                argument3[TWO] = args[TWO];
                return argument3;
            }else if( FOUR == args.length ){
                for(int i = ZERO; i < FOUR; ++i){
                    argument4[i] = args[i];
                }
                return argument4;
            }else if( FIVE == args.length ){
                for(int i = ZERO; i < FIVE; ++i){
                    argument5[i] = args[i];
                }
                return argument5;
            }else if( SIX == args.length ){
                for(int i = ZERO; i < SIX; ++i){
                    argument6[i] = args[i];
                }
                return argument6;
            }
        }
        return null;
    }
    
    /**
     * 
     * <p>Clone New.</p>
     * @param src TransferDoDataFromCigmaDomain
     * @param result TransferDoDataFromCigmaDomain
     * @return TransferDoDataFromCigmaDomain
     */
    private TransferDoDataFromCigmaDomain cloneNew(TransferDoDataFromCigmaDomain src, 
        TransferDoDataFromCigmaDomain result){
        result.setDataType( src.getDataType() ); // Data Type
        result.setCompanyName( src.getCompanyName() ); // Company Name
        result.setCompanyAddress( src.getCompanyAddress() ); // Company Address
        result.setTruckRoute( src.getTruckRoute() ); // TRUCK ROUTE #
        result.setTruckSeq( src.getTruckSeq() ); // TRUCK SEQ #
        result.setVendorCd( src.getVendorCd() ); // Supplier Code
        result.setSupplierName( src.getSupplierName() ); // Supplier Name
        result.setSupplierLocation( src.getSupplierLocation() ); // Supplier Location
        result.setIssueDate( src.getIssueDate() ); // Issued Date
        result.setCigmaDoNo( src.getCigmaDoNo() ); // D/O Number
        result.setShipDate( src.getShipDate() ); // Ship Date
        result.setShipTime( src.getShipTime() ); // Ship Time
        result.setDeliveryDate( src.getDeliveryDate() ); // Delivery Date
        result.setDeliveryTime( src.getDeliveryTime() ); // Delivery Time
        result.setPlantCode( src.getPlantCode() ); // Plant Code
        result.setWhPrimeReceiving( src.getWhPrimeReceiving() ); // Warehouse for Prime Receiving
        result.setWhLocation( src.getWhLocation() ); // Werehouse Location
        result.setReceivingDock( src.getReceivingDock() ); // Receiving Dock
        result.setReceivingGate( src.getReceivingGate() ); // Receiving Gate
        result.setTm( src.getTm() ); // TM
        result.setCycle( src.getCycle() ); // Cycle
        result.setControlNo( src.getControlNo() ); // Control No.
        result.setTripNo( src.getTripNo() ); // Trip No.
        result.setBacklog( src.getBacklog() ); // Backlog
        result.setCigmaPoNo( src.getCigmaPoNo() ); // P/O Number
        result.setAddTruckRouteFlag( src.getAddTruckRouteFlag() ); // Add Truck Route Flag
        result.setCigmaDoDetail(new ArrayList<TransferDoDetailDataFromCigmaDomain>());
        result.setSCd( src.getSCd() );
        result.setUpdateByUserId(src.getUpdateByUserId());
        result.setScanReceiveFlag(src.getScanReceiveFlag());
        result.setTagOutput(src.getTagOutput());
        return result;
    }
    
    /**
     * 
     * <p>Clone New.</p>
     * @param src TransferChangeDoDataFromCigmaDomain
     * @param result TransferChangeDoDataFromCigmaDomain
     * @return TransferChangeDoDataFromCigmaDomain
     */
    private TransferChangeDoDataFromCigmaDomain cloneNew(TransferChangeDoDataFromCigmaDomain src, 
        TransferChangeDoDataFromCigmaDomain result){
        result.setDataType( src.getDataType() ); // Data Type
        result.setCompanyName( src.getCompanyName() ); // Company Name
        result.setCompanyAddress( src.getCompanyAddress() ); // Company Address
        result.setTruckRoute( src.getTruckRoute() ); // TRUCK ROUTE #
        result.setTruckSeq( src.getTruckSeq() ); // TRUCK SEQ #
        result.setVendorCd( src.getVendorCd() ); // Supplier Code
        result.setSupplierName( src.getSupplierName() ); // Supplier Name
        result.setSupplierLocation( src.getSupplierLocation() ); // Supplier Location
        result.setIssueDate( src.getIssueDate() ); // Issued Date
        result.setCurrentDelivery( src.getCurrentDelivery() ); // D/O Number
        result.setPrevDelivery(src.getPrevDelivery()); // Previous D/O number
        result.setShipDate( src.getShipDate() ); // Ship Date
        result.setShipTime( src.getShipTime() ); // Ship Time
        result.setDeliveryDate( src.getDeliveryDate() ); // Delivery Date
        result.setDeliveryTime( src.getDeliveryTime() ); // Delivery Time
        result.setPlantCode( src.getPlantCode() ); // Plant Code
        result.setWhPrimeReceiving( src.getWhPrimeReceiving() ); // Warehouse for Prime Receiving
//        result.setWhLocation( src.getWhLocation() ); // Werehouse Location
        result.setReceivingDock( src.getReceivingDock() ); // Receiving Dock
        result.setReceivingGate( src.getReceivingGate() ); // Receiving Gate
        result.setTm( src.getTm() ); // TM
        result.setCycle( src.getCycle() ); // Cycle
        result.setControlNo( src.getControlNo() ); // Control No.
//        result.setTripNo( src.getTripNo() ); // Trip No.
//        result.setBacklog( src.getBacklog() ); // Backlog
//        result.setCigmaPoNo( src.getCigmaPoNo() ); // P/O Number
//        result.setAddTruckRouteFlag( src.getAddTruckRouteFlag() ); // Add Truck Route Flag
        result.setCigmaDoDetail(new ArrayList<TransferChangeDoDetailDataFromCigmaDomain>());
        result.setSCd( src.getSCd() );
        result.setOriginalDelivery( src.getOriginalDelivery() );
        result.setUpdateByUserId(src.getUpdateByUserId());
        result.setScanReceiveFlag(src.getScanReceiveFlag());
        result.setTagOutput(src.getTagOutput());
        return result;
    } // end cloneNew
    
    /**
     * 
     * <p>Generate Delivery Order.</p>
     *
     * @param locale locale
     * @param cigmaDoNo CIGMA DO No
     * @param dataType dataType
     * @param doId DO ID.
     * @param jobId jobId
     * @param change change
     * @throws Exception Exception
     */
    private void generateDeliveryOrder(
        Locale locale,
        String cigmaDoNo,
        String dataType,
        BigDecimal doId,
        String jobId,
        boolean change) throws Exception {
     // Generate Report.
        InputStream isReportData = null;
        StringBuffer filenameReport = new StringBuffer();
        String fileId = null;
        //if( DDO_TYPE.equals(dataType) || DCD_TYPE.equals(dataType) ){
        if(DDO_TYPE.equals(dataType)) {
            DeliveryOrderSubDetailDomain doReport = new DeliveryOrderSubDetailDomain();
            doReport.setDoId( doId.toString() );
            isReportData = deliveryOrderService.searchDeliveryOrderReport(doReport);
            if( null == isReportData ){
                if( change ) {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                        cigmaDoNo, 
                        EMPTY_STRING, 
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_SEARCH_DELIVERY_ORDER_REPORT) );
                }else {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0022, 
                        cigmaDoNo, 
                        EMPTY_STRING, 
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_SEARCH_DELIVERY_ORDER_REPORT) );
                }
            }
            filenameReport.append(SupplierPortalConstant.DELIVERY_ORDER);
            filenameReport.append(doId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
        }else {
            KanbanOrderInformationDoDetailReturnDomain kanbanReport = 
                new KanbanOrderInformationDoDetailReturnDomain();
            kanbanReport.setDoId( doId.toString() );
            isReportData = deliveryOrderService.searchKanbanDeliveryOrderReport( kanbanReport );
            if( null == isReportData ){
                if( change ) {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0027, 
                        cigmaDoNo, 
                        EMPTY_STRING, 
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_SEARCH_KANBAN_DELIVERY_ORDER_REPORT) );
                }else {
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0022, 
                        cigmaDoNo, 
                        EMPTY_STRING, 
                        getLabelHandledException(locale
                            , TRANSFER_DO_STEP_SEARCH_KANBAN_DELIVERY_ORDER_REPORT) );
                }
            }
            filenameReport.append(SupplierPortalConstant.KANBAN);
            filenameReport.append(doId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
        }
        try {
            fileId = fileManagementService.createFileUpload(
                isReportData, filenameReport.toString(), Constants.SAVE_LIMIT_TERM, jobId);
        } catch (Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_80_0016, cigmaDoNo );
        }
        
        try {
            SpsTDoDomain doFileDomain = new SpsTDoDomain();
            doFileDomain.setPdfFileId(fileId);
            SpsTDoCriteriaDomain doCriteriaFileDomain = new SpsTDoCriteriaDomain();
            doCriteriaFileDomain.setDoId( doId );
            doFileDomain.setPdfFileId(fileId);
            spsTDoService.updateByCondition(doFileDomain, doCriteriaFileDomain);
        } catch (Exception e) {
            if( change ) {
                throwsErrorMessage(locale, ERROR_CD_SP_E6_0027,
                    cigmaDoNo, 
                    EMPTY_STRING, 
                    getLabelHandledException(locale, TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID) );
            }else {
                throwsErrorMessage(locale, ERROR_CD_SP_E6_0022,
                    cigmaDoNo, 
                    EMPTY_STRING, 
                    getLabelHandledException(locale, TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID) );
            }
        }
    }
    
    /**
     * 
     * <p>Find Supplier.</p>
     *
     * @param as400VendorCriteriaDomain SpsMAs400VendorDomain
     * @param vendorCd Vendor Code
     * @return SpsMAs400VendorDomain
     * @throws ApplicationException ApplicationException
     */
    private SpsMAs400VendorDomain findSupplier(
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain,
        String vendorCd) throws ApplicationException {
        as400VendorCriteriaDomain.setVendorCd( vendorCd );
        List<SpsMAs400VendorDomain> vendorList = 
            spsMAs400VendorService.searchByCondition(as400VendorCriteriaDomain);
        // check is not found Supplier Code.
        if( null != vendorList && ZERO < vendorList.size() ){
            return vendorList.get(ZERO);
        }else {
            return null;
        }
    }
    /**
     * 
     * <p>Find Part.</p>
     *
     * @param partCriteria SpsMDensoSupplierPartsCriteriaDomain
     * @param dpn Denso Part No
     * @param sortBy sort by
     * @return SpsMDensoSupplierPartsDomain
     * @throws ApplicationException ApplicationException
     */
    private SpsMDensoSupplierPartsDomain findPart(SpsMDensoSupplierPartsCriteriaDomain partCriteria
        , String dpn, String sortBy) throws ApplicationException
    {
        partCriteria.setDPn(dpn);
        partCriteria.setPreferredOrder(sortBy);
        
        List<SpsMDensoSupplierPartsDomain> partList =
            spsMDensoSupplierPartsService.searchByCondition( partCriteria );
        
        // check is not found Supplier Part No
        if( null != partList && ZERO < partList.size() ){
            return partList.get(ZERO);
        }else {
            return null;
        }
    }
    /**
     * 
     * <p>Get Message.</p>
     *
     * @param locale Locale
     * @param msgId Message ID
     * @param params arguments
     * @return Message String
     * @throws ApplicationException ApplicationException
     */
    private String getMessage(
        Locale locale, 
        String msgId, 
        String ... params) throws ApplicationException {
        return MessageUtil.getErrorMessage(locale, msgId, null, getArgumentMessage( params ) );
    }
    
    /**
     * 
     * <p>Throws ErrorMessage.</p>
     *
     * @param locale Locale
     * @param msgId Message ID
     * @param params argument
     * @throws ApplicationException ApplicationException
     */
    private void throwsErrorMessage(
        Locale locale, 
        String msgId, 
        String ... params) throws ApplicationException {
        MessageUtil.throwsApplicationMessage(locale, msgId, null, getArgumentMessage( params ) );
    }
    
    /**
     * <p>fill Mail D/O Detil.</p>
     *
     * @param sbContent StringBuffer
     * @param locale Locale
     * @param errorType Error Type Flag
     * @param domain TransferDoErrorEmailDomain
     * @param detail TransferDoErrorEmailDetailDomain
     * @throws Exception the Exception
     */
    private void fillDoDetil(
        StringBuffer sbContent, 
        Locale locale,
        String errorType,
        TransferDoErrorEmailDomain domain,
        TransferDoErrorEmailDetailDomain detail) throws Exception
    {
        /* [User Name] [D_CD] [D_PCD] [D_PN] [D/O Type] [Error delivery date] [Delivery Time]
         * [S_PCD] [Supplier P/No] [Effective Date] [CIGMA D/O No.]
         */
        String username = nullToEmpty(detail.getCigmaDoError().getUpdateByUserId());
        if( EMPTY_STRING.equals(username) ){
            username = SYMBOL_NBSP;
        }
        String dcd = nullToEmpty(domain.getDCd());
        if( EMPTY_STRING.equals(dcd) ){
            dcd = SYMBOL_NBSP;
        }
        String dpcd = nullToEmpty(domain.getDPcd());
        if( EMPTY_STRING.equals(dpcd) ){
            dpcd = SYMBOL_NBSP;
        }
        String dpn = nullToEmpty(detail.getCigmaDoError().getDPn());
        if( EMPTY_STRING.equals(dpn) ){
            dpn = SYMBOL_NBSP;
        }
        String dataType = nullToEmpty(detail.getCigmaDoError().getDataType());
        if( EMPTY_STRING.equals(dataType) ){
            dataType = SYMBOL_NBSP;
        }
        String deliveryDate = format(domain.getDeliveryDate()
            , PATTERN_YYYYMMDD_SLASH);
        if( EMPTY_STRING.equals(deliveryDate) ){
            deliveryDate = SYMBOL_NBSP;
        }
        String deliveryTime = nullToEmpty(domain.getDeliveryTime());
        if( EMPTY_STRING.equals(deliveryTime) ){
            deliveryTime = SYMBOL_NBSP;
        }
        
        // Start : [IN068] Use first date of minimum Issue Month for this Part No.
        // Start : [IN055] change delivery date to first date of month to avoid P/O error
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(errorType) && null != domain.getDeliveryDate()) {
        //    deliveryTime = SYMBOL_NBSP;
        //    Calendar deliveryCal = Calendar.getInstance();
        //    deliveryCal.setTimeInMillis(domain.getDeliveryDate().getTime());
        //    deliveryCal.set(Calendar.DATE, Constants.ONE);
        //    java.sql.Date firstDayOfMonth = new java.sql.Date(
        //        deliveryCal.getTimeInMillis());
        //    deliveryDate = format(firstDayOfMonth, YYYYMMDD_SLASH);
        //}
        // End : [IN055] change delivery date to first date of month to avoid P/O error
        // [IN068] apply to Supplier Info Not found and Effective Date Not Match
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(errorType)) {
        if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(errorType)
            || ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(errorType))
        {
            deliveryTime = SYMBOL_NBSP;
            SpsCigmaPoErrorDomain minIssueCriteria = new SpsCigmaPoErrorDomain();
            minIssueCriteria.setDCd(domain.getDCd());
            minIssueCriteria.setSCd(domain.getSCd());
            minIssueCriteria.setDPcd(domain.getDPcd());
            minIssueCriteria.setDPn(detail.getCigmaDoError().getDPn());
            Date minIssueDate = this.cigmaPoErrorService.searchMinIssueDate(minIssueCriteria);
            Calendar deliveryCal = Calendar.getInstance();
            deliveryCal.setTimeInMillis(minIssueDate.getTime());
            deliveryCal.set(Calendar.DATE, Constants.ONE);
            java.sql.Date firstDayOfMonth = new java.sql.Date(deliveryCal.getTimeInMillis());
            deliveryDate = StringUtil.appendsString(Constants.SYMBOL_SPAN_RED
                , format(firstDayOfMonth, PATTERN_YYYYMMDD_SLASH)
                , Constants.SYMBOL_END_SPAN);
        }
        // End : [IN068] Use first date of minimum Issue Month for this Part No.
        
        String spcd = SYMBOL_NBSP;
        String sPn = SYMBOL_NBSP;
        String effect = SYMBOL_NBSP;
        if (ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(errorType)) {
            // [IN016] move S_PCD to domain
            //spcd = nullToEmpty(detail.getDensoSupplerParts().getSPcd());
            spcd = nullToEmpty(domain.getSPcd());
            if( EMPTY_STRING.equals(spcd) ){
                spcd = SYMBOL_NBSP;
            }
            sPn = nullToEmpty(detail.getDensoSupplerParts().getSPn());
            if( EMPTY_STRING.equals(sPn) ){
                sPn = SYMBOL_NBSP;
            }
            effect = format(detail.getDensoSupplerParts().getEffectDate()
                , PATTERN_YYYYMMDD_SLASH);
            if(null == detail.getDensoSupplerParts().getEffectDate() || EMPTY_STRING.equals(effect) ){
                effect = SYMBOL_NBSP;
            }
        }
        
        sbContent.append(appendsString(String.format(
            getEmailLabel(locale , MAIL_CIGMA_DO_ERROR_DETAIL)
            , username
            , dcd
            , dpcd
            , dpn
            , dataType
            , deliveryDate
            , deliveryTime
            , spcd
            , sPn
            , effect
            , domain.getCigmaDoNo())
            , SYMBOL_CR_LF));
    }
    
    /**
     * 
     * <p>fill Mail Change D/O Detail.</p>
     *
     * @param sbContent StringBuffer
     * @param locale Locale
     * @param errorType Error Type Flag
     * @param domain TransferChgDoErrorEmailDomain
     * @param detail TransferChgDoErrorEmailDetailDomain
     * @throws Exception the Exception
     */
    private void fillChgDoDetail(
        StringBuffer sbContent, 
        Locale locale,
        String errorType,
        TransferChgDoErrorEmailDomain domain,
        TransferChgDoErrorEmailDetailDomain detail) throws Exception
    {
        /* [User Name] [D_CD] [D_PCD] [D_PN] [D/O Type] [Error delivery date] [Delivery Time]
         * [S_PCD] [Supplier P/No] [Effective Date] [CIGMA D/O No.]
         */
        String username = nullToEmpty(detail.getCigmaChgDoError().getUpdateByUserId());
        if( EMPTY_STRING.equals(username) ){
            username = SYMBOL_NBSP;
        }
        String dcd = nullToEmpty(domain.getDCd());
        if( EMPTY_STRING.equals(dcd) ){
            dcd = SYMBOL_NBSP;
        }
        String dpcd = nullToEmpty(domain.getDPcd());
        if( EMPTY_STRING.equals(dpcd) ){
            dpcd = SYMBOL_NBSP;
        }
        String dpn = nullToEmpty(detail.getCigmaChgDoError().getDPn());
        if( EMPTY_STRING.equals(dpn) ){
            dpn = SYMBOL_NBSP;
        }
        String dataType = nullToEmpty(detail.getCigmaChgDoError().getDataType());
        if( EMPTY_STRING.equals(dataType) ){
            dataType = SYMBOL_NBSP;
        }
        
        String deliveryDate = format(domain.getDeliveryDate()
            , PATTERN_YYYYMMDD_SLASH);
        if( EMPTY_STRING.equals(deliveryDate) ){
            deliveryDate = SYMBOL_NBSP;
        }
        String deliveryTime = nullToEmpty(domain.getDeliveryTime());
        if( EMPTY_STRING.equals(deliveryTime) ){
            deliveryTime = SYMBOL_NBSP;
        }

        // Start : [IN068] Use first date of minimum Issue Month for this Part No.
        // Start : [IN055] change delivery date to first date of month to avoid P/O error
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(errorType) && null != domain.getDeliveryDate()) {
        //    deliveryTime = SYMBOL_NBSP;
        //    Calendar deliveryCal = Calendar.getInstance();
        //    deliveryCal.setTimeInMillis(domain.getDeliveryDate().getTime());
        //    deliveryCal.set(Calendar.DATE, Constants.ONE);
        //    java.sql.Date firstDayOfMonth = new java.sql.Date(
        //        deliveryCal.getTimeInMillis());
        //    deliveryDate = format(firstDayOfMonth, YYYYMMDD_SLASH);
        //}
        // End : [IN055] change delivery date to first date of month to avoid P/O error
        
        // [IN068] apply to Supplier Info Not found and Effective Date Not Match
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(errorType)) {
        if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(errorType)
            || ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(errorType))
        {
            deliveryTime = SYMBOL_NBSP;
            SpsCigmaPoErrorDomain minIssueCriteria = new SpsCigmaPoErrorDomain();
            minIssueCriteria.setDCd(domain.getDCd());
            minIssueCriteria.setSCd(domain.getSCd());
            minIssueCriteria.setDPcd(domain.getDPcd());
            minIssueCriteria.setDPn(detail.getCigmaChgDoError().getDPn());
            Date minIssueDate = this.cigmaPoErrorService.searchMinIssueDate(minIssueCriteria);
            Calendar deliveryCal = Calendar.getInstance();
            deliveryCal.setTimeInMillis(minIssueDate.getTime());
            deliveryCal.set(Calendar.DATE, Constants.ONE);
            java.sql.Date firstDayOfMonth = new java.sql.Date(deliveryCal.getTimeInMillis());
            deliveryDate = StringUtil.appendsString(Constants.SYMBOL_SPAN_RED
                , format(firstDayOfMonth, PATTERN_YYYYMMDD_SLASH)
                , Constants.SYMBOL_END_SPAN);
        }
        // End : [IN068] Use first date of minimum Issue Month for this Part No.
        
        String spcd = SYMBOL_NBSP;
        String sPn = SYMBOL_NBSP;
        String effect = SYMBOL_NBSP;
        if (ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(errorType)) {
            
            // [IN016] S_PCD move to domain
            //spcd = nullToEmpty(detail.getDensoSupplerParts().getSPcd());
            spcd = nullToEmpty(domain.getSPcd());
            
            if( EMPTY_STRING.equals(spcd) ){
                spcd = SYMBOL_NBSP;
            }
            sPn = nullToEmpty(detail.getDensoSupplerParts().getSPn());
            if( EMPTY_STRING.equals(sPn) ){
                sPn = SYMBOL_NBSP;
            }
            effect = format(detail.getDensoSupplerParts().getEffectDate()
                , PATTERN_YYYYMMDD_SLASH);
            if(null == detail.getDensoSupplerParts().getEffectDate() || EMPTY_STRING.equals(effect) ){
                effect = SYMBOL_NBSP;
            }
        }

        sbContent.append(appendsString(String.format(
            getEmailLabel(locale , MAIL_CIGMA_CHG_DO_2_ERROR_DETAIL)
            , username
            , dcd
            , dpcd
            , dpn
            , dataType
            , deliveryDate
            , deliveryTime
            , spcd
            , sPn
            , effect
            , domain.getCigmaDoNo())
            , SYMBOL_CR_LF));
    }
    
    /**
     * 
     * <p>send D/O Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param sbTitle StringBuffer
     * @param sbFrom StringBuffer
     * @param emailTo List of mail
     * @param msgFooterId Message ID
     * @param companyCd company code
     * @param log to output error message
     * @throws Exception the Exception
     */
    private void sendDoMail(
        Locale locale, 
        StringBuffer sbContent, 
        StringBuffer sbTitle, 
        StringBuffer sbFrom, 
        List<String> emailTo,
        String msgFooterId,
        String companyCd,
        Log log ) throws Exception {
        sbContent.append( Constants.SYMBOL_END_TABLE );
        sbContent.append( 
            appendsString( 
                getEmailLabel(locale
                    , msgFooterId) // MAIL_CIGMA_DO_ERROR_FOOTER)
                    /*, SYMBOL_NEWLINE*/) );
        
        sendMail(
            locale,
            sbTitle.toString(), 
            sbContent.toString(), 
            sbFrom.toString(), 
            emailTo,
            false,
            companyCd,
            log);
        sbContent.delete(0, sbContent.length());
    }
    
    // Start : [IN025] change email content have to check errorTypeFlag
    ///**
    // * <p> initial D/O Mail.</p>
    // *
    // * @param locale Locale
    // * @param emailTo List of mail
    // * @param sbContent StringBuffer
    // * @throws Exception the Exception
    // */
    //private void initialDoMail(
    //    Locale locale,
    //    List<String> emailTo,
    //    StringBuffer sbContent) throws Exception {
    //    sbContent.append(appendsString(
    //        getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER1)
    //        , SYMBOL_NEWLINE
    //        , appendsString(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER2))));
    //}
    /**
     * <p> initial D/O Mail.</p>
     *
     * @param locale Locale
     * @param emailTo List of mail
     * @param sbContent StringBuffer
     * @param errorTypeFlag error type flag
     * @throws Exception the Exception
     */
    private void initialDoMail(
        Locale locale,
        List<String> emailTo,
        StringBuffer sbContent,
        String errorTypeFlag) throws Exception {
        
        String header2Id = MAIL_CIGMA_DO_ERROR_HEADER2;
        
        if (ERROR_TYPE_PO_NOT_FOUND.equals(errorTypeFlag)) {
            header2Id = MAIL_CIGMA_DO_ERROR_HEADER2_PONOTFOUND;
        } else if (ERROR_TYPE_DO_DUP.equals(errorTypeFlag)) {
            header2Id = MAIL_CIGMA_DO_ERROR_HEADER2_DODUP;
        } else if (ERROR_TYPE_PREVIOUS_REV_ERROR.equals(errorTypeFlag)) {
            header2Id = MAIL_CIGMA_CHG_DO_2_ERROR_HEADER2_PREREVERROR;
        }
        
        sbContent.append(appendsString(
            getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER1)
            , SYMBOL_NEWLINE
            , appendsString(getEmailLabel(locale, header2Id))));
        
        // [IN070] add list of email to send in content
        if (null != emailTo && ZERO != emailTo.size()) {
            String emailToString = StringUtil.convertListToStringCommaSeperate(emailTo);
            sbContent.append(appendsString(
                SYMBOL_NEWLINE
                , getEmailLabel(locale, MAIL_CIGMA_ERROR_PERSON_IN_CHARGE)
                , emailToString
                , Constants.SYMBOL_END_PARAGRAPH
                , SYMBOL_NEWLINE));
        }
        
    }
    // End : [IN025] change email content have to check errorTypeFlag
    
    /**
     * 
     * <p>Find Mail.</p>
     * @param criSupplierUser UserSupplierDetailDomain
     * @param sCd String
     * @param sPcd String
     * @param emailTo List of mail
     * @param urgent boolean
     * @return skip S_CD
     * @throws ApplicationException the ApplicationException
     */
    // [IN016] Send email to supplier by plant
    //private boolean findMail(
    //    UserSupplierDetailDomain criSupplierUser,
    //    String sCd,
    //    List<String> emailTo,
    //    boolean urgent) throws ApplicationException {
    private boolean findMail(
        UserSupplierDetailDomain criSupplierUser,
        String sCd,
        String sPcd,
        List<String> emailTo,
        boolean urgent) throws ApplicationException
    {
        criSupplierUser.getSpsMUserSupplierDomain().setSCd(sCd);

        // [IN016] Send email to supplier by plant
        criSupplierUser.getSpsMUserSupplierDomain().setSPcd(sPcd);
        
        if( ZERO == emailTo.size() ) {
            
            // [IN016] avoid send duplicate email
            //emailTo.addAll( 
            //    userSupplierService
            //        .searchUserSupplierEmailByNotFoundFlag(criSupplierUser)
            //);
            List<String> mailList
                = userSupplierService.searchUserSupplierEmailByNotFoundFlag(criSupplierUser);
            for (String mail : mailList) {
                if (!emailTo.contains(mail)) {
                    emailTo.add(mail);
                }
            }
            
            if( ZERO == emailTo.size() ){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Find User DENSO's email.
     * @param criDensoUser criteria for search
     * @param dCd DENSO company Code
     * @param dPcd DENSO plant code.
     * @param emailTo list of email to send
     * @return isSkip
     * */
    private boolean findDensoMail(SpsMUserDensoDomain criDensoUser, String dCd, String dPcd
        , List<String> emailTo)
    {
        criDensoUser.setDCd(dCd);
        criDensoUser.setDPcd(dPcd);
        emailTo.clear();
        List<SpsMUserDomain> densoList = userService.searchEmailUserDenso(criDensoUser);
        if (ZERO != densoList.size()) {
            emailTo.addAll(convertEmail(densoList));
            return false;
        }
        return true;
    }
    
    /**
     * 
     * <p>Update Mail Flag.</p>
     *
     * @param locale locale
     * @param domain DeliveryOrderDomain
     * @throws ApplicationException the ApplicationException
     */
    private void updateMailFlag(Locale locale, DeliveryOrderDomain domain )
        throws ApplicationException
    {
        SpsTDoDetailDomain detailDo = new SpsTDoDetailDomain();
        SpsTDoDetailCriteriaDomain criteriaDo = new SpsTDoDetailCriteriaDomain();
        
        detailDo.setMailFlg( STR_ONE );
        detailDo.setLastUpdateDscId( SupplierPortalConstant.SPS_SYSTEM_NAME );
        detailDo.setLastUpdateDatetime( commonService.searchSysDate() );
        
        criteriaDo.setDoId( domain.getDoHeader().getDoId() );
        criteriaDo.setDPn( domain.getDoDetail().getDPn() );
        criteriaDo.setLastUpdateDatetime( domain.getDoDetail().getLastUpdateDatetime() );
        
        if( ZERO == spsTDoDetailService.updateByCondition(detailDo, criteriaDo) ) {
            throwsErrorMessage(locale, ERROR_CD_SP_E6_0007);
        }
    }
    
    /**
     * 
     * <p>Initial Cigma Urgent.</p>
     *
     * @param locale locale
     * @param sbContent StringBuffer
     * @param domain for Supplier Information
     * @throws Exception the Exception
     */
    private void initialCigmaUrgent(Locale locale, StringBuffer sbContent
        , DeliveryOrderDomain domain) throws Exception 
    {
        sbContent.append(appendsString(
            getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER1)
            , SYMBOL_NEWLINE) );
        sbContent.append(appendsString(
            getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER2)
            , SYMBOL_NEWLINE) );
        
        // Cigma Vendor CD, S.CD, S.P.CD
        sbContent.append(
            getEmailLabel(locale, MAIL_CIGMA_DO_URGENT_HEADER3)
                .replaceAll(MAIL_VENDOR_CD_REPLACEMENT,
                    domain.getDoHeader().getVendorCd())
                .replaceAll(MAIL_SCD_REPLACEMENT,
                    domain.getDoHeader().getSCd()) 
                .replaceAll(MAIL_SPCD_REPLACEMENT, 
                    domain.getDoHeader().getSPcd()));
    }
    
    /**
     * 
     * <p>Send Urgent Mail.</p>
     *
     * @param locale locale
     * @param sbTitle StringBuffer
     * @param sbFrom StringBuffer
     * @param sbContent StringBuffer
     * @param emailTo List of mail
     * @param companyCd company code
     * @param log to output error message
     * @throws Exception the Exception
     */
    private void sendUrgentMail(
        Locale locale, 
        StringBuffer sbTitle, 
        StringBuffer sbFrom, 
        StringBuffer sbContent, 
        List<String> emailTo,
        String companyCd,
        Log log ) throws Exception {
        sbContent.append( Constants.SYMBOL_END_TABLE );
        sbContent.append( 
            appendsString( 
                getEmailLabel(locale
                    , MAIL_CIGMA_DO_URGENT_FOOTER) 
                    , SYMBOL_NEWLINE) );
        // 1-6) Call MailService sendEmail() to create e-mail
        sendMail(
            locale,
            sbTitle.toString(), 
            sbContent.toString(), 
            sbFrom.toString(), 
            emailTo,
            true,
            companyCd,
            log);
        
        sbContent.setLength(ZERO);
        //sbContent.delete(ZERO, sbContent.length() - ONE);
    }

    /** SPS Phase II */
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchNotifyDeliveryOrderToSupplier(com.globaldenso.asia.sps.business.domain.SpsTDoDomain)
     */
    public List<SpsTDoDomain> searchNotifyDeliveryOrderToSupplier(SpsTDoDomain spsTDoDomain) 
        throws ApplicationException {
        return deliveryOrderService.searchNotifyDeliveryOrderToSupplier(spsTDoDomain);
    }

    /**
     * 
     * <p>Send mail Notification.</p>
     *
     * @param locale Locale
     * @param spsTDoDomainList List of SpsTDoDomain
     * @param limit int limit
     * @param log log
     * @param spsTDoDomainCritiria in SpsTDoDomain
     * @throws Exception Exception
     */
    public void transactNotifyDeliveryOrderToSupplier(Locale locale,
        List<SpsTDoDomain> spsTDoDomainList, int limit, Log log, SpsTDoDomain spsTDoDomainCritiria)
        throws Exception {
        //1.    Set the initial variable for notify e-mail
        String sCd = null;
        String subject = new String();
        StringBuffer sbTitle = new StringBuffer();
        StringBuffer sbContent = new StringBuffer();
        StringBuffer sbFrom = new StringBuffer();
        List<String> emailTo = new ArrayList<String>();
        SpsTDoDomain spsTDoDomain = new SpsTDoDomain();
        
        //2.    Check the type to notify 
        if(spsTDoDomainCritiria.getNotificationMailDoFlag() != null){
            spsTDoDomain.setNotificationMailDoFlag(Constants.STR_ONE);
            subject = getEmailLabel(locale, MAIL_DO_NOTIFICATION_SUBJECT);
        } else if (spsTDoDomainCritiria.getNotificationMailKanbanFlag() != null){
            spsTDoDomain.setNotificationMailKanbanFlag(Constants.STR_ONE);
            subject = getEmailLabel(locale, MAIL_KANBAN_NOTIFICATION_SUBJECT);
        }
        
        //3.    Set initial content of e-mail to send to supplier.
        // HashMap for Grouping E-mail sending by supplier plant Code
        Map<String, String> supplierInfo = new HashMap<String, String>();
        Map<String, List<String>> supplierMailTo = new HashMap<String, List<String>>();
        Map<String, StringBuffer> supplierSubject = new HashMap<String, StringBuffer>();
        Map<String, StringBuffer> supplierDetail = new HashMap<String, StringBuffer>();
        Map<String, String> supplierUtc = new HashMap<String, String>();
        Map<String, List<BigDecimal>> supplierDoId = new HashMap<String, List<BigDecimal>>();
        
        UserSupplierDetailDomain criSupplierUser = new UserSupplierDetailDomain();
        criSupplierUser.setSpsMUserDomain( new SpsMUserDomain() );
        criSupplierUser.setSpsMUserSupplierDomain( new SpsMUserSupplierDomain() );
        criSupplierUser.getSpsMUserDomain().setIsActive( STR_ONE );
        criSupplierUser.getSpsMUserDomain().setUserType( Constants.STR_S );
//        criSupplierUser.getSpsMUserSupplierDomain().setEmlSInfoNotfoundFlag( STR_ONE );
        // User request to send the e-mail notification to the person who get the urgent order. 
        criSupplierUser.getSpsMUserSupplierDomain().setEmlUrgentOrderFlag( STR_ONE );
        sbFrom.append( ContextParams.getDefaultEmailFrom());
        
        if( null != spsTDoDomainList){
            for(SpsTDoDomain domain : spsTDoDomainList){
                String keyInfo = domain.getDCd().concat(domain.getDPcd()).concat(domain.getSCd()).concat(domain.getSPcd());
                if(supplierInfo.get(keyInfo) == null){
                    supplierInfo.put(keyInfo, keyInfo);
                    supplierSubject.put(keyInfo, new StringBuffer(subject));
                    supplierDetail.put(keyInfo, new StringBuffer().append(
                        appendsString(getEmailLabel(locale, MAIL_DO_NOTIFICATION_CONTENT_HEADER),
                            Constants.SYMBOL_NEWLINE,
                            getEmailLabel(locale, MAIL_DO_NOTIFICATION_CONTENT_DETAIL_HEADER))));
                    
                    criSupplierUser.getSpsMUserSupplierDomain().setSCd(domain.getSCd());
                    criSupplierUser.getSpsMUserSupplierDomain().setSPcd(domain.getSPcd());
                    List<String> mailList
                        = userSupplierService.searchUserSupplierEmailByNotFoundFlag(criSupplierUser);
                    supplierMailTo.put(keyInfo, mailList);
                    

                    SpsMCompanyDensoDomain spsMCompanyDensoDomain = new SpsMCompanyDensoDomain();
                    spsMCompanyDensoDomain.setDCd(StringUtil.convertListToVarcharCommaSeperate(domain.getDCd()));
                    List<SpsMCompanyDensoDomain> spsMCompanyDensoDomainList = 
                        companyDensoService.searchCompanyDensoByCodeList(spsMCompanyDensoDomain);
                    supplierUtc.put(keyInfo, spsMCompanyDensoDomainList.get(Constants.ZERO).getUtc());
                    supplierDoId.put(keyInfo, new ArrayList<BigDecimal>());
                }
                StringBuffer utc = new StringBuffer();
                utc.append(Constants.SYMBOL_OPEN_BRACKET).append(Constants.WORD_UTC).append(supplierUtc.get(keyInfo).trim()).append(Constants.SYMBOL_CLOSE_BRACKET);
                StringBuffer stringForAppend = supplierDetail.get(keyInfo);
                String orderMethodName = new String();
                if(domain.getOrderMethod().equals(Constants.STR_ZERO)){
                    orderMethodName = MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_FIXED_ORDER);
                } else if(domain.getOrderMethod().equals(Constants.STR_ONE)){
                    orderMethodName = MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_KANBAN_ORDER);
                }
                
                stringForAppend.append(getEmailLabel(locale, MAIL_DO_NOTIFICATION_CONTENT_DETAIL)
                    .replaceAll(MAIL_SCD_REPLACEMENT, domain.getVendorCd()) 
                    .replaceAll(MAIL_SPCD_REPLACEMENT, domain.getSPcd())
                    .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDCd())
                    .replaceAll(MAIL_DPCD_REPLACEMENT, domain.getDPcd())
                    .replaceAll(MAIL_CIGMA_DO_NO_REPLACEMENT, domain.getCigmaDoNo())
                    .replaceAll(MAIL_SPS_DO_NO_REPLACEMENT, domain.getSpsDoNo())
                    .replaceAll(MAIL_REV_NO_REPLACEMENT, domain.getRevision())
                    .replaceAll(MAIL_DELIVERY_DATE_REPLACEMENT, 
                        DateUtil.format(
                            new Date(domain.getDeliveryDatetime().getTime()),
                            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH).concat(utc.toString()))
                    .replaceAll(MAIL_SHIPMENT_DATE_REPLACEMENT, 
                        DateUtil.format(
                            new Date(domain.getShipDatetime().getTime()),
                            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH).concat(utc.toString()))
                    .replaceAll(MAIL_DO_ISSUE_DATE, 
                        DateUtil.format(
                            new Date(domain.getDoIssueDate().getTime()),
                            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH).concat(utc.toString()))
                    .replaceAll(MAIL_ORDER_TYPE_REPLACEMENT, orderMethodName)
                    .replaceAll(MAIL_SHIPMENT_STATUS_REPLACEMENT, domain.getShipmentStatus()));
                supplierDetail.put(keyInfo, stringForAppend);
                
                List<BigDecimal> tmp = supplierDoId.get(keyInfo);
                tmp.add(domain.getDoId());
                supplierDoId.put(keyInfo, tmp);
            }
        }
        
        //4.    Send the e-mail to notify the supplier and update the status of NOTIFICATION_MAIL_DO_FLAG and NOTIFICATION_MAIL_KANBAN_FLAG
        for (String key : supplierInfo.keySet()) {
            sbTitle = supplierSubject.get(key);
            sbContent = supplierDetail.get(key).append(
                getEmailLabel(locale, MAIL_DO_NOTIFICATION_CONTENT_FOOTER)).append(
                    Constants.SYMBOL_NEWLINE);
            emailTo = supplierMailTo.get(key);
            sendMail(
                locale,
                sbTitle.toString(), 
                sbContent.toString(), 
                sbFrom.toString(), 
                emailTo,
                true,
                sCd,
                log);
            // Update SPS_T_DO.NOTIFICATION_MAIL_DO_FLAG = '1' for all DO_ID in spsTDoDomainList.
            // Or update SPS_T_DO.NOTIFICATION_MAIL_KANBAN_FLAG = '1' for all DO_ID in spsTDoDomainList.
            for (BigDecimal keyDoId : supplierDoId.get(key)) {
                spsTDoDomain.setLastUpdateDscId( SupplierPortalConstant.SPS_SYSTEM_NAME );
                spsTDoDomain.setLastUpdateDatetime( commonService.searchSysDate() );
                
                SpsTDoCriteriaDomain spsTDoCriteriaDomain = new SpsTDoCriteriaDomain();
                spsTDoCriteriaDomain.setDoId(keyDoId);
                spsTDoService.updateByCondition(spsTDoDomain, spsTDoCriteriaDomain);
                
            }
        }
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchOnewayKanbanTag(java.util.Locale, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> searchOnewayKanbanTag(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag, String originalCigmaDoNumber, String vendorCode) throws Exception
    {
        // 1    Get CIGMA One Way Kanban data which finished writing to data file  (check with Transmit data control file)
    	System.out.println("spsFlag = " + spsFlag);
    	System.out.println("originalCigmaDoNumber = " + originalCigmaDoNumber);
    	System.out.println("vendorCode = " + vendorCode);
        List<PseudoCigmaOneWayKanbanInformationDomain> pseudoList = 
            CommonWebServiceUtil.searchCigmaDeliveryOrderOneWayKanban(as400Server, 
                spsFlag,
                originalCigmaDoNumber,
                vendorCode);
        Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> resultMap
            = new HashMap<String, List<TransferOneWayKanbanDataFromCigmaDomain>>();
        if( ZERO < pseudoList.size() ){
            for(TransferOneWayKanbanDataFromCigmaDomain domain : convertToCigmaOneWayKanban(pseudoList)){
                if( null == resultMap.get(domain.getCigmaDoNo()) ){
                    resultMap.put(domain.getCigmaDoNo(), 
                        new ArrayList<TransferOneWayKanbanDataFromCigmaDomain>());
                }
                resultMap.get(domain.getCigmaDoNo()).add(domain);
            }
        }
        return resultMap;
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#searchOnewayKanbanTag(java.util.Locale, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> searchOnewayKanbanTagSeparateByVendorCode(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag) throws Exception
    {
        // 1    Get CIGMA One Way Kanban data which finished writing to data file  (check with Transmit data control file)
        List<PseudoCigmaOneWayKanbanInformationDomain> pseudoList = 
            CommonWebServiceUtil.searchCigmaDeliveryOrderOnewayKanbanTagSeparateByVendorCode(as400Server, spsFlag);
        Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> resultMap
            = new HashMap<String, List<TransferOneWayKanbanDataFromCigmaDomain>>();
        if( ZERO < pseudoList.size() ){
            for(TransferOneWayKanbanDataFromCigmaDomain domain : convertToCigmaOneWayKanban(pseudoList)){
                if( null == resultMap.get(domain.getCigmaDoNo()) ){
                    resultMap.put(domain.getCigmaDoNo(), 
                        new ArrayList<TransferOneWayKanbanDataFromCigmaDomain>());
                }
                resultMap.get(domain.getCigmaDoNo()).add(domain);
            }
        }
        return resultMap;
    }
    /**
     * 
     * <p>convert To Cigma One Way Kanban.</p>
     *
     * @param pseudoList List of PseudoCigmaOneWayKanbanInformationDomain
     * @return List of TransferOneWayKanbanDataFromCigmaDomain
     */
    private List<TransferOneWayKanbanDataFromCigmaDomain> convertToCigmaOneWayKanban(
        List<PseudoCigmaOneWayKanbanInformationDomain> pseudoList)
    {
        List<TransferOneWayKanbanDataFromCigmaDomain> resultList = new ArrayList<TransferOneWayKanbanDataFromCigmaDomain>();
        for(PseudoCigmaOneWayKanbanInformationDomain pseudo : pseudoList){
            TransferOneWayKanbanDataFromCigmaDomain transfer = new TransferOneWayKanbanDataFromCigmaDomain();
            transfer.setCigmaDoNo(pseudo.getPseudoCigmaDoNo());
            transfer.setsCd(pseudo.getPseudosCd());
            transfer.setDeliveryDate(pseudo.getPseudoDeliveryDate());
            transfer.setDnItemNumber(pseudo.getPseudoDnItemNumber());
            transfer.setTagSeqNumber(pseudo.getPseudoTagSeqNumber());
            transfer.setKanbanType(pseudo.getPseudoKanbanType());
            transfer.setCapacity(pseudo.getPseudoCapacity());
            transfer.setPartialFlag(pseudo.getPseudoPartialFlag());
            transfer.setKanbanInformation(pseudo.getPseudoKanbanInformation());
            transfer.setKanbanDataType(pseudo.getPseudoKanbanDataType());
            resultList.add(transfer);
        }
        return resultList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactCheckTransferOneWayKanbanFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferOneWayKanbanDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, boolean)
     */
    public List<TransferOneWayKanbanDataFromCigmaDomain> transactCheckTransferOneWayKanbanFromCigma(
        Locale locale,
        List<TransferOneWayKanbanDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        Set<String> originalDoErrorSet,
        boolean bError, 
        String jobId,
        Log log) throws Exception
    {
        List<TransferOneWayKanbanDataFromCigmaDomain> resultList
            = new ArrayList<TransferOneWayKanbanDataFromCigmaDomain>();
        StringBuffer sbErrorMsg = new StringBuffer();
        // 1. Find Supplier Plant Code for each DENSO Part No and find SPS P/O No relate with D/O
        // 1.1  Set initial criteria search vendor in AS400
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = 
            new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        
        // 2.   Validation each row information
        for(TransferOneWayKanbanDataFromCigmaDomain transfer : transferList) {
            // 2.1 Validation for the information has the Vendor in AS400 vendor or not?
            as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            as400VendorCriteriaDomain.setVendorCd(transfer.getsCd());
            List<SpsMAs400VendorDomain> vendorList = 
                spsMAs400VendorService.searchByCondition(as400VendorCriteriaDomain);
            if( null == vendorList || ZERO == vendorList.size() ){
                sbErrorMsg.append("[One way Kanban Tag] ")
                    .append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCigmaDoNo(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                break;
            } // end check not found.
            
            // 2.2 Validation for the information has the part in D/O or not?
            SpsTDoCriteriaDomain criteria = new SpsTDoCriteriaDomain();
            String currentSpsCigmaDoNo = transfer.getCigmaDoNo();
            if(transfer.getCigmaDoNo().length() == Constants.EIGHT){
                currentSpsCigmaDoNo = transfer.getCigmaDoNo().concat(Constants.DEFAULT_REVISION);
            }
            criteria.setCurrentCigmaDoNo(currentSpsCigmaDoNo);
            criteria.setVendorCd(transfer.getsCd());
            criteria.setDataType(transfer.getKanbanDataType());
//            criteria.setLatestRevisionFlg(Constants.STR_ONE);
            List<SpsTDoDomain> spsTDoDomainList = spsTDoService.searchByCondition(criteria);
            if( null == spsTDoDomainList || ZERO == spsTDoDomainList.size() ){
                sbErrorMsg.append("[One Way Kanban Tag] ")
                    .append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCigmaDoNo(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                break;
            }
            resultList.add(transfer);
        }
        if( !EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
            writeLog(log, TWO, sbErrorMsg.toString());
            resultList.clear();
        }
        return resultList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactTransferChgDoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    /*Backup20231010
    public String transactTransferOneWayKanbanFromCigma(
        Locale locale, 
        List<TransferOneWayKanbanDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId,
        List<String> listDoIdUpdate) 
        throws Exception {
        StringBuffer sbErrorMsg = new StringBuffer();
        String cigmaDoNo = null;
        String originalCigmaDoNo = null;
        String sCd = null;
        String strPartialFlag = null;
//        List<BigDecimal> listDoIdUpdate = new ArrayList<BigDecimal>();
        
        // 1.   Insert all Kanban Tag information to SPS database for each CIGMA D/O No.
        for(TransferOneWayKanbanDataFromCigmaDomain transfer : transferList) {
            BigDecimal doId = null;
//            1-1 Search D/O ID
            SpsTDoCriteriaDomain criteria = new SpsTDoCriteriaDomain();
//            originalCigmaDoNo = transfer.getCigmaDoNo();
            String currentSpsCigmaDoNo = transfer.getCigmaDoNo();
//          Start [IN1770]SPS_TK Live environment ; Tag don't show on SPS 
            originalCigmaDoNo = transfer.getCigmaDoNo().trim();
            if(transfer.getCigmaDoNo().trim().length() > Constants.EIGHT){
                originalCigmaDoNo = transfer.getCigmaDoNo().trim().substring(Constants.ZERO, Constants.EIGHT);
            }
//            criteria.setCurrentCigmaDoNo(currentSpsCigmaDoNo);
            criteria.setCigmaDoNo(originalCigmaDoNo);
//          End  [IN1770]SPS_TK Live environment ; Tag don't show on SPS 
            criteria.setVendorCd(transfer.getsCd());
            criteria.setDataType(transfer.getKanbanDataType());
            criteria.setLatestRevisionFlg(Constants.STR_ONE);
            List<SpsTDoDomain> SpsTDoDomainList = spsTDoService.searchByCondition(criteria);
            sCd = transfer.getsCd();
            originalCigmaDoNo = SpsTDoDomainList.get(Constants.ZERO).getCigmaDoNo();
                
            //1-2   Search D/O detail
            for(SpsTDoDomain spsTDoDomain : SpsTDoDomainList){
                SpsTDoDetailCriteriaDomain criteriaDetail = new SpsTDoDetailCriteriaDomain();
                criteriaDetail.setDoId(spsTDoDomain.getDoId());
                criteriaDetail.setDPn(transfer.getDnItemNumber());
                List<SpsTDoDetailDomain> SpsTDoDetailDomainList = spsTDoDetailService.searchByCondition(criteriaDetail);
                if( Constants.ZERO != SpsTDoDetailDomainList.size()){
                    doId = SpsTDoDetailDomainList.get(Constants.ZERO).getDoId();
                    break;
                }
            }
            if( null == doId){
                sbErrorMsg.append("[One Way Kanban Tag] ")
                    .append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCigmaDoNo(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                break;
            }
            SpsTDoDetailCriteriaDomain criteriaDetail = new SpsTDoDetailCriteriaDomain();
            criteriaDetail.setDoId(doId);
            criteriaDetail.setDPn(transfer.getDnItemNumber());
            List<SpsTDoDetailDomain> SpsTDoDetailDomainList = spsTDoDetailService.searchByCondition(criteriaDetail);
            if(!StringUtil.checkNullOrEmpty(SpsTDoDetailDomainList.get(Constants.ZERO).getChgCigmaDoNo())){
                cigmaDoNo = SpsTDoDetailDomainList.get(Constants.ZERO).getChgCigmaDoNo();
            } else {
                cigmaDoNo = originalCigmaDoNo;
            }
            
            
            //1-3 Set information to KanbanTagDomain
            try{
                //Fix issue SPS get data from AS400 on content QR code for exactly 300 digits.
                //AS400 will insert # to tell SPS to know after # is unnessesary
                String[] qrContentFromCigma = transfer.getKanbanInformation().split(SupplierPortalConstant.SPS_SPLIT_QR_CONTENT_SIGN);
                if(transfer.getKanbanType().equals(Constants.STR_FOUR)){
                    qrContentFromCigma = SupplierPortalConstant.QR_CODE_ASN_SYSTEM_ID
                        .concat(SupplierPortalConstant.QR_CODE_ASN_TYPE)
                        .concat(SupplierPortalConstant.SPS_SPLIT_QR_CONTENT_SIGN)
                        .split(SupplierPortalConstant.SPS_SPLIT_QR_CONTENT_SIGN);
                }
                transfer.setKanbanInformation(qrContentFromCigma[Constants.ZERO]);
                
                KanbanTagDomain kanbanTag = new KanbanTagDomain();
                kanbanTag.setDoId(doId);
                kanbanTag.setdPn(transfer.getDnItemNumber());
                kanbanTag.setKanbanTagSeq(transfer.getTagSeqNumber());
                kanbanTag.setKanbanType(transfer.getKanbanType());
                kanbanTag.setCapacity(new BigDecimal(transfer.getCapacity()));
                kanbanTag.setQrContent(transfer.getKanbanInformation()
                    .concat(fillData(SpsTDoDetailDomainList.get(Constants.ZERO).getRcvLane().trim(),
                        SupplierPortalConstant.LENGTH_RCV_LANE))
                    .concat(fillData(cigmaDoNo.trim(),
                        SupplierPortalConstant.LENGTH_CIGMA_DO_NO))
                    .concat(fillData(SpsTDoDetailDomainList.get(Constants.ZERO).getSPn().trim(),
                        SupplierPortalConstant.LENGTH_SUPPLIER_PART_NO))
                    .concat(fillDataNumber(transfer.getKanbanType().trim(),
                        SupplierPortalConstant.LENGTH_QTY_FLOATING, Constants.ZERO)));
                if(transfer.getPartialFlag().equals(Constants.STR_Y)){
                    strPartialFlag = Constants.STR_ONE;
                } else {
                    strPartialFlag = Constants.STR_ZERO;
                }
                kanbanTag.setPartialFlag(strPartialFlag);
                kanbanTag.setCreteDscId(jobId);
                kanbanTag.setCreateDateTime(commonService.searchSysDate());
                kanbanTag.setUpdateDscId(jobId);
                kanbanTag.setUpdateDateTime(commonService.searchSysDate());
                deliveryOrderService.createKanbanTag(kanbanTag);
                
//                20230517 Rungsiwut change logic to update T_SPS_DO
                if(!listDoIdUpdate.contains(doId.toString())){
                    listDoIdUpdate.add(doId.toString());
                }
                
            }catch (ApplicationException e) {
                sbErrorMsg.append("[One Way Kanban Tag] ")
                    .append(  getMessage(locale, ERROR_CD_SP_E6_0062, 
                    transfer.getCigmaDoNo(), transfer.getTagSeqNumber(), "deliveryOrderService.createKanbanTag",
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
            }
        }
//      20230323 Rungsiwut comment
        //2.    Call Web services to CIGMA_REST to update all transferred Kanban tag information status to '2'
//        CommonWebServiceUtil.kanbanResourceUpdateKanbanTag(as400Server, originalCigmaDoNo, sCd, 
//            SupplierPortalConstant.SPS_KANBAN_FLAG_TRANSFERED, jobId);
//        
//        //3.  Update ONE_WAY_KANBAN_TAG_FLAG = '1' on SPS_T_DO to notify e-mail to user
//        try{
//            for(BigDecimal doId : listDoIdUpdate){
//                KanbanTagDomain kanbanTag = new KanbanTagDomain();
//                kanbanTag.setDoId(doId);
//                deliveryOrderService.updateOneWayKanbanTagFlag(kanbanTag);
//            }
//        }catch (ApplicationException e) {
//            sbErrorMsg.append("[One Way Kanban Tag] ")
//                .append(  getMessage(locale, ERROR_CD_SP_E6_0063, 
//                originalCigmaDoNo,
//                EMPTY_STRING ) )
//                .append( SYMBOL_CR_LF );
//        }
        return sbErrorMsg.toString();
    }
    */
    public String transactTransferOneWayKanbanFromCigma(
            Locale locale, 
            List<TransferOneWayKanbanDataFromCigmaDomain> transferList, 
            As400ServerConnectionInformationDomain as400Server, 
            String jobId,
            List<String> listDoIdUpdate) 
            throws Exception {
            StringBuffer sbErrorMsg = new StringBuffer();
            String cigmaDoNo = null;
            String originalCigmaDoNo = null;
            String sCd = null;
            String strPartialFlag = null;
//            List<BigDecimal> listDoIdUpdate = new ArrayList<BigDecimal>();
            Boolean canCreateKanban = true;
            List<KanbanTagDomain> kanbanTagList = new ArrayList<KanbanTagDomain>();
            
            int rowInsertKanban = 0;
            int checkInsertKanban = 0;
            
            // 1.   Insert all Kanban Tag information to SPS database for each CIGMA D/O No.
            for(TransferOneWayKanbanDataFromCigmaDomain transfer : transferList) {
                BigDecimal doId = null;
//                1-1 Search D/O ID
                SpsTDoCriteriaDomain criteria = new SpsTDoCriteriaDomain();
//                originalCigmaDoNo = transfer.getCigmaDoNo();
                String currentSpsCigmaDoNo = transfer.getCigmaDoNo();
//              Start [IN1770]SPS_TK Live environment ; Tag don't show on SPS 
                originalCigmaDoNo = transfer.getCigmaDoNo().trim();
                if(transfer.getCigmaDoNo().trim().length() > Constants.EIGHT){
                    originalCigmaDoNo = transfer.getCigmaDoNo().trim().substring(Constants.ZERO, Constants.EIGHT);
                }
//                criteria.setCurrentCigmaDoNo(currentSpsCigmaDoNo);
                criteria.setCigmaDoNo(originalCigmaDoNo);
//              End  [IN1770]SPS_TK Live environment ; Tag don't show on SPS 
                criteria.setVendorCd(transfer.getsCd());
                criteria.setDataType(transfer.getKanbanDataType());
                criteria.setLatestRevisionFlg(Constants.STR_ONE);
                List<SpsTDoDomain> SpsTDoDomainList = spsTDoService.searchByCondition(criteria);
                sCd = transfer.getsCd();
                originalCigmaDoNo = SpsTDoDomainList.get(Constants.ZERO).getCigmaDoNo();
                    
                //1-2   Search D/O detail
                for(SpsTDoDomain spsTDoDomain : SpsTDoDomainList){
                    SpsTDoDetailCriteriaDomain criteriaDetail = new SpsTDoDetailCriteriaDomain();
                    criteriaDetail.setDoId(spsTDoDomain.getDoId());
                    criteriaDetail.setDPn(transfer.getDnItemNumber());
                    List<SpsTDoDetailDomain> SpsTDoDetailDomainList = spsTDoDetailService.searchByCondition(criteriaDetail);
                    if( Constants.ZERO != SpsTDoDetailDomainList.size()){
                        doId = SpsTDoDetailDomainList.get(Constants.ZERO).getDoId();
                        break;
                    }
                }
                if( null == doId){
                    sbErrorMsg.append("[One Way Kanban Tag] ")
                        .append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                        transfer.getCigmaDoNo(),
                        EMPTY_STRING ) )
                        .append( SYMBOL_CR_LF );
                    break;
                }
                SpsTDoDetailCriteriaDomain criteriaDetail = new SpsTDoDetailCriteriaDomain();
                criteriaDetail.setDoId(doId);
                criteriaDetail.setDPn(transfer.getDnItemNumber());
                List<SpsTDoDetailDomain> SpsTDoDetailDomainList = spsTDoDetailService.searchByCondition(criteriaDetail);
                if(!StringUtil.checkNullOrEmpty(SpsTDoDetailDomainList.get(Constants.ZERO).getChgCigmaDoNo())){
                    cigmaDoNo = SpsTDoDetailDomainList.get(Constants.ZERO).getChgCigmaDoNo();
                } else {
                    cigmaDoNo = originalCigmaDoNo;
                }
                
                
                //1-3 Set information to KanbanTagDomain
                
                //Fix issue SPS get data from AS400 on content QR code for exactly 300 digits.
				//AS400 will insert # to tell SPS to know after # is unnessesary
				String[] qrContentFromCigma = transfer.getKanbanInformation().split(SupplierPortalConstant.SPS_SPLIT_QR_CONTENT_SIGN);
				if(transfer.getKanbanType().equals(Constants.STR_FOUR)){
				    qrContentFromCigma = SupplierPortalConstant.QR_CODE_ASN_SYSTEM_ID
				        .concat(SupplierPortalConstant.QR_CODE_ASN_TYPE)
				        .concat(SupplierPortalConstant.SPS_SPLIT_QR_CONTENT_SIGN)
				        .split(SupplierPortalConstant.SPS_SPLIT_QR_CONTENT_SIGN);
				}
				transfer.setKanbanInformation(qrContentFromCigma[Constants.ZERO]);
				
				KanbanTagDomain kanbanTag = new KanbanTagDomain();
				kanbanTag.setDoId(doId);
				kanbanTag.setdPn(transfer.getDnItemNumber());
				kanbanTag.setKanbanTagSeq(transfer.getTagSeqNumber());
				kanbanTag.setKanbanType(transfer.getKanbanType());
				kanbanTag.setCapacity(new BigDecimal(transfer.getCapacity()));
				kanbanTag.setQrContent(transfer.getKanbanInformation()
				    .concat(fillData(SpsTDoDetailDomainList.get(Constants.ZERO).getRcvLane().trim(),
				        SupplierPortalConstant.LENGTH_RCV_LANE))
				    .concat(fillData(cigmaDoNo.trim(),
				        SupplierPortalConstant.LENGTH_CIGMA_DO_NO))
				    .concat(fillData(SpsTDoDetailDomainList.get(Constants.ZERO).getSPn().trim(),
				        SupplierPortalConstant.LENGTH_SUPPLIER_PART_NO))
				    .concat(fillDataNumber(transfer.getKanbanType().trim(),
				        SupplierPortalConstant.LENGTH_QTY_FLOATING, Constants.ZERO)));
				if(transfer.getPartialFlag().equals(Constants.STR_Y)){
				    strPartialFlag = Constants.STR_ONE;
				} else {
				    strPartialFlag = Constants.STR_ZERO;
				}
				kanbanTag.setPartialFlag(strPartialFlag);
				kanbanTag.setCreteDscId(jobId);
				kanbanTag.setCreateDateTime(commonService.searchSysDate());
				kanbanTag.setUpdateDscId(jobId);
				kanbanTag.setUpdateDateTime(commonService.searchSysDate());
				//deliveryOrderService.createKanbanTag(kanbanTag);
				try{
					rowInsertKanban = deliveryOrderService.insertKanbanTag(kanbanTag);
						
				}catch(Exception e){
					if(e.getCause().toString().contains("ORA-00001: unique constraint")){
						rowInsertKanban++;
					}else{
						throw e;
					}
				}
				
							
				
				//kanbanTagList.add(kanbanTag);
//                  20230517 Rungsiwut change logic to update T_SPS_DO
				//if(!listDoIdUpdate.contains(doId.toString())){
				//    listDoIdUpdate.add(doId.toString());
				//}
				
				if(rowInsertKanban>0){
					if(!listDoIdUpdate.contains(doId.toString())){
					    listDoIdUpdate.add(doId.toString());
					}
				}
            }
            
            //Boolean resultCreate = deliveryOrderService.createKanbanTagList(kanbanTagList);
            
            //2.    Call Web services to CIGMA_REST to update all transferred Kanban tag information status to '2'
            /*
            if (checkInsertKanban==0){
            	
            	originalCigmaDoNo = StringUtil.convertListToVarcharCommaSeperate(originalCigmaDoNo);
				System.out.println(originalCigmaDoNo);
            	String updateKanbanTagResult = CommonWebServiceUtil.kanbanResourceUpdateKanbanTag(as400Server, originalCigmaDoNo, sCd, 
                        SupplierPortalConstant.SPS_KANBAN_FLAG_TRANSFERED, jobId);
            	System.out.println("updateKanbanTagResult : "+updateKanbanTagResult);
                    
                    //3.  Update ONE_WAY_KANBAN_TAG_FLAG = '1' on SPS_T_DO to notify e-mail to user
                    try{
                    	String originalCigmaDoId = StringUtil.convertListToVarcharCommaSeperate(listDoIdUpdate);
						System.out.println(originalCigmaDoId);
						KanbanTagDomain kanbanTag = new KanbanTagDomain();
						kanbanTag.setDoNo(originalCigmaDoId);
						deliveryOrderService.updateOneWayKanbanTagFlag(kanbanTag);

                    }catch (ApplicationException e) {
                        sbErrorMsg.append("[One Way Kanban Tag] ")
                            .append(  getMessage(locale, ERROR_CD_SP_E6_0063, 
                            originalCigmaDoNo,
                            EMPTY_STRING ) )
                            .append( SYMBOL_CR_LF );
                    }
            }
            */
            return sbErrorMsg.toString();
        }
    
    /**
     * If parameter input shorter than parameter len. Append input with space.
     * @param input - String input
     * @param len - target length
     * @return String that lenght equal to parameter len
     * */
    private String fillData(String input, int len) {
        StringBuffer result = new StringBuffer();
        result.append(input);
        for (int i = input.length(); i < len; i++) {
            result.append(Constants.SYMBOL_SPACE);
        }
        return result.toString();
    }
    /**
     * Split input to decimal part and floating part. If decimal part shorter than 
     * allLen - floatLen, fill with zero.<br />
     * If floating part shorter than floatLen, fill with zero.
     * @param input - String input
     * @param allLen - target length for whole string
     * @param floatLen - target length for floating part
     * @return String that lenght equal to parameter allLen
     * */
    private String fillDataNumber(String input, int allLen, int floatLen) {
        StringBuffer result = new StringBuffer();
        StringBuffer regexDot = new StringBuffer();
        // [.]
        regexDot.append(Constants.SYMBOL_OPEN_SQUARE_BRACKET).append(Constants.SYMBOL_DOT)
            .append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET);
        
        input = input.replaceAll(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
        
        String[] splitedInput = input.split(regexDot.toString());
        String decimal = splitedInput[Constants.ZERO];
        String floating = Constants.EMPTY_STRING;
        if (Constants.TWO == splitedInput.length) {
            floating = splitedInput[Constants.ONE];
        }
        
        int decLen = allLen - floatLen;
        int padding = decLen - decimal.length();
        for (int i = Constants.ZERO; i < padding; i++) {
            result.append(Constants.STR_ZERO);
        }
        result.append(decimal);
        
        result.append(floating);
        for (int j = floating.length(); j < floatLen; j++) {
            result.append(Constants.STR_ZERO);
        }
        
        return result.toString();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferDoDataFromCigmaFacadeService#transactCheckTransferDoBackOrderFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, boolean)
     */
    public List<TransferDoDataFromCigmaDomain> transactCheckTransferDoBackOrderFromCigma(
        Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId,
        Log log
    ) throws Exception {
        //error List
        List<TransferDoDataFromCigmaDomain> errorList = new ArrayList<TransferDoDataFromCigmaDomain>(transferList.size());

        List<TransferDoDataFromCigmaDomain> resultList = new ArrayList<TransferDoDataFromCigmaDomain>(transferList.size());   

        StringBuffer sbErrorMsg = new StringBuffer();
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
        for(TransferDoDataFromCigmaDomain transfer : transferList) {
            //set temp transfer (error)
            TransferDoDataFromCigmaDomain tempTransfer = new TransferDoDataFromCigmaDomain();
            tempTransfer.setVendorCd(transfer.getVendorCd());
            tempTransfer.setCigmaDoNo(transfer.getCigmaDoNo());
            tempTransfer.setCigmaDoDetail(null);
            tempTransfer.setDeliveryDate(transfer.getDeliveryDate());
            
            //+++++++ Get Vendor information with condition DCD and VendorCd
            SpsMAs400VendorDomain vendorDo = findSupplier(as400VendorCriteriaDomain, transfer.getVendorCd());
            if( null == vendorDo ){
                //error email
                insertCigmaDoError( locale, 
                    as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, null,
                    STR_ONE,
                    ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND);
                //error message
                sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCigmaDoNo(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                
                errorList.add(transfer);
                break;
            } 
            else {
                transfer.setSCd( vendorDo.getSCd() );
            }
            //++++++++ Get Part information with condition DCD ,DPCD, SCD and DPN
            SpsMDensoSupplierPartsCriteriaDomain partCriteria = new SpsMDensoSupplierPartsCriteriaDomain();
            partCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            partCriteria.setDPcd(transfer.getPlantCode());
            partCriteria.setSCd(transfer.getSCd());
            boolean detailError = false;
            
            // 1-2) Get Supplier Information data from DENSOSupplierPartService
            List<TransferDoDetailDataFromCigmaDomain> removeList = new ArrayList<TransferDoDetailDataFromCigmaDomain>();
            for(TransferDoDetailDataFromCigmaDomain doDetail : transfer.getCigmaDoDetail()) {

                SpsMDensoSupplierPartsDomain partDo = findPart(partCriteria, doDetail.getPartNo(), SupplierPortalConstant.SORT_PART_MASTER);
                
                // check is not found Supplier Part No
                if( null == partDo ) {
                    
                    insertCigmaDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND);
     
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                        transfer.getCigmaDoNo(),
                        doDetail.getPartNo() ) )
                        .append( SYMBOL_CR_LF );
                    //remove List transfer
                    removeList.add(doDetail);

                    detailError = true;
                    continue;
                }
                String sPcd = partDo.getSPcd();
                doDetail.setSPcd(sPcd);
                transfer.setSPcd(sPcd);
                if (detailError) {
                    continue;
                }
            } 
            //remove detail that no doPart
            if(removeList != null || removeList.size() != 0){
                tempTransfer.setCigmaDoDetail(removeList);
            }
            for(TransferDoDetailDataFromCigmaDomain rm: removeList){
                transfer.getCigmaDoDetail().remove(rm);
            }
            //++++++++  Check part in DO  
            
            List<TransferDoDetailDataFromCigmaDomain> removeDodList = new ArrayList<TransferDoDetailDataFromCigmaDomain>();

            for(TransferDoDetailDataFromCigmaDomain doDetail : transfer.getCigmaDoDetail()) {
                String dPn = doDetail.getPartNo();
                DeliveryOrderDomain doParameterHeader = new DeliveryOrderDomain();
                doParameterHeader.setDoHeader( new SpsTDoDomain() );
                doParameterHeader.setDoDetail( new SpsTDoDetailDomain() );
                doParameterHeader.getDoHeader().setVendorCd(transfer.getVendorCd());
                doParameterHeader.getDoHeader().setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
                doParameterHeader.getDoHeader().setCigmaDoNo( transfer.getCigmaDoNo());
                doParameterHeader.getDoHeader().setSCd(transfer.getSCd());
                doParameterHeader.getDoHeader().setSPcd(transfer.getSPcd());
                doParameterHeader.getDoHeader().setDataType(transfer.getDataType());
                doParameterHeader.getDoDetail().setDPn(dPn);
                doParameterHeader.getDoHeader().setDeliveryDatetime( parseToTimestamp(appendsString(transfer.getDeliveryDate(), transfer.getDeliveryTime()), PATTERN_YYYYMMDD_HHMM) );
                
                //List<SpsTDoDomain> doDetailSearch = deliveryOrderService.searchExistDeliveryOrder(doParameterHeader);
                List<SpsTDoDetailDomain> doDetailSearch = deliveryOrderService.searchExistDeliveryOrderBackOrderDetail(doParameterHeader);
                
                if(doDetailSearch == null || doDetailSearch.size() == 0){
                    insertCigmaDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST);
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0079) ).append( SYMBOL_CR_LF );
                    removeDodList.add(doDetail);
                    break;
                }
                //check asn
                
                //for(SpsTDoDomain dd : doDetailSearch){
                for(SpsTDoDetailDomain doDetailOld : doDetailSearch){
                    AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
                    criteria.setDoId(doDetailOld.getDoId().toString());
                    criteria.setDPn(dPn);
                    List<AsnMaintenanceReturnDomain> doGroupAsnResultList = deliveryOrderService.searchDoGroupAsn(criteria);
                    //should be one
                    if(doGroupAsnResultList.size()== 1 && doGroupAsnResultList != null){
                        AsnMaintenanceReturnDomain doGroupAsnResult =  doGroupAsnResultList.get(0);
                        BigDecimal currentQty = new BigDecimal(doDetail.getCurrentOrderQty());
                        BigDecimal totalShippedQty = doGroupAsnResult.getTotalShippedQty();
                        BigDecimal originalOrderQty = doDetailOld.getCurrentOrderQty();
                        /*
                        if( (currentQty.equals(totalShippedQty)) || (totalShippedQty.equals(BigDecimal.ZERO)) ){
                            //pass
                        }
                        else{
                            insertCigmaDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN);
                            sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0080) ).append( SYMBOL_CR_LF );
                            removeDodList.add(doDetail);
                            continue;
                        }
                        */
                        if (currentQty.compareTo(totalShippedQty)<0){
                            insertCigmaDoErrorBackorder( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN, originalOrderQty);
                            sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0080) ).append( SYMBOL_CR_LF );
                            removeDodList.add(doDetail);
                            continue;
                        }
                    }
                }

            }
          //remove dod
            for(TransferDoDetailDataFromCigmaDomain rm : removeDodList){
                transfer.getCigmaDoDetail().remove(rm);
                tempTransfer.getCigmaDoDetail().add(rm);
            }
           
            if(tempTransfer != null){
                errorList.add(tempTransfer);
            }
          //set resultList is null when no detail
            if(transfer.getCigmaDoDetail().size() == 0 || transfer.getCigmaDoDetail() == null){
                transfer = null;
            }
            resultList.add(transfer);

        } 
        //if have error update fail flag to CIGMA
        if( ZERO < sbErrorMsg.length() ) {
            writeLog(log, TWO, sbErrorMsg.toString());
            
            String partNoError = null;
            try{
                partNoError = CommonWebServiceUtil.updateCigmaDeliveryOrder( as400Server, errorList, STR_FIVE, jobId );
            }catch(Exception e){
                
            }     
        }
        
        boolean isNull= true;
        for(TransferDoDataFromCigmaDomain r : resultList){
            if(r != null){
                isNull = false;
            }
        }
        if(isNull){
            resultList = null;
        }
        return resultList;
    }
    
    public void transactTransferDoBackOrderFromCigma(Locale locale, List<TransferDoDataFromCigmaDomain> transferList, As400ServerConnectionInformationDomain as400Server, String jobId) 
        throws Exception{
        
            //update back order detail qty and  shipment status != 'CPS'
        SpsTDoDetailDomain doDetailUpdate = new SpsTDoDetailDomain();
        StringBuffer sbErrorMsg = new StringBuffer();
        for(TransferDoDataFromCigmaDomain transfer : transferList){
            //search for get doId
            String dPn = transfer.getCigmaDoDetail().get(0).getPartNo();
            
            DeliveryOrderDomain doParameterHeader = new DeliveryOrderDomain();
            doParameterHeader.setDoHeader( new SpsTDoDomain() );
            doParameterHeader.setDoDetail( new SpsTDoDetailDomain() );
            doParameterHeader.getDoHeader().setVendorCd(transfer.getVendorCd());
            doParameterHeader.getDoHeader().setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            doParameterHeader.getDoHeader().setCigmaDoNo( transfer.getCigmaDoNo());
            doParameterHeader.getDoHeader().setSCd(transfer.getSCd());
            doParameterHeader.getDoHeader().setSPcd(transfer.getSPcd());
            doParameterHeader.getDoHeader().setDataType(transfer.getDataType());
            doParameterHeader.getDoDetail().setDPn(dPn);
            doParameterHeader.getDoHeader().setDeliveryDatetime( parseToTimestamp(appendsString(transfer.getDeliveryDate(), transfer.getDeliveryTime()), PATTERN_YYYYMMDD_HHMM) );
            
            //header should be one
            List<SpsTDoDomain> doList = deliveryOrderService.searchExistDeliveryOrder(doParameterHeader);            
            BigDecimal doId = doList.get(0).getDoId();
           
            //update detail
            boolean isHeaderStatusCPS = true;
            int issCount = 0;
            int ptsCount = 0;
            int cpsCount = 0;
            int cclCount = 0;
            String shipmentStatusHeader = doList.get(0).getShipmentStatus();
            for(TransferDoDetailDataFromCigmaDomain detail : transfer.getCigmaDoDetail()){
                
                //search old detail
                SpsTDoDetailCriteriaDomain searchD = new SpsTDoDetailCriteriaDomain();
                searchD.setDoId(doId);
                searchD.setDPn(detail.getPartNo());
                List<SpsTDoDetailDomain> detaiLSearch = spsTDoDetailService.searchByCondition(searchD);
                //search asn
                AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
                criteria.setDoId(doId.toString());
                criteria.setDPn( detail.getPartNo());
                List<AsnMaintenanceReturnDomain> doGroupAsnResultList = deliveryOrderService.searchDoGroupAsn(criteria);
                if(doGroupAsnResultList != null && doGroupAsnResultList.size() == 1){
                    AsnMaintenanceReturnDomain doGroupAsnResult =  doGroupAsnResultList.get(0);
                    BigDecimal currentQty = new BigDecimal(detail.getCurrentOrderQty());
                    BigDecimal totalShippedQty = doGroupAsnResult.getTotalShippedQty();
                    //totalShippedQty == 0 ===> no asn 
                    /*
                    if(totalShippedQty.equals(BigDecimal.ZERO)){
                        if(currentQty.equals(BigDecimal.ZERO)){
                            doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                        }
                        else{
                            doDetailUpdate.setPnShipmentStatus(detaiLSearch.get(0).getPnShipmentStatus());
                        }
                    }
                    else{
                        if(currentQty.equals(totalShippedQty)){
                            doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                        }
                    }
                    */
                    if(totalShippedQty.equals(BigDecimal.ZERO) && currentQty.equals(BigDecimal.ZERO)){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CCL);
                    }else if(totalShippedQty.compareTo(BigDecimal.ZERO) > 0 && currentQty.compareTo(totalShippedQty) > 0){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
                    }else if(totalShippedQty.equals(currentQty)){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                    }else{
                        doDetailUpdate.setPnShipmentStatus(detaiLSearch.get(0).getPnShipmentStatus());
                    }
                    
                }else{
                    //not found ASN
                    if(detail.getCurrentOrderQty().equals("0")){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CCL);
                    }
                }
                
                doDetailUpdate.setDoId(doId);
                doDetailUpdate.setDPn(detail.getPartNo());
                doDetailUpdate.setCurrentOrderQty(new BigDecimal(detail.getCurrentOrderQty()));
                doDetailUpdate.setLastUpdateDatetime(commonService.searchSysDate());
                doDetailUpdate.setNoOfBoxes(new BigDecimal(detail.getNoOfBoxes()));
                
                try{
                    deliveryOrderService.updateBackOrderDetail(doDetailUpdate);
                }
                catch (ApplicationException e) {
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0083) ).append( SYMBOL_CR_LF );
                    break;
                }    
                
            }
            //check header status from detail status
            //search all detail again after update
            SpsTDoDetailCriteriaDomain searchDetailCheck = new SpsTDoDetailCriteriaDomain();
            searchDetailCheck.setDoId(doId);
            List<SpsTDoDetailDomain> detaiLSearchCheck = spsTDoDetailService.searchByCondition(searchDetailCheck);
            if(detaiLSearchCheck != null && detaiLSearchCheck.size() != 0){
                for(SpsTDoDetailDomain d : detaiLSearchCheck){
                    if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_ISS)){
                        issCount++;
                    }
                    else if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_PTS)){
                        ptsCount++;
                    }
                    else if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_CPS)){
                        cpsCount++;
                    }
                    else if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_CCL)){
                        cclCount++;
                    }
                }
            } 
            if(cclCount == detaiLSearchCheck.size()){
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_CCL;
            }else if(issCount == detaiLSearchCheck.size() || (issCount != detaiLSearchCheck.size() && (issCount + cclCount) == detaiLSearchCheck.size())){
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_ISS;
            }else if (cpsCount == detaiLSearchCheck.size() ||(cpsCount != detaiLSearchCheck.size() && (cpsCount + cclCount) == detaiLSearchCheck.size())){
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_CPS;
            }else{
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_PTS;
            }
             
            SpsTDoDomain doHeaderUpdate =  new SpsTDoDomain();
            doHeaderUpdate.setDoId(doId);
            doHeaderUpdate.setShipmentStatus(shipmentStatusHeader);
            doHeaderUpdate.setLastUpdateDatetime(commonService.searchSysDate());
            try{
                deliveryOrderService.updateBackOrderHeader(doHeaderUpdate); 
            }      
            catch (ApplicationException e) {
                sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0084) ).append( SYMBOL_CR_LF );
                break;
            }

        }
        //update CIGMA flag
        try{
            CommonWebServiceUtil.updateCigmaDeliveryOrder( as400Server, transferList, STR_FOUR, jobId );
        } catch (ApplicationException e) {
            
        }
    }
        
    public List<TransferChangeDoDataFromCigmaDomain> transactCheckTransferChangeDoBackOrderFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId,
        Log log
    ) throws Exception{
        
        List<TransferChangeDoDataFromCigmaDomain> errorList = new ArrayList<TransferChangeDoDataFromCigmaDomain>(transferList.size());

        List<TransferChangeDoDataFromCigmaDomain> resultList = new ArrayList<TransferChangeDoDataFromCigmaDomain>(transferList.size());   

        StringBuffer sbErrorMsg = new StringBuffer();
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
        for(TransferChangeDoDataFromCigmaDomain transfer : transferList) {
            
            TransferChangeDoDataFromCigmaDomain tempTransfer = new TransferChangeDoDataFromCigmaDomain();
            tempTransfer.setVendorCd(transfer.getVendorCd());
            tempTransfer.setOriginalDelivery(transfer.getOriginalDelivery());
            tempTransfer.setCurrentDelivery(transfer.getCurrentDelivery());
            tempTransfer.setCigmaDoDetail(null);
            tempTransfer.setDeliveryDate(transfer.getDeliveryDate());
            
            //+++++++ Get Vendor information with condition DCD and VendorCd
            SpsMAs400VendorDomain vendorDo = findSupplier(as400VendorCriteriaDomain, transfer.getVendorCd());
            // check is not found Supplier Code.
            if( null == vendorDo ){
                // (1) Keep CIGMA Error data into SPS_CIGMA_DO_ERROR 
                // (Keep all data from the same CIGMA D/O No)
                insertCigmaChgDoError( locale, 
                    as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, null,
                    STR_ONE,
                    ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND);
                
                sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                    transfer.getCurrentDelivery(),
                    EMPTY_STRING ) )
                    .append( SYMBOL_CR_LF );
                errorList.add(transfer);
                break;
            } // end check not found.
            else {
                transfer.setSCd( vendorDo.getSCd() );
            }
            //++++++++ Get Part information with condition DCD ,DPCD, SCD and DPN
            SpsMDensoSupplierPartsCriteriaDomain partCriteria = new SpsMDensoSupplierPartsCriteriaDomain();
            partCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            partCriteria.setDPcd(transfer.getPlantCode());
            partCriteria.setSCd(transfer.getSCd());
            boolean detailError = false;
            
            
            List<TransferChangeDoDetailDataFromCigmaDomain> removeChangePartList = new ArrayList<TransferChangeDoDetailDataFromCigmaDomain>();
            for(TransferChangeDoDetailDataFromCigmaDomain doDetail : transfer.getCigmaDoDetail()) {

                SpsMDensoSupplierPartsDomain partDo = findPart(partCriteria, doDetail.getPartNo(), SupplierPortalConstant.SORT_PART_MASTER);
                
                // check is not found Supplier Part No
                if( null == partDo ) { 
                    insertCigmaChgDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND);
     
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0021, 
                        transfer.getCurrentDelivery(),
                        doDetail.getPartNo() ) )
                        .append( SYMBOL_CR_LF );    
                    removeChangePartList.add(doDetail);
                    
                    detailError = true;
                    continue;
                }
                String sPcd = partDo.getSPcd();
                doDetail.setSPcd(sPcd);
                transfer.setSPcd(sPcd);
                
                if (detailError) {
                    continue;
                }
            }
            if(removeChangePartList != null || removeChangePartList.size() != 0){
                tempTransfer.setCigmaDoDetail(removeChangePartList);
            }
            for(TransferChangeDoDetailDataFromCigmaDomain rm: removeChangePartList){
                transfer.getCigmaDoDetail().remove(rm);
            }
            
          //++++++++  Check part in DO  
            List<TransferChangeDoDetailDataFromCigmaDomain> removeDodList = new ArrayList<TransferChangeDoDetailDataFromCigmaDomain>();

            for(TransferChangeDoDetailDataFromCigmaDomain doDetail : transfer.getCigmaDoDetail()) {
                String dPn = doDetail.getPartNo();
                DeliveryOrderDomain doParameterHeader = new DeliveryOrderDomain();
                doParameterHeader.setDoHeader( new SpsTDoDomain() );
                doParameterHeader.setDoDetail( new SpsTDoDetailDomain() );
                doParameterHeader.getDoHeader().setVendorCd(transfer.getVendorCd());
                doParameterHeader.getDoHeader().setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
                doParameterHeader.getDoHeader().setCigmaDoNo( transfer.getOriginalDelivery());
                doParameterHeader.getDoHeader().setSCd(transfer.getSCd());
                doParameterHeader.getDoHeader().setSPcd(transfer.getSPcd());
                doParameterHeader.getDoHeader().setDataType(transfer.getDataType());
                doParameterHeader.getDoDetail().setDPn(dPn);
                doParameterHeader.getDoHeader().setDeliveryDatetime( parseToTimestamp(appendsString(transfer.getDeliveryDate(), transfer.getDeliveryTime()), PATTERN_YYYYMMDD_HHMM) );
                
                
                List<SpsTDoDomain> doDetailSearch = deliveryOrderService.searchExistDeliveryOrder(doParameterHeader);
                
                if(doDetailSearch == null || doDetailSearch.size() == 0){
                    insertCigmaChgDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST);
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0081) ).append( SYMBOL_CR_LF );
                    removeDodList.add(doDetail);
                    break;
                }
                //check asn
                
                for(SpsTDoDomain dd : doDetailSearch){
                    AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
                    criteria.setDoId(dd.getDoId().toString());
                    criteria.setDPn(dPn);
                    List<AsnMaintenanceReturnDomain> doGroupAsnResultList = deliveryOrderService.searchDoGroupAsn(criteria);
                    //should be one
                    if(doGroupAsnResultList.size()== 1 && doGroupAsnResultList != null){
                        AsnMaintenanceReturnDomain doGroupAsnResult =  doGroupAsnResultList.get(0);
                        BigDecimal currentQty = new BigDecimal(doDetail.getCurrentOrderQty());
                        BigDecimal totalShippedQty = doGroupAsnResult.getTotalShippedQty();
                        /*
                        if( (currentQty.equals(totalShippedQty)) || (totalShippedQty.equals(BigDecimal.ZERO)) ){
                            //pass
                        }
                        else{
                            insertCigmaChgDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN);
                            sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0082) ).append( SYMBOL_CR_LF );
                            removeDodList.add(doDetail);
                            continue;
                        }
                        */
                        if(currentQty.compareTo(totalShippedQty)<0){
                            insertCigmaChgDoError( locale, as400Server.getSpsMCompanyDensoDomain(), transfer, doDetail, STR_ONE, ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN);
                            sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0082) ).append( SYMBOL_CR_LF );
                            removeDodList.add(doDetail);
                            continue;
                        }
                    }
                }

            }
          //remove dod
            for(TransferChangeDoDetailDataFromCigmaDomain rm : removeDodList){
                transfer.getCigmaDoDetail().remove(rm);
                tempTransfer.getCigmaDoDetail().add(rm);
            }
            
            if(tempTransfer != null){
                errorList.add(tempTransfer);
            }
          //set resultList is null when no detail
            if(transfer.getCigmaDoDetail().size() == 0 || transfer.getCigmaDoDetail() == null){
                transfer = null;
            }
            resultList.add(transfer);
            
        } 
        //if have error update fail flag to CIGMA
        if( ZERO < sbErrorMsg.length() ) {
            writeLog(log, TWO, sbErrorMsg.toString());
            resultList.clear();
            
            String partNoError = null;
            try{
                partNoError = CommonWebServiceUtil.updateCigmaChangeDeliveryOrder( as400Server, errorList, STR_FIVE, jobId );
            }catch(Exception e){
                
            }     
        }

        boolean isNull= true;
        for(TransferChangeDoDataFromCigmaDomain r : resultList){
            if(r != null){
                isNull = false;
            }
        }
        if(isNull){
            resultList = null;
        }
        
        return resultList;
    }
    
    public void transactTransferChangeDoBackOrderFromCigma(Locale locale, List<TransferChangeDoDataFromCigmaDomain> transferList, As400ServerConnectionInformationDomain as400Server, String jobId) 
        throws Exception{
        
            //update back order detail qty and  shipment status = 'CPS'
        SpsTDoDetailDomain doDetailUpdate = new SpsTDoDetailDomain();
        StringBuffer sbErrorMsg = new StringBuffer();
        for(TransferChangeDoDataFromCigmaDomain transfer : transferList){
            //search for get doId
            String dPn = transfer.getCigmaDoDetail().get(0).getPartNo();
            
            DeliveryOrderDomain doParameterHeader = new DeliveryOrderDomain();
            doParameterHeader.setDoHeader( new SpsTDoDomain() );
            doParameterHeader.setDoDetail( new SpsTDoDetailDomain() );
            doParameterHeader.getDoHeader().setVendorCd(transfer.getVendorCd());
            doParameterHeader.getDoHeader().setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            doParameterHeader.getDoHeader().setCigmaDoNo( transfer.getOriginalDelivery());
            doParameterHeader.getDoHeader().setSCd(transfer.getSCd());
            doParameterHeader.getDoHeader().setSPcd(transfer.getSPcd());
            doParameterHeader.getDoHeader().setDataType(transfer.getDataType());
            doParameterHeader.getDoDetail().setDPn(dPn);
            doParameterHeader.getDoHeader().setDeliveryDatetime( parseToTimestamp(appendsString(transfer.getDeliveryDate(), transfer.getDeliveryTime()), PATTERN_YYYYMMDD_HHMM) );
            
            //header should be one
            List<SpsTDoDomain> doList = deliveryOrderService.searchExistDeliveryOrder(doParameterHeader);            
            BigDecimal doId = doList.get(0).getDoId();
            
            //update detail
            boolean isHeaderStatusCPS = true;
            String shipmentStatusHeader = doList.get(0).getShipmentStatus();
            int issCount = 0;
            int ptsCount = 0;
            int cpsCount = 0;
            int cclCount = 0;
            for(TransferChangeDoDetailDataFromCigmaDomain detail : transfer.getCigmaDoDetail()){
                
                //search old detail
                SpsTDoDetailCriteriaDomain searchD = new SpsTDoDetailCriteriaDomain();
                searchD.setDoId(doId);
                searchD.setDPn(detail.getPartNo());
                List<SpsTDoDetailDomain> detaiLSearch = spsTDoDetailService.searchByCondition(searchD);
                //search asn
                AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
                criteria.setDoId(doId.toString());
                criteria.setDPn( detail.getPartNo());
                List<AsnMaintenanceReturnDomain> doGroupAsnResultList = deliveryOrderService.searchDoGroupAsn(criteria);
                if(doGroupAsnResultList != null && doGroupAsnResultList.size() == 1){
                    AsnMaintenanceReturnDomain doGroupAsnResult =  doGroupAsnResultList.get(0);
                    BigDecimal currentQty = new BigDecimal(detail.getCurrentOrderQty());
                    BigDecimal totalShippedQty = doGroupAsnResult.getTotalShippedQty();
                    //totalShippedQty == 0 ===> no asn 
                    /*
                    if(totalShippedQty.equals(BigDecimal.ZERO)){
                        if(currentQty.equals(BigDecimal.ZERO)){
                            doDetailUpdate.setPnShipmentStatus("CPS");
                        }
                        else{
                            doDetailUpdate.setPnShipmentStatus(detaiLSearch.get(0).getPnShipmentStatus());
                        }
                    }
                    else{
                        if(currentQty.equals(totalShippedQty)){
                            doDetailUpdate.setPnShipmentStatus("CPS");
                        }
                    }
                    */
                    if(totalShippedQty.equals(BigDecimal.ZERO) && currentQty.equals(BigDecimal.ZERO)){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CCL);
                    }else if(totalShippedQty.compareTo(BigDecimal.ZERO) > 0 && currentQty.compareTo(totalShippedQty) > 0){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
                    }else if(totalShippedQty.equals(currentQty)){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CPS);
                    }else{
                        doDetailUpdate.setPnShipmentStatus(detaiLSearch.get(0).getPnShipmentStatus());
                    }
                }else{
                    //not found ASN
                    if(detail.getCurrentOrderQty().equals("0")){
                        doDetailUpdate.setPnShipmentStatus(Constants.SHIPMENT_STATUS_CCL);
                    }
                }
                
                doDetailUpdate.setDoId(doId);
                doDetailUpdate.setDPn(detail.getPartNo());
                doDetailUpdate.setCurrentOrderQty(new BigDecimal(detail.getCurrentOrderQty()));
                doDetailUpdate.setLastUpdateDatetime(commonService.searchSysDate());
                doDetailUpdate.setNoOfBoxes(new BigDecimal(detail.getNoOfBoxes()));
                try{
                    deliveryOrderService.updateBackOrderDetail(doDetailUpdate);
                }
                catch (ApplicationException e) {
                    sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0083) ).append( SYMBOL_CR_LF );
                    break;
                } 
            }
            //check header status from detail status
            //search all detail again after update
            SpsTDoDetailCriteriaDomain searchDetailCheck = new SpsTDoDetailCriteriaDomain();
            searchDetailCheck.setDoId(doId);
            List<SpsTDoDetailDomain> detaiLSearchCheck = spsTDoDetailService.searchByCondition(searchDetailCheck);
            if(detaiLSearchCheck != null && detaiLSearchCheck.size() != 0){
                for(SpsTDoDetailDomain d : detaiLSearchCheck){
                    if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_ISS)){
                        issCount++;
                    }
                    else if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_PTS)){
                        ptsCount++;
                    }
                    else if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_CPS)){
                        cpsCount++;
                    }
                    else if(d.getPnShipmentStatus().equals(Constants.SHIPMENT_STATUS_CCL)){
                        cclCount++;
                    }
                }
            } 
            if(cclCount == detaiLSearchCheck.size()){
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_CCL;
            }else if(issCount == detaiLSearchCheck.size() || (issCount != detaiLSearchCheck.size() && (issCount + cclCount) == detaiLSearchCheck.size())){
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_ISS;
            }else if (cpsCount == detaiLSearchCheck.size() ||(cpsCount != detaiLSearchCheck.size() && (cpsCount + cclCount) == detaiLSearchCheck.size())){
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_CPS;
            }else{
                shipmentStatusHeader = Constants.SHIPMENT_STATUS_PTS;
            }
                //update header
            SpsTDoDomain doHeaderUpdate =  new SpsTDoDomain();
            doHeaderUpdate.setDoId(doId);
            doHeaderUpdate.setShipmentStatus(shipmentStatusHeader);
            doHeaderUpdate.setLastUpdateDatetime(commonService.searchSysDate());
            try{
                deliveryOrderService.updateBackOrderHeader(doHeaderUpdate); 
            }      
            catch (ApplicationException e) {
                sbErrorMsg.append(  getMessage(locale, ERROR_CD_SP_E6_0084) ).append( SYMBOL_CR_LF );
                break;
            }

        }
        //update CIGMA flag
        try{
            CommonWebServiceUtil.updateCigmaChangeDeliveryOrder( as400Server, transferList, "4", jobId );
        } catch (ApplicationException e) {
            
        }
    }
    //21082021 Backorder
    
    private void insertCigmaDoErrorBackorder( Locale locale, SpsMCompanyDensoDomain criteria, TransferDoDataFromCigmaDomain transfer, 
        TransferDoDetailDataFromCigmaDomain transferDetail, String mailFlag, String errorType, BigDecimal originalOrderQty ) throws ApplicationException {
        try {
            SpsCigmaDoErrorDomain errorDo = new SpsCigmaDoErrorDomain();
            errorDo.setCigmaDoNo( transfer.getCigmaDoNo().trim() );
            errorDo.setDataType( transfer.getDataType() );
            if( null != transfer.getSCd() ){
                errorDo.setSCd( transfer.getSCd() );
            }else{
                errorDo.setSCd( null );
            }
            errorDo.setIssueDate( new Date(transfer.getIssueDate().getTime()) );
            errorDo.setMailFlg( mailFlag );
            errorDo.setErrorTypeFlg(errorType);
            errorDo.setDCd( criteria.getDCd().trim() );
            errorDo.setDPcd( transfer.getPlantCode().trim() );
            errorDo.setVendorCd( transfer.getVendorCd() );
            errorDo.setDeliveryDate( parseToSqlDate(transfer.getDeliveryDate(), PATTERN_YYYYMMDD) );
            errorDo.setDeliveryTime( transfer.getDeliveryTime() );
            errorDo.setUpdateByUserId( transfer.getUpdateByUserId().trim() );

            if (null != transferDetail) {
                errorDo.setSPcd( transferDetail.getSPcd() );
                errorDo.setDPn( transferDetail.getPartNo() );
                errorDo.setCurrentOrderQty( toBigDecimal(transferDetail.getCurrentOrderQty()) );
                errorDo.setOriginalOrderQty( originalOrderQty );
            }
            
            spsCigmaDoErrorService.create(errorDo);
        } catch (ApplicationException e) {
            String parnNo = EMPTY_STRING;
            String orderQty = EMPTY_STRING;
            
            if (null != transferDetail) {
                parnNo = transferDetail.getPartNo();
                orderQty = transferDetail.getCurrentOrderQty();
            }
            
            throwsErrorMessage(locale, 
                ERROR_CD_SP_E6_0025,
                Constants.DO, transfer.getCigmaDoNo(), parnNo, orderQty,
                TABLE_CIGMA_DO_NAME
            );
        }
    }
    
    private void initialDoMailBackorder( Locale locale, List<String> emailTo, StringBuffer sbContent, String errorTypeFlag, String doType) throws Exception 
    {
        String hearderBackorder = MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER;
        if(doType.equalsIgnoreCase("CHG")){
            hearderBackorder = MAIL_CIGMA_CHG_DO_ERROR_HEADER_BACKORDER;
        }
        
        sbContent.append(getEmailLabel(locale, hearderBackorder));
        
        // [IN070] add list of email to send in content
        if (null != emailTo && ZERO != emailTo.size()) {
            String emailToString = StringUtil.convertListToStringCommaSeperate(emailTo);
            sbContent.append(appendsString(
                SYMBOL_NEWLINE
                , getEmailLabel(locale, MAIL_CIGMA_ERROR_PERSON_IN_CHARGE)
                , emailToString
                , Constants.SYMBOL_END_PARAGRAPH
                , SYMBOL_NEWLINE));
        }
        
    }
    
    private void fillDoErrorMailBackorder(Locale locale, StringBuffer sbContent, TransferDoErrorEmailDomain domain) throws Exception
    {
        if(ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTMATCH_ASN));
        } else if(ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN));
        }

        // #vendorCd# #sCd# #dCd# #issueDate#
        if(domain.getSCd()== null){
            domain.setSCd("");
        }
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE)
                .replaceAll(MAIL_VENDOR_CD_REPLACEMENT, domain.getVendorCd())
                .replaceAll(MAIL_SCD_REPLACEMENT, domain.getSCd()) 
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDCd())
                .replaceAll(MAIL_ISSUE_DATE_REPLACEMENT, format(domain.getIssueDate(), PATTERN_YYYYMMDD_SLASH)));
        
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE2));
        
    } 
    
    private void fillDoDetilBackorder( StringBuffer sbContent, Locale locale, String errorType, TransferDoErrorEmailDomain domain, TransferDoErrorEmailDetailDomain detail) throws Exception
    {
        /* [User Name] [D_CD] [D_PCD] [CIGMA D/O No.] [VENDOR_CD] [D/O Type] [D_PN] [delivery date] [Delivery Time]
         * [S_PCD] [Supplier P/No] [Current QTY] [Backorder QTY]
         */
        String username = nullToEmpty(detail.getCigmaDoError().getUpdateByUserId());
        if( EMPTY_STRING.equals(username) ){
            username = SYMBOL_NBSP;
        }
        String dcd = nullToEmpty(domain.getDCd());
        if( EMPTY_STRING.equals(dcd) ){
            dcd = SYMBOL_NBSP;
        }
        String dpcd = nullToEmpty(domain.getDPcd());
        if( EMPTY_STRING.equals(dpcd) ){
            dpcd = SYMBOL_NBSP;
        }
        String vendorcd = nullToEmpty(domain.getVendorCd());
        if( EMPTY_STRING.equals(vendorcd) ){
            vendorcd = SYMBOL_NBSP;
        }
        String dataType = nullToEmpty(detail.getCigmaDoError().getDataType());
        if( EMPTY_STRING.equals(dataType) ){
            dataType = SYMBOL_NBSP;
        }
        String dpn = nullToEmpty(detail.getCigmaDoError().getDPn());
        if( EMPTY_STRING.equals(dpn) ){
            dpn = SYMBOL_NBSP;
        }
        String deliveryDate = format(domain.getDeliveryDate()
            , PATTERN_YYYYMMDD_SLASH);
        if( EMPTY_STRING.equals(deliveryDate) ){
            deliveryDate = SYMBOL_NBSP;
        }
        String deliveryTime = nullToEmpty(domain.getDeliveryTime());
        if( EMPTY_STRING.equals(deliveryTime) ){
            deliveryTime = SYMBOL_NBSP;
        }else{
            deliveryTime = deliveryTime.substring(0,2)+":"+deliveryTime.substring(2,4);
        }

        String spcd = nullToEmpty(domain.getSPcd());
        if( EMPTY_STRING.equals(spcd) ){
            spcd = SYMBOL_NBSP;
        }
        String sPn = nullToEmpty(detail.getDensoSupplerParts().getSPn());
        if( EMPTY_STRING.equals(sPn) ){
            sPn = SYMBOL_NBSP;
        }
        
        String originalOrderQty = nullToEmpty(domain.getOriginalOrderQty());
        if( EMPTY_STRING.equals(originalOrderQty) ){
            originalOrderQty = SYMBOL_NBSP;
        }
        
        String currentOrderQty = nullToEmpty(domain.getCurrentOrderQty());
        if( EMPTY_STRING.equals(currentOrderQty) ){
            currentOrderQty = SYMBOL_NBSP;
        }
        
        sbContent.append(appendsString(String.format(
            getEmailLabel(locale , MAIL_CIGMA_DO_ERROR_DETAIL_BACKORDER)
            , username, dcd, dpcd, domain.getCigmaDoNo(), vendorcd, dataType, dpn
            , deliveryDate, deliveryTime
            , spcd, sPn
            , originalOrderQty, currentOrderQty)
            , SYMBOL_CR_LF));
    }
    
    private void fillChgDoErrorMailBackorder(Locale locale, StringBuffer sbContent, TransferChgDoErrorEmailDomain domain) throws Exception
    {
        if(ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND));
        } else if(ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN.equals(domain.getErrorTypeFlg().trim())){
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN));
        }

        // #vendorCd# #sCd# #dCd# #issueDate#
        if(domain.getSCd()==null){
            domain.setSCd("");
        }
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE)
                .replaceAll(MAIL_VENDOR_CD_REPLACEMENT, domain.getVendorCd())
                .replaceAll(MAIL_SCD_REPLACEMENT, domain.getSCd()) 
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDCd())
                .replaceAll(MAIL_ISSUE_DATE_REPLACEMENT, format(domain.getIssueDate(), PATTERN_YYYYMMDD_SLASH)));
        
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE2));
        
    } 
    
    private void fillChgDoDetilBackorder( StringBuffer sbContent, Locale locale, String errorType, TransferChgDoErrorEmailDomain domain, TransferChgDoErrorEmailDetailDomain detail) throws Exception
    {
        /* [User Name] [D_CD] [D_PCD] [CIGMA D/O No.] [VENDOR_CD] [D/O Type] [D_PN] [delivery date] [Delivery Time]
         * [S_PCD] [Supplier P/No] [Current QTY] [Backorder QTY]
         */
        
        String username = nullToEmpty(detail.getCigmaChgDoError().getUpdateByUserId());
        if( EMPTY_STRING.equals(username) ){
            username = SYMBOL_NBSP;
        }
        String dcd = nullToEmpty(domain.getDCd());
        if( EMPTY_STRING.equals(dcd) ){
            dcd = SYMBOL_NBSP;
        }
        String dpcd = nullToEmpty(domain.getDPcd());
        if( EMPTY_STRING.equals(dpcd) ){
            dpcd = SYMBOL_NBSP;
        }
        String vendorcd = nullToEmpty(domain.getVendorCd());
        if( EMPTY_STRING.equals(vendorcd) ){
            vendorcd = SYMBOL_NBSP;
        }
        String dataType = nullToEmpty(detail.getCigmaChgDoError().getDataType());
        if( EMPTY_STRING.equals(dataType) ){
            dataType = SYMBOL_NBSP;
        }
        String dpn = nullToEmpty(detail.getCigmaChgDoError().getDPn());
        if( EMPTY_STRING.equals(dpn) ){
            dpn = SYMBOL_NBSP;
        }
        String deliveryDate = format(domain.getDeliveryDate(), PATTERN_YYYYMMDD_SLASH);
        if( EMPTY_STRING.equals(deliveryDate) ){
            deliveryDate = SYMBOL_NBSP;
        }
        String deliveryTime = nullToEmpty(domain.getDeliveryTime());
        if( EMPTY_STRING.equals(deliveryTime) ){
            deliveryTime = SYMBOL_NBSP;
        }else{
            deliveryTime = deliveryTime.substring(0,2)+":"+deliveryTime.substring(2,4);
        }

        String spcd = nullToEmpty(domain.getSPcd());
        if( EMPTY_STRING.equals(spcd) ){
            spcd = SYMBOL_NBSP;
        }
        String sPn = nullToEmpty(detail.getDensoSupplerParts().getSPn());
        if( EMPTY_STRING.equals(sPn) ){
            sPn = SYMBOL_NBSP;
        }
        
        String originalOrderQty = nullToEmpty(domain.getOriginalOrderQty());
        if( EMPTY_STRING.equals(originalOrderQty) ){
            originalOrderQty = SYMBOL_NBSP;
        }
        
        String currentOrderQty = nullToEmpty(domain.getCurrentOrderQty());
        if( EMPTY_STRING.equals(currentOrderQty) ){
            currentOrderQty = SYMBOL_NBSP;
        }
        
        sbContent.append(appendsString(String.format(
            getEmailLabel(locale , MAIL_CIGMA_DO_ERROR_DETAIL_BACKORDER)
            , username, dcd, dpcd, domain.getCigmaDoNo(), vendorcd, dataType, dpn
            , deliveryDate, deliveryTime
            , spcd, sPn
            , originalOrderQty, currentOrderQty)
            , SYMBOL_CR_LF));
    }
    
    public List<DeliveryOrderDomain> searchMailBackOrder(SpsTDoDetailDomain criteria) 
        throws ApplicationException {
        return deliveryOrderService.searchMailBackOrder(criteria);
    }
    
    public void transactSendNotificationChangeBackOrder(Locale locale, List<DeliveryOrderDomain> sendBackOrderMail, int limit, Log log) throws Exception {
        
        String sCd = null;
        String sPcd = null;
        
        String cigmaDoNo = null;
        int iCount = Constants.ZERO;
        boolean bSended = false;
        StringBuffer sbTitle = new StringBuffer( getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_SUBJECT) );
        StringBuffer sbContent = new StringBuffer();
        StringBuffer sbFrom = new StringBuffer();
        List<String> emailTo = new ArrayList<String>();
        UserSupplierDetailDomain criSupplierUser = new UserSupplierDetailDomain();
        criSupplierUser.setSpsMUserDomain( new SpsMUserDomain() );
        criSupplierUser.setSpsMUserSupplierDomain( new SpsMUserSupplierDomain() );
        criSupplierUser.getSpsMUserDomain().setIsActive( STR_ONE );
        criSupplierUser.getSpsMUserDomain().setUserType( Constants.STR_S );
        criSupplierUser.getSpsMUserSupplierDomain().setEmlUrgentOrderFlag(STR_ONE);
        sbFrom.append( ContextParams.getDefaultEmailFrom() );
        if( null != sendBackOrderMail){
            for(DeliveryOrderDomain domain : sendBackOrderMail){
                boolean isSkip = false;
                if( null == sCd ){
                    
                    isSkip = findMailBackorder(criSupplierUser, domain.getDoHeader().getSCd(), domain.getDoHeader().getSPcd(), emailTo, true);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0001, new String[] {domain.getDoHeader().getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    sCd = domain.getDoHeader().getSCd();
                    sPcd = domain.getDoHeader().getSPcd();
                    
                    cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                    updateMailFlag(locale, domain);
                    iCount = ONE;
                    initialCigmaNotiBackorder(locale, sbContent, domain);
                    fillNotiBackorderMail(locale, sbContent, domain);
                    if(!domain.getDoHeader().getRevision().equals("00")){
                        sbTitle = new StringBuffer( getEmailLabel(locale, MAIL_CIGMA_CHG_DO_NOTI_BACKORDER_SUBJECT) );
                    }
                } else if(!sCd.equals( domain.getDoHeader().getSCd()) || !sPcd.equals(domain.getDoHeader().getSPcd())) {
                    
                    if (!bSended) {
                        sendUrgentMail(locale, sbTitle, sbFrom, sbContent, emailTo, sCd, log);
                        bSended = true;
                        emailTo.clear();
                    }
                    
                    isSkip = findMailBackorder(criSupplierUser, domain.getDoHeader().getSCd(), domain.getDoHeader().getSPcd(), emailTo, true);
                    
                    if (isSkip) {
                        MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0001
                            , new String[] {domain.getDoHeader().getSCd()}, log, TWO, false);
                        bSended = true;
                        continue;
                    }
                    sCd = domain.getDoHeader().getSCd();
                    sPcd = domain.getDoHeader().getSPcd();
                    
                    cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                    updateMailFlag(locale, domain);
                    iCount = ONE;
                    initialCigmaNotiBackorder(locale, sbContent, domain);
                    fillNotiBackorderMail(locale, sbContent, domain);
                    if(!domain.getDoHeader().getRevision().equals("00")){
                        sbTitle = new StringBuffer( getEmailLabel(locale, MAIL_CIGMA_CHG_DO_NOTI_BACKORDER_SUBJECT) );
                    }
                }else if( !cigmaDoNo.equals( domain.getDoHeader().getCigmaDoNo() ) ) {
                    if( iCount == limit ){
                        sendUrgentMail(locale, sbTitle, sbFrom, sbContent, emailTo, sCd, log);
                        bSended = true;
                        iCount = ZERO;
                        
                        initialCigmaNotiBackorder(locale, sbContent, domain);
                    }else {
                        sbContent.append( Constants.SYMBOL_END_TABLE );
                        sbContent.append( SYMBOL_NEWLINE );
                    }
                    
                    cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                    updateMailFlag(locale, domain);
                    fillNotiBackorderMail(locale, sbContent, domain);
                    iCount++;
                }else {
                    updateMailFlag(locale, domain);
                }
                
                String userName = domain.getDoDetail().getLastUpdateDscId();
                if(StringUtil.checkNullOrEmpty(userName)){
                    userName = SYMBOL_NBSP;
                }
                
                String um = domain.getDoDetail().getUnitOfMeasure();
                if(StringUtil.checkNullOrEmpty(um)){
                    um = SYMBOL_NBSP;
                }
                
                String wh = domain.getDoHeader().getWhPrimeReceiving();
                if(StringUtil.checkNullOrEmpty(wh)){
                    wh = SYMBOL_NBSP;
                }
                
                String dockCode = domain.getDoHeader().getDockCode();
                if(StringUtil.checkNullOrEmpty(dockCode)){
                    dockCode = SYMBOL_NBSP;
                }

                cigmaDoNo = domain.getDoHeader().getCigmaDoNo();
                
                String backOrderDoDetail = getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_DETAIL);
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_USERNAME, userName);
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_DPARTNO, domain.getDoDetail().getDPn());
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_SPARTNO, domain.getDoDetail().getSPn());
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_UM, um);
                //backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_PREVIOUS_QTY,domain.getDoDetail().getPreviousQty().toString());
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_CURRENT_QTY, domain.getDoDetail().getCurrentOrderQty().toString());
                //backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_DIFFERENCE_QTY,domain.getDoDetail().getCurrentOrderQty().subtract(domain.getDoDetail().getPreviousQty()).toString());
                //backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_QTYBOX,domain.getDoDetail().getQtyBox().toString());
                //backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_RSN, rsn);
                //backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_CHG_CIGMA_DO_NO, chgCigmaDoNo);
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_PN_SHIPMENT_STATUS, domain.getDoDetail().getPnShipmentStatus());
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_CIGMADONO, cigmaDoNo);
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_WH, wh);
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_DOCKCODE, dockCode);
                backOrderDoDetail = backOrderDoDetail.replaceAll(SupplierPortalConstant.REPLACE_RCVLANE, domain.getDoDetail().getRcvLane());
                sbContent.append(backOrderDoDetail);
                bSended = false;
            } // end for each SPS DO URGENT
            if( !bSended && ZERO < sendBackOrderMail.size() ){
                sendUrgentMail(locale, sbTitle, sbFrom, sbContent, emailTo, sCd, log);
            }
        } // end if SPS DO URGENT
        return ;
    }
    
    private void initialCigmaNotiBackorder(Locale locale, StringBuffer sbContent, DeliveryOrderDomain domain) throws Exception 
    {
        sbContent.append(appendsString(getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER1), SYMBOL_NEWLINE) );
        sbContent.append(appendsString(getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER2), SYMBOL_NEWLINE) );
        
        // Cigma Vendor CD, S.CD, S.P.CD
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER3)
            .replaceAll(MAIL_VENDOR_CD_REPLACEMENT, domain.getDoHeader().getVendorCd())
            .replaceAll(MAIL_SCD_REPLACEMENT ,domain.getDoHeader().getSCd()) 
            .replaceAll(MAIL_SPCD_REPLACEMENT, domain.getDoHeader().getSPcd()));
    }
    
    private void fillNotiBackorderMail(Locale locale, StringBuffer sbContent, DeliveryOrderDomain domain) throws Exception
    {
        String deliveryDate = format(domain.getDoHeader().getDeliveryDatetime(), PATTERN_YYYYMMDD_SLASH);
        String deliveryTime = format(domain.getDoHeader().getDeliveryDatetime(), PATTERN_HHMM_COLON);

        /* D.CD, D.P.CD, Route, Del
         * SPS D/O No, Rev, Delivery Date, Delivery Time, Issue Date
         * */
        sbContent.append(
            getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER4)
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getDoHeader().getDCd())
                .replaceAll(MAIL_DPCD_REPLACEMENT, domain.getDoHeader().getDPcd())
                .replaceAll(MAIL_TRUCK_ROUTE_REPLACEMENT, domain.getDoHeader().getTruckRoute())
                .replaceAll(MAIL_TRUCK_SEQ_REPLACEMENT, domain.getDoHeader().getTruckSeq().toString())
                .replaceAll(MAIL_ORDER_TYPE_REPLACEMENT, domain.getDoHeader().getDataType())    
                .replaceAll(MAIL_SPS_DO_NO_REPLACEMENT, domain.getDoHeader().getSpsDoNo())
                .replaceAll(MAIL_REV_NO_REPLACEMENT, domain.getDoHeader().getRevision())
                .replaceAll(MAIL_DELIVERY_DATE_REPLACEMENT, deliveryDate)
                .replaceAll(MAIL_DELIVERY_TIME_REPLACEMENT, appendsString(deliveryTime
                    , Constants.SYMBOL_SPACE
                    , Constants.SYMBOL_OPEN_BRACKET
                    , Constants.WORD_UTC, domain.getCompany().getUtc().trim()
                    , Constants.SYMBOL_CLOSE_BRACKET))
                .replaceAll(MAIL_DO_ISSUE_DATE, 
                    format(domain.getDoHeader().getDoIssueDate(), PATTERN_YYYYMMDD_SLASH))
        );
        
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE1));
        sbContent.append(getEmailLabel(locale, MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE2));
    }
    
    private boolean findMailBackorder(
        UserSupplierDetailDomain criSupplierUser,
        String sCd,
        String sPcd,
        List<String> emailTo,
        boolean urgent) throws ApplicationException
    {
        criSupplierUser.getSpsMUserSupplierDomain().setSCd(sCd);

        // [IN016] Send email to supplier by plant
        criSupplierUser.getSpsMUserSupplierDomain().setSPcd(sPcd);
        
        if( ZERO == emailTo.size() ) {
            List<String> mailList = userSupplierService.searchUserSupplierEmailByUrgentFlagForBackorder(criSupplierUser);
            for (String mail : mailList) {
                if (!emailTo.contains(mail)) {
                    emailTo.add(mail);
                }
            }
            
            if( ZERO == emailTo.size() ){
                return true;
            }
        }
        return false;
    }

	public Integer updateOneWayKanbanTagFlag(KanbanTagDomain kanbanTag) throws ApplicationException {
		return deliveryOrderService.updateOneWayKanbanTagFlag(kanbanTag);
	}
} // end class.