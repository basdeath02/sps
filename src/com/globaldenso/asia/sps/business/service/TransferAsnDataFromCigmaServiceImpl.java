/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2015/10/06 CSI Akat                  [IN016]
 * 2016/02/04 CSI Akat                  [IN056]
 * 2016/03/15 CSI Akat                  [IN069]
 * 2016/04/08 CSI Akat                  [IN068]
 * 2016/04/18 CSI Akat                  [IN070]
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change P/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;






















//import com.globaldenso.ai.common.core.context.DensoContext;
//import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnDNErrorListDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnHeaderDNErrorListDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnFromCIGMAService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoToCIGMAService;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaChangePoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueForCalculateMonthDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//.MAIL_CIGMA_DO_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_PO_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_PO_1_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_CHG_PO_1_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_VENDOR_CD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_SCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_DCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_ISSUE_DATE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .PO_DATA_TYPE_MAP;
// [IN070] add list of email to send in content
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MAIL_CIGMA_ERROR_PERSON_IN_CHARGE;

// static import
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0002;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0013;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0016;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0001;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0015;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0016;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0017;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0018;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0019;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0020;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0022;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0023;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_TYPE_VENDOR_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_TYPE_PARTS_NO_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0033;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TABLE_CIGMA_PO_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TABLE_CIGMA_CHG_PO_NAME;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_PO; // Create Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_PO_DETAIL; // Create Purchase Order Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_PO_DUE; // Create Purchase Order Due
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_CHG_PO; // Create Change Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_CHG_PO_DETAIL; // Create Change Purchase Order Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_CHG_PO_DUE; // Create Change Purchase Order Due
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE; // Create P/O Cover Page
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE_DETAIL; // Create P/O Cover Page Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT; // Search P/O Cover Page Report
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID; // Update PDF Original File ID
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID; // Update PDF Change File ID
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_SEARCH_EXIST_PO; // Search Exist Purchase Order
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO_DUE; // Update Purchase Order Due
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO_DETAIL; // Update Purchase Order Detail
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO; // Update Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TRANSFER_PO_STEP_SEARCH_CHG_MAT_REPORT; // Search Change Material Report
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_SEARCH_EXISTS_FOR_FIRM; // Search Exists Purchaser Order for Firm Period
import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NBSP;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_C;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_Y;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.THREE;
import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;
import static com.globaldenso.asia.sps.common.constant.Constants.FIVE;
import static com.globaldenso.asia.sps.common.constant.Constants.SIX;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NEWLINE;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_CR_LF;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_HHMMSS;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToSqlDate;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToTimestamp;
import static com.globaldenso.asia.sps.common.utils.DateUtil.format;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.toBigDecimal;
import static com.globaldenso.asia.sps.common.utils.StringUtil.appendsString;
import static com.globaldenso.asia.sps.common.utils.StringUtil.nullToEmpty;
//import static com.globaldenso.asia.sps.common.utils.MessageUtil.throwsApplicationMessage;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getLabelHandledException;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getEmailLabel;
//import static com.globaldenso.asia.sps.common.utils.MessageUtil.getErrorMessage;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.writeLog;

/**
 * <p>
 * The class TransferPoDataFromCigmaFacadeServiceImpl.
 * </p>
 * <p>
 * Facade for TransferPoDataFromCigmaFacadeServiceImpl.
 * </p>
 * <ul>
 * <li>Method search : searchInvoiceInformation</li>
 * </ul>
 * 
 * @author CSI
 */
public class TransferAsnDataFromCigmaServiceImpl implements
		TransferAsnDataFromCigmaService {

	/** Reuse Argument 1. */
	private String[] argument1 = new String[ONE];
	/** Reuse Argument 2. */
	private String[] argument2 = new String[TWO];
	/** Reuse Argument 3. */
	private String[] argument3 = new String[THREE];
	/** Reuse Argument 4. */
	private String[] argument4 = new String[FOUR];
	/** Reuse Argument 5. */
	private String[] argument5 = new String[FIVE];
	/** Reuse Argument 6. */
	private String[] argument6 = new String[SIX];

	/** Common Services. */
	private CommonService commonService;
	/** Denso Company Services. */
	private SpsTAsnFromCIGMAService spsTAsnFromCIGMAService;

	 SpsMCompanyDensoCriteriaDomain companyDensoCriteria = null;
	private CompanyDensoService companyDensoService;

	  /** The company denso service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The plant denso service. */
    private SpsMPlantDensoService spsMPlantDensoService;
    
    /** The plant supplier service. */
    private SpsMPlantSupplierService spsMPlantSupplierService;
    
	/** The Constant LOG. */
	private static final Log LOG = LogFactory
			.getLog(SpsTAsnFromCIGMAService.class);

	   /** The delivery order service. */
    private DeliveryOrderService deliveryOrderService;
    
	 /** The as400 vendor service. */
    private SpsMAs400VendorService spsMAs400VendorService;
    
    /** The asn maintenance facade service. */
    private AsnMaintenanceFacadeService asnMaintenanceFacadeService; 
    
    /** The asn service. */
    private AsnService asnService;
    

    
	public SpsMCompanyDensoCriteriaDomain getCompanyDensoCriteria() {
		return companyDensoCriteria;
	}

	public void setCompanyDensoCriteria(
			SpsMCompanyDensoCriteriaDomain companyDensoCriteria) {
		this.companyDensoCriteria = companyDensoCriteria;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public CompanyDensoService getCompanyDensoService() {
		return companyDensoService;
	}

	/**
	 * Instantiates a new upload facade service impl.
	 */
	public TransferAsnDataFromCigmaServiceImpl() {
		super();
	}

	private MailService mailService;

	/**
	 * <p>
	 * Setter method for commonService.
	 * </p>
	 * 
	 * @param commonService
	 *            Set for commonService
	 */
	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public void setCompanyDensoService(CompanyDensoService companyDensoService) {
		this.companyDensoService = companyDensoService;
	}

	public MailService getMailService() {
		return mailService;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	
	public SpsMCompanyDensoService getSpsMCompanyDensoService() {
		return spsMCompanyDensoService;
	}

	public void setSpsMCompanyDensoService(
			SpsMCompanyDensoService spsMCompanyDensoService) {
		this.spsMCompanyDensoService = spsMCompanyDensoService;
	}

	public SpsMPlantDensoService getSpsMPlantDensoService() {
		return spsMPlantDensoService;
	}

	public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
		this.spsMPlantDensoService = spsMPlantDensoService;
	}

	public SpsMAs400VendorService getSpsMAs400VendorService() {
		return spsMAs400VendorService;
	}

	public void setSpsMAs400VendorService(
			SpsMAs400VendorService spsMAs400VendorService) {
		this.spsMAs400VendorService = spsMAs400VendorService;
	}

	public SpsMPlantSupplierService getSpsMPlantSupplierService() {
		return spsMPlantSupplierService;
	}

	public void setSpsMPlantSupplierService(
			SpsMPlantSupplierService spsMPlantSupplierService) {
		this.spsMPlantSupplierService = spsMPlantSupplierService;
	}

	public AsnMaintenanceFacadeService getAsnMaintenanceFacadeService() {
		return asnMaintenanceFacadeService;
	}

	public void setAsnMaintenanceFacadeService(
			AsnMaintenanceFacadeService asnMaintenanceFacadeService) {
		this.asnMaintenanceFacadeService = asnMaintenanceFacadeService;
	}

	public AsnService getAsnService() {
		return asnService;
	}

	public void setAsnService(AsnService asnService) {
		this.asnService = asnService;
	}

	public DeliveryOrderService getDeliveryOrderService() {
		return deliveryOrderService;
	}

	public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
		this.deliveryOrderService = deliveryOrderService;
	}

	
	/**
	 * <p>
	 * Setter method for poService.
	 * </p>
	 * 
	 * @param poService
	 *            Set for poService
	 */

	public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
			SpsMCompanyDensoDomain criteria) throws Exception {
		List<As400ServerConnectionInformationDomain> selectList = companyDensoService
				.searchAs400ServerList(criteria);
		return selectList;
	}

	public SpsTAsnFromCIGMAService getSpsTAsnFromCIGMAService() {
		return spsTAsnFromCIGMAService;
	}

	public void setSpsTAsnFromCIGMAService(
			SpsTAsnFromCIGMAService spsTAsnFromCIGMAService) {
		this.spsTAsnFromCIGMAService = spsTAsnFromCIGMAService;
	}

	/**
	 * 
	 * <p>
	 * Search As400 Server List.
	 * </p>
	 * 
	 * @param criteria
	 *            SpsMCompanyDensoDomain
	 * @return List of SpsMAs400SchemaDomain
	 * @throws Exception
	 *             error
	 */
	public List<SpsMDensoDensoRelationWithASNDNDomain> searchData(
			SpsMDensoDensoRelationWithASNDNDomain criteria) throws Exception {
		List<SpsMDensoDensoRelationWithASNDNDomain> selectList = spsTAsnFromCIGMAService
				.searchByCondition(criteria);
		return selectList;
	}

	/**
	 * 
	 * <p>
	 * get Argument Message.
	 * </p>
	 * 
	 * @param args
	 *            array String
	 * @return String[]
	 */
	private String[] getArgumentMessage(String... args) {
		if (null != args && ZERO < args.length) {
			if (ONE == args.length) {
				argument1[ZERO] = args[ZERO];
				return argument1;
			} else if (TWO == args.length) {
				argument2[ZERO] = args[ZERO];
				argument2[ONE] = args[ONE];
				return argument2;
			} else if (THREE == args.length) {
				argument3[ZERO] = args[ZERO];
				argument3[ONE] = args[ONE];
				argument3[TWO] = args[TWO];
				return argument3;
			} else if (FOUR == args.length) {
				for (int i = ZERO; i < FOUR; ++i) {
					argument4[i] = args[i];
				}
				return argument4;
			} else if (FIVE == args.length) {
				for (int i = ZERO; i < FIVE; ++i) {
					argument5[i] = args[i];
				}
				return argument5;
			} else if (SIX == args.length) {
				for (int i = ZERO; i < SIX; ++i) {
					argument6[i] = args[i];
				}
				return argument6;
			}
		}
		return null;
	}
	
	public void transferData(
        As400ServerConnectionInformationDomain as400Server, String spsFlag,
        List<SpsMDensoDensoRelationWithASNDNDomain> criteria)
        throws Exception {
	    
	    Locale locale = this.getDefaultLocale();
        String tempdCd = "";
        String message ="";
        String temp_planeta="";
        String temp_planetd="";
        String temp_acplanetd="";
        String temp_acplaneta="";
        String temp_planeta_time="";
        String temp_planetd_time="";
        String temp_acplanetd_time="";
        String temp_acplaneta_time="";
        
        /*List<String> errorCodeList = new ArrayList<String>();*/
        if (criteria != null) {
            if (criteria.size() > 0) {
                List<SPSTAsnHeaderDNDomain> listError = new ArrayList<SPSTAsnHeaderDNDomain>();
                List<SPSTAsnHeaderDNDomain> listSuccess = new ArrayList<SPSTAsnHeaderDNDomain>();
                List<SPSTAsnHeaderDNDomain> listFail = new ArrayList<SPSTAsnHeaderDNDomain>();
                List<SPSTAsnHeaderDNDomain> listCancel = new ArrayList<SPSTAsnHeaderDNDomain>();
                List<SPSTAsnHeaderDNDomain> listAll = new ArrayList<SPSTAsnHeaderDNDomain>();
                List<SPSTAsnHeaderDNErrorListDomain> errorCodeList = new ArrayList<SPSTAsnHeaderDNErrorListDomain>();
                
                for (int i = 0; i < criteria.size(); i++) {
                    // Get as400Server
                    SpsMCompanyDensoDomain company = new SpsMCompanyDensoDomain();
                    company.setDCd(StringUtil.convertListToVarcharCommaSeperate(criteria.get(i).getSchemaCd()));
                    // Call Façade Service TransferDODataFromCIGMAFacadeService
                    // searchAS400ServerList()
                    List<As400ServerConnectionInformationDomain> listAs400Server = companyDensoService.searchAs400ServerList(company);
                    
                    // 11/2/2020 : Find as400 server list for company Denso
                    SpsMCompanyDensoDomain companyDenso = new SpsMCompanyDensoDomain();
                    companyDenso.setDCd(StringUtil.convertListToVarcharCommaSeperate(criteria.get(i).getdCd()));
                    List<As400ServerConnectionInformationDomain> listAs400ServerDenso = companyDensoService.searchAs400ServerList(companyDenso);
                    
                    // 11/2/2020 : Check as400 server list for company denso
                    if (null != listAs400Server && null != listAs400ServerDenso) {
                        if (Constants.ZERO == listAs400Server.size()) {
                            // Log OutPut Log
                            LOG.error(MessageUtil.getApplicationMessageHandledException(
                                            locale,
                                            SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                                            new String[] { company.getDCd() }));
                        }
                    } else {
                        LOG.error(MessageUtil
                                .getApplicationMessageHandledException(
                                        locale,
                                        SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                                        new String[] { company.getDCd() }));
                        // MessageUtil.throwsApplicationMessage(locale,
                        // SupplierPortalConstant.ERROR_CD_SP_E5_0026, null
                        // , new String[]{company.getDCd()} );
                    }

                    // LOOP all data in <List>as400Server
                    if (null != listAs400Server) {
                        as400Server = listAs400Server.get(0);
                        criteria.get(i).setDatabaseName(as400Server.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
                        SpsMDensoDensoRelationWithASNDNDomain res = new SpsMDensoDensoRelationWithASNDNDomain();
                        res = CommonWebServiceUtil.searchAsnData(as400Server,spsFlag, criteria.get(i));
                        if (res != null) {
                            if (res.getListASN() != null) {
                                criteria.get(i).setListASN(res.getListASN());
                            }
                        }
                        
                        if (criteria.get(i).getListASN() != null) {
                            if (criteria.get(i).getListASN().size() > 0) {
                                BigDecimal totalShippingQty = new BigDecimal(0);
                                
                                for (int j = 0; j < criteria.get(i).getListASN().size(); j++) {
                                    if(criteria.get(i).getListASN().get(j).getListAsnDet() != null){
                                        if(criteria.get(i).getListASN().get(j).getListAsnDet().size() >0){
                                     //Set data list asn
                                    List<AsnMaintenanceReturnDomain>  doGroupAsn = new ArrayList<AsnMaintenanceReturnDomain>();
                                    AsnMaintenanceReturnDomain datadoGroupAsn  = new AsnMaintenanceReturnDomain();
                                    String asnNo = criteria.get(i).getListASN().get(j).getAsnNo().toString();
                                    SPSTAsnHeaderDNDomain result = new SPSTAsnHeaderDNDomain();
                                    //Trim Data
                                    if(criteria.get(i).getListASN().get(j).getAsnNo() != null){
                                        criteria.get(i).getListASN().get(j).setAsnNo(criteria.get(i).getListASN().get(j).getAsnNo().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getAsnStatus() != null){
                                    criteria.get(i).getListASN().get(j).setAsnStatus(criteria.get(i).getListASN().get(j).getAsnStatus().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getVendorCd() != null){
                                    criteria.get(i).getListASN().get(j).setVendorCd(criteria.get(i).getListASN().get(j).getVendorCd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getsCd() != null){
                                    criteria.get(i).getListASN().get(j).setsCd(criteria.get(i).getListASN().get(j).getsCd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getsPcd() != null){
                                    criteria.get(i).getListASN().get(j).setsPcd(criteria.get(i).getListASN().get(j).getsPcd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getdCd() != null){
                                    criteria.get(i).getListASN().get(j).setdCd(criteria.get(i).getListASN().get(j).getdCd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getdPcd() != null){
                                    criteria.get(i).getListASN().get(j).setdPcd(criteria.get(i).getListASN().get(j).getdPcd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getPlanEtd() != null){
                                    criteria.get(i).getListASN().get(j).setPlanEtd(criteria.get(i).getListASN().get(j).getPlanEtd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getPlanEta() != null){
                                    criteria.get(i).getListASN().get(j).setPlanEta(criteria.get(i).getListASN().get(j).getPlanEta().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getActualEtd() != null){
                                    criteria.get(i).getListASN().get(j).setActualEtd(criteria.get(i).getListASN().get(j).getActualEtd().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getActualEta() != null){
                                    criteria.get(i).getListASN().get(j).setActualEta(criteria.get(i).getListASN().get(j).getActualEta().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getTransferCigmaFlg() != null){
                                    criteria.get(i).getListASN().get(j).setTransferCigmaFlg(criteria.get(i).getListASN().get(j).getTransferCigmaFlg().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getNumberOfPallet() != null){
                                    criteria.get(i).getListASN().get(j).setNumberOfPallet(criteria.get(i).getListASN().get(j).getNumberOfPallet().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getTripNo() != null){
                                    criteria.get(i).getListASN().get(j).setTripNo(criteria.get(i).getListASN().get(j).getTripNo().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getPdfFileId() != null){
                                    criteria.get(i).getListASN().get(j).setPdfFileId(criteria.get(i).getListASN().get(j).getPdfFileId().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getCreatedInvoiceFlag() != null){
                                    criteria.get(i).getListASN().get(j).setCreatedInvoiceFlag(criteria.get(i).getListASN().get(j).getCreatedInvoiceFlag().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getReceiveByScanFlag() != null){
                                    criteria.get(i).getListASN().get(j).setReceiveByScanFlag(criteria.get(i).getListASN().get(j).getReceiveByScanFlag().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getTm() != null){
                                    criteria.get(i).getListASN().get(j).setTm(criteria.get(i).getListASN().get(j).getTm().trim());
                                    }
                                    if(criteria.get(i).getListASN().get(j).getProcessFlag() != null){
                                    criteria.get(i).getListASN().get(j).setProcessFlag(criteria.get(i).getListASN().get(j).getProcessFlag().trim());
                                    }
                                    
                                    
                                    if(criteria.get(i).getListASN().get(j).getListAsnDet() != null){
                                        if(criteria.get(i).getListASN().get(j).getListAsnDet().size() >0){
                                            for(int ld=0;ld<criteria.get(i).getListASN().get(j).getListAsnDet().size();ld++){
                                                //Set total shipping
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getShippingQty() != null) {
                                                totalShippingQty = totalShippingQty.add(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getShippingQty());
                                                }
                                                SpsTAsnDetailDomain doAsnDetDomain = new SpsTAsnDetailDomain();
                                                doAsnDetDomain.setShippingQty(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getShippingQty());
                                                doAsnDetDomain.setDoId(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getDoId());
                                                datadoGroupAsn.setAsnDetailDomain(doAsnDetDomain);
                                                
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getAsnNo() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setAsnNo(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getAsnNo().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getdCd() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setdCd(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getdCd().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getsPn() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setsPn(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getsPn().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getdPn() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setdPn(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getdPn().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getSpsDoNo() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setSpsDoNo(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getSpsDoNo().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getReVision() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setReVision(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getReVision().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getCigmaDoNo() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setCigmaDoNo(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getCigmaDoNo().trim());
                                                  }
                                                
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getUnitOfMeasure() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setUnitOfMeasure(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getUnitOfMeasure().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getChangeReasonCd() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setChangeReasonCd(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getChangeReasonCd().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getReviseShippingQtyFlag() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setReviseShippingQtyFlag(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getReviseShippingQtyFlag().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getAllowReviseFlag() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setAllowReviseFlag(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getAllowReviseFlag().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getKanbanType() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setKanbanType(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getKanbanType().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getReceivingStatus() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setReceivingStatus(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getReceivingStatus().trim());
                                                  }
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getVar() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setVar(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getVar().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getIns() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setIns(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getIns().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getWh() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setWh(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getWh().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getProcessFlag() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setProcessFlag(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getProcessFlag().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getsCd() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setsCd(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getsCd().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getsPcd() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setsPcd(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getsPcd().trim());
                                                  }
                                                
                                                if(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getdPcd() != null)
                                                  {
                                                    criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).setdPcd(criteria.get(i).getListASN().get(j).getListAsnDet().get(ld).getdPcd().trim());
                                                  }
                                            }
                                        }
                                    }
                                    //Set Date time
                                    
                                    if(criteria.get(i).getListASN().get(j).getPlanEtd() != null){
                                        if(criteria.get(i).getListASN().get(j).getPlanEtd().length() <8){
                                             temp_planetd =criteria.get(i).getListASN().get(j).getPlanEtd();
                                             temp_planetd ="00000000"+temp_planetd;
                                             temp_planetd =temp_planetd.substring(temp_planetd.length()-8, temp_planetd.length());
                                            criteria.get(i).getListASN().get(j).setPlanEtd(temp_planetd);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getPlanEta() != null){
                                        if(criteria.get(i).getListASN().get(j).getPlanEta().length() <8){
                                             temp_planeta =criteria.get(i).getListASN().get(j).getPlanEta();
                                             temp_planeta ="00000000"+temp_planeta;
                                             temp_planeta =temp_planeta.substring(temp_planeta.length()-8, temp_planeta.length());
                                            criteria.get(i).getListASN().get(j).setPlanEta(temp_planeta);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getActualEtd() != null){
                                        if(criteria.get(i).getListASN().get(j).getActualEtd().length() <8){
                                            temp_acplanetd =criteria.get(i).getListASN().get(j).getActualEtd();
                                            temp_acplanetd ="00000000"+temp_acplanetd;
                                            temp_acplanetd =temp_acplanetd.substring(temp_acplanetd.length()-8, temp_acplanetd.length());
                                            criteria.get(i).getListASN().get(j).setActualEtd(temp_acplanetd);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getActualEta() != null){
                                        if(criteria.get(i).getListASN().get(j).getActualEta().length() <8){
                                            temp_acplaneta =criteria.get(i).getListASN().get(j).getActualEta();
                                            temp_acplaneta ="00000000"+temp_acplaneta;
                                            temp_acplaneta =temp_acplaneta.substring(temp_acplaneta.length()-8, temp_acplaneta.length());
                                            criteria.get(i).getListASN().get(j).setActualEta(temp_acplaneta);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getPlanEtaTime() != null){
                                        if(criteria.get(i).getListASN().get(j).getPlanEtaTime().length() <6){
                                            String temp_d  =criteria.get(i).getListASN().get(j).getPlanEtaTime();
                                            temp_d ="000000"+temp_d;
                                            temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                                            criteria.get(i).getListASN().get(j).setPlanEtaTime(temp_d);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getPlanEtdTime() != null){
                                        if(criteria.get(i).getListASN().get(j).getPlanEtdTime().length() <6){
                                            String temp_d  =criteria.get(i).getListASN().get(j).getPlanEtdTime();
                                            temp_d ="000000"+temp_d;
                                            temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                                            criteria.get(i).getListASN().get(j).setPlanEtdTime(temp_d);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getActualEtaTime() != null){
                                        if(criteria.get(i).getListASN().get(j).getActualEtaTime().length() <6){
                                            String temp_d  =criteria.get(i).getListASN().get(j).getActualEtaTime();
                                            temp_d ="000000"+temp_d;
                                            temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                                            criteria.get(i).getListASN().get(j).setActualEtaTime(temp_d);
                                        }
                                    }
                                    if(criteria.get(i).getListASN().get(j).getActualEtdTime() != null){
                                        if(criteria.get(i).getListASN().get(j).getActualEtdTime().length() <6){
                                            String temp_d  =criteria.get(i).getListASN().get(j).getActualEtdTime();
                                            temp_d ="000000"+temp_d;
                                            temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                                            criteria.get(i).getListASN().get(j).setActualEtdTime(temp_d);
                                        }
                                    }
                                     temp_planeta=criteria.get(i).getListASN().get(j).getPlanEta();
                                     temp_planetd=criteria.get(i).getListASN().get(j).getPlanEtd();
                                     temp_acplanetd=criteria.get(i).getListASN().get(j).getActualEtd();
                                     temp_acplaneta=criteria.get(i).getListASN().get(j).getActualEta();
                                     
                                     temp_planeta_time=criteria.get(i).getListASN().get(j).getPlanEtaTime();
                                     temp_planetd_time=criteria.get(i).getListASN().get(j).getPlanEtdTime();
                                     temp_acplanetd_time=criteria.get(i).getListASN().get(j).getActualEtdTime();
                                     temp_acplaneta_time=criteria.get(i).getListASN().get(j).getActualEtaTime();
                                                     
                                    result = criteria.get(i).getListASN().get(j);
                                    result.setIsSuccess(false);
                                    String supplierDatabasename = result.getDatabaseName();
                                    int check = 0;
                                    SPSTAsnHeaderDNErrorListDomain errorCodeListCurr = new SPSTAsnHeaderDNErrorListDomain();
                                    try {
                                         //validate DN-DN master
                                        
                                        errorCodeListCurr = validateDNDNMasterSch(result,criteria.get(i));//check schema
                                        if(errorCodeListCurr!=null){
                                            result.setDnDnTrnFlg("9");
                                        }else{
                                            errorCodeListCurr = Validate(result);
                                            result.setDnDnTrnFlg("2");
                                        }
                                        //errorCodeListCurr = Validate(result,masterDcd,masterDpcd,masterScd,masterSpcd);
                                        //errorCodeListCurr = Validate(result);
                                        //result.setDnDnTrnFlg("2");
                                        
                                         result.setPlanEtdTime(temp_planetd_time);
                                         result.setPlanEtaTime(temp_planeta_time);
                                         result.setActualEtdTime(temp_acplanetd_time);
                                         result.setActualEtaTime(temp_acplaneta_time);
                                         
                                        errorCodeList.add(errorCodeListCurr);
                                        if (errorCodeListCurr != null) {
                                            if(errorCodeListCurr.getErrorCodeList() != null){
                                            if (errorCodeListCurr.getErrorCodeList().size() > 0) {
                                                check++;
                                                
                                                for (int ii = 0; ii < errorCodeListCurr.getErrorCodeList().size(); ii++) {
                                                    LOG.error("Validate  Asn No "+errorCodeListCurr.getAsnNo()+" Fail : "+errorCodeListCurr.getErrorCodeList().get(ii));
                                                }
                                            }}
                                            if(errorCodeListCurr.getErrorListDet() != null){
                                                if (errorCodeListCurr.getErrorListDet().size() > 0) {
                                                    check++;
                                                     for(int iii=0;iii<errorCodeListCurr.getErrorListDet().size();iii++){
                                                         if(errorCodeListCurr.getErrorListDet().get(iii).getErrorCodeList() != null){
                                                             if(errorCodeListCurr.getErrorListDet().get(iii).getErrorCodeList().size() >0){
                                                                 for(int iiii=0;iiii<errorCodeListCurr.getErrorListDet().get(iii).getErrorCodeList().size();iiii++){
                                                                   LOG.error("Validate  Asn No/SPN "+errorCodeListCurr.getErrorListDet().get(iii).getAsnNo()+"/"+errorCodeListCurr.getErrorListDet().get(iii).getsPN()+" Fail : "+errorCodeListCurr.getErrorListDet().get(iii).getErrorCodeList().get(iiii));
                                                                 }
                                                             }
                                                         }
                                                        
                                                     }
                                                }
                                            }
                                        }
                                        
                                        if (check == 0) {
                                             
                                            // Insert asn
                                            try {
                                                result.setDnDnTrnFlg("1");
                                                boolean resultInsertUpdate = spsTAsnFromCIGMAService.InsertUpdateASN(result);
                                                 result.setPlanEtd(temp_planetd);
                                                 result.setPlanEta(temp_planeta);
                                                 result.setActualEtd(temp_acplanetd);
                                                 result.setActualEta(temp_acplaneta);
                                                 result.setPlanEtdTime(temp_planetd_time);
                                                 result.setPlanEtaTime(temp_planeta_time);
                                                 result.setActualEtdTime(temp_acplanetd_time);
                                                 result.setActualEtaTime(temp_acplaneta_time);
                                                if (resultInsertUpdate) {
                                                    try {
                                                        // Insert ud24,25
                                                        
                                                       //get data from do detail
                                                        
                                                        for(int iresult=0;iresult<result.getListAsnDet().size();iresult++){
                                                            
                                                            SPSTDoHeaderDNDomain criteriaDoDet = new SPSTDoHeaderDNDomain();
                                                            criteriaDoDet.setDoId(result.getListAsnDet().get(iresult).getDoId());
                                                            List<SPSTDoHeaderDNDomain> listDODet = spsTAsnFromCIGMAService.searchDODelivery(criteriaDoDet);
                                                             
                                                            if(listDODet!=null){
                                                                if(listDODet.size()>0){
                                                                    result.getListAsnDet().get(iresult).setDueDate(listDODet.get(0).getDeliveryDate());
                                                                }
                                                            }
                                                            //get chg_cigma_co_no 02082020
                                                            if(!(result.getListAsnDet().get(iresult).getReVision().equalsIgnoreCase("00"))){
                                                                SPSTDoDetailDNDomain criteriaDoDettail = new SPSTDoDetailDNDomain();
                                                                criteriaDoDettail.setDoId(result.getListAsnDet().get(iresult).getDoId());
                                                                criteriaDoDettail.setsPn(result.getListAsnDet().get(iresult).getsPn());
                                                                criteriaDoDettail.setdPn(result.getListAsnDet().get(iresult).getdPn());
                                                                //criteriaDoDettail.setPnRevision(result.getListAsnDet().get(iresult).getReVision());
                                                                List<SPSTDoDetailDNDomain> listDODetail = spsTAsnFromCIGMAService.searchDODet(criteriaDoDettail);
                                                                
                                                                if(listDODetail!=null){
                                                                    if(listDODetail.size()>0 && listDODetail.get(0).getChgCigmaDoNo()!=null){
                                                                        result.getListAsnDet().get(iresult).setCigmaCurrDoNo(listDODetail.get(0).getChgCigmaDoNo());
                                                                    }else{
                                                                        result.getListAsnDet().get(iresult).setCigmaCurrDoNo(result.getListAsnDet().get(iresult).getCigmaDoNo());
                                                                    }
                                                                }
                                                            }else{
                                                                result.getListAsnDet().get(iresult).setCigmaCurrDoNo(result.getListAsnDet().get(iresult).getCigmaDoNo());
                                                            }
                                                       }
                                                        
                                                        result.setMode("insert");
                                                        result.setTm("");
                                                        String receiveStatus ="";
                                                        if(result.getAsnStatus().trim().equals("ISS")){
                                                            receiveStatus ="N";
                                                        }
                                                        if(result.getAsnStatus().trim().equals("CCL")){
                                                            receiveStatus ="D";
                                                        }
                                                        result.setReceivingStatus(receiveStatus);
                                                        if(result.getListAsnDet() != null){
                                                            if(result.getListAsnDet().size() >0){
                                                                for(int la=0;la<result.getListAsnDet().size();la++){
                                                                    result.getListAsnDet().get(la).setReceivingStatus(receiveStatus);
                                                                }
                                                            }
                                                        }
                                                        
                                                        //result.setActualEtdTime("0");
                                                        result.setPlanEtdTime("0");
                                                        
                                                        result.setActualEtdTime(result.getActualEtdTime().substring(0,4));
                                                        result.setPlanEtaTime(result.getPlanEtaTime().substring(0,4));
                                                        
                                                        result.setActualEtaTime(result.getActualEtaTime().substring(0,4));
                                                                                                                
                                                        result.setProcessFlag("");
                                                        // 11/2/2020 Find database name from Denso company
                                                        String DensoDBname = listAs400ServerDenso.get(0).getSpsMAs400SchemaDomain().getAsnInvSchemaCigma(); 
                                                        //result = CommonWebServiceUtil.insertUpdateUD2425Data(as400Server,spsFlag,result, DensoDBname);
                                                        LOG.info("=== insertUpdateUD2425Data ===");
                                                        LOG.info("PARAM_SCHEMA_NAME_REPLICATE : "+listAs400ServerDenso.get(0).getSpsMAs400SchemaDomain().getAsnInvSchemaCigmaReplicate());
                                                        LOG.info("DensoDBname : "+DensoDBname);
                                                        result = CommonWebServiceUtil.insertUpdateUD2425Data(listAs400ServerDenso.get(0),spsFlag,result, DensoDBname);
                                                        if (result == null) {
                                                            result = criteria.get(i).getListASN().get(j);
                                                            result.setIsSuccess(false);
                                                        }
                                                        if (result.getIsSuccess()) {
                                                            result.setDnDnTrnFlg("1");
                                                            result.setIsSuccess(true);
                                                        } else {
                                                            check++;
                                                            spsTAsnFromCIGMAService.DeleteASN(result);
                                                            result.setDnDnTrnFlg("2");
                                                            message = MessageUtil.getApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0069, new String[] { asnNo });
                                                            result.setMsgError(message);
                                                            listError.add(result);
                                                            MessageUtil.getErrorMessageForBatch(locale,SupplierPortalConstant.ERROR_CD_SP_E6_0069,new String[] { asnNo },LOG,Constants.ZERO,false);
                                                        }
                                                    } catch (Exception e) {
                                                        check++;
                                                        if (result == null) {
                                                            result = criteria.get(i).getListASN().get(j);
                                                            result.setIsSuccess(false);
                                                        }
                                                        spsTAsnFromCIGMAService.DeleteASN(result);
                                                        result.setDnDnTrnFlg("2");
                                                        result.setIsSuccess(false);
                                                        result.setMsgError(e.getMessage());
                                                        listError.add(result);
                                                        LOG.error(e.getMessage());
                                                    }
                                                    
                                                    result.setActualEtdTime("0");
                                                    result.setPlanEtaTime(temp_planeta_time);
                                                    result.setActualEtaTime(temp_acplaneta_time);

                                                } else {
                                                    check++;
                                                    result.setDnDnTrnFlg("2");
                                                    message = MessageUtil.getApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0068, new String[] { asnNo });
                                                    result.setMsgError(message);
                                                    listError.add(result);
                                                    MessageUtil
                                                            .getErrorMessageForBatch(
                                                                    locale,
                                                                    SupplierPortalConstant.ERROR_CD_SP_E6_0068,
                                                                    new String[] { asnNo },
                                                                    LOG,
                                                                    Constants.ZERO,
                                                                    false);
                                                }
                                            } catch (Exception ex) {
                                                check++;
                                                if (result == null) {
                                                    result = criteria.get(i)
                                                            .getListASN()
                                                            .get(j);
                                                    result.setIsSuccess(false);
                                                }
                                                result.setDnDnTrnFlg("2");
                                                result.setIsSuccess(false);
                                                result.setMsgError(ex
                                                        .getMessage());
                                                listError.add(result);
                                                LOG.error(ex.getMessage());
                                            }
                                            
                                        }

                                    } catch (Exception e) {
                                        check++;
                                        if (result == null) {
                                            result = criteria.get(i)
                                                    .getListASN().get(j);
                                            result.setIsSuccess(false);
                                        }
                                        result.setDnDnTrnFlg("2");
                                        result.setIsSuccess(false);
                                        result.setMsgError(e.getMessage());
                                        listError.add(result);
                                        LOG.error(e.getMessage());
                                    }

                                    if(check ==0){
                                        // Update transfer flag
                                        // update transfer flag SPS
                                        boolean updateFlag =spsTAsnFromCIGMAService.UpdateTranFlag(result);
                                        if (!updateFlag) {
                                            MessageUtil.getErrorMessageForBatch(
                                                            locale,
                                                            SupplierPortalConstant.ERROR_CD_SP_E6_0070,
                                                            new String[] { asnNo },
                                                            LOG, Constants.ZERO,
                                                            false);
                                            message = MessageUtil.getApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0070, new String[] { asnNo });
                                            result.setMsgError(message);
                                            listError.add(result);
                                        }
                                    }
                                    
                                    if (check == 0) {
                                        // Update DO
                                        try {
                                            spsTAsnFromCIGMAService.UpdateDOStatus(result);
                                        } catch (Exception e) {
                                            LOG.error(e.getMessage());
                                        }

                                    }
                                    
                                        listAll.add(criteria.get(i).getListASN().get(j));
                                        //Set Status
                                        criteria.get(i).getListASN().get(j).setDnDnTrnFlg(result.getDnDnTrnFlg());
                                        criteria.get(i).getListASN().get(j).setIsSuccess(result.getIsSuccess());
                                        //Set Error
                                        criteria.get(i).getListASN().get(j).setMsgError(result.getMsgError());
                                        //Add success
                                        if(criteria.get(i).getListASN().get(j).getIsSuccess()){
                                             listSuccess.add(criteria.get(i).getListASN().get(j));
                                            
                                             //Add list cancel
                                                if(criteria.get(i).getListASN().get(j).getAsnStatus().equals("CCL")){
                                                    listCancel.add(criteria.get(i).getListASN().get(j));
                                                }
                                        }
                                        //Add Fail
                                        if(!criteria.get(i).getListASN().get(j).getIsSuccess()){
                                             listFail.add(criteria.get(i).getListASN().get(j));
                                        }
                                        
                                    //update transfer flag cigma
                                    try{
                                            result.setMode("update");
                                            if (result.getDnDnTrnFlg().equals("2")) {
                                                result.setDnDnTrnFlg("3");
                                            }
                                            if (result.getDnDnTrnFlg().equals("1")) {
                                                result.setDnDnTrnFlg("2");
                                            }
                                            result.setDatabaseName(supplierDatabasename);
                                            result = CommonWebServiceUtil.insertUpdateUD2425Data(as400Server,spsFlag, result, "");
                                            
                                            if (result == null) {
                                                result = criteria.get(i)
                                                        .getListASN().get(j);
                                            }
                                            if (!result.getIsSuccess()) {
                                                message = MessageUtil
                                                        .getApplicationMessage(
                                                                locale,
                                                                SupplierPortalConstant.ERROR_CD_SP_E6_0071,
                                                                new String[] { asnNo });
                                                result.setMsgError(message);
                                                listError.add(result);
                                                MessageUtil
                                                        .getErrorMessageForBatch(
                                                                locale,
                                                                SupplierPortalConstant.ERROR_CD_SP_E6_0071,
                                                                new String[] { asnNo },
                                                                LOG,
                                                                Constants.ZERO,
                                                                false);
                                            }
                                        } catch (Exception e) {
                                            if (result == null) {
                                                result = criteria.get(i)
                                                        .getListASN().get(j);
                                                result.setIsSuccess(false);
                                            }
                                            result.setDnDnTrnFlg("2");
                                            result.setIsSuccess(false);
                                            result.setMsgError(e.getMessage());
                                            listError.add(result);
                                            LOG.error(e.getMessage());
                                        }
                                        }else{
                                            LOG.error("ud@52pr data not found !");
                                        }   
                                    
                                }else{
                                    LOG.error("ud@52pr data not found !");
                                }
                                    
                                }
                                
                            }
                        }
                        
                    }

                    // Send Command
                    tempdCd = criteria.get(i).getdCd();
                    int checkSendCommand = 0;
                    if (i < criteria.size() - 1) {
                        if (!tempdCd.equals(criteria.get(i + 1).getdCd())) {
                            checkSendCommand++;
                        }else{
                            listAll = new ArrayList<SPSTAsnHeaderDNDomain>();
                            errorCodeList = new ArrayList<SPSTAsnHeaderDNErrorListDomain>();
                        }
                    } else {
                        checkSendCommand++;
                    }

                    if (checkSendCommand > 0) {
                        if(listAll != null){
                            if(listAll.size() >0){
                                
                                Collections.sort(listAll,new SPSTAsnTripAsnComparator());
                                int checkTripNo = 0;
                                int totalTripNo = 0;
                                String tempTripNO ="";
                                
                                int checkETDDate = 0;
                                int totalETDDate = 0;
                                String tempETDDate ="";
                                
                                List<SPSTAsnHeaderDNDomain> listErrorDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                List<SPSTAsnHeaderDNDomain> listSuccessDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                List<SPSTAsnHeaderDNDomain> listFailDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                List<SPSTAsnHeaderDNDomain> listCancelDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                for(int i_all=0;i_all <listAll.size();i_all++){
                                    tempTripNO = listAll.get(i_all).getTripNo();
                                    totalTripNo++;
                                    
                                    if (i_all < listAll.size() - 1) {
                                        if (!tempTripNO.equals(listAll.get(i_all + 1).getTripNo())) {
                                            checkTripNo++;
                                        }
                                    } else {
                                        checkTripNo++;
                                    }
                                    
                                    tempETDDate = listAll.get(i_all).getActualEtd();
                                    totalETDDate++;
                                    
                                    if (i_all < listAll.size() - 1) {
                                        if (!tempETDDate.equals(listAll.get(i_all + 1).getActualEtd())) {
                                            checkETDDate++;
                                        }
                                    } else {
                                        checkETDDate++;
                                    }
                                    
                                    
                                    if (listError != null) {
                                        if (listError.size() > 0) {
                                           for(int i_error =0;i_error<listError.size();i_error++){
                                               if(listAll.get(i_all).getAsnNo().equals(listError.get(i_error).getAsnNo())){
                                                   if(listAll.get(i_all).getTripNo().equals(listError.get(i_error).getTripNo())){
                                                       if(listAll.get(i_all).getAsnStatus().equals(listFail.get(i_error).getAsnStatus())){
                                                           if(listAll.get(i_all).getActualEtd().equalsIgnoreCase(listError.get(i_error).getActualEtd())){
                                                               listErrorDis.add(listError.get(i_error));
                                                               break;
                                                           }
                                                       }
                                                   }
                                                   //listErrorDis.add(listError.get(i_error));
                                               }
                                           }
                                        }
                                    }
                                    
                                    if (listSuccess != null) {
                                        if (listSuccess.size() > 0) {
                                           for(int i_success =0;i_success<listSuccess.size();i_success++){
                                               if(listAll.get(i_all).getAsnNo().equals(listSuccess.get(i_success).getAsnNo())){
                                                   if(listAll.get(i_all).getTripNo().equals(listSuccess.get(i_success).getTripNo())){
                                                       if(listAll.get(i_all).getAsnStatus().equals(listSuccess.get(i_success).getAsnStatus())){
                                                           if(listAll.get(i_all).getActualEtd().equalsIgnoreCase(listSuccess.get(i_success).getActualEtd())){
                                                               listSuccessDis.add(listSuccess.get(i_success));
                                                               break;
                                                           }
                                                       }
                                                   }
                                                   //listSuccessDis.add(listSuccess.get(i_success));
                                               }
                                           }
                                        }
                                    }
                                    
                                    if (listFail != null) {
                                        if (listFail.size() > 0) {
                                           for(int i_fail =0;i_fail<listFail.size();i_fail++){
                                               if(listAll.get(i_all).getAsnNo().equals(listFail.get(i_fail).getAsnNo())){
                                                   if(listAll.get(i_all).getAsnStatus().equals(listFail.get(i_fail).getAsnStatus())){
                                                       if(listAll.get(i_all).getTripNo().equals(listFail.get(i_fail).getTripNo())){
                                                           if(listAll.get(i_all).getActualEtd().equalsIgnoreCase(listFail.get(i_fail).getActualEtd())){
                                                               listFailDis.add(listFail.get(i_fail));
                                                               break;
                                                           } 
                                                       }
                                                   }
                                                   //listFailDis.add(listFail.get(i_fail));
                                               }
                                           }
                                        }
                                    }
                                    
                                    if (listCancel != null) {
                                        if (listCancel.size() > 0) {
                                           for(int i_cancel =0;i_cancel<listCancel.size();i_cancel++){
                                               if(listAll.get(i_all).getAsnNo().equals(listCancel.get(i_cancel).getAsnNo())){
                                                   if(listAll.get(i_all).getTripNo().equals(listCancel.get(i_cancel).getTripNo())){
                                                       if(listAll.get(i_all).getAsnStatus().equals(listCancel.get(i_cancel).getAsnStatus())){
                                                           if(listAll.get(i_all).getActualEtd().equalsIgnoreCase(listCancel.get(i_cancel).getActualEtd())){
                                                               listCancelDis.add(listCancel.get(i_cancel));
                                                               break;
                                                           }
                                                       }
                                                   }
                                                   //listCancelDis.add(listCancel.get(i_cancel));
                                               }
                                           }
                                        }
                                    }
                                    //if(checkTripNo >0){
                                    if(checkETDDate > 0){
                                        //Send Email
                                        
                                        List<SpsMDensoDensoRelationWithASNDNDomain> dndnMaster = null;
                                        SpsMDensoDensoRelationWithASNDNDomain criteriaMaster = new SpsMDensoDensoRelationWithASNDNDomain();
                                        criteriaMaster.setdCd(StringUtil.convertListToVarcharCommaSeperate(criteria.get(i).getdCd()));
                                        //criteriaMaster.setdPcd(criteria.get(i).getdPcd());
                                        //criteriaMaster.setdCd(criteria.get(i).getdCd());
                                        criteriaMaster.setSchemaCd(criteria.get(i).getSchemaCd());
                                        dndnMaster = spsTAsnFromCIGMAService.searchByCondition(criteriaMaster);
                                        
                                        // ***Send Email Error ASN*********
                                        try{
                                            if (listErrorDis != null) {
                                                if (listErrorDis.size() > 0) {
                                                    SendEmail(listErrorDis, criteria.get(i).getEmail());
                                                    
                                                }
                                            }
                                        }catch(Exception e){
                                            LOG.error(e.getMessage());
                                        }
                                        
                                
                                        // ***Send Email Success ASN*********
                                        String emailTo = "";
                                        try{
                                            // 6/2/2020 : Fix send email to supplier user
                                            SpsMDensoDensoRelationWithASNDNDomain criteriaSupplier = new SpsMDensoDensoRelationWithASNDNDomain();
                                           // criteriaSupplier.setdCd(criteria.get(0).getdCd());
                                            criteriaSupplier.setsCd(dndnMaster.get(0).getsCd());
                                            //criteriaSupplier.setsPcd(criteria.get(i).getsPcd());
                                            //List<SpsMUserDomain> listUserSupplier = spsTAsnFromCIGMAService.searchUserSupplier(criteriaSupplier);
                                            List<SpsMUserDomain> listUserSupplier = spsTAsnFromCIGMAService.searchUserSupplier(criteriaMaster);

                                            emailTo = listUserSupplier.get(0).getEmail();
                                            for(int e=1; e < listUserSupplier.size();e++){
                                                emailTo +=  "," + listUserSupplier.get(e).getEmail(); 
                                            }
                                            
                                            if (listSuccessDis != null) {
                                                
                                                if (listSuccessDis.size() > 0) {
                                                    LOG.info("Start send OK email to : " + emailTo);
                                                    //SendEmailSuccess(listSuccessDis, emailTo,totalTripNo,criteria);
                                                    SendEmailSuccess(listSuccessDis, emailTo,totalETDDate,dndnMaster);
                                                }
                                            }
                                        }catch(Exception e){
                                            LOG.error(e.getMessage());
                                        }
                                        // ***Send Email Fail ASN*********
                                        
                                        try{
                                            if (listFailDis != null) {
                                                
                                                if (listFailDis.size() > 0) {
                                
                                                    LOG.info("Start send NG email to : " + emailTo);
                                                    //SendEmailFail(listFailDis, emailTo, totalTripNo, criteria,errorCodeList);
                                                    SendEmailFail(listFailDis, emailTo, totalETDDate, dndnMaster,errorCodeList);
                                                }
                                            }
                                        }catch(Exception e){
                                            LOG.error(e.getMessage());
                                        }
                                        // ***Send Email Cancel ASN*********
                                        try{
                                        if (listCancelDis != null) {
                                            
                                            if (listCancelDis.size() > 0) {
                                                SpsMDensoDensoRelationWithASNDNDomain criteriaDenso = new SpsMDensoDensoRelationWithASNDNDomain();
                                                criteriaDenso.setdCd(criteria.get(i).getdCd());
                                                //criteriaDenso.setdPcd(criteria.get(i).getdPcd());
                                                List<SpsMUserDomain> listUserDenso= spsTAsnFromCIGMAService.searchUserDenso(criteriaDenso);

                                                if(listUserDenso != null){
                                                    if(listUserDenso.size()>0){
                                                        //Send Email
                                                        for(int lden=0;lden <listUserDenso.size();lden++){
                                                            SendEmailCancel(listCancelDis, listUserDenso.get(lden).getEmail(), listUserDenso.get(lden).getFirstName(), listUserDenso.get(lden).getMiddleName(), listUserDenso.get(lden).getLastName());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        }catch(Exception e){
                                            LOG.error(e.getMessage());
                                        }
                                        checkTripNo =0;
                                        totalTripNo =0;
                                        
                                        checkETDDate =0;
                                        totalETDDate =0;
                                        
                                        listErrorDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                        listSuccessDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                        listFailDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                        listCancelDis = new ArrayList<SPSTAsnHeaderDNDomain>();
                                    }
                                }
                            }
                        }
                    
                        listError = new ArrayList<SPSTAsnHeaderDNDomain>();
                        listSuccess = new ArrayList<SPSTAsnHeaderDNDomain>();
                        listFail = new ArrayList<SPSTAsnHeaderDNDomain>();
                        listCancel = new ArrayList<SPSTAsnHeaderDNDomain>();
                        
                        listAll = new ArrayList<SPSTAsnHeaderDNDomain>();
                        errorCodeList = new ArrayList<SPSTAsnHeaderDNErrorListDomain>();
                        
                        /*CommonWebServiceUtil.callCommandPO(as400Server,
                                criteria.get(i).getSchemaCd());
                        if (listError != null) {
                            if (listError.size() > 0) {
                                // ***Send Email Error PO*********
                                SendEmail(listError, criteria.get(0).getEmail());
                                listError = new ArrayList<SPSTAsnHeaderDNDomain>();
                            }
                        }*/
                    }
                    
                }

            }
        }
	}
	

	private SPSTAsnHeaderDNErrorListDomain Validate(SPSTAsnHeaderDNDomain data)
			throws ApplicationException, IOException {

		SPSTAsnHeaderDNErrorListDomain errorCode = new SPSTAsnHeaderDNErrorListDomain();
		List<String> lhError =new ArrayList<String>();
		errorCode.setErrorCodeList(lhError);
		List<SPSTAsnDNErrorListDomain> errorCodeList = new ArrayList<SPSTAsnDNErrorListDomain>();
		
		Locale locale = this.getDefaultLocale();
		boolean isSameDCD = true;
		boolean isSameDPCD = true;
		boolean isSameSCD = true;
		boolean isSameSPCD = true;
		boolean isSameScanFlag = true;
		boolean isWH = true;
		String TempDensoCode = Constants.EMPTY_STRING;
		String TempDensoPlantCode = Constants.EMPTY_STRING;
		String TempSupplierCode = Constants.EMPTY_STRING;
		String TempSupplierPlantCode = Constants.EMPTY_STRING;
		String TempScanFlag = Constants.EMPTY_STRING;
		String TempWH = Constants.EMPTY_STRING;
		int rcvLane = 0;
		boolean isValidDCd = false;
	    boolean isValidVendorCd = false;
	    boolean isValidAsnNo = true;
	    //boolean isValidDeliveryOrder = true;
	    boolean isValidDPn = true;
	    int csvLineNo = Constants.ONE;
	    boolean isValidDCdAuthen = false;
        boolean isValidSCdAuthen = false;
        int checkError =0;
		if (data.getListAsnDet() != null) {
			if (data.getListAsnDet().size() > 0) {
				AsnUploadingDomain asnUserAuthority = new AsnUploadingDomain();
				List<SpsMPlantSupplierDomain> supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
				SpsMPlantSupplierDomain supplier = new SpsMPlantSupplierDomain();
				supplier.setSCd(data.getsCd());
				supplier.setSPcd(data.getsPcd());
				supplierAuthenList.add(supplier);
				List<SpsMPlantDensoDomain> densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
				SpsMPlantDensoDomain denso = new SpsMPlantDensoDomain();
				denso.setDCd(data.getdCd());
				denso.setDPcd(data.getdPcd());
				densoAuthenList.add(denso);
				
		        asnUserAuthority.setSupplierAuthenList(supplierAuthenList);
		        asnUserAuthority.setDensoAuthenList(densoAuthenList);
		        
		        errorCode.setAsnNo(data.getAsnNo());
		        errorCode.setAsnStatus(data.getAsnStatus());
		        
				for (int i = 0; i < data.getListAsnDet().size(); i++) {
					SPSTAsnDetailDNDomain dataAsnDet = data.getListAsnDet().get(i);
					SPSTDoHeaderDNDomain dataCheck = new SPSTDoHeaderDNDomain();
					dataCheck.setDoId(dataAsnDet.getDoId());
					SPSTAsnDNErrorListDomain errorData = new SPSTAsnDNErrorListDomain();
					errorData.setAsnNo(dataAsnDet.getAsnNo());
					errorData.setdCd(dataAsnDet.getdCd());
					errorData.setsPN(dataAsnDet.getsPn());
					List<String> lError =new ArrayList<String>();
					errorData.setErrorCodeList(lError);
					List<SPSTDoHeaderDNDomain> listDataCheck =new ArrayList<SPSTDoHeaderDNDomain>();
					
					errorData.setdPN(dataAsnDet.getdPn());
					errorData.setSpsDoNo(dataAsnDet.getSpsDoNo());

                    boolean isValidDeliveryOrder = true;

					   //*** Check blank Current SPS D/O No.
					try{
		            boolean isValidFormatSpsDoNo = false;
		            if(StringUtil.checkNullOrEmpty(dataAsnDet.getSpsDoNo())){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
		                isValidDeliveryOrder = false;
		            }else{
		                if(SupplierPortalConstant.LENGTH_SPS_DO_NO 
		                    < dataAsnDet.getSpsDoNo().length()){
		                	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F12);
		                    isValidDeliveryOrder = false;
		                }else{
		                    isValidFormatSpsDoNo = true;
		                    
		                //    if(Constants.STR_ZERO.equals(splitItem.get(INDEX_ORDER_METHOD))
		                  //      || Constants.STR_ONE.equals(splitItem.get(INDEX_ORDER_METHOD))){
		                        //*** Check SPS D/O No. not found in D/O transaction
		                    	DeliveryOrderInformationDomain  doCriteria = new DeliveryOrderInformationDomain();
		                        doCriteria.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                       // doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                        
		                        // Start : [IN034] add more criteria to search D/O
		                        doCriteria.setDCd(data.getdCd());
		                        doCriteria.setDPcd(data.getdPcd());
		                        doCriteria.setVendorCd(data.getVendorCd());
		                        // End : [IN034] add more criteria to search D/O
		                        
		                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
		                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
		                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
		                        		spsTAsnFromCIGMAService.searchExistDo(doCriteria);
		                        if(null != deliveryOrderInfoList 
		                            && Constants.ZERO < deliveryOrderInfoList.size())
		                        {
		                            for(DeliveryOrderDetailDomain deliveryOrderInfo : deliveryOrderInfoList)
		                            {
		                                DeliveryOrderSubDetailDomain doHeader 
		                                    = deliveryOrderInfo.getDeliveryOrderSubDetailDomain();
		                                
		                                if (Constants.ASN_STATUS_ISS.equals(data.getAsnStatus()) && 
		                                    Constants.SHIPMENT_STATUS_CPS.equals(doHeader.getShipmentStatus())) {
		                                    errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
                                            isValidDeliveryOrder = false;
                                            break;
		                                } else if (Constants.ASN_STATUS_CCL.equals(data.getAsnStatus()) &&
		                                    Constants.SHIPMENT_STATUS_ISS.equals(
                                                doHeader.getShipmentStatus())) {
		                                    errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
                                            isValidDeliveryOrder = false;
                                            break;
		                                }
		                            }
		                        }else{
		                        	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
		                            isValidDeliveryOrder = false;
		                        }
		                        
		                        //** Check P/O status of SPS D/O No.
		                        SpsTPoDomain poDomain = spsTAsnFromCIGMAService.searchDeliveryOrderAcknowledgement(doCriteria);
		                        if(null != poDomain && !StringUtil.checkNullOrEmpty(poDomain.getPoStatus()))
		                        {
		                            if(!Constants.PO_STATUS_ACKNOWLEDGE.equals(poDomain.getPoStatus())
		                                && !Constants.PO_STATUS_FORCE_ACKNOWLEDGE.equals(
		                                    poDomain.getPoStatus()))
		                            {
		                                if(!errorCodeList.contains(
		                                    SupplierPortalConstant.UPLOAD_ERROR_CODE_T12)){
		                                	//errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T12);
		                                    isValidDeliveryOrder = false;
		                                }
		                            }
		                        }
		                   // }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
					
		            //*** Check blank Revision
					try{
		            if(StringUtil.checkNullOrEmpty(dataAsnDet.getReVision())){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F14);
		            }else{
		                if(SupplierPortalConstant.LENGTH_REVISION 
		                    < dataAsnDet.getReVision().length()){
		                	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F14);
		                }else{
		                    if(isValidDeliveryOrder){
		                        //*** Check Revision in not latest revision
		                    	DeliveryOrderInformationDomain   doCriteria = new DeliveryOrderInformationDomain();
		                        doCriteria.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                       // doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                        
		                        // Start : [IN034] add more criteria to search D/O
		                        doCriteria.setDCd(data.getdCd());
		                        doCriteria.setDPcd(data.getdPcd());
		                        doCriteria.setVendorCd(data.getVendorCd());
		                        // End : [IN034] add more criteria to search D/O
		                        
		                        doCriteria.setRevision(dataAsnDet.getReVision());
		                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
		                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
		                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList = spsTAsnFromCIGMAService.searchExistDo(doCriteria);
		                        if(null == deliveryOrderInfoList || deliveryOrderInfoList.isEmpty()){
		                        	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T14);
		                        }
		                    }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
					
		            //*** Check blank DENNO Part No.
					try{
		            if(StringUtil.checkNullOrEmpty(dataAsnDet.getdPn())){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
		                isValidDPn = false;
		            }else{
		                //*** Check DENSO Part No. length
		                if(SupplierPortalConstant.LENGTH_DENSO_PART_NO < dataAsnDet.getdPn().length()){
		                	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F16);
		                    isValidDPn = false;
		                }else{
		                    if(isValidDeliveryOrder){
		                    	DeliveryOrderInformationDomain   doCriteria = new DeliveryOrderInformationDomain();
		                        doCriteria.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                       // doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                        
		                        // Start : [IN034] add more criteria to search D/O
		                        doCriteria.setDCd(data.getdCd());
		                        doCriteria.setDPcd(data.getdPcd());
		                        doCriteria.setVendorCd(data.getVendorCd());
		                        // End : [IN034] add more criteria to search D/O
		                        
		                        doCriteria.setDPn(dataAsnDet.getdPn());
		                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
		                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
		                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList = spsTAsnFromCIGMAService.searchExistDo(doCriteria);
		                        
		                        if(null != deliveryOrderInfoList 
		                            && Constants.ZERO < deliveryOrderInfoList.size()){
		                            for(DeliveryOrderDetailDomain deliveryOrderInfo : deliveryOrderInfoList)
		                            {
		                                for(DeliveryOrderDoDetailDomain deliveryOrderDetail 
		                                    : deliveryOrderInfo.getDeliveryOrderDoDetailDomain()){
		                                    
		                                    // 7/2/2020 Fix check pn shipment status for cancel ASN
		                                    if (Constants.ASN_STATUS_ISS.equals(data.getAsnStatus()) && 
	                                            Constants.SHIPMENT_STATUS_CPS.equals(deliveryOrderDetail.getPnShipmentStatus())) {
		                                        errorData.getErrorCodeList().add(
                                                    SupplierPortalConstant.UPLOAD_ERROR_CODE_T16);
                                                isValidDPn = false;
                                                break;
	                                        } else if (Constants.ASN_STATUS_CCL.equals(data.getAsnStatus()) &&
	                                            Constants.SHIPMENT_STATUS_ISS.equals(
	                                                deliveryOrderDetail.getPnShipmentStatus())) {
	                                            errorData.getErrorCodeList().add(
                                                    SupplierPortalConstant.UPLOAD_ERROR_CODE_T16);
                                                isValidDPn = false;
                                                break;
	                                        }
		                                }
		                            }
		                        }else{
		                        	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T16);
		                            isValidDPn = false;
		                        }
		                    }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
		            
		            //*** Check blank Supplier Part No.
					try{
		            if(StringUtil.checkNullOrEmpty(dataAsnDet.getsPn())){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
		            }else{
		                //*** Check Supplier Part No. length
		                if(SupplierPortalConstant.LENGTH_SUPPLIER_PART_NO 
		                    < dataAsnDet.getsPn().length()){
		                	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F17);
		                }else{
		                    if(isValidDeliveryOrder){
		                        //*** Check Supplier Part No. not found in D/O transaction
		                    	DeliveryOrderInformationDomain  doCriteria = new DeliveryOrderInformationDomain();
		                        doCriteria.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                        //doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                        
		                        // Start : [IN034] add more criteria to search D/O
		                        doCriteria.setDCd(data.getdCd());
		                        doCriteria.setDPcd(data.getdPcd());
		                        doCriteria.setVendorCd(data.getVendorCd());
		                        // End : [IN034] add more criteria to search D/O
		                        
		                        doCriteria.setSPn(dataAsnDet.getsPn());
		                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
		                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
		                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
		                        		spsTAsnFromCIGMAService.searchExistDo(doCriteria);
		                        if(null == deliveryOrderInfoList 
		                            || Constants.ZERO == deliveryOrderInfoList.size()){
		                        	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T17);
		                        }
		                    }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
		            
		            //*** Check RCV Lane length
					try{
		            if(SupplierPortalConstant.LENGTH_RCV_LANE < dataAsnDet.getRcvLane().length()){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F18);
		            }else{
		                
		                //*** Check RCV Lane not match in D/O transaction data
		                if(isValidDeliveryOrder && isValidDPn){
		                	DeliveryOrderInformationDomain   doCriteria = new DeliveryOrderInformationDomain();
		                    doCriteria.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                    //doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                    
		                    // Start : [IN034] add more criteria to search D/O
		                    doCriteria.setDCd(data.getdCd());
		                    doCriteria.setDPcd(data.getdPcd());
		                    doCriteria.setVendorCd(data.getVendorCd());
		                    // End : [IN034] add more criteria to search D/O
		                    
		                    doCriteria.setDPn(dataAsnDet.getdPn());
		                    doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
		                    doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
		                    List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
		                    		spsTAsnFromCIGMAService.searchExistDo(doCriteria);
		                    
		                    boolean hasRcvLane = false;
		                    if(null != deliveryOrderInfoList 
		                        && Constants.ZERO < deliveryOrderInfoList.size()){
		                        for(DeliveryOrderDetailDomain deliveryOrderInfo : deliveryOrderInfoList)
		                        {
		                            for(DeliveryOrderDoDetailDomain deliveryOrderDetail 
		                                : deliveryOrderInfo.getDeliveryOrderDoDetailDomain()){
		                                if(deliveryOrderDetail.getRcvLane().trim().equals(
		                                   dataAsnDet.getRcvLane().trim()))
		                                {
		                                    hasRcvLane = true;
		                                    break;
		                                }
		                            }
		                        }
		                    }
		                    if(!hasRcvLane){
		                    	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T18);
		                    }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
					
					
		            //*** Check blank Shipping QTY.
					
					try{
		            if(StringUtil.checkNullOrEmpty(dataAsnDet.getShippingQty())){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
		            }else{
		                //*** Check Shipping QTY data type
		                if(!StringUtil.isNumeric(dataAsnDet.getShippingQty().toString())){
		                	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
		                }else{
		                    //*** Check Shipping QTY. length
		                    boolean isNumberFormat = true;
		                    BigDecimal shippingQty = dataAsnDet.getShippingQty();//new BigDecimal(splitItem.get(INDEX_SHIPPING_QTY));
		                    if(dataAsnDet.getShippingQty().toString().contains(Constants.SYMBOL_DOT)){
		                        if(!StringUtil.isNumberFormat(
		                        		dataAsnDet.getShippingQty().toString(), Constants.NINE, Constants.TWO)){
		                            isNumberFormat = false;
		                        }
		                    }else{
		                        if(Constants.SEVEN < shippingQty.precision()){
		                            isNumberFormat = false;
		                        }
		                    }
		                    if(!isNumberFormat){
		                    	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F19);
		                    }
		                    //*** Check Shipping QTY < 0
		                    if(shippingQty.compareTo(new BigDecimal(Constants.ZERO)) < Constants.ZERO){
		                    	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T19);
		                    }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
		            
		          //*** Check blank U/M
					try{
		            if(StringUtil.checkNullOrEmpty(dataAsnDet.getUnitOfMeasure())){
		            	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
		            }else{
		                //*** Check U/M length
		                if(SupplierPortalConstant.LENGTH_UM 
		                    < dataAsnDet.getUnitOfMeasure().length()){
		                	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F20);
		                }else{
		                    if(isValidDeliveryOrder && isValidDPn){
		                        //*** Check U/M match with D/O transaction
		                    	DeliveryOrderInformationDomain  doCriteria = new DeliveryOrderInformationDomain();
		                        doCriteria.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                        //doCriteria.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                        
		                        // Start : [IN034] add more criteria to search D/O
		                        doCriteria.setDCd(data.getdCd());
		                        doCriteria.setDPcd(data.getdPcd());
		                        doCriteria.setVendorCd(data.getVendorCd());
		                        // End : [IN034] add more criteria to search D/O
		                        
		                        doCriteria.setDPn(dataAsnDet.getdPn());
		                        doCriteria.setUnitOfMeasure(dataAsnDet.getUnitOfMeasure());
		                        doCriteria.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
		                        doCriteria.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
		                        List<DeliveryOrderDetailDomain> deliveryOrderInfoList =
		                        		spsTAsnFromCIGMAService.searchExistDo(doCriteria);
		                        if(null == deliveryOrderInfoList || deliveryOrderInfoList.isEmpty()){
		                        	errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T20);
		                        }
		                       /* AcknowledgedDoInformationDomain acknowledgedDOInformationDomain = new AcknowledgedDoInformationDomain();
		                        acknowledgedDOInformationDomain.setDCd(data.getdCd());
		                        acknowledgedDOInformationDomain.setDPcd(data.getdPcd());
		                        acknowledgedDOInformationDomain.setVendorCd(data.getVendorCd());
		                        acknowledgedDOInformationDomain.setSPcd(data.getsPcd());
		                        acknowledgedDOInformationDomain.setSpsDoNo(dataAsnDet.getSpsDoNo());
		                       // acknowledgedDOInformationDomain.setOrderMethod(splitItem.get(INDEX_ORDER_METHOD));
		                        acknowledgedDOInformationDomain.setShipmentStatus(Constants.MISC_CODE_ALL);
		                        acknowledgedDOInformationDomain.setSupplierAuthenList(supplierAuthenList);
		                        acknowledgedDOInformationDomain.setDensoAuthenList(densoAuthenList);
		                        
		                        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInformationResultList = spsTAsnFromCIGMAService.
		                            searchAcknowledgedDo(acknowledgedDOInformationDomain);
		                        if(Constants.ZERO == acknowledgedDOInformationResultList.size()){
		                            MessageUtil.throwsApplicationMessage(locale,
		                                SupplierPortalConstant.ERROR_CD_SP_E7_0042,
		                                dataAsnDet.getSpsDoNo());
		                        }*/
		                    }
		                }
		            }
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
		           
					//Already check on top
					// ***********Check is exist SPS and Do status not
					// CCL***************************************
					/*try{
					listDataCheck = spsTAsnFromCIGMAService.searchDONotCcl(dataCheck);
					if (listDataCheck != null) {
						if (listDataCheck.size() <= 0) {
							
							String message =MessageUtil
									.getApplicationMessageHandledException(
											locale,
											SupplierPortalConstant.ERROR_CD_SP_E6_0073,
											new String[] { dataAsnDet
													.getAsnNo() });
							errorData.getErrorCodeList().add(message);
							LOG.error(MessageUtil
									.getApplicationMessageHandledException(
											locale,
											SupplierPortalConstant.ERROR_CD_SP_E6_0073,
											new String[] { dataAsnDet
													.getAsnNo() }));
						}
					} else {
						errorData.getErrorCodeList()
								.add(MessageUtil
										.getApplicationMessageHandledException(
												locale,
												SupplierPortalConstant.ERROR_CD_SP_E6_0073,
												new String[] { dataAsnDet
														.getAsnNo() }));
						LOG.error(MessageUtil
								.getApplicationMessageHandledException(
										locale,
										SupplierPortalConstant.ERROR_CD_SP_E6_0073,
										new String[] { dataAsnDet.getAsnNo() }));
					}
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}*/
					// ************Check If ASN status is CCL, previous version
					// must exists in SPS***************
					try{
					if (data.getAsnStatus().equals("CCL")) {
						dataCheck.setSpsDoNo(dataAsnDet.getSpsDoNo());
						
						  int found  = spsTAsnFromCIGMAService.SearchCountPrevASN(data);
						  
						  if (found == 0) {
	                              errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T05);
	                                LOG.error(MessageUtil
	                                        .getApplicationMessageHandledException(
	                                                locale,
	                                                SupplierPortalConstant.ERROR_CD_SP_E6_0072,
	                                                new String[] { dataAsnDet
	                                                        .getAsnNo() }));
	                            
						  } 
						/*listDataCheck = spsTAsnFromCIGMAService
								.searchDOPrevCcl(dataCheck);
					
						if (listDataCheck != null) {
							if (listDataCheck.size() <= 0) {
								errorData.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T05);
								LOG.error(MessageUtil
										.getApplicationMessageHandledException(
												locale,
												SupplierPortalConstant.ERROR_CD_SP_E6_0072,
												new String[] { dataAsnDet
														.getAsnNo() }));
							}
						} else {
							errorData.getErrorCodeList()
									.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T05);
							LOG.error(MessageUtil
									.getApplicationMessageHandledException(
											locale,
											SupplierPortalConstant.ERROR_CD_SP_E6_0072,
											new String[] { dataAsnDet
													.getAsnNo() }));
						}*/
					}
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
					// **************Check Is Same
					// *******************************
					try{

						listDataCheck = spsTAsnFromCIGMAService.searchDO(dataCheck);
						if (listDataCheck != null) {
							if (listDataCheck.size() > 0) {
								// Denso Code is same
								if (!StringUtil.checkNullOrEmpty(TempDensoCode)) {
									if (!TempDensoCode.equals(listDataCheck.get(0).getdCd())) {
										isSameDCD = false;
									}
								} else {
									TempDensoCode = listDataCheck.get(0).getdCd();
								}

								// Denso Plant Code is same
								if (!StringUtil.checkNullOrEmpty(TempDensoPlantCode)) {
									if (!TempDensoPlantCode.equals(listDataCheck.get(0).getdPcd())) {
										isSameDPCD = false;
									}
								} else {
									TempDensoPlantCode = listDataCheck.get(0).getdPcd();
								}

								// All Supplier Code is same
								if (!StringUtil.checkNullOrEmpty(TempSupplierCode)) {
									if (!TempSupplierCode.equals(listDataCheck.get(0).getsCd())) {
										isSameSCD = false;
									}
								} else {
									TempSupplierCode = listDataCheck.get(0).getsCd();
								}

								// All Supplier Plan code is same
								if (!StringUtil.checkNullOrEmpty(TempSupplierPlantCode)) {
									if (!TempSupplierPlantCode.equals(listDataCheck.get(0).getsPcd())) {
										isSameSPCD = false;
									}
								} else {
									TempSupplierPlantCode = listDataCheck.get(0).getsPcd();
								}

							}
						}
					

					if (isSameScanFlag) {
						listDataCheck = spsTAsnFromCIGMAService.searchDO(dataCheck);
						if (listDataCheck != null) {
							if (listDataCheck.size() > 0) {
								// All scan flag is same
								if (!StringUtil.checkNullOrEmpty(TempScanFlag)) {
									if (!TempScanFlag.equals(listDataCheck.get(0).getReceiveByScanFlag())) {
										isSameScanFlag = false;
									}
								} else {
									TempScanFlag = listDataCheck.get(0).getReceiveByScanFlag();
								}
							}
						}
					}
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
					
					if (data.getReceiveByScanFlag().equals("Y")) {
						// Check all W/H
						if (isWH) {
							try{
								listDataCheck = spsTAsnFromCIGMAService
										.searchDO(dataCheck);
								if (listDataCheck != null) {
									if (listDataCheck.size() > 0) {
										if (!StringUtil.checkNullOrEmpty(TempWH)) {
											if (!TempWH.equals(listDataCheck.get(0)
													.getWhPrimeReceiving())) {
												isWH = false;
											}
										} else {
											TempWH = listDataCheck.get(0)
													.getWhPrimeReceiving();
										}
									}
								}
							}catch(Exception e){
								checkError++;
								LOG.error(e.getMessage());
							}
						}

						// Check rcv lane
						try {
							rcvLane += Integer.parseInt(dataAsnDet.getRcvLane());
						} catch (Exception ex) {

						}
					}

					// Check Shipping qty
					try{
					
					SPSTDoDetailDNDomain dataDetCheck = new SPSTDoDetailDNDomain();
					dataDetCheck.setChgCigmaDoNo(dataAsnDet.getCigmaDoNo());
				    dataDetCheck.setDoId(dataAsnDet.getDoId());
					dataDetCheck.setdPn(dataAsnDet.getdPn());
					dataDetCheck.setsPn(dataAsnDet.getsPn());
					dataDetCheck.setCtrlNo(dataAsnDet.getAsnNo());
					dataDetCheck.setRcvLane(dataAsnDet.getRcvLane());
					dataDetCheck.setSpsDoNo(dataAsnDet.getSpsDoNo());
					
					List<SPSTDoDetailDNDomain> listDataDetCheck = spsTAsnFromCIGMAService.searchDODetail(dataDetCheck);
					if (listDataDetCheck != null) {
						if (listDataDetCheck.size() > 0) {
						    
							if (dataAsnDet.getShippingQty().compareTo(
									listDataDetCheck.get(0)
											.getCurrentOrderQty()) > 0) {
								errorData.getErrorCodeList()
										.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T19);
								LOG.error(MessageUtil
										.getApplicationMessageHandledException(
												locale,
												SupplierPortalConstant.ERROR_CD_SP_E6_0078,
												new String[] { dataAsnDet
														.getAsnNo() }));
							}
						}
					}
					
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
					
					
					if(errorData != null){
						if(errorData.getErrorCodeList() != null){
							if(errorData.getErrorCodeList().size() >0){
								errorCodeList.add(errorData);
							}
						}
					}
				}
				errorCode.setErrorListDet(errorCodeList);
				// Set error code is not same data
				try{
				if (!isSameDCD) {
					errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T01);
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E6_0074,
									new String[] { data.getAsnNo() }));
				}
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				
				try{
				if (!isSameDPCD) {
					errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T02);
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E6_0074,
									new String[] { data.getAsnNo() }));
				}
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				try{
					if (!isSameSCD) {
						errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T03);
						LOG.error(MessageUtil
								.getApplicationMessageHandledException(locale,
										SupplierPortalConstant.ERROR_CD_SP_E6_0074,
										new String[] { data.getAsnNo() }));
					}
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
				
				try{
					if (!isSameSPCD) {
						errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T04);
						LOG.error(MessageUtil
								.getApplicationMessageHandledException(locale,
										SupplierPortalConstant.ERROR_CD_SP_E6_0074,
										new String[] { data.getAsnNo() }));
					}
					}catch(Exception e){
						checkError++;
						LOG.error(e.getMessage());
					}
				// Set error code is not same data scanflag
				try{
				if (!isSameScanFlag) {
					errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T21);
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E6_0075,
									new String[] { data.getAsnNo() }));
				}
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				// Set error code is not same WH_PRIME_RECEIVING
				try{
				if (!isWH) {
				    errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T22);
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E6_0076,
									new String[] { data.getAsnNo() }));
				}
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				
				// Set error total of (record) of same RCV lane <=
				// MAX_RECORD_FOR RECEIVE_BY_SCAN
				try{
				if (rcvLane > SupplierPortalConstant.MAX_RECORD_FOR_RECEIVE_BY_SCAN) {
					errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_T18);
					LOG.error(MessageUtil
							.getApplicationMessageHandledException(locale,
									SupplierPortalConstant.ERROR_CD_SP_E6_0077,
									new String[] { data.getAsnNo() }));
				}
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				
				
		        
				//*** Check blank DENSO code
				try{
	            if(StringUtil.checkNullOrEmpty(data.getdCd())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F01);
	            }else{
	                //*** Check DENSO code length
	                if(SupplierPortalConstant.LENGTH_DENSO_CODE 
	                    < data.getdCd().length()){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F01);
	                }else{
	                    //Set Company Denso Code format to 5 digits
	                   /* splitItem.set(INDEX_DCD,
	                        String.format(Constants.DCD_FORMAT, splitItem.get(INDEX_DCD)));*/
	                    
	                    //*** Check exist DENSO company code in master data
	                    companyDensoCriteria = new SpsMCompanyDensoCriteriaDomain();
	                    companyDensoCriteria.setDCd(data.getdCd());
	                    companyDensoCriteria.setIsActive(Constants.IS_ACTIVE);
	                    int countDensoCompany = spsTAsnFromCIGMAService.searchCount(
	                        companyDensoCriteria);
	                    if(Constants.ZERO == countDensoCompany){
	                    	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
	                    }else{
	                        isValidDCd = true;
	                    }
	                    
	                    
	                    //*** Check user have authority in this DENSO code
	                    for(SpsMPlantDensoDomain densoAuthen : asnUserAuthority.getDensoAuthenList()){
	                        if(densoAuthen.getDCd().equals(data.getdCd())){
	                            isValidDCdAuthen = true;
	                            break;
	                        }
	                    }
	                    
	                    if(!isValidDCdAuthen){
	                    	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A01);
	                    }
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	            //*** Check blank DENSO plant code
				try{
	            if(StringUtil.checkNullOrEmpty(data.getdPcd())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F02);
	            }else{
	                //*** Check DENSO plant code length
	                if(SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE 
	                    < data.getdPcd().length()){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F02);
	                }else{
	                    //Set DENSO plant code format to 2 digits
	                   // splitItem.set(INDEX_DPCD, StringUtil.rightPad(
	                      //  splitItem.get(INDEX_DPCD), Constants.TWO));
	                    
	                    //*** Check exist DENSO plant code in master data
	                    if(isValidDCd){
	                    	SpsMPlantDensoCriteriaDomain  plantDensoCriteria = new SpsMPlantDensoCriteriaDomain();
	                        plantDensoCriteria.setDCd(data.getdCd());
	                        plantDensoCriteria.setDPcd(data.getdPcd());
	                        plantDensoCriteria.setIsActive(Constants.IS_ACTIVE);
	                        int countDensoPlant = spsTAsnFromCIGMAService.searchCountPlant(plantDensoCriteria);
	                        if(Constants.ZERO == countDensoPlant){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
	                        }
	                    }
	                    
	                    //*** Check user have authority in this DENSO plant code
	                    if(isValidDCd && isValidDCdAuthen){
	                        boolean isValidDPcdAuthen = false;
	                        for(SpsMPlantDensoDomain densoAuthen 
	                            : asnUserAuthority.getDensoAuthenList()){
	                            if(densoAuthen.getDCd().equals(data.getdCd())
	                                && densoAuthen.getDPcd().equals(data.getdPcd())){
	                                isValidDPcdAuthen = true;
	                                break;
	                            }
	                        }
	                        if(!isValidDPcdAuthen){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A02);
	                        }
	                    }
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	            
	            //*** Check blank vendor code
				try{
	            if(StringUtil.checkNullOrEmpty(data.getVendorCd())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F03);
	            }else{
	                //*** Check vendor code length
	                if(SupplierPortalConstant.LENGTH_VENDOR_CD
	                    < data.getVendorCd().length()){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F03);
	                }else{
	                    //*** Check exist vendor code in master data
	                    if(isValidDCd){
	                    	SpsMAs400VendorCriteriaDomain  vendorCriteria = new SpsMAs400VendorCriteriaDomain();
	                        vendorCriteria.setDCd(data.getdCd());
	                        vendorCriteria.setVendorCd(data.getVendorCd());
	                        vendorCriteria.setIsActive(Constants.IS_ACTIVE);
	                        List<SpsMAs400VendorDomain> vendorList
	                            = spsTAsnFromCIGMAService.searchByConditionAS400(vendorCriteria);
	                        if(null == vendorList || vendorList.isEmpty()){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M03);
	                        }else{
	                            isValidVendorCd = true;
	                           // sCd = StringUtil.nullToEmpty(vendorList.get(Constants.ZERO).getSCd());
	                        }
	                        
	                        //*** Check user have authority in this company supplier code
	                        for(SpsMPlantSupplierDomain supplierAuthen
	                            : asnUserAuthority.getSupplierAuthenList()){
	                            if(supplierAuthen.getSCd().equals(data.getsCd())){
	                                isValidSCdAuthen = true;
	                                break;
	                            }
	                        }
	                        if(!isValidSCdAuthen){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A03);
	                        }
	                    }
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	            //*** Check blank supplier plant code
				try{
	            if(StringUtil.checkNullOrEmpty(data.getsPcd())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F04);
	            }else{
	                //*** Check supplier plant code length
	                if(SupplierPortalConstant.LENGTH_SUPPLIER_PLANT_CODE 
	                    < data.getsPcd().length()){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F04);
	                }else{
	                    //*** Check exist supplier plant code in master data
	                    if(isValidVendorCd){
	                       SpsMPlantSupplierCriteriaDomain  plantSupplierCriteria = new SpsMPlantSupplierCriteriaDomain();
	                        plantSupplierCriteria.setSCd(data.getsCd());
	                        plantSupplierCriteria.setSPcd(data.getsPcd());
	                        plantSupplierCriteria.setIsActive(Constants.IS_ACTIVE);
	                        int countSupplierPlant = spsTAsnFromCIGMAService.searchCountPlantSupplier(plantSupplierCriteria);
	                        if(Constants.ZERO == countSupplierPlant){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M04);
	                        }
	                        
	                        //*** Check user have authority in this supplier plant code
	                        if(isValidSCdAuthen){
	                            boolean isValidSPcdAuthen = false;
	                            for(SpsMPlantSupplierDomain supplierAuthen
	                                : asnUserAuthority.getSupplierAuthenList()){
	                                if(supplierAuthen.getSCd().equals(data.getsCd())
	                                    && supplierAuthen.getSPcd().equals(data.getsPcd())){
	                                    isValidSPcdAuthen = true;
	                                    break;
	                                }
	                            }
	                            if(!isValidSPcdAuthen){
	                            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_A04);
	                            }
	                        }
	                    }
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	            //*** Check blank ASN No.
				try{
	            if(StringUtil.checkNullOrEmpty(data.getAsnNo())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
	                isValidAsnNo = false;
	            }else{
	                //*** Check ASN No. Length
	                String asnNoWithFormat =  data.getAsnNo();
	                if(asnNoWithFormat.length() < Constants.ELEVEN 
	                    || Constants.SIXTEEN < asnNoWithFormat.length())
	                {
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
	                    isValidAsnNo = false;
	                }else{
	                    //*** Check ASN No. Format
	                    if(!StringUtil.isNumeric(asnNoWithFormat.substring(
	                        Constants.ONE, Constants.TWO)) 
	                        || !asnNoWithFormat.substring(Constants.TWO, Constants.THREE).matches(
	                            Constants.REGX_ASN_NO_MONTH_FORMAT)
	                        || !asnNoWithFormat.substring(Constants.THREE, Constants.FIVE).matches(
	                            Constants.REGX_ASN_NO_DATE_FORMAT)
	                        || !Constants.STR_A.equals(asnNoWithFormat.substring(
	                            Constants.SEVEN, Constants.EIGHT))
	                        || !StringUtil.isNumeric(asnNoWithFormat.substring(
	                            Constants.EIGHT, Constants.TEN)))
	                    {
	                    	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
	                        isValidAsnNo = false;
	                    }
	                    
	                    String sPcdFirstDigit = Constants.EMPTY_STRING;
	                    String densoPlant = data.getdPcd();
	                    String vendorCd =data.getVendorCd();
	                    
	                    if(!StringUtil.checkNullOrEmpty(data.getsPcd())){
	                        sPcdFirstDigit = data.getsPcd()
	                            .substring(Constants.ZERO, Constants.ONE);
	                    }
	                    
	                    if(!asnNoWithFormat.substring(Constants.ZERO, Constants.ONE)
	                        .equals(sPcdFirstDigit)
	                        || !asnNoWithFormat.substring(Constants.FIVE, Constants.SEVEN)
	                            .equals(densoPlant)
	                        || !asnNoWithFormat.substring(Constants.TEN).equals(vendorCd))
	                    {
	                        if(!errorCodeList.contains(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05)){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F05);
	                            isValidAsnNo = false;
	                        }
	                    }
	                }
	            }
	            
	           if(isValidAsnNo && isValidDCd){
	                //*** Check ASN No. not found in ASN Information
	               if(!data.getAsnStatus().equals("CCL")){
	                   AsnDomain asnDomain = new AsnDomain();
	                    asnDomain.setAsnNo(data.getAsnNo());
	                    asnDomain.setDCd(data.getdCd());
	                    asnDomain.setSupplierAuthenList(asnUserAuthority.getSupplierAuthenList());
	                    asnDomain.setDensoAuthenList(asnUserAuthority.getDensoAuthenList());
	                    List<AsnDomain> asnDomainResultList =  asnService.searchExistAsn(asnDomain);
	                    if(Constants.ZERO < asnDomainResultList.size()){
	                        errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_D05);
	                        isValidAsnNo = false;
	                    }
	               }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	           
				//*** Check blank Plan ETA
				try{
	            if(StringUtil.checkNullOrEmpty(data.getPlanEta())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
	            }else{
	                //*** Check Plan ETA data type
	                if(!DateUtil.isValidDate(data.getPlanEta(),
	                    DateUtil.PATTERN_YYYYMMDD)){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				
				try{
	                if(StringUtil.checkNullOrEmpty(data.getPlanEtaTime())){
	                    errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
	                }else{
	                    //*** Check Plan ETA Time data type
	                    if(!data.getPlanEtaTime().matches(Constants.REGX_TIME24HOURSML_FORMAT)){
	                        errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F06);
	                    }
	                }
	                }catch(Exception e){
	                    checkError++;
	                    LOG.error(e.getMessage());
	                }
				
				//*** Check blank Delivery Date
				try{
	                if(StringUtil.checkNullOrEmpty(data.getActualEta())){
	                    errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
	                }else{
	                    //*** Check Delivery data type
	                    if(!DateUtil.isValidDate(data.getActualEta(),
	                        DateUtil.PATTERN_YYYYMMDD)){
	                        errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
	                    }
	                }
	                }catch(Exception e){
	                    checkError++;
	                    LOG.error(e.getMessage());
	            }
				try{
                    if(StringUtil.checkNullOrEmpty(data.getActualEtaTime())){
                        errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
                    }else{
                      //*** Check Delivery Time data type
                        if(!data.getActualEtaTime().matches(Constants.REGX_TIME24HOURSML_FORMAT)){
                            errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F07);
                        }
                    }
                    }catch(Exception e){
                        checkError++;
                        LOG.error(e.getMessage());
                }
                
				//*** Check blank Actual Departure
				try{
	            if(StringUtil.checkNullOrEmpty(data.getPlanEtd())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
	            }else{
	                //*** Check Actual Departure data type
	                if(!DateUtil.isValidDate(data.getPlanEtd(),
	                    DateUtil.PATTERN_YYYYMMDD)){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				try{
	                if(StringUtil.checkNullOrEmpty(data.getPlanEtdTime())){
	                    errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
	                }else{
	                    //*** Check Actual Departure Time data type
	                    if(!data.getPlanEtdTime().matches(Constants.REGX_TIME24HOURSML_FORMAT)){
	                        errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F08);
	                    }
	                }
	                }catch(Exception e){
	                    checkError++;
	                    LOG.error(e.getMessage());
	                }
	            //*** Check blank Shipping Time
				try{
	                if(StringUtil.checkNullOrEmpty(data.getActualEtd())){
	                    errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
	                }else{
	                    //*** Check Shipping Time data type
	                    if(!DateUtil.isValidDate(data.getActualEtd(),
	                        DateUtil.PATTERN_YYYYMMDD)){
	                        errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
	                    }
	                }
	                }catch(Exception e){
	                    checkError++;
	                    LOG.error(e.getMessage());
	                }
				try{
	            if(StringUtil.checkNullOrEmpty(data.getActualEtdTime())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
	            }else{
	                //*** Check Shipping Time data type
	                if(!data.getActualEtdTime().matches(Constants.REGX_TIME24HOURSML_FORMAT)){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F09);
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	            
	            //*** Check blank No of Pallet.
				try{
	            if(StringUtil.checkNullOrEmpty(data.getNumberOfPallet())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
	            }else{
	                //*** Check No. of Pallet data type
	                if(!StringUtil.isNumeric(data.getNumberOfPallet())){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
	                }else{
	                    //*** Check Digit of No. of Pallet
	                    if(data.getNumberOfPallet().contains(Constants.SYMBOL_DOT)){
	                        if(!StringUtil.isNumberFormat(
	                           data.getNumberOfPallet(), Constants.THIRTEEN, Constants.TWO)){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
	                        }
	                    }else{
	                        BigDecimal noOfPallet = new BigDecimal(data.getNumberOfPallet());
	                        if(Constants.ELEVEN < noOfPallet.precision()){
	                        	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F10);
	                        }
	                    }
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
	            
	            //*** Check blank Trip No.
				try{
	            if(StringUtil.checkNullOrEmpty(data.getTripNo())){
	            	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
	            }else{
	                //*** Check Trip No length
	                if(!StringUtil.isPositiveInteger(data.getTripNo())){
	                	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
	                }else{
	                    if(SupplierPortalConstant.LENGTH_TRIP_NO 
	                        < data.getTripNo().length()){
	                    	errorCode.getErrorCodeList().add(SupplierPortalConstant.UPLOAD_ERROR_CODE_F11);
	                    }
	                }
	            }
				}catch(Exception e){
					checkError++;
					LOG.error(e.getMessage());
				}
				
			}
		}
         if(checkError >0){
        	 
        	 errorCode.getErrorCodeList().add("system error");
         }
		return errorCode;
	}

	private void SendEmail(List<SPSTAsnHeaderDNDomain> listData,String emailTo) {

		Locale locale = this.getDefaultLocale();
		if (listData != null) {
			if (listData.size() > 0) {
				try {
					Collections.sort(listData,new SPSTAsnAsnComparator());
					SendEmailDomain email = new SendEmailDomain();

					Calendar calendar = Calendar.getInstance(Locale.US);
					String day = String.format("%1$02d",
							calendar.get(Calendar.DATE));
					String month = String.format("%1$02d",
							calendar.get(Calendar.MONTH) + 1);
					int year = calendar.get(Calendar.YEAR);
					String hour = String.format("%1$02d",
							calendar.get(Calendar.HOUR));
					String minute = String.format("%1$02d",
							calendar.get(Calendar.MINUTE));
					String milli = String.format("%1$02d",
							calendar.get(Calendar.SECOND));

					if (year > 2500) {
						year = year - 543;
					}
					String sdateTime = day + "/" + month + "/"
							+ String.valueOf(year) + " " + hour + ":" + minute;

					String title = "Transfer ASN failed for "
							+ listData.get(0).getAsnNo() + " on (" + sdateTime
							+ ")";
					String content = "Cannot create ASN at CIGMA system for ASN No. : \r\n";
					String emailFrom = ContextParams.getBaseEmail();
					for (int j = 0; j < listData.size(); j++) {
						if (j == 0) {
							content += listData.get(j).getAsnNo();
						} else {
							content += ", " + listData.get(j).getAsnNo();
						}

						content += " (" + listData.get(j).getMsgError() + ")";
						MessageUtil
								.getErrorMessageForBatch(
										locale,
										SupplierPortalConstant.ERROR_CD_SP_E6_0069,
										new String[] { listData.get(j)
												.getAsnNo().toString()
												+ ":"
												+ listData.get(j)
														.getMsgError() }, LOG,
										Constants.ZERO, false);
					}
					email.setHeader(title);
					email.setEmailSmtp(ContextParams.getEmailSmtp());
					email.setContents(content);
					email.setEmailFrom(emailFrom);
					email.setEmailTo(emailTo);
					mailService.sendEmail(email);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
    
	private void SendEmailSuccess(List<SPSTAsnHeaderDNDomain> listData,String emailTo,int total,List<SpsMDensoDensoRelationWithASNDNDomain> criteria) {

		Locale locale = this.getDefaultLocale();
		BigDecimal cusNo = new BigDecimal(0);
        String shipNo = "";
        String dCd = "";
        String dPcd = "";
        String sCd = "";
        String sPcd = "";
          
		if (listData != null) {
			if (listData.size() > 0) {
				try {
					SendEmailDomain email = new SendEmailDomain();
					Collections.sort(listData,new SPSTAsnAsnComparator());
					Calendar calendar = Calendar.getInstance(Locale.US);
					String day = String.format("%1$02d",
							calendar.get(Calendar.DATE));
					String month = String.format("%1$02d",
							calendar.get(Calendar.MONTH) + 1);
					int year = calendar.get(Calendar.YEAR);
					String hour = String.format("%1$02d",
							calendar.get(Calendar.HOUR));
					String minute = String.format("%1$02d",
							calendar.get(Calendar.MINUTE));
					String milli = String.format("%1$02d",
							calendar.get(Calendar.SECOND));

					if (year > 2500) {
						year = year - 543;
					}
					String sdateTime = day + "/" + month + "/"
							+ String.valueOf(year) + " " + hour + ":" + minute;

					String sDate =String.valueOf(year) + "/" + month + "/"+ day ;
					String actualEtd = listData.get(0).getActualEtd().substring(0,4)+"/"+listData.get(0).getActualEtd().substring(4,6)+"/"+listData.get(0).getActualEtd().substring(6,8);
                    
					String sTime =hour + ":" + minute;
					String title = "[ OK Result ] ASN document for "+listData.get(0).getdCd()+" / Trip no. "+listData.get(0).getTripNo()+" on "+actualEtd+" has been updated. ";
					String body ="Dear All,";
					 body = 
                     body += "<br>";
                     body += "ASN for "+listData.get(0).getdCd()+" customer / Trip no. "+listData.get(0).getTripNo()+" on "+actualEtd+" / has been updated, OK result of this transaction is <a style='color:green;font-weight:700'>"+listData.size()+"/"+total+" (number of ASN).</a> Please go to SPS website to print out ASN which has &apos;ISS&apos; status as following list.";
                     body += "<br>";
                     body += "<br>";
                     body += "Ps. In case this transaction  isn&apos;t complete totally, user can see detail from &apos;NG result&apos; e-mail which be sent about the same time of this e-mail.";

                     body += "<br>";
                     body += " <table style='border-collapse: collapse;font-size:10pt;font-family: Arial;' border='1px' cellspacing='0' cellpadding='4'>";
                     body += " <tr bgcolor='#DCDCDC'>";
                     body += "  <th>No.</th>";
                     body += "  <th>Customer</th>";
                     body += "  <th>Ship to</th>";
                     body += "  <th>D.CD</th>";
                     body += "  <th>D.PCD</th>";
                     body += "  <th>S.CD</th>";
                     body += "  <th>S.PCD</th>";
                     body += "  <th>Truck no.</th>";
                     body += "  <th>ASN no.</th>";
                     body += "  <th>Action type</th>";
                     body += "  <th>Status</th>";
                     body += "  <th>Result</th>";
                     body += "  </tr>";
					String emailFrom = ContextParams.getBaseEmail();
					String trColor=Constants.EMPTY_STRING;
					for (int j = 0; j < listData.size(); j++) {
						String actionType ="";
						
						
						if(listData.get(j).getAsnStatus().equals("SPS")){
							actionType="ASN Status";
						}
						if(listData.get(j).getAsnStatus().equals("ISS")){
							actionType="Issue";
						}
						if(listData.get(j).getAsnStatus().equals("CCL")){
							actionType="Cancel";
						}
						
						// 6/2/2020 Find customer code and ship no
						cusNo = new BigDecimal(0);
				        shipNo = "";
				        dCd = "";
				        dPcd = "";
				        sCd = "";
				        sPcd = "";
                        for (int k = 0; k < criteria.size(); k++)
                        {
                            if(listData.get(j).getsCd().trim().equals(criteria.get(k).getsCd().trim()) &&
                                listData.get(j).getsPcd().trim().equals(criteria.get(k).getsPcd().trim()) &&
                                listData.get(j).getdCd().trim().equals(criteria.get(k).getdCd().trim()) &&
                                listData.get(j).getdPcd().trim().equals(criteria.get(k).getdPcd().trim()) &&
                                listData.get(j).getVendorCd().trim().equals(criteria.get(k).getVendorCd().trim())
                                ){
                                cusNo = criteria.get(k).getCusNo();
                                shipNo = criteria.get(k).getShipNo().trim();
                                dCd = criteria.get(k).getdCd().trim();
                                dPcd = criteria.get(k).getdPcd().trim();
                                sCd = criteria.get(k).getsCd().trim();
                                sPcd = criteria.get(k).getsPcd().trim();
                                    
                                break;
                            }
                        }
                        // end find customer no and ship no
                        
						 body += " <tr "+trColor+">";
						 body += " <td>"+(j+1)+"</td>";
						 body += " <td>"+cusNo+"</td>";
						 body += " <td>"+shipNo+"</td>";
						 body += " <td>"+dCd+"</td>";
						 body += " <td>"+dPcd+"</td>";
						 body += " <td>"+sCd+"</td>";
						 body += " <td>"+sPcd+"</td>";
						 body += " <td>"+listData.get(j).getTruckNo()+"</td>";
						 body += " <td>"+listData.get(j).getAsnNo()+"</td>";
						 body += " <td>"+actionType+"</td>";
						 body += " <td>"+listData.get(j).getAsnStatus()+"</td>";
						 body += " <td>OK</td>";
						 body += " </tr>";
						 
						 if(!StringUtil.checkNullOrEmpty(trColor)){
								trColor=Constants.EMPTY_STRING;
							}else{
								trColor="bgcolor='#FFFFE0'";
							}
					}
					 body += "  </table>";
                     body += "<br>";
                     body += "https://sps.asia.gscm.globaldenso.com/SPS/";
					email.setHeader(title);
					email.setEmailSmtp(ContextParams.getEmailSmtp());
					email.setContents(body);
					email.setEmailFrom(emailFrom);
					email.setEmailTo(emailTo);
					mailService.sendEmail(email);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	private void SendEmailFail(List<SPSTAsnHeaderDNDomain> listData,String emailTo,int total,List<SpsMDensoDensoRelationWithASNDNDomain> criteria,List<SPSTAsnHeaderDNErrorListDomain> errorList) {

		Locale locale = this.getDefaultLocale();
		BigDecimal cusNo = new BigDecimal(0);
        String shipNo = "";
        String dCd = "";
        String dPcd = "";
        String sCd = "";
        String sPcd = "";
        String vendorCd = "";
        
		if (listData != null) {
			if (listData.size() > 0) {
				try {
					SendEmailDomain email = new SendEmailDomain();
					Collections.sort(listData,new SPSTAsnAsnComparator());
					Calendar calendar = Calendar.getInstance(Locale.US);
					String day = String.format("%1$02d",calendar.get(Calendar.DATE));
					String month = String.format("%1$02d",calendar.get(Calendar.MONTH) + 1);
					int year = calendar.get(Calendar.YEAR);
					String hour = String.format("%1$02d",calendar.get(Calendar.HOUR));
					String minute = String.format("%1$02d",calendar.get(Calendar.MINUTE));
					String milli = String.format("%1$02d",calendar.get(Calendar.SECOND));

					if (year > 2500) {
						year = year - 543;
					}
					String sdateTime = day + "/" + month + "/"+ String.valueOf(year) + " " + hour + ":" + minute;

					String sDate =String.valueOf(year) + "/" + month + "/"+ day ;
					String actualEtd = listData.get(0).getActualEtd().substring(0,4)+"/"+listData.get(0).getActualEtd().substring(4,6)+"/"+listData.get(0).getActualEtd().substring(6,8);
					String sTime =hour + ":" + minute;
					String title = "[ NG Result ] ASN document for "+listData.get(0).getdCd()+" / Trip no. "+listData.get(0).getTripNo()+" on "+actualEtd+" can not be updated. ";
					String body ="Dear All,";
					body +="<br>";
				
                     body += "<br>";
                     body += "ASN for "+listData.get(0).getdCd()+" customer / Trip no. "+listData.get(0).getTripNo()+" on "+actualEtd+" can&apos;t be updated, &apos;NG result&apos; of this transaction  is <a style='color:red;font-weight:700'>"+listData.size()+"/"+total+" (number of ASN).</a> Please see detail of &apos;NG result&apos; as following table and take action.";
                     body += "<br>";
                     body +="<br>";
					String emailFrom = ContextParams.getBaseEmail();
					for (int j = 0; j < listData.size(); j++) {
						String actionType ="";
						if(listData.get(j).getAsnStatus().equals("SPS")){
							actionType="ASN Status";
						}
						if(listData.get(j).getAsnStatus().equals("ISS")){
							actionType="Issue";
						}
						if(listData.get(j).getAsnStatus().equals("CCL")){
							actionType="Cancel";
						}
						
						// 6/2/2020 Find customer code and ship no
						cusNo = new BigDecimal(0);
						shipNo = "";
						
						for (int k = 0; k < criteria.size(); k++)
                        {
                            if(listData.get(j).getsCd().trim().equals(criteria.get(k).getsCd().trim()) &&
                                listData.get(j).getsPcd().trim().equals(criteria.get(k).getsPcd().trim()) &&
                                listData.get(j).getdCd().trim().equals(criteria.get(k).getdCd().trim()) &&
                                listData.get(j).getdPcd().trim().equals(criteria.get(k).getdPcd().trim()) &&
                                listData.get(j).getVendorCd().trim().equals(criteria.get(k).getVendorCd().trim())
                                ){
                                cusNo = criteria.get(k).getCusNo();
                                shipNo = criteria.get(k).getShipNo().trim();
                                break;
                            }
                        }
						// end find customer no and ship no
						
						body +=" <table style='border-collapse: collapse;font-size: 10pt;font-family: Arial;' border='1px' cellspacing='0' cellpadding='4'>";
						body +="     <tr>";
						body +="         <td bgcolor='#DCDCDC'>ASN No: </td>";
						body +="         <td>"+listData.get(j).getAsnNo()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Truck no: </td>";
						body +="         <td>"+listData.get(j).getTruckNo()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Cus code: </td>";
						body +="         <td>"+cusNo+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Ship to: </td>";
						body +="         <td>"+shipNo+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Action type: </td>";
						body +="         <td>"+actionType+"</td>";
						body +="         <td bgcolor='#DCDCDC'>ETD date/Time: </td>";
						
						// Set Actual ETA time
						if (null != listData.get(j).getActualEtaTime() && listData.get(j).getActualEtaTime().length() < 6)
						{
						  String temp_d  =listData.get(j).getActualEtaTime();
                          temp_d ="000000"+temp_d;
                          temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                          listData.get(j).setActualEtaTime(temp_d);
						}
						else
						{
						    // 11/2/2020 abnormal case
						      String temp_d = "0";
	                          temp_d ="000000"+temp_d;
	                          temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
	                          listData.get(j).setActualEtaTime(temp_d);
						}
						
						// Set Actual ETD time
						if (listData.get(j).getActualEtdTime().length() < 6)
                        {
                          String temp_d  = listData.get(j).getActualEtdTime();
                          temp_d ="000000"+temp_d;
                          temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                          listData.get(j).setActualEtdTime(temp_d);
                        }
						
						String temp_d =listData.get(j).getActualEta().substring(0,4)+"/"+listData.get(j).getActualEta().substring(4,6)+"/"+listData.get(j).getActualEta().substring(6,8);
						String temp_t =listData.get(j).getActualEtaTime().substring(0,2)+":"+listData.get(j).getActualEtaTime().substring(2,4);
						String temp_ad =listData.get(j).getActualEtd().substring(0,4)+"/"+listData.get(j).getActualEtd().substring(4,6)+"/"+listData.get(j).getActualEtd().substring(6,8);
						String temp_at = listData.get(j).getActualEtdTime().substring(0,2)+":"+listData.get(j).getActualEtdTime().substring(2,4);
						body +="         <td>"+temp_ad+"/"+temp_at+"</td>";
						body +=" 		<td bgcolor='#DCDCDC'>ETA date/Time: </td>";
						
						body +="         <td>"+temp_d+"/"+temp_t+"</td>";
						body +="     </tr>";
						body +=" </table>";
						body +="<br>";
						
						body += " <table style='border-collapse: collapse;font-size:10pt;font-family: Arial;' border='1px' cellspacing='0' cellpadding='4'>";
						 body += " <tr bgcolor='#DCDCDC'>";
						 body += "  <th>No.</th>";
						 body += "  <th>D.CD</th>";
						 body += "  <th>D.PCD</th>";
						 body += "  <th>S.CD</th>";
						 body += "  <th>S.PCD</th>";
						 body += "  <th>Customer Part no.</th>";
						 body += "  <th>Supplier part no.</th>";
						 body += "  <th>Result</th>";
						 body += "  </tr>";
						String trColor=Constants.EMPTY_STRING;
						Collections.sort(listData.get(j).getListAsnDet(),new SPSTAsnDetComparator());
						int errorRow = 0;
						for (int k = 0; k < listData.get(j).getListAsnDet().size(); k++) {

						    
						 // 6/2/2020 Find customer code and ship no
	                        cusNo = new BigDecimal(0);
	                        shipNo = "";
	                        dCd = "";
	                        dPcd = "";
	                        sCd = "";
	                        sPcd = "";
	                        /*
	                        for (int l = 0; l < criteria.size(); l++)
	                        {
	                            if(listData.get(j).getsCd().trim().equals(criteria.get(l).getsCd().trim()) &&
	                                listData.get(j).getsPcd().trim().equals(criteria.get(l).getsPcd().trim()) &&
	                                listData.get(j).getdCd().trim().equals(criteria.get(l).getdCd().trim()) &&
	                                listData.get(j).getdPcd().trim().equals(criteria.get(l).getdPcd().trim()) &&
	                                listData.get(j).getVendorCd().trim().equals(criteria.get(l).getVendorCd().trim())
	                                ){
	                                vendorCd = criteria.get(l).getVendorCd().trim();
	                                dCd = criteria.get(l).getdCd().trim();
	                                dPcd = criteria.get(l).getdPcd().trim();
	                                sCd = criteria.get(l).getsCd().trim();
	                                sPcd = criteria.get(l).getsPcd().trim();
	                                    
	                                break;
	                            }
	                        }
	                        */
	                        vendorCd = listData.get(j).getVendorCd().trim();
                            dCd = listData.get(j).getdCd().trim();
                            dPcd = listData.get(j).getdPcd().trim();
                            sCd = listData.get(j).getsCd().trim();
                            sPcd = listData.get(j).getsPcd().trim();
	                        // end find customer no and ship no
	                     	                     
						 int check = 0;
						 String error_msg="";
						 List<String> list_check_error=  new ArrayList<String>();
						 if(errorList!= null){
							 for(int l=0;l<errorList.size();l++){
								 if(errorList.get(l).getAsnNo().equals(listData.get(j).getAsnNo())&&errorList.get(l).getAsnStatus().equals(listData.get(j).getAsnStatus())){
								     
								     if(errorList.get(l).getErrorCodeList() != null){
										 if(errorList.get(l).getErrorCodeList().size() >0){
										     for(int ll=0;ll<errorList.get(l).getErrorCodeList().size();ll++){
												 int check_error =0;
												 if(list_check_error.size() >0){
												     for(int i_error=0;i_error <list_check_error.size();i_error++){
														 if(list_check_error.get(i_error).equals(errorList.get(l).getErrorCodeList().get(ll))){
															 check_error++;
															 break;
														 }
													 }
												
												 }
												 if(check_error ==0){
													 error_msg += " NG("+errorList.get(l).getErrorCodeList().get(ll)+")";
													 list_check_error.add(errorList.get(l).getErrorCodeList().get(ll));
												 }
											 }
										 }
									 }
								     
									 if(errorList.get(l).getErrorListDet() != null){
										 if(errorList.get(l).getErrorListDet().size() >0){
											 for(int ll=0;ll<errorList.get(l).getErrorListDet().size();ll++){
											     // chk error by cPn and sPN
											     
											     if(errorList.get(l).getErrorListDet().get(ll).getErrorCodeList() != null){
                                                     if(errorList.get(l).getErrorListDet().get(ll).getErrorCodeList().size() >0){
                                                         if(listData.get(j).getListAsnDet().get(k).getSpsDoNo().equalsIgnoreCase(errorList.get(l).getErrorListDet().get(ll).getSpsDoNo())){
                                                             if(listData.get(j).getListAsnDet().get(k).getdPn().equalsIgnoreCase(errorList.get(l).getErrorListDet().get(ll).getdPN())){
                                                                 for(int lll=0;lll<errorList.get(l).getErrorListDet().get(ll).getErrorCodeList().size();lll++){
                                                                     String msg =errorList.get(l).getErrorListDet().get(ll).getErrorCodeList().get(lll);
                                                                         
                                                                     int check_error =0;
                                                                     if(list_check_error.size() >0){
                                                                         for(int i_error=0;i_error <list_check_error.size();i_error++){
                                                                             if(list_check_error.get(i_error).equals(msg)){
                                                                                 check_error++;
                                                                                 break;
                                                                             }
                                                                         }
                                                                    
                                                                     }
                                                                     
                                                                     if(check_error ==0){
                                                                         if(msg.indexOf("Validate") >=0){
                                                                             error_msg += " NG(G)";
                                                                             list_check_error.add("G");
                                                                         }else{
                                                                             error_msg += " NG("+errorList.get(l).getErrorListDet().get(ll).getErrorCodeList().get(lll)+")";
                                                                             list_check_error.add(errorList.get(l).getErrorListDet().get(ll).getErrorCodeList().get(lll));
                                                                         }
                                                                     }
                                                                     
                                                                    
                                                                 }
                                                             }
                                                         }
                                                         
                                                     }
                                                 }
											 }
										 }
									 }
									// error_tran +="NG("+errorList.get(l).get+")";
									 if(!error_msg.equalsIgnoreCase("")){
                                         
                                         body += " <tr "+trColor+" >";
                                         body += " <td>"+(errorRow+1)+"</td>";
                                         body += " <td>"+dCd+"</td>";
                                         body += " <td >"+dPcd+"</td>";
                                         body += " <td >"+vendorCd+"</td>"; // 5/2/2020 
                                         body += " <td >"+sPcd+"</td>";
                                         body += " <td>"+listData.get(j).getListAsnDet().get(k).getdPn()+"</td>";
                                         body += " <td >"+listData.get(j).getListAsnDet().get(k).getsPn()+"</td>";
                                         body += " <td >"+error_msg+"</td>";
                                         body += " </tr>";
                                         
                                         if(!StringUtil.checkNullOrEmpty(trColor)){
                                             trColor=Constants.EMPTY_STRING;
                                         }else{
                                             trColor="bgcolor='#FFFFE0'";
                                         }
                                         errorRow = errorRow+1;
                                     }
								 }

							 }
						 }
						  
						}
						 body += "  </table>";
						 body +="<br>";
							
	                     body += "<br>";
					}
				
					body +="<br>";
					body +="<br>";
					body +=" <table>";
					body +=" <tr>";
					body +=" <td>**NG code meaning</td>";
					body +=" <td style='padding-left:20px'>M = Master error</td>";
					body +=" <td style='padding-left:20px'>F = Format error</td>";
					body +=" <td style='padding-left:20px'>T = Transaction error</td>";
					body +=" <td style='padding-left:20px'>D = Duplicate data</td>";
					body +=" </tr>";
					body +=" </table>";
					
					email.setHeader(title);
					email.setEmailSmtp(ContextParams.getEmailSmtp());
					email.setContents(body);
					email.setEmailFrom(emailFrom);
					email.setEmailTo(emailTo);
					mailService.sendEmail(email);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	private void SendEmailCancel(List<SPSTAsnHeaderDNDomain> listData, String emailTo,
	            String firstName, String middleName,String lastName) {

		Locale locale = this.getDefaultLocale();
		if (listData != null) {
			if (listData.size() > 0) {
				try {
					SendEmailDomain email = new SendEmailDomain();
					Collections.sort(listData,new SPSTAsnAsnComparator());
					Calendar calendar = Calendar.getInstance(Locale.US);
					String day = String.format("%1$02d",
							calendar.get(Calendar.DATE));
					String month = String.format("%1$02d",
							calendar.get(Calendar.MONTH) + 1);
					int year = calendar.get(Calendar.YEAR);
					String hour = String.format("%1$02d",
							calendar.get(Calendar.HOUR));
					String minute = String.format("%1$02d",
							calendar.get(Calendar.MINUTE));
					String milli = String.format("%1$02d",
							calendar.get(Calendar.SECOND));

					if (year > 2500) {
						year = year - 543;
					}
					String sdateTime = day + "/" + month + "/"
							+ String.valueOf(year) + " " + hour + ":" + minute;

					String sDate =String.valueOf(year) + "/" + month + "/"+ day ;
					String sTime =hour + ":" + minute;
					
					String title = "Cancel ASN on"+sdateTime;
					String body = MessageUtil.getEmailLabelHandledException(locale, 
				            SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE1, null);
					body +="<br>";
				
                     body += "<br>";
                     body += MessageUtil.getEmailLabelHandledException(locale, 
                             SupplierPortalConstant.EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CANCEL, null);
                     body += "<br>";
                     body +="<br>";
					String emailFrom = ContextParams.getBaseEmail();
					for (int j = 0; j < listData.size(); j++) {
						String actionType ="";
						if(listData.get(j).getAsnStatus().equals("SPS")){
							actionType="ASN Status";
						}
						if(listData.get(j).getAsnStatus().equals("ISS")){
							actionType="Issue";
						}
						if(listData.get(j).getAsnStatus().equals("CCL")){
							actionType="Cancel";
						}
						
						// Set Actual ETA time
                        if (null != listData.get(j).getActualEtaTime() && listData.get(j).getActualEtaTime().length() < 6)
                        {
                          String temp_d  =listData.get(j).getActualEtaTime();
                          temp_d ="000000"+temp_d;
                          temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                          listData.get(j).setActualEtaTime(temp_d);
                        }
                        else
                        {
                            // 11/2/2020 abnormal case
                              String temp_d = "0";
                              temp_d ="000000"+temp_d;
                              temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                              listData.get(j).setActualEtaTime(temp_d);
                        }
                        
                        // Set Actual ETD time
                        if (listData.get(j).getActualEtdTime().length() < 6)
                        {
                          String temp_d  = listData.get(j).getActualEtdTime();
                          temp_d ="000000"+temp_d;
                          temp_d =temp_d.substring(temp_d.length()-6, temp_d.length());
                          listData.get(j).setActualEtdTime(temp_d);
                        }
						String temp_md = listData.get(j).getLastUpdateDate().substring(0,4)+"/"+listData.get(j).getLastUpdateDate().substring(4,6)+"/"+listData.get(j).getLastUpdateDate().substring(6,8);
						String temp_mt = listData.get(j).getLastUpdateTime().substring(0,2)+":"+listData.get(j).getLastUpdateTime().substring(2,4);
						String temp_d =listData.get(j).getActualEta().substring(0,4)+"/"+listData.get(j).getActualEta().substring(4,6)+"/"+listData.get(j).getActualEta().substring(6,8);
	                    String temp_t =listData.get(j).getActualEtaTime().substring(0,2)+":"+listData.get(j).getActualEtaTime().substring(2,4);
	                    String temp_ad =listData.get(j).getActualEtd().substring(0,4)+"/"+listData.get(j).getActualEtd().substring(4,6)+"/"+listData.get(j).getActualEtd().substring(6,8);
	                    String temp_at = listData.get(j).getActualEtdTime().substring(0,2)+":"+listData.get(j).getActualEtdTime().substring(2,4);
	                    
						body +=" <table style='border-collapse: collapse;font-size: 10pt;font-family: Arial;' border='1px' cellspacing='0' cellpadding='4'>";
						body +="     <tr>";
						body +="         <td bgcolor='#DCDCDC'>ASN No: </td>";
						body +="         <td>"+listData.get(j).getAsnNo()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>S.CD: </td>";
						body +="         <td>"+listData.get(j).getsCd()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>S.P.CD: </td>";
						body +="         <td>"+listData.get(j).getsPcd()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>D.CD: </td>";
						body +="         <td>"+listData.get(j).getdCd()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>D.P.CD: </td>";
						body +="         <td>"+listData.get(j).getdPcd()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Last Modified: </td>";
						//body +="         <td>"+listData.get(j).getLastUpdateDatetime()+"/</td>";
						body +="         <td>"+temp_md+" "+temp_mt+"/</td>";
						body +="     </tr>";
						body +="     <tr>";
						body +="         <td bgcolor='#DCDCDC'>Actual ETD: </td>";
						//body +="         <td>"+listData.get(j).getActualEtd()+"</td>";
						body +="         <td>"+temp_d+" "+temp_t+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Plan ETA: </td>";
						//body +="         <td>"+listData.get(j).getPlanEta()+"</td>";
						body +="         <td>"+temp_ad+" "+temp_at+"</td>";
						body +="         <td bgcolor='#DCDCDC'>No. of Pallet: </td>";
						body +="         <td>"+listData.get(j).getNumberOfPallet()+"</td>";
						body +="         <td bgcolor='#DCDCDC'>Trip No: </td>";
						body +="         <td>"+listData.get(j).getTripNo()+"</td>";
						body +="         <td></td>";
						body +="         <td></td>";
						body +="         <td></td>";
						body +="         <td></td>";
						body +="     </tr>";
						body +=" </table>";
						body +="<br>";
						
						body += " <table style='border-collapse: collapse;font-size:10pt;font-family: Arial;' border='1px' cellspacing='0' cellpadding='4'>";
						 body += " <tr bgcolor='#DCDCDC'>";
						 body += "  <th>User Name                      </th>";
						 body += "  <th>  No. </th>";
						 body += "  <th> Rcv Lane </th>";
						 body += "  <th>    D.Part No    </th>";
						 body += "  <th>  SPS D/O No.  </th>";
						 body += "  <th>  Rev </th>";
						 body += "  <th>  Shipping QTY  </th>";
						 body += "  <th>  U/M  </th>";
						 body += "  <th>    QTY/Box    </th>";
						 body += "  <th>    Shipping   </th>";
						 body += "  <th>  Change Reason</th>";
						 body += "  </tr>";
						String trColor=Constants.EMPTY_STRING;
						String name =firstName+" "+StringUtil.nullToEmpty(middleName)+" "+lastName;
				
						
						if(listData.get(j).getListAsnDet() != null){
							if(listData.get(j).getListAsnDet().size() >0){
								Collections.sort(listData.get(j).getListAsnDet(),new SPSTAsnDetComparator());
								for (int k = 0; k < listData.get(j).getListAsnDet().size(); k++) {
									SPSTDoDetailDNDomain criteriaDoDet = new SPSTDoDetailDNDomain();
									criteriaDoDet.setDoId(listData.get(j).getListAsnDet().get(k).getDoId());
									criteriaDoDet.setdPn(listData.get(j).getListAsnDet().get(k).getdPn());
									criteriaDoDet.setsPn(listData.get(j).getListAsnDet().get(k).getsPn());
									List<SPSTDoDetailDNDomain> listDODet =spsTAsnFromCIGMAService.searchDODet(criteriaDoDet);
									 BigDecimal doOrderQty = new BigDecimal(0);
									 if(listDODet != null){
										 if(listDODet.size() >0){
											 doOrderQty =listDODet.get(0).getCurrentOrderQty();
										 }
									 }
									 body += " <tr "+trColor+" >";
									 body += " <td>"+name+"</td>";
									 body += " <td>"+(k+1)+"</td>";
									 body += " <td >"+listData.get(j).getListAsnDet().get(k).getRcvLane()+"</td>";
									 body += " <td >"+listData.get(j).getListAsnDet().get(k).getdPn()+"</td>";
									 body += " <td >"+listData.get(j).getListAsnDet().get(k).getSpsDoNo()+"</td>";
									 body += " <td>"+listData.get(j).getListAsnDet().get(k).getReVision()+"</td>";
									  body += " <td >"+listData.get(j).getListAsnDet().get(k).getShippingQty()+"</td>";
									  body += " <td >"+listData.get(j).getListAsnDet().get(k).getUnitOfMeasure()+"</td>";
									  body += " <td >"+listData.get(j).getListAsnDet().get(k).getQtyBox()+"</td>";
									  body += " <td >"+listData.get(j).getListAsnDet().get(k).getShippingBoxQty()+"</td>";
									  body += " <td >"+StringUtil.nullToEmpty(listData.get(j).getListAsnDet().get(k).getChangeReasonCd())+"</td>";
									 body += " </tr>";
								
									 if(!StringUtil.checkNullOrEmpty(trColor)){
											trColor=Constants.EMPTY_STRING;
										}else{
											trColor="bgcolor='#FFFFE0'";
										}
									}
							}
						}
						
						 body += "  </table>";
					}
				
					body +="<br>";
					body +="<br>";
					body +=" <table>";
					body +=" <tr>";
					body +=" <td>**NG code meaning</td>";
					body +=" <td style='padding-left:20px'>M = Master error</td>";
					body +=" <td style='padding-left:20px'>F = Format error</td>";
					body +=" <td style='padding-left:20px'>T = Transaction error</td>";
					body +=" <td style='padding-left:20px'>D = Duplicate data</td>";
					body +=" </tr>";
					body +=" </table>";
					email.setHeader(title);
					email.setEmailSmtp(ContextParams.getEmailSmtp());
					email.setContents(body);
					email.setEmailFrom(emailFrom);
					email.setEmailTo(emailTo);
					mailService.sendEmail(email);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	private String getMessage(Locale locale, String msgId, String... params)
			throws ApplicationException {
		return MessageUtil.getErrorMessage(locale, msgId, null,
				getArgumentMessage(params));
	}

	/**
	 * 
	 * <p>
	 * Throws ErrorMessage.
	 * </p>
	 * 
	 * @param locale
	 *            Locale
	 * @param msgId
	 *            Message ID
	 * @param params
	 *            argument
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	private void throwsErrorMessage(Locale locale, String msgId,
			String... params) throws ApplicationException {
		MessageUtil.throwsApplicationMessage(locale, msgId, null,
				getArgumentMessage(params));
	}

	/**
	 * Gets the content.
	 * 
	 * @return default locale
	 */
	private Locale getDefaultLocale() {
		Properties propApp = Props.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
		String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
		String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
		String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
		ContextParams.setDefaultLocale(strDefaultLocale);
		ContextParams.setBaseDirGui(strBaseDirGui);
		ContextParams.setBaseDirMsg(strBaseDirMsg);
		return LocaleUtil.getLocaleFromString(strDefaultLocale);
	}
	
	private SPSTAsnHeaderDNErrorListDomain validateDNDNMasterSch(SPSTAsnHeaderDNDomain data,SpsMDensoDensoRelationWithASNDNDomain criteria)
        throws ApplicationException, IOException {

        SPSTAsnHeaderDNErrorListDomain errorCode = null;
        List<String> errorList = new ArrayList<String>();
        int checkError =0;
        
        try{
            SpsMDensoDensoRelationWithASNDNDomain dataCom = new SpsMDensoDensoRelationWithASNDNDomain();
            dataCom.setSchemaCd(criteria.getSchemaCd());
            dataCom.setdCd(data.getdCd());
            List<SpsMDensoDensoRelationWithASNDNDomain> selectListCom = spsTAsnFromCIGMAService.searchSupplierBySchema(dataCom);
            if(selectListCom==null||selectListCom.size()==0){
                errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
            }else{
                dataCom.setdPcd(data.getdPcd());
                selectListCom = spsTAsnFromCIGMAService.searchSupplierBySchema(dataCom);
                if(selectListCom==null||selectListCom.size()==0){
                    errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                }else{
                    dataCom.setsCd(data.getsCd());
                    selectListCom = spsTAsnFromCIGMAService.searchSupplierBySchema(dataCom);
                    if(selectListCom==null||selectListCom.size()==0){
                        errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M03);
                    }else{
                        dataCom.setsPcd(data.getsPcd());
                        selectListCom = spsTAsnFromCIGMAService.searchSupplierBySchema(dataCom);
                        if(selectListCom==null||selectListCom.size()==0){
                            errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M04);
                        }
                    }
                }
            }
            
            if(errorList.size()>0){
                errorCode = new SPSTAsnHeaderDNErrorListDomain();
                
                errorCode.setAsnNo(data.getAsnNo());
                errorCode.setAsnStatus(data.getAsnStatus());
                errorCode.setErrorCodeList(errorList);
                
                if(data.getListAsnDet()!=null){
                    if(data.getListAsnDet().size()>0){
                        List<SPSTAsnDNErrorListDomain> detailList = new ArrayList<SPSTAsnDNErrorListDomain>();
                        SPSTAsnDNErrorListDomain detail = null;
                        for(int di=0;di<data.getListAsnDet().size();di++){
                            detail = new SPSTAsnDNErrorListDomain();
                            detail.setdCd(data.getListAsnDet().get(di).getdCd());
                            detail.setdPN(data.getListAsnDet().get(di).getdPn());
                            detail.setsPN(data.getListAsnDet().get(di).getsPn());
                            detail.setAsnNo(data.getListAsnDet().get(di).getAsnNo());
                            detailList.add(detail);
                        }
                        errorCode.setErrorListDet(detailList);
                    }
                }
            }
        }catch(Exception e){
            LOG.error(e.getMessage());
        }
        return errorCode;
    } 
	
	private SPSTAsnHeaderDNDomain setDoData(SPSTAsnHeaderDNDomain data)
        throws ApplicationException, IOException {

	    SPSTAsnHeaderDNDomain tempData = null;
	    
        try{
            
            for(int i=0;i<data.getListAsnDet().size();i++){
                
                SPSTDoDetailDNDomain criteriaDoDet = new SPSTDoDetailDNDomain();
                criteriaDoDet.setDoId(data.getListAsnDet().get(i).getDoId());
                criteriaDoDet.setdPn(data.getListAsnDet().get(i).getdPn());
                criteriaDoDet.setsPn(data.getListAsnDet().get(i).getsPn());
                List<SPSTDoDetailDNDomain> listDODet =spsTAsnFromCIGMAService.searchDODet(criteriaDoDet);
                 
                if(listDODet!=null){
                    if(listDODet.size()>0){
                        data.getListAsnDet().get(i).setDueDate(listDODet.get(0).getDeliveryDate());
                    }
                }
           }
            
        }catch(Exception e){
            LOG.error(e.getMessage());
        }
        
        return tempData;
    } 
	
	public List<SpsMDensoDensoRelationWithASNDNDomain> searchDataGroup(
        SpsMDensoDensoRelationWithASNDNDomain criteria) throws Exception {
	    List<SpsMDensoDensoRelationWithASNDNDomain> selectList = spsTAsnFromCIGMAService.searchByConditionGroup(criteria);
    return selectList;
	}
}
class SPSTAsnTripAsnComparator implements Comparator<SPSTAsnHeaderDNDomain> {
	  public int compare(SPSTAsnHeaderDNDomain record1, SPSTAsnHeaderDNDomain record2) {
		  int c;
		    c = record1.getTripNo().compareTo(record2.getTripNo());
		    if (c == 0)
		       c = record1.getAsnNo().compareTo(record2.getAsnNo());
		    return c;
	  }
	}

class SPSTAsnAsnComparator implements Comparator<SPSTAsnHeaderDNDomain> {
	  public int compare(SPSTAsnHeaderDNDomain record1, SPSTAsnHeaderDNDomain record2) {
		  int c;
		       c = record1.getAsnNo().compareTo(record2.getAsnNo());
		    return c;
	  }
	}
class SPSTAsnDetComparator implements Comparator<SPSTAsnDetailDNDomain> {
	  public int compare(SPSTAsnDetailDNDomain record1, SPSTAsnDetailDNDomain record2) {
		  int c;
		    c = record1.getAsnNo().compareTo(record2.getAsnNo());
		    if (c == 0)
		       c = record1.getsPn().compareTo(record2.getsPn());
		    return c;
	  }
	}

