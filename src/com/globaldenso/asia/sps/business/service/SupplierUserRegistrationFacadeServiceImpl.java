/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserRegistrationDomain;
import com.globaldenso.asia.sps.business.domain.UserDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class SupplierUserRegistrationFacadeServiceImpl.</p>
 * <p>Manage data of Supplier User.</p>
 * <ul>
 * <li>Method search  : initialRegister</li>
 * <li>Method search  : initialEdit</li>
 * <li>Method insert  : transacCreateSupplierUser</li>
 * <li>Method update  : transacUpdateSupplierUser</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * </ul>
 *
 * @author CSI
 */
public class SupplierUserRegistrationFacadeServiceImpl implements 
    SupplierUserRegistrationFacadeService {
    
    /** The user service. */
    private UserService userService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The Supplier Company Service. */
    private CompanySupplierService companySupplierService;
    
    /** The Denso Company Service. */
    private CompanyDensoService companyDensoService;
    
    /** The Supplier Plant Service. */
    private PlantSupplierService plantSupplierService;
    
    /** The SPS M User Service. */
    private SpsMUserService spsMUserService;
    
    /** The SPS M User Supplier Service. */
    private SpsMUserSupplierService spsMUserSupplierService;
    
    /** The SPS M Company Supplier Service. */
    private SpsMCompanySupplierService spsMCompanySupplierService;
    
    /** The SPS M Plant Supplier Service. */
    private SpsMPlantSupplierService spsMPlantSupplierService;
    
    /** The SPS M Company DENSO Service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The As400 Vendor Service. */
    private SpsMAs400VendorService spsMAs400VendorService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new Supplier User Registration Facade Service impl.
     */
    public SupplierUserRegistrationFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the user service.
     * 
     * @param userService the new user service
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    /**
     * Sets the supplier company Service.
     * 
     * @param companySupplierService the new supplier company Service.
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    /**
     * Sets the supplier plant service.
     * 
     * @param plantSupplierService the new supplier plant service.
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }
    /**
     * Sets the company Denso Service.
     * 
     * @param companyDensoService the new company Denso Service.
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    /**
     * Sets the SPS M User Service.
     * 
     * @param spsMUserService the new SPS M User Service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    /**
     * Sets the SPS M User Supplier Service.
     * 
     * @param spsMUserSupplierService the new SPS M User Supplier Service.
     */
    public void setSpsMUserSupplierService(
        SpsMUserSupplierService spsMUserSupplierService) {
        this.spsMUserSupplierService = spsMUserSupplierService;
    }
    /**
     * Sets the SPS M Company Supplier Service.
     * 
     * @param spsMCompanySupplierService the new SPS M Company Supplier Service.
     */
    public void setSpsMCompanySupplierService(
        SpsMCompanySupplierService spsMCompanySupplierService) {
        this.spsMCompanySupplierService = spsMCompanySupplierService;
    }
    /**
     * Sets the SPS M Plant Supplier Service.
     * 
     * @param spsMPlantSupplierService the new SPS M Plant Supplier Service.
     */
    public void setSpsMPlantSupplierService(
        SpsMPlantSupplierService spsMPlantSupplierService) {
        this.spsMPlantSupplierService = spsMPlantSupplierService;
    }
    /**
     * Sets the SPS M Company DENSO Service.
     * 
     * @param spsMCompanyDensoService the new SPS M Company DENSO Service.
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    /**
     * Sets the As400 Vendor Service.
     * 
     * @param spsMAs400VendorService the new As400 Vendor Service.
     */
    public void setSpsMAs400VendorService(SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#initialRegister
     * (com.globaldenso.asia.sps.business.domain.dataScopeControlDomain)
     */
    public SupplierUserRegistrationDomain searchInitialRegister(DataScopeControlDomain
        dataScopeControlDomain) throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        List<CompanySupplierDomain> companySupplierDomainList =
            new ArrayList<CompanySupplierDomain>();
        List<CompanyDensoDomain> companyDensoDomainList =
            new ArrayList<CompanyDensoDomain>();
        CompanyDensoWithScopeDomain densoCompanyWithScopeDomain =
            new CompanyDensoWithScopeDomain();
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain =
            new CompanySupplierWithScopeDomain();
        SupplierUserRegistrationDomain supplierUserRegistrationDomain =
            new SupplierUserRegistrationDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList =
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        /*Get DENSO company information*/
        CompanyDensoDomain companyDensoDomain = densoCompanyWithScopeDomain.getCompanyDensoDomain();
        companyDensoDomain.setIsActive(Constants.IS_ACTIVE);
        densoCompanyWithScopeDomain.setCompanyDensoDomain(companyDensoDomain);
        densoCompanyWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoDomainList = companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(companyDensoDomainList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        /*Get Supplier company information*/
        companySupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        companySupplierWithScopeDomain.getCompanySupplierDomain().setIsActive(Constants.IS_ACTIVE);
        companySupplierDomainList = companySupplierService.searchCompanySupplierNameByRelation(
            companySupplierWithScopeDomain);
        
        if(companySupplierDomainList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME);
        }
        
        /*Get Supplier plant information*/
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantSupplierDomain> plantSupplierList
            = this.plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        if(null == plantSupplierList || plantSupplierList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        /*Get list of department name */
        List<SpsMUserDomain> departmentList = userService.searchDepartmentName();
        if(Constants.ZERO < departmentList.size()){
            supplierUserRegistrationDomain.setDepartmentList(departmentList);
        }
        
        supplierUserRegistrationDomain.setCompanySupplierList(companySupplierDomainList);
        supplierUserRegistrationDomain.setPlantSupplierList(plantSupplierList);
        supplierUserRegistrationDomain.setCompanyDensoList(companyDensoDomainList);
        return supplierUserRegistrationDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#initialEdit
     * (com.globaldenso.asia.sps.business.domain.supplierUserRegistrationDomain)
     */
    public SupplierUserRegistrationDomain searchInitialEdit(SupplierUserRegistrationDomain 
       supplierUserRegistrationDomain) throws ApplicationException
    {
        Locale locale = supplierUserRegistrationDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain = supplierUserRegistrationDomain
            .getDataScopeControlDomain();
        List<CompanySupplierDomain> companySupplierDomainList  = 
            new ArrayList<CompanySupplierDomain>();
        List<CompanyDensoDomain> companyDensoDomainList = 
            new ArrayList<CompanyDensoDomain>();
        CompanyDensoWithScopeDomain densoCompanyWithScopeDomain = new CompanyDensoWithScopeDomain();
        CompanySupplierWithScopeDomain companySupplierWithScope = 
            new CompanySupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierCriteria = new PlantSupplierDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        List<PlantSupplierDomain> plantSupplierList = 
            new ArrayList<PlantSupplierDomain>();
        List<SpsMAs400VendorDomain> vendorCdList = new ArrayList<SpsMAs400VendorDomain>();
        CompanyDensoDomain companyDensoDomain = new CompanyDensoDomain();
        
        /*Get supplier user information*/
        UserDomain userDomain = supplierUserRegistrationDomain.getUserSupplierDetailDomain()
            .getUserDomain();
        userDomain.setDscId(userDomain.getDscId());
        userDomain.setLocale(supplierUserRegistrationDomain.getLocale());
        UserSupplierDetailDomain supplierUserDetailDomain
            = this.searchSupplierUserInformation(userDomain);
        supplierUserRegistrationDomain.setUserSupplierDetailDomain(supplierUserDetailDomain);
        
        /*Convert last update date time*/
        int pattern = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Date lastUpdateDatetimeUser = new Date(supplierUserDetailDomain.getSpsMUserDomain()
            .getLastUpdateDatetime().getTime());
        Date lastUpdateDatetimeSupplier = new Date(supplierUserDetailDomain
            .getSpsMUserSupplierDomain().getLastUpdateDatetime().getTime());
        
        supplierUserRegistrationDomain.setUpdateDatetime(
            DateUtil.format(lastUpdateDatetimeUser, pattern));
        supplierUserRegistrationDomain.setUpdateDatetimeSupplier(
            DateUtil.format(lastUpdateDatetimeSupplier, pattern));
        
        /*Check DENSO owner with the current user roles on current screen*/
        boolean isDCompany = false;
        for(UserRoleDomain userRoleDomain : dataScopeControlDomain.getUserRoleDomainList()){
            if(!StringUtil.checkNullOrEmpty(supplierUserDetailDomain
                .getSpsMUserSupplierDomain().getDOwner())){
                if(supplierUserDetailDomain.getSpsMUserSupplierDomain().getDOwner().equals(
                    userRoleDomain.getDCd())){
                    isDCompany = true;
                    break;
                }
            }
        }
        supplierUserRegistrationDomain.setIsDCompany(isDCompany);
        
        if(isDCompany){
            /*Get relation between DENSO and Supplier*/
            List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
                densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
            if(densoSupplierRelationDomainList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
            }
            
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
            
            /*Get Denso company information*/
            companyDensoDomain.setIsActive(Constants.IS_ACTIVE);
            densoCompanyWithScopeDomain.setCompanyDensoDomain(companyDensoDomain);
            densoCompanyWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            
            PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
            plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            companyDensoDomainList = companyDensoService.searchCompanyDenso(plantDensoWithScope);
            if(companyDensoDomainList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
            }else{
                supplierUserRegistrationDomain.setCompanyDensoList(companyDensoDomainList);
            }
            
            /*Get Supplier company information*/
            companyDensoDomain = densoCompanyWithScopeDomain.getCompanyDensoDomain();
            companyDensoDomain.setIsActive(Constants.IS_ACTIVE);
            densoCompanyWithScopeDomain.setCompanyDensoDomain(companyDensoDomain);
            densoCompanyWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            companySupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            companySupplierWithScope.getCompanySupplierDomain().setIsActive(Constants.IS_ACTIVE);
            
            companySupplierDomainList = companySupplierService.searchCompanySupplierNameByRelation(
                companySupplierWithScope);
            
            if(companySupplierDomainList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME);
            }else{
                supplierUserRegistrationDomain.setCompanySupplierList(
                    companySupplierDomainList);
            }
            /* Get list of supplier plant information*/
            plantSupplierCriteria.setIsActive(Constants.IS_ACTIVE);
            plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierCriteria);
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            plantSupplierList =  plantSupplierService.searchPlantSupplier(
                plantSupplierWithScopeDomain);
            if(plantSupplierList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
            }else{
                supplierUserRegistrationDomain.setPlantSupplierList(plantSupplierList);
            }
            
            /* Get list of vendor code information*/
            SpsMAs400VendorCriteriaDomain vendorCriteriaDomain = 
                new SpsMAs400VendorCriteriaDomain();
            vendorCriteriaDomain.setSCd(
                supplierUserDetailDomain.getSpsMUserSupplierDomain().getSCd());
            vendorCdList = spsMAs400VendorService.searchByCondition(vendorCriteriaDomain);
            if(vendorCdList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_AS400_VENDOR);
            }else{
                supplierUserRegistrationDomain.setVendorCdList(vendorCdList);
            }
            
            /*Get list of department name */
            List<SpsMUserDomain> departmentList = userService.searchDepartmentName();
            if(Constants.ZERO < departmentList.size()){
                supplierUserRegistrationDomain.setDepartmentList(departmentList);
            }
        }
        return supplierUserRegistrationDomain;
    }
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#transactCreateSupplierUser
     * (com.globaldenso.asia.sps.business.domain.SupplierUserRegistrationDomain)
     */
    public SupplierUserRegistrationDomain transactCreateSupplierUser(SupplierUserRegistrationDomain 
        supplierUserRegistrationDomain) throws ApplicationException
    {
        Locale locale = supplierUserRegistrationDomain.getLocale();
        /*validate for create supplier user*/
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        errorMessageList = validate(supplierUserRegistrationDomain);
        
        if(Constants.ZERO < errorMessageList.size()){
            supplierUserRegistrationDomain.setErrorMessageList(errorMessageList);
            return supplierUserRegistrationDomain;
        }
        
        /*Check exist DSC ID in master data*/
        if(!StringUtil.checkNullOrEmpty(
            supplierUserRegistrationDomain.getUserDomain().getDscId()))
        {
            SpsMUserCriteriaDomain criteriaDomain = new SpsMUserCriteriaDomain();
            criteriaDomain.setDscId(supplierUserRegistrationDomain.getUserDomain().getDscId());
            SpsMUserDomain userDomain = spsMUserService.searchByKey(criteriaDomain);
            if(null != userDomain){
                //1. User DSC ID was found in master data and is active.
                if(Constants.IS_ACTIVE.equals(userDomain.getIsActive())){
                    MessageUtil.throwsApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0032);
                }else{
                  //2. User DSC ID was found in master data and is not active.
                    /*Update user information to the system from SpsMUserService*/
                    SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                    spsMUserDomain.setDscId(supplierUserRegistrationDomain.getUserDomain()
                        .getDscId());
                    spsMUserDomain.setFirstName(supplierUserRegistrationDomain.getUserDomain()
                        .getFirstName());
                    spsMUserDomain.setMiddleName(supplierUserRegistrationDomain.getUserDomain()
                        .getMiddleName());
                    spsMUserDomain.setLastName(supplierUserRegistrationDomain.getUserDomain()
                        .getLastName());
                    spsMUserDomain.setDepartmentName(supplierUserRegistrationDomain
                        .getUserDomain().getDepartmentName());
                    spsMUserDomain.setEmail(supplierUserRegistrationDomain.getUserDomain()
                        .getEmail());
                    spsMUserDomain.setTelephone(supplierUserRegistrationDomain.getUserDomain()
                        .getTelephone());
                    spsMUserDomain.setUserType(Constants.STR_S);
                    spsMUserDomain.setIsActive(supplierUserRegistrationDomain.getUserDomain()
                        .getIsActive());
                    spsMUserDomain.setCreateDscId(supplierUserRegistrationDomain.getUserDomain()
                        .getCreateUser());
                    spsMUserDomain.setCreateDatetime(commonService.searchSysDate());
                    spsMUserDomain.setLastUpdateDscId(supplierUserRegistrationDomain
                        .getUserDomain().getUpdateUser());
                    spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    criteriaDomain = new SpsMUserCriteriaDomain();
                    criteriaDomain.setDscId(supplierUserRegistrationDomain.getUserDomain()
                        .getDscId());
                    
                    int updateRecord = spsMUserService.updateByCondition(spsMUserDomain,
                        criteriaDomain);
                    if(updateRecord <= Constants.ZERO){
                        MessageUtil.throwsApplicationMessageWithLabel(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,  
                            SupplierPortalConstant.LBL_UPDATE_USER_INFO);
                    }
                    
                    /*Update new supplier user information to the system*/
                    SpsMUserSupplierDomain spsMUserSupplierDomain = new 
                        SpsMUserSupplierDomain();
                    spsMUserSupplierDomain.setDscId(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDscId());
                    spsMUserSupplierDomain.setSCd(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
                    spsMUserSupplierDomain.setSPcd(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSPcd());
                    spsMUserSupplierDomain.setDOwner(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDOwner());
                    spsMUserSupplierDomain.setEmlSInfoNotfoundFlag(
                        supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                            .getSpsMUserSupplierDomain().getEmlSInfoNotfoundFlag());
                    spsMUserSupplierDomain.setEmlUrgentOrderFlag(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                        .getEmlUrgentOrderFlag());
                    spsMUserSupplierDomain.setEmlAllowReviseFlag(
                        supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                            .getSpsMUserSupplierDomain().getEmlAllowReviseFlag());
                    spsMUserSupplierDomain.setEmlCancelInvoiceFlag(
                        supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                            .getSpsMUserSupplierDomain().getEmlCancelInvoiceFlag());
                    spsMUserSupplierDomain.setCreateDscId(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                        .getCreateDscId());
                    spsMUserSupplierDomain.setCreateDatetime(commonService.searchSysDate());
                    spsMUserSupplierDomain.setLastUpdateDscId(supplierUserRegistrationDomain
                        .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                        .getLastUpdateDscId());
                    spsMUserSupplierDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    
                    SpsMUserSupplierCriteriaDomain supplierCriteriaDomain = 
                        new SpsMUserSupplierCriteriaDomain();
                    supplierCriteriaDomain.setDscId(supplierUserRegistrationDomain
                        .getUserDomain().getDscId());
                    int updateSupplier = spsMUserSupplierService.updateByCondition(
                        spsMUserSupplierDomain, supplierCriteriaDomain);
                    if(updateSupplier <= Constants.ZERO){
                        MessageUtil.throwsApplicationMessageWithLabel(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0034,  
                            SupplierPortalConstant.LBL_UPDATE_SUPPLIER_USER_INFO);
                    }
                }
            }else{
                //3. User is not found in master data.
                /*Create new user information to the system */
                SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                spsMUserDomain.setDscId(supplierUserRegistrationDomain.getUserDomain()
                    .getDscId());
                spsMUserDomain.setFirstName(supplierUserRegistrationDomain.getUserDomain()
                    .getFirstName());
                spsMUserDomain.setMiddleName(supplierUserRegistrationDomain.getUserDomain()
                    .getMiddleName());
                spsMUserDomain.setLastName(supplierUserRegistrationDomain.getUserDomain()
                    .getLastName());
                spsMUserDomain.setDepartmentName(supplierUserRegistrationDomain.getUserDomain()
                    .getDepartmentName());
                spsMUserDomain.setEmail(supplierUserRegistrationDomain.getUserDomain()
                    .getEmail());
                spsMUserDomain.setTelephone(supplierUserRegistrationDomain.getUserDomain()
                    .getTelephone());
                spsMUserDomain.setUserType(Constants.STR_S);
                spsMUserDomain.setIsActive(supplierUserRegistrationDomain.getUserDomain()
                    .getIsActive());
                spsMUserDomain.setCreateDscId(supplierUserRegistrationDomain.getUserDomain()
                    .getCreateUser());
                spsMUserDomain.setCreateDatetime(commonService.searchSysDate());
                spsMUserDomain.setLastUpdateDscId(supplierUserRegistrationDomain.getUserDomain()
                    .getUpdateUser());
                spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
                
                spsMUserService.create(spsMUserDomain);
                
                /*Create new supplier user information to the system*/
                SpsMUserSupplierDomain spsMUserSupplierDomain = new SpsMUserSupplierDomain();
                spsMUserSupplierDomain.setDscId(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDscId());
                spsMUserSupplierDomain.setSCd(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
                spsMUserSupplierDomain.setSPcd(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSPcd());
                spsMUserSupplierDomain.setDOwner(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDOwner());
                spsMUserSupplierDomain.setEmlSInfoNotfoundFlag(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                    .getEmlSInfoNotfoundFlag());
                spsMUserSupplierDomain.setEmlUrgentOrderFlag(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                    .getEmlUrgentOrderFlag());
                spsMUserSupplierDomain.setEmlAllowReviseFlag(
                    supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                        .getSpsMUserSupplierDomain().getEmlAllowReviseFlag());
                spsMUserSupplierDomain.setEmlCancelInvoiceFlag(
                    supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                        .getSpsMUserSupplierDomain().getEmlCancelInvoiceFlag());
                spsMUserSupplierDomain.setCreateDscId(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                    .getCreateDscId());
                spsMUserSupplierDomain.setCreateDatetime(commonService.searchSysDate());
                spsMUserSupplierDomain.setLastUpdateDscId(supplierUserRegistrationDomain
                    .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
                    .getLastUpdateDscId());
                spsMUserSupplierDomain.setLastUpdateDatetime(commonService.searchSysDate());
                
                spsMUserSupplierService.create(spsMUserSupplierDomain);
            }
            
        }
        
        /*Get Company Name*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = new 
            ArrayList<DensoSupplierRelationDomain>();
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain = 
            new CompanySupplierWithScopeDomain();
        DensoSupplierRelationDomain densoSupplierRelationDomain = 
            new DensoSupplierRelationDomain();
        densoSupplierRelationDomain.setSCd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
        densoSupplierRelationDomainList.add(densoSupplierRelationDomain);
        companySupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        companySupplierWithScopeDomain.getCompanySupplierDomain().setIsActive(
            Constants.IS_ACTIVE);
        List<CompanySupplierDomain> companySupplierList = companySupplierService
            .searchCompanySupplierNameByRelation(companySupplierWithScopeDomain);
        
        supplierUserRegistrationDomain.getUserSupplierDetailDomain()
            .getSpsMCompanySupplierDomain().setCompanyName(
                companySupplierList.get(Constants.ZERO).getCompanyName());
        supplierUserRegistrationDomain.setIsActive(Constants.IS_ACTIVE);
        return supplierUserRegistrationDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#transactUpdateSupplierUser
     * (com.globaldenso.asia.sps.business.domain.SupplierUserRegistrationDomain)
     */
    public SupplierUserRegistrationDomain transactUpdateSupplierUser(SupplierUserRegistrationDomain
        supplierUserRegistrationDomain) throws ApplicationException
    {
        Locale locale = supplierUserRegistrationDomain.getLocale();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(
            supplierUserRegistrationDomain.getUpdateDatetime(), patternTimestamp);
        Timestamp lastUpdateDatetimeSupplier = DateUtil.parseToTimestamp(
            supplierUserRegistrationDomain.getUpdateDatetimeSupplier(), patternTimestamp);
        
        /*validate for create supplier user*/
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        errorMessageList = validate(supplierUserRegistrationDomain);
        
        /*Check existing DSC ID in master data*/
        if(!StringUtil.checkNullOrEmpty(supplierUserRegistrationDomain.getUserDomain().getDscId()))
        {
            SpsMUserCriteriaDomain criteriaDomain = new SpsMUserCriteriaDomain();
            criteriaDomain.setDscId(supplierUserRegistrationDomain.getUserDomain().getDscId());
            int countUser = spsMUserService.searchCount(criteriaDomain);
            if(countUser < Constants.ZERO ){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                        SupplierPortalConstant.LBL_CURRENT_USER)));
            }
        }
        
        if(Constants.ZERO < errorMessageList.size()){
            supplierUserRegistrationDomain.setErrorMessageList(errorMessageList);
            return supplierUserRegistrationDomain;
        }
        
        /*Update user information*/
        SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
        SpsMUserCriteriaDomain spsMUserCriteriaDomain = new SpsMUserCriteriaDomain();
        spsMUserDomain.setDscId(supplierUserRegistrationDomain.getUserDomain().getDscId());
        spsMUserDomain.setFirstName(supplierUserRegistrationDomain.getUserDomain()
            .getFirstName());
        spsMUserDomain.setMiddleName(supplierUserRegistrationDomain.getUserDomain()
            .getMiddleName());
        spsMUserDomain.setLastName(supplierUserRegistrationDomain.getUserDomain()
            .getLastName());
        spsMUserDomain.setDepartmentName(supplierUserRegistrationDomain.getUserDomain()
            .getDepartmentName());
        spsMUserDomain.setEmail(supplierUserRegistrationDomain.getUserDomain().getEmail());
        spsMUserDomain.setTelephone(supplierUserRegistrationDomain.getUserDomain()
            .getTelephone());
        spsMUserDomain.setIsActive(supplierUserRegistrationDomain.getUserDomain()
            .getIsActive());
        spsMUserDomain.setLastUpdateDscId(supplierUserRegistrationDomain.getUserDomain()
            .getUpdateUser());
        spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
        
        spsMUserCriteriaDomain.setDscId(supplierUserRegistrationDomain.getUserDomain()
            .getDscId());
        spsMUserCriteriaDomain.setLastUpdateDatetime(lastUpdateDatetime);
        
        int countUpdateUser = spsMUserService.updateByCondition(spsMUserDomain, 
            spsMUserCriteriaDomain);
        if(countUpdateUser <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_UPDATE_USER_INFO);
        }
        
        /*Update supplier user information*/
        SpsMUserSupplierDomain spsMUserSupplierDomain = new SpsMUserSupplierDomain();
        spsMUserSupplierDomain.setDscId(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDscId());
        spsMUserSupplierDomain.setSCd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
        spsMUserSupplierDomain.setSPcd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSPcd());
        spsMUserSupplierDomain.setDOwner(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDOwner());
        spsMUserSupplierDomain.setEmlSInfoNotfoundFlag(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
            .getEmlSInfoNotfoundFlag());
        spsMUserSupplierDomain.setEmlUrgentOrderFlag(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain()
            .getEmlUrgentOrderFlag());
        spsMUserSupplierDomain.setEmlAllowReviseFlag(
            supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                .getSpsMUserSupplierDomain().getEmlAllowReviseFlag());
        spsMUserSupplierDomain.setEmlCancelInvoiceFlag(
            supplierUserRegistrationDomain.getUserSupplierDetailDomain()
                .getSpsMUserSupplierDomain().getEmlCancelInvoiceFlag());
        spsMUserSupplierDomain.setLastUpdateDscId(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getLastUpdateDscId());
        spsMUserSupplierDomain.setLastUpdateDatetime(commonService.searchSysDate());
        
        SpsMUserSupplierCriteriaDomain spsMUserSupplierCriteriaDomain = 
            new SpsMUserSupplierCriteriaDomain();
        spsMUserSupplierCriteriaDomain.setDscId(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDscId());
        spsMUserSupplierCriteriaDomain.setLastUpdateDatetime(lastUpdateDatetimeSupplier);
        
        int countUpdateSupplier = spsMUserSupplierService.updateByCondition(
            spsMUserSupplierDomain, spsMUserSupplierCriteriaDomain);
        if(countUpdateSupplier <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_UPDATE_SUPPLIER_USER_INFO);
        }
        
        /*Get Company Name*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = new 
            ArrayList<DensoSupplierRelationDomain>();
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain = 
            new CompanySupplierWithScopeDomain();
        DensoSupplierRelationDomain densoSupplierRelationDomain = 
            new DensoSupplierRelationDomain();
        densoSupplierRelationDomain.setSCd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
        densoSupplierRelationDomainList.add(densoSupplierRelationDomain);
        companySupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        companySupplierWithScopeDomain.getCompanySupplierDomain().setIsActive(
            Constants.IS_ACTIVE);
        List<CompanySupplierDomain> companySupplierList = companySupplierService
            .searchCompanySupplierNameByRelation(companySupplierWithScopeDomain);
        
        supplierUserRegistrationDomain.getUserSupplierDetailDomain()
            .getSpsMCompanySupplierDomain().setCompanyName(
                companySupplierList.get(Constants.ZERO).getCompanyName());
        supplierUserRegistrationDomain.setIsActive(Constants.IS_ACTIVE);
        return supplierUserRegistrationDomain;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#searchSelectedCompanySupplier
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public SupplierUserRegistrationDomain searchSelectedCompanySupplier(PlantSupplierWithScopeDomain
        plantSupplierWithScopeDomain) throws ApplicationException
    {
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        List<PlantSupplierDomain> plantSupplierList = 
            new ArrayList<PlantSupplierDomain>();
        DataScopeControlDomain dataScopeControlDomain = plantSupplierWithScopeDomain
            .getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain = plantSupplierWithScopeDomain
            .getPlantSupplierDomain();
        PlantSupplierDomain plantSupplierCriteria = new PlantSupplierDomain();
        List<SpsMAs400VendorDomain> vendorCdList = new ArrayList<SpsMAs400VendorDomain>();
        SupplierUserRegistrationDomain result = new SupplierUserRegistrationDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(Constants.ZERO < densoSupplierRelationDomainList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
       /* Get list of supplier plant information*/
        plantSupplierCriteria.setSCd(plantSupplierDomain.getSCd());
        plantSupplierCriteria.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierCriteria);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierList =  plantSupplierService.searchPlantSupplier(
            plantSupplierWithScopeDomain);
        if(plantSupplierList.size() <= Constants.ZERO){
            plantSupplierList = new ArrayList<PlantSupplierDomain>();
        }
        result.setPlantSupplierList(plantSupplierList);
        
        /* Get list of vendor code information*/
        SpsMAs400VendorCriteriaDomain vendorCriteriaDomain = new SpsMAs400VendorCriteriaDomain();
        vendorCriteriaDomain.setSCd(plantSupplierDomain.getSCd());
        vendorCdList = spsMAs400VendorService.searchByCondition(vendorCriteriaDomain);
        if(Constants.ZERO == vendorCdList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_AS400_VENDOR);
        }else{
            result.setVendorCdList(vendorCdList);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain) throws ApplicationException
    {
        Locale locale = companySupplierWithScopeDomain.getLocale();
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = companySupplierWithScopeDomain.getDataScopeControlDomain();
        CompanySupplierDomain companySupplier
            = companySupplierWithScopeDomain.getCompanySupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if (densoSupplierRelationDomainList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        /*Get list of company supplier information*/
        companySupplier.setIsActive(Constants.IS_ACTIVE);
        result = companySupplierService.searchCompanySupplierNameByRelation(
            companySupplierWithScopeDomain);
        
        if(null == result || result.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService#searchRoleCanOperate(java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /** Private method for get Supplier User Information.
     * 
     * @param userDomain the UserDomain.
     * @return SupplierUserDetailDomain
     * @throws ApplicationException the ApplicationException
     */
    private UserSupplierDetailDomain searchSupplierUserInformation(UserDomain userDomain)
        throws ApplicationException
    {
        Locale locale = userDomain.getLocale();
        UserSupplierDetailDomain supplierUserDetailDomain = new UserSupplierDetailDomain();
        SpsMUserCriteriaDomain spsMUserCriteriaDomain = new SpsMUserCriteriaDomain();
        SpsMUserSupplierCriteriaDomain spsMUserSupplierCriteriaDomain = 
            new SpsMUserSupplierCriteriaDomain();
        List<SpsMUserDomain> SpsMUserList = new ArrayList<SpsMUserDomain>();
        List<SpsMUserSupplierDomain> spsMUserSupplierList = new ArrayList<SpsMUserSupplierDomain>();
        
        /*Get user information*/
        spsMUserCriteriaDomain.setDscId(userDomain.getDscId());
        spsMUserCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        SpsMUserList = spsMUserService.searchByCondition(spsMUserCriteriaDomain);
        if(SpsMUserList.size() < Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,  
                SupplierPortalConstant.LBL_USER);
        }else{
            for(SpsMUserDomain userResult : SpsMUserList){
                supplierUserDetailDomain.setSpsMUserDomain(userResult);
            }
        }
        
        /*Get supplier user information*/
        spsMUserSupplierCriteriaDomain.setDscId(userDomain.getDscId());
        spsMUserSupplierList = spsMUserSupplierService.searchByCondition(
            spsMUserSupplierCriteriaDomain);
        if(spsMUserSupplierList.size() < Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,  
                SupplierPortalConstant.LBL_SUPPLIER_USER);
        }else{
            for(SpsMUserSupplierDomain supplierUserResult : spsMUserSupplierList){
                supplierUserDetailDomain.setSpsMUserSupplierDomain(supplierUserResult);
            }
        }
        return supplierUserDetailDomain;
    }
    
    /** Private method for validate
     * 
     * @param supplierUserRegistrationDomain the supplierUserRegistrationDomain.
     * @return the list of error message.
     * @throws ApplicationException the ApplicationException
     */
    private List<ApplicationMessageDomain> validate(SupplierUserRegistrationDomain 
        supplierUserRegistrationDomain) throws ApplicationException
    {
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = supplierUserRegistrationDomain.getLocale();
        String message = new String();
        
        UserDomain userDomain = supplierUserRegistrationDomain.getUserDomain();
        SpsMUserSupplierDomain supplierDomain = supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain();
        
        /*Check Mandatory field.*/
        if(StringUtil.checkNullOrEmpty(userDomain.getDscId())){
            message = MessageUtil.getApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0007, SupplierPortalConstant.LBL_DSC_ID);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                message));
        }
        if(StringUtil.checkNullOrEmpty(supplierDomain.getDOwner())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_D_OWNER)));
        }
        if(StringUtil.checkNullOrEmpty(supplierDomain.getSCd())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_NAME)));
        }
        if(StringUtil.checkNullOrEmpty(supplierDomain.getSPcd())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE)));
        }
        if(StringUtil.checkNullOrEmpty(userDomain.getFirstName())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_FIRST_NAME)));
        }
        if(StringUtil.checkNullOrEmpty(userDomain.getLastName())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_LAST_NAME)));
        }
        if(StringUtil.checkNullOrEmpty(userDomain.getEmail())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                    SupplierPortalConstant.LBL_EMAIL)));
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getDscId())){
            if(Constants.MAX_DSC_ID_LENGTH < userDomain.getDscId().trim().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_DSC_ID),
                            String.valueOf(Constants.MAX_DSC_ID_LENGTH)})));
            }else{
                /**Changed DSC ID Validation refer to 23/10/2014 12:00 am.*/
                /*Boolean isnumber = userDomain.getDscId().matches(Constants.REGX_NUMBER_FORMAT);
                if(!isnumber){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_DSC_ID)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }*/
            }
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getFirstName())){
            if(Constants.MAX_FIRST_NAME_LENGTH < userDomain.getFirstName().trim().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_FIRST_NAME),
                            String.valueOf(Constants.MAX_FIRST_NAME_LENGTH)})));
            }
            /*else{
                Boolean isLetter = userDomain.getFirstName().matches(Constants.REGX_LETTER_FORMAT);
                if(!isLetter){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_FIRST_NAME)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
            }*/
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getMiddleName())){
            if(Constants.MAX_MIDDLE_NAME_LENGTH < userDomain.getMiddleName().trim().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_MIDDLE_NAME),
                            String.valueOf(Constants.MAX_MIDDLE_NAME_LENGTH)})));
            }
            /*else{
                Boolean isLetter = userDomain.getMiddleName().matches(Constants.REGX_LETTER_FORMAT);
                if(!isLetter){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_MIDDLE_NAME)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                }
            }*/
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getLastName())){
            if(Constants.MAX_LAST_NAME_LENGTH < userDomain.getLastName().trim().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_LAST_NAME),
                            String.valueOf(Constants.MAX_LAST_NAME_LENGTH)})));
            }
            /*else{
                Boolean isLetter = userDomain.getLastName().matches(Constants.REGX_LETTER_FORMAT);
                if(!isLetter){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_LAST_NAME)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                }
            }*/
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getEmail())){
            if(Constants.MAX_EMAIL_LENGTH < userDomain.getEmail().trim().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_EMAIL),
                            String.valueOf(Constants.MAX_EMAIL_LENGTH)})));
            }
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getTelephone())){
            if(Constants.MAX_TELEPHONE_LENGTH < userDomain.getTelephone().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_TELEPHONE),
                            String.valueOf(Constants.MAX_TELEPHONE_LENGTH)})));
            }
        }
        if(!StringUtil.checkNullOrEmpty(userDomain.getDepartmentName())){
            if(Constants.MAX_DEPARTMENT_LENGTH < userDomain.getDepartmentName().length()){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        new String[]{ MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_DEPARTMENT_NAME),
                            String.valueOf(Constants.MAX_DEPARTMENT_LENGTH)})));
            }
            /*else{
                Boolean isLetter = userDomain.getDepartmentName()
                    .matches(Constants.REGX_LETTER_FORMAT);
                if(!isLetter){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                            MessageUtil.getLabel(locale, 
                                SupplierPortalConstant.LBL_DEPARTMENT_NAME)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
            }*/
        }
        /**** Validate email format that should "a@b.com" and support "," separate*/
        
        if(!StringUtil.checkNullOrEmpty(userDomain.getEmail())){
            String[] strEmail = StringUtils.splitPreserveAllTokens(userDomain.getEmail(), 
                Constants.SYMBOL_COMMA);
            List<String> emailInvalidList = new ArrayList<String>();
            int count = 0;
            for(int i = Constants.ZERO; i < strEmail.length; i++){
                boolean isEmail = strEmail[i].matches(Constants.REGX_EMAIL_FORMAT);
                if(!isEmail){
                    count = i + Constants.ONE;
                    emailInvalidList.add(String.valueOf(count));
                }
            }
            if(Constants.ZERO < emailInvalidList.size()){
                String result = StringUtil.convertListToStringCommaSeperate(emailInvalidList);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0041, new String[]{result})));
            }
        }
        /*Check existing DENSO company code*/
        SpsMCompanyDensoCriteriaDomain companyDensoCriteriaDomain = 
            new SpsMCompanyDensoCriteriaDomain();
        companyDensoCriteriaDomain.setDCd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getDOwner());
        int countDensoCompany = spsMCompanyDensoService.searchCount(companyDensoCriteriaDomain);
        if(countDensoCompany < Constants.ZERO){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_OWNER_COMPANY)));
        }
        
        /*Check existing supplier company code*/
        SpsMCompanySupplierCriteriaDomain spsMCompanySupplierCriteriaDomain = 
            new SpsMCompanySupplierCriteriaDomain();
        spsMCompanySupplierCriteriaDomain.setSCd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
        int countCompanySupplier = spsMCompanySupplierService.searchCount(
            spsMCompanySupplierCriteriaDomain);
        if(countCompanySupplier < Constants.ZERO){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY)));
        }
        /*Check existing supplier plant code*/
        SpsMPlantSupplierCriteriaDomain spsMPlantSupplierCriteriaDomain = 
            new SpsMPlantSupplierCriteriaDomain();
        spsMPlantSupplierCriteriaDomain.setSCd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSCd());
        spsMPlantSupplierCriteriaDomain.setSPcd(supplierUserRegistrationDomain
            .getUserSupplierDetailDomain().getSpsMUserSupplierDomain().getSPcd());
        int countPlantSupplier = spsMPlantSupplierService.searchCount(
            spsMPlantSupplierCriteriaDomain);
        if(countPlantSupplier < Constants.ZERO){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_SUPPLIER_PLANT)));
        }
        return errorMessageList;
    }
}