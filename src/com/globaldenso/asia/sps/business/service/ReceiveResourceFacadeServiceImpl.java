/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Parichat            Create
 * 2015/11/19 CSI Akat                [IN037]
 * 2018/12/17 CTC P.Pawan             [IN1744]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.webservicecallrest.WebServiceCallerRest;
import com.globaldenso.ai.library.webservicecallrest.domain.WebServiceCallerRestDomain;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.WebServiceConstants;
import com.globaldenso.asia.sps.ws.rest.resource.ReceiveResource;
import com.sun.jersey.api.client.GenericType;

/**
 * <p>The Interface ReceiveResourceServices.</p>
 * <p>Service for Receive Resource about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchAs400ServerList</li>
 * </ul>
 *
 * @author Netband
 */
public class ReceiveResourceFacadeServiceImpl implements ReceiveResourceFacadeService {

    // [IN037] : output log when create/drop alias.
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(ReceiveResourceFacadeServiceImpl.class);
    
    /** Denso Company Services. */
    private CompanyDensoService companyDensoService;
    
    /** Delivery Order Service. */
    private DeliveryOrderService deliveryOrderService;
    
    /** common Service. */
    private CommonService commonService;
    /**
     * Instantiates a new Receive Resource Service service impl.
     */
    public ReceiveResourceFacadeServiceImpl(){
        super();
    }

    /**
     * <p>Setter method for companyDensoService.</p>
     *
     * @param companyDensoService Set for companyDensoService
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    /**
     * <p>Setter method for companyDensoService.</p>
     *
     * @param deliveryOrderService Set for deliveryOrderService
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }
    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * If parameter input shorter than parameter len. Append input with space.
     * @param input - String input
     * @param len - target length
     * @return String that lenght equal to parameter len
     * */
    private String fillData(String input, int len) {
        StringBuffer result = new StringBuffer();
        result.append(input);
        for (int i = input.length(); i < len; i++) {
            result.append(Constants.SYMBOL_SPACE);
        }
        return result.toString();
    }

    /**
     * Split input to decimal part and floating part. If decimal part shorter than 
     * allLen - floatLen, fill with zero.<br />
     * If floating part shorter than floatLen, fill with zero.
     * @param input - String input
     * @param allLen - target length for whole string
     * @param floatLen - target length for floating part
     * @return String that lenght equal to parameter allLen
     * */
    private String fillDataNumber(String input, int allLen, int floatLen) {
        StringBuffer result = new StringBuffer();
        StringBuffer regexDot = new StringBuffer();
        // [.]
        regexDot.append(Constants.SYMBOL_OPEN_SQUARE_BRACKET).append(Constants.SYMBOL_DOT)
            .append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET);
        
        input = input.replaceAll(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
        
        String[] splitedInput = input.split(regexDot.toString());
        String decimal = splitedInput[Constants.ZERO];
        String floating = Constants.EMPTY_STRING;
        if (Constants.TWO == splitedInput.length) {
            floating = splitedInput[Constants.ONE];
        }
        
        int decLen = allLen - floatLen;
        int padding = decLen - decimal.length();
        for (int i = Constants.ZERO; i < padding; i++) {
            result.append(Constants.STR_ZERO);
        }
        result.append(decimal);
        
        result.append(floating);
        for (int j = floating.length(); j < floatLen; j++) {
            result.append(Constants.STR_ZERO);
        }
        
        return result.toString();
    }

    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.ReceiveResourceFacadeService#createReceiveByScan(
     * List<com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain>)
     */
    public String createReceiveByScan(
        List<ReceiveByScanDomain> receiveByScanDomainList)
        throws ApplicationException {
        String resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SUCCESS;
        Locale locale =  LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
        
        //These string are declare for the paramenter of web service.
        String textDeviceId = Constants.EMPTY_STRING;
        String textDscId = Constants.EMPTY_STRING;
        String textServerName = Constants.EMPTY_STRING;
        String textReceiveBatchNo = Constants.EMPTY_STRING;
        String textTextSeqNo = Constants.EMPTY_STRING;
        String textNumOfText = Constants.EMPTY_STRING;
        String textReceiveDate = Constants.EMPTY_STRING;
        String textReceiveTime = Constants.EMPTY_STRING;
        String textProcessStatus = Constants.EMPTY_STRING;
        String textProcessDate = Constants.EMPTY_STRING;
        String textProcessTime = Constants.EMPTY_STRING;
        String textReceiveDataMsg = Constants.EMPTY_STRING;
        
        //These list are declare for collect data before change to below string the paramenter of web service.
        List<String> listDeviceId = new ArrayList<String>();
        List<String> listDscId = new ArrayList<String>();
        List<String> listServerName = new ArrayList<String>();
        List<String> listReceiveBatchNo = new ArrayList<String>();
        List<String> listTextSeqNo = new ArrayList<String>();
        List<String> listNumOfText = new ArrayList<String>();
        List<String> listReceiveDate = new ArrayList<String>();
        List<String> listReceiveTime = new ArrayList<String>();
        List<String> listProcessStatus = new ArrayList<String>();
        List<String> listProcessDate = new ArrayList<String>();
        List<String> listProcessTime = new ArrayList<String>();
        List<String> listReceiveDataMsg = new ArrayList<String>();
        
        List<ReceiveByScanDomain> receiveByScanDomainNotExist = new ArrayList<ReceiveByScanDomain>();
        List<String> receiveByScanDomainBatchNumber = new ArrayList<String>();
        
        //1 get CIGMA end point from CompanyDensoService. searchAs400ServerList
        SpsMCompanyDensoDomain companyDensoDomain = new SpsMCompanyDensoDomain();
        String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(
            receiveByScanDomainList.get(Constants.ZERO).getDensoCompanyCode());
        companyDensoDomain.setDCd(densoCodeStr);
        List<As400ServerConnectionInformationDomain> schemaResultList = 
            companyDensoService.searchAs400ServerList(companyDensoDomain);
        
        //2 Call searchAs400ServerStatus from ReceiveResource
        WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();
        StringBuffer url = new StringBuffer();
        url.append(schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestIpAddress());
//        url.append("http://localhost:8086/CIGMA_REST/rest/");
        url.append(WebServiceConstants.CIGMA_RECEIVE);
        domain.setUrl(url.toString());
        domain.setUserName(schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestUserName());
        domain.setPassword(schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestPassword());
        //Start [IN1744]  Extract Lib name to database.
        domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_OTHER_1,
            schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getSchemaNameOther1());
        //End [IN1744]  Extract Lib name to database.
        domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
            schemaResultList.get(Constants.ZERO).getOrderLpar().getLparName());
        domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
            schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdSchemaCigma());
        domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
            schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdSchemaNameShare());
        domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
            schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdSchemaCigma());
        domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
            schemaResultList.get(Constants.ZERO).getSpsMCompanyDensoDomain().getDCd());
        domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
            schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdConnectionNo().toString());
        try {
            resultCode = WebServiceCallerRest.put(domain, new GenericType<String>() {});
        } catch (WebServiceCallerRestException e){
            resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
            LOG.error(resultCode);
            LOG.error(e.getMessage());
            return resultCode;
        } catch (Exception e) {
            resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
            LOG.error(resultCode);
            LOG.error(e.getMessage());
            return resultCode;
        }
        if(resultCode.equals(ReceiveResource.RECEIVE_BY_SCAN_SERVICE_CIGMA_BUSY)){
            LOG.error(ReceiveResource.RECEIVE_BY_SCAN_SERVICE_CIGMA_BUSY);
            resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_CIGMA_BUSY;
            return resultCode;
        }
        
        //3   generate batch No. for processing
        Integer receiveBatchNo = deliveryOrderService.searchBatchNumber();
        
        //4 Loop each ReceiveByScanDomain in <List> ReceiveByScanDomain to convert data for sending to CIGMA
        Integer count = 1;
        for(ReceiveByScanDomain receiveByScanDomain : receiveByScanDomainList){
            //prepare data to set to receiveByScanDomain var.
            receiveByScanDomain.setBatchNo(fillDataNumber(receiveBatchNo.toString(), SupplierPortalConstant.LENGTH_TAG_SEQ, Constants.ZERO));
            receiveByScanDomain.setAs400ServerInformation(schemaResultList.get(Constants.ZERO));
            receiveByScanDomain.setTextSeqNo((count++).toString());
            receiveByScanDomain.setNumOfText(new StringBuffer().append(receiveByScanDomainList.size()).toString());
            receiveByScanDomain.setServerName(schemaResultList.get(Constants.ZERO).getOrderLpar().getLparName());
            receiveByScanDomain.setTransmitionDatetime(commonService.searchSysDate());
            receiveByScanDomain.setAsnReceiveDatetime(commonService.searchSysDate());
            
            receiveByScanDomain.setReceiveDatetime(DateUtil.parseToTimestamp(receiveByScanDomain.getReceiveDate(), DateUtil.PATTERN_YYYYMMDD_HHMMSS));
            receiveByScanDomain.setTagScanDateTime(DateUtil.parseToTimestamp(receiveByScanDomain.getTagScanDate(), DateUtil.PATTERN_YYYYMMDD_HHMM));

            StringBuffer receiveDataMsg = new StringBuffer().append(Constants.EMPTY_STRING);
            receiveDataMsg
                .append(fillData(receiveByScanDomain.getAsnNo(), SupplierPortalConstant.LENGTH_ASN_NO))
                .append(fillData(receiveByScanDomain.getDensoCompanyCode(), SupplierPortalConstant.LENGTH_DENSO_CODE))
                .append(fillData(receiveByScanDomain.getCigmaDoNo(), SupplierPortalConstant.LENGTH_CIGMA_DO_NO))
                .append(fillData(receiveByScanDomain.getdPn(), SupplierPortalConstant.LENGTH_DENSO_PART_NO))
                .append(fillDataNumber(receiveByScanDomain.getKanbanTagSeq(), SupplierPortalConstant.LENGTH_TAG_SEQ, Constants.ZERO))
                .append(fillData(receiveByScanDomain.getSupplierCode(), SupplierPortalConstant.LENGTH_SUPPLIER_CODE))
                .append(fillData(receiveByScanDomain.getDensoPlantCode(), SupplierPortalConstant.LENGTH_DENSO_PLANT_CODE))
                .append(fillData(receiveByScanDomain.getWh(), SupplierPortalConstant.LENGTH_WAREHOUSE))
                .append(fillDataNumber(receiveByScanDomain.getAllocateQty(), SupplierPortalConstant.LENGTH_ALLOCATE_QTY, Constants.TWO))
                .append(fillData(DateUtil.format(receiveByScanDomain.getReceiveDatetime(), DateUtil.PATTERN_YYYYMMDD), SupplierPortalConstant.LENGTH_ETD))
                .append(fillData(receiveByScanDomain.getAsnStatus(), SupplierPortalConstant.LENGTH_IS_PARTIAL))
                .append(fillData(receiveByScanDomain.getRcvLane(), SupplierPortalConstant.LENGTH_RCV_LANE))
                .append(fillDataNumber(receiveByScanDomain.getKanbanType(), SupplierPortalConstant.LENGTH_KANBAN_TYPE, Constants.ZERO))
                .append(receiveByScanDomain.getTagScanDate())
                .append(receiveByScanDomain.getAsnScanDateTime())
                .append(fillData(Constants.EMPTY_STRING, SupplierPortalConstant.LENGTH_APPEND_RECEIVE_DATA_MSG));
            receiveByScanDomain.setReceiveDataMsg(receiveDataMsg.toString());
            List<ReceiveByScanDomain> isExistTransmitLog = null;
            try{
                isExistTransmitLog = deliveryOrderService.isExistTransmitLog(receiveByScanDomain);
            } catch (Exception e){
                resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
                LOG.error(resultCode);
                LOG.error(e.getMessage());
            }
            if(isExistTransmitLog.size() == Constants.ZERO){
                if(!receiveByScanDomainBatchNumber.contains(receiveByScanDomain.getBatchNo())){
                    receiveByScanDomainBatchNumber.add(receiveByScanDomain.getBatchNo());
                }
                try{
//                    deliveryOrderService.createTransmitLog(receiveByScanDomain);
//                    5   Append <List> ReceiveByScanDomain. receiveDataMsg  as CSV
//                    Set data to list before change to string separate by comma
                    listDeviceId.add(receiveByScanDomain.getDeviceId());
                    listDscId.add(receiveByScanDomain.getDscId());
                    listServerName.add(receiveByScanDomain.getServerName());
                    listReceiveBatchNo.add(receiveByScanDomain.getBatchNo());
                    listTextSeqNo.add(receiveByScanDomain.getTextSeqNo());
                    listNumOfText.add(receiveByScanDomain.getNumOfText());
                    listReceiveDate.add(DateUtil.format(receiveByScanDomain.getReceiveDatetime(), DateUtil.PATTERN_YYYYMMDD));
                    listReceiveTime.add(DateUtil.format(receiveByScanDomain.getReceiveDatetime(), DateUtil.PATTERN_HHMM));
                    listProcessStatus.add(Constants.EMPTY_STRING);
                    listProcessDate.add(Constants.EMPTY_STRING);
                    listProcessTime.add(Constants.EMPTY_STRING);
                    listReceiveDataMsg.add(receiveByScanDomain.getReceiveDataMsg());
                    
                    receiveByScanDomainNotExist.add(receiveByScanDomain);
                } catch (Exception e){
                    resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
                    LOG.error(resultCode);
                    LOG.error(e.getMessage());
                }
            } else {
                if(!receiveByScanDomainBatchNumber.contains(isExistTransmitLog.get(Constants.ZERO).getBatchNo())){
                    receiveByScanDomainBatchNumber.add(isExistTransmitLog.get(Constants.ZERO).getBatchNo());
                }
                try {
                    LOG.error(MessageUtil.getApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0044));
                } catch (Exception e) {
                    resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
                    LOG.error(resultCode);
                    LOG.error(e.getMessage());
                }
            }
        }
        //6 Call CommonWebServiceUtil.createReceiveByScan() 
        if(resultCode.equals(ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SUCCESS)){
            if(listDeviceId.size() > Constants.ZERO){
                textDeviceId = StringUtil.convertListToStringCommaSeperate(listDeviceId);
                textDscId = StringUtil.convertListToStringCommaSeperate(listDscId);
                textServerName = StringUtil.convertListToStringCommaSeperate(listServerName);
                textReceiveBatchNo = StringUtil.convertListToStringCommaSeperate(listReceiveBatchNo);
                textTextSeqNo = StringUtil.convertListToStringCommaSeperate(listTextSeqNo);
                textNumOfText = StringUtil.convertListToStringCommaSeperate(listNumOfText);
                textReceiveDate = StringUtil.convertListToStringCommaSeperate(listReceiveDate);
                textReceiveTime = StringUtil.convertListToStringCommaSeperate(listReceiveTime);
                textProcessStatus = StringUtil.convertListToStringCommaSeperate(listProcessStatus);
                textProcessDate = StringUtil.convertListToStringCommaSeperate(listProcessDate);
                textProcessTime = StringUtil.convertListToStringCommaSeperate(listProcessTime);
                textReceiveDataMsg = StringUtil.convertListToStringCommaSeperate(listReceiveDataMsg);
                
                domain.setParamMap(WebServiceConstants.PARAM_DEVICE_ID, textDeviceId);
                domain.setParamMap(WebServiceConstants.PARAM_DSC_ID, textDscId);
                domain.setParamMap(WebServiceConstants.PARAM_SERVER_NAME, textServerName);
                domain.setParamMap(WebServiceConstants.PARAM_RECEIVE_BATCH_NO, textReceiveBatchNo);
                domain.setParamMap(WebServiceConstants.PARAM_TEXT_SEQ_NO, textTextSeqNo);
                domain.setParamMap(WebServiceConstants.PARAM_NUM_OF_TEXT, textNumOfText);
                domain.setParamMap(WebServiceConstants.PARAM_RECEIVE_DATE, textReceiveDate);
                domain.setParamMap(WebServiceConstants.PARAM_RECEIVE_TIME, textReceiveTime);
                domain.setParamMap(WebServiceConstants.PARAM_PROCESS_STATUS, textProcessStatus);
                domain.setParamMap(WebServiceConstants.PARAM_PROCESS_DATE, textProcessDate);
                domain.setParamMap(WebServiceConstants.PARAM_PROCESS_TIME, textProcessTime);
                domain.setParamMap(WebServiceConstants.PARAM_RECEIVE_DATA_MSG, textReceiveDataMsg);
                
                domain.setParamMap(WebServiceConstants.PARAM_AS400_ADDRESS,
                    schemaResultList.get(Constants.ZERO).getOrderLpar().getIpAddress());
                domain.setParamMap(WebServiceConstants.PARAM_AS400_USERNAME, 
                    schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestUserName());
                domain.setParamMap(WebServiceConstants.PARAM_AS400_PASSWORD, 
                    schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestPassword());
                
                try{
                    resultCode = WebServiceCallerRest.put(domain, new GenericType<String>(){});
                } catch (WebServiceCallerRestException e){
                    resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
                    LOG.error(resultCode);
                    LOG.error(e.getMessage());
                }
            }
        }
        
        //7 Save log of transmit data to SPS (SPS_T_BHT_TRANSMIT_LOG)
        if(resultCode.equals(ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SUCCESS)){
            for(ReceiveByScanDomain receiveByScanDomain : receiveByScanDomainNotExist){
                try{
                    deliveryOrderService.createTransmitLog(receiveByScanDomain);
                } catch (Exception e){
                    resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
                    LOG.error(resultCode);
                    LOG.error(e.getMessage());
                }
            }
        }
        //8. Check the result of execution from CIGMA.
//        try{
//            Thread.sleep(receiveByScanDomainList.size() * SupplierPortalConstant.AVERAGE_PER_RECORD);
//            if(resultCode.equals(ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SUCCESS)){
//                if(receiveByScanDomainBatchNumber.size() > Constants.ZERO){
//
//                    WebServiceCallerRestDomain domainCheckExecute = new WebServiceCallerRestDomain();
//                    StringBuffer urlCheckExecute = new StringBuffer();
//                    urlCheckExecute.append(schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestIpAddress());
////                    url.append("http://localhost:8086/CIGMA_REST/rest/");
//                    urlCheckExecute.append(WebServiceConstants.CIGMA_RECEIVE);
//                    domainCheckExecute.setUrl(urlCheckExecute.toString());
//                    domainCheckExecute.setUserName(schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestUserName());
//                    domainCheckExecute.setPassword(schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdRestPassword());
//                    domainCheckExecute.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
//                        schemaResultList.get(Constants.ZERO).getOrderLpar().getLparName());
//                    domainCheckExecute.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
//                        schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdSchemaCigma());
//                    domainCheckExecute.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
//                        schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdSchemaNameShare());
//                    domainCheckExecute.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
//                        schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdSchemaCigma());
//                    domainCheckExecute.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
//                        schemaResultList.get(Constants.ZERO).getSpsMCompanyDensoDomain().getDCd());
//                    domainCheckExecute.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
//                        schemaResultList.get(Constants.ZERO).getSpsMAs400SchemaDomain().getOrdConnectionNo().toString());
//                    for(String batchNumber : receiveByScanDomainBatchNumber){
//                        domainCheckExecute.setParamMap(WebServiceConstants.PARAM_RECEIVE_BATCH_NO, batchNumber);
//                        try{
//                            resultCode = WebServiceCallerRest.get(domainCheckExecute, new GenericType<String>(){});
//                        } catch (WebServiceCallerRestException e){
//                            resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
//                            LOG.error(resultCode);
//                            LOG.error(e.getMessage());
//                        }                        
//                    }
//                }
//            }
//        } catch (InterruptedException e) {
//            resultCode = ReceiveResource.RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
//            LOG.error(resultCode);
//            LOG.error(e.getMessage());
//        }
        return resultCode;
    }
}
