/*
 * ModifyDate Development company    Describe 
 * 2014/07/10 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.asia.sps.business.domain.SendEmailDomain;


/**
 * <p>The Interface MailService.</p>
 * <p>Manage data send mail of Abnormal Order data</p>
 * <ul>
 * <li>Method sendEmail : sendEmail</li>
 * </ul>
 *
 * @author CSI
 * @version 1.00
 */
public interface MailService {
    
    /**
     * <p>Create mail of Abnormal Order data.</p>
     * <p>Create mail of Abnormal Order data result</p>
     * <ul>
     * <li>Send notify mail to person list.</li>
     * </ul>
     * 
     * @param sendEmailDomain the send email domain
     * @return true, if successful
     * @throws Exception the Exception
     */
    public boolean sendEmail(SendEmailDomain sendEmailDomain)throws Exception;
}