/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.dao.MiscellaneousDao;;

/**
 * <p>The Class MiscServiceImpl.</p>
 * <p>Service for miscellaneous about search miscellaneous data.</p>
 * <ul>
 * <li>Method search  : searchMiscValue</li>
 * </ul>
 *
 * @author CSI
 */
public class MiscellaneousServiceImpl implements MiscellaneousService {
    
    /** The misc dao. */
    private MiscellaneousDao miscDao;

    /**
     * Instantiates a new misc service impl.
     */
    public MiscellaneousServiceImpl(){
        super();
    }
    
    /**
     * Sets the misc dao.
     * 
     * @param miscDao the misc dao.
     */
    public void setMiscDao(MiscellaneousDao miscDao) {
        this.miscDao = miscDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.MiscService#searchMiscValue(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public Integer searchMiscValue(MiscellaneousDomain miscDomain) {
        return this.miscDao.searchMiscValue(miscDomain);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.MiscService#searchMisc(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public List<MiscellaneousDomain> searchMisc(MiscellaneousDomain miscDomain) {
        return this.miscDao.searchMisc(miscDomain);
    }
}