/*
 * ModifyDate Development company       Describe 
 * 2014/07/01 CSI Phakaporn             Create
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change P/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;

/**
 * <p>The Class Purchase Order Information Facade Service Implement.</p>
 * <p>Manage data of Purchase Order Information.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchPurchaseOrderInformation</li>
 * <li>Method search  : searchPurchaseOrderInformationCsv</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchPurchaseOrderReport</li>
 * <li>Method search  : searchChangeMaterialRelease</li>
 * <li>Method search  : searchPlantBySelectSupplierCompany</li>
 * <li>Method search  : searchPlantBySelectDensoCompany</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method update  : updateAcknowledgementPo</li>
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderInformationFacadeServiceImpl 
    implements PurchaseOrderInformationFacadeService
{

    /** The user service. */
    private MiscellaneousService miscService;
    
    /** The Recrod Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The Purchase Order information service. */
    private PurchaseOrderService purchaseOrderService;
    
    /** The DENSO and Supplier relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;

    /** The Company Supplier service. */
    private CompanySupplierService companySupplierService;

    /** The Company DENSO Service */
    private CompanyDensoService companyDensoService;
    
    /** The SPS_T_PO Service. */
    private SpsTPoService spsTPoService;

    /** The SPS_T_PO_DETAIL Service. */
    private SpsTPoDetailService spsTPoDetailService;

    /** The File Management Service. */
    private FileManagementService fileManagementService;

    /** The Supplier Plant Service. */
    private PlantSupplierService plantSupplierService;

    /** The Plant Denso Service. */
    private PlantDensoService plantDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /** The service for purchaseOrderCoverPageService. */
    private PurchaseOrderCoverPageService purchaseOrderCoverPageService;
    
    /**
     * Instantiates a new Purchase Order Information Facade Service implement.
     */
    public PurchaseOrderInformationFacadeServiceImpl(){
        super();
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets the Misc service.
     * 
     * @param miscService the new Misc service.
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    /**
     * Sets the Purchase Order information service.
     * 
     * @param purchaseOrderService the new Purchase Order information service.
     */
    public void setPurchaseOrderService(
        PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    /**
     * Set method for densoSupplierRelationService.
     * @param densoSupplierRelationService the densoSupplierRelationService to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * Set method for companyDensoService.
     * @param companyDensoService the companyDensoService to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * Set method for companySupplierService.
     * @param companySupplierService the companySupplierService to set
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }

    /**
     * <p>Setter method for fileManagementService.</p>
     *
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * <p>Setter method for plantSupplierService.</p>
     *
     * @param plantSupplierService Set for plantSupplierService
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>Setter method for plantDensoService.</p>
     *
     * @param plantDensoService Set for plantDensoService
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }

    /**
     * <p>Setter method for spsTPoService.</p>
     *
     * @param spsTPoService Set for spsTPoService
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }

    /**
     * <p>Setter method for spsTPoDetailService.</p>
     *
     * @param spsTPoDetailService Set for spsTPoDetailService
     */
    public void setSpsTPoDetailService(SpsTPoDetailService spsTPoDetailService) {
        this.spsTPoDetailService = spsTPoDetailService;
    }

    /**
     * <p>Setter method for recordLimitService.</p>
     *
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for purchaseOrderCoverPageService.</p>
     *
     * @param purchaseOrderCoverPageService Set for purchaseOrderCoverPageService
     */
    public void setPurchaseOrderCoverPageService(PurchaseOrderCoverPageService purchaseOrderCoverPageService) {
        this.purchaseOrderCoverPageService = purchaseOrderCoverPageService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#initial(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public PurchaseOrderInformationDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException
    {
        PurchaseOrderInformationDomain purchaseOrderInformationDomain
            = new PurchaseOrderInformationDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        
        // 1. Get relation between DENSO and Supplier
        List<DensoSupplierRelationDomain> densoSupplierRelationList
            = this.densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if (null == densoSupplierRelationList 
            || densoSupplierRelationList.size() <= Constants.ZERO)
        {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        purchaseOrderInformationDomain.setDensoSupplierRelationList(densoSupplierRelationList);
        
        // 2. Get supplier company information from SupplierCompanyService
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanySupplierDomain> companySupplierList
            = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if (null == companySupplierList || companySupplierList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        purchaseOrderInformationDomain.setCompanySupplierList(companySupplierList);
        
        // X. Get Supplier Plant information from PlantSupplierService
        List<PlantSupplierDomain> plantSupplierList = this.plantSupplierService.searchPlantSupplier(
            plantSupplierWithScope);
        if (plantSupplierList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        purchaseOrderInformationDomain.setPlantSupplierList(plantSupplierList);
        
        // 3. Get DENSO company information from DENSOCompanyService
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoList
            = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if (null == companyDensoList || companyDensoList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        purchaseOrderInformationDomain.setCompanyDensoList(companyDensoList);
        
        // Y. Get DENSO plant information from PlantDensoService
        /* Get list of supplier plant information*/
        List<PlantDensoDomain> plantDensoList = this.plantDensoService.searchPlantDenso(
            plantDensoWithScope);
        if (plantDensoList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        purchaseOrderInformationDomain.setPlantDensoList(plantDensoList);
        
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        
        // 4. Get information for P/O Status Combo box from MiscellaneousService
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_PO_STATUS);
        List<MiscellaneousDomain> poStatusList = miscService.searchMisc(miscDomain);
        if (null == poStatusList || poStatusList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_PO_STATUS);
        }
        purchaseOrderInformationDomain.setPoStatusList(poStatusList);
        
        // 5. Get information for P/O Type Combo box from MiscellaneousService
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_PO_TYPE);
        List<MiscellaneousDomain> poTypeList = miscService.searchMisc(miscDomain);
        if (null == poTypeList || poTypeList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_PO_TYPE);
        }
        purchaseOrderInformationDomain.setPoTypeList(poTypeList);
        
        // 6. Get information for Period Type Combo box from MiscellaneousService
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_PERIOD_TYPE);
        List<MiscellaneousDomain> periodTyList = miscService.searchMisc(miscDomain);
        if (null == periodTyList || periodTyList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_PERIOD_TYPE);
        }
        purchaseOrderInformationDomain.setPeriodTypeList(periodTyList);
        
        // 7. Get information for ViewPdf Combo box from MiscellaneousService
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_VIEW_PDF);
        List<MiscellaneousDomain> viewPdfList = miscService.searchMisc(miscDomain);
        if (null == viewPdfList || viewPdfList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_VIEW_PDF);
        }
        purchaseOrderInformationDomain.setViewPdfList(viewPdfList);
        
        /*Set default issued date From - To*/
        purchaseOrderInformationDomain = setIssueDate(purchaseOrderInformationDomain);
       
        return purchaseOrderInformationDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#initial(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public PurchaseOrderInformationResultDomain searchPurchaseOrderInformation(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain) throws ApplicationException
    {
        PurchaseOrderInformationResultDomain result = new PurchaseOrderInformationResultDomain();
        List<PurchaseOrderInformationDomain> searchResult = null;
        Locale locale = purchaseOrderInformationDomain.getLocale();
        
        // 1. Validate input parameter
        Set<String> applicationMessageSet = this.validateCriteria(purchaseOrderInformationDomain);
        if (Constants.ZERO < applicationMessageSet.size()) {
            result.setApplicationMessageSet(applicationMessageSet);
            return result;
        }

        /* 2. Count Purchase Order Header return record from PurchaseOrderService to check record
         * limit and display on screen */
        int recordCount = purchaseOrderService.searchCountPurchaseOrderHeader(
            purchaseOrderInformationDomain);
        if (recordCount <= Constants.ZERO) {
            //Data not found;
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001, null);
        }

        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        
        // 3. Get maximum record limit from RecordLimitService
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD002_RLM);
        MiscellaneousDomain recordLimitDomain = recordLimitService.searchRecordLimit(miscDomain);
        if (null == recordLimitDomain) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032, null);
        }
        int recordLimit = Integer.parseInt(recordLimitDomain.getMiscValue());

        // 4. Check If recordCount > recordLimit
        if (recordLimit < recordCount) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0003
                , null, new String[] {String.valueOf(recordLimit)});
        }
        
        // 5. Get maximum record per page from RecordLimitService
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD002_PLM);
        MiscellaneousDomain limitPerPageDomain
            = recordLimitService.searchRecordLimitPerPage(miscDomain);
        if (null == limitPerPageDomain) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032, null);
        }
        int limitPerPage = Integer.parseInt(limitPerPageDomain.getMiscValue());
        
        // 6. Calculate rownum for query
        purchaseOrderInformationDomain.setMaxRowPerPage(limitPerPage);
        SpsPagingUtil.calcPaging(purchaseOrderInformationDomain, recordCount);
        
        // 7. Get Purchase Order data to display at screen from PurchaseOrderService
        searchResult = purchaseOrderService.searchPurchaseOrderHeader(
            purchaseOrderInformationDomain);
        
        // 8. Set <List> poInformation to return Domain
        if (Constants.ZERO < searchResult.size()) {
            for (PurchaseOrderInformationDomain POInfo : searchResult) {
                if (Constants.STR_ZERO.equals(POInfo.getSpsTPoDomain().getPeriodType()))
                    //&& Constants.STR_ZERO.equals(POInfo.getSpsTPoDomain().getChangeFlg()))
                {
                    POInfo.setPeriodTypeName(SupplierPortalConstant.FIRM);
                } else {
                    POInfo.setPeriodTypeName(SupplierPortalConstant.FORECAST);
                }

                if (Strings.judgeBlank(POInfo.getSpsTPoDomain().getPdfChangeFileId())) {
                    POInfo.setPdfChange(Constants.STR_ZERO);
                } else {
                    POInfo.setPdfChange(Constants.STR_ONE);
                }
                if (Strings.judgeBlank(POInfo.getSpsTPoDomain().getPdfOriginalFileId())) {
                    POInfo.setPdfOriginal(Constants.STR_ZERO);
                } else {
                    POInfo.setPdfOriginal(Constants.STR_ONE);
                }
                
                if (Constants.STR_ZERO.equals(POInfo.getSpsTPoDomain().getPeriodType())
                    && Constants.STR_ONE.equals(POInfo.getSpsTPoDomain().getOrderMethod())
                    && Constants.STR_ZERO.equals(POInfo.getSpsTPoDomain().getChangeFlg()))
                {
                    POInfo.setPeriodTypeName(SupplierPortalConstant.FORECAST);
                }
            }
            result.setPurchaseOrderInformationDomainList(searchResult);
        }
        
        return result;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#updateAcknowledgementPo(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer updateAcknowledgementPo(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain) throws ApplicationException
    {
        Locale locale = purchaseOrderInformationDomain.getLocale();
        Timestamp currentDatetime = commonService.searchSysDate();
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setParseBigDecimal(true);
        String updateDscId = purchaseOrderInformationDomain.getLastUpdateDscId();
        Integer count = Constants.ZERO;
        
        List<PurchaseOrderInformationDomain> poInfoList 
            = purchaseOrderInformationDomain.getPoList();
        
        /*Update Acknowledge PO from list of PurchaseOrderInformationDomain.*/
        List<SpsTPoDomain> poValueList = new ArrayList<SpsTPoDomain>();
        List<SpsTPoCriteriaDomain> poCriteriaList = new ArrayList<SpsTPoCriteriaDomain>();
        List<SpsTPoDetailDomain> poDetailValueList = new ArrayList<SpsTPoDetailDomain>();
        List<SpsTPoDetailCriteriaDomain> poDetailCriteriaList
            = new ArrayList<SpsTPoDetailCriteriaDomain>();
        SpsTPoDomain poValueDomain = null;
        SpsTPoCriteriaDomain poCriteriaDomain = null;
        SpsTPoDetailDomain poDetailValue = null;
        SpsTPoDetailCriteriaDomain poDetailCriteria = null;
        for (PurchaseOrderInformationDomain poInfo : poInfoList) {
            if (Strings.judgeBlank(poInfo.getPoIdSelected())
                || Strings.judgeBlank(poInfo.getPoStatusSelected()))
            {
                continue;
            }
            
            // 1. Validate input for update
            if(!poInfo.getPoStatusSelected().equals(SupplierPortalConstant.PO_STATUS_ISSUED)){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0003, null);
            }
            
            BigDecimal poId = null;
            try {
                poId = (BigDecimal)decimalFormat.parse(poInfo.getPoIdSelected());
            } catch (ParseException ex) {
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002, null);
            }
            
            poValueDomain = new SpsTPoDomain();
            poValueDomain.setPoStatus(SupplierPortalConstant.PO_STATUS_ACKNOWLEDGE);
            poValueDomain.setLastUpdateDscId(updateDscId);
            poValueDomain.setLastUpdateDatetime(currentDatetime);
            poValueList.add(poValueDomain);
            
            poCriteriaDomain = new SpsTPoCriteriaDomain();
            poCriteriaDomain.setPoId(poId);
            poCriteriaDomain.setLastUpdateDatetime(
                poInfo.getSpsTPoDomain().getLastUpdateDatetime());
            poCriteriaList.add(poCriteriaDomain);
            
            poDetailValue = new SpsTPoDetailDomain();
            poDetailValue.setPnStatus(SupplierPortalConstant.PO_STATUS_ACKNOWLEDGE);
            poDetailValue.setLastUpdateDscId(updateDscId);
            poDetailValue.setLastUpdateDatetime(currentDatetime);
            poDetailValueList.add(poDetailValue);
            
            poDetailCriteria = new SpsTPoDetailCriteriaDomain();
            poDetailCriteria.setPoId(poId);
            poDetailCriteria.setLastUpdateDatetime(poInfo.getLastUpdateDatetime());
            poDetailCriteriaList.add(poDetailCriteria);
        }
        
        if (Constants.ZERO == poValueList.size() || Constants.ZERO == poCriteriaList.size()
            || Constants.ZERO == poDetailValueList.size() 
            || Constants.ZERO == poDetailCriteriaList.size())
        {
            return Constants.ZERO;
        }
        
        // 2. Update Purchase Order Header from SpsTPoService
        count = this.spsTPoService.updateByCondition(poValueList, poCriteriaList);
        
        // 3. Update all Purchase Order Detail under Purchase Order Header from SpsTPoDetailService
        this.spsTPoDetailService.updateByCondition(poDetailValueList, poDetailCriteriaList);
        
        if (poValueList.size() != count) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0029, null);
        }
        
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchPurchaseOrderInformationCSV(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public PurchaseOrderInformationResultDomain searchPurchaseOrderInformationCsv(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain, CommonDomain commonDomain)
        throws ApplicationException
    {
        PurchaseOrderInformationResultDomain result = new PurchaseOrderInformationResultDomain();
        StringBuffer resultStr = null;
        Locale locale = commonDomain.getLocale();
        
        // 1. Validate input parameter
        Set<String> applicationMessageSet = this.validateCriteria(purchaseOrderInformationDomain);
        if (Constants.ZERO < applicationMessageSet.size()) {
            result.setApplicationMessageSet(applicationMessageSet);
            return result;
        }
        
        try {
            // 2. Count Purchase Order return record from PurchaseOrderService
            int recordCount = Constants.ZERO;
            if (Constants.STR_ZERO.equals(purchaseOrderInformationDomain.getDataType())) {
                recordCount = purchaseOrderService.searchCountPurchaseOrder(
                    purchaseOrderInformationDomain);
            } else if (Constants.STR_ONE.equals(purchaseOrderInformationDomain.getDataType())) {
                recordCount = purchaseOrderService.searchCountChangePurchaseOrder(
                    purchaseOrderInformationDomain);
            } else if (null == purchaseOrderInformationDomain.getDataType()) {
                purchaseOrderInformationDomain.getSpsTPoDomain().setChangeFlg(Constants.STR_ZERO);
                int originalCount = purchaseOrderService.searchCountPurchaseOrder(
                    purchaseOrderInformationDomain);

                purchaseOrderInformationDomain.getSpsTPoDomain().setChangeFlg(Constants.STR_ONE);
                int changeCount = purchaseOrderService.searchCountChangePurchaseOrder(
                    purchaseOrderInformationDomain);
                
                recordCount = originalCount + changeCount;
            }
            
            if (recordCount <= Constants.ZERO) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001, null);
            }

            // 3. Get maximum record limit from RecordLimitService
            MiscellaneousDomain miscDomain = new MiscellaneousDomain();
            miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
            miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD002_RLM);
            MiscellaneousDomain recordLimitDomain
                = recordLimitService.searchRecordLimit(miscDomain);
            if (null == recordLimitDomain) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032, null);
            }
            int recordLimit = Integer.parseInt(recordLimitDomain.getMiscValue());
            
            // 4. Check If recordCount > recordLimit
            if (recordLimit < recordCount) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0020, null
                    , new String[] {String.valueOf(recordLimit)});
            }
            
            // 5. Get Purchase Order data from PurchaseOrderService
            List<PurchaseOrderInformationDomain> resultList
                = new ArrayList<PurchaseOrderInformationDomain>();
            if (Constants.STR_ZERO.equals(purchaseOrderInformationDomain.getDataType())) {
                resultList = purchaseOrderService
                    .searchPurchaseOrder(purchaseOrderInformationDomain);
            } else if (Constants.STR_ONE.equals(purchaseOrderInformationDomain.getDataType())) {
                resultList = purchaseOrderService
                    .searchChangePurchaseOrder(purchaseOrderInformationDomain);
            } else if (null == purchaseOrderInformationDomain.getDataType()) {
                purchaseOrderInformationDomain.getSpsTPoDomain().setChangeFlg(Constants.STR_ZERO);
                resultList.addAll(purchaseOrderService.searchPurchaseOrder(
                    purchaseOrderInformationDomain));
                purchaseOrderInformationDomain.getSpsTPoDomain().setChangeFlg(Constants.STR_ONE);
                resultList.addAll(purchaseOrderService.searchChangePurchaseOrder(
                    purchaseOrderInformationDomain));
            }
            
            if(0 < resultList.size()){
                // 6. Prepare Data by setting from <List> poInformation
                String[] headerArr = new String[] {
                    SupplierPortalConstant.LBL_DATA_TYPE,
                    SupplierPortalConstant.LBL_ISSUE_DATE,
                    SupplierPortalConstant.LBL_S_CD,
                    SupplierPortalConstant.LBL_S_PCD,
                    SupplierPortalConstant.LBL_D_CD,
                    SupplierPortalConstant.LBL_D_PCD,
                    SupplierPortalConstant.LBL_PO_STATUS,
                    SupplierPortalConstant.LBL_PO_TYPE,
                    SupplierPortalConstant.LBL_CURRENT_SPS_PO_NO,
                    SupplierPortalConstant.LBL_CURRENT_CIGMA_PO_NO,
                    SupplierPortalConstant.LBL_RELEASE_NO,
                    SupplierPortalConstant.LBL_TM,
                    SupplierPortalConstant.LBL_PERIOD_TYPE,
                    SupplierPortalConstant.LBL_REPORT_TYPE,
                    SupplierPortalConstant.LBL_ORDER_METHOD,
                    SupplierPortalConstant.LBL_D_PN,
                    SupplierPortalConstant.LBL_ITEM_DESC,
                    SupplierPortalConstant.LBL_S_PN,
                    SupplierPortalConstant.LBL_DUE_DATE,
                    SupplierPortalConstant.LBL_ETD,
                    SupplierPortalConstant.LBL_ORDER_QTY,
                    SupplierPortalConstant.LBL_ORDER_LOT,
                    SupplierPortalConstant.LBL_QTY_BOX,
                    SupplierPortalConstant.LBL_UNIT_OF_MEASURE,
                    SupplierPortalConstant.LBL_UNIT_PRICE,
                    SupplierPortalConstant.LBL_CURRENCY_CODE,
                    SupplierPortalConstant.LBL_TEMP_PRICE,
                    SupplierPortalConstant.LBL_MODEL,
                    SupplierPortalConstant.LBL_PLANNER_CODE,
                    SupplierPortalConstant.LBL_PHASE_CODE,
                    SupplierPortalConstant.LBL_WH_PRIME_RECEIVING,
                    SupplierPortalConstant.LBL_RECEIVING_DOCK,
                    SupplierPortalConstant.LBL_RECEIVING_LANE,
                    SupplierPortalConstant.LBL_PN_STATUS,
                    SupplierPortalConstant.LBL_ROUTE,
                    SupplierPortalConstant.LBL_DEL,
                    SupplierPortalConstant.LBL_PREVIOUS_SPS_PO_NO,
                    SupplierPortalConstant.LBL_ORIGINAL_SPS_PO_NO,
                    SupplierPortalConstant.LBL_PREVIOUS_QTY,
                    SupplierPortalConstant.LBL_DIFFERENCE_QTY,
                    SupplierPortalConstant.LBL_RSN
                };
                
                List<Map<String, Object>> resultDetail = this.doMapPurchaseOrderInfo(
                    resultList, headerArr, purchaseOrderInformationDomain.getDataType());
                
                // 7. Create CSV file and set file to InputStream from CreateFileService
                commonDomain.setResultList(resultDetail);
                commonDomain.setHeaderArr(headerArr);
                commonDomain.setHeaderFlag(true);
                resultStr = commonService.createCsvString(commonDomain);
                result.setCsvBufffer(resultStr);
            } else {
                throw new ApplicationException(Constants.EMPTY_STRING);
            }
        } catch (IOException ex) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        } 
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public FileManagementDomain searchFileName(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException
    {
        FileManagementDomain result = null;
        Locale locale = purchaseOrderInformationDomain.getLocale();
        
        SpsTPoCriteriaDomain criteria = new SpsTPoCriteriaDomain();
        criteria.setPoId(new BigDecimal(purchaseOrderInformationDomain.getFileIdSelected()));
        List<SpsTPoDomain> spsTPoDomain = spsTPoService.searchByCondition(criteria);
        BigDecimal poId = spsTPoDomain.get(Constants.ZERO).getPoId();
        String orderType = spsTPoDomain.get(Constants.ZERO).getPeriodType();
        String spcd = spsTPoDomain.get(Constants.ZERO).getSPcd();
        String dpcd = spsTPoDomain.get(Constants.ZERO).getDPcd();
        String cigmaPoNo = spsTPoDomain.get(Constants.ZERO).getCigmaPoNo();
        
        PurchaseOrderInformationDomain poInfo = new PurchaseOrderInformationDomain();
        poInfo.setPoId( purchaseOrderInformationDomain.getFileIdSelected().toString() );
        SpsTPoDomain poDetail = new SpsTPoDomain();
        poDetail.setPoId( poId );
        
        List<PurchaseOrderCoverPageReportDomain> coverReport = null;
        List<PurchaseOrderReportDomain> poReport = null;
        coverReport = 
            purchaseOrderCoverPageService.searchPoCoverPageReport(poInfo);
        //if( Constants.KPO_TYPE.equals( dataType ) ){
        if( Constants.STR_ONE.equals( orderType ) ){
            poReport = 
                purchaseOrderService.searchPurchaseOrderKanbanReport( poDetail );
        }else {
            poDetail.setSPcd( spcd );
            poDetail.setDPcd( dpcd );
            poDetail.setCigmaPoNo( cigmaPoNo );
            
            poReport = 
                purchaseOrderService.searchPurchaseOrderReport( poDetail );
        }
        try {
            InputStream isReportData = purchaseOrderService.generatePo(locale, coverReport, poReport);
            
            StringBuffer filenameReport = new StringBuffer();
            filenameReport.append(SupplierPortalConstant.PURCHASE_ORDER);
            filenameReport.append(poId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
            
            result = new FileManagementDomain();
            result.setFileData(isReportData);
            result.setFileName(filenameReport.toString());
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(
                locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0013,
                cigmaPoNo );
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchPurchaseOrderReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain, java.io.OutputStream)
     */
    public void searchPurchaseOrderReport(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain
        , OutputStream output) throws ApplicationException
    {
        // 1. Get PDF file from FileManagementService
        try {
            this.fileManagementService.searchFileDownload(
                purchaseOrderInformationDomain.getFileIdSelected(), true, output);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(purchaseOrderInformationDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchChangeMaterialRelease(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain, java.io.OutputStream)
     */
    public void searchChangeMaterialRelease(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain
        , OutputStream output) throws ApplicationException
    {
        // 1. Get PDF file from FileManagementService
        try {
            this.fileManagementService.searchFileDownload(
                purchaseOrderInformationDomain.getFileIdSelected(), true, output);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(purchaseOrderInformationDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService#searchPlantBySelectSupplierCompany(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException
    {
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        List<PlantSupplierDomain> plantSupplierList = 
            new ArrayList<PlantSupplierDomain>();
        DataScopeControlDomain dataScopeControlDomain = plantSupplierWithScopeDomain
            .getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain = plantSupplierWithScopeDomain
            .getPlantSupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        plantSupplierList = plantSupplierService.searchPlantSupplier(plantSupplierWithScopeDomain);
        if (plantSupplierList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchCompanyBySelectSupplierPlant(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope) throws ApplicationException
    {
        Locale locale = plantSupplierWithScope.getLocale();
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();

        DataScopeControlDomain dataScopeControlDomain = plantSupplierWithScope
            .getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain = plantSupplierWithScope.getPlantSupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if (result.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchPlantBySelectDensoCompany(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException
    {
        Locale locale = plantDensoWithScopeDomain.getLocale();
        List<PlantDensoDomain> plantDensoList = null;

        DataScopeControlDomain dataScopeControlDomain = plantDensoWithScopeDomain
            .getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScopeDomain.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if (plantDensoList.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchCompanyBySelectDensoPlant(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();

        DataScopeControlDomain dataScopeControlDomain = plantDensoWithScope
            .getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if (result.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain, java.io.OutputStream)
     */
    public void searchLegendInfo(PurchaseOrderInformationDomain purchaseOrderInformationDomain
        , OutputStream output) throws ApplicationException
    {
        // 1. Get PDF file from FileManagementService
        try {
            this.fileManagementService.searchFileDownload(
                purchaseOrderInformationDomain.getFileIdSelected(), true, output);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(purchaseOrderInformationDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * <p>Method for set default Issued Date From - To,
     * then display on screen.</p>
     *
     * @param purchaseOrderInformationDomain the Purchase Order Domain.
     * @return PurchaseOrderInformationDomain the Purchase Order Domain.
     */
    private PurchaseOrderInformationDomain setIssueDate(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        Timestamp currentTimestamp = commonService.searchSysDate();

        Date currentDate = new Date(currentTimestamp.getTime());

        Calendar prevDate = Calendar.getInstance();
        prevDate.setTime(currentTimestamp);
        prevDate.add(Calendar.DATE, Constants.MINUS_ONE);

        purchaseOrderInformationDomain.setIssuedDateTo(DateUtil.format(currentDate,
            DateUtil.PATTERN_YYYYMMDD_SLASH));
        purchaseOrderInformationDomain.setIssuedDateFrom(DateUtil.format(prevDate.getTime(),
            DateUtil.PATTERN_YYYYMMDD_SLASH));

        return purchaseOrderInformationDomain;
    }
    
    /**
     * Validate searc criteria.
     * @param purchaseOrderInformationDomain criteria domain
     * @return list of error message
     * */
    private Set<String> validateCriteria(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        Locale locale = purchaseOrderInformationDomain.getLocale();
        
        if (Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getVendorCd())
            || Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getSPcd())
            || Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getDCd())
            || Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getDPcd())
            || Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getPoStatus())
            || Strings.judgeBlank(purchaseOrderInformationDomain.getIssuedDateFrom())
            || Strings.judgeBlank(purchaseOrderInformationDomain.getIssuedDateTo()))
        {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0011));
        } else {
            
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getVendorCd()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setVendorCd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getSPcd()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setSPcd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getDCd()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setDCd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getDPcd()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setDPcd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getPoStatus()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setPoStatus(null);
            }
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getPoType()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setPoType(null);
            }
            if (Constants.MISC_CODE_ALL.equals(
                purchaseOrderInformationDomain.getSpsTPoDomain().getPeriodType()))
            {
                purchaseOrderInformationDomain.getSpsTPoDomain().setPeriodType(null);
            }
            if (Constants.MISC_CODE_ALL.equals(purchaseOrderInformationDomain.getDataType())) {
                purchaseOrderInformationDomain.setDataType(null);
            }
            
            String dataType = purchaseOrderInformationDomain.getDataType();
            if (Strings.judgeBlank(dataType)) {
                purchaseOrderInformationDomain.setViewPdfFlag(null);
                purchaseOrderInformationDomain.getSpsTPoDomain().setChangeFlg(null);
            } else {
                purchaseOrderInformationDomain.setViewPdfFlag(dataType);
                purchaseOrderInformationDomain.getSpsTPoDomain().setChangeFlg(dataType);
            }
        }
        
        if (!Strings.judgeBlank(purchaseOrderInformationDomain.getIssuedDateFrom())) {
            if (!purchaseOrderInformationDomain.getIssuedDateFrom().matches(
                Constants.REGX_DATEFORMAT))
            {
                applicationMessageSet.add(MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ISSUE_DATE_FROM));
            }
        }
        
        if (!Strings.judgeBlank(purchaseOrderInformationDomain.getIssuedDateTo())) {
            if (!purchaseOrderInformationDomain.getIssuedDateTo().matches(
                Constants.REGX_DATEFORMAT))
            {
                applicationMessageSet.add(MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ISSUE_DATE_TO));
            }
        }
        
        if (!Strings.judgeBlank(purchaseOrderInformationDomain.getIssuedDateFrom())
            && !Strings.judgeBlank(purchaseOrderInformationDomain.getIssuedDateTo()))
        {
            if (Constants.ZERO < DateUtil.compareDate(
                purchaseOrderInformationDomain.getIssuedDateFrom(),
                    purchaseOrderInformationDomain.getIssuedDateTo()))
            {
                String dateFromLabel = MessageUtil.getLabelHandledException(locale,
                    SupplierPortalConstant.LBL_ISSUE_DATE_FROM);
                String dateToLabel = MessageUtil.getLabelHandledException(locale,
                    SupplierPortalConstant.LBL_ISSUE_DATE_FROM);
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                    new String[] {dateFromLabel, dateToLabel}));
            }
        }
        
        if (!Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getSpsPoNo())) {
            if (SupplierPortalConstant.LENGTH_SPS_PO_NO < purchaseOrderInformationDomain
                .getSpsTPoDomain().getSpsPoNo().length())
            {
                String spsPoLabel = MessageUtil.getLabelHandledException(locale,
                    SupplierPortalConstant.LBL_SPS_PO_NO);
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                    new String[] {spsPoLabel, String.valueOf(
                        SupplierPortalConstant.LENGTH_SPS_PO_NO)}));
            } else {
                purchaseOrderInformationDomain.getSpsTPoDomain().setSpsPoNo(
                    purchaseOrderInformationDomain.getSpsTPoDomain().getSpsPoNo().trim());
            }
        }
        
        if (!Strings.judgeBlank(purchaseOrderInformationDomain.getSpsTPoDomain().getCigmaPoNo())) {
            if (SupplierPortalConstant.LENGTH_CIGMA_PO_NO < purchaseOrderInformationDomain
                .getSpsTPoDomain().getCigmaPoNo().length())
            {
                String spsPoLabel = MessageUtil.getLabelHandledException(locale,
                    SupplierPortalConstant.LBL_CIGMA_PO_NO);
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                    new String[] {spsPoLabel, String.valueOf(
                        SupplierPortalConstant.LENGTH_CIGMA_PO_NO)}));
            } else {
                purchaseOrderInformationDomain.getSpsTPoDomain().setCigmaPoNo(
                    purchaseOrderInformationDomain.getSpsTPoDomain().getCigmaPoNo().trim());
            }
        }
        
        return applicationMessageSet;
    }

    /**
     * Do Map Purchase Order Information.
     * 
     * @param poInfoList the list of Purchase Order Information Domain.
     * @param header the List
     * @param viewPdf original or change
     * @return resultList 
     */
    private List<Map<String, Object>> doMapPurchaseOrderInfo(
        List<PurchaseOrderInformationDomain> poInfoList, String[] header, String viewPdf)
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> poInfoMap = null;
        SpsTPoDomain po = null;
        SpsTPoDetailDomain poDetail = null;
        SpsTPoDueDomain poDue = null;
        String itemDesc = null;
        for(PurchaseOrderInformationDomain poInfo : poInfoList){
            po = poInfo.getSpsTPoDomain();
            
            for (PurchaseOrderDetailDomain poInfoDetail : poInfo.getPurchaseOrderDetailList()) {
                poDetail = poInfoDetail.getSpsTPoDetailDomain();
                poDue = poInfoDetail.getSpsTPoDueDomain();
                
                poInfoMap = new HashMap<String, Object>();
                if (Constants.STR_ONE.equals(po.getChangeFlg())) {
                    poInfoMap.put(SupplierPortalConstant.LBL_DATA_TYPE, Constants.WORD_CHANGE);
                } else {
                    poInfoMap.put(SupplierPortalConstant.LBL_DATA_TYPE, Constants.WORD_ORIGINAL);
                }
                
                itemDesc = Constants.EMPTY_STRING;
                if(!StringUtil.checkNullOrEmpty(poDetail.getItemDesc())){
                    itemDesc = this.addDoubleQuote(poDetail.getItemDesc());
                }
                
                poInfoMap.put(SupplierPortalConstant.LBL_ISSUE_DATE,
                    StringUtil.checkNullToEmpty(poInfo.getStringPoIssueDate()));
                poInfoMap.put(SupplierPortalConstant.LBL_S_CD,
                    StringUtil.checkNullToEmpty(po.getVendorCd()));
                poInfoMap.put(SupplierPortalConstant.LBL_S_PCD,
                    StringUtil.checkNullToEmpty(po.getSPcd()));
                poInfoMap.put(SupplierPortalConstant.LBL_D_CD,
                    StringUtil.checkNullToEmpty(po.getDCd()));
                poInfoMap.put(SupplierPortalConstant.LBL_D_PCD,
                    StringUtil.checkNullToEmpty(po.getDPcd()));
                poInfoMap.put(SupplierPortalConstant.LBL_PO_STATUS,
                    StringUtil.checkNullToEmpty(po.getPoStatus()));
                poInfoMap.put(SupplierPortalConstant.LBL_PO_TYPE,
                    StringUtil.checkNullToEmpty(po.getPoType()));
                poInfoMap.put(SupplierPortalConstant.LBL_CURRENT_SPS_PO_NO,
                    StringUtil.checkNullToEmpty(po.getSpsPoNo()));
                poInfoMap.put(SupplierPortalConstant.LBL_CURRENT_CIGMA_PO_NO,
                    StringUtil.checkNullToEmpty(po.getCigmaPoNo()));
                poInfoMap.put(SupplierPortalConstant.LBL_RELEASE_NO,
                    StringUtil.checkNullToEmpty(poInfo.getStringReleaseNo()));
                poInfoMap.put(SupplierPortalConstant.LBL_TM,
                    StringUtil.checkNullToEmpty(poInfo.getTm()));
                poInfoMap.put(SupplierPortalConstant.LBL_PERIOD_TYPE,
                    poInfo.getPeriodTypeName());
                poInfoMap.put(SupplierPortalConstant.LBL_REPORT_TYPE, StringUtil.nullToEmpty(
                    SupplierPortalConstant.PO_DWM.get(poDue.getReportTypeFlg())));
                poInfoMap.put(SupplierPortalConstant.LBL_ORDER_METHOD,
                    StringUtil.checkNullToEmpty(po.getOrderMethod()));
                poInfoMap.put(SupplierPortalConstant.LBL_D_PN,
                    StringUtil.checkNullToEmpty(poDetail.getDPn()));
                poInfoMap.put(SupplierPortalConstant.LBL_ITEM_DESC, itemDesc);
                poInfoMap.put(SupplierPortalConstant.LBL_S_PN,
                    StringUtil.checkNullToEmpty(poDetail.getSPn()));
                poInfoMap.put(SupplierPortalConstant.LBL_DUE_DATE,
                    StringUtil.checkNullToEmpty(poInfoDetail.getStringDueDate()));
                poInfoMap.put(SupplierPortalConstant.LBL_ETD,
                    StringUtil.checkNullToEmpty(poInfoDetail.getStringEtd()));
                poInfoMap.put(SupplierPortalConstant.LBL_ORDER_QTY,
                    this.addDoubleQuote(StringUtil.nullToEmpty(poInfoDetail.getStringOrderQty())));
                poInfoMap.put(SupplierPortalConstant.LBL_ORDER_LOT,
                    this.addDoubleQuote(StringUtil.nullToEmpty(poInfoDetail.getStringOrderLot())));
                poInfoMap.put(SupplierPortalConstant.LBL_QTY_BOX,
                    this.addDoubleQuote(StringUtil.nullToEmpty(poInfoDetail.getStringQtyBox())));
                poInfoMap.put(SupplierPortalConstant.LBL_UNIT_OF_MEASURE,
                    StringUtil.checkNullToEmpty(poDetail.getUnitOfMeasure()));
                poInfoMap.put(SupplierPortalConstant.LBL_UNIT_PRICE,
                    this.addDoubleQuote(StringUtil.nullToEmpty(poInfoDetail.getStringUnitPrice())));
                poInfoMap.put(SupplierPortalConstant.LBL_CURRENCY_CODE,
                    StringUtil.checkNullToEmpty(poDetail.getCurrencyCode()));
                poInfoMap.put(SupplierPortalConstant.LBL_TEMP_PRICE,
                    StringUtil.checkNullToEmpty(poDetail.getTmpPriceFlg()));
                poInfoMap.put(SupplierPortalConstant.LBL_MODEL,
                    StringUtil.checkNullToEmpty(poDetail.getModel()));
                poInfoMap.put(SupplierPortalConstant.LBL_PLANNER_CODE,
                    StringUtil.checkNullToEmpty(poDetail.getPlannerCode()));
                poInfoMap.put(SupplierPortalConstant.LBL_PHASE_CODE,
                    StringUtil.checkNullToEmpty(poDetail.getPhaseCode()));
                poInfoMap.put(SupplierPortalConstant.LBL_WH_PRIME_RECEIVING,
                    StringUtil.checkNullToEmpty(poDetail.getWhPrimeReceiving()));
                poInfoMap.put(SupplierPortalConstant.LBL_RECEIVING_DOCK,
                    StringUtil.checkNullToEmpty(poDetail.getReceivingDock()));
                poInfoMap.put(SupplierPortalConstant.LBL_RECEIVING_LANE,
                    StringUtil.checkNullToEmpty(poDetail.getRcvLane()));
                poInfoMap.put(SupplierPortalConstant.LBL_PN_STATUS,
                    StringUtil.checkNullToEmpty(poDetail.getPnStatus()));
                poInfoMap.put(SupplierPortalConstant.LBL_ROUTE,
                    StringUtil.checkNullToEmpty(poDue.getDel()));
                poInfoMap.put(SupplierPortalConstant.LBL_DEL,
                    StringUtil.checkNullToEmpty(poDue.getSeq().toString()));
                
                if (Constants.STR_ONE.equals(po.getChangeFlg())) {
                    poInfoMap.put(SupplierPortalConstant.LBL_PREVIOUS_SPS_PO_NO,
                        StringUtil.nullToEmpty(poInfo.getPreviousSpsPoNo()));
                    poInfoMap.put(SupplierPortalConstant.LBL_ORIGINAL_SPS_PO_NO,
                        StringUtil.nullToEmpty(poInfo.getOriginalSpsPoNo()));
                    poInfoMap.put(SupplierPortalConstant.LBL_PREVIOUS_QTY, this.addDoubleQuote(
                        StringUtil.nullToEmpty(poInfoDetail.getStringPreviousQty())));
                    poInfoMap.put(SupplierPortalConstant.LBL_DIFFERENCE_QTY, this.addDoubleQuote(
                        StringUtil.nullToEmpty(poInfoDetail.getStringDifferenceQty())));
                    poInfoMap.put(SupplierPortalConstant.LBL_RSN,
                        StringUtil.nullToEmpty(poDue.getRsn()));
                } else {
                    poInfoMap.put(SupplierPortalConstant.LBL_PREVIOUS_SPS_PO_NO,
                        Constants.EMPTY_STRING);
                    poInfoMap.put(SupplierPortalConstant.LBL_ORIGINAL_SPS_PO_NO,
                        Constants.EMPTY_STRING);
                    poInfoMap.put(SupplierPortalConstant.LBL_PREVIOUS_QTY,
                        Constants.EMPTY_STRING);
                    poInfoMap.put(SupplierPortalConstant.LBL_DIFFERENCE_QTY,
                        Constants.EMPTY_STRING);
                    poInfoMap.put(SupplierPortalConstant.LBL_RSN,
                        Constants.EMPTY_STRING);
                }
                
                resultList.add(poInfoMap);
            }
        }
        return resultList;
    }
    
    /**
     * Add double quote at front and back of target string
     * @param target to add double quote
     * @return string that already add double quote
     * */
    private String addDoubleQuote(String target) {
        StringBuffer result = new StringBuffer();
        
        result.append(Constants.SYMBOL_DOUBLE_QUOTE).append(target)
            .append(Constants.SYMBOL_DOUBLE_QUOTE);
        
        return result.toString();
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#generateOriginalPo(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public FileManagementDomain generateOriginalPo(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException
    {
        FileManagementDomain result = null;
        Locale locale = purchaseOrderInformationDomain.getLocale();
        
        SpsTPoCriteriaDomain criteria = new SpsTPoCriteriaDomain();
        criteria.setPoId(new BigDecimal(purchaseOrderInformationDomain.getFileIdSelected()));
        List<SpsTPoDomain> spsTPoDomain = spsTPoService.searchByCondition(criteria);
        BigDecimal poId = spsTPoDomain.get(Constants.ZERO).getPoId();
        String orderType = spsTPoDomain.get(Constants.ZERO).getPeriodType();
        String spcd = spsTPoDomain.get(Constants.ZERO).getSPcd();
        String dpcd = spsTPoDomain.get(Constants.ZERO).getDPcd();
        String cigmaPoNo = spsTPoDomain.get(Constants.ZERO).getCigmaPoNo();
        
        PurchaseOrderInformationDomain poInfo = new PurchaseOrderInformationDomain();
        poInfo.setPoId( poId.toString() );
        SpsTPoDomain poDetail = new SpsTPoDomain();
        poDetail.setPoId( poId );
        
        List<PurchaseOrderCoverPageReportDomain> coverReport = null;
        List<PurchaseOrderReportDomain> poReport = null;
        coverReport = 
            purchaseOrderCoverPageService.searchPoCoverPageReport(poInfo);
        //if( Constants.KPO_TYPE.equals( dataType ) ){
        if( Constants.STR_ONE.equals( orderType ) ){
            poReport = 
                purchaseOrderService.searchPurchaseOrderKanbanReport( poDetail );
        }else {
            poDetail.setSPcd( spcd );
            poDetail.setDPcd( dpcd );
            poDetail.setCigmaPoNo( cigmaPoNo );
            
            poReport = 
                purchaseOrderService.searchPurchaseOrderReport( poDetail );
        }
        try {
            InputStream isReportData = purchaseOrderService.generatePo(locale, coverReport, poReport);
            
            StringBuffer filenameReport = new StringBuffer();
            filenameReport.append(SupplierPortalConstant.PURCHASE_ORDER);
            filenameReport.append(poId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
            
            result = new FileManagementDomain();
            result.setFileData(isReportData);
            result.setFileName(filenameReport.toString());
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(
                locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0013,
                cigmaPoNo );
        }
        
        return result;
    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService#generateChangePo(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public FileManagementDomain generateChangePo(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
        throws ApplicationException
    {
        FileManagementDomain result = null;
        InputStream isReportData = null;
        Locale locale = purchaseOrderInformationDomain.getLocale();
        
        SpsTPoCriteriaDomain criteria = new SpsTPoCriteriaDomain();
        criteria.setPoId(new BigDecimal(purchaseOrderInformationDomain.getFileIdSelected()));
        List<SpsTPoDomain> spsTPoDomain = spsTPoService.searchByCondition(criteria);
        BigDecimal poId = spsTPoDomain.get(Constants.ZERO).getPoId();
        String cigmaPoNo = spsTPoDomain.get(Constants.ZERO).getCigmaPoNo();
        
        PurchaseOrderInformationDomain poInfo = new PurchaseOrderInformationDomain();
        poInfo.setPoId( poId.toString() );
        StringBuffer filenameReport = new StringBuffer();
        List<ChangeMaterialReleaseReportDomain> cmrReport 
            = purchaseOrderService.searchChangeMaterialReleaseReport(poInfo);

        try {
            isReportData = purchaseOrderService.generateChgPo(locale, cmrReport);
            
            filenameReport.append(SupplierPortalConstant.CHANGE_PURCHASE_ORDER);
            filenameReport.append(poId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
            
            result = new FileManagementDomain();
            result.setFileData(isReportData);
            result.setFileName(filenameReport.toString());
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(
                locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0013,
                cigmaPoNo );
        }
        
        return result;
    }
    
}