/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationReturnDomain;

/**
 * <p>
 * The Interface PriceDifferenceInformationFacadeService.
 * </p>
 * <p>
 * Facade for PriceDifferenceInformationFacadeService.
 * </p>
 * <ul>
 * <li>Method search : searchPriceDifferenceInformation</li>
 * <li>Method create : downloadInvoiceInformation</li>
 * </ul>
 * 
 * @author CSI
 */
public interface PriceDifferenceInformationFacadeService {

    /**
     * <p>
     * Download invoice information.
     * </p>
     * <ul>
     * <li>Call initial method of PriceDifferenceInformationFacadeService class in order to 
     * price difference information data into database for preparation data 
     * for export CSV File.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain
     * @param plantSupplierWithScopeDomain the plantSupplierWithScope Domain
     * @param priceDifferenceInformationDomain priceDifferenceInformationDomain
     * @return the string.
     * @throws ApplicationException ApplicationException
     */
    public PriceDifferenceInformationDomain searchInitial(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain,
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search price difference information detail.
     * </p>
     * <ul>
     * <li>Search price difference information item detail for display on Price
     * Difference Information Screen.</li>
     * </ul>
     * 
     * @param priceDifferenceInformationDomain the price difference information domain
     * @return the list of invoice information domain.
     * @throws ApplicationException ApplicationException
     */
    public PriceDifferenceInformationReturnDomain searchPriceDifferenceInformation(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search price difference information CSV.
     * </p>
     * <ul>
     * <li>Search price difference information detail of CSV file for export CSV File.</li>
     * </ul>
     * 
     * @param priceDifferenceInformationDomain the price difference information domain
     * @return the list of price different information domain.
     * @throws ApplicationException ApplicationException
     */
    public PriceDifferenceInformationReturnDomain searchPriceDifferenceInformationCsv(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search selected company supplier.
     * </p>
     * <ul>
     * <li>Search selected company supplier for display on supplier plant code combo box of Price
     * Difference Information Screen.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain the  plant supplier with scope domain
     * @return the list of SPS miscellaneous domain 
     * @throws ApplicationException ApplicationException
     */
    public List<SpsMMiscDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScope PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
        throws ApplicationException;

    /**
     * <p>
     * Search selected company DENSO.
     * </p>
     * <ul>
     * <li>Search selected company DENSO for display on DENSO plant code combo box of Price
     * Difference Information Screen.</li>
     * </ul>
     * @param plantDensoWithScopeDomain the  plant DENSO with scope domain
     * @return the list of SPS miscellaneous domain 
     * @throws ApplicationException ApplicationException
     */
    public List<SpsMMiscDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search file name.
     * </p>
     * <ul>
     * <li>Search file name for for export CSV File.</li>
     * </ul>
     * 
     * @param priceDifferenceInformationDomain the price difference information domain
     * @return the string.
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException;

    /**
     * <p>
     * Download invoice information.
     * </p>
     * <ul>
     * <li>Preparation data for export CSV File.</li>
     * </ul>
     * 
     * @param commonDomain the common domain
     * @return the string.
     * @throws ApplicationException ApplicationException
     */
    public StringBuffer downloadInvoiceInformation(CommonDomain commonDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search legend info
     * </p>
     * <ul>
     * <li>Call search file download method of PriceDifferenceInformationFacadeService 
     * for get output stream</li>
     * </ul>
     * 
     * @param priceDifferenceInformationDomain the price difference information domain
     * @param output the output stream
     * @return output stream.
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchLegendInfo(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}