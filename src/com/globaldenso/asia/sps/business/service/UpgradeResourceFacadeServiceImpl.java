/*
 * ModifyDate Development company     Describe 
 * 2017/09/26 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.dao.UpgradeDao;
import com.globaldenso.asia.sps.business.domain.BhtUpgradeDomain;

/**
 * <p>The Interface UResourceServices.</p>
 * <p>Service for Upgrade Resource about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchBhtUpgrade</li>
 * </ul>
 *
 * @author Netband
 */
public class UpgradeResourceFacadeServiceImpl implements UpgradeResourceFacadeService {
    
    /** The upgradeDao. */
    private UpgradeDao upgradeDao;

    /** The default constructor. */
    public UpgradeResourceFacadeServiceImpl() {
        super();
    }
    
    /**
     * Sets the upgradeDao.
     * 
     * @param upgradeDao the upgradeDao.
     */
    public void setUpgradeDao(UpgradeDao upgradeDao) {
        this.upgradeDao = upgradeDao;
    }
    /**
     * <p>searchBhtUpgrade.</p>
     * 
     * @param bhtUpgradeDomain the BhtUpgradeDomain 
     * @return the bhtUpgradeDomain
     * @throws ApplicationException an ApplicationException
     */
    public BhtUpgradeDomain searchBhtUpgrade(BhtUpgradeDomain bhtUpgradeDomain)
        throws ApplicationException {
        BhtUpgradeDomain bhtUpgradeDomainResult = upgradeDao.searchBhtUpgrade(bhtUpgradeDomain);
        return bhtUpgradeDomainResult;
    }

}
