/*
 * ModifyDate Development company     Describe 
 * 2014/08/16 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferOneWayKanbanDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;

/**
 * <p>The class TransferDoDataFromCigmaFacadeServiceImpl.</p>
 * <p>Facade for TransferDoDataFromCigmaFacadeServiceImpl.</p>
 * <ul>
 * </ul>
 *
 * @author CSI
 */
public interface TransferDoDataFromCigmaFacadeService {
    /**
     * 
     * <p>Search As400 List.</p>
     *
     * @param criteria company Denso
     * @return List of AS400 Server
     * @throws Exception Exception
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList
        (SpsMCompanyDensoDomain criteria) throws Exception ;

    /**
     * 
     * <p>Search Delivery Order.</p>
     *
     * @param locale Locale
     * @param as400Server As400
     * @param spsFlag flag
     * @return Map List of TransferDoDataFromCigmaDomain
     * @throws Exception Exception
     */
    public Map<String, List<TransferDoDataFromCigmaDomain>> searchDeliveryOrder(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag) throws Exception;
    
    /**
     * 
     * <p>Separate CIGMA D/O data which have error in previous batch running to SPS system.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferDoDataFromCigmaDomain
     * @param as400Server As400ServerConnectionInformationDomain
     * @param bError boolean
     * @param jobId jobId
     * @param log log
     * @return List TransferDoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferDoDataFromCigmaDomain> transactCheckTransferDoFromCigma(
        Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        boolean bError, 
        String jobId,
        Log log
    ) throws Exception;
    
    
    /**
     * 
     * <p>Transaction Transfer Error DO From CIGMA by Do No.
     *    Separate CIGMA D/O data
     * </p>
     *
     * @param locale Locale
     * @param transferList List of TransferDoDataFromCigmaDomain
     * @param as400Server SpsMAs400SchemaDomain
     * @param jobId String
     * @throws Exception Exception
     */
    public void transactTransferErrorDoFromCigma(Locale locale,  
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception ;
    
    /**
     * 
     * <p>Transaction Transfer DO From CIGMA by Do No.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferDoDataFromCigmaDomain
     * @param as400Server SpsMAs400SchemaDomain
     * @param jobId String
     * @throws Exception Exception
     */
    public void transactTransferDoFromCigma(Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception;
    
    /**
     * 
     * <p>Search Change Delivery Order.</p>
     *
     * @param locale Locale
     * @param server serverConfig
     * @param spsFlag flag
     * @return List of TransferChangeDoDataFromCigmaDomain
     * @throws Exception Exception
     */
    public Map<String, List<TransferChangeDoDataFromCigmaDomain>> searchChangeDeliveryOrder(
        Locale locale, 
        As400ServerConnectionInformationDomain server, 
        String spsFlag) throws Exception;
    
    /**
     * 
     * <p>Check Transfer Change Do From Cigma.</p>
     *
     * @param locale locale
     * @param transferList List of TransferChangeDoDataFromCigmaDomain
     * @param as400Server serverConfig
     * @param originalDoErrorSet Set of Original D/O number Error.
     * @param bError error
     * @param jobId jobId
     * @param log log
     * @return List of TransferChangeDoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferChangeDoDataFromCigmaDomain> transactCheckTransferChangeDoFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server,
        Set<String> originalDoErrorSet,
        boolean bError, 
        String jobId,
        Log log) throws Exception;
    
    /**
     * 
     * <p>Transfer Error Chg D/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferChangeDoDataFromCigmaDomain
     * @param as400Server serverConfig
     * @param jobId jobId
     * @return String
     * @throws Exception Exception
     */
    public String transactTransferErrorChgDoFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception;
    
    /**
     * 
     * <p>Transfer Chg D/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferChangeDoDataFromCigmaDomain
     * @param as400Server serverConfig
     * @param jobId jobId
     * @return String
     * @throws Exception Exception
     */
    public String transactTransferChgDoFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception;
    
    /**
     * 
     * <p>Search Record Limit.</p>
     *
     * @return MiscellaneousDomain
     * @throws ApplicationException Exception
     */
    public MiscellaneousDomain searchRecordLimit() throws ApplicationException;
    
    /**
     * 
     * <p>search Cigma D/O Error.</p>
     * @param criteria criteria for search.
     * @return List of SpsCigmaDoErrorDomain
     * @throws ApplicationException Exception
     */
    public List<TransferDoErrorEmailDomain> searchCigmaDoError(SpsCigmaDoErrorDomain criteria)
        throws ApplicationException;
    
    /**
     * 
     * <p>Search Cigma Chg D/O Error.</p>
     *
     * @param criteria criteria for search. 
     * @return List of Change D/O Error
     * @throws ApplicationException Exception
     */
    public List<TransferChgDoErrorEmailDomain> searchCigmaChgDoError(
        SpsCigmaChgDoErrorDomain criteria) throws ApplicationException;
    
    /**
     * 
     * <p>Send Notification Abnormal Data D/O.</p>
     *
     * @param locale Locale
     * @param sendDoErrorToSupplier List of D/O Error to send to Supplier
     * @param sendDoErrorToDenso List of D/O Error to send to DENSO
     * @param sendDoErrorToAdmin List of D/O Error to send to Admin
     * @param sendChgDoErrorToSupplier List of change D/O Error to send to Supplier
     * @param sendChgDoErrorToDenso List of change D/O Error to send to DENSO
     * @param sendChgDoErrorToAdmin List of change D/O Error to send to Admin
     * @param limit limit
     * @param log Logger for output log
     * @return List of String
     * @throws Exception Exception
     */
    public List<String> transactSendNotificationAbnormalDataDo(Locale locale, 
        List<TransferDoErrorEmailDomain> sendDoErrorToSupplier,
        List<TransferDoErrorEmailDomain> sendDoErrorToDenso,
        List<TransferDoErrorEmailDomain> sendDoErrorToAdmin,
        List<TransferChgDoErrorEmailDomain> sendChgDoErrorToSupplier,
        List<TransferChgDoErrorEmailDomain> sendChgDoErrorToDenso,
        List<TransferChgDoErrorEmailDomain> sendChgDoErrorToAdmin,
        List<TransferDoErrorEmailDomain> sendDoBackOrderErrorToAdmin,
        List<TransferChgDoErrorEmailDomain> sendChgDoBackOrderErrorToAdmin
        , int limit, Log log) throws Exception;
    
    /**
     * 
     * <p>Search Urgent Record Limit.</p>
     *
     * @return MiscellaneousDomain
     * @throws ApplicationException Exception
     */
    public MiscellaneousDomain searchUrgentRecordLimit() throws ApplicationException;
    
    /**
     * 
     * <p>Search Urgent Order.</p>
     *
     * @param criteria criteria
     * @return List of urgent mail
     * @throws ApplicationException Exception
     */
    public List<DeliveryOrderDomain> searchUrgentOrder(UrgentOrderDomain criteria) 
        throws ApplicationException;
    
    /**
     * 
     * <p>Send mail Notification.</p>
     *
     * @param locale Locale
     * @param sendUrgentMail List of urgent mail
     * @param limit int limit
     * @param log log
     * @throws Exception Exception
     */
    public void transactSendNotificationChangeOrder(Locale locale
        , List<DeliveryOrderDomain> sendUrgentMail, int limit, Log log) 
        throws Exception;

    /**
     * 
     * <p>Search Notify Delivery Order ToSupplier.</p>
     *
     * @param spsTDoDomain SpsTDoDomain
     * @return List of SpsTDoDomain
     * @throws ApplicationException Exception
     */
    public List<SpsTDoDomain> searchNotifyDeliveryOrderToSupplier(SpsTDoDomain spsTDoDomain) 
        throws ApplicationException;
    
    /**
     * 
     * <p>Send mail Notification.</p>
     *
     * @param locale Locale
     * @param spsTDoDomainList List of SpsTDoDomain
     * @param limit int limit
     * @param log log
     * @param spsTDoDomainCritiria in SpsTDoDomain
     * @throws Exception Exception
     */
    public void transactNotifyDeliveryOrderToSupplier(Locale locale
        , List<SpsTDoDomain> spsTDoDomainList, int limit, Log log, SpsTDoDomain spsTDoDomainCritiria) 
        throws Exception;
    /**
     * 
     * <p>Search Vendor Code and Original CIGMA D/O Number.</p>
     *
     * @param locale Locale
     * @param as400Server As400
     * @param spsFlag flag
     * @return Map List of TransferDoDataFromCigmaDomain
     * @throws Exception Exception
     */
    public Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> searchOnewayKanbanTagSeparateByVendorCode(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag) throws Exception;
    /**
     * 
     * <p>Search One-way Kanban Tag.</p>
     *
     * @param locale Locale
     * @param as400Server As400
     * @param spsFlag flag
     * @param originalCigmaDoNumber originalCigmaDoNumber
     * @param vendorCode vendorCode
     * @return Map List of TransferDoDataFromCigmaDomain
     * @throws Exception Exception
     */
    public Map<String, List<TransferOneWayKanbanDataFromCigmaDomain>> searchOnewayKanbanTag(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag, String originalCigmaDoNumber, String vendorCode) throws Exception;
    
    /**
     * 
     * <p>Separate CIGMA D/O data which have error in previous batch running to SPS system.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferOneWayKanbanDataFromCigmaDomain
     * @param as400Server As400ServerConnectionInformationDomain
     * @param originalDoErrorSet is set of string
     * @param bError boolean
     * @param jobId jobId
     * @param log log
     * @return List TransferDoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferOneWayKanbanDataFromCigmaDomain> transactCheckTransferOneWayKanbanFromCigma(
        Locale locale, 
        List<TransferOneWayKanbanDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        Set<String> originalDoErrorSet,
        boolean bError, 
        String jobId,
        Log log
    ) throws Exception;
    
    /**
     * 
     * <p>transact Transfer One Way Kanban From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferOneWayKanbanDataFromCigmaDomain
     * @param as400Server serverConfig
     * @param jobId jobId
     * @return String
     * @throws Exception Exception
     */
    public String transactTransferOneWayKanbanFromCigma(
        Locale locale, 
        List<TransferOneWayKanbanDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId,
        List<String> listDoIdUpdate) throws Exception;
    
    /**
     * 
     * <p>Separate CIGMA D/O back order data which have error in previous batch running to SPS system.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferDoDataFromCigmaDomain
     * @param as400Server As400ServerConnectionInformationDomain
     * @param jobId jobId
     * @param log log
     * @return List TransferDoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferDoDataFromCigmaDomain> transactCheckTransferDoBackOrderFromCigma(
        Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId,
        Log log
    ) throws Exception;
    
    /**
     * 
     * <p>Transaction Transfer DO From CIGMA by Do No.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferDoDataFromCigmaDomain
     * @param as400Server SpsMAs400SchemaDomain
     * @param jobId String
     * @throws Exception Exception
     */
    public void transactTransferDoBackOrderFromCigma(Locale locale, 
        List<TransferDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId) throws Exception;
    
    
    public List<TransferChangeDoDataFromCigmaDomain> transactCheckTransferChangeDoBackOrderFromCigma(
        Locale locale, 
        List<TransferChangeDoDataFromCigmaDomain> transferList, 
        As400ServerConnectionInformationDomain as400Server, 
        String jobId,
        Log log
    ) throws Exception;
    
    
    public void transactTransferChangeDoBackOrderFromCigma(Locale locale, List<TransferChangeDoDataFromCigmaDomain> transferList, As400ServerConnectionInformationDomain as400Server, String jobId) 
        throws Exception;
    
    
    /**
     * 
     * <p>Search Mail BackOrder.</p>
     *
     * @param criteria criteria
     * @return List of backorder mail
     * @throws ApplicationException Exception
     */
    public List<DeliveryOrderDomain> searchMailBackOrder(SpsTDoDetailDomain criteria) 
        throws ApplicationException;
    
    public void transactSendNotificationChangeBackOrder(Locale locale
        , List<DeliveryOrderDomain> sendUrgentMail, int limit, Log log) 
        throws Exception;
    
	public Integer updateOneWayKanbanTagFlag(KanbanTagDomain kanbanTag)
			throws ApplicationException;
}
