/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2015/10/06 CSI Akat                  [IN016]
 * 2016/02/04 CSI Akat                  [IN056]
 * 2016/03/15 CSI Akat                  [IN069]
 * 2016/04/08 CSI Akat                  [IN068]
 * 2016/04/18 CSI Akat                  [IN070]
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change P/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;


//import com.globaldenso.ai.common.core.context.DensoContext;
//import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaChangePoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueForCalculateMonthDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//.MAIL_CIGMA_DO_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_PO_1_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_VENDOR_CD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_SCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_ISSUE_DATE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .PO_DATA_TYPE_MAP;
// [IN070] add list of email to send in content
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_PERSON_IN_CHARGE;

// static import
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0002;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0013;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0016;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0001;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0015;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0016;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0017;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0018;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0019;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0020;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0022;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0023;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_VENDOR_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_PARTS_NO_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0033;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TABLE_CIGMA_PO_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TABLE_CIGMA_CHG_PO_NAME;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO; // Create Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_DETAIL; // Create Purchase Order Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_DUE; // Create Purchase Order Due
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_CHG_PO; // Create Change Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_CHG_PO_DETAIL; // Create Change Purchase Order Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_CHG_PO_DUE; // Create Change Purchase Order Due
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE; // Create P/O Cover Page
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE_DETAIL; // Create P/O Cover Page Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT; // Search P/O Cover Page Report
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID; // Update PDF Original File ID
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID; // Update PDF Change File ID
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_SEARCH_EXIST_PO; // Search Exist Purchase Order
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO_DUE; // Update Purchase Order Due
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO_DETAIL; // Update Purchase Order Detail
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO; // Update Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_SEARCH_CHG_MAT_REPORT; // Search Change Material Report
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_SEARCH_EXISTS_FOR_FIRM; // Search Exists Purchaser Order for Firm Period
import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NBSP;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_C;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_Y;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.THREE;
import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;
import static com.globaldenso.asia.sps.common.constant.Constants.FIVE;
import static com.globaldenso.asia.sps.common.constant.Constants.SIX;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NEWLINE;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_CR_LF;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_HHMMSS;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToSqlDate;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToTimestamp;
import static com.globaldenso.asia.sps.common.utils.DateUtil.format;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.toBigDecimal;
import static com.globaldenso.asia.sps.common.utils.StringUtil.appendsString;
import static com.globaldenso.asia.sps.common.utils.StringUtil.nullToEmpty;
//import static com.globaldenso.asia.sps.common.utils.MessageUtil.throwsApplicationMessage;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getLabelHandledException;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getEmailLabel;
//import static com.globaldenso.asia.sps.common.utils.MessageUtil.getErrorMessage;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.writeLog;

/**
 * <p>The class TransferPoDataFromCigmaFacadeServiceImpl.</p>
 * <p>Facade for TransferPoDataFromCigmaFacadeServiceImpl.</p>
 * <ul>
 * <li>Method search  : searchInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class TransferPoDataFromCigmaFacadeServiceImpl
    implements TransferPoDataFromCigmaFacadeService {
    
    /** Reuse Argument 1. */
    private String[] argument1 = new String[ONE];
    /** Reuse Argument 2. */
    private String[] argument2 = new String[TWO];
    /** Reuse Argument 3. */
    private String[] argument3 = new String[THREE];
    /** Reuse Argument 4. */
    private String[] argument4 = new String[FOUR];
    /** Reuse Argument 5. */
    private String[] argument5 = new String[FIVE];
    /** Reuse Argument 6. */
    private String[] argument6 = new String[SIX];
    
    /** Common Services. */
    private CommonService commonService;
    /** Denso Company Services. */
    private CompanyDensoService companyDensoService;
    /** PurchaseOrderService */
    private PurchaseOrderService purchaseOrderService;
    /** PurchaseOrderCoverPageService */
    private PurchaseOrderCoverPageService purchaseOrderCoverPageService;
    /** UserService */
    private UserService userService;
    /** UserSupplierService */
    private UserSupplierService userSupplierService;
    /** RecordLimitService */
    private RecordLimitService recordLimitService;
    /** FileManagementService */
    private FileManagementService fileManagementService;
    /** Mail Service */
    private MailService mailService;
    /** CIGMA P/O Error Service */
    private CigmaPoErrorService cigmaPoErrorService;
    /** CIGMA Change P/O Error Service */
    private CigmaChgPoErrorService cigmaChgPoErrorService;
    
    /** Service Auto */
    /** Transaction P/O Service */
    private SpsTPoService spsTPoService;
    /** Transaction P/O Detail Service */
    private SpsTPoDetailService spsTPoDetailService;
    /** Transaction P/O Due Service */
    private SpsTPoDueService spsTPoDueService;
    /** Transaction P/O Cover Page Service */
    private SpsTPoCoverPageService spsTPoCoverPageService;
    /** Transaction P/O Cover Page Detail Service */
    private SpsTPoCoverPageDetailService spsTPoCoverPageDetailService;
    /** Transaction Cigma P/O Error Service */
    private SpsCigmaPoErrorService spsCigmaPoErrorService;
    /** Transaction Cigma Change P/O Error Service */
    private SpsCigmaChgPoErrorService spsCigmaChgPoErrorService;
    /** Master Denso Supplier Part Service */
    private SpsMDensoSupplierPartsService spsMDensoSupplierPartsService;
    /**  SpsMAs400Vendor Service */
    private SpsMAs400VendorService spsMAs400VendorService; 
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public TransferPoDataFromCigmaFacadeServiceImpl(){
        super();
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>Setter method for companyDensoService.</p>
     *
     * @param companyDensoService Set for companyDensoService
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * <p>Setter method for spsMAs400VendorService.</p>
     *
     * @param spsMAs400VendorService Set for spsMAs400VendorService
     */
    public void setSpsMAs400VendorService(
        SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }

    /**
     * <p>Setter method for purchaseOrderService.</p>
     *
     * @param purchaseOrderService Set for purchaseOrderService
     */
    public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    /**
     * <p>Setter method for purchaseOrderCoverPageService.</p>
     *
     * @param purchaseOrderCoverPageService Set for purchaseOrderCoverPageService
     */
    public void setPurchaseOrderCoverPageService(
        PurchaseOrderCoverPageService purchaseOrderCoverPageService) {
        this.purchaseOrderCoverPageService = purchaseOrderCoverPageService;
    }
    /**
     * <p>Setter method for supplierUserService.</p>
     *
     * @param supplierUserService Set for supplierUserService
     */
    public void setUserSupplierService(UserSupplierService supplierUserService) {
        this.userSupplierService = supplierUserService;
    }

    /**
     * <p>Setter method for recordLimitService.</p>
     *
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * <p>Setter method for fileManagementService.</p>
     *
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * <p>Setter method for mailService.</p>
     *
     * @param mailService Set for mailService
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * <p>Setter method for spsTPoService.</p>
     *
     * @param spsTPoService Set for spsTPoService
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }

    /**
     * <p>Setter method for spsTPoDetailService.</p>
     *
     * @param spsTPoDetailService Set for spsTPoDetailService
     */
    public void setSpsTPoDetailService(SpsTPoDetailService spsTPoDetailService) {
        this.spsTPoDetailService = spsTPoDetailService;
    }

    /**
     * <p>Setter method for spsTPoDueService.</p>
     *
     * @param spsTPoDueService Set for spsTPoDueService
     */
    public void setSpsTPoDueService(SpsTPoDueService spsTPoDueService) {
        this.spsTPoDueService = spsTPoDueService;
    }

    /**
     * <p>Setter method for spsTPoCoverPageService.</p>
     *
     * @param spsTPoCoverPageService Set for spsTPoCoverPageService
     */
    public void setSpsTPoCoverPageService(
        SpsTPoCoverPageService spsTPoCoverPageService) {
        this.spsTPoCoverPageService = spsTPoCoverPageService;
    }

    /**
     * <p>Setter method for spsTPoCoverPageDetailService.</p>
     *
     * @param spsTPoCoverPageDetailService Set for spsTPoCoverPageDetailService
     */
    public void setSpsTPoCoverPageDetailService(
        SpsTPoCoverPageDetailService spsTPoCoverPageDetailService) {
        this.spsTPoCoverPageDetailService = spsTPoCoverPageDetailService;
    }

    /**
     * <p>Setter method for spsCigmaPoErrorService.</p>
     *
     * @param spsCigmaPoErrorService Set for spsCigmaPoErrorService
     */
    public void setSpsCigmaPoErrorService(
        SpsCigmaPoErrorService spsCigmaPoErrorService) {
        this.spsCigmaPoErrorService = spsCigmaPoErrorService;
    }

    /**
     * <p>Setter method for spsCigmaChgPoErrorService.</p>
     *
     * @param spsCigmaChgPoErrorService Set for spsCigmaChgPoErrorService
     */
    public void setSpsCigmaChgPoErrorService(
        SpsCigmaChgPoErrorService spsCigmaChgPoErrorService) {
        this.spsCigmaChgPoErrorService = spsCigmaChgPoErrorService;
    }

    /**
     * <p>Setter method for spsMDensoSupplierPartsService.</p>
     *
     * @param spsMDensoSupplierPartsService Set for spsMDensoSupplierPartsService
     */
    public void setSpsMDensoSupplierPartsService(
        SpsMDensoSupplierPartsService spsMDensoSupplierPartsService) {
        this.spsMDensoSupplierPartsService = spsMDensoSupplierPartsService;
    }

    /**
     * <p>Setter method for cigmaPoErrorService.</p>
     *
     * @param cigmaPoErrorService Set for cigmaPoErrorService
     */
    public void setCigmaPoErrorService(CigmaPoErrorService cigmaPoErrorService) {
        this.cigmaPoErrorService = cigmaPoErrorService;
    }

    /**
     * <p>Setter method for cigmaChgPoErrorService.</p>
     *
     * @param cigmaChgPoErrorService Set for cigmaChgPoErrorService
     */
    public void setCigmaChgPoErrorService(
        CigmaChgPoErrorService cigmaChgPoErrorService) {
        this.cigmaChgPoErrorService = cigmaChgPoErrorService;
    }

    /**
     * <p>Setter method for userService.</p>
     *
     * @param userService Set for userService
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * 
     * <p>Search As400 Server List.</p>
     *
     * @param criteria SpsMCompanyDensoDomain
     * @return List of SpsMAs400SchemaDomain
     * @throws Exception error
     */
    public List<As400ServerConnectionInformationDomain> 
    searchAs400ServerList(SpsMCompanyDensoDomain criteria) 
        throws Exception {
        List<As400ServerConnectionInformationDomain> selectList = 
            companyDensoService.searchAs400ServerList(criteria);
        return selectList;
    }

    // [IN056] Get P/O from CIGMA by CIGMA Vendor Code
    // [IN056-2] Not get Vendor code from Oracle, get from CIGMA
    //public List<SpsMAs400VendorDomain> searchCigmaVendorCd(String dCd) throws Exception {
    //    List<SpsMAs400VendorDomain> result = null;
    //    SpsMAs400VendorCriteriaDomain criteria = new SpsMAs400VendorCriteriaDomain();
    //    criteria.setDCd(dCd);
    //    result = this.spsMAs400VendorService.searchByCondition(criteria);
    //    return result;
    //}
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#searchCigmaVendorCd(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain, java.lang.String)
     */
    public List<PseudoCigmaPoCoverPageInformationDomain> searchCigmaVendorCd(
        As400ServerConnectionInformationDomain as400Server, String spsFlag
    ) throws Exception {
        List<PseudoCigmaPoCoverPageInformationDomain> result = null;
        result = CommonWebServiceUtil.searchCigmaVendorCode(as400Server, spsFlag);
        return result;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#searchPurchaseOrder(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain, java.lang.String, java.lang.String)
     */
    public TransferPoDataFromCigmaResultDomain searchPurchaseOrder(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag
        // [IN056] : Add condition to get P/O by Vendor
        , String vendorCd
    ) throws Exception {
        // If search P/O transfer error (first step), delete all error record.
        if (Constants.STR_TWO.equals(spsFlag)) {
            // [IN056] Add Vendor Code to criteria for delete
            //this.deletePoError(as400Server.getSpsMCompanyDensoDomain().getDCd(), locale);
            this.deletePoError(as400Server.getSpsMCompanyDensoDomain().getDCd(), locale, vendorCd);
        }
        
        // [IN056] : Add condition to get P/O by Vendor
        //List<PseudoCigmaPoInformationDomain> pseudoList = 
        //    CommonWebServiceUtil.searchCigmaPurchaseOrder(as400Server, spsFlag);
        List<PseudoCigmaPoInformationDomain> pseudoList = 
            CommonWebServiceUtil.searchCigmaPurchaseOrder(as400Server, spsFlag, vendorCd);
        
        List<PseudoCigmaPoCoverPageInformationDomain> pseudoCoverPageList = null;
        if( null != pseudoList && ZERO < pseudoList.size() ) {
            // [IN056] : Add condition to get P/O by Vendor
            //pseudoCoverPageList = 
            ///    CommonWebServiceUtil.searchCigmaPurchaseOrderCoverPage(as400Server, spsFlag);
            pseudoCoverPageList = CommonWebServiceUtil.searchCigmaPurchaseOrderCoverPage(
                as400Server, spsFlag, vendorCd);
        }
        if( null != pseudoList && ZERO < pseudoList.size() ){
            TransferPoDataFromCigmaResultDomain result 
                = this.convertToCigmaPo(pseudoList, pseudoCoverPageList);
            return result;
        } else {
            return new TransferPoDataFromCigmaResultDomain();
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#transactCheckTransferPoFromCigma(java.util.Locale, com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, boolean, java.lang.String)
     */
    public List<TransferPoDataFromCigmaDomain> transactCheckTransferPoFromCigma(Locale locale, 
        List<TransferPoDataFromCigmaDomain> transferList,
        As400ServerConnectionInformationDomain as400Server, boolean bError,
        String jobId, Log log) throws Exception
    {
        StringBuffer sbErrorMsg = new StringBuffer();
        List<TransferPoDataFromCigmaDomain> resultList 
            = new ArrayList<TransferPoDataFromCigmaDomain>();
        // initial criteria 1-1
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = 
            new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
        BigDecimal zeroDecimal = new BigDecimal(ZERO);
        for( TransferPoDataFromCigmaDomain transfer : transferList ){
            Map<String, TransferPoDataFromCigmaDomain> headerMap 
                = new LinkedHashMap<String, TransferPoDataFromCigmaDomain>();
            
            // 1. Find Supplier Plant Code for each DENSO Part No and find SPS P/O No relate with P/O
            // LOOP until end of cigmaDOInformation. <List>cigmaDODetail. dPartNo
            // 1-1) Get SPS SupplierCode from SpsMAs400VendorService.
            SpsMAs400VendorDomain vendorDo = findSupplier(
                as400VendorCriteriaDomain, 
                transfer.getVendorCd());
            // check is not found Supplier Code.
            if( null == vendorDo ){
                insertCigmaPoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, null, null, STR_ZERO, ERROR_TYPE_VENDOR_NOT_FOUND);
                    
                sbErrorMsg.append(getMessage(locale, ERROR_CD_SP_E6_0015,
                    transfer.getCigmaPoNo(), EMPTY_STRING))
                    .append( SYMBOL_CR_LF );
                break;
            } // end check not found.
            transfer.setSCd(vendorDo.getSCd());
            
            SortUtil.sort(transfer.getCigmaPoDetailList(), SortUtil.COMPARE_CIGMA_PO_DETAIL);
            for(TransferPoDetailDataFromCigmaDomain detail : transfer.getCigmaPoDetailList()){
                SortUtil.sort(detail.getCigmaPoDueList(), SortUtil.COMPARE_CIGMA_PO_DUE);
            }
            
            TransferPoDataFromCigmaDomain header = null;
            String sPcd = null;
            String sPn = null;
            String keyDetail = null;
            
            SpsMDensoSupplierPartsCriteriaDomain partCriteria = 
                new SpsMDensoSupplierPartsCriteriaDomain();
            partCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            partCriteria.setDPcd(transfer.getDPcd());
            partCriteria.setSCd(transfer.getSCd());
            // 1-2) Get Supplier Information data from DENSOSupplierPartService
            boolean detailError = false;
            for(TransferPoDetailDataFromCigmaDomain doDetail : transfer.getCigmaPoDetailList()) {
                Map<String, TransferPoDetailDataFromCigmaDomain> detailMap
                    = new LinkedHashMap<String, TransferPoDetailDataFromCigmaDomain>();
                TransferPoDetailDataFromCigmaDomain detail = null;
                List<TransferPoDueForCalculateMonthDomain> reportTypeMList
                    = new ArrayList<TransferPoDueForCalculateMonthDomain>();
                
                // Find Max Due Date for compare with Effective Date in SPS_T_DENSO_SUPPLIER_PARTS
                Date maxDueDate = null;
                Date endFirm = null;
                for (TransferPoDueDataFromCigmaDomain tmpDue : doDetail.getCigmaPoDueList()) {
                    // Keep all report type M
                    if (Constants.STR_M.equals(tmpDue.getReportType())) {
                        TransferPoDueForCalculateMonthDomain reportM
                            = new TransferPoDueForCalculateMonthDomain();
                        reportM.setMonthDue(tmpDue);
                        reportM.setSummaryFromCigma(tmpDue.getOrderQty());
                        reportM.setSummaryFromTransfer(zeroDecimal);
                        reportM.setPreBuffer(zeroDecimal);
                        reportM.setPostBuffer(zeroDecimal);
                        reportM.setMonth(DateUtil.format(tmpDue.getEtd()
                            , DateUtil.PATTERN_YYYYMM_SLASH));
                        reportTypeMList.add(reportM);
                        continue;
                    }
                    
                    if (Constants.STR_C.equals(tmpDue.getOrderType())||Constants.STR_K.equals(tmpDue.getOrderType())) {
                        endFirm = tmpDue.getEtd();
                    }
                    
                    if (null == maxDueDate) {
                        maxDueDate = tmpDue.getDueDate();
                        if (null == maxDueDate) {
                            maxDueDate = tmpDue.getEtd();
                        }
                    } else {
                        if (null != tmpDue.getDueDate()) {
                            if (maxDueDate.compareTo(tmpDue.getDueDate()) < ZERO) {
                                maxDueDate = tmpDue.getDueDate();
                            }
                        } else {
                            if (maxDueDate.compareTo(tmpDue.getEtd()) < ZERO) {
                                maxDueDate = tmpDue.getEtd();
                            }
                        }
                    }
                }
                
                // Get all effective Parts information
                partCriteria.setEffectDateLessThanEqual(new Timestamp(maxDueDate.getTime()));
                List<SpsMDensoSupplierPartsDomain> partDoList = findPart(
                    partCriteria, doDetail.getDPn(), SupplierPortalConstant.SORT_PART_MASTER);
                
                if (null == partDoList) {
                    this.partNumberNotFoundError(partCriteria, doDetail, locale, as400Server,
                        transfer, doDetail.getCigmaPoDueList().get(ZERO), sbErrorMsg);
                    detailError = true;
                    continue;
                }
                
                // For each P/O Due
                List<SpsMDensoSupplierPartsDomain> effectPartList
                    = new ArrayList<SpsMDensoSupplierPartsDomain>();
                for (TransferPoDueDataFromCigmaDomain poDue : doDetail.getCigmaPoDueList()) {
                    Iterator<SpsMDensoSupplierPartsDomain> effectPartsIterator
                        = partDoList.iterator();
                    SpsMDensoSupplierPartsDomain effectParts = effectPartsIterator.next();
                    
                    Date compareEffect = poDue.getDueDate();
                    if (null == compareEffect) {
                        compareEffect = poDue.getEtd();
                    }
                    
                    // Search for appropriate part
                    while (compareEffect.compareTo(effectParts.getEffectDate()) < ZERO
                        && effectPartsIterator.hasNext())
                    {
                        effectParts = effectPartsIterator.next();
                    }
                    
                    if (compareEffect.compareTo(effectParts.getEffectDate()) < ZERO) {
                        this.partNumberNotFoundError(partCriteria, doDetail, locale, as400Server,
                            transfer, poDue, sbErrorMsg);
                        detailError = true;
                        continue;
                    }
                    
                    if (detailError) {
                        // Current P/O error, don't have to keep data
                        continue;
                    }
                    
                    sPcd = effectParts.getSPcd();
                    sPn = effectParts.getSPn();
                    poDue.setSPcd(sPcd);
                    transfer.setSPcd(sPcd);
                    doDetail.setSPn(sPn);
                    doDetail.setEndFirm(endFirm);
                    
                    // Ignore of record type M
                    /*Comment for fix change part 20230527
                    if (!Constants.STR_M.equals(poDue.getReportType())) {
                        if( null == headerMap.get(sPcd) ){
                            header = new TransferPoDataFromCigmaDomain();
                            header = this.cloneNew(transfer, header);
                            headerMap.put(sPcd, header);
                        }
                        header = headerMap.get( sPcd );
                        header.setSPcd( sPcd );
                        
                        keyDetail = appendsString(sPcd, Constants.SYMBOL_COLON, sPn);
                        
                        if (null == detailMap.get(keyDetail)) {
                            effectPartList.add(effectParts);
                            detail = this.cloneTransferDetail(doDetail);
                            detailMap.put(keyDetail, detail);
                            header.getCigmaPoDetailList().add( detail );
                        }
                        
                        detail = detailMap.get(keyDetail);
                        detail.getCigmaPoDueList().add(poDue);
                        
                        // Summary Order Quantity from all record for each month
                        String month = DateUtil.format(compareEffect, DateUtil.PATTERN_YYYYMM_SLASH);
                        for (TransferPoDueForCalculateMonthDomain dueM : reportTypeMList) {
                            if (month.equals(dueM.getMonth())) {
                                BigDecimal dueQty = poDue.getOrderQty();
                                if (null == dueQty) {
                                    dueQty = zeroDecimal;
                                }
                                dueM.setSummaryFromTransfer(dueM.getSummaryFromTransfer().add(dueQty));

                                if (Constants.STR_W.equals(poDue.getReportType())) {
                                    dueM.setLastWeekDue(poDue);
                                }
                            }
                        }
                    }
                    */
                    
                    if( null == headerMap.get(sPcd) ){
                        header = new TransferPoDataFromCigmaDomain();
                        header = this.cloneNew(transfer, header);
                        headerMap.put(sPcd, header);
                    }
                    header = headerMap.get( sPcd );
                    header.setSPcd( sPcd );
                    
                    keyDetail = appendsString(sPcd, Constants.SYMBOL_COLON, sPn);
                    
                    if (null == detailMap.get(keyDetail)) {
                        effectPartList.add(effectParts);
                        detail = this.cloneTransferDetail(doDetail);
                        detailMap.put(keyDetail, detail);
                        header.getCigmaPoDetailList().add( detail );
                    }
                    
                    detail = detailMap.get(keyDetail);
                    detail.getCigmaPoDueList().add(poDue);
                    
                    if (!Constants.STR_M.equals(poDue.getReportType())) {
                    	// Summary Order Quantity from all record for each month
                        String month = DateUtil.format(compareEffect, DateUtil.PATTERN_YYYYMM_SLASH);
                        for (TransferPoDueForCalculateMonthDomain dueM : reportTypeMList) {
                            if (month.equals(dueM.getMonth())) {
                                BigDecimal dueQty = poDue.getOrderQty();
                                if (null == dueQty) {
                                    dueQty = zeroDecimal;
                                }
                                dueM.setSummaryFromTransfer(dueM.getSummaryFromTransfer().add(dueQty));

                                if (Constants.STR_W.equals(poDue.getReportType())) {
                                    dueM.setLastWeekDue(poDue);
                                }
                            }
                        }
                    }
                    
                    
                } // End for DUE
                
                /*Comment for fix change part 20230527
                if (ONE == detailMap.size()) {
                    keyDetail = appendsString(sPcd, Constants.SYMBOL_COLON, sPn);
                    detail = detailMap.get(keyDetail);
                    for (TransferPoDueForCalculateMonthDomain dueM : reportTypeMList) {
                        detail.getCigmaPoDueList().add(dueM.getMonthDue());
                    }
                    
                    // Current part not split, Not have to do following complex.
                    continue;
                }
                */
                
                if (ONE == detailMap.size()) {
                    // Current part not split, Not have to do following complex.
                    continue;
                }
                

                TransferPoDueForCalculateMonthDomain month1 = reportTypeMList.get(ZERO);
                TransferPoDueForCalculateMonthDomain month2 = reportTypeMList.get(ONE);
                TransferPoDueForCalculateMonthDomain month3 = reportTypeMList.get(TWO);
                TransferPoDueForCalculateMonthDomain month4 = reportTypeMList.get(THREE);
                
                /* Font Courier
                 * ______________________________
                 * | Month 1                    |
                 * |____________________________|_____________
                 * | Pre Buffer | Report type D
                 * |____________|_____________________________
                 * */
                if (ZERO != month1.getSummaryFromCigma().compareTo(
                    month1.getSummaryFromTransfer()))
                {
                    month1.setPreBuffer(month1.getSummaryFromCigma().subtract(
                        month1.getSummaryFromTransfer()));
                }
                
                /* Font Courier
                 * __________________________________________________________________
                 *  Month 1   | Month 2                           | Month 3
                 * ___________|___________________________________|__________________
                 * Report type D              | Report Type W     | Pre Buffer |
                 * ___________________________|________________________________|
                 * */
                if (ZERO != month2.getSummaryFromCigma().compareTo(
                    month2.getSummaryFromTransfer()))
                {
                    month3.setPreBuffer(month2.getSummaryFromTransfer().subtract(
                        month2.getSummaryFromCigma()));
                }
                
                /* Font Courier
                 * __________________________________________________________________
                 * Month 2  | Month 3                              | Month 4
                 * _________|______________________________________|_________________
                 *          | Pre Buffer | Report Type W           | Pre Buffer |
                 * ______________________|______________________________________|
                 * */
                BigDecimal summary3 = month3.getSummaryFromTransfer().add(month3.getPreBuffer());
                if (ZERO != summary3.compareTo(month3.getSummaryFromCigma())) {
                    month4.setPreBuffer(summary3.subtract(month3.getSummaryFromCigma()));
                }
                
                /* Font Courier
                 * __________________________________________________________
                 * Month 3  | Month 4                                       |
                 * _________|_______________________________________________|
                 *          | Pre Buffer | Report Type W      | Post Buffer |
                 * ______________________|____________________|_____________|
                 * */
                BigDecimal summary4 = month4.getSummaryFromTransfer().add(month4.getPreBuffer());
                if (ZERO != summary4.compareTo(month4.getSummaryFromCigma())) {
                    month4.setPostBuffer(month4.getSummaryFromCigma().subtract(summary4));
                }
                
                // detailMap Map<String, TransferPoDetailDataFromCigmaDomain>
                int effectIndex = ZERO;
                int monthIndex = ZERO;
                TransferPoDueForCalculateMonthDomain currentMonth = null;
                TransferPoDueForCalculateMonthDomain tmpMonth = null;
                List<TransferPoDueDataFromCigmaDomain> monthForDetailList = null;
                TransferPoDueDataFromCigmaDomain monthPreviousDetail = null;
                for (Entry<String, TransferPoDetailDataFromCigmaDomain> detailEntry
                    : detailMap.entrySet())
                {
                	TransferPoDetailDataFromCigmaDomain poDetail = detailEntry.getValue();
                    monthForDetailList = new ArrayList<TransferPoDueDataFromCigmaDomain>();
                    if (null == currentMonth) {
                        currentMonth = reportTypeMList.get(monthIndex);
                    }
                    
                    /*   Comment for fix change part 20230527                 
                    // Start new Detail Split with cloned current month
                    tmpMonth = new TransferPoDueForCalculateMonthDomain();
                    tmpMonth.setMonthDue(
                        this.clonePurchaseOrderDue(currentMonth.getMonthDue()));
                    tmpMonth.setMonth(currentMonth.getMonth());
                    tmpMonth.setSummaryFromTransfer(zeroDecimal);
                    monthForDetailList.add(tmpMonth.getMonthDue());
                    
                    boolean monthStarter = false;
                    boolean firstDue = true;
                    
                    for (TransferPoDueDataFromCigmaDomain due : poDetail.getCigmaPoDueList()) {
                        
                        Date compareEffect = due.getDueDate();
                        if (null == compareEffect) {
                            compareEffect = due.getEtd();
                        }
                        
                        String month = DateUtil.format(compareEffect,
                            DateUtil.PATTERN_YYYYMM_SLASH);

                        // End Current Month
                        if (!month.equals(currentMonth.getMonth())) {
                        	//TransferPoDueForCalculateMonthDomain nextMonth = reportTypeMList.get(monthIndex + ONE);
                        	
                        	TransferPoDueForCalculateMonthDomain nextMonth = null;
                        	if(monthIndex < reportTypeMList.size()-1){
                        		nextMonth = reportTypeMList.get(monthIndex + ONE);
                        	}
                        	
                            // If not change month at first Due
                            if (!firstDue) {
                                // Close and calculate Qty for current month
                                BigDecimal mQty = tmpMonth.getSummaryFromTransfer()
                                    .subtract(nextMonth.getPreBuffer());
                                
                                // Current month start by current P/O Detail
                                if (monthStarter) {
                                    mQty = mQty.add(currentMonth.getPreBuffer());
                                }
                                tmpMonth.getMonthDue().setOrderQty(mQty);
                            } else {
                                // If First Due, remove last added
                                monthForDetailList.clear();
                                                                
                                if (null != monthPreviousDetail) {
                                    monthPreviousDetail.setOrderQty(
                                        monthPreviousDetail.getOrderQty().subtract(
                                            nextMonth.getPreBuffer()));
                                }
                            } // End Close current month
                            
                            // Start New Month
                            monthIndex++;

                            currentMonth = reportTypeMList.get(monthIndex);
                            tmpMonth = new TransferPoDueForCalculateMonthDomain();
                            tmpMonth.setMonthDue(
                                this.clonePurchaseOrderDue(currentMonth.getMonthDue()));
                            tmpMonth.setMonth(currentMonth.getMonth());
                            tmpMonth.setSummaryFromTransfer(zeroDecimal);
                            monthForDetailList.add(tmpMonth.getMonthDue());
                            
                            monthStarter = true;
                        } // End Change Month process
                        
                        // Keep Order Quantity to current month
                        BigDecimal orderQty = due.getOrderQty();
                        if (null == orderQty) {
                            orderQty = zeroDecimal;
                        }
                        tmpMonth.setSummaryFromTransfer(
                            tmpMonth.getSummaryFromTransfer().add(orderQty));
                        
                        firstDue = false;
                    } // End each P/O Due
                    
                    
                    poDetail.getCigmaPoDueList().addAll(monthForDetailList);
                    
                    // End P/O Detail, close and calculate Qty for current month
                    BigDecimal mQty = tmpMonth.getSummaryFromTransfer();
                    // Current month start by current P/O Detail
                    if (monthStarter) {
                        mQty = mQty.add(currentMonth.getPreBuffer());
                    }
                    tmpMonth.getMonthDue().setOrderQty(mQty);
                    
                    monthPreviousDetail = tmpMonth.getMonthDue();
                    */
                    
                    // Set Start / End Period Date
                    
                    if (ZERO != effectIndex) {
                        poDetail.setStartPeriodDate(new Date(
                            effectPartList.get(effectIndex).getEffectDate().getTime()));
                    }
                    /*
                    if (effectIndex != effectPartList.size()) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(effectPartList.get(effectIndex).getEffectDate().getTime());
                        cal.add(Calendar.DATE, Constants.MINUS_ONE);
                        poDetail.setEndPeriodDate(new Date(cal.getTimeInMillis()));
                    }
                    */
                    //ISSUE ENE_PERIOD_DATE 20230508
                    
                    if (effectIndex < effectPartList.size()-1) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(effectPartList.get(effectIndex+1).getEffectDate().getTime());
                        cal.add(Calendar.DATE, Constants.MINUS_ONE);
                        poDetail.setEndPeriodDate(new Date(cal.getTimeInMillis()));
                    }
                    
                    
                    effectIndex++;
                } // End each Entry from P/O detail split map
                
                /* Comment for fix change part 20230527
                // Last month Quantity that not show in current report
                if (null != monthPreviousDetail) {
                    monthPreviousDetail.setOrderQty(
                        monthPreviousDetail.getOrderQty().add(
                            currentMonth.getPostBuffer()));
                }
                */
            } // end for Detail.
            
            if( EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
                for (String strSpcd : headerMap.keySet()) {
                    resultList.add( headerMap.get(strSpcd) );
                }
            }
        } // end for Header.
        if( !EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
            writeLog(log, TWO, sbErrorMsg.toString());
            resultList.clear();
            if( !bError ){
                String partNoError = null;
                try{
                    partNoError = CommonWebServiceUtil
                        .updateCigmaPurchaseOrder(as400Server, transferList, null, STR_TWO, jobId);
                }catch(Exception ex){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0018, STR_TWO, Constants.PO );
                }
                if( !StringUtil.checkNullOrEmpty(partNoError) ){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0018, STR_TWO, Constants.PO );
                }
            } // end if
        }
        return resultList;
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#transactTransferErrorPoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, java.util.List, com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public String transactTransferErrorPoFromCigma(Locale locale
        , List<TransferPoDataFromCigmaDomain> transferList
        , List<TransferPoCoverPageDataFromCigmaDomain> cover
        , As400ServerConnectionInformationDomain as400Server, String jobId) 
        throws Exception {
        return transferPoFromCigma(locale, 
            transferList, 
            cover, 
            as400Server, 
            true, 
            jobId);
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#transactTransferPoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, java.util.List, com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public String transactTransferPoFromCigma(Locale locale
        , List<TransferPoDataFromCigmaDomain> transferList
        , List<TransferPoCoverPageDataFromCigmaDomain> coverList
        , As400ServerConnectionInformationDomain as400Server, String jobId) 
        throws Exception {
        return transferPoFromCigma(locale, 
            transferList, 
            coverList, 
            as400Server, 
            false, 
            jobId);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#searchChangePurchaseOrder(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public Map<String, List<TransferChangePoDataFromCigmaDomain>> searchChangePurchaseOrder(
        Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag) throws Exception
    {
        // If search for Change P/O error (first step), delete all error record.
        if(Constants.STR_TWO.equals(spsFlag)) {
            this.deleteChgPoError(as400Server.getSpsMCompanyDensoDomain().getDCd(), locale);
        }
        
        List<PseudoCigmaChangePoInformationDomain> pseudoList = 
            CommonWebServiceUtil.searchCigmaChangePurchaseOrder(as400Server, spsFlag);
//        if( null == pseudoList || ZERO == pseudoList.size() ){
//            MessageUtil.throwsApplicationMessageWithLabel(locale, 
//                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
//                null);
//        }
        Map<String, List<TransferChangePoDataFromCigmaDomain>> resultMap 
            = new HashMap<String, List<TransferChangePoDataFromCigmaDomain>>();
        if( ZERO < pseudoList.size() ) {
            for(TransferChangePoDataFromCigmaDomain pseudo : convertToCigmaChangePo(pseudoList)){
                String chgPoKey = appendsString(pseudo.getCigmaPoNo()
                    , Constants.SYMBOL_PIPE, pseudo.getVendorCd());
                if( null == resultMap.get(chgPoKey) ){
                    List<TransferChangePoDataFromCigmaDomain> list
                        = new ArrayList<TransferChangePoDataFromCigmaDomain>();
                    list.add(pseudo);
                    resultMap.put(chgPoKey, list);
                    continue;
                }
                List<TransferChangePoDataFromCigmaDomain> list = resultMap.get(chgPoKey);
                list.add(pseudo);
            }
        }
        return resultMap; //convertToCigmaChangePo(pseudoList);
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#checkTransferChangePOFromCIGMA(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain, com.globaldenso.asia.sps.auto.business.domain.As400ServerConnectionInformationDomain, boolean)
     */
    public List<TransferChangePoDataFromCigmaDomain> transactCheckTransferChangePoFromCigma(
        Locale locale, List<TransferChangePoDataFromCigmaDomain> transferList,
        As400ServerConnectionInformationDomain as400Server, boolean bError,
        String jobId, Log log) throws Exception {
        StringBuffer sbErrorMsg = new StringBuffer();
        List<TransferChangePoDataFromCigmaDomain> resultList 
            = new ArrayList<TransferChangePoDataFromCigmaDomain>();
        // 1. Find Supplier Plant Code for each DENSO Part No and find SPS P/O No relate with P/O
        // initial criteria 1-1
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain = 
            new SpsMAs400VendorCriteriaDomain();
        as400VendorCriteriaDomain.setIsActive(STR_ONE);
        as400VendorCriteriaDomain.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
        for(TransferChangePoDataFromCigmaDomain transfer : transferList) {
            Map<String, TransferChangePoDataFromCigmaDomain> headerMap 
                = new LinkedHashMap<String, TransferChangePoDataFromCigmaDomain>();
            // initial criteria 1-3
            // LOOP until end of cigmaDOInformation. <List>cigmaDODetail. dPartNo
            // 1-1) Get SPS SupplierCode from SpsMAs400VendorService.
            as400VendorCriteriaDomain.setVendorCd( transfer.getVendorCd() );
            SpsMAs400VendorDomain vendorDo 
                = findSupplier(as400VendorCriteriaDomain, transfer.getVendorCd());
            // check is not found Supplier Code.
            if ( null == vendorDo ) {
                // (1) Keep CIGMA Error data into SPS_CIGMA_PO_ERROR (Keep all data from the same CIGMA P/O No)
                insertCigmaChgPoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, null, null, STR_ZERO, ERROR_TYPE_VENDOR_NOT_FOUND);
                    
                sbErrorMsg.append(getMessage(locale, ERROR_CD_SP_E6_0015,
                    transfer.getCigmaPoNo(), EMPTY_STRING ))
                    .append( SYMBOL_CR_LF );
                break;
            } // end check not found.
            transfer.setSCd( vendorDo.getSCd() );
            TransferChangePoDataFromCigmaDomain header = null;
            String sPcd = null;
            String sPn = null;
            String keyDetail = null;
            
            SpsMDensoSupplierPartsCriteriaDomain partCriteria = 
                new SpsMDensoSupplierPartsCriteriaDomain();
            partCriteria.setDCd(as400Server.getSpsMCompanyDensoDomain().getDCd());
            partCriteria.setDPcd(transfer.getDPcd());
            partCriteria.setSCd(transfer.getSCd());
    
            // 1-2) Get Supplier Information data from DENSOSupplierPartService
            boolean detailError = false;
            for(TransferChangePoDetailDataFromCigmaDomain doDetail
                : transfer.getCigmaPoDetailList())
            {
                Map<String, TransferChangePoDetailDataFromCigmaDomain> detailMap
                    = new LinkedHashMap<String, TransferChangePoDetailDataFromCigmaDomain>();
                TransferChangePoDetailDataFromCigmaDomain detail = null;
                for (TransferChangePoDueDataFromCigmaDomain poDue : doDetail.getCigmaPoDueList()) {
                    Date dueDate = poDue.getDueDate();
                    if (null == dueDate) {
                        dueDate = poDue.getEtd();
                    }
                    partCriteria.setEffectDateLessThanEqual(new Timestamp(dueDate.getTime()));
                    
                    List<SpsMDensoSupplierPartsDomain> partDoList
                        = findPart(partCriteria, doDetail.getDPn()
                            , SupplierPortalConstant.SORT_PART_MASTER);
                    
                    // check is not found Supplier Part No
                    if( null == partDoList ){
                        String cause = ERROR_TYPE_PARTS_NO_NOT_FOUND;
                        partCriteria.setEffectDateLessThanEqual(null);
                        List<SpsMDensoSupplierPartsDomain> effectList = findPart(partCriteria,
                            doDetail.getDPn(), SupplierPortalConstant.SORT_PART_MASTER_ASC);
                        
                        if (null != effectList) {
                            cause = ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
                        }
                        
                        insertCigmaChgPoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
                            transfer, doDetail, poDue, STR_ONE, cause);
                        
                        sbErrorMsg.append(getMessage(locale, ERROR_CD_SP_E6_0015,
                            transfer.getCigmaPoNo(), doDetail.getDPn()))
                            .append( SYMBOL_CR_LF );
                        detailError = true;
                        break;
                    }
                    
                    if (detailError) {
                        // Current P/O error, don't have to keep data
                        continue;
                    }
                    
//                    doDetail.setSPn( partList.get(ZERO).getSPn() );
                    sPcd = partDoList.get(ZERO).getSPcd();
                    sPn = partDoList.get(ZERO).getSPn();
                    poDue.setSPcd( sPcd );
                    transfer.setSPcd(sPcd);
                    doDetail.setSPn( sPn );
                    
                    if( null == headerMap.get(sPcd) ){
                        header = new TransferChangePoDataFromCigmaDomain();
                        header = this.cloneNew(transfer, header);
                        headerMap.put(sPcd, header);
                    }
                    header = headerMap.get( sPcd );
                    header.setSPcd( sPcd );
                    
                    keyDetail = appendsString(sPcd, Constants.SYMBOL_COLON, sPn);
                    
                    if (null == detailMap.get(keyDetail)) {
                        detail = this.cloneChangePoDetail(doDetail);
                        detailMap.put(keyDetail, detail);
                        header.getCigmaPoDetailList().add( detail );
                    }
                    
                    detail = detailMap.get(keyDetail);
                    detail.getCigmaPoDueList().add(poDue);
                }
            } // end for Detail.
            if( EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
                for (String strSpcd : headerMap.keySet()) {
                    resultList.add( headerMap.get(strSpcd) );
                }
            }
        } // end for.
        if( !EMPTY_STRING.equals( sbErrorMsg.toString() ) ){
            writeLog(log, TWO, sbErrorMsg.toString());
            resultList.clear();
            if( !bError ){
                String partNoError = null;
                try{
                    partNoError = CommonWebServiceUtil
                        .updateCigmaChangePurchaseOrder(as400Server, transferList, STR_TWO, jobId);
                }catch(Exception ex){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0018, STR_TWO, Constants.CHG_PO );
                }
                if( !StringUtil.checkNullOrEmpty(partNoError) ){
                    throwsErrorMessage(locale, ERROR_CD_SP_E6_0018, STR_TWO, Constants.CHG_PO );
                }
            } // end if
        }
        return resultList;
    } // end check.
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#transactTransferChgPoFromCigma(java.util.Locale, com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain, com.globaldenso.asia.sps.auto.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public List<TransferChangePoDataFromCigmaDomain> transactTransferChgPoFromCigma(Locale locale
        , List<TransferChangePoDataFromCigmaDomain> transferList
        , As400ServerConnectionInformationDomain as400Server, String jobId) 
        throws Exception {
        return transferChangPoFromCigma(locale, transferList, as400Server
            , false, jobId);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#transactTransferErrorChgPoFromCigma(java.util.Locale, java.util.List, com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain, java.lang.String)
     */
    public List<TransferChangePoDataFromCigmaDomain> transactTransferErrorChgPoFromCigma(
        Locale locale
        , List<TransferChangePoDataFromCigmaDomain> transferList
        , As400ServerConnectionInformationDomain as400Server
        , String jobId) throws Exception {
        return transferChangPoFromCigma(locale, transferList, as400Server
            , true, jobId);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#searchRecordLimit()
     */
    public MiscellaneousDomain searchRecordLimit() throws ApplicationException {
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.LIMIT_BATCH_RECORD_TYPE);
        miscDomain.setMiscCode(SupplierPortalConstant.LIMIT_MAX_REC_ORD_E_CODE);
        return recordLimitService.searchRecordLimit(miscDomain);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#searchCigmaPoError()
     */
    public List<TransferPoErrorEmailDomain> searchCigmaPoError(SpsCigmaPoErrorDomain criteria)
        throws ApplicationException
    {
        return this.cigmaPoErrorService.searchTransferPoError(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#searchCigmaChgPoError()
     */
    public List<TransferChgPoErrorEmailDomain> searchCigmaChgPoError(
        SpsCigmaChgPoErrorDomain criteria) throws ApplicationException
    {
        return this.cigmaChgPoErrorService.searchTransferChgPoError(criteria);
    }

    /**
     * <p>Find Mail.</p>
     *
     * @param criSupplierUser UserSupplierDetailDomain
     * @param sCd String
     * @param sPcd String
     * @param emailTo List of mail
     * @return isSkip
     */
    // [IN016] Send email to supplier by plant
    //private boolean findMail(
    //    UserSupplierDetailDomain criSupplierUser,
    //    String sCd,
    //    List<String> emailTo)
    //{
    private boolean findMail(
        UserSupplierDetailDomain criSupplierUser,
        String sCd,
        String sPcd,
        List<String> emailTo)
    {
        criSupplierUser.getSpsMUserSupplierDomain().setSCd(sCd);
        
        // [IN016] Send email to supplier by plant
        criSupplierUser.getSpsMUserSupplierDomain().setSPcd(sPcd);
        
        if( ZERO == emailTo.size() ) {
            
            // [IN016] avoid send duplicate email
            //emailTo.addAll( 
            //    userSupplierService
            //        .searchUserSupplierEmailByNotFoundFlag(criSupplierUser)
            //);
            List<String> mailList
                = userSupplierService.searchUserSupplierEmailByNotFoundFlag(criSupplierUser);
            for (String mail : mailList) {
                if (!emailTo.contains(mail)) {
                    emailTo.add(mail);
                }
            }
            
            if( ZERO == emailTo.size() ){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Find User DENSO's email.
     * @param criDensoUser criteria for search
     * @param dCd DENSO company Code
     * @param dPcd DENSO plant code.
     * @param emailTo list of email to send
     * @return is skip
     * */
    private boolean findDensoMail(SpsMUserDensoDomain criDensoUser, String dCd, String dPcd
        , List<String> emailTo)
    {
        criDensoUser.setDCd(dCd);
        criDensoUser.setDPcd(dPcd);
        emailTo.clear();
        List<SpsMUserDomain> densoList = userService.searchEmailUserDenso(criDensoUser);
        if (ZERO != densoList.size()) {
            emailTo.addAll(convertEmail(densoList));
            return false;
        }
        return true;
    }
    
    /**
     * 
     * <p>convert Email.</p>
     *
     * @param userList List of SpsMUserDomain
     * @return List of String
     */
    private List<String> convertEmail(List<SpsMUserDomain> userList){
        List<String> resultList = new ArrayList<String>(userList.size());
        for(SpsMUserDomain user : userList){
            if( !resultList.contains( user.getEmail() ) ){
                resultList.add( user.getEmail() );
            }
        }
        return resultList;
    }
    
    /**
     * <p> initial P/O Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param emailTo list of email to send
     * @throws Exception the Exception
     */
    private void initialPoMail(
        Locale locale,
        StringBuffer sbContent
        
        // [IN070] add list of email to send in content
        , List<String> emailTo
        
    ) throws Exception {
        
        sbContent.append(appendsString(
            getEmailLabel(locale, MAIL_CIGMA_PO_ERROR_HEADER1)
            , SYMBOL_CR_LF
            , getEmailLabel(locale, MAIL_CIGMA_PO_ERROR_HEADER2)
            , SYMBOL_CR_LF));
        
        // [IN070] add list of email to send in content
        if (null != emailTo && ZERO != emailTo.size()) {
            String emailToString = StringUtil.convertListToStringCommaSeperate(emailTo);
            sbContent.append(appendsString(
                SYMBOL_NEWLINE
                , getEmailLabel(locale, MAIL_CIGMA_ERROR_PERSON_IN_CHARGE)
                , emailToString
                , Constants.SYMBOL_END_PARAGRAPH
                , SYMBOL_NEWLINE));
        }
    }
    
    /**
     * 
     * <p>fill Mail D/O Detil.</p>
     *
     * @param sbContent StringBuffer
     * @param locale Locale
     * @param domain SpsCigmaDoErrorDomain
     * @throws Exception the Exception
     */
    private void fillPoDetil(
        StringBuffer sbContent, 
        Locale locale, 
        TransferPoErrorEmailDomain domain) throws Exception {
        
        /* [User Name] [D_CD] [D_PCD] [D_PN] [P/O Type] [Start error delivery date] [S_PCD]
         * [Supplier P/No] [Effective Date] [CIGMA P/O No.]
         */
        String username = nullToEmpty(domain.getCigmaPoError().getUpdateByUserId());
        if( EMPTY_STRING.equals(username) ){
            username = SYMBOL_NBSP;
        }
        String dcd = nullToEmpty(domain.getCigmaPoError().getDCd());
        if( EMPTY_STRING.equals(dcd) ){
            dcd = SYMBOL_NBSP;
        }
        String dpcd = nullToEmpty(domain.getCigmaPoError().getDPcd());
        if( EMPTY_STRING.equals(dpcd) ){
            dpcd = SYMBOL_NBSP;
        }
        String dpn = nullToEmpty(domain.getCigmaPoError().getDPn());
        if( EMPTY_STRING.equals(dpn) ){
            dpn = SYMBOL_NBSP;
        }
        String dataType = nullToEmpty(domain.getCigmaPoError().getDataType());
        if( EMPTY_STRING.equals(dataType) ){
            dataType = SYMBOL_NBSP;
        }
        String etd = format(domain.getCigmaPoError().getEtd(), PATTERN_YYYYMMDD_SLASH);
        if( EMPTY_STRING.equals(etd) ){
            etd = SYMBOL_NBSP;
        }

        // Start : [IN068] Use first date of minimum Issue Month for this Part No.
        // [IN068] apply to Supplier Info Not found and Effective Date Not Match
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getCigmaPoError().getErrorTypeFlg())) {
        SpsCigmaPoErrorDomain minIssueCriteria = new SpsCigmaPoErrorDomain();
        minIssueCriteria.setDCd(domain.getCigmaPoError().getDCd());
        minIssueCriteria.setSCd(domain.getCigmaPoError().getSCd());
        minIssueCriteria.setDPcd(domain.getCigmaPoError().getDPcd());
        minIssueCriteria.setDPn(domain.getCigmaPoError().getDPn());
        Date minIssueDate = this.cigmaPoErrorService.searchMinIssueDate(minIssueCriteria);
        Calendar deliveryCal = Calendar.getInstance();
        deliveryCal.setTimeInMillis(minIssueDate.getTime());
        deliveryCal.set(Calendar.DATE, Constants.ONE);
        java.sql.Date firstDayOfMonth = new java.sql.Date(deliveryCal.getTimeInMillis());
        etd = StringUtil.appendsString(Constants.SYMBOL_SPAN_RED
            , format(firstDayOfMonth, PATTERN_YYYYMMDD_SLASH)
            , Constants.SYMBOL_END_SPAN);
        //}
        // End : [IN068] Use first date of minimum Issue Month for this Part No.
        
        String spcd = nullToEmpty(domain.getDensoSupplerParts().getSPcd());
        if( EMPTY_STRING.equals(spcd) ){
            spcd = SYMBOL_NBSP;
        }
        String sPn = nullToEmpty(domain.getDensoSupplerParts().getSPn());
        if( EMPTY_STRING.equals(sPn) ){
            sPn = SYMBOL_NBSP;
        }
        String effect = format(domain.getDensoSupplerParts().getEffectDate()
            , PATTERN_YYYYMMDD_SLASH);
        if(null == domain.getDensoSupplerParts().getEffectDate() || EMPTY_STRING.equals(effect) ){
            effect = SYMBOL_NBSP;
        }
        
        sbContent.append(appendsString(String.format(
            getEmailLabel(locale , MAIL_CIGMA_PO_ERROR_DETAIL)
            , username
            , dcd
            , dpcd
            , dpn
            , dataType
            , etd
            , spcd
            , sPn
            , effect
            , domain.getCigmaPoError().getCigmaPoNo())
            , SYMBOL_CR_LF));
    }
    
    /**
     * 
     * <p>send P/O Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param sbTitle StringBuffer
     * @param sbFrom StringBuffer
     * @param emailTo List of mail
     * @param companyCd company code
     * @param log to output error message
     * @throws Exception the Exception
     */
    private void sendPoMail(
        Locale locale, 
        StringBuffer sbContent, 
        StringBuffer sbTitle, 
        StringBuffer sbFrom, 
        List<String> emailTo,
        String companyCd,
        Log log) throws Exception {
        sbContent.append( Constants.SYMBOL_END_TABLE );
        sbContent.append( 
            appendsString( 
                getEmailLabel(locale
                    , MAIL_CIGMA_PO_ERROR_FOOTER) 
                    /*, SYMBOL_NEWLINE*/, SYMBOL_CR_LF) );
        
        boolean result = sendMail(
            sbTitle.toString(), 
            sbContent.toString(), 
            sbFrom.toString(), 
            emailTo);
        sbContent.delete(0, sbContent.length());

        if (!result) {
            // If cannot send email, not throws Exception
            //throwsErrorMessage(locale, ERROR_CD_SP_E5_0002, sCd);
            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                , new String[] {companyCd}
                , log, TWO, false);
        }
    }
    
    /**
     * <p> initial Change P/O Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param emailTo list of email to send
     * @throws Exception the Exception
     */
    private void initialChangePoMail(
        Locale locale,
        StringBuffer sbContent
        
        // [IN070] add list of email to send in content
        , List<String> emailTo
    ) throws Exception {
        sbContent.append(appendsString(
            getEmailLabel(locale, MAIL_CIGMA_CHG_PO_1_ERROR_HEADER1) 
            , SYMBOL_CR_LF
            , getEmailLabel(locale, MAIL_CIGMA_CHG_PO_1_ERROR_HEADER2)
            , SYMBOL_CR_LF));
        
        // [IN070] add list of email to send in content
        if (null != emailTo && ZERO != emailTo.size()) {
            String emailToString = StringUtil.convertListToStringCommaSeperate(emailTo);
            sbContent.append(appendsString(
                SYMBOL_NEWLINE
                , getEmailLabel(locale, MAIL_CIGMA_ERROR_PERSON_IN_CHARGE)
                , emailToString
                , Constants.SYMBOL_END_PARAGRAPH
                , SYMBOL_NEWLINE));
        }
        
    }
    
    /**
     * 
     * <p>fill Mail Change P/O Detil.</p>
     *
     * @param sbContent StringBuffer
     * @param locale Locale
     * @param domain SpsCigmaDoErrorDomain
     * @throws Exception the Exception
     */
    private void fillChgPoDetail(
        StringBuffer sbContent, 
        Locale locale, 
        TransferChgPoErrorEmailDomain domain) throws Exception {
        
        /* [User Name] [D_CD] [D_PCD] [D_PN] [P/O Type] [Start error delivery date] [S_PCD]
         * [Supplier P/No] [Effective Date] [CIGMA P/O No.]
         */
        String username = nullToEmpty(domain.getCigmaChgPoError().getUpdateByUserId());
        if( EMPTY_STRING.equals(username) ){
            username = SYMBOL_NBSP;
        }
        String dcd = nullToEmpty(domain.getCigmaChgPoError().getDCd());
        if( EMPTY_STRING.equals(dcd) ){
            dcd = SYMBOL_NBSP;
        }
        String dpcd = nullToEmpty(domain.getCigmaChgPoError().getDPcd());
        if( EMPTY_STRING.equals(dpcd) ){
            dpcd = SYMBOL_NBSP;
        }
        String dpn = nullToEmpty(domain.getCigmaChgPoError().getDPn());
        if( EMPTY_STRING.equals(dpn) ){
            dpn = SYMBOL_NBSP;
        }
        String dataType = nullToEmpty(domain.getCigmaChgPoError().getDataType());
        if( EMPTY_STRING.equals(dataType) ){
            dataType = SYMBOL_NBSP;
        }
        String dueDate = format(domain.getCigmaChgPoError().getDueDate(), PATTERN_YYYYMMDD_SLASH);
        if( EMPTY_STRING.equals(dueDate) ){
            dueDate = SYMBOL_NBSP;
        }
        
        // Start : [IN068] Use first date of minimum Issue Month for this Part No.
        // [IN068] apply to Supplier Info Not found and Effective Date Not Match
        //if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(domain.getCigmaChgPoError().getErrorTypeFlg())) {
        SpsCigmaPoErrorDomain minIssueCriteria = new SpsCigmaPoErrorDomain();
        minIssueCriteria.setDCd(domain.getCigmaChgPoError().getDCd());
        minIssueCriteria.setSCd(domain.getCigmaChgPoError().getSCd());
        minIssueCriteria.setDPcd(domain.getCigmaChgPoError().getDPcd());
        minIssueCriteria.setDPn(domain.getCigmaChgPoError().getDPn());
        Date minIssueDate = this.cigmaPoErrorService.searchMinIssueDate(minIssueCriteria);
        Calendar deliveryCal = Calendar.getInstance();
        deliveryCal.setTimeInMillis(minIssueDate.getTime());
        deliveryCal.set(Calendar.DATE, Constants.ONE);
        java.sql.Date firstDayOfMonth = new java.sql.Date(deliveryCal.getTimeInMillis());
        dueDate = StringUtil.appendsString(Constants.SYMBOL_SPAN_RED
            , format(firstDayOfMonth, PATTERN_YYYYMMDD_SLASH)
            , Constants.SYMBOL_END_SPAN);
        //}
        // End : [IN068] Use first date of minimum Issue Month for this Part No.
        
        String spcd = nullToEmpty(domain.getDensoSupplerParts().getSPcd());
        if( EMPTY_STRING.equals(spcd) ){
            spcd = SYMBOL_NBSP;
        }
        String sPn = nullToEmpty(domain.getDensoSupplerParts().getSPn());
        if( EMPTY_STRING.equals(sPn) ){
            sPn = SYMBOL_NBSP;
        }
        String effect = format(domain.getDensoSupplerParts().getEffectDate()
            , PATTERN_YYYYMMDD_SLASH);
        if(null == domain.getDensoSupplerParts().getEffectDate() ||  EMPTY_STRING.equals(effect)) {
            effect = SYMBOL_NBSP;
        }
        
        sbContent.append(appendsString(String.format(
            getEmailLabel(locale , MAIL_CIGMA_CHG_PO_1_ERROR_DETAIL)
            , username
            , dcd
            , dpcd
            , dpn
            , dataType
            , dueDate
            , spcd
            , sPn
            , effect
            , domain.getCigmaChgPoError().getCigmaPoNo())
            , SYMBOL_CR_LF));
    }
    
    /**
     * 
     * <p>send Change P/O Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param sbTitle StringBuffer
     * @param sbFrom StringBuffer
     * @param emailTo List of mail
     * @param companyCd company code
     * @param log to output error message
     * @throws Exception the Exception
     */
    private void sendChgPoMail(
        Locale locale, 
        StringBuffer sbContent, 
        StringBuffer sbTitle, 
        StringBuffer sbFrom, 
        List<String> emailTo,
        String companyCd,
        Log log) throws Exception {
        sbContent.append( Constants.SYMBOL_END_TABLE );
        sbContent.append( 
            appendsString( 
                getEmailLabel(locale
                    , MAIL_CIGMA_CHG_PO_1_ERROR_FOOTER) 
                    /*, SYMBOL_NEWLINE*/, SYMBOL_CR_LF) );
        
        boolean result = sendMail(
            sbTitle.toString(), 
            sbContent.toString(), 
            sbFrom.toString(), 
            emailTo);
        sbContent.delete(0, sbContent.length());
        
        if (!result) {
            // If cannot send email, not throws Exception
            //throwsErrorMessage(locale, ERROR_CD_SP_E5_0002, sCd);
            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                , new String[] {companyCd}
                , log, TWO, false);
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TransferPoDataFromCigmaFacadeService#transactSendNotificationAbnormalDataPo(java.util.Locale, java.util.List, java.util.List, int)
     */
    public List<String> transactSendNotificationAbnormalDataPo(Locale locale
        , List<TransferPoErrorEmailDomain> sendPoError
        , List<TransferPoErrorEmailDomain> sendPoErrorToDenso
        , List<TransferChgPoErrorEmailDomain> sendChgPoError
        , List<TransferChgPoErrorEmailDomain> sendChgPoErrorToDenso
        , int limit, Log log) 
        throws Exception
    {
        List<String> recordErrorList = new ArrayList<String>();
        try{
            String sCd = null; // Supplier Code.
            
            // [IN016] Send email to supplier by plant
            String sPcd = null; 
            String tmpSPcd = null;
            
            String cigmaPoNo = null;
            String errorType = null;
            String dCd = null;
            String dPcd = null;
            int iCount = ZERO;
            boolean bSended = false;
            StringBuffer sbTitle = new StringBuffer( 
                getEmailLabel(locale, MAIL_CIGMA_PO_ERROR_SUBJECT) );
            StringBuffer sbContent = new StringBuffer();
            StringBuffer sbFrom = new StringBuffer();
            List<String> emailTo = new ArrayList<String>();
            UserSupplierDetailDomain criSupplierUser = new UserSupplierDetailDomain();
            criSupplierUser.setSpsMUserDomain( new SpsMUserDomain() );
            criSupplierUser.setSpsMUserSupplierDomain( new SpsMUserSupplierDomain() );
            criSupplierUser.getSpsMUserDomain().setIsActive( STR_ONE );
            criSupplierUser.getSpsMUserDomain().setUserType( Constants.STR_S );
            criSupplierUser.getSpsMUserSupplierDomain().setEmlSInfoNotfoundFlag( STR_ONE );
            sbFrom.append( ContextParams.getDefaultEmailFrom() );

            SpsMUserDensoDomain criDensoUser = new SpsMUserDensoDomain();
            // [IN049] Change column name
            //criDensoUser.setEmlAbnormalTransfer1Flag( STR_ONE );
            criDensoUser.setEmlPedpoDoalcasnUnmatpnFlg( STR_ONE );
            
            /* ------------------------------------------------------------------------------------
             * P/O error Email to Supplier
             * */
            if( null != sendPoError){
                for(TransferPoErrorEmailDomain domain : sendPoError){
                    boolean isSkip = false;
                    if( null == sCd ){
                        emailTo.clear();
                        
                        // [IN016] Send email to supplier by plant
                        //isSkip = findMail(criSupplierUser, domain.getCigmaPoError().getSCd(),
                        //    emailTo);
                        isSkip = findMail(criSupplierUser, domain.getCigmaPoError().getSCd(),
                            domain.getDensoSupplerParts().getSPcd(), emailTo);
                        
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {domain.getCigmaPoError().getSCd()}
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        sCd = domain.getCigmaPoError().getSCd();
                        
                        // [IN016] send email to supplier by plant
                        if (null == domain.getDensoSupplerParts().getSPcd()) {
                            sPcd = Constants.STR_NULL;
                        } else {
                            sPcd = domain.getDensoSupplerParts().getSPcd();
                        }
                        
                        cigmaPoNo = domain.getCigmaPoError().getCigmaPoNo();
                        errorType = domain.getCigmaPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaPoError().getDCd();
                        iCount = ONE;
                        sbContent = new StringBuffer();
                        // [IN070] add list of email to send in content
                        //initialPoMail(locale, sbContent);
                        initialPoMail(locale, sbContent, emailTo);
                        fillPoErrorMail(locale, sbContent, domain);
                        fillPoDetil(sbContent, locale, domain);
                        bSended = false;
                        continue;
                    }

                    // [IN016] send email to supplier by plant
                    //if( !sCd.equals( domain.getCigmaPoError().getSCd() ) ){
                    if (null == domain.getDensoSupplerParts().getSPcd()) {
                        tmpSPcd = Constants.STR_NULL;
                    } else {
                        tmpSPcd = domain.getDensoSupplerParts().getSPcd();
                    }
                    if( !sCd.equals(domain.getCigmaPoError().getSCd())
                        || !sPcd.equals(tmpSPcd))
                    {
                        
                        if (!bSended) {
                            sendPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, sCd, log);
                            bSended = true;
                            emailTo.clear();
                        }

                        // [IN016] Send email to supplier by plant
                        //isSkip = findMail(criSupplierUser, 
                        //    domain.getCigmaPoError().getSCd(), emailTo);
                        isSkip = findMail(criSupplierUser, domain.getCigmaPoError().getSCd()
                            , domain.getDensoSupplerParts().getSPcd(), emailTo);
                        
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {domain.getCigmaPoError().getSCd()}
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        
                        sCd = domain.getCigmaPoError().getSCd();
                        
                        // [IN016] send email to supplier by plant
                        if (null == domain.getDensoSupplerParts().getSPcd()) {
                            sPcd = Constants.STR_NULL;
                        } else {
                            sPcd = domain.getDensoSupplerParts().getSPcd();
                        }
                        
                        cigmaPoNo = domain.getCigmaPoError().getCigmaPoNo();
                        errorType = domain.getCigmaPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaPoError().getDCd();
                        iCount = ONE;
                        // [IN070] add list of email to send in content
                        //initialPoMail(locale, sbContent);
                        initialPoMail(locale, sbContent, emailTo);
                        fillPoErrorMail(locale, sbContent, domain);
                        fillPoDetil(sbContent, locale, domain);
                        bSended = false;
                        continue;
                    } else if( !cigmaPoNo.equals( domain.getCigmaPoError().getCigmaPoNo())
                        || !errorType.equals(domain.getCigmaPoError().getErrorTypeFlg())
                        || !dCd.equals(domain.getCigmaPoError().getDCd()))
                    {
                        if( iCount == limit ){
                            sendPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, sCd, log);
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                            sbContent.append( SYMBOL_NEWLINE );
                        }
                        cigmaPoNo = domain.getCigmaPoError().getCigmaPoNo();
                        errorType = domain.getCigmaPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaPoError().getDCd();
                        sbContent.append( Constants.SYMBOL_START_PARAGRAPH );
                        fillPoErrorMail(locale, sbContent, domain);
                        fillPoDetil(sbContent, locale, domain);
                        iCount++;
                        bSended = false;
                        continue;
                    }
                    
                    fillPoDetil(sbContent, locale, domain);
                    bSended = false;
                } // end for each SPS DO ERROR
                if( !bSended && ZERO < sendPoError.size()){
                    sendPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, sCd, log);
                }
            } // end if SPS DO ERROR
        
            /* ------------------------------------------------------------------------------------
             * P/O error Email to DENSO
             * */
            sCd = null;
            cigmaPoNo = null;
            errorType = null;
            dCd = null;
            dPcd = null;
            bSended = false;
            if (null != sendPoErrorToDenso) {
                for(TransferPoErrorEmailDomain domain : sendPoErrorToDenso) {
                    boolean isSkip = false;
                    if( null == dCd ){
                        isSkip = findDensoMail(criDensoUser, domain.getCigmaPoError().getDCd(),
                            domain.getCigmaPoError().getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getCigmaPoError().getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getCigmaPoError().getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getCigmaPoError().getDCd();
                        dPcd = domain.getCigmaPoError().getDPcd();
                        sCd = domain.getCigmaPoError().getSCd();
                        cigmaPoNo = domain.getCigmaPoError().getCigmaPoNo();
                        errorType = domain.getCigmaPoError().getErrorTypeFlg();
                        iCount = ONE;
                        sbContent = new StringBuffer();
                        // [IN070] add list of email to send in content
                        //initialPoMail(locale, sbContent);
                        initialPoMail(locale, sbContent, emailTo);
                        fillPoErrorMail(locale, sbContent, domain);
                        fillPoDetil(sbContent, locale, domain);
                        bSended = false;
                        continue;
                    }
                    
                    if( !dCd.equals( domain.getCigmaPoError().getDCd())
                        || !dPcd.equals(domain.getCigmaPoError().getDPcd()))
                    {
                        if (!bSended) {
                            sendPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, dCd, log);
                            bSended = true;;
                            emailTo.clear();
                        }
                        
                        isSkip = findDensoMail(criDensoUser, domain.getCigmaPoError().getDCd(),
                            domain.getCigmaPoError().getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getCigmaPoError().getDCd(), 
                                        Constants.SYMBOL_COLON, domain.getCigmaPoError().getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        sCd = domain.getCigmaPoError().getSCd();
                        cigmaPoNo = domain.getCigmaPoError().getCigmaPoNo();
                        errorType = domain.getCigmaPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaPoError().getDCd();
                        dPcd = domain.getCigmaPoError().getDPcd();
                        iCount = ONE;
                        // [IN070] add list of email to send in content
                        //initialPoMail(locale, sbContent);
                        initialPoMail(locale, sbContent, emailTo);
                        fillPoErrorMail(locale, sbContent, domain);
                        fillPoDetil(sbContent, locale, domain);
                        bSended = false;
                        continue;
                    }else if( !cigmaPoNo.equals( domain.getCigmaPoError().getCigmaPoNo())
                        || !errorType.equals(domain.getCigmaPoError().getErrorTypeFlg())
                        || !sCd.equals(domain.getCigmaPoError().getSCd()))
                    {
                        if( iCount == limit ){
                            sendPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, dCd, log);
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                            sbContent.append( SYMBOL_NEWLINE );
                        }
                        cigmaPoNo = domain.getCigmaPoError().getCigmaPoNo();
                        errorType = domain.getCigmaPoError().getErrorTypeFlg();
                        sCd = domain.getCigmaPoError().getSCd();
                        sbContent.append( Constants.SYMBOL_START_PARAGRAPH );
                        fillPoErrorMail(locale, sbContent, domain);
                        fillPoDetil(sbContent, locale, domain);
                        iCount++;
                        bSended = false;
                        continue;
                    }
                    
                    fillPoDetil(sbContent, locale, domain);
                    bSended = false;
                }
                if( !bSended && ZERO < sendPoErrorToDenso.size()){
                    sendPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, dCd, log);
                }
            }
            
            /* ------------------------------------------------------------------------------------
             * Change P/O error Email to Supplier
             * */
            sCd = null;

            // [IN016] send email to supplier by plant
            sPcd = null;
            
            cigmaPoNo = null;
            errorType = null;
            dCd = null;
            bSended = false;
            if( null != sendChgPoError && ZERO < sendChgPoError.size() ){
                sbTitle = new StringBuffer( 
                    getEmailLabel(locale
                        , MAIL_CIGMA_CHG_PO_1_ERROR_SUBJECT) );
                for(TransferChgPoErrorEmailDomain domain : sendChgPoError){
                    boolean isSkip = false;
                    if( null == sCd ){
                        emailTo.clear();
                        
                        // [IN016] Send email to supplier by plant
                        //isSkip = findMail(criSupplierUser,
                        //    domain.getCigmaChgPoError().getSCd(), emailTo);
                        isSkip = findMail(criSupplierUser, domain.getCigmaChgPoError().getSCd()
                            , domain.getDensoSupplerParts().getSPcd(), emailTo);
                        
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {domain.getCigmaChgPoError().getSCd()}
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        sCd = domain.getCigmaChgPoError().getSCd();

                        // [IN016] send email to supplier by plant
                        if (null == domain.getDensoSupplerParts().getSPcd()) {
                            sPcd = Constants.STR_NULL;
                        } else {
                            sPcd = domain.getDensoSupplerParts().getSPcd();
                        }
                        
                        cigmaPoNo = domain.getCigmaChgPoError().getCigmaPoNo();
                        errorType = domain.getCigmaChgPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaChgPoError().getDCd();
                        iCount = ONE;
                        sbContent = new StringBuffer();
                        // [IN070] add list of email to send in content
                        //initialChangePoMail(locale, sbContent);
                        initialChangePoMail(locale, sbContent, emailTo);
                        fillChgPoErrorMail(locale, sbContent, domain);
                        fillChgPoDetail(sbContent, locale, domain);
                        continue;
                    }
                    
                    // [IN016] send email to supplier by plant
                    //if( !sCd.equals( domain.getCigmaChgPoError().getSCd() ) ){
                    if (null == domain.getDensoSupplerParts().getSPcd()) {
                        tmpSPcd = Constants.STR_NULL;
                    } else {
                        tmpSPcd = domain.getDensoSupplerParts().getSPcd();
                    }
                    if( !sCd.equals( domain.getCigmaChgPoError().getSCd())
                        || !sPcd.equals(tmpSPcd))
                    {
                        
                        if (!bSended) {
                            sendChgPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, sCd, log);
                            bSended = true;
                            emailTo.clear();
                        }

                        // [IN016] Send email to supplier by plant
                        //isSkip = findMail(criSupplierUser,
                        //    domain.getCigmaChgPoError().getSCd(), emailTo);
                        isSkip = findMail(criSupplierUser, domain.getCigmaChgPoError().getSCd()
                            , domain.getDensoSupplerParts().getSPcd(), emailTo);
                        
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {domain.getCigmaChgPoError().getSCd()}
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        sCd = domain.getCigmaChgPoError().getSCd();

                        // [IN016] send email to supplier by plant
                        if (null == domain.getDensoSupplerParts().getSPcd()) {
                            sPcd = Constants.STR_NULL;
                        } else {
                            sPcd = domain.getDensoSupplerParts().getSPcd();
                        }
                        
                        cigmaPoNo = domain.getCigmaChgPoError().getCigmaPoNo();
                        errorType = domain.getCigmaChgPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaChgPoError().getDCd();
                        iCount = ONE;
                        // [IN070] add list of email to send in content
                        //initialChangePoMail(locale, sbContent);
                        initialChangePoMail(locale, sbContent, emailTo);
                        fillChgPoErrorMail(locale, sbContent, domain);
                        fillChgPoDetail(sbContent, locale, domain);
                        bSended = false;
                        continue;
                    }else if( !cigmaPoNo.equals( domain.getCigmaChgPoError().getCigmaPoNo())
                        || !errorType.equals(domain.getCigmaChgPoError().getErrorTypeFlg())
                        || !dCd.equals(domain.getCigmaChgPoError().getDCd()))
                    {
                        if( iCount == limit ){
                            sendChgPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, sCd, log);
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                            sbContent.append( SYMBOL_NEWLINE );
                        }
                        cigmaPoNo = domain.getCigmaChgPoError().getCigmaPoNo();
                        errorType = domain.getCigmaChgPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaChgPoError().getDCd();
                        sbContent.append( Constants.SYMBOL_START_PARAGRAPH );
                        fillChgPoErrorMail(locale, sbContent, domain);
                        fillChgPoDetail(sbContent, locale, domain);
                        iCount++;
                        bSended = false;
                        continue;
                    }
                    
                    fillChgPoDetail(sbContent, locale, domain);
                    bSended = false;
                } // end for each SPS DO ERROR
                if( !bSended && ZERO < sendChgPoError.size()){
                    sendChgPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, sCd, log);
                }
            } // end if SPS CHG DO ERROR
            
            /* ------------------------------------------------------------------------------------
             * Change P/O error Email to DENSO
             * */
            sCd = null;
            cigmaPoNo = null;
            errorType = null;
            dCd = null;
            dPcd = null;
            bSended = false;
            if( null != sendChgPoErrorToDenso && ZERO < sendChgPoErrorToDenso.size() ){
                sbTitle = new StringBuffer( 
                    getEmailLabel(locale
                        , MAIL_CIGMA_CHG_PO_1_ERROR_SUBJECT) );
                for(TransferChgPoErrorEmailDomain domain : sendChgPoErrorToDenso){
                    boolean isSkip = false;
                    if( null == dCd ){
                        emailTo.clear();
                        
                        isSkip = findDensoMail(criDensoUser, domain.getCigmaChgPoError().getDCd(),
                            domain.getCigmaChgPoError().getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getCigmaChgPoError().getDCd(), 
                                        Constants.SYMBOL_COLON,
                                        domain.getCigmaChgPoError().getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        dCd = domain.getCigmaChgPoError().getDCd();
                        dPcd = domain.getCigmaChgPoError().getDPcd();
                        sCd = domain.getCigmaChgPoError().getSCd();
                        cigmaPoNo = domain.getCigmaChgPoError().getCigmaPoNo();
                        errorType = domain.getCigmaChgPoError().getErrorTypeFlg();
                        iCount = ONE;
                        sbContent = new StringBuffer();
                        // [IN070] add list of email to send in content
                        //initialChangePoMail(locale, sbContent);
                        initialChangePoMail(locale, sbContent, emailTo);
                        fillChgPoErrorMail(locale, sbContent, domain);
                        fillChgPoDetail(sbContent, locale, domain);
                        continue;
                    }
                    
                    if( !dCd.equals( domain.getCigmaChgPoError().getDCd() )
                        || !dPcd.equals(domain.getCigmaChgPoError().getDPcd()))
                    {
                        if (!bSended) {
                            sendChgPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, dCd, log);
                            bSended = true;
                            emailTo.clear();
                        }
                        
                        isSkip = findDensoMail(criDensoUser, domain.getCigmaChgPoError().getDCd(),
                            domain.getCigmaChgPoError().getDPcd(), emailTo);
                        if (isSkip) {
                            MessageUtil.getErrorMessageForBatch(locale, ERROR_CD_SP_E5_0002
                                , new String[] {
                                    StringUtil.appendsString(domain.getCigmaChgPoError().getDCd(), 
                                        Constants.SYMBOL_COLON,
                                        domain.getCigmaChgPoError().getDPcd())
                                }
                                , log, TWO, false);
                            bSended = true;
                            continue;
                        }
                        sCd = domain.getCigmaChgPoError().getSCd();
                        cigmaPoNo = domain.getCigmaChgPoError().getCigmaPoNo();
                        errorType = domain.getCigmaChgPoError().getErrorTypeFlg();
                        dCd = domain.getCigmaChgPoError().getDCd();
                        dPcd = domain.getCigmaChgPoError().getDPcd();
                        iCount = ONE;
                        // [IN070] add list of email to send in content
                        //initialChangePoMail(locale, sbContent);
                        initialChangePoMail(locale, sbContent, emailTo);
                        fillChgPoErrorMail(locale, sbContent, domain);
                        fillChgPoDetail(sbContent, locale, domain);
                        bSended = false;
                        continue;
                    }else if( !cigmaPoNo.equals( domain.getCigmaChgPoError().getCigmaPoNo())
                        || !errorType.equals(domain.getCigmaChgPoError().getErrorTypeFlg())
                        || !sCd.equals(domain.getCigmaChgPoError().getSCd()))
                    {
                        if( iCount == limit ){
                            sendChgPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, dCd, log);
                            iCount = ZERO;
                        }else{
                            sbContent.append( Constants.SYMBOL_END_TABLE );
                            sbContent.append( SYMBOL_NEWLINE );
                        }
                        cigmaPoNo = domain.getCigmaChgPoError().getCigmaPoNo();
                        errorType = domain.getCigmaChgPoError().getErrorTypeFlg();
                        sCd = domain.getCigmaChgPoError().getSCd();
                        sbContent.append( Constants.SYMBOL_START_PARAGRAPH );
                        fillChgPoErrorMail(locale, sbContent, domain);
                        fillChgPoDetail(sbContent, locale, domain);
                        iCount++;
                        bSended = false;
                        continue;
                    }
                    
                    fillChgPoDetail(sbContent, locale, domain);
                    bSended = false;
                } // end for each SPS DO ERROR
                if( !bSended && ZERO < sendChgPoErrorToDenso.size()){
                    sendChgPoMail(locale, sbContent, sbTitle, sbFrom, emailTo, dCd, log);
                }
            }
        } catch(ApplicationException ae) {
            throw ae;
        } catch(Exception e) {
            throwsErrorMessage(locale, ERROR_CD_SP_E5_0002);
        }
        return recordErrorList;
    }
    
    /**
     * 
     * <p>Convert To Cigma P/O.</p>
     *
     * @param poList List of PseudoCigmaPoInformationDomain
     * @param poCoverList List of PseudoCigmaPoCoverPageInformationDomain
     * @return TransferPoDataFromCigmaResultDomain
     */
    private TransferPoDataFromCigmaResultDomain
    convertToCigmaPo(List<PseudoCigmaPoInformationDomain> poList,
        List<PseudoCigmaPoCoverPageInformationDomain> poCoverList){
        TransferPoDataFromCigmaResultDomain result = new TransferPoDataFromCigmaResultDomain();
        
        List<TransferPoDataFromCigmaDomain> purchaseList 
            = this.convertToCigmaPo(poList);
        // covert P/O Cover page
        for( TransferPoDataFromCigmaDomain purchase : purchaseList ){
            
            if( null == result.getPoDataMap().get(purchase.getCigmaPoNo()) ) {
                result
                    .getPoDataMap()
                        .put(purchase.getCigmaPoNo(), 
                            new ArrayList<TransferPoDataFromCigmaDomain>());
            }
            result.getPoDataMap().get(purchase.getCigmaPoNo()).add(purchase);
        }
        
        
        List<TransferPoCoverPageDataFromCigmaDomain> coverList 
            = convertToCigmaPoCoverPage(poCoverList);
        // covert P/O Cover page
        for( TransferPoCoverPageDataFromCigmaDomain cover : coverList ){
            if( null == result.getCoverPageMap().get(cover.getCigmaPoNo()) ){
                result.getCoverPageMap().put(
                    cover.getCigmaPoNo(), new ArrayList<TransferPoCoverPageDataFromCigmaDomain>());
            }
            result.getCoverPageMap().get(cover.getCigmaPoNo()).add(cover);
        }
        
        return result;
    }
    
    /**
     * 
     * <p>Convert To Cigma P/O.</p>
     *
     * @param poList List of PseudoCigmaPoInformationDomain
     * @return List of TransferPoDataFromCigmaDomain
     */
    private List<TransferPoDataFromCigmaDomain> convertToCigmaPo(
        List<PseudoCigmaPoInformationDomain> poList)
    {
        List<TransferPoDataFromCigmaDomain> resultList = 
            new ArrayList<TransferPoDataFromCigmaDomain>();
        TransferPoDataFromCigmaDomain transfer = null;
        TransferPoDetailDataFromCigmaDomain detail = null;
        TransferPoDueDataFromCigmaDomain due = null;
        
        Long maxCreateDetailDateTime = null;
        Long maxUpdateDetailDateTime = null;
        Long tmpCreateDetailDateTime = null;
        Long tmpUpdateDetailDateTime = null;
        
        Long maxDueDetailDateTimeMonth = null;
        Long minDueDetailDateTimeMonth = null;
        
//        java.util.Date bufferDate = null;
        
        for(PseudoCigmaPoInformationDomain pseudo : poList){
            if( null == transfer 
                || !pseudo.getPseudoVendorCd().equals(transfer.getVendorCd())
                || !pseudo.getPseudoPoNumber().equals(transfer.getCigmaPoNo())
                || !pseudo.getPseudoDPcd().equals(transfer.getDPcd())
                || !pseudo.getPseudoDataType().equals(transfer.getPoType()) 
//                || !pseudo.getPseudoPartNumber().equals(detail.getDPn())
                //|| !pseudo.getPseudoOrderType().equals(transfer.getOrderType())
            ) {
                transfer = new TransferPoDataFromCigmaDomain();
                transfer.setPoType( pseudo.getPseudoDataType() ); // poType
                transfer.setCompanyName( pseudo.getPseudoCompanyName() ); // companyName
                transfer.setVendorCd( pseudo.getPseudoVendorCd() ); // supplierCode
                transfer.setSupplierName( pseudo.getPseudoSupplierName() ); // supplierName
                transfer.setCigmaPoNo( pseudo.getPseudoPoNumber() ); // cigmaPONo
                if( !STR_ZERO.equals(pseudo.getPseudoIssuedDate()) ) {
                    transfer
                        .setIssueDate( parseToSqlDate(pseudo.getPseudoIssuedDate()
                        , PATTERN_YYYYMMDD) ); // issueDate
                }
                if( !STR_ZERO.equals(pseudo.getPseudoDueDateFrom()) ) {
                    transfer.setDueDateFrom( parseToSqlDate(pseudo.getPseudoDueDateFrom()
                        , PATTERN_YYYYMMDD) ); // dueDateFrom
                }
                if( !STR_ZERO.equals(pseudo.getPseudoDueDateTo()) ) {
                    transfer.setDueDateTo( parseToSqlDate(pseudo.getPseudoDueDateTo()
                        , PATTERN_YYYYMMDD) ); // dueDateTo
                }
                transfer.setReleaseNo( toBigDecimal(pseudo.getPseudoReleaseNo()) ); // releaseNo
                transfer.setDPcd( pseudo.getPseudoDPcd() ); // dPlantCode
                transfer.setTr( pseudo.getPseudoTransportationCode() ); // tr
                //transfer.setOrderType( pseudo.getPseudoOrderType() ); // orderType
                if( null == pseudo.getPseudoForceAcknowledgeDate() 
                    || Constants.STR_ZERO.equals(pseudo.getPseudoForceAcknowledgeDate()) ){
                    transfer.setForceAckDate( null );
                } else {
                    transfer
                        .setForceAckDate( 
                            parseToSqlDate(pseudo.getPseudoForceAcknowledgeDate()
                                , PATTERN_YYYYMMDD) ); // forceAckDate
                }
                transfer.setTm( pseudo.getPseudoTm() ); // tm
                transfer.setAddTruckRouteFlag( pseudo.getPseudoAddTruckRouteFlag() ); // addTruckRouteFlag
                transfer.setBlanketPoNo( pseudo.getPseudoBlanketPoNumber() ); // blanketPONo
                
                maxCreateDetailDateTime = null;
                maxUpdateDetailDateTime = null;
                maxDueDetailDateTimeMonth = null;
                minDueDetailDateTimeMonth = null;
                
                transfer.setCigmaPoDetailList( 
                    new ArrayList<TransferPoDetailDataFromCigmaDomain>() );
                resultList.add(transfer);
                
                detail = new TransferPoDetailDataFromCigmaDomain();
            }
            
            if( !pseudo.getPseudoPartNumber().equals(detail.getDPn()) ) {
                
                detail = new TransferPoDetailDataFromCigmaDomain();
                transfer.getCigmaPoDetailList().add(detail);
                detail.setDPn( pseudo.getPseudoPartNumber() ); // dPartNo
                detail.setItemDesc( pseudo.getPseudoItemDescription() ); // itemDesc
                detail.setUnitPrice( toBigDecimal(pseudo.getPseudoUnitPrice()) ); // unitPrice
                detail.setCurrencyCode( pseudo.getPseudoCurrencyCode() ); // currencyCode
                detail.setReceivingDock( pseudo.getPseudoReceivingDock() ); // receivingDock
                detail.setOrderLot( toBigDecimal(pseudo.getPseudoOrderLot()) ); // orderLot
                detail.setQtyBox( toBigDecimal(pseudo.getPseudoQtyBox()) ); // qtyBox
                detail.setUnitOfMeasure( pseudo.getPseudoUnitOfMeasure() ); // unitOfMeasure
                detail.setVariableQtyCode( pseudo.getPseudoVariableQtyCode() ); // variableQtyCode
                detail.setPartLblPrintRemarks( pseudo.getPseudoPartLabelPrintRemarks() ); // partLblPrintRemarks
                detail.setPhaseCode( pseudo.getPseudoPhaseInOutCode() ); // phaseCode
                detail.setPlannerCode( pseudo.getPseudoPlannerCode() ); // plannerCode
                detail.setTmpPriceFlg(pseudo.getPseudoTmpPriceFlg()); // tmpPriceFlg
                
                maxCreateDetailDateTime = null;
                maxUpdateDetailDateTime = null;
                maxDueDetailDateTimeMonth = null;
                minDueDetailDateTimeMonth = null;
                
                detail.setCigmaPoDueList( new ArrayList<TransferPoDueDataFromCigmaDomain>() );
            }
            
            due = new TransferPoDueDataFromCigmaDomain();
            detail.getCigmaPoDueList().add(due);
            due.setOrderType(pseudo.getPseudoOrderType());
            due.setEtd( parseToSqlDate( pseudo.getPseudoEtd(), PATTERN_YYYYMMDD) ); // etd
            
            
            if( null == pseudo.getPseudoDueDate() 
                || Constants.STR_ZERO.equals(pseudo.getPseudoDueDate()) ){
                due.setDueDate( null );
            } else {
                due.setDueDate( parseToSqlDate(pseudo.getPseudoDueDate()
                    , PATTERN_YYYYMMDD) ); // dueDate
            }
            
            due.setOrderQty( toBigDecimal(pseudo.getPseudoOrderQty()) ); // orderQty
            due.setReportType( pseudo.getPseudoReportType() ); // reportType
            due.setSpsFlag( pseudo.getPseudoSpsFlag() ); // spsFlag
            due.setCreateBy( pseudo.getPseudoCreateByUserId() ); // createBy
            due.setCreateDate( pseudo.getPseudoCreateDate() ); // createDate
            due.setCreateTime( pseudo.getPseudoCreateTime() ); // createTime
            due.setUpdateBy( pseudo.getPseudoUpdateByUserId() ); // updateBy
            due.setUpdateDate( pseudo.getPseudoUpdateDate() ); // updateDate
            due.setUpdateTime( pseudo.getPseudoUpdateTime() ); // updateTime
            
            tmpCreateDetailDateTime = 
                Long.parseLong( appendsString(pseudo.getPseudoCreateDate(), 
                    pseudo.getPseudoCreateTime()) );
            if( null != maxCreateDetailDateTime 
                && maxCreateDetailDateTime < tmpCreateDetailDateTime ) {
                maxCreateDetailDateTime = tmpCreateDetailDateTime;
                detail.setMaxCreateDateTime(maxCreateDetailDateTime.toString());
            }else if( null == maxCreateDetailDateTime  ){
                maxCreateDetailDateTime = tmpCreateDetailDateTime;
                detail.setMaxCreateDateTime(maxCreateDetailDateTime.toString());
            }else if( null != transfer.getMaxCreateDateTime() 
                && Long.parseLong(transfer.getMaxCreateDateTime()) < tmpUpdateDetailDateTime  ){
                transfer.setMaxCreateDateTime(tmpUpdateDetailDateTime.toString());
            }else if( null == transfer.getMaxCreateDateTime() ){
                transfer.setMaxCreateDateTime(tmpUpdateDetailDateTime.toString());
            }
                
            tmpUpdateDetailDateTime = 
                Long.parseLong( appendsString(pseudo.getPseudoCreateDate(), 
                    pseudo.getPseudoCreateTime()) );
            if( null != maxUpdateDetailDateTime 
                && maxUpdateDetailDateTime < tmpUpdateDetailDateTime ) {
                maxUpdateDetailDateTime = tmpUpdateDetailDateTime;
                detail.setMaxUpdateDateTime(maxUpdateDetailDateTime.toString());
            }else if( null == maxUpdateDetailDateTime ){
                maxUpdateDetailDateTime = tmpUpdateDetailDateTime;
                detail.setMaxUpdateDateTime(maxUpdateDetailDateTime.toString());
            }else if( null != transfer.getMaxUpdateDateTime() 
                && Long.parseLong(transfer.getMaxUpdateDateTime()) < tmpUpdateDetailDateTime  ){
                transfer.setMaxUpdateDateTime(tmpUpdateDetailDateTime.toString());
            }else if( null == transfer.getMaxUpdateDateTime() ){
                transfer.setMaxUpdateDateTime(tmpUpdateDetailDateTime.toString());
            }
            
            if( Constants.STR_M.equals( due.getReportType() ) ){
                tmpCreateDetailDateTime = Long.parseLong( pseudo.getPseudoDueDate() );
                if( null != maxDueDetailDateTimeMonth
                    && maxDueDetailDateTimeMonth < tmpCreateDetailDateTime ) {
                    maxDueDetailDateTimeMonth = tmpCreateDetailDateTime;
                    detail.setMaxDueDateTimeMonth( 
                        parseToSqlDate(maxDueDetailDateTimeMonth.toString()
                            , PATTERN_YYYYMMDD) );
                }else if( null == maxDueDetailDateTimeMonth ){
                    maxDueDetailDateTimeMonth = tmpCreateDetailDateTime;
                    detail.setMaxDueDateTimeMonth( 
                        parseToSqlDate(maxDueDetailDateTimeMonth.toString()
                            , PATTERN_YYYYMMDD) );
                }
                
                if( null != minDueDetailDateTimeMonth
                    && tmpCreateDetailDateTime < minDueDetailDateTimeMonth ) {
                    minDueDetailDateTimeMonth = tmpCreateDetailDateTime;
                    detail.setMinDueDateTimeMonth( 
                        parseToSqlDate(minDueDetailDateTimeMonth.toString()
                            , PATTERN_YYYYMMDD) );
                }else if( null == minDueDetailDateTimeMonth ){
                    minDueDetailDateTimeMonth = tmpCreateDetailDateTime;
                    detail.setMinDueDateTimeMonth( 
                        parseToSqlDate(minDueDetailDateTimeMonth.toString()
                            , PATTERN_YYYYMMDD) );
                }
                
            }
        } // end for. Header
        
        List<TransferPoDataFromCigmaDomain> returnList 
            = new ArrayList<TransferPoDataFromCigmaDomain>();
        Map<String, TransferPoDataFromCigmaDomain> keyMap 
            = new HashMap<String, TransferPoDataFromCigmaDomain>();
        String strKey = null;
        TransferPoDataFromCigmaDomain buffer = null;
        for(TransferPoDataFromCigmaDomain domain : resultList){
            strKey = StringUtil.appendsString(
                domain.getCigmaPoNo(), domain.getPoType(), domain.getDPcd()//, domain.getOrderType()
            );
            if( null == keyMap.get( strKey )){
                keyMap.put(strKey, domain);
                returnList.add(domain);
                continue;
            }else {
                buffer = keyMap.get( strKey );
            }
            
            buffer.getCigmaPoDetailList().addAll( domain.getCigmaPoDetailList() );
        }
        return returnList;
    }
    
    /**
     * 
     * <p>Convert To Cigma P/O Cover Page.</p>
     *
     * @param doList List of PseudoCigmaPoCoverPageInformationDomain
     * @return List of TransferPoCoverPageDataFromCigmaDomain
     */
    private List<TransferPoCoverPageDataFromCigmaDomain> 
    convertToCigmaPoCoverPage(List<PseudoCigmaPoCoverPageInformationDomain> doList){
        List<TransferPoCoverPageDataFromCigmaDomain> resultList = 
            new ArrayList<TransferPoCoverPageDataFromCigmaDomain>();
        TransferPoCoverPageDataFromCigmaDomain transfer = null;
        TransferPoCoverPageDetailDataFromCigmaDomain detail = null;
        for(PseudoCigmaPoCoverPageInformationDomain pseudo : doList){
            if( null == transfer 
                || !transfer.getCigmaPoNo().equals(pseudo.getPseudoPoNo())
                || !transfer.getPoType().equals(pseudo.getPseudoDataType()) // except data type
                || !transfer.getVendorCd().equals(pseudo.getPseudoVendorCd())
                || !transfer.getDPcd().equals(pseudo.getPseudoDPcd())
            ) { 
                transfer = new TransferPoCoverPageDataFromCigmaDomain();
                transfer.setPotype( pseudo.getPseudoDataType() ); // poType
                transfer.setCigmaPoNo( pseudo.getPseudoPoNo() ); // cigmaPONo
                transfer.setReleaseNo( toBigDecimal(pseudo.getPseudoReleaseNo()) ); // releaseNo
                transfer.setIssueDate( pseudo.getPseudoIssueDate() ); // issueDate
                transfer.setSellerContractName( pseudo.getPseudoSellerContractName() ); // sellerContractName
                transfer.setSellerName( pseudo.getPseudoSellerName() ); // sellerName
                transfer.setSellerAddress1( pseudo.getPseudoSellerAddress1() ); // sellerAddress1
                transfer.setSellerAddress2( pseudo.getPseudoSellerAddress2() ); // sellerAddress2
                transfer.setSellerAddress3( pseudo.getPseudoSellerAddress3() ); // sellerAddress3
                transfer.setSellerFaxNumber( pseudo.getPseudoSellerFaxNumber() ); // sellerFaxNumber
                transfer.setVendorCd( pseudo.getPseudoVendorCd() ); // supplierCode
                transfer.setPaymentTerm( pseudo.getPseudoPaymentTerm() ); // paymentTerm
                transfer.setShipVia( pseudo.getPseudoShipVia() ); // shipVIA
                transfer.setPriceTerm( pseudo.getPseudoPriceTerm() ); // priceTerm
                transfer.setPurchaserContractName( pseudo.getPseudoPurchaserContractName() ); // purchaserContractName
                transfer.setPurchaserName( pseudo.getPseudoPurchaserName() ); // purchaserName
                transfer.setPurchaserAddress1( pseudo.getPseudoPurchaserAddress1() ); // purchaserAddress1
                transfer.setPurchaserAddress2( pseudo.getPseudoPurchaserAddress2() ); // purchaserAddress2
                transfer.setPurchaserAddress3( pseudo.getPseudoPurchaserAddress3() ); // purchaserAddress3
                transfer.setPoAckKnowledge( pseudo.getPseudoPoAckDate() );
                transfer.setDPcd(pseudo.getPseudoDPcd());
                transfer.setCigmaPoCoverPageDetailList( 
                    new ArrayList<TransferPoCoverPageDetailDataFromCigmaDomain>() );
                detail = new TransferPoCoverPageDetailDataFromCigmaDomain();
                transfer.getCigmaPoCoverPageDetailList().add(detail);
                if( Constants.STR_ZERO.equals(pseudo.getPseudoDueDate()) ){
                    detail.setDueDate( null ); // dueDate
                }else {
                    detail.setDueDate( 
                        parseToSqlDate(pseudo.getPseudoDueDate(), PATTERN_YYYYMMDD) ); // dueDate
                }
                detail.setDoNo( pseudo.getPseudoDoNumber() ); // doNo
                detail.setCreateBy( pseudo.getPseudoCreateByPgmId() ); // createBy
                detail.setCreateDate( pseudo.getPseudoCreateDate() ); // createDate
                detail.setCreateTime( pseudo.getPseudoCreateTime() ); // createTime
                detail.setUpdateBy( pseudo.getPseudoUpdateByPgmId() ); // updateBy
                detail.setUpdateDate( pseudo.getPseudoUpdateDate() ); // updateDate
                detail.setUpdateTime( pseudo.getPseudoUpdateTime() ); // updateTime
                
                resultList.add(transfer);
                continue;
            }
            
            detail = new TransferPoCoverPageDetailDataFromCigmaDomain();
            transfer.getCigmaPoCoverPageDetailList().add(detail);
            if( Constants.STR_ZERO.equals(pseudo.getPseudoDueDate()) ){
                detail.setDueDate( null ); // dueDate
            }else {
                detail.setDueDate( 
                    parseToSqlDate(pseudo.getPseudoDueDate(), PATTERN_YYYYMMDD) ); // dueDate
            }
            detail.setDoNo( pseudo.getPseudoDoNumber() ); // doNo
            detail.setCreateBy( pseudo.getPseudoCreateByPgmId() ); // createBy
            detail.setCreateDate( pseudo.getPseudoCreateDate() ); // createDate
            detail.setCreateTime( pseudo.getPseudoCreateTime() ); // createTime
            detail.setUpdateBy( pseudo.getPseudoUpdateByPgmId() ); // updateBy
            detail.setUpdateDate( pseudo.getPseudoUpdateDate() ); // updateDate
            detail.setUpdateTime( pseudo.getPseudoUpdateTime() ); // updateTime
                
        } // end for. Header
        return resultList;
    }
    
    /**
     * 
     * <p>Convert To Cigma Change P/O.</p>
     *
     * @param doList List of PseudoCigmaChangePoInformationDomain
     * @return List of TransferChangePoDataFromCigmaDomain
     */
    private List<TransferChangePoDataFromCigmaDomain> convertToCigmaChangePo(
        List<PseudoCigmaChangePoInformationDomain> doList)
    {
        List<TransferChangePoDataFromCigmaDomain> resultList = 
            new ArrayList<TransferChangePoDataFromCigmaDomain>();
        TransferChangePoDataFromCigmaDomain transfer = null;
        TransferChangePoDetailDataFromCigmaDomain detail = null;
        TransferChangePoDueDataFromCigmaDomain due = null;
        for(PseudoCigmaChangePoInformationDomain pseudo : doList){
            if( null == transfer 
                || !pseudo.getPseudoVendorCd().equals(transfer.getVendorCd())
                || !pseudo.getPseudoPoNumber().equals(transfer.getCigmaPoNo())
                || !pseudo.getPseudoIssuedDate().equals( 
                    format(transfer.getIssueDate(), PATTERN_YYYYMMDD) )
                || !pseudo.getPseudoDataType().equals(transfer.getPoType()) 
                || !pseudo.getPseudoDPcd().equals(transfer.getDPcd())
//                || !pseudo.getPseudoPartNumber().equals(detail.getDPn())
                || !pseudo.getPseudoOrderType().equals(transfer.getOrderType())
            ) {
            
                transfer = new TransferChangePoDataFromCigmaDomain();
                transfer.setPoType( pseudo.getPseudoDataType() ); // poType
                transfer.setCompanyName( pseudo.getPseudoCompanyName() ); // companyName
                transfer.setVendorCd( pseudo.getPseudoVendorCd() ); // supplierCode
                transfer.setSupplierName( pseudo.getPseudoSupplierName() ); // supplierName
                transfer.setCigmaPoNo( pseudo.getPseudoPoNumber() ); // cigmaPONo
                transfer.setIssueDate( parseToSqlDate(pseudo.getPseudoIssuedDate()
                    , PATTERN_YYYYMMDD) ); // issueDate
                transfer.setDueDateFrom( parseToSqlDate(pseudo.getPseudoDueDateFrom()
                    , PATTERN_YYYYMMDD) ); // dueDateFrom
                transfer.setDueDateTo( parseToSqlDate(pseudo.getPseudoDueDateTo()
                    , PATTERN_YYYYMMDD) ); // dueDateTo
                transfer.setReleaseNo( toBigDecimal(pseudo.getPseudoReleaseNo()) ); // releaseNo
                transfer.setDPcd( pseudo.getPseudoDPcd() ); // dPlantCode
                transfer.setTr( pseudo.getPseudoTransportationCode() ); // tr
                transfer.setOrderType( pseudo.getPseudoOrderType() ); // orderType
                transfer.setForceAckDate( parseToSqlDate(pseudo.getPseudoForceAcknowledgeDate()
                    , PATTERN_YYYYMMDD) ); // forceAckDate
                transfer.setTm( pseudo.getPseudoTm() ); // tm
                transfer.setAddTruckRouteFlag( pseudo.getPseudoAddTruckRouteFlag() );
    //                transfer.setAddTruckRouteFlag( pseudo.getPseudoAddTruckRouteFlag() ); // addTruckRouteFlag
    //                transfer.setBlanketPoNo( pseudo.getPseudoBlanketPONumber() ); // blanketPONo
                
                transfer.setCigmaPoDetailList(
                    new ArrayList<TransferChangePoDetailDataFromCigmaDomain>());
                detail = new TransferChangePoDetailDataFromCigmaDomain();
                resultList.add(transfer);
            }
            
            if( !pseudo.getPseudoPartNumber().equals(detail.getDPn()) ) {
                detail = new TransferChangePoDetailDataFromCigmaDomain();
                transfer.getCigmaPoDetailList().add(detail);
                detail.setDPn( pseudo.getPseudoPartNumber() ); // dPartNo
                detail.setPlannerCode( pseudo.getPseudoPlannerCode() ); // plannerCode
                detail.setWhPrimeReceiving( pseudo.getPseudoWarehouseForPrimeReceiving() ); // W/H
                detail.setLocation( pseudo.getPseudoLocation() );
                detail.setUnitOfMeasure( pseudo.getPseudoUnitOfMeasure() ); // unitOfMeasure
                detail.setItemDesc( pseudo.getPseudoItemDescription() ); // itemDesc
                detail.setModel( pseudo.getPseudoEngineeringDrawingNumber() );
                detail.setTmpPriceFlg(pseudo.getPseudoTmpPriceFlg()); // tmpPriceFlg
                detail.setCigmaPoDueList(new ArrayList<TransferChangePoDueDataFromCigmaDomain>());
            }
            
            due = new TransferChangePoDueDataFromCigmaDomain();
            detail.getCigmaPoDueList().add(due);
            due.setEtd( parseToSqlDate( pseudo.getPseudoEtd(), PATTERN_YYYYMMDD) ); // etd
            if( !STR_ZERO.equals(pseudo.getPseudoDueDate()) ) {
                due.setDueDate( parseToSqlDate(pseudo.getPseudoDueDate()
                    , PATTERN_YYYYMMDD) ); // dueDate
            }
            due.setOldQty( toBigDecimal(pseudo.getPseudoOldQty()) ); // orderQty
            due.setNewQty( toBigDecimal(pseudo.getPseudoNewQty()) ); // orderQty
            due.setDifferenceQty( toBigDecimal(pseudo.getPseudoDifferenceQty()) ); // orderQty
            due.setReportType( pseudo.getPseudoReportType() ); // reportType
            due.setDel(pseudo.getPseudoDel());
            due.setSeq(toBigDecimal(pseudo.getPseudoSeq()));
            
            due.setSpsFlag( pseudo.getPseudoSpsFlag() ); // spsFlag
            due.setReason( pseudo.getPseudoReason() );
            due.setCreateBy( pseudo.getPseudoCreateByUserId() ); // createBy
            due.setCreateDate( pseudo.getPseudoCreateDate() ); // createDate
            due.setCreateTime( pseudo.getPseudoCreateTime() ); // createTime
            due.setUpdateBy( pseudo.getPseudoUpdateByUserId() ); // updateBy
            due.setUpdateDate( pseudo.getPseudoUpdateDate() ); // updateDate
            due.setUpdateTime( pseudo.getPseudoUpdateTime() ); // updateTime
        } // end for. Header
        return resultList;
    }

    /**
     * <p>Fill Do Error Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param domain domain
     * @throws Exception exception
     */
    private void fillPoErrorMail(Locale locale, StringBuffer sbContent, 
        TransferPoErrorEmailDomain domain) throws Exception {

        if (ERROR_TYPE_VENDOR_NOT_FOUND.equals(domain.getCigmaPoError().getErrorTypeFlg())) {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND));
        } else if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(
            domain.getCigmaPoError().getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND)
                .replaceAll("#transferProcess#", Constants.PO));
        } else if (ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(
            domain.getCigmaPoError().getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale
                , MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE)
                .replaceAll("#transferProcess#", Constants.PO));
        }
        
        // #vendorCd# #sCd# #dCd# #issueDate#
        sbContent.append(
            getEmailLabel(locale, MAIL_CIGMA_PO_ERROR_HEADER3)
                .replaceAll(MAIL_VENDOR_CD_REPLACEMENT, domain.getCigmaPoError().getVendorCd())
                .replaceAll(MAIL_SCD_REPLACEMENT, domain.getCigmaPoError().getSCd()) 
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getCigmaPoError().getDCd())
                .replaceAll(MAIL_ISSUE_DATE_REPLACEMENT, 
                    format(domain.getCigmaPoError().getIssueDate(), PATTERN_YYYYMMDD_SLASH)));
        
        sbContent.append( appendsString(
            SYMBOL_NEWLINE
            , getEmailLabel(locale, MAIL_CIGMA_PO_ERROR_HEADER_TABLE3) 
            , SYMBOL_CR_LF));
    }
    
    /**
     * 
     * <p>Fill Chg P/O Error Mail.</p>
     *
     * @param locale Locale
     * @param sbContent StringBuffer
     * @param domain domain
     * @throws Exception exception
     */
    private void fillChgPoErrorMail(Locale locale, StringBuffer sbContent,
        TransferChgPoErrorEmailDomain domain) throws Exception
    {
        if (ERROR_TYPE_VENDOR_NOT_FOUND.equals(domain.getCigmaChgPoError().getErrorTypeFlg())) {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND));
        } else if (ERROR_TYPE_PARTS_NO_NOT_FOUND.equals(
            domain.getCigmaChgPoError().getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale, MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND)
                .replaceAll("#transferProcess#", Constants.CHG_PO));
        } else if (ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE.equals(
            domain.getCigmaChgPoError().getErrorTypeFlg()))
        {
            sbContent.append(getEmailLabel(locale
                , MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE)
                .replaceAll("#transferProcess#", Constants.CHG_PO));
        }
        
        sbContent.append(MessageUtil
            .getEmailLabel(locale, MAIL_CIGMA_CHG_PO_1_ERROR_HEADER3)
                .replaceAll(MAIL_VENDOR_CD_REPLACEMENT, domain.getCigmaChgPoError().getVendorCd())
                .replaceAll(MAIL_SCD_REPLACEMENT, domain.getCigmaChgPoError().getSCd()) 
                .replaceAll(MAIL_DCD_REPLACEMENT, domain.getCigmaChgPoError().getDCd())
                .replaceAll(MAIL_ISSUE_DATE_REPLACEMENT, 
                    format(domain.getCigmaChgPoError().getIssueDate(), PATTERN_YYYYMMDD_SLASH)));

        sbContent.append(appendsString(
            SYMBOL_NEWLINE
            , getEmailLabel(locale, MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE3)
            , SYMBOL_CR_LF));
    }

    /**
     * 
     * <p>call send email method.</p>
     *
     * @param title String
     * @param content String
     * @param emailFrom String
     * @param emailTo List of mail
     * @return can send?
     * @throws Exception exception
     */
    private boolean sendMail(String title, String content, String emailFrom, List<String> emailTo)
        throws Exception {
        SendEmailDomain email = new SendEmailDomain();
        email.setHeader( title );
        email.setEmailSmtp(ContextParams.getEmailSmtp());
        email.setContents( content );
        email.setEmailFrom( emailFrom );
        
        // Start : [IN069] loop send email for each email in to list
        //email.setEmailTo(Constants.EMPTY_STRING);
        //email.setToList( emailTo );
        //return mailService.sendEmail(email);
        boolean result = true;
        for (String receiver : emailTo) {
            email.setEmailTo(receiver);
            if (!mailService.sendEmail(email)) {
                result = false;
            }
        }
        return result;
        // End : [IN069] loop send email for each email in to list
    }
    
    
    /**
     * 
     * <p>Transfer Chang P/O From Cigma.</p>
     *
     * @param locale locale
     * @param transferList List of TransferChangePoDataFromCigmaDomain
     * @param as400Server serverConfg
     * @param bError isError
     * @param jobId jobId
     * @return TransferChangePoDataFromCigmaDomain
     * @throws Exception exception
     */
    private List<TransferChangePoDataFromCigmaDomain> transferChangPoFromCigma(
        Locale locale, List<TransferChangePoDataFromCigmaDomain> transferList,  
        As400ServerConnectionInformationDomain as400Server, boolean bError,
        String jobId) throws Exception{
        
        Timestamp current = commonService.searchSysDate();
        Map<TransferChangePoDataFromCigmaDomain, BigDecimal> idMap 
            = new HashMap<TransferChangePoDataFromCigmaDomain, BigDecimal>();
//        String result = EMPTY_STRING;
        BigDecimal poId = null;
//        String strPoType = transferList.get(ZERO).getPoType();
//        String strPoNo = SupplierPortalConstant.SPS_PO_NO_START;
        for( TransferChangePoDataFromCigmaDomain transfer : transferList ){
//            if( !strPoType.equals( transfer.getPoType() ) ) {
//                strPoType = transfer.getPoType();
//                strPoNo = NumberUtil.increaseNumber(strPoNo, NumberUtil.REVISION_FORMAT);
//            }
            // 2-1) Separate CIGMA P/O to SPS P/O by Supplier Plant Code and Data Type
            SpsTPoDomain poDomain = new SpsTPoDomain();
            poId = insertChangePo(locale, 
                as400Server.getSpsMCompanyDensoDomain(), 
                transfer, 
                poDomain,
                current);
            // 2-1-2) LOOP-2 until end of <List> cigmaDOInformation. cigmaDODetail
            for(TransferChangePoDetailDataFromCigmaDomain transferDetail 
                : transfer.getCigmaPoDetailList()) {
                
                // insert Po Detail
                insertChangePoDetail(locale, poId, poDomain, transfer, transferDetail, current);
                // insert Po Due
                insertChangePoDue(locale, poId, transfer, transferDetail, current);
            } // end for detail.
            
            idMap.put(transfer, poId);
            
        } 
        // end for.
        // 20180410 Don't generate BLOB file
//        for( TransferChangePoDataFromCigmaDomain transfer : transferList ){
//            
//            poId = idMap.get(transfer);
//            // insert P/O Cover Page
//            generateChangePurchaseOrder(
//                locale, 
//                poId, 
//                transfer.getCigmaPoNo(), 
//                jobId);
//            
//            if( null != result && ZERO < result.length() ){
//                break;
//            }
//        }
        // end for.
        String partNoError = CommonWebServiceUtil.updateCigmaChangePurchaseOrder(as400Server, 
            transferList, STR_ONE, jobId);
        if( !StringUtil.checkNullOrEmpty(partNoError) ){
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0018,
                STR_ONE,
                Constants.CHG_PO);
        } // end if
        
        return transferList;
    }
    
    /**
     * Create record for Part Number not found error in SPS_CIGMA_PO_ERROR.
     * @param partCriteria criteria for search part
     * @param doDetail part information
     * @param locale for get properties
     * @param as400Server AS400 server information
     * @param transfer PO information
     * @param poDue Due information
     * @param sbErrorMsg error message buffer
     * @throws ApplicationException ApplicationException
     * */
    private void partNumberNotFoundError(SpsMDensoSupplierPartsCriteriaDomain partCriteria,
        TransferPoDetailDataFromCigmaDomain doDetail, Locale locale,
        As400ServerConnectionInformationDomain as400Server, TransferPoDataFromCigmaDomain transfer,
        TransferPoDueDataFromCigmaDomain poDue, StringBuffer sbErrorMsg)
        throws ApplicationException
    {
        String cause = ERROR_TYPE_PARTS_NO_NOT_FOUND;
        partCriteria.setEffectDateLessThanEqual(null);
        List<SpsMDensoSupplierPartsDomain> effectList = findPart(partCriteria,
            doDetail.getDPn(), SupplierPortalConstant.SORT_PART_MASTER_ASC);
        
        if (null != effectList) {
            cause = ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
        }
        
        insertCigmaPoError(locale, as400Server.getSpsMCompanyDensoDomain(), 
            transfer, doDetail, poDue, STR_ONE, cause);
        
        sbErrorMsg.append(getMessage(locale, ERROR_CD_SP_E6_0015,
            transfer.getCigmaPoNo(), doDetail.getDPn()))
            .append( SYMBOL_CR_LF );
    }
    
    
    
    
    /**
     * 
     * <p>insert Cigma Po Error.</p>
     *
     * @param locale locale
     * @param criteria criteria
     * @param transfer Transfer P/O Data
     * @param transferDetail Transfer P/O Detail
     * @param transferDue Transfer P/O Due
     * @param mailFlag mailFlag
     * @param errorType ERROR_TYPE_FLG
     * @throws ApplicationException exception
     */
    private void insertCigmaPoError( Locale locale, SpsMCompanyDensoDomain criteria
        , TransferPoDataFromCigmaDomain transfer
        , TransferPoDetailDataFromCigmaDomain transferDetail
        , TransferPoDueDataFromCigmaDomain transferDue
        , String mailFlag, String errorType ) throws ApplicationException 
    {
        try {
            SpsCigmaPoErrorDomain errorPo = new SpsCigmaPoErrorDomain();
            errorPo.setCigmaPoNo( transfer.getCigmaPoNo() );
            errorPo.setDataType( transfer.getPoType() );
            if( null != transfer.getSCd() ){
                errorPo.setSCd( transfer.getSCd() );
            }else{
                errorPo.setSCd( null );
            }
            errorPo.setIssueDate( transfer.getIssueDate() );
            errorPo.setMailFlg( mailFlag );
            errorPo.setErrorTypeFlg(errorType);
            errorPo.setDCd( criteria.getDCd().trim() );
            errorPo.setDPcd( transfer.getDPcd() );
            errorPo.setVendorCd( transfer.getVendorCd() );
            if (null != transferDetail) {
                errorPo.setDPn( transferDetail.getDPn() );
            }
            if (null != transferDue) {
                errorPo.setUpdateByUserId( transferDue.getUpdateBy() );
                errorPo.setSPcd( transferDue.getSPcd() );
                errorPo.setEtd( transferDue.getEtd() );
                errorPo.setOrderQty( transferDue.getOrderQty() );
                if( null == transferDue.getDueDate() ) {
                    errorPo.setDueDate( transferDue.getEtd() );
                } else {
                    errorPo.setDueDate( transferDue.getDueDate() );
                }
            }
            spsCigmaPoErrorService.create(errorPo);
        } catch (Exception e) {
            String dPn = EMPTY_STRING;
            String etd = EMPTY_STRING;
            if (null != transferDetail) {
                dPn = transferDetail.getDPn();
            }
            if (null != transferDue) {
                etd = DateUtil.format(transferDue.getEtd(), DateUtil.PATTERN_YYYYMMDD_DASH);
            }
            
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0019, null, 
                new String[]{Constants.PO, transfer.getCigmaPoNo(), dPn, etd,
                    SupplierPortalConstant.SPS_CIGMA_PO_ERROR
                } );
        }
    }
    
    /**
     * Delete error record in SPS_CIGMA_PO_ERROR for complete or update transfer status.
     * @param dCd - DENSO company code
     * @param locale - for get message
     * @param vendorCd - CIGMA Vendor Code
     * @throws Exception when catch Exception
     * */
    private void deletePoError(String dCd, Locale locale
        // [IN056] : Add Vendor Code as criteria
        , String vendorCd
    ) throws Exception {
        SpsCigmaPoErrorCriteriaDomain errorPomain = new SpsCigmaPoErrorCriteriaDomain();
        errorPomain.setDCd(dCd);
        // [IN056] : Add Vendor Code as criteria
        errorPomain.setVendorCd(vendorCd);
        
        try {
            spsCigmaPoErrorService.deleteByCondition(errorPomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, ERROR_CD_SP_E6_0023,
                EMPTY_STRING, EMPTY_STRING,
                TABLE_CIGMA_PO_NAME );
        }
    }
    
    
    /**
     * 
     * <p>insert Po.</p>
     *
     * @param locale locale
     * @param criteria criteria
     * @param transfer transferData
     * @param cover TransferPoCoverPageDataFromCigmaDomain
     * @param poDomain poDomain
     * @param current current time
     * @param orderType Order Type (OTY420)
     * @return P/O ID
     * @throws ApplicationException exception
     */
    private BigDecimal insertPo(Locale locale,
        SpsMCompanyDensoDomain criteria, 
        TransferPoDataFromCigmaDomain transfer,
        TransferPoCoverPageDataFromCigmaDomain cover,
        SpsTPoDomain poDomain,
        Timestamp current,
        String orderType) throws ApplicationException {
        BigDecimal poId = null;
        try {
            // 2-1-1) For the same cigmaDONo, supplierCode, supplierPlantCode,

            if( STR_C.equals( orderType ) ) {
                poDomain
                    .setSpsPoNo(
                        appendsString(
                            transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
                            transfer.getCigmaPoNo()));
                poDomain.setCigmaPoNo(transfer.getCigmaPoNo());
            } else {
                if( Constants.DPO_TYPE.equals(transfer.getPoType()) 
                    || Constants.DKO_TYPE.equals(transfer.getPoType()) ) {
                    poDomain
                        .setSpsPoNo(
                            appendsString(
                                transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
                                ContextParams.getDefaultCigmaPoNo()));
                    poDomain.setCigmaPoNo( ContextParams.getDefaultCigmaPoNo() );
                } else {
                    poDomain
                        .setSpsPoNo(
                            appendsString(
                                transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
                                transfer.getBlanketPoNo()));
                    poDomain.setCigmaPoNo( transfer.getBlanketPoNo() );
                }
            }
            poDomain.setRefCigmaPoNo(transfer.getCigmaPoNo());
            
            poDomain.setPoType(transfer.getPoType());
            poDomain.setPoIssueDate(transfer.getIssueDate());
            if (STR_Y.equals(transfer.getAddTruckRouteFlag())) {
                poDomain.setPoStatus(SupplierPortalConstant.FAC);
            } else {
                poDomain.setPoStatus(Constants.ISS_TYPE);
            }
            poDomain.setVendorCd( transfer.getVendorCd() );
            poDomain.setSCd(transfer.getSCd());
            poDomain.setSPcd(transfer.getSPcd());
            poDomain.setDCd(criteria.getDCd());
            poDomain.setDPcd(transfer.getDPcd());
            poDomain.setTm(transfer.getTm());

            if ( STR_C.equals( orderType ) ) {
                poDomain.setPeriodType(STR_ZERO);
            } else {
                poDomain.setPeriodType(STR_ONE);
            }
            if (Constants.DPO_TYPE.equals(transfer.getPoType())
                || Constants.IPO_TYPE.equals(transfer.getPoType())) {
                poDomain.setOrderMethod(STR_ZERO);
            } else {
                poDomain.setOrderMethod(STR_ONE);
            }
            poDomain.setForceAckDate( parseToSqlDate(cover.getPoAckKnowledge(), PATTERN_YYYYMMDD) );
            //transfer.getForceAckDate());
            poDomain.setChangeFlg(STR_ZERO);
            poDomain.setPdfOriginalFileId(null);
            poDomain.setPdfChangeFileId(null);
            poDomain.setDueDateFrom(transfer.getDueDateFrom());
            poDomain.setDueDateTo(transfer.getDueDateTo());
            poDomain.setReleaseNo(transfer.getReleaseNo());
            poDomain.setFax(null);
            if( STR_Y.equals(transfer.getAddTruckRouteFlag()) ){
                poDomain.setNewTruckRouteFlg( STR_ONE );
            }else{
                poDomain.setNewTruckRouteFlg( STR_ZERO );
            }
            
            String createId = null;
            String updateId = null;
            Long createTime = null;
            Long updateTime = null;
            Long createBuffer = null;
            Long updateBuffer = null;
            
            for( TransferPoDetailDataFromCigmaDomain detail : transfer.getCigmaPoDetailList() ) {
                for( TransferPoDueDataFromCigmaDomain due : detail.getCigmaPoDueList() ) {
                    if (!orderType.equals(due.getOrderType())) {
                        continue;
                    }
                    
                    createBuffer = Long.parseLong( appendsString(
                        due.getCreateDate(), due.getCreateTime() ) );
                    updateBuffer = Long.parseLong( appendsString(
                        due.getUpdateDate(), due.getUpdateTime() ) );
                    
                    if( null == createTime || createTime < createBuffer ){
                        createTime = createBuffer;
                        createId = due.getCreateBy();
                    }
                    
                    if( null == updateTime || updateTime < updateBuffer ){
                        updateTime = updateBuffer;
                        updateId = due.getUpdateBy();
                    }
                }
            }
            
            poDomain.setCreateDscId( createId );
            poDomain.setCreateDatetime( 
                parseToTimestamp(createTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDomain.setLastUpdateDscId( updateId );
            poDomain.setLastUpdateDatetime( current );
                
            poId = purchaseOrderService.createPurchaseOrder(poDomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0016,
                transfer.getCigmaPoNo(),
                EMPTY_STRING,
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_PO) );
        }
        return poId;
    }
    
    
    
    /**
     * 
     * <p>insert P/O Detail.</p>
     *
     * @param locale locale
     * @param poId poId
     * @param poDomain poDomain
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param current current time
     * @param orderType Order Type (OTY420)
     * @throws ApplicationException exception
     */
    private void insertPoDetail(Locale locale, BigDecimal poId, SpsTPoDomain poDomain
        , TransferPoDataFromCigmaDomain transfer
        , TransferPoDetailDataFromCigmaDomain transferDetail
        , Timestamp current
        , String orderType
    ) throws ApplicationException{
        try {
            SpsTPoDetailDomain poDetailDomain = new SpsTPoDetailDomain();
            poDetailDomain.setPoId( poId );
            poDetailDomain.setSPn( transferDetail.getSPn() );
            poDetailDomain.setDPn( transferDetail.getDPn() );
            poDetailDomain.setPnStatus( poDomain.getPoStatus() );
            poDetailDomain.setItemDesc( transferDetail.getItemDesc() );
            poDetailDomain.setQtyBox( transferDetail.getQtyBox() );
            poDetailDomain.setUnitPrice( transferDetail.getUnitPrice() );
            poDetailDomain.setCurrencyCode( transferDetail.getCurrencyCode() );
            poDetailDomain.setPnLblPrintRemark( transferDetail.getPartLblPrintRemarks() );
            poDetailDomain.setPlannerCode( transferDetail.getPlannerCode() );
            poDetailDomain.setWhPrimeReceiving( null );
            poDetailDomain.setLocation( null );
            poDetailDomain.setPhaseCode( transferDetail.getPhaseCode() );
            poDetailDomain.setReceivingDock( transferDetail.getReceivingDock() );
            poDetailDomain.setVariableQtyCode( transferDetail.getVariableQtyCode() );
            poDetailDomain.setOrderLot( transferDetail.getOrderLot() );
            poDetailDomain.setRcvLane( null );
            poDetailDomain.setTmpPriceFlg(transferDetail.getTmpPriceFlg());

            Date minDate = null;
            Date maxDate = null;
            String createId = null;
            String updateId = null;
            Long createTime = null;
            Long updateTime = null;
            Long createBuffer = null;
            Long updateBuffer = null;
            int countDueMatch = ZERO;
            for( TransferPoDueDataFromCigmaDomain due : transferDetail.getCigmaPoDueList() ){
                if (!orderType.equals(due.getOrderType())) {
                    continue;
                }
                
                createBuffer = Long.parseLong( appendsString(
                    due.getCreateDate(), due.getCreateTime() ) );
                updateBuffer = Long.parseLong( appendsString(
                    due.getUpdateDate(), due.getUpdateTime() ) );
                
                if( null == createTime || createTime < createBuffer ){
                    createTime = createBuffer;
                    createId = due.getCreateBy();
                }
                
                if( null == updateTime || updateTime < updateBuffer ){
                    updateTime = updateBuffer;
                    updateId = due.getUpdateBy();
                }
                
                countDueMatch++;
                
                if( Constants.STR_M.equals(due.getReportType())) {
                    continue;
                }
                
                if( null == minDate ){
                    minDate = due.getEtd();
                }
                
                if( null == maxDate ){
                    maxDate = due.getEtd();
                }
                
                if( minDate.after(due.getEtd()) ){
                    minDate = due.getEtd();
                }
                if( maxDate.before(due.getEtd()) ){
                    maxDate = due.getEtd();
                }
            }
            
            if (ZERO == countDueMatch) {
                return;
            }
            
            if (null == transferDetail.getStartPeriodDate()) {
                poDetailDomain.setStartPeriodDate( minDate );
            } else {
                poDetailDomain.setStartPeriodDate(transferDetail.getStartPeriodDate());
            }
            /*ISSUE END_PERIOD_DATE CASE Firm 20230529
            if (null == transferDetail.getEndPeriodDate()) {
                if (STR_C.equals(orderType)) {
                    poDetailDomain.setEndPeriodDate( maxDate );
                } else {
                    poDetailDomain.setEndPeriodDate( transfer.getDueDateTo() );
                }
            } else {
                poDetailDomain.setEndPeriodDate( transferDetail.getEndPeriodDate() );
            }
            */
            if (STR_C.equals(orderType)) {
            	poDetailDomain.setEndPeriodDate( maxDate );
            }else{
            	if (null == transferDetail.getEndPeriodDate()) {
                    if (!STR_C.equals(orderType)) {
                    	poDetailDomain.setEndPeriodDate( transfer.getDueDateTo() );
                    } 
                } else {
                    poDetailDomain.setEndPeriodDate( transferDetail.getEndPeriodDate() );
                }
            }
            poDetailDomain.setEndFirmDate(transferDetail.getEndFirm());
            poDetailDomain.setUnitOfMeasure( transferDetail.getUnitOfMeasure() );
            poDetailDomain.setModel( null );

            poDetailDomain.setCreateDscId( createId );
            poDetailDomain.setCreateDatetime( 
                parseToTimestamp(createTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDetailDomain.setLastUpdateDscId( updateId );
//            poDetailDomain.setLastUpdateDatetime( 
//                parseToTimestamp(updateTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDetailDomain.setLastUpdateDatetime( current );
            spsTPoDetailService.create(poDetailDomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0016,
                transfer.getCigmaPoNo(),
                transferDetail.getDPn(),
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_PO_DETAIL) );
        }
    }
    
    /**
     * 
     * <p>insert Po Due.</p>
     *
     * @param locale locale
     * @param poId poId
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param current current time
     * @param orderType Order Type (OTY420)
     * @throws ApplicationException exception
     */
    private void insertPoDue(Locale locale, BigDecimal poId
        , TransferPoDataFromCigmaDomain transfer
        , TransferPoDetailDataFromCigmaDomain transferDetail
        , Timestamp current
        , String orderType) throws ApplicationException{
        Date dueDate = null;
        try {
            int dueSeq = Constants.ONE;
            for( TransferPoDueDataFromCigmaDomain transferDue 
                : transferDetail.getCigmaPoDueList() )
            {
                if (!orderType.equals(transferDue.getOrderType())) {
                    continue;
                }
                SpsTPoDueDomain poDueDomain = new SpsTPoDueDomain();
                poDueDomain.setPoId( poId );
                poDueDomain.setSPn( transferDetail.getSPn() );
                poDueDomain.setDPn(transferDetail.getDPn());
                poDueDomain.setDueId( toBigDecimal(dueSeq++) );
                dueDate = transferDue.getEtd();
                poDueDomain.setDueDate( transferDue.getDueDate() );
                if( Constants.STR_D.equals( transferDue.getReportType() ) ){
                    poDueDomain.setReportTypeFlg( STR_ZERO );
                }else if( Constants.STR_W.equals( transferDue.getReportType() ) ){
                    poDueDomain.setReportTypeFlg( STR_ONE );
                }else if( Constants.STR_M.equals( transferDue.getReportType() ) ){
                    poDueDomain.setReportTypeFlg( Constants.STR_TWO );
                }
                poDueDomain.setOrderQty( transferDue.getOrderQty() );
                poDueDomain.setEtd( transferDue.getEtd() );
                poDueDomain.setPreviousQty( Constants.BIG_DECIMAL_ZERO );
                poDueDomain.setDifferenceQty( Constants.BIG_DECIMAL_ZERO );
                poDueDomain.setRsn(null);
                poDueDomain.setSpsProposedDueDate(null);
                poDueDomain.setSpsProposedQty(null);
                poDueDomain.setSpsPendingReasonCd(null);
                poDueDomain.setMarkPendingFlg( STR_ZERO );
                poDueDomain.setCreateDscId( transferDue.getCreateBy() );
                poDueDomain.setCreateDatetime( 
                    parseToTimestamp( 
                        appendsString(transferDue.getCreateDate(), 
                            transferDue.getCreateTime()), PATTERN_YYYYMMDD_HHMMSS ) );
                poDueDomain.setLastUpdateDscId( transferDue.getCreateBy() );
                poDueDomain.setLastUpdateDatetime( current );
                
                spsTPoDueService.create(poDueDomain);
            }
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0016, 
                transfer.getCigmaPoNo(),
                transferDetail.getDPn(),
                format(dueDate, PATTERN_YYYYMMDD_SLASH),
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_PO_DUE) );
        }
    }
    
    /**
     * 
     * <p>insert Change P/O.</p>
     *
     * @param locale locale
     * @param criteria criteria
     * @param transfer transferData
     * @param poDomain poDomain
     * @param current current time
     * @return P/O ID
     * @throws ApplicationException exception
     */
    private BigDecimal insertChangePo(Locale locale, 
        SpsMCompanyDensoDomain criteria, 
        TransferChangePoDataFromCigmaDomain transfer,
        SpsTPoDomain poDomain,
        Timestamp current ) throws ApplicationException {
        BigDecimal poId = null;
        try {
            // 2-1-1) For the same cigmaDONo, supplierCode, supplierPlantCode,
            // dataType insert data from PurchaseOrderService
//            poDomain
//                .setSpsPoNo(
//                    appendsString(
//                        transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
//                        transfer.getCigmaPoNo(),
//                    strPoNo));
//            poDomain.setCigmaPoNo(transfer.getCigmaPoNo());
            // Change Requirement.
            if( STR_C.equals( transfer.getOrderType() )
            // DKO ignored CIGMA_PO_NUMBER
            //    || (
            //        //Constants.KPO_TYPE.equals(transfer.getPoType())
            //        Constants.DKO_TYPE.equals(transfer.getPoType())
            //        && !EMPTY_STRING.equals(nullToEmpty(transfer.getCigmaPoNo()))
            //        )
            ) {
                poDomain
                    .setSpsPoNo(
                        appendsString(
                            transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
                            transfer.getCigmaPoNo()));
                poDomain.setCigmaPoNo(transfer.getCigmaPoNo());
            } else {
                //if( Constants.DCP_TYPE.equals(transfer.getPoType()) 
                //    || Constants.KCP_TYPE.equals(transfer.getPoType()) ) {
                if( Constants.DPO_TYPE.equals(transfer.getPoType()) 
                    || Constants.DKO_TYPE.equals(transfer.getPoType()) ) {
                    poDomain
                        .setSpsPoNo(
                            appendsString(
                                transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
                                ContextParams.getDefaultCigmaPoNo()));
                    poDomain.setCigmaPoNo( ContextParams.getDefaultCigmaPoNo() );
                } else {
                    poDomain
                        .setSpsPoNo(
                            appendsString(
                                transfer.getSPcd().substring(ZERO, ONE), //transfer.getSPcd(), // 
                                transfer.getCigmaPoNo()));
                    poDomain.setCigmaPoNo( transfer.getCigmaPoNo() );
                }
            }
            
            poDomain.setRefCigmaPoNo(transfer.getCigmaPoNo());
            
            poDomain.setPoType(transfer.getPoType());
            poDomain.setPoIssueDate(transfer.getIssueDate());
            if( STR_C.equals(transfer.getOrderType()) ) {
                poDomain.setPoStatus(SupplierPortalConstant.FAC);
            } else {
                poDomain.setPoStatus(Constants.ISS_TYPE);
            }
            poDomain.setVendorCd( transfer.getVendorCd() );
            poDomain.setSCd(transfer.getSCd());
            poDomain.setSPcd(transfer.getSPcd());
            poDomain.setDCd(criteria.getDCd());
            poDomain.setDPcd(transfer.getDPcd());
            poDomain.setTm(transfer.getTm());

            if (STR_C.equals(transfer.getOrderType())) {
                poDomain.setPeriodType(STR_ZERO);
            } else {
                poDomain.setPeriodType(STR_ONE);
            }
            if (Constants.DPO_TYPE.equals(transfer.getPoType())
                || Constants.IPO_TYPE.equals(transfer.getPoType())) {
                poDomain.setOrderMethod(STR_ZERO);
            } else {
                poDomain.setOrderMethod(STR_ONE);
            }
            poDomain.setForceAckDate( transfer.getForceAckDate() );
            poDomain.setChangeFlg(STR_ONE);
            poDomain.setPdfOriginalFileId(null);
            poDomain.setPdfChangeFileId(null);
            poDomain.setDueDateFrom(transfer.getDueDateFrom());
            poDomain.setDueDateTo(transfer.getDueDateTo());
            poDomain.setReleaseNo(transfer.getReleaseNo());
            poDomain.setFax( transfer.getFax() );
            if( STR_Y.equals( transfer.getAddTruckRouteFlag() ) ){
                poDomain.setNewTruckRouteFlg( STR_ONE );
            }else {
                poDomain.setNewTruckRouteFlg( STR_ZERO );
            }
            
            String createId = null;
            String updateId = null;
            Long createTime = null;
            Long updateTime = null;
            Long createBuffer = null;
            Long updateBuffer = null;
            
            for( TransferChangePoDetailDataFromCigmaDomain detail 
                : transfer.getCigmaPoDetailList() ) {
                for( TransferChangePoDueDataFromCigmaDomain due : detail.getCigmaPoDueList() ){
                    createBuffer = Long.parseLong( appendsString(
                        due.getCreateDate(), due.getCreateTime() ) );
                    updateBuffer = Long.parseLong( appendsString(
                        due.getUpdateDate(), due.getUpdateTime() ) );
                    
                    if( null == createTime || createTime < createBuffer ){
                        createTime = createBuffer;
                        createId = due.getCreateBy();
                    }
                    
                    if( null == updateTime || updateTime < updateBuffer ){
                        updateTime = updateBuffer;
                        updateId = due.getUpdateBy();
                    }
                }
            }
            
            poDomain.setCreateDscId( createId );
            poDomain.setCreateDatetime( 
                parseToTimestamp(createTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDomain.setLastUpdateDscId( updateId );
//            poDomain.setLastUpdateDatetime( 
//                parseToTimestamp(updateTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDomain.setLastUpdateDatetime( current );
            
            poId = purchaseOrderService.createPurchaseOrder(poDomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0020,
                transfer.getCigmaPoNo(),
                EMPTY_STRING,
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_CHG_PO) );
        }
        return poId;
    }
    
    /**
     * 
     * <p>insert Change P/O Detail.</p>
     *
     * @param locale locale
     * @param poId poId
     * @param poDomain poDomain
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param current current time
     * @throws ApplicationException exception
     */
    private void insertChangePoDetail(Locale locale, BigDecimal poId, SpsTPoDomain poDomain
        , TransferChangePoDataFromCigmaDomain transfer
        , TransferChangePoDetailDataFromCigmaDomain transferDetail
        , Timestamp current
    ) throws ApplicationException{
        try {
            SpsTPoDetailDomain poDetailDomain = new SpsTPoDetailDomain();
            poDetailDomain.setPoId( poId );
            poDetailDomain.setSPn( transferDetail.getSPn() );
            poDetailDomain.setDPn( transferDetail.getDPn() );
            poDetailDomain.setPnStatus( poDomain.getPoStatus() );
            poDetailDomain.setItemDesc( transferDetail.getItemDesc() );
//            poDetailDomain.setQtyBox( transferDetail.getQtyBox() );
//            poDetailDomain.setUnitPrice( transferDetail.getUnitPrice() );
//            poDetailDomain.setCurrencyCode( transferDetail.getCurrencyCode() );
//            poDetailDomain.setPnLblPrintRemark( transferDetail.getPartLblPrintRemarks() );
            poDetailDomain.setPlannerCode( transferDetail.getPlannerCode() );
            poDetailDomain.setWhPrimeReceiving( null );
            poDetailDomain.setLocation( null );
//            poDetailDomain.setPhaseCode( transferDetail.getPhaseCode() );
//            poDetailDomain.setReceivingDock( transferDetail.getReceivingDock() );
//            poDetailDomain.setVariableQtyCode( transferDetail.getVariableQtyCode() );
//            poDetailDomain.setOrderLot( transferDetail.getOrderLot() );
            poDetailDomain.setRcvLane( null );
            poDetailDomain.setTmpPriceFlg(transferDetail.getTmpPriceFlg());
            Date minDate = null;
            Date maxDate = null;
            for( TransferChangePoDueDataFromCigmaDomain due : transferDetail.getCigmaPoDueList() ){
                if( Constants.STR_M.equals(due.getReportType()) ){
                    continue;
                }
                
                if( null == minDate ){
                    minDate = due.getDueDate();
                }
                
                if( null == maxDate ){
                    maxDate = due.getDueDate();
                }
                
                if( minDate.after(due.getDueDate()) ){
                    minDate = due.getDueDate();
                }
                if( maxDate.before(due.getDueDate()) ){
                    maxDate = due.getDueDate();
                }
            }
            poDetailDomain.setStartPeriodDate( minDate );
            if (STR_C.equals(transfer.getOrderType())) {
                poDetailDomain.setEndPeriodDate( maxDate );
            } else {
                poDetailDomain.setEndPeriodDate( transfer.getDueDateTo() );
            }
            poDetailDomain.setUnitOfMeasure( transferDetail.getUnitOfMeasure() );
            poDetailDomain.setModel( poDetailDomain.getModel() );
            String createId = null;
            String updateId = null;
            Long createTime = null;
            Long updateTime = null;
            Long createBuffer = null;
            Long updateBuffer = null;
            
            for( TransferChangePoDueDataFromCigmaDomain due : transferDetail.getCigmaPoDueList() ){
                createBuffer = Long.parseLong( appendsString(
                    due.getCreateDate(), due.getCreateTime() ) );
                updateBuffer = Long.parseLong( appendsString(
                    due.getUpdateDate(), due.getUpdateTime() ) );
                
                if( null == createTime || createTime < createBuffer ){
                    createTime = createBuffer;
                    createId = due.getCreateBy();
                }
                
                if( null == updateTime || updateTime < updateBuffer ){
                    updateTime = updateBuffer;
                    updateId = due.getUpdateBy();
                }
            }
            
            poDetailDomain.setCreateDscId( createId );
            poDetailDomain.setCreateDatetime( 
                parseToTimestamp(createTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDetailDomain.setLastUpdateDscId( updateId );
//            poDetailDomain.setLastUpdateDatetime( 
//                parseToTimestamp(updateTime.toString(), PATTERN_YYYYMMDD_HHMMSS) );
            poDetailDomain.setLastUpdateDatetime( current );
            spsTPoDetailService.create(poDetailDomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0020,
                transfer.getCigmaPoNo(),
                transferDetail.getDPn(),
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_CHG_PO_DETAIL) );
        }
    }
    
    /**
     * 
     * <p>insert Change P/O Due.</p>
     *
     * @param locale locale
     * @param poId poId
     * @param transfer transferData
     * @param transferDetail transferDetailData
     * @param current current time
     * @throws ApplicationException exception
     */
    private void insertChangePoDue(Locale locale, BigDecimal poId
        , TransferChangePoDataFromCigmaDomain transfer
        , TransferChangePoDetailDataFromCigmaDomain transferDetail
        , Timestamp current) throws ApplicationException{
        Date dueDate = null;
        try {
            int dueSeq = Constants.ONE;
            for( TransferChangePoDueDataFromCigmaDomain transferDue 
                : transferDetail.getCigmaPoDueList() ){
                SpsTPoDueDomain poDueDomain = new SpsTPoDueDomain();
                poDueDomain.setPoId( poId );
                poDueDomain.setSPn( transferDetail.getSPn() );
                poDueDomain.setDPn( transferDetail.getDPn() );
                poDueDomain.setDueId( toBigDecimal(dueSeq++) );
                poDueDomain.setDueDate( transferDue.getDueDate() );
                if( Constants.STR_D.equals( transferDue.getReportType() ) ){
                    poDueDomain.setReportTypeFlg( STR_ZERO );
                }else if( Constants.STR_W.equals( transferDue.getReportType() ) ){
                    poDueDomain.setReportTypeFlg( STR_ONE );
                }else if( Constants.STR_M.equals( transferDue.getReportType() ) ){
                    poDueDomain.setReportTypeFlg( Constants.STR_TWO );
                }
                poDueDomain.setOrderQty( transferDue.getNewQty() );
                poDueDomain.setEtd( transferDue.getDueDate() );
                poDueDomain.setDel(transferDue.getDel());
                poDueDomain.setSeq(transferDue.getSeq());
                poDueDomain.setPreviousQty( transferDue.getOldQty() );
                poDueDomain.setDifferenceQty( transferDue.getDifferenceQty() );
                poDueDomain.setRsn( transferDue.getReason() );
                poDueDomain.setSpsProposedDueDate(null);
                poDueDomain.setSpsProposedQty(null);
                poDueDomain.setSpsPendingReasonCd(null);
                poDueDomain.setMarkPendingFlg( STR_ZERO );
                poDueDomain.setChangeFlg( STR_ONE );
                poDueDomain.setCreateDscId( transferDue.getCreateBy() );
                poDueDomain.setCreateDatetime( 
                    parseToTimestamp( 
                        appendsString(transferDue.getCreateDate(), 
                            transferDue.getCreateTime()), PATTERN_YYYYMMDD_HHMMSS ) );
                poDueDomain.setLastUpdateDscId( transferDue.getCreateBy() );
//                poDueDomain.setLastUpdateDatetime( 
//                    parseToTimestamp( 
//                        appendsString(transferDue.getCreateDate(), 
//                            transferDue.getCreateTime()), PATTERN_YYYYMMDD_HHMMSS ) );
                poDueDomain.setLastUpdateDatetime( current );
                
                spsTPoDueService.create(poDueDomain);    
            }
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0020, 
                transfer.getCigmaPoNo(),
                transferDetail.getDPn(),
                nullToEmpty( format(dueDate, PATTERN_YYYYMMDD_SLASH) ),
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_CHG_PO_DUE) );
        }
    }
    
    /**
     * 
     * <p>insert Cover Page.</p>
     *
     * @param locale locale
     * @param poId poId
     * @param transfer transferData
     * @param cover coverData
     * @param jobId jobId
     * @throws ApplicationException exception
     */
    private void insertCoverPage( Locale locale, BigDecimal poId
        , TransferPoDataFromCigmaDomain transfer
        , TransferPoCoverPageDataFromCigmaDomain cover
        , String jobId) throws ApplicationException {
        try {
            SpsTPoCoverPageDomain coverDomain = new SpsTPoCoverPageDomain();
            coverDomain.setPoId( poId );
            coverDomain.setReleaseNo( cover.getReleaseNo() );
            coverDomain.setPoIssueDate( transfer.getIssueDate() );
            coverDomain.setSellerContractName( cover.getSellerContractName() );
            coverDomain.setSellerName( cover.getSellerName() );
            coverDomain.setSellerAddress1( cover.getSellerAddress1() );
            coverDomain.setSellerAddress2( cover.getSellerAddress2() );
            coverDomain.setSellerAddress3( cover.getSellerAddress3() );
            coverDomain.setSellerFaxNumber( cover.getSellerFaxNumber() );
            coverDomain.setPaymentTerm( cover.getPaymentTerm() );
            coverDomain.setShipVia( cover.getShipVia() );
            coverDomain.setPriceTerm( cover.getPriceTerm() );
            coverDomain.setPurchaseContractName( cover.getPurchaserContractName() );
            coverDomain.setPurchaseName( cover.getPurchaserName() );
            coverDomain.setPurchaseAddress1( cover.getPurchaserAddress1() );
            coverDomain.setPurchaseAddress2( cover.getPurchaserAddress2() );
            coverDomain.setPurchaseAddress3( cover.getPurchaserAddress3() );
            coverDomain.setCreateDscId( jobId );
            coverDomain.setCreateDatetime( new Timestamp(System.currentTimeMillis()) );
            coverDomain.setLastUpdateDscId( jobId );
            coverDomain.setLastUpdateDatetime( new Timestamp(System.currentTimeMillis()) );
            spsTPoCoverPageService.create(coverDomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0016, 
                transfer.getCigmaPoNo(),
                EMPTY_STRING,
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE) );
        }
        // Insert Cover page detail
        BigDecimal runningDo = new BigDecimal( ZERO );
        BigDecimal increse = new BigDecimal( Constants.ONE );
//        String preDoNo = null;
        for(TransferPoCoverPageDetailDataFromCigmaDomain coverDetail 
            : cover.getCigmaPoCoverPageDetailList() ){
            try {
                SpsTPoCoverPageDetailDomain coverDetailDomain 
                    = new SpsTPoCoverPageDetailDomain();
                
//                if( !coverDetail.getDoNo().equals( preDoNo ) ){
//                    preDoNo = coverDetail.getDoNo();
//                    runningDo = new BigDecimal( Constants.ONE );
//                }else{
//                    runningDo = runningDo.add( increse );
//                }
                runningDo = runningDo.add( increse );
                
                coverDetailDomain.setPoId( poId );
                coverDetailDomain.setRunningDoId( runningDo );
                coverDetailDomain.setDoDueDate( coverDetail.getDueDate() );
                coverDetailDomain.setCigmaDoNo( coverDetail.getDoNo() );
                coverDetailDomain.setCreateDscId( coverDetail.getCreateBy() );
                coverDetailDomain.setCreateDatetime( 
                    parseToTimestamp( appendsString(
                        coverDetail.getCreateDate(), coverDetail.getCreateTime())
                        , PATTERN_YYYYMMDD_HHMMSS));
                coverDetailDomain.setLastUpdateDscId( coverDetail.getUpdateBy() );
                coverDetailDomain.setLastUpdateDatetime( 
                    parseToTimestamp( appendsString(
                        coverDetail.getUpdateDate(), coverDetail.getUpdateTime())
                        , PATTERN_YYYYMMDD_HHMMSS));
                spsTPoCoverPageDetailService.create( coverDetailDomain );
            } catch (Exception e) {
                // trace(e);
                throwsErrorMessage(
                    locale, 
                    ERROR_CD_SP_E6_0016, 
                    transfer.getCigmaPoNo(),
                    EMPTY_STRING,
                    EMPTY_STRING,
                    getLabelHandledException(locale, 
                        TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE_DETAIL) );
            }
        }
    }
    
    /**
     * 
     * <p>Transfer P/O From Cigma.</p>
     *
     * @param locale locale
     * @param transferList List of TransferPoDataFromCigmaDomain
     * @param coverList List of TransferPoCoverPageDataFromCigmaDomain
     * @param as400Server serverConfg
     * @param bError isError
     * @param jobId jobId
     * @return String
     * @throws Exception exception
     */
    private String transferPoFromCigma(Locale locale, 
        List<TransferPoDataFromCigmaDomain> transferList, 
        List<TransferPoCoverPageDataFromCigmaDomain> coverList,  
        As400ServerConnectionInformationDomain as400Server, 
        boolean bError,
        String jobId) throws Exception
    {
        Timestamp current = commonService.searchSysDate();
        String result = EMPTY_STRING;
        BigDecimal poId = null;
//        String mainOrderType = null;
        TransferPoCoverPageDataFromCigmaDomain cover = null;
        for( TransferPoDataFromCigmaDomain transfer : transferList ) {
            if( null == coverList ){
                throw new ApplicationException(
                    String.format("Cannot find P/O Cover page Cigma P/O No : %s, Vendor : %s, Plant : %s"
                        , transfer.getCigmaPoNo(), transfer.getVendorCd(), transfer.getDPcd()));
            }
            cover = null;
            for(TransferPoCoverPageDataFromCigmaDomain co : coverList){
                if( co.getPoType().equals( transfer.getPoType() ) 
                    && co.getVendorCd().equals( transfer.getVendorCd() )
                    && co.getDPcd().equals( transfer.getDPcd() )
                ){
                    cover = co;
                    break;
                }
            }
            if( null == cover ){
                throw new ApplicationException(
                    String.format("Cannot find P/O Cover page Cigma P/O No : %s, Vendor : %s, Plant : %s"
                        , transfer.getCigmaPoNo(), transfer.getVendorCd(), transfer.getDPcd()));
            }
            
            Set<String> orderTypeSet = new HashSet<String>();
            for(TransferPoDetailDataFromCigmaDomain transferDetail
                : transfer.getCigmaPoDetailList())
            {
                for( TransferPoDueDataFromCigmaDomain transferDue 
                    : transferDetail.getCigmaPoDueList() )
                {
                    if(transferDue.getOrderType().equalsIgnoreCase(Constants.STR_K)){
                        orderTypeSet.add(Constants.STR_D); 
                        transferDue.setOrderType(Constants.STR_D);
                    }else{
                        orderTypeSet.add(transferDue.getOrderType()); 
                    }
                }
            }
            
//            BigDecimal firmPoId = null;
//            BigDecimal forcastPoId = null;
            
            for (String orderType : orderTypeSet) {
                // 2-1) Separate CIGMA P/O to SPS P/O by Supplier Plant Code and Data Type
                SpsTPoDomain poDomain = new SpsTPoDomain();
                poId = insertPo(locale, as400Server.getSpsMCompanyDensoDomain(), 
                    transfer, cover, poDomain, current, orderType);
                
//                if (STR_C.equals(orderType)) {
//                    firmPoId = poId;
//                } else {
//                    forcastPoId = poId;
//                }
                // 2-1-2) LOOP-2 until end of <List> cigmaDOInformation. cigmaDODetail
                for(TransferPoDetailDataFromCigmaDomain transferDetail 
                    : transfer.getCigmaPoDetailList()) {
                    
                    // insert Po Detail
                    insertPoDetail(locale, poId, poDomain, transfer
                        , transferDetail, current, orderType);
                    
                    // insert Po Due
                    insertPoDue(locale, poId, transfer, transferDetail, current, orderType);
                    
                } // end for detail.
            }
            
//            if (null != firmPoId) {
//                mainOrderType = STR_C;
//                poId = firmPoId;
//            } else {
//                mainOrderType = Constants.STR_D;
//                poId = forcastPoId;
//            }
            
            cover = null;
            for(TransferPoCoverPageDataFromCigmaDomain co : coverList){
                if( co.getPoType().equals( transfer.getPoType() )
                    && co.getVendorCd().equals( transfer.getVendorCd() )
                    && co.getDPcd().equals( transfer.getDPcd() )
                ) {
                    cover = co;
                    break;
                }
            }
            if( null == cover ){
                throw new ApplicationException(
                    String.format("Cannot find P/O Cover page Cigma P/O No : %s, Vendor : %s, Plant : %s"
                        , transfer.getCigmaPoNo(), transfer.getVendorCd(), transfer.getDPcd()));
            }
            insertCoverPage(locale, poId, transfer, cover, jobId);
            // insert P/O Cover Page
            // 20180410 Don't generate BLOB
//            result = this.generatePurchaseOrder(locale, 
//                poId, 
//                transfer.getSPcd(),
//                transfer.getDPcd(),
//                transfer.getCigmaPoNo(),
//                jobId, false, mainOrderType);
//            
//            if( null != result && ZERO < result.length() ){
//                break;
//            }
        } // end for.
        
        String partNoError = CommonWebServiceUtil.updateCigmaPurchaseOrder(as400Server, 
            transferList, cover, STR_ONE, jobId);
        if( !StringUtil.checkNullOrEmpty(partNoError) ){
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0018,
                STR_ONE,
                Constants.PO);
        } // end if
        
        return result;
    }
    
    /**
     * 
     * <p>insert Cigma Chg Po Error.</p>
     *
     * @param locale locale
     * @param criteria criteria
     * @param transfer transferData
     * @param transferDetail Transfer P/O Detail
     * @param transferDue Transfer P/O Due
     * @param mailFlg MAIL_FLG
     * @param errorType ERROR_TYPE_FLG
     * @throws ApplicationException exception
     */
    private void insertCigmaChgPoError( Locale locale, SpsMCompanyDensoDomain criteria
        , TransferChangePoDataFromCigmaDomain transfer
        , TransferChangePoDetailDataFromCigmaDomain transferDetail
        , TransferChangePoDueDataFromCigmaDomain transferDue
        , String mailFlg, String errorType )
        throws ApplicationException
    {
        try {
            SpsCigmaChgPoErrorDomain errorPo = new SpsCigmaChgPoErrorDomain();
            if( StringUtil.checkNullOrEmpty( transfer.getCigmaPoNo() ) ){
                errorPo.setCigmaPoNo( ContextParams.getDefaultCigmaPoNo() );
            }else {
                errorPo.setCigmaPoNo( transfer.getCigmaPoNo() );
            }
            
            errorPo.setDataType( transfer.getPoType() );
            if( null != transfer.getSCd() ){
                errorPo.setSCd( transfer.getSCd() );
            }else{
                errorPo.setSCd( null );
            }
            errorPo.setIssueDate( transfer.getIssueDate() );
            errorPo.setMailFlg(mailFlg);
            errorPo.setErrorTypeFlg(errorType);
            errorPo.setDCd( criteria.getDCd().trim() );
            errorPo.setDPcd( transfer.getDPcd() );
            errorPo.setVendorCd( transfer.getVendorCd() );
            if (null != transferDetail) {
                errorPo.setDPn( transferDetail.getDPn() );
            }
            if (null != transferDue) {
                errorPo.setUpdateByUserId( transferDue.getUpdateBy() );
                errorPo.setSPcd( transferDue.getSPcd() );
                errorPo.setDueDate( transferDue.getDueDate() );
                errorPo.setOrderQty( transferDue.getOldQty() );
                errorPo.setNewQty( transferDue.getNewQty() );
            }
            spsCigmaChgPoErrorService.create(errorPo);
        } catch (Exception e) {
            String dPn = EMPTY_STRING;
            String etd = EMPTY_STRING;
            if (null != transferDetail) {
                dPn = transferDetail.getDPn();
            }
            if (null != transferDue) {
                etd = transferDue.getDueDate().toString();
            }
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0019, null, 
                new String[]{transfer.getCigmaPoNo(), dPn, etd, TABLE_CIGMA_CHG_PO_NAME
                } );
        }
    }
    
    /**
     * Delete error record in SPS_CIGMA_CHG_PO_ERROR for complete or update transfer status.
     * @param dCd - DENSO Company Code
     * @param locale - for get message
     * @throws Exception when catch Exception
     * */
    private void deleteChgPoError(String dCd, Locale locale)
        throws Exception
    {
        SpsCigmaChgPoErrorCriteriaDomain errorPomain = new SpsCigmaChgPoErrorCriteriaDomain();
        errorPomain.setDCd(dCd);
        try {
            spsCigmaChgPoErrorService.deleteByCondition(errorPomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, ERROR_CD_SP_E6_0023,
                EMPTY_STRING, EMPTY_STRING,
                TABLE_CIGMA_PO_NAME );
        }
    }
    
    /**
     * 
     * <p>get Argument Message.</p>
     *
     * @param args array String
     * @return String[]
     */
    private String[] getArgumentMessage(String...args){
        if( null != args && ZERO < args.length ){
            if( ONE == args.length ){
                argument1[ZERO] = args[ZERO];
                return argument1;
            }else if( TWO == args.length ){
                argument2[ZERO] = args[ZERO];
                argument2[ONE] = args[ONE];
                return argument2;
            }else if( THREE == args.length ){
                argument3[ZERO] = args[ZERO];
                argument3[ONE] = args[ONE];
                argument3[TWO] = args[TWO];
                return argument3;
            }else if( FOUR == args.length ){
                for(int i = ZERO; i < FOUR; ++i){
                    argument4[i] = args[i];
                }
                return argument4;
            }else if( FIVE == args.length ){
                for(int i = ZERO; i < FIVE; ++i){
                    argument5[i] = args[i];
                }
                return argument5;
            }else if( SIX == args.length ){
                for(int i = ZERO; i < SIX; ++i){
                    argument6[i] = args[i];
                }
                return argument6;
            }
        }
        return null;
    }
    
    /**
     * 
     * <p>Clone New.</p>
     * @param src TransferPoDataFromCigmaDomain
     * @param result TransferPoDataFromCigmaDomain
     * @return TransferPoDataFromCigmaDomain
     */
    private TransferPoDataFromCigmaDomain cloneNew(TransferPoDataFromCigmaDomain src, 
        TransferPoDataFromCigmaDomain result){
        result.setAddTruckRouteFlag( src.getAddTruckRouteFlag() );
        result.setBlanketPoNo( src.getBlanketPoNo() );
        result.setCigmaPoDetailList( new ArrayList<TransferPoDetailDataFromCigmaDomain>() );
        result.setCigmaPoNo( src.getCigmaPoNo() );
        result.setCompanyName( src.getCompanyName() );
        result.setDPcd( src.getDPcd() );
        result.setDueDateFrom( src.getDueDateFrom() );
        result.setDueDateTo( src.getDueDateTo() );
        result.setForceAckDate( src.getForceAckDate() );
        result.setIssueDate( src.getIssueDate() );
        result.setMaxCreateDateTime( src.getMaxCreateDateTime() );
        result.setMaxUpdateDateTime( src.getMaxUpdateDateTime() );
        result.setPoId( src.getPoId() );
        result.setPoType( src.getPoType() );
        result.setReleaseNo( src.getReleaseNo() );
        result.setSCd( src.getSCd() );
        result.setSPcd( src.getSPcd() );
        result.setSupplierName( src.getSupplierName() );
        result.setTm( src.getTm() );
        result.setTr( src.getTr() );
        result.setVendorCd( src.getVendorCd() );
        return result;
    }
    /**
     * 
     * <p>Clone New Transfer PO Detail.</p>
     * @param source TransferPoDetailDataFromCigmaDomain
     * @return TransferPoDataFromCigmaDomain
     */
    private TransferPoDetailDataFromCigmaDomain cloneTransferDetail(
        TransferPoDetailDataFromCigmaDomain source)
    {
        TransferPoDetailDataFromCigmaDomain result = new TransferPoDetailDataFromCigmaDomain();
        result.setDPn(source.getDPn());
        result.setItemDesc(source.getItemDesc());
        result.setUnitPrice(source.getUnitPrice());
        result.setCurrencyCode(source.getCurrencyCode());
        result.setReceivingDock(source.getReceivingDock());
        result.setOrderLot(source.getOrderLot());
        result.setQtyBox(source.getQtyBox());
        result.setUnitOfMeasure(source.getUnitOfMeasure());
        result.setVariableQtyCode(source.getVariableQtyCode());
        result.setPartLblPrintRemarks(source.getPartLblPrintRemarks());
        result.setPhaseCode(source.getPhaseCode());
        result.setPlannerCode(source.getPlannerCode());
        result.setSPn(source.getSPn());
        result.setMaxDueDateTimeMonth(source.getMaxDueDateTimeMonth());
        result.setMinDueDateTimeMonth(source.getMinDueDateTimeMonth());
        result.setMaxCreateDateTime(source.getMaxCreateDateTime());
        result.setMaxUpdateDateTime(source.getMaxUpdateDateTime());
        result.setEndFirm(source.getEndFirm());
        result.setTmpPriceFlg(source.getTmpPriceFlg());
        result.setCigmaPoDueList(new ArrayList<TransferPoDueDataFromCigmaDomain>());
        return result;
    }
    
    /**
     * <p>Clone New Transfer PO Due.</p>
     * @param src TransferPoDueDataFromCigmaDomain
     * @return TransferPoDueDataFromCigmaDomain
     */
    private TransferPoDueDataFromCigmaDomain clonePurchaseOrderDue(
        TransferPoDueDataFromCigmaDomain src)
    {
        TransferPoDueDataFromCigmaDomain result = new TransferPoDueDataFromCigmaDomain();
        
        result.setOrderType(src.getOrderType());
        result.setEtd(src.getEtd());
        result.setDueDate(src.getDueDate());
        result.setOrderQty(src.getOrderQty());
        result.setReportType(src.getReportType());
        result.setSpsFlag(src.getSpsFlag());
        result.setCreateBy(src.getCreateBy());
        result.setCreateDate(src.getCreateDate());
        result.setCreateTime(src.getCreateTime());
        result.setUpdateBy(src.getUpdateBy());
        result.setUpdateDate(src.getUpdateDate());
        result.setUpdateTime(src.getUpdateTime());
        result.setSPcd(src.getSPcd());
        
        return result;
    }
    
    
    /**
     * 
     * <p>Clone Change New.</p>
     * @param src TransferPoDataFromCigmaDomain
     * @param result TransferPoDataFromCigmaDomain
     * @return TransferPoDataFromCigmaDomain
     */
    private TransferChangePoDataFromCigmaDomain cloneNew(TransferChangePoDataFromCigmaDomain src, 
        TransferChangePoDataFromCigmaDomain result){
        result.setPoType( src.getPoType() ); // poType
        result.setCompanyName( src.getCompanyName() ); // companyName
        result.setVendorCd( src.getVendorCd() ); // supplierCode
        result.setSupplierName( src.getSupplierName() ); // supplierName
        result.setCigmaPoNo( src.getCigmaPoNo() ); // cigmaPONo
        result.setIssueDate( src.getIssueDate() ); // issueDate
        result.setDueDateFrom( src.getDueDateFrom() ); // dueDateFrom
        result.setDueDateTo( src.getDueDateTo() ); // dueDateTo
        result.setReleaseNo( src.getReleaseNo() ); // releaseNo
        result.setDPcd( src.getDPcd() ); // dPlantCode
        result.setTr( src.getTr() ); // tr
        result.setOrderType( src.getOrderType() ); // orderType
        result.setForceAckDate( src.getForceAckDate() ); // forceAckDate
        result.setTm( src.getTm() ); // tm
        result.setSCd( src.getSCd() );
        result.setCigmaPoDetailList( new ArrayList<TransferChangePoDetailDataFromCigmaDomain>() );
        return result;
    }
    
    /**
     * 
     * <p>Clone new Change PO detail.</p>
     * @param source TransferPoDataFromCigmaDomain
     * @return TransferPoDataFromCigmaDomain
     */
    private TransferChangePoDetailDataFromCigmaDomain cloneChangePoDetail(
        TransferChangePoDetailDataFromCigmaDomain source)
    {
        TransferChangePoDetailDataFromCigmaDomain result
            = new TransferChangePoDetailDataFromCigmaDomain();
        result.setDPn(source.getDPn());
        result.setPlannerCode(source.getPlannerCode());
        result.setWhPrimeReceiving(source.getWhPrimeReceiving());
        result.setLocation(source.getLocation());
        result.setUnitOfMeasure(source.getUnitOfMeasure());
        result.setItemDesc(source.getItemDesc());
        result.setModel(source.getModel());
        result.setSPn(source.getSPn());
        result.setTmpPriceFlg(source.getTmpPriceFlg());
        result.setCigmaPoDueList(new ArrayList<TransferChangePoDueDataFromCigmaDomain>());
        return result;
    }
    
    /**
     * 
     * <p>Generate PurchaseOrder Report.</p>
     *
     * @param locale Locale
     * @param poId poId
     * @param spcd Supplier Code
     * @param dpcd Denso Code
     * @param cigmaPoNo CIGMA PO No
     * @param user user
     * @param change change
     * @param orderType C or D
     * @return String
     * @throws Exception Exception
     */
    private String generatePurchaseOrder(Locale locale,
        BigDecimal poId, 
        String spcd, 
        String dpcd, 
        String cigmaPoNo,
        String user, 
        boolean change,
        String orderType)
        throws Exception {
        
        String result = null;
        
        PurchaseOrderInformationDomain poInfo = new PurchaseOrderInformationDomain();
        poInfo.setPoId( poId.toString() );
        SpsTPoDomain poDetail = new SpsTPoDomain();
        poDetail.setPoId( poId );
        
        List<PurchaseOrderCoverPageReportDomain> coverReport = null;
        List<PurchaseOrderReportDomain> poReport = null;
        coverReport = 
            purchaseOrderCoverPageService.searchPoCoverPageReport(poInfo);
        //if( Constants.KPO_TYPE.equals( dataType ) ){
        if( Constants.STR_D.equals( orderType ) ){
            poReport = 
                purchaseOrderService.searchPurchaseOrderKanbanReport( poDetail );
        }else {
            poDetail.setSPcd( spcd );
            poDetail.setDPcd( dpcd );
            poDetail.setCigmaPoNo( cigmaPoNo );
            
            poReport = 
                purchaseOrderService.searchPurchaseOrderReport( poDetail );
        }
        if( null != coverReport && ZERO == coverReport.size() ){
            if( change ){
                throwsErrorMessage(
                    locale, ERROR_CD_SP_E6_0020, cigmaPoNo, 
                    EMPTY_STRING, EMPTY_STRING,
                    getLabelHandledException(locale, TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT));
            } else {
                throwsErrorMessage(
                    locale, ERROR_CD_SP_E6_0016, cigmaPoNo, 
                    EMPTY_STRING, EMPTY_STRING,
                    getLabelHandledException(locale, TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT));
            }
            if( !EMPTY_STRING.equals(result) ){
                return result;
            }
        }
        
        InputStream isReportData = null;
        String fileId = null;
        try {
            isReportData = purchaseOrderService.generatePo(locale, coverReport, poReport);
            
            StringBuffer filenameReport = new StringBuffer();
            filenameReport.append(SupplierPortalConstant.PURCHASE_ORDER);
            filenameReport.append(poId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
            
            fileId = fileManagementService.createFileUpload(
                isReportData, 
                filenameReport.toString(), 
                Constants.SAVE_LIMIT_TERM, 
                user);
        } catch (Exception e) {
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_80_0013,
                cigmaPoNo );
        }
//        int iUpdate = ZERO;
        try {
            SpsTPoDomain doFileDomain = new SpsTPoDomain();
            doFileDomain.setPdfOriginalFileId(fileId);
            SpsTPoCriteriaDomain poCriteriaFileDomain = new SpsTPoCriteriaDomain();
            poCriteriaFileDomain.setPoId( poId );
            /*iUpdate = */spsTPoService.updateByCondition(doFileDomain, poCriteriaFileDomain);
        } catch (Exception e) {
            if( change ){
                throwsErrorMessage(
                    locale, ERROR_CD_SP_E6_0020, cigmaPoNo, 
                    EMPTY_STRING, EMPTY_STRING,
                    getLabelHandledException(locale, TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID));
            } else {
                throwsErrorMessage(
                    locale, ERROR_CD_SP_E6_0016, cigmaPoNo, 
                    EMPTY_STRING, EMPTY_STRING,
                    getLabelHandledException(locale, TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID));
            }
        }
//        if( ZERO == iUpdate ){
//            throwsErrorMessage(
//                locale, 
//                ERROR_CD_SP_E6_0016, 
//                cigmaPoNo, 
//                EMPTY_STRING,
//                EMPTY_STRING,
//                getLabelHandledException(locale, TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID) );
//        }
        return result;
    }
    
    /**
     * 
     * <p>Generate Change Material Release Report.</p>
     *
     * @param locale locale
     * @param poId poId
     * @param cigmaPoNo cigmaPoNo
     * @param user user
     * @throws Exception Exception
     */
    private void generateChangePurchaseOrder(Locale locale, 
        BigDecimal poId, 
        String cigmaPoNo,
        String user) throws Exception {
        InputStream isReportData = null;
        String fileId = null;
        PurchaseOrderInformationDomain poInfo = new PurchaseOrderInformationDomain();
        poInfo.setPoId( poId.toString() );
        StringBuffer filenameReport = new StringBuffer();
        List<ChangeMaterialReleaseReportDomain> cmrReport 
            = purchaseOrderService.searchChangeMaterialReleaseReport(poInfo);
        
        if( null == cmrReport || ZERO == cmrReport.size() ) {
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0020,
                cigmaPoNo,
                EMPTY_STRING,
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_SEARCH_CHG_MAT_REPORT ) );
        }

        try {
            isReportData = purchaseOrderService.generateChgPo(locale, cmrReport);
            
            filenameReport.append(SupplierPortalConstant.CHANGE_PURCHASE_ORDER);
            filenameReport.append(poId);
            filenameReport.append(Constants.SYMBOL_DOT);
            filenameReport.append(Constants.REPORT_PDF_FORMAT);
            
            fileId = fileManagementService.createFileUpload(
                isReportData, 
                filenameReport.toString(), 
                Constants.SAVE_LIMIT_TERM, 
                user); // if(true) throw new Exception("Test Error");
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_80_0013,
                cigmaPoNo );
        }
//        int iUpdate = ZERO;
        try {//if(true)throw new Exception("");
            SpsTPoDomain doFileDomain = new SpsTPoDomain();
            doFileDomain.setPdfChangeFileId(fileId);
            SpsTPoCriteriaDomain poCriteriaFileDomain = new SpsTPoCriteriaDomain();
            poCriteriaFileDomain.setPoId( poId ); // if(true) throw new Exception("Test Error");
            /*iUpdate = */spsTPoService.updateByCondition(doFileDomain, poCriteriaFileDomain);
        } catch (Exception e) {
            // trace(e);
            throwsErrorMessage(
                locale, 
                ERROR_CD_SP_E6_0020, 
                cigmaPoNo, 
                EMPTY_STRING,
                EMPTY_STRING,
                getLabelHandledException(locale, TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID) );
        }
//        if( ZERO == iUpdate ){
//            throwsErrorMessage(
//                locale, 
//                ERROR_CD_SP_E6_0020, 
//                cigmaPoNo, 
//                EMPTY_STRING,
//                EMPTY_STRING,
//                getLabelHandledException(locale, TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID) );
//        }
    }
    
    /**
     * 
     * <p>Find Supplier.</p>
     *
     * @param as400VendorCriteriaDomain SpsMAs400VendorDomain
     * @param vendorCd Vendor Code
     * @return SpsMAs400VendorDomain
     * @throws ApplicationException ApplicationException
     */
    private SpsMAs400VendorDomain findSupplier(
        SpsMAs400VendorCriteriaDomain as400VendorCriteriaDomain,
        String vendorCd) throws ApplicationException {
        as400VendorCriteriaDomain.setVendorCd( vendorCd );
        List<SpsMAs400VendorDomain> vendorList = 
            spsMAs400VendorService.searchByCondition(as400VendorCriteriaDomain);
        // check is not found Supplier Code.
        if( null != vendorList && ZERO < vendorList.size() ){
            return vendorList.get(ZERO);
        }else {
            return null;
        }
    }
    /**
     * 
     * <p>Find Part.</p>
     *
     * @param partCriteria SpsMDensoSupplierPartsCriteriaDomain
     * @param dpn Denso Part No
     * @param preferredSort Column for sorting
     * @return List of SpsMDensoSupplierPartsDomain
     * @throws ApplicationException ApplicationException
     */
    private List<SpsMDensoSupplierPartsDomain> findPart(
        SpsMDensoSupplierPartsCriteriaDomain partCriteria,
        String dpn, String preferredSort) throws ApplicationException
    {
        partCriteria.setDPn(dpn);
        partCriteria.setPreferredOrder( preferredSort );
        
        List<SpsMDensoSupplierPartsDomain> partList =
            spsMDensoSupplierPartsService.searchByCondition( partCriteria );
        
        // check is not found Supplier Part No
        if( null != partList && ZERO < partList.size() ){
            return partList;
        }else {
            return null;
        }
    }
    /**
     * 
     * <p>Get Message.</p>
     *
     * @param locale Locale
     * @param msgId Message ID
     * @param params arguments
     * @return Message String
     * @throws ApplicationException ApplicationException
     */
    private String getMessage(
        Locale locale, 
        String msgId, 
        String ... params) throws ApplicationException {
        return MessageUtil.getErrorMessage(locale, msgId, null, getArgumentMessage( params ) );
    }
    
    /**
     * 
     * <p>Throws ErrorMessage.</p>
     *
     * @param locale Locale
     * @param msgId Message ID
     * @param params argument
     * @throws ApplicationException ApplicationException
     */
    private void throwsErrorMessage(
        Locale locale, 
        String msgId, 
        String ... params) throws ApplicationException {
        MessageUtil.throwsApplicationMessage(locale, msgId, null, getArgumentMessage( params ) );
    }

}
