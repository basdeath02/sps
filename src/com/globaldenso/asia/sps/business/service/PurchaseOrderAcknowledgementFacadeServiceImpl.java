/*
 * ModifyDate Development company     Describe 
 * 2014/07/03 CSI Phakaporn           Create
 * 2016/02/10 CSI Akat                [IN049]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;


/**
 * <p>The Class Purchase Order Information Facade Service Implement.</p>
 * <p>Manage data of Supplier User.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method insert/update : transactSaveAndSendPo</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderAcknowledgementFacadeServiceImpl 
    implements PurchaseOrderAcknowledgementFacadeService
{
    /** The service for get record limit. */
    private RecordLimitService recordLimitService;
    
    /** The Purchase Order service. */
    private PurchaseOrderService purchaseOrderService;

    /** The File Management Service. */
    private FileManagementService fileManagementService;

    /** The service for SPS_T_PO table. */
    private SpsTPoService spsTPoService;
    
    /** The service for SPS_T_PO_DETAIL table. */
    private SpsTPoDetailService spsTPoDetailService;
    
    /** The Common Service. */
    private CommonService commonService;
    
    /** The User Service. */
    private UserService userService;
    
    /** The Service for send Email. */
    private MailService mailService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    // [IN054] For search Supplier User to reply.
    /** The user supplier service. */
    private UserSupplierService userSupplierService;
    
    /**
     * Instantiates a new Purchase Order Acknowledgement Facade Service implement.
     */
    public PurchaseOrderAcknowledgementFacadeServiceImpl(){
        super();
    }

    /**
     * Sets the Purchase Order information service.
     * 
     * @param purchaseOrderService the new Purchase Order information service.
     */
    public void setPurchaseOrderService(
        PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }
    
    /**
     * <p>Setter method for fileManagementService.</p>
     *
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * <p>Setter method for spsTPoService.</p>
     *
     * @param spsTPoService Set for spsTPoService
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>Setter method for spsTPoDetailService.</p>
     *
     * @param spsTPoDetailService Set for spsTPoDetailService
     */
    public void setSpsTPoDetailService(SpsTPoDetailService spsTPoDetailService) {
        this.spsTPoDetailService = spsTPoDetailService;
    }

    /**
     * <p>Setter method for userService.</p>
     *
     * @param userService Set for userService
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * <p>Setter method for mailService.</p>
     *
     * @param mailService Set for mailService
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * <p>Setter method for recordLimitService.</p>
     *
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    // [IN054] For search Supplier User to reply.
    /**
     * <p>Setter method for userSupplierService.</p>
     *
     * @param userSupplierService Set for userSupplierService
     */
    public void setUserSupplierService(UserSupplierService userSupplierService) {
        this.userSupplierService = userSupplierService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderAcknowledgementFacadeService#searchInitial(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public List<PurchaseOrderDetailDomain> searchInitial(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain) throws ApplicationException
    {
        List<PurchaseOrderDetailDomain> result = null;
        Locale locale = purchaseOrderAcknowledgementDomain.getLocale();
        String periodType = purchaseOrderAcknowledgementDomain.getPeriodType();
        // 1. Validate input parameter which pass from other screen (WORD002, WORD004, WORD005)
        if (Strings.judgeBlank(purchaseOrderAcknowledgementDomain.getPoId())) {
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                SupplierPortalConstant.LBL_PO_NO);
        }
        
        // 2. Count Purchase Order Detail return record from PurchaseOrderService
        int recordCount = purchaseOrderService.searchCountPurchaseOrderDetail(
            purchaseOrderAcknowledgementDomain);
        if (recordCount <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                null);
        }
        
        // 3. Get maximum record per page from RecordLimitService
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD003_PLM);
        MiscellaneousDomain limitPerPageDomain 
            = recordLimitService.searchRecordLimitPerPage(miscDomain);
        if (null == limitPerPageDomain) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032, null);
        }
        int limitPerPage = Integer.parseInt(limitPerPageDomain.getMiscValue());

        // 4. Calculate rownum for query
        purchaseOrderAcknowledgementDomain.setMaxRowPerPage(limitPerPage);
        SpsPagingUtil.calcPaging(purchaseOrderAcknowledgementDomain, recordCount);
        
        // 5. Get Purchase Order Detail data from PurchaseOrderService
        result = purchaseOrderService.searchPurchaseOrderDetail(purchaseOrderAcknowledgementDomain);
        if (result.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                null);
        }
        
        // 6. Set <List> poDetailInformation to return Domain
        for (PurchaseOrderDetailDomain poDetail : result) {
            // Start : [IN054] Add action mode Reply and also check action mode by user type
            //if (Constants.STR_ONE.equals(periodType)
            //    && SupplierPortalConstant.PO_STATUS_ISSUED.equals(poDetail.getPoStatus()))
            //{
            //    poDetail.setActionMode(Constants.MODE_EDIT);
            //} else {
            //    poDetail.setActionMode(Constants.MODE_VIEW);
            //}
            String actionMode = Constants.MODE_VIEW;
            if (Constants.STR_ONE.equals(periodType)
                && SupplierPortalConstant.PO_STATUS_ISSUED.equals(poDetail.getPoStatus())
                && Constants.STR_S.equals(purchaseOrderAcknowledgementDomain.getUserType()))
            {
                actionMode = Constants.MODE_EDIT;
            }
            
            if (Constants.STR_ONE.equals(periodType)
                && SupplierPortalConstant.PO_STATUS_PENDING.equals(poDetail.getPoStatus())
                && (SupplierPortalConstant.PO_STATUS_PENDING.equals(poDetail.getPnStatus())
                    || SupplierPortalConstant.PO_STATUS_REPLY.equals(poDetail.getPnStatus()))
                && Constants.STR_D.equals(purchaseOrderAcknowledgementDomain.getUserType()))
            {
                actionMode = Constants.MODE_REPLY;
            }
            
            poDetail.setActionMode(actionMode);
            // End : [IN054] Add action mode Reply and also check action mode by user type
            
            if (Constants.STR_ZERO.equals(periodType)) {
                poDetail.setFirmQty(poDetail.getOrderQty());
            } else {
                poDetail.setFirmQty(null);
            }
            if (Constants.STR_ONE.equals(periodType)){
                poDetail.setForecastQty(poDetail.getOrderQty());
            } else {
                poDetail.setForecastQty(null);
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderAcknowledgementFacadeService#searchCountPurchaseOrderDetailPending(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrderDetailPending(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain) {
        Integer count = this.purchaseOrderService.searchCountPurchaseOrderDetailPending(
            purchaseOrderAcknowledgementDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderAcknowledgementFacadeService#transactSaveAndSendPo(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public PurchaseOrderAcknowledgementDomain transactSaveAndSendPo(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain) 
        throws ApplicationException
    {
        Locale locale = purchaseOrderAcknowledgementDomain.getLocale();
        Timestamp updateDatetime = this.commonService.searchSysDate();
        BigDecimal poId = new BigDecimal(purchaseOrderAcknowledgementDomain.getPoId());
        
        // 2. Search all PO Detail
        SpsTPoDetailCriteriaDomain detailCriteria = new SpsTPoDetailCriteriaDomain();
        detailCriteria.setPoId(poId);
        List<SpsTPoDetailDomain> detailList = this.spsTPoDetailService.searchByCondition(
            detailCriteria);
        
        boolean foundStatusPending = false;
        for (SpsTPoDetailDomain poDetail : detailList) {
            if (SupplierPortalConstant.PO_STATUS_PENDING.equals(poDetail.getPnStatus())) {
                foundStatusPending = true;
            }
            
            if (!SupplierPortalConstant.PO_STATUS_PENDING.equals(poDetail.getPnStatus())) {
                poDetail.setPnStatus(SupplierPortalConstant.PO_STATUS_ACKNOWLEDGE);
            }
            poDetail.setLastUpdateDscId(purchaseOrderAcknowledgementDomain.getLastUpdateDscId());
            poDetail.setLastUpdateDatetime(updateDatetime);
        }
        
        // 3. Update Purchase Order Header from SpsTPoService
        SpsTPoDomain headerValueDomain = new SpsTPoDomain();
        SpsTPoCriteriaDomain headerCriteriaDomain = new SpsTPoCriteriaDomain();
        headerCriteriaDomain.setPoId(poId);
        headerCriteriaDomain.setLastUpdateDatetime(
            purchaseOrderAcknowledgementDomain.getLastUpdateDatetime());
        headerValueDomain.setLastUpdateDscId(
            purchaseOrderAcknowledgementDomain.getLastUpdateDscId());
        headerValueDomain.setLastUpdateDatetime(updateDatetime);
        if (foundStatusPending) {
            headerValueDomain.setPoStatus(SupplierPortalConstant.PO_STATUS_PENDING);
        } else {
            headerValueDomain.setPoStatus(SupplierPortalConstant.PO_STATUS_ACKNOWLEDGE);
        }
        int updateHeaderCount = this.spsTPoService.updateByCondition(
            headerValueDomain, headerCriteriaDomain);
        if (Constants.ONE != updateHeaderCount) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0029,
                SupplierPortalConstant.LBL_NOTIFICATION_PENDING_PO);
        }
        
        // 4. Update Purchase Order Detail from SpsTPoDetailService
        this.spsTPoDetailService.update(detailList);
        
        /* 5. Send List of Part No and Pending Reason (Which status is "Pending") to Denso user
         * by e-mail
         * */
        // 5-1) Get Purchase Order Notification data from PurchaseOrderService
        PurchaseOrderNotificationDomain criteria = new PurchaseOrderNotificationDomain();
        criteria.getSpsTPo().setPoId(poId);
        criteria.setMarkPendingFlag(Constants.STR_ONE);
        List<PurchaseOrderNotificationDomain> poNotificationList
            = this.purchaseOrderService.searchPurchaseOrderNotification(criteria);
        if (null == poNotificationList || Constants.ZERO == poNotificationList.size()) {
            return purchaseOrderAcknowledgementDomain;
        }
        
        // 5-2) Get email to send from UserService
        SpsMUserDensoDomain userCriteria = new SpsMUserDensoDomain();
        userCriteria.setDCd(purchaseOrderAcknowledgementDomain.getDCd());
        userCriteria.setDPcd(purchaseOrderAcknowledgementDomain.getDPcd());
        
        // IN[049] Change column name
        //userCriteria.setEmlAbnormalTransfer1Flag(Constants.STR_ONE);
        userCriteria.setEmlPedpoDoalcasnUnmatpnFlg(Constants.STR_ONE);
        
        List<SpsMUserDomain> userList = this.userService.searchEmailUserDenso(userCriteria);
        if (null == userList || Constants.ZERO == userList.size()) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0054);
        }
        
        
        // Start : Not add duplicate email to ToList
        List<String> toList = new ArrayList<String>();
        //for (SpsMUserDomain user : userList) {
        //    if (!Strings.judgeBlank(user.getEmail())) {
        //        toList.add(user.getEmail());
        //    }
        //}
        Set<String> mailToSet = new HashSet<String>();
        for (SpsMUserDomain user : userList) {
            if (!Strings.judgeBlank(user.getEmail())) {
                mailToSet.add(user.getEmail());
            }
        }
        toList.addAll(mailToSet);
        // End : Not add duplicate email to ToList
        
        // 5-3) Get Email Template from Property File
        String subject = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_SUBJECT, null);
        String headerText1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_HEADER_TEXT_LINE1, null);
        String headerText2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_HEADER_TEXT_LINE2, null);
        String headerText3 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_HEADER_TEXT_LINE3, null);
        String headerText4 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_HEADER_TEXT_LINE4, null);
        String headerTable1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_HEADER_TABLE_LINE1, null);
        String headerTable2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_HEADER_TABLE_LINE2, null);
        String detailTable1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_DETAIL_TABLE_LINE1, null);
        String detailTable2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_DETAIL_TABLE_LINE2, null);
        String footer1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_FOOTER_LINE1, null);
        String footer2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_PENDING_PO_FOOTER_LINE2, null);

        // 5-4) Prepare Parameter
        PurchaseOrderNotificationDomain firstNotification = poNotificationList.get(Constants.ZERO);
        StringBuffer emailBuffer = new StringBuffer();
        emailBuffer.append(subject).append(Constants.SYMBOL_SPACE)
            .append(DateUtil.format(updateDatetime, DateUtil.PATTERN_YYYYMMDD_SLASH));
        subject = emailBuffer.toString();

        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SUPPLIER_CODE,
            firstNotification.getSpsTPo().getVendorCd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SUPPLIER_PLANT_CODE,
            firstNotification.getSpsTPo().getSPcd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_DENSO_CODE,
            firstNotification.getSpsTPo().getDCd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_DENSO_PLANT_CODE,
            firstNotification.getSpsTPo().getDPcd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_ISSUE_DATE,
            firstNotification.getStringPoIssueDate());
        
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_CIGMAPONO,
            firstNotification.getSpsTPo().getCigmaPoNo());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_SPSPONO,
            firstNotification.getSpsTPo().getSpsPoNo());
        String newLine = null;
        String allLine = null;
        emailBuffer.setLength(Constants.ZERO);
        for (PurchaseOrderNotificationDomain notification : poNotificationList) {
            newLine = detailTable1;
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DPARTNO,
                fillData(notification.getSpsTPoDetail().getDPn(), Constants.FIFTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPARTNO,
                fillData(notification.getSpsTPoDetail().getSPn(), Constants.TWENTY));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_UNITOFMEASURE,
                fillData(notification.getSpsTPoDetail().getUnitOfMeasure(), Constants.THREE));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DUEDATE,
                fillData(notification.getStringEtd(), Constants.TEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_ORDERQTY,
                fillData(notification.getStringOrderQty(), Constants.SIXTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSPROPOSED_DUDATE,
                fillData(notification.getStringProposedDueDate(), Constants.TEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSPROPOSED_QTY,
                fillData(notification.getStringProposedQty(), Constants.SIXTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSPENDING_REASON,
                fillData(notification.getPendingReason(), Constants.TWENTY));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_MODEL,
                fillData(notification.getSpsTPoDetail().getModel(), Constants.FIFTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_PLANNERCODE,
                fillData(notification.getSpsTPoDetail().getPlannerCode(), Constants.FIVE));
            emailBuffer.append(newLine);
        }
        allLine = emailBuffer.toString();
        
        // 5-5) Use Purchase Order Notification data to create Email from MailService
        String detail = null;
        emailBuffer.setLength(Constants.ZERO);
        emailBuffer.append(headerText1)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText2)
            .append(Constants.SYMBOL_NEWLINE)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText3)
            .append(headerText4)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerTable1)
            .append(headerTable2)
            .append(allLine)
            .append(detailTable2)
            .append(Constants.SYMBOL_NEWLINE)
            .append(footer1)
            .append(Constants.SYMBOL_NEWLINE)
            .append(footer2);
        detail = emailBuffer.toString();
        
        SendEmailDomain sendEmail = new SendEmailDomain();
        sendEmail.setEmailSmtp(ContextParams.getEmailSmtp());
        sendEmail.setEmailFrom(ContextParams.getDefaultEmailFrom());
        sendEmail.setEmailTo(Constants.EMPTY_STRING);
        sendEmail.setToList(toList);
        sendEmail.setHeader(subject);
        sendEmail.setContents(detail);
        
        boolean success = false;
        try {
            success = this.mailService.sendEmail(sendEmail);
        } catch (Exception e) {
            success = false;
        }
        
        if (!success) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0023,
                SupplierPortalConstant.LBL_NOTIFICATION_PENDING_PO);
        }
        
        return purchaseOrderAcknowledgementDomain;
    }

    // [IN054] Add new function DENSO Reply
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderAcknowledgementFacadeService#transactDensoReply(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public PurchaseOrderAcknowledgementDomain transactDensoReply(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain) 
        throws ApplicationException
    {
        Locale locale = purchaseOrderAcknowledgementDomain.getLocale();
        Timestamp updateDatetime = this.commonService.searchSysDate();
        BigDecimal poId = new BigDecimal(purchaseOrderAcknowledgementDomain.getPoId());
        
        // 1. Update P/O status to RPL
        SpsTPoDomain headerValueDomain = new SpsTPoDomain();
        SpsTPoCriteriaDomain headerCriteriaDomain = new SpsTPoCriteriaDomain();
        headerCriteriaDomain.setPoId(poId);
        headerCriteriaDomain.setLastUpdateDatetime(
            purchaseOrderAcknowledgementDomain.getLastUpdateDatetime());
        headerValueDomain.setLastUpdateDscId(
            purchaseOrderAcknowledgementDomain.getLastUpdateDscId());
        headerValueDomain.setLastUpdateDatetime(updateDatetime);
        headerValueDomain.setPoStatus(SupplierPortalConstant.PO_STATUS_REPLY);
        int updateHeaderCount = this.spsTPoService.updateByCondition(
            headerValueDomain, headerCriteriaDomain);
        if (Constants.ONE != updateHeaderCount) {
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0057,
                null);
        }
        
        // 2. Update PN Status from PED to RPL
        SpsTPoDetailCriteriaDomain detailCriteria = new SpsTPoDetailCriteriaDomain();
        detailCriteria.setPoId(poId);
        detailCriteria.setPnStatus(SupplierPortalConstant.PO_STATUS_PENDING);
        SpsTPoDetailDomain detailValue = new SpsTPoDetailDomain();
        detailValue.setPnStatus(SupplierPortalConstant.PO_STATUS_REPLY);
        detailValue.setLastUpdateDscId(purchaseOrderAcknowledgementDomain.getLastUpdateDscId());
        detailValue.setLastUpdateDatetime(updateDatetime);
        this.spsTPoDetailService.updateByCondition(detailValue, detailCriteria);
        
        // 3. Update Supplier Promised Due that not reply yet
        SpsTPoDomain poUpdateDue = new SpsTPoDomain();
        poUpdateDue.setPoId(poId);
        poUpdateDue.setLastUpdateDscId(
            purchaseOrderAcknowledgementDomain.getLastUpdateDscId());
        poUpdateDue.setLastUpdateDatetime(updateDatetime);
        this.purchaseOrderService.updateRejectNotReplyDue(poUpdateDue);
        
        // Search P/O Due for create email
        PurchaseOrderNotificationDomain criteria = new PurchaseOrderNotificationDomain();
        criteria.getSpsTPo().setPoId(poId);
        criteria.setMarkPendingFlag(Constants.STR_ONE);
        List<PurchaseOrderNotificationDomain> poNotificationList
            = this.purchaseOrderService.searchPurchaseOrderNotification(criteria);
        if (null == poNotificationList || Constants.ZERO == poNotificationList.size()) {
            // TODO should it error ?
            return purchaseOrderAcknowledgementDomain;
        }

        // Search supplier user to reply
        PurchaseOrderNotificationDomain firstNotification = poNotificationList.get(Constants.ZERO);
        List<SpsMUserDomain> userList = null;
        UserSupplierDetailDomain userSupplierDomain = new UserSupplierDetailDomain();
        SpsMUserSupplierDomain spsMUserSupplierDomain = new SpsMUserSupplierDomain();
        SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
        
        spsMUserSupplierDomain.setSCd(firstNotification.getSpsTPo().getSCd());
        spsMUserSupplierDomain.setSPcd(firstNotification.getSpsTPo().getSPcd());
        spsMUserSupplierDomain.setEmlUrgentOrderFlag(Constants.STR_ONE);
        spsMUserDomain.setIsActive(Constants.STR_ONE);
        
        userSupplierDomain.setSpsMUserSupplierDomain(spsMUserSupplierDomain);
        userSupplierDomain.setSpsMUserDomain(spsMUserDomain);
        
        userList = userSupplierService.searchEmailUserSupplier(userSupplierDomain);
        if(null == userList || userList.isEmpty()){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_E6_0058,
                null);
        }
        
        List<String> toList = new ArrayList<String>();
        Set<String> mailToSet = new HashSet<String>();
        for (SpsMUserDomain user : userList) {
            if (!Strings.judgeBlank(user.getEmail())) {
                mailToSet.add(user.getEmail());
            }
        }
        toList.addAll(mailToSet);
        
        // Get Email Template from Property File
        String subject = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_SUBJECT, null);
        String headerText1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_HEADER_TEXT_LINE1, null);
        String headerText2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_HEADER_TEXT_LINE2, null);
        String headerText3 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_HEADER_TEXT_LINE3, null);
        String headerText4 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_HEADER_TEXT_LINE4, null);
        String headerTable1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_HEADER_TABLE_LINE1, null);
        String headerTable2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_HEADER_TABLE_LINE2, null);
        String detailTable1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_DETAIL_TABLE_LINE1, null);
        String detailTable2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_DETAIL_TABLE_LINE2, null);
        String footer1 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_FOOTER_LINE1, null);
        String footer2 = MessageUtil.getEmailLabelHandledException(
            locale, SupplierPortalConstant.EMAIL_REPLY_PO_FOOTER_LINE2, null);

        // Set subject and P/O Header Information
        subject = subject.replaceAll(SupplierPortalConstant.REPLACE_SPSPONO,
            firstNotification.getSpsTPo().getSpsPoNo());
        subject = subject.replaceAll(SupplierPortalConstant.REPLACE_ISSUE_DATE,
            firstNotification.getStringPoIssueDate());
        
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SUPPLIER_CODE,
            firstNotification.getSpsTPo().getVendorCd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_SUPPLIER_PLANT_CODE,
            firstNotification.getSpsTPo().getSPcd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_DENSO_CODE,
            firstNotification.getSpsTPo().getDCd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_DENSO_PLANT_CODE,
            firstNotification.getSpsTPo().getDPcd());
        headerText3 = headerText3.replaceAll(SupplierPortalConstant.REPLACE_ISSUE_DATE,
            firstNotification.getStringPoIssueDate());
        
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_CIGMAPONO,
            firstNotification.getSpsTPo().getCigmaPoNo());
        headerText4 = headerText4.replaceAll(SupplierPortalConstant.REPLACE_SPSPONO,
            firstNotification.getSpsTPo().getSpsPoNo());
        
        String newLine = null;
        String allLine = null;
        StringBuffer emailBuffer = new StringBuffer();
        for (PurchaseOrderNotificationDomain notification : poNotificationList) {
            newLine = detailTable1;
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DPARTNO,
                fillData(notification.getSpsTPoDetail().getDPn(), Constants.FIFTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPARTNO,
                fillData(notification.getSpsTPoDetail().getSPn(), Constants.TWENTY));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_UNITOFMEASURE,
                fillData(notification.getSpsTPoDetail().getUnitOfMeasure(), Constants.THREE));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DUEDATE,
                fillData(notification.getStringEtd(), Constants.TEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_ORDERQTY,
                fillData(notification.getStringOrderQty(), Constants.SIXTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSPROPOSED_DUDATE,
                fillData(notification.getStringProposedDueDate(), Constants.TEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSPROPOSED_QTY,
                fillData(notification.getStringProposedQty(), Constants.SIXTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_SPSPENDING_REASON,
                fillData(notification.getPendingReason(), Constants.TWENTY));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_MODEL,
                fillData(notification.getSpsTPoDetail().getModel(), Constants.FIFTEEN));
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_PLANNERCODE,
                fillData(notification.getSpsTPoDetail().getPlannerCode(), Constants.FIVE));
            String replyColor = Constants.COLOR_BLACK;
            if (Constants.STR_ZERO.equals(notification.getSpsTPoDue().getDensoReplyFlg())) {
                replyColor = Constants.COLOR_RED;
            }
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_REPLY_COLOR, replyColor);
            newLine = newLine.replaceAll(SupplierPortalConstant.REPLACE_DENSO_REPLY, 
                SupplierPortalConstant.PO_REPLY_TYPE.get(
                    notification.getSpsTPoDue().getDensoReplyFlg()));
            emailBuffer.append(newLine);
        }
        allLine = emailBuffer.toString();
        
        // 5-5) Use Purchase Order Notification data to create Email from MailService
        String detail = null;
        emailBuffer.setLength(Constants.ZERO);
        emailBuffer.append(headerText1)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText2)
            .append(Constants.SYMBOL_NEWLINE)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerText3)
            .append(headerText4)
            .append(Constants.SYMBOL_NEWLINE)
            .append(headerTable1)
            .append(headerTable2)
            .append(allLine)
            .append(detailTable2)
            .append(Constants.SYMBOL_NEWLINE)
            .append(footer1)
            .append(Constants.SYMBOL_NEWLINE)
            .append(footer2);
        detail = emailBuffer.toString();
        
        SendEmailDomain sendEmail = new SendEmailDomain();
        sendEmail.setEmailSmtp(ContextParams.getEmailSmtp());
        sendEmail.setEmailFrom(ContextParams.getDefaultEmailFrom());
        sendEmail.setEmailTo(Constants.EMPTY_STRING);
        sendEmail.setToList(toList);
        sendEmail.setHeader(subject);
        sendEmail.setContents(detail);
        
        boolean success = false;
        try {
            success = this.mailService.sendEmail(sendEmail);
        } catch (Exception e) {
            success = false;
        }
        
        if (!success) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E5_0023,
                SupplierPortalConstant.LBL_DENSO_REPLY_PENDING_PO);
        }
        
        return purchaseOrderAcknowledgementDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderAcknowledgementFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain)
     */
    public FileManagementDomain searchFileName(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain)
        throws ApplicationException
    {
        FileManagementDomain result = null;
        
        // 1. Find file name from FileManagementService
        try {
            result = this.fileManagementService.searchFileDownload(
                purchaseOrderAcknowledgementDomain.getFileIdSelected(), false, null);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(purchaseOrderAcknowledgementDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
        
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderAcknowledgementFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain, java.io.OutputStream)
     */
    public void searchLegendInfo(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain
        , OutputStream output) throws ApplicationException
    {
        // 1. Get PDF file from FileManagementService
        try {
            this.fileManagementService.searchFileDownload(
                purchaseOrderAcknowledgementDomain.getFileIdSelected(), true, output);
        } catch (IOException ioException) {
            MessageUtil.throwsApplicationMessage(purchaseOrderAcknowledgementDomain.getLocale(),
                SupplierPortalConstant.ERROR_CD_SP_80_0009, null);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * If parameter input shorter than parameter len. Append input with space.
     * @param input - String input
     * @param len - target length
     * @return String that lenght equal to parameter len
     * */
    private String fillData(String input, int len) {
        String appended = null;
        
        if (null == input) {
            appended = Constants.EMPTY_STRING;
        } else {
            appended = input;
        }
        
        StringBuffer result = new StringBuffer();
        result.append(appended);
        for (int i = appended.length(); i < len; i++) {
            result.append(Constants.SYMBOL_SPACE);
        }
        return result.toString();
    }

}


