/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService;
import com.globaldenso.asia.sps.business.domain.AnnounceMessageDomain;

/**
 * <p>
 * The Class ImportMessageToSystemFacadeServiceImpl.
 * </p>
 * <p>
 * This job read CSV file and upload into SPS system.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class ImportMessageToSystemFacadeServiceImpl implements
    ImportMessageToSystemFacadeService {

    /** Call Service CommonService. */
    private CommonService commonService;

    /** Call Service SpsMCompanyDensoService. */
    private SpsMCompanyDensoService spsMCompanyDensoService;

    /** Call Service SpsMAnnounceMessageService. */
    private SpsMAnnounceMessageService spsMAnnounceMessageService;

    /** Call Service SpsTmpFileBackupService. */
    private SpsTmpFileBackupService spsTmpFileBackupService;

    /**
     * Instantiates a new Import message facade service impl.
     */
    public ImportMessageToSystemFacadeServiceImpl() {
        super();
    }

    /**
     * Sets the common service.
     * 
     * @param commonService the new common service
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>
     * Setter method for spsMCompanyDensoService.
     * </p>
     * 
     * @param spsMCompanyDensoService Set for spsMCompanyDensoService
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }

    /**
     * <p>
     * Setter method for spsMAnnounceMessageService.
     * </p>
     * 
     * @param spsMAnnounceMessageService Set for spsMAnnounceMessageService
     */
    public void setSpsMAnnounceMessageService(
        SpsMAnnounceMessageService spsMAnnounceMessageService) {
        this.spsMAnnounceMessageService = spsMAnnounceMessageService;
    }

    /**
     * <p>
     * Setter method for spsTmpFileBackupService.
     * </p>
     * 
     * @param spsTmpFileBackupService Set for spsTmpFileBackupService
     */
    public void setSpsTmpFileBackupService(
        SpsTmpFileBackupService spsTmpFileBackupService) {
        this.spsTmpFileBackupService = spsTmpFileBackupService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService#searchFileConfiguration(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public List<SpsMCompanyDensoDomain> searchFileConfiguration(
        SpsMCompanyDensoCriteriaDomain spsMCompanyDensoCriteriaDomain)
        throws ApplicationException {
        /** Call service SpsMCompanyDensoService searchByCondition() --> search all data. */
        List<SpsMCompanyDensoDomain> spsMCompanyDensoDomains = spsMCompanyDensoService
            .searchByCondition(spsMCompanyDensoCriteriaDomain);
        return spsMCompanyDensoDomains;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService#deleteExpireMessage(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int deleteExpireMessage(
        SpsMAnnounceMessageCriteriaDomain spsMAnnounceMessageCriteriaDomain)
        throws ApplicationException {
        /** Call service SpsMAnnounceMessageService deleteByCondition()  --> delete all data with criteria. */
        int deletedRecordCount = Constants.ZERO;
        deletedRecordCount = spsMAnnounceMessageService
            .deleteByCondition(spsMAnnounceMessageCriteriaDomain);
        return deletedRecordCount;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService#createAnnouncementMessage(com.globaldenso.asia.sps.business.domain.AnnounceMessageDomain)
     */
    public void createAnnouncementMessage(
        List<AnnounceMessageDomain> announceMessageDomainList)
        throws ApplicationException {

        SpsMAnnounceMessageDomain spsMAnnounceMessageDomain = null;
        
        /** Create message from file to SPS tables. */
        for(int i = Constants.ZERO; i < announceMessageDomainList.size(); ++i){
            if (!StringUtil.checkNullOrEmpty(announceMessageDomainList.get(i).getMessageList())) {
                spsMAnnounceMessageDomain = new SpsMAnnounceMessageDomain();
                
                spsMAnnounceMessageDomain
                    .setDCd(announceMessageDomainList.get(i).getDCd());
                spsMAnnounceMessageDomain.setEffectStart(
                    DateUtil.parseToTimestamp(announceMessageDomainList.get(i)
                        .getMessageList().get(Constants.ZERO), DateUtil.PATTERN_YYYYMMDD_SLASH));
                spsMAnnounceMessageDomain.setEffectEnd(
                    DateUtil.parseToTimestamp(announceMessageDomainList.get(i)
                        .getMessageList().get(Constants.ONE), DateUtil.PATTERN_YYYYMMDD_SLASH));
                spsMAnnounceMessageDomain
                    .setAnnounceMessage(announceMessageDomainList.get(i).getMessageList()
                        .get(Constants.TWO));
                spsMAnnounceMessageDomain.setCreateDscId(Constants.SPS_SYSTEM);
                spsMAnnounceMessageDomain.setCreateDatetime(commonService
                    .searchSysDate());

                spsMAnnounceMessageService.create(spsMAnnounceMessageDomain);
            }
        }
        
        /** Call service SpsTmpFileBackupService create() for create file name to temp file backup. */
        SpsTmpFileBackupDomain tmpFileBackupDomain = new SpsTmpFileBackupDomain();
        tmpFileBackupDomain.setFilePath(announceMessageDomainList.
            get(Constants.ZERO).getTempPath());
        tmpFileBackupDomain.setCreateDatetime(commonService.searchSysDate());

        spsTmpFileBackupService.create(tmpFileBackupDomain);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService#searchTempFileBackup(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public List<SpsTmpFileBackupDomain> searchTempFileBackup(
        SpsTmpFileBackupCriteriaDomain spsTmpFileBackupCriteriaDomain)
        throws ApplicationException {
        /** Call service SpsTmpFileBackupService searchByCondition()   --> search all data. */
        List<SpsTmpFileBackupDomain> spsTmpFileBackupDomains = 
            new ArrayList<SpsTmpFileBackupDomain>();
        spsTmpFileBackupDomains = spsTmpFileBackupService
            .searchByCondition(spsTmpFileBackupCriteriaDomain);
        return spsTmpFileBackupDomains;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService#validateMessage(java.util.List)
     */
    public List<String> validateMessage(List<String> message, String[] messageCheck)
    {
        List<String> messageError = new ArrayList<String>();
        boolean effectiveStartDateStatus = true;
        boolean effectiveEndDateStatus = true;
        boolean messageStatus = true;
        
        /** Validate message. */
        if (Constants.THREE != messageCheck.length) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0004);
        }
        if (StringUtil.checkNullOrEmpty(message.get(Constants.ZERO).toString())) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0005);
            effectiveStartDateStatus = false;
        }
        if (StringUtil.checkNullOrEmpty(message.get(Constants.ONE)
            .toString())) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0006);
            effectiveEndDateStatus = false;
        } 
        if (StringUtil.checkNullOrEmpty(message.get(Constants.TWO)
            .toString())) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0007);
            messageStatus = false;
        } 
        if (!DateUtil.isValidDate(
            message.get(Constants.ZERO).toString(), DateUtil.PATTERN_YYYYMMDD_SLASH) 
            && effectiveStartDateStatus) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0008);
        } 
        if (!DateUtil.isValidDate(message.get(Constants.ONE).toString(),
            DateUtil.PATTERN_YYYYMMDD_SLASH) && effectiveEndDateStatus) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0009);
        } 
        if ((Constants.FIVE_HUNDREDTH < message.get(Constants.TWO)
            .toString().length()) && messageStatus) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0010);
        } 
        if ((Constants.ZERO < DateUtil.compareDate(
            message.get(Constants.ZERO).toString(), message.get(Constants.ONE)
                .toString())) && effectiveStartDateStatus && effectiveEndDateStatus) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0011);
        } 
        
        String commonServiceTime = DateUtil.format(commonService.searchSysDate(), 
            DateUtil.PATTERN_YYYYMMDD_SLASH);
        
        if ((Constants.ZERO < DateUtil.compareDate(commonServiceTime, 
            message.get(Constants.ONE).toString())) && effectiveEndDateStatus) {
            messageError.add(SupplierPortalConstant.ERROR_CD_SP_E5_0012);
        }

        return messageError;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.ImportMessageToSystemFacadeService#deleteTempFileBackupExpire(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int deleteTempFileBackupExpire(
        SpsTmpFileBackupCriteriaDomain spsTmpFileBackupCriteriaDomain)
        throws ApplicationException {
        /** Call service SpsTmpFileBackupService delete(). */
        int deletedRecordCount = Constants.ZERO;
        deletedRecordCount = spsTmpFileBackupService
            .delete(spsTmpFileBackupCriteriaDomain);
        return deletedRecordCount;
    }
    
}