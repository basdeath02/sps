/*
 * ModifyDate Development company     Describe 
 * 2014/07/28 CSI Parichat            Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;

/**
 * <p>The Interface InquiryDODetailFacadeService.</p>
 * <p>Facade for InquiryDODetailService.</p>
 * <ul>
 * <li>Method search : searchInitial</li>
 * <li>Method search : searchFileName</li>
 * <li>Method search : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public interface InquiryDoDetailFacadeService {

    
    /**
     * <p>Search Initial</p>
     * <ul>
     * <li>Search initial for inquiry do detail</li>
     * </ul>
     * 
     * @param inquiryDoDetailDomain the inquiry do detail domain
     * @throws ApplicationException ApplicationException
     */
    public void searchInitial(InquiryDoDetailDomain inquiryDoDetailDomain) 
        throws ApplicationException;
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search PDF file name from PDF file id.</li>
     * </ul>
     * 
     * @param inquiryDoDetailDomain the inquiry do detail domain
     * @return the file management domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName (InquiryDoDetailDomain inquiryDoDetailDomain) 
        throws ApplicationException;
    
    /**
     * <p>Search legend info.</p>
     * <ul>
     * <li>Search legend information from PDF file id.</li>
     * </ul>
     * 
     * @param inquiryDoDetailDomain the inquiry do detail domain
     * @param outputStream the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo (InquiryDoDetailDomain inquiryDoDetailDomain,
        OutputStream outputStream) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}
