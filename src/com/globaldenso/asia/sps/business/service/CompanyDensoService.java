/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface CompanyDensoService.</p>
 * <p>Service for Company Denso about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchCompanydensoDetail</li>
 * <li>Method search  : searchDensoCompany</li>
 * <li>Method search  : searchAS400ServerList</li>
 * </ul>
 *
 * @author CSI
 */
public interface CompanyDensoService {
    
    /**
     * <p>Search company denso detail</p>
     * <ul>
     * <li>Search company denso Detail</li>
     * </ul>
     * 
     * @return the list
     */
    public List<CompanyDensoDomain> searchCompanyDensoDetail();

    /**
     * Search DENSO company by user role (or relate to user role).
     * 
     * @param plantDensoWithScope plant information with data scope control
     * @return List of DENSO company
     * */
    public List<CompanyDensoDomain> searchCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScope);
    
    /**
     * Search AS400 service list.
     * 
     * @param companyDensoDomain the common denso domain
     * @return List of AS400 server connection information domain
     * */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain companyDensoDomain);
    
    /**
     * Search Denso Company By Code List.
     * 
     * @param companyDensoDomain the SPS master company denso domain
     * @return the list of SPS master company denso domain
     * */
    public List<SpsMCompanyDensoDomain> searchCompanyDensoByCodeList(
        SpsMCompanyDensoDomain companyDensoDomain);
    
    /**
     * Search DENSO Exist company by user Relation.
     * 
     * @param companyDensoWithScopeDomain Supplier DENSO with scope
     * @return count of company supplier.
     * */
    public Integer searchExistCompanyDenso
        (CompanyDensoWithScopeDomain companyDensoWithScopeDomain);
}