/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2015/08/04 CSI Akat                  [IN009]
 * 2015/09/30 CSI Akat                  [IN020]
 * 2015/10/06 CSI Akat                  [IN016]
 * 2015/11/25 CSI Akat                  [IN040]
 * 2018/04/19 Netband U.Rungsiwut       Generate invoice PDF via click on screen
 * 2018/11/30 CTC P.Pawan               [IN1781]
 * 2019/02/04 CTC P.Pawan               [IN1898]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_DETAIL_TABLE_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_DETAIL_TABLE_LINE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_FOOTER_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_FOOTER_LINE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TABLE_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TABLE_LINE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE2_CANCEL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE4;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE5;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_SUBJECT_INVOICE_CANCEL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_ASNNO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_CN_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_CN_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_COVER_PAGE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_DCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_DIFF_BASE_AMT_BY_PART;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_DPARTNO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_DPCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_D_UNIT_OF_MEASURE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_D_UNIT_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_FIRST_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_GOOD_RCV_STATUS;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_INVOICE_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_INVOICE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_LAST_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_MIDDLE_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_PLAN_ETA;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_ROW_COLOR;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_SCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_SHIPPING_QTY;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_S_UNIT_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_TEMP_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_TOTAL_CN_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_TOTAL_INVOICE_AMOUNT;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTCnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceDetailDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The class InvoiceInformationFacadeServiceImpl.</p>
 * <p>Facade for InvoiceInformationFacadeServiceImpl.</p>
 * <ul>
 * <li>Method initial : searchInitial</li>
 * <li>Method search  : searchInvoiceInformation</li>
 * <li>Method update  : updateInvoiceStatus</li>
 * <li>Method create  : downloadInvoiceInformation</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchInvoiceCoverPage</li>
 * <li>Method search  : searchSelectedDensoCompany</li>
 * <li>Method search  : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public class InvoiceInformationFacadeServiceImpl implements InvoiceInformationFacadeService {

    /** The file management service. */
    private InvoiceService invoiceService = null;
    
    /** The common service. */
    private CommonService commonService = null;
    
    /** The misc service. */
    private MiscellaneousService miscService = null;
    
    /** The sps invoice service. */
    private SpsTInvoiceService spsTInvoiceService = null;
    
    /** The sps invoice service. */
    private SpsTAsnService spsTAsnService = null;
    
    /** The sps cn service. */
    private SpsTCnService spsTCnService = null;
    
    /** The file management service. */
    private FileManagementService fileManagementService = null;
    
    /** The Denso Supplier Relation service. */
    private DensoSupplierRelationService densoSupplierRelationService = null; 
    
    /** The Plant Denso Service. */
    private PlantDensoService plantDensoService = null;
    
    /** The Supplier company service. */
    private CompanySupplierService companySupplierService = null;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;

    /** The sps master company denso service. */
    private SpsMCompanyDensoService spsMCompanyDensoService = null;

    /** Mail Service */
    private MailService mailService;

    /** UserSupplierService */
    private UserSupplierService userSupplierService;

    /** Service for SPS_M_USER. */
    private SpsMUserService spsMUserService;

    /** Service for InvoiceMaintenanceFacadeService. */
    private InvoiceMaintenanceFacadeService invoiceMaintenanceFacadeService;

    /**
     * Instantiates a new upload facade service impl.
     */
    public InvoiceInformationFacadeServiceImpl(){
        super();
    }

    /**
     * Set the file invoice service.
     * 
     * @param invoiceService the invoice service to set
     */
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the sps invoice service.
     * 
     * @param spsTInvoiceService the sps invoice service to set
     */
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService) {
        this.spsTInvoiceService = spsTInvoiceService;
    }
    
    /**
     * Set the SPS ASN Service.
     * 
     * @param spsTAsnService the SPS ASN Service to set
     */
    public void setSpsTAsnService(SpsTAsnService spsTAsnService) {
        this.spsTAsnService = spsTAsnService;
    }
    
    /**
     * Set the SPS CN Service.
     * 
     * @param spsTCnService the SPS CN Service to set
     */
    public void setSpsTCnService(SpsTCnService spsTCnService) {
        this.spsTCnService = spsTCnService;
    }
    
    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService)
    {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Set the plant denso service.
     * 
     * @param plantDensoService the plant denso service to set
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * Set the sps master company denso service.
     * 
     * @param spsMCompanyDensoService the sps master company denso service to set
     */
    public void setSpsMCompanyDensoService(SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * <p>Setter method for mailService.</p>
     *
     * @param mailService Set for mailService
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * <p>Setter method for spsMUserService.</p>
     *
     * @param spsMUserService Set for spsMUserService
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    
    /**
     * <p>Setter method for supplierUserService.</p>
     *
     * @param supplierUserService Set for supplierUserService
     */
    public void setUserSupplierService(UserSupplierService supplierUserService) {
        this.userSupplierService = supplierUserService;
    }
    
    /**
     * <p>Setter method for invoiceMaintenanceFacadeService.</p>
     *
     * @param invoiceMaintenanceFacadeService Set for invoiceMaintenanceFacadeService
     */
    public void setInvoiceMaintenanceFacadeService(InvoiceMaintenanceFacadeService invoiceMaintenanceFacadeService) {
        this.invoiceMaintenanceFacadeService = invoiceMaintenanceFacadeService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#initial(
     * com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public InvoiceInformationReturnDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain, 
        InvoiceInformationDomain invoiceInformationDomain)throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        InvoiceInformationReturnDomain invoiceInformationReturnDomain 
            = new InvoiceInformationReturnDomain();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<CompanySupplierDomain> companySupplierList = null;
        List<CompanyDensoDomain> companyDensoList = null;
        List<PlantDensoDomain> plantDensoList = null;
        MiscellaneousDomain invoiceStatusRcvMiscDomain = null;
        MiscellaneousDomain invoiceStatusMiscDomain = null;
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            dataScopeControlDomain);
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                Constants.DENSO_AND_SUPPLIER_RELATION);
        }
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationList);

        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companySupplierList = this.companySupplierService.searchCompanySupplier(
            plantSupplierWithScope);
        if(Constants.ZERO == companySupplierList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        invoiceInformationReturnDomain.setCompanySupplierList(companySupplierList);

        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoList = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(Constants.ZERO == companyDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        invoiceInformationReturnDomain.setCompanyDensoList(companyDensoList);
        
        if(null != invoiceInformationDomain.getDCd()){
            if(!Constants.MISC_CODE_ALL.equals(invoiceInformationDomain.getDCd())){
                plantDensoWithScope.getPlantDensoDomain().setDCd(
                    invoiceInformationDomain.getDCd());
            }
        }
        plantDensoList = this.plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(Constants.ZERO == plantDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        invoiceInformationReturnDomain.setPlantDensoList(plantDensoList);
        
        invoiceStatusRcvMiscDomain = new MiscellaneousDomain();
        invoiceStatusRcvMiscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_INVOICE_RCV_STATUS);
        List<MiscellaneousDomain> invoiceRcvStatusList
            = miscService.searchMisc(invoiceStatusRcvMiscDomain);
        if(Constants.ZERO == invoiceRcvStatusList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_INVOICE_RCV_STATUS);
        }
        invoiceInformationReturnDomain.setInvoiceRcvStatusList(invoiceRcvStatusList);
        
        invoiceStatusMiscDomain = new MiscellaneousDomain();
        invoiceStatusMiscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_INVOICE_STATUS);
        List<MiscellaneousDomain> invoiceStatusList
            = miscService.searchMisc(invoiceStatusMiscDomain);
        if(Constants.ZERO == invoiceStatusList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_INVOICE_STATUS);
        }
        invoiceInformationReturnDomain.setInvoiceStatusList(invoiceStatusList);
        
        return invoiceInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchInvoiceInformation(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public InvoiceInformationReturnDomain searchInvoiceInformation(
        InvoiceInformationDomain invoiceInformationDomain) throws ApplicationException
    {
        Locale locale = invoiceInformationDomain.getLocale();
        InvoiceInformationReturnDomain invoiceInformationReturnDomain = 
            new InvoiceInformationReturnDomain();
        
        List<ApplicationMessageDomain> errorMessageList = this.validate(invoiceInformationDomain);
        if(null != errorMessageList && Constants.ZERO < errorMessageList.size()){
            
            invoiceInformationReturnDomain.setErrorMessageList(errorMessageList);
        }else{
            
            Integer recordLimit = Constants.ZERO;
            Integer recordLimitPerPage = Constants.ZERO;
            Integer recordCount = Constants.ZERO;
            List<InvoiceInformationDomain> result = null;
            
            recordCount = this.invoiceService.searchCountInvoiceInformation(
                invoiceInformationDomain);
            if(Constants.ZERO == recordCount){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
            miscLimitDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            miscLimitDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV001_RLM);
            recordLimit = miscService.searchMiscValue(miscLimitDomain);
            if(null == recordLimit){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032);
            }else if(recordLimit < recordCount){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    new String[]{String.valueOf(recordLimit)});
            }
            
            MiscellaneousDomain miscLimitPerPageDomain = new MiscellaneousDomain();
            miscLimitPerPageDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            miscLimitPerPageDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV001_PLM);
            recordLimitPerPage = miscService.searchMiscValue(miscLimitPerPageDomain);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032);
            }
            invoiceInformationDomain.setMaxRowPerPage(recordLimitPerPage);
            
            result = this.invoiceService.searchInvoiceInformation(invoiceInformationDomain);
            if(Constants.ZERO == result.size()){
                
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            Set<String> densoCodeList = new TreeSet<String>();
            Map<String, List<InvoiceInformationDomain>> invoiceInformationResultMap 
                = new HashMap<String, List<InvoiceInformationDomain>>();
            Map<String, Set<String>> asnNoMap = new HashMap<String, Set<String>>();
            for(InvoiceInformationDomain invoiceItem:result){
                Set<String> asnNoList = new TreeSet<String>();
                for(InvoiceDetailDomain invoiceDetailItem:invoiceItem.getInvoiceDetailDomainList()){
                    asnNoList.add(invoiceDetailItem.getSpsTInvoiceDetailDomain().getAsnNo());
                }
                
                String densoCode = invoiceItem.getSpsTInvoiceDomain().getDCd();
                densoCodeList.add(densoCode);
                if(invoiceInformationResultMap.containsKey(densoCode)){
                    
                    invoiceInformationResultMap.get(densoCode).add(invoiceItem);
                    asnNoMap.get(densoCode).addAll(asnNoList);
                }else{
                    
                    List<InvoiceInformationDomain> invoiceInformationList 
                        = new ArrayList<InvoiceInformationDomain>();
                    invoiceInformationList.add(invoiceItem);
                    invoiceInformationResultMap.put(densoCode, invoiceInformationList);
                    
                    asnNoMap.put(densoCode, asnNoList);
                }
            }
            
            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
            SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
                = new SpsMCompanyDensoDomain();
            companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
            List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
                = this.companyDensoService.searchAs400ServerList(
                    companyDensoCriteriaForSearchAS400);
            if(Constants.ZERO == spsMAs400SchemaList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            if(spsMAs400SchemaList.size() != densoCodeList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0025,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            
            Map<String, String> ansStatusmap = new HashMap<String, String>();
            ansStatusmap.put(Constants.ASN_STATUS_N, Constants.ASN_STATUS_ISS);
            ansStatusmap.put(Constants.ASN_STATUS_D, Constants.ASN_STATUS_CCL);
            ansStatusmap.put(Constants.ASN_STATUS_P, Constants.ASN_STATUS_PTR);
            ansStatusmap.put(Constants.ASN_STATUS_R, Constants.ASN_STATUS_RCP);
            List<InvoiceInformationDomain> invoiceInformationResultList 
                = new ArrayList<InvoiceInformationDomain>();
            for(As400ServerConnectionInformationDomain as400Server : spsMAs400SchemaList){
                
                String asnNoStr = 
                    StringUtil.convertListToVarcharCommaSeperate(
                        asnNoMap.get(as400Server.getSpsMCompanyDensoDomain().getDCd()));
                List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
                    CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                        asnNoStr, null, as400Server, locale);
                
                List<InvoiceInformationDomain> invoiceInformationList 
                    = invoiceInformationResultMap.get(
                        as400Server.getSpsMCompanyDensoDomain().getDCd()); 
                Iterator<InvoiceInformationDomain> iterator = invoiceInformationList.iterator();
                if(Constants.ZERO < cigmaAsnDomainList.size()){
                    
                    boolean isBreak = false;
                    while(iterator.hasNext()) {
                        InvoiceInformationDomain invoiceItem = iterator.next();
                        invoiceItem.setInvoiceRcvStatus(
                            SupplierPortalConstant.OK_INVOICE_RCV_STATUS);
                        for(InvoiceDetailDomain invoiceDetial
                            :invoiceItem.getInvoiceDetailDomainList()){
                            isBreak = false;
                            for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList){
                                if(invoiceDetial.getSpsTInvoiceDetailDomain().getAsnNo().trim()
                                    .equals(cigmaAsnitem.getPseudoAsnNo().trim())
                                    && !Constants.ASN_STATUS_R.equals(
                                        cigmaAsnitem.getPseudoAsnStatus())){
                                    invoiceItem.setInvoiceRcvStatus(
                                        SupplierPortalConstant.NO_INVOICE_RCV_STATUS);
                                    isBreak = true;
                                    break;
                                }
                            }
                            if(isBreak){
                                break;
                            }
                        }
                        if(!Constants.MISC_CODE_ALL.equals(
                            invoiceInformationDomain.getInvoiceRcvStatus())
                            && !invoiceInformationDomain.getInvoiceRcvStatus().equals(
                                invoiceItem.getInvoiceRcvStatus())){
                            iterator.remove();
                        }
                    }
                }else{
                    while(iterator.hasNext()) {
                        InvoiceInformationDomain invoiceItem = iterator.next();
                        invoiceItem.setInvoiceRcvStatus(
                            SupplierPortalConstant.NO_INVOICE_RCV_STATUS);
                        
                        if(!Constants.MISC_CODE_ALL.equals(
                            invoiceInformationDomain.getInvoiceRcvStatus())
                            && !invoiceInformationDomain.getInvoiceRcvStatus().equals(
                                invoiceItem.getInvoiceRcvStatus())){
                            iterator.remove();
                        }
                    }
                }
                
                invoiceInformationResultList.addAll(invoiceInformationList);
            }
            
            if(Constants.ZERO == invoiceInformationResultList.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            SpsPagingUtil.calcPaging(invoiceInformationDomain, 
                invoiceInformationResultList.size());
            invoiceInformationReturnDomain.setInvoiceInformationDomainList(
                invoiceInformationResultList);
        }
        
        return invoiceInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchInvoiceInformation(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public InvoiceInformationReturnDomain searchInvoiceInformationCsv(
        InvoiceInformationDomain invoiceInformationDomain) throws ApplicationException
    {
        Locale locale = invoiceInformationDomain.getLocale();
        InvoiceInformationReturnDomain invoiceInformationReturnDomain = 
            new InvoiceInformationReturnDomain();
        
        List<ApplicationMessageDomain> errorMessageList = this.validate(invoiceInformationDomain);
        if(null != errorMessageList && Constants.ZERO < errorMessageList.size()){
            
            invoiceInformationReturnDomain.setErrorMessageList(errorMessageList);
        }else{
            Integer recordLimit = Constants.ZERO;
            Integer recordCount = Constants.ZERO;
            List<InvoiceInformationDomain> result = null;
            
            recordCount = this.invoiceService.searchCountInvoiceInformation(
                invoiceInformationDomain);
            if(Constants.ZERO == recordCount){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
            miscLimitDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_CSV);
            miscLimitDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV001_RLM);
            recordLimit = miscService.searchMiscValue(miscLimitDomain);
            if(null == recordLimit){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032);
            }else if(recordLimit < recordCount){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    new String[]{String.valueOf(recordLimit)});
            }
            
            result = this.invoiceService.searchInvoiceInformation(invoiceInformationDomain);
            if(Constants.ZERO == result.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            Set<String> densoCodeList = new TreeSet<String>();
            Map<String, List<InvoiceInformationDomain>> invoiceInformationResultMap 
                = new HashMap<String, List<InvoiceInformationDomain>>();
            Map<String, Set<String>> asnNoMap = new HashMap<String, Set<String>>();
            for(InvoiceInformationDomain invoiceItem:result){
                Set<String> asnNoList = new TreeSet<String>();
                for(InvoiceDetailDomain invoiceDetailItem:invoiceItem.getInvoiceDetailDomainList()){
                    asnNoList.add(invoiceDetailItem.getSpsTInvoiceDetailDomain().getAsnNo());
                }
                
                String densoCode = invoiceItem.getSpsTInvoiceDomain().getDCd();
                densoCodeList.add(densoCode);
                if(invoiceInformationResultMap.containsKey(densoCode)){
                    
                    invoiceInformationResultMap.get(densoCode).add(invoiceItem);
                    asnNoMap.get(densoCode).addAll(asnNoList);
                }else{
                    
                    List<InvoiceInformationDomain> invoiceInformationList 
                        = new ArrayList<InvoiceInformationDomain>();
                    invoiceInformationList.add(invoiceItem);
                    invoiceInformationResultMap.put(densoCode, invoiceInformationList);
                    
                    asnNoMap.put(densoCode, asnNoList);
                }
            }
            
            String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
            SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
                = new SpsMCompanyDensoDomain();
            companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
            List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
                = this.companyDensoService.searchAs400ServerList(
                    companyDensoCriteriaForSearchAS400);
            if(Constants.ZERO == spsMAs400SchemaList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            if(spsMAs400SchemaList.size() != densoCodeList.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E5_0025,
                    SupplierPortalConstant.LBL_DENSO_COMPANY);
            }
            
            Map<String, String> ansStatusmap = new HashMap<String, String>();
            ansStatusmap.put(Constants.ASN_STATUS_N, Constants.ASN_STATUS_ISS);
            ansStatusmap.put(Constants.ASN_STATUS_D, Constants.ASN_STATUS_CCL);
            ansStatusmap.put(Constants.ASN_STATUS_P, Constants.ASN_STATUS_PTR);
            ansStatusmap.put(Constants.ASN_STATUS_R, Constants.ASN_STATUS_RCP);
            List<InvoiceInformationDomain> invoiceInformationResultList 
                = new ArrayList<InvoiceInformationDomain>();
            for(As400ServerConnectionInformationDomain as400Server : spsMAs400SchemaList){
                
                String asnNoStr = 
                    StringUtil.convertListToVarcharCommaSeperate(
                        asnNoMap.get(as400Server.getSpsMCompanyDensoDomain().getDCd()));
                List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
                    CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                        asnNoStr, null, as400Server, locale);
                
                List<InvoiceInformationDomain> invoiceInformationList 
                    = invoiceInformationResultMap.get(
                        as400Server.getSpsMCompanyDensoDomain().getDCd()); 
                Iterator<InvoiceInformationDomain> iterator = invoiceInformationList.iterator();
                if(Constants.ZERO < cigmaAsnDomainList.size()){
                    
                    // [IN040] If found receive not complete, not have to break, just continue.
                    //boolean isBreak = false;

                    // [IN040] check ASN No. and Part Number
                    String invoicePart = null;
                    String cigmaPart = null;
                    String invoiceAsnNo = null;
                    String cigmaAsnNo = null;
                    
                    while(iterator.hasNext()) {
                        InvoiceInformationDomain invoiceItem = iterator.next();
                        invoiceItem.setInvoiceRcvStatus(
                            SupplierPortalConstant.OK_INVOICE_RCV_STATUS);
                        for(InvoiceDetailDomain invoiceDetial
                            : invoiceItem.getInvoiceDetailDomainList())
                        {
                            // [IN040] If found receive not complete, not have to break, just continue.
                            //isBreak = false;
                            for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList) {
                                
                                // Start : [IN040] check ASN No. and Part Number
                                //if(invoiceDetial.getSpsTInvoiceDetailDomain().getAsnNo().trim()
                                //    .equals(cigmaAsnitem.getPseudoAsnNo().trim()))
                                //{
                                if (null != invoiceDetial.getSpsTInvoiceDetailDomain().getAsnNo()) {
                                    invoiceAsnNo = invoiceDetial.getSpsTInvoiceDetailDomain()
                                        .getAsnNo().trim();
                                } else {
                                    invoiceAsnNo = Constants.EMPTY_STRING;
                                }
                                if (null != cigmaAsnitem.getPseudoAsnNo()) {
                                    cigmaAsnNo = cigmaAsnitem.getPseudoAsnNo().trim();
                                } else {
                                    cigmaAsnNo = Constants.EMPTY_STRING;
                                }
                                if (null != invoiceDetial.getSpsTInvoiceDetailDomain().getDPn()) {
                                    invoicePart = invoiceDetial.getSpsTInvoiceDetailDomain()
                                        .getDPn().trim();
                                } else {
                                    invoicePart = Constants.EMPTY_STRING;
                                }
                                if (null != cigmaAsnitem.getPseudoDPn()) {
                                    cigmaPart = cigmaAsnitem.getPseudoDPn().trim();
                                } else {
                                    cigmaPart = Constants.EMPTY_STRING;
                                }
                                if (invoiceAsnNo.equals(cigmaAsnNo)
                                    && invoicePart.equals(cigmaPart)) 
                                {
                                // End : [IN040] check ASN No. and Part Number
                                    
                                    invoiceDetial.getSpsTAsnDomain().setAsnStatus(
                                        ansStatusmap.get(cigmaAsnitem.getPseudoAsnStatus()));
                                    if(!StringUtil.checkNullOrEmpty(
                                        cigmaAsnitem.getPseudoReceivedDate().trim())
                                        && !Constants.STR_ZERO.equals(
                                            cigmaAsnitem.getPseudoReceivedDate().trim()))
                                    {
                                        Date receivingDate = DateUtil.parseToSqlDate(
                                            cigmaAsnitem.getPseudoReceivedDate().trim(),
                                            DateUtil.PATTERN_YYYYMMDD);
                                        invoiceDetial.setReceivedDateStr(DateUtil.format(
                                            receivingDate, DateUtil.PATTERN_YYYYMMDD_SLASH));
                                    }
                                    if(!Constants.ASN_STATUS_R.equals(
                                        cigmaAsnitem.getPseudoAsnStatus())){
                                        invoiceItem.setInvoiceRcvStatus(
                                            SupplierPortalConstant.NO_INVOICE_RCV_STATUS);
                                        
                                        // [IN040] If found receive not complete, not have to break, just continue.
                                        //isBreak = true;
                                        //break;
                                    }
                                }
                            }

                            // [IN040] If found receive not complete, not have to break, just continue.
                            //if(isBreak){
                            //    break;
                            //}
                        }
                        if(!Constants.MISC_CODE_ALL.equals(
                            invoiceInformationDomain.getInvoiceRcvStatus())
                            && !invoiceInformationDomain.getInvoiceRcvStatus().equals(
                                invoiceItem.getInvoiceRcvStatus())){
                            iterator.remove();
                        }
                    }
                }else{
                    while(iterator.hasNext()) {
                        InvoiceInformationDomain invoiceItem = iterator.next();
                        invoiceItem.setInvoiceRcvStatus(
                            SupplierPortalConstant.NO_INVOICE_RCV_STATUS);
                        
                        if(!Constants.MISC_CODE_ALL.equals(
                            invoiceInformationDomain.getInvoiceRcvStatus())
                            && !invoiceInformationDomain.getInvoiceRcvStatus().equals(
                                invoiceItem.getInvoiceRcvStatus())){
                            iterator.remove();
                        }
                    }
                }
                invoiceInformationResultList.addAll(invoiceInformationList);
            }
            
            if(Constants.ZERO == result.size()){
                MessageUtil.throwsApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
            
            String fileName = StringUtil.appendsString(
                Constants.INVOICE_LIST_INFORMATION,
                Constants.SYMBOL_UNDER_SCORE,
                DateUtil.format(this.commonService.searchSysDate(), 
                    DateUtil.PATTERN_YYYYMMDD_HHMM));
            
            final String[] headerArr = new String[] {
                SupplierPortalConstant.LBL_INVOICE_STATUS,
                SupplierPortalConstant.LBL_INVOICE_NO,
                SupplierPortalConstant.LBL_INVOICE_DATE,
                SupplierPortalConstant.LBL_CREDIT_NOTE_NO,
                SupplierPortalConstant.LBL_CREDIT_NOTE_DATE,
                SupplierPortalConstant.LBL_COVER_PAGE_NO,
                SupplierPortalConstant.LBL_INVOICE_RCV_STATUS,
                SupplierPortalConstant.LBL_RECEIVED_DATE,
                SupplierPortalConstant.LBL_SUPPLIER_CURRENCY,
                SupplierPortalConstant.LBL_VAT_TYPE,
                SupplierPortalConstant.LBL_VAT_RATE,
                SupplierPortalConstant.LBL_DENSO_TAX_ID,
                SupplierPortalConstant.LBL_SUPPLIER_TAX_ID,
                SupplierPortalConstant.LBL_S_CD,
                SupplierPortalConstant.LBL_S_PCD,
                SupplierPortalConstant.LBL_D_CD,
                SupplierPortalConstant.LBL_D_PCD,
                SupplierPortalConstant.LBL_SUPPLIER_BASE_AMOUNT,
                SupplierPortalConstant.LBL_SUPPLIER_VAT_AMOUNT,
                SupplierPortalConstant.LBL_SUPPLIER_TOTAL_AMOUNT,
                SupplierPortalConstant.LBL_CREDIT_NOTE_BASE_AMOUNT,
                SupplierPortalConstant.LBL_CREDIT_NOTE_VAT_AMOUNT,
                SupplierPortalConstant.LBL_CREDIT_NOTE_TOTAL_AMOUNT,
                SupplierPortalConstant.LBL_DENSO_BASE_AMOUNT,
                SupplierPortalConstant.LBL_DENSO_VAT_AMOUNT,
                SupplierPortalConstant.LBL_DENSO_TOTAL_AMOUNT,
                SupplierPortalConstant.LBL_DIFFERENCE_BASE_AMOUNT,
                SupplierPortalConstant.LBL_DIFFERENCE_VAT_GST_AMOUNT,
                SupplierPortalConstant.LBL_TOTAL_DIFFERENCE_AMOUNT,
                SupplierPortalConstant.LBL_PAYMENT_DATE,
                SupplierPortalConstant.LBL_ASN_DATE,
                SupplierPortalConstant.LBL_ASN_NO,
                SupplierPortalConstant.LBL_ASN_STATUS,
                SupplierPortalConstant.LBL_TRIP_NO,
                SupplierPortalConstant.LBL_PLAN_ETA,
                SupplierPortalConstant.LBL_ACTUAL_ETD,
                SupplierPortalConstant.LBL_D_PN,
                SupplierPortalConstant.LBL_DENSO_UNIT_OF_MEASURE,
                SupplierPortalConstant.LBL_DENSO_PRICE_UNIT,
                SupplierPortalConstant.LBL_TEMP_PRICE,
                SupplierPortalConstant.LBL_S_PN,
                SupplierPortalConstant.LBL_SHIPPING_QTY,
                SupplierPortalConstant.LBL_SUPPLIER_PRICE_UNIT,
                SupplierPortalConstant.LBL_SUPPLIER_BASE_AMOUNT_BY_PN,
                SupplierPortalConstant.LBL_DENSO_BASE_AMOUNT_BY_PN
            };
            
            List<Map<String, Object>> resultDetail = this.getInvoiceInformationMap(
                invoiceInformationResultList, headerArr);
            CommonDomain commonDomain = new CommonDomain();
            commonDomain.setLocale(locale);
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            try {
                invoiceInformationReturnDomain.setCsvResult(
                    this.commonService.createCsvString(commonDomain));
                invoiceInformationReturnDomain.setFileName(fileName);
                
            } catch (IOException ioe) {
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012);
            }
        }
        return invoiceInformationReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#transactCancelInvoice(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationReturnDomain)
     */
    //Todo
    public boolean transactCancelInvoice(
        InvoiceInformationReturnDomain invoiceInformationReturnDomain) throws ApplicationException
    {
        Locale locale = invoiceInformationReturnDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = null;
        
        if(null == invoiceInformationReturnDomain.getInvoiceInformationDomainList()
            || invoiceInformationReturnDomain.getInvoiceInformationDomainList().isEmpty()){
            errorMessageList = new ArrayList<ApplicationMessageDomain>();
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0012,
                    SupplierPortalConstant.LBL_INVOICE_NO)));
            
            invoiceInformationReturnDomain.setErrorMessageList(errorMessageList);
            return false;
        }
        //validate invoice
        List<InvoiceInformationDomain> invoiceInformationList
            = invoiceInformationReturnDomain.getInvoiceInformationDomainList();
        StringBuffer invoiceNoErrorListStr = new StringBuffer();
        for(InvoiceInformationDomain inv : invoiceInformationList){
            if(!Constants.INVOICE_STATUS_ISS.equals(inv.getSpsTInvoiceDomain().getInvoiceStatus())
                && !Constants.INVOICE_STATUS_ISC.equals(
                    inv.getSpsTInvoiceDomain().getInvoiceStatus())){
                invoiceNoErrorListStr.append(inv.getSpsTInvoiceDomain().getInvoiceNo());
                invoiceNoErrorListStr.append(Constants.SYMBOL_COMMA).append(Constants.SYMBOL_SPACE);
            }
        }
        
        if(!StringUtil.checkNullOrEmpty(invoiceNoErrorListStr)){
            StringBuffer msgError = new StringBuffer();
            String invoiceNoError = invoiceNoErrorListStr.toString().trim();
            msgError.append(MessageUtil.getLabelHandledException(
                locale, SupplierPortalConstant.LBL_CANCEL));
            msgError.append(Constants.SYMBOL_SPACE);
            msgError.append(MessageUtil.getLabelHandledException(
                locale, SupplierPortalConstant.LBL_INVOICE_NO));
            msgError.append(Constants.SYMBOL_SPACE);
            msgError.append(invoiceNoError.substring(Constants.ZERO,
                invoiceNoError.length() - Constants.ONE));
            msgError.append(Constants.SYMBOL_SPACE);
            msgError.append(MessageUtil.getLabelHandledException(
                locale, SupplierPortalConstant.LBL_BECAUSE_ALREADY_TRANSFERED_OR_CANCELLED));
            
            errorMessageList = new ArrayList<ApplicationMessageDomain>();
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0037,
                    new String[] {msgError.toString()})));
            
            invoiceInformationReturnDomain.setErrorMessageList(errorMessageList);
            return false;
        }
        
        for(InvoiceInformationDomain inv : invoiceInformationList){
//            SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
//                = new SpsMCompanyDensoDomain();
//            String dCd = StringUtil.appendsString(
//                Constants.SYMBOL_SINGLE_QUOTE, inv.getSpsTInvoiceDomain().getDCd(),
//                Constants.SYMBOL_SINGLE_QUOTE);
//            companyDensoCriteriaForSearchAS400.setDCd(dCd);
//            List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
//                = this.companyDensoService.searchAs400ServerList(
//                    companyDensoCriteriaForSearchAS400);
//            if(Constants.ZERO == spsMAs400SchemaList.size()){
//                MessageUtil.throwsApplicationMessageWithLabel(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_E5_0026,
//                    SupplierPortalConstant.LBL_DENSO_COMPANY);
//            }
//            List<String> asnNoList = new ArrayList<String>();
//            for(InvoiceDetailDomain invoiceDetailItem : invoiceDetailList){
//                if(!asnNoList.contains(invoiceDetailItem.getSpsTAsnDomain().getAsnNo())){
//                    asnNoList.add(invoiceDetailItem.getSpsTAsnDomain().getAsnNo());
//                }
//            }
//            String asnNoStr = StringUtil.convertListToVarcharCommaSeperate(asnNoList);
//            List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
//                CommonWebServiceUtil.asnResourceSearchAsnReceiving(
//                    asnNoStr, null, spsMAs400SchemaList.get(Constants.ZERO));
//            
//            boolean receivedCompleted = true;
//            for(PseudoCigmaAsnDomain cigmaAsnItem : cigmaAsnDomainList){
//                if(!Constants.ASN_STATUS_R.equals(cigmaAsnItem.getPseudoAsnStatus())){
//                    receivedCompleted = false;
//                    break;
//                }
//            }
//            if(receivedCompleted){
//                StringBuffer msgError = new StringBuffer();
//                msgError.append(MessageUtil.getLabelHandledException(
//                    locale, SupplierPortalConstant.LBL_CANCEL));
//                msgError.append(Constants.SYMBOL_SPACE);
//                msgError.append(MessageUtil.getLabelHandledException(
//                    locale, SupplierPortalConstant.LBL_INVOICE_NO));
//                msgError.append(Constants.SYMBOL_SPACE);
//                msgError.append(inv.getSpsTInvoiceDomain().getInvoiceNo());
//                msgError.append(Constants.SYMBOL_SPACE);
//                msgError.append(MessageUtil.getLabelHandledException(
//                    locale, SupplierPortalConstant.LBL_BECAUSE_RECEIVED_COMPLETED));
//                msgError.append(Constants.SYMBOL_DOT);
//                
//                MessageUtil.throwsApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_E7_0037, new String[] {msgError.toString()});
//            }
            
            InvoiceInformationDomain invoiceInformationDomainForUpdateAsn 
                = new InvoiceInformationDomain();
            invoiceInformationDomainForUpdateAsn.getSpsTInvoiceDomain().setInvoiceId(
                inv.getSpsTInvoiceDomain().getInvoiceId());
            List<InvoiceInformationDomain> invoiceDetailInformation 
                = this.invoiceService.searchInvoiceInformation(
                    invoiceInformationDomainForUpdateAsn);
            
            List<InvoiceDetailDomain> invoiceDetailList
                = invoiceDetailInformation.get(Constants.ZERO).getInvoiceDetailDomainList();
            
            Timestamp sysDate = this.commonService.searchSysDate();
            SpsTInvoiceDomain spsTInvoiceDomain = new SpsTInvoiceDomain();
            spsTInvoiceDomain.setInvoiceStatus(Constants.INVOICE_STATUS_DCL);
            spsTInvoiceDomain.setLastUpdateDscId(invoiceInformationReturnDomain.getDscId());
            spsTInvoiceDomain.setLastUpdateDatetime(sysDate);
            
            SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain = new SpsTInvoiceCriteriaDomain();
            spsTInvoiceCriteriaDomain.setInvoiceId(inv.getSpsTInvoiceDomain().getInvoiceId());
            spsTInvoiceCriteriaDomain.setLastUpdateDatetime(
                inv.getSpsTInvoiceDomain().getLastUpdateDatetime());
            int recordUpdate = this.spsTInvoiceService.updateByCondition(spsTInvoiceDomain,
                spsTInvoiceCriteriaDomain);
            
            if(Constants.ONE != recordUpdate){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034, 
                    SupplierPortalConstant.LBL_UPDATE_INVOICE_INFORMATION);
            }
            
            List<SpsTAsnDomain> spsTAsnDomainList = new ArrayList<SpsTAsnDomain>();
            List<SpsTAsnCriteriaDomain> spsTAsnCriteriaDomainList = 
                new ArrayList<SpsTAsnCriteriaDomain>();
            Set<String> updateAsn = new TreeSet<String>();
            for(InvoiceDetailDomain invoiceDetailItem : invoiceDetailList){
                if(updateAsn.add(invoiceDetailItem.getSpsTAsnDomain().getAsnNo().trim())){
                    SpsTAsnDomain spsTAsnDomain = new SpsTAsnDomain();
                    spsTAsnDomain.setCreatedInvoiceFlag(Constants.STR_ZERO);
                    spsTAsnDomain.setLastUpdateDscId(invoiceInformationReturnDomain.getDscId());
                    spsTAsnDomain.setLastUpdateDatetime(sysDate);
                    spsTAsnDomainList.add(spsTAsnDomain);
                    
                    SpsTAsnCriteriaDomain spsTAsnCriteriaDomain = new SpsTAsnCriteriaDomain();
                    spsTAsnCriteriaDomain.setAsnNo(invoiceDetailItem.getSpsTAsnDomain().getAsnNo());
                    spsTAsnCriteriaDomain.setLastUpdateDatetime(
                        invoiceDetailItem.getSpsTAsnDomain().getLastUpdateDatetime());
                    spsTAsnCriteriaDomainList.add(spsTAsnCriteriaDomain);
                }
            }
            
            int resultCountUpdateAsn = this.spsTAsnService.updateByCondition(spsTAsnDomainList,
                spsTAsnCriteriaDomainList);
            
            if(resultCountUpdateAsn != updateAsn.size()){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034, 
                    SupplierPortalConstant.LBL_UPDATE_INVOICE_INFORMATION);
            }
            
            SpsTCnCriteriaDomain spsTCnCriteriaDomain = new SpsTCnCriteriaDomain();
            spsTCnCriteriaDomain.setInvoiceId(inv.getSpsTInvoiceDomain().getInvoiceId());
            List<SpsTCnDomain> spsTCnDomainList
                = this.spsTCnService.searchByCondition(spsTCnCriteriaDomain);
            int resultCountCn = spsTCnDomainList.size();
            
            if(Constants.ZERO != resultCountCn){
                SpsTCnDomain spsTCnDomain = new SpsTCnDomain();
                spsTCnDomain.setCnStatus(Constants.CCL_STATUS);
                spsTCnDomain.setLastUpdateDscId(invoiceInformationReturnDomain.getDscId());
                spsTCnDomain.setLastUpdateDatetime(sysDate);
                
                SpsTCnCriteriaDomain spsTCnCriteriaForUpdate = new SpsTCnCriteriaDomain();
                spsTCnCriteriaForUpdate.setInvoiceId(inv.getSpsTInvoiceDomain().getInvoiceId());
                spsTCnCriteriaForUpdate.setLastUpdateDatetime(
                    spsTCnDomainList.get(Constants.ZERO).getLastUpdateDatetime());
                int resultCountUpdateCn = this.spsTCnService.updateByCondition(
                    spsTCnDomain, spsTCnCriteriaForUpdate);
                
                if(resultCountCn != resultCountUpdateCn){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                        SupplierPortalConstant.LBL_UPDATE_CN_INFORMATION);
                }
            }
        }
        return true;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchSendingEmail(com.globaldenso.asia.sps.business.domain.InvoiceInformationReturnDomain)
     */
    public boolean searchSendingEmail(
        InvoiceInformationReturnDomain invoiceInformationReturnDomain)
        throws ApplicationException
    {
        //Use Invoice data to create Email from MailService
        //1) Get email to send from UserService
        boolean isSuccess = true;
        StringBuffer invNoErrorStr = new StringBuffer();
        Locale locale = invoiceInformationReturnDomain.getLocale();
        List<InvoiceInformationDomain> invoiceInformationList
            = invoiceInformationReturnDomain.getInvoiceInformationDomainList();
        for(InvoiceInformationDomain inv : invoiceInformationList)
        {            
            isSuccess = true;
            InvoiceInformationDomain invoiceInformationDomainForUpdateAsn 
                = new InvoiceInformationDomain();
            invoiceInformationDomainForUpdateAsn.getSpsTInvoiceDomain().setInvoiceId(
                inv.getSpsTInvoiceDomain().getInvoiceId());
            List<InvoiceInformationDomain> invoiceDetailInformation 
                = this.invoiceService.searchInvoiceInformation(
                    invoiceInformationDomainForUpdateAsn);
            
            InvoiceInformationDomain firstInv = invoiceDetailInformation.get(ZERO);
            UserSupplierDetailDomain criUserSupp = new UserSupplierDetailDomain();
            SpsMUserSupplierDomain spsMUserSupp = new SpsMUserSupplierDomain();
            spsMUserSupp.setEmlCancelInvoiceFlag(STR_ONE);
            spsMUserSupp.setSCd(firstInv.getSpsTInvoiceDomain().getSCd());
            criUserSupp.setSpsMUserSupplierDomain(spsMUserSupp);
            
            // Start : [IN016] Send email to supplier by plant
            //List<SpsMUserDomain> userList
            //    = this.userSupplierService.searchEmailUserSupplier(criUserSupp);
            //
            //if (null == userList || Constants.ZERO == userList.size()) {
            //    isSuccess = false;
            //} else {
            //    List<String> toList = new ArrayList<String>(userList.size());
            //    for(SpsMUserDomain user : userList){
            //        if( !toList.contains( user.getEmail() ) ){
            //            toList.add( user.getEmail() );
            //        }
            //    }
            Set<String> sPcdSet = new HashSet<String>();
            for (InvoiceDetailDomain invDetail : firstInv.getInvoiceDetailDomainList()) {
                sPcdSet.add(invDetail.getSpsTAsnDomain().getSPcd());
            }
            
            Set<String> emailSet = new HashSet<String>();
            for (String sPcd : sPcdSet) {
                criUserSupp.getSpsMUserSupplierDomain().setSPcd(sPcd);
                List<SpsMUserDomain> userList
                    = this.userSupplierService.searchEmailUserSupplier(criUserSupp);
                if (null != userList && Constants.ZERO != userList.size()) {
                    for(SpsMUserDomain user : userList){
                        emailSet.add( user.getEmail() );
                    }
                }
            }
            
            if (Constants.ZERO == emailSet.size()) {
                isSuccess = false;
            } else {
                List<String> toList = new ArrayList<String>();
                toList.addAll(emailSet);

            // End : [IN016] Send email to supplier by plant
                
                try {
                    SendEmailDomain mailDomain = this.getEmailCreateInvoiceWithCnContent(
                        invoiceDetailInformation, inv.getInvoiceRcvStatus(),
                        invoiceInformationReturnDomain.getDscId(), locale);
                    
                    if (null == mailDomain) {
                        isSuccess = false;
                    } else {
                        //mailDomain.setHeader( title );
                        mailDomain.setEmailSmtp(ContextParams.getEmailSmtp());
                        //mailDomain.setContents( content );
                        mailDomain.setEmailFrom( ContextParams.getDefaultEmailFrom() );
                        mailDomain.setEmailTo(Constants.EMPTY_STRING);
                        mailDomain.setToList( toList );
                        isSuccess = this.mailService.sendEmail(mailDomain);
                    }
                } catch(Exception ex) {
                    isSuccess = false;
                }
            }
            if(!isSuccess){
                invNoErrorStr.append(inv.getSpsTInvoiceDomain().getInvoiceNo());
                invNoErrorStr.append(Constants.SYMBOL_COMMA);
                invNoErrorStr.append(Constants.SYMBOL_SPACE);
            }
        }
        if(Constants.ZERO < invNoErrorStr.length()){
            String invNoError = invNoErrorStr.toString().trim();
            invoiceInformationReturnDomain.setInvNoErrorResult(
                invNoError.substring(Constants.ZERO, invNoError.length() - Constants.ONE));
            return false;
        }
        return true;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchFileName(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public FileManagementDomain searchFileName(InvoiceInformationDomain invoiceInformationDomain)
        throws ApplicationException {
        
        Locale locale = invoiceInformationDomain.getLocale();
        FileManagementDomain resultDomain = null;
        
        try {
            resultDomain = this.fileManagementService.searchFileDownload(
                invoiceInformationDomain.getFileId(), false, null);
            
        } catch (IOException ie) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchFileDownload(
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain,
     * OutputStream)
     */
    public void searchInvoiceCoverPage(InvoiceInformationDomain invoiceInformationDomain,
        OutputStream output) 
        throws ApplicationException {
        
        Locale locale = invoiceInformationDomain.getLocale();
        
        try {
            this.fileManagementService.searchFileDownload(invoiceInformationDomain.getFileId(),
                true, output);
            
        }catch (IOException ie) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchSelectedDensoCompany(
     * com.globaldenso.asia.sps.business.domain.DataScopeControlDomain,
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException{
        
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<PlantDensoDomain> companyDensoPlantList = null;
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            plantDensoWithScopeDomain.getDataScopeControlDomain());
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companyDensoPlantList = this.plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(Constants.ZERO == companyDensoPlantList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        return companyDensoPlantList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchSelectedPlantDenso(
     * com.globaldenso.asia.sps.business.domain.DataScopeControlDomain,
     * com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException{
        
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = null;
        List<CompanyDensoDomain> companyDensoList = null;
        
        densoSupplierRelationList = this.densoSupplierRelationService.searchDensoSupplierRelation(
            plantDensoWithScopeDomain.getDataScopeControlDomain());
        if(Constants.ZERO == densoSupplierRelationList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            densoSupplierRelationList);
        companyDensoList = this.companyDensoService.searchCompanyDenso(plantDensoWithScopeDomain);
        if(Constants.ZERO == companyDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        return companyDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchLegendInfo
     * (com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain,
     * OutputStream)
     */
    public void searchLegendInfo(InvoiceInformationDomain invoiceInformationDomain,
        OutputStream output) throws ApplicationException {
        
        Locale locale = invoiceInformationDomain.getLocale();
        
        try {
            fileManagementService.searchFileDownload(invoiceInformationDomain.getFileId(), true, 
                output);
            
        } catch (IOException e) {
            
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        //[IN073]
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        roleTypeCriteria.setIsActive("1");
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            if (null == role || null == role.getRoleName() ) {
                continue;
            }
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    
    /**
     * <p>Validate.</p>
     * <ul>
     * <li>Validation criteria data before search data.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the Invoice Information Domain 
     * @return list of the Application Message Domain 
     */
    private List<ApplicationMessageDomain> validate(
        InvoiceInformationDomain invoiceInformationDomain)
    {
        String invoiceDateFrom = null;
        String invoiceDateTo = null;
        String cnDateFrom = null;
        String cnDateTo = null;
        boolean hasInvoiceDateFrom = false;
        boolean hasInvoiceDateTo = false;
        boolean hasCnDateFrom = false;
        boolean hasCnDateTo = false;
        
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = invoiceInformationDomain.getLocale();
        
        if(Strings.judgeBlank(invoiceInformationDomain.getInvoiceDateFrom())
            || Strings.judgeBlank(invoiceInformationDomain.getInvoiceDateTo())
            || Strings.judgeBlank(invoiceInformationDomain.getVendorCd())
            || Strings.judgeBlank(invoiceInformationDomain.getDCd())
            || Strings.judgeBlank(invoiceInformationDomain.getDPcd())){
            
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        }
        
        if(Constants.MISC_CODE_ALL.equals(invoiceInformationDomain.getVendorCd())){
            invoiceInformationDomain.setVendorCd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(invoiceInformationDomain.getDCd())){
            invoiceInformationDomain.setDCd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(invoiceInformationDomain.getDPcd())){
            invoiceInformationDomain.setDPcd(null);
        }
        if(Constants.MISC_CODE_ALL.equals(invoiceInformationDomain.getSpsTInvoiceDomain()
            .getInvoiceStatus())){
            invoiceInformationDomain.getSpsTInvoiceDomain().setInvoiceStatus(null);
        }
        
        if(!Strings.judgeBlank(invoiceInformationDomain.getInvoiceDateFrom())){
            invoiceDateFrom = invoiceInformationDomain.getInvoiceDateFrom();
            if(!DateUtil.isValidDate(invoiceDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_INVOICE_DATE_FROM)));
            }else{
                hasInvoiceDateFrom = true;
            }
        }
        if(!Strings.judgeBlank(invoiceInformationDomain.getInvoiceDateTo())){
            invoiceDateTo = invoiceInformationDomain.getInvoiceDateTo();
            if(!DateUtil.isValidDate(invoiceDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_INVOICE_DATE_TO)));
            }else{
                hasInvoiceDateTo = true;
            }
        }
        if(hasInvoiceDateFrom && hasInvoiceDateTo){
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            
            dateFrom.setTime(DateUtil.parseToSqlDate
                (invoiceDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH));
            dateTo.setTime(DateUtil.parseToSqlDate
                (invoiceDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH));
            if(dateTo.before(dateFrom)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_INVOICE_DATE_TO),
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_INVOICE_DATE_FROM)})));
            }
        }
        
        if(!Strings.judgeBlank(invoiceInformationDomain.getCnDateFrom())){
            cnDateFrom = invoiceInformationDomain.getCnDateFrom();
            if(!DateUtil.isValidDate(cnDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_CN_DATE_FROM)));
            }else{
                hasCnDateFrom = true;
            }
        }
        if(!Strings.judgeBlank(invoiceInformationDomain.getCnDateTo())){
            cnDateTo = invoiceInformationDomain.getCnDateTo();
            if(!DateUtil.isValidDate(cnDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_CN_DATE_TO)));
            }else{
                hasCnDateTo = true;
            }
        }
        if(hasCnDateFrom && hasCnDateTo){
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            
            dateFrom.setTime(DateUtil.parseToSqlDate(cnDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH));
            dateTo.setTime(DateUtil.parseToSqlDate(cnDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH));
            
            if(dateTo.before(dateFrom)){
                
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031, 
                        new String[]{MessageUtil.getLabelHandledException(locale, 
                            SupplierPortalConstant.LBL_CN_DATE_TO),
                            MessageUtil.getLabelHandledException(locale, 
                                SupplierPortalConstant.LBL_CN_DATE_FROM)})));
            }
        }
        
        if(!Strings.judgeBlank(invoiceInformationDomain.getSpsTInvoiceDomain().getInvoiceNo())
            && Constants.MAX_INVOICE_LENGTH 
                < invoiceInformationDomain.getSpsTInvoiceDomain().getInvoiceNo().length()){
                
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                    new String[]{MessageUtil.getLabelHandledException(locale, 
                        SupplierPortalConstant.LBL_INVOICE_NO),
                        String.valueOf(Constants.MAX_INVOICE_LENGTH)})));
        }
        
        if(!Strings.judgeBlank(invoiceInformationDomain.getSpsTCnDomain().getCnNo())
            && Constants.MAX_CN_NO_LENGTH 
                < invoiceInformationDomain.getSpsTCnDomain().getCnNo().length()){
            
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, 
                    new String[]{MessageUtil.getLabelHandledException(locale, 
                        SupplierPortalConstant.LBL_CN_NO),
                        String.valueOf(Constants.MAX_CN_NO_LENGTH)})));
        }
        
        return errorMessageList;
    }

    /**
     * Create email detail when register Invoice with CN or Temp Price.
     * @param invoiceInformationList List of invoice information
     * @param invoiceRcvStatus Invoice Receive Status
     * @param dscId current user login
     * @param locale for get properties
     * @return SendEmailDomain
     * @throws Exception Exception
     * */
    private SendEmailDomain getEmailCreateInvoiceWithCnContent(
        List<InvoiceInformationDomain> invoiceInformationList, String invoiceRcvStatus,
        String dscId, Locale locale)
        throws Exception
    {
        SendEmailDomain result = new SendEmailDomain();
        InvoiceInformationDomain firstInv = invoiceInformationList.get(ZERO);
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        int scale = 0;
        if(!StringUtil.checkNullOrEmpty(firstInv.getDecimalDisp())){
            scale = Integer.valueOf(firstInv.getDecimalDisp());
        }
        decimalFormat.setMinimumFractionDigits(scale);
        decimalFormat.setMaximumFractionDigits(scale);
        
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat defaultDecimalFormat = (DecimalFormat)defaultNumberFormat;
        defaultDecimalFormat.setGroupingUsed(true);
        defaultDecimalFormat.setMinimumFractionDigits(Constants.TWO);
        defaultDecimalFormat.setMaximumFractionDigits(Constants.TWO);

        NumberFormat priceNumberFormat = NumberFormat.getInstance();
        DecimalFormat priceDecimalFormat = (DecimalFormat)priceNumberFormat;
        priceDecimalFormat.setGroupingUsed(true);
        priceDecimalFormat.setMinimumFractionDigits(Constants.FOUR);
        priceDecimalFormat.setMaximumFractionDigits(Constants.FOUR);

        SpsMCompanyDensoCriteriaDomain densoCriteria = new SpsMCompanyDensoCriteriaDomain();
        densoCriteria.setDCd(firstInv.getSpsTInvoiceDomain().getDCd());
        SpsMCompanyDensoDomain denso = this.spsMCompanyDensoService.searchByKey(densoCriteria);
        
        SpsMUserCriteriaDomain userCriteria = new SpsMUserCriteriaDomain();
        userCriteria.setDscId(dscId);
        SpsMUserDomain user = this.spsMUserService.searchByKey(userCriteria);
        String firstName = Constants.EMPTY_STRING;
        if (null != user.getFirstName()) {
            firstName = user.getFirstName();
        }
        String middleName = Constants.EMPTY_STRING;
        if (null != user.getMiddleName()) {
            middleName = user.getMiddleName();
        }
        String lastName = Constants.EMPTY_STRING;
        if (null != user.getLastName()) {
            lastName = user.getLastName();
        }
        
        
        String subject = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_SUBJECT_INVOICE_CANCEL);
        String headerText1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE1);
        String headerText2 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE2_CANCEL);
        String headerText3 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE3);
        String headerText4 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE4);
        String headerText5 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE5);
        String headerTable1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TABLE_LINE1);
        String headerTable2 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TABLE_LINE2);
        String detailTable1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_DETAIL_TABLE_LINE1);
        String detailTable3 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_DETAIL_TABLE_LINE3);
        String footer1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_FOOTER_LINE1);
        String footer2 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_FOOTER_LINE2);
        
        //#invoiceNo#
        subject = subject.replace(REPLACE_INVOICE_NO
            , firstInv.getSpsTInvoiceDomain().getInvoiceNo());
        result.setHeader(subject);
        
        StringBuffer detail = new StringBuffer();
        detail.append(headerText1);
        detail.append(headerText2);
        
        SpsTInvoiceDomain invoice = null;
        SpsTInvoiceDetailDomain invoiceDetail = null;
        for (InvoiceInformationDomain invoiceInfo : invoiceInformationList) {
            invoice = invoiceInfo.getSpsTInvoiceDomain();
            
            // #coverPageNo# #supplierCode# #densoCode# #densoPlantCode#
            headerText3 = headerText3.replace(REPLACE_COVER_PAGE_NO, invoice.getCoverPageNo())
                .replace(REPLACE_SCD, invoice.getVendorCd())
                .replace(REPLACE_DCD, invoice.getDCd())
                .replace(REPLACE_DPCD, invoice.getDPcd());
            detail.append(headerText3);
            
            // #invoiceNo# #invoiceDate# #totalInvoiceAmount# 
            headerText4 = headerText4.replace(REPLACE_INVOICE_NO, invoice.getInvoiceNo())
                .replace(REPLACE_INVOICE_DATE, DateUtil.format(
                    invoice.getInvoiceDate(), DateUtil.PATTERN_YYYYMMDD_SLASH))
                    
                // [IN009] Rounding Mode use HALF_UP
                //.replace(REPLACE_TOTAL_INVOICE_AMOUNT, decimalFormat.format(
                //    invoice.getSTotalAmount()));
                .replace(REPLACE_TOTAL_INVOICE_AMOUNT, NumberUtil.formatRoundingHalfUp(
                    decimalFormat, invoice.getSTotalAmount(), scale));
                
            detail.append(headerText4);
            
            String cnNo = Constants.EMPTY_STRING;
            String cnDate = Constants.EMPTY_STRING;
            String cnAmount = Constants.EMPTY_STRING;
            if (null != invoiceInfo.getSpsTCnDomain()) {
                if (null != invoiceInfo.getSpsTCnDomain().getCnNo()) {
                    cnNo = invoiceInfo.getSpsTCnDomain().getCnNo();
                }
                if (null != invoiceInfo.getSpsTCnDomain().getCnDate()) {
                    cnDate = DateUtil.format(invoiceInfo.getSpsTCnDomain().getCnDate()
                        , DateUtil.PATTERN_YYYYMMDD_SLASH);
                }
                if (null != invoiceInfo.getSpsTCnDomain().getTotalAmount()) {
                    // [IN009] Rounding Mode use HALF_UP
                    //cnAmount = decimalFormat.format(
                    //    invoiceInfo.getSpsTCnDomain().getTotalAmount());
                    cnAmount = NumberUtil.formatRoundingHalfUp(
                        decimalFormat, invoiceInfo.getSpsTCnDomain().getTotalAmount(), scale);
                }
            }
            
            // #cnNo# #cnDate# #totalCnAmount#
            headerText5 = headerText5.replace(REPLACE_CN_NO, cnNo)
                .replace(REPLACE_CN_DATE, cnDate)
                .replace(REPLACE_TOTAL_CN_AMOUNT, cnAmount);
            detail.append(headerText5);
            
            detail.append(headerTable1);
            detail.append(headerTable2);
            

            for(InvoiceDetailDomain invoiceInfoDetail : invoiceInfo.getInvoiceDetailDomainList()){
                invoiceDetail = invoiceInfoDetail.getSpsTInvoiceDetailDomain();
                
                String rowColor = Constants.COLOR_WHITE;
                
                Timestamp planEta = invoiceInfoDetail.getSpsTAsnDomain().getPlanEta();
                String etaDateTime = DateUtil.format(planEta, DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
                
                String diffBaseAmount = Constants.STR_ZERO;
                if (null != invoiceDetail.getDiffBaseAmt()) {
                    // [IN009] Rounding Mode use HALF_UP
                    //diffBaseAmount = decimalFormat.format(invoiceDetail.getDiffBaseAmt());
                    diffBaseAmount = NumberUtil.formatRoundingHalfUp(
                        decimalFormat, invoiceDetail.getDiffBaseAmt(), scale);
                }
                
                /* #rowColor# #firstName# #middleName# #lastName# #asnNo# #goodRcvStatus# #planEta#
                 * #dPartNo# #dUnitPrice# #sUnitPrice# #tempPrice# #shippingQty# #dUnitOfMeasure#
                 * #diffBaseAmtByPart#
                 * */
                detail.append(detailTable1.replace(REPLACE_ROW_COLOR, rowColor)
                    .replace(REPLACE_FIRST_NAME, firstName)
                    .replace(REPLACE_MIDDLE_NAME, middleName)
                    .replace(REPLACE_LAST_NAME, lastName)
                    .replace(REPLACE_ASNNO, invoiceDetail.getAsnNo())
                    .replace(REPLACE_GOOD_RCV_STATUS, invoiceRcvStatus)
                    .replace(REPLACE_PLAN_ETA, StringUtil.appendsString(
                        etaDateTime
                        , Constants.SYMBOL_SPACE
                        , Constants.SYMBOL_OPEN_BRACKET
                        , Constants.WORD_UTC, denso.getUtc().trim()
                        , Constants.SYMBOL_CLOSE_BRACKET))
                    .replace(REPLACE_DPARTNO, invoiceDetail.getDPn())
                    .replace(REPLACE_TEMP_PRICE, invoiceDetail.getTmpPriceFlg())
                    
                    // [IN009] Rounding Mode use HALF_UP
                    //.replace(REPLACE_D_UNIT_PRICE, 
                    //    decimalFormat.format(invoiceDetail.getDUnitPrice()))
                    //.replace(REPLACE_S_UNIT_PRICE, 
                    //    decimalFormat.format(invoiceDetail.getSUnitPrice()))
                    //.replace(REPLACE_SHIPPING_QTY, 
                    //    decimalFormat.format(invoiceDetail.getShippingQty()))
                    .replace(REPLACE_D_UNIT_PRICE, NumberUtil.formatRoundingHalfUp(
                        priceDecimalFormat, invoiceDetail.getDUnitPrice(), Constants.FOUR))
                    .replace(REPLACE_S_UNIT_PRICE, NumberUtil.formatRoundingHalfUp(
                        priceDecimalFormat, invoiceDetail.getSUnitPrice(), Constants.FOUR))
                    .replace(REPLACE_SHIPPING_QTY, NumberUtil.formatRoundingHalfUp(
                        defaultDecimalFormat, invoiceDetail.getShippingQty(), Constants.TWO))
                    
                    .replace(REPLACE_D_UNIT_OF_MEASURE, invoiceDetail.getDUnitOfMeasure())
                    .replace(REPLACE_DIFF_BASE_AMT_BY_PART, diffBaseAmount));
            }
            detail.append(detailTable3);

            detail.append(Constants.SYMBOL_NEWLINE);
        }
        
        detail.append(footer1);
        detail.append(footer2);
        
        result.setContents(detail.toString());
        
        return result;
    }
    
    /**
     * <p>Get back order information map.</p>
     * <ul>
     * <li>Preparation data for create CSV file.</li>
     * </ul>
     * 
     * @param invoiceInformationList the List of Invoice Information Domain
     * @param header the array of string
     * @return List<Map<String, Object>>
     */
    private List<Map<String, Object>> getInvoiceInformationMap(
        List<InvoiceInformationDomain> invoiceInformationList, String[] header)
    {
        
        List<Map<String, Object>> invoiceInformationMapList 
            = new ArrayList<Map<String, Object>>();
        Map<String, Object>  invoiceInformationMap = null;
        
        String asnDate = null;
        String cnDate = null;
        String planEta = null;
        String actualEtd = null;
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        
        for(InvoiceInformationDomain recordHead : invoiceInformationList)
        {
            int scale = 0;
            if(!StringUtil.checkNullOrEmpty(recordHead.getDecimalDisp())){
                scale = Integer.valueOf(recordHead.getDecimalDisp());
            }
            decimalFormat.setMinimumFractionDigits(scale);
            decimalFormat.setMaximumFractionDigits(scale);

            NumberFormat defaultNumberFormat = NumberFormat.getInstance();
            DecimalFormat defaultDecimalFormat = (DecimalFormat)defaultNumberFormat;
            defaultDecimalFormat.setGroupingUsed(true);
            defaultDecimalFormat.setMinimumFractionDigits(Constants.TWO);
            defaultDecimalFormat.setMaximumFractionDigits(Constants.TWO);

            NumberFormat priceNumberFormat = NumberFormat.getInstance();
            DecimalFormat priceDecimalFormat = (DecimalFormat)priceNumberFormat;
            priceDecimalFormat.setGroupingUsed(true);
            priceDecimalFormat.setMinimumFractionDigits(Constants.FOUR);
            priceDecimalFormat.setMaximumFractionDigits(Constants.FOUR);

            for(InvoiceDetailDomain recordDetail : recordHead.getInvoiceDetailDomainList())
            {
                invoiceInformationMap = new HashMap<String, Object>();
                asnDate = Constants.EMPTY_STRING;
                cnDate = Constants.EMPTY_STRING;
                planEta = Constants.EMPTY_STRING;
                actualEtd = Constants.EMPTY_STRING;
                
                SpsTInvoiceDomain invoiceDomain = recordHead.getSpsTInvoiceDomain();
                SpsTCnDomain cnDomain = recordHead.getSpsTCnDomain();
                SpsTAsnDomain asnDomain = recordDetail.getSpsTAsnDomain();
                SpsTInvoiceDetailDomain invoiceDetailDomain
                    = recordDetail.getSpsTInvoiceDetailDomain();
                
                invoiceInformationMap.put(header[Constants.ZERO], 
                    invoiceDomain.getInvoiceStatus());
                invoiceInformationMap.put(header[Constants.ONE], invoiceDomain.getInvoiceNo());
                invoiceInformationMap.put(header[Constants.TWO], recordHead.getInvoiceDate());
                if(null != cnDomain && !StringUtil.checkNullOrEmpty(cnDomain.getCnNo())){
                    cnDate = DateUtil.format(cnDomain.getCnDate(), DateUtil.PATTERN_YYYYMMDD_SLASH);
                    invoiceInformationMap.put(header[Constants.THREE], 
                        StringUtil.nullToEmpty(cnDomain.getCnNo()));
                    invoiceInformationMap.put(header[Constants.FOUR], cnDate);
                    
                    // [IN009] Rounding Mode use HALF_UP
                    //invoiceInformationMap.put(header[Constants.TWENTY],
                    //    this.convertAmountToDecimalFormat(decimalFormat, cnDomain.getBaseAmount()));
                    //invoiceInformationMap.put(header[Constants.TWENTY_ONE],
                    //    this.convertAmountToDecimalFormat(decimalFormat, cnDomain.getVatAmount()));
                    //invoiceInformationMap.put(header[Constants.TWENTY_TWO], 
                    //    this.convertAmountToDecimalFormat(
                    //        decimalFormat, cnDomain.getTotalAmount()));
                    invoiceInformationMap.put(header[Constants.TWENTY],
                        this.convertAmountToDecimalFormat(
                            decimalFormat, cnDomain.getBaseAmount(), scale));
                    invoiceInformationMap.put(header[Constants.TWENTY_ONE],
                        this.convertAmountToDecimalFormat(
                            decimalFormat, cnDomain.getVatAmount(), scale));
                    invoiceInformationMap.put(header[Constants.TWENTY_TWO], 
                        this.convertAmountToDecimalFormat(
                            decimalFormat, cnDomain.getTotalAmount(), scale));
                    
                }else{
                    invoiceInformationMap.put(header[Constants.THREE], Constants.EMPTY_STRING);
                    invoiceInformationMap.put(header[Constants.FOUR], Constants.EMPTY_STRING);
                    invoiceInformationMap.put(header[Constants.TWENTY], Constants.EMPTY_STRING);
                    invoiceInformationMap.put(header[Constants.TWENTY_ONE], Constants.EMPTY_STRING);
                    invoiceInformationMap.put(header[Constants.TWENTY_TWO], Constants.EMPTY_STRING);
                }
                
                invoiceInformationMap.put(header[Constants.FIVE], StringUtil.checkNullToEmpty(
                    invoiceDomain.getCoverPageNo()));
                invoiceInformationMap.put(header[Constants.SIX],
                    recordHead.getInvoiceRcvStatus());
                invoiceInformationMap.put(header[Constants.SEVEN],
                    StringUtil.checkNullToEmpty(recordDetail.getReceivedDateStr()));
                invoiceInformationMap.put(header[Constants.EIGHT], 
                    StringUtil.checkNullToEmpty(invoiceDomain.getSCurrencyCd()));
                invoiceInformationMap.put(header[Constants.NINE], invoiceDomain.getVatType());
                invoiceInformationMap.put(header[Constants.TEN], invoiceDomain.getVatRate());
                invoiceInformationMap.put(header[Constants.ELEVEN], invoiceDomain.getDTaxId());
                invoiceInformationMap.put(header[Constants.TWELVE], invoiceDomain.getSTaxId());
                invoiceInformationMap.put(header[Constants.THIRTEEN], 
                    StringUtil.checkNullToEmpty(invoiceDomain.getVendorCd()));
                invoiceInformationMap.put(header[Constants.FOURTEEN], 
                    StringUtil.checkNullToEmpty(asnDomain.getSPcd()));
                invoiceInformationMap.put(header[Constants.FIFTEEN],
                    StringUtil.checkNullToEmpty(invoiceDomain.getDCd()));
                invoiceInformationMap.put(header[Constants.SIXTEEN], 
                    StringUtil.checkNullToEmpty(invoiceDomain.getDPcd()));
                
                // [IN009] Rounding Mode use HALF_UP
                //invoiceInformationMap.put(header[Constants.SEVENTEEN],
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getSBaseAmount()));
                //invoiceInformationMap.put(header[Constants.EIGHTEEN],
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getSVatAmount()));
                //invoiceInformationMap.put(header[Constants.NINETEEN], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getSTotalAmount()));
                //invoiceInformationMap.put(header[Constants.TWENTY_THREE], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getTotalBaseAmount()));
                //invoiceInformationMap.put(header[Constants.TWENTY_FOUR], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getTotalVatAmount()));
                //invoiceInformationMap.put(header[Constants.TWENTY_FIVE], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getTotalAmount()));
                //invoiceInformationMap.put(header[Constants.TWENTY_SIX], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getTotalDiffBaseAmount()));
                //invoiceInformationMap.put(header[Constants.TWENTY_SEVEN], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getTotalDiffVatAmount()));
                //invoiceInformationMap.put(header[Constants.TWENTY_EIGHT], 
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDomain.getTotalDiffAmount()));
                invoiceInformationMap.put(header[Constants.SEVENTEEN],
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getSBaseAmount(), scale));
                invoiceInformationMap.put(header[Constants.EIGHTEEN],
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getSVatAmount(), scale));
                invoiceInformationMap.put(header[Constants.NINETEEN], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getSTotalAmount(), scale));
                invoiceInformationMap.put(header[Constants.TWENTY_THREE], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getTotalBaseAmount(), scale));
                invoiceInformationMap.put(header[Constants.TWENTY_FOUR], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getTotalVatAmount(), scale));
                invoiceInformationMap.put(header[Constants.TWENTY_FIVE], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getTotalAmount(), scale));
                invoiceInformationMap.put(header[Constants.TWENTY_SIX], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getTotalDiffBaseAmount(), scale));
                invoiceInformationMap.put(header[Constants.TWENTY_SEVEN], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getTotalDiffVatAmount(), scale));
                invoiceInformationMap.put(header[Constants.TWENTY_EIGHT], 
                    this.convertAmountToDecimalFormat(
                        decimalFormat, invoiceDomain.getTotalDiffAmount(), scale));
                
                invoiceInformationMap.put(header[Constants.TWENTY_NINE], 
                    StringUtil.nullToEmpty(recordHead.getPaymentDate()));
                
                if(null != asnDomain.getCreateDatetime()){
                    asnDate = DateUtil.format(new Date(asnDomain.getCreateDatetime().getTime()),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                }
                
                if(null != asnDomain.getPlanEta()){
                    planEta = DateUtil.format(new Date(asnDomain.getPlanEta().getTime()),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                }
                
                if(null != asnDomain.getActualEtd()){
                    actualEtd = DateUtil.format(new Date(asnDomain.getActualEtd().getTime()),
                        DateUtil.PATTERN_YYYYMMDD_SLASH);
                }
                
                invoiceInformationMap.put(header[Constants.THIRTY], asnDate);
                invoiceInformationMap.put(header[Constants.THIRTY_ONE], asnDomain.getAsnNo());
                invoiceInformationMap.put(header[Constants.THIRTY_TWO],
                    recordDetail.getSpsTAsnDomain().getAsnStatus());
                invoiceInformationMap.put(header[Constants.THIRTY_THREE],
                    invoiceDetailDomain.getTripNo());
                invoiceInformationMap.put(header[Constants.THIRTY_FOUR], planEta);
                invoiceInformationMap.put(header[Constants.THIRTY_FIVE], actualEtd);
                invoiceInformationMap.put(header[Constants.THIRTY_SIX],
                    invoiceDetailDomain.getDPn());
                invoiceInformationMap.put(header[Constants.THIRTY_SEVEN],
                    invoiceDetailDomain.getDUnitOfMeasure());
                
                // [IN009] Rounding Mode use HALF_UP
                //invoiceInformationMap.put(header[Constants.THIRTY_EIGHT],
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDetailDomain.getDUnitPrice()));
                BigDecimal dUnitPrice = invoiceDetailDomain.getDUnitPrice().setScale(
                    Constants.FOUR, RoundingMode.HALF_UP);
                //invoiceInformationMap.put(header[Constants.THIRTY_EIGHT],
                //    priceDecimalFormat.format(dUnitPrice));
                // [IN020] Download CSV incorrect
                invoiceInformationMap.put(header[Constants.THIRTY_EIGHT]
                    , this.convertAmountToDecimalFormat(priceDecimalFormat
                        , invoiceDetailDomain.getDUnitPrice(),  Constants.FOUR));
                
                invoiceInformationMap.put(header[Constants.THIRTY_NINE],
                    StringUtil.checkNullToEmpty(invoiceDetailDomain.getTmpPriceFlg()));
                invoiceInformationMap.put(header[Constants.FORTY],
                    invoiceDetailDomain.getSPn());
                
                // [IN009] Rounding Mode use HALF_UP
                //invoiceInformationMap.put(header[Constants.FORTY_ONE],
                //    invoiceDetailDomain.getShippingQty());
                BigDecimal shippingQty = invoiceDetailDomain.getShippingQty().setScale(
                    Constants.TWO, RoundingMode.HALF_UP);
                // [IN020] Download CSV incorrect
                //invoiceInformationMap.put(header[Constants.FORTY_ONE],
                //    defaultNumberFormat.format(shippingQty));
                invoiceInformationMap.put(header[Constants.FORTY_ONE]
                    , this.convertAmountToDecimalFormat(defaultDecimalFormat
                        , invoiceDetailDomain.getShippingQty(), Constants.TWO));
                
                // [IN009] Rounding Mode use HALF_UP
                //invoiceInformationMap.put(header[Constants.FORTY_TWO],
                //    this.convertAmountToDecimalFormat(
                //        decimalFormat, invoiceDetailDomain.getSUnitPrice()));
                BigDecimal sUnitPrice = invoiceDetailDomain.getSUnitPrice().setScale(
                    Constants.FOUR, RoundingMode.HALF_UP);
                // [IN020] Download CSV incorrect
                //invoiceInformationMap.put(header[Constants.FORTY_TWO],
                //    priceDecimalFormat.format(sUnitPrice));
                invoiceInformationMap.put(header[Constants.FORTY_TWO]
                    , this.convertAmountToDecimalFormat(priceDecimalFormat
                        , invoiceDetailDomain.getSUnitPrice(), Constants.FOUR));
                
                BigDecimal sBaseAmountByPN = sUnitPrice.multiply(shippingQty);
                BigDecimal dBaseAmountByPN = dUnitPrice.multiply(shippingQty);

                // [IN009] Rounding Mode use HALF_UP
                //invoiceInformationMap.put(header[Constants.FORTY_THREE],
                //    this.convertAmountToDecimalFormat(decimalFormat, sBaseAmountByPN));
                //invoiceInformationMap.put(header[Constants.FORTY_FOUR],
                //    this.convertAmountToDecimalFormat(decimalFormat, dBaseAmountByPN));
                invoiceInformationMap.put(header[Constants.FORTY_THREE],
                    this.convertAmountToDecimalFormat(decimalFormat, sBaseAmountByPN, scale));
                invoiceInformationMap.put(header[Constants.FORTY_FOUR],
                    this.convertAmountToDecimalFormat(decimalFormat, dBaseAmountByPN, scale));
                
                invoiceInformationMapList.add(invoiceInformationMap);
            }
        }
        return invoiceInformationMapList;
    }
    
    /**
     * Add double quote at front and back of target string
     * @param target to add double quote
     * @return string that already add double quote
     * */
    private String addDoubleQuote(String target) {
        StringBuffer result = new StringBuffer();
        
        result.append(Constants.SYMBOL_DOUBLE_QUOTE).append(target)
            .append(Constants.SYMBOL_DOUBLE_QUOTE);
        
        return result.toString();
    }
    
    // [IN009] Rounding Mode use HALF_UP
    ///**
    // * Convert Amount data to Decimal Format
    // * @param decimalFormat decimal format by currency
    // * @param target to convert decimal format
    // * @return string decimal format ("1000.00") return ("1,000.00")
    // * */
    //private String convertAmountToDecimalFormat(DecimalFormat decimalFormat, BigDecimal target){
    //    String result = Constants.EMPTY_STRING;
    //    if(null != target){
    //        result = this.addDoubleQuote(decimalFormat.format(target));
    //    }
    //    return result;
    //}
    /**
     * Convert Amount data to Decimal Format
     * @param decimalFormat decimal format by currency
     * @param target to convert decimal format
     * @param scale for decigmal point
     * @return string decimal format ("1000.00") return ("1,000.00")
     * */
    private String convertAmountToDecimalFormat(DecimalFormat decimalFormat, BigDecimal target
        , int scale)
    {
        String result = Constants.EMPTY_STRING;
        if(null != target){
            result = this.addDoubleQuote(decimalFormat.format(
                target.setScale(scale, RoundingMode.HALF_UP)));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.searchGenerateInvoice#searchGenerateDo(com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public FileManagementDomain searchGenerateInvoice(
        InvoiceInformationDomain invoiceInformationDomain) throws ApplicationException
    {
        SpsTInvoiceCriteriaDomain criteria = new SpsTInvoiceCriteriaDomain();
        criteria.setInvoiceId(new BigDecimal(invoiceInformationDomain.getInvoiceIdSelected()));
        
        List<SpsTInvoiceDomain> spsTInvoiceDomainList = spsTInvoiceService.searchByCondition(criteria);
        SpsTInvoiceDomain spsTInvoiceDomain = spsTInvoiceDomainList.get(Constants.ZERO);
        
        invoiceInformationDomain.getSpsTInvoiceDomain().setInvoiceId(criteria.getInvoiceId());
        List<InvoiceInformationDomain> invoiceInformationDomainList = invoiceService.searchInvoiceInformation(invoiceInformationDomain);
        InvoiceInformationDomain decimalDisplay = invoiceInformationDomainList.get(Constants.ZERO);
//      [IN1898]    Invoice cover page shows wrong in case invoice number are duplicated.  
        String invoiceId =  invoiceInformationDomain.getInvoiceIdSelected();
//      [IN1898]    Invoice cover page shows wrong in case invoice number are duplicated. 
        String invoiceNo = spsTInvoiceDomain.getInvoiceNo();
        String dCd = spsTInvoiceDomain.getDCd();
        String dPcd = spsTInvoiceDomain.getDPcd();
        String vendorCd = spsTInvoiceDomain.getVendorCd();
        String decimalDisp = decimalDisplay.getDecimalDisp();
        Locale locale = invoiceInformationDomain.getLocale();
        
        InvoiceMaintenanceDomain invoiceMaintenanceDomain = new InvoiceMaintenanceDomain();
//      [IN1898]    Invoice cover page shows wrong in case invoice number are duplicated. 
        invoiceMaintenanceDomain.setInvoiceId(invoiceId);
//      [IN1898]    Invoice cover page shows wrong in case invoice number are duplicated. 
        invoiceMaintenanceDomain.setInvoiceNo(invoiceNo);
        invoiceMaintenanceDomain.setDCd(dCd);
        invoiceMaintenanceDomain.setDPcd(dPcd);
        invoiceMaintenanceDomain.setVendorCd(vendorCd);
        invoiceMaintenanceDomain.setLocale(locale);
        invoiceMaintenanceDomain.setDecimalDisp(decimalDisp);
        return this.invoiceMaintenanceFacadeService.searchGenerateInvoice(invoiceMaintenanceDomain);
////      2018-04-18 Add generate PDF to BLOB
//        List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList = invoiceMaintenanceFacadeService.createInvoiceCoverPageData(
//            invoiceId,
//            invoiceCoverPageNo,
//            invoiceInformationDomain.getDecimalDisp(),
//            locale);
//        InputStream tsInvoiceCoverPageReport = null;
//
//        try {
//            tsInvoiceCoverPageReport = this.createInvoiceCoverPageReport(invoiceCoverPageReportList);
//        } catch (Exception e) {
//            MessageUtil.throwsApplicationMessageHandledException(locale,
//                SupplierPortalConstant.ERROR_CD_SP_80_0015);
//        }
//        String fileName = StringUtil.appendsString(
//            INVOICE_COVER_PAGE, invoiceCoverPageNo.toString(),
//            Constants.SYMBOL_DOT, Constants.REPORT_PDF_FORMAT);
//        FileManagementDomain resultDomain = new FileManagementDomain();
//        resultDomain = new FileManagementDomain();
//        resultDomain.setFileData(tsInvoiceCoverPageReport);
//        resultDomain.setFileName(fileName);
    }
}
