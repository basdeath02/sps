/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.dao.PlantDensoDao;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * <p>The Interface PlantDensoService.</p>
 * <p>Service for Plant DENSO about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchDensoPlant</li>
 * </ul>
 *
 * @author CSI
 */
public class PlantDensoServiceImpl implements PlantDensoService {

    /** The Plant DENSO Dao. */
    private PlantDensoDao plantDensoDao;
    
    /** The default constructor. */
    public PlantDensoServiceImpl() {
        super();
    }

    /**
     * Set method for plantDensoDao.
     * @param plantDensoDao the plantDensoDao to set
     */
    public void setPlantDensoDao(PlantDensoDao plantDensoDao) {
        this.plantDensoDao = plantDensoDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantDensoService#searchPlantDenso
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchPlantDenso(
            PlantDensoWithScopeDomain plantDensoWithScopeDomain)
    {
        List<PlantDensoDomain> plantDensoList = null;
        plantDensoWithScopeDomain.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(
            plantDensoWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            plantDensoList = this.plantDensoDao.searchPlantDensoByRole(plantDensoWithScopeDomain);
        } else {
            plantDensoList = this.plantDensoDao.searchPlantDensoByRelation(
                plantDensoWithScopeDomain);
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantDensoService#searchPlantDensoInformation
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchPlantDensoInformation(
            PlantDensoWithScopeDomain plantDensoWithScopeDomain)
    {
        List<PlantDensoDomain> plantDensoList = null;
        plantDensoWithScopeDomain.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(
            plantDensoWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            plantDensoList = this.plantDensoDao.searchPlantDensoInformationByRole(
                plantDensoWithScopeDomain);
        } else {
            plantDensoList = this.plantDensoDao.searchPlantDensoInformationByRelation(
                plantDensoWithScopeDomain);
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantSupplierService#searchExistPlantDenso
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public Integer searchExistPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        Integer recordCount = 0;
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(
            plantDensoWithScopeDomain.getDataScopeControlDomain().getUserType())){
            recordCount = this.plantDensoDao.searchExistPlantDensoByRole(
                plantDensoWithScopeDomain);
        }else{
            recordCount = this.plantDensoDao.searchExistPlantDensoByRelation(
                plantDensoWithScopeDomain);
        }
        return recordCount;
    }
}
