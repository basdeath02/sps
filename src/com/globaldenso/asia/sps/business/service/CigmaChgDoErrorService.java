/*
 * ModifyDate Development company     Describe 
 * 2015/03/11 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDomain;

/**
 * <p>The Interface CigmaChgDoErrorService.</p>
 * <p>For SPS_CIGMA_CHG_DO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferChgDoError</li>
 * </ul>
 *
 * @author CSI
 */
public interface CigmaChgDoErrorService {
    
    /**
     * Search transfer Change D/O error.
     * 
     * @param spsCigmaChgDoErrorDomain the SPS_CIGMA_CHG_DO_ERROR
     * @return List<TransferDoErrorEmailDomain>
    */
    public List<TransferChgDoErrorEmailDomain> searchTransferChgDoError(
        SpsCigmaChgDoErrorDomain spsCigmaChgDoErrorDomain);
}
