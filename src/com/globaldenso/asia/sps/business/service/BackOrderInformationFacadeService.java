/*
 * ModifyDate Development company     Describe 
 * 2014/08/26 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface BackOrderInformationFacadeService.</p>
 * <p>Facade for BackOrderInformationFacadeService.</p>
 * <ul>
 * <li>Method search   : searchInitial</li>
 * <li>Method search   : searchBackOrderInformation</li>
 * <li>Method search   : searchBackOrderInformationCsv</li>
 * <li>Method search   : searchSelectedCompanySupplier</li>
 * <li>Method search   : searchSelectedCompanyDenso</li>
 * <li>Method search   : searchSelectedPlantSupplier</li>
 * <li>Method search   : searchSelectedPlantDenso</li> 
 * <li>Method search   : searchFileName</li>
 * <li>Method search   : searchLegendInfo</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public interface BackOrderInformationFacadeService {
    
    /**
     * <p>Search Initial.</p>
     * <ul>
     * <li>Initialization of Back Order Information Screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the Data Scope Control Domain
     * @return the Back Order Information Return Domain.
     * @throws ApplicationException ApplicationException
     */
    public BackOrderInformationReturnDomain searchInitial(DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException; 
    
    /**
     * <p>Search back order information detail.</p>
     * <ul>
     * <li>Search back order information item detail for display on Back Order Information Screen.</li>
     * </ul>
     * 
     * @param backOrderInformationCriteria the Back Order Information Domain
     * @return the list of Invoice Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public BackOrderInformationReturnDomain searchBackOrderInformation(
        BackOrderInformationDomain backOrderInformationCriteria)
        throws ApplicationException;
    
    /**
     * <p>Search back order information detail.</p>
     * <ul>
     * <li>Search back order information item detail for display on Back Order Information Screen.</li>
     * </ul>
     * 
     * @param backOrderInformationCriteria the Back Order Information Domain
     * @return the list of Invoice Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public BackOrderInformationReturnDomain searchBackOrderInformationCsv(
        BackOrderInformationDomain backOrderInformationCriteria) 
        throws ApplicationException;
    
    /**
     * <p>Search selected company denso.</p>
     * <ul>
     * <li>Search supplier plant code for related supplier company code.</li>
     * </ul>
     * 
     * @return the list
     * @param plantSupplierWithScopeDomain the Plant Denso With Scope Domain
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)throws ApplicationException;
    
    /**
     * <p>Search selected company supplier.</p>
     * <ul>
     * <li>Search denso plant code for related denso company code.</li>
     * </ul>
     * 
     * @return the list
     * @param plantDensoWithScopeDomain the Plant Denso With Scope Domain
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant supplier code.</p>
     * <ul>
     * <li>Search list of company supplier to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Plant Supplier
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Find file name.</p>
     * <ul>
     * <li>Search PDF file name from file id.</li>
     * </ul>
     * 
     * @param backOrderInformation the Back Order Information Domain
     * @return the File Management Domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(BackOrderInformationDomain backOrderInformation) 
        throws ApplicationException;
    
    /**
     * <p>Search Legend Information.</p>
     * <ul>
     * <li>Search delivery order report from PDF file id.</li>
     * </ul>
     * 
     * @param backOrderInformation the Back Order Information Domain
     * @param output the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(BackOrderInformationDomain backOrderInformation, 
        OutputStream output) throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}