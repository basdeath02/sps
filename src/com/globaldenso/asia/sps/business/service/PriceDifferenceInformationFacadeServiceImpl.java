/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 2015/08/04 CSI Akat                [IN009]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationReturnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;

/**
 * <p>
 * The class PriceDifferenceInformationFacadeServiceImpl.
 * </p>
 * <p>
 * Facade for PriceDifferenceInformationFacadeServiceImpl.
 * </p>
 * <ul>
 * <li>Method search : searchPriceDifferenceInformation</li>
 * <li>Method create : downloadInvoiceInformation</li>
 * </ul>
 * 
 * @author CSI
 */
public class PriceDifferenceInformationFacadeServiceImpl implements
    PriceDifferenceInformationFacadeService {

    /** The cn service. */
    private CnService cnService = null;

    /** The common service. */
    private CommonService commonService = null;

    /** The misc service. */
    private MiscellaneousService miscService = null;

    /** The DensoSupplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService = null;

    /** The Company Supplier Service. */
    private CompanySupplierService companySupplierService = null;

    /** The Company Denso Servicee. */
    private CompanyDensoService companyDensoService = null;

    /** The Plant Supplier Service. */
    private PlantSupplierService plantSupplierService = null;

    /** The Plant Supplier Service. */
    private PlantDensoService plantDensoService = null;

    /** The record Limit Service. */
    private RecordLimitService recordLimitService = null;

    /** The price Difference Information Facade Service. */
    private FileManagementService fileManagementService = null;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new Price Difference Information Facade Service Impl.
     */
    public PriceDifferenceInformationFacadeServiceImpl() {
        super();
    }

    /**
     * <p>
     * Setter method for fileManagementService.
     * </p>
     * 
     * @param fileManagementService Set for fileManagementService
     */
    public void setFileManagementService(
        FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * Set the cn service.
     * 
     * @param cnService the cn service to set
     */
    public void setCnService(CnService cnService) {
        this.cnService = cnService;
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }

    /**
     * <p>
     * Setter method for recordLimitService.
     * </p>
     * 
     * @param recordLimitService Set for recordLimitService
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationService.
     * </p>
     * 
     * @param densoSupplierRelationService Set for densoSupplierRelationService
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * <p>
     * Setter method for companySupplierService.
     * </p>
     * 
     * @param companySupplierService Set for companySupplierService
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }

    /**
     * <p>
     * Setter method for companyDensoService.
     * </p>
     * 
     * @param companyDensoService Set for companyDensoService
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * <p>
     * Setter method for plantSupplierService.
     * </p>
     * 
     * @param plantSupplierService Set for plantSupplierService
     */
    public void setPlantSupplierService(
        PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>
     * Setter method for plantDensoService.
     * </p>
     * 
     * @param plantDensoService Set for plantDensoService
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchInitial(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain,
     *      com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain)
     */
    public PriceDifferenceInformationDomain searchInitial(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain,
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException {

        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = null;
        List<CompanySupplierDomain> companySupplierDomainList = null;
        List<PlantSupplierDomain> plantSupplierDomainList = null;
        List<PlantDensoDomain> plantDensoDomainList = null;
        List<CompanyDensoDomain> companyDensoDomainList = null;
        List<MiscellaneousDomain> invoiceStatusList;
        Locale locale = plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getLocale();

        densoSupplierRelationDomainList = densoSupplierRelationService.searchDensoSupplierRelation(
            plantSupplierWithScopeDomain.getDataScopeControlDomain());

        if (null == densoSupplierRelationDomainList
            || Constants.ZERO == densoSupplierRelationDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        plantSupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);

        companySupplierDomainList = companySupplierService
            .searchCompanySupplier(plantSupplierWithScopeDomain);
        if (null == companySupplierDomainList
            || Constants.ZERO == companySupplierDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE, locale)});
        }
        
        plantDensoWithScope.setDataScopeControlDomain(
            plantSupplierWithScopeDomain.getDataScopeControlDomain());
        companyDensoDomainList = companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if (null == companyDensoDomainList 
            || Constants.ZERO == companyDensoDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }

        plantSupplierDomainList = plantSupplierService
            .searchPlantSupplier(plantSupplierWithScopeDomain);
        if (priceDifferenceInformationDomain.getVendorCd() != null) {
            if (null == plantSupplierDomainList 
                || Constants.ZERO == plantSupplierDomainList.size()) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE,
                        locale)});
            }
        }
        List<SpsMMiscDomain> plantSupplierDomainMiscList = new ArrayList<SpsMMiscDomain>();
        for (PlantSupplierDomain plantSupplierDomain : plantSupplierDomainList) {
            SpsMMiscDomain companyCode = new SpsMMiscDomain();
            companyCode.setMiscCd(plantSupplierDomain.getSPcd());
            companyCode.setMiscValue(plantSupplierDomain.getSPcd());
            plantSupplierDomainMiscList.add(companyCode);
        }
        
        plantDensoWithScope.getPlantDensoDomain().setDCd(priceDifferenceInformationDomain.getDCd());
        plantDensoDomainList = plantDensoService.searchPlantDenso(plantDensoWithScope);
        if (null != priceDifferenceInformationDomain.getDCd()) {
            if (null == plantDensoDomainList
                || Constants.ZERO == plantDensoDomainList.size()) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
            }
        }
        List<SpsMMiscDomain> densoPlantCodeList = new ArrayList<SpsMMiscDomain>();
        for (PlantDensoDomain PlantDensoDomain : plantDensoDomainList) {
            SpsMMiscDomain densoCode = new SpsMMiscDomain();
            densoCode.setMiscCd(PlantDensoDomain.getDPcd());
            densoCode.setMiscValue(PlantDensoDomain.getDPcd());
            densoPlantCodeList.add(densoCode);
        }
        
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_INVOICE_STATUS);
        invoiceStatusList = miscService.searchMisc(miscDomain);
        if (null == invoiceStatusList 
            || Constants.ZERO == invoiceStatusList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_INVOICE_STATUS, locale)});
        }

        priceDifferenceInformationDomain
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        priceDifferenceInformationDomain.setCompanySupplierDomainList(companySupplierDomainList);
        priceDifferenceInformationDomain.setCompanyDensoDomainList(companyDensoDomainList);
        priceDifferenceInformationDomain.setPlantDensoMiscList(densoPlantCodeList);
        priceDifferenceInformationDomain.setPlantSupplierMiscList(plantSupplierDomainMiscList);
        priceDifferenceInformationDomain.setPlantSupplierDomainList(plantSupplierDomainList);
        priceDifferenceInformationDomain.setPlantDensoDomainList(plantDensoDomainList);
        priceDifferenceInformationDomain.setInvoiceStatusList(invoiceStatusList);

        return priceDifferenceInformationDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchPriceDifferenceInformation
     *      (com.globaldenso.asia.sps.presentation.form.WINV005Form,
     *      com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain,
     *      com.globaldenso.asia.sps.business.domain.MiscDomain, boolean)
     */
    public PriceDifferenceInformationReturnDomain searchPriceDifferenceInformation(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException {

        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        PriceDifferenceInformationReturnDomain priceDifferenceInformationReturnDomain = 
            new PriceDifferenceInformationReturnDomain();
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        errorMessageList = null;
        Locale locale = priceDifferenceInformationDomain.getLocale();
        errorMessageList = validate(priceDifferenceInformationDomain);
        
        if (null != errorMessageList && Constants.ZERO < errorMessageList.size()) {
            priceDifferenceInformationReturnDomain
                .setErrorMessageList(errorMessageList);
            return priceDifferenceInformationReturnDomain;
        } else {
            if (Constants.MISC_CODE_ALL.equals(priceDifferenceInformationDomain.getVendorCd())) {
                priceDifferenceInformationDomain.setVendorCd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(priceDifferenceInformationDomain.getSPcd())) {
                priceDifferenceInformationDomain.setSPcd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(priceDifferenceInformationDomain.getDCd())) {
                priceDifferenceInformationDomain.setDCd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(priceDifferenceInformationDomain.getDPcd())) {
                priceDifferenceInformationDomain.setDPcd(null);
            }
            if (Constants.MISC_CODE_ALL.equals(priceDifferenceInformationDomain
                .getInvoiceStatus())) {
                priceDifferenceInformationDomain.setInvoiceStatus(null);
            }
            
            int recordCount = cnService
                .searchCountCn(priceDifferenceInformationDomain);
            if (Constants.ZERO == recordCount) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
        
            miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV005_RLM);
            MiscellaneousDomain recordLimit = recordLimitService
                .searchRecordLimit(miscDomain);
            if (null == recordLimit) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            if (Integer.valueOf(recordLimit.getMiscValue()) < recordCount) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {recordLimit.getMiscValue()});
                
            }
            miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV005_PLM);
            MiscellaneousDomain recordLimitPerPage = recordLimitService
                .searchRecordLimitPerPage(miscDomain);
            if (null == recordLimitPerPage) {
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }

            List<PriceDifferenceInformationDomain> priceDifferenceInformationDomainList = 
                new ArrayList<PriceDifferenceInformationDomain>();
            
            
            priceDifferenceInformationDomain.setMaxRowPerPage(Integer
                .parseInt(recordLimitPerPage.getMiscValue()));
            SpsPagingUtil.calcPaging(priceDifferenceInformationDomain,
                recordCount);
            priceDifferenceInformationDomainList = cnService
                .searchCn(priceDifferenceInformationDomain);
            priceDifferenceInformationDomain
                .setRowNumFrom(priceDifferenceInformationDomain.getRowNumFrom());
            priceDifferenceInformationDomain
                .setRowNumTo(priceDifferenceInformationDomain.getRowNumTo());

            priceDifferenceInformationReturnDomain
            .setPriceDifferenceInformationDomain(priceDifferenceInformationDomain);
            priceDifferenceInformationReturnDomain
                .setErrorMessageList(errorMessageList);
            priceDifferenceInformationReturnDomain
                .setPriceDifferenceInformationDomainList(priceDifferenceInformationDomainList);
            
        }
        return priceDifferenceInformationReturnDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @return
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchPriceDifferenceInformationCsv
     *      (com.globaldenso.asia.sps.presentation.form.WINV005Form,
     *      com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain,
     *      com.globaldenso.asia.sps.business.domain.MiscDomain, boolean)
     */
    public PriceDifferenceInformationReturnDomain searchPriceDifferenceInformationCsv(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException {

        int recordCount = Constants.ZERO;
        CommonDomain commonDomain = new CommonDomain();
        StringBuffer resultCsv = null;
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        Locale locale = priceDifferenceInformationDomain.getLocale();
        PriceDifferenceInformationReturnDomain priceReturn = 
            new PriceDifferenceInformationReturnDomain();
        List<ApplicationMessageDomain> errorMessageList = 
            validate(priceDifferenceInformationDomain);
        
        if (null != errorMessageList
            && Constants.ZERO < errorMessageList.size()) {
            priceReturn.setErrorMessageList(errorMessageList);
            return priceReturn;
        }
        
        int maxRecord = cnService.searchCountCn(priceDifferenceInformationDomain);
        if (Constants.ZERO == maxRecord) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0001,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        } else {
            recordCount = maxRecord;
        }
       
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        miscDomain.setMiscCode(SupplierPortalConstant.MISC_CODE_WINV005_RLM);
        MiscellaneousDomain recordLimit = recordLimitService.searchRecordLimit(miscDomain);
        if (null == recordLimit) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
            
        if (Integer.valueOf(recordLimit.getMiscValue()) < recordCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[] {recordLimit.getMiscValue()});
        }
            
        try {
            List<PriceDifferenceInformationDomain> result
                = cnService.searchCn(priceDifferenceInformationDomain);
            
            if (Constants.ZERO < result.size()){
                String fileName = StringUtil.appendsString(
                    Constants.PRICE_DIFFERENCE_INFORMATION_LIST,
                    Constants.SYMBOL_UNDER_SCORE,
                    DateUtil.format(this.commonService.searchSysDate(), 
                        DateUtil.PATTERN_YYYYMMDD_HHMM));
                
                final String[] headerArr = new String[] {
                    SupplierPortalConstant.LBL_S_CD,
                    SupplierPortalConstant.LBL_S_PCD,
                    SupplierPortalConstant.LBL_D_CD,
                    SupplierPortalConstant.LBL_D_PCD,
                    SupplierPortalConstant.LBL_D_PN,
                    SupplierPortalConstant.LBL_S_PN,
                    SupplierPortalConstant.LBL_DENSO_PRICE_UNIT,
                    SupplierPortalConstant.LBL_DENSO_CURRENCY,
                    SupplierPortalConstant.LBL_TEMP_PRICE,
                    SupplierPortalConstant.LBL_SUPPLIER_PRICE_UNIT,
                    SupplierPortalConstant.LBL_DIFFERENCE_PRICE_UNIT,
                    SupplierPortalConstant.LBL_ASN_DATE,
                    SupplierPortalConstant.LBL_ASN_NO,
                    SupplierPortalConstant.LBL_PLAN_ETA,
                    SupplierPortalConstant.LBL_SHIPPING_QTY,
                    SupplierPortalConstant.LBL_DENSO_UNIT_OF_MEASURE,
                    SupplierPortalConstant.LBL_DENSO_BASE_AMOUNT_BY_PN,
                    SupplierPortalConstant.LBL_SUPPLIER_BASE_AMOUNT_BY_PN,
                    SupplierPortalConstant.LBL_DIFFERENCE_BASE_AMOUNT_BY_PN,
                    SupplierPortalConstant.LBL_COVER_PAGE_NO,
                    SupplierPortalConstant.LBL_INVOICE_NO,
                    SupplierPortalConstant.LBL_INVOICE_DATE,
                    SupplierPortalConstant.LBL_INVOICE_STATUS,
                    SupplierPortalConstant.LBL_CREDIT_NOTE_NO,
                    SupplierPortalConstant.LBL_CREDIT_NOTE_DATE,
                    SupplierPortalConstant.LBL_CREDIT_NOTE_BASE_AMOUNT,
                    SupplierPortalConstant.LBL_CREDIT_NOTE_VAT_AMOUNT,
                    SupplierPortalConstant.LBL_CREDIT_NOTE_TOTAL_AMOUNT,
                    SupplierPortalConstant.LBL_PAYMENT_DATE};
                        
                List<Map<String, Object>> resultDetail
                    = getPriceDiffInformationMap(result, headerArr);
                
                commonDomain.setResultList(resultDetail);
                commonDomain.setHeaderArr(headerArr);
                commonDomain.setHeaderFlag(true);
                resultCsv = downloadInvoiceInformation(commonDomain);
                priceReturn.setCsvBufffer(resultCsv);
                priceReturn.setFileName(fileName);
            } else {
                throw new ApplicationException(Constants.EMPTY_STRING);
            }
        } catch (ApplicationException e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0012, null);
        } finally {
            priceReturn.setErrorMessageList(errorMessageList);
        }
        return priceReturn;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchSelectedCompanySupplier
     *      (com.globaldenso.asia.sps.presentation.form.WINV005Form,
     *      com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain,
     *      com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain)
     */
    public List<SpsMMiscDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {

        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = null;
        List<PlantSupplierDomain> plantSupplierDomainList = null;

        densoSupplierRelationDomainList = densoSupplierRelationService.searchDensoSupplierRelation(
            plantSupplierWithScopeDomain.getDataScopeControlDomain());
        Locale locale = plantSupplierWithScopeDomain.getDataScopeControlDomain().getLocale();
        if (null == densoSupplierRelationDomainList) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        
        plantSupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        plantSupplierDomainList = plantSupplierService
            .searchPlantSupplier(plantSupplierWithScopeDomain);
        if (null == plantSupplierDomainList || Constants.ZERO == plantSupplierDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE, locale)});
        }
        List<SpsMMiscDomain> plantSupplierDomainMiscList = new ArrayList<SpsMMiscDomain>();
        for (PlantSupplierDomain plantSupplierDomain : plantSupplierDomainList) {
            SpsMMiscDomain companyCode = new SpsMMiscDomain();
            companyCode.setMiscCd(plantSupplierDomain.getSPcd());
            companyCode.setMiscValue(plantSupplierDomain.getSPcd());
            plantSupplierDomainMiscList.add(companyCode);
        }
        return plantSupplierDomainMiscList;
    }
    
    /**
     * {@inheritDoc}
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchSelectedPlantSupplier
     *      (com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {

        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = null;

        densoSupplierRelationDomainList = densoSupplierRelationService.searchDensoSupplierRelation(
            plantSupplierWithScopeDomain.getDataScopeControlDomain());
        Locale locale = plantSupplierWithScopeDomain.getDataScopeControlDomain().getLocale();
        if (null == densoSupplierRelationDomainList) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        
        plantSupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        List<CompanySupplierDomain> companySupplierDomainList = null;
        companySupplierDomainList = companySupplierService
            .searchCompanySupplier(plantSupplierWithScopeDomain);
        if (null == companySupplierDomainList
            || Constants.ZERO == companySupplierDomainList.size())
        {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE, locale)});
        }
        return companySupplierDomainList;
    }
    
    /**
     * {@inheritDoc}
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchSelectedCompanyDenso
     *      (com.globaldenso.asia.sps.presentation.form.WINV005Form,
     *      com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain,
     *      com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain)
     */
    public List<SpsMMiscDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = null;
        List<PlantDensoDomain> plantDensoDomainList = null;
        densoSupplierRelationDomainList = densoSupplierRelationService.searchDensoSupplierRelation(
            plantDensoWithScopeDomain.getDataScopeControlDomain());
        Locale locale = plantDensoWithScopeDomain.getDataScopeControlDomain().getLocale();

        if (null == densoSupplierRelationDomainList) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);

        plantDensoDomainList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);

        if (null == plantDensoDomainList || Constants.ZERO == plantDensoDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }

        List<SpsMMiscDomain> densoPlantCodeList = new ArrayList<SpsMMiscDomain>();
        for (PlantDensoDomain PlantDensoDomain : plantDensoDomainList) {
            SpsMMiscDomain densoCode = new SpsMMiscDomain();
            densoCode.setMiscCd(PlantDensoDomain.getDPcd());
            densoCode.setMiscValue(PlantDensoDomain.getDPcd());
            densoPlantCodeList.add(densoCode);
        }
        return densoPlantCodeList;
    }

    /**
     * {@inheritDoc}
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchSelectedCompanyDenso
     *      (com.globaldenso.asia.sps.presentation.form.WINV005Form,
     *      com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain,
     *      com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException
    {
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = null;
        densoSupplierRelationDomainList = densoSupplierRelationService.searchDensoSupplierRelation(
            plantDensoWithScopeDomain.getDataScopeControlDomain());
        Locale locale = plantDensoWithScopeDomain.getDataScopeControlDomain().getLocale();

        if (null == densoSupplierRelationDomainList) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);

        List<CompanyDensoDomain> companyDensoDomainList = null;
        companyDensoDomainList = companyDensoService.searchCompanyDenso(plantDensoWithScopeDomain);

        if (null == companyDensoDomainList || Constants.ZERO == companyDensoDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }
        return companyDensoDomainList;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNDownloadingFacadeService#searchFileName
     *      (com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public FileManagementDomain searchFileName(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException {

        Locale locale = priceDifferenceInformationDomain.getLocale();
        FileManagementDomain resultDomain = null;

        try {
            resultDomain = fileManagementService.searchFileDownload(
                priceDifferenceInformationDomain.getFileId(), false, null);

        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0001);
        }

        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #downloadInvoiceInformation
     *      (com.globaldenso.asia.sps.business.domain.CommonDomain)
     */
    public StringBuffer downloadInvoiceInformation(CommonDomain commonDomain)
        throws ApplicationException {

        Locale locale = commonDomain.getLocale();
        StringBuffer result = null;
        try {
            result = commonService.createCsvString(commonDomain);

        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                SupplierPortalConstant.ERROR_CD_SP_90_0001);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService
     *      #searchLegendInfo
     *      (com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain)
     */
    public FileManagementDomain searchLegendInfo(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain,
        OutputStream output) throws ApplicationException {

        Locale locale = priceDifferenceInformationDomain.getLocale();
        String fileId = priceDifferenceInformationDomain.getFileId();
        FileManagementDomain fileManagementDomain = new FileManagementDomain();
        boolean dataflag = true;
        try {
            fileManagementDomain = fileManagementService.searchFileDownload(
                fileId, dataflag, output);
        } catch (IOException e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0001);
        }
        return fileManagementDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }

    /** 
     * <p>
     * Validate criteria Method.
     * </p>  
     * @param priceDifferenceInformationDomain the price difference information domain
     * @return list of application message domain
     * @throws ApplicationException ApplicationException
     */
    private List<ApplicationMessageDomain> validate(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain)
        throws ApplicationException {

        String invoiceDateFrom = null;
        String invoiceDateTo = null;
        String cnDateFrom = null;
        String cnDateTo = null;
        boolean hasInvoiceDateFrom = false;
        boolean hasInvoiceDateTo = false;
        boolean hasCnDateFrom = false;
        boolean hasCnDateTo = false;

        Locale locale = priceDifferenceInformationDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        if (Strings.judgeBlank(priceDifferenceInformationDomain
            .getInvoiceDateFrom())
            || Strings.judgeBlank(priceDifferenceInformationDomain
                .getInvoiceDateTo())
            || Strings.judgeBlank(priceDifferenceInformationDomain.getVendorCd())
            || Strings.judgeBlank(priceDifferenceInformationDomain.getSPcd())
            || Strings.judgeBlank(priceDifferenceInformationDomain.getDCd())
            || Strings.judgeBlank(priceDifferenceInformationDomain.getDPcd())) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil
                    .getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        } else {
            if (priceDifferenceInformationDomain.getVendorCd().equals(
                Constants.MISC_CODE_ALL)) {
                priceDifferenceInformationDomain.setVendorCd(Constants.EMPTY_STRING);
            }
            if (priceDifferenceInformationDomain.getSPcd().equals(
                Constants.MISC_CODE_ALL)) {
                priceDifferenceInformationDomain
                    .setSPcd(Constants.EMPTY_STRING);
            }
            if (priceDifferenceInformationDomain.getDCd().equals(
                Constants.MISC_CODE_ALL)) {
                priceDifferenceInformationDomain.setDCd(Constants.EMPTY_STRING);
            }
            if (priceDifferenceInformationDomain.getDPcd().equals(
                Constants.MISC_CODE_ALL)) {
                priceDifferenceInformationDomain
                    .setDPcd(Constants.EMPTY_STRING);
            }
            if (priceDifferenceInformationDomain.getInvoiceStatus().equals(
                Constants.MISC_CODE_ALL)) {
                priceDifferenceInformationDomain
                    .setInvoiceStatus(Constants.EMPTY_STRING);
            }
        }
        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getInvoiceDateFrom())) {
            invoiceDateFrom = priceDifferenceInformationDomain.getInvoiceDateFrom();
            if (!DateUtil.isValidDate(invoiceDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {getLabel(
                            SupplierPortalConstant.LBL_INVOICE_DATE_FROM,
                            locale)})));
                hasInvoiceDateFrom = false;
            } else {
                hasInvoiceDateFrom = true;
            }
        }
        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getInvoiceDateTo())) {
            invoiceDateTo = priceDifferenceInformationDomain.getInvoiceDateTo();
            if (!DateUtil.isValidDate(invoiceDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {getLabel(
                            SupplierPortalConstant.LBL_INVOICE_DATE_TO,
                            locale)})));
                hasInvoiceDateTo = false;
            } else {
                hasInvoiceDateTo = true;
            }
        }
        if (hasInvoiceDateFrom && hasInvoiceDateTo) {
            if (Constants.ZERO < DateUtil.compareDate(
                priceDifferenceInformationDomain.getInvoiceDateFrom(),
                priceDifferenceInformationDomain.getInvoiceDateTo())) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(
                                SupplierPortalConstant.LBL_INVOICE_DATE_FROM,
                                locale),
                            getLabel(
                                SupplierPortalConstant.LBL_INVOICE_DATE_TO,
                                locale)})));

            }
        }
        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getCnDateFrom())) {
            cnDateFrom = priceDifferenceInformationDomain.getCnDateFrom();
            hasCnDateFrom = true;
            if (!DateUtil.isValidDate(cnDateFrom, DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                hasCnDateFrom = false;
                errorMessageList
                    .add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getErrorMessage(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                                new String[] {getLabel(
                                    SupplierPortalConstant.LBL_CN_DATE_FROM,
                                    locale)})));
            }
        }

        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getCnDateTo())) {
            cnDateTo = priceDifferenceInformationDomain.getCnDateTo();
            hasCnDateTo = true;
            if (!DateUtil.isValidDate(cnDateTo, DateUtil.PATTERN_YYYYMMDD_SLASH)) {
                hasCnDateTo = false;
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {getLabel(
                            SupplierPortalConstant.LBL_CN_DATE_TO, locale)})));
            }
        }
        if (hasCnDateFrom && hasCnDateTo) {
            Calendar dateFrom = Calendar.getInstance();
            Calendar dateTo = Calendar.getInstance();
            try {
                dateFrom.setTime(DateUtil.parseToUtilDate(cnDateFrom,
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
                dateTo.setTime(DateUtil.parseToUtilDate(cnDateTo,
                    DateUtil.PATTERN_YYYYMMDD_SLASH));
            } catch (Exception e) {
                throw new ApplicationException(e.getMessage());
            }
            if (Constants.ZERO < DateUtil.compareDate(cnDateFrom, cnDateTo)) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0031,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_CN_DATE_FROM,
                                locale),
                            getLabel(SupplierPortalConstant.LBL_CN_DATE_TO,
                                locale)})));
            }
        }
        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getInvoiceNo())) {
            if (Constants.MAX_INVOICE_NO_LENGTH < priceDifferenceInformationDomain
                .getInvoiceNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_INVOICE_NO,
                                locale),
                            String.valueOf(Constants.MAX_INVOICE_NO_LENGTH)})));
            }else{
                priceDifferenceInformationDomain.setInvoiceNo(priceDifferenceInformationDomain
                    .getInvoiceNo().trim().toUpperCase());
            }
        }
        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getCnNo())) {
            if (Constants.MAX_CN_NO_LENGTH < priceDifferenceInformationDomain
                .getCnNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_CN_NO, locale),
                            String.valueOf(Constants.MAX_CN_NO_LENGTH)})));
            }else{
                priceDifferenceInformationDomain.setCnNo(priceDifferenceInformationDomain
                    .getCnNo().trim().toUpperCase());
            }
        }

        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getAsnNo())) {
            if (Constants.MAX_ASN_NO_LENGTH < priceDifferenceInformationDomain
                .getAsnNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getErrorMessage(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                            SupplierPortalConstant.ERROR_CD_SP_90_0002,
                            new String[] {
                                getLabel(SupplierPortalConstant.LBL_ASN_NO,
                                    locale),
                                String.valueOf(Constants.MAX_ASN_NO_LENGTH)})));
            }else{
                priceDifferenceInformationDomain.setAsnNo(priceDifferenceInformationDomain
                    .getAsnNo().trim().toUpperCase());
            }
        }
        
        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getSPn())) {
            if (Constants.MAX_S_PART_NO_LENGTH < priceDifferenceInformationDomain
                .getSPn().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(
                                SupplierPortalConstant.LBL_SUPPLIER_PART_NO,
                                locale),
                            String.valueOf(Constants.MAX_S_PART_NO_LENGTH)})));
            }else{
                priceDifferenceInformationDomain.setSPn(priceDifferenceInformationDomain
                    .getSPn().trim().toUpperCase());
            }
        }

        if (!StringUtil.checkNullOrEmpty(priceDifferenceInformationDomain.getDPn())) {
            if (Constants.MAX_D_PART_NO_LENGTH < priceDifferenceInformationDomain
                .getDPn().length()) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil.getErrorMessage(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {
                            getLabel(SupplierPortalConstant.LBL_DENSO_PART_NO,
                                locale),
                            String.valueOf(Constants.MAX_D_PART_NO_LENGTH)})));
            }else{
                priceDifferenceInformationDomain.setDPn(priceDifferenceInformationDomain
                    .getDPn().trim().toUpperCase());
            }
        }
        
        if (Constants.ZERO < errorMessageList.size()) {
            return errorMessageList;
        } else {
            return null;
        }
    }

    /** 
     * <p>
     * Price difference information map Method.
     * </p> 
     * @param priceDiffInformationList the list of invoice information domain
     * @param header the array of string
     * @return List<Map<String, Object>>
     */
    private List<Map<String, Object>> getPriceDiffInformationMap(
        List<PriceDifferenceInformationDomain> priceDiffInformationList, String[] header)
    {
        List<Map<String, Object>> priceDiffInformationMapList = 
            new ArrayList<Map<String, Object>>();
        Map<String, Object> priceDiffInformationMap = null;
        
        int decimalPoint = 0;
        if(!StringUtil.checkNullOrEmpty(
            priceDiffInformationList.get(Constants.ZERO).getDecimalDisp())){
            decimalPoint = Integer.valueOf(
                priceDiffInformationList.get(Constants.ZERO).getDecimalDisp());
        }
        
        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        companyDecimalFormat.setMinimumFractionDigits(decimalPoint);
        companyDecimalFormat.setMaximumFractionDigits(decimalPoint);
        
        NumberFormat priceNumberFormat = NumberFormat.getInstance();
        DecimalFormat priceDecimalFormat = (DecimalFormat)priceNumberFormat;
        priceDecimalFormat.setGroupingUsed(true);
        priceDecimalFormat.setMinimumFractionDigits(Constants.FOUR);
        priceDecimalFormat.setMaximumFractionDigits(Constants.FOUR);
        
        for(PriceDifferenceInformationDomain cnItem : priceDiffInformationList)
        {
            priceDiffInformationMap = new HashMap<String, Object>();
            priceDiffInformationMap.put(header[Constants.ZERO], StringUtil.checkNullToEmpty(
                cnItem.getVendorCd()));
            priceDiffInformationMap.put(header[Constants.ONE], StringUtil.checkNullToEmpty(
                cnItem.getSPcd()));
            priceDiffInformationMap.put(header[Constants.TWO], StringUtil.checkNullToEmpty(
                cnItem.getDCd()));
            priceDiffInformationMap.put(header[Constants.THREE], StringUtil.checkNullToEmpty(
                cnItem.getDPcd()));
            priceDiffInformationMap.put(header[Constants.FOUR], cnItem.getDPn());
            priceDiffInformationMap.put(header[Constants.FIVE], cnItem.getSPn());

            // [IN009] Rounding Mode use HALF_UP
            //priceDiffInformationMap.put(header[Constants.SIX], this.convertAmountToDecimalFormat(
            //    priceDecimalFormat, cnItem.getSpsTCnDetailDomain().getDPriceUnit()));
            //priceDiffInformationMap.put(header[Constants.SEVEN], StringUtil.checkNullToEmpty(
            //    cnItem.getSpsTInvoiceDomain().getDCurrencyCd()));
            //priceDiffInformationMap.put(header[Constants.EIGHT], StringUtil.checkNullToEmpty(
            //    cnItem.getSpsTInvoiceDetailDomain().getTmpPriceFlg()));
            //priceDiffInformationMap.put(header[Constants.NINE], this.convertAmountToDecimalFormat(
            //    priceDecimalFormat, cnItem.getSpsTCnDetailDomain().getSPriceUnit()));
            //priceDiffInformationMap.put(header[Constants.TEN], this.convertAmountToDecimalFormat(
            //    priceDecimalFormat, cnItem.getSpsTCnDetailDomain().getDiffPriceUnit()));
            priceDiffInformationMap.put(header[Constants.SIX], this.convertAmountToDecimalFormat(
                priceDecimalFormat, cnItem.getSpsTCnDetailDomain().getDPriceUnit(), FOUR));
            priceDiffInformationMap.put(header[Constants.SEVEN], StringUtil.checkNullToEmpty(
                cnItem.getSpsTInvoiceDomain().getDCurrencyCd()));
            priceDiffInformationMap.put(header[Constants.EIGHT], StringUtil.checkNullToEmpty(
                cnItem.getSpsTInvoiceDetailDomain().getTmpPriceFlg()));
            priceDiffInformationMap.put(header[Constants.NINE], this.convertAmountToDecimalFormat(
                priceDecimalFormat, cnItem.getSpsTCnDetailDomain().getSPriceUnit(), FOUR));
            priceDiffInformationMap.put(header[Constants.TEN], this.convertAmountToDecimalFormat(
                priceDecimalFormat, cnItem.getSpsTCnDetailDomain().getDiffPriceUnit(), FOUR));
            
            String asnDate = Constants.EMPTY_STRING;
            String planEta = Constants.EMPTY_STRING;
            if(null != cnItem.getAsnDate()){
                asnDate = DateUtil.format(
                    new Date(cnItem.getAsnDate().getTime()), DateUtil.PATTERN_YYYYMMDD_SLASH);
            }
            
            if(null != cnItem.getPlanEta()){
                planEta = DateUtil.format(
                    new Date(cnItem.getPlanEta().getTime()), DateUtil.PATTERN_YYYYMMDD_SLASH);
            }
            
            priceDiffInformationMap.put(header[Constants.ELEVEN], asnDate);
            priceDiffInformationMap.put(header[Constants.TWELVE],
                cnItem.getSpsTCnDetailDomain().getAsnNo());
            priceDiffInformationMap.put(header[Constants.THIRTEEN], planEta);
            priceDiffInformationMap.put(header[Constants.FOURTEEN],
                cnItem.getSpsTInvoiceDetailDomain().getShippingQty());
            priceDiffInformationMap.put(header[Constants.FIFTEEN],
                cnItem.getSpsTInvoiceDetailDomain().getDUnitOfMeasure());
            
            BigDecimal sBaseAmountByPN = cnItem.getSpsTCnDetailDomain().getSPriceUnit().multiply(
                cnItem.getSpsTInvoiceDetailDomain().getShippingQty());
            BigDecimal dBaseAmountByPN = cnItem.getSpsTCnDetailDomain().getDPriceUnit().multiply(
                cnItem.getSpsTInvoiceDetailDomain().getShippingQty());
            BigDecimal diffBaseAmountByPn = dBaseAmountByPN.subtract(sBaseAmountByPN);

            // [IN009] Rounding Mode use HALF_UP
            //priceDiffInformationMap.put(header[Constants.SIXTEEN],
            //    this.convertAmountToDecimalFormat(companyDecimalFormat, sBaseAmountByPN));
            //priceDiffInformationMap.put(header[Constants.SEVENTEEN],
            //    this.convertAmountToDecimalFormat(companyDecimalFormat, dBaseAmountByPN));
            //priceDiffInformationMap.put(header[Constants.EIGHTEEN],
            //    this.convertAmountToDecimalFormat(companyDecimalFormat, diffBaseAmountByPn));
            priceDiffInformationMap.put(header[Constants.SIXTEEN],
                this.convertAmountToDecimalFormat(
                    companyDecimalFormat, sBaseAmountByPN, decimalPoint));
            priceDiffInformationMap.put(header[Constants.SEVENTEEN],
                this.convertAmountToDecimalFormat(
                    companyDecimalFormat, dBaseAmountByPN, decimalPoint));
            priceDiffInformationMap.put(header[Constants.EIGHTEEN],
                this.convertAmountToDecimalFormat(
                    companyDecimalFormat, diffBaseAmountByPn, decimalPoint));
            
            priceDiffInformationMap.put(header[Constants.NINETEEN], StringUtil.checkNullToEmpty(
                cnItem.getSpsTInvoiceDomain().getCoverPageNo()));
            priceDiffInformationMap.put(header[Constants.TWENTY],
                cnItem.getSpsTInvoiceDomain().getInvoiceNo());
            
            String paymentDate = Constants.EMPTY_STRING;
            if(null != cnItem.getSpsTInvoiceDomain().getPaymentDate()){
                paymentDate = DateUtil.format(
                    new Date(cnItem.getSpsTInvoiceDomain().getPaymentDate().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_SLASH);
            }
            
            priceDiffInformationMap.put(header[Constants.TWENTY_ONE], cnItem.getInvoiceDate());
            priceDiffInformationMap.put(header[Constants.TWENTY_TWO],
                cnItem.getSpsTInvoiceDomain().getInvoiceStatus());
            priceDiffInformationMap.put(header[Constants.TWENTY_THREE],
                cnItem.getSpsTCnDomain().getCnNo());
            priceDiffInformationMap.put(header[Constants.TWENTY_FOUR], cnItem.getCnDate());

            // [IN009] Rounding Mode use HALF_UP
            //priceDiffInformationMap.put(header[Constants.TWENTY_FIVE],
            //    this.convertAmountToDecimalFormat(companyDecimalFormat, 
            //        cnItem.getSpsTCnDomain().getBaseAmount()));
            //priceDiffInformationMap.put(header[Constants.TWENTY_SIX],
            //    this.convertAmountToDecimalFormat(companyDecimalFormat,
            //        cnItem.getSpsTCnDomain().getVatAmount()));
            //priceDiffInformationMap.put(header[Constants.TWENTY_SEVEN],
            //    this.convertAmountToDecimalFormat(companyDecimalFormat,
            //        cnItem.getSpsTCnDomain().getTotalAmount()));
            priceDiffInformationMap.put(header[Constants.TWENTY_FIVE],
                this.convertAmountToDecimalFormat(companyDecimalFormat
                    , cnItem.getSpsTCnDomain().getBaseAmount(), decimalPoint));
            priceDiffInformationMap.put(header[Constants.TWENTY_SIX],
                this.convertAmountToDecimalFormat(companyDecimalFormat
                    , cnItem.getSpsTCnDomain().getVatAmount(), decimalPoint));
            priceDiffInformationMap.put(header[Constants.TWENTY_SEVEN],
                this.convertAmountToDecimalFormat(companyDecimalFormat
                    , cnItem.getSpsTCnDomain().getTotalAmount(), decimalPoint));
            
            priceDiffInformationMap.put(header[Constants.TWENTY_EIGHT], paymentDate);
            priceDiffInformationMapList.add(priceDiffInformationMap);
        }
        return priceDiffInformationMapList;
    }

    /**
     * <p>
     * Get Label method.
     * </p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {

        String labelString = MessageUtil.getLabelHandledException(locale, key);
        return labelString;
    }

    // [IN009] Rounding Mode use HALF_UP
    ///**
    // * Convert Amount data to Decimal Format
    // * @param decimalFormat decimal format by currency
    // * @param target to convert decimal format
    // * @return string decimal format ("1000") return ("1,000.00")
    // * */
    //private String convertAmountToDecimalFormat(DecimalFormat decimalFormat, BigDecimal target){
    //    String result = Constants.EMPTY_STRING;
    //    if(null != target){
    //        result = StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE,
    //            decimalFormat.format(target), Constants.SYMBOL_DOUBLE_QUOTE);
    //    }
    //    return result;
    //}
    /**
     * Convert Amount data to Decimal Format
     * @param decimalFormat decimal format by currency
     * @param target to convert decimal format
     * @param scale for decimal point
     * @return string decimal format ("1000") return ("1,000.00")
     * */
    private String convertAmountToDecimalFormat(DecimalFormat decimalFormat, BigDecimal target
        , int scale)
    {
        String result = Constants.EMPTY_STRING;
        if(null != target){
            result = StringUtil.appendsString(
                Constants.SYMBOL_DOUBLE_QUOTE
                , decimalFormat.format(target.setScale(scale, RoundingMode.HALF_UP))
                , Constants.SYMBOL_DOUBLE_QUOTE);
        }
        return result;
    }
}
