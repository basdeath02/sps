/*
 * ModifyDate Development company     Describe 
 * 2014/06/06 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;

/**
 * <p>The Interface UserService.</p>
 * <p>Service for User about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method update  : updateSupplierUserDetail</li>
 * <li>Method search  : searchEmailUserDenso</li>
 * </ul>
 *
 * @author CSI
 */
public interface UserService {
    
    /**
     * <p>search department name</p>
     * <ul>
     * <li>search distinct all of department name.</li>
     * </ul>
     * 
     * @return the list of String.
     */
    public List<SpsMUserDomain> searchDepartmentName();
    
    /**
     * Search Email User Denso.
     * 
     * @param userDensoDomain the user denso domain
     * @return the list of user domain
     */
    public List<SpsMUserDomain> searchEmailUserDenso (SpsMUserDensoDomain userDensoDomain);
    
}