/*
 * ModifyDate Development company     Describe 
 * 2014/05/30 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;
import com.globaldenso.asia.sps.business.dao.UserSupplierDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class SupplierUserServiceImpl.</p>
 * <p>Manage data of Supplier User.</p>
 * <ul>
 * <li>Method search  : searchCountSupplierUser</li>
 * <li>Method search  : searchSupplierUser</li>
 * <li>Method search  : searchUserEmailBySupplierCode</li>
 * <li>Method search  : searchEmailUserSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public class UserSupplierServiceImpl implements UserSupplierService {
    
    /** The supplier user dao. */
    private UserSupplierDao userSupplierDao;
    
    /**
     * Instantiates a new Supplier User service impl.
     */
    public UserSupplierServiceImpl(){
        super();
    }
    
    /**
     * Sets the supplier user dao.
     * 
     * @param userSupplierDao the new supplier user dao
     */
    public void setUserSupplierDao(UserSupplierDao userSupplierDao) {
        this.userSupplierDao = userSupplierDao;
    }
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserSupplierService#searchCountSupplierUser
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public Integer searchCountUserSupplier(SupplierUserInformationDomain supplierUserInfoDomain){

        /* Concatenates string name for use 'like' condition in query, example %name%. */
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setLastName(lastName);
        }

        Integer count = userSupplierDao.searchCountUserSupplier(supplierUserInfoDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserSupplierService#searchUserSupplier
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public List<SupplierUserInformationDomain> searchUserSupplier(SupplierUserInformationDomain 
        supplierUserInfoDomain){
        
        /* Concatenates string name for use 'like' condition in query, example %name%. */
        /* Concatenates string name for use 'like' condition in query, example %name%. */
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setLastName(lastName);
        }

        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)userSupplierDao
                .searchUserSupplier(supplierUserInfoDomain);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserSupplierService#searchUserEmailBySupplierCode
     * (com.globaldenso.asia.sps.business.domain.supplierUserDomain)
     */
    public List<String> searchUserEmailBySupplierCode(
        UserSupplierDomain supplierUserDomain){
        List<String> result = userSupplierDao.searchUserEmailBySupplierCode(supplierUserDomain);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.searchSupplierUserEmailByNotFoundFlag#searchUserSupplierEmailByNotFoundFlag
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public List<String> searchUserSupplierEmailByNotFoundFlag(
        UserSupplierDetailDomain criteria){
        List<SupplierUserInformationDomain> bufferList 
            = userSupplierDao.searchUserSupplierEmailByNotFoundFlag(criteria);
        List<String> resultList = new ArrayList<String>(bufferList.size());
        for( SupplierUserInformationDomain su : bufferList ){
            if( !resultList.contains(su.getUserDomain().getEmail()) ){
                resultList.add(su.getUserDomain().getEmail());
            }
        }
        return resultList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserSupplierService#searchUserSupplierIncludeRole
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public List<SupplierUserInformationDomain> searchUserSupplierIncludeRole(
        SupplierUserInformationDomain supplierUserInfoDomain){
        /* Concatenates string name for use 'like' condition in query, example %name%. */
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getFirstName())){
            StringBuffer firstnameBuff = new StringBuffer();
            String firstName = firstnameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getFirstName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setFirstName(firstName);
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getMiddleName())){
            StringBuffer middlenameBuff = new StringBuffer();
            String middleName = middlenameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getMiddleName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setMiddleName(middleName);
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserInfoDomain.getLastName())){
            StringBuffer lastnameBuff = new StringBuffer();
            String lastName = lastnameBuff.append(Constants.SYMBOL_PERCENT).append(
                supplierUserInfoDomain.getLastName().trim()).append(Constants.SYMBOL_PERCENT)
                .toString();
            supplierUserInfoDomain.setLastName(lastName);
        }
        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)userSupplierDao
                .searchUserSupplierIncludeRole(supplierUserInfoDomain);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserSupplierService#searchEmailUserSupplier(com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain)
     */
    public List<SpsMUserDomain> searchEmailUserSupplier(
        UserSupplierDetailDomain userSupplierDetailDomain) {
        List<SpsMUserDomain> userSupplierList = null;
        userSupplierList = userSupplierDao.searchEmailUserSupplier(userSupplierDetailDomain);
        return userSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.searchSupplierUserEmailByNotFoundFlag#searchUserSupplierEmailByUrgentFlagForBackorder
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public List<String> searchUserSupplierEmailByUrgentFlagForBackorder(
        UserSupplierDetailDomain criteria){
        List<SupplierUserInformationDomain> bufferList = userSupplierDao.searchUserSupplierEmailByUrgentFlagForBackorder(criteria);
        List<String> resultList = new ArrayList<String>(bufferList.size());
        for( SupplierUserInformationDomain su : bufferList ){
            if( !resultList.contains(su.getUserDomain().getEmail()) ){
                resultList.add(su.getUserDomain().getEmail());
            }
        }
        return resultList;
    }
}