/*
 * ModifyDate Development company     Describe 
 * 2014/06/25 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.dao.RecordLimitDao;

/**
 * <p>The Class RecordLimitServiceImpl.</p>
 * <p>Service about miscellaneous for search miscellaneous data.</p>
 * <ul>
 * <li>Method search  : searchMiscValue</li>
 * <li>Method search  : searchRecordLimitPerPage</li>
 * </ul>
 *
 * @author CSI
 */
public class RecordLimitServiceImpl implements RecordLimitService {
    
    /** The record limit dao. */
    private RecordLimitDao recordLimitDao;

    /**
     * Instantiates a new record limit service impl.
     *
     **/
    public RecordLimitServiceImpl(){
        super();
    }
    
    /**
     * Sets the record limit dao.
     * 
     * @param recordLimitDao the record limit dao.
     */
    public void setRecordLimitDao(RecordLimitDao recordLimitDao) {
        this.recordLimitDao = recordLimitDao;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.MiscService#searchMiscValue(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public MiscellaneousDomain searchRecordLimit(MiscellaneousDomain miscDomain) {
        MiscellaneousDomain miscResult = new MiscellaneousDomain();
        miscResult = recordLimitDao.searchRecordLimit(miscDomain);
        return miscResult;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.MiscService#searchRecordLimitPerPage(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public MiscellaneousDomain searchRecordLimitPerPage(MiscellaneousDomain miscDomain) {
        MiscellaneousDomain miscResult = new MiscellaneousDomain();
        miscResult = recordLimitDao.searchRecordLimitPerPage(miscDomain);
        return miscResult;
    }

}