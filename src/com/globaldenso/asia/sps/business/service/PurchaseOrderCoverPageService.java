/*
 * ModifyDate Development company Describe 
 * 2014/09/01 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;

/**
 * <p>The Interface purchase Order Cover Page Dao.</p>
 * <p>Service for purchase Order Cover Page about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchPOCoverPageReport</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public interface PurchaseOrderCoverPageService {
    /**
     * 
     * <p>Search P/O Cover page report.</p>
     *
     * @param input criteria
     * @return List of PurchaseOrderCoverPageReportDomain
     */
    public List<PurchaseOrderCoverPageReportDomain> searchPoCoverPageReport(
        PurchaseOrderInformationDomain input);
}
