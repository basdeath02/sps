/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.dao.DensoSupplierRelationDao;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * <p>The class DensoSupplierRelationServiceImpl implements DensoSupplierRelationService.</p>
 * <p>Service for DENSO and Supplier company relation.</p>
 * <ul>
 * <li>Method search  : searchDENSOSupplierRelation</li>
 * </ul>
 *
 * @author CSI
 */
public class DensoSupplierRelationServiceImpl implements
        DensoSupplierRelationService {
    
    /** The DENSO Supplier Relation Dao. */
    DensoSupplierRelationDao densoSupplierRelationDao;
    
    /** The default constructor. */
    public DensoSupplierRelationServiceImpl() {
        super();
    }
    
    /**
     * Setter method for densoSupplierRelationDao.
     * 
     * @param densoSupplierRelationDao DENSO Supplier Relation Dao to set
     * */
    public void setDensoSupplierRelationDao(DensoSupplierRelationDao densoSupplierRelationDao) {
        this.densoSupplierRelationDao = densoSupplierRelationDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoSupplierRelationService#searchDENSOSupplierRelation(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public List<DensoSupplierRelationDomain> searchDensoSupplierRelation(
        DataScopeControlDomain dataScopeControlDemain)
    {
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(dataScopeControlDemain.getUserType())) {
            return densoSupplierRelationDao.searchRelationByRoleDenso(dataScopeControlDemain);
        } else {
            return densoSupplierRelationDao.searchRelationByRoleSupplier(dataScopeControlDemain);
        }
    }

}
