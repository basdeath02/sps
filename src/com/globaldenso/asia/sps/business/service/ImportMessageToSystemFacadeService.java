/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Parichat        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.AnnounceMessageDomain;


/**
 * <p>The Interface MessageService.</p>
 * <p>This job read CSV file and upload into SPS system.</p>
 * @author CSI
 * @version 1.00
 */
public interface ImportMessageToSystemFacadeService {
  
    /**
     * <p>Search File Configuration method.</p>
     *
     * @param spsMCompanyDensoCriteriaDomain spsMCompanyDensoCriteriaDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<SpsMCompanyDensoDomain> searchFileConfiguration(SpsMCompanyDensoCriteriaDomain 
        spsMCompanyDensoCriteriaDomain) throws ApplicationException;
    
    /**
     * <p>Delete Expire Message method.</p>
     *
     * @param spsMAnnounceMessageCriteriaDomain spsMAnnounceMessageCriteriaDomain
     * @return int
     * @throws ApplicationException ApplicationException
     */
    public int deleteExpireMessage(SpsMAnnounceMessageCriteriaDomain 
        spsMAnnounceMessageCriteriaDomain) throws ApplicationException;
    
    /**
     * <p>Create Announcement Message method.</p>
     *
     * @param announceMessageDomainsList announceMessageDomainsList
     * @throws ApplicationException ApplicationException
     */
    public void createAnnouncementMessage(List<AnnounceMessageDomain> announceMessageDomainsList) 
        throws ApplicationException;
    
    /**
     * <p>Search Temp File Backup method.</p>
     *
     * @param spsTmpFileBackupCriteriaDomain spsTmpFileBackupCriteriaDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<SpsTmpFileBackupDomain> searchTempFileBackup(SpsTmpFileBackupCriteriaDomain 
        spsTmpFileBackupCriteriaDomain) 
        throws ApplicationException;
    
    /**
     * <p>Validate Message method.</p>
     *
     * @param message message
     * @param messageCheck messageCheck
     * @return List
     */
    public List<String> validateMessage(List<String> message, String[] messageCheck);
    
    /**
     * <p>Delete Temp File Backup Expire method.</p>
     *
     * @param spsTmpFileBackupCriteriaDomain spsTmpFileBackupCriteriaDomain
     * @return int
     * @throws ApplicationException ApplicationException
     */
    public int deleteTempFileBackupExpire(SpsTmpFileBackupCriteriaDomain 
        spsTmpFileBackupCriteriaDomain) 
        throws ApplicationException;
}