/*
 * ModifyDate Development company     Describe 
 * 2014/06/15 CSI Phakaporn           Create
 * 2016/02/10 CSI Akat                [IN049]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.UserDensoDetailDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.UserDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>The Class DensoUserRegistrationFacadeServiceImpl.</p>
 * <p>Manage data of DENSO User.</p>
 * <ul>
 * <li>Method search  : searchInitialRegister</li>
 * <li>Method search  : searchInitialEdit</li>
 * <li>Method create  : transactRegisterNewUserDenso</li>
 * <li>Method update  : transactRegisterExistUserDenso</li>
 * <li>Method update  : searchSelectedCompanyDenso</li>
 * <li>Method update  : searchSelectedPlantDenso</li>
 * <li>Method update  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public class DensoUserRegistrationFacadeServiceImpl implements DensoUserRegistrationFacadeService {
    
    /** The user service. */
    private UserService userService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The DENSO Company Service. */
    private CompanyDensoService companyDensoService;
    
    /** The SPS M User Service. */
    private SpsMUserService spsMUserService;
    
    /** The User DENSO Service. */
    private SpsMUserDensoService spsMUserDensoService;
    
    /** The SPS M Company DENSO Service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The Plant DENSO service. */
    private PlantDensoService plantDensoService;
    
    /** The Plant DENSO service. */
    private SpsMPlantDensoService spsMPlantDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new DensoUserRegistrationFacade service impl.
     */
    public DensoUserRegistrationFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the user service.
     * 
     * @param userService the new user service
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    /**
     * Set the DENSO Supplier Relation service.
     * 
     * @param densoSupplierRelationService the DENSO Supplier Relation service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    /**
     * Set the company DENSO service.
     * 
     * @param companyDensoService the company DENSO service.
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    /**
     * Set the user service.
     * 
     * @param spsMUserService the user service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    /**
     * Set the company DENSO service.
     * 
     * @param spsMCompanyDensoService the company DENSO service.
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * Sets The Plant DENSO Service.
     * 
     * @param plantDensoService the new Plant DENSO Service.
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }
    /**
     * Sets The User DENSO Service.
     * 
     * @param spsMUserDensoService the new User DENSO Service.
     */
    public void setSpsMUserDensoService(SpsMUserDensoService spsMUserDensoService) {
        this.spsMUserDensoService = spsMUserDensoService;
    }
    /**
     * Sets The Plant DENSO Service.
     * 
     * @param spsMPlantDensoService the new Plant DENSO Service.
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#initialRegister
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public DensoUserRegistrationDomain searchInitialRegister(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException
    {
        Locale locale = dataScopeControlDomain.getLocale();
        List<CompanyDensoDomain> companyDensoDomainList  = new ArrayList<CompanyDensoDomain>();
        DensoUserRegistrationDomain result = new DensoUserRegistrationDomain();
        List<PlantDensoDomain> plantDensoList = new ArrayList<PlantDensoDomain>();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationDomainList);
        if(Constants.ZERO < densoSupplierRelationDomainList.size()){
            result.setDataScopeControlDomain(dataScopeControlDomain);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        /*Get DENSO company information*/
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        companyDensoDomainList = companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == plantDensoWithScope || companyDensoDomainList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }else{
            result.setCompanyDensoList(companyDensoDomainList);
        }
        /*Get list of department name  */
        List<SpsMUserDomain> departmentList = userService.searchDepartmentName();
        if(null != departmentList && Constants.ZERO < departmentList.size()){
            result.setDepartmentList(departmentList);
        }
        
        /*Get list of DENSO plant by DENSO company code*/
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null == plantDensoList || plantDensoList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }else{
            result.setPlantDensoList(plantDensoList);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#initialEdit
     * (com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain)
     */
    public DensoUserRegistrationDomain searchInitialEdit(DensoUserRegistrationDomain 
        densoUserRegistrationDomain) throws ApplicationException
    {
        Locale locale = densoUserRegistrationDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain = 
            densoUserRegistrationDomain.getDataScopeControlDomain();
        DensoUserRegistrationDomain result = new DensoUserRegistrationDomain();
        List<PlantDensoDomain> plantDensoList = new ArrayList<PlantDensoDomain>();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationDomainList);
        if(Constants.ZERO < densoSupplierRelationDomainList.size()){
            result.setDataScopeControlDomain(dataScopeControlDomain);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /*Get DENSO user information*/
        UserDomain userDomain
            = densoUserRegistrationDomain.getUserDensoDetailDomain().getUserDomain();
        userDomain.setDscId(userDomain.getDscId());
        userDomain.setLocale(densoUserRegistrationDomain.getLocale());
        UserDensoDetailDomain densoUserDetailDomain = this.searchDensoUserInformation(userDomain);
        result.setUserDensoDetailDomain(densoUserDetailDomain);
        
        /*Convert last update date time*/
        int pattern = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Date lastUpdateDatetimeUser = new Date(densoUserDetailDomain.getSpsMUserDomain()
            .getLastUpdateDatetime().getTime());
        Date lastUpdateDatetimeDenso = new Date(densoUserDetailDomain
            .getSpsMUserDensoDomain().getLastUpdateDatetime().getTime());
        result.setUpdateDatetime(DateUtil.format(lastUpdateDatetimeUser, pattern));
        result.setUpdateDatetimeDenso(DateUtil.format(lastUpdateDatetimeDenso, pattern));
        
        /*Get DENSO company information*/
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoList = companyDensoService
            .searchCompanyDenso(plantDensoWithScope);
        if(null == companyDensoList || companyDensoList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031, 
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }else{
            result.setCompanyDensoList(companyDensoList);
        }
        
        /*Get list of DENSO plant by DENSO company code*/
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null == plantDensoList || plantDensoList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }else{
            result.setPlantDensoList(plantDensoList);
        }
        
        /*Get list of department name  */
        List<SpsMUserDomain> departmentList = userService.searchDepartmentName();
        if(null != departmentList && Constants.ZERO < departmentList.size()){
            result.setDepartmentList(departmentList);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#transactRegisterNewDensoUser
     * (com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain)
     */
    public DensoUserRegistrationDomain transactRegisterNewUserDenso(DensoUserRegistrationDomain 
        densoUserRegistrationDomain) throws ApplicationException
    {
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = densoUserRegistrationDomain.getLocale(); 
        try{
            /*Validate all input value*/
            errorMessageList = validateInputValue(densoUserRegistrationDomain);
            
            /*Throw list of error message.*/
            if(Constants.ZERO < errorMessageList.size()){
                densoUserRegistrationDomain.setErrorMessageList(errorMessageList);
                return densoUserRegistrationDomain;
            }
            
            /*Check exist DSC ID in master data*/
            if(!StringUtil.checkNullOrEmpty(densoUserRegistrationDomain.getUserDomain().getDscId()))
            {
                SpsMUserCriteriaDomain criteriaDomain = new SpsMUserCriteriaDomain();
                criteriaDomain.setDscId(densoUserRegistrationDomain.getUserDomain().getDscId());
                SpsMUserDomain userDomain = spsMUserService.searchByKey(criteriaDomain);
                if(null != userDomain){
                    //1. User DSC ID was found in master data and is active.
                    if(Constants.IS_ACTIVE.equals(userDomain.getIsActive())){
                        MessageUtil.throwsApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0032);
                    }else{
                        //2. User DSC ID was found in master data and is not active.
                        /*Update user information to the system from SpsMUserService*/
                        SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                        spsMUserDomain.setDscId(densoUserRegistrationDomain.getUserDomain()
                            .getDscId());
                        spsMUserDomain.setFirstName(densoUserRegistrationDomain.getUserDomain()
                            .getFirstName());
                        spsMUserDomain.setMiddleName(densoUserRegistrationDomain.getUserDomain()
                            .getMiddleName());
                        spsMUserDomain.setLastName(densoUserRegistrationDomain.getUserDomain()
                            .getLastName());
                        spsMUserDomain.setDepartmentName(densoUserRegistrationDomain.getUserDomain()
                            .getDepartmentName());
                        spsMUserDomain.setEmail(densoUserRegistrationDomain.getUserDomain()
                            .getEmail());
                        spsMUserDomain.setTelephone(densoUserRegistrationDomain.getUserDomain()
                            .getTelephone());
                        spsMUserDomain.setUserType(Constants.STR_D);
                        spsMUserDomain.setIsActive(densoUserRegistrationDomain.getUserDomain()
                            .getIsActive());
                        spsMUserDomain.setCreateDscId(densoUserRegistrationDomain.getUserDomain()
                            .getCreateUser());
                        spsMUserDomain.setCreateDatetime(commonService.searchSysDate());
                        spsMUserDomain.setLastUpdateDscId(densoUserRegistrationDomain
                            .getUserDomain().getUpdateUser());
                        spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
                        
                        criteriaDomain = new SpsMUserCriteriaDomain();
                        criteriaDomain.setDscId(densoUserRegistrationDomain.getUserDomain()
                            .getDscId());
                        
                        int updateRecord = spsMUserService.updateByCondition(spsMUserDomain, 
                            criteriaDomain);
                        if(updateRecord <= Constants.ZERO){
                            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E6_0034,  
                                SupplierPortalConstant.LBL_UPDATE_USER_INFO);
                        }
                        
                        /*Update DENSO user information to the system*/
                        SpsMUserDensoDomain spsMUserDensoDomain = new SpsMUserDensoDomain();
                        spsMUserDensoDomain.setDscId(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDscId());
                        spsMUserDensoDomain.setEmployeeCd(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmployeeCd());
                        spsMUserDensoDomain.setDCd(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDCd());
                        spsMUserDensoDomain.setDPcd(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDPcd());
                        spsMUserDensoDomain.setEmlCreateCancelAsnFlag(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                            .getEmlCreateCancelAsnFlag());
                        spsMUserDensoDomain.setEmlReviseAsnFlag(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                            .getEmlReviseAsnFlag());
                        spsMUserDensoDomain.setEmlCreateInvoiceFlag(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                            .getEmlCreateInvoiceFlag());
                        
                        // Start : [IN049] Change column name
                        //spsMUserDensoDomain.setEmlAbnormalTransfer1Flag(densoUserRegistrationDomain
                        //    .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        //    .getEmlAbnormalTransfer1Flag());
                        //spsMUserDensoDomain.setEmlAbnormalTransfer2Flag(densoUserRegistrationDomain
                        //    .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        //    .getEmlAbnormalTransfer2Flag());
                        spsMUserDensoDomain.setEmlPedpoDoalcasnUnmatpnFlg(
                            densoUserRegistrationDomain.getUserDensoDetailDomain()
                                .getSpsMUserDensoDomain().getEmlPedpoDoalcasnUnmatpnFlg());
                        spsMUserDensoDomain.setEmlAbnormalTransferFlg(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                            .getEmlAbnormalTransferFlg());
                        // End : [IN049] Change column name
                        
                        spsMUserDensoDomain.setCreateDscId(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain().getCreateDscId());
                        spsMUserDensoDomain.setCreateDatetime(commonService.searchSysDate());
                        spsMUserDensoDomain.setLastUpdateDscId(densoUserRegistrationDomain
                            .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                            .getLastUpdateDscId());
                        spsMUserDensoDomain.setLastUpdateDatetime(commonService.searchSysDate());
                        
                        SpsMUserDensoCriteriaDomain userDensoCriteriaDomain = 
                            new SpsMUserDensoCriteriaDomain();
                        userDensoCriteriaDomain.setDscId(densoUserRegistrationDomain.getUserDomain()
                            .getDscId());
                        int updateDenso = spsMUserDensoService.updateByCondition(
                            spsMUserDensoDomain, userDensoCriteriaDomain);
                        if(updateDenso <= Constants.ZERO){
                            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_E6_0034,  
                                SupplierPortalConstant.LBL_UPDATE_DENSO_USER_INFO);
                        }
                    }
                }else{
                    //3. User DSC ID not found in master data.
                    /*Create new user information to the system from SpsMUserService*/
                    SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
                    spsMUserDomain.setDscId(densoUserRegistrationDomain.getUserDomain().getDscId());
                    spsMUserDomain.setFirstName(densoUserRegistrationDomain.getUserDomain()
                        .getFirstName());
                    spsMUserDomain.setMiddleName(densoUserRegistrationDomain.getUserDomain()
                        .getMiddleName());
                    spsMUserDomain.setLastName(densoUserRegistrationDomain.getUserDomain()
                        .getLastName());
                    spsMUserDomain.setDepartmentName(densoUserRegistrationDomain.getUserDomain()
                        .getDepartmentName());
                    spsMUserDomain.setEmail(densoUserRegistrationDomain.getUserDomain().getEmail());
                    spsMUserDomain.setTelephone(densoUserRegistrationDomain.getUserDomain()
                        .getTelephone());
                    spsMUserDomain.setUserType(Constants.STR_D);
                    spsMUserDomain.setIsActive(densoUserRegistrationDomain.getUserDomain()
                        .getIsActive());
                    spsMUserDomain.setCreateDscId(densoUserRegistrationDomain.getUserDomain()
                        .getCreateUser());
                    spsMUserDomain.setCreateDatetime(commonService.searchSysDate());
                    spsMUserDomain.setLastUpdateDscId(densoUserRegistrationDomain.getUserDomain()
                        .getUpdateUser());
                    spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    spsMUserService.create(spsMUserDomain);
                    
                    /*Create new DENSO user information to the system*/
                    SpsMUserDensoDomain spsMUserDensoDomain = new SpsMUserDensoDomain();
                    spsMUserDensoDomain.setDscId(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDscId());
                    spsMUserDensoDomain.setEmployeeCd(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmployeeCd());
                    spsMUserDensoDomain.setDCd(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDCd());
                    spsMUserDensoDomain.setDPcd(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDPcd());
                    spsMUserDensoDomain.setEmlCreateCancelAsnFlag(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        .getEmlCreateCancelAsnFlag());
                    spsMUserDensoDomain.setEmlReviseAsnFlag(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        .getEmlReviseAsnFlag());
                    spsMUserDensoDomain.setEmlCreateInvoiceFlag(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        .getEmlCreateInvoiceFlag());

                    // Start : [IN049] Change column name
                    //spsMUserDensoDomain.setEmlAbnormalTransfer1Flag(densoUserRegistrationDomain
                    //    .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                    //    .getEmlAbnormalTransfer1Flag());
                    //spsMUserDensoDomain.setEmlAbnormalTransfer2Flag(densoUserRegistrationDomain
                    //    .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                    //    .getEmlAbnormalTransfer2Flag());
                    spsMUserDensoDomain.setEmlPedpoDoalcasnUnmatpnFlg(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        .getEmlPedpoDoalcasnUnmatpnFlg());
                    spsMUserDensoDomain.setEmlAbnormalTransferFlg(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                        .getEmlAbnormalTransferFlg());
                    // End : [IN049] Change column name
                    
                    spsMUserDensoDomain.setCreateDscId(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain().getCreateDscId());
                    spsMUserDensoDomain.setCreateDatetime(commonService.searchSysDate());
                    spsMUserDensoDomain.setLastUpdateDscId(densoUserRegistrationDomain
                        .getUserDensoDetailDomain().getSpsMUserDensoDomain().getLastUpdateDscId());
                    spsMUserDensoDomain.setLastUpdateDatetime(commonService.searchSysDate());
                    spsMUserDensoService.create(spsMUserDensoDomain);
                }
            }
            densoUserRegistrationDomain.setIsActive(Constants.IS_ACTIVE);
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return densoUserRegistrationDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#transactRegisterNewDensoUser
     * (com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain)
     */
    public DensoUserRegistrationDomain transactRegisterExistUserDenso(DensoUserRegistrationDomain 
        densoUserRegistrationDomain) throws ApplicationException
    {
        Locale locale = densoUserRegistrationDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        String message = new String();
        /*Validate all input value*/
        errorMessageList = validateInputValue(densoUserRegistrationDomain);
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(
            densoUserRegistrationDomain.getUpdateDatetime(), patternTimestamp);
        Timestamp lastUpdateDatetimeDenso = DateUtil.parseToTimestamp(
            densoUserRegistrationDomain.getUpdateDatetimeDenso(), patternTimestamp);
        try{
            /*Check existing DSC ID in master data*/
            if(!StringUtil.checkNullOrEmpty(densoUserRegistrationDomain.getUserDomain().getDscId()))
            {
                SpsMUserCriteriaDomain criteriaDomain = new SpsMUserCriteriaDomain();
                criteriaDomain.setDscId(densoUserRegistrationDomain.getUserDomain().getDscId());
                int countUser = spsMUserService.searchCount(criteriaDomain);
                if(countUser < Constants.ZERO ){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
                            MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_CURRENT_USER)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                }
            }
            
            /*Throw list of error message.*/
            if(Constants.ZERO < errorMessageList.size()){
                densoUserRegistrationDomain.setErrorMessageList(errorMessageList);
                return densoUserRegistrationDomain;
            }
            
            /*Update new user information to the system from SpsMUserService*/
            SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
            spsMUserDomain.setDscId(densoUserRegistrationDomain.getUserDomain().getDscId());
            spsMUserDomain.setFirstName(densoUserRegistrationDomain.getUserDomain()
                .getFirstName());
            spsMUserDomain.setMiddleName(densoUserRegistrationDomain.getUserDomain()
                .getMiddleName());
            spsMUserDomain.setLastName(densoUserRegistrationDomain.getUserDomain()
                .getLastName());
            spsMUserDomain.setDepartmentName(densoUserRegistrationDomain.getUserDomain()
                .getDepartmentName());
            spsMUserDomain.setEmail(densoUserRegistrationDomain.getUserDomain().getEmail());
            spsMUserDomain.setTelephone(densoUserRegistrationDomain.getUserDomain()
                .getTelephone());
            spsMUserDomain.setUserType(Constants.STR_D);
            spsMUserDomain.setIsActive(densoUserRegistrationDomain.getUserDomain()
                .getIsActive());
            spsMUserDomain.setLastUpdateDscId(densoUserRegistrationDomain.getUserDomain()
                .getUpdateUser());
            spsMUserDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            SpsMUserCriteriaDomain spsMUserCriteriaDomain = new SpsMUserCriteriaDomain();
            spsMUserCriteriaDomain.setDscId(densoUserRegistrationDomain.getUserDomain()
                .getDscId());
            spsMUserCriteriaDomain.setLastUpdateDatetime(lastUpdateDatetime);
            
            int countUpdateUser = spsMUserService.updateByCondition(spsMUserDomain, 
                spsMUserCriteriaDomain);
            if(countUpdateUser <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034,  
                    SupplierPortalConstant.LBL_UPDATE_USER_INFO);
            }
            
            /*Create new DENSO user information to the system*/
            SpsMUserDensoDomain spsMUserDensoDomain = new SpsMUserDensoDomain();
            spsMUserDensoDomain.setDscId(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDscId());
            spsMUserDensoDomain.setEmployeeCd(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmployeeCd());
            spsMUserDensoDomain.setDCd(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDCd());
            spsMUserDensoDomain.setDPcd(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDPcd());
            spsMUserDensoDomain.setEmlCreateCancelAsnFlag(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmlCreateCancelAsnFlag());
            spsMUserDensoDomain.setEmlReviseAsnFlag(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmlReviseAsnFlag());
            spsMUserDensoDomain.setEmlCreateInvoiceFlag(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmlCreateInvoiceFlag());
            
            // Start : [IN049] Change column name
            //spsMUserDensoDomain.setEmlAbnormalTransfer1Flag(densoUserRegistrationDomain
            //    .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmlAbnormalTransfer1Flag());
            //spsMUserDensoDomain.setEmlAbnormalTransfer2Flag(densoUserRegistrationDomain
            //    .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmlAbnormalTransfer2Flag());
            spsMUserDensoDomain.setEmlPedpoDoalcasnUnmatpnFlg(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain()
                .getEmlPedpoDoalcasnUnmatpnFlg());
            spsMUserDensoDomain.setEmlAbnormalTransferFlg(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getEmlAbnormalTransferFlg());
            // End : [IN049] Change column name
            
            spsMUserDensoDomain.setLastUpdateDscId(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getLastUpdateDscId());
            spsMUserDensoDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            SpsMUserDensoCriteriaDomain spsMUserDensoCriteriaDomain = 
                new SpsMUserDensoCriteriaDomain();
            spsMUserDensoCriteriaDomain.setDscId(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDscId());
            spsMUserDensoCriteriaDomain.setLastUpdateDatetime(lastUpdateDatetimeDenso);
            
            int countUpdateDenso = spsMUserDensoService.updateByCondition(
                spsMUserDensoDomain, spsMUserDensoCriteriaDomain);
            if(countUpdateDenso <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034,  
                    SupplierPortalConstant.LBL_UPDATE_DENSO_USER_INFO);
            }
            densoUserRegistrationDomain.setIsActive(Constants.IS_ACTIVE);
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return densoUserRegistrationDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#searchSelectedCompanyDenso
     * (com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        DensoUserRegistrationDomain densoUserRegistrationDomain) throws ApplicationException
    {
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = densoUserRegistrationDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain = 
            densoUserRegistrationDomain.getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
            densoUserRegistrationDomain.getDCd());
        plantDensoWithScopeDomain.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(Constants.ZERO == plantDensoList.size()){
            plantDensoList = new ArrayList<PlantDensoDomain>();
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScope.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if(dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == result || Constants.ZERO == result.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /** Private method for get DENSO User Information.
     * 
     * @param userDomain the UserDomain.
     * @return DensoUserDetailDomain
     * @throws ApplicationException the ApplicationException
     */
    private UserDensoDetailDomain searchDensoUserInformation(UserDomain userDomain)
        throws ApplicationException
    {
        Locale locale = userDomain.getLocale();
        UserDensoDetailDomain densoUserDetailDomain = new UserDensoDetailDomain();
        SpsMUserCriteriaDomain spsMUserCriteriaDomain = new SpsMUserCriteriaDomain();
        SpsMUserDensoCriteriaDomain criteriaDenso = new SpsMUserDensoCriteriaDomain();
        List<SpsMUserDomain> spsMUserList = new ArrayList<SpsMUserDomain>();
        List<SpsMUserDensoDomain> userDensoList = new ArrayList<SpsMUserDensoDomain>();
        
        /*Get user information*/
        spsMUserCriteriaDomain.setDscId(userDomain.getDscId());
        spsMUserCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        spsMUserList = spsMUserService.searchByCondition(spsMUserCriteriaDomain);
        if(Constants.ZERO < spsMUserList.size()){
            for(SpsMUserDomain userResult : spsMUserList){
                densoUserDetailDomain.setSpsMUserDomain(userResult);
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,  
                SupplierPortalConstant.LBL_USER);
        }
        
        /*Get DENSO user information*/
        criteriaDenso.setDscId(userDomain.getDscId());
        userDensoList = spsMUserDensoService.searchByCondition(criteriaDenso);
        if(Constants.ZERO < userDensoList.size()){
            for(SpsMUserDensoDomain densoUserResult : userDensoList){
                densoUserDetailDomain.setSpsMUserDensoDomain(densoUserResult);
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_USER);
        }
        return densoUserDetailDomain;
    }
    
    /**
     * 
     * <p>Validate input value.</p>
     *
     * @param densoUserRegistrationDomain that keep all input from the screen.
     * @return the List of validation error message
     * @throws ApplicationException the ApplicationException.
     */
    private List<ApplicationMessageDomain> validateInputValue(DensoUserRegistrationDomain 
        densoUserRegistrationDomain)throws ApplicationException
    {
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = densoUserRegistrationDomain.getLocale();
        String message = new String();
        
        /*Validate criteria input for search*/
        try{
            UserDomain userDomain = densoUserRegistrationDomain.getUserDomain();
            SpsMUserDensoDomain userDenso = densoUserRegistrationDomain.getUserDensoDetailDomain()
                .getSpsMUserDensoDomain();
            if(StringUtil.checkNullOrEmpty(userDomain.getDscId())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_DSC_ID)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(userDomain.getFirstName())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_FIRST_NAME)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(userDomain.getLastName())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_LAST_NAME)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(userDomain.getEmail())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_EMAIL)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(userDenso.getEmployeeCd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_EMPLOYEE_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(userDenso.getDCd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_DENSO_COMPANY_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(StringUtil.checkNullOrEmpty(userDenso.getDPcd())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0007, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_DENSO_PLANT_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            if(!StringUtil.checkNullOrEmpty(densoUserRegistrationDomain.getDscId())){
                if(Constants.MAX_DSC_ID_LENGTH < densoUserRegistrationDomain.getDscId().trim()
                    .length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_DSC_ID),
                                String.valueOf(Constants.MAX_DSC_ID_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }else{
                    /**Changed DSC ID Validation refer to 23/10/2014 12:00 am.*/
                    /*Boolean isNumber = userDomain.getDscId().trim().matches(
                        Constants.REGX_NUMBER_FORMAT);
                    if(!isNumber){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_DSC_ID)});
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            message));
                    }*/
                }
            }
            if(!StringUtil.checkNullOrEmpty(userDenso.getEmployeeCd())){
                if(Constants.MAX_EMPLOYEE_CD_LENGTH < userDenso.getEmployeeCd().trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_EMPLOYEE_CODE),
                                String.valueOf(Constants.MAX_EMPLOYEE_CD_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
                /*else{
                    Boolean isNumber = densoUserRegistrationDomain.getEmployeeCode().trim().matches(
                        Constants.REGX_NUMBER_FORMAT);
                    if(!isNumber){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_DSC_ID)});
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            message));
                    }
                }*/
            }
            if(!StringUtil.checkNullOrEmpty(userDomain.getFirstName())){
                if(Constants.MAX_FIRST_NAME_LENGTH < userDomain.getFirstName().trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_FIRST_NAME),
                                String.valueOf(Constants.MAX_FIRST_NAME_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                }
                /*else{
                    Boolean isLetter = userDomain.getFirstName().trim().matches(
                        Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_FIRST_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            message));
                    }
                }*/
            }
            if(!StringUtil.checkNullOrEmpty(userDomain.getMiddleName())){
                if(Constants.MAX_MIDDLE_NAME_LENGTH < userDomain.getMiddleName().trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_MIDDLE_NAME),
                                String.valueOf(Constants.MAX_MIDDLE_NAME_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                        message));
                }
                /*else{
                    Boolean isLetter = userDomain.getMiddleName().trim().matches(
                        Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_MIDDLE_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            message));
                    }
                }*/
            }
            if(!StringUtil.checkNullOrEmpty(userDomain.getLastName())){
                if(Constants.MAX_LAST_NAME_LENGTH < userDomain.getLastName().trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_LAST_NAME),
                                String.valueOf(Constants.MAX_LAST_NAME_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
                /*else{
                    Boolean isLetter = userDomain.getLastName().trim().matches(
                        Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_LAST_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            message));
                    }
                }*/
            }
            if(!StringUtil.checkNullOrEmpty(userDomain.getEmail())){
                if(Constants.MAX_EMAIL_LENGTH < userDomain.getEmail().trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_EMAIL),
                                String.valueOf(Constants.MAX_EMAIL_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
            }
            if(!StringUtil.checkNullOrEmpty(userDomain.getTelephone())){
                if(Constants.MAX_TELEPHONE_LENGTH < userDomain.getTelephone().trim().length()){
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_TELEPHONE),
                                String.valueOf(Constants.MAX_TELEPHONE_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
            }
            if(!StringUtil.checkNullOrEmpty(userDomain.getDepartmentName())){
                if(Constants.MAX_DEPARTMENT_LENGTH < userDomain.getDepartmentName().trim().length())
                {
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0030,  new String[]{
                            MessageUtil.getLabel(
                                locale, SupplierPortalConstant.LBL_DEPARTMENT_NAME),
                                String.valueOf(Constants.MAX_DEPARTMENT_LENGTH)});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
                /*else{
                    Boolean isLetter = userDomain.getDepartmentName().trim()
                        .matches(Constants.REGX_LETTER_FORMAT);
                    if(!isLetter){
                        message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0023,  new String[]{
                                MessageUtil.getLabel(locale, 
                                    SupplierPortalConstant.LBL_DEPARTMENT_NAME)});
                        errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                            message));
                    }
                }*/
            }
            /**** Validate email format that should "a@b.com" and support "," separate*/
            if(!StringUtil.checkNullOrEmpty(userDomain.getEmail())){
                String[] strEmail = StringUtils.splitPreserveAllTokens(userDomain.getEmail(), 
                    Constants.SYMBOL_COMMA);
                List<String> emailInvalidList = new ArrayList<String>();
                int count = 0;
                for(int i = Constants.ZERO; i < strEmail.length; i++){
                    boolean isEmail = strEmail[i].matches(Constants.REGX_EMAIL_FORMAT);
                    if(!isEmail){
                        count = i + Constants.ONE;
                        emailInvalidList.add(String.valueOf(count));
                    }
                }
                if(Constants.ZERO < emailInvalidList.size()){
                    String result = StringUtil.convertListToStringCommaSeperate(emailInvalidList);
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0041,  new String[]{result});
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        message));
                }
            }
            
            /*Check existing DENSO company code*/
            SpsMCompanyDensoCriteriaDomain companyDensoCriteriaDomain = 
                new SpsMCompanyDensoCriteriaDomain();
            companyDensoCriteriaDomain.setDCd(densoUserRegistrationDomain
                .getUserDensoDetailDomain().getSpsMUserDensoDomain().getDCd());
            int countDensoCompany = spsMCompanyDensoService.searchCount(companyDensoCriteriaDomain);
            if(countDensoCompany <= Constants.ZERO){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,  new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_DENSO_OWNER_COMPANY)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
            
            /*Check existing DENSO plant code in master data*/
            SpsMPlantDensoCriteriaDomain criteriaPlantDenso = new SpsMPlantDensoCriteriaDomain();
            criteriaPlantDenso.setDCd(densoUserRegistrationDomain.getUserDensoDetailDomain()
                .getSpsMUserDensoDomain().getDCd());
            criteriaPlantDenso.setDPcd(densoUserRegistrationDomain.getUserDensoDetailDomain()
                .getSpsMUserDensoDomain().getDPcd());
            int countPlantDenso = spsMPlantDensoService.searchCount(
                criteriaPlantDenso);
            if(countPlantDenso < Constants.ZERO){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,  new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_DENSO_PLANT_CODE)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    message));
            }
        }catch(Exception ex){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return errorMessageList;
    }
}