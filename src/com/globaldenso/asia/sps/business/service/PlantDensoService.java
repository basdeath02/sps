/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface PlantDensoService.</p>
 * <p>Service for Plant DENSO about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchDensoPlant</li>
 * </ul>
 *
 * @author CSI
 */
public interface PlantDensoService {

    /**
     * Search DENSO Plant by user role (or relate to user role).
     * @param plantDensoWithScopeDomain Plant DENSO with scope
     * @return List Of Plant DENSO
     * */
    public List<PlantDensoDomain> searchPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search DENSO Plant Information by user role (or relate to user role).
     * @param plantDensoWithScopeDomain Plant DENSO with scope
     * @return List Of Plant DENSO
     * */
    public List<PlantDensoDomain> searchPlantDensoInformation(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain);
    
    /**
     * Search Exist Plant DENSO relate with user role.
     * 
     * @param plantDensoWithScopeDomain plant DENSO with scope
     * @return count of plant DENSO.
     * */
    public Integer searchExistPlantDenso(PlantDensoWithScopeDomain plantDensoWithScopeDomain);
}
