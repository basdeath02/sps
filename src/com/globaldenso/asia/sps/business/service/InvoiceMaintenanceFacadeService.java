/*
 * ModifyDate Development company       Describe 
 * 2014/07/10 CSI Karnrawee             Create
 * 2018/04/18 Netband U.Rungsiwut       Generate invoice PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.io.OutputStream;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain;

/**
 * <p>The Interface InvoiceMaintenanceFacadeService.</p>
 * <p>Facade for InvoiceMaintenanceFacadeService.</p>
 * <ul>
 * <li>Method initial  : searchInitialGroupInvoiceByManual</li>
 * <li>Method initial  : searchInitialGroupInvoiceByCsv</li>
 * <li>Method initial  : searchInitialViewInvoiceMaintenance</li>
 * <li>Method create   : transactRegisterInvoice</li>
 * <li>Method delete   : deleteTmpUploadInvoice</li>
 * <li>Method search   : searchPreviewCoverPage</li>
 * <li>Method search   : searchInvoiceMaintenanceCsv</li>
 * <li>Method search   : searchFileName</li>
 * <li>Method search   : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public interface InvoiceMaintenanceFacadeService {
    
    /**
     * <p>Initial group Invoice by manual.</p>
     * <ul>
     * <li>Initialization of group invoice by manual on Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Maintenance Domain
     * @throws ApplicationException ApplicationException
     */
    public InvoiceMaintenanceDomain searchInitialGroupInvoiceByManual(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws ApplicationException;
    
    /**
     * <p>Initial group Invoice by CSV.</p>
     * <ul>
     * <li>Initialization of group invoice by CSV on Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Maintenance Domain
     * @throws ApplicationException ApplicationException
     */
    public InvoiceMaintenanceDomain searchInitialGroupInvoiceByCsv(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws ApplicationException;
    
    /**
     * <p>Initial view invoice maintenance.</p>
     * <ul>
     * <li>Initialization of view invoice maintenance on Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Maintenance Domain
     * @throws ApplicationException ApplicationException
     */
    public InvoiceMaintenanceDomain searchInitialViewInvoiceMaintenance(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws ApplicationException;
    
    /**
     * <p>Transact register invoice.</p>
     * <ul>
     * <li>Register invoice data form Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Maintenance Domain
     * @throws Exception Exception
     */
    public InvoiceMaintenanceDomain transactRegisterInvoice(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws Exception;
    
    /**
     * <p>Delete tmp upload invoice.</p>
     * <ul>
     * <li>Delete upload invoice data n temporary upload invoice table.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @throws Exception Exception
     */
    public void deleteTmpUploadInvoice(InvoiceMaintenanceDomain invoiceMaintenanceDomain)
        throws Exception;
    
    /**
     * <p>Search preview cover page.</p>
     * <ul>
     * <li>Search cover page data in order to preview report on Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Maintenance Domain
     * @throws Exception Exception
     */
    public InvoiceMaintenanceDomain searchPreviewCoverPage(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws Exception;
    
    /**
     * <p>Search invoice maintenance CSV.</p>
     * <ul>
     * <li>Search invoice maintenance data in order to export CSV file on Invoice Maintenance Screen.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Maintenance Domain
     * @throws ApplicationException ApplicationException
     */
    public InvoiceMaintenanceDomain searchInvoiceMaintenanceCsv(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain) throws ApplicationException;
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search PDF file name by file id in order to export CSV file.</li>
     * </ul>
     * 
     * @param invoiceMaintenance the Invoice Maintenance Domain
     * @return the string
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName (InvoiceMaintenanceDomain invoiceMaintenance) 
        throws ApplicationException;
    
    /**
     * <p>Search Legend Information.</p>
     * <ul>
     * <li>Search PDF file from PDF file id in order to export CSV file.</li>
     * </ul>
     * 
     * @param invoiceMaintenance the Invoice Maintenance Domain
     * @param output the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(InvoiceMaintenanceDomain invoiceMaintenance, OutputStream output)
        throws ApplicationException;
    
    /**
     * <p>Search download cover page report.</p>
     * <ul>
     * <li>Search cover page report from PDF file id in order to export CSV file.</li>
     * </ul>
     * 
     * @param invoiceMaintenance the Invoice Maintenance Domain
     * @param outputStream the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchDownloadCoverPageReport(InvoiceMaintenanceDomain invoiceMaintenance,
        OutputStream outputStream)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
    
    /**
     * Generate Invoice Report Information and return by throw ApplicationException.
     * @param invoiceMaintenanceDomain invoiceMaintenanceDomain
     * @return FileManagementDomain createInvoiceReport
     * @throws ApplicationException contain application message
     * */
    public FileManagementDomain searchGenerateInvoice(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)
        throws ApplicationException;
}