/*
 * ModifyDate Development company     Describe 
 * 2014/08/14 CSI Akat                Create
 * 2016/02/24 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain;

/**
 * Facade for screen WORD004 Supplier Promised Due Submittion.
 * 
 * <ul>
 * <li>Method search    : searchInitial</li>
 * <li>Method search    : findFileName</li>
 * <li>Method search    : searchLegendInfo</li>
 * <li>Method search    : transactNewDue</li>
 * </ul>
 * 
 * @author CSI
 * */
public interface SupplierPromisedDueSubmittionFacadeService {

    /**
     * Search initial data for show screen Supplier Promised Due Submittion.
     * @param supplierPromisedDueSubmittionDomain Supplier Promised Due Submittion Domain
     * @return List of PO Due and list of Pending Reason in Supplier Promised Due Submittion Domain
     * @throws ApplicationException an Application Exception
     * */
    public SupplierPromisedDueSubmittionDomain searchInitial(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException;

    /**
     * Call FileManagementService.serachFileDownload to get file name.
     * 
     * @param supplierPromisedDueSubmittionDomain Supplier Promised Due Submittion Domain
     * @return File Management Domain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException;

    /**
     * Call FileManagementService.serachFileDownload to send Legend File data to client.
     * 
     * @param supplierPromisedDueSubmittionDomain Supplier Promised Due Submittion Domain
     * @param output Output Stream to client
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain,
        OutputStream output) throws ApplicationException;

    /**
     * Add new Propose Due Date and Quantity and update PO Detail status.
     * @param supplierPromisedDueSubmittionDomain Supplier Promised Due Submittion Domain
     * @return list of error message
     * @throws ApplicationException an Application Exception
     * */
    public List<ApplicationMessageDomain> transactNewDue(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
    
    // [IN054] Add new method for register DENSO Reply result
    /**
     * Register DENSO Reply Result to Purchase Order Due.
     * @param supplierPromisedDueSubmittionDomain Supplier Promised Due Submittion Domain
     * @return list of error message
     * @throws ApplicationException an Application Exception
     * */
    public List<ApplicationMessageDomain> transactReplyDue(
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain)
        throws ApplicationException;
    
}
