/*
 * ModifyDate Development company     Describe 
 * 2014/07/29 CSI Chatchai              Create
 * 2017/08/30 Netband U.Rungsiwut       Modify
 * 2018/04/11 Netband U.Rungsiwut       Generate Kanban PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultMasterDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>
 * Kanban Order Information Facade Service class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public interface KanbanOrderInformationFacadeService {

    /**
     * <p>
     * Initial method.
     * </p>
     * 
     * @param dataScopeControlDomain dataScopeControlDomain
     * @return KanbanOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public KanbanOrderInformationDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Kanban Order Information method.
     * </p>
     * 
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return KanbanOrderInformationResultMasterDomain
     * @throws ApplicationException ApplicationException
     */
    public KanbanOrderInformationResultMasterDomain searchKanbanOrderInformation(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>Search Kanban Order Information CSV method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return KanbanOrderInformationResultMasterDomain
     * @throws ApplicationException ApplicationException
     */
    public KanbanOrderInformationResultMasterDomain searchKanbanOrderInformationCsv(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>Find File Name method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return KanbanOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException;

    /**
     * <p>Search Kanban Order Report method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @param output output
     * @return KanbanOrderInformationDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchKanbanOrderReport(
        KanbanOrderInformationDomain kanbanOrderInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * <p>Change Select Supplier Company method.</p>
     *
     * @param plantSupplierWithScopeDomain plantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScope PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
        throws ApplicationException;

    /**
     * <p>Change Select Denso Companye method.</p>
     *
     * @param plantDensoWithScopeDomain plantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>Search Legend Info method.</p>
     *
     * @param kanbanOrderInformationDomain 
     * @param output output
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchLegendInfo(
        KanbanOrderInformationDomain kanbanOrderInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;

    /**
     * <p>
     * Search update DateTime KanbanPdf method.
     * </p>
     * 
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return resultCode
     * @throws ApplicationException ApplicationException
     */
    public Integer updateDateTimeKanbanPdf(KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException;
    
    /**
     * Generate One Way Kanban Tag Report Information and return by throw ApplicationException.
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return InputStream createOneWayKanbanTagReport
     * @throws ApplicationException contain application message
     * */
    public InputStream createOneWayKanbanTagReport(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException;
    
    /**
     * Generate Kanban D/O Report Information and return by throw ApplicationException.
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return FileManagementDomain createOneWayKanbanTagReport
     * @throws ApplicationException contain application message
     * */
    public FileManagementDomain searchGenerateDo(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
        throws ApplicationException;
}
