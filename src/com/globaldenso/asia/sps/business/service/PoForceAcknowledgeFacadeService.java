/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/24 CSI Chatchai        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.PoForceAcknowledgeDomain;

/**
 * <p>Update Acknowledge Flag when Supplier didn’t confirm in time.</p>
 * <p>Facade for POForceAcknowledgeFacadeService.</p>
 * <ul>
 * <li>Method search  : searchForceAcknowledgePO</li>
 * <li>Method search  : updateForceAcknowledgePO</li>
 * </ul>
 *
 * @author CSI
 */
public interface PoForceAcknowledgeFacadeService {
    /**
     * Transact notification abnormal order.
     * Get abnormal order data. method will sent mail notification to supplier. 
     * 
     * @param poCriteria the POForceAcknowledgeDomain
     * @throws ApplicationException ApplicationException
     * @return SpsTPoDomain
     */
    public List<SpsTPoDomain> searchForceAcknowledgePo(
        SpsTPoCriteriaDomain poCriteria) throws ApplicationException;
    
    /**
     * Transact notification abnormal order.
     * Get abnormal order data. method will sent mail notification to supplier. 
     * @throws ApplicationException ApplicationException
     * @param pOForceAcknowledgeDomain the POForceAcknowledgeDomain
     * 
     */
    public void transactForceAcknowledgePo(
        PoForceAcknowledgeDomain pOForceAcknowledgeDomain) throws ApplicationException;
    

}
