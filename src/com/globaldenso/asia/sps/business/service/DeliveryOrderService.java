/*
 * ModifyDate Development company    Describe 
 * 2014/07/22 CSI Parichat           Create
 * 2015/08/24 CSI Akat               [IN012]
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DoCreatedAsnDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;

/**
 * <p>The Interface Delivery Order service.</p>
 * <p>Service for Delivery Order about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchCountAcknowledgedDo</li>
 * <li>Method search  : searchAcknowledgedDo</li>
 * <li>Method search  : searchCountDeliveryOrder</li>
 * <li>Method search  : searchDeliveryOrder</li>
 * <li>Method search  : searchCountDoDetail</li>
 * <li>Method search  : searchDoDetail</li>
 * <li>Method search  : searchCountKanbanOrder</li>
 * <li>Method search  : searchKanbanOrder</li>
 * <li>Method search  : searchDoGroupAsn</li>
 * <li>Method search  : searchCountKanbanOrderHeader</li>
 * <li>Method search  : searchKanbanOrderHeader</li>
 * <li>Method search  : searchCountDeliveryOrderHeader</li>
 * <li>Method search  : searchDeliveryOrderHeader</li>
 * <li>Method search  : searchUrgentOrderForSupplierUser</li>
 * <li>Method search  : searchExistDo</li>
 * <li>Method search  : searchDeliveryOrderReport</li>
 * <li>Method search  : searchKanbanDeliveryOrderReport</li>
 * <li>Method search  : searchPurgingUrgentDeliveryOrder</li>
 * <li>Method search  : searchOneWayKanbanTagReportInformation</li>
 * </ul>
 *
 * @author CSI
 */
public interface DeliveryOrderService {
    
    /**
     * <p>Search count acknowledged do.</p>
     * <ul>
     * <li>Search count acknowledged do information.</li>
     * </ul>
     * 
     * @return the integer
     * @param acknowledgedDOInformationDomain the acknowledged do information domain
     */
    public Integer searchCountAcknowledgedDo(AcknowledgedDoInformationDomain 
        acknowledgedDOInformationDomain);
    
    /**
     * <p>Search acknowledge do information.</p>
     * <ul>
     * <li>Search acknowledge do information.</li>
     * </ul>
     * 
     * @return the list
     * @param acknowledgedDOInformationDomain the acknowledged do information domain
     */
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDOInformationDomain);
    
    /**
     * <p>Search Count Delivery Order.</p>
     *
     * @param deliveryOrderInformationDomain the delivery order information domain
     * @return the Integer
     */
    public Integer searchCountDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Delivery Order.</p>
     *
     * @param deliveryOrderInformationDomain the delivery order information domain
     * @return the list
     */
    public List<DeliveryOrderDetailDomain> searchDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search count do detail.</p>
     * <ul>
     * <li>Search count do detail information.</li>
     * </ul>
     * 
     * @return the integer
     * @param inquiryDoDetailDomain the inquiry do detail domain
     */
    public Integer searchCountDoDetail(InquiryDoDetailDomain inquiryDoDetailDomain);
    
    /**
     * <p>Search do detail.</p>
     * <ul>
     * <li>Search do detail information.</li>
     * </ul>
     * 
     * @return the list
     * @param inquiryDODetailDomain the inquiry do detail domain
     */
    public List<InquiryDoDetailReturnDomain> searchDoDetail(
        InquiryDoDetailDomain inquiryDODetailDomain);
    
    
  
    /**
     * <p>Search Count Kanban Order method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return Integer
     */
    public Integer searchCountKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);

   
    /**
     * <p>Search Kanban Order method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return List
     */
    public List<KanbanOrderInformationResultDomain> searchKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);
    
    /**
     * <p>Search do group asn.</p>
     * <ul>
     * <li>Search delivery order data for grouping asn.</li>
     * </ul>
     * 
     * @return the list
     * @param asnMaintenanceDomain the asn maintenance domain
     */
    public List<AsnMaintenanceReturnDomain> searchDoGroupAsn(
        AsnMaintenanceDomain asnMaintenanceDomain);

    // [IN012] : Not use
    ///**
    // * Search completed ship do.
    // * <p>Search completed ship delivery order information.</p>
    // *  
    // * @param criteriaDomainList the list of asn maintenance domain
    // * @return the list of asn maintenance domain
    // */
    //public List<AsnMaintenanceDomain> searchCompleteShipDo(
    //    List<AsnMaintenanceDomain> criteriaDomainList);
    
    // [IN012] : Create method for check D/O status
    /**
     * Examine D/O Ship Status by search for P/No. Status in D/O and select proper Ship Status.
     * 
     * @param criteriaDomainList the list of SpsTDoDomain
     * @return the list of SpsTDoDomain as result
     * */
    public List<SpsTDoDomain> searchExamineDoStatus(
        List<SpsTDoDomain> criteriaDomainList);
    
    /**
     * <p>Search Count Kanban Order Header method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return Integer
     */
    public Integer searchCountKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);
    
    /**
     * <p>Search Kanban Order Header method.</p>
     *
     * @param kanbanOrderInformationDomain kanbanOrderInformationDomain
     * @return List
     */
    public List<KanbanOrderInformationResultDomain> searchKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain);
    
    /**
     * <p>Search Count Delivery Order Header method.</p>
     *
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return Integer
     */
    public Integer searchCountDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Delivery Order Header method.</p>
     *
     * @param deliveryOrderInformationDomain deliveryOrderInformationDomain
     * @return List
     */
    public List<DeliveryOrderDetailDomain> searchDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Urgent Order For Supplier User method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchUrgentOrderForSupplierUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Exit Do.</p>
     *
     * @param deliveryOrderInformationDomain the delivery order information domain
     * @return the list of delivery order detail domain
     */
    public List<DeliveryOrderDetailDomain> searchExistDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * <p>Search Delivery Order Report method.</p>
     *
     * @param deliveryOrderSubDetailDomain deliveryOrderSubDetailDomain
     * @return List
     * @throws Exception Exception
     */
    public InputStream searchDeliveryOrderReport(
        DeliveryOrderSubDetailDomain deliveryOrderSubDetailDomain)
        throws Exception;
    
    /**
     * <p>Search Kanban Delivery Order Report method.</p>
     *
     * @param kanbanOrderInformationDODetailReturnDomain kanbanOrderInformationDODetailReturnDomain
     * @return List
     * @throws Exception Exception
     */
    public InputStream searchKanbanDeliveryOrderReport(
        KanbanOrderInformationDoDetailReturnDomain kanbanOrderInformationDODetailReturnDomain)
        throws Exception;
    
    /**
     * <p>Search count back order information.</p>
     * <ul>
     * <li>Search count back order information by criteria from search.</li>
     * </ul>
     * 
     * @param backOrderInformationDomain the back order information domain
     * @return the integer
     */
    public Integer searchCountBackOrder(BackOrderInformationDomain backOrderInformationDomain);
    
    /**
     * <p>Search back order information detail.</p>
     * <ul>
     * <li>Search back order information detail by criteria from search.</li>
     * </ul>
     * 
     * @param backOrderInformationDomain the back order information domain
     * @return the list of file upload domain
     */
    public List<BackOrderInformationDomain> searchBackOrder(
        BackOrderInformationDomain backOrderInformationDomain);
    
    /**
     * 
     * <p>Create Delivery Order.</p>
     *
     * @param domain SpsTDoDomain
     * @return BigDecimal doId
     * @throws ApplicationException other error
     */
    public BigDecimal createDeliveryOrder(SpsTDoDomain domain) throws ApplicationException;
    
    /**
     * 
     * <p>Search Exists Delivery Order.</p>
     *
     * @param paramters DeliveryOrderDomain
     * @return List of SpsTDoDomain
     */
    public List<SpsTDoDomain> searchExistDeliveryOrder(DeliveryOrderDomain paramters);
    
    /**
     * 
     * <p>searchUrgentOrder method.</p>
     *
     * @param criteria UrgentOrderDomain
     * @return List of DeliveryOrderDomain
     */
    public List<DeliveryOrderDomain> searchUrgentOrder(UrgentOrderDomain criteria);
    
    /**
     * 
     * <p>Search ASN from DO.</p>
     *
     * @param criteria DeliveryOrderDomain
     * @return List of DoCreatedAsnDomain
     */
    public List<DoCreatedAsnDomain> searchDoCreatedAsn(DeliveryOrderDomain criteria);
    
    /**
     * <p>Search purging delivery order.</p>
     * <ul>
     * <li>Search delivery order for purging.</li>
     * 
     * 
     * @param purgingPoDomain the Purging PO domain
     * @return the list of Purging DO domain
     */
    public List<PurgingDoDomain> searchPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain);

    // [IN012] for check delete P/O
    /**
     * <p>Search count not purging delivery order.</p>
     * <ul>
     * <li>Search count number of Delivery Order that not have to purge.</li>
     * 
     * 
     * @param purgingPoDomain the Purging PO domain
     * @return number of Delivery Order that not have to purge
     */
    public Integer searchCountNotPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain);
    
    /**
     * Search purging urgent delivery order.
     * Search purging urgent delivery order by criteria from batch.
     * 
     * @param purgingPoDomain the Purging PO domain
     * @return the list of Purging DO domain
     */
    public List<PurgingDoDomain> searchPurgingUrgentDeliveryOrder(PurgingPoDomain purgingPoDomain);
    
    /**
     * Search delivery order acknowledgement.
     * <p>Search P/O status of SPS D/O no.</p>
     * 
     * @param deliveryOrderInformationDomain the deliveryOrderInformation domain
     * @return the SPS master PO domain
     */
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    /**
     * <p>Update date time of the Kanban Pdf information.</p>
     * <ul>
     * <li>Update date time of the Kanban Pdf information by criteria from search.</li>
     * </ul>
     * 
     * @param DeliveryOrderInformationDomain the back order information domain
     * @return the integer
     */
    public Integer updateDateTimeKanbanPdf(DeliveryOrderInformationDomain DeliveryOrderInformationDomain);
    
    /**
     * Search One Way Kanban Tag Report Information.
     * <p>Search One Way Kanban Tag Report Information</p>
     * 
     * @param deliveryOrderInformationDomain the DeliveryOrderInformationDomain domain
     * @return the SPS master PO domain
     */
    public List<OneWayKanbanTagReportDomain> searchOneWayKanbanTagReportInformation(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    /**
     * To generate One way Kanban tag report.
     * <p>To generate One way Kanban tag report</p>
     * 
     * @param oneWayKanbanTagReportDomain the List of OneWayKanbanTagReportDomain
     * @return the InputStream of One way Kanban tag report
     * @throws Exception Exception
     */
    
    public InputStream createOneWayKanbanTagReport(
        List<OneWayKanbanTagReportDomain> oneWayKanbanTagReportDomain) throws Exception;
    
    /**
     * search Notify Delivery Order To Supplier.
     * <p>search Notify Delivery Order To Supplier</p>
     * 
     * @param spsTDoDomain the SpsTDoDomain domain
     * @return the List of SpsTDoDomain domain
     */
    public List<SpsTDoDomain> searchNotifyDeliveryOrderToSupplier(SpsTDoDomain spsTDoDomain);

    /**
     * 
     * <p>Create Delivery Order.</p>
     *
     * @param domain KanbanTagDomain
     * @return BigDecimal doId
     * @throws ApplicationException other error
     */
    public BigDecimal createKanbanTag(KanbanTagDomain domain) throws ApplicationException;
    /**
     * 
     * <p>Create Delivery Order.</p>
     *
     * @param domain KanbanTagDomain
     * @return Integer
     * @throws ApplicationException other error
     */
    public Integer updateOneWayKanbanTagFlag(KanbanTagDomain domain) throws ApplicationException;
    /**
     * 
     * <p>Search Batch Number.</p>
     *
     * @return Integer
     * @throws ApplicationException other error
     */
    public Integer searchBatchNumber() throws ApplicationException;
    /**
     * 
     * <p>Create SPS_T_BHT_TRANSMIT_LOG .</p>
     *
     * @param receiveByScanDomain ReceiveByScanDomain
     * @return BigDecimal doId
     * @throws ApplicationException other error
     */
    public BigDecimal createTransmitLog(ReceiveByScanDomain receiveByScanDomain) throws ApplicationException;
    /**
     * 
     * <p>Check the message information is exist in SPS_T_BHT_TRANSMIT_LOG table.</p>
     *
     * @param receiveByScanDomain ReceiveByScanDomain
     * @return List of ReceiveByScanDomain
     * @throws ApplicationException other error
     */
    public List<ReceiveByScanDomain> isExistTransmitLog(ReceiveByScanDomain receiveByScanDomain) throws ApplicationException;
    /**
     * <p>Delete Purging D/O.</p>
     * <ul>
     * <li>Delete D/O data for purging.</li>
     * 
     * @param doId the doId
     * @return the amount of SPS Transaction Invoice Domain
     */
    public int deletePurgingDo(String doId);
    
    /**
     * 
     * <p>Search Exists Delivery Order Back Order.</p>
     *
     * @param paramters DeliveryOrderDomain
     * @return List of SpsTDoDomain
     */
    public List<SpsTDoDomain> searchExistDeliveryOrderBackOrder(DeliveryOrderDomain paramters);
    
    /**
     * 
     * <p>Search Exists Delivery Order Detail Back Order.</p>
     *
     * @param paramters DeliveryOrderDomain
     * @return List of SpsTDoDetailDomain
     */
    public List<SpsTDoDetailDomain> searchExistDeliveryOrderBackOrderDetail(DeliveryOrderDomain paramters);
    
    /**
     * 
     * <p>Update Delivery Order Detail Back Order.</p>
     *
     * @param paramters SpsTDoDetailDomain
     * @return Integer
     */
    public Integer updateBackOrderDetail(SpsTDoDetailDomain paramters) throws ApplicationException;
    
    public Integer updateBackOrderHeader(SpsTDoDomain paramters) throws ApplicationException;

    /**
     * 
     * <p>searchMailBackOrder method.</p>
     *
     * @param criteria SpsTDoDetailDomain
     * @return List of DeliveryOrderDomain
     */
    public List<DeliveryOrderDomain> searchMailBackOrder(SpsTDoDetailDomain criteria);
    
    /**
     * 
     * <p>Purge data from SPS_T_BHT_TRANSMIT_LOG.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deleteBhtTransmitLog(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException;
    
    /**
     * 
     * <p>Purge data from SPS_T_DO.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deleteDeliveryOrder(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException;
    
    //public boolean createKanbanTagList(List<KanbanTagDomain> kanbanList) throws ApplicationException;
    public Integer insertKanbanTag(KanbanTagDomain domain) throws ApplicationException;
}