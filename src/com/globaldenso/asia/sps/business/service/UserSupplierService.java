/*
 * ModifyDate Development company     Describe 
 * 2014/05/30 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;

/**
 * <p>The Interface SupplierUserService.</p>
 * <p>Service for Supplier User about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search  : searchCountSupplierUser</li>
 * <li>Method search  : searchSupplierUser</li>
 * <li>Method search  : searchUserEmailBySupplierCode</li>
 * <li>Method search  : searchEmailUserSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public interface UserSupplierService {
    
    /**
     * <p>Search Supplier User Count</p>
     * <ul>
     * <li>Search count of row</li>
     * </ul>
     * 
     * @param supplierUserInfoDomain the Supplier User Info domain
     * @return the integer
     */
    public Integer searchCountUserSupplier(SupplierUserInformationDomain supplierUserInfoDomain);
    
    /**
     * <p>Search Supplier User Detail.</p>
     * <ul>
     * <li>Search Supplier User Item Detail for display on Inquiry Supplier User screen.</li>
     * </ul>
     * 
     * @param supplierUserInfoDomain the Supplier User Info domain
     * @return the list of Supplier User Item.
     */
    public List<SupplierUserInformationDomain> searchUserSupplier(SupplierUserInformationDomain 
        supplierUserInfoDomain);
    
    /**
     * <p>Search user email by supplier code.
     * <ul>
     * <li>Search list of supplier user's email from supplier code</li>
     * </ul>
     * 
     * @param supplierUserDomain the Supplier User domain
     * @return the list
     */
    public List<String> searchUserEmailBySupplierCode(UserSupplierDomain supplierUserDomain);
    
    /**
     * 
     * <p>search Supplier User Email By Not Found Flag.</p>
     *
     * @param criteria UserSupplierInfoDomain
     * @return List Email String
     */
    public List<String> searchUserSupplierEmailByNotFoundFlag(
        UserSupplierDetailDomain criteria);
    
    /**
     * Search User Supplier Include Role.
     * Search User Supplier Include Role by criteria for export CSV file.
     * 
     * @param supplierUserInfoDomain that keep criteria of searching.
     * @return the List of supplier user information including role.
     */
    public List<SupplierUserInformationDomain> searchUserSupplierIncludeRole(
        SupplierUserInformationDomain supplierUserInfoDomain);
    
    /**
     * Search Email User Supplier.
     * Search Email User Supplier by criteria for sending email to supplier.
     * 
     * @param userSupplierDetailDomain the user supplier detail domain
     * @return the list of spsMUserDomain
     */
    public List<SpsMUserDomain> searchEmailUserSupplier(
        UserSupplierDetailDomain userSupplierDetailDomain);
    
    /**
     * 
     * <p>search Supplier User Email By Urgent Flag.</p>
     *
     * @param criteria UserSupplierInfoDomain
     * @return List Email String
     */
    public List<String> searchUserSupplierEmailByUrgentFlagForBackorder(
        UserSupplierDetailDomain criteria);
}