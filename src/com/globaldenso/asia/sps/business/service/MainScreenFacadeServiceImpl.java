/*
 * ModifyDate Development company     Describe 
 * 2014/08/15 CSI Chatchai                Create
 * 2015/09/17 CSI Akat           FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService;
import com.globaldenso.asia.sps.business.domain.AnnounceMessageDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.UserDensoDetailDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.MenuWithItemDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>
 * Facade Service for Welcome screen.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class MainScreenFacadeServiceImpl implements MainScreenFacadeService {

    /** Call Service SpsMUserService. */
    private SpsMUserService spsMUserService;

    /** Call Service SpsMUserDensoService. */
    private SpsMUserDensoService spsMUserDensoService;

    /** Call Service SpsMUserSupplierService. */
    private SpsMUserSupplierService spsMUserSupplierService;

    /** Call Service SpsMCompanyDensoService. */
    private SpsMCompanyDensoService spsMCompanyDensoService;

    /** Call Service SpsMPlantDensoService. */
    private SpsMPlantDensoService spsMPlantDensoService;

    /** Call Service SpsMCompanySupplierService. */
    private SpsMCompanySupplierService spsMCompanySupplierService;

    /** Call Service SpsMPlantSupplierService. */
    private SpsMPlantSupplierService spsMPlantSupplierService;

    /** Service for get Menu information. */
    private MenuService menuService;

    /** Service for get Screen information. */
    private ScreenService screenService;

    /** Call Service CommonService. */
    private CommonService commonService;
    
    /** The denso supplier relation service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The service for User Role. */
    private UserRoleService userRoleService;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * <p>
     * Call Service SpsMAnnounceMessageService.
     * </p>
     */
    private SpsMAnnounceMessageService spsMAnnounceMessageService;

    /**
     * <p>
     * Call Service PurchaseOrderService.
     * </p>
     */
    private PurchaseOrderService purchaseOrderService;

    /**
     * <p>
     * Call Service SpsMUserRoleService.
     * </p>
     */
    private SpsMUserRoleService spsMUserRoleService;

    /**
     * <p>
     * Call Service DeliveryOrderService.
     * </p>
     */
    private DeliveryOrderService deliveryOrderService;

    /** The Default constructor. */
    public MainScreenFacadeServiceImpl() {
        super();
    }

    /**
     * Set method for spsMUserService.
     * 
     * @param spsMUserService the spsMUserService to set
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }

    /**
     * Set method for spsMUserDensoService.
     * 
     * @param spsMUserDensoService the spsMUserDensoService to set
     */
    public void setSpsMUserDensoService(
        SpsMUserDensoService spsMUserDensoService) {
        this.spsMUserDensoService = spsMUserDensoService;
    }

    /**
     * Set method for spsMUserSupplierService.
     * 
     * @param spsMUserSupplierService the spsMUserSupplierService to set
     */
    public void setSpsMUserSupplierService(
        SpsMUserSupplierService spsMUserSupplierService) {
        this.spsMUserSupplierService = spsMUserSupplierService;
    }

    /**
     * Set method for spsMCompanyDensoService.
     * 
     * @param spsMCompanyDensoService the spsMCompanyDensoService to set
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }

    /**
     * Set method for spsMPlantDensoService.
     * 
     * @param spsMPlantDensoService the spsMPlantDensoService to set
     */
    public void setSpsMPlantDensoService(
        SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }

    /**
     * Set method for spsMCompanySupplierService.
     * 
     * @param spsMCompanySupplierService the spsMCompanySupplierService to set
     */
    public void setSpsMCompanySupplierService(
        SpsMCompanySupplierService spsMCompanySupplierService) {
        this.spsMCompanySupplierService = spsMCompanySupplierService;
    }

    /**
     * Set method for spsMPlantSupplierService.
     * 
     * @param spsMPlantSupplierService the spsMPlantSupplierService to set
     */
    public void setSpsMPlantSupplierService(
        SpsMPlantSupplierService spsMPlantSupplierService) {
        this.spsMPlantSupplierService = spsMPlantSupplierService;
    }

    /**
     * Set method for menuService.
     * 
     * @param menuService the menuService to set
     */
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    /**
     * Set method for screenService.
     * 
     * @param screenService the screenService to set
     */
    public void setScreenService(ScreenService screenService) {
        this.screenService = screenService;
    }

    /**
     * <p>
     * Setter method for commonService.
     * </p>
     * 
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * <p>
     * Setter method for spsMAnnounceMessageService.
     * </p>
     * 
     * @param spsMAnnounceMessageService Set for spsMAnnounceMessageService
     */
    public void setSpsMAnnounceMessageService(
        SpsMAnnounceMessageService spsMAnnounceMessageService) {
        this.spsMAnnounceMessageService = spsMAnnounceMessageService;
    }

    /**
     * <p>
     * Setter method for purchaseOrderService.
     * </p>
     * 
     * @param purchaseOrderService Set for purchaseOrderService
     */
    public void setPurchaseOrderService(
        PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    /**
     * <p>
     * Setter method for deliveryOrderService.
     * </p>
     * 
     * @param deliveryOrderService Set for deliveryOrderService
     */
    public void setDeliveryOrderService(
        DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }

    /**
     * <p>
     * Setter method for spsMUserRoleService.
     * </p>
     * 
     * @param spsMUserRoleService Set for spsMUserRoleService
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }

    /**
     * <p>Setter method for userRoleService.</p>
     *
     * @param userRoleService Set for userRoleService
     */
    public void setUserRoleService(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.MainScreenFacadeService#searchUserLogin(java.lang.String)
     */
    public UserLoginDomain searchInitial(String dscId, Locale locale)
        throws ApplicationException {
        UserLoginDomain userLogin = new UserLoginDomain();
        Timestamp currentDatetime = this.commonService.searchSysDate();
        userLogin.setDscId(dscId);

        /** Search User info */
        SpsMUserCriteriaDomain userCriteria = new SpsMUserCriteriaDomain();
        userCriteria.setDscId(dscId);

        SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
        spsMUserDomain = spsMUserService.searchByKey(userCriteria);

        if (null != spsMUserDomain) {
            userLogin.setSpsMUserDomain(spsMUserDomain);
        } else {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0027,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }

        /** Search User DENSO */
        SpsMUserDensoCriteriaDomain userDensoCriteria = new SpsMUserDensoCriteriaDomain();
        userDensoCriteria.setDscId(dscId);
        SpsMUserDensoDomain spsMUserDenso = spsMUserDensoService
            .searchByKey(userDensoCriteria);

        if (null != spsMUserDenso) {
            /** User is DENSO */
            userLogin.setUserType(SupplierPortalConstant.USER_TYPE_DENSO);
            UserDensoDetailDomain densoUser = new UserDensoDetailDomain();
            densoUser.setSpsMUserDensoDomain(spsMUserDenso);
            userLogin.setUserDensoDetailDomain(densoUser);

            /** Search Company DENSO */
            SpsMCompanyDensoCriteriaDomain companyDensoCriteria = 
                new SpsMCompanyDensoCriteriaDomain();
            companyDensoCriteria.setDCd(spsMUserDenso.getDCd());
            densoUser.setSpsMCompanyDensoDomain(
                spsMCompanyDensoService.searchByKey(companyDensoCriteria));

            /** Search Plant DENSO */
            SpsMPlantDensoCriteriaDomain plantDensoCriteria = new SpsMPlantDensoCriteriaDomain();
            plantDensoCriteria.setDCd(spsMUserDenso.getDCd());
            plantDensoCriteria.setDPcd(spsMUserDenso.getDPcd());
            densoUser.setSpsMPlantDensoDomain(
                spsMPlantDensoService.searchByKey(plantDensoCriteria));

        } else {
            /** If User DENSO not null, this user is SUPPLIER
             * Search User Supplier */
            userLogin.setUserType(SupplierPortalConstant.USER_TYPE_SUPPLIER);
            SpsMUserSupplierCriteriaDomain supplierUserCriteria = 
                new SpsMUserSupplierCriteriaDomain();
            supplierUserCriteria.setDscId(dscId);
            UserSupplierDetailDomain supplierUser = new UserSupplierDetailDomain();
            SpsMUserSupplierDomain spsMUserSupplier 
                = spsMUserSupplierService.searchByKey(supplierUserCriteria);
            supplierUser.setSpsMUserSupplierDomain(spsMUserSupplier);
            userLogin.setUserSupplierDetailDomain(supplierUser);

            /** Search Company Supplier */
            SpsMCompanySupplierCriteriaDomain companySupplierCriteria = 
                new SpsMCompanySupplierCriteriaDomain();
            companySupplierCriteria.setSCd(spsMUserSupplier.getSCd());
            supplierUser.setSpsMCompanySupplierDomain(
                spsMCompanySupplierService.searchByKey(companySupplierCriteria));

            /** Search Plant Supplier */
            SpsMPlantSupplierCriteriaDomain plantSupplierCriteria = 
                new SpsMPlantSupplierCriteriaDomain();
            plantSupplierCriteria.setSCd(spsMUserSupplier.getSCd());
            plantSupplierCriteria.setSPcd(spsMUserSupplier.getSPcd());
            supplierUser.setSpsMPlantSupplierDomain(spsMPlantSupplierService
                .searchByKey(plantSupplierCriteria));
        }
        

        /** Search Menu */
        List<SpsMMenuDomain> spsMMenuList = this.menuService
            .searchMenuByUserPermission(dscId, Constants.IS_ACTIVE,
                currentDatetime);
        userLogin.setSpsMMenuDomainlist(spsMMenuList);
        List<MenuWithItemDomain> menuWithItemList = this
            .manageMenuItem(spsMMenuList);
        userLogin.setMenuWithItemDoaminList(menuWithItemList);
        
        /** Search Role & Screen */
        List<RoleScreenDomain> roleScreenList = this.screenService
            .searchScreenByUserPermission(dscId, Constants.IS_ACTIVE,
                Constants.IS_ACTIVE, currentDatetime);

        userLogin.setRoleScreenDomainList(roleScreenList);
        
        // Search Role Type.
        List<SpsMRoleTypeDomain> roleTypeList = this.userRoleService.searchUserRoleType(dscId,
                Constants.IS_ACTIVE, currentDatetime);
        List<String> buttonLinkNameList = new ArrayList<String>();
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            if (!Strings.judgeBlank(roleType.getRoleTypeName())) {
                buttonLinkNameList.add(roleType.getRoleTypeName().trim());
            }
        }
        userLogin.setButtonLinkNameList(buttonLinkNameList);
        
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        if(null != roleScreenList && !roleScreenList.isEmpty()){
            dataScopeControl.setUserType(userLogin.getUserType());
            for(RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()){
                if(SupplierPortalConstant.SCREEN_ID_WCOM002.equals(
                    roleScreen.getSpsMScreenDomain().getScreenCd())){
                    dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
                }
            }
            dataScopeControl.setDensoSupplierRelationDomainList(null);
        }
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControl);
        if(null != densoSupplierRelationList 
            && Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControl.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        userLogin.setDataScopeControlDomain(dataScopeControl);
        return userLogin;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.MainScreenFacadeService#searchUserRole
     */
    public void searchUserRole(String dscId, Locale locale)throws ApplicationException {
        /** Search Role*/
        SpsMUserRoleCriteriaDomain spsMUserRoleCriteriaDomain 
            = new SpsMUserRoleCriteriaDomain();
        int userCount = Constants.ZERO;
        spsMUserRoleCriteriaDomain.setDscId(dscId);
        userCount = spsMUserRoleService.searchCount(spsMUserRoleCriteriaDomain);
        if (Constants.ZERO == userCount) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_USER_ROLE, locale)});
        }
        
        spsMUserRoleCriteriaDomain.setEffectStartLessThanEqual(commonService.searchSysDate());
        spsMUserRoleCriteriaDomain.setEffectEndGreaterThanEqual(commonService.searchSysDate());
        
        List<SpsMUserRoleDomain> spsMUserRoleDomainList = spsMUserRoleService.
            searchByCondition(spsMUserRoleCriteriaDomain);
        
        if(null == spsMUserRoleDomainList || Constants.ZERO == spsMUserRoleDomainList.size()){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E7_0028,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.MainScreenFacadeService#searchWelcomMessage(com.globaldenso.asia.sps.business.domain.UserLoginDomain)
     */
    public MainScreenDomain searchWelcomMessage(UserLoginDomain userLoginDomain, Locale locale,
        List<SpsMPlantSupplierDomain> supplierAuthenList,
        List<SpsMPlantDensoDomain> densoAuthenList) throws ApplicationException
    {
        SpsMAnnounceMessageCriteriaDomain spsMAnnounceMessageCriteriaDomain = 
            new SpsMAnnounceMessageCriteriaDomain();
        Timestamp currentDatetime = this.commonService.searchSysDate();
        List<SpsMAnnounceMessageDomain> spsMAnnounceMessageDomainList = null;
        MainScreenDomain mainScreenDomain = new MainScreenDomain();
        TaskListDomain taskListDomain = new TaskListDomain();
        
        String userType = userLoginDomain.getUserType();
        spsMAnnounceMessageCriteriaDomain.setDCd(userLoginDomain
            .getUserSupplierDetailDomain().getUserRoleDetailDomain().getDCd());
        spsMAnnounceMessageCriteriaDomain.setEffectStartLessThanEqual(currentDatetime);
        spsMAnnounceMessageCriteriaDomain.setEffectEndGreaterThanEqual(currentDatetime);
        spsMAnnounceMessageCriteriaDomain.setPreferredOrder(
            SupplierPortalConstant.PREFERRED_ORDER_ANNOUNCE_MESSAGE);
        
        /**
         * Call Service SpsMAnnounceMessageService searchAnnounceMessage to get
         * announce message.
         */
        spsMAnnounceMessageDomainList = spsMAnnounceMessageService
            .searchByCondition(spsMAnnounceMessageCriteriaDomain);
        
        if (null != spsMAnnounceMessageDomainList) {
            mainScreenDomain.setSpsMAnnounceMessageDomainList(spsMAnnounceMessageDomainList);
            
            // FIX : wrong dataformat
            List<AnnounceMessageDomain> announceList = new ArrayList<AnnounceMessageDomain>();
            for (SpsMAnnounceMessageDomain spsAnnounceMsg : spsMAnnounceMessageDomainList) {
                AnnounceMessageDomain announceMsg = new AnnounceMessageDomain();
                announceMsg.setDCd(spsAnnounceMsg.getDCd());
                announceMsg.setAnnounceMessage(spsAnnounceMsg.getAnnounceMessage());
                announceMsg.setEffectStart(spsAnnounceMsg.getEffectStart());
                announceMsg.setEffectStartShow(DateUtil.format(
                    spsAnnounceMsg.getEffectStart(), PATTERN_YYYYMMDD_SLASH));
                announceList.add(announceMsg);
            }
            mainScreenDomain.setAnnounceMessageDomainList(announceList);
            
        }
        
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(userType)) {
            
            List<MainScreenResultDomain> mainScreenResultDomainList
                = new ArrayList<MainScreenResultDomain>();
            List<MainScreenResultDomain> purchaseOrderFirm = null;
            List<MainScreenResultDomain> purchaseOrderForcast = null;
            List<MainScreenResultDomain> urgentOrder = null;
            
            taskListDomain.setSupplierAuthenList(supplierAuthenList);
            taskListDomain.setDensoAuthenList(densoAuthenList);
            
            taskListDomain.setSCd(userLoginDomain.getUserSupplierDetailDomain()
                .getSpsMUserSupplierDomain().getSCd());
            
            /**
             * Search count Purchase order (Firm) waiting for acknowledge from
             * PurchaseOrderService.
             */
            purchaseOrderFirm = purchaseOrderService.searchPoFirmWaitToAckForSUser(taskListDomain);
            
            /**
             * Search count Purchase order (Forcast) waiting for acknowledge
             * from PurchaseOrderService.
             */
            purchaseOrderForcast
                = purchaseOrderService.searchPoForcastWaitToAckForSUser(taskListDomain);
            
            SpsMCompanySupplierCriteriaDomain supplierCriteriaDomain
                = new SpsMCompanySupplierCriteriaDomain();
            supplierCriteriaDomain.setSCd(userLoginDomain.getUserSupplierDetailDomain()
                .getSpsMUserSupplierDomain().getSCd());
            SpsMCompanySupplierDomain companySupplierDomain
                = spsMCompanySupplierService.searchByKey(supplierCriteriaDomain);
            
            taskListDomain.setNoticeDoUrgentPeriod(
                NumberUtil.toIntegerDefaultZero(companySupplierDomain.getNoticeDoUrgentPeriod()));
            
            /** Search count Urgent order from DeliveryOrderService. */
            urgentOrder = deliveryOrderService.searchUrgentOrderForSupplierUser(taskListDomain);
            
            int maxValue = Constants.ZERO;
            int position = Constants.ZERO;
            boolean checkNull = true;
            boolean checkStatus = true;
            
            if (maxValue < purchaseOrderFirm.size()) {
                maxValue = purchaseOrderFirm.size();
                position = Constants.ONE;
            }
            
            if (maxValue < purchaseOrderForcast.size()) {
                maxValue = purchaseOrderForcast.size();
                position = Constants.TWO;
            }
            
            if (maxValue < urgentOrder.size()) {
                maxValue = urgentOrder.size();
                position = Constants.THREE;
            }
            
            String[] dcdArr = new String[maxValue];
            
            if (Constants.ONE == position) {
                for (int i = Constants.ZERO; i < maxValue; ++i ) {
                    dcdArr[i] = purchaseOrderFirm.get(i).getDCd();
                }
            } else if (Constants.TWO == position) {
                for (int i = Constants.ZERO; i < maxValue; ++i ) {
                    dcdArr[i] = purchaseOrderForcast.get(i).getDCd();
                }
            } else {
                for (int i = Constants.ZERO; i < maxValue; ++i ) {
                    dcdArr[i] = urgentOrder.get(i).getDCd();
                }
            }
            
            MainScreenResultDomain mainScreenResultDomain = null;
            
            for (int i = Constants.ZERO; i < maxValue; ++i ) {
                checkNull = true;
                checkStatus = true;
                if (Constants.ZERO == purchaseOrderFirm.size()) {
                    checkNull = false;
                }
                
                if (checkNull) {
                    for (int j = Constants.ZERO; j < purchaseOrderFirm.size(); ++j ) {
                        if (dcdArr[i].equals(purchaseOrderFirm.get(j).getDCd())) {
                            mainScreenResultDomain = new MainScreenResultDomain();
                            mainScreenResultDomain.setDCd(dcdArr[i]);
                            mainScreenResultDomain.setCategory(getLabel(
                                    SupplierPortalConstant.
                                    LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE, locale));
                            mainScreenResultDomain.setQuantity(
                                purchaseOrderFirm.get(j).getQuantity());
                            mainScreenResultDomainList.add(mainScreenResultDomain);
                            checkStatus = false;
                        } else {
                            if (j == purchaseOrderFirm.size() - Constants.ONE && checkStatus) {
                                mainScreenResultDomain = new MainScreenResultDomain();
                                mainScreenResultDomain.setDCd(dcdArr[i]);
                                mainScreenResultDomain.setCategory(getLabel(
                                        SupplierPortalConstant.
                                        LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE, locale));
                                mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                                mainScreenResultDomainList.add(mainScreenResultDomain);
                            }
                        }
                    }
                } else {
                    mainScreenResultDomain = new MainScreenResultDomain();
                    mainScreenResultDomain.setDCd(dcdArr[i]);
                    mainScreenResultDomain.setCategory(getLabel(
                            SupplierPortalConstant.LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE,
                            locale));
                    mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                    mainScreenResultDomainList.add(mainScreenResultDomain);
                }
                
                checkNull = true;
                checkStatus = true;
                if (Constants.ZERO == purchaseOrderForcast.size()) {
                    checkNull = false;
                }
                
                if (checkNull) {
                    for (int j = Constants.ZERO; j < purchaseOrderForcast.size(); ++j ) {
                        if (dcdArr[i].equals(purchaseOrderForcast.get(j).getDCd())) {
                            mainScreenResultDomain = new MainScreenResultDomain();
                            mainScreenResultDomain.setDCd(dcdArr[i]);
                            mainScreenResultDomain.setCategory(getLabel(
                                    SupplierPortalConstant.
                                    LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE, locale));
                            mainScreenResultDomain.setQuantity(
                                purchaseOrderForcast.get(j).getQuantity());
                            mainScreenResultDomainList.add(mainScreenResultDomain);
                            checkStatus = false;
                        } else {
                            if (j == purchaseOrderForcast.size() - Constants.ONE && checkStatus) {
                                mainScreenResultDomain = new MainScreenResultDomain();
                                mainScreenResultDomain.setDCd(dcdArr[i]);
                                mainScreenResultDomain.setCategory(getLabel(
                                        SupplierPortalConstant.
                                        LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE, locale));
                                mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                                mainScreenResultDomainList.add(mainScreenResultDomain);
                            }
                        }
                    }
                } else {
                    mainScreenResultDomain = new MainScreenResultDomain();
                    mainScreenResultDomain.setDCd(dcdArr[i]);
                    mainScreenResultDomain.setCategory(getLabel(
                            SupplierPortalConstant.LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE,
                            locale));
                    mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                    mainScreenResultDomainList.add(mainScreenResultDomain);
                }
                
                checkNull = true;
                checkStatus = true;
                if (Constants.ZERO == urgentOrder.size()) {
                    checkNull = false;
                }
                
                if (checkNull) {
                    for (int j = Constants.ZERO; j < urgentOrder.size(); ++j ) {
                        if (dcdArr[i].equals(urgentOrder.get(j).getDCd())) {
                            mainScreenResultDomain = new MainScreenResultDomain();
                            mainScreenResultDomain.setDCd(dcdArr[i]);
                            mainScreenResultDomain.setCategory(getLabel(
                                    SupplierPortalConstant.LBL_URGENT_ORDER, locale));
                            mainScreenResultDomain.setQuantity(urgentOrder.get(j).getQuantity());
                            mainScreenResultDomainList.add(mainScreenResultDomain);
                            checkStatus = false;
                        } else {
                            if (j == (urgentOrder.size() - Constants.ONE) && checkStatus) {
                                mainScreenResultDomain = new MainScreenResultDomain();
                                mainScreenResultDomain.setDCd(dcdArr[i]);
                                mainScreenResultDomain.setCategory(getLabel(
                                    SupplierPortalConstant.LBL_URGENT_ORDER, locale));
                                mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                                mainScreenResultDomainList.add(mainScreenResultDomain);
                            }
                        }
                    }
                } else {
                    mainScreenResultDomain = new MainScreenResultDomain();
                    mainScreenResultDomain.setDCd(dcdArr[i]);
                    mainScreenResultDomain.setCategory(getLabel(
                        SupplierPortalConstant.LBL_URGENT_ORDER, locale));
                    mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                    mainScreenResultDomainList.add(mainScreenResultDomain);
                }
            }
            mainScreenDomain.setMainScreenResultDomain(mainScreenResultDomainList);
        }
        
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(userType)) {
            
            List<MainScreenResultDomain> mainScreenResultDomainList = 
                new ArrayList<MainScreenResultDomain>();
            List<MainScreenResultDomain> purchaseOrderFirm = null;
            List<MainScreenResultDomain> purchaseOrderForcast = null;
            
            SpsMCompanyDensoCriteriaDomain densoCriteria = new SpsMCompanyDensoCriteriaDomain();
            densoCriteria.setDCd(
                userLoginDomain.getUserDensoDetailDomain().getSpsMUserDensoDomain().getDCd());
            SpsMCompanyDensoDomain companyDensoDomain
                = spsMCompanyDensoService.searchByKey(densoCriteria);
            
            Date date = new Date(currentDatetime.getTime());
            Date issueDateCriteria = DateUtil.addDays(date,
                NumberUtil.toIntegerDefaultZero(companyDensoDomain.getNoticePoPeriod()
                    .multiply(new BigDecimal(Constants.MINUS_ONE))));

            taskListDomain.setSupplierAuthenList(supplierAuthenList);
            taskListDomain.setDensoAuthenList(densoAuthenList);
            taskListDomain.setDCd(
                userLoginDomain.getUserDensoDetailDomain().getSpsMUserDensoDomain().getDCd());
            taskListDomain.setIssueDate(issueDateCriteria);
            
            /**
             * Search count Purchase order (Firm) waiting for acknowledge from
             * PurchaseOrderService.
             */
            purchaseOrderFirm = purchaseOrderService.searchPoFirmWaitToAckForDUser(taskListDomain);
            
            /**
             * Search count Purchase order (Forcast) waiting for acknowledge
             * from PurchaseOrderService.
             */
            purchaseOrderForcast
                = purchaseOrderService.searchPoForcastWaitToAckForDUser(taskListDomain);
            
            int maxValue = Constants.ZERO;
            int position = Constants.ZERO;
            boolean checkNull = true;
            boolean checkStatus = true;
            
            if (maxValue < purchaseOrderFirm.size()) {
                maxValue = purchaseOrderFirm.size();
                position = Constants.ONE;
            }
            
            if (maxValue < purchaseOrderForcast.size()) {
                maxValue = purchaseOrderForcast.size();
                position = Constants.TWO;
            }
            
            String[] dcdArr = new String[maxValue];
            
            if (Constants.ONE == position) {
                for (int i = Constants.ZERO; i < maxValue; ++i ) {
                    dcdArr[i] = purchaseOrderFirm.get(i).getDCd();
                }
            } else if (Constants.TWO == position) {
                for (int i = Constants.ZERO; i < maxValue; ++i ) {
                    dcdArr[i] = purchaseOrderForcast.get(i).getDCd();
                }
            }
            
            MainScreenResultDomain mainScreenResultDomain = null;
            for (int i = Constants.ZERO; i < maxValue; ++i ) {
                checkNull = true;
                checkStatus = true;
                if (Constants.ZERO == purchaseOrderFirm.size()) {
                    checkNull = false;
                }
                
                if (checkNull) {
                    for (int j = Constants.ZERO; j < purchaseOrderFirm.size(); ++j ) {
                        if (dcdArr[i].equals(purchaseOrderFirm.get(j).getDCd())) {
                            mainScreenResultDomain = new MainScreenResultDomain();
                            mainScreenResultDomain.setDCd(dcdArr[i]);
                            mainScreenResultDomain.setCategory(getLabel(
                                    SupplierPortalConstant.
                                    LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE, locale));
                            mainScreenResultDomain.setQuantity(
                                purchaseOrderFirm.get(j).getQuantity());
                            mainScreenResultDomainList.add(mainScreenResultDomain);
                            checkStatus = false;
                        } else {
                            if (j == purchaseOrderFirm.size() - Constants.ONE && checkStatus) {
                                mainScreenResultDomain = new MainScreenResultDomain();
                                mainScreenResultDomain.setDCd(dcdArr[i]);
                                mainScreenResultDomain.setCategory(getLabel(
                                        SupplierPortalConstant.
                                        LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE, locale));
                                mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                                mainScreenResultDomainList.add(mainScreenResultDomain);
                            }
                        }
                    }
                } else {
                    mainScreenResultDomain = new MainScreenResultDomain();
                    mainScreenResultDomain.setDCd(dcdArr[i]);
                    mainScreenResultDomain.setCategory(getLabel(
                        SupplierPortalConstant.LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE,
                        locale));
                    mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                    mainScreenResultDomainList.add(mainScreenResultDomain);
                }
                
                checkNull = true;
                checkStatus = true;
                if (Constants.ZERO == purchaseOrderForcast.size()) {
                    checkNull = false;
                }
                
                if (checkNull) {
                    mainScreenResultDomain = new MainScreenResultDomain();
                    for (int j = Constants.ZERO; j < purchaseOrderForcast.size(); ++j ) {
                        
                        if (dcdArr[i].equals(purchaseOrderForcast.get(j).getDCd())) {
                            mainScreenResultDomain.setDCd(dcdArr[i]);
                            mainScreenResultDomain.setCategory(getLabel(
                                    SupplierPortalConstant.
                                    LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE, locale));
                            mainScreenResultDomain.setQuantity(
                                purchaseOrderForcast.get(j).getQuantity());
                            mainScreenResultDomainList.add(mainScreenResultDomain);
                            checkStatus = false;
                        } else {
                            if (j == purchaseOrderForcast.size() - Constants.ONE && checkStatus) {
                                mainScreenResultDomain.setDCd(dcdArr[i]);
                                mainScreenResultDomain
                                    .setCategory(getLabel(
                                        SupplierPortalConstant.
                                        LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE,
                                        locale));
                                mainScreenResultDomain.setQuantity(String.valueOf(Constants.ZERO));
                                mainScreenResultDomainList.add(mainScreenResultDomain);
                            }
                        }
                    }
                } else {
                    mainScreenResultDomain.setDCd(dcdArr[i]);
                    mainScreenResultDomain
                        .setCategory(getLabel(
                            SupplierPortalConstant.LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE,
                            locale));
                    mainScreenResultDomain.setQuantity(String
                        .valueOf(Constants.ZERO));
                    mainScreenResultDomainList.add(mainScreenResultDomain);
                }
            }
            mainScreenDomain.setMainScreenResultDomain(mainScreenResultDomainList);
        }
        return mainScreenDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }

    /**
     * Arrange Menu Item by parent-child.
     * 
     * @param spsMMenuList list of menu to manage
     * @return Menu that already managed
     */
    private List<MenuWithItemDomain> manageMenuItem(
        List<SpsMMenuDomain> spsMMenuList) {
        List<MenuWithItemDomain> result = new ArrayList<MenuWithItemDomain>();

        Map<String, MenuWithItemDomain> firstLayer = 
            new LinkedHashMap<String, MenuWithItemDomain>();

        Map<String, MenuWithItemDomain> secondLayer = 
            new LinkedHashMap<String, MenuWithItemDomain>();

        MenuWithItemDomain menuWithItem = null;
        MenuWithItemDomain parent = null;
        for (SpsMMenuDomain menu : spsMMenuList) {
            menuWithItem = new MenuWithItemDomain();
            if (Constants.RANK_FIRST_RANK.equals(menu.getRank().toString())) {
                menuWithItem.setSpsMMenuDomain(menu);
                firstLayer.put(menu.getMenuCd(), menuWithItem);
            } else if (Constants.RANK_SECOND_RANK.equals(menu.getRank()
                .toString())) {
                menuWithItem.setSpsMMenuDomain(menu);
                secondLayer.put(menu.getMenuCd(), menuWithItem);

                parent = firstLayer.get(menu.getMenuCd());
                if (null != parent) {
                    parent.getSpsMMenuDomainList().add(menu);
                }
            } else {
                parent = secondLayer.get(menu.getMenuCd());
                if (null != parent) {
                    parent.getSpsMMenuDomainList().add(menu);
                }
            }
        }

        result.addAll(firstLayer.values());

        return result;
    }

    /**
     * <p>
     * Get Label method.
     * </p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {

        String labelString = MessageUtil.getLabelHandledException(locale, key);

        return labelString;
    }

}
