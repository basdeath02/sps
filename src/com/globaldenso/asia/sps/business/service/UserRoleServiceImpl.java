/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;


import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain;
import com.globaldenso.asia.sps.business.dao.UserRoleDao;


/**
 * <p>The Class SupplierUserRoleServiceImpl.</p>
 * <p>Manage data of Supplier User Role.</p>
 * <ul>
 * <li>Method search  : searchUserRoleByDscId</li>
 * <li>Method delete  : deleteUserRoleByDscId</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class UserRoleServiceImpl implements UserRoleService {
    
    /** The  User Role dao. */
    private UserRoleDao userRoleDao;
    
    /**
     * Instantiates a new Supplier User Role Service Impl.
     */
    public UserRoleServiceImpl(){
        super();
    }
    
    /**
     * Sets the supplier user role dao.
     * 
     * @param userRoleDao the new supplier user role dao.
     */   
    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }

    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserRoleService#searchUserRoleByDscId
     * (com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain)
     */
    public List<UserRoleDetailDomain> searchUserRoleByDscId(UserRoleDetailDomain 
        userRoleDetailDomain){
        List<UserRoleDetailDomain> result = (List<UserRoleDetailDomain>)userRoleDao
            .searchUserRoleByDscId(userRoleDetailDomain); 
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserRoleService#deleteUserRoleByDscId
     * (com.globaldenso.asia.sps.business.domain.SpsMUserRoleCriteriaDomain)
     */
    public int deleteUserRoleByDscId(SpsMUserRoleCriteriaDomain
        userRoleCriteriaDomain){
        int count = (int)userRoleDao.deleteUserRoleByDscId(userRoleCriteriaDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UserRoleService#searchUserRoleType
     * (java.lang.String,java.lang.String,java.lang.String,java.sql.Timestamp)
     * */
    public List<SpsMRoleTypeDomain> searchUserRoleType(
        String dscId, String isRoleUserActive, Timestamp currentDatetime)
    {
        UserRoleScreenDomain roleUserScreen = new UserRoleScreenDomain();
        roleUserScreen.getSpsMUserRoleDomain().setDscId(dscId);
        roleUserScreen.getSpsMUserRoleDomain().setIsActive(isRoleUserActive);
        roleUserScreen.setCurrentDatetime(currentDatetime);
        return this.userRoleDao.searchUserRoleType(roleUserScreen);
    }
}