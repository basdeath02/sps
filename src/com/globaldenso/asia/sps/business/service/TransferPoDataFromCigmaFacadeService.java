/*
 * ModifyDate Development company     Describe 
 * 2014/08/16 CSI Arnon               Create
 * 2016/02/04 CSI Akat                [IN056]
 * 2016/03/14 CSI Akat                [IN056-2]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;

/**
 * <p>The class TransferDoDataFromCigmaFacadeServiceImpl.</p>
 * <p>Facade for TransferDoDataFromCigmaFacadeServiceImpl.</p>
 * <ul>
 * </ul>
 *
 * @author CSI
 */
public interface TransferPoDataFromCigmaFacadeService{
    /**
     * 
     * <p>Search As400 Server List.</p>
     *
     * @param criteria company
     * @return List
     * @throws Exception exception
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain criteria) throws Exception;
    
    // [IN056] Get P/O from CIGMA by CIGMA Vendor Code
    // [IN056-2] Not get Vendor code from Oracle, get from CIGMA
    //public List<SpsMAs400VendorDomain> searchCigmaVendorCd(String dCd) throws Exception;
    /**
     * Search CIGMA Vendor Code for each DENSO company.
     * @param as400Server config
     * @param spsFlag flag
     * @return List of PseudoCigmaPoCoverPageInformationDomain
     * @throws Exception exception
     * */
    public List<PseudoCigmaPoCoverPageInformationDomain> searchCigmaVendorCd(
        As400ServerConnectionInformationDomain as400Server, String spsFlag) throws Exception;
    
    /**
     * 
     * <p>Search Purchase Order.</p>
     *
     * @param locale Locale
     * @param as400Server config
     * @param spsFlag flag
     * @param vendorCd CIGMA Vendor Code
     * @return List
     * @throws Exception exception
     */
    public TransferPoDataFromCigmaResultDomain searchPurchaseOrder(Locale locale, 
        As400ServerConnectionInformationDomain as400Server, String spsFlag
        
        // [IN056] : Add condition to get P/O by Vendor
        , String vendorCd
        
        ) throws Exception;
    
    /**
     * 
     * <p>Check Transfer P/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList TransferPoDataFromCigmaDomain
     * @param as400Server config
     * @param bError flag
     * @param jobId jobId
     * @param log Log
     * @return List of TransferPoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferPoDataFromCigmaDomain> transactCheckTransferPoFromCigma(Locale locale, 
        List<TransferPoDataFromCigmaDomain> transferList,
        As400ServerConnectionInformationDomain as400Server, boolean bError,
        String jobId, Log log) throws Exception;
    /**
     * 
     * <p>Transfer Error P/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferPoDataFromCigmaDomain
     * @param coverList List of TransferPoCoverPageDataFromCigmaDomain
     * @param as400Server config
     * @param jobId id
     * @return String
     * @throws Exception exception
     */
    public String transactTransferErrorPoFromCigma(Locale locale
        , List<TransferPoDataFromCigmaDomain> transferList
        , List<TransferPoCoverPageDataFromCigmaDomain> coverList
        , As400ServerConnectionInformationDomain as400Server, String jobId) throws Exception;
    
    /**
     * 
     * <p>Transfer P/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferPoDataFromCigmaDomain
     * @param coverList List of TransferPoCoverPageDataFromCigmaDomain
     * @param as400Server config
     * @param jobId id
     * @return String
     * @throws Exception exception
     */
    public String transactTransferPoFromCigma(Locale locale
        , List<TransferPoDataFromCigmaDomain> transferList
        , List<TransferPoCoverPageDataFromCigmaDomain> coverList
        , As400ServerConnectionInformationDomain as400Server, String jobId) throws Exception;
    
    /**
     * 
     * <p>Search Change Purchase Order.</p>
     *
     * @param locale Locale
     * @param as400Server config
     * @param spsFlag flag
     * @return Map value
     * @throws Exception exception
     */
    public Map<String, List<TransferChangePoDataFromCigmaDomain>> searchChangePurchaseOrder(
        Locale locale, As400ServerConnectionInformationDomain as400Server, 
        String spsFlag) throws Exception;
    
    /**
     * 
     * <p>Check Transfer Change P/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferChangePoDataFromCigmaDomain
     * @param as400Server config
     * @param bError flag
     * @param jobId jobId
     * @param log Log
     * @return TransferChangePoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferChangePoDataFromCigmaDomain> transactCheckTransferChangePoFromCigma(
        Locale locale, List<TransferChangePoDataFromCigmaDomain> transferList,
        As400ServerConnectionInformationDomain as400Server, boolean bError,
        String jobId, Log log) throws Exception;
    
    /**
     * 
     * <p>Transfer Error Chg P/O From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferChangePoDataFromCigmaDomain
     * @param as400Server config
     * @param jobId id
     * @return List of TransferChangePoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferChangePoDataFromCigmaDomain> transactTransferErrorChgPoFromCigma(
        Locale locale
        , List<TransferChangePoDataFromCigmaDomain> transferList
        , As400ServerConnectionInformationDomain as400Server
        , String jobId) throws Exception;
    
    /**
     * 
     * <p>Transfer Chg Po From Cigma.</p>
     *
     * @param locale Locale
     * @param transferList List of TransferChangePoDataFromCigmaDomain
     * @param as400Server config
     * @param jobId id
     * @return List of TransferChangePoDataFromCigmaDomain
     * @throws Exception exception
     */
    public List<TransferChangePoDataFromCigmaDomain> transactTransferChgPoFromCigma(Locale locale
        , List<TransferChangePoDataFromCigmaDomain> transferList
        , As400ServerConnectionInformationDomain as400Server, String jobId) throws Exception;
    
    /**
     * 
     * <p>Search Record Limit.</p>
     *
     * @return limit
     * @throws Exception exception
     */
    public MiscellaneousDomain searchRecordLimit() throws Exception;
    
    /**
     * 
     * <p>Search Cigma P/O Error.</p>
     * @param criteria - search criteria
     * @return List
     * @throws Exception exception
     */
    public List<TransferPoErrorEmailDomain> searchCigmaPoError(SpsCigmaPoErrorDomain criteria)
        throws Exception;
    
    /**
     * 
     * <p>Search Cigma Chg P/O Error.</p>
     * @param criteria - search criteria
     * @return List
     * @throws Exception exception
     */
    public List<TransferChgPoErrorEmailDomain> searchCigmaChgPoError(
        SpsCigmaChgPoErrorDomain criteria) throws Exception;
    
    /**
     * 
     * <p>Send Notification Abnormal Data P/O.</p>
     *
     * @param locale Locale
     * @param sendPoError List
     * @param sendPoErrorToDenso List of PO error send to DENSO
     * @param sendChgPoError List
     * @param sendChgPoErrorToDenso List of Change PO error send to DENSO
     * @param limit int
     * @param log Logger
     * @return List
     * @throws Exception exception
     */
    public List<String> transactSendNotificationAbnormalDataPo(Locale locale
        , List<TransferPoErrorEmailDomain> sendPoError
        , List<TransferPoErrorEmailDomain> sendPoErrorToDenso
        , List<TransferChgPoErrorEmailDomain> sendChgPoError
        , List<TransferChgPoErrorEmailDomain> sendChgPoErrorToDenso
        , int limit, Log log) throws Exception;
}
