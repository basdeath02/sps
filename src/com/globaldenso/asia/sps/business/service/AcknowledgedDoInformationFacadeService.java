/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Parichat           Create
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface AcknowledgedDoInformationFacadeService.</p>
 * <p>Service for acknowledged D/O information about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchAcknowledgedDoInformation</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchDeliveryOrderReport</li>
 * <li>Method search  : searchValidateGroupAsn</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchSelectedPlantDenso</li>
 * <li>Method search  : searchSelectedPlantSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public interface AcknowledgedDoInformationFacadeService {
    
    /**
     * <p>Initial.</p>
     * <ul>
     * <li>Initial for display on Acknowledged DO Information screen.</li>
     * </ul>
     * 
     * @param acknowledgedDOInformation the acknowledged DO information domain
     * @return the acknowledgedDOInformation domain
     * @throws ApplicationException ApplicationException
     */
    public AcknowledgedDoInformationDomain searchInitial (AcknowledgedDoInformationDomain 
        acknowledgedDOInformation) throws ApplicationException;
    
    /**
     * <p>Search acknowledged do information.</p>
     * <ul>
     * <li>Search acknowledged do information.</li>
     * </ul>
     * 
     * @param acknowledgedDOInformation the acknowledged DO information domain
     * @return the acknowledgedDOInformationReturnDomain domain
     * @throws ApplicationException ApplicationException
     */
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDoInformation (
        AcknowledgedDoInformationDomain acknowledgedDOInformation) throws ApplicationException;
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search PDF file name from PDF file id.</li>
     * </ul>
     * 
     * @param acknowledgedDOInformation the acknowledged DO information domain
     * @return the FileManagementDomain that Keep File name.
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName (AcknowledgedDoInformationDomain
        acknowledgedDOInformation) throws ApplicationException;
    
    /**
     * <p>Search legend info.</p>
     * <ul>
     * <li>Search download legend information from file id.</li>
     * </ul>
     * 
     * @param acknowledgedDOInformation the acknowledgedDOInformation Domain
     * @param outputStream the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(AcknowledgedDoInformationDomain acknowledgedDOInformation,
        OutputStream outputStream) throws ApplicationException;
    
    /**
     * <p>Search delivery order report.</p>
     * <ul>
     * <li>Search delivery order report from PDF file id.</li>
     * </ul>
     * 
     * @param acknowledgedDOInformation the acknowledged DO information domain
     * @param outputStream the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchDeliveryOrderReport (AcknowledgedDoInformationDomain
        acknowledgedDOInformation, OutputStream outputStream) throws ApplicationException;
    
    /**
     * <p>Search Validate Group ASN.</p>
     * <ul>
     * <li>Validate D/O information for group asn.</li>
     * </ul>
     * 
     * @param acknowledgedDoInformation the acknowledged DO information domain
     * @throws ApplicationException ApplicationException
     */
    public void searchValidateGroupAsn(AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException;

    /**
     * <p>Search Validate amount each RCV lane Group ASN.</p>
     * <ul>
     * <li>Validate D/O information for group asn.</li>
     * </ul>
     * 
     * @param acknowledgedDoInformationDomain the AcknowledgedDoInformationDomain
     * @param inquiryDODetailDomain the InquiryDoDetailDomain
     * @throws ApplicationException ApplicationException
     */
    public void isAmountPartGroupByRcvLaneOverQrCode(AcknowledgedDoInformationDomain acknowledgedDoInformationDomain, InquiryDoDetailDomain inquiryDODetailDomain)
        throws ApplicationException;
    
    /**
     * <p>Search selected company denso.</p>
     * <ul>
     * <li>Search denso plant when change denso company.</li>
     * </ul>
     * 
     * @return the list
     * @param plantDensoWithScopeDomain the plant denso with scope domain
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Search selected company supplier.</p>
     * <ul>
     * <li>Search supplier plant when change supplier company.</li>
     * </ul>
     * 
     * @return the list
     * @param plantSupplierWithScopeDomain the plant supplier with scope domain
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant supplier code.</p>
     * <ul>
     * <li>Search list of company supplier to show in combo box.</li>
     * </ul>
     * 
     * @param plantSupplierWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanySupplierDomain filter by selected Plant Supplier
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain>  searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;

    /**
     * <p>
     * Search update DateTime KanbanPdf method.
     * </p>
     * 
     * @param acknowledgedDoInformation acknowledgedDoInformation
     * @return resultCode
     * @throws ApplicationException ApplicationException
     */
    public Integer updateDateTimeKanbanPdf(AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException;
    
    
    /**
     * Generate One Way Kanban Tag Report Information and return by throw ApplicationException.
     * @param acknowledgedDoInformation acknowledgedDoInformation
     * @return InputStream createOneWayKanbanTagReport
     * @throws ApplicationException contain application message
     * */
    public InputStream createOneWayKanbanTagReport(
        AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException;

    /**
     * <p>
     * Generate D/O PDF method.
     * </p>
     * 
     * @param acknowledgedDoInformation acknowledgedDoInformation
     * @return FileManagementDomain
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchGenerateDo(
        AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException;
}