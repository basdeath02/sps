/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The interface CompanySupplierService.</p>
 * <p>Service for Supplier company.</p>
 * <ul>
 * <li>Method search  : searchCompanySupplier</li>
 * <li>Method search  : searchCompanySupplierNameByRelation</li>
 * <li>Method search  : seachCompanySupplierByCode</li>
 * </ul>
 *
 * @author CSI
 */
public interface CompanySupplierService {

    /**
     * Search Supplier company.
     * 
     * @param plantSupplierWithScope plant supplier information with data scope control
     * @return List of Supplier company
     * */
    public List<CompanySupplierDomain> searchCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope);
    
    /**
     * Search Supplier information by Supplier company code.
     * 
     * @param companySupplierCriteria the Company Supplier Domain
     * @return the Company Supplier Domain
     * */
    public CompanySupplierDomain searchCompanySupplierByCode(
        CompanySupplierDomain companySupplierCriteria);
    
    /**
     * Search company supplier name by Relation.
     * 
     * @param companySupplierWithScopeDomain company Supplier with scope
     * @return List of Company Supplier
     * */
    public List<CompanySupplierDomain> searchCompanySupplierNameByRelation
        (CompanySupplierWithScopeDomain companySupplierWithScopeDomain);
    
    /**
     * Search Exist Company Supplier Information.
     * 
     * @param companySupplierDomain the Company Supplier Domain
     * @return the Integer
     * */
    public Integer searchExistCompanySupplierInformation(
        CompanySupplierDomain companySupplierDomain);
    
    /**
     * Search Supplier Exist company by user Relation.
     * 
     * @param companySupplierWithScopeDomain Supplier company with scope
     * @return count of company supplier.
     * */
    public Integer searchExistCompanySupplier
        (CompanySupplierWithScopeDomain companySupplierWithScopeDomain);
}
