/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.ArrayList;
import java.util.List;
import com.globaldenso.asia.sps.business.dao.DensoSupplierPartDao;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * <p>The class DensoSupplierPartServiceImpl implements DensoSupplierPartService.</p>
 * <p>Service for DENSO and Supplier company relation.</p>
 * <ul>
 * <li>Method search  : searchDensoSupplierPart</li>
 * </ul>
 *
 * @author CSI
 */
public class DensoSupplierPartServiceImpl implements DensoSupplierPartService {
    
    /** The DENSO Supplier Relation Dao. */
    DensoSupplierPartDao densoSupplierPartDao;
    
    /** The default constructor. */
    public DensoSupplierPartServiceImpl() {
        super();
    }
    
    /**
     * Setter method for densoSupplierRelationDao.
     * 
     * @param densoSupplierPartDao DENSO Supplier Part Dao to set
     * */
    public void setDensoSupplierPartDao(DensoSupplierPartDao densoSupplierPartDao) {
        this.densoSupplierPartDao = densoSupplierPartDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoSupplierPartService#searchDensoSupplierPart(com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain)
     */
    public DensoSupplierPartDomain searchDensoSupplierPart(DensoSupplierPartDomain 
        densoSupplierPartDomain){
        DensoSupplierPartDomain result = densoSupplierPartDao.searchDensoSupplierPart(
            densoSupplierPartDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DensoSupplierRelationService#searchAllDensoSupplierPart(com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain)
     */
    public List<DensoSupplierPartDomain> searchAllDensoSupplierPart(DataScopeControlDomain 
        dataScopeControlDomain){
        List<DensoSupplierPartDomain> result = new ArrayList<DensoSupplierPartDomain>();
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(dataScopeControlDomain.getUserType())) {
            result = densoSupplierPartDao.searchDensoSupplierPartByRole(dataScopeControlDomain);
            return result;
        } else {
            result = densoSupplierPartDao.searchDensoSupplierPartByRelation(dataScopeControlDomain);
            return result;
        }
    }

}
