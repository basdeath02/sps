/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoUpdatedInvoiceStatusDomain;
import com.globaldenso.asia.sps.business.domain.UpdateInvoiceInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The Class UpdatePaymentStatusFacadeServiceImpl.</p>
 * <p>Interface for Update Payment Status From JDE batch.</p>
 * <ul>
 * <li>Method search  : searchAS400ServerList</li>
 * <li>Method update  : updatePaymentStatus</li>
 * </ul>
 *
 * @author CSI
 */
public class UpdatePaymentStatusFacadeServiceImpl implements UpdatePaymentStatusFacadeService {
    
    /** The Common Service. */
    private CommonService commonService;
    
    /** The Service for Company DENSO information. */
    private CompanyDensoService companyDensoService;
    
    /** The Service for SPS_T_INVOICE. */
    private SpsTInvoiceService spsTInvoiceService;
    
    /** The Default Constructor. */
    public UpdatePaymentStatusFacadeServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for companyDensoService.</p>
     *
     * @param companyDensoService Set for companyDensoService
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }

    /**
     * <p>Setter method for spsTInvoiceService.</p>
     *
     * @param spsTInvoiceService Set for spsTInvoiceService
     */
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService) {
        this.spsTInvoiceService = spsTInvoiceService;
    }

    /**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UpdatePaymentStatusFacadeService#searchAs400ServerList(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        SpsMCompanyDensoDomain companyDensoDomain) throws ApplicationException
    {
        List<As400ServerConnectionInformationDomain> serverList = null;
        serverList = this.companyDensoService.searchAs400ServerList(companyDensoDomain);
        return serverList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.UpdatePaymentStatusFacadeService#updatePaymentStatus(com.globaldenso.asia.sps.business.domain.UpdateInvoiceInformationDomain)
     */
    public void updatePaymentStatus(UpdateInvoiceInformationDomain updateInvoiceInformationDomain)
        throws ApplicationException
    {
        List<PseudoUpdatedInvoiceStatusDomain> updateInvoiceList
            = CommonWebServiceUtil.searchInvoiceStatusUpdate(updateInvoiceInformationDomain);
        
        if (null == updateInvoiceList || Constants.ZERO == updateInvoiceList.size()) {
            return;
        }

        List<SpsTInvoiceDomain> valueList = new ArrayList<SpsTInvoiceDomain>();
        Timestamp lastUpdateDatetime = this.commonService.searchSysDate();
        
        SpsTInvoiceDomain value = null;
        SpsTInvoiceCriteriaDomain criteria = null;
        for (PseudoUpdatedInvoiceStatusDomain updateInvoice : updateInvoiceList) {
            if (null == updateInvoice.getPseudoInvoiceNo()
                || null == updateInvoice.getPseudoPaymentDate()
                || null == updateInvoice.getPseudoSCd()
                || null == updateInvoice.getPseudoInvoiceStatus())
            {
                continue;
            }
            
            if (SupplierPortalConstant.INVOICE_STATUS_HOLD_PAYMENT_CIGMA.equals(
                updateInvoice.getPseudoInvoiceStatus().trim())
                || SupplierPortalConstant.INVOICE_STATUS_PAID_CIGMA.equals(
                    updateInvoice.getPseudoInvoiceStatus().trim()))
            {
                value = new SpsTInvoiceDomain();
                criteria = new SpsTInvoiceCriteriaDomain();
                
                if (SupplierPortalConstant.INVOICE_STATUS_HOLD_PAYMENT_CIGMA.equals(
                    updateInvoice.getPseudoInvoiceStatus()))
                {
                    value.setInvoiceStatus(Constants.INVOICE_STATUS_DHP);
                }
                if (SupplierPortalConstant.INVOICE_STATUS_PAID_CIGMA.equals(
                    updateInvoice.getPseudoInvoiceStatus()))
                {
                    Date paymentDate = DateUtil.convertStringJulianDateToDateGregorianDate(
                        updateInvoice.getPseudoPaymentDate().trim());
                    if (null == paymentDate) {
                        continue;
                    }
                    value.setInvoiceStatus(Constants.INVOICE_STATUS_PAY);
                    value.setPaymentDate(paymentDate);
                }
                
                value.setLastUpdateDatetime(lastUpdateDatetime);
                value.setLastUpdateDscId(updateInvoiceInformationDomain.getDscId());
                
                criteria.setInvoiceNo(updateInvoice.getPseudoInvoiceNo().trim());
                criteria.setVendorCd(updateInvoice.getPseudoSCd().trim());
                criteria.setDCd(updateInvoiceInformationDomain.getDCd());
                
                List<SpsTInvoiceDomain> invList
                    = this.spsTInvoiceService.searchByCondition(criteria);
                for (SpsTInvoiceDomain inv : invList) {
                    if (!Constants.INVOICE_STATUS_DCL.equals(inv.getInvoiceStatus())) {
                        value.setInvoiceId(inv.getInvoiceId());
                        valueList.add(value);
                        break;
                    }
                }
            }
        }
        
        if (Constants.ZERO == valueList.size())  {
            return;
        }
        
        int totalUpdate = this.spsTInvoiceService.update(valueList);
        if (totalUpdate != valueList.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(
                updateInvoiceInformationDomain.getLocale(), 
                SupplierPortalConstant.ERROR_CD_SP_E6_0042,
                SupplierPortalConstant.LBL_UPDATE_INVOICE_STATUS_FROM_JDE);
        }
    }
}
