/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2015/08/03 CSI Akat                  [IN009], [IN010]
 * 2015/10/20 CSI Akat                  [IN035]
 * 2015/10/21 CSI Akat                  [IN030]
 * 2016/03/01 CSI Akat                  [IN059]
 * 2016/03/02 CSI Akat                  [FIX] Don't have to absolute value of CN
 * 2016/03/03 CSI Akat                  [FIX] CN VAT/GST amount can equal to zero
 * 2016/03/03 CSI Akat                  [FIX] ASN in CN section type=ASN, date=ASN Date
 * 2016/03/07 CSI Akat                  [FIX] None VAT will not input any VAT amount
 * 2018/04/11 Netband U.Rungsiwut       Generate invoice PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.sf.jasperreports.engine.JasperPrint;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsTAsnService;
import com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.CnInformationDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceCoverPageDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceDetailDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPurchasePriceMasterDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ASN_DOCUMENT_TYPE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.CN_DOCUMENT_TYPE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0009;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0012;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E9_0004;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0025;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0026;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0001;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0003;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0013;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0032;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0034;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0055;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E7_0007;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E7_0009;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E7_0023;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E7_0030;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E7_0037;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.INV_DOCUMENT_TYPE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.INVOICE_COVER_PAGE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_ASN_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_ASN_STATUS;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_CN_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_CN_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_CREDIT_NOTE_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_CREDIT_NOTE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DENSO_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DENSO_VAT_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DENSO_TOTAL_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DENSO_CODE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DENSO_COMPANY;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DENSO_BASE_AMOUNT_BY_PN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_D_PN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DENSO_UNIT_OF_MEASURE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DENSO_PRICE_UNIT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DENSO_TAX_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_DSC_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_INVOICE_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_INVOICE_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_INVOICE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_S_CD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_D_CD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_D_PCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_SESSION_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_SHIPPING_QTY;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_SUPPLIER_BASE_AMOUNT_BY_PN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_SUPPLIER_CODE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_SUPPLIER_CURRENCY;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_S_PN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_SUPPLIER_TAX_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_S_UM;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_VAT_RATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_VAT_TYPE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_TEMP_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_TOTAL_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_TOTAL_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_TRIP_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_UNIT_PRICE_OF;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_UPDATE_FILE_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_VAT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.LBL_VAT_GST_RAET;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MISC_CODE_WINV004_RLM;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.MISC_TYPE_SCR_REC_LM;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .INVOICE_COVER_PAGE_PATH;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREATE_INVOICE_COVERPAGE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREATE_INVOICE_DUPLICATE_CN_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREATE_INVOICE_DUPLICATE_INVOICE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREDIT_NOTE_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREDIT_NOTE_BASE_AMOUNT_CREDIT_NOTE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREDIT_NOTE_TOTAL_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREDIT_NOTE_VAT_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CREDIT_NOTE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DENSO_COMPANY_CODE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DIFFERENCE_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DIFFERENCE_BASE_AMOUNT_BY_PN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INPUT_CNDATE_LESS_THAN_INVDATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INVOICE_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INVOICE_BASE_AMOUNT_INVOICE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INVOICE_TOTAL_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INVOICE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_NO_PRICE_DIFFERENCEN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_REGISTER_INVOICE_DATA;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_REGISTER_CREDIT_NOTE_DATA;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_SUPPLIER_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_SUPPLIER_PRICE_UNIT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_SUPPLIER_TOTAL_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_SUPPLIER_VAT_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_AMOUNT_TOTAL_DIFFERENCE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_BASE_AMOUNT_TOTAL_DIFFERENCE_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_DIFFERENCE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_DIFFERENCE_BASE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_DIFFERENCE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_TOTAL_VAT_GST_AMOUNT_TOTAL_DIFFERENCE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_UPDATE_COVER_PAGE_RUNNING_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_UPDATE_CREATED_INVOICE_FLAG;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_DIFFERENCE_VAT_GST_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_CN_AMOUNT_NEGATIVE_VALUE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_PARTS_IN_INVOICE_NOT_SAME_UNIT_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INPUT_COMMA_IN_INVOICE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .LBL_INPUT_COMMA_IN_CN_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_SUBJECT_INVOICE_CN; 
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE2_CN;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE4;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TEXT_LINE5;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TABLE_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_HEADER_TABLE_LINE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_DETAIL_TABLE_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_DETAIL_TABLE_LINE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_FOOTER_LINE1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_INVOICE_FOOTER_LINE2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_ASNNO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_SCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_DCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_DPCD;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_PLAN_ETA;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_DPARTNO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_SHIPPING_QTY;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_INVOICE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_INVOICE_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_CN_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_CN_DATE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_FIRST_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_MIDDLE_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_LAST_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_D_UNIT_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_S_UNIT_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_TEMP_PRICE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_ROW_COLOR;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.REPLACE_COVER_PAGE_NO;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_D_UNIT_OF_MEASURE;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_DIFF_BASE_AMT_BY_PART;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_TOTAL_INVOICE_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_GOOD_RCV_STATUS;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .REPLACE_TOTAL_CN_AMOUNT;
import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.MESSAGE_FAILURE;
import static com.globaldenso.asia.sps.common.constant.Constants.ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.REGX_DECIMAL_NUMBER_FORMAT;
import static com.globaldenso.asia.sps.common.constant.Constants.REGX_DECIMAL_NUMBER_FORMAT_NEG;
import static com.globaldenso.asia.sps.common.constant.Constants.RUNNING_NO_FORMAT;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_SPACE;
import static com.globaldenso.asia.sps.common.constant.Constants.BIG_DECIMAL_ZERO;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;

/**
 * <p>The class InvoiceMaintenanceFacadeServiceImpl.</p>
 * <p>Facade for InvoiceMaintenanceFacadeServiceImpl.</p>
 * <ul>
 * <li>Method initial  : searchInitialGroupInvoiceByManual</li>
 * <li>Method initial  : searchInitialGroupInvoiceByCsv</li>
 * <li>Method initial  : searchInitialViewInvoiceMaintenance</li>
 * <li>Method create   : transactRegisterInvoice</li>
 * <li>Method delete   : deleteTmpUploadInvoice</li>
 * <li>Method search   : searchPreviewCoverPage</li>
 * <li>Method search   : searchInvoiceMaintenanceCsv</li>
 * <li>Method search   : searchFileName</li>
 * <li>Method search   : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public class InvoiceMaintenanceFacadeServiceImpl extends AbstractReportServicesImpl 
    implements InvoiceMaintenanceFacadeService {

    /** The high of header. */
    private static final int HEAD_HIGH = 2;
    
    /** The high of footer */
    private static final int FOOT_HIGH = 4;
    
    /** The high of sign. */
    private static final int SIGN_HIGH = 6;
    
    /** The content space in first page. */
    private static final int FIRST_PAGE_SPACE = 39;
    
    /** The content space in other page. */
    private static final int OTHER_PAGE_SPACE = 43;
    
    /** The sps master company denso service. */
    private SpsMCompanyDensoService spsMCompanyDensoService = null;
    
    /** The service for SPS_M_PLANT_DENSO. */
    private SpsMPlantDensoService spsMPlantDensoService = null;
    
    /** The Supplier company service. */
    private CompanySupplierService companySupplierService = null;
    
    /** The ASN service. */
    private AsnService asnService = null;
    
    /** The temporary upload invoice service. */
    private TmpUploadInvoiceService tmpUploadInvoiceService = null;
    
    /** The invoice service. */
    private InvoiceService invoiceService = null;
    
    /** The SPS transaction invoice service. */
    private SpsTInvoiceService spsTInvoiceService = null;
    
    /** The SPS transaction invoice detail service. */
    private SpsTInvoiceDetailService spsTInvoiceDetailService = null;
    
    /** The SPS transaction cn detail service. */
    private SpsTCnDetailService spsTCnDetailService = null;
    
    /** The sps master as400 vendor service. */
    private SpsMAs400VendorService spsMAs400VendorService = null;
    
    /** The cn service. */
    private CnService cnService = null;
    
    /** The SPS transaction asn service. */
    private SpsTAsnService spsTAsnService = null;
    
    /** The SPS transaction invoice cover page run no service. */
    private SpsTInvCoverpageRunnoService spsTInvCoverpageRunnoService = null;
    
    /** The sps temp upload invoice service. */
    private SpsTmpUploadInvoiceService spsTmpUploadInvoiceService = null;
    
    /** The file management service. */
    private FileManagementService fileManagementService = null;
    
    /** The common service. */
    private CommonService commonService = null;
    
    /** The misc service. */
    private MiscellaneousService miscService = null;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /** UserService */
    private UserService userService;

    /** Mail Service */
    private MailService mailService;
    
    /** Service for SPS_M_USER. */
    private SpsMUserService spsMUserService;

    /**
     * Instantiates a new upload facade service impl.
     */
    public InvoiceMaintenanceFacadeServiceImpl(){
        super();
    }
    
    /**
     * Set the sps master company denso service.
     * 
     * @param spsMCompanyDensoService the sps master company denso service to set
     */
    public void setSpsMCompanyDensoService(SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the ASN Service.
     * 
     * @param asnService the ASN service to set
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Set the temporary upload invoice service.
     * 
     * @param tmpUploadInvoiceService the temporary upload invoice service to set
     */
    public void setTmpUploadInvoiceService(TmpUploadInvoiceService tmpUploadInvoiceService){
        this.tmpUploadInvoiceService = tmpUploadInvoiceService;
    }
    
    /**
     * Set the invoice service.
     * 
     * @param invoiceService the invoice service to set
     */
    public void setInvoiceService(InvoiceService invoiceService){
        this.invoiceService = invoiceService;
    }
    
    /**
     * Set the invoice service.
     * 
     * @param spsTInvoiceService the invoice service to set
     */
    public void setSpsTInvoiceService(SpsTInvoiceService spsTInvoiceService){
        this.spsTInvoiceService = spsTInvoiceService;
    }
    
    /**
     * Set the invoice detail service.
     * 
     * @param spsTInvoiceDetailService the invoice detail service to set
     */
    public void setSpsTInvoiceDetailService(SpsTInvoiceDetailService spsTInvoiceDetailService){
        this.spsTInvoiceDetailService = spsTInvoiceDetailService;
    }
    
    /**
     * Sets the sps master as400 vendor service.
     * 
     * @param spsMAs400VendorService the sps master as400 vendor service.
     */ 
    public void setSpsMAs400VendorService(SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }
    
    /**
     * Set the cn service.
     * 
     * @param cnService the cn service to set
     */
    public void setCnService(CnService cnService){
        this.cnService = cnService;
    }
    
    /**
     * Set the sps transaction cn detail service.
     * 
     * @param spsTCnDetailService the sps transaction cn detail service to set
     */
    public void setSpsTCnDetailService(SpsTCnDetailService spsTCnDetailService){
        this.spsTCnDetailService = spsTCnDetailService;
    }
    
    /**
     * Set the sps transaction asn service.
     * 
     * @param spsTAsnService the sps transaction asn service to set
     */
    public void setSpsTAsnService(SpsTAsnService spsTAsnService){
        this.spsTAsnService = spsTAsnService;
    }
    
    /**
     * Set the sps transaction invoice cover page run no service.
     * 
     * @param spsTInvCoverpageRunnoService the sps transaction invoice cover page run no service to set
     */
    public void setSpsTInvCoverpageRunnoService(
        SpsTInvCoverpageRunnoService spsTInvCoverpageRunnoService){
        this.spsTInvCoverpageRunnoService = spsTInvCoverpageRunnoService;
    }
    
    /**
     * Set the sps temp upload invoice service.
     * 
     * @param spsTmpUploadInvoiceService the sps temp upload invoice service to set
     */
    public void setSpsTmpUploadInvoiceService(
        SpsTmpUploadInvoiceService spsTmpUploadInvoiceService){
        this.spsTmpUploadInvoiceService = spsTmpUploadInvoiceService;
    }
    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * <p>Setter method for userService.</p>
     *
     * @param userService Set for userService
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * <p>Setter method for mailService.</p>
     *
     * @param mailService Set for mailService
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * <p>Setter method for spsMUserService.</p>
     *
     * @param spsMUserService Set for spsMUserService
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }

    /**
     * <p>Setter method for spsMPlantDensoService.</p>
     *
     * @param spsMPlantDensoService Set for spsMPlantDensoService
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchInvoiceInformation(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public InvoiceMaintenanceDomain searchInitialGroupInvoiceByManual(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws ApplicationException {
        
        Locale locale = invoiceMaintenanceDomain.getLocale();
        InvoiceMaintenanceDomain invoiceMaintenanceReturnDomain 
            = new InvoiceMaintenanceDomain();
        
        if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getVendorCd())){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E7_0007, LBL_SUPPLIER_CODE);
        } else if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getSupplierPlantTaxId())){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E7_0007, LBL_SUPPLIER_TAX_ID);
        } else if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDCd())){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E7_0007, LBL_DENSO_CODE);
//        } else if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDPcd())){
//            MessageUtil.throwsApplicationMessageWithLabel(locale,
//                ERROR_CD_SP_E7_0007, LBL_DENSO_PLANT_CODE);
        } else if(null == invoiceMaintenanceDomain.getAsnSelectedMap()
            || ZERO == invoiceMaintenanceDomain.getAsnSelectedMap().size())
        {
            MessageUtil.throwsApplicationMessageWithLabel(locale, ERROR_CD_SP_E7_0007, LBL_ASN_NO);
        }
        
        MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
        miscLimitDomain.setMiscType(MISC_TYPE_SCR_REC_LM);
        miscLimitDomain.setMiscCode(MISC_CODE_WINV004_RLM);
        Integer recordLimit = miscService.searchMiscValue(miscLimitDomain);
        if(null == recordLimit){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0032);   
        }
        
        SpsMCompanyDensoCriteriaDomain companyDensoCriteria = new SpsMCompanyDensoCriteriaDomain();
        companyDensoCriteria.setDCd(invoiceMaintenanceDomain.getDCd());
        SpsMCompanyDensoDomain spsMCompanyDensoDomain 
            = this.spsMCompanyDensoService.searchByKey(companyDensoCriteria);
        if(null == spsMCompanyDensoDomain){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                ERROR_CD_SP_E6_0013, LBL_DENSO_COMPANY_CODE);
        }
        
        SpsMAs400VendorCriteriaDomain vendorCriteria = new SpsMAs400VendorCriteriaDomain();
        vendorCriteria.setVendorCd(invoiceMaintenanceDomain.getVendorCd());
        vendorCriteria.setDCd(invoiceMaintenanceDomain.getDCd());
        vendorCriteria.setIsActive(STR_ONE);
        List<SpsMAs400VendorDomain> vendorDomainList = 
            this.spsMAs400VendorService.searchByCondition(vendorCriteria);
        if(ZERO == vendorDomainList.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                ERROR_CD_SP_E6_0001);
        }
        
        CompanySupplierDomain companySupplierCriteria = new CompanySupplierDomain();
        companySupplierCriteria.setSCd(vendorDomainList.get(ZERO).getSCd());
        CompanySupplierDomain companySupplierDomain 
            = this.companySupplierService.searchCompanySupplierByCode(companySupplierCriteria);
        if(null == companySupplierDomain){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0055);
        }
        
        String asnNo = StringUtil.convertListToVarcharCommaSeperate(
            invoiceMaintenanceDomain.getAsnSelectedMap().keySet());
        AsnDomain asnCriteria = new AsnDomain();
        asnCriteria.getSpsTAsnDomain().setAsnNo(asnNo);
        asnCriteria.getSpsTAsnDomain().setDCd(invoiceMaintenanceDomain.getDCd());
        List<AsnDomain> result = this.asnService.searchAsnForGroupInvoice(asnCriteria);
        if(ZERO == result.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0001);
        }else if(recordLimit < result.size()) {
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0003, 
                new String[]{String.valueOf(recordLimit)});
        }
        
        Set<String> densoCodeList = new TreeSet<String>();
        Map<String, List<AsnDomain>> asnResultMap = new HashMap<String, List<AsnDomain>>();
        Map<String, Set<String>> asnNoMap = new HashMap<String, Set<String>>();
        for(AsnDomain asnItem : result){
            String densoCode = asnItem.getSpsTAsnDomain().getDCd();
            densoCodeList.add(densoCode);
            if(asnResultMap.containsKey(densoCode)){
                
                asnResultMap.get(densoCode).add(asnItem);
                asnNoMap.get(densoCode).add(asnItem.getAsnNo());
            }else{
                
                List<AsnDomain> asnList = new ArrayList<AsnDomain>();
                asnList.add(asnItem);
                asnResultMap.put(densoCode, asnList);
                
                Set<String> asnNoList = new TreeSet<String>();
                asnNoList.add(asnItem.getAsnNo());
                asnNoMap.put(densoCode, asnNoList);
            }
        }
        
        String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
        SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
            = new SpsMCompanyDensoDomain();
        companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
        List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
            = this.companyDensoService.searchAs400ServerList(
                companyDensoCriteriaForSearchAS400);
        if(ZERO == spsMAs400SchemaList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E5_0026, LBL_DENSO_COMPANY);
        }
        if(spsMAs400SchemaList.size() != densoCodeList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E5_0025, LBL_DENSO_COMPANY);
        }
        
        Map<String, String> asnStatusMap = new HashMap<String, String>();
        asnStatusMap.put(Constants.ASN_STATUS_N, Constants.ASN_STATUS_ISS);
        asnStatusMap.put(Constants.ASN_STATUS_D, Constants.ASN_STATUS_CCL);
        asnStatusMap.put(Constants.ASN_STATUS_P, Constants.ASN_STATUS_PTR);
        asnStatusMap.put(Constants.ASN_STATUS_R, Constants.ASN_STATUS_RCP);
        List<AsnDomain> asnResultList = new ArrayList<AsnDomain>();
        
        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        int scale = 0;
        if(!StringUtil.checkNullOrEmpty(companySupplierDomain.getDecimalDisp())){
            scale = Integer.valueOf(companySupplierDomain.getDecimalDisp());
        }
        companyDecimalFormat.setMinimumFractionDigits(scale);
        companyDecimalFormat.setMaximumFractionDigits(scale);
        
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat defaultDecimalFormat = (DecimalFormat)defaultNumberFormat;
        defaultDecimalFormat.setGroupingUsed(true);
        defaultDecimalFormat.setMinimumFractionDigits(Constants.TWO);
        defaultDecimalFormat.setMaximumFractionDigits(Constants.TWO);

        NumberFormat priceNumberFormat = NumberFormat.getInstance();
        DecimalFormat priceDecimalFormat = (DecimalFormat)priceNumberFormat;
        priceDecimalFormat.setGroupingUsed(true);
        priceDecimalFormat.setMinimumFractionDigits(Constants.FOUR);
        priceDecimalFormat.setMaximumFractionDigits(Constants.FOUR);
        
        String zeroStr = StringUtil.setBigDecimalScaleToString(NumberUtil.ZERO, scale);
        BigDecimal totalBaseAmount = new BigDecimal(ZERO);
        Set<String> densoPartErrorStr = new TreeSet<String>();
        for(As400ServerConnectionInformationDomain as400Server : spsMAs400SchemaList){
            
            String asnNoStr = 
                StringUtil.convertListToVarcharCommaSeperate(
                    asnNoMap.get(as400Server.getSpsMCompanyDensoDomain().getDCd()));
            List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
                CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                    asnNoStr, null, as400Server, locale);
            
            List<AsnDomain> asnList = asnResultMap.get(
                as400Server.getSpsMCompanyDensoDomain().getDCd()); 
            Iterator<AsnDomain> iterator = asnList.iterator();
            
            while(iterator.hasNext()) {
                AsnDomain asnItem = iterator.next();
                
                if(!Constants.ASN_STATUS_CCL.equals(asnItem.getAsnStatus())){
                    if(ZERO < cigmaAsnDomainList.size()){
                        for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList){
                            if(asnItem.getAsnNo().trim().equals(
                                cigmaAsnitem.getPseudoAsnNo().trim())){
                                asnItem.setAsnStatus(
                                    asnStatusMap.get(cigmaAsnitem.getPseudoAsnStatus()));
                            }
                        }
                    }
                }
                
                String vendorCodeStr = StringUtil.convertListToVarcharCommaSeperate(
                    asnItem.getSpsTAsnDomain().getVendorCd());
                String densoPartStr = StringUtil.convertListToVarcharCommaSeperate(
                    asnItem.getDPn());
                String plantEtaStr = StringUtil.convertListToVarcharCommaSeperate(
                    DateUtil.format(asnItem.getSpsTAsnDomain().getPlanEta(), 
                        PATTERN_YYYYMMDD_SLASH)
                        .replace(Constants.SYMBOL_SLASH, EMPTY_STRING));
                
                List<PseudoCigmaPurchasePriceMasterDomain> cigmaPriceMasterDomainList = 
                    CommonWebServiceUtil.purchasePriceMasterResourceSearchUnitPrice(
                        vendorCodeStr, densoPartStr, plantEtaStr, as400Server);
                
                for(PseudoCigmaPurchasePriceMasterDomain cigmaPriceMasteritem
                    : cigmaPriceMasterDomainList)
                {
                    if(asnItem.getDPn().trim().equals(
                        cigmaPriceMasteritem.getPseudoPartNo().trim()))
                    {
                        asnItem.setDensoUnitPrice(cigmaPriceMasteritem.getPseudoUnitPrice());
                        asnItem.setDensoCurrencyCd(cigmaPriceMasteritem.getPseudoCurrencyCd());
                        asnItem.setTempPriceFlag(cigmaPriceMasteritem.getPseudoTempPriceFlag());
                        // [IN030] Price Master can return more than 1 record, use first record
                        break; 
                    }
                }

                if(StringUtil.checkNullOrEmpty(asnItem.getDensoUnitPrice())){
                    densoPartErrorStr.add(asnItem.getDPn().trim());
                }

                // [IN009] Rounding Mode use HALF_UP
                // [IN010] Move this section up : calculate Amount after rounding price
                //String unitPrice = 
                //    priceDecimalFormat.format(
                //        NumberUtil.toBigDecimalDefaultZero(asnItem.getDensoUnitPrice()));
                BigDecimal bdUnitPrice = NumberUtil.toBigDecimalDefaultZero(
                    asnItem.getDensoUnitPrice());
                bdUnitPrice = bdUnitPrice.setScale(Constants.FOUR, RoundingMode.HALF_UP);
                String unitPrice = priceDecimalFormat.format(bdUnitPrice);
                asnItem.setDensoUnitPrice(unitPrice);
                asnItem.setSupplierUnitPrice(unitPrice);

                // [IN009] Rounding Mode use HALF_UP
                // [IN010] Move this section up : calculate Amount after rounding qty
                //asnItem.setShippingQty(
                //    defaultDecimalFormat.format(
                //        asnItem.getSpsTAsnDetailDomain().getShippingQty()));
                BigDecimal bdShippingQty = asnItem.getSpsTAsnDetailDomain().getShippingQty()
                    .setScale(Constants.TWO, RoundingMode.HALF_UP);
                asnItem.setShippingQty(defaultDecimalFormat.format(bdShippingQty));
                
                // [IN010] Summary total base amount after rounding
                //totalBaseAmount =
                //    totalBaseAmount.add(asnItem.getSpsTAsnDetailDomain().getShippingQty().multiply(
                //        NumberUtil.toBigDecimalDefaultZero(asnItem.getDensoUnitPrice())));
                BigDecimal densoBaseAmount = bdUnitPrice.multiply(bdShippingQty);
                densoBaseAmount = densoBaseAmount.setScale(scale, RoundingMode.HALF_UP);
                totalBaseAmount = totalBaseAmount.add(densoBaseAmount);
                
                // [IN009] Rounding Mode use HALF_UP
                //asnItem.setDensoBaseAmount(
                //    companyDecimalFormat.format(
                //        NumberUtil.toBigDecimalDefaultZero(
                //            asnItem.getDensoUnitPrice())
                //                .multiply(asnItem.getSpsTAsnDetailDomain().getShippingQty())));
                asnItem.setDensoBaseAmount(
                    companyDecimalFormat.format(densoBaseAmount));

                asnItem.setDensoUnitOfMeasure(asnItem.getSpsTAsnDetailDomain()
                    .getUnitOfMeasure());
                asnItem.setSupplierUnitOfMeasure(asnItem.getSpsTAsnDetailDomain()
                    .getUnitOfMeasure());
                asnItem.setDiffBaseAmount(zeroStr);
                asnItem.setBdDiffBaseAmount(zeroStr);

                asnItem.setDensoDiffBaseAmount(STR_ZERO);
                
            }
            asnResultList.addAll(asnList);
        }
        if(ZERO < densoPartErrorStr.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                ERROR_CD_SP_E6_0013,
                new String[]{
                    StringUtil.appendsString(MessageUtil.getLabelHandledException(locale, 
                        LBL_UNIT_PRICE_OF),
                        SYMBOL_SPACE,
                        densoPartErrorStr.toString()
                            .replace(Constants.SYMBOL_OPEN_SQUARE_BRACKET, EMPTY_STRING)
                            .replace(Constants.SYMBOL_CLOSE_SQUARE_BRACKET, EMPTY_STRING))
                });
        }
        
        if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDPcd())){
            invoiceMaintenanceReturnDomain.setDiffDPcdFlag(Constants.STR_ONE);
            invoiceMaintenanceReturnDomain.setDensoAddress(
                StringUtil.appendsStringDefaultEmptyString(
                    spsMCompanyDensoDomain.getAddress1(), SYMBOL_SPACE,
                    spsMCompanyDensoDomain.getAddress2(), SYMBOL_SPACE,
                    spsMCompanyDensoDomain.getAddress3(), SYMBOL_SPACE));
        }else{
            invoiceMaintenanceReturnDomain.setDiffDPcdFlag(Constants.STR_ZERO);
            SpsMPlantDensoCriteriaDomain plantDensoCriteria = new SpsMPlantDensoCriteriaDomain();
            plantDensoCriteria.setDCd(invoiceMaintenanceDomain.getDCd());
            plantDensoCriteria.setDPcd(invoiceMaintenanceDomain.getDPcd());
            SpsMPlantDensoDomain spsMPlantDenso
                = this.spsMPlantDensoService.searchByKey(plantDensoCriteria);
            invoiceMaintenanceReturnDomain.setDensoAddress(spsMPlantDenso.getAddress());
        }
        invoiceMaintenanceReturnDomain.setDensoName(spsMCompanyDensoDomain.getCompanyName());
        invoiceMaintenanceReturnDomain.setDCd(spsMCompanyDensoDomain.getDCd());
        invoiceMaintenanceReturnDomain.setDPcd(invoiceMaintenanceDomain.getDPcd());
        invoiceMaintenanceReturnDomain.setDensoTaxId(spsMCompanyDensoDomain.getDTaxId());
        invoiceMaintenanceReturnDomain.setDensoCurrency(spsMCompanyDensoDomain.getCurrencyCd());
        invoiceMaintenanceReturnDomain.setSupplierName(companySupplierDomain.getCompanyName());
        invoiceMaintenanceReturnDomain.setSCd(vendorDomainList.get(ZERO).getSCd());
        invoiceMaintenanceReturnDomain.setVendorCd(vendorDomainList.get(ZERO).getVendorCd());
        invoiceMaintenanceReturnDomain.setSupplierTaxId(companySupplierDomain.getSupplierTaxId());
        invoiceMaintenanceReturnDomain.setSupplierAddress(
            StringUtil.appendsStringDefaultEmptyString(
                companySupplierDomain.getAddress1(), SYMBOL_SPACE,
                companySupplierDomain.getAddress2(), SYMBOL_SPACE,
                companySupplierDomain.getAddress3(), SYMBOL_SPACE));
        invoiceMaintenanceReturnDomain.setSupplierAddress1(companySupplierDomain.getAddress1());
        invoiceMaintenanceReturnDomain.setSupplierAddress2(companySupplierDomain.getAddress2());
        invoiceMaintenanceReturnDomain.setSupplierAddress3(companySupplierDomain.getAddress3());
        invoiceMaintenanceReturnDomain.setSupplierCurrency(companySupplierDomain.getCurrencyCd());
        invoiceMaintenanceReturnDomain.setDecimalDisp(String.valueOf(scale));
        invoiceMaintenanceReturnDomain.setVatRate(
            NumberUtil.toBigDecimal(companySupplierDomain.getTaxRate()).toString());
        
        /* [IN059] If first character of Tax Code is 'N' then set to None Vat.
         * If first character is Tax Code is 'V' then set to Vat/GST
         * */
        //invoiceMaintenanceReturnDomain.setVatType(STR_ZERO);
        if (null != companySupplierDomain.getTaxCd()) {
            if(Constants.STR_N.equals(companySupplierDomain.getTaxCd().substring(
                Constants.ZERO, Constants.ONE)))
            {
                invoiceMaintenanceReturnDomain.setVatType(STR_ZERO);
            } else {
                invoiceMaintenanceReturnDomain.setVatType(STR_ONE);
            }
        }
        
        invoiceMaintenanceReturnDomain.setInvoiceNo(null);
        invoiceMaintenanceReturnDomain.setInvoiceDate(
            DateUtil.format(this.commonService.searchSysDate(), PATTERN_YYYYMMDD_SLASH));
        invoiceMaintenanceReturnDomain.setSupplierBaseAmount(null);
        invoiceMaintenanceReturnDomain.setSupplierVatAmount(null);
        invoiceMaintenanceReturnDomain.setSupplierTotalAmount(null);
        invoiceMaintenanceReturnDomain.setCnNo(null);
        invoiceMaintenanceReturnDomain.setCnDate(null);
        invoiceMaintenanceReturnDomain.setCnBaseAmount(null);
        invoiceMaintenanceReturnDomain.setCnVatAmount(null);
        invoiceMaintenanceReturnDomain.setCnTotalAmount(null);
        invoiceMaintenanceReturnDomain.setTotalDiffBaseAmount(zeroStr);
        invoiceMaintenanceReturnDomain.setTotalDiffVatAmount(zeroStr);
        invoiceMaintenanceReturnDomain.setTotalDiffAmount(zeroStr);
        invoiceMaintenanceReturnDomain.setTotalBaseAmount(
            companyDecimalFormat.format(totalBaseAmount.setScale(scale, RoundingMode.HALF_UP)));
        BigDecimal totalVatAmount = new BigDecimal(companySupplierDomain.getTaxRate());
        totalVatAmount = (totalVatAmount.multiply(totalBaseAmount))
            .divide(new BigDecimal(Constants.ONE_HUNDRED)).setScale(scale, RoundingMode.HALF_UP);
        invoiceMaintenanceReturnDomain.setTotalVatAmount(
            companyDecimalFormat.format(totalVatAmount));
        invoiceMaintenanceReturnDomain.setTotalAmount(
            companyDecimalFormat.format(totalBaseAmount.add(totalVatAmount)
                .setScale(scale, RoundingMode.HALF_UP)));
        SortUtil.sort(asnResultList, SortUtil.COMPARE_ASN_DETAIL_INFORMATION);
        invoiceMaintenanceReturnDomain.setAsnDetailList(asnResultList);
        invoiceMaintenanceReturnDomain.setFileId(Constants.EMPTY_STRING);
        
        if(null != invoiceMaintenanceDomain.getWarningMessage()){
            List<ApplicationMessageDomain> applicationException 
                = new ArrayList<ApplicationMessageDomain>();
            applicationException.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
                    invoiceMaintenanceDomain.getWarningMessage()));
            invoiceMaintenanceReturnDomain.setWarningMessageList(applicationException);
        }
        
        return invoiceMaintenanceReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchInitialGroupInvoiceByCsv(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public InvoiceMaintenanceDomain searchInitialGroupInvoiceByCsv(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws ApplicationException
    {
        Locale locale = invoiceMaintenanceDomain.getLocale();
        InvoiceMaintenanceDomain invoiceMaintenanceReturnDomain 
            = new InvoiceMaintenanceDomain();
        
        if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getSessionId())){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E7_0007, LBL_SESSION_ID);
        }else  if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDscId())){
            MessageUtil.throwsApplicationMessageWithLabel(locale, ERROR_CD_SP_E7_0007, LBL_DSC_ID);
        }
        
        MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
        miscLimitDomain.setMiscType(MISC_TYPE_SCR_REC_LM );
        miscLimitDomain.setMiscCode(MISC_CODE_WINV004_RLM);
        Integer recordLimit = miscService.searchMiscValue(miscLimitDomain);
        if(null == recordLimit){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                ERROR_CD_SP_E6_0032);
        }
        
        TmpUploadInvoiceDomain criteria = new TmpUploadInvoiceDomain();
        criteria.setSessionId(invoiceMaintenanceDomain.getSessionId());
        criteria.setDscId(invoiceMaintenanceDomain.getDscId());
        List<TmpUploadInvoiceDomain> result 
            = this.tmpUploadInvoiceService.searchTmpUploadInvoice(criteria);
        if(ZERO == result.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0001);
        }else if(recordLimit < result.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0003, 
                new String[]{String.valueOf(recordLimit)});
        }
        
        boolean isDiffDPcd = false;
        String dPcd = Constants.EMPTY_STRING;
        Set<String> densoCodeList = new TreeSet<String>();
        Map<String, List<TmpUploadInvoiceDomain>> tmpUploadInvoiceResultMap 
            = new HashMap<String, List<TmpUploadInvoiceDomain>>();
        Map<String, Set<String>> asnNoMap = new HashMap<String, Set<String>>();
        for(TmpUploadInvoiceDomain tmpUploadInvoiceItem : result){
            Set<String> asnNoList = new TreeSet<String>();
            for(AsnDomain asnItem : tmpUploadInvoiceItem.getAsnDtailList()){
                asnNoList.add(asnItem.getAsnNo());
                
                //Get DENSO Plant Code
                if(StringUtil.checkNullOrEmpty(dPcd)){
                    dPcd = asnItem.getSpsTAsnDomain().getDPcd();
                }else{
                    if(!dPcd.equals(asnItem.getSpsTAsnDomain().getDPcd()) && !isDiffDPcd){
                        isDiffDPcd = true;
                    }
                }
            }
            
            String densoCode = tmpUploadInvoiceItem.getDCd();
            densoCodeList.add(densoCode);
            if(tmpUploadInvoiceResultMap.containsKey(densoCode)){
                tmpUploadInvoiceResultMap.get(densoCode).add(tmpUploadInvoiceItem);
                asnNoMap.get(densoCode).addAll(asnNoList);
            }else{
                List<TmpUploadInvoiceDomain> tmpUploadInvoiceList 
                    = new ArrayList<TmpUploadInvoiceDomain>();
                tmpUploadInvoiceList.add(tmpUploadInvoiceItem);
                tmpUploadInvoiceResultMap.put(densoCode, tmpUploadInvoiceList);
                asnNoMap.put(densoCode, asnNoList);
            }
        }
        
        String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCodeList);
        SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
            = new SpsMCompanyDensoDomain();
        companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
        List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
            = this.companyDensoService.searchAs400ServerList(
                companyDensoCriteriaForSearchAS400);
        if(ZERO == spsMAs400SchemaList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E5_0026, LBL_DENSO_COMPANY);
        }
        if(spsMAs400SchemaList.size() != densoCodeList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E5_0025, LBL_DENSO_COMPANY);
        }
        
        Map<String, String> asnStatusMap = new HashMap<String, String>();
        asnStatusMap.put(Constants.ASN_STATUS_N, Constants.ASN_STATUS_ISS);
        asnStatusMap.put(Constants.ASN_STATUS_D, Constants.ASN_STATUS_CCL);
        asnStatusMap.put(Constants.ASN_STATUS_P, Constants.ASN_STATUS_PTR);
        asnStatusMap.put(Constants.ASN_STATUS_R, Constants.ASN_STATUS_RCP);
        
        TmpUploadInvoiceDomain tmpUploadInvoice = result.get(ZERO);
        
        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        int scale = 0;
        if(!StringUtil.checkNullOrEmpty(tmpUploadInvoice.getDecimalDisp())){
            scale = Integer.valueOf(tmpUploadInvoice.getDecimalDisp());
        }
        companyDecimalFormat.setMinimumFractionDigits(scale);
        companyDecimalFormat.setMaximumFractionDigits(scale);
        
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat defaultDecimalFormat = (DecimalFormat)defaultNumberFormat;
        defaultDecimalFormat.setGroupingUsed(true);
        defaultDecimalFormat.setMinimumFractionDigits(Constants.TWO);
        defaultDecimalFormat.setMaximumFractionDigits(Constants.TWO);

        NumberFormat priceNumberFormat = NumberFormat.getInstance();
        DecimalFormat priceDecimalFormat = (DecimalFormat)priceNumberFormat;
        priceDecimalFormat.setGroupingUsed(true);
        priceDecimalFormat.setMinimumFractionDigits(Constants.FOUR);
        priceDecimalFormat.setMaximumFractionDigits(Constants.FOUR);
        
        BigDecimal totalDiffBaseAmount = new BigDecimal(ZERO);
        BigDecimal totalBaseAmount = new BigDecimal(ZERO);
        BigDecimal totalVatAmount =  new BigDecimal(ZERO);
        Set<String> densoPartErrorStr = new TreeSet<String>();
        for(As400ServerConnectionInformationDomain as400Server : spsMAs400SchemaList){
            
            String asnNoStr = 
                StringUtil.convertListToVarcharCommaSeperate(
                    asnNoMap.get(as400Server.getSpsMCompanyDensoDomain().getDCd()));
            List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
                CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                    asnNoStr, null, as400Server, locale);
            
            List<TmpUploadInvoiceDomain> tmpUploadInvoiceList 
                = tmpUploadInvoiceResultMap.get(
                    as400Server.getSpsMCompanyDensoDomain().getDCd()); 
            Iterator<TmpUploadInvoiceDomain> iterator = tmpUploadInvoiceList.iterator();
            while(iterator.hasNext()) {
                TmpUploadInvoiceDomain tmpUploadInvoiceItem = iterator.next();
                for(AsnDomain asnItem : tmpUploadInvoiceItem.getAsnDtailList()){
                    if(!Constants.ASN_STATUS_CCL.equals(asnItem.getAsnStatus())){
                        if(ZERO < cigmaAsnDomainList.size()){
                            for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList){
                                if(asnItem.getAsnNo().trim().equals(
                                    cigmaAsnitem.getPseudoAsnNo().trim())){
                                    asnItem.setAsnStatus(asnStatusMap.get(
                                        cigmaAsnitem.getPseudoAsnStatus()));
                                }
                            }
                        }
                    }
                    
                    String vendorCodeStr = StringUtil.convertListToVarcharCommaSeperate(
                        tmpUploadInvoiceItem.getVendorCd());
                    String densoPartStr = StringUtil.convertListToVarcharCommaSeperate(
                        asnItem.getDPn());
                    String plantEtaStr = StringUtil.convertListToVarcharCommaSeperate(
                        DateUtil.format(asnItem.getSpsTmpUploadInvoiceDomain().getPlanEta(), 
                            PATTERN_YYYYMMDD_SLASH).replace(Constants.SYMBOL_SLASH, EMPTY_STRING));
                    
                    List<PseudoCigmaPurchasePriceMasterDomain> cigmaPriceMasterDomainList = 
                        CommonWebServiceUtil.purchasePriceMasterResourceSearchUnitPrice(
                            vendorCodeStr, densoPartStr, plantEtaStr, as400Server);
                    
                    for(PseudoCigmaPurchasePriceMasterDomain cigmaPriceMasteritem 
                        : cigmaPriceMasterDomainList)
                    {
                        if(asnItem.getDPn().trim().equals(
                            cigmaPriceMasteritem.getPseudoPartNo().trim()))
                        {
                            asnItem.setDensoUnitPrice(cigmaPriceMasteritem.getPseudoUnitPrice());
                            asnItem.setDensoCurrencyCd(cigmaPriceMasteritem.getPseudoCurrencyCd());
                            asnItem.setTempPriceFlag(cigmaPriceMasteritem.getPseudoTempPriceFlag());
                            // [IN030] Price Master can return more than 1 record, use first record
                            break;
                        }
                    }
                    
                    if(StringUtil.checkNullOrEmpty(asnItem.getDensoUnitPrice())){
                        densoPartErrorStr.add(asnItem.getDPn().trim());
                    }

                    // [IN009] Rounding Mode use HALF_UP
                    //asnItem.setDensoUnitPrice(priceDecimalFormat.format(
                    //    NumberUtil.toBigDecimalDefaultZero(asnItem.getDensoUnitPrice())));
                    //asnItem.setSupplierUnitPrice(
                    //    priceDecimalFormat.format(asnItem.getBdSupplierUnitPrice()));
                    BigDecimal bdDUnitPrice = NumberUtil.toBigDecimalDefaultZero(
                        asnItem.getDensoUnitPrice());
                    bdDUnitPrice = bdDUnitPrice.setScale(Constants.FOUR, RoundingMode.HALF_UP);
                    asnItem.setDensoUnitPrice(priceDecimalFormat.format(bdDUnitPrice));
                    BigDecimal bdSUnitPrice = asnItem.getBdSupplierUnitPrice().setScale(
                        Constants.FOUR, RoundingMode.HALF_UP);
                    asnItem.setSupplierUnitPrice(priceDecimalFormat.format(bdSUnitPrice));
                    
                    // [IN009] Rounding Mode use HALF_UP
                    // [IN010] Move this section up : calculate Amount after rounding qty
                    //asnItem.setShippingQty(defaultDecimalFormat.format(
                    //    asnItem.getSpsTmpUploadInvoiceDomain().getShippingQty()));
                    BigDecimal bdShippingQty = asnItem.getSpsTmpUploadInvoiceDomain()
                        .getShippingQty().setScale(Constants.TWO, RoundingMode.HALF_UP);
                    asnItem.setShippingQty(defaultDecimalFormat.format(bdShippingQty));
                    
                    // [IN009] Rounding Mode use HALF_UP
                    // [IN010] Calculate total difference base amount after rounding
                    //BigDecimal densoUnitPrice 
                    //    = NumberUtil.toBigDecimalDefaultZero(asnItem.getDensoUnitPrice());
                    //BigDecimal diffBaseAmount = 
                    //    densoUnitPrice
                    //        .subtract(asnItem.getBdSupplierUnitPrice())
                    //        .multiply(asnItem.getSpsTmpUploadInvoiceDomain().getShippingQty());
                    //asnItem.setBdDiffBaseAmount(diffBaseAmount.toString());
                    //asnItem.setDiffBaseAmount(companyDecimalFormat.format(diffBaseAmount));
                    BigDecimal diffBaseAmount = bdDUnitPrice.subtract(bdSUnitPrice)
                            .multiply(bdShippingQty);
                    diffBaseAmount = diffBaseAmount.setScale(scale, RoundingMode.HALF_UP);
                    asnItem.setBdDiffBaseAmount(diffBaseAmount.toString());
                    asnItem.setDiffBaseAmount(companyDecimalFormat.format(diffBaseAmount));
                    
                    // [IN010] Calculate densoBaseAmount after rounding
                    //BigDecimal densoBaseAmount = 
                    //    densoUnitPrice
                    //        .multiply(asnItem.getSpsTmpUploadInvoiceDomain().getShippingQty());
                    BigDecimal densoBaseAmount = bdDUnitPrice
                        .multiply(asnItem.getSpsTmpUploadInvoiceDomain().getShippingQty());
                    
                    // [IN009] Rounding Mode use HALF_UP
                    //asnItem.setDensoBaseAmount(companyDecimalFormat.format(densoBaseAmount));
                    densoBaseAmount = densoBaseAmount.setScale(scale, RoundingMode.HALF_UP);
                    asnItem.setDensoBaseAmount(companyDecimalFormat.format(densoBaseAmount));

                    totalDiffBaseAmount = totalDiffBaseAmount.add(diffBaseAmount);
                    totalBaseAmount = totalBaseAmount.add(densoBaseAmount);
                }
            }
        }
        if(ZERO < densoPartErrorStr.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                ERROR_CD_SP_E6_0013,
                new String[]{
                    StringUtil.appendsString(MessageUtil.getLabelHandledException(locale, 
                        LBL_UNIT_PRICE_OF),
                        SYMBOL_SPACE,
                        densoPartErrorStr.toString()
                            .replace(Constants.SYMBOL_OPEN_SQUARE_BRACKET, EMPTY_STRING)
                            .replace(Constants.SYMBOL_CLOSE_SQUARE_BRACKET, EMPTY_STRING))
                });
        }
        
        if(isDiffDPcd){
            dPcd = Constants.EMPTY_STRING;
            invoiceMaintenanceReturnDomain.setDiffDPcdFlag(Constants.STR_ONE);
            invoiceMaintenanceReturnDomain.setDensoAddress(
                StringUtil.appendsStringDefaultEmptyString(
                    tmpUploadInvoice.getDensoAddress1(), SYMBOL_SPACE,
                    tmpUploadInvoice.getDensoAddress2(), SYMBOL_SPACE,
                    tmpUploadInvoice.getDensoAddress3(), SYMBOL_SPACE));
        }else{
            invoiceMaintenanceReturnDomain.setDiffDPcdFlag(Constants.STR_ZERO);
            invoiceMaintenanceReturnDomain.setDensoAddress(tmpUploadInvoice.getDensoAddress());
        }
        
        invoiceMaintenanceReturnDomain.setDensoName(tmpUploadInvoice.getCompanyDensoName());
        invoiceMaintenanceReturnDomain.setDCd(tmpUploadInvoice.getDCd());
        invoiceMaintenanceReturnDomain.setDPcd(dPcd);
        invoiceMaintenanceReturnDomain.setDensoTaxId(tmpUploadInvoice.getDensoTaxId());
        invoiceMaintenanceReturnDomain.setDensoCurrency(tmpUploadInvoice.getDensoCurrencyCd());
        invoiceMaintenanceReturnDomain.setSupplierName(
            tmpUploadInvoice.getCompanySupplierName());
        invoiceMaintenanceReturnDomain.setSCd(tmpUploadInvoice.getSCd());
        invoiceMaintenanceReturnDomain.setVendorCd(tmpUploadInvoice.getVendorCd());
        invoiceMaintenanceReturnDomain.setSupplierTaxId(tmpUploadInvoice.getSupplierTaxId());
        invoiceMaintenanceReturnDomain.setSupplierAddress(
            StringUtil.appendsStringDefaultEmptyString(
                tmpUploadInvoice.getSupplierAddress1(), SYMBOL_SPACE,
                tmpUploadInvoice.getSupplierAddress2(), SYMBOL_SPACE,
                tmpUploadInvoice.getSupplierAddress3(), SYMBOL_SPACE));
        invoiceMaintenanceReturnDomain.setSupplierAddress1(tmpUploadInvoice.getSupplierAddress1());
        invoiceMaintenanceReturnDomain.setSupplierAddress2(tmpUploadInvoice.getSupplierAddress2());
        invoiceMaintenanceReturnDomain.setSupplierAddress3(tmpUploadInvoice.getSupplierAddress3());
        invoiceMaintenanceReturnDomain.setSupplierCurrency(
            tmpUploadInvoice.getSupplierCurrencyCd());
        invoiceMaintenanceReturnDomain.setDecimalDisp(String.valueOf(scale));
        invoiceMaintenanceReturnDomain.setVatRate(
            NumberUtil.toBigDecimal(tmpUploadInvoice.getVatRate()).toString());
        invoiceMaintenanceReturnDomain.setVatType(tmpUploadInvoice.getVatType());
        invoiceMaintenanceReturnDomain.setInvoiceNo(tmpUploadInvoice.getInvoiceNo());
        invoiceMaintenanceReturnDomain.setInvoiceDate(tmpUploadInvoice.getInvoiceDate());
        
        // [IN009] Rounding Mode use HALF_UP
        //invoiceMaintenanceReturnDomain.setSupplierBaseAmount(
        //    companyDecimalFormat.format(tmpUploadInvoice.getBaseAmount()));
        //invoiceMaintenanceReturnDomain.setSupplierVatAmount(
        //    companyDecimalFormat.format(tmpUploadInvoice.getVatAmount()));
        //invoiceMaintenanceReturnDomain.setSupplierTotalAmount(
        //    companyDecimalFormat.format(tmpUploadInvoice.getTotalAmount()));
        invoiceMaintenanceReturnDomain.setSupplierBaseAmount(
            NumberUtil.formatRoundingHalfUp(companyDecimalFormat
                , tmpUploadInvoice.getBaseAmount(), scale));
        invoiceMaintenanceReturnDomain.setSupplierVatAmount(
            NumberUtil.formatRoundingHalfUp(companyDecimalFormat
                , tmpUploadInvoice.getVatAmount(), scale));
        invoiceMaintenanceReturnDomain.setSupplierTotalAmount(
            NumberUtil.formatRoundingHalfUp(companyDecimalFormat
                , tmpUploadInvoice.getTotalAmount(), scale));
        
        invoiceMaintenanceReturnDomain.setCnNo(tmpUploadInvoice.getCnNo());
        invoiceMaintenanceReturnDomain.setCnDate(tmpUploadInvoice.getCnDate());
        if(null != tmpUploadInvoice.getCnBaseAmount()){
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceReturnDomain.setCnBaseAmount(
            //    companyDecimalFormat.format(tmpUploadInvoice.getCnBaseAmount()));
            invoiceMaintenanceReturnDomain.setCnBaseAmount(
                NumberUtil.formatRoundingHalfUp(companyDecimalFormat
                    , tmpUploadInvoice.getCnBaseAmount(), scale));
        }else{
            invoiceMaintenanceReturnDomain.setCnBaseAmount(null);
        }
        if(null != tmpUploadInvoice.getCnVatAmount()){
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceReturnDomain.setCnVatAmount(
            //    companyDecimalFormat.format(tmpUploadInvoice.getCnVatAmount()));
            invoiceMaintenanceReturnDomain.setCnVatAmount(
                NumberUtil.formatRoundingHalfUp(companyDecimalFormat
                    , tmpUploadInvoice.getCnVatAmount(), scale));
        }else{
            invoiceMaintenanceReturnDomain.setCnVatAmount(null);
        }
        if(null != tmpUploadInvoice.getCnTotalAmount()){
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceReturnDomain.setCnTotalAmount(
            //    companyDecimalFormat.format(tmpUploadInvoice.getCnTotalAmount()));
            invoiceMaintenanceReturnDomain.setCnTotalAmount(
                NumberUtil.formatRoundingHalfUp(companyDecimalFormat
                    , tmpUploadInvoice.getCnTotalAmount(), scale));
        }else{
            invoiceMaintenanceReturnDomain.setCnTotalAmount(null);
        }
        
        invoiceMaintenanceReturnDomain.setTotalDiffBaseAmount(
            companyDecimalFormat.format(totalDiffBaseAmount));
        BigDecimal totalDiffVatAmount = totalDiffBaseAmount.multiply(
            NumberUtil.toBigDecimalDefaultZero(tmpUploadInvoice.getVatRate()))
            .divide(new BigDecimal(Constants.ONE_HUNDRED).setScale(scale, RoundingMode.HALF_UP));
        invoiceMaintenanceReturnDomain.setTotalDiffVatAmount(
            companyDecimalFormat.format(totalDiffVatAmount));
        invoiceMaintenanceReturnDomain.setTotalDiffAmount(
            companyDecimalFormat.format(totalDiffBaseAmount.add(totalDiffVatAmount)
                .setScale(scale, RoundingMode.HALF_UP)));
        invoiceMaintenanceReturnDomain.setTotalBaseAmount(
            companyDecimalFormat.format(totalBaseAmount));
        totalVatAmount = (totalBaseAmount.multiply(
                NumberUtil.toBigDecimalDefaultZero(tmpUploadInvoice.getVatRate())))
                .divide(new BigDecimal(Constants.ONE_HUNDRED)
                    .setScale(scale, RoundingMode.HALF_UP));
        invoiceMaintenanceReturnDomain.setTotalVatAmount(
            companyDecimalFormat.format(totalVatAmount));
        invoiceMaintenanceReturnDomain.setTotalAmount(
            companyDecimalFormat.format(totalBaseAmount.add(totalVatAmount)
                .setScale(scale, RoundingMode.HALF_UP)));
        
        SortUtil.sort(tmpUploadInvoice.getAsnDtailList(), SortUtil.COMPARE_ASN_DETAIL_INFORMATION);
        invoiceMaintenanceReturnDomain.setAsnDetailList(tmpUploadInvoice.getAsnDtailList());
        invoiceMaintenanceReturnDomain.setFileId(Constants.EMPTY_STRING);
        
        if(null != invoiceMaintenanceDomain.getWarningMessage()){
            List<ApplicationMessageDomain> applicationException 
                = new ArrayList<ApplicationMessageDomain>();
            applicationException.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING, 
                    invoiceMaintenanceDomain.getWarningMessage()));
            invoiceMaintenanceReturnDomain.setWarningMessageList(applicationException);
        }
        return invoiceMaintenanceReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchInitialViewInvoiceMaintenance(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public InvoiceMaintenanceDomain searchInitialViewInvoiceMaintenance(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws ApplicationException {
        
        Locale locale = invoiceMaintenanceDomain.getLocale();
        InvoiceMaintenanceDomain invoiceMaintenanceReturnDomain 
            = new InvoiceMaintenanceDomain();
        
        if(StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getInvoiceId())){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E7_0007, LBL_INVOICE_ID);
        }
        
        MiscellaneousDomain miscLimitDomain = new MiscellaneousDomain();
        miscLimitDomain.setMiscType(MISC_TYPE_SCR_REC_LM );
        miscLimitDomain.setMiscCode(MISC_CODE_WINV004_RLM);
        Integer recordLimit = miscService.searchMiscValue(miscLimitDomain);
        if(null == recordLimit){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0032);
        }
        
        InvoiceInformationDomain criteria = new InvoiceInformationDomain();
        criteria.getSpsTInvoiceDomain().setInvoiceId(new BigDecimal(
            invoiceMaintenanceDomain.getInvoiceId()));
        List<InvoiceInformationDomain> invoiceInformationList
            = this.invoiceService.searchInvoiceInformation(criteria);
        if(null == invoiceInformationList){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0001);
        }else if(recordLimit
            < invoiceInformationList.get(ZERO).getInvoiceDetailDomainList().size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_E6_0003, 
                new String[]{String.valueOf(recordLimit)});
        }
        
        InvoiceInformationDomain invoiceItem = invoiceInformationList.get(ZERO);
        
        Set<String> asnNoList = new TreeSet<String>();
        String densoCode = invoiceItem.getSpsTInvoiceDomain().getDCd();
        //String vendorCode = invoiceItem.getSpsTInvoiceDomain().getVendorCd();
        for(InvoiceDetailDomain invoiceDetailItem:invoiceItem.getInvoiceDetailDomainList()){
            asnNoList.add(invoiceDetailItem.getSpsTInvoiceDetailDomain().getAsnNo());
        }
        
        String densoCodeStr = StringUtil.convertListToVarcharCommaSeperate(densoCode);
        SpsMCompanyDensoDomain companyDensoCriteriaForSearchAS400 
            = new SpsMCompanyDensoDomain();
        companyDensoCriteriaForSearchAS400.setDCd(densoCodeStr);
        List<As400ServerConnectionInformationDomain> spsMAs400SchemaList
            = this.companyDensoService.searchAs400ServerList(
                companyDensoCriteriaForSearchAS400);
        if(ZERO == spsMAs400SchemaList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E5_0026, LBL_DENSO_COMPANY);
        }
        if(ONE != spsMAs400SchemaList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E5_0025, LBL_DENSO_COMPANY);
        }
        
        Map<String, String> asnStatusMap = new HashMap<String, String>();
        asnStatusMap.put(Constants.ASN_STATUS_N, Constants.ASN_STATUS_ISS);
        asnStatusMap.put(Constants.ASN_STATUS_D, Constants.ASN_STATUS_CCL);
        asnStatusMap.put(Constants.ASN_STATUS_P, Constants.ASN_STATUS_PTR);
        asnStatusMap.put(Constants.ASN_STATUS_R, Constants.ASN_STATUS_RCP);

        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        int scale = 0;
        if(!StringUtil.checkNullOrEmpty(invoiceItem.getDecimalDisp())){
            scale = Integer.valueOf(invoiceItem.getDecimalDisp());
        }
        companyDecimalFormat.setMinimumFractionDigits(scale);
        companyDecimalFormat.setMaximumFractionDigits(scale);
        
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat defaultDecimalFormat = (DecimalFormat)defaultNumberFormat;
        defaultDecimalFormat.setGroupingUsed(true);
        defaultDecimalFormat.setMinimumFractionDigits(Constants.TWO);
        defaultDecimalFormat.setMaximumFractionDigits(Constants.TWO);

        NumberFormat priceNumberFormat = NumberFormat.getInstance();
        DecimalFormat priceDecimalFormat = (DecimalFormat)priceNumberFormat;
        priceDecimalFormat.setGroupingUsed(true);
        priceDecimalFormat.setMinimumFractionDigits(Constants.FOUR);
        priceDecimalFormat.setMaximumFractionDigits(Constants.FOUR);
        
        List<AsnDomain> asnDomainList = new ArrayList<AsnDomain>();
        Set<String> densoPartErrorStr = new TreeSet<String>();
        
        As400ServerConnectionInformationDomain as400Server 
            = spsMAs400SchemaList.get(ZERO);
        String asnNoStr = 
            StringUtil.convertListToVarcharCommaSeperate(asnNoList);
        List<PseudoCigmaAsnDomain> cigmaAsnDomainList = 
            CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                asnNoStr, null, as400Server, locale);
        
        //String vendorCodeStr = StringUtil.convertListToVarcharCommaSeperate(vendorCode);
        for(InvoiceDetailDomain invoiceDetailItem : invoiceItem.getInvoiceDetailDomainList()){
            //set default ASN Status
            invoiceDetailItem.setAsnStatus(invoiceDetailItem.getSpsTAsnDomain().getAsnStatus());
            if(!Constants.ASN_STATUS_CCL.equals(
                invoiceDetailItem.getSpsTAsnDomain().getAsnStatus()))
            {
                if(ZERO < cigmaAsnDomainList.size()){
                    for(PseudoCigmaAsnDomain cigmaAsnitem : cigmaAsnDomainList){
                        if(invoiceDetailItem.getSpsTInvoiceDetailDomain().getAsnNo().trim().equals(
                            cigmaAsnitem.getPseudoAsnNo().trim())){
                            invoiceDetailItem.setAsnStatus(
                                asnStatusMap.get(cigmaAsnitem.getPseudoAsnStatus()));
                        }
                    }
                }
            }
            
            //String densoPartStr = StringUtil.convertListToVarcharCommaSeperate(
            //    invoiceDetailItem.getSpsTInvoiceDetailDomain().getDPn());
            //String plantEtaStr = StringUtil.convertListToVarcharCommaSeperate(DateUtil.format(
            //    invoiceDetailItem.getSpsTAsnDomain().getPlanEta(), PATTERN_YYYYMMDD_SLASH)
            //    .replace(Constants.SYMBOL_SLASH, EMPTY_STRING));
            
            //List<PseudoCigmaPurchasePriceMasterDomain> cigmaPriceMasterDomainList = 
            //    CommonWebServiceUtil.purchasePriceMasterResourceSearchUnitPrice(
            //        vendorCodeStr, densoPartStr, plantEtaStr, as400Server);
            
            AsnDomain asnDetail = new AsnDomain();
            //for(PseudoCigmaPurchasePriceMasterDomain cigmaPriceMasteritem 
            //    : cigmaPriceMasterDomainList)
            //{
            //    if(invoiceDetailItem.getSpsTInvoiceDetailDomain().getDPn().equals(
            //        cigmaPriceMasteritem.getPseudoPartNo()))
            //    {
            //        asnDetail.setDensoUnitPrice(cigmaPriceMasteritem.getPseudoUnitPrice());
            //        asnDetail.setDensoCurrencyCd(cigmaPriceMasteritem.getPseudoCurrencyCd());
            //        asnDetail.setTempPriceFlag(cigmaPriceMasteritem.getPseudoTempPriceFlag());
            //    }
            //}
            
            SpsTInvoiceDetailDomain spsTInvoiceDetail 
                = invoiceDetailItem.getSpsTInvoiceDetailDomain();
            asnDetail.setAsnNo(spsTInvoiceDetail.getAsnNo());
            asnDetail.setSpsDoNo(spsTInvoiceDetail.getSpsDoNo());
            asnDetail.setRevision(spsTInvoiceDetail.getRevision());
            asnDetail.setTripNo(spsTInvoiceDetail.getTripNo());
            asnDetail.setAsnStatus(invoiceDetailItem.getAsnStatus());
            asnDetail.setDPn(spsTInvoiceDetail.getDPn());
            asnDetail.setDensoUnitOfMeasure(spsTInvoiceDetail.getDUnitOfMeasure());
            asnDetail.setTempPriceFlag(spsTInvoiceDetail.getTmpPriceFlg());
            asnDetail.setSupplierUnitOfMeasure(spsTInvoiceDetail.getSUnitOfMeasure());
            asnDetail.setSPn(spsTInvoiceDetail.getSPn());
            asnDetail.setBdDiffBaseAmount(spsTInvoiceDetail.getDiffBaseAmt().toString());

            // [IN009] Rounding Mode use HALF_UP
            //asnDetail.setDensoUnitPrice(
            //    priceDecimalFormat.format(spsTInvoiceDetail.getDUnitPrice()));
            //asnDetail.setSupplierUnitPrice(
            //    priceDecimalFormat.format(spsTInvoiceDetail.getSUnitPrice()));
            //asnDetail.setShippingQty(defaultDecimalFormat.format(
            //    spsTInvoiceDetail.getShippingQty()));
            //asnDetail.setDiffBaseAmount(
            //    companyDecimalFormat.format(spsTInvoiceDetail.getDiffBaseAmt()));
            //asnDetail.setDensoBaseAmount(companyDecimalFormat.format(
            //    spsTInvoiceDetail.getBaseAmt()));
            asnDetail.setDensoUnitPrice(NumberUtil.formatRoundingHalfUp(
                priceDecimalFormat, spsTInvoiceDetail.getDUnitPrice(), Constants.FOUR));
            asnDetail.setSupplierUnitPrice(NumberUtil.formatRoundingHalfUp(
                priceDecimalFormat, spsTInvoiceDetail.getSUnitPrice(), Constants.FOUR));
            asnDetail.setShippingQty(NumberUtil.formatRoundingHalfUp(
                defaultDecimalFormat, spsTInvoiceDetail.getShippingQty(), Constants.TWO));
            asnDetail.setDiffBaseAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, spsTInvoiceDetail.getDiffBaseAmt(), scale));
            asnDetail.setDensoBaseAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, spsTInvoiceDetail.getBaseAmt(), scale));
                
            asnDetail.getSpsTAsnDomain().setCreateDatetime(
                invoiceDetailItem.getSpsTAsnDomain().getCreateDatetime());;
            asnDomainList.add(asnDetail);
            
            if(StringUtil.checkNullOrEmpty(asnDetail.getDensoUnitPrice())){
                densoPartErrorStr.add(
                    invoiceDetailItem.getSpsTInvoiceDetailDomain().getDPn().trim());
            }
        }
        if(ZERO < densoPartErrorStr.size()){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                ERROR_CD_SP_E6_0013,
                new String[]{ StringUtil.appendsString(MessageUtil.getLabelHandledException(locale, 
                    LBL_UNIT_PRICE_OF), densoPartErrorStr.toString())});
        }
        
        SpsTInvoiceDomain spsTInvoiceDomain = invoiceItem.getSpsTInvoiceDomain();
        SpsTCnDomain spsTCnDomain = invoiceItem.getSpsTCnDomain();
        invoiceMaintenanceReturnDomain.setDensoName(spsTInvoiceDomain.getDCompanyName());
        invoiceMaintenanceReturnDomain.setDCd(spsTInvoiceDomain.getDCd());
        invoiceMaintenanceReturnDomain.setDPcd(spsTInvoiceDomain.getDPcd());
        invoiceMaintenanceReturnDomain.setDensoTaxId(spsTInvoiceDomain.getDTaxId());
        invoiceMaintenanceReturnDomain.setDensoAddress(spsTInvoiceDomain.getDpAddress());
        invoiceMaintenanceReturnDomain.setDensoCurrency(spsTInvoiceDomain.getDCurrencyCd());
        invoiceMaintenanceReturnDomain.setSupplierName(spsTInvoiceDomain.getSCompanyName());
        invoiceMaintenanceReturnDomain.setSCd(spsTInvoiceDomain.getSCd());
        invoiceMaintenanceReturnDomain.setVendorCd(spsTInvoiceDomain.getVendorCd());
        invoiceMaintenanceReturnDomain.setSupplierTaxId(spsTInvoiceDomain.getSTaxId());
        invoiceMaintenanceReturnDomain.setSupplierAddress(
            StringUtil.appendsStringDefaultEmptyString(
                spsTInvoiceDomain.getSAddress1(), SYMBOL_SPACE,
                spsTInvoiceDomain.getSAddress2(), SYMBOL_SPACE,
                spsTInvoiceDomain.getSAddress3(), SYMBOL_SPACE));
        invoiceMaintenanceReturnDomain.setSupplierAddress1(spsTInvoiceDomain.getSAddress1());
        invoiceMaintenanceReturnDomain.setSupplierAddress2(spsTInvoiceDomain.getSAddress2());
        invoiceMaintenanceReturnDomain.setSupplierAddress3(spsTInvoiceDomain.getSAddress3());
        invoiceMaintenanceReturnDomain.setSupplierCurrency(spsTInvoiceDomain.getSCurrencyCd());
        invoiceMaintenanceReturnDomain.setVatRate(spsTInvoiceDomain.getVatRate().toString());
        invoiceMaintenanceReturnDomain.setVatType(spsTInvoiceDomain.getVatType());
        invoiceMaintenanceReturnDomain.setInvoiceNo(spsTInvoiceDomain.getInvoiceNo());
        invoiceMaintenanceReturnDomain.setInvoiceDate(invoiceItem.getInvoiceDate());
        invoiceMaintenanceReturnDomain.setDecimalDisp(String.valueOf(scale));
        
        // [IN009] Rounding Mode use HALF_UP
        //invoiceMaintenanceReturnDomain.setSupplierBaseAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getSBaseAmount()));
        //invoiceMaintenanceReturnDomain.setSupplierVatAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getSVatAmount()));
        //invoiceMaintenanceReturnDomain.setSupplierTotalAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getSTotalAmount()));
        //invoiceMaintenanceReturnDomain.setTotalDiffBaseAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getTotalDiffBaseAmount()));
        //invoiceMaintenanceReturnDomain.setTotalDiffVatAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getTotalDiffVatAmount()));
        //invoiceMaintenanceReturnDomain.setTotalDiffAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getTotalDiffAmount()));
        //invoiceMaintenanceReturnDomain.setTotalBaseAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getTotalBaseAmount()));
        //invoiceMaintenanceReturnDomain.setTotalVatAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getTotalVatAmount()));
        //invoiceMaintenanceReturnDomain.setTotalAmount(
        //    companyDecimalFormat.format(spsTInvoiceDomain.getTotalAmount()));
        invoiceMaintenanceReturnDomain.setSupplierBaseAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getSBaseAmount(), scale));
        invoiceMaintenanceReturnDomain.setSupplierVatAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getSVatAmount(), scale));
        invoiceMaintenanceReturnDomain.setSupplierTotalAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getSTotalAmount(), scale));
        invoiceMaintenanceReturnDomain.setTotalDiffBaseAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getTotalDiffBaseAmount(), scale));
        invoiceMaintenanceReturnDomain.setTotalDiffVatAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getTotalDiffVatAmount(), scale));
        invoiceMaintenanceReturnDomain.setTotalDiffAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getTotalDiffAmount(), scale));
        invoiceMaintenanceReturnDomain.setTotalBaseAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getTotalBaseAmount(), scale));
        invoiceMaintenanceReturnDomain.setTotalVatAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getTotalVatAmount(), scale));
        invoiceMaintenanceReturnDomain.setTotalAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, spsTInvoiceDomain.getTotalAmount(), scale));
        
        invoiceMaintenanceReturnDomain.setFileId(spsTInvoiceDomain.getCoverPageFileId());
        invoiceMaintenanceReturnDomain.setCnNo(spsTCnDomain.getCnNo());
        invoiceMaintenanceReturnDomain.setCnDate(invoiceItem.getCnDate());
        if(null != spsTCnDomain.getBaseAmount()){
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceReturnDomain.setCnBaseAmount(
            //    companyDecimalFormat.format(spsTCnDomain.getBaseAmount()));
            invoiceMaintenanceReturnDomain.setCnBaseAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, spsTCnDomain.getBaseAmount(), scale));
        }else{
            invoiceMaintenanceReturnDomain.setCnBaseAmount(null);
        }
        if(null != spsTCnDomain.getVatAmount()){
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceReturnDomain.setCnVatAmount(
            //    companyDecimalFormat.format(spsTCnDomain.getVatAmount()));
            invoiceMaintenanceReturnDomain.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, spsTCnDomain.getVatAmount(), scale));
        }else{
            invoiceMaintenanceReturnDomain.setCnVatAmount(null);
        }
        if(null != spsTCnDomain.getTotalAmount()){
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceReturnDomain.setCnTotalAmount(
            //    companyDecimalFormat.format(spsTCnDomain.getTotalAmount()));
            invoiceMaintenanceReturnDomain.setCnTotalAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, spsTCnDomain.getTotalAmount(), scale));
        }else{
            invoiceMaintenanceReturnDomain.setCnTotalAmount(null);
        }
        SortUtil.sort(asnDomainList, SortUtil.COMPARE_ASN_DETAIL_INFORMATION);
        invoiceMaintenanceReturnDomain.setAsnDetailList(asnDomainList);
        return invoiceMaintenanceReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#transactRegisterInvoice(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public InvoiceMaintenanceDomain transactRegisterInvoice(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain) throws ApplicationException, Exception
    {
        Locale locale = invoiceMaintenanceDomain.getLocale();
        InvoiceMaintenanceDomain invoiceMaintenanceReturnDomain 
            = new InvoiceMaintenanceDomain();
        
        List<ApplicationMessageDomain> errorMessageList = this.validate(invoiceMaintenanceDomain);
        if(ZERO < errorMessageList.size()){
            invoiceMaintenanceReturnDomain.setErrorMessageList(errorMessageList);
            return invoiceMaintenanceReturnDomain;
        }
        
        Timestamp sysDate = this.commonService.searchSysDate();
        boolean sendEmailFlag = false;
        
        Calendar sysDateCalendar = Calendar.getInstance();
        sysDateCalendar.setTimeInMillis(sysDate.getTime());
        int coverPageDate = sysDateCalendar.get(Calendar.DAY_OF_MONTH);
        int coverPageMonth = sysDateCalendar.get(Calendar.MONTH) + ONE;
        int coverPageYear = sysDateCalendar.get(Calendar.YEAR);
        
        SpsTInvoiceDomain spsTInvoiceDomain = new SpsTInvoiceDomain();
        spsTInvoiceDomain.setDCompanyName(invoiceMaintenanceDomain.getDensoName());
        spsTInvoiceDomain.setDCd(invoiceMaintenanceDomain.getDCd());
        spsTInvoiceDomain.setDPcd(invoiceMaintenanceDomain.getDPcd());
        spsTInvoiceDomain.setDpAddress(invoiceMaintenanceDomain.getDensoAddress());
        spsTInvoiceDomain.setDCurrencyCd(invoiceMaintenanceDomain.getDensoCurrency());
        spsTInvoiceDomain.setSCompanyName(invoiceMaintenanceDomain.getSupplierName());
        spsTInvoiceDomain.setSCd(invoiceMaintenanceDomain.getSCd());
        spsTInvoiceDomain.setVendorCd(invoiceMaintenanceDomain.getVendorCd());
        spsTInvoiceDomain.setSTaxId(invoiceMaintenanceDomain.getSupplierTaxId());
        spsTInvoiceDomain.setDTaxId(invoiceMaintenanceDomain.getDensoTaxId());
        spsTInvoiceDomain.setSAddress1(invoiceMaintenanceDomain.getSupplierAddress1());
        spsTInvoiceDomain.setSAddress2(invoiceMaintenanceDomain.getSupplierAddress2());
        spsTInvoiceDomain.setSAddress3(invoiceMaintenanceDomain.getSupplierAddress3());
        spsTInvoiceDomain.setSCurrencyCd(invoiceMaintenanceDomain.getSupplierCurrency());
        spsTInvoiceDomain.setInvoiceNo(invoiceMaintenanceDomain.getInvoiceNo());
        spsTInvoiceDomain.setInvoiceDate(DateUtil.parseToSqlDate(
            invoiceMaintenanceDomain.getInvoiceDate(), PATTERN_YYYYMMDD_SLASH));
        if(!StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getCnNo())){
            spsTInvoiceDomain.setInvoiceStatus(Constants.INVOICE_STATUS_ISC);
        }else{
            spsTInvoiceDomain.setInvoiceStatus(Constants.INVOICE_STATUS_ISS);
        }
        
        // [IN059] change method to convert string to BigDecimal
        //spsTInvoiceDomain.setVatRate(new BigDecimal(invoiceMaintenanceDomain.getVatRate()
        //    .replace(Constants.SYMBOL_PERCENT, EMPTY_STRING)));
        spsTInvoiceDomain.setVatRate(NumberUtil.toBigDecimalDefaultZero(
            invoiceMaintenanceDomain.getVatRate().replace(Constants.SYMBOL_PERCENT, EMPTY_STRING)));
        
        spsTInvoiceDomain.setVatType(invoiceMaintenanceDomain.getVatType());
        spsTInvoiceDomain.setSBaseAmount(
            new BigDecimal(invoiceMaintenanceDomain.getSupplierBaseAmount()));

        // Start : [FIX] None VAT will not input any VAT amount
        //spsTInvoiceDomain.setSVatAmount(
        //    new BigDecimal(invoiceMaintenanceDomain.getSupplierVatAmount()));
        spsTInvoiceDomain.setSVatAmount(NumberUtil.toBigDecimalDefaultZero(
            invoiceMaintenanceDomain.getSupplierVatAmount()));
        
        spsTInvoiceDomain.setSTotalAmount(
            new BigDecimal(invoiceMaintenanceDomain.getSupplierTotalAmount()));
        spsTInvoiceDomain.setTotalDiffBaseAmount(
            new BigDecimal(invoiceMaintenanceDomain.getTotalDiffBaseAmount()));
        
        // Start : [FIX] None VAT will not input any VAT amount
        //spsTInvoiceDomain.setTotalDiffVatAmount(
        //    new BigDecimal(invoiceMaintenanceDomain.getTotalDiffVatAmount()));
        spsTInvoiceDomain.setTotalDiffVatAmount(NumberUtil.toBigDecimalDefaultZero(
            invoiceMaintenanceDomain.getTotalDiffVatAmount()));
        
        spsTInvoiceDomain.setTotalDiffAmount(
            new BigDecimal(invoiceMaintenanceDomain.getTotalDiffAmount()));
        spsTInvoiceDomain.setTotalBaseAmount(
            new BigDecimal(invoiceMaintenanceDomain.getTotalBaseAmount()));
        
        // Start : [FIX] None VAT will not input any VAT amount
        //spsTInvoiceDomain.setTotalVatAmount(
        //    new BigDecimal(invoiceMaintenanceDomain.getTotalVatAmount()));
        spsTInvoiceDomain.setTotalVatAmount(NumberUtil.toBigDecimalDefaultZero(
            invoiceMaintenanceDomain.getTotalVatAmount()));
        
        spsTInvoiceDomain.setTotalAmount(
            new BigDecimal(invoiceMaintenanceDomain.getTotalAmount()));
        spsTInvoiceDomain.setPaymentDate(null);
        spsTInvoiceDomain.setTransferJdeFlag(STR_ZERO);
        spsTInvoiceDomain.setCoverPageFileId(null);
        spsTInvoiceDomain.setCreateDscId(invoiceMaintenanceDomain.getDscId());
        spsTInvoiceDomain.setLastUpdateDscId(invoiceMaintenanceDomain.getDscId());
        spsTInvoiceDomain.setCreateDatetime(sysDate);
        spsTInvoiceDomain.setLastUpdateDatetime(sysDate);
        spsTInvoiceDomain.setCoverPageNo(Constants.EMPTY_STRING);
        Integer invoiceId = this.invoiceService.createInvoice(spsTInvoiceDomain);
        if(null == invoiceId || ZERO == invoiceId){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E6_0034, LBL_REGISTER_INVOICE_DATA);
        }
        
        List<SpsTInvoiceDetailDomain> spsTInvoiceDetailList 
            = new ArrayList<SpsTInvoiceDetailDomain>();
        for(AsnDomain asnItem : invoiceMaintenanceDomain.getAsnDetailList()){
            SpsTInvoiceDetailDomain spsTInvoiceDetail = new SpsTInvoiceDetailDomain();
            spsTInvoiceDetail.setInvoiceId(BigDecimal.valueOf(invoiceId));
            spsTInvoiceDetail.setAsnNo(asnItem.getAsnNo());
            spsTInvoiceDetail.setDoId(NumberUtil.toBigDecimal(asnItem.getDoId()));
            spsTInvoiceDetail.setSpsDoNo(asnItem.getSpsDoNo());
            spsTInvoiceDetail.setRevision(asnItem.getRevision());
            spsTInvoiceDetail.setTripNo(asnItem.getTripNo());
            spsTInvoiceDetail.setDPn(asnItem.getDPn());
            spsTInvoiceDetail.setDUnitOfMeasure(asnItem.getDensoUnitOfMeasure());
            spsTInvoiceDetail.setDUnitPrice(NumberUtil.toBigDecimal(
                StringUtil.removeComma(asnItem.getDensoUnitPrice())));
            spsTInvoiceDetail.setTmpPriceFlg(asnItem.getTempPriceFlag());
            spsTInvoiceDetail.setSPn(asnItem.getSPn());
            spsTInvoiceDetail.setSUnitOfMeasure(asnItem.getSupplierUnitOfMeasure());
            spsTInvoiceDetail.setSUnitPrice(NumberUtil.toBigDecimal(
                StringUtil.removeComma(asnItem.getSupplierUnitPrice())));
            spsTInvoiceDetail.setShippingQty(
                NumberUtil.toBigDecimal(StringUtil.removeComma(asnItem.getShippingQty())));
            spsTInvoiceDetail.setDiffBaseAmt(NumberUtil.toBigDecimal(
                StringUtil.removeComma(asnItem.getDiffBaseAmount())));
            spsTInvoiceDetail.setBaseAmt(NumberUtil.toBigDecimalDefaultZero(
                StringUtil.removeComma(asnItem.getDensoBaseAmount())));
            if (Constants.STR_T.equals(asnItem.getTempPriceFlag())) {
                sendEmailFlag = true;
            }
            spsTInvoiceDetail.setCreateDscId(invoiceMaintenanceDomain.getDscId());
            spsTInvoiceDetail.setLastUpdateDscId(invoiceMaintenanceDomain.getDscId());
            spsTInvoiceDetail.setCreateDatetime(sysDate);
            spsTInvoiceDetail.setLastUpdateDatetime(sysDate);
            spsTInvoiceDetailList.add(spsTInvoiceDetail);
        }
        this.spsTInvoiceDetailService.create(spsTInvoiceDetailList);
        
        if(!StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getCnNo())){
            SpsTCnDomain spsTCn = new SpsTCnDomain();
            spsTCn.setInvoiceId(BigDecimal.valueOf(invoiceId));
            spsTCn.setCnStatus(Constants.ISS_STATUS);
            spsTCn.setCnNo(invoiceMaintenanceDomain.getCnNo());
            spsTCn.setCnDate(DateUtil.parseToSqlDate(
                invoiceMaintenanceDomain.getCnDate(), PATTERN_YYYYMMDD_SLASH));
            spsTCn.setBaseAmount(new BigDecimal(StringUtil.removeComma(
                invoiceMaintenanceDomain.getCnBaseAmount())));
            
            //[FIX] None VAT will not input any VAT amount
            //spsTCn.setVatAmount(new BigDecimal(StringUtil.removeComma(
            //    invoiceMaintenanceDomain.getCnVatAmount())));
            spsTCn.setVatAmount(NumberUtil.toBigDecimalDefaultZero(StringUtil.removeComma(
                invoiceMaintenanceDomain.getCnVatAmount())));
            
            spsTCn.setTotalAmount(new BigDecimal(StringUtil.removeComma(
                invoiceMaintenanceDomain.getCnTotalAmount())));
            spsTCn.setCreateDscId(invoiceMaintenanceDomain.getDscId());
            spsTCn.setLastUpdateDscId(invoiceMaintenanceDomain.getDscId());
            spsTCn.setCreateDatetime(sysDate);
            spsTCn.setLastUpdateDatetime(sysDate);
            Integer cnId = this.cnService.createCn(spsTCn);
            if(null == cnId || ZERO == cnId){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    ERROR_CD_SP_E6_0034, LBL_REGISTER_CREDIT_NOTE_DATA);
            }
            
            List<SpsTCnDetailDomain> spsTCnDetailList = new ArrayList<SpsTCnDetailDomain>();
            for(AsnDomain asnItem : invoiceMaintenanceDomain.getAsnDetailList()){
                if(ZERO != Constants.BIG_DECIMAL_ZERO.compareTo(
                        NumberUtil.toBigDecimal(asnItem.getBdDiffBaseAmount())))
                {
                    sendEmailFlag = true;
                    SpsTCnDetailDomain spsTCnDetail = new SpsTCnDetailDomain();
                    spsTCnDetail.setCnId(new BigDecimal(cnId));
                    spsTCnDetail.setDoId(new BigDecimal(asnItem.getDoId()));
                    spsTCnDetail.setAsnNo(asnItem.getAsnNo());
                    spsTCnDetail.setDPn(asnItem.getDPn());
                    spsTCnDetail.setSPn(asnItem.getSPn());
                    BigDecimal supplierUnitPrice = new BigDecimal(StringUtil.removeComma(
                        asnItem.getSupplierUnitPrice()));
                    BigDecimal densoUnitPrice = new BigDecimal(StringUtil.removeComma(
                        asnItem.getDensoUnitPrice()));
                    spsTCnDetail.setSPriceUnit(supplierUnitPrice);
                    spsTCnDetail.setDPriceUnit(densoUnitPrice);
                    spsTCnDetail.setDiffPriceUnit(densoUnitPrice.subtract(supplierUnitPrice));
                    spsTCnDetail.setDiffBaseAmt(new BigDecimal(
                        StringUtil.removeComma(asnItem.getDiffBaseAmount())));
                    spsTCnDetail.setCreateDscId(invoiceMaintenanceDomain.getDscId());
                    spsTCnDetail.setLastUpdateDscId(invoiceMaintenanceDomain.getDscId());
                    spsTCnDetail.setCreateDatetime(sysDate);
                    spsTCnDetail.setLastUpdateDatetime(sysDate);
                    spsTCnDetailList.add(spsTCnDetail);
                }
            }
            this.spsTCnDetailService.create(spsTCnDetailList);
        }
        
        InvoiceInformationDomain invoiceInformationCriteria = new InvoiceInformationDomain();
        invoiceInformationCriteria.getSpsTInvoiceDomain().setInvoiceId(
            BigDecimal.valueOf(invoiceId));
        List<InvoiceInformationDomain> invoiceInformationList 
            = this.invoiceService.searchInvoiceInformation(invoiceInformationCriteria);
        
        List<SpsTAsnDomain> spsTAsnList = new ArrayList<SpsTAsnDomain>();
        List<SpsTAsnCriteriaDomain> spsTAsnCriteriaList 
            = new ArrayList<SpsTAsnCriteriaDomain>();
        Set<String> updateAsn = new TreeSet<String>();
        for(InvoiceDetailDomain invoiceDetailItem : invoiceInformationList.get(ZERO)
            .getInvoiceDetailDomainList()){
            if(updateAsn.add(invoiceDetailItem.getSpsTAsnDomain().getAsnNo().trim())){
                SpsTAsnDomain spsTAsn = new SpsTAsnDomain();
                spsTAsn.setCreatedInvoiceFlag(STR_ONE);
                spsTAsn.setLastUpdateDscId(invoiceMaintenanceDomain.getDscId());
                spsTAsn.setLastUpdateDatetime(sysDate);
                spsTAsnList.add(spsTAsn);
                
                SpsTAsnCriteriaDomain spsTAsnCriteria = new SpsTAsnCriteriaDomain();
                spsTAsnCriteria.setAsnNo(invoiceDetailItem.getSpsTAsnDomain().getAsnNo());
                spsTAsnCriteria.setCreatedInvoiceFlag(STR_ZERO);
                spsTAsnCriteria.setLastUpdateDatetime(invoiceDetailItem.getSpsTAsnDomain()
                    .getLastUpdateDatetime());
                spsTAsnCriteriaList.add(spsTAsnCriteria);
            }
        }
        
        int resultCountAsn = updateAsn.size();
        
        int resultCountUpdateAsn = this.spsTAsnService.updateByCondition(
            spsTAsnList, spsTAsnCriteriaList);
        if(resultCountAsn != resultCountUpdateAsn){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E6_0034, LBL_UPDATE_CREATED_INVOICE_FLAG);
        }
        
        String dPcd = Constants.EMPTY_STRING;
        if(Constants.STR_ONE.equals(invoiceMaintenanceDomain.getDiffDPcdFlag())){
            dPcd  = SupplierPortalConstant.DEFAULT_INV_COVERPAGE_DIFF_DPCD;
        }else{
            dPcd = invoiceMaintenanceDomain.getDPcd();
        }
        
        SpsTInvCoverpageRunnoCriteriaDomain spsTInvCoverpageRunnoCriteria 
            = new SpsTInvCoverpageRunnoCriteriaDomain();
        spsTInvCoverpageRunnoCriteria.setRunningYear(String.valueOf(coverPageYear));
        spsTInvCoverpageRunnoCriteria.setRunningMonth(
            String.format(RUNNING_NO_FORMAT, coverPageMonth));
        spsTInvCoverpageRunnoCriteria.setRunningDate(
            String.format(RUNNING_NO_FORMAT, coverPageDate));
        spsTInvCoverpageRunnoCriteria.setDCd(invoiceMaintenanceDomain.getDCd());
        spsTInvCoverpageRunnoCriteria.setDPcd(dPcd);
        SpsTInvCoverpageRunnoDomain spsTInvCoverpageRunno 
            = this.spsTInvCoverpageRunnoService.searchByKey(spsTInvCoverpageRunnoCriteria);
        BigDecimal runningCoverPageNo = null;
        StringBuffer invoiceCoverPageNo = new StringBuffer();
        invoiceCoverPageNo.append(coverPageYear);
        invoiceCoverPageNo.append(String.format(RUNNING_NO_FORMAT, coverPageMonth));
        invoiceCoverPageNo.append(String.format(RUNNING_NO_FORMAT, coverPageDate));
        invoiceCoverPageNo.append(dPcd);
        if(null == spsTInvCoverpageRunno){
            invoiceCoverPageNo.append(Constants.DEFAULT_RUINNING_NO);
            
            SpsTInvCoverpageRunnoDomain spsTInvCoverpageRunnoForCreate 
                = new SpsTInvCoverpageRunnoDomain();
            spsTInvCoverpageRunnoForCreate.setRunningYear(String.valueOf(coverPageYear));
            spsTInvCoverpageRunnoForCreate.setRunningMonth(
                String.format(RUNNING_NO_FORMAT, coverPageMonth));
            spsTInvCoverpageRunnoForCreate.setRunningDate(
                String.format(RUNNING_NO_FORMAT, coverPageDate));
            spsTInvCoverpageRunnoForCreate.setDCd(invoiceMaintenanceDomain.getDCd());
            spsTInvCoverpageRunnoForCreate.setDPcd(dPcd);
            spsTInvCoverpageRunnoForCreate.setRunno(BigDecimal.valueOf(ONE));
            spsTInvCoverpageRunnoForCreate.setCreateDscId(
                invoiceMaintenanceDomain.getDscId());
            spsTInvCoverpageRunnoForCreate.setLastUpdateDscId(
                invoiceMaintenanceDomain.getDscId());
            spsTInvCoverpageRunnoForCreate.setCreateDatetime(sysDate);
            spsTInvCoverpageRunnoForCreate.setLastUpdateDatetime(sysDate);
            
            this.spsTInvCoverpageRunnoService.create(spsTInvCoverpageRunnoForCreate);
        }else{
            runningCoverPageNo = spsTInvCoverpageRunno.getRunno();
            runningCoverPageNo = runningCoverPageNo.add(BigDecimal.valueOf(ONE));
            invoiceCoverPageNo.append(
                String.format(Constants.FORMAT_FOUR_DIGIT, runningCoverPageNo.intValue()));
            
            SpsTInvCoverpageRunnoDomain spsTInvCoverpageRunnoForUpdate 
                = new SpsTInvCoverpageRunnoDomain();
            spsTInvCoverpageRunnoForUpdate.setRunno(runningCoverPageNo);
            spsTInvCoverpageRunnoForUpdate.setLastUpdateDscId(
                invoiceMaintenanceDomain.getDscId());
            spsTInvCoverpageRunnoForUpdate.setLastUpdateDatetime(sysDate);
            
            SpsTInvCoverpageRunnoCriteriaDomain spsTInvCoverpageRunnoCriteriaForUpdate 
                = new SpsTInvCoverpageRunnoCriteriaDomain();
            spsTInvCoverpageRunnoCriteriaForUpdate.setRunningYear(
                String.valueOf(coverPageYear));
            spsTInvCoverpageRunnoCriteriaForUpdate.setRunningMonth(
                String.format(RUNNING_NO_FORMAT, coverPageMonth));
            spsTInvCoverpageRunnoCriteriaForUpdate.setRunningDate(
                String.format(RUNNING_NO_FORMAT, coverPageDate));
            spsTInvCoverpageRunnoCriteriaForUpdate.setDCd(invoiceMaintenanceDomain.getDCd());
            spsTInvCoverpageRunnoCriteriaForUpdate.setDPcd(spsTInvCoverpageRunno.getDPcd());
            spsTInvCoverpageRunnoCriteriaForUpdate.setLastUpdateDatetime(
                spsTInvCoverpageRunno.getLastUpdateDatetime());
            int resultCountUpdateInvCoverpage = this.spsTInvCoverpageRunnoService
                .updateByCondition(
                    spsTInvCoverpageRunnoForUpdate, spsTInvCoverpageRunnoCriteriaForUpdate);
            if(ONE != resultCountUpdateInvCoverpage){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    ERROR_CD_SP_E6_0034, LBL_UPDATE_COVER_PAGE_RUNNING_NO);
            }
        }
//        2018-04-18 Dont generate PDF to BLOB
//        List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList 
//            = this.createInvoiceCoverPageData(invoiceId, invoiceCoverPageNo,
//                invoiceMaintenanceDomain.getDecimalDisp(), locale);
//        
//        InputStream tsInvoiceCoverPageReport 
//            = this.createInvoiceCoverPageReport(invoiceCoverPageReportList);
//        String fileName = StringUtil.appendsString(
//            INVOICE_COVER_PAGE, invoiceCoverPageNo.toString(),
//            Constants.SYMBOL_DOT, Constants.REPORT_PDF_FORMAT);
//        String fileId = null;
//        try{
//            fileId = this.fileManagementService.createFileUpload(tsInvoiceCoverPageReport,
//                fileName, Constants.SAVE_LIMIT_TERM,
//                invoiceMaintenanceDomain.getDscId());
//        }
//        catch(IOException ie){
//            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_80_0015);
//        }
        
        SpsTInvoiceDomain spsTInvoice = new SpsTInvoiceDomain();
//        spsTInvoice.setCoverPageFileId(fileId);
        spsTInvoice.setCoverPageNo(invoiceCoverPageNo.toString());
        spsTInvoice.setLastUpdateDscId(invoiceMaintenanceDomain.getDscId());
        spsTInvoice.setCreateDatetime(sysDate);
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteria = new SpsTInvoiceCriteriaDomain();
        spsTInvoiceCriteria.setInvoiceId(new BigDecimal(invoiceId));
        spsTInvoiceCriteria.setLastUpdateDatetime(sysDate);
        int resultCountUpdateFileId 
            = this.spsTInvoiceService.updateByCondition(spsTInvoice, spsTInvoiceCriteria);
        if(ONE != resultCountUpdateFileId){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E6_0034, LBL_UPDATE_FILE_ID);
        }
        
        invoiceMaintenanceReturnDomain.setAsnNoRegister(updateAsn);
//        invoiceMaintenanceReturnDomain.setInvoiceCoverPage(tsInvoiceCoverPageReport);
//        invoiceMaintenanceReturnDomain.setFileId(fileId);
        
        // 3.2.7 Send Mail to DENSO when created Invoice with CN or use Temp Price
        boolean registerProcessComplete = true;
        if (sendEmailFlag) {
            // 1) Get email to send from UserService
            SpsMUserDensoDomain criDensoUser = new SpsMUserDensoDomain();
            criDensoUser.setEmlCreateInvoiceFlag( STR_ONE );
            criDensoUser.setDCd(invoiceMaintenanceDomain.getDCd());
            if(!StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDPcd())){
                criDensoUser.setDPcd(invoiceMaintenanceDomain.getDPcd());
            }
            List<SpsMUserDomain> userList =  userService.searchEmailUserDenso(criDensoUser);
            
            if (null == userList || Constants.ZERO == userList.size()) {
                registerProcessComplete = false;
            } else {
                List<String> toList = new ArrayList<String>(userList.size());
                for(SpsMUserDomain user : userList){
                    if( !toList.contains( user.getEmail() ) ){
                        toList.add( user.getEmail() );
                    }
                }
                try {
                    SendEmailDomain mailDomain = this.getEmailCreateInvoiceWithCnContent(
                        invoiceMaintenanceDomain, invoiceCoverPageNo.toString());
                    
                    if (null == mailDomain) {
                        registerProcessComplete = false;
                    } else {
                        //mailDomain.setHeader( title );
                        mailDomain.setEmailSmtp(ContextParams.getEmailSmtp());
                        //mailDomain.setContents( content );
                        mailDomain.setEmailFrom( ContextParams.getDefaultEmailFrom() );
                        mailDomain.setEmailTo(Constants.EMPTY_STRING);
                        mailDomain.setToList( toList );
                        registerProcessComplete = this.mailService.sendEmail(mailDomain);
                    }
                } catch(Exception ex) {
                    registerProcessComplete = false;
                }
            }
        }
        if (!registerProcessComplete) {
            invoiceMaintenanceReturnDomain.getSuccessMessageList().add(
                new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_I6_0023,
                        SupplierPortalConstant.LBL_REGISTER_INVOICE_W_CN)));
        }
        //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --start--
        invoiceMaintenanceReturnDomain.setInvoiceId(invoiceId.toString());
        //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --end--
        return invoiceMaintenanceReturnDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#deleteTmpUploadInvoice(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public void deleteTmpUploadInvoice(InvoiceMaintenanceDomain invoiceMaintenanceDomain)
        throws ApplicationException {
        
        SpsTmpUploadInvoiceCriteriaDomain criteria = new SpsTmpUploadInvoiceCriteriaDomain();
        criteria.setSessionId(invoiceMaintenanceDomain.getSessionId());
        criteria.setUploadDscId(invoiceMaintenanceDomain.getDscId());
        this.spsTmpUploadInvoiceService.deleteByCondition(criteria);
        
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchPreviewCoverPage(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public InvoiceMaintenanceDomain searchPreviewCoverPage(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)throws Exception {
        
        InvoiceMaintenanceDomain invoiceMaintenanceReturnDomain = new InvoiceMaintenanceDomain();
        List<ApplicationMessageDomain> errorMessageList = this.validate(invoiceMaintenanceDomain);
        if(ZERO < errorMessageList.size()){
            
            invoiceMaintenanceReturnDomain.setErrorMessageList(errorMessageList);
        }else{
            
            Calendar sysDateCalendar = Calendar.getInstance();
            sysDateCalendar.setTimeInMillis(this.commonService.searchSysDate().getTime());
            int coverPageDate = sysDateCalendar.get(Calendar.DAY_OF_MONTH);
            int coverPageMonth = sysDateCalendar.get(Calendar.MONTH) + ONE;
            int coverPageYear = sysDateCalendar.get(Calendar.YEAR);
            int coverPageHour = sysDateCalendar.get(Calendar.HOUR_OF_DAY);
            int coverPageMinute = sysDateCalendar.get(Calendar.MINUTE);
            
            List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList 
                = this.createInvoiceCoverPageDataForView(invoiceMaintenanceDomain);
            invoiceMaintenanceReturnDomain.setInvoiceCoverPage(
                this.createInvoiceCoverPageReport(invoiceCoverPageReportList));
            
            String fileName = StringUtil.appendsString(
                INVOICE_COVER_PAGE,
                Constants.SYMBOL_UNDER_SCORE,
                String.valueOf(coverPageDate),
                String.valueOf(coverPageMonth),
                String.valueOf(coverPageYear),
                String.valueOf(coverPageHour),
                String.valueOf(coverPageMinute),
                Constants.SYMBOL_DOT,
                Constants.REPORT_PDF_FORMAT);
            invoiceMaintenanceReturnDomain.setFileName(fileName);
        }
        return invoiceMaintenanceReturnDomain;
        
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchInvoiceMaintenanceCsv(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public InvoiceMaintenanceDomain searchInvoiceMaintenanceCsv(
       InvoiceMaintenanceDomain invoiceMaintenanceDomain) throws ApplicationException {
        
        String fileName = StringUtil.appendsString(
            Constants.INVOICE_DETAIL_INFORMATION, Constants.SYMBOL_UNDER_SCORE,
            DateUtil.format(this.commonService.searchSysDate(), DateUtil.PATTERN_YYYYMMDD_HHMM));
        
        final String[] headerArr = new String[] {
            LBL_S_CD,
            LBL_D_CD,
            LBL_D_PCD,
            LBL_DENSO_TAX_ID,
            LBL_SUPPLIER_TAX_ID,
            LBL_INVOICE_DATE,
            LBL_INVOICE_NO,
            LBL_VAT_TYPE,
            LBL_VAT_RATE,
            LBL_SUPPLIER_CURRENCY,
            LBL_SUPPLIER_BASE_AMOUNT,
            LBL_SUPPLIER_VAT_AMOUNT,
            LBL_SUPPLIER_TOTAL_AMOUNT,
            LBL_CREDIT_NOTE_NO,
            LBL_CREDIT_NOTE_DATE,
            LBL_CREDIT_NOTE_BASE_AMOUNT,
            LBL_CREDIT_NOTE_VAT_AMOUNT,
            LBL_CREDIT_NOTE_TOTAL_AMOUNT,
            LBL_DENSO_BASE_AMOUNT,
            LBL_DENSO_VAT_AMOUNT,
            LBL_DENSO_TOTAL_AMOUNT,
            LBL_DIFFERENCE_BASE_AMOUNT,
            LBL_DIFFERENCE_VAT_GST_AMOUNT,
            LBL_TOTAL_DIFFERENCE_AMOUNT,
            LBL_ASN_NO, 
            LBL_TRIP_NO,
            LBL_ASN_STATUS,
            LBL_D_PN,
            LBL_DENSO_UNIT_OF_MEASURE,
            LBL_DENSO_PRICE_UNIT,
            LBL_TEMP_PRICE,
            LBL_S_PN,
            LBL_S_UM,
            LBL_SUPPLIER_PRICE_UNIT,
            LBL_SHIPPING_QTY,
            LBL_DENSO_BASE_AMOUNT_BY_PN,
            LBL_SUPPLIER_BASE_AMOUNT_BY_PN,
            LBL_DIFFERENCE_BASE_AMOUNT_BY_PN
        };
        
        Locale locale = invoiceMaintenanceDomain.getLocale();
        InvoiceMaintenanceDomain invoiceMaintenanceReturn = new InvoiceMaintenanceDomain();
        
        List<Map<String, Object>> resultDetail = this.getInvoiceMaintenanceMap(
            invoiceMaintenanceDomain, headerArr);
        CommonDomain commonDomain = new CommonDomain();
        commonDomain.setLocale(locale);
        commonDomain.setResultList(resultDetail);
        commonDomain.setHeaderArr(headerArr);
        commonDomain.setHeaderFlag(true);
        
        try {
            invoiceMaintenanceReturn.setCsvResult(
                this.commonService.createCsvString(commonDomain));
            invoiceMaintenanceReturn.setFileName(fileName);
            
        } catch (IOException ioe) {
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_80_0012);  
        }
        
        return invoiceMaintenanceReturn;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchFileName(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public FileManagementDomain searchFileName(InvoiceMaintenanceDomain invoiceMaintenance) 
        throws ApplicationException {
        
        Locale locale = invoiceMaintenance.getLocale();
        FileManagementDomain resultDomain = null;
        
        try {
            
            resultDomain = fileManagementService.searchFileDownload(invoiceMaintenance.getFileId(),
                false, null);
        } catch (IOException e) {
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_80_0009); 
        }
        
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService#searchLegendInfo(
     * com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain, OutputStream)
     */
    public void searchLegendInfo(InvoiceMaintenanceDomain invoiceMaintenance, OutputStream output)
        throws ApplicationException {
        
        Locale locale = invoiceMaintenance.getLocale();
        
        try {
            fileManagementService.searchFileDownload(invoiceMaintenance.getFileId(), true, output);
            
        } catch (IOException e) {
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_80_0009); 
        }
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeServiceImpl#searchDownloadCoverPageReport(com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain, OutputStream)
     */
    public void searchDownloadCoverPageReport(InvoiceMaintenanceDomain invoiceMaintenance,
        OutputStream outputStream) throws ApplicationException
    {
        Locale locale = invoiceMaintenance.getLocale();
        try{
            fileManagementService.searchFileDownload(invoiceMaintenance.getFileId(), true,
                outputStream);
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessageHandledException(locale, ERROR_CD_SP_80_0009); 
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeServiceImpl#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    @Override
    protected String getReportPath() throws Exception {
        return  StringUtil.appendsString(ContextParams.getJasperFilePath(), 
            INVOICE_COVER_PAGE_PATH) ;
    }
    
    /**
     * <p>Get invoice maintenance map.</p>
     * <ul>
     * <li>Preparation data for create CSV file.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @param header the array of string
     * @return List<Map<String, Object>>
     */
    private List<Map<String, Object>> getInvoiceMaintenanceMap(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain, String[] header)
    {
        List<Map<String, Object>> invoiceMaintenanceMapList 
            = new ArrayList<Map<String, Object>>();
        List<AsnDomain> asnDetailList = invoiceMaintenanceDomain.getAsnDetailList();
        Map<String, Object>  invoiceMaintenanceMap = null;
        
        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        int decimalPoint = 0;
        if(!StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDecimalDisp())){
            decimalPoint = Integer.valueOf(invoiceMaintenanceDomain.getDecimalDisp());
        }
        companyDecimalFormat.setMinimumFractionDigits(decimalPoint);
        companyDecimalFormat.setMaximumFractionDigits(decimalPoint);
        
        for(AsnDomain record : asnDetailList){
            invoiceMaintenanceMap = new HashMap<String, Object>();
            invoiceMaintenanceMap.put(header[ZERO], 
                invoiceMaintenanceDomain.getVendorCd());
            invoiceMaintenanceMap.put(header[ONE], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getDCd()));
            invoiceMaintenanceMap.put(header[Constants.TWO], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getDPcd()));
            invoiceMaintenanceMap.put(header[Constants.THREE],
                invoiceMaintenanceDomain.getDensoTaxId());
            invoiceMaintenanceMap.put(header[Constants.FOUR], 
                invoiceMaintenanceDomain.getSupplierTaxId());
            invoiceMaintenanceMap.put(header[Constants.FIVE], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getInvoiceDate()));
            invoiceMaintenanceMap.put(header[Constants.SIX], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getInvoiceNo()));
            invoiceMaintenanceMap.put(header[Constants.SEVEN], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getVatType()));
            invoiceMaintenanceMap.put(header[Constants.EIGHT], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getVatRate()));
            invoiceMaintenanceMap.put(header[Constants.NINE], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getSupplierCurrency()));
            
            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceMap.put(header[Constants.TEN], this.convertAmountToDecimalFormat(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getSupplierBaseAmount()));
            //invoiceMaintenanceMap.put(header[Constants.ELEVEN], this.convertAmountToDecimalFormat(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getSupplierVatAmount()));
            //invoiceMaintenanceMap.put(header[Constants.TWELVE], this.convertAmountToDecimalFormat(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getSupplierTotalAmount()));
            //invoiceMaintenanceMap.put(header[Constants.THIRTEEN], StringUtil.checkNullToEmpty(
            //    invoiceMaintenanceDomain.getCnNo()));
            //invoiceMaintenanceMap.put(header[Constants.FOURTEEN], StringUtil.checkNullToEmpty(
            //    invoiceMaintenanceDomain.getCnDate()));
            //invoiceMaintenanceMap.put(header[Constants.FIFTEEN], this.convertAmountToDecimalFormat(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getCnBaseAmount()));
            //invoiceMaintenanceMap.put(header[Constants.SIXTEEN], this.convertAmountToDecimalFormat(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getCnVatAmount()));
            //invoiceMaintenanceMap.put(header[Constants.SEVENTEEN],
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getCnTotalAmount()));
            //invoiceMaintenanceMap.put(header[Constants.EIGHTEEN], 
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getTotalBaseAmount()));
            //invoiceMaintenanceMap.put(header[Constants.NINETEEN], 
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getTotalVatAmount()));
            //invoiceMaintenanceMap.put(header[Constants.TWENTY], 
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getTotalAmount()));
            //invoiceMaintenanceMap.put(header[Constants.TWENTY_ONE], 
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getTotalDiffBaseAmount()));
            //invoiceMaintenanceMap.put(header[Constants.TWENTY_TWO], 
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getTotalDiffVatAmount()));
            //invoiceMaintenanceMap.put(header[Constants.TWENTY_THREE],
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, invoiceMaintenanceDomain.getTotalDiffAmount()));
            invoiceMaintenanceMap.put(header[Constants.TEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getSupplierBaseAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.ELEVEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getSupplierVatAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.TWELVE],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getSupplierTotalAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.THIRTEEN], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getCnNo()));
            invoiceMaintenanceMap.put(header[Constants.FOURTEEN], StringUtil.checkNullToEmpty(
                invoiceMaintenanceDomain.getCnDate()));
            invoiceMaintenanceMap.put(header[Constants.FIFTEEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getCnBaseAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.SIXTEEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getCnVatAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.SEVENTEEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getCnTotalAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.EIGHTEEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getTotalBaseAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.NINETEEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getTotalVatAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.TWENTY],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getTotalAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.TWENTY_ONE],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getTotalDiffBaseAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.TWENTY_TWO],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getTotalDiffVatAmount(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.TWENTY_THREE],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , invoiceMaintenanceDomain.getTotalDiffAmount(), decimalPoint));
            
            invoiceMaintenanceMap.put(header[Constants.TWENTY_FOUR], record.getAsnNo());
            invoiceMaintenanceMap.put(header[Constants.TWENTY_FIVE], StringUtil.checkNullToEmpty(
                record.getTripNo()));
            invoiceMaintenanceMap.put(header[Constants.TWENTY_SIX], record.getAsnStatus());
            invoiceMaintenanceMap.put(header[Constants.TWENTY_SEVEN], record.getDPn());
            invoiceMaintenanceMap.put(header[Constants.TWENTY_EIGHT], StringUtil.checkNullToEmpty(
                record.getDensoUnitOfMeasure()));
            invoiceMaintenanceMap.put(header[Constants.TWENTY_NINE],
                this.addDoubleQuote(record.getDensoUnitPrice()));
            invoiceMaintenanceMap.put(header[Constants.THIRTY], StringUtil.checkNullToEmpty(
                record.getTempPriceFlag()));
            invoiceMaintenanceMap.put(header[Constants.THIRTY_ONE], record.getSPn());
            invoiceMaintenanceMap.put(header[Constants.THIRTY_TWO], 
                record.getSupplierUnitOfMeasure());
            invoiceMaintenanceMap.put(header[Constants.THIRTY_THREE], 
                this.addDoubleQuote(record.getSupplierUnitPrice()));
            invoiceMaintenanceMap.put(header[Constants.THIRTY_FOUR], 
                this.addDoubleQuote(record.getShippingQty()));
            invoiceMaintenanceMap.put(header[Constants.THIRTY_FIVE], 
                this.addDoubleQuote(record.getDensoBaseAmount()));
            
            BigDecimal supplierUnitPrice = new BigDecimal(Constants.ZERO);
            BigDecimal shippingQty = new BigDecimal(Constants.ZERO);
            if(!StringUtil.checkNullOrEmpty(record.getSupplierUnitPrice())){
                supplierUnitPrice = new BigDecimal(
                    StringUtil.removeComma(record.getSupplierUnitPrice()));
            }
            if(!StringUtil.checkNullOrEmpty(record.getShippingQty())){
                shippingQty = new BigDecimal(StringUtil.removeComma(record.getShippingQty()));
            }
            
            BigDecimal supplierBaseAmount = supplierUnitPrice.multiply(shippingQty);
            // [IN010] Calculate supplierBaseAmount after rounding
            supplierBaseAmount = supplierBaseAmount.setScale(decimalPoint, RoundingMode.HALF_UP);
            
            BigDecimal densoBaseAmount = new BigDecimal(
                StringUtil.removeComma(record.getDensoBaseAmount()));
            BigDecimal diffBaseAmount = densoBaseAmount.subtract(supplierBaseAmount);

            // [IN009] Rounding Mode use HALF_UP
            //invoiceMaintenanceMap.put(header[Constants.THIRTY_SIX],
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, supplierBaseAmount.toString()));
            //invoiceMaintenanceMap.put(header[Constants.THIRTY_SEVEN],
            //    this.convertAmountToDecimalFormat(
            //        companyDecimalFormat, diffBaseAmount.toString()));
            invoiceMaintenanceMap.put(header[Constants.THIRTY_SIX],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , supplierBaseAmount.toString(), decimalPoint));
            invoiceMaintenanceMap.put(header[Constants.THIRTY_SEVEN],
                this.convertAmountToCsvDecimalFormat(companyDecimalFormat
                    , diffBaseAmount.toString(), decimalPoint));
            
            invoiceMaintenanceMapList.add(invoiceMaintenanceMap);
        }
        return invoiceMaintenanceMapList;
    }
    
    /**
     * <p>Create invoice cover page data.</p>
     * <ul>
     * <li>Preparation data for create invoice cover page report.</li>
     * </ul>
     * 
     * @param invoiceId the invoice id
     * @param invoiceCoverPageNo the list invoice cover page no 
     * @param decimalDisp the String
     * @param locale the locale 
     * @return list of the Invoice Cover Page Report Domain 
     * @throws ApplicationException 
     */
    private List<InvoiceCoverPageReportDomain> createInvoiceCoverPageData(Integer invoiceId,
            StringBuffer invoiceCoverPageNo, String decimalDisp, Locale locale)
        throws ApplicationException {
        
        List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList 
            = new ArrayList<InvoiceCoverPageReportDomain>();
        
        int invoiceRunningNo = ZERO;
        int cnRunningNo = ZERO;

        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        int scale = 0;
        if(!StringUtil.checkNullOrEmpty(decimalDisp)){
            scale = Integer.valueOf(decimalDisp);
        }
        companyDecimalFormat.setMinimumFractionDigits(scale);
        companyDecimalFormat.setMaximumFractionDigits(scale);
        
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat noDecimalFormat = (DecimalFormat)defaultNumberFormat;
        noDecimalFormat.setGroupingUsed(true);
        noDecimalFormat.setMinimumFractionDigits(Constants.ZERO);
        noDecimalFormat.setMaximumFractionDigits(Constants.ZERO);

        InvoiceCoverPageDomain invoiceCoverPageCriteria = new InvoiceCoverPageDomain();
        invoiceCoverPageCriteria.getSpsTInvoiceDomain().setInvoiceId(BigDecimal.valueOf(invoiceId));
        // [IN010]
        invoiceCoverPageCriteria.setDecimalDigit(String.valueOf(scale));
        
        List<InvoiceCoverPageDomain> invoiceCoverPageList 
            = this.invoiceService.searchInvoiceCoverPage(invoiceCoverPageCriteria);
        if(ZERO == invoiceCoverPageList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                ERROR_CD_SP_E6_0034, LBL_CREATE_INVOICE_COVERPAGE);
        }
        
        CnInformationDomain CNInformation = new CnInformationDomain();
        CNInformation.setInvoiceId(BigDecimal.valueOf(invoiceId));
        List<CnInformationDomain> cnCoverPageList = this.cnService.searchCnCoverPage(CNInformation);
        String strInvoiceCoverPageNo = null;
        if(null != invoiceCoverPageNo){
            strInvoiceCoverPageNo = invoiceCoverPageNo.toString();
        }
        
        StringBuffer documentNumberBuffer = new StringBuffer();
        
        // Invoice Header at first row
        InvoiceCoverPageDomain firstRecord = invoiceCoverPageList.get(ZERO);
        InvoiceCoverPageReportDomain invoiceCoverPageFristLine = this.getCoverPageHeader(
            strInvoiceCoverPageNo, firstRecord.getSpsTInvoiceDomain(), locale);
        invoiceCoverPageFristLine.setInvoiceRunningNo(String.valueOf(++invoiceRunningNo));
        
        invoiceCoverPageFristLine.setDocumentType(INV_DOCUMENT_TYPE);
        invoiceCoverPageFristLine.setDocumentDate(DateUtil.format(
            firstRecord.getSpsTInvoiceDomain().getInvoiceDate(), PATTERN_YYYYMMDD_SLASH));
        invoiceCoverPageFristLine.setDocumentNo(firstRecord.getSpsTInvoiceDomain().getInvoiceNo());

        // [IN009] Rounding Mode use HALF_UP
        //invoiceCoverPageFristLine.setInvoiceAmount(formatBigDecimalToString(companyDecimalFormat,
        //    firstRecord.getSpsTInvoiceDomain().getSBaseAmount()));
        //invoiceCoverPageFristLine.setTaxRate(this.formatBigDecimalToString(noDecimalFormat,
        //    firstRecord.getSpsTInvoiceDomain().getVatRate()));
        //invoiceCoverPageFristLine.setInvoiceBaseAmount(this.formatBigDecimalToString(
        //    companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSBaseAmount()));
        //invoiceCoverPageFristLine.setInvoiceVatAmount(this.formatBigDecimalToString(
        //    companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSVatAmount()));
        //invoiceCoverPageFristLine.setInvoiceTotalAmount(this.formatBigDecimalToString(
        //    companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSTotalAmount()));
        invoiceCoverPageFristLine.setInvoiceAmount(this.formatBigDecimalToString(
            companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSBaseAmount(), scale));
        invoiceCoverPageFristLine.setTaxRate(this.formatBigDecimalToString(
            noDecimalFormat, firstRecord.getSpsTInvoiceDomain().getVatRate(), Constants.ZERO));
        invoiceCoverPageFristLine.setInvoiceBaseAmount(this.formatBigDecimalToString(
            companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSBaseAmount(), scale));
        invoiceCoverPageFristLine.setInvoiceVatAmount(this.formatBigDecimalToString(
            companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSVatAmount(), scale));
        invoiceCoverPageFristLine.setInvoiceTotalAmount(this.formatBigDecimalToString(
            companyDecimalFormat, firstRecord.getSpsTInvoiceDomain().getSTotalAmount(), scale));
        
        invoiceCoverPageFristLine.setFlag(STR_ONE);
        invoiceCoverPageReportList.add(invoiceCoverPageFristLine);
        
        for(InvoiceCoverPageDomain invoiceItem : invoiceCoverPageList){
            InvoiceCoverPageReportDomain invoiceCoverPage = this.getCoverPageHeader(
                strInvoiceCoverPageNo,
                invoiceCoverPageList.get(ZERO).getSpsTInvoiceDomain(), locale);
            invoiceCoverPage.setInvoiceRunningNo(String.valueOf(++invoiceRunningNo));
            invoiceCoverPage.setDocumentType(ASN_DOCUMENT_TYPE);
            invoiceCoverPage.setDocumentDate(DateUtil.format(
                invoiceItem.getInvoiceCoverPageDetailDomainList().get(ZERO)
                .getSpsTAsnDomain().getCreateDatetime(), PATTERN_YYYYMMDD_SLASH));
            
            documentNumberBuffer.setLength(ZERO);
            documentNumberBuffer.append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                .append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                .append(invoiceItem.getInvoiceCoverPageDetailDomainList()
                    .get(ZERO).getSpsTInvoiceDetailDomain().getAsnNo());
            
            invoiceCoverPage.setDocumentNo(documentNumberBuffer.toString());
            
            // [IN009] Rounding Mode use HALF_UP
            //invoiceCoverPage.setAsnBaseAmount(this.formatBigDecimalToString(companyDecimalFormat,
            //    invoiceItem.getInvoiceCoverPageDetailDomainList()
            //        .get(ZERO).getSpsTInvoiceDetailDomain().getBaseAmt()));
            //invoiceCoverPage.setTaxRate(this.formatBigDecimalToString(noDecimalFormat,
            //    invoiceItem.getSpsTInvoiceDomain().getVatRate()));
            //invoiceCoverPage.setInvoiceBaseAmount(this.formatBigDecimalToString(
            //    companyDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getSBaseAmount()));
            //invoiceCoverPage.setInvoiceVatAmount(this.formatBigDecimalToString(
            //    companyDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getSVatAmount()));
            //invoiceCoverPage.setInvoiceTotalAmount(this.formatBigDecimalToString(
            //    companyDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getSTotalAmount()));
            invoiceCoverPage.setAsnBaseAmount(this.formatBigDecimalToString(
                companyDecimalFormat, invoiceItem.getInvoiceCoverPageDetailDomainList()
                    .get(ZERO).getSpsTInvoiceDetailDomain().getBaseAmt(), scale));
            invoiceCoverPage.setTaxRate(this.formatBigDecimalToString(
                noDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getVatRate(), Constants.ZERO));
            invoiceCoverPage.setInvoiceBaseAmount(this.formatBigDecimalToString(
                companyDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getSBaseAmount(), scale));
            invoiceCoverPage.setInvoiceVatAmount(this.formatBigDecimalToString(
                companyDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getSVatAmount(), scale));
            invoiceCoverPage.setInvoiceTotalAmount(this.formatBigDecimalToString(
                companyDecimalFormat, invoiceItem.getSpsTInvoiceDomain().getSTotalAmount(), scale));
            
            invoiceCoverPage.setFlag(STR_ONE);
            invoiceCoverPageReportList.add(invoiceCoverPage);
        }

        int gapCount = this.calculateInsertGap(invoiceRunningNo,
            ONE + cnCoverPageList.size());
        for(int i = 0; i < gapCount; i++) {
            InvoiceCoverPageReportDomain invoiceCoverPage = new InvoiceCoverPageReportDomain();
            invoiceCoverPage.setCnRunningNo(EMPTY_STRING);
            invoiceCoverPage.setFlag(Constants.STR_TWO);
            invoiceCoverPageReportList.add(invoiceCoverPage);
        }
        
        if(ZERO == cnCoverPageList.size()){
            InvoiceCoverPageReportDomain cnCoverPage = this.getCoverPageHeader(
                strInvoiceCoverPageNo,
                invoiceCoverPageList.get(ZERO).getSpsTInvoiceDomain(), locale);
            cnCoverPage.setCnRunningNo(EMPTY_STRING);
            cnCoverPage.setDocumentDate(EMPTY_STRING);
            cnCoverPage.setDocumentType(EMPTY_STRING);
            
            documentNumberBuffer.setLength(ZERO);
            documentNumberBuffer.append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                .append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                .append(MessageUtil.getLabelHandledException(locale, 
                    LBL_NO_PRICE_DIFFERENCEN));
            
            cnCoverPage.setDocumentNo(documentNumberBuffer.toString());
            cnCoverPage.setAsnDifferenceAmount(EMPTY_STRING);
            cnCoverPage.setCnAmount(EMPTY_STRING);
            cnCoverPage.setFlag(Constants.STR_FOUR);
            invoiceCoverPageReportList.add(cnCoverPage);
        }else{
            
            // CN Header at first row.
            CnInformationDomain firstCn = cnCoverPageList.get(ZERO);
            InvoiceCoverPageReportDomain cnCoverFirstLine = this.getCoverPageHeader(
                strInvoiceCoverPageNo,
                invoiceCoverPageList.get(ZERO).getSpsTInvoiceDomain(), locale);
            cnCoverFirstLine.setCnRunningNo(String.valueOf(++cnRunningNo));
            cnCoverFirstLine.setDocumentType(INV_DOCUMENT_TYPE);
            cnCoverFirstLine.setDocumentDate(
                    DateUtil.format(firstCn.getCnDate(), PATTERN_YYYYMMDD_SLASH));
            cnCoverFirstLine.setDocumentNo(firstCn.getCnNo());
            cnCoverFirstLine.setAsnDifferenceAmount(EMPTY_STRING);
            cnCoverFirstLine.setDocumentType(CN_DOCUMENT_TYPE);
            
            // [IN009] Rounding Mode use HALF_UP
            //cnCoverFirstLine.setCnAmount(this.formatBigDecimalToString(companyDecimalFormat,
            //    firstCn.getBaseAmount()));
            //cnCoverFirstLine.setCnBaseAmount(this.formatBigDecimalToString(companyDecimalFormat,
            //    firstCn.getBaseAmount()));
            //cnCoverFirstLine.setCnVatAmount(this.formatBigDecimalToString(companyDecimalFormat,
            //    firstCn.getVatAmount()));
            //cnCoverFirstLine.setCnTotalAmount(this.formatBigDecimalToString(companyDecimalFormat,
            //    firstCn.getTotalAmount()));
            cnCoverFirstLine.setCnAmount(this.formatBigDecimalToString(
                companyDecimalFormat, firstCn.getBaseAmount(), scale));
            cnCoverFirstLine.setCnBaseAmount(this.formatBigDecimalToString(
                companyDecimalFormat, firstCn.getBaseAmount(), scale));
            cnCoverFirstLine.setCnVatAmount(this.formatBigDecimalToString(
                companyDecimalFormat, firstCn.getVatAmount(), scale));
            cnCoverFirstLine.setCnTotalAmount(this.formatBigDecimalToString(
                companyDecimalFormat, firstCn.getTotalAmount(), scale));
            
            cnCoverFirstLine.setFlag(Constants.STR_THREE);
            invoiceCoverPageReportList.add(cnCoverFirstLine);
            
            BigDecimal zeroBigDecimal = new BigDecimal(ZERO);
            
            for(CnInformationDomain cnItem : cnCoverPageList){
                InvoiceCoverPageReportDomain invoiceCoverPage = this.getCoverPageHeader(
                    strInvoiceCoverPageNo,
                    invoiceCoverPageList.get(ZERO).getSpsTInvoiceDomain(), locale);
                invoiceCoverPage.setCnRunningNo(String.valueOf(++cnRunningNo));
                // [FIX] ASN in invoice section type=ASN, date=ASN Date
                //invoiceCoverPage.setDocumentType(CN_DOCUMENT_TYPE);
                //invoiceCoverPage.setDocumentDate(
                //    DateUtil.format(cnItem.getCnDate(), PATTERN_YYYYMMDD_SLASH));
                invoiceCoverPage.setDocumentType(ASN_DOCUMENT_TYPE);
                invoiceCoverPage.setDocumentDate(
                    DateUtil.format(cnItem.getAsnDate(), PATTERN_YYYYMMDD_SLASH));
                
                documentNumberBuffer.setLength(ZERO);
                documentNumberBuffer.append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                    .append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                    .append(cnItem.getAsnNo());
                
                if (null == cnItem.getAsnAmount()) {
                    cnItem.setAsnAmount(zeroBigDecimal);
                }

                invoiceCoverPage.setCnAmount(EMPTY_STRING);
                // [FIX] duplicate statement
                //invoiceCoverPage.setDocumentType(ASN_DOCUMENT_TYPE);
                invoiceCoverPage.setDocumentNo(documentNumberBuffer.toString());

                // [IN009] Rounding Mode use HALF_UP
                //invoiceCoverPage.setAsnDifferenceAmount(this.formatBigDecimalToString(
                //    companyDecimalFormat, cnItem.getAsnAmount().abs()));
                //invoiceCoverPage.setCnBaseAmount(this.formatBigDecimalToString(
                //    companyDecimalFormat, cnItem.getBaseAmount()));
                //invoiceCoverPage.setCnVatAmount(this.formatBigDecimalToString(
                //    companyDecimalFormat, cnItem.getVatAmount()));
                //invoiceCoverPage.setCnTotalAmount(this.formatBigDecimalToString(
                //    companyDecimalFormat, cnItem.getTotalAmount()));
                // [FIX] Don't have to absolute value of CN
                //invoiceCoverPage.setAsnDifferenceAmount(this.formatBigDecimalToString(
                //    companyDecimalFormat, cnItem.getAsnAmount().abs(), scale));
                invoiceCoverPage.setAsnDifferenceAmount(this.formatBigDecimalToString(
                    companyDecimalFormat, cnItem.getAsnAmount(), scale));
                invoiceCoverPage.setCnBaseAmount(this.formatBigDecimalToString(
                    companyDecimalFormat, cnItem.getBaseAmount(), scale));
                invoiceCoverPage.setCnVatAmount(this.formatBigDecimalToString(
                    companyDecimalFormat, cnItem.getVatAmount(), scale));
                invoiceCoverPage.setCnTotalAmount(this.formatBigDecimalToString(
                    companyDecimalFormat, cnItem.getTotalAmount(), scale));
                
                invoiceCoverPage.setFlag(Constants.STR_THREE);
                invoiceCoverPageReportList.add(invoiceCoverPage);
            }
        }

        return invoiceCoverPageReportList;
    }
    
    /**
     * <p>Create invoice cover page data for view.</p>
     * <ul>
     * <li>Preparation data for create invoice cover page report for view mode.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return list of the Invoice Cover Page Report Domain 
     * @throws ApplicationException 
     */
    private List<InvoiceCoverPageReportDomain> createInvoiceCoverPageDataForView(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain)
        throws ApplicationException{
        
        Locale locale = invoiceMaintenanceDomain.getLocale();
        
        List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList 
            = new ArrayList<InvoiceCoverPageReportDomain>();
        Map<String, BigDecimal> asnSumData = new HashMap<String, BigDecimal>();
        Map<String, BigDecimal> cnSumData = new HashMap<String, BigDecimal>();
        Map<String, AsnDomain> asnData = new HashMap<String, AsnDomain>();
        
        int invoiceRunningNo = ZERO;
        int cnRunningNo = ZERO;

        NumberFormat companyNumberFormat = NumberFormat.getInstance();
        DecimalFormat companyDecimalFormat = (DecimalFormat)companyNumberFormat;
        companyDecimalFormat.setGroupingUsed(true);
        int scale = 0;
        if(!StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getDecimalDisp())){
            scale = Integer.valueOf(invoiceMaintenanceDomain.getDecimalDisp());
        }
        companyDecimalFormat.setMinimumFractionDigits(scale);
        companyDecimalFormat.setMaximumFractionDigits(scale);
        
        NumberFormat defaultNumberFormat = NumberFormat.getInstance();
        DecimalFormat noDecimalFormat = (DecimalFormat)defaultNumberFormat;
        noDecimalFormat.setGroupingUsed(true);
        noDecimalFormat.setMinimumFractionDigits(Constants.ZERO);
        noDecimalFormat.setMaximumFractionDigits(Constants.ZERO);

        for(AsnDomain asnItem : invoiceMaintenanceDomain.getAsnDetailList()){
            
            String asnNo = asnItem.getAsnNo();
            BigDecimal densoBaseAmount = new BigDecimal(
                StringUtil.removeComma(asnItem.getSupplierUnitPrice()))
                    .multiply(new BigDecimal(StringUtil.removeComma(asnItem.getShippingQty())));
            // [IN010] : calculate summary after rounding
            densoBaseAmount = densoBaseAmount.setScale(scale, RoundingMode.HALF_UP);
            
            asnData.put(asnNo, asnItem);
            if(asnSumData.containsKey(asnNo)){
                BigDecimal sumBaseAmount = asnSumData.get(asnNo);
                sumBaseAmount = sumBaseAmount.add(densoBaseAmount);
                asnSumData.put(asnNo, sumBaseAmount);
            }else{
                asnSumData.put(asnNo, densoBaseAmount);
            }
            
            if(ZERO != new BigDecimal(asnItem.getBdDiffBaseAmount()).compareTo(
                Constants.BIG_DECIMAL_ZERO)){
                if(cnSumData.containsKey(asnNo)){
                    BigDecimal sumDiffBaseAmount = cnSumData.get(asnNo);
                    sumDiffBaseAmount = sumDiffBaseAmount.add(
                        NumberUtil.toBigDecimal(asnItem.getBdDiffBaseAmount()));
                    cnSumData.put(asnNo, sumDiffBaseAmount);
                }else{
                    cnSumData.put(asnNo, 
                        NumberUtil.toBigDecimal(asnItem.getBdDiffBaseAmount()));
                }
            }
            
        }
        
        StringBuffer documentNumberBuffer = new StringBuffer();
        
        InvoiceCoverPageReportDomain invoiceCoverPageFristLine 
            = this.getCoverPageHeaderForView(invoiceMaintenanceDomain);
        invoiceCoverPageFristLine.setInvoiceRunningNo(String.valueOf(++invoiceRunningNo));
        invoiceCoverPageFristLine.setDocumentType(INV_DOCUMENT_TYPE);
        invoiceCoverPageFristLine.setDocumentDate(invoiceMaintenanceDomain.getInvoiceDate());
        invoiceCoverPageFristLine.setDocumentNo(invoiceMaintenanceDomain.getInvoiceNo());
        invoiceCoverPageFristLine.setAsnBaseAmount(EMPTY_STRING);
        
        // [IN009] Rounding Mode use HALF_UP
        //invoiceCoverPageFristLine.setInvoiceAmount(
        //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
        //        invoiceMaintenanceDomain.getSupplierBaseAmount())));
        //invoiceCoverPageFristLine.setInvoiceBaseAmount(
        //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
        //        invoiceMaintenanceDomain.getSupplierBaseAmount())));
        //invoiceCoverPageFristLine.setInvoiceVatAmount(
        //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
        //        invoiceMaintenanceDomain.getSupplierVatAmount())));
        //invoiceCoverPageFristLine.setInvoiceTotalAmount(
        //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
        //        invoiceMaintenanceDomain.getSupplierTotalAmount())));
        invoiceCoverPageFristLine.setInvoiceAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, invoiceMaintenanceDomain.getSupplierBaseAmount(), scale));
        invoiceCoverPageFristLine.setInvoiceBaseAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, invoiceMaintenanceDomain.getSupplierBaseAmount(), scale));
        invoiceCoverPageFristLine.setInvoiceTotalAmount(NumberUtil.formatRoundingHalfUp(
            companyDecimalFormat, invoiceMaintenanceDomain.getSupplierTotalAmount(), scale));
        // Start : [FIX] None VAT will not input any VAT amount
        //invoiceCoverPageFristLine.setInvoiceVatAmount(NumberUtil.formatRoundingHalfUp(
        //    companyDecimalFormat, invoiceMaintenanceDomain.getSupplierVatAmount(), scale));
        boolean isVatTypeVat = Constants.STR_ONE.equals(invoiceMaintenanceDomain.getVatType());
        if (isVatTypeVat) {
            invoiceCoverPageFristLine.setInvoiceVatAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, invoiceMaintenanceDomain.getSupplierVatAmount(), scale));
        } else {
            invoiceCoverPageFristLine.setInvoiceVatAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, Constants.STR_ZERO, scale));
        }
        // End : [FIX] None VAT will not input any VAT amount
        
        invoiceCoverPageFristLine.setFlag(STR_ONE);
        invoiceCoverPageReportList.add(invoiceCoverPageFristLine);
        
        for(String asnKey : asnSumData.keySet()){
            InvoiceCoverPageReportDomain invoiceCoverPage 
                = this.getCoverPageHeaderForView(invoiceMaintenanceDomain);
            invoiceCoverPage.setInvoiceRunningNo(String.valueOf(++invoiceRunningNo));
            invoiceCoverPage.setDocumentType(ASN_DOCUMENT_TYPE);
            invoiceCoverPage.setDocumentDate(DateUtil.format(asnData.get(asnKey)
                .getSpsTAsnDomain().getCreateDatetime(), PATTERN_YYYYMMDD_SLASH));

            documentNumberBuffer.setLength(ZERO);
            documentNumberBuffer.append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                .append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                .append(asnData.get(asnKey).getAsnNo());
            
            invoiceCoverPage.setDocumentNo(documentNumberBuffer.toString());
            invoiceCoverPage.setAsnBaseAmount(companyDecimalFormat.format(
                asnSumData.get(asnKey).setScale(scale, RoundingMode.HALF_UP)));
            invoiceCoverPage.setInvoiceAmount(EMPTY_STRING);
            
            // [IN009] Rounding Mode use HALF_UP
            //invoiceCoverPage.setInvoiceBaseAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getSupplierBaseAmount())));
            //invoiceCoverPage.setInvoiceVatAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getSupplierVatAmount())));
            //invoiceCoverPage.setInvoiceTotalAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getSupplierTotalAmount())));
            invoiceCoverPage.setInvoiceBaseAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, invoiceMaintenanceDomain.getSupplierBaseAmount(), scale));
            invoiceCoverPage.setInvoiceTotalAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, invoiceMaintenanceDomain.getSupplierTotalAmount(), scale));
            // Start : [FIX] None VAT will not input any VAT amount
            //invoiceCoverPage.setInvoiceVatAmount(NumberUtil.formatRoundingHalfUp(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getSupplierVatAmount(), scale));
            if (isVatTypeVat) {
                invoiceCoverPage.setInvoiceVatAmount(NumberUtil.formatRoundingHalfUp(
                    companyDecimalFormat, invoiceMaintenanceDomain.getSupplierVatAmount(), scale));
            } else {
                invoiceCoverPage.setInvoiceVatAmount(NumberUtil.formatRoundingHalfUp(
                    companyDecimalFormat, Constants.STR_ZERO, scale));
            }
            // End : [FIX] None VAT will not input any VAT amount
            
            invoiceCoverPage.setFlag(STR_ONE);
            invoiceCoverPageReportList.add(invoiceCoverPage);
        }

        int gapCount = this.calculateInsertGap(invoiceRunningNo, ONE + cnSumData.size());
        for(int i = 0; i < gapCount; i++) {
            InvoiceCoverPageReportDomain invoiceCoverPage = new InvoiceCoverPageReportDomain();
            invoiceCoverPage.setCnRunningNo(EMPTY_STRING);
            invoiceCoverPage.setFlag(Constants.STR_TWO);
            invoiceCoverPageReportList.add(invoiceCoverPage);
        }
        
        if(ZERO == cnSumData.size()){
            cnRunningNo++;
            InvoiceCoverPageReportDomain cnCoverPage
                = this.getCoverPageHeaderForView(invoiceMaintenanceDomain);
            cnCoverPage.setCnRunningNo(EMPTY_STRING);
            cnCoverPage.setDocumentDate(EMPTY_STRING);
            cnCoverPage.setDocumentType(EMPTY_STRING);
            cnCoverPage.setDocumentNo(
                MessageUtil.getLabelHandledException(locale, 
                    LBL_NO_PRICE_DIFFERENCEN));
            cnCoverPage.setAsnDifferenceAmount(EMPTY_STRING);
            cnCoverPage.setCnAmount(EMPTY_STRING);
            cnCoverPage.setFlag(Constants.STR_FOUR);
            invoiceCoverPageReportList.add(cnCoverPage);
        } else {
            InvoiceCoverPageReportDomain cnCoverPageFristLine
                = this.getCoverPageHeaderForView(invoiceMaintenanceDomain);
            cnCoverPageFristLine.setCnRunningNo(String.valueOf(++cnRunningNo));
            cnCoverPageFristLine.setDocumentDate(invoiceMaintenanceDomain.getCnDate());
            cnCoverPageFristLine.setDocumentNo(invoiceMaintenanceDomain.getCnNo());
            cnCoverPageFristLine.setAsnDifferenceAmount(EMPTY_STRING);
            cnCoverPageFristLine.setDocumentType(CN_DOCUMENT_TYPE);
            
            // [IN009] Rounding Mode use HALF_UP
            //cnCoverPageFristLine.setCnAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getCnBaseAmount())));
            //cnCoverPageFristLine.setDocumentType(CN_DOCUMENT_TYPE);
            //cnCoverPageFristLine.setCnBaseAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getCnBaseAmount())));
            //cnCoverPageFristLine.setCnVatAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getCnVatAmount())));
            //cnCoverPageFristLine.setCnTotalAmount(
            //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
            //        invoiceMaintenanceDomain.getCnTotalAmount())));
            cnCoverPageFristLine.setCnAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, invoiceMaintenanceDomain.getCnBaseAmount(), scale));
            cnCoverPageFristLine.setCnBaseAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, invoiceMaintenanceDomain.getCnBaseAmount(), scale));
            cnCoverPageFristLine.setCnTotalAmount(NumberUtil.formatRoundingHalfUp(
                companyDecimalFormat, invoiceMaintenanceDomain.getCnTotalAmount(), scale));
            // Start : [FIX] None VAT will not input any VAT amount
            //cnCoverPageFristLine.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
            //    companyDecimalFormat, invoiceMaintenanceDomain.getCnVatAmount(), scale));
            if (isVatTypeVat) {
                cnCoverPageFristLine.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
                    companyDecimalFormat, invoiceMaintenanceDomain.getCnVatAmount(), scale));
            } else {
                cnCoverPageFristLine.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
                    companyDecimalFormat, Constants.STR_ZERO, scale));
            }
            // End : [FIX] None VAT will not input any VAT amount
            
            cnCoverPageFristLine.setFlag(Constants.STR_THREE);
            invoiceCoverPageReportList.add(cnCoverPageFristLine);
            
            for(String cnKey:cnSumData.keySet()) {
                
                InvoiceCoverPageReportDomain cnCoverPage 
                    = this.getCoverPageHeaderForView(invoiceMaintenanceDomain);
                cnCoverPage.setCnRunningNo(String.valueOf(++cnRunningNo));
                cnCoverPage.setDocumentDate(DateUtil.format(asnData.get(cnKey)
                    .getSpsTAsnDomain().getCreateDatetime(), PATTERN_YYYYMMDD_SLASH));
                
                documentNumberBuffer.setLength(ZERO);
                documentNumberBuffer.append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                    .append(SYMBOL_SPACE).append(SYMBOL_SPACE)
                    .append(asnData.get(cnKey).getAsnNo());
                
                cnCoverPage.setDocumentNo(documentNumberBuffer.toString());
                
                // [FIX] Don't have to absolute value of CN
                //cnCoverPage.setAsnDifferenceAmount(companyDecimalFormat.format(
                //    cnSumData.get(cnKey).abs().setScale(scale, RoundingMode.HALF_UP)));
                cnCoverPage.setAsnDifferenceAmount(companyDecimalFormat.format(
                    cnSumData.get(cnKey).setScale(scale, RoundingMode.HALF_UP)));
                
                cnCoverPage.setCnAmount(EMPTY_STRING);
                cnCoverPage.setDocumentType(ASN_DOCUMENT_TYPE);
                
                // [IN009] Rounding Mode use HALF_UP
                //cnCoverPage.setCnBaseAmount(
                //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
                //        invoiceMaintenanceDomain.getCnBaseAmount())));
                //cnCoverPage.setCnVatAmount(
                //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
                //        invoiceMaintenanceDomain.getCnVatAmount())));
                //cnCoverPage.setCnTotalAmount(
                //    companyDecimalFormat.format(NumberUtil.toBigDecimal(
                //        invoiceMaintenanceDomain.getCnTotalAmount())));
                cnCoverPage.setCnBaseAmount(NumberUtil.formatRoundingHalfUp(
                    companyDecimalFormat, invoiceMaintenanceDomain.getCnBaseAmount(), scale));
                cnCoverPage.setCnTotalAmount(NumberUtil.formatRoundingHalfUp(
                    companyDecimalFormat, invoiceMaintenanceDomain.getCnTotalAmount(), scale));
                // Start : [FIX] None VAT will not input any VAT amount
                //cnCoverPage.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
                //    companyDecimalFormat, invoiceMaintenanceDomain.getCnVatAmount(), scale));
                if (isVatTypeVat) {
                    cnCoverPage.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
                        companyDecimalFormat, invoiceMaintenanceDomain.getCnVatAmount(), scale));
                } else {
                    cnCoverPage.setCnVatAmount(NumberUtil.formatRoundingHalfUp(
                        companyDecimalFormat, Constants.STR_ZERO, scale));
                }
                // End : [FIX] None VAT will not input any VAT amount
                
                cnCoverPage.setFlag(Constants.STR_THREE);
                invoiceCoverPageReportList.add(cnCoverPage);
            }
        }

        return invoiceCoverPageReportList;
    }
    
    /**
     * <p>Get cover page header.</p>
     * <ul>
     * <li>Preparation header data for create invoice cover page report.</li>
     * </ul>
     * 
     * @param invoiceCoverPageNo the Invoice Cover Page No
     * @param coverPageHeader the SpsTInvoiceDomain 
     * @param locale for get property file
     * @return the Invoice Cover Page Domain 
     */
    private InvoiceCoverPageReportDomain getCoverPageHeader(String invoiceCoverPageNo,
        SpsTInvoiceDomain coverPageHeader, Locale locale){
        
        InvoiceCoverPageReportDomain invoiceCoverPage = new InvoiceCoverPageReportDomain();
        invoiceCoverPage.setCoverPageNo(invoiceCoverPageNo);
        invoiceCoverPage.setCompanySupplierName(coverPageHeader.getSCompanyName());
        invoiceCoverPage.setSupplierTaxId(coverPageHeader.getSTaxId());
        invoiceCoverPage.setSupplierAddress1(coverPageHeader.getSAddress1());
        invoiceCoverPage.setSupplierAddress2(coverPageHeader.getSAddress2());
        invoiceCoverPage.setSupplierAddress3(coverPageHeader.getSAddress3());
        invoiceCoverPage.setInvoiceCurrency(coverPageHeader.getSCurrencyCd());
        invoiceCoverPage.setCompanyDensoName(coverPageHeader.getDCompanyName());
        invoiceCoverPage.setDensoTaxId(coverPageHeader.getDTaxId());
        invoiceCoverPage.setDCd(coverPageHeader.getDCd());
        invoiceCoverPage.setDPcd(coverPageHeader.getDPcd());
        invoiceCoverPage.setDensoAddress(coverPageHeader.getDpAddress());
        invoiceCoverPage.setTaxType(MessageUtil.getLabelHandledException(locale, LBL_VAT));
        invoiceCoverPage.setTaxRate(String.valueOf(coverPageHeader.getVatRate()));
        return invoiceCoverPage;
    }
    
    /**
     * <p>Get cover page header.</p>
     * <ul>
     * <li>Preparation header data for create invoice cover page report.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain
     * @return the Invoice Cover Page Domain 
     */
    private InvoiceCoverPageReportDomain getCoverPageHeaderForView(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain){
        
        Locale locale = invoiceMaintenanceDomain.getLocale();
        
        InvoiceCoverPageReportDomain invoiceCoverPage = new InvoiceCoverPageReportDomain();
        invoiceCoverPage.setCoverPageNo(EMPTY_STRING);
        invoiceCoverPage.setCompanySupplierName(invoiceMaintenanceDomain.getSupplierName());
        invoiceCoverPage.setSupplierTaxId(invoiceMaintenanceDomain.getSupplierTaxId());
        invoiceCoverPage.setSupplierAddress1(invoiceMaintenanceDomain.getSupplierAddress1());
        invoiceCoverPage.setSupplierAddress2(invoiceMaintenanceDomain.getSupplierAddress2());
        invoiceCoverPage.setSupplierAddress3(invoiceMaintenanceDomain.getSupplierAddress3());
        invoiceCoverPage.setInvoiceCurrency(invoiceMaintenanceDomain.getSupplierCurrency());
        invoiceCoverPage.setCompanyDensoName(invoiceMaintenanceDomain.getDensoName());
        invoiceCoverPage.setDensoTaxId(invoiceMaintenanceDomain.getDensoTaxId());
        invoiceCoverPage.setDCd(invoiceMaintenanceDomain.getDCd());
        invoiceCoverPage.setDPcd(invoiceMaintenanceDomain.getDPcd());
        invoiceCoverPage.setDensoAddress(invoiceMaintenanceDomain.getDensoAddress());
        invoiceCoverPage.setTaxType(MessageUtil.getLabelHandledException(locale, 
            LBL_VAT));
        // [IN059] If VAT rate is blank, show 0
        //invoiceCoverPage.setTaxRate(String.valueOf(invoiceMaintenanceDomain.getVatRate()));
        if (Strings.judgeBlank(invoiceMaintenanceDomain.getVatRate())) {
            invoiceCoverPage.setTaxRate(Constants.STR_ZERO);
        } else {
            invoiceCoverPage.setTaxRate(invoiceMaintenanceDomain.getVatRate());
        }
        
        return invoiceCoverPage;
    }
    
    /**
     * Create email detail when register Invoice with CN or Temp Price.
     * @param invMaintenance invoice maintenance domain
     * @param invoiceCoverPageNo Invoice Cover Page No.
     * @return SendEmailDomain
     * @throws Exception Exception
     * 
     * */
    private SendEmailDomain getEmailCreateInvoiceWithCnContent(
        InvoiceMaintenanceDomain invMaintenance, String invoiceCoverPageNo)
        throws Exception
    {
        SendEmailDomain result = new SendEmailDomain();
        Locale locale = invMaintenance.getLocale();
        
        SpsMUserCriteriaDomain userCriteria = new SpsMUserCriteriaDomain();
        userCriteria.setDscId(invMaintenance.getDscId());
        SpsMUserDomain user = this.spsMUserService.searchByKey(userCriteria);
        String firstName = Constants.EMPTY_STRING;
        if (null != user.getFirstName()) {
            firstName = user.getFirstName();
        }
        String middleName = Constants.EMPTY_STRING;
        if (null != user.getMiddleName()) {
            middleName = user.getMiddleName();
        }
        String lastName = Constants.EMPTY_STRING;
        if (null != user.getLastName()) {
            lastName = user.getLastName();
        }
        
        SpsMCompanyDensoCriteriaDomain densoCriteria = new SpsMCompanyDensoCriteriaDomain();
        densoCriteria.setDCd(invMaintenance.getDCd());
        SpsMCompanyDensoDomain denso = this.spsMCompanyDensoService.searchByKey(densoCriteria);
        
        String subject = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_SUBJECT_INVOICE_CN);
        String headerText1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE1);
        String headerText2 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE2_CN);
        String headerText3 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE3);
        String headerText4 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE4);
        String headerText5 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TEXT_LINE5);
        String headerTable1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TABLE_LINE1);
        String headerTable2 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_HEADER_TABLE_LINE2);
        String detailTable1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_DETAIL_TABLE_LINE1);
        String detailTable3 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_DETAIL_TABLE_LINE3);
        String footer1 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_FOOTER_LINE1);
        String footer2 = MessageUtil.getEmailLabel(locale, MAIL_INVOICE_FOOTER_LINE2);
        
        //#invoiceNo#
        subject = subject.replace(REPLACE_INVOICE_NO, invMaintenance.getInvoiceNo());
        result.setHeader(subject);
        
        StringBuffer detail = new StringBuffer();
        detail.append(headerText1);
        detail.append(headerText2);
        
        // #coverPageNo# #supplierCode# #densoCode# #densoPlantCode#
        headerText3 = headerText3.replace(REPLACE_COVER_PAGE_NO, invoiceCoverPageNo)
            .replace(REPLACE_SCD, invMaintenance.getSCd())
            .replace(REPLACE_DCD, invMaintenance.getDCd())
            .replace(REPLACE_DPCD, invMaintenance.getDPcd());
        detail.append(headerText3);
        
        // #invoiceNo# #invoiceDate# #totalInvoiceAmount# 
        headerText4 = headerText4.replace(REPLACE_INVOICE_NO, invMaintenance.getInvoiceNo())
            .replace(REPLACE_INVOICE_DATE, invMaintenance.getInvoiceDate())
            .replace(REPLACE_TOTAL_INVOICE_AMOUNT, invMaintenance.getSupplierTotalAmount());
        detail.append(headerText4);
        
        String cnNo = Constants.EMPTY_STRING;
        String cnDate = Constants.EMPTY_STRING;
        String cnAmount = Constants.EMPTY_STRING;
        if (null != invMaintenance.getCnNo()) {
            cnNo = invMaintenance.getCnNo();
        }
        if (null != invMaintenance.getCnDate()) {
            cnDate = invMaintenance.getCnDate();
        }
        if (null != invMaintenance.getCnTotalAmount()) {
            cnAmount = invMaintenance.getCnTotalAmount();
        }
        
        // #cnNo# #cnDate# #totalCnAmount#
        headerText5 = headerText5.replace(REPLACE_CN_NO, cnNo)
            .replace(REPLACE_CN_DATE, cnDate)
            .replace(REPLACE_TOTAL_CN_AMOUNT, cnAmount);
        detail.append(headerText5);
        
        detail.append(headerTable1);
        detail.append(headerTable2);
        Map<String, Timestamp> planEtaMap = new HashMap<String, Timestamp>();
        for(AsnDomain asnItem : invMaintenance.getAsnDetailList()){
            String rowColor = Constants.COLOR_WHITE;
            
            if(ZERO != Constants.BIG_DECIMAL_ZERO.compareTo(
                NumberUtil.toBigDecimal(asnItem.getBdDiffBaseAmount()))
                || Constants.STR_T.equals(asnItem.getTempPriceFlag()))
            {
                rowColor = Constants.COLOR_YELLOW;
            }
            
            String planEtaKey = StringUtil.appendsString(asnItem.getAsnNo(), invMaintenance.getDCd());
            Timestamp planEta = planEtaMap.get(planEtaKey);
            if (null == planEta) {
                SpsTAsnCriteriaDomain asnCriteria = new SpsTAsnCriteriaDomain();
                asnCriteria.setAsnNo(asnItem.getAsnNo());
                asnCriteria.setDCd(invMaintenance.getDCd());
                SpsTAsnDomain asn = this.spsTAsnService.searchByKey(asnCriteria);
                planEta = asn.getPlanEta();
                planEtaMap.put(planEtaKey, asn.getPlanEta());
            }
            
            String etaDateTime = DateUtil.format(planEta, DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH);
            
            String diffBaseAmount = Constants.STR_ZERO;
            if (null != asnItem.getDiffBaseAmount()) {
                diffBaseAmount = asnItem.getDiffBaseAmount();
            }
            
            /* #rowColor# #firstName# #middleName# #lastName# #asnNo# #goodRcvStatus# #planEta#
             * #dPartNo# #dUnitPrice# #sUnitPrice# #tempPrice# #shippingQty# #dUnitOfMeasure#
             * #diffBaseAmtByPart#
             * */
            detail.append(detailTable1.replace(REPLACE_ROW_COLOR, rowColor)
                .replace(REPLACE_FIRST_NAME, firstName)
                .replace(REPLACE_MIDDLE_NAME, middleName)
                .replace(REPLACE_LAST_NAME, lastName)
                .replace(REPLACE_ASNNO, asnItem.getAsnNo())
                .replace(REPLACE_GOOD_RCV_STATUS, asnItem.getAsnStatus())
                .replace(REPLACE_PLAN_ETA, StringUtil.appendsString(
                    etaDateTime
                    , Constants.SYMBOL_SPACE
                    , Constants.SYMBOL_OPEN_BRACKET
                    , Constants.WORD_UTC, denso.getUtc().trim()
                    , Constants.SYMBOL_CLOSE_BRACKET))
                .replace(REPLACE_DPARTNO, asnItem.getDPn())
                .replace(REPLACE_D_UNIT_PRICE, asnItem.getDensoUnitPrice())
                .replace(REPLACE_S_UNIT_PRICE, asnItem.getSupplierUnitPrice())
                .replace(REPLACE_TEMP_PRICE, asnItem.getTempPriceFlag())
                .replace(REPLACE_SHIPPING_QTY, asnItem.getShippingQty())
                .replace(REPLACE_D_UNIT_OF_MEASURE, asnItem.getDensoUnitOfMeasure())
                .replace(REPLACE_DIFF_BASE_AMT_BY_PART, diffBaseAmount));
        }
        detail.append(detailTable3);
        detail.append(footer1);
        detail.append(footer2);
        
        result.setContents(detail.toString());
        
        return result;
    }
    
    /**
     * <p>Validate.</p>
     * <ul>
     * <li>Validation data before register data and creation invoice cover page report.</li>
     * </ul>
     * 
     * @param invoiceMaintenanceDomain the Invoice Maintenance Domain 
     * @return list of the Application Message Domain 
     * @throws ApplicationException 
     */
    private List<ApplicationMessageDomain> validate(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain) throws ApplicationException{
        
        Locale locale = invoiceMaintenanceDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        
        BigDecimal supplierBase = null;
        BigDecimal supplierVat = null;
        BigDecimal supplierTotal = null;
        BigDecimal cnBase = null;
        BigDecimal cnVat = null;
        BigDecimal cnTotal = null;
        BigDecimal diffBase = null;
        BigDecimal diffVat = null;
        BigDecimal diffTotal = null;
        BigDecimal totalBase = null;
        BigDecimal totalVat = null;
        BigDecimal grandTotal = null;
        
        // INVOICE_NO is null
        if (Strings.judgeBlank(invoiceMaintenanceDomain.getInvoiceNo())) {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0007, LBL_INVOICE_NO);
        } else {
            SpsTInvoiceCriteriaDomain criteria = new SpsTInvoiceCriteriaDomain();
            criteria.setVendorCd(invoiceMaintenanceDomain.getVendorCd());
            criteria.setInvoiceNo(invoiceMaintenanceDomain.getInvoiceNo());
            List<SpsTInvoiceDomain> result = 
                this.spsTInvoiceService.searchByCondition(criteria);
            Iterator<SpsTInvoiceDomain> iterator = result.iterator();
            while(iterator.hasNext()) {
                SpsTInvoiceDomain item = iterator.next();
                if (Constants.INVOICE_STATUS_DCL.equals(item.getInvoiceStatus())) {
                    iterator.remove();
                }
            }
            // INVOICE_NO duplicate
            if(ZERO != result.size()) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0037, LBL_CREATE_INVOICE_DUPLICATE_INVOICE_NO);
            }
            // INVOICE_NO to long
            if(Constants.MAX_INVOICE_NO_LENGTH < invoiceMaintenanceDomain.getInvoiceNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        ERROR_CD_SP_E7_0030, new String[]{
                            MessageUtil.getLabelHandledException(locale, LBL_INVOICE_NO), 
                            String.valueOf(Constants.MAX_INVOICE_NO_LENGTH) })));
            }
            // Cannot input comma in Invoice Number.
            if(invoiceMaintenanceDomain.getInvoiceNo().contains(Constants.SYMBOL_COMMA)) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0037, LBL_INPUT_COMMA_IN_INVOICE_NO);
            }
        }
        
        // INVOICE_DATE is null
        if (Strings.judgeBlank(invoiceMaintenanceDomain.getInvoiceDate())) {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0007, LBL_INVOICE_DATE);
        // INVOICE_DATE wrong format
        } else if(!invoiceMaintenanceDomain.getInvoiceDate().matches(Constants.REGX_DATEFORMAT)) {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_INVOICE_DATE);
        }
        
        // VAT_RATE is null
        boolean isVatTypeVat = STR_ONE.equals(invoiceMaintenanceDomain.getVatType());
        if(isVatTypeVat && Strings.judgeBlank(invoiceMaintenanceDomain.getVatRate())) {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0007, LBL_VAT_RATE);
        }
        
        // SUPPLIER_BASE_AMOUNT is null
        if(Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierBaseAmount())
            || STR_ZERO.equals(invoiceMaintenanceDomain.getSupplierBaseAmount()))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0007, LBL_SUPPLIER_BASE_AMOUNT);
        // SUPPLIER_BASE_AMOUNT wrong format
        } else if(!invoiceMaintenanceDomain.getSupplierBaseAmount().matches(
            REGX_DECIMAL_NUMBER_FORMAT))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_INVOICE_BASE_AMOUNT);
        } else {
            supplierBase = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getSupplierBaseAmount());
        }
        
        // Start : [FIX] None VAT will not input any VAT amount
        //if(Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierVatAmount())
        //    || STR_ZERO.equals(invoiceMaintenanceDomain.getSupplierVatAmount()))
        //{
        //    this.addErrorMessageSingleLabel(errorMessageList, locale,
        //        ERROR_CD_SP_E7_0007, LBL_SUPPLIER_VAT_AMOUNT);
        // SUPPLIER_VAT_AMOUNT wrong format
        //} else if(!Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierVatAmount()) 
        //    && !invoiceMaintenanceDomain.getSupplierVatAmount().matches(
        //        REGX_DECIMAL_NUMBER_FORMAT))
        //{
        //    this.addErrorMessageSingleLabel(errorMessageList, locale,
        //        ERROR_CD_SP_E7_0023, LBL_INVOICE_VAT_GST_AMOUNT);
        //} else {
        //    supplierVat = NumberUtil.toBigDecimalDefaultZero(
        //        invoiceMaintenanceDomain.getSupplierVatAmount());
        //}
        if (isVatTypeVat) {
            // SUPPLIER_VAT_AMOUNT is null
            if(Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierVatAmount())) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0007, LBL_SUPPLIER_VAT_AMOUNT);
            // SUPPLIER_VAT_AMOUNT wrong format
            } else if(!Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierVatAmount()) 
                && !invoiceMaintenanceDomain.getSupplierVatAmount().matches(
                    REGX_DECIMAL_NUMBER_FORMAT))
            {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0023, LBL_INVOICE_VAT_GST_AMOUNT);
            } else {
                supplierVat = NumberUtil.toBigDecimalDefaultZero(
                    invoiceMaintenanceDomain.getSupplierVatAmount());
            }
        } else {
            supplierVat = BigDecimal.ZERO;
        }
        // End : [FIX] None VAT will not input any VAT amount
        
        // SUPPLIER_TOTAL_AMOUNT is null
        if(Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierTotalAmount())
            || STR_ZERO.equals(invoiceMaintenanceDomain.getSupplierTotalAmount()))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0007, LBL_SUPPLIER_TOTAL_AMOUNT);
        // SUPPLIER_TOTAL_AMOUNT wrong format
        } else if(!Strings.judgeBlank(invoiceMaintenanceDomain.getSupplierTotalAmount())
            && !invoiceMaintenanceDomain.getSupplierTotalAmount().matches(
                REGX_DECIMAL_NUMBER_FORMAT))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_INVOICE_TOTAL_AMOUNT);
        } else {
            supplierTotal = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getSupplierTotalAmount());
        }

        // CN_DATE wrong format
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnDate())
            && !invoiceMaintenanceDomain.getCnDate().matches(Constants.REGX_DATEFORMAT))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_CN_DATE);
        } else if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnDate())
            && !StringUtil.checkNullOrEmpty(invoiceMaintenanceDomain.getInvoiceDate()))
        {
            // CN_DATE less than INVOICE_DATE
            if(DateUtil.compareDate(invoiceMaintenanceDomain.getCnDate(),
                    invoiceMaintenanceDomain.getInvoiceDate()) < ZERO)
            {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0037, LBL_INPUT_CNDATE_LESS_THAN_INVDATE);
            }
        }

        // CN_NO too long
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnNo())) {
            if (Constants.MAX_CN_NO_LENGTH < invoiceMaintenanceDomain.getCnNo().length()) {
                errorMessageList.add(new ApplicationMessageDomain(MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        ERROR_CD_SP_E7_0030, new String[]{
                            MessageUtil.getLabelHandledException(locale, LBL_CN_NO), 
                            String.valueOf(Constants.MAX_CN_NO_LENGTH) })));
            }
            
            // Cannot input comma in Invoice Number.
            if(invoiceMaintenanceDomain.getCnNo().contains(Constants.SYMBOL_COMMA)) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0037, LBL_INPUT_COMMA_IN_CN_NO);
            }
        }

        // VAT_RATE wrong format
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getVatRate())
            && !invoiceMaintenanceDomain.getVatRate().matches(Constants.REGX_NUMBER_FORMAT))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_VAT_GST_RAET);
        }
        
        // CN_BASE_AMOUNT wrong format
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnBaseAmount())
            && !invoiceMaintenanceDomain.getCnBaseAmount().matches(
                REGX_DECIMAL_NUMBER_FORMAT_NEG))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_CREDIT_NOTE_BASE_AMOUNT);
        } else if (!Strings.judgeBlank(invoiceMaintenanceDomain.getCnBaseAmount())) {
            cnBase = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getCnBaseAmount());
        }
        
        // Start : [FIX] None VAT will not input any VAT amount
        // CN_VAT_AMOUNT wrong format
        //if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnVatAmount())
        //    && !invoiceMaintenanceDomain.getCnVatAmount().matches(
        //        REGX_DECIMAL_NUMBER_FORMAT_NEG))
        //{
        //    this.addErrorMessageSingleLabel(errorMessageList, locale,
        //        ERROR_CD_SP_E7_0023, LBL_CREDIT_NOTE_VAT_GST_AMOUNT);
        //} else if (!Strings.judgeBlank(invoiceMaintenanceDomain.getCnVatAmount())) {
        //    cnVat = NumberUtil.toBigDecimalDefaultZero(
        //        invoiceMaintenanceDomain.getCnVatAmount());
        //}
        if (isVatTypeVat) {
            // CN_VAT_AMOUNT wrong format
            if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnVatAmount())
                && !invoiceMaintenanceDomain.getCnVatAmount().matches(
                    REGX_DECIMAL_NUMBER_FORMAT_NEG))
            {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0023, LBL_CREDIT_NOTE_VAT_GST_AMOUNT);
            } else if (!Strings.judgeBlank(invoiceMaintenanceDomain.getCnVatAmount())) {
                cnVat = NumberUtil.toBigDecimalDefaultZero(
                    invoiceMaintenanceDomain.getCnVatAmount());
            }
        } else {
            cnVat = BigDecimal.ZERO;
        }
        // End : [FIX] None VAT will not input any VAT amount
            
        // CN_TOTAL_AMOUNT wrong format
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getCnTotalAmount())
            && !invoiceMaintenanceDomain.getCnTotalAmount().matches(
                REGX_DECIMAL_NUMBER_FORMAT_NEG))
        {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0023, LBL_CREDIT_NOTE_TOTAL_AMOUNT);
        } else if (!Strings.judgeBlank(invoiceMaintenanceDomain.getCnTotalAmount())) {
            cnTotal = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getCnTotalAmount());
        }
        
        /* TOTAL_DIFF_BASE_AMOUNT is not zero, check CN_NO, CN_DATE, CN_BASE_AMOUNT, CN_VAT_AMOUNT,
         * CN_TOTAL_AMOUNT
         * */
        if(ZERO != Constants.BIG_DECIMAL_ZERO.compareTo(NumberUtil.toBigDecimalDefaultZero(
            invoiceMaintenanceDomain.getTotalDiffBaseAmount())))
        {
            // CN_NO is null
            if (Strings.judgeBlank(invoiceMaintenanceDomain.getCnNo())) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0007, LBL_CREDIT_NOTE_NO);
            } else {
                PriceDifferenceInformationDomain criteria 
                    = new PriceDifferenceInformationDomain();
                criteria.setVendorCd(invoiceMaintenanceDomain.getVendorCd());
                criteria.setCnNo(invoiceMaintenanceDomain.getCnNo());
                List<PriceDifferenceInformationDomain> result = this.cnService.searchCn(criteria);
                Iterator<PriceDifferenceInformationDomain> iterator = result.iterator();
                while (iterator.hasNext()) {
                    PriceDifferenceInformationDomain item = iterator.next();
                    if (Constants.CCL_STATUS.equals(item.getSpsTCnDomain().getCnStatus())) {
                        iterator.remove();
                    }
                }
                // CN_NO duplicate
                if (ZERO != result.size()) {
                    this.addErrorMessageSingleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0037, LBL_CREATE_INVOICE_DUPLICATE_CN_NO);
                }
            }
            // CN_DATE is null
            if (Strings.judgeBlank(invoiceMaintenanceDomain.getCnDate())) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0007, LBL_CREDIT_NOTE_DATE);
            }
            // CN_BASE_AMOUNT is null
            if(Strings.judgeBlank(invoiceMaintenanceDomain.getCnBaseAmount()) 
                || (null != cnBase && ZERO == Constants.BIG_DECIMAL_ZERO.compareTo(cnBase)))
            {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0007, LBL_CREDIT_NOTE_BASE_AMOUNT);
            }
            // CN_VAT_AMOUNT is null
            if (null == cnVat) {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0007, LBL_CREDIT_NOTE_VAT_AMOUNT);
            }
            // CN_TOTAL_AMOUNT is null
            if(Strings.judgeBlank(invoiceMaintenanceDomain.getCnTotalAmount()) 
                || (null != cnTotal && ZERO == Constants.BIG_DECIMAL_ZERO.compareTo(cnTotal)))
            {
                this.addErrorMessageSingleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0007, LBL_CREDIT_NOTE_TOTAL_AMOUNT);
            }
        }
        
        /* Calculate DIFF_BASE_AMOUNT, DIFF_VAT_AMOUNT, DIFF_TOTAL_AMOUNT, TOTAL_BASE_AMOUNT,
         * TOTAL_VAT_AMOUNT and GRAND_TOTAL_AMOUNT.
         * */
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalDiffBaseAmount())) {
            diffBase = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getTotalDiffBaseAmount()).abs();
        }
        
        // Start : [FIX] None VAT will not input any VAT amount
        //if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalDiffVatAmount())) {
        //    diffVat = NumberUtil.toBigDecimalDefaultZero(
        //        invoiceMaintenanceDomain.getTotalDiffVatAmount()).abs();
        //}
        if (isVatTypeVat) {
            if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalDiffVatAmount())) {
                diffVat = NumberUtil.toBigDecimalDefaultZero(
                    invoiceMaintenanceDomain.getTotalDiffVatAmount()).abs();
            }
        } else {
            diffVat = BigDecimal.ZERO;
        }
        // End : [FIX] None VAT will not input any VAT amount
        
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalDiffAmount())) {
            diffTotal = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getTotalDiffAmount()).abs();
        }
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalBaseAmount())
            && invoiceMaintenanceDomain.getTotalBaseAmount().matches(
                REGX_DECIMAL_NUMBER_FORMAT))
        {
            totalBase = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getTotalBaseAmount());
        }
        // Start : [FIX] None VAT will not input any VAT amount
        //if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalVatAmount())
        //    && invoiceMaintenanceDomain.getTotalVatAmount().matches(
        //        REGX_DECIMAL_NUMBER_FORMAT))
        //{
        //    totalVat = NumberUtil.toBigDecimalDefaultZero(
        //        invoiceMaintenanceDomain.getTotalVatAmount());
        //}
        if (isVatTypeVat) {
            if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalVatAmount())
                && invoiceMaintenanceDomain.getTotalVatAmount().matches(
                    REGX_DECIMAL_NUMBER_FORMAT))
            {
                totalVat = NumberUtil.toBigDecimalDefaultZero(
                    invoiceMaintenanceDomain.getTotalVatAmount());
            }
        } else {
            totalVat = BigDecimal.ZERO;
        }
        // End : [FIX] None VAT will not input any VAT amount
        
        if(!Strings.judgeBlank(invoiceMaintenanceDomain.getTotalAmount())
            && invoiceMaintenanceDomain.getTotalAmount().matches(
                REGX_DECIMAL_NUMBER_FORMAT))
        {
            grandTotal = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getTotalAmount());
        }

        // Check SUPPLIER_TOTAL_AMOUNT = SUPPLIER_VAT_AMOUNT + SUPPLIER_BASE_AMOUNT
        if (null != supplierBase && null != supplierVat && null != supplierTotal) {
            BigDecimal calSupplierTotal = supplierBase.add(supplierVat);
            if (ZERO != supplierTotal.compareTo(calSupplierTotal)) {
                this.addErrorMessageDoubleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0009, LBL_INVOICE_TOTAL_AMOUNT,
                    LBL_INVOICE_BASE_AMOUNT_INVOICE_VAT_GST_AMOUNT);
            }
        }
        
        // TOTAL_DIFF_BASE_AMOUNT is zero check SUPPLIER_AMOUNT and TOTAL_AMOUNT
        if (null == diffBase || ZERO == Constants.BIG_DECIMAL_ZERO.compareTo(diffBase)) {
            if (null != supplierBase && null != totalBase) {
                // SUPPLIER_BASE_AMOUNT != TOTAL_BASE_AMOUNT
                if (ZERO != supplierBase.compareTo(totalBase)) {
                    this.addErrorMessageDoubleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0009, LBL_INVOICE_BASE_AMOUNT, LBL_TOTAL_BASE_AMOUNT);
                }
            }
            if(null != supplierVat && null != totalVat) {
                BigDecimal diff = supplierVat.subtract(totalVat).abs();
                /*  Akat K. : Change Invoice input allow input different from calculate
                 * from 0.01 to 0.0 (Not Allow).
                 * */
                // SUPPLIER_VAT_AMOUNT != TOTAL_VAT_AMOUNT
                if (ONE == diff.compareTo(Constants.INVOICE_ALLOW_DIFFERENT)) {
                    this.addErrorMessageDoubleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0009, LBL_INVOICE_VAT_GST_AMOUNT, LBL_TOTAL_VAT_GST_AMOUNT);
                }
            }
            if(null != supplierTotal && null != grandTotal) {
                BigDecimal diff = supplierTotal.subtract(grandTotal).abs();
                /*  Akat K. : Change Invoice input allow input different from calculate
                 * from 0.01 to 0.0 (Not Allow).
                 * */
                // SUPPLIER_TOTAL_AMOUNT != TOTAL_AMOUNT
                if (ONE == diff.compareTo(Constants.INVOICE_ALLOW_DIFFERENT)) {
                    this.addErrorMessageDoubleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0009, LBL_INVOICE_TOTAL_AMOUNT, LBL_TOTAL_AMOUNT);
                }
            }
        // TOTAL_DIFF_BASE_AMOUNT is not zero, check SUPPLIER_AMOUNT = TOTAL_AMOUNT + DIFF_AMOUNT
        } else {
            if(null != supplierBase && null != totalBase && null != diffBase) {
                BigDecimal invoiceBase = totalBase.add(diffBase);
                // SUPPLIER_BASE_AMOUNT != TOTAL_BASE_AMOUNT + DIFF_BASE_AMOUNT
                if (ZERO != supplierBase.compareTo(invoiceBase)) {
                    this.addErrorMessageDoubleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0009, LBL_INVOICE_BASE_AMOUNT,
                        LBL_TOTAL_BASE_AMOUNT_TOTAL_DIFFERENCE_BASE_AMOUNT);
                }
            }
            if(null != supplierVat && null != totalVat && null != diffVat) {
                BigDecimal invoiceVat = totalVat.add(diffVat);
                BigDecimal diff = supplierVat.subtract(invoiceVat).abs();
                /*  Akat K. : Change Invoice input allow input different from calculate
                 * from 0.01 to 0.0 (Not Allow).
                 * */
                // SUPPLIER_VAT_AMOUNT != TOTAL_VAT_AMOUNT + DIFF_VAT_AMOUNT
                if (ONE == diff.compareTo(Constants.INVOICE_ALLOW_DIFFERENT)) {
                    this.addErrorMessageDoubleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0009, LBL_INVOICE_VAT_GST_AMOUNT,
                        LBL_TOTAL_VAT_GST_AMOUNT_TOTAL_DIFFERENCE_VAT_GST_AMOUNT);
                }
            }
            if(null != supplierTotal && null != grandTotal && null != diffTotal) {
                BigDecimal invoiceTotal = grandTotal.add(diffTotal);
                BigDecimal diff = supplierTotal.subtract(invoiceTotal).abs();
                /*  Akat K. : Change Invoice input allow input different from calculate
                 * from 0.01 to 0.0 (Not Allow).
                 * */
                // SUPPLIER_TOTAL_AMOUNT != TOTAL_AMOUNT + DIFF_TOTAL_AMOUNT
                if (ONE == diff.compareTo(Constants.INVOICE_ALLOW_DIFFERENT)) {
                    this.addErrorMessageDoubleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0009, LBL_INVOICE_TOTAL_AMOUNT,
                        LBL_TOTAL_AMOUNT_TOTAL_DIFFERENCE_AMOUNT);
                }
            }
        }

        // CN_BASE_AMOUNT must less than 0
        boolean errorCnMoreThanZero = false;
        if (null != cnBase) {
            if (BIG_DECIMAL_ZERO.compareTo(cnBase) <= ZERO) {
                errorCnMoreThanZero = true;
            }
        }
        // CN_VAT_AMOUNT must less than 0
        if (null != cnVat) {
            // [FIX] CN VAT/GST amount can equal to zero
            //if (BIG_DECIMAL_ZERO.compareTo(cnVat) <= ZERO) {
            if (BIG_DECIMAL_ZERO.compareTo(cnVat) < ZERO) {
                errorCnMoreThanZero = true;
            }
        }
        // CN_TOTAL_AMOUNT must less than 0
        if (null != cnTotal) {
            if (BIG_DECIMAL_ZERO.compareTo(cnTotal) <= ZERO) {
                errorCnMoreThanZero = true;
            }
        }
        if (errorCnMoreThanZero) {
            this.addErrorMessageSingleLabel(errorMessageList, locale,
                ERROR_CD_SP_E7_0007, LBL_CN_AMOUNT_NEGATIVE_VALUE);
        }
        
        // Check between CN_AMOUNT and DIFF_AMOUNT
        if (null != cnBase && null != diffBase) {
            BigDecimal negativeDiffBase = NumberUtil.toBigDecimalDefaultZero(
                invoiceMaintenanceDomain.getTotalDiffBaseAmount());
            // CN_BASE_AMOUNT != DIFF_BASE_AMOUNT
            if (ZERO != cnBase.compareTo(negativeDiffBase)) {
                this.addErrorMessageDoubleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0009, LBL_CREDIT_NOTE_BASE_AMOUNT,
                    LBL_TOTAL_DIFFERENCE_BASE_AMOUNT);
            }
        }
        if (null != cnVat && null != diffVat) {
            BigDecimal diff = cnVat.add(diffVat).abs();
            /*  Akat K. : Change Invoice input allow input different from calculate
             * from 0.01 to 0.0 (Not Allow).
             * */
            // CN_VAT_AMOUNT != DIFF_VAT_AMOUNT
            if (ONE == diff.compareTo(Constants.INVOICE_ALLOW_DIFFERENT)) {
                this.addErrorMessageDoubleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0009, LBL_CREDIT_NOTE_VAT_GST_AMOUNT,
                    LBL_TOTAL_DIFFERENCE_VAT_GST_AMOUNT);
            }
        }
        if (null != cnTotal && null != diffTotal) {
            BigDecimal diff = cnTotal.add(diffTotal).abs();
            /*  Akat K. : Change Invoice input allow input different from calculate
             * from 0.01 to 0.0 (Not Allow).
             * */
            // CN_TOTAL_AMOUNT != DIFF_TOTAL_AMOUNT
            if (ONE == diff.compareTo(Constants.INVOICE_ALLOW_DIFFERENT)) {
                this.addErrorMessageDoubleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0009, LBL_CREDIT_NOTE_TOTAL_AMOUNT, LBL_TOTAL_DIFFERENCE_AMOUNT);
            }
        }

        // Check CN_TOTAL_AMOUNT = CN_VAT_AMOUNT + CN_BASE_AMOUNT
        if (null != cnBase && null != cnVat && null != cnTotal) {
            BigDecimal calCnTotal = cnBase.add(cnVat);
            if (ZERO != cnTotal.compareTo(calCnTotal)) {
                this.addErrorMessageDoubleLabel(errorMessageList, locale,
                    ERROR_CD_SP_E7_0009, LBL_CREDIT_NOTE_TOTAL_AMOUNT,
                    LBL_CREDIT_NOTE_BASE_AMOUNT_CREDIT_NOTE_VAT_GST_AMOUNT);
            }
        }
        
        // Check S.U/P and D.U/P must not different in same D.P/N
        Map<String, AsnDomain> partsMap = new HashMap<String, AsnDomain>();
        for(AsnDomain asnItem : invoiceMaintenanceDomain.getAsnDetailList()){
            AsnDomain partsInMap = partsMap.get(asnItem.getDPn());
            if (null == partsInMap) {
                partsMap.put(asnItem.getDPn(), asnItem);
            } else {
                
                // [IN035] remove comma before parse to BigDecimal
                //BigDecimal sUMap = new BigDecimal(partsInMap.getSupplierUnitPrice());
                //BigDecimal sUCurrent = new BigDecimal(asnItem.getSupplierUnitPrice());
                BigDecimal sUMap = NumberUtil.toBigDecimalDefaultZero(
                    StringUtil.removeComma(partsInMap.getSupplierUnitPrice()));
                BigDecimal sUCurrent = NumberUtil.toBigDecimalDefaultZero(
                    StringUtil.removeComma(asnItem.getSupplierUnitPrice()));
                
                if (ZERO != sUMap.compareTo(sUCurrent)) {
                    this.addErrorMessageSingleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0037, LBL_PARTS_IN_INVOICE_NOT_SAME_UNIT_PRICE);
                    break;
                }

                // [IN035] remove comma before parse to BigDecimal
                //BigDecimal dUMap = new BigDecimal(partsInMap.getDensoUnitPrice());
                //BigDecimal dUCurrent = new BigDecimal(asnItem.getDensoUnitPrice());
                BigDecimal dUMap = NumberUtil.toBigDecimalDefaultZero(
                    StringUtil.removeComma(partsInMap.getDensoUnitPrice()));
                BigDecimal dUCurrent = NumberUtil.toBigDecimalDefaultZero(
                    StringUtil.removeComma(asnItem.getDensoUnitPrice()));
                
                if (ZERO != dUMap.compareTo(dUCurrent)) {
                    this.addErrorMessageSingleLabel(errorMessageList, locale,
                        ERROR_CD_SP_E7_0037, LBL_PARTS_IN_INVOICE_NOT_SAME_UNIT_PRICE);
                    break;
                }
            }
        }
        
        return errorMessageList;
    }
    
    /**
     * <p>Create invoice cover page report.</p>
     * <ul>
     * <li>To create report in input stream pattern.</li>
     * </ul>
     * 
     * @param invoiceCoverPageReportList the list of the Invoice Cover Page Report Domain
     * @return the inputStream
     * @throws Exception 
     */
    private InputStream createInvoiceCoverPageReport(
        List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList)throws Exception{
        Map<String, Object> parameters = new HashMap<String, Object>();
        
        JasperPrint jpChangeMR = generateReport(getReportPath(), invoiceCoverPageReportList,
            parameters);
        return generate(jpChangeMR);
    }

    // [IN009] Rounding Mode use HALF_UP
    ///**
    // * Format decimal number to string. If decimal number is null then return empty string.
    // * @param decimalFormat formatter
    // * @param decimalNumber decimal number to format
    // * @return formatted string
    // * */
    //private String formatBigDecimalToString(DecimalFormat decimalFormat, BigDecimal decimalNumber) {
    //    String result = EMPTY_STRING;
    //    if (null ==  decimalNumber) {
    //        return null;
    //    }
    //    try {
    //        result = decimalFormat.format(decimalNumber);
    //    } catch (Exception e) {
    //        return EMPTY_STRING;
    //    }
    //    return result;
    //}
    /**
     * Format decimal number to string. If decimal number is null then return empty string.
     * @param decimalFormat formatter
     * @param decimalNumber decimal number to format
     * @param scale for decimal digit
     * @return formatted string
     * */
    private String formatBigDecimalToString(DecimalFormat decimalFormat, BigDecimal decimalNumber
        , int scale)
    {
        String result = EMPTY_STRING;
        if (null ==  decimalNumber) {
            return null;
        }
        try {
            result = decimalFormat.format(decimalNumber.setScale(scale, RoundingMode.HALF_UP));
        } catch (Exception e) {
            return EMPTY_STRING;
        }
        return result;
    }
    
    /**
     * Calculate number of gap to insert between Invoice and CN in Invoice Cover Page report.
     * @param invoiceCount - number of invoice
     * @param cnCount - number of CN
     * @return number of gap to insert
     * */
    private int calculateInsertGap(int invoiceCount, int cnCount) {
        // invoice content = invoice header + invoice content + invoice footer
        int invoiceContent = HEAD_HIGH + invoiceCount + FOOT_HIGH;
        
        // cn content = cn header + cn content + cn footer
        int cnContent = HEAD_HIGH + cnCount + FOOT_HIGH;
        
        // content length = invoice content + cn content + sign
        int contentLength = invoiceContent + cnContent + SIGN_HIGH;
        
        // Content fit in one page
        if (contentLength < FIRST_PAGE_SPACE) {
            return ZERO;
        }
        
        // Invoice fit in one page
        if (invoiceContent <= FIRST_PAGE_SPACE) {
            return FIRST_PAGE_SPACE - invoiceContent;
        }
        
        // To second page
        invoiceContent -= FIRST_PAGE_SPACE;
        
        // Only footer left
        if (ONE == invoiceContent || Constants.TWO == invoiceContent) {
            invoiceContent = Constants.THREE;
        }
        
        // content fit in second page
        contentLength = invoiceContent + cnContent + SIGN_HIGH;
        if (contentLength < OTHER_PAGE_SPACE) {
            return ZERO;
        }
        
        // invoice fit in second page
        if (invoiceContent <= OTHER_PAGE_SPACE) {
            return OTHER_PAGE_SPACE - invoiceContent;
        }
        
        // TO other page and so on
        while (OTHER_PAGE_SPACE < invoiceContent) {
            invoiceContent -= OTHER_PAGE_SPACE;
            
            // Only footer left
            if (ONE == invoiceContent || Constants.TWO == invoiceContent) {
                invoiceContent = Constants.THREE;
            }
            
            // content fit in this page
            contentLength = invoiceContent + cnContent + SIGN_HIGH;
            if (contentLength < OTHER_PAGE_SPACE) {
                return ZERO;
            }
            
            // invoice fit in this
            if (invoiceContent <= OTHER_PAGE_SPACE) {
                return OTHER_PAGE_SPACE - invoiceContent;
            }
        }
        
        return ZERO;
    }
    
    /**
     * Add ErrorMessage to error list from property with only one label.
     * @param errorMessageList - list of error message to add
     * @param errorCode - Error Code
     * @param labelCode - Label Code
     * @param locale - for get message and label
     * */
    private void addErrorMessageSingleLabel(List<ApplicationMessageDomain> errorMessageList
        , Locale locale, String errorCode, String labelCode)
    {
        errorMessageList.add(new ApplicationMessageDomain(MESSAGE_FAILURE,
            MessageUtil.getApplicationMessageWithLabel(locale, errorCode, labelCode)));
    }
    
    /**
     * Add ErrorMessage to error list from property with only one label.
     * @param errorMessageList - list of error message to add
     * @param errorCode - Error Code
     * @param locale - for get message and label
     * @param firstLabel - Label Code for first one
     * @param secondLabel - Label Code for the second one
     * */
    private void addErrorMessageDoubleLabel(List<ApplicationMessageDomain> errorMessageList
        , Locale locale, String errorCode, String firstLabel, String secondLabel)
    {
        errorMessageList.add(new ApplicationMessageDomain(MESSAGE_FAILURE,
            MessageUtil.getApplicationMessageHandledException(locale, 
                errorCode, new String[] {
                    MessageUtil.getLabelHandledException(locale, firstLabel),
                    MessageUtil.getLabelHandledException(locale, secondLabel)})));
    }

    // [IN009] Rounding Mode use HALF_UP
    ///**
    // * Convert Amount data to Decimal Format
    // * @param decimalFormat decimal format by currency
    // * @param target to convert decimal format
    // * @return string decimal format ("1000") return ("1,000.00")
    // * */
    //private String convertAmountToDecimalFormat(DecimalFormat decimalFormat, String target){
    //    String result = Constants.EMPTY_STRING;
    //    if(!StringUtil.checkNullOrEmpty(target)){
    //        BigDecimal amount = new BigDecimal(target);
    //        result = this.addDoubleQuote(decimalFormat.format(amount));
    //    }
    //    return result;
    //}
    /**
     * Convert Amount data to Decimal Format
     * @param decimalFormat decimal format by currency
     * @param target to convert decimal format
     * @param scale for decimal point
     * @return string decimal format ("1000") return ("1,000.00")
     * */
    private String convertAmountToCsvDecimalFormat(DecimalFormat decimalFormat, String target
        , int scale)
    {
        String result = Constants.EMPTY_STRING;
        if(!StringUtil.checkNullOrEmpty(target)){
            BigDecimal amount = new BigDecimal(target);
            amount = amount.setScale(scale, RoundingMode.HALF_UP);
            result = this.addDoubleQuote(decimalFormat.format(amount));
        }
        return result;
    }
    
    /**
     * Add double quote at front and back of target string
     * @param target to add double quote
     * @return string that already add double quote
     * */
    private String addDoubleQuote(String target) {
        StringBuffer result = new StringBuffer();
        
        result.append(Constants.SYMBOL_DOUBLE_QUOTE).append(target)
            .append(Constants.SYMBOL_DOUBLE_QUOTE);
        
        return result.toString();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.searchGenerateInvoice#searchGenerateDo(com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain)
     */
    public FileManagementDomain searchGenerateInvoice(
        InvoiceMaintenanceDomain invoiceMaintenanceDomain) throws ApplicationException
    {
        SpsTInvoiceCriteriaDomain criteria = new SpsTInvoiceCriteriaDomain();
        criteria.setInvoiceId(new BigDecimal(invoiceMaintenanceDomain.getInvoiceId()));
        criteria.setInvoiceNo(invoiceMaintenanceDomain.getInvoiceNo());
        criteria.setDCd(invoiceMaintenanceDomain.getDCd());
        criteria.setDPcd(invoiceMaintenanceDomain.getDPcd());
        criteria.setVendorCd(invoiceMaintenanceDomain.getVendorCd());
        
        List<SpsTInvoiceDomain> spsTInvoiceDomainList = spsTInvoiceService.searchByCondition(criteria);
        SpsTInvoiceDomain spsTInvoiceDomain = spsTInvoiceDomainList.get(Constants.ZERO);
        Integer invoiceId = new Integer(spsTInvoiceDomain.getInvoiceId().toString());
        StringBuffer invoiceCoverPageNo = new StringBuffer().append(spsTInvoiceDomain.getCoverPageNo());
        Locale locale = invoiceMaintenanceDomain.getLocale();
//      2018-04-18 Add generate PDF to BLOB
        List<InvoiceCoverPageReportDomain> invoiceCoverPageReportList = this.createInvoiceCoverPageData(
            invoiceId,
            invoiceCoverPageNo,
            invoiceMaintenanceDomain.getDecimalDisp(),
            locale);
        InputStream tsInvoiceCoverPageReport = null;

        try {
            tsInvoiceCoverPageReport = this.createInvoiceCoverPageReport(invoiceCoverPageReportList);
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0015);
        }
        String fileName = StringUtil.appendsString(
            INVOICE_COVER_PAGE, invoiceCoverPageNo.toString(),
            Constants.SYMBOL_DOT, Constants.REPORT_PDF_FORMAT);
        FileManagementDomain resultDomain = new FileManagementDomain();
        resultDomain = new FileManagementDomain();
        resultDomain.setFileData(tsInvoiceCoverPageReport);
        resultDomain.setFileName(fileName);
        return resultDomain;
    }
}
