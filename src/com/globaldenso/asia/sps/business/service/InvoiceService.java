/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceCoverPageDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;;

/**
 * <p>The Interface invoice service.</p>
 * <p>Service for invoice about search and update invoice status.</p>
 * <ul>
 * <li>Method search  : searchCountInvoiceInformation</li>
 * <li>Method search  : searchInvoiceInformation</li>
 * <li>Method create  : createInvoice</li>
 * <li>Method search  : searchInvoiceCoverPage</li>
 * <li>Method search  : searchInvoiceByAsn</li>
 * <li>Method search  : searchPurgingInvoice</li>
 * <li>Method search  : searchInvoiceForTransferToJde</li>
 * </ul>
 *
 * @author CSI
 */
public interface InvoiceService {

    /**
     * <p>Search count invoice information.</p>
     * <ul>
     * <li>Search count invoice information by criteria from search.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the invoice information domain
     * @return the integer
     */
    public Integer searchCountInvoiceInformation(InvoiceInformationDomain invoiceInformationDomain);
    
    /**
     * <p>Search invoice information.</p>
     * <ul>
     * <li>Search invoice information detail for display on Invoice Information Screen.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the Invoice Information Domain
     * @return the list of Invoice Information Domain.
     */
    public List<InvoiceInformationDomain> searchInvoiceInformation(
        InvoiceInformationDomain invoiceInformationDomain);
    
    /**
     * <p>Create invoice.</p>
     * <ul>
     * <li>Create invoice data.</li>
     * 
     * @param spsTInvoiceDomain the SPS Table Invoice Domain
     * @return the integer
     */
    public Integer createInvoice(SpsTInvoiceDomain spsTInvoiceDomain);
    
    /**
     * <p>Search invoice cover page.</p>
     * <ul>
     * <li>Search invoice cover page data in order to create report.</li>
     * 
     * @param invoiceCoverPageDomain the Invoice Cover Page Domain
     * @return the list of Invoice Information Domain
     */
    public List<InvoiceCoverPageDomain> searchInvoiceCoverPage(
        InvoiceCoverPageDomain invoiceCoverPageDomain);

    // [IN012] ASN can create by many Invoice (in case cancel invoice)
    ///**
    // * <p>Search invoice by ASN.</p>
    // * <ul>
    // * <li>Search invoice data by ASN No.</li>
    // * 
    // * @param purgingAsnDomain the Purging ASN Domain
    // * @return the SPS Transaction Invoice Domain
    // */
    //public SpsTInvoiceDomain searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain);
    /**
     * <p>Search invoice by ASN.</p>
     * <ul>
     * <li>Search invoice data by ASN No.</li>
     * 
     * @param purgingAsnDomain the Purging ASN Domain
     * @return the SPS Transaction Invoice Domain
     */
    public List<SpsTInvoiceDomain> searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain);
    
    /**
     * <p>Search Purging Invoice.</p>
     * <ul>
     * <li>Search invoice data for purging.</li>
     * 
     * @param purgingPoDomain the Purging PO Domain
     * @return the list of SPS Transaction Invoice Domain
     */
    public List<SpsTInvoiceDomain> searchPurgingInvoice(PurgingPoDomain purgingPoDomain);

    /**
     * Search Invoice for Transfer to JDE.
     * @param spsTInvoiceCriteriaDomain search criteria
     * @return List of Invoice to check for transfer.
     * */
    public List<InvoiceInformationDomain> searchInvoiceForTransferToJde(
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain);
    
    /**
     * <p>Delete Purging Invoice.</p>
     * <ul>
     * <li>Delete invoice data for purging.</li>
     * 
     * @param invoiceId the invoiceId
     * @return the amount of SPS Transaction Invoice Domain
     */
    public int deletePurgingInvoice(String invoiceId);
    
    /**
     * 
     * <p>Purge data from SPS_T_INVOICE.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deleteInvoice(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException;
    
}