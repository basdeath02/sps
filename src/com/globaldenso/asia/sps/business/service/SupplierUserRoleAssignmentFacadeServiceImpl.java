/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 2016/01/15 CSI Akat                [IN053]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;


/**
 * <p>The Class SupplierUserRoleAssignmentFacadeServiceImpl.</p>
 * <p>Manage data of Supplier User Role Assignment.</p>
 * <ul>
 * <li>Method search  : initial</li>
 * <li>Method search  : initialPopup</li>
 * <li>Method insert  : createSupplierUserRole</li>
 * <li>Method update  : deleteSupplierUserRole</li>
 * <li>Method update  : updateSupplierUserRole</li>
 * </ul>
 *
 * @author CSI
 */
public class SupplierUserRoleAssignmentFacadeServiceImpl implements
    SupplierUserRoleAssignmentFacadeService {
    
    
    /** The user role service. */
    private UserRoleService userRoleService;
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The User Service. */
    private SpsMUserService spsMUserService;
    
    /** The User Supplier Service. */
    private SpsMUserSupplierService spsMUserSupplierService;
    
    /** The User Role Service. */
    private SpsMUserRoleService spsMUserRoleService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The Role Service. */
    private SpsMRoleService spsMRoleService;
    
    /** The Supplier Plant Service. */
    private PlantSupplierService plantSupplierService;
    
    /** The Supplier Plant Service. */
    private SpsMPlantSupplierService spsMPlantSupplierService;
    
    /** The Company Supplier Service. */
    private CompanySupplierService companySupplierService;
    
    /** The Company Supplier Service. */
    private SpsMCompanySupplierService spsMCompanySupplierService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /**
     * Instantiates a new Supplier User Role Assignment Facade service impl.
     */
    public SupplierUserRoleAssignmentFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Sets the User Role Service.
     * 
     * @param userRoleService the new User Role Service.
     */
    public void setUserRoleService(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    
    /**
     * Sets The SPS M User Service.
     * 
     * @param spsMUserService the new SPS M User Service.
     */
    public void setSpsMUserService(SpsMUserService spsMUserService) {
        this.spsMUserService = spsMUserService;
    }
    /**
     * Sets the SPS M User Supplier Service.
     * 
     * @param spsMUserSupplierService the new SPS M User Supplier Service.
     */
    public void setSpsMUserSupplierService(
        SpsMUserSupplierService spsMUserSupplierService) {
        this.spsMUserSupplierService = spsMUserSupplierService;
    }
    /**
     * Sets the SPS M User Role Service.
     * 
     * @param spsMUserRoleService the new SPS M User Role Service.
     */
    public void setSpsMUserRoleService(SpsMUserRoleService spsMUserRoleService) {
        this.spsMUserRoleService = spsMUserRoleService;
    }

    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    /**
     * Sets the role service.
     * 
     * @param spsMRoleService the new role service.
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /**
     * Sets the supplier plant service.
     * 
     * @param plantSupplierService the new supplier plant service.
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }
    /**
     * Sets the supplier plant service.
     * 
     * @param spsMPlantSupplierService the new supplier plant service.
     */
    public void setSpsMPlantSupplierService(
        SpsMPlantSupplierService spsMPlantSupplierService) {
        this.spsMPlantSupplierService = spsMPlantSupplierService;
    }
    /**
     * Sets the company supplier service.
     * 
     * @param companySupplierService the new company supplier service.
     */
    public void setCompanySupplierService(CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    /**
     * Sets the company supplier service.
     * 
     * @param spsMCompanySupplierService the new company supplier service.
     */
    public void setSpsMCompanySupplierService(
        SpsMCompanySupplierService spsMCompanySupplierService) {
        this.spsMCompanySupplierService = spsMCompanySupplierService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRoleAssignmentFacadeService#searchInitial
     * (com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain)
     */
    public SupplierUserRoleAssignmentDomain searchInitial(SupplierUserRoleAssignmentDomain
        supplierUserRoleAssignmentDomain) throws ApplicationException
    {
        Locale locale = supplierUserRoleAssignmentDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain = supplierUserRoleAssignmentDomain
            .getDataScopeControlDomain();
        SpsMUserDomain spsMUserDomain = new SpsMUserDomain();
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain = 
            new CompanySupplierWithScopeDomain();
        SpsMUserRoleCriteriaDomain spsMUserRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        UserRoleDetailDomain userRoleDetailDomain = supplierUserRoleAssignmentDomain
            .getUserSupplierDetailDomain().getUserRoleDetailDomain();
        List<UserRoleDetailDomain> userRoleResultlist = new ArrayList<UserRoleDetailDomain>();
        
        int patternDate = DateUtil.PATTERN_YYYYMMDD_SLASH;
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        if(Constants.ZERO < dataScopeControlDomain.getDensoSupplierRelationDomainList().size()){
            supplierUserRoleAssignmentDomain.setDataScopeControlDomain(dataScopeControlDomain);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /*Get Company Name*/
        DensoSupplierRelationDomain densoSupplierRelationDomain = new DensoSupplierRelationDomain();
        densoSupplierRelationDomain.setSCd(supplierUserRoleAssignmentDomain.getSCd());
        densoSupplierRelationDomainList.add(densoSupplierRelationDomain);
        companySupplierWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        companySupplierWithScopeDomain.getCompanySupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<CompanySupplierDomain> companySupplierList = companySupplierService
            .searchCompanySupplierNameByRelation(companySupplierWithScopeDomain);
        
        /*Get selected user information*/
        spsMUserDomain.setDscId(userRoleDetailDomain.getDscId());
        UserSupplierDetailDomain supplierDetailDomain = getSupplierUserInformation(spsMUserDomain);
        if(Constants.ZERO < supplierDetailDomain.getErrorMessageList().size()){
            supplierUserRoleAssignmentDomain.setErrorMessageList(supplierDetailDomain
                .getErrorMessageList());
            return supplierUserRoleAssignmentDomain;
        }
        
        //TODO error
        supplierUserRoleAssignmentDomain.getUserSupplierDetailDomain()
            .getSpsMCompanySupplierDomain().setCompanyName(
                companySupplierList.get(Constants.ZERO).getCompanyName());
        
        /*Check DENSO owner with the current user roles on current screen*/
        boolean isDCompany = false;
        for(UserRoleDomain userRoleDomain : dataScopeControlDomain.getUserRoleDomainList()){
            if(!StringUtil.checkNullOrEmpty(supplierDetailDomain.getSpsMUserSupplierDomain()
                .getDOwner())){
                if(supplierDetailDomain.getSpsMUserSupplierDomain().getDOwner().equals(
                    userRoleDomain.getDCd())){
                    isDCompany = true;
                    break;
                }
            }
        }
        supplierUserRoleAssignmentDomain.setIsDCompany(isDCompany);
        
        if(!Constants.MODE_REGISTER.equals(supplierUserRoleAssignmentDomain.getMode())){
            /*call private method for get relation between DENSO and supplier*/
            densoSupplierRelationDomainList = getDensoSupplierRelation(dataScopeControlDomain);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
            
            /*Delete existing old supplier user role*/
            SpsMUserRoleCriteriaDomain userRoleCriteria = new SpsMUserRoleCriteriaDomain();
            userRoleCriteria.setDscId(userRoleDetailDomain.getDscId());
            userRoleCriteria.setSCd(supplierUserRoleAssignmentDomain.getSCd());
            userRoleService.deleteUserRoleByDscId(userRoleCriteria);
            
            /*call private method for Get Supplier user information*/
            spsMUserDomain.setDscId(supplierUserRoleAssignmentDomain.getUserSupplierDetailDomain()
                .getUserRoleDetailDomain().getDscId());
            UserSupplierDetailDomain supplierUserDetailDomain = getSupplierUserInformation(
                spsMUserDomain);
            if(Constants.ZERO < supplierUserDetailDomain.getErrorMessageList().size()){
                supplierUserRoleAssignmentDomain.setErrorMessageList(supplierUserDetailDomain
                    .getErrorMessageList());
                return supplierUserRoleAssignmentDomain;
            }
            supplierUserRoleAssignmentDomain.setUserSupplierDetailDomain(supplierUserDetailDomain);
            
            /*Get a number of records by DSC ID.*/
            spsMUserRoleCriteriaDomain.setDscId(supplierUserRoleAssignmentDomain
                .getUserSupplierDetailDomain().getSpsMUserDomain().getDscId());
            spsMUserRoleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
            int maxRecord = spsMUserRoleService.searchCount(spsMUserRoleCriteriaDomain);
            
            /* Get Maximum record limit*/
            recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
            recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM004_RLM);
            recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
            if(null == recordsLimit){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Check max record*/
            if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{recordsLimit.getMiscValue()});
            }
            
            /* Get Maximum record per page limit*/
            recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
            recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WADM004_PLM);
            recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
            if(null == recordLimitPerPage){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Calculate Rownum for query*/
            userRoleDetailDomain.setPageNumber(supplierUserRoleAssignmentDomain.getPageNumber());
            userRoleDetailDomain.setMaxRowPerPage(Integer.valueOf(
                recordLimitPerPage.getMiscValue()));
            SpsPagingUtil.calcPaging(userRoleDetailDomain, maxRecord);
            
            /*Get target user roles information*/
            userRoleDetailDomain.setDscId(supplierUserRoleAssignmentDomain
                .getUserSupplierDetailDomain().getSpsMUserDomain().getDscId());
            userRoleDetailDomain.setIsActive(Constants.IS_ACTIVE);
            List<UserRoleDetailDomain> userRoleDetailList = getUserRole(userRoleDetailDomain);
            
            /*set date format for display on screen*/
            for(UserRoleDetailDomain userRoleDomain : userRoleDetailList){
                UserRoleDetailDomain userRoleResult = new UserRoleDetailDomain();
                Date effectEndDate = new Date(userRoleDomain.getEffectEnd().getTime());
                Date effectStartDate = new Date(userRoleDomain.getEffectStart().getTime());
                Date createDate = new Date(userRoleDomain.getCreateDatetime().getTime());
                Date lastUpdateDatetime = new Date(userRoleDomain.getLastUpdateDatetime().getTime());
                
                userRoleResult.setSeqNoScreen(String.valueOf(userRoleDomain.getSeqNo()));
                userRoleResult.setSeqNo(userRoleDomain.getSeqNo());
                userRoleResult.setEffectEndScreen(DateUtil.format(effectEndDate, patternDate));
                userRoleResult.setEffectStartScreen(DateUtil.format(effectStartDate, patternDate));
                userRoleResult.setCreateDateScreen(DateUtil.format(createDate, patternDate));
                userRoleResult.setSCd(userRoleDomain.getSCd());
                userRoleResult.setSPcd(userRoleDomain.getSPcd());
                userRoleResult.setFirstName(userRoleDomain.getFirstName());
                userRoleResult.setMiddleName(userRoleDomain.getMiddleName());
                userRoleResult.setLastName(userRoleDomain.getLastName());
                userRoleResult.setRoleCd(userRoleDomain.getRoleCd());
                userRoleResult.setRoleName(userRoleDomain.getRoleName());
                userRoleResult.setLastUpdateDatetimeScreen(DateUtil.format(lastUpdateDatetime, 
                    patternTimestamp));
                userRoleResultlist.add(userRoleResult);
            }
            supplierUserRoleAssignmentDomain.getUserSupplierDetailDomain()
                .setUserRoleDetailList(userRoleResultlist);
            supplierUserRoleAssignmentDomain.getUserSupplierDetailDomain()
                .setUserRoleDetailDomain(userRoleDetailDomain);
        }
        
        return supplierUserRoleAssignmentDomain;
        
    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRoleAssignmentFacadeService#initialPopup
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public SupplierUserRoleAssignmentDomain searchInitialPopup(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain) throws ApplicationException{
        DataScopeControlDomain dataScopeControlDomain = supplierUserRoleAssignmentDomain
            .getDataScopeControlDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        SupplierUserRoleAssignmentDomain result = new SupplierUserRoleAssignmentDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            new ArrayList<DensoSupplierRelationDomain>();

        /*Get relation between DENSO and Supplier*/
        densoSupplierRelationDomainList = getDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationDomainList);
        result.setDataScopeControlDomain(dataScopeControlDomain);
        supplierUserRoleAssignmentDomain.setDataScopeControlDomain(dataScopeControlDomain);

        /*Get all roles from SpsMRoleService*/
        SpsMRoleCriteriaDomain criteria = new SpsMRoleCriteriaDomain();
        criteria.setIsActive(Constants.IS_ACTIVE);
        List<SpsMRoleDomain> spsMRoleList = spsMRoleService.searchByCondition(criteria);
        if(Constants.ZERO < spsMRoleList.size()){
            result.getUserSupplierDetailDomain().setSpsMRoleList(spsMRoleList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_ROLE);
        }
        /*Get Supplier plant list */
        List<PlantSupplierDomain> plantSupplierList = getSupplierPlant(
            supplierUserRoleAssignmentDomain);
        result.setPlantSupplierList(plantSupplierList);

        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRoleAssignmentFacadeService#deleteSupplierUserRole
     * (com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain)
     */
    public  void deleteSupplierUserRole(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain)throws ApplicationException{
        List<UserRoleDetailDomain> userRoleDetailSelectedList = supplierUserRoleAssignmentDomain
            .getUserSupplierDetailDomain().getUserRoleDetailList();
        List<SpsMUserRoleCriteriaDomain> criteriaList = new ArrayList<SpsMUserRoleCriteriaDomain>();
        List<SpsMUserRoleDomain> domainList = new ArrayList<SpsMUserRoleDomain>();
        Locale locale = supplierUserRoleAssignmentDomain.getLocale();
        
        /*set effect end date to (CommonService.searchSysDate() - 1 day)*/
        Timestamp currentTimestamp = commonService.searchSysDate();
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Calendar prevDate = Calendar.getInstance();
        prevDate.setTime(currentTimestamp);
        prevDate.add(Calendar.DATE, Constants.MINUS_ONE);
        String previousDate = DateUtil.format(prevDate.getTime(), patternTimestamp);
        Timestamp effectEnd = DateUtil.parseToTimestamp(previousDate, patternTimestamp);

        /*Update user role active flag to inactive and effective end date to expire*/
        for(UserRoleDetailDomain userRoleSelected : userRoleDetailSelectedList){
            SpsMUserRoleDomain roleDomain = new SpsMUserRoleDomain();
            roleDomain.setRoleCd(userRoleSelected.getRoleCd());
            roleDomain.setSPcd(userRoleSelected.getSPcd());
            roleDomain.setIsActive(Constants.IS_NOT_ACTIVE);
            roleDomain.setEffectEnd(effectEnd);
            roleDomain.setLastUpdateDscId(supplierUserRoleAssignmentDomain.getUpdateUser());
            roleDomain.setLastUpdateDatetime(currentTimestamp);
            domainList.add(roleDomain);
            
            /*Check existing user role from spsMUserRoleService*/
            SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
            userRoleCriteriaDomain.setSeqNo(userRoleSelected.getSeqNo());
            int countUserRole = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
            if(countUserRole <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_USER_ROLE);
            }
            
            Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(userRoleSelected
                .getLastUpdateDatetimeScreen(), patternTimestamp);
            SpsMUserRoleCriteriaDomain roleCriteria = new SpsMUserRoleCriteriaDomain();
            roleCriteria.setSeqNo(userRoleSelected.getSeqNo());
            roleCriteria.setLastUpdateDatetime(lastUpdateDatetime);
            criteriaList.add(roleCriteria);
        }
        int updateRecord = spsMUserRoleService.updateByCondition(domainList, criteriaList);
        if(updateRecord != criteriaList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_DELETE_USER_ROLE);
        }

    } 
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRoleAssignmentFacadeService#createUserRole
     * (com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain)
     */
    public SupplierUserRoleAssignmentDomain createUserRole(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain)throws ApplicationException{
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
        DataScopeControlDomain dataScopeControlDomain = supplierUserRoleAssignmentDomain
            .getDataScopeControlDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        UserRoleDetailDomain userRoleDetailDomain = supplierUserRoleAssignmentDomain
            .getUserSupplierDetailDomain().getUserRoleDetailDomain();
        try{
            /*Validate all input items.*/
            errorMessageList = validateInputValue(supplierUserRoleAssignmentDomain);
            
            /*In case of found an error message return the message to ActionForm*/
            if(Constants.ZERO < errorMessageList.size()){
                supplierUserRoleAssignmentDomain.setErrorMessageList(errorMessageList);
                return supplierUserRoleAssignmentDomain;
            }
            
            Date effectStartUtil = DateUtil.parseToUtilDate(supplierUserRoleAssignmentDomain
                .getEffectStartScreen(), pattern);
            Date effectEndUtil = DateUtil.parseToUtilDate(supplierUserRoleAssignmentDomain
                .getEffectEndScreen(), pattern);
            Timestamp effectStart = new Timestamp(effectStartUtil.getTime());
            Timestamp effectEnd = new Timestamp(effectEndUtil.getTime());
            
            /*Check existing assigned user role from SPSMUserRoleService*/
            SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
            userRoleCriteriaDomain.setDscId(userRoleDetailDomain.getDscId());
            userRoleCriteriaDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
            userRoleCriteriaDomain.setSCd(userRoleDetailDomain.getSCd());
            userRoleCriteriaDomain.setSPcd(userRoleDetailDomain.getSPcd());
            userRoleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
            int recordCount = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
            if(Constants.ZERO < recordCount){
                MessageUtil.throwsApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0024,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Check existing company supplier code from SPSMCompanySupplierService*/
            SpsMCompanySupplierCriteriaDomain supplierCriteriaDomain = 
                new SpsMCompanySupplierCriteriaDomain();
            supplierCriteriaDomain.setSCd(userRoleDetailDomain.getSCd());
            supplierCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
            int countScd = spsMCompanySupplierService.searchCount(supplierCriteriaDomain);
            if(countScd <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
            }
            
            /*Create new user role.*/
            userRoleDomain.setDscId(userRoleDetailDomain.getDscId());
            userRoleDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
            userRoleDomain.setSCd(userRoleDetailDomain.getSCd().trim());
            userRoleDomain.setSPcd(userRoleDetailDomain.getSPcd());
            userRoleDomain.setIsActive(Constants.IS_ACTIVE);
            userRoleDomain.setEffectStart(effectStart);
            userRoleDomain.setEffectEnd(effectEnd);
            userRoleDomain.setCreateDscId(userRoleDetailDomain.getCreateDscId());
            userRoleDomain.setCreateDatetime(commonService.searchSysDate());
            userRoleDomain.setLastUpdateDscId(userRoleDetailDomain.getCreateDscId());
            userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            spsMUserRoleService.create(userRoleDomain);
            
            supplierUserRoleAssignmentDomain.setIsActive(Constants.IS_ACTIVE);
            
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return supplierUserRoleAssignmentDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierUserRoleAssignmentFacadeService#updateUserRole
     * (com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain)
     */
    public SupplierUserRoleAssignmentDomain updateSupplierUserRole(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain) throws ApplicationException{
        Locale locale = supplierUserRoleAssignmentDomain.getLocale();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        SpsMUserRoleDomain userRoleDomain = new SpsMUserRoleDomain();
        SpsMUserRoleCriteriaDomain userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
        UserRoleDetailDomain userRoleDetailDomain = supplierUserRoleAssignmentDomain
            .getUserSupplierDetailDomain().getUserRoleDetailDomain();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        int patternTimestamp = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        try{
            /*Validate all input items.*/
            errorMessageList = validateInputValue(supplierUserRoleAssignmentDomain);
            
            /*In case of found an error message return the message to ActionForm*/
            if(Constants.ZERO < errorMessageList.size()){
                supplierUserRoleAssignmentDomain.setErrorMessageList(errorMessageList);
                return supplierUserRoleAssignmentDomain;
            }
            
            Date effectStartUtil = DateUtil.parseToUtilDate(supplierUserRoleAssignmentDomain
                .getEffectStartScreen(), pattern);
            Date effectEndUtil = DateUtil.parseToUtilDate(supplierUserRoleAssignmentDomain
                .getEffectEndScreen(), pattern);
            Timestamp lastUpdateDatetime = DateUtil.parseToTimestamp(userRoleDetailDomain
                .getLastUpdateDatetimeScreen(), patternTimestamp);
            Timestamp effectStart = new Timestamp(effectStartUtil.getTime());
            Timestamp effectEnd = new Timestamp(effectEndUtil.getTime());
            
            if(!userRoleDetailDomain.getRoleCd().equals(supplierUserRoleAssignmentDomain
                .getRoleCode())){
                /*Check existing assigned user role from SPSMUserRoleService*/
                userRoleCriteriaDomain.setDscId(userRoleDetailDomain.getDscId());
                userRoleCriteriaDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
                userRoleCriteriaDomain.setSCd(userRoleDetailDomain.getSCd());
                userRoleCriteriaDomain.setSPcd(userRoleDetailDomain.getSPcd());
                userRoleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
                int recordCount = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
                if(Constants.ZERO < recordCount){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0024,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
            
            /*Check existing company supplier code from SPSMCompanySupplierService*/
            SpsMCompanySupplierCriteriaDomain supplierCriteriaDomain = 
                new SpsMCompanySupplierCriteriaDomain();
            supplierCriteriaDomain.setSCd(userRoleDetailDomain.getSCd());
            supplierCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
            int countScd = spsMCompanySupplierService.searchCount(supplierCriteriaDomain);
            if(countScd <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
            }

            /*Check existing user role from spsMUserRoleService*/
            userRoleCriteriaDomain = new SpsMUserRoleCriteriaDomain();
            userRoleCriteriaDomain.setSeqNo(userRoleDetailDomain.getSeqNo());
            int countUserRole = spsMUserRoleService.searchCount(userRoleCriteriaDomain);
            if(countUserRole <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_USER_ROLE);
            }

            userRoleDomain.setDscId(userRoleDetailDomain.getDscId());
            userRoleDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
            userRoleDomain.setSCd(userRoleDetailDomain.getSCd());
            userRoleDomain.setSPcd(userRoleDetailDomain.getSPcd());
            userRoleDomain.setIsActive(Constants.IS_ACTIVE);
            userRoleDomain.setEffectStart((Timestamp)effectStart);
            userRoleDomain.setEffectEnd((Timestamp)effectEnd);

            // [IN053] Assign Role show assign by used create user, change to update user
            //userRoleDomain.setLastUpdateDscId(userRoleDetailDomain.getCreateDscId());
            userRoleDomain.setLastUpdateDscId(userRoleDetailDomain.getLastUpdateDscId());
            
            userRoleDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            SpsMUserRoleCriteriaDomain userRoleCriteria = new SpsMUserRoleCriteriaDomain();
            userRoleCriteria.setSeqNo(userRoleDetailDomain.getSeqNo());
            userRoleCriteria.setLastUpdateDatetime(lastUpdateDatetime);
            
            int updateRecord = spsMUserRoleService.updateByCondition(userRoleDomain,
                userRoleCriteria);
            if(updateRecord <= Constants.ZERO){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                    SupplierPortalConstant.LBL_UPDATE_USER_ROLE);
            }
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        return supplierUserRoleAssignmentDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * 
     * <p>Get relation between DENSO and Supplier.</p>
     *
     * @param dataScopeControlDomain that keep data scope criteria.
     * @return list the keep list of relation between DENSO and Supplier.
     * @throws ApplicationException the ApplicationException from error message.
     */
    private List<DensoSupplierRelationDomain> getDensoSupplierRelation(DataScopeControlDomain
        dataScopeControlDomain)throws ApplicationException{
        Locale locale = dataScopeControlDomain.getLocale();
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = new 
            ArrayList<DensoSupplierRelationDomain>();

        /*Get relation between DENSO and Supplier*/
        densoSupplierRelationDomainList = densoSupplierRelationService
            .searchDensoSupplierRelation(dataScopeControlDomain);

        if(densoSupplierRelationDomainList.size() < Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, 
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }

        return densoSupplierRelationDomainList;
        
    }
    /**
     * 
     * <p>Get supplier User Information.</p>
     *
     * @param spsMUserDomain that keep criteria for search.
     * @return SupplierUserDetailDomain that keep supplier user detail.
     * @throws ApplicationException the ApplicationException from error message.
     */
   
    private UserSupplierDetailDomain getSupplierUserInformation(SpsMUserDomain spsMUserDomain)
        throws ApplicationException{
        
        Locale locale = spsMUserDomain.getLocale();
        SpsMUserCriteriaDomain spsMUserCriteriaDomain = new SpsMUserCriteriaDomain();
        SpsMUserSupplierCriteriaDomain spsMUserSupplierCriteriaDomain = 
            new SpsMUserSupplierCriteriaDomain();
        List<SpsMUserSupplierDomain> spsMUserSupplierDomainList = 
            new ArrayList<SpsMUserSupplierDomain>();
        UserSupplierDetailDomain supplierUserDetailDomain = new UserSupplierDetailDomain();
        List<SpsMUserDomain> spsMUserDomainList = new ArrayList<SpsMUserDomain>();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        String message = new String();
        spsMUserCriteriaDomain.setDscId(spsMUserDomain.getDscId());
        spsMUserCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        spsMUserSupplierCriteriaDomain.setDscId(spsMUserDomain.getDscId());
        
        try{
            /*Get user information*/
            spsMUserDomainList = spsMUserService.searchByCondition(spsMUserCriteriaDomain);
            if(Constants.ZERO < spsMUserDomainList.size()){
                supplierUserDetailDomain.setSpsMUserDomain(spsMUserDomainList.get(Constants.ZERO));
            }else{
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_USER)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
            /*Get Supplier user information*/
            spsMUserSupplierDomainList = spsMUserSupplierService.searchByCondition(
                spsMUserSupplierCriteriaDomain);
            if(Constants.ZERO < spsMUserSupplierDomainList.size()){
                supplierUserDetailDomain.setSpsMUserSupplierDomain(spsMUserSupplierDomainList
                    .get(Constants.ZERO));
            }else{
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{MessageUtil.getLabel(
                        locale, SupplierPortalConstant.LBL_SUPPLIER_USER)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        supplierUserDetailDomain.setErrorMessageList(errorMessageList);
        return supplierUserDetailDomain;
    }
    
    /**
     * 
     * <p>Get User Role Information.</p>
     *
     * @param userRoleDetailDomain that keep criteria for search.
     * @return list of UserRoleDetailDomain that keep user role detail.
     * @throws ApplicationException the ApplicationException from error message.
     */
    private List<UserRoleDetailDomain> getUserRole(UserRoleDetailDomain userRoleDetailDomain)
        throws ApplicationException{
        List<UserRoleDetailDomain> resultList = new ArrayList<UserRoleDetailDomain>();

        resultList = userRoleService.searchUserRoleByDscId(userRoleDetailDomain);

        return resultList;
    }
    
    /**
     * 
     * <p>Get User Role Information.</p>
     *
     * @param supplierUserRoleAssignmentDomain that keep criteria for search.
     * @return list of CompanySupplierDomain that keep company supplier.
     * @throws ApplicationException the ApplicationException from error message.
     */
    private List<PlantSupplierDomain> getSupplierPlant(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain)
        throws ApplicationException{
        List<PlantSupplierDomain> plantSupplierList = new ArrayList<PlantSupplierDomain>();
        DataScopeControlDomain dataScopeControlDomain = supplierUserRoleAssignmentDomain
            .getDataScopeControlDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = supplierUserRoleAssignmentDomain
            .getPlantSupplierWithScopeDomain();
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);

        plantSupplierList = plantSupplierService.searchPlantSupplier(
            plantSupplierWithScopeDomain);
        if(plantSupplierList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT);
        }
        
        return plantSupplierList;
    }

    /**
     * 
     * <p>Validate input value.</p>
     *
     * @param supplierUserRoleAssignmentDomain that keep criteria for search.
     * @return list of ApplicationMessageDomain that keep error message.
     * @throws Exception the Exception.
     */
    private List<ApplicationMessageDomain> validateInputValue(SupplierUserRoleAssignmentDomain 
        supplierUserRoleAssignmentDomain) throws Exception{
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = supplierUserRoleAssignmentDomain.getLocale();
        String message = new String();
        boolean isValidDateFrom = false;
        boolean isValidDateTo = false;
        Timestamp currentDatetime = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Date currentDate = new Date(currentDatetime.getTime());
        
        /*Validate input*/
        if(StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain
            .getUserSupplierDetailDomain().getUserRoleDetailDomain().getRoleCd())
            || StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain
                .getUserSupplierDetailDomain().getUserRoleDetailDomain().getSPcd())
            || StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectEndScreen())
            || StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectStartScreen()))
        {
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                SupplierPortalConstant.ERROR_CD_SP_W6_0002, new String[]{MessageUtil.getLabel(
                    locale, SupplierPortalConstant.LBL_ASSIGN_NEW_USER_ROLE)});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                message));
        }
        
        if(!StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectEndScreen())){
            if(!DateUtil.isValidDate(supplierUserRoleAssignmentDomain.getEffectEndScreen(), 
                pattern)){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_EFFECTIVE_END)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectStartScreen())){
            if(!DateUtil.isValidDate(supplierUserRoleAssignmentDomain.getEffectStartScreen(), 
                pattern)){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023, new String[]{
                        MessageUtil.getLabel(
                            locale, SupplierPortalConstant.LBL_EFFECTIVE_START)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectStartScreen())
            && !StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectEndScreen())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO < DateUtil.compareDate(supplierUserRoleAssignmentDomain
                .getEffectStartScreen(), supplierUserRoleAssignmentDomain.getEffectEndScreen())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_W6_0003);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(supplierUserRoleAssignmentDomain.getEffectStartScreen())){
            if(Constants.ZERO < DateUtil.compareDate(DateUtil.format(currentDate, pattern), 
                supplierUserRoleAssignmentDomain.getEffectEndScreen())){
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_W6_0004);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        /*Check values existing in master data*/
        List<ApplicationMessageDomain> masterErrorList = checkExistingMaster(
            supplierUserRoleAssignmentDomain.getUserSupplierDetailDomain()
                .getUserRoleDetailDomain());
        if(Constants.ZERO <= masterErrorList.size()){
            for(ApplicationMessageDomain errorMessage : masterErrorList){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    errorMessage.getMessage()));
            }
        }
        return errorMessageList;
    }
 
    /**
     * 
     * <p>Check Existing Master.</p>
     *
     * @param userRoleDetailDomain that keep a data that need to check existing in master data.
     * @return list of ApplicationMessageDomain that keep error message.
     * @throws Exception the Exception.
     */
    private List<ApplicationMessageDomain> checkExistingMaster(UserRoleDetailDomain 
        userRoleDetailDomain) throws Exception{
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = userRoleDetailDomain.getLocale();
        String message = new String();
        
        /*Check existing role code in master data*/
        SpsMRoleCriteriaDomain roleCriteriaDomain = new SpsMRoleCriteriaDomain();
        roleCriteriaDomain.setRoleCd(userRoleDetailDomain.getRoleCd());
        roleCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int recordCount = spsMRoleService.searchCount(roleCriteriaDomain);
        if(recordCount <= Constants.ZERO){
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
                    MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_ROLE)});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                message));
        }
        /*Check existing Supplier plant code in master data from SpsMPlantSupplierService*/
        SpsMPlantSupplierCriteriaDomain plantSupplierCriteriaDomain = 
            new SpsMPlantSupplierCriteriaDomain();
        plantSupplierCriteriaDomain.setSCd(userRoleDetailDomain.getSCd());
        plantSupplierCriteriaDomain.setSPcd(userRoleDetailDomain.getSPcd());
        plantSupplierCriteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int count = spsMPlantSupplierService.searchCount(plantSupplierCriteriaDomain);
        if(count <= Constants.ZERO){
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013, new String[]{
                    MessageUtil.getLabel(locale, SupplierPortalConstant.LBL_SUPPLIER_PLANT)});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                message));
        }
        return errorMessageList;
    }
}