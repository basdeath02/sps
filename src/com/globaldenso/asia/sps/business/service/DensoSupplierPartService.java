/*
 * ModifyDate Development company     Describe 
 * 2014/06/16 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain;

/**
 * <p>The Interface DensoSupplierPartService.</p>
 * <p>Service for DENSO Supplier Part about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search  : searchDensoSupplierPart</li>
 * <li>Method search  : searchAllDensoSupplierPlant</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoSupplierPartService {     

    /**
     * Search DENSO Supplier Part.
     * 
     * @param densoSupplierPartDomain the It keep a criteria to search DENSO supplier part.
     * @return the DensoSupplierPartDomain.
     */
    public DensoSupplierPartDomain searchDensoSupplierPart(DensoSupplierPartDomain 
        densoSupplierPartDomain);
    
    /**
     * Search All DENSO Supplier Part.
     * 
     * @param dataScopeControlDomain the It keep a criteria to search All DENSO Supplier Part.
     * @return the DensoSupplierPartDomain.
     */
    public List<DensoSupplierPartDomain> searchAllDensoSupplierPart(DataScopeControlDomain 
        dataScopeControlDomain);
    
}