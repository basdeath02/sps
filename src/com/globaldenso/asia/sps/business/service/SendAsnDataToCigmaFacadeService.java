/*
 * ModifyDate Developmentcompany Describe 
 * 2014/04/26 CSI Parichat        Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.SendAsnDataToCigmaDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;


/**
 * <p>The Interface MessageService.</p>
 * <p>Facade for Send ASN Data to Cigma.</p>
 * <li>Method search : searchSendingAsn</li>
 * <li>Method search : searchAs400ServerList</li>
 * <li>Method update : transactSendAsn</li>
 * @author CSI
 * @version 1.00
 */
public interface SendAsnDataToCigmaFacadeService {
    /**
     * Search Send ASN to Cigma.
     * <ul>
     * <li>Search ASN data and limit records for sending to CIGMA.</li>
     * </ul>
     * @param sendingAsnDomain the sending asn domain 
     * @return the sending asn domain 
     */
    public SendingAsnDomain searchSendingAsn(SendingAsnDomain sendingAsnDomain);
    
    /**
     * Search AS400 Server List.
     * <ul>
     * <li>Search AS400 server list from denso company code.</li>
     * </ul>
     * @param companyDensoCode the company denso code (separate by comma)
     * @return the list of SpsMAs400Schema domain
     */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
        String companyDensoCode);
    
    /**
     * Transact send ASN.
     * <ul>
     * <li>Transact for sending ASN data to CIGMA.</li>
     * </ul>
     * @param sendAsnDataToCigmaDomin the sendAsnDataToCigma domain
     * @throws ApplicationException ApplicationException
     * @throws WebServiceCallerRestException WebServiceCallerRestException
     */
    public void transactSendAsn(SendAsnDataToCigmaDomain sendAsnDataToCigmaDomin) 
        throws ApplicationException, WebServiceCallerRestException;
    
}