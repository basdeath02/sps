/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 2016/03/16 CSI Akat                [IN068]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Date;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaPoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;

/**
 * <p>The Class CigmaPoErrorServiceImpl.</p>
 * <p>For SPS_CIGMA_PO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferPoError</li>
 * <li>Method search  : searchMinIssueDate</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaPoErrorServiceImpl implements CigmaPoErrorService {

    /** The DAO for SPS_CIGMA_PO_ERROR. */
    private CigmaPoErrorDao cigmaPoErrorDao;
    
    /** The default constructor. */
    public CigmaPoErrorServiceImpl() {
        super();
    }

    /**
     * <p>Setter method for cigmaPoErrorDao.</p>
     *
     * @param cigmaPoErrorDao Set for cigmaPoErrorDao
     */
    public void setCigmaPoErrorDao(CigmaPoErrorDao cigmaPoErrorDao) {
        this.cigmaPoErrorDao = cigmaPoErrorDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.CigmaPoErrorService#searchTransferPoError(
     * com.globaldenso.asia.sps.auto.business.domain.CigmaPoErrorDomain)
     */
    public List<TransferPoErrorEmailDomain> searchTransferPoError(
        SpsCigmaPoErrorDomain spsCigmaPoErrorDomain)
    {
        List<TransferPoErrorEmailDomain> result = null;
        result = this.cigmaPoErrorDao.searchTransferPoError(spsCigmaPoErrorDomain);
        return result;
    }

    // [IN068] add method for search minimum issue date
    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaPoErrorService#searchMinIssueDate(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    public Date searchMinIssueDate(SpsCigmaPoErrorDomain spsCigmaPoErrorDomain) {
        Date result = this.cigmaPoErrorDao.searchMinIssueDate(spsCigmaPoErrorDomain);
        return result;
    }

}
