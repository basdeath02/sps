/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Parichat            Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 2015/12/09 CSI Akat                [IN042]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.FileManagementException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

/**
 * <p>The Class AcknowledgedDOInformationFacadeServiceImpl.</p>
 * <p>Manage data of acknowledged DO information.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchAcknowledgedDoInformation</li>
 * <li>Method search  : findFileName</li>
 * <li>Method search  : validateGroupAsn</li>
 * </ul>
 *
 * @author CSI
 */
public class AsnProgressInformationFacadeServiceImpl implements 
    AsnProgressInformationFacadeService {
    
    /** Header CSV */
    private static final String [] HEADER_CSV = new String[] {
        SupplierPortalConstant.LBL_S_CD,
        SupplierPortalConstant.LBL_S_PCD,
        SupplierPortalConstant.LBL_D_CD,
        SupplierPortalConstant.LBL_D_PCD,
        SupplierPortalConstant.LBL_ASN_STATUS,
        SupplierPortalConstant.LBL_ASN_DATE,
        SupplierPortalConstant.LBL_ASN_NO,
        SupplierPortalConstant.LBL_TRIP_NO,
        SupplierPortalConstant.LBL_NUMBER_OF_PALLET,
        SupplierPortalConstant.LBL_ACTUAL_ETD,
        SupplierPortalConstant.LBL_PLAN_ETA,
        SupplierPortalConstant.LBL_CURRENT_SPS_DO_NO,
        SupplierPortalConstant.LBL_REVISION,
        SupplierPortalConstant.LBL_CYCLE,
        SupplierPortalConstant.LBL_ROUTE,
        SupplierPortalConstant.LBL_DEL,
        SupplierPortalConstant.LBL_TM,
        SupplierPortalConstant.LBL_WH_PRIME_RECEIVING,
        SupplierPortalConstant.LBL_DOCKCODE,
        SupplierPortalConstant.LBL_CTRL_NO,
        SupplierPortalConstant.LBL_D_PN,
        SupplierPortalConstant.LBL_ITEM_DESC,
        SupplierPortalConstant.LBL_S_PN,
        SupplierPortalConstant.LBL_ORDER_METHOD,
        SupplierPortalConstant.LBL_SHIPPING_QTY,
        SupplierPortalConstant.LBL_NO_OF_BOX,
        SupplierPortalConstant.LBL_RSN,
        SupplierPortalConstant.LBL_REVISE_SHIPPING_QTY_FLAG,
        SupplierPortalConstant.LBL_CURRENT_CIGMA_DO_NO,
        SupplierPortalConstant.LBL_RECEIVED_QTY,
        SupplierPortalConstant.LBL_UNIT_OF_MEASURE,
        SupplierPortalConstant.LBL_RECEIVING_LANE,
        SupplierPortalConstant.LBL_RECEIVED_DATE,
        SupplierPortalConstant.LBL_PN_RCV_STATUS,
        SupplierPortalConstant.LBL_INVOICE_NO,
        SupplierPortalConstant.LBL_INVOICE_DATE,
        SupplierPortalConstant.LBL_INVOICE_STATUS};
    
    /** The misc service. */
    private MiscellaneousService miscService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The denso supplier relation service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The company supplier service. */
    private CompanySupplierService companySupplierService;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The record limit service. */
    private RecordLimitService recordLimitService;
    
    /** The file management service. */
    private FileManagementService fileManagementService;
    
    /** The plant denso service. */
    private PlantDensoService plantDensoService;
    
    /** The plant supplier service. */
    private PlantSupplierService plantSupplierService;
    
    /** The plant ASN service. */
    private AsnService asnService;
    
    /** The unit of measure service. */
    private SpsMUnitOfMeasureService spsMUnitOfMeasureService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new Supplier User Role Assignment Facade service impl.
     */
    public AsnProgressInformationFacadeServiceImpl(){
        super();
    }

    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * Set the record limit service.
     * 
     * @param recordLimitService the record limit service to set
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    
    /**
     * Set the file management service.
     * 
     * @param fileManagementService the file management service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the plant denso service.
     * 
     * @param plantDensoService the plant denso service to set
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }

    /**
     * Set the plant supplier service.
     * 
     * @param plantSupplierService the plant supplier service to set
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>Setter method for asnService.</p>
     *
     * @param asnService Set for asnService
     */
    public void setAsnService(AsnService asnService) {
        this.asnService = asnService;
    }
    
    /**
     * Set the unit of measure service.
     * 
     * @param spsMUnitOfMeasureService the unit of measure service to set
     */
    public void setSpsMUnitOfMeasureService(
        SpsMUnitOfMeasureService spsMUnitOfMeasureService) {
        this.spsMUnitOfMeasureService = spsMUnitOfMeasureService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#initial(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public AsnProgressInformationDomain searchInitial(
        AsnProgressInformationDomain asnProgressInformationDomain)throws ApplicationException
    {
        List<MiscellaneousDomain> asnStatusList = new ArrayList<MiscellaneousDomain>();
        List<MiscellaneousDomain> invoiceStatusList = null;
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        Locale locale = asnProgressInformationDomain.getLocale();
        
        // 1. Get relation between DENSO and Supplier.
        DataScopeControlDomain dataScopeControlDomain = 
            asnProgressInformationDomain.getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationList
            = densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        // 2. Get supplier company information from companySupplierService.
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanySupplierDomain> companySupplierDomainList
            =  companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if(null == companySupplierDomainList
            || Constants.ZERO == companySupplierDomainList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        
        // 3. Get plant supplier information from plantSupplierService
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantSupplierDomain> plantSupplierList
            = this.plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        if(null == plantSupplierList || Constants.ZERO == plantSupplierList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        // 4. Get DENSO company information from companyDensoService
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoDomainList
            = companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null ==  companyDensoDomainList
            || Constants.ZERO == companyDensoDomainList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        // 5. Get plant DENSO information from plantDensoService
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantDensoDomain> plantDensoList
            = this.plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null == plantDensoList || Constants.ZERO == plantDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        asnProgressInformationDomain.setCompanySupplierList(companySupplierDomainList);
        asnProgressInformationDomain.setCompanyDensoList(companyDensoDomainList);
        asnProgressInformationDomain.setPlantSupplierList(plantSupplierList);
        asnProgressInformationDomain.setPlantDensoList(plantDensoList);
        
        // 6. Get information for ASN Status Combo box from MiscellaneousService
        miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_ASN_STATUS);
        asnStatusList = miscService.searchMisc(miscDomain);
        if(Constants.ZERO < asnStatusList.size()){
            for(MiscellaneousDomain ansStatus : asnStatusList){
                ansStatus.setMiscValue(StringUtil.appendsString(
                    ansStatus.getMiscCode(), Constants.SYMBOL_SPACE,
                    Constants.SYMBOL_COLON, Constants.SYMBOL_SPACE, ansStatus.getMiscValue()));
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_ASN_STATUS);
        }
        asnProgressInformationDomain.setAsnStatusList(asnStatusList);
        
        // 7. Get information for Invoice Status Combo box from MiscellaneousService
        miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_INVOICE_STATUS);
        invoiceStatusList = miscService.searchMisc(miscDomain);
        if(null == invoiceStatusList || invoiceStatusList.isEmpty()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_INVOICE_STATUS);
        }
        asnProgressInformationDomain.setInvoiceStatusList(invoiceStatusList);
        return asnProgressInformationDomain;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchAsnProgressInformation(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public List<AsnProgressInformationReturnDomain> searchAsnProgressInformation(
        AsnProgressInformationDomain asnProgressInformationDomain)
        throws ApplicationException {
        return search(asnProgressInformationDomain, true);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchAsnProgressInformationCsv(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public List<AsnProgressInformationReturnDomain> searchAsnProgressInformationCsv(
        AsnProgressInformationDomain asnProgressInformationDomain)
        throws ApplicationException {
        return search(asnProgressInformationDomain, false);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchSelectedCompanySupplier(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException
    {
        List<PlantSupplierDomain> plantSupplierList = null;
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantSupplierWithScopeDomain.getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain
            = plantSupplierWithScopeDomain.getPlantSupplierDomain();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(null != densoSupplierRelationList && Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierList = plantSupplierService.searchPlantSupplier(plantSupplierWithScopeDomain);
        if(null == plantSupplierList || Constants.ZERO == plantSupplierList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        return plantSupplierList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchSelectedCompanyDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException
    {
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScopeDomain.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScopeDomain.getPlantDensoDomain();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList
            = densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(null == plantDensoList || Constants.ZERO == plantDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope) throws ApplicationException
    {
        Locale locale = plantSupplierWithScope.getLocale();
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantSupplierWithScope.getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain = plantSupplierWithScope.getPlantSupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if(result.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScope.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if (result.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return result;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public FileManagementDomain searchFileName(
        AsnProgressInformationDomain asnProgressInformationDomain)throws ApplicationException
    {
        Locale locale = asnProgressInformationDomain.getLocale();
        FileManagementDomain resultDomain = null;
        try{
            resultDomain = fileManagementService.searchFileDownload(
                asnProgressInformationDomain.getFileId(), false, null);
        }catch(FileManagementException ex){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }catch(IOException io){
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        asnProgressInformationDomain.setFileName(resultDomain.getFileName());
        return resultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain, java.io.OutputStream)
     */
    public FileManagementDomain searchLegendInfo(
        AsnProgressInformationDomain asnProgressInformationDomain, OutputStream outputStream)
        throws ApplicationException
    {
        Locale locale = asnProgressInformationDomain.getLocale();
        try{
            FileManagementDomain fileManagementDomain = fileManagementService.searchFileDownload(
                asnProgressInformationDomain.getFileId(), true, outputStream);
            return fileManagementDomain;
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        return null;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchValidate(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public boolean searchValidate(
        AsnProgressInformationDomain asnProgressInformationDomain)
        throws ApplicationException {
        // Declare variable.
        final List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        final Locale locale = asnProgressInformationDomain.getLocale();
        boolean result = true;
        String message = Constants.STR_ZERO;
        boolean isValidDateFrom = false;
        boolean isValidDateTo = false;
        
        if(StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtdFrom()) 
            || StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtdTo())
            || StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getVendorCd())
            || StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getSPcd())
            || StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getDCd())
            || StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getDPcd())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale
                    , SupplierPortalConstant.ERROR_CD_SP_E7_0011, null)));
        }else{
            if(Constants.MISC_CODE_ALL.equals(asnProgressInformationDomain.getVendorCd())){
                asnProgressInformationDomain.setVendorCd(Constants.EMPTY_STRING);
            }
            if(Constants.MISC_CODE_ALL.equals(asnProgressInformationDomain.getSPcd())){
                asnProgressInformationDomain.setSPcd(Constants.EMPTY_STRING);
            }
            if(Constants.MISC_CODE_ALL.equals(asnProgressInformationDomain.getDCd())){
                asnProgressInformationDomain.setDCd(Constants.EMPTY_STRING);
            }
            if(Constants.MISC_CODE_ALL.equals(asnProgressInformationDomain.getDPcd())){
                asnProgressInformationDomain.setDPcd(Constants.EMPTY_STRING);
            }
        }
        // Validate ** ActualETD
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtdFrom())){
            if(!DateUtil.isValidDate(asnProgressInformationDomain.getActualEtdFrom(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH)){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_FROM);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtdTo())){
            if(!DateUtil.isValidDate(asnProgressInformationDomain.getActualEtdTo(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH )){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_TO);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtdFrom())
            && !StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtdTo())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO < DateUtil.compareDate(
                asnProgressInformationDomain.getActualEtdFrom(), 
                asnProgressInformationDomain.getActualEtdTo())){
                
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_FROM),
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ACTUAL_ETD_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        
        // Validate ** ActualETA
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtaFrom())){
            if(!DateUtil.isValidDate(asnProgressInformationDomain.getActualEtaFrom(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH)){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ACTUAL_ETA_DATE_FROM);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtaTo())){
            if(!DateUtil.isValidDate(asnProgressInformationDomain.getActualEtaTo(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH)){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ACTUAL_ETA_DATE_TO);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtaFrom())
            && !StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getActualEtaTo())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO < DateUtil.compareDate(
                asnProgressInformationDomain.getActualEtaFrom(), 
                asnProgressInformationDomain.getActualEtaTo())){
                
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ACTUAL_ETA_DATE_FROM),
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ACTUAL_ETA_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        // Validate ** InvoiceDate
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getInvoiceDateFrom())){
            if(!DateUtil.isValidDate(asnProgressInformationDomain.getInvoiceDateFrom(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH)){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_INVOICE_DATE_FROM);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getInvoiceDateTo())){
            if(!DateUtil.isValidDate(asnProgressInformationDomain.getInvoiceDateTo(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH)){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_INVOICE_DATE_TO);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getInvoiceDateFrom())
            && !StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getInvoiceDateTo())
            && isValidDateFrom && isValidDateTo){
            if(Constants.ZERO < DateUtil.compareDate(
                asnProgressInformationDomain.getInvoiceDateFrom(), 
                asnProgressInformationDomain.getInvoiceDateTo())){
                
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_INVOICE_DATE_FROM),
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_INVOICE_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        // Validate ASN No.
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getAsnNo() )){
            if(Constants.MAX_ASN_NO_LENGTH < asnProgressInformationDomain.getAsnNo().length()){
                
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ASN_NO),
                            String.valueOf(Constants.MAX_ASN_NO_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        // Validate Invoice No.
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getInvoiceNo() )){
            if(Constants.MAX_INVOICE_NO_LENGTH 
                < asnProgressInformationDomain.getInvoiceNo().length()){

                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_INVOICE_NO),
                            String.valueOf(Constants.MAX_INVOICE_NO_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        // Validate D Part No.
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getDPn() )){
            if(Constants.MAX_D_PART_NO_LENGTH < asnProgressInformationDomain.getDPn().length()){
                
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_D_PART_NO),
                            String.valueOf(Constants.MAX_D_PART_NO_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        // Validate S Part No.
        if(!StringUtil.checkNullOrEmpty(asnProgressInformationDomain.getSPn() )){
            if(Constants.MAX_S_PART_NO_LENGTH < asnProgressInformationDomain.getSPn().length()){
                
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_S_PART_NO),
                            String.valueOf(Constants.MAX_S_PART_NO_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        
        if(Constants.ZERO < errorMessageList.size()){
            result = false;
        }
        asnProgressInformationDomain.setErrorMessageList(errorMessageList);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#convertToCsvData(java.util.List, java.lang.String[])
     */
    public String convertToCsvData(
        List<AsnProgressInformationReturnDomain> asnProgressInformationReturnDomains, Locale locale)
        throws ApplicationException, IOException
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> asnMap = null;
        String result = null;
        String dateStr = Constants.EMPTY_STRING;
        String asnDate = Constants.EMPTY_STRING;
        String planEta = Constants.EMPTY_STRING;
        String receivingDateStr = Constants.EMPTY_STRING;
        String utc = Constants.EMPTY_STRING;
        String itemDesc = Constants.EMPTY_STRING;
        
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.setGroupingUsed(true);
        
        decimalFormat.setMinimumFractionDigits(Constants.ZERO);
        decimalFormat.setMaximumFractionDigits(Constants.TWO);
        
        if(null == asnProgressInformationReturnDomains
            || Constants.ZERO == asnProgressInformationReturnDomains.size()){
            return Constants.EMPTY_STRING;
        }
        for(AsnProgressInformationReturnDomain domain : asnProgressInformationReturnDomains){
            dateStr = Constants.EMPTY_STRING;
            receivingDateStr = Constants.EMPTY_STRING;
            itemDesc = Constants.EMPTY_STRING;
            
            utc = StringUtil.appendsString(Constants.SYMBOL_OPEN_BRACKET, Constants.WORD_UTC,
                domain.getUtc().trim(), Constants.SYMBOL_CLOSE_BRACKET);
            planEta = StringUtil.appendsString(DateUtil.format(new Date(
                domain.getPlanEta().getTime()), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH), utc);
            asnDate = DateUtil.format(new Date(
                domain.getCreateDateTime().getTime()), DateUtil.PATTERN_YYYYMMDD_SLASH);
            
            if(!StringUtil.checkNullOrEmpty(domain.getItemDesc())){
                itemDesc = StringUtil.appendsString(Constants.SYMBOL_DOUBLE_QUOTE,
                    domain.getItemDesc(), Constants.SYMBOL_DOUBLE_QUOTE);
            }
            
            if(!StringUtil.checkNullOrEmpty(domain.getReceivingDate())
                && !Constants.STR_ZERO.equals(domain.getReceivingDate())){
                Date receivingDate = DateUtil.parseToSqlDate(
                    domain.getReceivingDate(), DateUtil.PATTERN_YYYYMMDD);
                receivingDateStr = DateUtil.format(receivingDate, DateUtil.PATTERN_YYYYMMDD_SLASH);
            }
            
            if(!StringUtil.checkNullOrEmpty(domain.getInvoiceDate())){
                dateStr = DateUtil.format(domain.getInvoiceDate(), DateUtil.PATTERN_YYYYMMDD_SLASH);
            }
            
            asnMap = new HashMap<String, Object>();
            asnMap.put(HEADER_CSV[Constants.ZERO],
                StringUtil.checkNullToEmpty(domain.getVendorCd()));
            asnMap.put(HEADER_CSV[Constants.ONE], StringUtil.checkNullToEmpty(domain.getSPcd()));
            asnMap.put(HEADER_CSV[Constants.TWO], StringUtil.checkNullToEmpty(domain.getDCd()));
            asnMap.put(HEADER_CSV[Constants.THREE], StringUtil.checkNullToEmpty(domain.getDPcd()));
            asnMap.put(HEADER_CSV[Constants.FOUR], domain.getAsnStatus());
            asnMap.put(HEADER_CSV[Constants.FIVE], asnDate);
            asnMap.put(HEADER_CSV[Constants.SIX], domain.getAsnNo());
            asnMap.put(HEADER_CSV[Constants.SEVEN],
                StringUtil.checkNullToEmpty(domain.getTripNo()));
            asnMap.put(HEADER_CSV[Constants.EIGHT], StringUtil.appendsString(
                Constants.SYMBOL_DOUBLE_QUOTE, decimalFormat.format(domain.getNumberOfPallet()),
                Constants.SYMBOL_DOUBLE_QUOTE));
            asnMap.put(HEADER_CSV[Constants.NINE], domain.getActualEtdStr());
            asnMap.put(HEADER_CSV[Constants.TEN], planEta);
            asnMap.put(HEADER_CSV[Constants.ELEVEN], domain.getCurrentSpsDoNo());
            asnMap.put(HEADER_CSV[Constants.TWELVE], domain.getRevision());
            asnMap.put(HEADER_CSV[Constants.THIRTEEN],
                StringUtil.checkNullToEmpty(domain.getCycle()));
            asnMap.put(HEADER_CSV[Constants.FOURTEEN],
                StringUtil.checkNullToEmpty(domain.getTruckRoute()));
            asnMap.put(HEADER_CSV[Constants.FIFTEEN], domain.getTruckSeq());
            asnMap.put(HEADER_CSV[Constants.SIXTEEN],
                StringUtil.checkNullToEmpty(domain.getTrans()));
            asnMap.put(HEADER_CSV[Constants.SEVENTEEN],
                StringUtil.checkNullToEmpty(domain.getWhPrimeReceiving()));
            asnMap.put(HEADER_CSV[Constants.EIGHTEEN],
                StringUtil.checkNullToEmpty(domain.getDockCode()));
            asnMap.put(HEADER_CSV[Constants.NINETEEN],
                StringUtil.checkNullToEmpty(domain.getCtrlNo()));
            asnMap.put(HEADER_CSV[Constants.TWENTY], domain.getDPn());
            asnMap.put(HEADER_CSV[Constants.TWENTY_ONE], itemDesc);
            asnMap.put(HEADER_CSV[Constants.TWENTY_TWO], domain.getSPn());
            asnMap.put(HEADER_CSV[Constants.TWENTY_THREE], domain.getOrderMethod());
            asnMap.put(HEADER_CSV[Constants.TWENTY_FOUR], domain.getShippingQtyStr().replaceAll(
                Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
            asnMap.put(HEADER_CSV[Constants.TWENTY_FIVE], domain.getShippingBoxQty().toString());
            asnMap.put(HEADER_CSV[Constants.TWENTY_SIX],
                StringUtil.checkNullToEmpty(domain.getChangeReasonCd()));
            asnMap.put(HEADER_CSV[Constants.TWENTY_SEVEN], domain.getReviseShippingQtyFlag());
            
            /* [IN042] : Current CIGMA D/O No. is CHG CIGMA D/O No. in DO_DETAIL (if empty use
             * CIGMA_DO_NO)
             * */
            //asnMap.put(HEADER_CSV[Constants.TWENTY_EIGHT], domain.getCurrentCigmaDoNo());
            asnMap.put(HEADER_CSV[Constants.TWENTY_EIGHT], domain.getCigmaDoNo());
            if (!Strings.judgeBlank(domain.getChgCigmaDoNo())) {
                asnMap.put(HEADER_CSV[Constants.TWENTY_EIGHT], domain.getChgCigmaDoNo());
            }
            
            asnMap.put(HEADER_CSV[Constants.TWENTY_NINE], domain.getReceivedQtyStr().replaceAll(
                Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
            asnMap.put(HEADER_CSV[Constants.THIRTY], StringUtil.checkNullToEmpty(domain.getUm()));
            asnMap.put(HEADER_CSV[Constants.THIRTY_ONE], 
                StringUtil.checkNullToEmpty(domain.getRcvLane()));
            asnMap.put(HEADER_CSV[Constants.THIRTY_TWO], receivingDateStr);
            asnMap.put(HEADER_CSV[Constants.THIRTY_THREE], domain.getPnRcvStatus());
            asnMap.put(HEADER_CSV[Constants.THIRTY_FOUR],
                StringUtil.nullToEmpty(domain.getInvoiceNo()));
            asnMap.put(HEADER_CSV[Constants.THIRTY_FIVE], dateStr);
            asnMap.put(HEADER_CSV[Constants.THIRTY_SIX],
                StringUtil.checkNullToEmpty(domain.getInvoiceStatus()));
            resultList.add(asnMap);
        }
        
        CommonDomain commonDomain = new CommonDomain();
        commonDomain.setResultList(resultList);
        commonDomain.setHeaderArr(HEADER_CSV);
        commonDomain.setHeaderFlag(true);
        
        result = commonService.createCsvString(commonDomain).toString();
        if(null == result){
            MessageUtil.throwsApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0012);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService#searchRoleCanOperate(java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * <p>search AsnProgressInformation.</p>
     *
     * @param asnProgressInformationDomain asnProgressInformationDomain
     * @param includePage boolean
     * @return List
     * @throws ApplicationException 
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    private List<AsnProgressInformationReturnDomain> search(
        AsnProgressInformationDomain asnProgressInformationDomain, boolean includePage)
        throws ApplicationException
    {
        List<AsnProgressInformationReturnDomain> asnProgressInformationList = null;
        List<AsnProgressInformationReturnDomain> asnProgressInformationReturnList
            = new ArrayList<AsnProgressInformationReturnDomain>();
        List<AsnProgressInformationReturnDomain> asnProgressInfoResultList
            = new ArrayList<AsnProgressInformationReturnDomain>();
        List<AsnProgressInformationReturnDomain> asnProgressTmpList = null;
        MiscellaneousDomain recordLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        Locale locale = asnProgressInformationDomain.getLocale();
        Map asnProgressInfoMap = new HashMap();
        
        //1. Validate input parameter
        boolean resultValidate = this.searchValidate(asnProgressInformationDomain);
        if(resultValidate){
            //search
            Integer recordCount
                = asnService.searchCountAsnInformation(asnProgressInformationDomain);
            if(Constants.ZERO < recordCount){
                // 2. Get maximum record limit from RecordLimitService
                recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
                recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP009_RLM);
                recordLimit = recordLimitService.searchRecordLimit(recordLimit);
                if(null == recordLimit
                    || StringUtil.checkNullOrEmpty(recordLimit.getMiscValue())){
                    MessageUtil.throwsApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032);
                }
                
                if(Integer.valueOf(recordLimit.getMiscValue()) < recordCount){
                    MessageUtil.throwsApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                        new String[] {recordLimit.getMiscValue()});
                }
                
                if(includePage){
                    // 3. Get maximum record per page from RecordLimitService
                    recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
                    recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP009_PLM);
                    recordLimitPerPage
                        = recordLimitService.searchRecordLimitPerPage(recordLimitPerPage);
                    if(null == recordLimitPerPage
                        || StringUtil.checkNullOrEmpty(recordLimitPerPage.getMiscValue())){
                        MessageUtil.throwsApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0032);
                    }
                    if(Integer.parseInt(recordLimit.getMiscValue()) < recordCount){
                        MessageUtil.throwsApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_E6_0003,
                            new String[]{recordLimit.getMiscValue()});
                    }
                }
                
                // 4.1. Get ASN Progress data
                asnProgressInformationList = asnService.searchAsnInformation(
                    asnProgressInformationDomain);
                
                // 4.2. LOOP until end of <List> asnInformation to find DENSO Company Code
                Set<String> densoCodeList = new TreeSet<String>();
                for (AsnProgressInformationReturnDomain asnProgress : asnProgressInformationList){
                    String dCd = asnProgress.getDCd();
                    if(!asnProgressInfoMap.containsKey(dCd)){
                        densoCodeList.add(dCd);
                        asnProgressTmpList = new ArrayList<AsnProgressInformationReturnDomain>();
                        asnProgressTmpList.add(asnProgress);
                        asnProgressInfoMap.put(dCd, asnProgressTmpList);
                    }else{
                        List<AsnProgressInformationReturnDomain> asnProgressInfoList
                            = (List<AsnProgressInformationReturnDomain>)asnProgressInfoMap.get(dCd);
                        asnProgressInfoList.add(asnProgress);
                    }
                }
                
                SpsMCompanyDensoDomain spsMCompanyDensoDomain = new SpsMCompanyDensoDomain();
                spsMCompanyDensoDomain.setDCd(
                    StringUtil.convertListToVarcharCommaSeperate(densoCodeList));
                List<As400ServerConnectionInformationDomain> as400ServerList
                    = companyDensoService.searchAs400ServerList(spsMCompanyDensoDomain);
                
                if(null == as400ServerList || as400ServerList.isEmpty()){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E5_0026,
                        SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
                }
                if( as400ServerList.size() != densoCodeList.size() ){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E5_0025,
                        SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
                }
                
                if(null != as400ServerList){
                    // 4.4. Get ASN Receiving record from ASNResource for each DENSO Company
                    for(As400ServerConnectionInformationDomain as400Server : as400ServerList)
                    {
                        Set<String> asnNoSet = new HashSet<String>();
                        String dCd = as400Server.getSpsMCompanyDensoDomain().getDCd();
                        asnProgressTmpList
                            = (List<AsnProgressInformationReturnDomain>)asnProgressInfoMap.get(dCd);
                        for(AsnProgressInformationReturnDomain asnProgressTmp : asnProgressTmpList){
                            asnNoSet.add(asnProgressTmp.getAsnNo());
                        }
                        // Call REST receiveing Data.
                        List<PseudoCigmaAsnDomain> cigmaAsnResultList = 
                            CommonWebServiceUtil.asnResourceSearchAsnReceiving(
                                StringUtil.convertListToVarcharCommaSeperate(asnNoSet),
                                Constants.EMPTY_STRING, as400Server, locale);
                        this.mergeAsnProgressCigmaData(cigmaAsnResultList,
                            asnProgressTmpList, asnProgressInformationReturnList);
                    }
                }
                
                //Filter Asn Progress List by asn status.
                String asnStatus = asnProgressInformationDomain.getAsnStatus();
                if(!Constants.MISC_CODE_ALL.equals(asnStatus)){
                    Iterator itr = asnProgressInformationReturnList.iterator();
                    while(itr.hasNext()){
                        AsnProgressInformationReturnDomain asnDetailInfo
                            = (AsnProgressInformationReturnDomain)itr.next();
                        if(!asnStatus.equals(asnDetailInfo.getAsnStatus())){
                            itr.remove();
                        }
                    }
                }
                
                if(null == asnProgressInformationReturnList
                    || asnProgressInformationReturnList.isEmpty()){
                    MessageUtil.throwsApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0001);
                }
                
                if(includePage){
                    // 5. Calculate rownum for query
                    asnProgressInformationDomain.setMaxRowPerPage(
                        Integer.valueOf(recordLimitPerPage.getMiscValue()));
                    SpsPagingUtil.calcPaging(asnProgressInformationDomain,
                        asnProgressInformationReturnList.size());
                    int rowNumfrom = asnProgressInformationDomain.getRowNumFrom() - Constants.ONE;
                    int rowNumTo = asnProgressInformationDomain.getRowNumTo();
                    
                    // 6. Set <List> asnProgressInformation to return Domain
                    for(int i = rowNumfrom; i <  rowNumTo; i++){
                        AsnProgressInformationReturnDomain asnProgressInfo
                            = asnProgressInformationReturnList.get(i);
                        this.doSetAsnProgressForDisplay(asnProgressInfo, locale);
                        asnProgressInfoResultList.add(asnProgressInfo);
                    }
                }else{
                    for(AsnProgressInformationReturnDomain asnProgressInfo
                        : asnProgressInformationReturnList){
                        this.doSetAsnProgressForDisplay(asnProgressInfo, locale);
                        asnProgressInfoResultList.add(asnProgressInfo);
                    }
                }
            }else{
                MessageUtil.throwsApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001);
            }
        }else{
            return null;
        }
        return asnProgressInfoResultList;
    }
    
    /**
     * Merge data from AsnService.searchAsnInformation and CigmaAsnService.serachAsnReceiving
     * @param asnProgressInformationList the list of asnProgressInformationReturnDomain Domain
     * @param cigmaAsnInformationList the list of cigmaAsnInformation Domain
     * @param asnProgressResultList the list of asnProgressInformationReturnDomain Domain
     */
    private void mergeAsnProgressCigmaData(List<PseudoCigmaAsnDomain> cigmaAsnInformationList,
        List<AsnProgressInformationReturnDomain> asnProgressInformationList,
        List<AsnProgressInformationReturnDomain> asnProgressResultList){
        for(AsnProgressInformationReturnDomain asnProgressInfo : asnProgressInformationList){
            String cigmaAsnStatus = Constants.EMPTY_STRING;
            String cigmaPnRcvStatus = Constants.EMPTY_STRING;
            String cigmaReceivingDate = Constants.EMPTY_STRING;
            BigDecimal cigmaReceivedQty = new BigDecimal(Constants.ZERO);
            for(PseudoCigmaAsnDomain cigmaAsn : cigmaAsnInformationList){
                String cigmaCurDoNo = asnProgressInfo.getChgCigmaDoNo();
                if(StringUtil.checkNullOrEmpty(cigmaCurDoNo)){
                    cigmaCurDoNo = asnProgressInfo.getCigmaDoNo();
                }
                if(asnProgressInfo.getAsnNo().trim().equals(cigmaAsn.getPseudoAsnNo().trim()) 
                    && asnProgressInfo.getDPn().equals(cigmaAsn.getPseudoDPn().trim())
                    && cigmaCurDoNo.equals(cigmaAsn.getPseudoCigmaCurDoNo().trim()))
                {
                    cigmaAsnStatus = cigmaAsn.getPseudoAsnStatus();
                    cigmaPnRcvStatus = cigmaAsn.getPseudoPnReceivingStatus();
                    cigmaReceivingDate = StringUtil.nullToEmpty(cigmaAsn.getPseudoReceivedDate());
                    cigmaReceivedQty = new BigDecimal(cigmaAsn.getPseudoReceivedQty());
                    break;
                }
            }
            if(!Constants.ASN_STATUS_CCL.equals(asnProgressInfo.getAsnStatus())){
                if(!StringUtil.checkNullOrEmpty(cigmaAsnStatus)){
                    if(Constants.ASN_STATUS_N.equals(cigmaAsnStatus)){
                        cigmaAsnStatus = Constants.ASN_STATUS_ISS;
                    }else if(Constants.ASN_STATUS_D.equals(cigmaAsnStatus)){
                        cigmaAsnStatus = Constants.ASN_STATUS_CCL;
                    }else if(Constants.ASN_STATUS_P.equals(cigmaAsnStatus)){
                        cigmaAsnStatus = Constants.ASN_STATUS_PTR;
                    }else{
                        cigmaAsnStatus = Constants.ASN_STATUS_RCP;
                    }
                    asnProgressInfo.setAsnStatus(cigmaAsnStatus);
                }
            }
            
            if(!Constants.ASN_STATUS_CCL.equals(asnProgressInfo.getAsnStatus())){
                if(!StringUtil.checkNullOrEmpty(cigmaPnRcvStatus)){
                    if(Constants.ASN_STATUS_N.equals(cigmaPnRcvStatus)){
                        cigmaPnRcvStatus = Constants.ASN_STATUS_ISS;
                    }else if(Constants.ASN_STATUS_D.equals(cigmaPnRcvStatus)){
                        cigmaPnRcvStatus = Constants.ASN_STATUS_CCL;
                    }else if(Constants.ASN_STATUS_P.equals(cigmaPnRcvStatus)){
                        cigmaPnRcvStatus = Constants.ASN_STATUS_PTR;
                    }else{
                        cigmaPnRcvStatus = Constants.ASN_STATUS_RCP;
                    }
                }else{
                    cigmaPnRcvStatus = Constants.ASN_STATUS_ISS;
                }
            }else{
                cigmaPnRcvStatus = Constants.ASN_STATUS_CCL;
            }
            asnProgressInfo.setReceivingDate(cigmaReceivingDate);
            asnProgressInfo.setPnRcvStatus(cigmaPnRcvStatus);
            asnProgressInfo.setReceivedQty(cigmaReceivedQty);
            asnProgressResultList.add(asnProgressInfo);
        }
    }
    
    /**
     * Method for setting asn progress information to display on screen.
     * 
     * @param asnProgressInformation the asnProgressInformationReturn domain
     * @param locale the locale
     * @throws ApplicationException ApplicationException
     */
    private void doSetAsnProgressForDisplay(AsnProgressInformationReturnDomain
        asnProgressInformation, Locale locale)throws ApplicationException
    {
        SpsMUnitOfMeasureCriteriaDomain unitOfMeasureCriteria = null;
        SpsMUnitOfMeasureDomain unitOfMeasureResult = null;
        String shippingQtyStr = Constants.EMPTY_STRING;
        String receivedQtyStr = Constants.EMPTY_STRING;
        StringBuffer actualEtdStr = new StringBuffer();
        StringBuffer actualEtaStr = new StringBuffer();
        StringBuffer utc = new StringBuffer();
        
        utc.append(Constants.SYMBOL_OPEN_BRACKET).append(Constants.WORD_UTC);
        utc.append(asnProgressInformation.getUtc().trim());
        utc.append(Constants.SYMBOL_CLOSE_BRACKET);
        
        actualEtdStr.append(DateUtil.format(new Date(
            asnProgressInformation.getActualEtd().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH)).append(utc);
        
        actualEtaStr.append(DateUtil.format(new Date(
            asnProgressInformation.getActualEta().getTime()),
            DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH)).append(utc);
        
        unitOfMeasureCriteria = new SpsMUnitOfMeasureCriteriaDomain();
        unitOfMeasureCriteria.setUnitOfMeasureId(asnProgressInformation.getUm());
        unitOfMeasureResult = spsMUnitOfMeasureService.searchByKey(
            unitOfMeasureCriteria);
        if(null == unitOfMeasureResult){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_UNIT_OF_MEASURE);
        }
        
        if(Constants.DISPLAY_DECIMAL_FLAG_NO.equals(
            unitOfMeasureResult.getDisplayDecimalPointFlg())){
            shippingQtyStr = StringUtil.toNumberFormat(
                asnProgressInformation.getShippingQty().toString());
            receivedQtyStr = StringUtil.toNumberFormat(
                asnProgressInformation.getReceivedQty().toString());
        }else{
            shippingQtyStr = StringUtil.toCurrencyFormat(
                asnProgressInformation.getShippingQty().toString());
            receivedQtyStr = StringUtil.toCurrencyFormat(
                asnProgressInformation.getReceivedQty().toString());
        }
        
        // FIX : wrong dateformat
        if (null != asnProgressInformation.getInvoiceDate()) {
            asnProgressInformation.setInvoiceDateShow(DateUtil.format(
                asnProgressInformation.getInvoiceDate(), DateUtil.PATTERN_YYYYMMDD_SLASH));
        }
        
        asnProgressInformation.setShippingQtyStr(shippingQtyStr);
        asnProgressInformation.setReceivedQtyStr(receivedQtyStr);
        asnProgressInformation.setActualEtdStr(actualEtdStr.toString());
        asnProgressInformation.setActualEtaStr(actualEtaStr.toString());
    }
}