/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/02/24 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;

/**
 * <p>The Interface Purchase Order Information Service.</p>
 * <p>Service for Supplier User about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search : searchPurchaseOrderReport</li>
 * <li>Method search : searchCountPurchaseOrder</li>
 * <li>Method search : searchPurchaseOrder</li>
 * <li>Method search : searchLastUpdateDatetime</li>
 * <li>Method search : searchCountPurchaseOrderHeader</li>
 * <li>Method search : searchPurchaseOrderHeader</li>
 * <li>Method search : searchPurchaseOrderByDo</li>
 * <li>Method update : updatePurchaseOrder</li>
 * <li>Method update : searchPoFirmWaitToAckForSUser</li>
 * <li>Method update : searchPoForcastWaitToAckForSUser</li>
 * <li>Method update : searchPoFirmWaitToAckForDUser</li>
 * <li>Method update : searchPoForcastWaitToAckForDUser</li>
 * <li>Method search : searchPurgingPurchaseOrder</li>
 * <li>Method search : searchCountEffectParts</li>
 * <li>Method update : updateRejectNotReplyDue</li>
 * </ul>
 *
 * @author CSI
 */
public interface PurchaseOrderService {
    
    /**
     * 
     * <p>Search Purchase Order Report.</p>
     *
     * @param input SpsTPoDomain
     * @return List of PurchaseOrderReportDomain
     */
    public List<PurchaseOrderReportDomain> searchPurchaseOrderReport ( 
        SpsTPoDomain input) ; 
    
    /**
     * 
     * <p>Search Change Material Release report.</p>
     *
     * @param input P/O Domain
     * @return List of Change Material Release
     */
    public List<ChangeMaterialReleaseReportDomain> searchChangeMaterialReleaseReport ( 
        PurchaseOrderInformationDomain input) ; 
    
    /**
     * 
     * <p>generate P/O.</p>
     *
     * @param locale locale
     * @param coverList List of PurchaseOrderCoverPageReportDomain
     * @param reportList List of PurchaseOrderReportDomain
     * @return InputStream report
     * @throws Exception exception
     */
    public InputStream generatePo(Locale locale,
        List<PurchaseOrderCoverPageReportDomain> coverList,
        List<PurchaseOrderReportDomain> reportList) throws Exception;
    
    /**
     * 
     * <p>generate Change Material Release Report.</p>
     *
     * @param locale locale
     * @param reportList List of ChangeMaterialReleaseReportDomain
     * @return InputStream report
     * @throws Exception exception
     */
    public InputStream generateChgPo(Locale locale,
        List<ChangeMaterialReleaseReportDomain> reportList) throws Exception;
    
    /**
     * Search count purchase Order.
     * Search count purchase Order by criteria from screen.
     *  
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the Integer
     */
    public Integer searchCountPurchaseOrder(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain);
    
    /**
     * Search purchase Order.
     * Search purchase Order by criteria from screen.
     * 
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the list of purchase Order Information Domain
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain);

    /**
     * Search count purchase Order Detail.
     * Search count purchase Order Detail by criteria from screen.
     *  
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderDetail(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain);

    /**
     * Search count purchase Order Detail that has PN Status PENDING and match to criteria
     * from screen.
     *  
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderDetailPending(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain);
    
    /**
     * Search purchase Order Detail.
     * Search purchase Order Detail by criteria from screen.
     * 
     * @param purchaseOrderAcknowledgementDomain the purchase Order Acknowledge Domain
     * @return the list of purchase Order Detail Domain
     */
    public List<PurchaseOrderDetailDomain> searchPurchaseOrderDetail(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain);
    
    /**
     * Search purchase Order Detail.
     * Search purchase Order Detail by criteria from screen.
     * 
     * @param purchaseOrderNotificationDomain the purchase Order Notification Domain.
     * @return the list of purchase Order Notification Domain.
     */
    public List<PurchaseOrderNotificationDomain> searchPurchaseOrderNotification(
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain);
    
    /**
     * Search purchase Order Detail.
     * Search purchase Order Detail by criteria from screen.
     * 
     * @param purchaseOrderDueDomain the purchase order due Domain.
     * @return the list of purchase Order Acknowledge Domain.
     */
    public List<PurchaseOrderDueDomain> searchPurchaseOrderDue(
        PurchaseOrderDueDomain purchaseOrderDueDomain);
    
    /**
     * <p>Search Po Firm Wait To Ack For SUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Forcast Wait To Ack For SUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    //[IN072]
    /**
     * <p>Search Po Forcast Reply from Denso Accept.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastDensoAcceptForSUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Forcast Reply from Denso Reject.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastDensoRejectForSUser(
        TaskListDomain taskListCriteriaDomain);
    
        
    /**
     * <p>Search Po Forcast Pending Wait Denso Accept/Reject.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastDensoPendingForDUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Firm Wait To Ack For DUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * <p>Search Po Forcast Wait To Ack For DUser method.</p>
     *
     * @param taskListCriteriaDomain taskListCriteriaDomain
     * @return List
     */
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain);
    
    /**
     * Search count purchase Order Header.
     * Search count purchase Order Header by criteria from screen.
     *  
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the Integer
     */
    public Integer searchCountPurchaseOrderHeader(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain);
    
    /**
     * Search purchase Order Header.
     * Search purchase Order Header by criteria from screen.
     * 
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the list of purchase Order Information Domain
     */
    public List<PurchaseOrderInformationDomain> searchPurchaseOrderHeader(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain);
    
    /**
     * 
     * <p>Search PurchaseOrderByDo</p>
     *
     * @param criteria PurchaseOrderInformationDomain
     * @return List of PurchaseOrderInformationDomain
     */
    public List<PurchaseOrderInformationDomain> 
    searchPurchaseOrderByDo(PurchaseOrderInformationDomain criteria);
    
    /**
     * 
     * <p>the create P/O and return PO_ID method.</p>
     *
     * @param tpoDomain P/O
     * @return BigDecimal id
     */
    public BigDecimal createPurchaseOrder( SpsTPoDomain tpoDomain );
    
    /**
     * 
     * <p>the search exists method.</p>
     *
     * @param poDomain PurchaseOrderDomain
     * @return SpsTPoDomain P/O
     */
    public SpsTPoDomain searchExistPurchaseOrder( PurchaseOrderDomain poDomain ) ;
    
    /**
     * Search Purging Purchase Order.
     * Search Purchase Order for Purging.
     * 
     * @param purgingPODomain the Purging PO Domain
     * @return the list of Purging PO Domain
     */
    public List<PurgingPoDomain> searchPurgingPurchaseOrder(PurgingPoDomain purgingPODomain);

    /**
     * Search change purchase Order.
     * Search change purchase Order by criteria from screen.
     * 
     * @param purchaseOrderInformationDomain the purchase Order Domain
     * @return the list of purchase Order Information Domain
     */
    public List<PurchaseOrderInformationDomain> searchChangePurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain);
    
    /**
     * Search count change purchase Order.
     * Search count change purchase Order by criteria from screen.
     *  
     * @param purchaseOrderInformationDomain the purchase Order Information Domain
     * @return the Integer
     */
    public Integer searchCountChangePurchaseOrder(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain);

    /**
     * 
     * <p>Search Purchase Order Kanban Report.</p>
     *
     * @param input SpsTPoDomain
     * @return List of PurchaseOrderReportDomain
     */
    public List<PurchaseOrderReportDomain> searchPurchaseOrderKanbanReport ( 
        SpsTPoDomain input) ;
    
    // [IN055] Validate Supplier Information before delete
    /**
     * Search Count Effect Parts
     * Search DENSO Parts that effected to Supplier Information
     * @param poDomain search criteria
     * @return Number of effected parts
     * */
    public Integer searchCountEffectParts(PurchaseOrderDomain poDomain);
    
    // [IN054] Update all Supplier Promised Due that not reply to Reject
    /**
     * Update all Supplier Promised Due that not reply to Reject
     * @param spsTPoDomain P/O
     * @return Integer effect record
     */
    public Integer updateRejectNotReplyDue( SpsTPoDomain spsTPoDomain );
    /**
     * <p>Delete Purging P/O.</p>
     * <ul>
     * <li>Delete P/O data for purging.</li>
     * 
     * @param poId the poId
     * @return the amount of SPS Transaction P/O Domain
     */
    public int deletePurgingPo(String poId);
    
    /**
     * 
     * <p>Purge data from SPS_T_PO.</p>
     *
     * @param paramters PurgingBhtTransmitLogDomain
     * @return Integer
     */
    public Integer deletePurchaseOrder(
            PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria)
			throws ApplicationException;
    
}