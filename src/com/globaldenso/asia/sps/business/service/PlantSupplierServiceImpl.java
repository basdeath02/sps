/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.dao.PlantSupplierDao;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * <p>The class PlantSupplierServiceImpl implement PlantSupplierService.</p>
 * <p>Service for Plant Service about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchSupplierPlant</li>
 * </ul>
 *
 * @author CSI
 */
public class PlantSupplierServiceImpl implements PlantSupplierService {

    /** The Plant Supplier Dao. */
    private PlantSupplierDao plantSupplierDao;
    
    /** The default constructor. */
    public PlantSupplierServiceImpl() {
        super();
    }
    
    /**
     * Set method for plantSupplierDao.
     * @param plantSupplierDao the plantSupplierDao to set
     */
    public void setPlantSupplierDao(PlantSupplierDao plantSupplierDao) {
        this.plantSupplierDao = plantSupplierDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantSupplierService#searchPlantSupplier
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
    {
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(
            plantSupplierWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            plantSupplierList = this.plantSupplierDao.searchPlantSupplierByRole(
                plantSupplierWithScopeDomain);
        } else {
            plantSupplierList = this.plantSupplierDao.searchPlantSupplierByRelation(
                plantSupplierWithScopeDomain);
        }
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantSupplierService#searchPlantSupplierNew
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchPlantSupplierNew(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
    {
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(
            plantSupplierWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            plantSupplierList = this.plantSupplierDao.searchPlantSupplierByRole(
                plantSupplierWithScopeDomain);
        } else {
            plantSupplierList = this.plantSupplierDao.searchPlantSupplierByRelation(
                plantSupplierWithScopeDomain);
        }
//        return plantSupplierList;
        
        // Akat K. comment : not have to do this any more
//        List<PlantSupplierDomain> result = new ArrayList<PlantSupplierDomain>();
//        for (Iterator<SpsMPlantSupplierDomain> iterator = plantSupplierList.iterator(); iterator.hasNext();) {
//            SpsMPlantSupplierDomain plantSupplierDomain = (SpsMPlantSupplierDomain)iterator.next();
//            PlantSupplierDomain domain = new PlantSupplierDomain();
//            domain.setPlantName( plantSupplierDomain.getPlantName() );
//            domain.setSupplierCompanyCode( plantSupplierDomain.getSCd() );
//            domain.setSupplierPlantCode( plantSupplierDomain.getSPcd() );
//            result.add(domain);
//        }
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantSupplierService#searchPlantSupplierInformation
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchPlantSupplierInformation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
    {
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(
            plantSupplierWithScopeDomain.getDataScopeControlDomain().getUserType()))
        {
            plantSupplierList = this.plantSupplierDao.searchPlantSupplierInformationByRole(
                plantSupplierWithScopeDomain);
        } else {
            plantSupplierList = this.plantSupplierDao.searchPlantSupplierInformationByRelation(
                plantSupplierWithScopeDomain);
        }
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PlantSupplierService#searchExistPlantSupplier
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public Integer searchExistPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        Integer recordCount = 0;
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(
            plantSupplierWithScopeDomain.getDataScopeControlDomain().getUserType())){
            recordCount = this.plantSupplierDao.searchExistPlantSupplierByRole(
                plantSupplierWithScopeDomain);
        }else{
            recordCount = this.plantSupplierDao.searchExistPlantSupplierByRelation(
                plantSupplierWithScopeDomain);
        }
        return recordCount;
    }

}
