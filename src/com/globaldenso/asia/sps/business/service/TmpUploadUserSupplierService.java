/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import com.globaldenso.asia.sps.business.domain.TmpUserSupplierDomain;

/**
 * <p>The Interface TempUserSupplierService.</p>
 * <p>Service for Temporary User Supplier about search from CSV file.</p>
 * <ul>
 *  <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * </ul>
 *
 * @author CSI
 */
public interface TmpUploadUserSupplierService {
       
    /**
     * <p>Search Temporary Upload Supplier One Record.</p>
     * 
     * @param tmpUserSupplierDomain that contain upload supplier user information.
     * @return the Supplier user information from upload.
     */
    public TmpUserSupplierDomain searchTmpUploadSupplierOneRecord(TmpUserSupplierDomain 
        tmpUserSupplierDomain);

}