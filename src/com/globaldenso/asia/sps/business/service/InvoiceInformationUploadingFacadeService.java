/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain;

/**
 * <p>The Interface InvoiceInformationFacadeService.</p>
 * <p>Facade for InvoiceInformationFacadeService.</p>
 * <ul>
 * <li>Method delete   : deleteTmpUploadInvoice</li>
 * <li>Method search   : searchByReturn</li>
 * <li>Method create   : transactInvoice</li>
 * <li>Method search   : searchTmpUploadInvoiceCsv</li>
 * <li>Method search   : searchDensoSupplierRelation</li>
 * <li>Method validate : searchValidateGroupInvoice</li>
 * <li>Method search   : downloadInvoiceInformation</li>
 * <li>Method search   : changeSelectDensoCompany</li>
 * </ul>
 *
 * @author CSI
 */
public interface InvoiceInformationUploadingFacadeService {
    
    /**
     * <p>Delete temporary upload invoice.</p>
     * <ul>
     * <li>Delete data by condition in temporary upload invoice table.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingDomain the Invoice Information Uploading Domain
     * @throws ApplicationException ApplicationException
     */
    public void deleteTmpUploadInvoice(InvoiceInformationUploadingDomain 
        invoiceInformationUploadingDomain) throws ApplicationException;
    
    /**
     * <p>Delete temporary upload invoice.</p>
     * <ul>
     * <li>Delete data by condition in temporary upload invoice table.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingDomain the Invoice Information Uploading Domain
     * @return the Invoice Information Uploading Return Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationUploadingDomain searchByReturn(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain) 
        throws ApplicationException;
    
    /**
     * <p>Transact invoice.</p>
     * <ul>
     * <li>Validate and import data from CSV File into database.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingDomain the Invoice Information Uploading Domain
     * @param dataScopeControlDomain the Data Scope Control Domain
     * @return the Invoice Information Uploading Return Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationUploadingDomain transactInvoice(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain, 
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>Search Temp Upload Invoice CSV.</p>
     * <ul>
     * <li>Search and create CSV File data.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingDomain the Invoice Information Uploading Domain
     * @return the Invoice Information Uploading Return Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationUploadingDomain searchTmpUploadInvoiceCsv(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain) 
        throws ApplicationException;
    
    /**
     * <p>Validate group invoice.</p>
     * <ul>
     * <li>Search and create CSV File data.</li>
     * </ul>
     * 
     * @param invoiceInformationUploadingDomain the Invoice Information Uploading Domain.
     * @return the Invoice Information Uploading Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationUploadingDomain searchValidateGroupInvoice(
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain) 
        throws ApplicationException;
    /**
     * <p>Search Temp Upload Invoice CSV.</p>
     * <ul>
     * <li>Search and create CSV File data.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the Data Scope Control Domain
     * @throws ApplicationException ApplicationException
     */
    public void searchDensoSupplierRelation(
        DataScopeControlDomain dataScopeControlDomain) throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}