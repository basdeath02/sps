/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2018/04/19 Netband U.Rungsiwut       Generate invoice PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface InvoiceInformationFacadeService.</p>
 * <p>Facade for InvoiceInformationFacadeService.</p>
 * <ul>
 * <li>Method initial : searchInitial</li>
 * <li>Method search  : searchInvoiceInformation</li>
 * <li>Method update  : updateInvoiceStatus</li>
 * <li>Method create  : downloadInvoiceInformation</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchInvoiceCoverPage</li>
 * <li>Method search  : searchSelectedDensoCompany</li>
 * <li>Method search  : searchLegendInfo</li>
 * </ul>
 *
 * @author CSI
 */
public interface InvoiceInformationFacadeService {
    
    /**
     * <p>Search invoice information detail.</p>
     * <ul>
     * <li>Search invoice information item detail for display on invoice Information Screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the Data Scope Control Domain
     * @param invoiceInformationDomain the Invoice Information Domain
     * @return the list of ASN Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationReturnDomain searchInitial(
        DataScopeControlDomain dataScopeControlDomain, 
        InvoiceInformationDomain invoiceInformationDomain)throws ApplicationException;
    
    /**
     * <p>Search invoice information detail.</p>
     * <ul>
     * <li>Search invoice information item detail for display on Invoice Information Screen.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the Invoice Information Domain
     * @return the list of Invoice Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationReturnDomain searchInvoiceInformation(
        InvoiceInformationDomain invoiceInformationDomain) throws ApplicationException;
    
    /**
     * <p>Search invoice information CSV.</p>
     * <ul>
     * <li>Search invoice information item detail for display on Invoice Information Screen.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the Invoice Information Domain
     * @return the list of Invoice Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public InvoiceInformationReturnDomain searchInvoiceInformationCsv(
        InvoiceInformationDomain invoiceInformationDomain) throws ApplicationException;
    
    /**
     * <p>Transact Cancel Invoice.</p>
     * <ul>
     * <li>Updating invoice status from old status to be cancel status.</li>
     * </ul>
     * 
     * @param invoiceInformationReturnDomain the invoice information return domain
     * @return is process complete normally
     * @throws ApplicationException ApplicationException
     */
    public boolean transactCancelInvoice(
        InvoiceInformationReturnDomain invoiceInformationReturnDomain) 
        throws ApplicationException;
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search file name for setting header of file download.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the invoice information domain.
     * @return the string.
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName(InvoiceInformationDomain invoiceInformationDomain) 
        throws ApplicationException;
    
    /**
     * <p>Search invoice cover page.</p>
     * <ul>
     * <li>Search file download from file was uploaded.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the invoice information domain.
     * @param output the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchInvoiceCoverPage(InvoiceInformationDomain invoiceInformationDomain, 
        OutputStream output)throws ApplicationException;
    
    /**
     * <p>Change select denso company.</p>
     * <ul>
     * <li>Change select denso company.</li>
     * </ul>
     * 
     * @return the list
     * @param plantDensoWithScopeDomain the Plant Denso With Scope Domain
     * @throws ApplicationException ApplicationException
     */
    List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>Search Legend Information.</p>
     * <ul>
     * <li>Search delivery order report from PDF file id.</li>
     * </ul>
     * 
     * @param invoiceInformationDomain the invoice information domain.
     * @param output the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(InvoiceInformationDomain invoiceInformationDomain,
        OutputStream output) throws ApplicationException;

    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
    
    
    /**
     * Search Sending Email for Cancel Invoice.
     * @return boolean : return true if success send email else return false
     * @param invoiceInformationReturnDomain : invoice information detail
     * @throws ApplicationException contain application message
     * */
    public boolean searchSendingEmail(InvoiceInformationReturnDomain invoiceInformationReturnDomain)
        throws ApplicationException;
    
    /**
     * Generate Invoice Report Information and return by throw ApplicationException.
     * @param invoiceInformationDomain invoiceInformationDomain
     * @return FileManagementDomain createInvoiceReport
     * @throws ApplicationException contain application message
     * */
    public FileManagementDomain searchGenerateInvoice(
        InvoiceInformationDomain invoiceInformationDomain)
        throws ApplicationException;
}