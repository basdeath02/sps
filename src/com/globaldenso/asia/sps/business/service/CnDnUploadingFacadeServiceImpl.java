/*
d * ModifyDate Development company     Describe 
 * 2014/06/11 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CnDnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>
 * The class CNDNUploadingFacadeServiceImpl.
 * </p>
 * <p>
 * Facade for CNDNUploadingFacadeServiceImpl.
 * </p>
 * <ul>
 * <li>Method create : transactUpload</li>
 * </ul>
 * 
 * @author CSI
 */
public class CnDnUploadingFacadeServiceImpl implements
    CnDnUploadingFacadeService {

    /** The DensoSupplierRelation service. */
    private DensoSupplierRelationService densoSupplierRelationService;

    /** The CompanySupplier service. */
    private CompanySupplierService companySupplierService;

    /** The file management service. */
    private FileManagementService fileManagementService = null;

    /** The common service. */
    private CommonService commonService = null;

    /** The SpsTFileUpload Service. */
    private SpsTFileUploadService spsTFileUploadService;

    /** The PlantSupplierService . */
    private PlantSupplierService plantSupplierService;

    /** Call Service spsMAs400VendorService. */
    private SpsMAs400VendorService spsMAs400VendorService;

    /**
     * <p>
     * Call Service CompanyDensoService.
     * </p>
     */
    private CompanyDensoService densoCompanyService;

    /**
     * <p>
     * Call Service PlantDensoService.
     * </p>
     */
    private PlantDensoService plantDensoService;

    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /**
     * Instantiates a new upload facade service impl.
     */
    public CnDnUploadingFacadeServiceImpl() {
        super();
    }

    /**
     * Set the file manager service.
     * 
     * @param fileManagementService the file manager service to set
     */
    public void setFileManagementService(
        FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service to set
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationService.
     * </p>
     * 
     * @param densoSupplierRelationService Set for densoSupplierRelationService
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * <p>
     * Setter method for companySupplierService.
     * </p>
     * 
     * @param companySupplierService Set for companySupplierService
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }

    /**
     * <p>
     * Setter method for spsTFileUploadService.
     * </p>
     * 
     * @param spsTFileUploadService Set for spsTFileUploadService
     */
    public void setSpsTFileUploadService(
        SpsTFileUploadService spsTFileUploadService) {
        this.spsTFileUploadService = spsTFileUploadService;
    }

    /**
     * <p>
     * Setter method for plantSupplierService.
     * </p>
     * 
     * @param plantSupplierService Set for plantSupplierService
     */
    public void setPlantSupplierService(
        PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * <p>
     * Setter method for plantDensoService.
     * </p>
     * 
     * @param plantDensoService Set for plantDensoService
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }

    /**
     * <p>
     * Setter method for densoCompanyService.
     * </p>
     * 
     * @param densoCompanyService Set for densoCompanyService
     */
    public void setDensoCompanyService(CompanyDensoService densoCompanyService) {
        this.densoCompanyService = densoCompanyService;
    }

    /**
     * <p>
     * Setter method for spsMAs400VendorService.
     * </p>
     * 
     * @param spsMAs400VendorService Set for spsMAs400VendorService
     */
    public void setSpsMAs400VendorService(
        SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService#transactUpload
     *      (com.globaldenso.asia.sps.business.domain.FileManagementDomain,
     *      com.globaldenso.asia.sps .business.domain.FileUploadDomain)
     */
    public CnDnUploadingDomain transactUploadCnDn(
        FileManagementDomain fileManagementDomain,
        SpsTFileUploadDomain spsTFileUploadDomain, int fileSize , int maxCsvFileSize) 
        throws ApplicationException {

        Locale locale = spsTFileUploadDomain.getLocale();
        Timestamp dateTime;
        SpsTFileUploadCriteriaDomain spsTFileUploadCriteriaDomain = 
            new SpsTFileUploadCriteriaDomain();
        spsTFileUploadCriteriaDomain.setFileName(spsTFileUploadDomain
            .getFileName());
        spsTFileUploadCriteriaDomain.setVendorCd(spsTFileUploadDomain
            .getVendorCd());
        spsTFileUploadCriteriaDomain.setSPcd(spsTFileUploadDomain.getSPcd());
        spsTFileUploadCriteriaDomain.setFileComment(spsTFileUploadDomain
            .getFileComment());
        spsTFileUploadCriteriaDomain.setDCd(spsTFileUploadDomain.getDCd());
        spsTFileUploadCriteriaDomain.setDPcd(spsTFileUploadDomain.getDPcd());
        String fileId = new String();
        CnDnUploadingDomain cnDnUploadingDomain = new CnDnUploadingDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        boolean fileCheck = true;

        try {

            if (StringUtil.checkNullOrEmpty(spsTFileUploadDomain.getVendorCd())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_S_CD, locale)})));
            }

            if (StringUtil.checkNullOrEmpty(spsTFileUploadDomain.getSPcd())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_S_P, locale)})));
            }

            if (StringUtil.checkNullOrEmpty(spsTFileUploadDomain.getDCd())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_D_CD, locale)})));
            }

            if (StringUtil.checkNullOrEmpty(spsTFileUploadDomain.getDPcd())) {

                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(
                            locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                            new String[] {getLabel(
                                SupplierPortalConstant.LBL_D_P, locale)})));
            }
            
            if (fileSize <= Constants.ZERO) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessage(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0014)));
                
                fileCheck = false;
                
            } else {
                if (maxCsvFileSize < fileSize) {
                    
                    errorMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(
                                locale,
                                SupplierPortalConstant.ERROR_CD_SP_E7_0015,
                                new String[] {String.valueOf(maxCsvFileSize)})));
                    
                    fileCheck = false;

                }
            }

            StringBuffer filename = new StringBuffer();

            filename.append(Constants.SYMBOL_DOT);
            filename.append(Constants.REPORT_CSV_FORMAT);

            if (!fileManagementDomain.getFileName().toLowerCase().endsWith(
                filename.toString()) && fileCheck) {
                
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, MessageUtil
                        .getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0013,
                            new String[] {Constants.REPORT_CSV_FORMAT})));
                
                fileCheck = false;
            }
            
            SpsMAs400VendorCriteriaDomain spsMAs400VendorCriteriaDomain = 
                new SpsMAs400VendorCriteriaDomain();
            List<SpsMAs400VendorDomain> as400VendorDomainList = 
                new ArrayList<SpsMAs400VendorDomain>();
            
            if(fileCheck){
                
                spsMAs400VendorCriteriaDomain.setVendorCd(spsTFileUploadDomain
                    .getVendorCd());
                spsMAs400VendorCriteriaDomain.setDCd(spsTFileUploadDomain.getDCd());
                spsMAs400VendorCriteriaDomain.setIsActive(Constants.IS_ACTIVE);

                as400VendorDomainList = spsMAs400VendorService
                    .searchByCondition(spsMAs400VendorCriteriaDomain);
                
                if (Constants.ZERO != checkFilenameExist(spsTFileUploadCriteriaDomain, 
                    as400VendorDomainList)) {

                    errorMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, MessageUtil
                            .getApplicationMessageHandledException(locale,
                                SupplierPortalConstant.ERROR_CD_SP_E6_0006, null)));
                
                }
            }
            
            if (Constants.ZERO != errorMessageList.size()) {
                return cnDnUploadingDomain;
            }
            
            fileId = fileManagementService.createFileUpload(
                fileManagementDomain.getFileData(),
                fileManagementDomain.getFileName(), Constants.SAVE_LIMIT_TERM,
                fileManagementDomain.getUploadUser());

            dateTime = commonService.searchSysDate();
            spsTFileUploadDomain.setFileId(fileId);
            spsTFileUploadDomain
                .setVendorCd(spsTFileUploadDomain.getVendorCd());

            spsTFileUploadDomain.setSCd(as400VendorDomainList
                .get(Constants.ZERO).getSCd().toUpperCase());
            spsTFileUploadDomain.setFileName(fileManagementDomain.getFileName()
                .toUpperCase());
            spsTFileUploadDomain.setFileComment(spsTFileUploadDomain
                .getFileComment().toUpperCase());
            spsTFileUploadDomain.setSPcd(spsTFileUploadDomain.getSPcd()
                .toUpperCase());
            spsTFileUploadDomain.setCreateDatetime(dateTime);
            spsTFileUploadDomain.setLastUpdateDatetime(dateTime);
            spsTFileUploadDomain.setLastUpdateDscId(spsTFileUploadDomain
                .getLastUpdateDscId());
            spsTFileUploadDomain.setCreateDscId(spsTFileUploadDomain
                .getCreateDscId());
            spsTFileUploadDomain.setDCd(spsTFileUploadDomain.getDCd()
                .toUpperCase());
            spsTFileUploadDomain.setDPcd(spsTFileUploadDomain.getDPcd()
                .toUpperCase());

            spsTFileUploadService.create(spsTFileUploadDomain);

        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception exception) {
            throw new ApplicationException(exception.getMessage());
        } finally {
            cnDnUploadingDomain.setErrorMessageList(errorMessageList);
        }

        return cnDnUploadingDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CnDnUploadingFacadeService
     *      #searchInitial(com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    public CnDnUploadingDomain searchInitial(DataScopeControlDomain dataScopeControlDomain)
        throws ApplicationException {

        Locale locale = dataScopeControlDomain.getLocale();
        List<MiscellaneousDomain> companySupplierDomainMiscList = 
            new ArrayList<MiscellaneousDomain>();
        CnDnUploadingDomain cnDnUploadingDomain = new CnDnUploadingDomain();

        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService
            .searchDensoSupplierRelation(dataScopeControlDomain);

        if (null == densoSupplierRelationDomainList 
            || Constants.ZERO == densoSupplierRelationDomainList.size()) {
            MessageUtil.throwsApplicationMessage(
                locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }

        dataScopeControlDomain
            .setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanySupplierDomain> companySupplierDomainList = companySupplierService
            .searchCompanySupplier(plantSupplierWithScope);
        if (null == companySupplierDomainList 
            || Constants.ZERO == companySupplierDomainList.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE, locale)});
        }
        for (CompanySupplierDomain company : companySupplierDomainList) {
            MiscellaneousDomain companyCode = new MiscellaneousDomain();
            companyCode.setMiscCode(company.getVendorCd());
            companyCode.setMiscValue(company.getVendorCd());
            companySupplierDomainMiscList.add(companyCode);
        }
        cnDnUploadingDomain.setCompanySupplierList(companySupplierDomainMiscList);
        
        /** Get list of supplier plant information from SupplierPlantService. */
        List<PlantSupplierDomain> spsMPlantSupplierDomain = null;
        spsMPlantSupplierDomain = plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        if (null == spsMPlantSupplierDomain
            || spsMPlantSupplierDomain.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE, locale)});
        }
        cnDnUploadingDomain.setPlantSupplierList(spsMPlantSupplierDomain);
        
        /** Get DENSO company information from DensoCompanyService. */
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoDomain = null;
        companyDensoDomain = densoCompanyService.searchCompanyDenso(plantDensoWithScope);
        if (null == companyDensoDomain
            || Constants.ZERO == companyDensoDomain.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }
        MiscellaneousDomain miscDomainDenso = null;
        List<MiscellaneousDomain> companyDensoList = new ArrayList<MiscellaneousDomain>();
        for (CompanyDensoDomain company : companyDensoDomain) {
            miscDomainDenso = new MiscellaneousDomain();
            miscDomainDenso.setMiscCode(company.getDCd());
            miscDomainDenso.setMiscValue(company.getDCd());
            companyDensoList.add(miscDomainDenso);
        }
        cnDnUploadingDomain.setCompanyDensoList(companyDensoList);
        
        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
        plantDensoDomain = plantDensoService.searchPlantDenso(plantDensoWithScope);
        if (null == plantDensoDomain
            || plantDensoDomain.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }
        cnDnUploadingDomain.setPlantDensoList(plantDensoDomain);
        
        return cnDnUploadingDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService
     *      #searchSelectedCompanySupplier
     *      (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)
        throws ApplicationException {

        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(plantSupplierWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantSupplierWithScopeDomain
                .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        
        if(!StringUtil.checkNullOrEmpty(plantSupplierWithScopeDomain.getPlantSupplierDomain()
            .getVendorCd()))
        {
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                plantSupplierWithScopeDomain.getPlantSupplierDomain().getVendorCd().toUpperCase());
        }
       
        plantSupplierWithScopeDomain.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of supplier plant information from SupplierPlantService. */
        List<PlantSupplierDomain> spsMPlantSupplierDomain = null;
        spsMPlantSupplierDomain = plantSupplierService
            .searchPlantSupplier(plantSupplierWithScopeDomain);

        if (null == spsMPlantSupplierDomain
            || spsMPlantSupplierDomain.size() <= Constants.ZERO) {
            Locale locale = plantSupplierWithScopeDomain.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE, locale)});
        }

        /** Return to Action with <List>PlantSupplierDomain */
        return spsMPlantSupplierDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService
     *      #searchSelectedCompanySupplier
     *      (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
        throws ApplicationException {

        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(plantSupplierWithScope
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantSupplierWithScope
                .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            Locale locale = plantSupplierWithScope.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        
        if(!StringUtil.checkNullOrEmpty(plantSupplierWithScope.getPlantSupplierDomain()
            .getSPcd()))
        {
            plantSupplierWithScope.getPlantSupplierDomain().setSPcd(
                plantSupplierWithScope.getPlantSupplierDomain().getSPcd().toUpperCase());
        }
       
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScope.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of supplier plant information from SupplierPlantService. */
        List<CompanySupplierDomain> companySupplierList = null;
        companySupplierList = companySupplierService.searchCompanySupplier(plantSupplierWithScope);

        if (null == companySupplierList
            || companySupplierList.size() <= Constants.ZERO) {
            Locale locale = plantSupplierWithScope.getLocale();
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE, locale)});
        }

        /** Return to Action with <List>PlantSupplierDomain */
        return companySupplierList;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CnDnUploadingFacadeService#searchSelectedCompanyDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        Locale locale = plantDensoWithScopeDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        if (!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDCd()))
        {
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDCd().toUpperCase());
        }

        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            dataScopeControlDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
        plantDensoDomain = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if (null == plantDensoDomain || plantDensoDomain.size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(
                    SupplierPortalConstant.LBL_DENSO_PLANT_CODE, locale)});
        }

        return plantDensoDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.CnDnUploadingFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException {

        Locale locale = plantDensoWithScopeDomain.getLocale();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserType());
        dataScopeControlDomain.setUserRoleDomainList(plantDensoWithScopeDomain
            .getDataScopeControlDomain().getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /** Get relation between DENSO and Supplier. */
        dataScopeControlDomain.setDensoSupplierRelationDomainList(
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain));
        if (null == dataScopeControlDomain.getDensoSupplierRelationDomainList()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION,
                    locale)});
        }
        if (!StringUtil.checkNullOrEmpty(plantDensoWithScopeDomain.getPlantDensoDomain().getDPcd()))
        {
            plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(
                plantDensoWithScopeDomain.getPlantDensoDomain().getDPcd().toUpperCase());
        }

        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            dataScopeControlDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain().setDensoSupplierRelationDomainList(
            dataScopeControlDomain.getDensoSupplierRelationDomainList());

        /** Get list of DENSO plant information from DENSOPlantService. */
        List<CompanyDensoDomain> companyDensoDomain = null;
        companyDensoDomain = densoCompanyService.searchCompanyDenso(plantDensoWithScopeDomain);
        if (null == companyDensoDomain
            || Constants.ZERO == companyDensoDomain.size()) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[] {getLabel(SupplierPortalConstant.LBL_DENSO_COMPANY_CODE, locale)});
        }

        return companyDensoDomain;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws ApplicationException
     * @see com.globaldenso.asia.sps.business.service.CNDNUploadingFacadeService
     *      #checkFilenameExist
     *      (com.globaldenso.asia.sps.business.domain.SpsTFileUploadCriteriaDomain)
     */
    public Integer checkFilenameExist(SpsTFileUploadCriteriaDomain criteria, 
        List<SpsMAs400VendorDomain> as400VendorDomainList)
        throws ApplicationException {
        
        SpsTFileUploadCriteriaDomain  spsTFileUploadCriteriaDomain = 
            new SpsTFileUploadCriteriaDomain();
        
        spsTFileUploadCriteriaDomain.setSCd(as400VendorDomainList.get(Constants.ZERO).getSCd()
            .toUpperCase());
        spsTFileUploadCriteriaDomain.setSPcd(criteria.getSPcd().toUpperCase());
        spsTFileUploadCriteriaDomain.setDCd(criteria.getDCd().toUpperCase());
        spsTFileUploadCriteriaDomain.setDPcd(criteria.getDPcd().toUpperCase());
        spsTFileUploadCriteriaDomain.setFileName(criteria.getFileName().toUpperCase());
        
        Integer recordCount = Constants.ZERO;
        recordCount = spsTFileUploadService.searchCount(spsTFileUploadCriteriaDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }

    /**
     * <p>
     * Get Label method.
     * </p>
     * 
     * @param key message key
     * @param locale locale
     * @return Error massage
     */
    private String getLabel(String key, Locale locale) {

        String labelString = MessageUtil.getLabelHandledException(locale, key);
        return labelString;
    }
}
