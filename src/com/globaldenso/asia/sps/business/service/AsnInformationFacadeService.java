/*
 * ModifyDate Development company     Describe 
 * 2014/07/10 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;

/**
 * <p>The Interface ASNInformationFacadeService.</p>
 * <p>Facade for ASNInformationFacadeService.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchAsnInformation</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchLegendInfo</li>
 * <li>Method search  : validateGroupInvoice</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnInformationFacadeService {
    
    /**
     * <p>Initial.</p>
     * <ul>
     * <li>Initialization of ASN Information Screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain the Data Scope Control Domain
     * @param asnInformationDomain the ASN Information Criteria Domain
     * @return the list of ASN Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public AsnInformationReturnDomain searchInitial(DataScopeControlDomain dataScopeControlDomain, 
        AsnInformationDomain asnInformationDomain)throws ApplicationException;
        
    /**
     * <p>Search ASN information detail.</p>
     * <ul>
     * <li>Search ASN information item detail for display on ASN Information Screen.</li>
     * </ul>
     * 
     * @param asnInformationDomain the ASN Information Criteria Domain
     * @return the list of ASN Information Domain.
     * @throws ApplicationException ApplicationException
     */
    public AsnInformationReturnDomain searchAsnInformation(AsnInformationDomain asnInformationDomain)
        throws ApplicationException;    
    
    /**
     * <p>Search file name.</p>
     * <ul>
     * <li>Search PDF file name from file id.</li>
     * </ul>
     * 
     * @param asnInformationDomain the ASN Information Domain
     * @return the string
     * @throws ApplicationException ApplicationException
     */
    public FileManagementDomain searchFileName (AsnInformationDomain asnInformationDomain) 
        throws ApplicationException;
    
    /**
     * <p>Search Legend Information.</p>
     * <ul>
     * <li>Search delivery order report from PDF file id.</li>
     * </ul>
     * 
     * @param asnInformationDomain the ASN Information Domain
     * @param output the output stream
     * @throws ApplicationException ApplicationException
     */
    public void searchLegendInfo(AsnInformationDomain asnInformationDomain,
        OutputStream output) throws ApplicationException;
    
    /**
     * <p>Search selected company denso.</p>
     * <ul>
     * <li>Search selected company denso.</li>
     * </ul>
     * 
     * @return the list
     * @param plantDensoWithScopeDomain the Plant Denso With Scope Domain
     * @throws ApplicationException ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)throws ApplicationException ;

    /**
     * <p>
     * Search Selected Plant Denso.
     * </p>
     * 
     * @param plantDensoWithScopeDomain PlantDensoWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain)
        throws ApplicationException;

    /**
     * <p>Search selected company supplier.</p>
     * <ul>
     * <li>Search selected company supplier.</li>
     * </ul>
     * 
     * @return the list
     * @param plantSupplierWithScopeDomain the Plant Denso With Scope Domain
     * @throws ApplicationException ApplicationException
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain)throws ApplicationException;

    /**
     * <p>
     * Search Selected Plant Supplier method.
     * </p>
     * 
     * @param plantSupplierWithScope PlantSupplierWithScopeDomain
     * @return List
     * @throws ApplicationException ApplicationException
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
        throws ApplicationException;

    /**
     * <p>Search ASN information detail.</p>
     * <ul>
     * <li>Search ASN information item detail for display on ASN Information Screen.</li>
     * </ul>
     * 
     * @param asnSelectedMap the map of string with ASN Information Domain
     * @param locale the Locale
     * @return the list of String.
     * @throws ApplicationException ApplicationException
     */
    public AsnInformationReturnDomain validateGroupInvoice(
        Map<String, AsnInformationDomain> asnSelectedMap, Locale locale)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}