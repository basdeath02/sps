/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Chowwanat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadDomain;
import com.globaldenso.asia.sps.business.dao.FileUploadDao;;

/**
 * <p>The Class FileUploadServiceImpl.</p>
 * <p>Service for File Upload about search file data.</p>
 * <ul>
 * <li>Method search  : searchFileUploadInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class FileUploadServiceImpl implements FileUploadService {
    
    /** The file upload service dao. */
    private FileUploadDao fileUploadDao;

    /**
     * Instantiates a new misc service impl.
     */
    public FileUploadServiceImpl(){
        super();
    }

    /**
     * <p>Setter method for fileUploadDao.</p>
     *
     * @param fileUploadDao Set for fileUploadDao
     */
    public void setFileUploadDao(FileUploadDao fileUploadDao) {
        this.fileUploadDao = fileUploadDao;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.FileUploadService#searchFileUploadInformation
     * (com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    
    public List<SpsTFileUploadDomain> searchFileUploadInformation(
        FileUploadDomain fileUploadDomain) {
        return this.fileUploadDao.searchFileUploadInformation(fileUploadDomain);
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.FileUploadService#searchCount
     * (com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public Integer searchCount(FileUploadDomain fileUploadDomain) {
        return this.fileUploadDao.searchCount(fileUploadDomain);
    }
    
}  
    