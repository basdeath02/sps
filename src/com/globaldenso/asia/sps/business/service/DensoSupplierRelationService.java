/*
 * ModifyDate Development company    Describe 
 * 2014/06/12 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;

/**
 * <p>The Interface DensoSupplierRelationService.</p>
 * <p>Service for DENSO and Supplier company relation.</p>
 * <ul>
 * <li>Method search  : searchDENSOSupplierRelation</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoSupplierRelationService {
    
    /**
     * Search DENSO company and Supplier company relation.
     * @param dataScopeControlDomain data scope control
     * @return list of DENSO Supplier Relation
     * */
    public List<DensoSupplierRelationDomain> searchDensoSupplierRelation(
        DataScopeControlDomain dataScopeControlDomain);
}
