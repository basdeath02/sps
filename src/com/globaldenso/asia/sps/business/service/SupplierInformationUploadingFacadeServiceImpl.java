/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 2014/08/22 CSI Phakaporn           Modified method
 * 2015/10/14 CSI Akat                [IN026]
 * 2015/12/24 CSI Akat                [IN043]
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/03/17 CSI Akat                [IN068]
 * 2016/04/07 CSI Akat                [IN068]
 * 2019/31/01 CTC Pawan               [IN1773] User could upload Part which Supplier Plant Code not related Supplier company.
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService;
import com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain;
import com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain;
import com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * <p>The Interface SupplierUserUploadingFacadeService.</p>
 * <p>Service for Supplier Information Uploading about manipulate data form CSV file.</p>
 * <ul>
 * <li>Method search : initial</li>
 * <li>Method search : searchUploadErrorList</li>
 * <li>Method insert : transactUploadSupplierUser</li>
 * <li>Method insert : transactRegisterUploadItem</li>
 * <li>Method search : searchSupplierInformationCSV</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class SupplierInformationUploadingFacadeServiceImpl implements 
             SupplierInformationUploadingFacadeService {
    
    /** The DENSO Supplier Relation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The Temporary Upload Error service.*/
    private SpsTmpUploadErrorService spsTmpUploadErrorService;
    
    /** The Plant Supplier service.*/
    private SpsMPlantSupplierService spsMPlantSupplierService;
    
    /** The Plant Supplier service.*/
    private PlantSupplierService  plantSupplierService;
    
    /** The Record Limit Service. */
    private RecordLimitService recordLimitService;
    
    /** The common service. */
    private CommonService commonService;
    
    /** The Temporary Upload Error service. */
    private TmpUploadErrorService tmpUploadErrorService;
    
    /** The Temporary Supplier Info service. */
    private SpsTmpSupplierInfoService spsTmpSupplierInfoService;
    
    /** The Plant DENSO service. */
    private SpsMPlantDensoService spsMPlantDensoService;
    
    /** The Company DENSO service. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The DENSO Supplier Relation service. */
    private SpsMDensoSupplierRelationService spsMDensoSupplierRelationService;
    
    /** The DENSO Supplier Part service.*/
    private SpsMDensoSupplierPartsService spsMDensoSupplierPartsService;
    
    /** The Temporary Supplier Info service.*/
    private TmpSupplierInfoService tmpSupplierInfoService;
    
    /** The Temporary Supplier Info service.*/
    private DensoSupplierPartService densoSupplierPartService;
    
    /** The As400 vendor service.*/
    private SpsMAs400VendorService spsMAs400VendorService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    // Start : [IN026] for check D/O delivery date
    /** The service for SPS_T_DO. */
    private SpsTDoService spsTDoService;
    
    /** The service for SPS_T_DO_DETAIL. */
    private SpsTDoDetailService spsTDoDetailService;
    // End : [IN026] for check D/O delivery date
    
    // [IN043] for check P/O end firm date
    /** The service for SPS_T_PO. */
    private SpsTPoService spsTPoService;
    
    // [IN055] Check effected parts before delete Supplier Information
    /** The service for Purchase Order. */
    private PurchaseOrderService purchaseOrderService;
    
    // [IN068] For get minimum issue date from CIGMA ERROR table
    /** CIGMA P/O Error Service */
    private CigmaPoErrorService cigmaPoErrorService;
    
    /**
     * Instantiates a new Supplier User Uploading Facade service impl.
     */
    public SupplierInformationUploadingFacadeServiceImpl(){
        super();
    }
    
    /**
     * Sets the DENSO Supplier Relation Service.
     * 
     * @param densoSupplierRelationService the new DENSO Supplier Relation Service.
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
 
    /**
     * Sets the Temp Upload Error Service.
     * 
     * @param tmpUploadErrorService the Temporary Upload Error Service.
     */ 
    public void setTmpUploadErrorService(
        TmpUploadErrorService tmpUploadErrorService) {
        this.tmpUploadErrorService = tmpUploadErrorService;
    }

    /**
     * Sets the Temporary Upload Error Service.
     * 
     * @param spsTmpUploadErrorService the Temporary Upload Error Service.
     */ 
    public void setSpsTmpUploadErrorService(
        SpsTmpUploadErrorService spsTmpUploadErrorService) {
        this.spsTmpUploadErrorService = spsTmpUploadErrorService;
    }

    /**
     * Set the common service.
     * 
     * @param commonService the common service.
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }
    /**
     * Set the plant supplier service.
     * 
     * @param spsMPlantSupplierService the plant supplier service.
     */
    public void setSpsMPlantSupplierService(
        SpsMPlantSupplierService spsMPlantSupplierService) {
        this.spsMPlantSupplierService = spsMPlantSupplierService;
    }
    /**
     * Set the plant supplier service.
     * 
     * @param plantSupplierService the plant supplier service.
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    /**
     * Sets the record limit service.
     * 
     * @param recordLimitService the new record limit service.
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }

    /**
     * Sets the supplier Information service.
     * 
     * @param spsTmpSupplierInfoService the new supplier Information service.
     */
    public void setSpsTmpSupplierInfoService(SpsTmpSupplierInfoService spsTmpSupplierInfoService) {
        this.spsTmpSupplierInfoService = spsTmpSupplierInfoService;
    }
    
    /**
     * Set the plant DENSO service.
     * 
     * @param spsMPlantDensoService the plant DENSO service.
     */
    public void setSpsMPlantDensoService(SpsMPlantDensoService spsMPlantDensoService) {
        this.spsMPlantDensoService = spsMPlantDensoService;
    }
    
    /**
     * Sets the Company DENSO service.
     * 
     * @param spsMCompanyDensoService the new Company DENSO service.
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    /**
     * Sets the DENSO Supplier Relation service.
     * 
     * @param spsMDensosupplierRelateService the new DENSO Supplier Relation service.
     */  
    public void setSpsMDensoSupplierRelationService(
        SpsMDensoSupplierRelationService spsMDensosupplierRelateService) {
        this.spsMDensoSupplierRelationService = spsMDensosupplierRelateService;
    }

    /**
     * Sets the DENSO Supplier Part service.
     * 
     * @param spsMDensoSupplierPartService the new Company DENSO service.
     */
    public void setSpsMDensoSupplierPartsService(
        SpsMDensoSupplierPartsService spsMDensoSupplierPartService) {
        this.spsMDensoSupplierPartsService = spsMDensoSupplierPartService;
    }
    /**
     * Sets the Temporary Supplier Info service.
     * 
     * @param tmpSupplierInfoService the new Temporary Supplier Info service.
     */
    public void setTmpSupplierInfoService(
        TmpSupplierInfoService tmpSupplierInfoService) {
        this.tmpSupplierInfoService = tmpSupplierInfoService;
    }
    /**
     * Sets the DENSO Supplier Part service.
     * 
     * @param densoSupplierPartService the new DENSO Supplier Part service.
     */
    public void setDensoSupplierPartService(
        DensoSupplierPartService densoSupplierPartService) {
        this.densoSupplierPartService = densoSupplierPartService;
    }
    /**
     * Sets the As400 vendor service.
     * 
     * @param spsMAs400VendorService the new As400 vendor service.
     */
    public void setSpsMAs400VendorService(
        SpsMAs400VendorService spsMAs400VendorService) {
        this.spsMAs400VendorService = spsMAs400VendorService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    // Start : [IN026] for check D/O delivery date
    /**
     * <p>Setter method for spsTDoService.</p>
     *
     * @param spsTDoService Set for spsTDoService
     */
    public void setSpsTDoService(SpsTDoService spsTDoService) {
        this.spsTDoService = spsTDoService;
    }

    /**
     * <p>Setter method for spsTDoDetailService.</p>
     *
     * @param spsTDoDetailService Set for spsTDoDetailService
     */
    public void setSpsTDoDetailService(SpsTDoDetailService spsTDoDetailService) {
        this.spsTDoDetailService = spsTDoDetailService;
    }
    // End : [IN026] for check D/O delivery date

    // Start : [IN043] for check P/O end firm date
    /**
     * <p>Setter method for spsTPoService.</p>
     *
     * @param spsTPoService Set for spsTPoService
     */
    public void setSpsTPoService(SpsTPoService spsTPoService) {
        this.spsTPoService = spsTPoService;
    }
    // End : [IN043] for check P/O end firm date
    

    // Start : [IN055] Check effected parts before delete Supplier Information
    /**
     * <p>Setter method for purchaseOrderService.</p>
     *
     * @param purchaseOrderService Set for purchaseOrderService
     */
    public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }
    // End : [IN055] Check effected parts before delete Supplier Information

    // Start : [IN068] For get minimum issue date from CIGMA ERROR table
    /**
     * <p>Setter method for cigmaPoErrorService.</p>
     *
     * @param cigmaPoErrorService Set for cigmaPoErrorService
     */
    public void setCigmaPoErrorService(CigmaPoErrorService cigmaPoErrorService) {
        this.cigmaPoErrorService = cigmaPoErrorService;
    }
    // End : [IN068] For get minimum issue date from CIGMA ERROR table

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeService#searchInitial
     * (com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain)
     */
    public void transactInitial(SupplierInfoUploadDomain supplierInfoUploadDomain) 
        throws ApplicationException {
        String userDscId = supplierInfoUploadDomain.getUserDscId();
        String sessionCode = supplierInfoUploadDomain.getSessionCode();
        /*Call method deleteUploadTempTable for clear all data in temporary table*/
        deleteUploadTempTable(userDscId, sessionCode);
       
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeService#transactUploadSupplierInformation
     * (com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain)
     */
    public SupplierInfoUploadDomain transactUploadSupplierInformation(SupplierInfoUploadDomain 
        supplierInfoUploadDomain) throws ApplicationException, IOException {
        DataScopeControlDomain dataScopeControlDomain = supplierInfoUploadDomain
            .getDataScopeControlDomain();
        FileManagementDomain fileManagementDomain = supplierInfoUploadDomain
            .getFileManagementDomain();
        Locale locale = dataScopeControlDomain.getLocale();
        boolean isValidateDensoCompanyPass  =  true;
        boolean isValidateDensoPlantPass  =  true;
        boolean isValidateSupplierPlantPass  =  false;
        boolean isValidateDensoPartPass = false;
        boolean isValidateSupplierPartPass = false;
        boolean isValidateError = false;
        String line = new String();
        String companySupplierCode = new String();
        String plantSupplierCode = new String();
        String companyDensoCode = new String();
        String plantDensoCode = new String();
        String densoPartNumber = new String();
        String effectDate = new String(); 
        String companySupplierOwner = supplierInfoUploadDomain.getCompanySupplierOwner();
        int lineNo = Constants.ZERO;
        String errorCode = new String();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferReader = null;
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        
        try {
            
            /*Get each record and validate the content */
            inputStream = fileManagementDomain.getFileData();
            inputStream = FileUtil.checkUtf8BOMAndSkip(inputStream);
            inputStreamReader = new InputStreamReader(inputStream);
            bufferReader = new BufferedReader(inputStreamReader);
            
            /*LOOP each CSV record until end of file*/
            while(!StringUtil.checkNullOrEmpty(line = bufferReader.readLine())){
                lineNo++;
                isValidateSupplierPlantPass  =  false;
                isValidateDensoPartPass = false;
                isValidateSupplierPartPass = false;
                isValidateError = false;
                List<String> splitItem = StringUtil.splitCsvData(line, Constants.EIGHT);
                splitItem.set(Constants.ZERO,
                    splitItem.get(Constants.ZERO).toUpperCase()); //DENSO Code
                splitItem.set(Constants.ONE,
                    splitItem.get(Constants.ONE).toUpperCase()); //DENSO Plant Code
                splitItem.set(Constants.TWO,
                    splitItem.get(Constants.TWO).toUpperCase()); //DENSO Part No.
                splitItem.set(Constants.THREE,
                    splitItem.get(Constants.THREE).toUpperCase()); //Supplier Part No.
                splitItem.set(Constants.FOUR,
                    splitItem.get(Constants.FOUR).toUpperCase()); //Vendor Code
                splitItem.set(Constants.FIVE,
                    splitItem.get(Constants.FIVE).toUpperCase()); //Supplier Plant Code
                splitItem.set(Constants.SIX,
                    splitItem.get(Constants.SIX).toUpperCase()); //Effective Date
                splitItem.set(Constants.SEVEN,
                    splitItem.get(Constants.SEVEN).toUpperCase()); //Flag
                
                /**** Check number of record elements*/
                if(Constants.EIGHT != splitItem.size()){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR001;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                    isValidateError = true;
                    continue;
                }
                /**** Check NULL on DENSO company code*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                    /**** Check data length on DENSO company code*/
                    if(Constants.FIVE < splitItem.get(Constants.ZERO).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR060;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError =  true;
                    }else{
                        splitItem.set(Constants.ZERO,
                            StringUtil.rightPad(splitItem.get(Constants.ZERO), Constants.FIVE));
                        
                        /**** Check existing DENSO company in system*/
                        companyDensoCode = splitItem.get(Constants.ZERO);
                        boolean isExistDensoCompany = searchExistDensoCompany(companyDensoCode);
                        if(!isExistDensoCompany){
                            /**** Check existing DENSO company not found in the master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR061;
                            createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                            isValidateError = true;
                        }
                    }
                }else{
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR059;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                    isValidateError = true;
                }

                /** Check NULL on DENSO plant code*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ONE))
                    && isValidateDensoCompanyPass){
                    /** Check data length on DENSO plant code*/
                    if(Constants.TWO < splitItem.get(Constants.ONE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR064;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError = true;
                    }else{
                        splitItem.set(Constants.ONE,
                            StringUtil.rightPad(splitItem.get(Constants.ONE), Constants.TWO));
                        
                        /**Check existing DENSO plant in system*/
                        companyDensoCode = splitItem.get(Constants.ZERO);
                        plantDensoCode = splitItem.get(Constants.ONE);
                        boolean isExistDensoPlant = searchExistDensoPlant(
                            companyDensoCode, plantDensoCode);
                        if(!isExistDensoPlant){
                            /**** Check existing DENSO plant not found in the master data*/
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR065;
                            createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                            isValidateError = true;
                        }
                    }
                }else{
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR063;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                    isValidateError = true;
                }
                
                /**** Check NULL on Vendor code*/
                if(isValidateDensoCompanyPass){
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.FOUR))){
                        /** Check data length on Vendor code*/
                        if(Constants.SIX < splitItem.get(Constants.FOUR).length()){
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR094;
                            createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                            isValidateError = true;
                        }else{
                            if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.ZERO))){
                                /**Check existing Vendor code in master data*/
                                String companyDensoCd = splitItem.get(Constants.ZERO);
                                String vendorCd = splitItem.get(Constants.FOUR);
                                boolean isExistVendorCd = searchExistVendorCd(
                                    companyDensoCd, vendorCd);
                                if(!isExistVendorCd){
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR095;
                                    createItemUploadError(supplierInfoUploadDomain, errorCode,
                                        lineNo);
                                    isValidateError = true;
                                }else{
                                    /**Check existing vendor code in system by current user data scope*/
                                    boolean isExistVendorCdWithScope = 
                                        searchExistVendorCdWithScope(companySupplierOwner, 
                                            companyDensoCd, vendorCd);
                                    if(!isExistVendorCdWithScope){
                                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR096;
                                        createItemUploadError(supplierInfoUploadDomain,
                                            errorCode, lineNo);
                                        isValidateError = true;
                                    }else{
                                        /**** Check NULL on Supplier plant code*/
                                        if(!StringUtil.checkNullOrEmpty(
                                            splitItem.get(Constants.FIVE)))
                                        {
                                            /** Check data length on Supplier plant code*/
                                            if(Constants.ONE
                                                < splitItem.get(Constants.FIVE).length()){
                                                errorCode = SupplierPortalConstant
                                                    .UPLOAD_ERROR_CODE_ERR015;
                                                createItemUploadError(supplierInfoUploadDomain, 
                                                    errorCode, lineNo);
                                                isValidateError = true;
                                            }else{
//                                    Start     [IN1773] User could upload Part which Supplier Plant Code not related Supplier company. 
                                                /**Check existing Supplier plant in system*/
                                                companySupplierCode = companySupplierOwner;
                                                plantSupplierCode = splitItem.get(Constants.FIVE);
                                                boolean isExistSupplierPlant = 
                                                    searchExistSupplierPlant(
                                                        companySupplierCode, plantSupplierCode);
                                                if(isExistSupplierPlant){
                                                    /**Check existing Supplier plant in system by current user data scope*/
                                                    companySupplierCode = companySupplierOwner;
                                                    plantSupplierCode
                                                        = splitItem.get(Constants.FIVE);
                                                    boolean isExistSupplierPlantByScope = 
                                                        searchExistSupplierPlantWithScope(
                                                            companySupplierCode, 
                                                            plantSupplierCode,
                                                            dataScopeControlDomain);
                                                    if(!isExistSupplierPlantByScope){
                                                        /**Current user not have a right to working on this Supplier plant.*/
                                                        errorCode = SupplierPortalConstant
                                                            .UPLOAD_ERROR_CODE_ERR017;
                                                        createItemUploadError(
                                                            supplierInfoUploadDomain, errorCode, 
                                                            lineNo);
                                                        isValidateError = true;
                                                    }else{
                                                        isValidateSupplierPlantPass  =  true;
                                                    }
//                                      End      [IN1773] User could upload Part which Supplier Plant Code not related Supplier company.      
                                                }else{
                                                    /**** Check existing supplier plant not found in the master data*/
                                                    errorCode = SupplierPortalConstant
                                                        .UPLOAD_ERROR_CODE_ERR016;
                                                    createItemUploadError(
                                                        supplierInfoUploadDomain, 
                                                        errorCode, lineNo);
                                                    isValidateError = true;
                                                }
                                            }
                                        }else{
                                            errorCode = SupplierPortalConstant
                                                .UPLOAD_ERROR_CODE_ERR014;
                                            createItemUploadError(supplierInfoUploadDomain, 
                                                errorCode, lineNo);
                                            isValidateError = true;
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        /** Vendor code is null.*/
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR093;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError = true;
                    }
                }

                /**** Check effective date Null value*/
                if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR047;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                }else{
                    /**** Check format effective date*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX)) 
                        && !DateUtil.isValidDate(splitItem.get(Constants.SIX), pattern)){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR048;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError = true;
                    }
                }

                /**** Check DENSO part number*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.TWO))){
                    /**** Check DENSO part length*/
                    if(Constants.FIFTEEN < splitItem.get(Constants.TWO).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR086;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError = true;
                    }else{
                        isValidateDensoPartPass = true;
                    }
                }else{
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR085;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                    isValidateError = true;
                }
                /**** Check Supplier part number*/
                if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.THREE))){
                    /**** Check Supplier part length*/
                    if(Constants.TWENTY < splitItem.get(Constants.THREE).length()){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR088;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError = true;
                    }else{
                        isValidateSupplierPartPass = true;
                    }
                }else{
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR087;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                    isValidateError = true;
                }
                
                /**** Check flag data range*/
                if(!Constants.STR_A.equals(splitItem.get(Constants.SEVEN))
                    && !Constants.STR_U.equals(splitItem.get(Constants.SEVEN))
                    && !Constants.STR_D.equals(splitItem.get(Constants.SEVEN))){
                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR090;
                    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                    isValidateError = true;
                }else{
                    /**** Check all validation */
                    if(isValidateDensoCompanyPass && !isValidateError
                        && isValidateDensoPlantPass && isValidateSupplierPlantPass
                        && isValidateDensoPartPass && isValidateSupplierPartPass){
                        /**** Check condition by flag value*/
                        if(Constants.STR_A.equals(splitItem.get(Constants.SEVEN))){
                            /**** Check existing part in master data*/
                            companyDensoCode     =     splitItem.get(Constants.ZERO);
                            plantDensoCode       =     splitItem.get(Constants.ONE);
                            companySupplierCode  =     companySupplierOwner;
                            densoPartNumber      =     splitItem.get(Constants.TWO);
                            effectDate           =     splitItem.get(Constants.SIX);
                            boolean isExistPart  = this.searchExistPartNumber(companyDensoCode,
                                plantDensoCode, companySupplierCode,
                                densoPartNumber, effectDate);
                            if(isExistPart){
                                /**** Duplicate part number*/
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR091;
                                createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                                isValidateError = true;
                            }
                        }//EndIf:Flag = "A"
                        
                        if(Constants.STR_U.equals(splitItem.get(Constants.SEVEN))){
                            /**** Check existing part in master data*/
                            companyDensoCode     =     splitItem.get(Constants.ZERO);
                            plantDensoCode       =     splitItem.get(Constants.ONE);
                            companySupplierCode  =     companySupplierOwner;
                            densoPartNumber      =     splitItem.get(Constants.TWO);
                            effectDate           =     splitItem.get(Constants.SIX);
                            
                            boolean isExistPart  = this.searchExistPartNumber(companyDensoCode,
                                plantDensoCode, companySupplierCode,
                                densoPartNumber, effectDate);
                            // [IN055] Flag U is now for update Supplier Information record
                            if(!isExistPart){
                                /**** Part number not found for update*/
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR092;
                                createItemUploadError(supplierInfoUploadDomain, errorCode, 
                                    lineNo);
                                isValidateError = true;
                            }
                            // [IN055] Flag U is now for update Supplier Information record
                            //if(isExistPart){
                            //    /**** Duplicate part number*/
                            //    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR091;
                            //    createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                            //    isValidateError = true;
                            //}
                        }//EndIf:Flag = "U"
                        
                        if(Constants.STR_D.equals(splitItem.get(Constants.SEVEN))){
                            /**** Check existing part in master data*/
                            companyDensoCode     =     splitItem.get(Constants.ZERO);
                            plantDensoCode       =     splitItem.get(Constants.ONE);
                            companySupplierCode  =     companySupplierOwner;
                            densoPartNumber      =     splitItem.get(Constants.TWO);
                            effectDate           =     splitItem.get(Constants.SIX);
                            boolean isExistPart  = this.searchExistPartNumber(companyDensoCode,
                                plantDensoCode, companySupplierCode,
                                densoPartNumber, effectDate);
                            if(!isExistPart){
                                /**** Part number not found for update*/
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR092;
                                createItemUploadError(supplierInfoUploadDomain, errorCode,
                                    lineNo);
                                isValidateError = true;
                            }
                        }//EndIf:Flag = "D"
                        
                        // Start : [IN026] Validate Effective date with current Order
                        if(Constants.STR_A.equals(splitItem.get(Constants.SEVEN))
                            || Constants.STR_U.equals(splitItem.get(Constants.SEVEN)))
                        {
                            companyDensoCode     =     splitItem.get(Constants.ZERO);
                            plantDensoCode       =     splitItem.get(Constants.ONE);
                            companySupplierCode  =     companySupplierOwner;
                            densoPartNumber      =     splitItem.get(Constants.TWO);
                            effectDate           =     splitItem.get(Constants.SIX);
                            Timestamp effectDateTimestamp = null;
                            
                            // Start : [IN043] Check is supplier type Import P/O
                            String vendorCd = splitItem.get(Constants.FOUR);
                            SpsTPoCriteriaDomain poCritreia = new SpsTPoCriteriaDomain();
                            poCritreia.setDCd(companyDensoCode);
                            poCritreia.setVendorCd(vendorCd);
                            List<SpsTPoDomain> poList
                                = this.spsTPoService.searchByCondition(poCritreia);
                            String poType = Constants.EMPTY_STRING;
                            if (Constants.ZERO != poList.size()) {
                                SpsTPoDomain firstPo = poList.get(Constants.ZERO);
                                poType = firstPo.getPoType();
                                if (Constants.IPO_TYPE.equals(poType)
                                    || Constants.IKO_TYPE.equals(poType))
                                {
                                    // Import P/O check by P/O's End Firm Date
                                    java.sql.Date effectDateSql = DateUtil.parseToSqlDate(effectDate
                                        , DateUtil.PATTERN_YYYYMMDD_SLASH);
                                    
                                    PurchaseOrderDomain poDomain = new PurchaseOrderDomain();
                                    poDomain.setPoHeader(new SpsTPoDomain());
                                    poDomain.setPoDetail(new SpsTPoDetailDomain());
                                    poDomain.getPoHeader().setDCd(companyDensoCode);
                                    poDomain.getPoHeader().setSCd(companySupplierCode);
                                    poDomain.getPoHeader().setDPcd(plantDensoCode);
                                    poDomain.getPoDetail().setDPn(densoPartNumber);
                                    poDomain.getPoDetail().setEndFirmDate(effectDateSql);
                                    
                                    int effectPartPoCount = this.purchaseOrderService
                                        .searchCountEffectParts(poDomain);
                                        
                                    if(Constants.ZERO != effectPartPoCount){
                                        // [IN055] Change error message when flag U
                                        //errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR103;
                                        if (Constants.STR_A.equals(splitItem.get(Constants.SEVEN)))
                                        {
                                            /**** Effective Overlap with P/O end firm date. */
                                            errorCode 
                                                = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR103;
                                        } else {
                                            /**** Supplier Information currently in use. */
                                            errorCode 
                                                = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR105;
                                        }
                                        
                                        createItemUploadError(supplierInfoUploadDomain, errorCode
                                            , lineNo);
                                        isValidateError = true;
                                    }
                                }
                            }
                            /*[SPS004] This case impact for result of fixing because it have statement to find Part No. on D/O only.
                             * We concern if we comment all it will be impact for another case.
                             * We change the == to != only.
                            if (Constants.ZERO == poList.size() || Constants.DPO_TYPE.equals(poType)
                                || Constants.DKO_TYPE.equals(poType))
                            {*/
                            if (Constants.ZERO != poList.size() || Constants.DPO_TYPE.equals(poType)
                                || Constants.DKO_TYPE.equals(poType))
                            {
                                // Domestic P/O check by D/O's Delivery Date
                                if(!StringUtil.checkNullOrEmpty(effectDate)){
                                    Date effectDateUtil = DateUtil.parseToUtilDate(
                                        effectDate, DateUtil.PATTERN_YYYYMMDD_SLASH);
                                    effectDateTimestamp = new Timestamp(effectDateUtil.getTime());
                                }
                                
                                SpsTDoCriteriaDomain doCriteria = new SpsTDoCriteriaDomain();
                                doCriteria.setDCd(companyDensoCode);
                                doCriteria.setDPcd(plantDensoCode);
                                doCriteria.setSCd(companySupplierCode);
                                doCriteria.setDeliveryDatetimeGreaterThanEqual(effectDateTimestamp);
                                List<SpsTDoDomain> effectDoList
                                    = this.spsTDoService.searchByCondition(doCriteria);
                                int effectPartCount = Constants.ZERO;
                                SpsTDoDetailCriteriaDomain doDetailCriteria = null;
                                for (SpsTDoDomain effectDo : effectDoList) {
                                    doDetailCriteria = new SpsTDoDetailCriteriaDomain();
                                    doDetailCriteria.setDoId(effectDo.getDoId());
                                    doDetailCriteria.setDPn(densoPartNumber);
                                    effectPartCount += 
                                        this.spsTDoDetailService.searchCount(doDetailCriteria);
                                }
                                
                                if(Constants.ZERO != effectPartCount){
                                    // [IN055] Change error message when flag U
                                    //errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR102;
                                    if (Constants.STR_A.equals(splitItem.get(Constants.SEVEN)))
                                    {
                                        /**** Effective Overlap with P/O end firm date. */
                                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR102;
                                    } else {
                                        /**** Supplier Information currently in use. */
                                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR105;
                                    }
                                    
                                    createItemUploadError(supplierInfoUploadDomain, errorCode,
                                        lineNo);
                                    isValidateError = true;
                                }
                            }
                            // End : [IN043] Check is supplier type Import P/O
                        }
                        // End : [IN026] Validate Effective date with current Order
                        
                        // Start : [IN055] Validate delete record
                        if (Constants.STR_D.equals(splitItem.get(Constants.SEVEN))) {
                            Timestamp current = this.commonService.searchSysDate();
                            Calendar today = Calendar.getInstance();
                            today.setTimeInMillis(current.getTime());
                            today.set(Calendar.DATE, Constants.ONE);
                            java.sql.Date firstDayOfMonth = new java.sql.Date(
                                today.getTimeInMillis());
                            
                            PurchaseOrderDomain poDomain = new PurchaseOrderDomain();
                            poDomain.setPoHeader(new SpsTPoDomain());
                            poDomain.setPoDetail(new SpsTPoDetailDomain());
                            poDomain.getPoHeader().setDCd(companyDensoCode);
                            poDomain.getPoHeader().setSCd(companySupplierCode);
                            poDomain.getPoHeader().setDPcd(plantDensoCode);
                            poDomain.getPoDetail().setDPn(densoPartNumber);
                            poDomain.getPoDetail().setEndPeriodDate(firstDayOfMonth);
                            
                            int effectPartPoCount = this.purchaseOrderService
                                .searchCountEffectParts(poDomain);
                                
                            if(Constants.ZERO != effectPartPoCount){
                                /**** Effective Overlap with P/O end firm date. */
                                errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR104;
                                createItemUploadError(supplierInfoUploadDomain, errorCode,
                                    lineNo);
                                isValidateError = true;
                            }
                        }
                        // End : [IN055] Validate delete record
                    }
                }
                
                // Start : [IN068] Check Part No. with CIGMA ERROR table
                if(isValidateDensoCompanyPass && !isValidateError
                    && isValidateDensoPlantPass && isValidateSupplierPlantPass
                    && isValidateDensoPartPass && isValidateSupplierPartPass
                    && Constants.STR_A.equals(splitItem.get(Constants.SEVEN)))
                {
                    companyDensoCode     =     splitItem.get(Constants.ZERO);
                    plantDensoCode       =     splitItem.get(Constants.ONE);
                    companySupplierCode  =     companySupplierOwner;
                    densoPartNumber      =     splitItem.get(Constants.TWO);
                    
                    // Check is this Parts No. already register to SPS
                    SpsMDensoSupplierPartsCriteriaDomain densoSupplierPartCriteria = 
                        new SpsMDensoSupplierPartsCriteriaDomain();
                    densoSupplierPartCriteria.setDCd(companyDensoCode);
                    densoSupplierPartCriteria.setDPcd(plantDensoCode);
                    densoSupplierPartCriteria.setSCd(companySupplierCode);
                    densoSupplierPartCriteria.setDPn(densoPartNumber);
                    int countParts 
                        = this.spsMDensoSupplierPartsService.searchCount(densoSupplierPartCriteria);
                    if (Constants.ZERO == countParts) {
                        effectDate = splitItem.get(Constants.SIX);
                        if(!Constants.STR_FIRST_DATE.equals(effectDate.substring(Constants.EIGHT)))
                        {
                            /* Effective Date is not first date of month */
                            errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR106;
                            createItemUploadError(supplierInfoUploadDomain, errorCode,
                                lineNo);
                            isValidateError = true;
                        } else {
                            SpsCigmaPoErrorDomain minIssueCriteria = new SpsCigmaPoErrorDomain();
                            minIssueCriteria.setDCd(companyDensoCode);
                            minIssueCriteria.setSCd(companySupplierCode);
                            minIssueCriteria.setDPcd(plantDensoCode);
                            minIssueCriteria.setDPn(densoPartNumber);
                            Date minIssueDate = this.cigmaPoErrorService.searchMinIssueDate(
                                minIssueCriteria);
                            
                            java.sql.Date effectDateSql = DateUtil.parseToSqlDate(effectDate
                                , DateUtil.PATTERN_YYYYMMDD_SLASH);
                            
                            if (null != minIssueDate) {
                                Calendar deliveryCal = Calendar.getInstance();
                                deliveryCal.setTimeInMillis(minIssueDate.getTime());
                                deliveryCal.set(Calendar.DATE, Constants.ONE);
                                java.sql.Date firstDayOfMonth
                                    = new java.sql.Date(deliveryCal.getTimeInMillis());

                                /* If effective date (Inputed by user) > 1st date of month of
                                 * minimum issue date
                                 * */
                                if (Constants.ZERO < effectDateSql.compareTo(firstDayOfMonth)) {
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR107;
                                    createItemUploadError(supplierInfoUploadDomain, errorCode,
                                        lineNo);
                                    isValidateError = true;
                                }
                            } else {
                                SpsMCompanyDensoCriteriaDomain companyDensoCriteria
                                    = new SpsMCompanyDensoCriteriaDomain();
                                companyDensoCriteria.setDCd(companyDensoCode);
                                SpsMCompanyDensoDomain denso
                                    = this.spsMCompanyDensoService.searchByKey(
                                        companyDensoCriteria);
                                
                                String densoUtc = denso.getUtc().replaceAll(
                                    Constants.REGX_PLUS, Constants.EMPTY_STRING).trim();
                                String[] separateUtc = densoUtc.split(Constants.STR_COLON);
                                
                                double densoCompanyGmtOffset
                                    = Double.parseDouble(separateUtc[Constants.ZERO]);
                                int hoursOffset = (int)(densoCompanyGmtOffset * Constants.ONE);
                                int minOffset = 0;
                                if (Constants.ONE < separateUtc.length) {
                                    minOffset = Integer.parseInt(separateUtc[Constants.ONE]);
                                } else {
                                    double minOffsetDouble = densoCompanyGmtOffset - hoursOffset;
                                    minOffset = (int)(minOffsetDouble * Constants.ONE_HUNDRED);
                                }
                                
                                Calendar currentLocalTime = Calendar.getInstance();
                                TimeZone tz = currentLocalTime.getTimeZone();
                                tz.getOffset(System.currentTimeMillis());
                                int apServerMillisecOffset
                                    = tz.getOffset(System.currentTimeMillis());
                                currentLocalTime.setTimeInMillis(
                                    System.currentTimeMillis() - apServerMillisecOffset);
                                currentLocalTime.add(Calendar.HOUR_OF_DAY, hoursOffset);
                                currentLocalTime.add(Calendar.MINUTE, minOffset);
                                currentLocalTime.set(Calendar.DATE, Constants.ONE);
                                java.sql.Date firstDayOfMonth
                                    = new java.sql.Date(currentLocalTime.getTimeInMillis());

                                /* If effective date (Inputed by user) > 1st date of current local
                                 * month of DENSO company
                                 * */
                                if (Constants.ZERO < effectDateSql.compareTo(firstDayOfMonth)) {
                                    errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR108;
                                    // [IN068] Add new field for keep message parameter
                                    //createItemUploadError(supplierInfoUploadDomain, errorCode,
                                    //    lineNo);
                                    createItemUploadError(supplierInfoUploadDomain, errorCode,
                                        lineNo, DateUtil.format(firstDayOfMonth
                                            , DateUtil.PATTERN_YYYYMMDD_SLASH));
                                    
                                    isValidateError = true;
                                }
                            }
                        }
                    }
                }
                // End : [IN068] Check Part No. with CIGMA ERROR table
                
                //Check duplicate record in file
                if(!isValidateError){
                    SpsTmpSupplierInfoCriteriaDomain supplierInfoCriteria
                        = new SpsTmpSupplierInfoCriteriaDomain();
                    if(StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))){
                        supplierInfoCriteria.setEffectDate(null);
                    }else{
                        Date effectDateTmp = DateUtil.parseToUtilDate(splitItem.get(
                            Constants.SIX), pattern);
                        supplierInfoCriteria.setEffectDate(new Timestamp(effectDateTmp.getTime()));
                    }
                    supplierInfoCriteria.setDCd(splitItem.get(Constants.ZERO));
                    supplierInfoCriteria.setDPcd(splitItem.get(Constants.ONE));
                    supplierInfoCriteria.setSCd(companySupplierOwner);
                    supplierInfoCriteria.setSPcd(splitItem.get(Constants.FIVE));
                    supplierInfoCriteria.setDPn(splitItem.get(Constants.TWO));
                    supplierInfoCriteria.setUserDscId(supplierInfoUploadDomain.getUserDscId());
                    supplierInfoCriteria.setSessionCd(supplierInfoUploadDomain.getSessionCode());
                    int recordCount = spsTmpSupplierInfoService.searchCount(supplierInfoCriteria);
                    if(Constants.ZERO < recordCount){
                        errorCode = SupplierPortalConstant.UPLOAD_ERROR_CODE_ERR097;
                        createItemUploadError(supplierInfoUploadDomain, errorCode, lineNo);
                        isValidateError = true;
                    }
                }
                
                /**** Does not has an error from validation process*/
                if(!isValidateError){
                    Timestamp effectDateTimestamp = null;
                    SpsMDensoSupplierRelationDomain densoSuppRelation =
                        new SpsMDensoSupplierRelationDomain();
                    SpsMDensoSupplierPartsDomain densoSupplierPart =
                        new SpsMDensoSupplierPartsDomain();
                    /*set date format if effect date is not null*/
                    if(!StringUtil.checkNullOrEmpty(splitItem.get(Constants.SIX))){
                        Date effectDateUtil = DateUtil.parseToUtilDate(splitItem.get(
                            Constants.SIX), pattern);
                        effectDateTimestamp = new Timestamp(effectDateUtil.getTime());
                    }

//                    if(Constants.STR_U.equals(splitItem.get(Constants.SEVEN))
//                        || Constants.STR_D.equals(splitItem.get(Constants.SEVEN))){
                    if(Constants.STR_D.equals(splitItem.get(Constants.SEVEN))) {
                        /*Get DENSO and Supplier relation last update date time*/
                        SpsMDensoSupplierRelationCriteriaDomain densoSupplierRelateCriteria =
                            new SpsMDensoSupplierRelationCriteriaDomain();
                        densoSupplierRelateCriteria.setDCd(splitItem.get(Constants.ZERO));
                        densoSupplierRelateCriteria.setDPcd(splitItem.get(Constants.ONE));
                        densoSupplierRelateCriteria.setSCd(companySupplierOwner);
                        densoSupplierRelateCriteria.setSPcd(splitItem.get(Constants.FIVE));
                        List<SpsMDensoSupplierRelationDomain> densoSuppRelationList = 
                            spsMDensoSupplierRelationService.searchByCondition(
                                densoSupplierRelateCriteria);
                        densoSuppRelation = densoSuppRelationList.get(Constants.ZERO);

                        /*Get DENSO and Supplier part last update date time*/
                        SpsMDensoSupplierPartsCriteriaDomain densoSupplierPartCriteria = 
                            new SpsMDensoSupplierPartsCriteriaDomain();
                        densoSupplierPartCriteria.setDCd(splitItem.get(Constants.ZERO));
                        densoSupplierPartCriteria.setDPcd(splitItem.get(Constants.ONE));
                        densoSupplierPartCriteria.setSCd(companySupplierOwner);
                        //densoSupplierPartCriteria.setSPcd(splitItem.get(Constants.FIVE));
                        densoSupplierPartCriteria.setDPn(splitItem.get(Constants.TWO));
                        densoSupplierPartCriteria.setEffectDate(effectDateTimestamp);
                        List<SpsMDensoSupplierPartsDomain> densoSupplierPartList =
                            spsMDensoSupplierPartsService.searchByCondition(
                                densoSupplierPartCriteria);
                        densoSupplierPart = densoSupplierPartList.get(Constants.ZERO);
                    }
                    /*Insert all record items to the temporary table*/
                    SpsTmpSupplierInfoDomain tmpSupplierInfoDomain = new 
                        SpsTmpSupplierInfoDomain();
                    tmpSupplierInfoDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
                    tmpSupplierInfoDomain.setSessionCd(supplierInfoUploadDomain.getSessionCode());
                    tmpSupplierInfoDomain.setLineNo(BigDecimal.valueOf(lineNo));
                    tmpSupplierInfoDomain.setDCd(splitItem.get(Constants.ZERO));
                    tmpSupplierInfoDomain.setDPcd(splitItem.get(Constants.ONE));
                    tmpSupplierInfoDomain.setVendorCd(splitItem.get(Constants.FOUR));
                    tmpSupplierInfoDomain.setSPcd(splitItem.get(Constants.FIVE));
                    tmpSupplierInfoDomain.setDPn(splitItem.get(Constants.TWO));
                    tmpSupplierInfoDomain.setSPn(splitItem.get(Constants.THREE));
                    tmpSupplierInfoDomain.setFlag(splitItem.get(Constants.SEVEN));
                    tmpSupplierInfoDomain.setEffectDate(effectDateTimestamp);
                    tmpSupplierInfoDomain.setSCd(companySupplierOwner);
                    tmpSupplierInfoDomain.setIsActualRegister(Constants.IS_NOT_ACTIVE);
                    tmpSupplierInfoDomain.setUploadDatetime(commonService.searchSysDate());
                    tmpSupplierInfoDomain.setRelationLastUpdate(densoSuppRelation
                        .getLastUpdateDatetime());
                    tmpSupplierInfoDomain.setPartLastUpdate(densoSupplierPart
                        .getLastUpdateDatetime());
                    spsTmpSupplierInfoService.create(tmpSupplierInfoDomain);

                }
            }/* End: LOOP*/
            
            /*Calculate total record*/
            int totalRecord = lineNo;
            int numberOfWarning = 0;
            int numberOfError = 0;
            int numberOfCorrect = 0;
            List<PlantSupplierDomain> plantSupplierList = new ArrayList<PlantSupplierDomain>();
            /*Check Existing data in TmpUploadUserSupplier table.*/
            SpsTmpSupplierInfoCriteriaDomain tmpSupplierInfoCriteriaDomain =
                new SpsTmpSupplierInfoCriteriaDomain();
            tmpSupplierInfoCriteriaDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
            tmpSupplierInfoCriteriaDomain.setSessionCd(supplierInfoUploadDomain.getSessionCode());
            TmpSupplierInfoDomain criteriaTmpSupplierInfo = new TmpSupplierInfoDomain();
            criteriaTmpSupplierInfo.setUserDscId(supplierInfoUploadDomain.getUserDscId());
            criteriaTmpSupplierInfo.setSessionCd(supplierInfoUploadDomain.getSessionCode());
            TmpSupplierInfoDomain resultTmpsSupplierInfo = new TmpSupplierInfoDomain();
            int countUserSupplier = spsTmpSupplierInfoService.searchCount(
                tmpSupplierInfoCriteriaDomain);
            if(Constants.ZERO < countUserSupplier){
                /*Get number of correct record*/
                numberOfCorrect = 
                    spsTmpSupplierInfoService.searchCount(tmpSupplierInfoCriteriaDomain);
                resultTmpsSupplierInfo = tmpSupplierInfoService
                    .searchTmpUploadSupplierInfoCompanyName(criteriaTmpSupplierInfo);
                if(!StringUtil.checkNullOrEmpty(resultTmpsSupplierInfo.getCompanyName())){
                    supplierInfoUploadDomain
                        .setCompanyName(resultTmpsSupplierInfo.getCompanyName());
                }
                plantSupplierList = tmpSupplierInfoService.searchTmpUploadSupplierInfoPlant(
                    criteriaTmpSupplierInfo);
                if(Constants.ZERO < plantSupplierList.size()){
                    supplierInfoUploadDomain.setPlantSupplierList(plantSupplierList);
                }
            }
            
            /*Check Existing data in TmpUploadError table.*/
            SpsTmpUploadErrorCriteriaDomain uploadErrorCriteriaDomain = new 
                SpsTmpUploadErrorCriteriaDomain();
            uploadErrorCriteriaDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
            uploadErrorCriteriaDomain.setSessionCd(supplierInfoUploadDomain.getSessionCode());
            int count = spsTmpUploadErrorService.searchCount(uploadErrorCriteriaDomain);
            if(Constants.ZERO < count){
                /*Get number of warning record*/
                GroupUploadErrorDomain groupUploadErrorDomain = new GroupUploadErrorDomain();
                groupUploadErrorDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(supplierInfoUploadDomain.getSessionCode());
                groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
                numberOfWarning = tmpUploadErrorService.searchCountWarning(groupUploadErrorDomain);
                
                /*Get number of incorrect record*/
                groupUploadErrorDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(supplierInfoUploadDomain.getSessionCode());
                groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
                numberOfError = tmpUploadErrorService.searchCountIncorrect(groupUploadErrorDomain);

                /*Calculate total record*/
                totalRecord = lineNo;
                
                int maxRecord = tmpUploadErrorService.searchCountGroupOfValidationError(
                    groupUploadErrorDomain);
                /* Get Maximum record limit*/
                MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
                recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
                recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD001_RLM);
                recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
                if(null == recordsLimit){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                /*Check max record*/
                if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[]{recordsLimit.getMiscValue()});
                }
                
                /* Get Maximum record per page limit*/
                MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
                recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
                recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD001_PLM);
                recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
                if(null == recordLimitPerPage){
                    MessageUtil.throwsApplicationMessage(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                /*Calculate row number for query*/
                supplierInfoUploadDomain.setPageNumber(supplierInfoUploadDomain.getPageNumber());
                supplierInfoUploadDomain.setMaxRowPerPage(Integer.valueOf(
                    recordLimitPerPage.getMiscValue()));
                SpsPagingUtil.calcPaging(supplierInfoUploadDomain, maxRecord);
                
                /*Get list of validation error and warning and group by code number*/
                List<GroupUploadErrorDomain> result = new ArrayList<GroupUploadErrorDomain>();
                groupUploadErrorDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
                groupUploadErrorDomain.setSessionId(supplierInfoUploadDomain.getSessionCode());
                groupUploadErrorDomain.setRowNumFrom(supplierInfoUploadDomain.getRowNumFrom());
                groupUploadErrorDomain.setRowNumTo(supplierInfoUploadDomain.getRowNumTo());
                result = tmpUploadErrorService.searchGroupOfValidationError(groupUploadErrorDomain);
                
                // Start : [IN068] If error message has parameter, set it to message
                for (GroupUploadErrorDomain error : result) {
                    if (Strings.judgeBlank(error.getMessageParam())) {
                        continue;
                    }
                    List<String> paramList = StringUtil.splitCsvData(
                        error.getMessageParam(), Constants.TEN);
                    String realMessage = error.getDescription();
                    
                    StringBuffer replacementBuffer = new StringBuffer();
                    for (int index = Constants.ZERO; index < paramList.size(); index++) {
                        replacementBuffer.setLength(Constants.ZERO);
                        if(Strings.judgeBlank(paramList.get(index))){
                            break;
                        }
                        replacementBuffer.append(Constants.SYMBOL_OPEN_BRACES).append(index)
                            .append(Constants.SYMBOL_CLOSE_BRACES);
                        realMessage = realMessage.replace(replacementBuffer.toString()
                            , paramList.get(index));
                    }
                    error.setDescription(realMessage);
                }
                // End : [IN068] If error message has parameter, set it to message
                
                supplierInfoUploadDomain.setUploadErrorDetailDomain(result);
            }
            
            /*Set data to domain*/
            supplierInfoUploadDomain.setTotalRecord(String.valueOf(totalRecord));
            supplierInfoUploadDomain.setCorrectRecord(String.valueOf(numberOfCorrect));
            supplierInfoUploadDomain.setInCorrectRecord(String.valueOf(numberOfError));
            supplierInfoUploadDomain.setWarningRecord(String.valueOf(numberOfWarning));

        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009, 
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }finally{
            if(null != inputStream){
                inputStream.close();
            }
            if(null != inputStreamReader){
                inputStreamReader.close();
            }
            if(null != bufferReader){
                bufferReader.close();
            }
        }
        return supplierInfoUploadDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeServiceImpl#transactRegisterSupplierInformation
     * (com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain)
     */
    public void transactRegisterSupplierInformation(SupplierInfoUploadDomain 
        supplierInfoUploadDomain)throws ApplicationException{
        Locale locale = supplierInfoUploadDomain.getLocale();
        
        /*Get record count in temporary table*/
        SpsTmpSupplierInfoCriteriaDomain tmpSupplierInfoCriteria = 
            new SpsTmpSupplierInfoCriteriaDomain();
        tmpSupplierInfoCriteria.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        tmpSupplierInfoCriteria.setSessionCd(supplierInfoUploadDomain.getSessionCode());
        int recordCount = spsTmpSupplierInfoService.searchCount(tmpSupplierInfoCriteria);
        if(recordCount <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0040,
                SupplierPortalConstant.LBL_SUPPLIER_INFO);
        }
        /*Get each record from temporary table*/
        for(int i = Constants.ZERO; i < recordCount; i++){

            /*Get temporary data each record to process*/
            TmpSupplierInfoDomain criteriaDomain = new TmpSupplierInfoDomain();
            criteriaDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
            criteriaDomain.setSessionCd(supplierInfoUploadDomain.getSessionCode());
            TmpSupplierInfoDomain tmpSupplierInfoDomain = tmpSupplierInfoService
                .searchTmpUploadSupplierInfoOneRecord(criteriaDomain);
            if(null == tmpSupplierInfoDomain){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0040,
                    SupplierPortalConstant.LBL_SUPPLIER_INFO);
            }
            
            /*Get Company supplier code from vendor code*/
            SpsMAs400VendorCriteriaDomain vendorCriteria = new SpsMAs400VendorCriteriaDomain();
            vendorCriteria.setVendorCd(tmpSupplierInfoDomain.getVendorCd());
            vendorCriteria.setDCd(tmpSupplierInfoDomain.getDCd());
            SpsMAs400VendorDomain as400VendorDomain = 
                spsMAs400VendorService.searchByKey(vendorCriteria);
            tmpSupplierInfoDomain.setSCd(as400VendorDomain.getSCd());
            
            /*Flag values operation.*/
            if(Constants.STR_A.equals(tmpSupplierInfoDomain.getFlag())){
                // Create DENSO and supplier relation to the system
                this.createDensoSupplierRelation(tmpSupplierInfoDomain, supplierInfoUploadDomain,
                    locale);
                
                // Create DENSO and supplier relation part number to the system
                this.createDensoSupplierParts(tmpSupplierInfoDomain, supplierInfoUploadDomain,
                    locale);
            }else if(Constants.STR_U.equals(tmpSupplierInfoDomain.getFlag())){
                /* Create DENSO and supplier relation part number to the system
                 * Not update DENSO and Supplier Relation, Insert when not exist.
                 */
                this.createDensoSupplierRelation(tmpSupplierInfoDomain, supplierInfoUploadDomain,
                    locale);
                
                // [IN055] Flag U is now for update existing Supplier Information
                /* Create DENSO and supplier relation part number to the system
                 * Not update DENSO and supplier relation part number, Insert when not exist.
                 * */
                //this.createDensoSupplierParts(tmpSupplierInfoDomain, supplierInfoUploadDomain,
                //    locale);
                // Update DENSO Supplier Parts
                this.updateDensoSupplierParts(tmpSupplierInfoDomain, supplierInfoUploadDomain,
                    locale);
            }else if(Constants.STR_D.equals(tmpSupplierInfoDomain.getFlag())){
                /*Delete DENSO and supplier relation part number to the system*/
                SpsMDensoSupplierPartsCriteriaDomain densoSupplierPartCriteriaDomain = 
                    new SpsMDensoSupplierPartsCriteriaDomain();
                densoSupplierPartCriteriaDomain.setDCd(tmpSupplierInfoDomain.getDCd());
                densoSupplierPartCriteriaDomain.setDPcd(tmpSupplierInfoDomain.getDPcd());
                densoSupplierPartCriteriaDomain.setSCd(tmpSupplierInfoDomain.getSCd());
                //densoSupplierPartCriteriaDomain.setSPcd(tmpSupplierInfoDomain.getSPcd());
                densoSupplierPartCriteriaDomain.setDPn(tmpSupplierInfoDomain.getDPn());
                if(!StringUtil.checkNullOrEmpty(tmpSupplierInfoDomain.getEffectDate())){
                    densoSupplierPartCriteriaDomain.setEffectDate(tmpSupplierInfoDomain
                        .getEffectDate());
                }
                densoSupplierPartCriteriaDomain.setLastUpdateDatetime(tmpSupplierInfoDomain
                    .getPartLastUpdate());
                int deleteRecord = spsMDensoSupplierPartsService.deleteByCondition(
                    densoSupplierPartCriteriaDomain);
                if(deleteRecord != Constants.ONE){
                    MessageUtil.throwsApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                        SupplierPortalConstant.LBL_DELETE_DENSO_SUPPLIER_PART);
                }
            }
            /*Set flag (register to actual table) in temporary table*/
            /*Set value to Domain*/
            SpsTmpSupplierInfoDomain supplierInfoDomain = new SpsTmpSupplierInfoDomain();
            supplierInfoDomain.setIsActualRegister(Constants.IS_ACTIVE);
            supplierInfoDomain.setToActualDatetime(commonService.searchSysDate());
            
            SpsTmpSupplierInfoCriteriaDomain supplierInfoCriteriaDomain = 
                new SpsTmpSupplierInfoCriteriaDomain();
            supplierInfoCriteriaDomain.setUserDscId(tmpSupplierInfoDomain.getUserDscId());
            supplierInfoCriteriaDomain.setSessionCd(tmpSupplierInfoDomain.getSessionCd());
            supplierInfoCriteriaDomain.setLineNo(tmpSupplierInfoDomain.getLineNo());

            int updateRecord = spsTmpSupplierInfoService.updateByCondition(supplierInfoDomain, 
                supplierInfoCriteriaDomain);
            if(updateRecord != Constants.ONE){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                    SupplierPortalConstant.LBL_UPDATE_MOVE_TO_ACTUAL_FLAG);
            }
        } /*END LOOP*/
        /*Output Log*/

    }

    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeServiceImpl#searchUploadErrorList
     * (com.globaldenso.asia.sps.business.domain.SupplierUserUploadingDomain)
     */
    public  SupplierInfoUploadDomain searchUploadErrorList(SupplierInfoUploadDomain 
        supplierInfoUploadDomain)throws ApplicationException{
        Locale locale = supplierInfoUploadDomain.getLocale();
        List<GroupUploadErrorDomain> result = new ArrayList<GroupUploadErrorDomain>();
        List<PlantSupplierDomain> plantSupplierList = new ArrayList<PlantSupplierDomain>();

        /*Get number of warning record*/
        GroupUploadErrorDomain groupUploadErrorDomain = new GroupUploadErrorDomain();
        groupUploadErrorDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(supplierInfoUploadDomain.getSessionCode());
        groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
        int numberOfWarning = tmpUploadErrorService.searchCountWarning(groupUploadErrorDomain);

        /*Get number of incorrect record */
        groupUploadErrorDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(supplierInfoUploadDomain.getSessionCode());
        groupUploadErrorDomain.setMessageType(Constants.MESSAGE_TYPE_ERROR);
        int numberOfError = tmpUploadErrorService.searchCountIncorrect(groupUploadErrorDomain);

        /*Get number of correct record*/
        SpsTmpSupplierInfoCriteriaDomain tmpSupplierInfoCriteria = new 
            SpsTmpSupplierInfoCriteriaDomain();
        tmpSupplierInfoCriteria.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        tmpSupplierInfoCriteria.setSessionCd(supplierInfoUploadDomain.getSessionCode());
        int numberOfCorrect = spsTmpSupplierInfoService.searchCount(tmpSupplierInfoCriteria);

        /*Calculate total record*/
        int totalRecord = numberOfCorrect + numberOfWarning + numberOfError;
        
        int maxRecord = tmpUploadErrorService.searchCountGroupOfValidationError(
            groupUploadErrorDomain);

        /* Get Maximum record limit*/
        MiscellaneousDomain recordsLimit = new MiscellaneousDomain();
        recordsLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
        recordsLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD001_RLM);
        recordsLimit = recordLimitService.searchRecordLimit(recordsLimit);
        if(null == recordsLimit){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        
        /*Check max record*/
        if(Integer.valueOf(recordsLimit.getMiscValue()) < maxRecord ){
            MessageUtil.throwsApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E6_0003,  
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{recordsLimit.getMiscValue()});
        }
        
        /* Get Maximum record per page limit*/
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
        recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WORD001_PLM);
        recordLimitPerPage = recordLimitService.searchRecordLimit(recordLimitPerPage);
        if(null == recordLimitPerPage){
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0032,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        /*Calculate row number for query*/
        supplierInfoUploadDomain.setPageNumber(supplierInfoUploadDomain.getPageNumber());
        supplierInfoUploadDomain.setMaxRowPerPage(Integer.valueOf(
            recordLimitPerPage.getMiscValue()));
        SpsPagingUtil.calcPaging(supplierInfoUploadDomain, maxRecord);

        /*Get list of validation error and warning and group by code number*/
        groupUploadErrorDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        groupUploadErrorDomain.setSessionId(supplierInfoUploadDomain.getSessionCode());
        groupUploadErrorDomain.setRowNumFrom(supplierInfoUploadDomain.getRowNumFrom());
        groupUploadErrorDomain.setRowNumTo(supplierInfoUploadDomain.getRowNumTo());
        result = tmpUploadErrorService.searchGroupOfValidationError(groupUploadErrorDomain);
        
        /*Get plant supplier list*/
        SpsTmpSupplierInfoCriteriaDomain tmpSupplierInfoCriteriaDomain =
            new SpsTmpSupplierInfoCriteriaDomain();
        tmpSupplierInfoCriteriaDomain.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        tmpSupplierInfoCriteriaDomain.setSessionCd(supplierInfoUploadDomain.getSessionCode());
        TmpSupplierInfoDomain criteriaTmpSupplierInfo = new TmpSupplierInfoDomain();
        criteriaTmpSupplierInfo.setUserDscId(supplierInfoUploadDomain.getUserDscId());
        criteriaTmpSupplierInfo.setSessionCd(supplierInfoUploadDomain.getSessionCode());
        int countUserSupplier = spsTmpSupplierInfoService.searchCount(
            tmpSupplierInfoCriteriaDomain);
        if(Constants.ZERO < countUserSupplier){
            plantSupplierList = tmpSupplierInfoService.searchTmpUploadSupplierInfoPlant(
                criteriaTmpSupplierInfo);
            if(Constants.ZERO < plantSupplierList.size()){
                supplierInfoUploadDomain.setPlantSupplierList(plantSupplierList);
            }
        }

        supplierInfoUploadDomain.setTotalRecord(String.valueOf(totalRecord));
        supplierInfoUploadDomain.setCorrectRecord(String.valueOf(numberOfCorrect));
        supplierInfoUploadDomain.setInCorrectRecord(String.valueOf(numberOfError));
        supplierInfoUploadDomain.setWarningRecord(String.valueOf(numberOfWarning));
        supplierInfoUploadDomain.setUploadErrorDetailDomain(result);
            
        return supplierInfoUploadDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeServiceImpl#searchSupplierInformationCSV
     * (com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain)
     */
    public SupplierInfoUploadDomain searchSupplierInformationCsv(SupplierInfoUploadDomain 
        supplierInfoUploadDomain) throws ApplicationException{
        Locale locale = supplierInfoUploadDomain.getLocale();
        SupplierInfoUploadDomain resultDomain = new SupplierInfoUploadDomain();
        StringBuffer resultString = new StringBuffer();
        CommonDomain commonDomain = new CommonDomain();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
//        List<DensoSupplierPartDomain> resultList = new 
//            ArrayList<DensoSupplierPartDomain>();
        try{
            DataScopeControlDomain dataScopeControlDomain = supplierInfoUploadDomain
                .getDataScopeControlDomain();
            
            /*Get relation between DENSO and Supplier*/
            List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
                densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationDomainList);
            
            /*Get list of supplier information*/
            List<DensoSupplierPartDomain> densoSupplierPartList = densoSupplierPartService
                .searchAllDensoSupplierPart(dataScopeControlDomain);
            if(densoSupplierPartList.size() <= Constants.ZERO){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_I6_0006,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
            
            /*Create CSV String*/
            /*Validate*/
            for(DensoSupplierPartDomain densoSupplierPartUser : densoSupplierPartList){
                if(null != densoSupplierPartUser.getEffectDate()){
                    densoSupplierPartUser.setEffectDateScreen(DateUtil.format(
                        densoSupplierPartUser.getEffectDate(), pattern));
                }
                if(null != densoSupplierPartUser.getCreateDatetime()){
                    densoSupplierPartUser.setCreateDatetimeScreen(DateUtil.format(
                        densoSupplierPartUser.getCreateDatetime(), pattern));
                }
//                resultList.add(densoSupplierPartUser);
            }
            
            
            final String[] headerArr = new String[] {
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_PCD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_D_PN),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_PN),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_CD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_S_PCD),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_EFFECTIVE_DATE),
                MessageUtil.getReportLabel(locale, SupplierPortalConstant.LBL_REGISTER_DATE)};

            List<Map<String, Object>> resultDetail = this
                .doMapSupplierInfoList(densoSupplierPartList, headerArr);
            
            commonDomain.setResultList(resultDetail);
            commonDomain.setHeaderArr(headerArr);
            commonDomain.setHeaderFlag(true);
            
            resultString = commonService.createCsvString(commonDomain);
            if(StringUtil.checkNullOrEmpty(resultString)){
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }else{
                resultDomain.setResultString(resultString);
            }
        }catch(ApplicationException applicationException){
            throw applicationException;
        }catch(Exception e){
            MessageUtil.throwsApplicationMessage(locale, SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @throws com.globaldenso.ai.common.core.exception.ApplicationException;
     * @see com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeServiceImpl#deleteUploadTempTable
     * (String userDscId, String sessionCode)
     */
    public void deleteUploadTempTable(String userDscId, String sessionCode)
        throws ApplicationException{
        
        SpsTmpUploadErrorCriteriaDomain criteriaDomain = new SpsTmpUploadErrorCriteriaDomain();
        SpsTmpSupplierInfoCriteriaDomain tmpSupplierInfoCriteriaDomain = 
            new SpsTmpSupplierInfoCriteriaDomain();
        /*Set Upload Date time Less Than Equal to (CommonService.searchSysDate() - 2 day)*/
        Timestamp currentTimestamp = commonService.searchSysDate();
        int pattern = DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH;
        Calendar prevDate = Calendar.getInstance();
        prevDate.setTime(currentTimestamp);
        prevDate.add(Calendar.DATE, Constants.MINUS_TWO);
        String previousDate = DateUtil.format(prevDate.getTime(), pattern);
        Timestamp uploadDatetime = DateUtil.parseToTimestamp(previousDate, pattern);

        /*1.Delete temporary table that keep upload error items.*/
        if(!StringUtil.checkNullOrEmpty(userDscId) && !StringUtil.checkNullOrEmpty(sessionCode)){
            criteriaDomain.setSessionCd(sessionCode);
            criteriaDomain.setUserDscId(userDscId);
        }else{
            criteriaDomain.setUploadDatetimeLessThanEqual(uploadDatetime);
        }
        spsTmpUploadErrorService.deleteByCondition(criteriaDomain);

        /*2.Delete temporary table that keep upload Supplier information*/
        if(!StringUtil.checkNullOrEmpty(userDscId) && !StringUtil.checkNullOrEmpty(sessionCode)){
            tmpSupplierInfoCriteriaDomain.setSessionCd(sessionCode);
            tmpSupplierInfoCriteriaDomain.setUserDscId(userDscId);
        }else{
            tmpSupplierInfoCriteriaDomain.setUploadDatetimeLessThanEqual(uploadDatetime);
        }
        spsTmpSupplierInfoService.deleteByCondition(tmpSupplierInfoCriteriaDomain);

    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale) throws ApplicationException {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * 
     * <p>Search Exist DENSO Plant.</p>
     *
     * @param densoCompanyCode the DENSO company code.
     * @param densoPlantCode the DENSO plant code.
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistDensoPlant(String densoCompanyCode, String densoPlantCode)
        throws ApplicationException{
        /*Check existing DENSO plant in the master data*/
        SpsMPlantDensoCriteriaDomain criteriaDomain = new SpsMPlantDensoCriteriaDomain();
        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setDPcd(densoPlantCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMPlantDensoService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist DENSO Company.</p>
     *
     * @param densoCompanyCode the DENSO Company code to check existing DENSO company.
     * @return boolean that keep true = found DENSO company code in master data 
     * and false = not found DENSO company code in master data.
     * @throws ApplicationException the ApplicationException.
     */
    private boolean searchExistDensoCompany(String densoCompanyCode)
        throws ApplicationException{
        
        /*Check existing DENSO company in the master data*/
        SpsMCompanyDensoCriteriaDomain criteriaDomain = new SpsMCompanyDensoCriteriaDomain();

        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMCompanyDensoService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist Supplier Company With Scope.</p>
     *
     * @param supplierCompanyCode the Supplier Company code to check existing supplier company.
     * @param supplierPlantCode the Supplier plant code
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException. 
     */
    private boolean searchExistSupplierPlant(String supplierCompanyCode, String supplierPlantCode)
        throws ApplicationException{
        /*Check existing Supplier plant in the master data*/
        SpsMPlantSupplierCriteriaDomain criteriaDomain = new SpsMPlantSupplierCriteriaDomain();
        criteriaDomain.setSCd(supplierCompanyCode);
        criteriaDomain.setSPcd(supplierPlantCode);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);

        int countRecord = spsMPlantSupplierService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist Supplier Plant With Scope.</p>
     *
     * @param supplierCompanyCode the Supplier Company code to check existing supplier company.
     * @param supplierPlantCode the Supplier plant code
     * @param dataScopeControlDomain the Limitation to search condition.
     * @return boolean that keep true = found and false = not found.
     */
    private boolean searchExistSupplierPlantWithScope(String supplierCompanyCode,
        String supplierPlantCode, DataScopeControlDomain dataScopeControlDomain){
        /*Check existing Supplier plant by current user data scope in the master data */
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        plantSupplierDomain.setSCd(supplierCompanyCode);
        plantSupplierDomain.setSPcd(supplierPlantCode);
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        int countRecord = plantSupplierService
            .searchExistPlantSupplier(plantSupplierWithScopeDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    // Start : [IN068] Add new field for keep message parameter at table SPS_TMP_UPLOAD_ERROR
    //private void createItemUploadError(SupplierInfoUploadDomain uploadDomain, 
    //    String errorCode, int lineNo)throws ApplicationException{
    //    SpsTmpUploadErrorDomain errorDomain = new SpsTmpUploadErrorDomain();
    //    errorDomain.setUserDscId(uploadDomain.getUserDscId());
    //    errorDomain.setSessionCd(uploadDomain.getSessionCode());
    //    errorDomain.setLineNo(BigDecimal.valueOf(lineNo));
    //    errorDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
    //    errorDomain.setMiscCd(errorCode);
    //    errorDomain.setFunctionCode(Constants.STR_ONE);
    //    errorDomain.setUploadDatetime(commonService.searchSysDate());
    //    spsTmpUploadErrorService.create(errorDomain);
    //}
    
    /**
     * <p>create Item Upload Error.</p>
     *
     * @param uploadDomain It keep current supplier information to create error data 
     * to temporary table.
     * @param errorCode the Error code from validation.
     * @param lineNo the Line no from validation.
     * @throws ApplicationException the ApplicationException.
     */
    private void createItemUploadError(SupplierInfoUploadDomain uploadDomain, 
        String errorCode, int lineNo)throws ApplicationException{
        this.createItemUploadError(uploadDomain, errorCode, lineNo, null);
    }

    /**
     * 
     * <p>create Item Upload Error.</p>
     *
     * @param uploadDomain It keep current supplier information to create error data 
     * to temporary table.
     * @param errorCode the Error code from validation.
     * @param lineNo the Line no from validation.
     * @param msgParam Message Parameter
     * @throws ApplicationException the ApplicationException.
     */
    private void createItemUploadError(SupplierInfoUploadDomain uploadDomain, 
        String errorCode, int lineNo, String msgParam)throws ApplicationException
    {
        SpsTmpUploadErrorDomain errorDomain = new SpsTmpUploadErrorDomain();
        errorDomain.setUserDscId(uploadDomain.getUserDscId());
        errorDomain.setSessionCd(uploadDomain.getSessionCode());
        errorDomain.setLineNo(BigDecimal.valueOf(lineNo));
        errorDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_VAL_UPLOAD);
        errorDomain.setMiscCd(errorCode);
        errorDomain.setFunctionCode(Constants.STR_ONE);
        errorDomain.setUploadDatetime(commonService.searchSysDate());
        errorDomain.setMsgParam(msgParam);
        spsTmpUploadErrorService.create(errorDomain);
    }
    // End : [IN068] Add new field for keep message parameter at table SPS_TMP_UPLOAD_ERROR
    
    /**
     * 
     * <p>search Exist Part Number.</p>
     *
     * @param densoCompanyCode the densoCompanyCode.
     * @param densoPlantCode the densoPlantCode.
     * @param supplierCompanyCode the supplierCompanyCode.
     * @param densoPartNumber the densoPartNumber.
     * @param effectDate the effectDate.
     * @return boolean the true = user DSC ID existing in master data, 
     * false = user DSC ID not in master data.
     * @throws Exception the Exception.
     */
    
    private boolean searchExistPartNumber(String densoCompanyCode, String densoPlantCode, 
        String supplierCompanyCode, String densoPartNumber, 
        String effectDate)throws Exception{
        /*Check existing part number in master data*/
        SpsMDensoSupplierPartsCriteriaDomain criteriaDomain = 
            new SpsMDensoSupplierPartsCriteriaDomain();
        int pattern = DateUtil.PATTERN_YYYYMMDD_SLASH;
        Timestamp effectDateTimestamp = null;
        if(!StringUtil.checkNullOrEmpty(effectDate)){
            Date effectDateUtil = DateUtil.parseToUtilDate(effectDate, pattern);
            effectDateTimestamp = new Timestamp(effectDateUtil.getTime());
        }
        criteriaDomain.setDCd(densoCompanyCode);
        criteriaDomain.setDPcd(densoPlantCode);
        criteriaDomain.setSCd(supplierCompanyCode);
        criteriaDomain.setDPn(densoPartNumber);
        criteriaDomain.setEffectDate(effectDateTimestamp);
        int countRecord = spsMDensoSupplierPartsService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
        
    }
    
    /**
     * 
     * <p>Search Exist Supplier Company With Scope.</p>
     *
     * @param companyDensoCd the company DENSO code
     * @param vendorCd the vendor code
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException. 
     */
    private boolean searchExistVendorCd(String companyDensoCd, String vendorCd)
        throws ApplicationException{
        
        /*Check existing vendor code in the master data*/
        SpsMAs400VendorCriteriaDomain criteriaDomain = new SpsMAs400VendorCriteriaDomain();
        criteriaDomain.setVendorCd(vendorCd);
        criteriaDomain.setDCd(companyDensoCd);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMAs400VendorService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Search Exist Supplier Company With Scope.</p>
     *
     * @param companySupplierOwner the Supplier Company code to check existing supplier company.
     * @param companyDensoCd the company DENSO code
     * @param vendorCd the vendor code
     * @return boolean that keep true = found and false = not found.
     * @throws ApplicationException the ApplicationException. 
     */
    private boolean searchExistVendorCdWithScope(String companySupplierOwner, String companyDensoCd,
        String vendorCd)throws ApplicationException{
        
        /*Check existing vendor code in the master data*/
        SpsMAs400VendorCriteriaDomain criteriaDomain = new SpsMAs400VendorCriteriaDomain();
        criteriaDomain.setVendorCd(vendorCd);
        criteriaDomain.setDCd(companyDensoCd);
        criteriaDomain.setSCd(companySupplierOwner);
        criteriaDomain.setIsActive(Constants.IS_ACTIVE);
        int countRecord = spsMAs400VendorService.searchCount(criteriaDomain);
        if(Constants.ZERO < countRecord){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Do Map Supplier Information List.
     * 
     * @param supplierInfoList the Supplier Information List.
     * @param header the List
     * @return resultList 
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    private List<Map<String, Object>> doMapSupplierInfoList(List<DensoSupplierPartDomain> 
        supplierInfoList, String[] header)
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> supplierInfoMap = null;
        
        Iterator itr = supplierInfoList.iterator();
        while(itr.hasNext()){
            DensoSupplierPartDomain supplierInfo = (DensoSupplierPartDomain)itr.next();
            supplierInfoMap = new HashMap<String, Object>();
            supplierInfoMap.put(header[Constants.ZERO],
                StringUtil.checkNullToEmpty(supplierInfo.getDCd()));
            supplierInfoMap.put(header[Constants.ONE],
                StringUtil.checkNullToEmpty(supplierInfo.getDPcd()));
            supplierInfoMap.put(header[Constants.TWO], supplierInfo.getDPn());
            supplierInfoMap.put(header[Constants.THREE], supplierInfo.getSPn());
            supplierInfoMap.put(header[Constants.FOUR],
                StringUtil.checkNullToEmpty(supplierInfo.getVendorCd()));
            supplierInfoMap.put(header[Constants.FIVE],
                StringUtil.checkNullToEmpty(supplierInfo.getSPcd()));
            supplierInfoMap.put(header[Constants.SIX], supplierInfo.getEffectDateScreen());
            supplierInfoMap.put(header[Constants.SEVEN], supplierInfo.getCreateDatetimeScreen());
            resultList.add(supplierInfoMap);
            itr.remove();
        }
        return resultList;
    }
    
    /**
     * If DENSO Supplier Relation is not exist, insert to table.
     * @param tmpSupplierInfoDomain - Temporary Supplier Info
     * @param supplierInfoUploadDomain -  Supplier Info Upload
     * @param locale - for get message
     * @throws ApplicationException - an ApplicationException
     * 
     * */
    private void createDensoSupplierRelation(TmpSupplierInfoDomain tmpSupplierInfoDomain,
        SupplierInfoUploadDomain supplierInfoUploadDomain, Locale locale)
        throws ApplicationException
    {
        try{
            SpsMDensoSupplierRelationDomain densosupplierRelateDomain = 
                new SpsMDensoSupplierRelationDomain();
            densosupplierRelateDomain.setDCd(tmpSupplierInfoDomain.getDCd());
            densosupplierRelateDomain.setDPcd(tmpSupplierInfoDomain.getDPcd());
            densosupplierRelateDomain.setSCd(tmpSupplierInfoDomain.getSCd());
            densosupplierRelateDomain.setSPcd(tmpSupplierInfoDomain.getSPcd());
            densosupplierRelateDomain.setCreateDscId(supplierInfoUploadDomain.getUserDscId());
            densosupplierRelateDomain.setCreateDatetime(commonService.searchSysDate());
            densosupplierRelateDomain.setLastUpdateDscId(supplierInfoUploadDomain.getUserDscId());
            densosupplierRelateDomain.setLastUpdateDatetime(commonService.searchSysDate());
            SpsMDensoSupplierRelationCriteriaDomain supplierRelationCriteriaDomain = 
                new SpsMDensoSupplierRelationCriteriaDomain();
            supplierRelationCriteriaDomain.setSCd(tmpSupplierInfoDomain.getSCd());
            supplierRelationCriteriaDomain.setSPcd(tmpSupplierInfoDomain.getSPcd());
            supplierRelationCriteriaDomain.setDCd(tmpSupplierInfoDomain.getDCd());
            supplierRelationCriteriaDomain.setDPcd(tmpSupplierInfoDomain.getDPcd());
            List<SpsMDensoSupplierRelationDomain> relationDomainList = 
                spsMDensoSupplierRelationService.searchByCondition(
                    supplierRelationCriteriaDomain);
            if(relationDomainList.size() <= Constants.ZERO){
                spsMDensoSupplierRelationService.create(densosupplierRelateDomain);
            }
        }catch(Exception e){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_REGISTER_DENSO_SUPPLIER_RELATE);
        }
    }
    
    /**
     * Insert new DENSO Supplier Parts.
     * @param tmpSupplierInfoDomain - Temporary Supplier Info
     * @param supplierInfoUploadDomain -  Supplier Info Upload
     * @param locale - for get message
     * @throws ApplicationException - an ApplicationException
     * 
     * */
    private void createDensoSupplierParts(TmpSupplierInfoDomain tmpSupplierInfoDomain,
        SupplierInfoUploadDomain supplierInfoUploadDomain, Locale locale)
        throws ApplicationException
    {
        try{
            // Create DENSO and supplier relation part number to the system
            SpsMDensoSupplierPartsDomain densoSupplierPartDomain = 
                new SpsMDensoSupplierPartsDomain();
            densoSupplierPartDomain.setDCd(tmpSupplierInfoDomain.getDCd());
            densoSupplierPartDomain.setDPcd(tmpSupplierInfoDomain.getDPcd());
            densoSupplierPartDomain.setSCd(tmpSupplierInfoDomain.getSCd());
            densoSupplierPartDomain.setSPcd(tmpSupplierInfoDomain.getSPcd());
            densoSupplierPartDomain.setDPn(tmpSupplierInfoDomain.getDPn());
            densoSupplierPartDomain.setSPn(tmpSupplierInfoDomain.getSPn());
            if(!StringUtil.checkNullOrEmpty(tmpSupplierInfoDomain.getEffectDate())){
                densoSupplierPartDomain.setEffectDate(tmpSupplierInfoDomain.getEffectDate());
            }
            densoSupplierPartDomain.setCreateDscId(supplierInfoUploadDomain.getUserDscId());
            densoSupplierPartDomain.setCreateDatetime(commonService.searchSysDate());
            densoSupplierPartDomain.setLastUpdateDscId(supplierInfoUploadDomain.getUserDscId());
            densoSupplierPartDomain.setLastUpdateDatetime(commonService.searchSysDate());
            spsMDensoSupplierPartsService.create(densoSupplierPartDomain);
        }catch(Exception e){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_REGISTER_DENSO_SUPPLIER_PART);
        }
    }

    // [IN055] Update Suplier Information
    /**
     * Update existing record DENSO Supplier Parts.
     * @param tmpSupplierInfoDomain - Temporary Supplier Info
     * @param supplierInfoUploadDomain -  Supplier Info Upload
     * @param locale - for get message
     * @throws ApplicationException - an ApplicationException
     * 
     * */
    private void updateDensoSupplierParts(TmpSupplierInfoDomain tmpSupplierInfoDomain,
        SupplierInfoUploadDomain supplierInfoUploadDomain, Locale locale)
        throws ApplicationException
    {
        try{
            // Create DENSO and supplier relation part number to the system
            SpsMDensoSupplierPartsDomain densoSupplierPartDomain = 
                new SpsMDensoSupplierPartsDomain();
            densoSupplierPartDomain.setSPcd(tmpSupplierInfoDomain.getSPcd());
            densoSupplierPartDomain.setSPn(tmpSupplierInfoDomain.getSPn());
            densoSupplierPartDomain.setLastUpdateDscId(supplierInfoUploadDomain.getUserDscId());
            densoSupplierPartDomain.setLastUpdateDatetime(commonService.searchSysDate());
            
            SpsMDensoSupplierPartsCriteriaDomain densoSupplierPartCriteria
                = new SpsMDensoSupplierPartsCriteriaDomain();
            densoSupplierPartCriteria.setDCd(tmpSupplierInfoDomain.getDCd());
            densoSupplierPartCriteria.setDPcd(tmpSupplierInfoDomain.getDPcd());
            densoSupplierPartCriteria.setSCd(tmpSupplierInfoDomain.getSCd());
            densoSupplierPartCriteria.setDPn(tmpSupplierInfoDomain.getDPn());
            if(!StringUtil.checkNullOrEmpty(tmpSupplierInfoDomain.getEffectDate())){
                densoSupplierPartCriteria.setEffectDate(tmpSupplierInfoDomain.getEffectDate());
            }
            spsMDensoSupplierPartsService.updateByCondition(
                densoSupplierPartDomain, densoSupplierPartCriteria);
        }catch(Exception e){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0034,
                SupplierPortalConstant.LBL_REGISTER_DENSO_SUPPLIER_PART);
        }
    }

}