/*
 * ModifyDate Development company       Describe 
 * 2014/07/07 CSI Karnrawee             Create
 * 2015/10/06 CSI Akat                  [IN016]
 * 2016/02/04 CSI Akat                  [IN056]
 * 2016/03/15 CSI Akat                  [IN069]
 * 2016/04/08 CSI Akat                  [IN068]
 * 2016/04/18 CSI Akat                  [IN070]
 * 2018/04/11 Netband U.Rungsiwut       Generate original/change P/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;













import org.apache.commons.logging.LogFactory;









//import com.globaldenso.ai.common.core.context.DensoContext;
//import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService;
import com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoToCIGMAService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoService;
import com.globaldenso.asia.sps.auto.business.service.SpsTPoToCIGMAService;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaChangePoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaResultDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueForCalculateMonthDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SortUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.webserviceutil.CommonWebServiceUtil;

import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//.MAIL_CIGMA_DO_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_PO_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_SUBJECT;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .MAIL_CIGMA_CHG_PO_1_ERROR_SENDER_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER1;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER2;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE3;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_DETAIL;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_CHG_PO_1_ERROR_FOOTER;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_VENDOR_CD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_SCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_DCD_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_ISSUE_DATE_REPLACEMENT;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .PO_DATA_TYPE_MAP;
// [IN070] add list of email to send in content
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .MAIL_CIGMA_ERROR_PERSON_IN_CHARGE;

// static import
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E5_0002;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0013;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_80_0016;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0001;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0015;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0016;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0017;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0018;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0019;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0020;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0022;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0023;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_VENDOR_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_PARTS_NO_NOT_FOUND;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.ERROR_CD_SP_E6_0033;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant.TABLE_CIGMA_PO_NAME;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TABLE_CIGMA_CHG_PO_NAME;
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID;
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO; // Create Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_DETAIL; // Create Purchase Order Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_DUE; // Create Purchase Order Due
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_CHG_PO; // Create Change Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_CHG_PO_DETAIL; // Create Change Purchase Order Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_CHG_PO_DUE; // Create Change Purchase Order Due
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE; // Create P/O Cover Page
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE_DETAIL; // Create P/O Cover Page Detail
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT; // Search P/O Cover Page Report
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID; // Update PDF Original File ID
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID; // Update PDF Change File ID
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_SEARCH_EXIST_PO; // Search Exist Purchase Order
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO_DUE; // Update Purchase Order Due
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO_DETAIL; // Update Purchase Order Detail
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_UPDATE_PO; // Update Purchase Order
import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
    .TRANSFER_PO_STEP_SEARCH_CHG_MAT_REPORT; // Search Change Material Report
//import static com.globaldenso.asia.sps.common.constant.SupplierPortalConstant
//    .TRANSFER_PO_STEP_SEARCH_EXISTS_FOR_FIRM; // Search Exists Purchaser Order for Firm Period
import static com.globaldenso.asia.sps.common.constant.Constants.EMPTY_STRING;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NBSP;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_C;
import static com.globaldenso.asia.sps.common.constant.Constants.STR_Y;
import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;
import static com.globaldenso.asia.sps.common.constant.Constants.ONE;
import static com.globaldenso.asia.sps.common.constant.Constants.TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.THREE;
import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;
import static com.globaldenso.asia.sps.common.constant.Constants.FIVE;
import static com.globaldenso.asia.sps.common.constant.Constants.SIX;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_NEWLINE;
import static com.globaldenso.asia.sps.common.constant.Constants.SYMBOL_CR_LF;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_HHMMSS;
import static com.globaldenso.asia.sps.common.utils.DateUtil.PATTERN_YYYYMMDD_SLASH;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToSqlDate;
import static com.globaldenso.asia.sps.common.utils.DateUtil.parseToTimestamp;
import static com.globaldenso.asia.sps.common.utils.DateUtil.format;
import static com.globaldenso.asia.sps.common.utils.NumberUtil.toBigDecimal;
import static com.globaldenso.asia.sps.common.utils.StringUtil.appendsString;
import static com.globaldenso.asia.sps.common.utils.StringUtil.nullToEmpty;
//import static com.globaldenso.asia.sps.common.utils.MessageUtil.throwsApplicationMessage;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getLabelHandledException;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.getEmailLabel;
//import static com.globaldenso.asia.sps.common.utils.MessageUtil.getErrorMessage;
import static com.globaldenso.asia.sps.common.utils.MessageUtil.writeLog;

/**
 * <p>The class TransferPoDataFromCigmaFacadeServiceImpl.</p>
 * <p>Facade for TransferPoDataFromCigmaFacadeServiceImpl.</p>
 * <ul>
 * <li>Method search  : searchInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class TransferDoDataToCigmaServiceImpl
    implements TransferDoDataToCigmaService {
    
    /** Reuse Argument 1. */
    private String[] argument1 = new String[ONE];
    /** Reuse Argument 2. */
    private String[] argument2 = new String[TWO];
    /** Reuse Argument 3. */
    private String[] argument3 = new String[THREE];
    /** Reuse Argument 4. */
    private String[] argument4 = new String[FOUR];
    /** Reuse Argument 5. */
    private String[] argument5 = new String[FIVE];
    /** Reuse Argument 6. */
    private String[] argument6 = new String[SIX];
    
    /** Common Services. */
    private CommonService commonService;
    /** Denso Company Services. */
    private SpsTDoToCIGMAService spsTDoToCIGMAService;
   
    private CompanyDensoService companyDensoService;
    
	/** The Constant LOG. */
	private static final Log LOG = LogFactory
			.getLog(SpsTPoToCIGMAService.class);
    /**
     * Instantiates a new upload facade service impl.
     */
    public TransferDoDataToCigmaServiceImpl(){
        super();
    }
    private MailService mailService;
    

    public MailService getMailService() {
		return mailService;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	/**
     * <p>Setter method for commonService.</p>
     *
     * @param commonService Set for commonService
     */
    public void setCommonService(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * <p>Setter method for poService.</p>
     *
     * @param poService Set for poService
     */
    public void setSpsTDoToCIGMAService(SpsTDoToCIGMAService spsTDoToCIGMAService) {
        this.spsTDoToCIGMAService = spsTDoToCIGMAService;
    }

   

    public CompanyDensoService getCompanyDensoService() {
		return companyDensoService;
	}

	public void setCompanyDensoService(CompanyDensoService companyDensoService) {
		this.companyDensoService = companyDensoService;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public SpsTDoToCIGMAService getSpsTDoToCIGMAService() {
		return spsTDoToCIGMAService;
	}

	/**
	 * 
	 * <p>
	 * Search As400 Server List.
	 * </p>
	 * 
	 * @param criteria
	 *            SpsMCompanyDensoDomain
	 * @return List of SpsMAs400SchemaDomain
	 * @throws Exception
	 *             error
	 */
    public List<As400ServerConnectionInformationDomain> searchAs400ServerList(
			SpsMCompanyDensoDomain criteria) throws Exception {
		List<As400ServerConnectionInformationDomain> selectList = companyDensoService.searchAs400ServerList(criteria);
		return selectList;
	}
	public List<SpsMDensoDensoRelationWithDODNDomain> searchDO(SpsMDensoDensoRelationWithDODNDomain criteria)
			throws Exception {
		List<SpsMDensoDensoRelationWithDODNDomain> selectList = spsTDoToCIGMAService.searchByCondition(criteria);
		return selectList;
	}

	/**
	 * 
	 * <p>
	 * get Argument Message.
	 * </p>
	 * 
	 * @param args
	 *            array String
	 * @return String[]
	 */
	private String[] getArgumentMessage(String... args) {
		if (null != args && ZERO < args.length) {
			if (ONE == args.length) {
				argument1[ZERO] = args[ZERO];
				return argument1;
			} else if (TWO == args.length) {
				argument2[ZERO] = args[ZERO];
				argument2[ONE] = args[ONE];
				return argument2;
			} else if (THREE == args.length) {
				argument3[ZERO] = args[ZERO];
				argument3[ONE] = args[ONE];
				argument3[TWO] = args[TWO];
				return argument3;
			} else if (FOUR == args.length) {
				for (int i = ZERO; i < FOUR; ++i) {
					argument4[i] = args[i];
				}
				return argument4;
			} else if (FIVE == args.length) {
				for (int i = ZERO; i < FIVE; ++i) {
					argument5[i] = args[i];
				}
				return argument5;
			} else if (SIX == args.length) {
				for (int i = ZERO; i < SIX; ++i) {
					argument6[i] = args[i];
				}
				return argument6;
			}
		}
		return null;
	}

	public void transferDO(
			As400ServerConnectionInformationDomain as400Server, String spsFlag,List<SpsMDensoDensoRelationWithDODNDomain> criteria)
			throws Exception {
		Locale locale = this.getDefaultLocale();
		String TempdCd="";
        if(criteria != null){
    		if(criteria.size() >0){
    			List<SPSTDoHeaderDNDomain> listErrorDo =new ArrayList<SPSTDoHeaderDNDomain>();
    			for(int i=0;i<criteria.size();i++){
                    //Get as400Server
    				SpsMCompanyDensoDomain company = new SpsMCompanyDensoDomain();
    				company.setDCd(StringUtil.convertListToVarcharCommaSeperate(criteria.get(i).getSchemaCd()));
    				// Call Façade Service TransferDODataFromCIGMAFacadeService
    				// searchAS400ServerList()
    				List<As400ServerConnectionInformationDomain> listAs400Server = companyDensoService.searchAs400ServerList(company);
    				if (null != listAs400Server) {
    					if (Constants.ZERO == listAs400Server.size()) {
    						// Log OutPut Log
    						LOG.error(MessageUtil
    								.getApplicationMessageHandledException(locale,
    										SupplierPortalConstant.ERROR_CD_SP_E5_0026,
    										new String[] { company.getDCd() }));
    					} 
    				} else {
    					LOG.error(MessageUtil.getApplicationMessageHandledException(
    							locale, SupplierPortalConstant.ERROR_CD_SP_E5_0026,
    							new String[] { company.getDCd() }));
    					// MessageUtil.throwsApplicationMessage(locale,
    					// SupplierPortalConstant.ERROR_CD_SP_E5_0026, null
    					// , new String[]{company.getDCd()} );
    				}

    				// LOOP all data in <List>as400Server
    				if (null != listAs400Server) {
    					as400Server =listAs400Server.get(0);
    					if(criteria.get(i).getListDO() != null){
        					if(criteria.get(i).getListDO().size() >0){
        						for(int j=0;j<criteria.get(i).getListDO().size();j++){
        							String doID =criteria.get(i).getListDO().get(j).getPoId().toString();
        							SPSTDoHeaderDNDomain result = new SPSTDoHeaderDNDomain();
        							result = criteria.get(i).getListDO().get(j);
        							try{
        							    if(result.getDnDnTrnFlg()!=null&&result.getDnDnTrnFlg().equals("9")){
        							        listErrorDo.add(result);
        							    }else{
        							        //Set Database Schema Name
                                            criteria.get(i).setDatabaseName(as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
                                            criteria.get(i).getListDO().get(j).setDatabaseName(as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
                                            if(criteria.get(i).getListDO().get(j).getListDoDet() != null){
                                                if(criteria.get(i).getListDO().get(j).getListDoDet().size() >0){
                                                    for(int k=0;k<criteria.get(i).getListDO().get(j).getListDoDet().size();k++){
                                                        criteria.get(i).getListDO().get(j).getListDoDet().get(k).setDatabaseName(as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
                                                    }
                                                }
                                            }
                                            if(criteria.get(i).getListDO().get(j).getListDoKanban() != null){
                                                if(criteria.get(i).getListDO().get(j).getListDoKanban().size() >0){
                                                    for(int k=0;k<criteria.get(i).getListDO().get(j).getListDoKanban().size();k++){
                                                        criteria.get(i).getListDO().get(j).getListDoKanban().get(k).setDatabaseName(as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
                                                    }
                                                }
                                            }    
                                            
                                            
                                            //Insert data to db2 by rest        
                                            result = CommonWebServiceUtil.insertDO(as400Server, spsFlag, criteria.get(i).getListDO().get(j));
                                            if(result == null){
                                                result = criteria.get(i).getListDO().get(j);
                                            }
                                            if(result.getIsSuccess()){
                                                result.setDnDnTrnFlg("1");
                                            }else{
                                                result.setDnDnTrnFlg("2");
                                                listErrorDo.add(result);
                                            }
        							    }
        							    
                                        
        							}catch(Exception e){
        								if(result == null){
            		    					result = criteria.get(i).getListDO().get(j);
            		    				}
        								result.setDnDnTrnFlg("2");
        								result.setIsSuccess(false);
        								result.setMsgError(e.getMessage());
        								listErrorDo.add(result);
        							}
        		    				
                                    //Update tran flag
        							boolean updateFlag= this.spsTDoToCIGMAService.UpdateDoTranFlag(result);
    								if (!updateFlag) {
    									MessageUtil
    											.getErrorMessageForBatch(
    													locale,
    													SupplierPortalConstant.ERROR_CD_SP_E6_0067,
    													new String[] { doID }, LOG,
    													Constants.ZERO, false);
    								}
    							}
        						
        					}
        				}
    				}
    				
    				 //Send Command	
    				TempdCd =criteria.get(i).getdCd();
    				int checkSendCommand =0;
    				if(i<criteria.size()-1){
    				   if(!TempdCd.equals(criteria.get(i+1).getdCd())){
    					   checkSendCommand++;
    				   }	
    				}else{
    					checkSendCommand++;
    				}
    				
    				if(checkSendCommand >0){
    					try{
    					 LOG.info("CALL CommandDO Param {"+as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma()+","+criteria.get(i).getSchemaCd()+"}");	
    					 CommonWebServiceUtil.callCommandDO(as400Server, criteria.get(i).getSchemaCd());
    					}catch(Exception e){
    						LOG.error(e.getMessage());
    						
    					}
    					 if (listErrorDo != null) {
    							if (listErrorDo.size() > 0) {
    								// ***Send Email Error DO*********
    								SendEmail(listErrorDo, criteria.get(0).getEmail());
    							}
    						}
    				}
    			}
				
    		}
    	} 
    	

	}

	private void SendEmail(List<SPSTDoHeaderDNDomain> listErrorDo,String emailTo){

		Locale locale = this.getDefaultLocale();
		if(listErrorDo!=null){
			if(listErrorDo.size()>0){
				 try{
					 SendEmailDomain email = new SendEmailDomain();
					 
					 Calendar calendar = Calendar.getInstance(Locale.US);
						String day = String.format("%1$02d",
								calendar.get(Calendar.DATE));
						String month = String.format("%1$02d",
								calendar.get(Calendar.MONTH) + 1);
						int year = calendar.get(Calendar.YEAR);
						String hour = String.format("%1$02d",
								calendar.get(Calendar.HOUR));
						String minute = String.format("%1$02d",
								calendar.get(Calendar.MINUTE));
						String milli = String.format("%1$02d",
								calendar.get(Calendar.SECOND));

						if (year > 2500) {
							year = year - 543;
						}
					String	sdateTime = day+"/"+month+"/"+String.valueOf(year)+" "+ hour
								+":"+ minute;
						
					 String title="Transfer DO failed for "+listErrorDo.get(0).getdCd()+" on ("+sdateTime+")";
						 String content="Cannot create DO at CIGMA system for SPS DO No. : \r\n";
					 String emailFrom=ContextParams.getBaseEmail();
					 for(int j=0;j<listErrorDo.size();j++){
							 if(j==0){
								 content +=listErrorDo.get(j).getSpsDoNo();
							 }else{
								 content +=", "+listErrorDo.get(j).getSpsDoNo();
							 }
							
							 content += " ("+listErrorDo.get(j).getMsgError()+")";
							 MessageUtil.getErrorMessageForBatch(
				                    locale, 
				                    SupplierPortalConstant.ERROR_CD_SP_E6_0066,
				                    new String[]{listErrorDo.get(j).getSpsDoNo()+":"+listErrorDo.get(j).getMsgError()}, 
				                    LOG, 
				                    Constants.ZERO, 
				                    false);
						 }
				
					        email.setHeader( title );
					        email.setEmailSmtp(ContextParams.getEmailSmtp());
					        email.setContents( content );
					        email.setEmailFrom( emailFrom );
					        email.setEmailTo(emailTo);
					        mailService.sendEmail(email);
					 }catch(Exception ex){
						 ex.printStackTrace();
					 }
			}
		}
	}
	
	 /**
		 * Gets the content.
		 * 
		 * @return default locale
		 */
		private Locale getDefaultLocale() {
			Properties propApp = Props
					.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
			String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
			String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
			String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
			ContextParams.setDefaultLocale(strDefaultLocale);
			ContextParams.setBaseDirGui(strBaseDirGui);
			ContextParams.setBaseDirMsg(strBaseDirMsg);
			return LocaleUtil.getLocaleFromString(strDefaultLocale);
		}
		
	private String getMessage(Locale locale, String msgId, String... params)
			throws ApplicationException {
		return MessageUtil.getErrorMessage(locale, msgId, null,
				getArgumentMessage(params));
	}

    /**
     * 
     * <p>Throws ErrorMessage.</p>
     *
     * @param locale Locale
     * @param msgId Message ID
     * @param params argument
     * @throws ApplicationException ApplicationException
     */
    private void throwsErrorMessage(
        Locale locale, 
        String msgId, 
        String ... params) throws ApplicationException {
        MessageUtil.throwsApplicationMessage(locale, msgId, null, getArgumentMessage( params ) );
    }

}
