/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Parichat            Create
 * 2015/09/01 CSI Akat                [IN012]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.InputStream;
import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;

/**
 * <p>The Interface ASN service.</p>
 * <p>Service for ASN about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchSendingAsn</li>
 * <li>Method update  : searchAsnDetail</li>
 * <li>Method search  : searchAsnInformation</li>
 * <li>Method search  : searchCountAsnInformation</li>
 * <li>Method search  : searchExistAsn</li>
 * <li>Method search  : searchAsnReport</li>
 * <li>Method search  : searchAsnForGroupInvoice</li>
 * <li>Method create  : createViewAsnReport</li>
 * <li>Method search  : searchPurgingAsnOrder</li>
 * <li>Method search  : searchSumShippingQty</li>
 * <li>Method search  : searchSumShippingQtyByPart</li>
 * </ul>
 *
 * @author CSI
 */
public interface AsnService {
    
    /**
     * <p>Search sending asn.</p>
     * <ul>
     * <li>Search Sending ASN data from transfer flag.</li>
     * </ul>
     * 
     * @param sendingAsnDomain the sending asn domain
     * @return the ASN Info Domain
     */
    public List<AsnInfoDomain> searchSendingAsn(SendingAsnDomain sendingAsnDomain);
    
    /**
     * <p>Search ASN detail.</p>
     * <ul>
     * <li>Search ASN detail information.</li>
     * </ul>
     * 
     * @param asnInformationDomain the ASN Information Criteria Domain
     * @return the list of Invoice Information Domain.
     */
    public List<AsnInformationDomain> searchAsnDetail (AsnInformationDomain asnInformationDomain);
    
    /**
     * 
     * <p>Search ASN Progress Information.</p>
     *
     * @param asnProgressInformationDomain 
     * @return List of AsnProgressInformationReturnDomain
     */
    public List<AsnProgressInformationReturnDomain> searchAsnInformation(
        AsnProgressInformationDomain asnProgressInformationDomain);
    
    /**
     * 
     * <p>Search count from search.</p>
     *
     * @param asnProgressInformationDomain 
     * @return count
     */
    public int searchCountAsnInformation(AsnProgressInformationDomain asnProgressInformationDomain);
    
    /**
     * 
     * <p>Search exist ASN.</p>
     *
     * @param asnDomain the ASN Domain.
     * @return the list of asn information domain.
     */
    public List<AsnDomain> searchExistAsn(AsnDomain asnDomain);
    
    /**
     * Generate Advanced Ship Notice report.
     * @param spsTAsnDomain domain for SPS_T_ASN
     * @return input stream of PDF report file
     * @throws Exception in case Jasper Report Error
     * */
    public InputStream searchAsnReport(SpsTAsnDomain spsTAsnDomain)throws Exception;
    
    /**
     * 
     * <p>Search ASN for group invoice.</p>
     *
     * @param asnDomain the ASN Domain.
     * @return the list of asn information domain.
     */
    public List<AsnDomain> searchAsnForGroupInvoice(AsnDomain asnDomain);
    
    /**
     * Generate ASN Report using data from ASN Maintenance screen.
     * @param asnMaintenanceDomain data from ASN Maintenance screen.
     * @throws ApplicationException ApplicationException
     * @throws Exception in case Jasper Report Error
     * @return the input stream of ASN report file
     * */
    public InputStream createViewAsnReport(AsnMaintenanceDomain asnMaintenanceDomain)
        throws ApplicationException, Exception;
    
    /**
     * Search purging asn order.
     * Search asn order for purging.
     * 
     * @param purgingDoDomain the Purging DO domain
     * @return the list of Purging DO domain
     */
    public List<PurgingAsnDomain> searchPurgingAsnOrder(PurgingDoDomain purgingDoDomain);
    
    // [IN012] For check delete P/O, D/O
    /**
     * Search count not purging asn order.
     * Search number of ASN that not have to purge.
     * 
     * @param purgingDoDomain the Purging DO domain
     * @return number of ASN that not have to purge
     */
    public Integer searchCountNotPurgingAsn(PurgingDoDomain purgingDoDomain);
    
    /**
     * Search sum shipping qty.
     * Search sum shipping quantity of each asn no.
     * 
     * @param spsTAsnDetailDomain the Sps T Asn Detail Domain
     * @return the list of Sps T Asn Detail Domain
     */
    public List<SpsTAsnDetailDomain> searchSumShippingQty(SpsTAsnDetailDomain spsTAsnDetailDomain);
    
    /**
     * Search sum shipping qty by part.
     * @param spsTAsnDetailDomain the spsTAsnDetailDomain
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain searchSumShippingQtyByPart(SpsTAsnDetailDomain spsTAsnDetailDomain);
    /**
     * <p>Delete Purging ASN.</p>
     * <ul>
     * <li>Delete ASN data for purging.</li>
     * 
     * @param asnNo the asnNo
     * @return the amount of SPS Transaction ASN Domain
     */
    public int deletePurgingAsn(String asnNo);
}