/*
 * ModifyDate Development company     Describe 
 * 2014/06/03 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;

/**
 * <p>The Interface DENSOUserInformationFacadeService.</p>
 * <p>Service for DENSO User about search data from criteria and manipulation data.</p>
 * <ul>
 * <li>Method search  : searchDensoSupplierRelation</li>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchinitialWithCriteria</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedPlantDenso</li>
 * <li>Method search  : searchUserDenso</li>
 * <li>Method update  : deleteUserDenso</li>
 * <li>Method search  : searchUserDensoCSV</li>
 * </ul>
 *
 * @author CSI
 */
public interface DensoUserInformationFacadeService {
    
    
    /**
     * <p>Search DENSO Supplier Relation</p>
     * <ul>
     * <li>Search DENSO supplier relation for initial screen.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain that keep user type, user role, commercial relation 
     * between DENSO and Supplier , and selected Supplier Company.
     * @return dataScopeControlDomain that keep DENSO and Supplier relation.
     * @throws ApplicationException the ApplicationException
     */
    public DataScopeControlDomain searchDensoSupplierRelation(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>initial</p>
     * <ul>
     * <li>Search DENSO company for set in combo box.</li>
     * </ul>
     * 
     * @param dataScopeControlDomain that keep user type, user role, commercial relation 
     * between DENSO and Supplier.
     * @return List that keep combo box data and hidden field data.
     * @throws ApplicationException the ApplicationException
     */
    public  DensoUserInformationDomain searchInitial(DataScopeControlDomain 
        dataScopeControlDomain) throws ApplicationException;
    
    /**
     * <p>initial with criteria.</p>
     * <ul>
     * <li>initial screen with criteria.</li>
     * </ul>
     * 
     * @param densoUserInfoDomain that keep user type, user role, commercial relation 
     * between DENSO and Supplier.
     * @return List that keep combo box data and hidden field data.
     * @throws ApplicationException the ApplicationException
     */
    public  DensoUserInformationDomain searchInitialWithCriteria(DensoUserInformationDomain
        densoUserInfoDomain) throws ApplicationException;
    
    /**
     * <p>Change select DENSO company.</p>
     * <ul>
     * <li>Change select DENSO company.</li>
     * </ul>
     * 
     * @return the list of PlantDensoDomain.
     * @param densoUserInfoDomain the DENSO user info domain.
     * @throws ApplicationException the ApplicationException
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        DensoUserInformationDomain densoUserInfoDomain)throws ApplicationException ;
    
    /**
     * <p>Initial data when change select plant denso code.</p>
     * <ul>
     * <li>Search list of Company DENSO to show in combo box.</li>
     * </ul>
     * 
     * @param plantDensoWithScopeDomain It keep user type, user role,
     * commercial relation between DENSO and Supplier.
     * @return List of CompanyDensoDomain filtered by selected plant DENSO
     * @throws ApplicationException Application Exception
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException;
    
    /**
     * <p>Search DENSO User Detail.</p>
     * <ul>
     * <li>Search DENSO User Item Detail for display on Inquiry Supplier User screen.</li>
     * </ul>
     * 
     * @param densoUserInfoDomain the DENSO User Info domain
     * @return the list of DENSO User Item.
     * @throws ApplicationException ApplicationException
     */
    public DensoUserInformationDomain searchUserDenso(
        DensoUserInformationDomain densoUserInfoDomain)throws ApplicationException;
   
    /**
     * <p>Delete DENSO User</p>
     * <ul>
     * <li>Delete DENSO User Detail</li>
     * </ul>
     * 
     * @param densoUserInfoDomain that keep List of DSC ID that choose from screen 
     * to set active flag to false.
     * @return DensoUserInfoDomain the that keep list of DENSO user and error message list.
     * @throws ApplicationException ApplicationException
     */
    public DensoUserInformationDomain deleteUserDenso(
        DensoUserInformationDomain densoUserInfoDomain)throws ApplicationException;
    
    /**
     * <p>Create Export</p>
     * <ul>
     * <li>Create CSV file for export.</li>
     * </ul>
     * 
     * @param densoUserInformationDomain that keep the criteria to search the DENSO user data 
     * for export CSV.
     * @return densoUserInformationDomain that keep stream of CSV file error message list.
     * @throws ApplicationException the ApplicationException
     */
    public DensoUserInformationDomain searchUserDensoCsv(
        DensoUserInformationDomain densoUserInformationDomain)throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}