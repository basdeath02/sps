/*
 * ModifyDate Development company    Describe 
 * 2014/07/22 CSI Parichat           Create
 * 2015/08/24 CSI Akat               [IN012]
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.FileManagementException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleService;
import com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService;
import com.globaldenso.asia.sps.auto.business.service.SpsTDoService;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * <p>The Class AcknowledgedDoInformationFacadeServiceImpl.</p>
 * <p>Manage data of acknowledged D/O information.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * <li>Method search  : searchAcknowledgedDoInformation</li>
 * <li>Method search  : searchFileName</li>
 * <li>Method search  : searchDeliveryOrderReport</li>
 * <li>Method search  : searchValidateGroupAsn</li>
 * <li>Method search  : searchSelectedCompanyDenso</li>
 * <li>Method search  : searchSelectedCompanySupplier</li>
 * <li>Method search  : searchRoleCanOperate</li>
 * </ul>
 *
 * @author CSI
 */
public class AcknowledgedDoInformationFacadeServiceImpl implements 
    AcknowledgedDoInformationFacadeService {
    
    /** The misc service. */
    private MiscellaneousService miscService;
    
    /** The denso supplier relation service. */
    private DensoSupplierRelationService densoSupplierRelationService;
    
    /** The company supplier service. */
    private CompanySupplierService companySupplierService;
    
    /** The company denso service. */
    private CompanyDensoService companyDensoService;
    
    /** The delivery order service. */
    private DeliveryOrderService deliveryOrderService;
    
    /** The record limit service. */
    private RecordLimitService recordLimitService;
    
    /** The file management service. */
    private FileManagementService fileManagementService;
    
    /** The plant denso service. */
    private PlantDensoService plantDensoService;
    
    /** The plant supplier service. */
    private PlantSupplierService plantSupplierService;
    
    /** The service for SPS_M_ROLETYPE. */
    private SpsMRoleTypeService spsMRoleTypeService;
    
    /** The service for SPS_M_ROLE. */
    private SpsMRoleService spsMRoleService;
    
    /** The service for SPS_M_COMPANY_DENSO. */
    private SpsMCompanyDensoService spsMCompanyDensoService;
    
    /** The SPS transaction do service. */
    private SpsTDoService spsTDoService;
    
    /** The SPS transaction do detail service. */
    private SpsTDoDetailService spsTDoDetailService;
    
    /**
     * Instantiates a new Supplier User Role Assignment Facade service impl.
     */
    public AcknowledgedDoInformationFacadeServiceImpl(){
        super();
    }

    /**
     * Set the misc service.
     * 
     * @param miscService the misc service to set
     */
    public void setMiscService(MiscellaneousService miscService) {
        this.miscService = miscService;
    }
    
    /**
     * Set the denso supplier relation service.
     * 
     * @param densoSupplierRelationService the denso supplier relation service to set
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }
    
    /**
     * Set the company supplier service.
     * 
     * @param companySupplierService the company supplier service to set
     */
    public void setCompanySupplierService(
        CompanySupplierService companySupplierService) {
        this.companySupplierService = companySupplierService;
    }
    
    /**
     * Set the company denso service.
     * 
     * @param companyDensoService the company denso service to set
     */
    public void setCompanyDensoService(CompanyDensoService companyDensoService) {
        this.companyDensoService = companyDensoService;
    }
    
    /**
     * Set the delivery order service.
     * 
     * @param deliveryOrderService the delivery order service to set
     */
    public void setDeliveryOrderService(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }
    
    /**
     * Set the record limit service.
     * 
     * @param recordLimitService the record limit service to set
     */
    public void setRecordLimitService(RecordLimitService recordLimitService) {
        this.recordLimitService = recordLimitService;
    }
    
    /**
     * Set the file management service.
     * 
     * @param fileManagementService the file management service to set
     */
    public void setFileManagementService(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }
    
    /**
     * Set the plant denso service.
     * 
     * @param plantDensoService the plant denso service to set
     */
    public void setPlantDensoService(PlantDensoService plantDensoService) {
        this.plantDensoService = plantDensoService;
    }

    /**
     * Set the plant supplier service.
     * 
     * @param plantSupplierService the plant supplier service to set
     */
    public void setPlantSupplierService(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }
    
    /**
     * <p>Setter method for spsMRoleTypeService.</p>
     *
     * @param spsMRoleTypeService Set for spsMRoleTypeService
     */
    public void setSpsMRoleTypeService(SpsMRoleTypeService spsMRoleTypeService) {
        this.spsMRoleTypeService = spsMRoleTypeService;
    }

    /**
     * <p>Setter method for spsMRoleService.</p>
     *
     * @param spsMRoleService Set for spsMRoleService
     */
    public void setSpsMRoleService(SpsMRoleService spsMRoleService) {
        this.spsMRoleService = spsMRoleService;
    }
    
    /**
     * <p>Setter method for spsMCompanyDensoService.</p>
     *
     * @param spsMCompanyDensoService Set for spsMCompanyDensoService
     */
    public void setSpsMCompanyDensoService(
        SpsMCompanyDensoService spsMCompanyDensoService) {
        this.spsMCompanyDensoService = spsMCompanyDensoService;
    }
    
    /**
     * Set the SPS transaction do service.
     * 
     * @param spsTDoService the SPS transaction do service to set
     */
    public void setSpsTDoService(SpsTDoService spsTDoService) {
        this.spsTDoService = spsTDoService;
    }
    
    /**
     * Set the SPS transaction do detail service.
     * 
     * @param spsTDoDetailService the SPS transaction do detail service to set
     */
    public void setSpsTDoDetailService(SpsTDoDetailService spsTDoDetailService) {
        this.spsTDoDetailService = spsTDoDetailService;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchInitial(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_RAWTYPES)
    public AcknowledgedDoInformationDomain searchInitial(
        AcknowledgedDoInformationDomain acknowledgedDoInformation) throws ApplicationException
    {
        List<MiscellaneousDomain> transList = null;
        List<MiscellaneousDomain> orderMethodList = null;
        List<MiscellaneousDomain> receiveByScanList = null;
        List<MiscellaneousDomain> ltFlagList = null;
        List<MiscellaneousDomain> shipmentStatusList = null;
        MiscellaneousDomain miscDomain = new MiscellaneousDomain();
        
        Locale locale = acknowledgedDoInformation.getLocale();
        DataScopeControlDomain dataScopeControlDomain = 
            acknowledgedDoInformation.getDataScopeControlDomain();
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(null != densoSupplierRelationList 
            && Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanySupplierDomain> companySupplierDomainList
            = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if(null == companySupplierDomainList || Constants.ZERO == companySupplierDomainList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        
        plantSupplierWithScope.getPlantSupplierDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantSupplierDomain> plantSupplierList
            = this.plantSupplierService.searchPlantSupplier(plantSupplierWithScope);
        if(null == plantSupplierList || plantSupplierList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        
        PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
        plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        List<CompanyDensoDomain> companyDensoDomainList
            = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if(null == companyDensoDomainList || Constants.ZERO == companyDensoDomainList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        
        plantDensoWithScope.getPlantDensoDomain().setIsActive(Constants.IS_ACTIVE);
        List<PlantDensoDomain> plantDensoList
            = this.plantDensoService.searchPlantDenso(plantDensoWithScope);
        if(null == plantDensoList || plantDensoList.size() <= Constants.ZERO){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        
        miscDomain = new MiscellaneousDomain();
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_SHIP_STS_CB);
        shipmentStatusList = miscService.searchMisc(miscDomain);
        if(null != shipmentStatusList && Constants.ZERO < shipmentStatusList.size())
        {
            StringBuffer shipStatusValue = new StringBuffer();
            Iterator itr = shipmentStatusList.iterator();
            while(itr.hasNext()){
                MiscellaneousDomain shipmentStatus = (MiscellaneousDomain)itr.next();
                
                // [IN012] Remove CPS and CCL
                //if(!Constants.SHIPMENT_STATUS_CPS.equals(shipmentStatus.getMiscCode())){
                if(!Constants.SHIPMENT_STATUS_CPS.equals(shipmentStatus.getMiscCode())
                    && !Constants.SHIPMENT_STATUS_CCL.equals(shipmentStatus.getMiscCode()))
                {
                    
                    shipStatusValue.append(shipmentStatus.getMiscCode())
                        .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_COLON)
                        .append(Constants.SYMBOL_SPACE).append(shipmentStatus.getMiscValue());
                    shipmentStatus.setMiscValue(shipStatusValue.toString());
                    shipStatusValue.setLength(Constants.ZERO);
                }else{
                    itr.remove();
                }
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SHIPMENT_STATUS);
        }
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_TRANS_CB);
        transList = miscService.searchMisc(miscDomain);
        if(null != transList && Constants.ZERO < transList.size())
        {
            StringBuffer transValue = new StringBuffer();
            for(MiscellaneousDomain trans : transList){
                transValue.append(trans.getMiscCode()).append(Constants.SYMBOL_SPACE)
                    .append(Constants.SYMBOL_COLON).append(Constants.SYMBOL_SPACE)
                    .append(trans.getMiscValue());
                trans.setMiscValue(transValue.toString());
                transValue.setLength(Constants.ZERO);
            }
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_TM);
        }
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_ORDER_MTH_CB);
        orderMethodList = miscService.searchMisc(miscDomain);
        if(null == orderMethodList || Constants.ZERO == orderMethodList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_ORDER_METHOD);
        }
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_RECEIVE_BY_SCAN);
        receiveByScanList = miscService.searchMisc(miscDomain);
        if(null == receiveByScanList || Constants.ZERO == receiveByScanList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_RECEIVE_BY_SCAN);
        }
        miscDomain.setMiscType(SupplierPortalConstant.MISC_TYPE_LT_FLAG);
        ltFlagList = miscService.searchMisc(miscDomain);
        if(null == ltFlagList || Constants.ZERO == ltFlagList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_LT_FLAG);
        }
        
        acknowledgedDoInformation.setCompanySupplierList(companySupplierDomainList);
        acknowledgedDoInformation.setCompanyDensoList(companyDensoDomainList);
        acknowledgedDoInformation.setPlantSupplierList(plantSupplierList);
        acknowledgedDoInformation.setPlantDensoList(plantDensoList);
        acknowledgedDoInformation.setShipmentStatusList(shipmentStatusList);
        acknowledgedDoInformation.setTransList(transList);
        acknowledgedDoInformation.setOrderMethodList(orderMethodList);
        acknowledgedDoInformation.setReceiveByScanList(receiveByScanList);
        acknowledgedDoInformation.setLtFlagList(ltFlagList);
        return acknowledgedDoInformation;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchAcknowledgedDoInformation(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationAction)
     */
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDoInformation (
        AcknowledgedDoInformationDomain acknowledgedDOInformationDomain)
        throws ApplicationException
    {
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInformationResultList = null;
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInfoReturnList = 
            new ArrayList<AcknowledgedDoInformationReturnDomain>();
        MiscellaneousDomain recordLimit = new MiscellaneousDomain();
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        Locale locale = acknowledgedDOInformationDomain.getLocale();
        
        //validate
        boolean resultValidate = this.validate(acknowledgedDOInformationDomain);
        if(resultValidate){
            //search
            Integer recordCount = deliveryOrderService.searchCountAcknowledgedDo(
                acknowledgedDOInformationDomain);
            if(Constants.ZERO < recordCount){
                recordLimit.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM);
                recordLimit.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP001_RLM);
                recordLimit = recordLimitService.searchRecordLimit(recordLimit);
                if(null == recordLimit || StringUtil.checkNullOrEmpty(
                    recordLimit.getMiscValue())){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                if(Integer.parseInt(recordLimit.getMiscValue()) < recordCount){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0003, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[]{recordLimit.getMiscValue()});
                }
                recordLimitPerPage.setMiscType(SupplierPortalConstant.MISC_TYPE_SCR_REC_LM_PG);
                recordLimitPerPage.setMiscCode(SupplierPortalConstant.MISC_CODE_WSHP001_PLM);
                recordLimitPerPage = recordLimitService.searchRecordLimitPerPage(
                    recordLimitPerPage);
                if(null == recordLimitPerPage || StringUtil.checkNullOrEmpty(
                    recordLimitPerPage.getMiscValue())){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0032, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                acknowledgedDOInformationResultList = deliveryOrderService.
                    searchAcknowledgedDo(acknowledgedDOInformationDomain);
                int max = acknowledgedDOInformationResultList.size();
                acknowledgedDOInformationDomain.setMaxRowPerPage(Integer.valueOf(
                    recordLimitPerPage.getMiscValue()));
                SpsPagingUtil.calcPaging(acknowledgedDOInformationDomain, max);
                int rowNumfrom 
                    = acknowledgedDOInformationDomain.getRowNumFrom() - Constants.ONE;
                int rowNumTo = acknowledgedDOInformationDomain.getRowNumTo();
                for(int i = rowNumfrom; i <  rowNumTo; i++){
                    AcknowledgedDoInformationReturnDomain acknowledgedDOInfo = 
                        acknowledgedDOInformationResultList.get(i);
                    if(null != acknowledgedDOInfo.getAsnDetailDomainList() 
                        && Constants.ZERO < acknowledgedDOInfo.getAsnDetailDomainList().size())
                    {
                        acknowledgedDOInfo.setShipNotice(Constants.SHIP_NOTICE_FLAG_YES);
                    }else{
                        acknowledgedDOInfo.setShipNotice(Constants.SHIP_NOTICE_FLAG_NO);
                    }
                    acknowledgedDOInfoReturnList.add(acknowledgedDOInfo);
                }
            }else{
                MessageUtil.throwsApplicationMessage(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002);
            }
        }else{
            acknowledgedDOInfoReturnList = null;
        }
        return acknowledgedDOInfoReturnList;
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchFileName(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public FileManagementDomain searchFileName(AcknowledgedDoInformationDomain
        acknowledgedDOInformation) throws ApplicationException
    {
        Locale locale = acknowledgedDOInformation.getLocale();
        FileManagementDomain resultDomain = null;
        try{
            resultDomain = fileManagementService.searchFileDownload(
                acknowledgedDOInformation.getPdfFileId(), false, null);
        }catch(FileManagementException ex){
            MessageUtil.throwsApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }catch(IOException io){
            MessageUtil.throwsApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
        return resultDomain;
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchDeliveryOrderReport(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain, OutputStream)
     */
    public void searchDeliveryOrderReport (AcknowledgedDoInformationDomain 
        acknowledgedDOInformation, OutputStream outputStream) throws ApplicationException
    {
        Locale locale = acknowledgedDOInformation.getLocale();
        try{
            fileManagementService.searchFileDownload(acknowledgedDOInformation.getPdfFileId(),
                true, outputStream);
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
        }
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchLegendInfo(com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain, OutputStream)
     */
    public void searchLegendInfo(AcknowledgedDoInformationDomain acknowledgedDOInformation,
        OutputStream outputStream) throws ApplicationException
    {
        Locale locale = acknowledgedDOInformation.getLocale();
        try{
            fileManagementService.searchFileDownload(acknowledgedDOInformation.getPdfFileId(),
                true, outputStream);
        }catch(IOException e) {
            MessageUtil.throwsApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009,
                SupplierPortalConstant.ERROR_CD_SP_90_0002);
        }
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchValidateGroupAsn(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public void searchValidateGroupAsn(AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException
    {
        boolean isTheSameSupplierCode = true;
        boolean isTheSameSupplierPlantCode = true;
        boolean isTheSameDensoCode = true;
        boolean isTheSameDensoPlantCode = true;
        boolean isTheSameTrans = true;
        boolean isTheSameReceiveByScan = true;
        boolean isTheSamePrintOneWayKanbanTag = true;
        boolean isTheSameOrderMethod = true;
        boolean isValidShipStatus = true;
        boolean isTheSameWhPrimeReceiving = true;
        
        String sCd = Constants.EMPTY_STRING;
        String sPcd = Constants.EMPTY_STRING;
        String dCd = Constants.EMPTY_STRING;
        String dPcd = Constants.EMPTY_STRING;
        String trans = Constants.EMPTY_STRING;
        String orderMethod = Constants.EMPTY_STRING;
        String shipStatus = Constants.EMPTY_STRING;
        String receiveByScan = Constants.EMPTY_STRING;
        String whPrimeReceiving = Constants.EMPTY_STRING;
        
        List<ApplicationMessageDomain> errorMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<ApplicationMessageDomain> warningMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInformationList 
            = acknowledgedDoInformation.getAcknowledgedDoInformationList();
        List<String> doNoDidNotPrintKanbanTag = new ArrayList<String>();
        
        Locale locale = acknowledgedDoInformation.getLocale();
        
        sCd = acknowledgedDOInformationList.get(Constants.ZERO).getDeliveryOrderDomain()
            .getVendorCd();
        sPcd = acknowledgedDOInformationList.get(Constants.ZERO).getSPcd();
        dCd = acknowledgedDOInformationList.get(Constants.ZERO).getDCd();
        dPcd = acknowledgedDOInformationList.get(Constants.ZERO).getDPcd();
        trans = acknowledgedDOInformationList.get(Constants.ZERO).getDeliveryOrderDomain().getTm();
        orderMethod = acknowledgedDOInformationList.get(Constants.ZERO).getDeliveryOrderDomain()
            .getOrderMethod();
        shipStatus = acknowledgedDOInformationList.get(Constants.ZERO).getDeliveryOrderDomain()
            .getShipmentStatus();
        receiveByScan = acknowledgedDOInformationList.get(Constants.ZERO).getReceiveByScan();
        whPrimeReceiving = acknowledgedDOInformationList.get(Constants.ZERO).getDeliveryOrderDomain().getWhPrimeReceiving();
        
        // [IN012] CPS and CCL are invalid ship status
        //if(Constants.SHIPMENT_STATUS_CPS.equals(shipStatus)){
        if(Constants.SHIPMENT_STATUS_CPS.equals(shipStatus)
            || Constants.SHIPMENT_STATUS_CCL.equals(shipStatus))
        {
            
            isValidShipStatus = false;
        }
        int i = Constants.ZERO;
//        while((i < acknowledgedDOInformationList.size()) && (isTheSameSupplierCode 
//            || isTheSameSupplierPlantCode || isTheSameDensoCode || isTheSameDensoPlantCode 
//            || isTheSameTrans || isTheSameOrderMethod || isValidShipStatus || !isTheSamePrintOneWayKanbanTag))
        while(i < acknowledgedDOInformationList.size())
        {
            AcknowledgedDoInformationReturnDomain doInfo = acknowledgedDOInformationList.get(i);
            
            if(!sCd.equals(doInfo.getDeliveryOrderDomain().getVendorCd())){
                isTheSameSupplierCode = false;    
            }
            if(!sPcd.equals(doInfo.getSPcd())){
                isTheSameSupplierPlantCode = false;
            }
            if(!dCd.equals(doInfo.getDCd())){
                isTheSameDensoCode = false;
            }
            if(!dPcd.equals(doInfo.getDPcd())){
                isTheSameDensoPlantCode = false;
            }
//            DENSO user need to remove this condition
//            On 2/2/2018
//            Add code for check if all part is kanban type 4 or not.
//            boolean isSkipForAllKanbanType4 = true;
//            SpsTDoDetailCriteriaDomain criteria = new SpsTDoDetailCriteriaDomain();
//            criteria.setDoId(doInfo.getDeliveryOrderDomain().getDoId());
//            List<SpsTDoDetailDomain> searchKanbanType = spsTDoDetailService.searchByCondition(criteria);
//            for(SpsTDoDetailDomain searchKanbanTypeEach : searchKanbanType){
//                if(!searchKanbanTypeEach.getKanbanType().equals(Constants.STR_FOUR)){
//                    isSkipForAllKanbanType4 = false;
//                }
//            }
//            if(doInfo.getReceiveByScan().equals(Constants.STR_Y) && doInfo.getIsPrintOneWayKanbanTag().equals(Constants.STR_N) 
//                && !doInfo.getDeliveryOrderDomain().getOneWayKanbanTagFlag().equals(Constants.STR_NINE) && !isSkipForAllKanbanType4){
//                isTheSamePrintOneWayKanbanTag = false;
//                doNoDidNotPrintKanbanTag.add(doInfo.getDeliveryOrderDomain().getCigmaDoNo());
//            }
            if(!receiveByScan.equals(doInfo.getReceiveByScan())){
                isTheSameReceiveByScan = false;
            }
            if(!trans.equals(doInfo.getDeliveryOrderDomain().getTm())){
                isTheSameTrans = false;
            }
            if(!orderMethod.equals(doInfo.getDeliveryOrderDomain().getOrderMethod())){
                isTheSameOrderMethod = false;
            }
            if(!whPrimeReceiving.equals(doInfo.getDeliveryOrderDomain().getWhPrimeReceiving()) 
                && doInfo.getReceiveByScan().equals(Constants.STR_Y)){
                isTheSameWhPrimeReceiving = false;
            }
            
            // [IN012] CPS and CCL are invalid ship status
            //if(Constants.SHIPMENT_STATUS_CPS.equals(
            //    doInfo.getDeliveryOrderDomain().getShipmentStatus())){
            if(Constants.SHIPMENT_STATUS_CPS.equals(
                doInfo.getDeliveryOrderDomain().getShipmentStatus())
                || Constants.SHIPMENT_STATUS_CCL.equals(
                    doInfo.getDeliveryOrderDomain().getShipmentStatus()))
            {
                
                isValidShipStatus = false;
            }
            i++;
        }
        if(!isValidShipStatus){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_SHIP_STATUS_INVALID)));
        }
        if(!isTheSameSupplierCode){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_S_CD_NOT_SAME)));
        }
        if(!isTheSameSupplierPlantCode){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_S_PCD_NOT_SAME)));
        }
        if(!isTheSameDensoCode){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_D_CD_NOT_SAME)));
        }
        if(!isTheSameDensoPlantCode){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_D_PCD_NOT_SAME)));
        }
        if(!isTheSameWhPrimeReceiving){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_WH_PRIME_RECEIVING_NOT_SAME)));
        }
        if(!isTheSameReceiveByScan){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                    SupplierPortalConstant.LBL_RECEIVE_BY_SCAN_NOT_SAME)));
        }
        if(!isTheSamePrintOneWayKanbanTag){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0042)
                    + doNoDidNotPrintKanbanTag.toString()));
        }
        if(!isTheSameTrans){
            warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_W6_0011,
                    SupplierPortalConstant.LBL_TRANS_NOT_SAME)));
        }
        if(isTheSameDensoCode && !isTheSameOrderMethod){
            SpsMCompanyDensoCriteriaDomain criteria = new SpsMCompanyDensoCriteriaDomain();
            criteria.setDCd(dCd);
            SpsMCompanyDensoDomain companyDensoDomain
                = spsMCompanyDensoService.searchByKey(criteria);
            if(null == companyDensoDomain
                || StringUtil.checkNullOrEmpty(companyDensoDomain.getGroupAsnErrorType())){
                MessageUtil.throwsApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                    SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
            }
            if(SupplierPortalConstant.GROUP_ASN_WARNING_TYPE.equals(
                companyDensoDomain.getGroupAsnErrorType())){
                warningMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_WARNING
                    , MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_W6_0011,
                        SupplierPortalConstant.LBL_ORDER_METHOD_NOT_SAME)));
            }else{
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                    , MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0006,
                        SupplierPortalConstant.LBL_ORDER_METHOD_NOT_SAME)));
            }
        }
        if(Constants.ZERO < errorMessageList.size()){
            acknowledgedDoInformation.setErrorMessageList(errorMessageList);
        }
        if(Constants.ZERO < warningMessageList.size()){
            acknowledgedDoInformation.setWarningMessageList(warningMessageList);
        }
    }
    

    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#isAmountPartGroupByRcvLaneOverQrCode(com.globaldenso.asia.sps.business.domain.SpsTDoDomain)
     */
    
    public void isAmountPartGroupByRcvLaneOverQrCode(
        AcknowledgedDoInformationDomain acknowledgedDoInformationDomain, InquiryDoDetailDomain inquiryDODetailDomain) throws ApplicationException
    {
        boolean isAmountPartGroupByRcvLaneOverQrCode = true;
        List<ApplicationMessageDomain> errorMessageList 
            = acknowledgedDoInformationDomain.getErrorMessageList();
        List<ApplicationMessageDomain> warningMessageList 
            = acknowledgedDoInformationDomain.getWarningMessageList();
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInformationList 
            = acknowledgedDoInformationDomain.getAcknowledgedDoInformationList();
    
        Locale locale = acknowledgedDoInformationDomain.getLocale();
        List<String> rcvLane = new ArrayList<String>();
        List<String> rcvLaneError = new ArrayList<String>();
        Map<String, Integer> quantityEachRcvLane = new HashMap<String, Integer>();
        
        if(errorMessageList == null){
            errorMessageList = new ArrayList<ApplicationMessageDomain>();
        }
        if(warningMessageList == null){
            warningMessageList = new ArrayList<ApplicationMessageDomain>();
        }
        int i = Constants.ZERO;
//      while((i < acknowledgedDOInformationList.size()) && (isTheSameSupplierCode 
//          || isTheSameSupplierPlantCode || isTheSameDensoCode || isTheSameDensoPlantCode 
//          || isTheSameTrans || isTheSameOrderMethod || isValidShipStatus || !isTheSamePrintOneWayKanbanTag))
        while(i < acknowledgedDOInformationList.size()){
            if(acknowledgedDOInformationList.get(i).getReceiveByScan().equals(Constants.STR_Y)){
                inquiryDODetailDomain.setDoId(acknowledgedDOInformationList.get(i).getDeliveryOrderDomain().getDoId().toString());
//                Remove because this command retrieve duplicate inforamtion
//                List<InquiryDoDetailReturnDomain> inquiryDoDetailReturnDomain =
//                    deliveryOrderService.searchDoDetail(inquiryDODetailDomain);
//                Check for ISSUE PN status
                SpsTDoDetailCriteriaDomain criteria = new SpsTDoDetailCriteriaDomain();
                criteria.setDoId(acknowledgedDOInformationList.get(i).getDeliveryOrderDomain().getDoId());
                criteria.setPnShipmentStatus(Constants.SHIPMENT_STATUS_ISS);
                List<SpsTDoDetailDomain> inquiryDoDetailReturnDomain = 
                    spsTDoDetailService.searchByCondition(criteria);
                int j = Constants.ZERO;
                while(j < inquiryDoDetailReturnDomain.size()){
//                    if(!quantityEachRcvLane.containsKey(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane())){
//                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane(), Constants.ONE);
//                        rcvLane.add(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane());
//                    } else {
//                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane(), quantityEachRcvLane.get(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane()) + 1);
//                    }
                    if(!quantityEachRcvLane.containsKey(inquiryDoDetailReturnDomain.get(j).getRcvLane())){
                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getRcvLane(), Constants.ONE);
                        rcvLane.add(inquiryDoDetailReturnDomain.get(j).getRcvLane());
                    } else {
                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getRcvLane(), quantityEachRcvLane.get(inquiryDoDetailReturnDomain.get(j).getRcvLane()) + 1);
                    }
                    j++;
                }
//                Check for PARTIAL SHIPMENT PN status
                criteria.setPnShipmentStatus(Constants.SHIPMENT_STATUS_PTS);
                inquiryDoDetailReturnDomain = 
                    spsTDoDetailService.searchByCondition(criteria);
                j = Constants.ZERO;
                while(j < inquiryDoDetailReturnDomain.size()){
//                    if(!quantityEachRcvLane.containsKey(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane())){
//                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane(), Constants.ONE);
//                        rcvLane.add(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane());
//                    } else {
//                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane(), quantityEachRcvLane.get(inquiryDoDetailReturnDomain.get(j).getDoDetailDomain().getRcvLane()) + 1);
//                    }
                    if(!quantityEachRcvLane.containsKey(inquiryDoDetailReturnDomain.get(j).getRcvLane())){
                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getRcvLane(), Constants.ONE);
                        rcvLane.add(inquiryDoDetailReturnDomain.get(j).getRcvLane());
                    } else {
                        quantityEachRcvLane.put(inquiryDoDetailReturnDomain.get(j).getRcvLane(), quantityEachRcvLane.get(inquiryDoDetailReturnDomain.get(j).getRcvLane()) + 1);
                    }
                    j++;
                }
            }
            i++;
        }
        i = Constants.ZERO;
        while(i < rcvLane.size()){
            if(SupplierPortalConstant.MAX_RECORD_FOR_RECEIVE_BY_SCAN <= quantityEachRcvLane.get(rcvLane.get(i))){
                isAmountPartGroupByRcvLaneOverQrCode = false;
                rcvLaneError.add(rcvLane.get(i));
            }
            i++;
        }
        if(!isAmountPartGroupByRcvLaneOverQrCode){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE
                , MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0043, SupplierPortalConstant.STR_MAX_RECORD_FOR_RECEIVE_BY_SCAN)
                    + rcvLaneError.toString()));
        }
        if(Constants.ZERO < errorMessageList.size()){
            acknowledgedDoInformationDomain.setErrorMessageList(errorMessageList);
        }
        if(Constants.ZERO < warningMessageList.size()){
            acknowledgedDoInformationDomain.setWarningMessageList(warningMessageList);
        }
    }

    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchSelectedCompanyDenso(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public List<PlantDensoDomain> searchSelectedCompanyDenso(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain) throws ApplicationException
    {
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = plantDensoWithScopeDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScopeDomain.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScopeDomain.getPlantDensoDomain();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList
            = densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(null != densoSupplierRelationList && Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        plantDensoList = plantDensoService.searchPlantDenso(plantDensoWithScopeDomain);
        if(null == plantDensoList || Constants.ZERO == plantDensoList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_PLANT_CODE);
        }
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchSelectedPlantDenso(com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public List<CompanyDensoDomain> searchSelectedPlantDenso(
        PlantDensoWithScopeDomain plantDensoWithScope) throws ApplicationException
    {
        Locale locale = plantDensoWithScope.getLocale();
        List<CompanyDensoDomain> result = new ArrayList<CompanyDensoDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantDensoWithScope.getDataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = plantDensoWithScope.getPlantDensoDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantDensoDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companyDensoService.searchCompanyDenso(plantDensoWithScope);
        if (null == result || Constants.ZERO == result.size()) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_DENSO_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @throws ApplicationException ApplicationException
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchSelectedCompanySupplier(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public List<PlantSupplierDomain> searchSelectedCompanySupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) throws ApplicationException
    {
        List<PlantSupplierDomain> plantSupplierList = null;
        Locale locale = plantSupplierWithScopeDomain.getLocale();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantSupplierWithScopeDomain.getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain
            = plantSupplierWithScopeDomain.getPlantSupplierDomain();
        
        List<DensoSupplierRelationDomain> densoSupplierRelationList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        if(null != densoSupplierRelationList && Constants.ZERO < densoSupplierRelationList.size()){
            dataScopeControlDomain.setDensoSupplierRelationDomainList(
                densoSupplierRelationList);
        }else{
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
       
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierList = plantSupplierService.searchPlantSupplier(plantSupplierWithScopeDomain);
        if(null == plantSupplierList || Constants.ZERO == plantSupplierList.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_PLANT_CODE);
        }
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchSelectedPlantSupplier(com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public List<CompanySupplierDomain> searchSelectedPlantSupplier(
        PlantSupplierWithScopeDomain plantSupplierWithScope) throws ApplicationException
    {
        Locale locale = plantSupplierWithScope.getLocale();
        List<CompanySupplierDomain> result = new ArrayList<CompanySupplierDomain>();
        
        DataScopeControlDomain dataScopeControlDomain
            = plantSupplierWithScope.getDataScopeControlDomain();
        PlantSupplierDomain plantSupplierDomain = plantSupplierWithScope.getPlantSupplierDomain();
        
        /*Get relation between DENSO and Supplier*/
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList = 
            densoSupplierRelationService.searchDensoSupplierRelation(dataScopeControlDomain);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(densoSupplierRelationDomainList);
        if (dataScopeControlDomain.getDensoSupplierRelationDomainList().size() <= Constants.ZERO) {
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0013,
                SupplierPortalConstant.LBL_DENSO_AND_SUPPLIER_RELATION);
        }
        
        /* Get list of supplier plant information*/
        plantSupplierDomain.setIsActive(Constants.IS_ACTIVE);
        result = this.companySupplierService.searchCompanySupplier(plantSupplierWithScope);
        if(null == result || Constants.ZERO == result.size()){
            MessageUtil.throwsApplicationMessageWithLabel(locale,
                SupplierPortalConstant.ERROR_CD_SP_E6_0031,
                SupplierPortalConstant.LBL_SUPPLIER_COMPANY_CODE);
        }
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService#searchRoleCanOperate
     * (java.lang.String,java.util.Locale)
     */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException
    {
        SpsMRoleTypeCriteriaDomain roleTypeCriteria = new SpsMRoleTypeCriteriaDomain();
        roleTypeCriteria.setRoleTypeName(roleTypeName);
        
        List<SpsMRoleTypeDomain> roleTypeList
            = this.spsMRoleTypeService.searchByCondition(roleTypeCriteria);
        
        SpsMRoleCriteriaDomain roleCriteria = null;
        SpsMRoleDomain role = null;
        List<String> roleNameList = new ArrayList<String>();
        
        for (SpsMRoleTypeDomain roleType : roleTypeList) {
            roleCriteria = new SpsMRoleCriteriaDomain();
            roleCriteria.setRoleCd(roleType.getRoleCode());
            role = this.spsMRoleService.searchByKey(roleCriteria);
            roleNameList.add(role.getRoleName());
        }
        
        String allRoleName = StringUtil.convertListToVarcharCommaSeperate(roleNameList);
        
        MessageUtil.throwsApplicationMessageHandledException(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E9_0004, new String[] {allRoleName});
    }
    
    /**
     * Validate
     * @return the boolean
     * @param acknowledgedDOInformation the acknowledged do information domain
     */
    private boolean validate(AcknowledgedDoInformationDomain acknowledgedDOInformation)
    {
        boolean result = true;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = acknowledgedDOInformation.getLocale();
        String message = Constants.EMPTY_STRING;
        boolean isValidDateFrom = false;
        boolean isValidDateTo = false;
        if(StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getVendorCd())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getSPcd())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDCd())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDPcd())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryDateFrom())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryDateTo())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getShipmentStatus())
            || StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getReceiveByScan())){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_E7_0011)));
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryDateFrom())){
            if(!DateUtil.isValidDate(acknowledgedDOInformation.getDeliveryDateFrom(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH)){
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_DELIVERY_DATE_FROM);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryDateTo())){
            if(!DateUtil.isValidDate(acknowledgedDOInformation.getDeliveryDateTo(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_DELIVERY_DATE_TO);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryDateFrom())
            && !StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryDateTo())
            && isValidDateFrom && isValidDateTo)
        {
            if(Constants.ZERO < DateUtil.compareDate(acknowledgedDOInformation.getDeliveryDateFrom()
                , acknowledgedDOInformation.getDeliveryDateTo()))
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_DELIVERY_DATE_FROM),
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_DELIVERY_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryTimeFrom())
            && !StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryTimeTo())){
            boolean isValidDeliveryTimeFrom = true;
            boolean isValidDeliveryTimeTo = true;
            if(!acknowledgedDOInformation.getDeliveryTimeFrom().matches(
                Constants.REGX_TIME24HOURS_FORMAT))
            {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_TIME_FROM)));
                isValidDeliveryTimeFrom = false;
            }
            if(!acknowledgedDOInformation.getDeliveryTimeTo().matches(
                Constants.REGX_TIME24HOURS_FORMAT)){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0023, 
                        SupplierPortalConstant.LBL_DELIVERY_TIME_TO)));
                isValidDeliveryTimeTo = false;
            }
            if(isValidDeliveryTimeFrom && isValidDeliveryTimeTo){
                Date d1 = DateUtil.parseToUtilDate(
                    acknowledgedDOInformation.getDeliveryTimeFrom(), DateUtil.PATTERN_HHMM_COLON);
                Date d2 = DateUtil.parseToUtilDate(
                    acknowledgedDOInformation.getDeliveryTimeTo(), DateUtil.PATTERN_HHMM_COLON);
                if(d1.after(d2)){
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(locale,
                            SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                                MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_DELIVERY_TIME_FROM),
                                    MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_DELIVERY_TIME_TO)})));
                }
            }
        }else{
            if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryTimeFrom())
                || !StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDeliveryTimeTo())){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0007,
                        SupplierPortalConstant.LBL_BOTH_DELIVERY_TIME_FROM_AND_TO)));
            }
        }
        
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getShipDateFrom())){
            if(!DateUtil.isValidDate(acknowledgedDOInformation.getShipDateFrom(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_SHIPMENT_DATE_FROM);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getShipDateTo())){
            if(!DateUtil.isValidDate(acknowledgedDOInformation.getShipDateTo(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_SHIPMENT_DATE_TO);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getShipDateFrom())
            && !StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getShipDateTo())
            && isValidDateFrom && isValidDateTo)
        {
            if(Constants.ZERO < DateUtil.compareDate(acknowledgedDOInformation.getShipDateFrom(), 
                acknowledgedDOInformation.getShipDateTo()))
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_SHIPMENT_DATE_FROM),
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_SHIPMENT_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getIssueDateFrom())){
            if(!DateUtil.isValidDate(acknowledgedDOInformation.getIssueDateFrom(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ISSUE_DATE_FROM);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateFrom = false;
            }else{
                isValidDateFrom = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getIssueDateTo())){
            if(!DateUtil.isValidDate(acknowledgedDOInformation.getIssueDateTo(), 
                DateUtil.PATTERN_YYYYMMDD_SLASH))
            {
                message = MessageUtil.getApplicationMessageWithLabel(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0023,
                    SupplierPortalConstant.LBL_ISSUE_DATE_TO);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
                isValidDateTo = false;
            }else{
                isValidDateTo = true;
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getIssueDateFrom())
            && !StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getIssueDateTo())
            && isValidDateFrom && isValidDateTo)
        {
            if(Constants.ZERO < DateUtil.compareDate(acknowledgedDOInformation.getIssueDateFrom(), 
                acknowledgedDOInformation.getIssueDateTo()))
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0031, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ISSUE_DATE_FROM),
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ISSUE_DATE_TO)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getSpsDoNo())){
            if(Constants.MAX_SPS_DO_NO_LENGTH < acknowledgedDOInformation.getSpsDoNo().length())
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_SPS_DO_NO),
                            String.valueOf(Constants.MAX_SPS_DO_NO_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getCigmaDoNo())){
            if(Constants.MAX_CIGMA_DO_NO_LENGTH < acknowledgedDOInformation.getCigmaDoNo().length())
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_CIGMA_DO_NO),
                            String.valueOf(Constants.MAX_CIGMA_DO_NO_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getRouteNo())){
            if(Constants.MAX_ROUTE_LENGTH < acknowledgedDOInformation.getRouteNo().length())
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_ROUTE),
                            String.valueOf(Constants.MAX_ROUTE_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(!StringUtil.checkNullOrEmpty(acknowledgedDOInformation.getDel())){
            if(Constants.MAX_DEL_LENGTH < acknowledgedDOInformation.getDel().length())
            {
                message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0030, new String[]{
                        MessageUtil.getLabelHandledException(locale,
                            SupplierPortalConstant.LBL_DEL),
                            String.valueOf(Constants.MAX_DEL_LENGTH)});
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    message));
            }
        }
        if(Constants.ZERO < errorMessageList.size()){
            result = false;
        }
        acknowledgedDOInformation.setErrorMessageList(errorMessageList);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#createOneWayKanbanTagReport(com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain)
     */
    public InputStream createOneWayKanbanTagReport(
        AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException {
        
        InputStream report = null;
        Locale locale = acknowledgedDoInformation.getLocale();
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
        deliveryOrderInformationDomain.setDoId(acknowledgedDoInformation.getDoId());
        List<OneWayKanbanTagReportDomain> oneWayKanbanTagReportDomain = deliveryOrderService
            .searchOneWayKanbanTagReportInformation(deliveryOrderInformationDomain);
        try {
            report = deliveryOrderService.createOneWayKanbanTagReport(oneWayKanbanTagReportDomain);
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0017);
        }
        return report;
    }    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService#updateDateTimeKanbanPdf(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer updateDateTimeKanbanPdf(
        AcknowledgedDoInformationDomain acknowledgedDoInformation)
        throws ApplicationException {
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = new DeliveryOrderInformationDomain();
        deliveryOrderInformationDomain.setDoId(acknowledgedDoInformation.getDoId());
        /** Count Delivery Order return record from DeliveryOrderService. */
        Integer resultCode = deliveryOrderService.updateDateTimeKanbanPdf(deliveryOrderInformationDomain);
        return resultCode;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService#searchGenerateDo(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public FileManagementDomain searchGenerateDo(
        AcknowledgedDoInformationDomain acknowledgedDoInformation) throws ApplicationException
    {
        BigDecimal doId = new BigDecimal(acknowledgedDoInformation.getPdfFileId());
        Locale locale = acknowledgedDoInformation.getLocale();
//        Generate Report.
        InputStream isReportData = null;
        StringBuffer filenameReport = new StringBuffer();
        try {
            SpsTDoCriteriaDomain criteria = new SpsTDoCriteriaDomain();
            criteria.setDoId(doId);
            List<SpsTDoDomain> spsTDoDomainList = spsTDoService.searchByCondition(criteria);
            if(Constants.DDO_TYPE.equals(spsTDoDomainList.get(Constants.ZERO).getDataType())){
                DeliveryOrderSubDetailDomain doReport = new DeliveryOrderSubDetailDomain();
                doReport.setDoId( doId.toString() );
                isReportData = deliveryOrderService.searchDeliveryOrderReport(doReport);
                filenameReport.append(SupplierPortalConstant.DELIVERY_ORDER);
                filenameReport.append(doId);
                filenameReport.append(Constants.SYMBOL_DOT);
                filenameReport.append(Constants.REPORT_PDF_FORMAT);
            } else {
                KanbanOrderInformationDoDetailReturnDomain kanbanReport = new KanbanOrderInformationDoDetailReturnDomain();
                kanbanReport.setDoId( doId.toString() );
                isReportData = deliveryOrderService.searchKanbanDeliveryOrderReport( kanbanReport );
                filenameReport.append(SupplierPortalConstant.KANBAN);
                filenameReport.append(doId);
                filenameReport.append(Constants.SYMBOL_DOT);
                filenameReport.append(Constants.REPORT_PDF_FORMAT);
            }
            
        } catch (Exception e) {
            MessageUtil.throwsApplicationMessage(
                locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0013,
                null );
        }
        FileManagementDomain resultDomain = new FileManagementDomain();
        resultDomain = new FileManagementDomain();
        resultDomain.setFileData(isReportData);
        resultDomain.setFileName(filenameReport.toString());
        return resultDomain;
    }
}