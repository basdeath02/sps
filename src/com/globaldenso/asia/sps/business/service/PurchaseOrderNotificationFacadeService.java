/*
 * ModifyDate Development company     Describe 
 * 2014/08/18 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;
import java.util.Locale;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;

/**
 * <p>The Interface Purchase Order Notification Facade Service.</p>
 * <p>Service for Purchase Order Notification about search data 
 * from Purchase Order Acknowledgement screen.</p>
 * <ul>
 * <li>Method search  : searchInitial</li>
 * </ul>
 *
 * @author CSI
 */
public interface PurchaseOrderNotificationFacadeService {

    /**
     * Search Initial Data for screen Purchase Order Notification.
     * @param purchaseOrderNotificationDomain Purchase Order Notification Domain
     * @return List of Purchase Order Notification
     * @throws ApplicationException an ApplicationException
     * */
    public List<PurchaseOrderNotificationDomain> searchInitial(
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain)
        throws ApplicationException;
    
    /**
     * Search Role Name that can operate Button/Link and return by throw ApplicationException.
     * @param roleTypeName : role type name (button/link name)
     * @param locale : for get property
     * @throws ApplicationException contain application message
     * */
    public void searchRoleCanOperate(String roleTypeName, Locale locale)
        throws ApplicationException;
}
