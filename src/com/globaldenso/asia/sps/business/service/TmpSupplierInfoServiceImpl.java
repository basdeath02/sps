/*
 * ModifyDate Development company     Describe 
 * 2014/07/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.service;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain;
import com.globaldenso.asia.sps.business.dao.TmpSupplierInfoDao;


/**
 * <p>The Class TempUserSupplierServiceImpl.</p>
 * <p>Manage data of Temporary User Supplier.</p>
 * <ul>
 * <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * 
 * </ul>
 *
 * @author CSI
 */
public class TmpSupplierInfoServiceImpl implements TmpSupplierInfoService {

    

    /** The  Temporary Supplier Info Dao. */
    private TmpSupplierInfoDao tmpSupplierInfoDao;
    
    /**
     * Instantiates a new Temporary User Supplier Service implement.
     */
    public TmpSupplierInfoServiceImpl(){
        super();
    }
    
    /**
     * Sets the Temporary Supplier Info Dao.
     * 
     * @param tmpSupplierInfoDao the new Temporary User DENSO Dao.
     */
    public void setTmpSupplierInfoDao(TmpSupplierInfoDao tmpSupplierInfoDao) {
        this.tmpSupplierInfoDao = tmpSupplierInfoDao;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpSupplierInfoService#searchTempUploadSupplierInfoOneRecord
     * (com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain)
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoOneRecord(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain){
        TmpSupplierInfoDomain result = tmpSupplierInfoDao.searchTmpUploadSupplierInfoOneRecord(
            tmpSupplierInfoDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpSupplierInfoService#searchTmpUploadSupplierInfoCompanyName
     * (com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain)
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoCompanyName(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain){
        TmpSupplierInfoDomain result = tmpSupplierInfoDao.searchTmpUploadSupplierInfoCompanyName(
            tmpSupplierInfoDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.TmpSupplierInfoService#searchTmpUploadSupplierInfoPlant
     * (com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain)
     */
    public List<PlantSupplierDomain> searchTmpUploadSupplierInfoPlant(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain) {
        List<PlantSupplierDomain> result = tmpSupplierInfoDao.searchTmpUploadSupplierInfoPlant(
            tmpSupplierInfoDomain);
        return result;
    }
    
}