/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/12 Parichat           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class AnnounceMessageDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class TmpUploadErrorDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3935827237501551534L;

    /** The DSC ID. */
    private String dscId;
    
    /** The Line No. **/
    private int lineNo;
    
    /** The Misc Type. */
    private String miscType;
    
    /** The Misc Code */
    private String miscCode;
    
    /** The Function Code. */
    private String functionCode;
    
    
    /**
     * Instantiates a new account domain.
     */
    public TmpUploadErrorDomain() {
        super();
    }


    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }


    /**
     * Gets the Misc Type.
     * 
     * @return the Misc Type.
     */
    public String getMiscType() {
        return miscType;
    }

    /**
     * Sets the Misc Type.
     * 
     * @param miscType the Misc Type.
     */
    public void setMiscType(String miscType) {
        this.miscType = miscType;
    }

    /**
     * Gets the Misc Code.
     * 
     * @return the Misc Code.
     */
    public String getMiscCode() {
        return miscCode;
    }

    /**
     * Sets the Misc Code.
     * 
     * @param miscCode the Misc Code.
     */
    public void setMiscCode(String miscCode) {
        this.miscCode = miscCode;
    }

    /**
     * Gets the Function Code.
     * 
     * @return the Function Code.
     */
    public String getFunctionCode() {
        return functionCode;
    }

    /**
     * Sets the Function Code.
     * 
     * @param functionCode the Function Code.
     */
    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }


    /**
     * Gets the Line No.
     * 
     * @return the Line No.
     */
    public int getLineNo() {
        return lineNo;
    }

    /**
     * Sets the Line No.
     * 
     * @param lineNo the Line No.
     */
    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }


}