package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;


/**
 * <p>The class PriceDifferenceInformationReturnDomain.</p>
 * <p>Facade for PriceDifferenceInformationImpl.</p>
 * <ul>
 * <li>Method search  : searchPriceDifferenceInformation</li>
 * <li>Method create  : downloadInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class PriceDifferenceInformationReturnDomain extends BaseDomain implements Serializable {

    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = 5167163911250136584L;

    /** The error Message List. */
    List<ApplicationMessageDomain> errorMessageList;
    
    /** The Price Difference Information Domain List. */
    List<PriceDifferenceInformationDomain> PriceDifferenceInformationDomainList;
    
    /** The price Difference InformationDomain. */
    private PriceDifferenceInformationDomain priceDifferenceInformationDomain;
    
    /** The file name. */
    private String fileName;
    
    /** The csv Bufffer. */
    private StringBuffer csvBufffer;
    
    
    /**
     * Instantiates a new Price Difference Information Return Domain.
     * 
     */
    public PriceDifferenceInformationReturnDomain(){
        super();
    }
    
    /**
     * <p>Getter method for errorMessageList.</p>
     *
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * <p>Setter method for errorMessageList.</p>
     *
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * <p>Getter method for priceDifferenceInformationDomainList.</p>
     *
     * @return the priceDifferenceInformationDomainList
     */
    public List<PriceDifferenceInformationDomain> getPriceDifferenceInformationDomainList() {
        return PriceDifferenceInformationDomainList;
    }

    /**
     * <p>Setter method for priceDifferenceInformationDomainList.</p>
     *
     * @param priceDifferenceInformationDomainList Set for priceDifferenceInformationDomainList
     */
    public void setPriceDifferenceInformationDomainList(
        List<PriceDifferenceInformationDomain> priceDifferenceInformationDomainList) {
        PriceDifferenceInformationDomainList = priceDifferenceInformationDomainList;
    }

    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for csvBufffer.</p>
     *
     * @return the csvBufffer
     */
    public StringBuffer getCsvBufffer() {
        return csvBufffer;
    }

    /**
     * <p>Setter method for csvBufffer.</p>
     *
     * @param csvBufffer Set for csvBufffer
     */
    public void setCsvBufffer(StringBuffer csvBufffer) {
        this.csvBufffer = csvBufffer;
    }

    /**
     * <p>Getter method for priceDifferenceInformationDomain.</p>
     *
     * @return the priceDifferenceInformationDomain
     */
    public PriceDifferenceInformationDomain getPriceDifferenceInformationDomain() {
        return priceDifferenceInformationDomain;
    }

    /**
     * <p>Setter method for priceDifferenceInformationDomain.</p>
     *
     * @param priceDifferenceInformationDomain Set for priceDifferenceInformationDomain
     */
    public void setPriceDifferenceInformationDomain(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain) {
        this.priceDifferenceInformationDomain = priceDifferenceInformationDomain;
    }

}
