package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * 
 * <p>UrgentOrderDomain class.</p>
 *
 * @author CSI
 */
public class UrgentOrderDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -1350245472960960098L;
    
    /** Urgent Order Flg Header */
    private Integer urgentOrderFlgHeader;
    
    /** Urgent Order Flg Detail */
    private Integer urgentOrderFlgDetail;
    
    /** Lastest Rev Flag */
    private Integer latestRevFlg;
    
    /** Mail Flag */
    private Integer mailFlg;
    
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public UrgentOrderDomain() {
        super();
    }
    /**
     * <p>Getter method for urgentOrderFlgHeader.</p>
     *
     * @return the urgentOrderFlgHeader
     */
    public Integer getUrgentOrderFlgHeader() {
        return urgentOrderFlgHeader;
    }
    /**
     * <p>Setter method for urgentOrderFlgHeader.</p>
     *
     * @param urgentOrderFlgHeader Set for urgentOrderFlgHeader
     */
    public void setUrgentOrderFlgHeader(Integer urgentOrderFlgHeader) {
        this.urgentOrderFlgHeader = urgentOrderFlgHeader;
    }
    /**
     * <p>Getter method for urgentOrderFlgDetail.</p>
     *
     * @return the urgentOrderFlgDetail
     */
    public Integer getUrgentOrderFlgDetail() {
        return urgentOrderFlgDetail;
    }
    /**
     * <p>Setter method for urgentOrderFlgDetail.</p>
     *
     * @param urgentOrderFlgDetail Set for urgentOrderFlgDetail
     */
    public void setUrgentOrderFlgDetail(Integer urgentOrderFlgDetail) {
        this.urgentOrderFlgDetail = urgentOrderFlgDetail;
    }
    /**
     * <p>Getter method for latestRevFlg.</p>
     *
     * @return the latestRevFlg
     */
    public Integer getLatestRevFlg() {
        return latestRevFlg;
    }
    /**
     * <p>Setter method for latestRevFlg.</p>
     *
     * @param latestRevFlg Set for latestRevFlg
     */
    public void setLatestRevFlg(Integer latestRevFlg) {
        this.latestRevFlg = latestRevFlg;
    }
    /**
     * <p>Getter method for mailFlg.</p>
     *
     * @return the mailFlg
     */
    public Integer getMailFlg() {
        return mailFlg;
    }
    /**
     * <p>Setter method for mailFlg.</p>
     *
     * @param mailFlg Set for mailFlg
     */
    public void setMailFlg(Integer mailFlg) {
        this.mailFlg = mailFlg;
    }
}
