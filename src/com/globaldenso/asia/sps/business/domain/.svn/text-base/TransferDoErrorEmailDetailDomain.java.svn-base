/*
 * ModifyDate Development company     Describe 
 * 2015/03/19 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;

/**
 * <p>
 * Transfer Change Delivery Order Error Email detail Domain.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TransferDoErrorEmailDetailDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 1328291648281064112L;

    /** SPS_CIGMA_DO_ERROR data. */
    private SpsCigmaDoErrorDomain cigmaDoError;
    
    /** SPS_M_DENSO_SUPPLIER_PARTS data. */
    private SpsMDensoSupplierPartsDomain densoSupplerParts;
    
    /** The default constructor. */
    public TransferDoErrorEmailDetailDomain() {
        this.cigmaDoError = new SpsCigmaDoErrorDomain();
        this.densoSupplerParts = new SpsMDensoSupplierPartsDomain();
    }

    /**
     * <p>Getter method for cigmaDoError.</p>
     *
     * @return the cigmaDoError
     */
    public SpsCigmaDoErrorDomain getCigmaDoError() {
        return cigmaDoError;
    }

    /**
     * <p>Setter method for cigmaDoError.</p>
     *
     * @param cigmaDoError Set for cigmaDoError
     */
    public void setCigmaDoError(SpsCigmaDoErrorDomain cigmaDoError) {
        this.cigmaDoError = cigmaDoError;
    }

    /**
     * <p>Getter method for densoSupplerParts.</p>
     *
     * @return the densoSupplerParts
     */
    public SpsMDensoSupplierPartsDomain getDensoSupplerParts() {
        return densoSupplerParts;
    }

    /**
     * <p>Setter method for densoSupplerParts.</p>
     *
     * @param densoSupplerParts Set for densoSupplerParts
     */
    public void setDensoSupplerParts(SpsMDensoSupplierPartsDomain densoSupplerParts) {
        this.densoSupplerParts = densoSupplerParts;
    }
}
