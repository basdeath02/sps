/*
 * ModifyDate Development company Describe 
 * 2014/05/14 Parichat           Create  
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Locale;

import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.fw.ContextParams;

/**
 * The Class MetColumn.
 * 
 * @author CSI
 * @version 1.00
 */
public class MetColumnDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8096849236858462250L;

    /** The table id. */
    private String tableId;
    
    /** The column id. */
    private String columnId;
    
    /** The column physical. */
    private String columnPhysical;
    
    /** The column logical. */
    private String columnLogical;
    
    /** The order sequence. */
    private String orderSequence;
    
    /** The system set sign. */
    private String systemSetSign;
    
    /** The data type. */
    private String dataType;
    
    /** The total length. */
    private String totalLength;
    
    /** The decimal length. */
    private String decimalLength;
    
    /** The search sign. */
    private String searchSign;
    
    /** The key sign. */
    private String keySign;
    
    /** The not null sign. */
    private String notNullSign;
    
    /** The min value. */
    private String minValue;
    
    /** The max value. */
    private String maxValue;
    
    /** The visibility. */
    private String visibility;
    
    /** The data type. */
    private String displayType;
    
    /** The db link table. */
    private String dbLinkTable;
    
    /** The db link col value. */
    private String dbLinkColValue;
    
    /** The db link col display. */
    private String dbLinkColDisplay;
    
    /** The action. */
    private String action;
    
    /** The date limit. */
    private String dateLimit;
    
    /** The compare to. */
    private String compareTo;
    
    /** The combine key. */
    private String combineKey;
    
    /** The row. */
    private Integer row;
    
    /** The target page. */
    private String targetPage;
    
    /** The flag transaction. */
    private String flagTransaction;
    
    /** The create date. */
    private Timestamp createDate;
    
    /** The create by. */
    private String createBy;
    
    /** The update date. */
    private Timestamp updateDate;
    
    /** The update by. */
    private String updateBy;
    
    /** The active flag. */
    private String activeFlag;
    
    /** The sort by. */
    private String sortBy;
    
    /** The current date. */
    private Timestamp currentDate;
    
    /** The locale. */
    private Locale locale;
    
    
    /**
     * Instantiates a new met column domain.
     */
    public MetColumnDomain() {
        setRow(ContextParams.getDefaultRowPerPage());
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the table id.
     * 
     * @return the table id
     */
    public String getTableId() {
        return tableId;
    }
    
    /**
     * Sets the table id.
     * 
     * @param tableId the new table id
     */
    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
    
    /**
     * Gets the column id.
     * 
     * @return the column id
     */
    public String getColumnId() {
        return columnId;
    }
    
    /**
     * Sets the column id.
     * 
     * @param columnId the new column id
     */
    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }
    
    /**
     * Gets the column physical.
     * 
     * @return the column physical
     */
    public String getColumnPhysical() {
        return columnPhysical;
    }
    
    /**
     * Sets the column physical.
     * 
     * @param columnPhysical the new column physical
     */
    public void setColumnPhysical(String columnPhysical) {
        this.columnPhysical = columnPhysical;
    }
    
    /**
     * Gets the column logical.
     * 
     * @return the column logical
     */
    public String getColumnLogical() {
        return columnLogical;
    }
    
    /**
     * Sets the column logical.
     * 
     * @param columnLogical the new column logical
     */
    public void setColumnLogical(String columnLogical) {
        this.columnLogical = columnLogical;
    }
    
    /**
     * Gets the order sequence.
     * 
     * @return the order sequence
     */
    public String getOrderSequence() {
        return orderSequence;
    }
    
    /**
     * Sets the order sequence.
     * 
     * @param orderSequence the new order sequence
     */
    public void setOrderSequence(String orderSequence) {
        this.orderSequence = orderSequence;
    }
    
    /**
     * Gets the system set sign.
     * 
     * @return the system set sign
     */
    public String getSystemSetSign() {
        return systemSetSign;
    }
    
    /**
     * Sets the system set sign.
     * 
     * @param systemSetSign the new system set sign
     */
    public void setSystemSetSign(String systemSetSign) {
        this.systemSetSign = systemSetSign;
    }
    
    /**
     * Gets the data type.
     * 
     * @return the data type
     */
    public String getDataType() {
        return dataType;
    }
    
    /**
     * Sets the data type.
     * 
     * @param dataType the new data type
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
    
    /**
     * Gets the total length.
     * 
     * @return the total length
     */
    public String getTotalLength() {
        return totalLength;
    }
    
    /**
     * Sets the total length.
     * 
     * @param totalLength the new total length
     */
    public void setTotalLength(String totalLength) {
        this.totalLength = totalLength;
    }
    
    /**
     * Gets the decimal length.
     * 
     * @return the decimal length
     */
    public String getDecimalLength() {
        return decimalLength;
    }
    
    /**
     * Sets the decimal length.
     * 
     * @param decimalLength the new decimal length
     */
    public void setDecimalLength(String decimalLength) {
        this.decimalLength = decimalLength;
    }
    
    /**
     * Gets the key sign.
     * 
     * @return the key sign
     */
    public String getKeySign() {
        return keySign;
    }
    
    /**
     * Sets the key sign.
     * 
     * @param keySign the new key sign
     */
    public void setKeySign(String keySign) {
        this.keySign = keySign;
    }
    
    /**
     * Gets the not null sign.
     * 
     * @return the not null sign
     */
    public String getNotNullSign() {
        return notNullSign;
    }
    
    /**
     * Sets the not null sign.
     * 
     * @param notNullSign the new not null sign
     */
    public void setNotNullSign(String notNullSign) {
        this.notNullSign = notNullSign;
    }
    
    /**
     * Gets the min value.
     * 
     * @return the min value
     */
    public String getMinValue() {
        return minValue;
    }
    
    /**
     * Sets the min value.
     * 
     * @param minValue the new min value
     */
    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }
    
    /**
     * Gets the max value.
     * 
     * @return the max value
     */
    public String getMaxValue() {
        return maxValue;
    }
    
    /**
     * Sets the max value.
     * 
     * @param maxValue the new max value
     */
    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * Gets the search sign.
     * 
     * @return the search sign
     */
    public String getSearchSign() {
        return searchSign;
    }

    /**
     * Sets the search sign.
     * 
     * @param searchSign the new search sign
     */
    public void setSearchSign(String searchSign) {
        this.searchSign = searchSign;
    }

    /**
     * Gets the visibility.
     * 
     * @return the visibility
     */
    public String getVisibility() {
        return visibility;
    }

    /**
     * Sets the visibility.
     * 
     * @param visibility the new visibility
     */
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    /**
     * Gets the display type.
     * 
     * @return the display type
     */
    public String getDisplayType() {
        return displayType;
    }

    /**
     * Sets the display type.
     * 
     * @param displayType the new display type
     */
    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    /**
     * Gets the db link table.
     * 
     * @return the db link table
     */
    public String getDbLinkTable() {
        return dbLinkTable;
    }

    /**
     * Sets the db link table.
     * 
     * @param dbLinkTable the new db link table
     */
    public void setDbLinkTable(String dbLinkTable) {
        this.dbLinkTable = dbLinkTable;
    }

    /**
     * Gets the db link col value.
     * 
     * @return the db link col value
     */
    public String getDbLinkColValue() {
        return dbLinkColValue;
    }

    /**
     * Sets the db link col value.
     * 
     * @param dbLinkColValue the new db link col value
     */
    public void setDbLinkColValue(String dbLinkColValue) {
        this.dbLinkColValue = dbLinkColValue;
    }

    /**
     * Gets the db link col display.
     * 
     * @return the db link col display
     */
    public String getDbLinkColDisplay() {
        return dbLinkColDisplay;
    }

    /**
     * Sets the db link col display.
     * 
     * @param dbLinkColDisplay the new db link col display
     */
    public void setDbLinkColDisplay(String dbLinkColDisplay) {
        this.dbLinkColDisplay = dbLinkColDisplay;
    }

    /**
     * Gets the action.
     * 
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the action.
     * 
     * @param action the new action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Gets the date limit.
     * 
     * @return the date limit
     */
    public String getDateLimit() {
        return dateLimit;
    }

    /**
     * Sets the date limit.
     * 
     * @param dateLimit the new date limit
     */
    public void setDateLimit(String dateLimit) {
        this.dateLimit = dateLimit;
    }

    /**
     * Gets the row.
     * 
     * @return the row
     */
    public Integer getRow() {
        return row;
    }

    /**
     * Sets the row.
     * 
     * @param row the new row
     */
    public void setRow(Integer row) {
        this.row = row;
    }

    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }

    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    /**
     * Gets the flag transaction.
     * 
     * @return the flag transaction
     */
    public String getFlagTransaction() {
        return flagTransaction;
    }

    /**
     * Sets the flag transaction.
     * 
     * @param flagTransaction the new flag transaction
     */
    public void setFlagTransaction(String flagTransaction) {
        this.flagTransaction = flagTransaction;
    }

    /**
     * Gets the creates the date.
     * 
     * @return the creates the date
     */
    public Timestamp getCreateDate() {
        return createDate;
    }

    /**
     * Sets the creates the date.
     * 
     * @param createDate the new creates the date
     */
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    /**
     * Gets the creates the by.
     * 
     * @return the creates the by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * Sets the creates the by.
     * 
     * @param createBy the new creates the by
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * Gets the update date.
     * 
     * @return the update date
     */
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the update date.
     * 
     * @param updateDate the new update date
     */
    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Gets the update by.
     * 
     * @return the update by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * Sets the update by.
     * 
     * @param updateBy the new update by
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * Gets the active flag.
     * 
     * @return the active flag
     */
    public String getActiveFlag() {
        return activeFlag;
    }

    /**
     * Sets the active flag.
     * 
     * @param activeFlag the new active flag
     */
    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    /**
     * Gets the sort by.
     * 
     * @return the sort by
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Sets the sort by.
     * 
     * @param sortBy the new sort by
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * Gets the current date.
     * 
     * @return the current date
     */
    public Timestamp getCurrentDate() {
        return currentDate;
    }

    /**
     * Sets the current date.
     * 
     * @param currentDate the new current date
     */
    public void setCurrentDate(Timestamp currentDate) {
        this.currentDate = currentDate;
    }

    /**
     * Gets the locale.
     * 
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Sets the locale.
     * 
     * @param locale the new locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * Gets the compare to.
     * 
     * @return the compare to
     */
    public String getCompareTo() {
        return compareTo;
    }

    /**
     * Sets the compare to.
     * 
     * @param compareTo the new compare to
     */
    public void setCompareTo(String compareTo) {
        this.compareTo = compareTo;
    }

    /**
     * Gets the combine key.
     * 
     * @return the combine key
     */
    public String getCombineKey() {
        return combineKey;
    }

    /**
     * Sets the combine key.
     * 
     * @param combineKey the new combine key
     */
    public void setCombineKey(String combineKey) {
        this.combineKey = combineKey;
    }
}