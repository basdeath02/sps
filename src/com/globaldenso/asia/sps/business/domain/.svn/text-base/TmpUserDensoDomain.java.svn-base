/*
 * ModifyDate Development company Describe 
 * 2014/08/22 Phakaporn           Create   
 * 2016/02/10 CSI Akat            [IN049]
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "TempUserDensoDomain"<br />
 * Table overview: DENSO user information temp table<br />
 * 
 * 
 * @author CSI
 * @version 1.00
 */
public class TmpUserDensoDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3260390899472784936L;

    /**
     * User DSC ID
     */
    private String userDscId;

    /**
     * Session code
     */
    private String sessionCd;

    /**
     * CSV line no.
     */
    private BigDecimal lineNo;

    /**
     * DENSO User DSC ID
     */
    private String dscId;

    /**
     * DENSO company code
     */
    private String dCd;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * DENSO employee code
     */
    private String employeeCd;

    /**
     * Department code
     */
    private String departmentCd;

    /**
     * User first name
     */
    private String firstName;

    /**
     * User middle name
     */
    private String middleName;

    /**
     * User last name
     */
    private String lastName;

    /**
     * Email address
     */
    private String email;

    /**
     * Telephone no.
     */
    private String telephone;

    /**
     * The email for create ASN(short ship) and cancel ASN
     */
    private String emlCreateCancelAsnFlag;

    /**
     * The email for revise ASN flag
     */
    private String emlReviseAsnFlag;

    /**
     * The email for create invoice flag
     */
    private String emlCreateInvoiceFlag;

    // Start : [IN049] Change Column Name
    /** The email for abnormal transfer 1 flag */
    //private String emlAbnormalTransfer1Flag;
    /** The email for abnormal transfer 2 flag */
    //private String emlAbnormalTransfer2Flag;

    /** Email to Pending P/O | D/O already create ASN | Supplier Info Not Found. */
    private String emlPedpoDoalcasnUnmatpnFlg;
    
    /** Email to abnormal data transfer. */
    private String emlAbnormalTransferFlg;
    // End : [IN049] Change Column Name
    
    /**
     * Role code
     */
    private String roleCd;

    /**
     * DENSO company code
     */
    private String roleDCd;

    /**
     * DENSO plant code
     */
    private String roleDPcd;

    /**
     * Effective start date
     */
    private Timestamp effectStart;

    /**
     * Effective end date
     */
    private Timestamp effectEnd;

    /**
     * User information flag
     */
    private String informationFlag;

    /**
     * Role information flag
     */
    private String roleFlag;

    /**
     * Upload data date time
     */
    private Timestamp uploadDatetime;

    /**
     * User data last update datetime
     */
    private Timestamp userLastUpdate;

    /**
     * DENSO user data last update datetime
     */
    private Timestamp densoLastUpdate;

    /**
     * Role data last update datetime
     */
    private Timestamp roleLastUpdate;

    /**
     * Move to actual table flag
     */
    private String isActualRegister;

    /**
     * Move to actual table date time
     */
    private Timestamp toActualDatetime;
    
    /** The To Actual Error Code. */
    private String toActualErrorCd;
    
    /** The To Actual Error Description. */
    private String toActualErrorDesc;

    /**
     * Default constructor
     */
    public TmpUserDensoDomain() {
    }

    /**
     * Getter method of "userDscId"
     * 
     * @return the userDscId
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId"
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd"
     * 
     * @return the sessionCd
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd"
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo"
     * 
     * @return the lineNo
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo"
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dscId"
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId"
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "employeeCd"
     * 
     * @return the employeeCd
     */
    public String getEmployeeCd() {
        return employeeCd;
    }

    /**
     * Setter method of "employeeCd"
     * 
     * @param employeeCd Set in "employeeCd".
     */
    public void setEmployeeCd(String employeeCd) {
        this.employeeCd = employeeCd;
    }

    /**
     * Getter method of "departmentCd"
     * 
     * @return the departmentCd
     */
    public String getDepartmentCd() {
        return departmentCd;
    }

    /**
     * Setter method of "departmentCd"
     * 
     * @param departmentCd Set in "departmentCd".
     */
    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    /**
     * Getter method of "firstName"
     * 
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName"
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName"
     * 
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName"
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName"
     * 
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName"
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email"
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email"
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlag"
     * 
     * @return the emlCreateCancelAsnFlag
     */
    public String getEmlCreateCancelAsnFlag() {
        return emlCreateCancelAsnFlag;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlag"
     * 
     * @param emlCreateCancelAsnFlag Set in "emlCreateCancelAsnFlag".
     */
    public void setEmlCreateCancelAsnFlag(String emlCreateCancelAsnFlag) {
        this.emlCreateCancelAsnFlag = emlCreateCancelAsnFlag;
    }

    /**
     * Getter method of "emlReviseAsnFlag"
     * 
     * @return the emlReviseAsnFlag
     */
    public String getEmlReviseAsnFlag() {
        return emlReviseAsnFlag;
    }

    /**
     * Setter method of "emlReviseAsnFlag"
     * 
     * @param emlReviseAsnFlag Set in "emlReviseAsnFlag".
     */
    public void setEmlReviseAsnFlag(String emlReviseAsnFlag) {
        this.emlReviseAsnFlag = emlReviseAsnFlag;
    }

    /**
     * Getter method of "emlCreateInvoiceFlag"
     * 
     * @return the emlCreateInvoiceFlag
     */
    public String getEmlCreateInvoiceFlag() {
        return emlCreateInvoiceFlag;
    }

    /**
     * Setter method of "emlCreateInvoiceFlag"
     * 
     * @param emlCreateInvoiceFlag Set in "emlCreateInvoiceFlag".
     */
    public void setEmlCreateInvoiceFlag(String emlCreateInvoiceFlag) {
        this.emlCreateInvoiceFlag = emlCreateInvoiceFlag;
    }

    // Start : [IN049] Change Column Name
    /**
     * Getter method of "emlAbnormalTransfer1Flag"
     * 
     * @return the emlAbnormalTransfer1Flag
     */
    //public String getEmlAbnormalTransfer1Flag() {
    //    return emlAbnormalTransfer1Flag;
    //}
    /**
     * Setter method of "emlAbnormalTransfer1Flag"
     * 
     * @param emlAbnormalTransfer1Flag Set in "emlAbnormalTransfer1Flag".
     */
    //public void setEmlAbnormalTransfer1Flag(String emlAbnormalTransfer1Flag) {
    //    this.emlAbnormalTransfer1Flag = emlAbnormalTransfer1Flag;
    //}
    /**
     * Getter method of "emlAbnormalTransfer2Flag"
     * 
     * @return the emlAbnormalTransfer2Flag
     */
    //public String getEmlAbnormalTransfer2Flag() {
    //    return emlAbnormalTransfer2Flag;
    //}
    /**
     * Setter method of "emlAbnormalTransfer2Flag"
     * 
     * @param emlAbnormalTransfer2Flag Set in "emlAbnormalTransfer2Flag".
     */
    //public void setEmlAbnormalTransfer2Flag(String emlAbnormalTransfer2Flag) {
    //    this.emlAbnormalTransfer2Flag = emlAbnormalTransfer2Flag;
    //}

    /**
     * <p>Getter method for emlPedpoDoalcasnUnmatpnFlg.</p>
     *
     * @return the emlPedpoDoalcasnUnmatpnFlg
     */
    public String getEmlPedpoDoalcasnUnmatpnFlg() {
        return emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * <p>Setter method for emlPedpoDoalcasnUnmatpnFlg.</p>
     *
     * @param emlPedpoDoalcasnUnmatpnFlg Set for emlPedpoDoalcasnUnmatpnFlg
     */
    public void setEmlPedpoDoalcasnUnmatpnFlg(String emlPedpoDoalcasnUnmatpnFlg) {
        this.emlPedpoDoalcasnUnmatpnFlg = emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * <p>Getter method for emlAbnormalTransferFlg.</p>
     *
     * @return the emlAbnormalTransferFlg
     */
    public String getEmlAbnormalTransferFlg() {
        return emlAbnormalTransferFlg;
    }

    /**
     * <p>Setter method for emlAbnormalTransferFlg.</p>
     *
     * @param emlAbnormalTransferFlg Set for emlAbnormalTransferFlg
     */
    public void setEmlAbnormalTransferFlg(String emlAbnormalTransferFlg) {
        this.emlAbnormalTransferFlg = emlAbnormalTransferFlg;
    }
    // End : [IN049] Change Column Name
    
    /**
     * Getter method of "roleCd"
     * 
     * @return the roleCd
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Setter method of "roleCd"
     * 
     * @param roleCd Set in "roleCd".
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Getter method of "roleDCd"
     * 
     * @return the roleDCd
     */
    public String getRoleDCd() {
        return roleDCd;
    }

    /**
     * Setter method of "roleDCd"
     * 
     * @param roleDCd Set in "roleDCd".
     */
    public void setRoleDCd(String roleDCd) {
        this.roleDCd = roleDCd;
    }

    /**
     * Getter method of "roleDPcd"
     * 
     * @return the roleDPcd
     */
    public String getRoleDPcd() {
        return roleDPcd;
    }

    /**
     * Setter method of "roleDPcd"
     * 
     * @param roleDPcd Set in "roleDPcd".
     */
    public void setRoleDPcd(String roleDPcd) {
        this.roleDPcd = roleDPcd;
    }

    /**
     * Getter method of "effectStart"
     * 
     * @return the effectStart
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart"
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd"
     * 
     * @return the effectEnd
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd"
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "informationFlag"
     * 
     * @return the informationFlag
     */
    public String getInformationFlag() {
        return informationFlag;
    }

    /**
     * Setter method of "informationFlag"
     * 
     * @param informationFlag Set in "informationFlag".
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }

    /**
     * Getter method of "roleFlag"
     * 
     * @return the roleFlag
     */
    public String getRoleFlag() {
        return roleFlag;
    }

    /**
     * Setter method of "roleFlag"
     * 
     * @param roleFlag Set in "roleFlag".
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "userLastUpdate"
     * 
     * @return the userLastUpdate
     */
    public Timestamp getUserLastUpdate() {
        return userLastUpdate;
    }

    /**
     * Setter method of "userLastUpdate"
     * 
     * @param userLastUpdate Set in "userLastUpdate".
     */
    public void setUserLastUpdate(Timestamp userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    /**
     * Getter method of "densoLastUpdate"
     * 
     * @return the densoLastUpdate
     */
    public Timestamp getDensoLastUpdate() {
        return densoLastUpdate;
    }

    /**
     * Setter method of "densoLastUpdate"
     * 
     * @param densoLastUpdate Set in "densoLastUpdate".
     */
    public void setDensoLastUpdate(Timestamp densoLastUpdate) {
        this.densoLastUpdate = densoLastUpdate;
    }

    /**
     * Getter method of "roleLastUpdate"
     * 
     * @return the roleLastUpdate
     */
    public Timestamp getRoleLastUpdate() {
        return roleLastUpdate;
    }

    /**
     * Setter method of "roleLastUpdate"
     * 
     * @param roleLastUpdate Set in "roleLastUpdate".
     */
    public void setRoleLastUpdate(Timestamp roleLastUpdate) {
        this.roleLastUpdate = roleLastUpdate;
    }

    /**
     * Getter method of "isActualRegister"
     * 
     * @return the isActualRegister
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister"
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime"
     * 
     * @return the toActualDatetime
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime"
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }
    
    /**
     * Gets the Actual Error Code.
     * 
     * @return the Actual Error Code.
     */
    public String getToActualErrorCd() {
        return toActualErrorCd;
    }

    /**
     * Sets the Actual Error Code.
     * 
     * @param toActualErrorCd the Actual Error Code.
     */
    public void setToActualErrorCd(String toActualErrorCd) {
        this.toActualErrorCd = toActualErrorCd;
    }

    /**
     * Gets the Actual Error Description.
     * 
     * @return the Actual Error Description.
     */
    public String getToActualErrorDesc() {
        return toActualErrorDesc;
    }

    /**
     * Sets the Actual Error Description.
     * 
     * @param toActualErrorDesc the Actual Error Description.
     */
    public void setToActualErrorDesc(String toActualErrorDesc) {
        this.toActualErrorDesc = toActualErrorDesc;
    }

}
