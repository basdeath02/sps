/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class Delivery Order Do Detail Domain.
 * 
 * @author CSI
 */
public class DeliveryOrderDoDetailDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7585915707573885292L;

    /** The supplier part no. */
    private String sPn;

    /** The denso part no. */
    private String dPn;

    /** The pn revision. */
    private String pnRevision;

    /** The pn shipment status */
    private String pnShipmentStatus;

    /** The chg cigma do no. */
    private String chgCigmaDoNo;

    /** The ctrl no. */
    private String ctrlNo;

    /** The current order qty. */
    private String currentOrderQty;

    /** The qty box. */
    private String qtyBox;

    /** The chg reason. */
    private String chgReason;

    /** The unit of measure. */
    private String unitOfMeasure;

    /** The model. */
    private String model;

    /** The original qty. */
    private String originalQty;

    /** The previous qty. */
    private String previousQty;

    /** The no of boxes. */
    private String noOfBoxes;

    /** The rcv lane. */
    private String rcvLane;

    /** The last update dsc id. */
    private String lastUpdateDscId;

    /** The last update date time. */
    private Timestamp lastUpdateDatetime;

    /** The kanban seq. */
    private BigDecimal kanbanSeq;

    /** The item desc. */
    private String itemDesc;

    /**
     * The kanbanType
     */
    private String kanbanType;

    /**
     * The kanbanTagSequence
     */
    private String kanbanTagSequence;

    /**
     * Instantiates a new delivery order do detail domain.
     */
    public DeliveryOrderDoDetailDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for sPn.
     * </p>
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>
     * Setter method for sPn.
     * </p>
     * 
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>
     * Getter method for dPn.
     * </p>
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>
     * Setter method for dPn.
     * </p>
     * 
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>
     * Getter method for pnRevision.
     * </p>
     * 
     * @return the pnRevision
     */
    public String getPnRevision() {
        return pnRevision;
    }

    /**
     * <p>
     * Setter method for pnRevision.
     * </p>
     * 
     * @param pnRevision Set for pnRevision
     */
    public void setPnRevision(String pnRevision) {
        this.pnRevision = pnRevision;
    }

    /**
     * <p>
     * Getter method for pnShipmentStatus.
     * </p>
     * 
     * @return the pnShipmentStatus
     */
    public String getPnShipmentStatus() {
        return pnShipmentStatus;
    }

    /**
     * <p>
     * Setter method for pnShipmentStatus.
     * </p>
     * 
     * @param pnShipmentStatus Set for pnShipmentStatus
     */
    public void setPnShipmentStatus(String pnShipmentStatus) {
        this.pnShipmentStatus = pnShipmentStatus;
    }

    /**
     * <p>
     * Getter method for chgCigmaDoNo.
     * </p>
     * 
     * @return the chgCigmaDoNo
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * <p>
     * Setter method for chgCigmaDoNo.
     * </p>
     * 
     * @param chgCigmaDoNo Set for chgCigmaDoNo
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * <p>
     * Getter method for ctrlNo.
     * </p>
     * 
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * <p>
     * Setter method for ctrlNo.
     * </p>
     * 
     * @param ctrlNo Set for ctrlNo
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * <p>
     * Getter method for currentOrderQty.
     * </p>
     * 
     * @return the currentOrderQty
     */
    public String getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * <p>
     * Setter method for currentOrderQty.
     * </p>
     * 
     * @param currentOrderQty Set for currentOrderQty
     */
    public void setCurrentOrderQty(String currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * <p>
     * Getter method for qtyBox.
     * </p>
     * 
     * @return the qtyBox
     */
    public String getQtyBox() {
        return qtyBox;
    }

    /**
     * <p>
     * Setter method for qtyBox.
     * </p>
     * 
     * @param qtyBox Set for qtyBox
     */
    public void setQtyBox(String qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * <p>
     * Getter method for chgReason.
     * </p>
     * 
     * @return the chgReason
     */
    public String getChgReason() {
        return chgReason;
    }

    /**
     * <p>
     * Setter method for chgReason.
     * </p>
     * 
     * @param chgReason Set for chgReason
     */
    public void setChgReason(String chgReason) {
        this.chgReason = chgReason;
    }

    /**
     * <p>
     * Getter method for unitOfMeasure.
     * </p>
     * 
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * <p>
     * Setter method for unitOfMeasure.
     * </p>
     * 
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>
     * Getter method for model.
     * </p>
     * 
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * <p>
     * Setter method for model.
     * </p>
     * 
     * @param model Set for model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * <p>
     * Getter method for originalQty.
     * </p>
     * 
     * @return the originalQty
     */
    public String getOriginalQty() {
        return originalQty;
    }

    /**
     * <p>
     * Setter method for originalQty.
     * </p>
     * 
     * @param originalQty Set for originalQty
     */
    public void setOriginalQty(String originalQty) {
        this.originalQty = originalQty;
    }

    /**
     * <p>
     * Getter method for previousQty.
     * </p>
     * 
     * @return the previousQty
     */
    public String getPreviousQty() {
        return previousQty;
    }

    /**
     * <p>
     * Setter method for previousQty.
     * </p>
     * 
     * @param previousQty Set for previousQty
     */
    public void setPreviousQty(String previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * <p>
     * Getter method for noOfBoxes.
     * </p>
     * 
     * @return the noOfBoxes
     */
    public String getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * <p>
     * Setter method for noOfBoxes.
     * </p>
     * 
     * @param noOfBoxes Set for noOfBoxes
     */
    public void setNoOfBoxes(String noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * <p>
     * Getter method for rcvLane.
     * </p>
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>
     * Setter method for rcvLane.
     * </p>
     * 
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>
     * Getter method for lastUpdateDscId.
     * </p>
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>
     * Setter method for lastUpdateDscId.
     * </p>
     * 
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>
     * Getter method for lastUpdateDatetime.
     * </p>
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>
     * Setter method for lastUpdateDatetime.
     * </p>
     * 
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>
     * Getter method for kanbanSeq.
     * </p>
     * 
     * @return the kanbanSeq
     */
    public BigDecimal getKanbanSeq() {
        return kanbanSeq;
    }

    /**
     * <p>
     * Setter method for kanbanSeq.
     * </p>
     * 
     * @param kanbanSeq Set for kanbanSeq
     */
    public void setKanbanSeq(BigDecimal kanbanSeq) {
        this.kanbanSeq = kanbanSeq;
    }

    /**
     * <p>
     * Getter method for itemDesc.
     * </p>
     * 
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * <p>
     * Setter method for itemDesc.
     * </p>
     * 
     * @param itemDesc Set for itemDesc
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * Getter method of "kanbanType"
     * 
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }

    /**
     * Setter method of "kanbanType"
     * 
     * @param kanbanType Set in "kanbanType".
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }

    /**
     * Getter method of "kanbanTagSequence"
     * 
     * @return the kanbanTagSequence
     */

    public String getKanbanTagSequence() {
        return kanbanTagSequence;
    }

    /**
     * Setter method of "kanbanTagSequence"
     * 
     * @param kanbanTagSequence Set in "kanbanTagSequence".
     */

    public void setKanbanTagSequence(String kanbanTagSequence) {
        this.kanbanTagSequence = kanbanTagSequence;
    }

}
