/*
 * ModifyDate Development company     Describe 
 * 2014/07/24  CSI Karnrawee          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Pseudo PO Domain for receive value from Web Service
 * 
 * @author CSI Arnon
 * @version 1.0.0
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaDoInformationDomain")
public class PseudoCigmaChangePoInformationDomain extends PseudoBaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -4119018714876706027L;
    /** The pseudo of Data Type */
    @XmlElement(name = "dataType")
    private String pseudoDataType;
    /** The pseudo of Company Name */
    @XmlElement(name = "companyName")
    private String pseudoCompanyName;
    /** The pseudo of Supplier Code */
    @XmlElement(name = "sCd")
    private String pseudoVendorCd;
    /** The pseudo of Supplier Name */
    @XmlElement(name = "supplierName")
    private String pseudoSupplierName;
    /** The pseudo of P/O Number */
    @XmlElement(name = "poNumber")
    private String pseudoPoNumber;
    /** The pseudo of Planner Code */
    @XmlElement(name = "plannerCode")
    private String pseudoPlannerCode;
    /** The pseudo of Issued Date */
    @XmlElement(name = "issuedDate")
    private String pseudoIssuedDate;
    /** The pseudo of Plant Code */
    @XmlElement(name = "plantCode")
    private String pseudoDPcd;
    /** The pseudo of WAREHOUSE FOR PRIME RECEIVING */
    @XmlElement(name = "warehouseForPrimeReceiving")
    private String pseudoWarehouseForPrimeReceiving;
    /** The pseudo of Location */
    @XmlElement(name = "location")
    private String pseudoLocation;
    /** The pseudo of Due Date From */
    @XmlElement(name = "dueDateFrom")
    private String pseudoDueDateFrom;
    /** The pseudo of Due Date To */
    @XmlElement(name = "dueDateTo")
    private String pseudoDueDateTo;
    /** The pseudo of Release No. */
    @XmlElement(name = "releaseNo")
    private String pseudoReleaseNo;
    /** The pseudo of DEL */
    @XmlElement(name = "del")
    private String pseudoDel;
    /** Add Truck Route Flag */
    @XmlElement(name = "addTruckRouteFlag")
    private String pseudoAddTruckRouteFlag;
    /** The pseudo of SEQ */
    @XmlElement(name = "seq")
    private String pseudoSeq;
    /** The pseudo of FAX TELEPHON NO. */
    @XmlElement(name = "faxTelephonNo")
    private String pseudoFaxTelephonNo;
    /** The pseudo of Part Number */
    @XmlElement(name = "dPn")
    private String pseudoPartNumber;
    /** The pseudo of DUE DATE */
    @XmlElement(name = "dueDate")
    private String pseudoDueDate;
    /** The pseudo of Order Type */
    @XmlElement(name = "orderType")
    private String pseudoOrderType;
    /** The pseudo of Report Type */
    @XmlElement(name = "reportType")
    private String pseudoReportType;
    /** The pseudo of Reason */
    @XmlElement(name = "reason")
    private String pseudoReason;
    /** The pseudo of Old Qty. */
    @XmlElement(name = "oldQty")
    private String pseudoOldQty;
    /** The pseudo of New Qty. */
    @XmlElement(name = "newQty")
    private String pseudoNewQty;
    /** The pseudo of Difference Qty. */
    @XmlElement(name = "differenceQty")
    private String pseudoDifferenceQty;
    /** The pseudo of UNIT OF MEASURE */
    @XmlElement(name = "unitOfMeasure")
    private String pseudoUnitOfMeasure;
    /** The pseudo of Item Description */
    @XmlElement(name = "itemDescription")
    private String pseudoItemDescription;
    /** The pseudo of ENGINEERING DRAWING NUMBER */
    @XmlElement(name = "engineeringDrawingNumber")
    private String pseudoEngineeringDrawingNumber;
    /** The pseudo of Transportation Code */
    @XmlElement(name = "transportationCode")
    private String pseudoTransportationCode;
    /** The pseudo of Seller Name */
    @XmlElement(name = "sellerName")
    private String pseudoSellerName;
    /** The pseudo of ETD */
    @XmlElement(name = "etd")
    private String pseudoEtd;
    /** The pseudo of Force Acknowledge Date */
    @XmlElement(name = "forceAcknowledgeDate")
    private String pseudoForceAcknowledgeDate;
    /** The pseudo of TM */
    @XmlElement(name = "tm")
    private String pseudoTm;
    /** The pseudo of SPS Flag */
    @XmlElement(name = "spsFlag")
    private String pseudoSpsFlag;
    /** The pseudo of Create by USER ID */
    @XmlElement(name = "createByUserId")
    private String pseudoCreateByUserId;
    /** The pseudo of Create Date */
    @XmlElement(name = "createDate")
    private String pseudoCreateDate;
    /** The pseudo of Create Time */
    @XmlElement(name = "createTime")
    private String pseudoCreateTime;
    /** The pseudo of Update by USER ID */
    @XmlElement(name = "updateByUserId")
    private String pseudoUpdateByUserId;
    /** The pseudo of Update Date */
    @XmlElement(name = "updateDate")
    private String pseudoUpdateDate;
    /** The pseudo of Update Time */
    @XmlElement(name = "updateTime")
    private String pseudoUpdateTime;
    /** The pseudo of Temp Price Flag */
    @XmlElement(name = "tmpPriceFlg")
    private String pseudoTmpPriceFlg;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public PseudoCigmaChangePoInformationDomain() {
    }
    /**
     * <p>Getter method for pseudoDataType.</p>
     *
     * @return the pseudoDataType
     */
    public String getPseudoDataType() {
        return pseudoDataType;
    }
    /**
     * <p>Setter method for pseudoDataType.</p>
     *
     * @param pseudoDataType Set for pseudoDataType
     */
    public void setPseudoDataType(String pseudoDataType) {
        this.pseudoDataType = pseudoDataType;
    }
    /**
     * <p>Getter method for pseudoCompanyName.</p>
     *
     * @return the pseudoCompanyName
     */
    public String getPseudoCompanyName() {
        return pseudoCompanyName;
    }
    /**
     * <p>Setter method for pseudoCompanyName.</p>
     *
     * @param pseudoCompanyName Set for pseudoCompanyName
     */
    public void setPseudoCompanyName(String pseudoCompanyName) {
        this.pseudoCompanyName = pseudoCompanyName;
    }
    /**
     * <p>Getter method for pseudoSupplierCode.</p>
     *
     * @return the pseudoSupplierCode
     */
    public String getPseudoVendorCd() {
        return pseudoVendorCd;
    }
    /**
     * <p>Setter method for pseudoSupplierCode.</p>
     *
     * @param pseudoVendorCd Set for pseudoSupplierCode
     */
    public void setPseudoVendorCd(String pseudoVendorCd) {
        this.pseudoVendorCd = pseudoVendorCd;
    }
    /**
     * <p>Getter method for pseudoSupplierName.</p>
     *
     * @return the pseudoSupplierName
     */
    public String getPseudoSupplierName() {
        return pseudoSupplierName;
    }
    /**
     * <p>Setter method for pseudoSupplierName.</p>
     *
     * @param pseudoSupplierName Set for pseudoSupplierName
     */
    public void setPseudoSupplierName(String pseudoSupplierName) {
        this.pseudoSupplierName = pseudoSupplierName;
    }
    /**
     * <p>Getter method for pseudoPONumber.</p>
     *
     * @return the pseudoPONumber
     */
    public String getPseudoPoNumber() {
        return pseudoPoNumber;
    }
    /**
     * <p>Setter method for pseudoPONumber.</p>
     *
     * @param pseudoPoNumber Set for pseudoPONumber
     */
    public void setPseudoPoNumber(String pseudoPoNumber) {
        this.pseudoPoNumber = pseudoPoNumber;
    }
    /**
     * <p>Getter method for pseudoPlannerCode.</p>
     *
     * @return the pseudoPlannerCode
     */
    public String getPseudoPlannerCode() {
        return pseudoPlannerCode;
    }
    /**
     * <p>Setter method for pseudoPlannerCode.</p>
     *
     * @param pseudoPlannerCode Set for pseudoPlannerCode
     */
    public void setPseudoPlannerCode(String pseudoPlannerCode) {
        this.pseudoPlannerCode = pseudoPlannerCode;
    }
    /**
     * <p>Getter method for pseudoIssuedDate.</p>
     *
     * @return the pseudoIssuedDate
     */
    public String getPseudoIssuedDate() {
        return pseudoIssuedDate;
    }
    /**
     * <p>Setter method for pseudoIssuedDate.</p>
     *
     * @param pseudoIssuedDate Set for pseudoIssuedDate
     */
    public void setPseudoIssuedDate(String pseudoIssuedDate) {
        this.pseudoIssuedDate = pseudoIssuedDate;
    }
    /**
     * <p>Getter method for pseudoPlantCode.</p>
     *
     * @return the pseudoPlantCode
     */
    public String getPseudoDPcd() {
        return pseudoDPcd;
    }
    /**
     * <p>Setter method for pseudoPlantCode.</p>
     *
     * @param pseudoDPcd Set for pseudoPlantCode
     */
    public void setPseudoDPcd(String pseudoDPcd) {
        this.pseudoDPcd = pseudoDPcd;
    }
    /**
     * <p>Getter method for pseudoWarehouseForPrimeReceiving.</p>
     *
     * @return the pseudoWarehouseForPrimeReceiving
     */
    public String getPseudoWarehouseForPrimeReceiving() {
        return pseudoWarehouseForPrimeReceiving;
    }
    /**
     * <p>Setter method for pseudoWarehouseForPrimeReceiving.</p>
     *
     * @param pseudoWarehouseForPrimeReceiving Set for pseudoWarehouseForPrimeReceiving
     */
    public void setPseudoWarehouseForPrimeReceiving(
        String pseudoWarehouseForPrimeReceiving) {
        this.pseudoWarehouseForPrimeReceiving = pseudoWarehouseForPrimeReceiving;
    }
    /**
     * <p>Getter method for pseudoLocation.</p>
     *
     * @return the pseudoLocation
     */
    public String getPseudoLocation() {
        return pseudoLocation;
    }
    /**
     * <p>Setter method for pseudoLocation.</p>
     *
     * @param pseudoLocation Set for pseudoLocation
     */
    public void setPseudoLocation(String pseudoLocation) {
        this.pseudoLocation = pseudoLocation;
    }
    /**
     * <p>Getter method for pseudoDueDateFrom.</p>
     *
     * @return the pseudoDueDateFrom
     */
    public String getPseudoDueDateFrom() {
        return pseudoDueDateFrom;
    }
    /**
     * <p>Setter method for pseudoDueDateFrom.</p>
     *
     * @param pseudoDueDateFrom Set for pseudoDueDateFrom
     */
    public void setPseudoDueDateFrom(String pseudoDueDateFrom) {
        this.pseudoDueDateFrom = pseudoDueDateFrom;
    }
    /**
     * <p>Getter method for pseudoDueDateTo.</p>
     *
     * @return the pseudoDueDateTo
     */
    public String getPseudoDueDateTo() {
        return pseudoDueDateTo;
    }
    /**
     * <p>Setter method for pseudoDueDateTo.</p>
     *
     * @param pseudoDueDateTo Set for pseudoDueDateTo
     */
    public void setPseudoDueDateTo(String pseudoDueDateTo) {
        this.pseudoDueDateTo = pseudoDueDateTo;
    }
    /**
     * <p>Getter method for pseudoReleaseNo.</p>
     *
     * @return the pseudoReleaseNo
     */
    public String getPseudoReleaseNo() {
        return pseudoReleaseNo;
    }
    /**
     * <p>Setter method for pseudoReleaseNo.</p>
     *
     * @param pseudoReleaseNo Set for pseudoReleaseNo
     */
    public void setPseudoReleaseNo(String pseudoReleaseNo) {
        this.pseudoReleaseNo = pseudoReleaseNo;
    }
    /**
     * <p>Getter method for pseudoDel.</p>
     *
     * @return the pseudoDel
     */
    public String getPseudoDel() {
        return pseudoDel;
    }
    /**
     * <p>Setter method for pseudoDel.</p>
     *
     * @param pseudoDel Set for pseudoDel
     */
    public void setPseudoDel(String pseudoDel) {
        this.pseudoDel = pseudoDel;
    }
    /**
     * <p>Getter method for pseudoSeq.</p>
     *
     * @return the pseudoSeq
     */
    public String getPseudoSeq() {
        return pseudoSeq;
    }
    /**
     * <p>Setter method for pseudoSeq.</p>
     *
     * @param pseudoSeq Set for pseudoSeq
     */
    public void setPseudoSeq(String pseudoSeq) {
        this.pseudoSeq = pseudoSeq;
    }
    /**
     * <p>Getter method for pseudoFaxTelephonNo.</p>
     *
     * @return the pseudoFaxTelephonNo
     */
    public String getPseudoFaxTelephonNo() {
        return pseudoFaxTelephonNo;
    }
    /**
     * <p>Setter method for pseudoFaxTelephonNo.</p>
     *
     * @param pseudoFaxTelephonNo Set for pseudoFaxTelephonNo
     */
    public void setPseudoFaxTelephonNo(String pseudoFaxTelephonNo) {
        this.pseudoFaxTelephonNo = pseudoFaxTelephonNo;
    }
    /**
     * <p>Getter method for pseudoPartNumber.</p>
     *
     * @return the pseudoPartNumber
     */
    public String getPseudoPartNumber() {
        return pseudoPartNumber;
    }
    /**
     * <p>Setter method for pseudoPartNumber.</p>
     *
     * @param pseudoPartNumber Set for pseudoPartNumber
     */
    public void setPseudoPartNumber(String pseudoPartNumber) {
        this.pseudoPartNumber = pseudoPartNumber;
    }
    /**
     * <p>Getter method for pseudoDueDate.</p>
     *
     * @return the pseudoDueDate
     */
    public String getPseudoDueDate() {
        return pseudoDueDate;
    }
    /**
     * <p>Setter method for pseudoDueDate.</p>
     *
     * @param pseudoDueDate Set for pseudoDueDate
     */
    public void setPseudoDueDate(String pseudoDueDate) {
        this.pseudoDueDate = pseudoDueDate;
    }
    /**
     * <p>Getter method for pseudoOrderType.</p>
     *
     * @return the pseudoOrderType
     */
    public String getPseudoOrderType() {
        return pseudoOrderType;
    }
    /**
     * <p>Setter method for pseudoOrderType.</p>
     *
     * @param pseudoOrderType Set for pseudoOrderType
     */
    public void setPseudoOrderType(String pseudoOrderType) {
        this.pseudoOrderType = pseudoOrderType;
    }
    /**
     * <p>Getter method for pseudoReportType.</p>
     *
     * @return the pseudoReportType
     */
    public String getPseudoReportType() {
        return pseudoReportType;
    }
    /**
     * <p>Setter method for pseudoReportType.</p>
     *
     * @param pseudoReportType Set for pseudoReportType
     */
    public void setPseudoReportType(String pseudoReportType) {
        this.pseudoReportType = pseudoReportType;
    }
    /**
     * <p>Getter method for pseudoReason.</p>
     *
     * @return the pseudoReason
     */
    public String getPseudoReason() {
        return pseudoReason;
    }
    /**
     * <p>Setter method for pseudoReason.</p>
     *
     * @param pseudoReason Set for pseudoReason
     */
    public void setPseudoReason(String pseudoReason) {
        this.pseudoReason = pseudoReason;
    }
    /**
     * <p>Getter method for pseudoOldQty.</p>
     *
     * @return the pseudoOldQty
     */
    public String getPseudoOldQty() {
        return pseudoOldQty;
    }
    /**
     * <p>Setter method for pseudoOldQty.</p>
     *
     * @param pseudoOldQty Set for pseudoOldQty
     */
    public void setPseudoOldQty(String pseudoOldQty) {
        this.pseudoOldQty = pseudoOldQty;
    }
    /**
     * <p>Getter method for pseudoNewQty.</p>
     *
     * @return the pseudoNewQty
     */
    public String getPseudoNewQty() {
        return pseudoNewQty;
    }
    /**
     * <p>Setter method for pseudoNewQty.</p>
     *
     * @param pseudoNewQty Set for pseudoNewQty
     */
    public void setPseudoNewQty(String pseudoNewQty) {
        this.pseudoNewQty = pseudoNewQty;
    }
    /**
     * <p>Getter method for pseudoDifferenceQty.</p>
     *
     * @return the pseudoDifferenceQty
     */
    public String getPseudoDifferenceQty() {
        return pseudoDifferenceQty;
    }
    /**
     * <p>Setter method for pseudoDifferenceQty.</p>
     *
     * @param pseudoDifferenceQty Set for pseudoDifferenceQty
     */
    public void setPseudoDifferenceQty(String pseudoDifferenceQty) {
        this.pseudoDifferenceQty = pseudoDifferenceQty;
    }
    /**
     * <p>Getter method for pseudoUnitOfMeasure.</p>
     *
     * @return the pseudoUnitOfMeasure
     */
    public String getPseudoUnitOfMeasure() {
        return pseudoUnitOfMeasure;
    }
    /**
     * <p>Setter method for pseudoUnitOfMeasure.</p>
     *
     * @param pseudoUnitOfMeasure Set for pseudoUnitOfMeasure
     */
    public void setPseudoUnitOfMeasure(String pseudoUnitOfMeasure) {
        this.pseudoUnitOfMeasure = pseudoUnitOfMeasure;
    }
    /**
     * <p>Getter method for pseudoItemDescription.</p>
     *
     * @return the pseudoItemDescription
     */
    public String getPseudoItemDescription() {
        return pseudoItemDescription;
    }
    /**
     * <p>Setter method for pseudoItemDescription.</p>
     *
     * @param pseudoItemDescription Set for pseudoItemDescription
     */
    public void setPseudoItemDescription(String pseudoItemDescription) {
        this.pseudoItemDescription = pseudoItemDescription;
    }
    /**
     * <p>Getter method for pseudoEngineeringDrawingNumber.</p>
     *
     * @return the pseudoEngineeringDrawingNumber
     */
    public String getPseudoEngineeringDrawingNumber() {
        return pseudoEngineeringDrawingNumber;
    }
    /**
     * <p>Setter method for pseudoEngineeringDrawingNumber.</p>
     *
     * @param pseudoEngineeringDrawingNumber Set for pseudoEngineeringDrawingNumber
     */
    public void setPseudoEngineeringDrawingNumber(
        String pseudoEngineeringDrawingNumber) {
        this.pseudoEngineeringDrawingNumber = pseudoEngineeringDrawingNumber;
    }
    /**
     * <p>Getter method for pseudoTransportationCode.</p>
     *
     * @return the pseudoTransportationCode
     */
    public String getPseudoTransportationCode() {
        return pseudoTransportationCode;
    }
    /**
     * <p>Setter method for pseudoTransportationCode.</p>
     *
     * @param pseudoTransportationCode Set for pseudoTransportationCode
     */
    public void setPseudoTransportationCode(String pseudoTransportationCode) {
        this.pseudoTransportationCode = pseudoTransportationCode;
    }
    /**
     * <p>Getter method for pseudoSellerName.</p>
     *
     * @return the pseudoSellerName
     */
    public String getPseudoSellerName() {
        return pseudoSellerName;
    }
    /**
     * <p>Setter method for pseudoSellerName.</p>
     *
     * @param pseudoSellerName Set for pseudoSellerName
     */
    public void setPseudoSellerName(String pseudoSellerName) {
        this.pseudoSellerName = pseudoSellerName;
    }
    /**
     * <p>Getter method for pseudoEtd.</p>
     *
     * @return the pseudoEtd
     */
    public String getPseudoEtd() {
        return pseudoEtd;
    }
    /**
     * <p>Setter method for pseudoEtd.</p>
     *
     * @param pseudoEtd Set for pseudoEtd
     */
    public void setPseudoEtd(String pseudoEtd) {
        this.pseudoEtd = pseudoEtd;
    }
    /**
     * <p>Getter method for pseudoForceAcknowledgeDate.</p>
     *
     * @return the pseudoForceAcknowledgeDate
     */
    public String getPseudoForceAcknowledgeDate() {
        return pseudoForceAcknowledgeDate;
    }
    /**
     * <p>Setter method for pseudoForceAcknowledgeDate.</p>
     *
     * @param pseudoForceAcknowledgeDate Set for pseudoForceAcknowledgeDate
     */
    public void setPseudoForceAcknowledgeDate(String pseudoForceAcknowledgeDate) {
        this.pseudoForceAcknowledgeDate = pseudoForceAcknowledgeDate;
    }
    /**
     * <p>Getter method for pseudoTm.</p>
     *
     * @return the pseudoTm
     */
    public String getPseudoTm() {
        return pseudoTm;
    }
    /**
     * <p>Setter method for pseudoTm.</p>
     *
     * @param pseudoTm Set for pseudoTm
     */
    public void setPseudoTm(String pseudoTm) {
        this.pseudoTm = pseudoTm;
    }
    /**
     * <p>Getter method for pseudoSpsFlag.</p>
     *
     * @return the pseudoSpsFlag
     */
    public String getPseudoSpsFlag() {
        return pseudoSpsFlag;
    }
    /**
     * <p>Setter method for pseudoSpsFlag.</p>
     *
     * @param pseudoSpsFlag Set for pseudoSpsFlag
     */
    public void setPseudoSpsFlag(String pseudoSpsFlag) {
        this.pseudoSpsFlag = pseudoSpsFlag;
    }
    /**
     * <p>Getter method for pseudoCreateByUserId.</p>
     *
     * @return the pseudoCreateByUserId
     */
    public String getPseudoCreateByUserId() {
        return pseudoCreateByUserId;
    }
    /**
     * <p>Setter method for pseudoCreateByUserId.</p>
     *
     * @param pseudoCreateByUserId Set for pseudoCreateByUserId
     */
    public void setPseudoCreateByUserId(String pseudoCreateByUserId) {
        this.pseudoCreateByUserId = pseudoCreateByUserId;
    }
    /**
     * <p>Getter method for pseudoCreateDate.</p>
     *
     * @return the pseudoCreateDate
     */
    public String getPseudoCreateDate() {
        return pseudoCreateDate;
    }
    /**
     * <p>Setter method for pseudoCreateDate.</p>
     *
     * @param pseudoCreateDate Set for pseudoCreateDate
     */
    public void setPseudoCreateDate(String pseudoCreateDate) {
        this.pseudoCreateDate = pseudoCreateDate;
    }
    /**
     * <p>Getter method for pseudoCreateTime.</p>
     *
     * @return the pseudoCreateTime
     */
    public String getPseudoCreateTime() {
        return pseudoCreateTime;
    }
    /**
     * <p>Setter method for pseudoCreateTime.</p>
     *
     * @param pseudoCreateTime Set for pseudoCreateTime
     */
    public void setPseudoCreateTime(String pseudoCreateTime) {
        this.pseudoCreateTime = pseudoCreateTime;
    }
    /**
     * <p>Getter method for pseudoUpdateByUserId.</p>
     *
     * @return the pseudoUpdateByUserId
     */
    public String getPseudoUpdateByUserId() {
        return pseudoUpdateByUserId;
    }
    /**
     * <p>Setter method for pseudoUpdateByUserId.</p>
     *
     * @param pseudoUpdateByUserId Set for pseudoUpdateByUserId
     */
    public void setPseudoUpdateByUserId(String pseudoUpdateByUserId) {
        this.pseudoUpdateByUserId = pseudoUpdateByUserId;
    }
    /**
     * <p>Getter method for pseudoUpdateDate.</p>
     *
     * @return the pseudoUpdateDate
     */
    public String getPseudoUpdateDate() {
        return pseudoUpdateDate;
    }
    /**
     * <p>Setter method for pseudoUpdateDate.</p>
     *
     * @param pseudoUpdateDate Set for pseudoUpdateDate
     */
    public void setPseudoUpdateDate(String pseudoUpdateDate) {
        this.pseudoUpdateDate = pseudoUpdateDate;
    }
    /**
     * <p>Getter method for pseudoUpdateTime.</p>
     *
     * @return the pseudoUpdateTime
     */
    public String getPseudoUpdateTime() {
        return pseudoUpdateTime;
    }
    /**
     * <p>Setter method for pseudoUpdateTime.</p>
     *
     * @param pseudoUpdateTime Set for pseudoUpdateTime
     */
    public void setPseudoUpdateTime(String pseudoUpdateTime) {
        this.pseudoUpdateTime = pseudoUpdateTime;
    }
    /**
     * <p>Getter method for pseudoAddTruckRouteFlag.</p>
     *
     * @return the pseudoAddTruckRouteFlag
     */
    public String getPseudoAddTruckRouteFlag() {
        return pseudoAddTruckRouteFlag;
    }
    /**
     * <p>Setter method for pseudoAddTruckRouteFlag.</p>
     *
     * @param pseudoAddTruckRouteFlag Set for pseudoAddTruckRouteFlag
     */
    public void setPseudoAddTruckRouteFlag(String pseudoAddTruckRouteFlag) {
        this.pseudoAddTruckRouteFlag = pseudoAddTruckRouteFlag;
    }
    /**
     * <p>Getter method for pseudoTmpPriceFlg.</p>
     *
     * @return the pseudoTmpPriceFlg
     */
    public String getPseudoTmpPriceFlg() {
        return pseudoTmpPriceFlg;
    }
    /**
     * <p>Setter method for pseudoTmpPriceFlg.</p>
     *
     * @param pseudoTmpPriceFlg Set for pseudoTmpPriceFlg
     */
    public void setPseudoTmpPriceFlg(String pseudoTmpPriceFlg) {
        this.pseudoTmpPriceFlg = pseudoTmpPriceFlg;
    }
}
