/*
 * ModifyDate Development company     Describe 
 * 2014/07/24  CSI Karnrawee          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Pseudo PO Domain for receive value from Web Service
 * 
 * @author CSI Karnrawee
 * @version 1.0.0
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaAsnInformationDomain")
public class PseudoCigmaAsnDomain extends PseudoBaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3030218759501306702L;
    
    /** The pseudo of sps asn no. */
    @XmlElement(name = "asnNo")
    private String pseudoAsnNo;

    /** The pseudo of received date. */
    @XmlElement(name = "receivedDate")
    private String pseudoReceivedDate;

    /** The pseudo of asn status. */
    @XmlElement(name = "asnStatus")
    private String pseudoAsnStatus;

    /** The pseudo of CIGMA current D/O no. */
    @XmlElement(name = "cigmaCurDoNo")
    private String pseudoCigmaCurDoNo;

    /** The pseudo of CIGMA original D/O no. */
    @XmlElement(name = "cigmaOrgDoNo")
    private String pseudoCigmaOrgDoNo;

    /** The pseudo of part no. */
    @XmlElement(name = "dPn")
    private String pseudoDPn;
    
    /** The pseudo of PN receiving status. */
    @XmlElement(name = "pnReceivingStatus")
    private String pseudoPnReceivingStatus;
    
    /** The pseudo of shipping qty. */
    @XmlElement(name = "shippingQty")
    private String pseudoShippingQty;
    
    /** The pseudo of received qty. */
    @XmlElement(name = "receivedQty")
    private String pseudoReceivedQty;

    /** The pseudo of remaining qty. */
    @XmlElement(name = "remainingQty")
    private String pseudoRemainingQty;
    
    /** The pseudo of VAR. */
    @XmlElement(name = "var")
    private String pseudoVar;
    
    /** The pseudo of INS. */
    @XmlElement(name = "ins")
    private String pseudoIns;
    
    /** The pseudo of ware house. */
    @XmlElement(name = "wareHouse")
    private String pseudoWareHouse;
    
    /** The pseudo of due date. */
    @XmlElement(name = "dueDate")
    private String pseudoDueDate;
    
    /** The pseudo of receiving lane. */
    @XmlElement(name = "receivingLane")
    private String pseudoReceivingLane;
    
    //For search WINV001
    /** The pseudo of invoice no. */
    @XmlElement(name = "invoiceNo")
    private String pseudoInvoiceNo;
    
    //For search WSHP007
    /** The pseudo of sps do no. */
    @XmlElement(name = "spsDoNo")
    private String pseudoSpsDoNo;
    
    /** The pseudo of last update date. */
    @XmlElement(name = "lastUpdate")
    private String pseudoLastUpdate;
    
    /** The pseudo of last update time. */
    @XmlElement(name = "lastUpdateTime")
    private String pseudoLastUpdateTime;
    
    /** The pseudo of pn last update date. */
    @XmlElement(name = "pnLastUpdate")
    private String pseudoPnLastUpdate;
    
    /** The pseudo of pn last update time. */
    @XmlElement(name = "pnLastUpdateTime")
    private String pseudoPnLastUpdateTime;
    
    /**
     * The default constructor.
     * */
    public PseudoCigmaAsnDomain() {
    }

    /**
     * @return the pseudo of Asn No
     */
    public String getPseudoAsnNo() {
        return pseudoAsnNo;
    }

    /**
     * @param pseudoAsnNo the pseudo of Asn No to set
     */
    public void setPseudoAsnNo(String pseudoAsnNo) {
        this.pseudoAsnNo = pseudoAsnNo;
    }

    /**
     * @return the pseudo of Received Date
     */
    public String getPseudoReceivedDate() {
        return pseudoReceivedDate;
    }

    /**
     * @param pseudoReceivedDate the pseudo of Received Date to set
     */
    public void setPseudoReceivedDate(String pseudoReceivedDate) {
        this.pseudoReceivedDate = pseudoReceivedDate;
    }

    /**
     * @return the pseudo of Asn Status
     */
    public String getPseudoAsnStatus() {
        return pseudoAsnStatus;
    }

    /**
     * @param pseudoAsnStatus the pseudo of Asn Status to set
     */
    public void setPseudoAsnStatus(String pseudoAsnStatus) {
        this.pseudoAsnStatus = pseudoAsnStatus;
    }

    /**
     * @return the pseudo of CIGMA current D/O no.
     */
    public String getPseudoCigmaCurDoNo() {
        return pseudoCigmaCurDoNo;
    }

    /**
     * @param pseudoCigmaCurDoNo the pseudo of CIGMA current D/O no to set
     */
    public void setPseudoCigmaCurDoNo(String pseudoCigmaCurDoNo) {
        this.pseudoCigmaCurDoNo = pseudoCigmaCurDoNo;
    }

    /**
     * @return the pseudo of Cigma Org DO No
     */
    public String getPseudoCigmaOrgDoNo() {
        return pseudoCigmaOrgDoNo;
    }

    /**
     * @param pseudoCigmaOrgDoNo the pseudo of Cigma Org DO No to set
     */
    public void setPseudoCigmaOrgDoNo(String pseudoCigmaOrgDoNo) {
        this.pseudoCigmaOrgDoNo = pseudoCigmaOrgDoNo;
    }

    /**
     * @return the pseudo of part no
     */
    public String getPseudoDPn() {
        return pseudoDPn;
    }

    /**
     * @param pseudoDPn the pseudo of part no to set
     */
    public void setPseudoDPn(String pseudoDPn) {
        this.pseudoDPn = pseudoDPn;
    }
    
    /**
     * @return the pseudo of PN receiving status
     */
    public String getPseudoPnReceivingStatus() {
        return pseudoPnReceivingStatus;
    }
    
    /**
     * @param pseudoPnReceivingStatus the pseudo of PN receiving status to set
     */
    public void setPseudoPnReceivingStatus(String pseudoPnReceivingStatus) {
        this.pseudoPnReceivingStatus = pseudoPnReceivingStatus;
    }
    
    /**
     * @return the pseudo of shipping qty
     */
    public String getPseudoShippingQty() {
        return pseudoShippingQty;
    }

    /**
     * @param pseudoShippingQty the pseudo of shipping qty to set
     */
    public void setPseudoShippingQty(String pseudoShippingQty) {
        this.pseudoShippingQty = pseudoShippingQty;
    }
    
    /**
     * @return the pseudo of received qty
     */
    public String getPseudoReceivedQty() {
        return pseudoReceivedQty;
    }

    /**
     * @param pseudoReceivedQty the pseudo of received qty to set
     */
    public void setPseudoReceivedQty(String pseudoReceivedQty) {
        this.pseudoReceivedQty = pseudoReceivedQty;
    }
    
    /**
     * @return the pseudo of remaining qty
     */
    public String getPseudoRemainingQty() {
        return pseudoRemainingQty;
    }

    /**
     * @param pseudoRemainingQty the pseudo of remaining qty to set
     */
    public void setPseudoRemainingQTY(String pseudoRemainingQty) {
        this.pseudoRemainingQty = pseudoRemainingQty;
    }
    
    /**
     * @return the pseudo of Var
     */
    public String getPseudoVar() {
        return pseudoVar;
    }

    /**
     * @param pseudoVar the pseudo of Var to set
     */
    public void setPseudoVar(String pseudoVar) {
        this.pseudoVar = pseudoVar;
    }
    
    /**
     * @return the pseudo of Ins
     */
    public String getPseudoIns() {
        return pseudoIns;
    }

    /**
     * @param pseudoIns the pseudo of Ins to set
     */
    public void setPseudoIns(String pseudoIns) {
        this.pseudoIns = pseudoIns;
    }
    
    /**
     * @return the pseudo of ware house
     */
    public String getPseudoWareHouse() {
        return pseudoWareHouse;
    }

    /**
     * @param pseudoWareHouse the pseudo of ware house to set
     */
    public void setPseudoWareHouse(String pseudoWareHouse) {
        this.pseudoWareHouse = pseudoWareHouse;
    }
    

    /**
     * @return the pseudo of Due Date
     */
    public String getPseudoDueDate() {
        return pseudoDueDate;
    }

    /**
     * @param pseudoDueDate the pseudo of Due Date to set
     */
    public void setPseudoDueDate(String pseudoDueDate) {
        this.pseudoDueDate = pseudoDueDate;
    }
    
    /**
     * @return the pseudo of receiving lane
     */
    public String getPseudoReceivingLane() {
        return pseudoReceivingLane;
    }

    /**
     * @param pseudoReceivingLane the pseudo of receiving lane to set
     */
    public void setPseudoReceivingLane(String pseudoReceivingLane) {
        this.pseudoReceivingLane = pseudoReceivingLane;
    }
    
    /**
     * @return the pseudo of invoice no
     */
    public String getPseudoInvoiceNo() {
        return pseudoInvoiceNo;
    }

    /**
     * @param pseudoInvoiceNo the pseudo of invoice no to set
     */
    public void setPseudoInvoiceNo(String pseudoInvoiceNo) {
        this.pseudoInvoiceNo = pseudoInvoiceNo;
    }

    /**
     * <p>Getter method for pseudoSpsDoNo.</p>
     *
     * @return the pseudoSpsDoNo
     */
    public String getPseudoSpsDoNo() {
        return pseudoSpsDoNo;
    }

    /**
     * <p>Setter method for pseudoSpsDoNo.</p>
     *
     * @param pseudoSpsDoNo Set for pseudoSpsDoNo
     */
    public void setPseudoSpsDoNo(String pseudoSpsDoNo) {
        this.pseudoSpsDoNo = pseudoSpsDoNo;
    }

    /**
     * @return the pseudo of last update
     */
    public String getPseudoLastUpdate() {
        return pseudoLastUpdate;
    }

    /**
     * @param pseudoLastUpdate the pseudo of last update to set
     */
    public void setPseudoLastUpdate(String pseudoLastUpdate) {
        this.pseudoLastUpdate = pseudoLastUpdate;
    }
    
    /**
     * @return the pseudo of last update time
     */
    public String getPseudoLastUpdateTime() {
        return pseudoLastUpdateTime;
    }

    /**
     * @param pseudoLastUpdateTime the pseudo of last update time to set
     */
    public void setPseudoLastUpdateTime(String pseudoLastUpdateTime) {
        this.pseudoLastUpdateTime = pseudoLastUpdateTime;
    }

    /**
     * @return the pseudo of pn last update
     */
    public String getPseudoPnLastUpdate() {
        return pseudoPnLastUpdate;
    }

    /**
     * @param pseudoPnLastUpdate the pseudo of pn last update to set
     */
    public void setPseudoPnLastUpdate(String pseudoPnLastUpdate) {
        this.pseudoPnLastUpdate = pseudoPnLastUpdate;
    }

    /**
     * @return the pseudo of pn last time
     */
    public String getPseudoPnLastUpdateTime() {
        return pseudoPnLastUpdateTime;
    }

    /**
     * @param pseudoPnLastUpdateTime the pseudo of pn last update time to set
     */
    public void setPseudoPnLastUpdateTime(String pseudoPnLastUpdateTime) {
        this.pseudoPnLastUpdateTime = pseudoPnLastUpdateTime;
    }
}
