/*
 * ModifyDate Development company     Describe 
 * 2014/09/01 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>Purchase Order Cover Page Result Report Domain.</p>
 *
 * @author CSI
  */
public class PurchaseOrderCoverPageReportDomain extends BaseDomain implements Serializable {
    
    /** Serializable ID */
    private static final long serialVersionUID = 2836779715427358012L;
    /** * Release No  */
    private String releaseNo;
    
    /** SPS P/O No */
    private String spsPoNo;
    
    /** CIGMA P/O No */
    private String cigmaPoNo;
    
    /** PO Issue date */
    private Date poIssueDate;
    
    /** Issue date */
    private String issueDate;
    
    /** Seller Contract Name */
    private String sellerContractName;
    
    /** Seller Name */
    private String sellerName;
    
    /** Seller Address1 */
    private String sellerAddress1;
    
    /** Seller Address2 */
    private String sellerAddress2;
    
    /** Seller Address3 */
    private String sellerAddress3;
    
    /** Supplier Code */
    private String sCd;
    
    /** Seller Fax Number */
    private String sellerFaxNumber;
    
    /** Payment Term */
    private String paymentTerm;
    
    /** Ship VIA */
    private String shipVia;
    
    /** Price Term */
    private String priceTerm;
    
    /** Purchaser Name */
    private String purchaserName;
    
    /** Purchaser Contract Name */
    private String purchaserContractName;
    
    /** Purchaser Address1 */
    private String purchaserAddress1;
    
    /** Purchaser Address2 */
    private String purchaserAddress2;
    
    /** Purchaser Address3 */
    private String purchaserAddress3;
    
    /** Delivery Order Due Date */
    private Date deliveryOrderDate;
    
    /** Delivery Order Due Date display */
    private String deliveryOrderDueDate;
    
    /** CIGMA D/O No */
    private String cigmaDoNo;
    
    /** Page No */
    private String pageNo;
    
    /** Company Denso Code */
    private String dCd;
    
    /** Company Denso Plant Code */
    private String dPcd;
    
    /** Company Name */
    private String companyName;
    
    /** Force Acknowledge Date */
    private Date forceAckDate;
    
    /** P/O Terms and Condition Flag */
    private String poTermsAndConditionFlg;
    
    /** Flag for show DENSO plant code */
    private String showDensoPlantFlg;
    
    /**
     * 
     * <p>Constructor Purchase Order Cover Page Report Domain.</p>
     *
     */
    public PurchaseOrderCoverPageReportDomain() {
    }
    /**
     * <p>Getter method for releaseNo.</p>
     *
     * @return the releaseNo
     */
    public String getReleaseNo() {
        return releaseNo;
    }
    /**
     * <p>Setter method for releaseNo.</p>
     *
     * @param releaseNo Set for releaseNo
     */
    public void setReleaseNo(String releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * <p>Getter method for spsPoNo.</p>
     *
     * @return the spsPoNo
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }
    /**
     * <p>Setter method for spsPoNo.</p>
     *
     * @param spsPoNo Set for spsPoNo
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }
    /**
     * <p>Getter method for cigmaPoNo.</p>
     *
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * <p>Setter method for cigmaPoNo.</p>
     *
     * @param cigmaPoNo Set for cigmaPoNo
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }
    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }
    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }
    /**
     * <p>Getter method for sellerContractName.</p>
     *
     * @return the sellerContractName
     */
    public String getSellerContractName() {
        return sellerContractName;
    }
    /**
     * <p>Setter method for sellerContractName.</p>
     *
     * @param sellerContractName Set for sellerContractName
     */
    public void setSellerContractName(String sellerContractName) {
        this.sellerContractName = sellerContractName;
    }
    /**
     * <p>Getter method for sellerName.</p>
     *
     * @return the sellerName
     */
    public String getSellerName() {
        return sellerName;
    }
    /**
     * <p>Setter method for sellerName.</p>
     *
     * @param sellerName Set for sellerName
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
    /**
     * <p>Getter method for sellerAddress1.</p>
     *
     * @return the sellerAddress1
     */
    public String getSellerAddress1() {
        return sellerAddress1;
    }
    /**
     * <p>Setter method for sellerAddress1.</p>
     *
     * @param sellerAddress1 Set for sellerAddress1
     */
    public void setSellerAddress1(String sellerAddress1) {
        this.sellerAddress1 = sellerAddress1;
    }
    /**
     * <p>Getter method for sellerAddress2.</p>
     *
     * @return the sellerAddress2
     */
    public String getSellerAddress2() {
        return sellerAddress2;
    }
    /**
     * <p>Setter method for sellerAddress2.</p>
     *
     * @param sellerAddress2 Set for sellerAddress2
     */
    public void setSellerAddress2(String sellerAddress2) {
        this.sellerAddress2 = sellerAddress2;
    }
    /**
     * <p>Getter method for sellerAddress3.</p>
     *
     * @return the sellerAddress3
     */
    public String getSellerAddress3() {
        return sellerAddress3;
    }
    /**
     * <p>Setter method for sellerAddress3.</p>
     *
     * @param sellerAddress3 Set for sellerAddress3
     */
    public void setSellerAddress3(String sellerAddress3) {
        this.sellerAddress3 = sellerAddress3;
    }
    /**
     * <p>Getter method for supplierCode.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * <p>Setter method for supplierCode.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * <p>Getter method for sellerFaxNumber.</p>
     *
     * @return the sellerFaxNumber
     */
    public String getSellerFaxNumber() {
        return sellerFaxNumber;
    }
    /**
     * <p>Setter method for sellerFaxNumber.</p>
     *
     * @param sellerFaxNumber Set for sellerFaxNumber
     */
    public void setSellerFaxNumber(String sellerFaxNumber) {
        this.sellerFaxNumber = sellerFaxNumber;
    }
    /**
     * <p>Getter method for paymentTerm.</p>
     *
     * @return the paymentTerm
     */
    public String getPaymentTerm() {
        return paymentTerm;
    }
    /**
     * <p>Setter method for paymentTerm.</p>
     *
     * @param paymentTerm Set for paymentTerm
     */
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }
    /**
     * <p>Getter method for shipVia.</p>
     *
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }
    /**
     * <p>Setter method for shipVia.</p>
     *
     * @param shipVia Set for shipVia
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }
    /**
     * <p>Getter method for priceTerm.</p>
     *
     * @return the priceTerm
     */
    public String getPriceTerm() {
        return priceTerm;
    }
    /**
     * <p>Setter method for priceTerm.</p>
     *
     * @param priceTerm Set for priceTerm
     */
    public void setPriceTerm(String priceTerm) {
        this.priceTerm = priceTerm;
    }
    /**
     * <p>Getter method for purchaserName.</p>
     *
     * @return the purchaserName
     */
    public String getPurchaserName() {
        return purchaserName;
    }
    /**
     * <p>Setter method for purchaserName.</p>
     *
     * @param purchaserName Set for purchaserName
     */
    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }
    /**
     * <p>Getter method for purchaserContractName.</p>
     *
     * @return the purchaserContractName
     */
    public String getPurchaserContractName() {
        return purchaserContractName;
    }
    /**
     * <p>Setter method for purchaserContractName.</p>
     *
     * @param purchaserContractName Set for purchaserContractName
     */
    public void setPurchaserContractName(String purchaserContractName) {
        this.purchaserContractName = purchaserContractName;
    }
    /**
     * <p>Getter method for purchaserAddress1.</p>
     *
     * @return the purchaserAddress1
     */
    public String getPurchaserAddress1() {
        return purchaserAddress1;
    }
    /**
     * <p>Setter method for purchaserAddress1.</p>
     *
     * @param purchaserAddress1 Set for purchaserAddress1
     */
    public void setPurchaserAddress1(String purchaserAddress1) {
        this.purchaserAddress1 = purchaserAddress1;
    }
    /**
     * <p>Getter method for purchaserAddress2.</p>
     *
     * @return the purchaserAddress2
     */
    public String getPurchaserAddress2() {
        return purchaserAddress2;
    }
    /**
     * <p>Setter method for purchaserAddress2.</p>
     *
     * @param purchaserAddress2 Set for purchaserAddress2
     */
    public void setPurchaserAddress2(String purchaserAddress2) {
        this.purchaserAddress2 = purchaserAddress2;
    }
    /**
     * <p>Getter method for purchaserAddress3.</p>
     *
     * @return the purchaserAddress3
     */
    public String getPurchaserAddress3() {
        return purchaserAddress3;
    }
    /**
     * <p>Setter method for purchaserAddress3.</p>
     *
     * @param purchaserAddress3 Set for purchaserAddress3
     */
    public void setPurchaserAddress3(String purchaserAddress3) {
        this.purchaserAddress3 = purchaserAddress3;
    }
    /**
     * <p>Getter method for deliveryOrderDueDate.</p>
     *
     * @return the deliveryOrderDueDate
     */
    public String getDeliveryOrderDueDate() {
        return deliveryOrderDueDate;
    }
    /**
     * <p>Setter method for deliveryOrderDueDate.</p>
     *
     * @param deliveryOrderDueDate Set for deliveryOrderDueDate
     */
    public void setDeliveryOrderDueDate(String deliveryOrderDueDate) {
        this.deliveryOrderDueDate = deliveryOrderDueDate;
    }
    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }
    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }
    /**
     * <p>Getter method for pageNo.</p>
     *
     * @return the pageNo
     */
    public String getPageNo() {
        return pageNo;
    }
    /**
     * <p>Setter method for pageNo.</p>
     *
     * @param pageNo Set for pageNo
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }
    /**
     * <p>Getter method for poIssueDate.</p>
     *
     * @return the poIssueDate
     */
    public Date getPoIssueDate() {
        return poIssueDate;
    }
    /**
     * <p>Setter method for poIssueDate.</p>
     *
     * @param poIssueDate Set for poIssueDate
     */
    public void setPoIssueDate(Date poIssueDate) {
        this.poIssueDate = poIssueDate;
    }
    /**
     * <p>Getter method for deliveryOrderDate.</p>
     *
     * @return the deliveryOrderDate
     */
    public Date getDeliveryOrderDate() {
        return deliveryOrderDate;
    }
    /**
     * <p>Setter method for deliveryOrderDate.</p>
     *
     * @param deliveryOrderDate Set for deliveryOrderDate
     */
    public void setDeliveryOrderDate(Date deliveryOrderDate) {
        this.deliveryOrderDate = deliveryOrderDate;
    }
    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * <p>Getter method for companyName.</p>
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }
    /**
     * <p>Setter method for companyName.</p>
     *
     * @param companyName Set for companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    /**
     * <p>Getter method for forceAckDate.</p>
     *
     * @return the forceAckDate
     */
    public Date getForceAckDate() {
        return forceAckDate;
    }
    
    /**
     * <p>Setter method for forceAckDate.</p>
     *
     * @param forceAckDate Set for forceAckDate
     */
    public void setForceAckDate(Date forceAckDate) {
        this.forceAckDate = forceAckDate;
    }
    
    /**
     * <p>Getter method for poTermsAndConditionFlg.</p>
     *
     * @return the poTermsAndConditionFlg
     */
    public String getPoTermsAndConditionFlg() {
        return poTermsAndConditionFlg;
    }
    
    /**
     * <p>Setter method for poTermsAndConditionFlg.</p>
     *
     * @param poTermsAndConditionFlg Set for poTermsAndConditionFlg
     */
    public void setPoTermsAndConditionFlg(String poTermsAndConditionFlg) {
        this.poTermsAndConditionFlg = poTermsAndConditionFlg;
    }
    
    /**
     * <p>Getter method for showDensoPlantFlg.</p>
     *
     * @return the showDensoPlantFlg
     */
    public String getShowDensoPlantFlg() {
        return showDensoPlantFlg;
    }
    
    /**
     * <p>Setter method for showDensoPlantFlg.</p>
     *
     * @param showDensoPlantFlg Set for showDensoPlantFlg
     */
    public void setShowDensoPlantFlg(String showDensoPlantFlg) {
        this.showDensoPlantFlg = showDensoPlantFlg;
    }
    
}
