/*
 * ModifyDate Development company    Describe 
 * 2014/08/14 CSI Phakaporn P.        Create   
 * 2016/04/07 CSI Akat                [IN068]
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class GroupUploadErrorDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class GroupUploadErrorDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2870205742807892054L;

    /** The User DSC ID. */
    private String userDscId;
    
    /** The Line No. **/
    private String lineNo;
    
    /** The Session ID. */
    private String sessionId;
    
    /** The Message Type. */
    private String messageType;
    
    /** The Description. */
    private String description;       
    
    // [IN068] Add new field for keep message parameter
    /** Message Parameter. */
    private String messageParam;
    
    /**
     * Instantiates a new GroupUploadErrorDomain
     */
    public GroupUploadErrorDomain() {
        super();
    }

    /**
     * Gets the User DSC ID.
     * 
     * @return the User DSC ID.
     */
    public String getUserDscId() {
        return userDscId;
    }
    /**
     * Sets the User DSC ID.
     * 
     * @param userDscId the User DSC ID.
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }
    /**
     * Gets the line number.
     * 
     * @return the line number.
     */
    public String getLineNo() {
        return lineNo;
    }
    /**
     * Sets the line number.
     * 
     * @param lineNo the line number.
     */
    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }
    /**
     * Gets the Session ID.
     * 
     * @return the Session ID.
     */
    public String getSessionId() {
        return sessionId;
    }
    /**
     * Sets the Session ID.
     * 
     * @param sessionId the Session ID.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    /**
     * Gets the message type.
     * 
     * @return the message type.
     */
    public String getMessageType() {
        return messageType;
    }
    /**
     * Sets the message type.
     * 
     * @param messageType the message type.
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
    /**
     * Gets the description.
     * 
     * @return the description.
     */
    public String getDescription() {
        return description;
    }
    /**
     * Sets the description.
     * 
     * @param description the description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter method for messageParam.</p>
     *
     * @return the messageParam
     */
    public String getMessageParam() {
        return messageParam;
    }

    /**
     * <p>Setter method for messageParam.</p>
     *
     * @param messageParam Set for messageParam
     */
    public void setMessageParam(String messageParam) {
        this.messageParam = messageParam;
    }

}