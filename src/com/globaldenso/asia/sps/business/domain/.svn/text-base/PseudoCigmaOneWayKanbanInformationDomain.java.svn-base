/*
 * ModifyDate Development company     Describe 
 * 2017/10/12 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;




import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * The Pseudo Class CigmaOneWayKanbanInformationDomain.
 * @author Netband
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaOneWayKanbanInformationDomain")
public class PseudoCigmaOneWayKanbanInformationDomain extends PseudoBaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -8307071399533125718L;

    /** cigmaDoNo */
    @XmlElement(name = "cigmaDoNo")
    private String pseudoCigmaDoNo;
    /** sCd */
    @XmlElement(name = "sCd")
    private String pseudosCd;
    /** deliveryDate */
    @XmlElement(name = "deliveryDate")
    private String pseudoDeliveryDate;
    /** dnItemNumber */
    @XmlElement(name = "dnItemNumber")
    private String pseudoDnItemNumber;
    /** tagSeqNumber */
    @XmlElement(name = "tagSeqNumber")
    private String pseudoTagSeqNumber;
    /** kanbanType */
    @XmlElement(name = "kanbanType")
    private String pseudoKanbanType;
    /** capacity */
    @XmlElement(name = "capacity")
    private String pseudoCapacity;
    /** partialFlag 
     * "Y" = Partial Lot
     * "N" = Full Lot
     * */
    @XmlElement(name = "partialFlag")
    private String pseudoPartialFlag;
    /** kanbanInformation */
    @XmlElement(name = "kanbanInformation")
    private String pseudoKanbanInformation;
    /** createByPgmID */
    @XmlElement(name = "createByPgmID")
    private String pseudoCreateByPgmID;
    /** createDate */
    @XmlElement(name = "createDate")
    private String pseudoCreateDate;
    /** createTime */
    @XmlElement(name = "createTime")
    private String pseudoCreateTime;
    /** createByPgmID */
    @XmlElement(name = "updateByPgmID")
    private String pseudoUpdateByPgmID;
    /** createByPgmID */
    @XmlElement(name = "updateDate")
    private String pseudoUpdateDate;
    /** createByPgmID */
    @XmlElement(name = "updateTime")
    private String pseudoUpdateTime;
    /** spsFlag */
    @XmlElement(name = "spsFlag")
    private String pseudoSpsFlag;
    /** kanbanSeqFlg */
    @XmlElement(name = "kanbanSeqFlg")
    private String pseudoKanbanSeqFlg;
    /** kanbanDataType */
    @XmlElement(name = "kanbanDataType")
    private String pseudoKanbanDataType;
    
    /**
     * 
     * <p>The constructor.</p>
     *
     */
    public PseudoCigmaOneWayKanbanInformationDomain() {
    }

    /**
     * <p>Getter method for pseudoCigmaDoNo.</p>
     *
     * @return the pseudoCigmaDoNo
     */
    public String getPseudoCigmaDoNo() {
        return pseudoCigmaDoNo;
    }

    /**
     * <p>Setter method for pseudoCigmaDoNo.</p>
     *
     * @param pseudoCigmaDoNo Set for pseudoCigmaDoNo
     */
    public void setPseudoCigmaDoNo(String pseudoCigmaDoNo) {
        this.pseudoCigmaDoNo = pseudoCigmaDoNo;
    }

    /**
     * <p>Getter method for pseudosCd.</p>
     *
     * @return the pseudosCd
     */
    public String getPseudosCd() {
        return pseudosCd;
    }

    /**
     * <p>Setter method for pseudosCd.</p>
     *
     * @param pseudosCd Set for pseudosCd
     */
    public void setPseudosCd(String pseudosCd) {
        this.pseudosCd = pseudosCd;
    }

    /**
     * <p>Getter method for pseudoDeliveryDate.</p>
     *
     * @return the pseudoDeliveryDate
     */
    public String getPseudoDeliveryDate() {
        return pseudoDeliveryDate;
    }

    /**
     * <p>Setter method for pseudoDeliveryDate.</p>
     *
     * @param pseudoDeliveryDate Set for pseudoDeliveryDate
     */
    public void setPseudoDeliveryDate(String pseudoDeliveryDate) {
        this.pseudoDeliveryDate = pseudoDeliveryDate;
    }

    /**
     * <p>Getter method for pseudoDnItemNumber.</p>
     *
     * @return the pseudoDnItemNumber
     */
    public String getPseudoDnItemNumber() {
        return pseudoDnItemNumber;
    }

    /**
     * <p>Setter method for pseudoDnItemNumber.</p>
     *
     * @param pseudoDnItemNumber Set for pseudoDnItemNumber
     */
    public void setPseudoDnItemNumber(String pseudoDnItemNumber) {
        this.pseudoDnItemNumber = pseudoDnItemNumber;
    }

    /**
     * <p>Getter method for pseudoTagSeqNumber.</p>
     *
     * @return the pseudoTagSeqNumber
     */
    public String getPseudoTagSeqNumber() {
        return pseudoTagSeqNumber;
    }

    /**
     * <p>Setter method for pseudoTagSeqNumber.</p>
     *
     * @param pseudoTagSeqNumber Set for pseudoTagSeqNumber
     */
    public void setPseudoTagSeqNumber(String pseudoTagSeqNumber) {
        this.pseudoTagSeqNumber = pseudoTagSeqNumber;
    }

    /**
     * <p>Getter method for pseudoKanbanType.</p>
     *
     * @return the pseudoKanbanType
     */
    public String getPseudoKanbanType() {
        return pseudoKanbanType;
    }

    /**
     * <p>Setter method for pseudoKanbanType.</p>
     *
     * @param pseudoKanbanType Set for pseudoKanbanType
     */
    public void setPseudoKanbanType(String pseudoKanbanType) {
        this.pseudoKanbanType = pseudoKanbanType;
    }

    /**
     * <p>Getter method for pseudoCapacity.</p>
     *
     * @return the pseudoCapacity
     */
    public String getPseudoCapacity() {
        return pseudoCapacity;
    }

    /**
     * <p>Setter method for pseudoCapacity.</p>
     *
     * @param pseudoCapacity Set for pseudoCapacity
     */
    public void setPseudoCapacity(String pseudoCapacity) {
        this.pseudoCapacity = pseudoCapacity;
    }

    /**
     * <p>Getter method for pseudoPartialFlag.</p>
     *
     * @return the pseudoPartialFlag
     */
    public String getPseudoPartialFlag() {
        return pseudoPartialFlag;
    }

    /**
     * <p>Setter method for pseudoPartialFlag.</p>
     *
     * @param pseudoPartialFlag Set for pseudoPartialFlag
     */
    public void setPseudoPartialFlag(String pseudoPartialFlag) {
        this.pseudoPartialFlag = pseudoPartialFlag;
    }

    /**
     * <p>Getter method for pseudoKanbanInformation.</p>
     *
     * @return the pseudoKanbanInformation
     */
    public String getPseudoKanbanInformation() {
        return pseudoKanbanInformation;
    }

    /**
     * <p>Setter method for pseudoKanbanInformation.</p>
     *
     * @param pseudoKanbanInformation Set for pseudoKanbanInformation
     */
    public void setPseudoKanbanInformation(String pseudoKanbanInformation) {
        this.pseudoKanbanInformation = pseudoKanbanInformation;
    }

    /**
     * <p>Getter method for pseudoCreateByPgmID.</p>
     *
     * @return the pseudoCreateByPgmID
     */
    public String getPseudoCreateByPgmID() {
        return pseudoCreateByPgmID;
    }

    /**
     * <p>Setter method for pseudoCreateByPgmID.</p>
     *
     * @param pseudoCreateByPgmID Set for pseudoCreateByPgmID
     */
    public void setPseudoCreateByPgmID(String pseudoCreateByPgmID) {
        this.pseudoCreateByPgmID = pseudoCreateByPgmID;
    }

    /**
     * <p>Getter method for pseudoCreateDate.</p>
     *
     * @return the pseudoCreateDate
     */
    public String getPseudoCreateDate() {
        return pseudoCreateDate;
    }

    /**
     * <p>Setter method for pseudoCreateDate.</p>
     *
     * @param pseudoCreateDate Set for pseudoCreateDate
     */
    public void setPseudoCreateDate(String pseudoCreateDate) {
        this.pseudoCreateDate = pseudoCreateDate;
    }

    /**
     * <p>Getter method for pseudoCreateTime.</p>
     *
     * @return the pseudoCreateTime
     */
    public String getPseudoCreateTime() {
        return pseudoCreateTime;
    }

    /**
     * <p>Setter method for pseudoCreateTime.</p>
     *
     * @param pseudoCreateTime Set for pseudoCreateTime
     */
    public void setPseudoCreateTime(String pseudoCreateTime) {
        this.pseudoCreateTime = pseudoCreateTime;
    }

    /**
     * <p>Getter method for pseudoUpdateByPgmID.</p>
     *
     * @return the pseudoUpdateByPgmID
     */
    public String getPseudoUpdateByPgmID() {
        return pseudoUpdateByPgmID;
    }

    /**
     * <p>Setter method for pseudoUpdateByPgmID.</p>
     *
     * @param pseudoUpdateByPgmID Set for pseudoUpdateByPgmID
     */
    public void setPseudoUpdateByPgmID(String pseudoUpdateByPgmID) {
        this.pseudoUpdateByPgmID = pseudoUpdateByPgmID;
    }

    /**
     * <p>Getter method for pseudoUpdateDate.</p>
     *
     * @return the pseudoUpdateDate
     */
    public String getPseudoUpdateDate() {
        return pseudoUpdateDate;
    }

    /**
     * <p>Setter method for pseudoUpdateDate.</p>
     *
     * @param pseudoUpdateDate Set for pseudoUpdateDate
     */
    public void setPseudoUpdateDate(String pseudoUpdateDate) {
        this.pseudoUpdateDate = pseudoUpdateDate;
    }

    /**
     * <p>Getter method for pseudoUpdateTime.</p>
     *
     * @return the pseudoUpdateTime
     */
    public String getPseudoUpdateTime() {
        return pseudoUpdateTime;
    }

    /**
     * <p>Setter method for pseudoUpdateTime.</p>
     *
     * @param pseudoUpdateTime Set for pseudoUpdateTime
     */
    public void setPseudoUpdateTime(String pseudoUpdateTime) {
        this.pseudoUpdateTime = pseudoUpdateTime;
    }

    /**
     * <p>Getter method for pseudoSpsFlag.</p>
     *
     * @return the pseudoSpsFlag
     */
    public String getPseudoSpsFlag() {
        return pseudoSpsFlag;
    }

    /**
     * <p>Setter method for pseudoSpsFlag.</p>
     *
     * @param pseudoSpsFlag Set for pseudoSpsFlag
     */
    public void setPseudoSpsFlag(String pseudoSpsFlag) {
        this.pseudoSpsFlag = pseudoSpsFlag;
    }

    /**
     * <p>Getter method for pseudoKanbanSeqFlg.</p>
     *
     * @return the pseudoKanbanSeqFlg
     */
    public String getPseudoKanbanSeqFlg() {
        return pseudoKanbanSeqFlg;
    }

    /**
     * <p>Setter method for pseudoKanbanSeqFlg.</p>
     *
     * @param pseudoKanbanSeqFlg Set for pseudoKanbanSeqFlg
     */
    public void setPseudoKanbanSeqFlg(String pseudoKanbanSeqFlg) {
        this.pseudoKanbanSeqFlg = pseudoKanbanSeqFlg;
    }

    /**
     * <p>Getter method for pseudoKanbanDataType.</p>
     *
     * @return the pseudoKanbanDataType
     */
    public String getPseudoKanbanDataType() {
        return pseudoKanbanDataType;
    }

    /**
     * <p>Setter method for pseudoKanbanDataType.</p>
     *
     * @param pseudoKanbanDataType Set for pseudoKanbanDataType
     */
    public void setPseudoKanbanDataType(String pseudoKanbanDataType) {
        this.pseudoKanbanDataType = pseudoKanbanDataType;
    }
}