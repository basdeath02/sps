/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Arnon               Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <p>Purchase Order Result Report Domain.</p>
 *
 * @author CSI
 */
public class PurchaseOrderReportDomain extends BaseDomain implements Serializable {
    /** Serializable ID */
    private static final long serialVersionUID = 1428629358398724649L;
    /** Page Number. */
    private String pageNo;
    /** Group Header */
    /** Line 1. */
    /** P/O Issue Date. */
    private String poIssueDate;
    /** P/O Issue Date. */
    private Date issueDate;
    
    /** Line 2. */
    /** Supplier Code. */
    private String sCd;
    /** Supplier Name. */
    private String supplierName;
    /** Supplier Plant Code. */
    private String sPcd;
    /** Transport Mode. */
    private String transportMode;
    /** Issue Date From. */
    private String issueDateFrom;
    /** Issue Date From Date. */
    private Date issueDateFromDate;
    
    /** Line 3. */
    /** Release No. */
    private String releaseNo;
    /** SPS P/O No. */
    private String spsPoNo;
    /** CIGMA P/O No. */
    private String cigmaPoNo;
    /** Reference CIGMA P/O No. */
    private String refCigmaPoNo;
    /** P/O Type. */
    private String poType;
    /** Issue Date To. */
    private String issueDateTo;
    /** Issue Date To Date. */
    private Date issueDateToDate;
    
    /** Line 4. */
    /** Denso Part No. */
    private String dPn;
    /** Supplier Part No. */
    private String sPn;
    /** Item Description. */
    private String itemDescription;
    /** Planner Code. */
    private String plannerCode;
    
    /** Line 5. */
    /** Receiving Dock. */
    private String receivingDock;
    /** Qty Box. */
    private String qtyBox;
    /** Unit of Measure. */
    private String unitOfMeasure;
    /** Variable Qty Code. */
    private String variableQtyCode;
    /** Phase In/Out Code. */
    private String phaseInOutCode;
    
    /** Line 6. */
    /** U/P. */
    private String up;
    /** Unit Price. */
    private BigDecimal unitPrice;
    /** Currency Code. */
    private String currencyCode;
    /** Order Lot. */
    private String orderLot;
    /** Order Method. */
    private String orderMethod;
    
    /** Group Detail */
    /** Region */
    private String region;
    /** Date1 */
    private String date;
    /** Date Date1 */
    private Date dateDate;
    /** Date etd */
    private Date etd;
    /** Code1 */
    private String cd;
    /** Report Type1 */
    private String dwm;
    /** Qty1 */
    private String qty;
    /** Date2 */
    private String date2;
    /** Date Date2 */
    private Date dateDate2;
    /** Code2 */
    private String cd2;
    /** Report Type2 */
    private String dwm2;
    /** Qty2 */
    private String qty2;
    /** MON */
    private String mon;
    /** MON Date */
    private String monDate;
    /** TUE */
    private String tue;
    /** WED */
    private String wed;
    /** WED Date */
    private String wedDate;
    /** THU */
    private String thu;
    /** THU Date */
    private String thuDate;
    /** FRI */
    private String fri;
    /** SAT */
    private String sat;
    /** SAT Date */
    private String satDate;
    /** SUN */
    private String sun;
    /** SUN Date */
    private String sunDate;
    /** Sum Tuesday Qty */
    private String tueQty;
    /** Sum Tuesday Date */
    private String tueDate;
    /** Sum Tuesday Date Date */
    private Date tueDateDate;
    /** Sum Friday Qty  */
    private String friQty;
    /** Sum Friday Date */
    private String friDate;
    /** Sum Friday Date Date */
    private Date friDateDate;
    /** Month */
    private String month;
    /** Month Date*/
    private Date monthDate;
    /** Report Type3 */
    private String monthdwm;
    /** Qty3 */
    private String monthQty;
    /** Report Flag */
    private String reportFlag;
    /** Order Method Db */
    private String orderMethodDb;
    /** Period Type */
    private String periodType;

    /** Start Period Date of P/O */
    private Date startPeriodDate;

    /** End Period Date of P/O */
    private Date endPeriodDate;
    
    /** End Period Type Firm Date */
    private Date endFirmDate;

    /** Company Denso Code */
    private String dCd;
    /** Company Denso Plant Code */
    private String dPcd;
    /** Company Denso Name */
    private String companyDensoName;
    
    /** List of Purchase order Due */
    private List<PurchaseOrderReportRegionDomain> poReportRegionList;
    
    /**
     * 
     * <p>Constructor Purchase Order Report Domain.</p>
     *
     */
    public PurchaseOrderReportDomain() {
        this.poReportRegionList = new ArrayList<PurchaseOrderReportRegionDomain>();
    }
    /**
     * <p>Getter method for pageNo.</p>
     *
     * @return the pageNo
     */
    public String getPageNo() {
        return pageNo;
    }
    /**
     * <p>Setter method for pageNo.</p>
     *
     * @param pageNo Set for pageNo
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }
    /**
     * <p>Getter method for poIssueDate.</p>
     *
     * @return the poIssueDate
     */
    public String getPoIssueDate() {
        return poIssueDate;
    }
    /**
     * <p>Setter method for poIssueDate.</p>
     *
     * @param poIssueDate Set for poIssueDate
     */
    public void setPoIssueDate(String poIssueDate) {
        this.poIssueDate = poIssueDate;
    }
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * <p>Setter method for supplierCode.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }
    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    /**
     * <p>Getter method for supplierPlantCode.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * <p>Getter method for transportMode.</p>
     *
     * @return the transportMode
     */
    public String getTransportMode() {
        return transportMode;
    }
    /**
     * <p>Setter method for transportMode.</p>
     *
     * @param transportMode Set for transportMode
     */
    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }
    /**
     * <p>Getter method for issueDateFrom.</p>
     *
     * @return the issueDateFrom
     */
    public String getIssueDateFrom() {
        return issueDateFrom;
    }
    /**
     * <p>Setter method for issueDateFrom.</p>
     *
     * @param issueDateFrom Set for issueDateFrom
     */
    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }
    /**
     * <p>Getter method for releaseNo.</p>
     *
     * @return the releaseNo
     */
    public String getReleaseNo() {
        return releaseNo;
    }
    /**
     * <p>Setter method for releaseNo.</p>
     *
     * @param releaseNo Set for releaseNo
     */
    public void setReleaseNo(String releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * <p>Getter method for spsPoNo.</p>
     *
     * @return the spsPoNo
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }
    /**
     * <p>Setter method for spsPoNo.</p>
     *
     * @param spsPoNo Set for spsPoNo
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }
    /**
     * <p>Getter method for cigmaPoNo.</p>
     *
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * <p>Setter method for cigmaPoNo.</p>
     *
     * @param cigmaPoNo Set for cigmaPoNo
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }
    /**
     * <p>Getter method for issueDateTo.</p>
     *
     * @return the issueDateTo
     */
    public String getIssueDateTo() {
        return issueDateTo;
    }
    /**
     * <p>Setter method for issueDateTo.</p>
     *
     * @param issueDateTo Set for issueDateTo
     */
    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }
    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }
    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }
    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    /**
     * <p>Getter method for itemDescription.</p>
     *
     * @return the itemDescription
     */
    public String getItemDescription() {
        return itemDescription;
    }
    /**
     * <p>Setter method for itemDescription.</p>
     *
     * @param itemDescription Set for itemDescription
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }
    /**
     * <p>Getter method for plannerCode.</p>
     *
     * @return the plannerCode
     */
    public String getPlannerCode() {
        return plannerCode;
    }
    /**
     * <p>Setter method for plannerCode.</p>
     *
     * @param plannerCode Set for plannerCode
     */
    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }
    /**
     * <p>Getter method for receivingDock.</p>
     *
     * @return the receivingDock
     */
    public String getReceivingDock() {
        return receivingDock;
    }
    /**
     * <p>Setter method for receivingDock.</p>
     *
     * @param receivingDock Set for receivingDock
     */
    public void setReceivingDock(String receivingDock) {
        this.receivingDock = receivingDock;
    }
    /**
     * <p>Getter method for qtyBox.</p>
     *
     * @return the qtyBox
     */
    public String getQtyBox() {
        return qtyBox;
    }
    /**
     * <p>Setter method for qtyBox.</p>
     *
     * @param qtyBox Set for qtyBox
     */
    public void setQtyBox(String qtyBox) {
        this.qtyBox = qtyBox;
    }
    /**
     * <p>Getter method for unitOfMeasure.</p>
     *
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }
    /**
     * <p>Setter method for unitOfMeasure.</p>
     *
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }
    /**
     * <p>Getter method for variableQtyCode.</p>
     *
     * @return the variableQtyCode
     */
    public String getVariableQtyCode() {
        return variableQtyCode;
    }
    /**
     * <p>Setter method for variableQtyCode.</p>
     *
     * @param variableQtyCode Set for variableQtyCode
     */
    public void setVariableQtyCode(String variableQtyCode) {
        this.variableQtyCode = variableQtyCode;
    }
    /**
     * <p>Getter method for phaseInOutCode.</p>
     *
     * @return the phaseInOutCode
     */
    public String getPhaseInOutCode() {
        return phaseInOutCode;
    }
    /**
     * <p>Setter method for phaseInOutCode.</p>
     *
     * @param phaseInOutCode Set for phaseInOutCode
     */
    public void setPhaseInOutCode(String phaseInOutCode) {
        this.phaseInOutCode = phaseInOutCode;
    }
    /**
     * <p>Getter method for up.</p>
     *
     * @return the up
     */
    public String getUp() {
        return up;
    }
    /**
     * <p>Setter method for up.</p>
     *
     * @param up Set for up
     */
    public void setUp(String up) {
        this.up = up;
    }
    /**
     * <p>Getter method for currencyCode.</p>
     *
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }
    /**
     * <p>Setter method for currencyCode.</p>
     *
     * @param currencyCode Set for currencyCode
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
    /**
     * <p>Getter method for orderLot.</p>
     *
     * @return the orderLot
     */
    public String getOrderLot() {
        return orderLot;
    }
    /**
     * <p>Setter method for orderLot.</p>
     *
     * @param orderLot Set for orderLot
     */
    public void setOrderLot(String orderLot) {
        this.orderLot = orderLot;
    }
    /**
     * <p>Getter method for orderMethod.</p>
     *
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }
    /**
     * <p>Setter method for orderMethod.</p>
     *
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }
    /**
     * <p>Getter method for region.</p>
     *
     * @return the region
     */
    public String getRegion() {
        return region;
    }
    /**
     * <p>Setter method for region.</p>
     *
     * @param region Set for region
     */
    public void setRegion(String region) {
        this.region = region;
    }
    /**
     * <p>Getter method for date.</p>
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }
    /**
     * <p>Setter method for date.</p>
     *
     * @param date Set for date
     */
    public void setDate(String date) {
        this.date = date;
    }
    /**
     * <p>Getter method for cd.</p>
     *
     * @return the cd
     */
    public String getCd() {
        return cd;
    }
    /**
     * <p>Setter method for cd.</p>
     *
     * @param cd Set for cd
     */
    public void setCd(String cd) {
        this.cd = cd;
    }
    /**
     * <p>Getter method for dwm.</p>
     *
     * @return the dwm
     */
    public String getDwm() {
        return dwm;
    }
    /**
     * <p>Setter method for dwm.</p>
     *
     * @param dwm Set for dwm
     */
    public void setDwm(String dwm) {
        this.dwm = dwm;
    }
    /**
     * <p>Getter method for qty.</p>
     *
     * @return the qty
     */
    public String getQty() {
        return qty;
    }
    /**
     * <p>Setter method for qty.</p>
     *
     * @param qty Set for qty
     */
    public void setQty(String qty) {
        this.qty = qty;
    }
    /**
     * <p>Getter method for date2.</p>
     *
     * @return the date2
     */
    public String getDate2() {
        return date2;
    }
    /**
     * <p>Setter method for date2.</p>
     *
     * @param date2 Set for date2
     */
    public void setDate2(String date2) {
        this.date2 = date2;
    }
    /**
     * <p>Getter method for cd2.</p>
     *
     * @return the cd2
     */
    public String getCd2() {
        return cd2;
    }
    /**
     * <p>Setter method for cd2.</p>
     *
     * @param cd2 Set for cd2
     */
    public void setCd2(String cd2) {
        this.cd2 = cd2;
    }
    /**
     * <p>Getter method for dwm2.</p>
     *
     * @return the dwm2
     */
    public String getDwm2() {
        return dwm2;
    }
    /**
     * <p>Setter method for dwm2.</p>
     *
     * @param dwm2 Set for dwm2
     */
    public void setDwm2(String dwm2) {
        this.dwm2 = dwm2;
    }
    /**
     * <p>Getter method for qty2.</p>
     *
     * @return the qty2
     */
    public String getQty2() {
        return qty2;
    }
    /**
     * <p>Setter method for qty2.</p>
     *
     * @param qty2 Set for qty2
     */
    public void setQty2(String qty2) {
        this.qty2 = qty2;
    }
    /**
     * <p>Getter method for mon.</p>
     *
     * @return the mon
     */
    public String getMon() {
        return mon;
    }
    /**
     * <p>Setter method for mon.</p>
     *
     * @param mon Set for mon
     */
    public void setMon(String mon) {
        this.mon = mon;
    }
    /**
     * <p>Getter method for tue.</p>
     *
     * @return the tue
     */
    public String getTue() {
        return tue;
    }
    /**
     * <p>Setter method for tue.</p>
     *
     * @param tue Set for tue
     */
    public void setTue(String tue) {
        this.tue = tue;
    }
    /**
     * <p>Getter method for wed.</p>
     *
     * @return the wed
     */
    public String getWed() {
        return wed;
    }
    /**
     * <p>Setter method for wed.</p>
     *
     * @param wed Set for wed
     */
    public void setWed(String wed) {
        this.wed = wed;
    }
    /**
     * <p>Getter method for thu.</p>
     *
     * @return the thu
     */
    public String getThu() {
        return thu;
    }
    /**
     * <p>Setter method for thu.</p>
     *
     * @param thu Set for thu
     */
    public void setThu(String thu) {
        this.thu = thu;
    }
    /**
     * <p>Getter method for fri.</p>
     *
     * @return the fri
     */
    public String getFri() {
        return fri;
    }
    /**
     * <p>Setter method for fri.</p>
     *
     * @param fri Set for fri
     */
    public void setFri(String fri) {
        this.fri = fri;
    }
    /**
     * <p>Getter method for sat.</p>
     *
     * @return the sat
     */
    public String getSat() {
        return sat;
    }
    /**
     * <p>Setter method for sat.</p>
     *
     * @param sat Set for sat
     */
    public void setSat(String sat) {
        this.sat = sat;
    }
    /**
     * <p>Getter method for sun.</p>
     *
     * @return the sun
     */
    public String getSun() {
        return sun;
    }
    /**
     * <p>Setter method for sun.</p>
     *
     * @param sun Set for sun
     */
    public void setSun(String sun) {
        this.sun = sun;
    }
    /**
     * <p>Getter method for tueQty.</p>
     *
     * @return the tueQty
     */
    public String getTueQty() {
        return tueQty;
    }
    /**
     * <p>Setter method for tueQty.</p>
     *
     * @param tueQty Set for tueQty
     */
    public void setTueQty(String tueQty) {
        this.tueQty = tueQty;
    }
    /**
     * <p>Getter method for tueDate.</p>
     *
     * @return the tueDate
     */
    public String getTueDate() {
        return tueDate;
    }
    /**
     * <p>Setter method for tueDate.</p>
     *
     * @param tueDate Set for tueDate
     */
    public void setTueDate(String tueDate) {
        this.tueDate = tueDate;
    }
    /**
     * <p>Getter method for friQty.</p>
     *
     * @return the friQty
     */
    public String getFriQty() {
        return friQty;
    }
    /**
     * <p>Setter method for friQty.</p>
     *
     * @param friQty Set for friQty
     */
    public void setFriQty(String friQty) {
        this.friQty = friQty;
    }
    /**
     * <p>Getter method for friDate.</p>
     *
     * @return the friDate
     */
    public String getFriDate() {
        return friDate;
    }
    /**
     * <p>Setter method for friDate.</p>
     *
     * @param friDate Set for friDate
     */
    public void setFriDate(String friDate) {
        this.friDate = friDate;
    }
    /**
     * <p>Getter method for month.</p>
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }
    /**
     * <p>Setter method for month.</p>
     *
     * @param month Set for month
     */
    public void setMonth(String month) {
        this.month = month;
    }
    /**
     * <p>Getter method for monthdwm.</p>
     *
     * @return the monthdwm
     */
    public String getMonthdwm() {
        return monthdwm;
    }
    /**
     * <p>Setter method for monthdwm.</p>
     *
     * @param monthdwm Set for monthdwm
     */
    public void setMonthdwm(String monthdwm) {
        this.monthdwm = monthdwm;
    }
    /**
     * <p>Getter method for monthQty.</p>
     *
     * @return the monthQty
     */
    public String getMonthQty() {
        return monthQty;
    }
    /**
     * <p>Setter method for monthQty.</p>
     *
     * @param monthQty Set for monthQty
     */
    public void setMonthQty(String monthQty) {
        this.monthQty = monthQty;
    }
    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }
    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
    /**
     * <p>Getter method for issueDateFromDate.</p>
     *
     * @return the issueDateFromDate
     */
    public Date getIssueDateFromDate() {
        return issueDateFromDate;
    }
    /**
     * <p>Setter method for issueDateFromDate.</p>
     *
     * @param issueDateFromDate Set for issueDateFromDate
     */
    public void setIssueDateFromDate(Date issueDateFromDate) {
        this.issueDateFromDate = issueDateFromDate;
    }
    /**
     * <p>Getter method for issueDateToDate.</p>
     *
     * @return the issueDateToDate
     */
    public Date getIssueDateToDate() {
        return issueDateToDate;
    }
    /**
     * <p>Setter method for issueDateToDate.</p>
     *
     * @param issueDateToDate Set for issueDateToDate
     */
    public void setIssueDateToDate(Date issueDateToDate) {
        this.issueDateToDate = issueDateToDate;
    }
    /**
     * <p>Getter method for dateDate.</p>
     *
     * @return the dateDate
     */
    public Date getDateDate() {
        return dateDate;
    }
    /**
     * <p>Setter method for dateDate.</p>
     *
     * @param dateDate Set for dateDate
     */
    public void setDateDate(Date dateDate) {
        this.dateDate = dateDate;
    }
    /**
     * <p>Getter method for dateDate2.</p>
     *
     * @return the dateDate2
     */
    public Date getDateDate2() {
        return dateDate2;
    }
    /**
     * <p>Setter method for dateDate2.</p>
     *
     * @param dateDate2 Set for dateDate2
     */
    public void setDateDate2(Date dateDate2) {
        this.dateDate2 = dateDate2;
    }
    /**
     * <p>Getter method for tueDateDate.</p>
     *
     * @return the tueDateDate
     */
    public Date getTueDateDate() {
        return tueDateDate;
    }
    /**
     * <p>Setter method for tueDateDate.</p>
     *
     * @param tueDateDate Set for tueDateDate
     */
    public void setTueDateDate(Date tueDateDate) {
        this.tueDateDate = tueDateDate;
    }
    /**
     * <p>Getter method for friDateDate.</p>
     *
     * @return the friDateDate
     */
    public Date getFriDateDate() {
        return friDateDate;
    }
    /**
     * <p>Setter method for friDateDate.</p>
     *
     * @param friDateDate Set for friDateDate
     */
    public void setFriDateDate(Date friDateDate) {
        this.friDateDate = friDateDate;
    }
    /**
     * <p>Getter method for monthDate.</p>
     *
     * @return the monthDate
     */
    public Date getMonthDate() {
        return monthDate;
    }
    /**
     * <p>Setter method for monthDate.</p>
     *
     * @param monthDate Set for monthDate
     */
    public void setMonthDate(Date monthDate) {
        this.monthDate = monthDate;
    }
    /**
     * <p>Getter method for reportFlag.</p>
     *
     * @return the reportFlag
     */
    public String getReportFlag() {
        return reportFlag;
    }
    /**
     * <p>Setter method for reportFlag.</p>
     *
     * @param reportFlag Set for reportFlag
     */
    public void setReportFlag(String reportFlag) {
        this.reportFlag = reportFlag;
    }
    /**
     * <p>Getter method for orderMethodDb.</p>
     *
     * @return the orderMethodDb
     */
    public String getOrderMethodDb() {
        return orderMethodDb;
    }
    /**
     * <p>Setter method for orderMethodDb.</p>
     *
     * @param orderMethodDb Set for orderMethodDb
     */
    public void setOrderMethodDb(String orderMethodDb) {
        this.orderMethodDb = orderMethodDb;
    }
    /**
     * <p>Getter method for periodType.</p>
     *
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }
    /**
     * <p>Setter method for periodType.</p>
     *
     * @param periodType Set for periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }
    /**
     * <p>Getter method for unitPrice.</p>
     *
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }
    /**
     * <p>Setter method for unitPrice.</p>
     *
     * @param unitPrice Set for unitPrice
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }
    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * <p>Getter method for companyDensoName.</p>
     *
     * @return the companyDensoName
     */
    public String getCompanyDensoName() {
        return companyDensoName;
    }
    /**
     * <p>Setter method for companyDensoName.</p>
     *
     * @param companyDensoName Set for companyDensoName
     */
    public void setCompanyDensoName(String companyDensoName) {
        this.companyDensoName = companyDensoName;
    }
    /**
     * <p>Getter method for monDate.</p>
     *
     * @return the monDate
     */
    public String getMonDate() {
        return monDate;
    }
    /**
     * <p>Setter method for monDate.</p>
     *
     * @param monDate Set for monDate
     */
    public void setMonDate(String monDate) {
        this.monDate = monDate;
    }
    /**
     * <p>Getter method for wedDate.</p>
     *
     * @return the wedDate
     */
    public String getWedDate() {
        return wedDate;
    }
    /**
     * <p>Setter method for wedDate.</p>
     *
     * @param wedDate Set for wedDate
     */
    public void setWedDate(String wedDate) {
        this.wedDate = wedDate;
    }
    /**
     * <p>Getter method for thuDate.</p>
     *
     * @return the thuDate
     */
    public String getThuDate() {
        return thuDate;
    }
    /**
     * <p>Setter method for thuDate.</p>
     *
     * @param thuDate Set for thuDate
     */
    public void setThuDate(String thuDate) {
        this.thuDate = thuDate;
    }
    /**
     * <p>Getter method for satDate.</p>
     *
     * @return the satDate
     */
    public String getSatDate() {
        return satDate;
    }
    /**
     * <p>Setter method for satDate.</p>
     *
     * @param satDate Set for satDate
     */
    public void setSatDate(String satDate) {
        this.satDate = satDate;
    }
    /**
     * <p>Getter method for sunDate.</p>
     *
     * @return the sunDate
     */
    public String getSunDate() {
        return sunDate;
    }
    /**
     * <p>Setter method for sunDate.</p>
     *
     * @param sunDate Set for sunDate
     */
    public void setSunDate(String sunDate) {
        this.sunDate = sunDate;
    }
    /**
     * <p>Getter method for etd.</p>
     *
     * @return the etd
     */
    public Date getEtd() {
        return etd;
    }
    /**
     * <p>Setter method for etd.</p>
     *
     * @param etd Set for etd
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }
    /**
     * <p>Getter method for poReportRegionList.</p>
     *
     * @return the poReportRegionList
     */
    public List<PurchaseOrderReportRegionDomain> getPoReportRegionList() {
        return poReportRegionList;
    }
    /**
     * <p>Setter method for poReportRegionList.</p>
     *
     * @param poReportRegionList Set for poReportRegionList
     */
    public void setPoReportRegionList(
        List<PurchaseOrderReportRegionDomain> poReportRegionList) {
        this.poReportRegionList = poReportRegionList;
    }
    /**
     * <p>Getter method for startPeriodDate.</p>
     *
     * @return the startPeriodDate
     */
    public Date getStartPeriodDate() {
        return startPeriodDate;
    }
    /**
     * <p>Setter method for startPeriodDate.</p>
     *
     * @param startPeriodDate Set for startPeriodDate
     */
    public void setStartPeriodDate(Date startPeriodDate) {
        this.startPeriodDate = startPeriodDate;
    }
    /**
     * <p>Getter method for endPeriodDate.</p>
     *
     * @return the endPeriodDate
     */
    public Date getEndPeriodDate() {
        return endPeriodDate;
    }
    /**
     * <p>Setter method for endPeriodDate.</p>
     *
     * @param endPeriodDate Set for endPeriodDate
     */
    public void setEndPeriodDate(Date endPeriodDate) {
        this.endPeriodDate = endPeriodDate;
    }
    /**
     * <p>Getter method for endFirmDate.</p>
     *
     * @return the endFirmDate
     */
    public Date getEndFirmDate() {
        return endFirmDate;
    }
    /**
     * <p>Setter method for endFirmDate.</p>
     *
     * @param endFirmDate Set for endFirmDate
     */
    public void setEndFirmDate(Date endFirmDate) {
        this.endFirmDate = endFirmDate;
    }
    /**
     * <p>Getter method for refCigmaPoNo.</p>
     *
     * @return the refCigmaPoNo
     */
    public String getRefCigmaPoNo() {
        return refCigmaPoNo;
    }
    /**
     * <p>Setter method for refCigmaPoNo.</p>
     *
     * @param refCigmaPoNo Set for refCigmaPoNo
     */
    public void setRefCigmaPoNo(String refCigmaPoNo) {
        this.refCigmaPoNo = refCigmaPoNo;
    }
    /**
     * <p>Getter method for poType.</p>
     *
     * @return the poType
     */
    public String getPoType() {
        return poType;
    }
    /**
     * <p>Setter method for poType.</p>
     *
     * @param poType Set for poType
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }

}
