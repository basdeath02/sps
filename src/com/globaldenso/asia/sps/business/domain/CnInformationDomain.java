/*
 * ModifyDate Development company     Describe 
 * 2014/08/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class CnInformationDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;
    
    /** The invoice id. */
    private BigDecimal invoiceId;
    
    /** The cn no. */
    private String cnNo;
    
    /** The cn date. */
    private Timestamp cnDate;
    
    /** The asn no. */
    private String asnNo;
    
    /** The asn date. */
    private Timestamp asnDate;
    
    /** The base amount. */
    private BigDecimal baseAmount;
    
    /** The vat amount. */
    private BigDecimal vatAmount;
    
    /** The total amount. */
    private BigDecimal totalAmount;
    
    /** The asn amount. */
    private BigDecimal asnAmount;
    
    /** The domain for SPS_T_CN (for transfer Invoice). */
    private SpsTCnDomain spsTCnDomain;
    
    /** The domain for SPS_T_INVOICE (for transfer Invoice). */
    private SpsTInvoiceDomain spsTInvoiceDomain;
    
    /** The domain for SPS_T_AS400_VENDOR (for transfer Invoice). */
    private SpsMAs400VendorDomain spsMAs400VendorDomain;
    
    /** The list of CN Detail Information. */
    private List<CnDetailInformationDomain> cnDetailInformationList;
    
    /**
     * Instantiates a new file upload domain.
     */
    public CnInformationDomain() {
        super();
    }

    /**
     * <p>Getter method for invoiceId.</p>
     *
     * @return the invoiceId
     */
    public BigDecimal getInvoiceId() {
        return invoiceId;
    }

    /**
     * <p>Setter method for invoiceId.</p>
     *
     * @param invoiceId Set for invoiceId
     */
    public void setInvoiceId(BigDecimal invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * <p>Getter method for cnNo.</p>
     *
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * <p>Setter method for cnNo.</p>
     *
     * @param cnNo Set for cnNo
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * <p>Getter method for cnDate.</p>
     *
     * @return the cnDate
     */
    public Timestamp getCnDate() {
        return cnDate;
    }

    /**
     * <p>Setter method for cnDate.</p>
     *
     * @param cnDate Set for cnDate
     */
    public void setCnDate(Timestamp cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * <p>Getter method for asnDate.</p>
     *
     * @return the asnDate
     */
    public Timestamp getAsnDate() {
        return asnDate;
    }

    /**
     * <p>Setter method for asnDate.</p>
     *
     * @param asnDate Set for asnDate
     */
    public void setAsnDate(Timestamp asnDate) {
        this.asnDate = asnDate;
    }

    /**
     * <p>Getter method for asnAmount.</p>
     *
     * @return the asnAmount
     */
    public BigDecimal getAsnAmount() {
        return asnAmount;
    }

    /**
     * <p>Setter method for asnAmount.</p>
     *
     * @param asnAmount Set for asnAmount
     */
    public void setAsnAmount(BigDecimal asnAmount) {
        this.asnAmount = asnAmount;
    }

    /**
     * <p>Getter method for baseAmount.</p>
     *
     * @return the baseAmount
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /**
     * <p>Setter method for baseAmount.</p>
     *
     * @param baseAmount Set for baseAmount
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /**
     * <p>Getter method for vatAmount.</p>
     *
     * @return the vatAmount
     */
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    /**
     * <p>Setter method for vatAmount.</p>
     *
     * @param vatAmount Set for vatAmount
     */
    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    /**
     * <p>Getter method for totalAmount.</p>
     *
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * <p>Setter method for totalAmount.</p>
     *
     * @param totalAmount Set for totalAmount
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * <p>Getter method for spsTCnDomain.</p>
     *
     * @return the spsTCnDomain
     */
    public SpsTCnDomain getSpsTCnDomain() {
        return spsTCnDomain;
    }

    /**
     * <p>Setter method for spsTCnDomain.</p>
     *
     * @param spsTCnDomain Set for spsTCnDomain
     */
    public void setSpsTCnDomain(SpsTCnDomain spsTCnDomain) {
        this.spsTCnDomain = spsTCnDomain;
    }

    /**
     * <p>Getter method for spsTInvoiceDomain.</p>
     *
     * @return the spsTInvoiceDomain
     */
    public SpsTInvoiceDomain getSpsTInvoiceDomain() {
        return spsTInvoiceDomain;
    }

    /**
     * <p>Setter method for spsTInvoiceDomain.</p>
     *
     * @param spsTInvoiceDomain Set for spsTInvoiceDomain
     */
    public void setSpsTInvoiceDomain(SpsTInvoiceDomain spsTInvoiceDomain) {
        this.spsTInvoiceDomain = spsTInvoiceDomain;
    }

    /**
     * <p>Getter method for spsMAs400VendorDomain.</p>
     *
     * @return the spsMAs400VendorDomain
     */
    public SpsMAs400VendorDomain getSpsMAs400VendorDomain() {
        return spsMAs400VendorDomain;
    }

    /**
     * <p>Setter method for spsMAs400VendorDomain.</p>
     *
     * @param spsMAs400VendorDomain Set for spsMAs400VendorDomain
     */
    public void setSpsMAs400VendorDomain(SpsMAs400VendorDomain spsMAs400VendorDomain) {
        this.spsMAs400VendorDomain = spsMAs400VendorDomain;
    }

    /**
     * <p>Getter method for cnDetailInformationList.</p>
     *
     * @return the cnDetailInformationList
     */
    public List<CnDetailInformationDomain> getCnDetailInformationList() {
        return cnDetailInformationList;
    }

    /**
     * <p>Setter method for cnDetailInformationList.</p>
     *
     * @param cnDetailInformationList Set for cnDetailInformationList
     */
    public void setCnDetailInformationList(
        List<CnDetailInformationDomain> cnDetailInformationList) {
        this.cnDetailInformationList = cnDetailInformationList;
    }
    
}