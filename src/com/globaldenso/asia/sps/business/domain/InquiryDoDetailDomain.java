/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/29 Parichat           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class InquiryDODetailDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class InquiryDoDetailDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4102365153408186856L;

    /** The do id. */
    private String doId;
    
    /** The pdf file id. */
    private String pdfFileId;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The inquiry do detail return list. */
    private List<InquiryDoDetailReturnDomain> inquiryDoDetailReturnList;
    
    /**
     * Instantiates a new inquiry do detail domain.
     */
    public InquiryDoDetailDomain() {
        super();
    }

    /**
     * Gets the do id.
     * 
     * @return the do id.
     */
    public String getDoId() {
        return doId;
    }

    /**
     * Sets the do id.
     * 
     * @param doId the do id.
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }
    
    /**
     * Gets the pdf file id.
     * 
     * @return the pdf file id.
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Sets the pdf file id.
     * 
     * @param pdfFileId the pdf file id.
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Gets the supplierAuthenList.
     * 
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * Sets the supplierAuthenList.
     * 
     * @param supplierAuthenList the supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * Gets the densoAuthenList.
     * 
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * Sets the densoAuthenList.
     * 
     * @param densoAuthenList the densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
    /**
     * Gets the inquiry do detail return list.
     * 
     * @return the inquiry do detail return list.
     */
    public List<InquiryDoDetailReturnDomain> getInquiryDoDetailReturnList() {
        return inquiryDoDetailReturnList;
    }

    /**
     * Sets the inquiry do detail return list.
     * 
     * @param inquiryDoDetailReturnList the inquiry do detail return list.
     */
    public void setInquiryDoDetailReturnList(
        List<InquiryDoDetailReturnDomain> inquiryDoDetailReturnList) {
        this.inquiryDoDetailReturnList = inquiryDoDetailReturnList;
    }

}