/*
 * ModifyDate Development company Describe 
 * 2014/07/21 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DensoSupplierRelationDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class DataScopeControlDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 4361542226149086134L;
    
    /** The type of user. */
    private String userType;
    
    /** List of User Role. */
    private List<UserRoleDomain> userRoleDomainList;
    
    /** List of relation between DENSO and Supplier. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationDomainList;
    
    
    /** The default constructor. */
    public DataScopeControlDomain() {
        super();
        this.userRoleDomainList = new ArrayList<UserRoleDomain>();
        this.densoSupplierRelationDomainList = new ArrayList<DensoSupplierRelationDomain>();
    }

    /**
     * Get method for userType.
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Set method for userType.
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * Get method for list of user role domain.
     * @return the list of user role domain.
     */
    public List<UserRoleDomain> getUserRoleDomainList() {
        return userRoleDomainList;
    }

    /**
     * Set method for list of user role domain.
     * @param userRoleDomainList the list of user role domain.
     */
    public void setUserRoleDomainList(List<UserRoleDomain> userRoleDomainList) {
        this.userRoleDomainList = userRoleDomainList;
    }

    /**
     * Get method for densoSupplierRelationDomainList.
     * @return the densoSupplierRelationDomainList
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationDomainList() {
        return densoSupplierRelationDomainList;
    }

    /**
     * Set method for densoSupplierRelationDomainList.
     * @param densoSupplierRelationDomainList the densoSupplierRelationDomainList to set
     */
    public void setDensoSupplierRelationDomainList(
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList)
    {
        this.densoSupplierRelationDomainList = densoSupplierRelationDomainList;
    }

}
