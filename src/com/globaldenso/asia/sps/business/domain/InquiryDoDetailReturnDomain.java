/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/29 Parichat           Create   
 * 2015/07/15 CSI Akat           [IN004]
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * The Class InquiryDODetailReturnDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class InquiryDoDetailReturnDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3160896421256903964L;
    
    /** The denso part no. */
    private String dPn;
    
    /** The supplier part no. */
    private String sPn;
    
    /** The received qty. */
    private BigDecimal receivedQty;
    
    /** The in-transit qty. */
    private BigDecimal inTransitQty;
    
    /** The shipping qty str. */
    private String shippingQtyStr;
    
    /** The received qty str. */
    private String receivingQtyStr;
    
    /** The in-transit qty str. */
    private String inTransitQtyStr;
    
    // [IN004] add field
    /** The order quantity in string format. */
    private String orderQtyStr;
    
    /** The chg cigma do no. */
    private String chgCigmaDoNo;
    
    // [IN004] add field
    /** The Rank of Parts for group in screen. */
    private String partsRank;
    
    /** The do domain. */
    private SpsTDoDomain doDomain;
    
    // [IN004] add field
    /** The D/O Detail Domain. */
    private SpsTDoDetailDomain doDetailDomain;
    
    /** The asn domain. */
    private SpsTAsnDomain asnDomain;
    
    /** The asn detail domain. */
    private SpsTAsnDetailDomain asnDetailDomain;
    
    /**
     * Instantiates a new inquiry do detail return domain.
     */
    public InquiryDoDetailReturnDomain() {
        super();
        doDomain = new SpsTDoDomain();
        asnDomain = new SpsTAsnDomain();
        asnDetailDomain = new SpsTAsnDetailDomain();
        // [IN004] initiate added field
        doDetailDomain = new SpsTDoDetailDomain();
    }
    
    /**
     * Gets the denso part no.
     * 
     * @return the denso part no
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Sets the denso part no.
     * 
     * @param dPn the denso part no
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Gets the supplier part no.
     * 
     * @return the supplier part no
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Sets the supplier part no.
     * 
     * @param sPn the supplier part no
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Gets the received qty.
     * 
     * @return the received qty
     */
    public BigDecimal getReceivedQty() {
        return receivedQty;
    }

    /**
     * Sets the received qty.
     * 
     * @param receivedQty the received qty
     */
    public void setReceivedQty(BigDecimal receivedQty) {
        this.receivedQty = receivedQty;
    }
    
    /**
     * Gets the in-transit qty.
     * 
     * @return the in-transit qty
     */
    public BigDecimal getInTransitQty() {
        return inTransitQty;
    }

    /**
     * Sets the in-transit qty.
     * 
     * @param inTransitQty the in-transit qty
     */
    public void setInTransitQty(BigDecimal inTransitQty) {
        this.inTransitQty = inTransitQty;
    }
    
    /**
     * Gets the shipping qty str.
     * 
     * @return the shipping qty str
     */
    public String getShippingQtyStr() {
        return shippingQtyStr;
    }

    /**
     * Sets the shipping qty str.
     * 
     * @param shippingQtyStr the shipping qty str
     */
    public void setShippingQtyStr(String shippingQtyStr) {
        this.shippingQtyStr = shippingQtyStr;
    }

    /**
     * Gets the receiving qty str.
     * 
     * @return the receiving qty str
     */
    public String getReceivingQtyStr() {
        return receivingQtyStr;
    }

    /**
     * Sets the receiving qty str.
     * 
     * @param receivingQtyStr the receiving qty str
     */
    public void setReceivingQtyStr(String receivingQtyStr) {
        this.receivingQtyStr = receivingQtyStr;
    }

    /**
     * Gets the in-transit qty str.
     * 
     * @return the in-transit qty str
     */
    public String getInTransitQtyStr() {
        return inTransitQtyStr;
    }

    /**
     * Sets the in-transit qty str.
     * 
     * @param inTransitQtyStr the in-transit qty str
     */
    public void setInTransitQtyStr(String inTransitQtyStr) {
        this.inTransitQtyStr = inTransitQtyStr;
    }
    
    /**
     * Gets the chg cigma do no.
     * 
     * @return the chg cigma do no
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * Sets the chg cigma do no.
     * 
     * @param chgCigmaDoNo the chg cigma do no
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * Gets the do domain.
     * 
     * @return the do domain
     */
    public SpsTDoDomain getDoDomain() {
        return doDomain;
    }
    
    /**
     * Sets the do domain.
     * 
     * @param doDomain the do domain
     */
    public void setDoDomain(SpsTDoDomain doDomain) {
        this.doDomain = doDomain;
    }
    
    /**
     * Gets the asn domain.
     * 
     * @return the asn domain
     */
    public SpsTAsnDomain getAsnDomain() {
        return asnDomain;
    }

    /**
     * Sets the asn domain.
     * 
     * @param asnDomain the asn domain
     */
    public void setAsnDomain(SpsTAsnDomain asnDomain) {
        this.asnDomain = asnDomain;
    }

    /**
     * Gets the asn detail domain.
     * 
     * @return the asn detail domain
     */
    public SpsTAsnDetailDomain getAsnDetailDomain() {
        return asnDetailDomain;
    }

    /**
     * Sets the asn detail domain.
     * 
     * @param asnDetailDomain the asn detail domain
     */
    public void setAsnDetailDomain(SpsTAsnDetailDomain asnDetailDomain) {
        this.asnDetailDomain = asnDetailDomain;
    }

    /**
     * <p>Getter method for doDetailDomain.</p>
     *
     * @return the doDetailDomain
     */
    public SpsTDoDetailDomain getDoDetailDomain() {
        return doDetailDomain;
    }

    /**
     * <p>Setter method for doDetailDomain.</p>
     *
     * @param doDetailDomain Set for doDetailDomain
     */
    public void setDoDetailDomain(SpsTDoDetailDomain doDetailDomain) {
        this.doDetailDomain = doDetailDomain;
    }

    /**
     * <p>Getter method for partsRank.</p>
     *
     * @return the partsRank
     */
    public String getPartsRank() {
        return partsRank;
    }

    /**
     * <p>Setter method for partsRank.</p>
     *
     * @param partsRank Set for partsRank
     */
    public void setPartsRank(String partsRank) {
        this.partsRank = partsRank;
    }

    /**
     * <p>Getter method for orderQtyStr.</p>
     *
     * @return the orderQtyStr
     */
    public String getOrderQtyStr() {
        return orderQtyStr;
    }

    /**
     * <p>Setter method for orderQtyStr.</p>
     *
     * @param orderQtyStr Set for orderQtyStr
     */
    public void setOrderQtyStr(String orderQtyStr) {
        this.orderQtyStr = orderQtyStr;
    }
    
}