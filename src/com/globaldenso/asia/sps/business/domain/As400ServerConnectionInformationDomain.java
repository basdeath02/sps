/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Parichat            Create
 * 2015/12/16 CSI Akat                [IN037]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;

/**
 * The Class As400ServerConnectionInformation Domain.
 * @author CSI
 */
public class As400ServerConnectionInformationDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4025473795010041604L;

    /** The domain for SPS_M_COMPANY_DENSO. */
    private SpsMCompanyDensoDomain spsMCompanyDensoDomain;
    
    /** The domain for SPS_M_AS400_SCHEMA. */
    private SpsMAs400SchemaDomain spsMAs400SchemaDomain;
    
    // Start : [IN037] separate REST between Order and ASN/Invoice
    //private SpsMAs400LparDomain spsMAs400LparDomain;
    /** The LPAR Domain for Order side. */
    private SpsMAs400LparDomain orderLpar;

    /** The LPAR Domain for ASN/Invoice side. */
    private SpsMAs400LparDomain asnInvoiceLpar;
    // End : [IN037] separate REST between Order and ASN/Invoice
    /**
     * Instantiates a new file upload domain.
     */
    public As400ServerConnectionInformationDomain() {
        super();
    }

    /**
     * <p>Getter method for spsMCompanyDensoDomain.</p>
     *
     * @return the spsMCompanyDensoDomain
     */
    public SpsMCompanyDensoDomain getSpsMCompanyDensoDomain() {
        return spsMCompanyDensoDomain;
    }

    /**
     * <p>Setter method for spsMCompanyDenso domain.</p>
     *
     * @param spsMCompanyDensoDomain Set for spsMCompanyDenso domain
     */
    public void setSpsMCompanyDensoDomain(SpsMCompanyDensoDomain spsMCompanyDensoDomain) {
        this.spsMCompanyDensoDomain = spsMCompanyDensoDomain;
    }

    /**
     * <p>Getter method for spsMAs400Schema domain.</p>
     *
     * @return the spsMAs400Schema domain
     */
    public SpsMAs400SchemaDomain getSpsMAs400SchemaDomain() {
        return spsMAs400SchemaDomain;
    }

    /**
     * <p>Setter method for spsMAs400Schema domain.</p>
     *
     * @param spsMAs400SchemaDomain Set for spsMAs400Schema domain
     */
    public void setSpsMAs400SchemaDomain(SpsMAs400SchemaDomain spsMAs400SchemaDomain) {
        this.spsMAs400SchemaDomain = spsMAs400SchemaDomain;
    }

    /**
     * <p>Getter method for orderLpar.</p>
     *
     * @return the orderLpar
     */
    public SpsMAs400LparDomain getOrderLpar() {
        return orderLpar;
    }

    /**
     * <p>Setter method for orderLpar.</p>
     *
     * @param orderLpar Set for orderLpar
     */
    public void setOrderLpar(SpsMAs400LparDomain orderLpar) {
        this.orderLpar = orderLpar;
    }

    /**
     * <p>Getter method for asnInvoiceLpar.</p>
     *
     * @return the asnInvoiceLpar
     */
    public SpsMAs400LparDomain getAsnInvoiceLpar() {
        return asnInvoiceLpar;
    }

    /**
     * <p>Setter method for asnInvoiceLpar.</p>
     *
     * @param asnInvoiceLpar Set for asnInvoiceLpar
     */
    public void setAsnInvoiceLpar(SpsMAs400LparDomain asnInvoiceLpar) {
        this.asnInvoiceLpar = asnInvoiceLpar;
    }

}