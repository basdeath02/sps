/*
 * ModifyDate Development company     Describe 
 * 2015/03/30 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * <p>TransferPoDueForCalculateMonthDomain class.</p>
 *
 * @author CSI
 */
public class TransferPoDueForCalculateMonthDomain extends BaseDomain implements Serializable {
    
    /** The generated serial version UID. */
    private static final long serialVersionUID = -4560304096017701879L;

    /** P/O Due data report type M from CIGMA. */
    private TransferPoDueDataFromCigmaDomain monthDue;
    
    /** P/O Due data report type W last record. */
    private TransferPoDueDataFromCigmaDomain lastWeekDue;
    
    /** Month to calculate (YYYY/MM). */
    private String month;
    
    /** Actual quantity from CIGMA data report type M. */
    private BigDecimal summaryFromCigma;
    
    /** Quantity calculate from Transfered data. */
    private BigDecimal summaryFromTransfer;
    
    /** Quantity not in record transfer from CIGMA at start of month. */
    private BigDecimal preBuffer;

    /** Quantity not in record transfer from CIGMA at end of month. */
    private BigDecimal postBuffer;
    
    /** The default constructor */
    public TransferPoDueForCalculateMonthDomain() {
        super();
    }

    /**
     * <p>Getter method for month.</p>
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * <p>Setter method for month.</p>
     *
     * @param month Set for month
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * <p>Getter method for summaryFromCigma.</p>
     *
     * @return the summaryFromCigma
     */
    public BigDecimal getSummaryFromCigma() {
        return summaryFromCigma;
    }

    /**
     * <p>Setter method for summaryFromCigma.</p>
     *
     * @param summaryFromCigma Set for summaryFromCigma
     */
    public void setSummaryFromCigma(BigDecimal summaryFromCigma) {
        this.summaryFromCigma = summaryFromCigma;
    }

    /**
     * <p>Getter method for summaryFromTransfer.</p>
     *
     * @return the summaryFromTransfer
     */
    public BigDecimal getSummaryFromTransfer() {
        return summaryFromTransfer;
    }

    /**
     * <p>Setter method for summaryFromTransfer.</p>
     *
     * @param summaryFromTransfer Set for summaryFromTransfer
     */
    public void setSummaryFromTransfer(BigDecimal summaryFromTransfer) {
        this.summaryFromTransfer = summaryFromTransfer;
    }

    /**
     * <p>Getter method for preBuffer.</p>
     *
     * @return the preBuffer
     */
    public BigDecimal getPreBuffer() {
        return preBuffer;
    }

    /**
     * <p>Setter method for preBuffer.</p>
     *
     * @param preBuffer Set for preBuffer
     */
    public void setPreBuffer(BigDecimal preBuffer) {
        this.preBuffer = preBuffer;
    }

    /**
     * <p>Getter method for postBuffer.</p>
     *
     * @return the postBuffer
     */
    public BigDecimal getPostBuffer() {
        return postBuffer;
    }

    /**
     * <p>Setter method for postBuffer.</p>
     *
     * @param postBuffer Set for postBuffer
     */
    public void setPostBuffer(BigDecimal postBuffer) {
        this.postBuffer = postBuffer;
    }

    /**
     * <p>Getter method for monthDue.</p>
     *
     * @return the monthDue
     */
    public TransferPoDueDataFromCigmaDomain getMonthDue() {
        return monthDue;
    }

    /**
     * <p>Setter method for monthDue.</p>
     *
     * @param monthDue Set for monthDue
     */
    public void setMonthDue(TransferPoDueDataFromCigmaDomain monthDue) {
        this.monthDue = monthDue;
    }

    /**
     * <p>Getter method for lastWeekDue.</p>
     *
     * @return the lastWeekDue
     */
    public TransferPoDueDataFromCigmaDomain getLastWeekDue() {
        return lastWeekDue;
    }

    /**
     * <p>Setter method for lastWeekDue.</p>
     *
     * @param lastWeekDue Set for lastWeekDue
     */
    public void setLastWeekDue(TransferPoDueDataFromCigmaDomain lastWeekDue) {
        this.lastWeekDue = lastWeekDue;
    }

}
