/*
 * ModifyDate Development company     Describe 
 * 2014/08/22 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;

/**
 * The Class Invoice Cover Page Detail Domain.
 * @author CSI
 */
public class InvoiceCoverPageDetailDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;
    
    /** The sps transaction invoice detail domain. */
    private SpsTInvoiceDetailDomain spsTInvoiceDetailDomain;
    
    /** The sps asn domain. */
    private SpsTAsnDomain spsTAsnDomain;
    
    /**
     * Instantiates a new Invoice Cover Page Domain.
     */
    public InvoiceCoverPageDetailDomain() {
        super();
    }
    
    /**
     * <p>Getter method for spsTInvoiceDetailDomain.</p>
     *
     * @return the spsTInvoiceDetailDomain
     */
    public SpsTInvoiceDetailDomain getSpsTInvoiceDetailDomain() {
        return spsTInvoiceDetailDomain;
    }

    /**
     * <p>Setter method for spsTInvoiceDetailDomain.</p>
     *
     * @param spsTInvoiceDetailDomain Set for spsTInvoiceDetailDomain
     */
    public void setSpsTInvoiceDetailDomain(
        SpsTInvoiceDetailDomain spsTInvoiceDetailDomain) {
        this.spsTInvoiceDetailDomain = spsTInvoiceDetailDomain;
    }

    
    /**
     * Gets the sps asn domain.
     * 
     * @return the spsTAsnDomain.
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }
    
    /**
     * Sets the sps asn domain.
     * 
     * @param spsTAsnDomain the sps asn domain.
     */
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }
}