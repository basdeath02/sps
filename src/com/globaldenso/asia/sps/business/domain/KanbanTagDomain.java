/*
 * ModifyDate Development company    Describe 
 * 2017/08/17 Netband U.Rungsiwut    Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class Kanban Tag Information Domain.
 * @author Netband
 */
public class KanbanTagDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 303558568853491292L;
    
    /** The doId. */
    private BigDecimal doId;
    
    /** The doNo. */
    private String doNo;
    
    /** The Denso Part No. */
    private String dPn;
    
    /** The Kanban Tag Sequence. */
    private String kanbanTagSeq;
    
    /** The Kanban type. */
    private String kanbanType;
    
    /** The capacity. */
    private BigDecimal capacity;
    
    /** The qrContent. */
    private String qrContent;
    
    /** The partial Flag. */
    private String partialFlag;
    
    /** The creteDscId. */
    private String creteDscId;
    
    /** The createDateTime. */
    private Timestamp createDateTime;
    
    /** The updateDscId. */
    private String updateDscId;
    
    /** The updateDateTime. */
    private Timestamp updateDateTime;
    
    /**
     * Instantiates a new Kanban Tag information domain.
     */
    public KanbanTagDomain() {
        super();
    }

    /**
     * <p>Getter method for doId.</p>
     *
     * @return the doId
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * <p>Setter method for doId.</p>
     *
     * @param doId Set for doId
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * <p>Getter method for doNo.</p>
     *
     * @return the doNo
     */
    public String getDoNo() {
        return doNo;
    }

    /**
     * <p>Setter method for doNo.</p>
     *
     * @param dPn Set for doNo
     */
    public void setDoNo(String doNo) {
        this.doNo = doNo;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getdPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setdPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for kanbanTagSeq.</p>
     *
     * @return the kanbanTagSeq
     */
    public String getKanbanTagSeq() {
        return kanbanTagSeq;
    }

    /**
     * <p>Setter method for kanbanTagSeq.</p>
     *
     * @param kanbanTagSeq Set for kanbanTagSeq
     */
    public void setKanbanTagSeq(String kanbanTagSeq) {
        this.kanbanTagSeq = kanbanTagSeq;
    }

    /**
     * <p>Getter method for kanbanType.</p>
     *
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }

    /**
     * <p>Setter method for kanbanType.</p>
     *
     * @param kanbanType Set for kanbanType
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }

    /**
     * <p>Getter method for capacity.</p>
     *
     * @return the capacity
     */
    public BigDecimal getCapacity() {
        return capacity;
    }

    /**
     * <p>Setter method for capacity.</p>
     *
     * @param capacity Set for capacity
     */
    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }

    /**
     * <p>Getter method for qrContent.</p>
     *
     * @return the qrContent
     */
    public String getQrContent() {
        return qrContent;
    }

    /**
     * <p>Setter method for qrContent.</p>
     *
     * @param qrContent Set for qrContent
     */
    public void setQrContent(String qrContent) {
        this.qrContent = qrContent;
    }

    /**
     * <p>Getter method for partialFlag.</p>
     *
     * @return the partialFlag
     */
    public String getPartialFlag() {
        return partialFlag;
    }

    /**
     * <p>Setter method for partialFlag.</p>
     *
     * @param partialFlag Set for partialFlag
     */
    public void setPartialFlag(String partialFlag) {
        this.partialFlag = partialFlag;
    }

    /**
     * <p>Getter method for creteDscId.</p>
     *
     * @return the creteDscId
     */
    public String getCreteDscId() {
        return creteDscId;
    }

    /**
     * <p>Setter method for creteDscId.</p>
     *
     * @param creteDscId Set for creteDscId
     */
    public void setCreteDscId(String creteDscId) {
        this.creteDscId = creteDscId;
    }

    /**
     * <p>Getter method for createDateTime.</p>
     *
     * @return the createDateTime
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * <p>Setter method for createDateTime.</p>
     *
     * @param createDateTime Set for createDateTime
     */
    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * <p>Getter method for updateDscId.</p>
     *
     * @return the updateDscId
     */
    public String getUpdateDscId() {
        return updateDscId;
    }

    /**
     * <p>Setter method for updateDscId.</p>
     *
     * @param updateDscId Set for updateDscId
     */
    public void setUpdateDscId(String updateDscId) {
        this.updateDscId = updateDscId;
    }

    /**
     * <p>Getter method for updateDateTime.</p>
     *
     * @return the updateDateTime
     */
    public Timestamp getUpdateDateTime() {
        return updateDateTime;
    }

    /**
     * <p>Setter method for updateDateTime.</p>
     *
     * @param updateDateTime Set for updateDateTime
     */
    public void setUpdateDateTime(Timestamp updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
    
}