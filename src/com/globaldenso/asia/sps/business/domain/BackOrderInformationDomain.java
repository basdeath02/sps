/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * The Class Back Order Information Domain.
 * @author CSI
 */
public class BackOrderInformationDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;
    
    /** The file Id.*/
    private String fileId;
    
    /** The invoice date from. */
    private String deliveryDateFrom;
    
    /** The invoice date to. */
    private String deliveryDateTo;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String spsDoNo;
    
    /** The supplier plant code. */
    private String asnNo;
    
    /** The denso part no.*/
    private String dPn;
    
    /** The supplier part no.*/
    private String sPn;
    
    /** The invoice no.*/
    private String transportModeCb;
    
    /** The total shiping qty.*/
    private String totalShipingQty;
    
    /** The total received qty.*/
    private String totalReceivedQty;
    
    /** The total in transit qty.*/
    private String totalInTransitQty;
    
    /** The total back order qty.*/
    private String totalBackOrderQty;
    
    /** The big decimal total shiping qty.*/
    private BigDecimal bdTotalShipingQty;
    
    /** The big decimal total received qty.*/
    private BigDecimal bdTotalReceivedQty;
    
    /** The current order qty.*/
    private String currentOrderQty;
    
    /** The shipping order qty.*/
    private String shippingRoundQty;
    
    /** The delivery date.*/
    private String deliveryDate;
    
    /** The link ASN flag.*/
    private String linkAsnFlag;
    
    /** The sps transaction do domain. */
    private SpsTDoDomain spsTDoDomain;
    
    /** The sps transaction do detail domain. */
    private SpsTDoDetailDomain spsTDoDetailDomain;
    
    /** The sps transaction asn domain. */
    private SpsTAsnDomain spsTAsnDomain;
    
    /** The sps transaction asn detail domain. */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** The sps master company denso domain. */
    private SpsMCompanyDensoDomain spsMCompanyDensoDomain;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    
    /**
     * Instantiates a new file upload domain.
     */
    public BackOrderInformationDomain() {
        super();
        this.spsTDoDomain = new SpsTDoDomain();
    }
    
    /**
     * Gets the file Id.
     * 
     * @return the file Id.
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Sets the file Id.
     * 
     * @param fileId the file Id.
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * <p>Getter method for deliveryDateFrom.</p>
     *
     * @return the deliveryDateFrom
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * <p>Setter method for deliveryDateFrom.</p>
     *
     * @param deliveryDateFrom Set for deliveryDateFrom
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * <p>Getter method for deliveryDateTo.</p>
     *
     * @return the deliveryDateTo
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * <p>Setter method for deliveryDateTo.</p>
     *
     * @param deliveryDateTo Set for deliveryDateTo
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for spsDoNo.</p>
     *
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }


    /**
     * <p>Setter method for spsDoNo.</p>
     *
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }


    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }


    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }


    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }


    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }


    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }


    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }


    /**
     * <p>Getter method for transportModeCb.</p>
     *
     * @return the transportModeCb
     */
    public String getTransportModeCb() {
        return transportModeCb;
    }


    /**
     * <p>Setter method for transportModeCb.</p>
     *
     * @param transportModeCb Set for transportModeCb
     */
    public void setTransportModeCb(String transportModeCb) {
        this.transportModeCb = transportModeCb;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * <p>Setter method for denso code.</p>
     *
     * @param dCd Set for denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getDPcd() {
        return dPcd;
    }
    
    /**
     * <p>Setter method for denso plan code.</p>
     *
     * @param dPcd Set for denso plan code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for totalShipingQty.</p>
     *
     * @return the totalShipingQty
     */
    public String getTotalShipingQty() {
        return totalShipingQty;
    }

    /**
     * <p>Setter method for totalShipingQty.</p>
     *
     * @param totalShipingQty Set for totalShipingQty
     */
    public void setTotalShipingQty(String totalShipingQty) {
        this.totalShipingQty = totalShipingQty;
    }

    /**
     * <p>Getter method for totalReceivedQty.</p>
     *
     * @return the totalReceivedQty
     */
    public String getTotalReceivedQty() {
        return totalReceivedQty;
    }


    /**
     * <p>Setter method for totalReceivedQty.</p>
     *
     * @param totalReceivedQty Set for totalReceivedQty
     */
    public void setTotalReceivedQty(String totalReceivedQty) {
        this.totalReceivedQty = totalReceivedQty;
    }


    /**
     * <p>Getter method for totalInTransitQty.</p>
     *
     * @return the totalInTransitQty
     */
    public String getTotalInTransitQty() {
        return totalInTransitQty;
    }

    /**
     * <p>Setter method for totalInTransitQty.</p>
     *
     * @param totalInTransitQty Set for totalInTransitQty
     */
    public void setTotalInTransitQty(String totalInTransitQty) {
        this.totalInTransitQty = totalInTransitQty;
    }

    /**
     * <p>Getter method for totalBackOrderQty.</p>
     *
     * @return the totalBackOrderQty
     */
    public String getTotalBackOrderQty() {
        return totalBackOrderQty;
    }

    /**
     * <p>Setter method for totalBackOrderQty.</p>
     *
     * @param totalBackOrderQty Set for totalBackOrderQty
     */
    public void setTotalBackOrderQty(String totalBackOrderQty) {
        this.totalBackOrderQty = totalBackOrderQty;
    }

    /**
     * <p>Getter method for bdTotalShipingQty.</p>
     *
     * @return the bdTotalShipingQty
     */
    public BigDecimal getBdTotalShipingQty() {
        return bdTotalShipingQty;
    }

    /**
     * <p>Setter method for bdTotalShipingQty.</p>
     *
     * @param bdTotalShipingQty Set for bdTotalShipingQty
     */
    public void setBdTotalShipingQty(BigDecimal bdTotalShipingQty) {
        this.bdTotalShipingQty = bdTotalShipingQty;
    }

    /**
     * <p>Getter method for bdTotalReceivedQty.</p>
     *
     * @return the bdTotalReceivedQty
     */
    public BigDecimal getBdTotalReceivedQty() {
        return bdTotalReceivedQty;
    }

    /**
     * <p>Setter method for bdTotalReceivedQty.</p>
     *
     * @param bdTotalReceivedQty Set for bdTotalReceivedQty
     */
    public void setBdTotalReceivedQty(BigDecimal bdTotalReceivedQty) {
        this.bdTotalReceivedQty = bdTotalReceivedQty;
    }
    
    /**
     * <p>Getter method for currentOrderQty.</p>
     *
     * @return the currentOrderQty
     */
    public String getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * <p>Setter method for currentOrderQty.</p>
     *
     * @param currentOrderQty Set for currentOrderQty
     */
    public void setCurrentOrderQty(String currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * <p>Getter method for shippingRoundQty.</p>
     *
     * @return the shippingRoundQty
     */
    public String getShippingRoundQty() {
        return shippingRoundQty;
    }

    /**
     * <p>Setter method for shippingRoundQty.</p>
     *
     * @param shippingRoundQty Set for shippingRoundQty
     */
    public void setShippingRoundQty(String shippingRoundQty) {
        this.shippingRoundQty = shippingRoundQty;
    }

    /**
     * <p>Getter method for deliveryDate.</p>
     *
     * @return the deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * <p>Setter method for deliveryDate.</p>
     *
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    
    /**
     * <p>Getter method for linkAsnFlag.</p>
     *
     * @return the linkAsnFlag
     */
    public String getLinkAsnFlag() {
        return linkAsnFlag;
    }

    /**
     * <p>Setter method for linkAsnFlag.</p>
     *
     * @param linkAsnFlag Set for linkAsnFlag
     */
    public void setLinkAsnFlag(String linkAsnFlag) {
        this.linkAsnFlag = linkAsnFlag;
    }

    /**
     * <p>Getter method for spsTDoDomain.</p>
     *
     * @return the spsTDoDomain
     */
    public SpsTDoDomain getSpsTDoDomain() {
        return spsTDoDomain;
    }


    /**
     * <p>Setter method for spsTDoDomain.</p>
     *
     * @param spsTDoDomain Set for spsTDoDomain
     */
    public void setSpsTDoDomain(SpsTDoDomain spsTDoDomain) {
        this.spsTDoDomain = spsTDoDomain;
    }


    /**
     * <p>Getter method for spsTDoDetailDomain.</p>
     *
     * @return the spsTDoDetailDomain
     */
    public SpsTDoDetailDomain getSpsTDoDetailDomain() {
        return spsTDoDetailDomain;
    }


    /**
     * <p>Setter method for spsTDoDetailDomain.</p>
     *
     * @param spsTDoDetailDomain Set for spsTDoDetailDomain
     */
    public void setSpsTDoDetailDomain(SpsTDoDetailDomain spsTDoDetailDomain) {
        this.spsTDoDetailDomain = spsTDoDetailDomain;
    }


    /**
     * <p>Getter method for spsTAsnDomain.</p>
     *
     * @return the spsTAsnDomain
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }


    /**
     * <p>Setter method for spsTAsnDomain.</p>
     *
     * @param spsTAsnDomain Set for spsTAsnDomain
     */
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }


    /**
     * <p>Getter method for spsTAsnDetailDomain.</p>
     *
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }


    /**
     * <p>Setter method for spsTAsnDetailDomain.</p>
     *
     * @param spsTAsnDetailDomain Set for spsTAsnDetailDomain
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }

    /**
     * <p>Getter method for spsMCompanyDensoDomain.</p>
     *
     * @return the spsMCompanyDensoDomain
     */
    public SpsMCompanyDensoDomain getSpsMCompanyDensoDomain() {
        return spsMCompanyDensoDomain;
    }

    /**
     * <p>Setter method for spsMCompanyDensoDomain.</p>
     *
     * @param spsMCompanyDensoDomain Set for spsMCompanyDensoDomain
     */
    public void setSpsMCompanyDensoDomain(
        SpsMCompanyDensoDomain spsMCompanyDensoDomain) {
        this.spsMCompanyDensoDomain = spsMCompanyDensoDomain;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

}