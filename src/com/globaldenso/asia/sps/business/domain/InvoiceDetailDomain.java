/*
 * ModifyDate Development company     Describe 
 * 2014/08/08 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;


import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;

/**
 * The Class Invoice Detail Domain.
 * @author CSI
 */
public class InvoiceDetailDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID. */
    private static final long serialVersionUID = -4608123550779549228L;
    
    /** The sps asn domain. */
    private SpsTInvoiceDetailDomain spsTInvoiceDetailDomain;
    
    /** The sps asn domain. */
    private SpsTAsnDomain spsTAsnDomain;
    
    /** The Domain for SPS_T_ASN_DETAIL (for Transfer to JDE). */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** The denso currency cd.*/
    private String densoCurrencyCd;
    
    /** The asn Status.*/
    private String asnStatus;
    
    /** The change cigma do no.*/
    private String changeCigmaDoNo;
    
    /** The received date in type string.*/
    private String receivedDateStr;
    
    /**
     * Instantiates a new Invoice Detail Domain.
     */
    public InvoiceDetailDomain() {
        
    }

    /**
     * Gets the sps asn domain.
     * 
     * @return the spsTAsnDomain.
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }
    
    /**
     * <p>Getter method for spsTInvoiceDetailDomain.</p>
     *
     * @return the spsTInvoiceDetailDomain
     */
    public SpsTInvoiceDetailDomain getSpsTInvoiceDetailDomain() {
        return spsTInvoiceDetailDomain;
    }

    /**
     * <p>Setter method for spsTInvoiceDetailDomain.</p>
     *
     * @param spsTInvoiceDetailDomain Set for spsTInvoiceDetailDomain
     */
    public void setSpsTInvoiceDetailDomain(
        SpsTInvoiceDetailDomain spsTInvoiceDetailDomain) {
        this.spsTInvoiceDetailDomain = spsTInvoiceDetailDomain;
    }

    /**
     * Sets the sps asn domain.
     * 
     * @param spsTAsnDomain the sps asn domain.
     */
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }
    
    /**
     * <p>Getter method for densoCurrencyCd.</p>
     *
     * @return the densoCurrencyCd
     */
    public String getDensoCurrencyCd() {
        return densoCurrencyCd;
    }

    /**
     * <p>Setter method for densoCurrencyCd.</p>
     *
     * @param densoCurrencyCd Set for densoCurrencyCd
     */
    public void setDensoCurrencyCd(String densoCurrencyCd) {
        this.densoCurrencyCd = densoCurrencyCd;
    }

    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * <p>Getter method for changeCigmaDoNo.</p>
     *
     * @return the changeCigmaDoNo
     */
    public String getChangeCigmaDoNo() {
        return changeCigmaDoNo;
    }

    /**
     * <p>Setter method for changeCigmaDoNo.</p>
     *
     * @param changeCigmaDoNo Set for changeCigmaDoNo
     */
    public void setChangeCigmaDoNo(String changeCigmaDoNo) {
        this.changeCigmaDoNo = changeCigmaDoNo;
    }

    /**
     * <p>Getter method for spsTAsnDetailDomain.</p>
     *
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }

    /**
     * <p>Setter method for spsTAsnDetailDomain.</p>
     *
     * @param spsTAsnDetailDomain Set for spsTAsnDetailDomain
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }

    /**
     * <p>Getter method for receivedDateStr.</p>
     *
     * @return the receivedDateStr
     */
    public String getReceivedDateStr() {
        return receivedDateStr;
    }

    /**
     * <p>Setter method for receivedDateStr.</p>
     *
     * @param receivedDateStr Set for receivedDateStr
     */
    public void setReceivedDateStr(String receivedDateStr) {
        this.receivedDateStr = receivedDateStr;
    }
}
