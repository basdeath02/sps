/*
 * ModifyDate Development company Describe 
 * 2014/08/20 Phakaporn P.           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;

/**
 * The Class DensoUserUploading Domain.
 * 
 * @author CSI
 * @version 1.00
 */
public class DensoUserUploadingDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5917066651917642886L;

    /** The file management domain. */
    private FileManagementDomain fileManagementDomain;
    
    /** The Data Scope Control Domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The file name.*/
    private String totalRecord;
    
    /** The file name.*/
    private String correctRecord;
    
    /** The file name.*/
    private String inCorrectRecord;
    
    /** The file name.*/
    private String warningRecord;
    
    /** The User DSC ID.*/
    private String userDscId;
    
    /** The Session Code.*/
    private String sessionCode;
    
    /** The List of Temporary Upload Error domain. */
    private List<TmpUploadErrorDomain> tmpUploadErrorList;
    
    /** The List of Group Upload Error domain. */
    private List<GroupUploadErrorDomain> uploadErrorDetailDomain;
    
    /** The success message list. */
    private List<ApplicationMessageDomain> successMessageList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new DENSO user Uploading domain.
     */
    public DensoUserUploadingDomain() {
        super();
        this.fileManagementDomain = new FileManagementDomain();
    }
    
    
    /**
     * Gets the file management domain.
     * 
     * @return the file management domain.
     */
    public FileManagementDomain getFileManagementDomain() {
        return fileManagementDomain;
    }
    
    /**
     * Sets the file management domain.
     * 
     * @param fileManagementDomain the file management domain.
     */  
    public void setFileManagementDomain(FileManagementDomain fileManagementDomain) {
        this.fileManagementDomain = fileManagementDomain;
    }

    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    
    /**
     * Gets the total record.
     * 
     * @return the  total record.
     */
    public String getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the total record.
     * 
     * @param totalRecord the total record.
     */
    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    /**
     * Gets the correct record.
     * 
     * @return the correct record.
     */
    public String getCorrectRecord() {
        return correctRecord;
    }

    /**
     * Sets the correct record.
     * 
     * @param correctRecord the correct record.
     */
    public void setCorrectRecord(String correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * Gets the incorrect record.
     * 
     * @return the incorrect record.
     */
    public String getInCorrectRecord() {
        return inCorrectRecord;
    }

    /**
     * Sets the incorrect record.
     * 
     * @param inCorrectRecord the incorrect record.
     */
    public void setInCorrectRecord(String inCorrectRecord) {
        this.inCorrectRecord = inCorrectRecord;
    }

    /**
     * Gets the warning Record.
     * 
     * @return the warning Record.
     */
    public String getWarningRecord() {
        return warningRecord;
    }

    /**
     * Sets the warning Record.
     * 
     * @param warningRecord the warning Record.
     */
    public void setWarningRecord(String warningRecord) {
        this.warningRecord = warningRecord;
    }

    /**
     * Gets the Temporary of Upload Error List.
     * 
     * @return the Temporary of Upload Error List.
     */
    public List<TmpUploadErrorDomain> getTmpUploadErrorList() {
        return tmpUploadErrorList;
    }

    /**
     * Sets the Temporary of Upload Error List.
     * 
     * @param tempUploadErrorList the Temporary of Upload Error List.
     */
    public void setTmpUploadErrorList(
        List<TmpUploadErrorDomain> tempUploadErrorList) {
        this.tmpUploadErrorList = tempUploadErrorList;
    }

    /**
     * Gets the User DSC ID.
     * 
     * @return the User DSC ID.
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Sets the User DSC ID.
     * 
     * @param userDscId the User DSC ID.
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Gets the session code.
     * 
     * @return the session code.
     */
    public String getSessionCode() {
        return sessionCode;
    }

    /**
     * Sets the session code.
     * 
     * @param sessionCode the session code.
     */
    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    /**
     * Gets the list of group error domain.
     * 
     * @return the list of group error domain.
     */
    public List<GroupUploadErrorDomain> getUploadErrorDetailDomain() {
        return uploadErrorDetailDomain;
    }

    /**
     * Sets the list of group error domain.
     * 
     * @param uploadErrorDetailDomain the list of group error domain.
     */
    public void setUploadErrorDetailDomain(
        List<GroupUploadErrorDomain> uploadErrorDetailDomain) {
        this.uploadErrorDetailDomain = uploadErrorDetailDomain;
    }
    
    /**
     * Gets the success message list.
     * 
     * @return the success message list.
     */
    public List<ApplicationMessageDomain> getSuccessMessageList() {
        return successMessageList;
    }

    /**
     * Sets the success message list.
     * 
     * @param successMessageList the success message list.
     */
    public void setSuccessMessageList(List<ApplicationMessageDomain> successMessageList) {
        this.successMessageList = successMessageList;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }

}