/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/12 Parichat           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Class AnnounceMessageDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class TmpFileBackupDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3935827237501551534L;

    /** The seq no. */
    private int seqNo;
    
    /** The file path. */
    private String filePath;
    
    /** The create date time. */
    private Timestamp createDateTime;
    
    
    /**
     * Instantiates a new account domain.
     */
    public TmpFileBackupDomain() {
        super();
    }

    /**
     * Gets the seq no.
     * 
     * @return the seq no.
     */
    public int getSeqNo() {
        return seqNo;
    }
    
    /**
     * Sets the seq no.
     * 
     * @param seqNo the seq no.
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Gets the file path.
     * 
     * @return the seq no.
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets the file path.
     * 
     * @param filePath the file path.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Gets the create date time.
     * 
     * @return the create date time.
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the create date time.
     * 
     * @param createDateTime the create date time.
     */
    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

}