/*
 * ModifyDate Development company     Describe 
 * 2014/06/18 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain;


/**
 * The Class File Upload Result Domain.
 * @author CSI
 */
public class FileUploadResultDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = -970430853868154934L;

    /**
     * FileUpload For Display Domain List.
     */
    private List<FileUploadForDisplayDomain> fileUploadForDisplayDomainList;
    
    /**
     * SpsTFile Upload Criteria Domain.
     */
    private SpsTFileUploadCriteriaDomain spsTFileUploadCriteriaDomain;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The company supplier list. */
    private List<MiscellaneousDomain> companySupplierList;

    /** The company denso list. */
    private List<MiscellaneousDomain> companyDensoList;

    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The plant supplier list. */
    private List<SpsMMiscDomain> plantSupplierList;
    
    /** The List of Denso Supplier Relation. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public FileUploadResultDomain() {
        super();
        this.companyDensoList = new ArrayList<MiscellaneousDomain>();
        this.companySupplierList = new ArrayList<MiscellaneousDomain>();
        this.plantSupplierList = new ArrayList<SpsMMiscDomain>();
        this.plantDensoList = new ArrayList<PlantDensoDomain>();
    }

    /**
     * <p>Getter method for fileUploadForDisplayDomainList.</p>
     *
     * @return the fileUploadForDisplayDomainList
     */
    public List<FileUploadForDisplayDomain> getFileUploadForDisplayDomainList() {
        return fileUploadForDisplayDomainList;
    }
    /**
     * <p>Setter method for fileUploadForDisplayDomainList.</p>
     *
     * @param fileUploadForDisplayDomainList Set for fileUploadForDisplayDomainList
     */
    public void setFileUploadForDisplayDomainList(
        List<FileUploadForDisplayDomain> fileUploadForDisplayDomainList) {
        this.fileUploadForDisplayDomainList = fileUploadForDisplayDomainList;
    }
    /**
     * <p>Getter method for spsTFileUploadCriteriaDomain.</p>
     *
     * @return the spsTFileUploadCriteriaDomain
     */
    public SpsTFileUploadCriteriaDomain getSpsTFileUploadCriteriaDomain() {
        return spsTFileUploadCriteriaDomain;
    }

    /**
     * <p>Setter method for spsTFileUploadCriteriaDomain.</p>
     *
     * @param spsTFileUploadCriteriaDomain Set for spsTFileUploadCriteriaDomain
     */
    public void setSpsTFileUploadCriteriaDomain(
        SpsTFileUploadCriteriaDomain spsTFileUploadCriteriaDomain) {
        this.spsTFileUploadCriteriaDomain = spsTFileUploadCriteriaDomain;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<MiscellaneousDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(List<MiscellaneousDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<MiscellaneousDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<MiscellaneousDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for densoSupplierRelationList.</p>
     *
     * @return the densoSupplierRelationList
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * <p>Setter method for densoSupplierRelationList.</p>
     *
     * @param densoSupplierRelationList Set for densoSupplierRelationList
     */
    public void setDensoSupplierRelationList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<SpsMMiscDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<SpsMMiscDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

}
