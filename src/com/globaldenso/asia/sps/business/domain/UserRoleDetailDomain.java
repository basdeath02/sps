/*
 * ModifyDate Development company Describe 
 * 2014/08/21 CSI Phakaporn           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class user role detail Domain.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserRoleDetailDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 322339133316860918L;

    /**
     * Role code
     */
    private String roleCd;
    
    /**
     * Role code that selected.
     */
    private String roleCdSelected;
    
    /**
     * Role name
     */
    private String roleName;

    /**
     * DENSO user DSC ID
     */
    private String dscId;

    /**
     * Sequence Number
     */
    private BigDecimal seqNo;
    
    /**
     * Sequence Number
     */
    private String seqNoScreen;

    /**
     * company DENSO code
     */
    private String dCd;

    /**
     * plant DENSO code
     */
    private String dPcd;

    /**
     * Company Supplier code
     */
    private String sCd;

    /**
     * Plant Supplier code
     */
    private String sPcd;
    
    /**
     * Effective start date
     */
    private Timestamp effectStart;

    /**
     * Effective end date
     */
    private Timestamp effectEnd;
    
    /** The The effect start for screen. */
    private String effectStartScreen;
    
    /** The The effect end for screen. */
    private String effectEndScreen;
    
    /** The The create date for screen. */
    private String createDateScreen;
    
    /** The The last update date time for screen. */
    private String lastUpdateDatetimeScreen;
    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create date time
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;
    
    /**
     * User first name
     */
    private String firstName;

    /**
     * User middle name
     */
    private String middleName;

    /**
     * User last name
     */
    private String lastName;

    /** The default constructor. */
    public UserRoleDetailDomain() {
        super();
    }

    /**
     * Get method for roleCd.
     * @return the roleCd
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Set method for roleCd.
     * @param roleCd the roleCd to set
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Getter method of "roleName"
     * 
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Setter method of "roleName"
     * 
     * @param roleName Set in "roleName".
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    
    /**
     * Get method for dscId.
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Set method for dscId.
     * @param dscId the dscId to set
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Get method for seqNo.
     * @return the seqNo
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Set method for seqNo.
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Get method for company DENSO code.
     * @return the company DENSO code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Set method for company DENSO code.
     * @param dCd the company DENSO code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * Get method for Plant DENSO code.
     * @return the Plant DENSO code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Set method for Plant DENSO code.
     * @param dPcd the Plant DENSO code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * Get method for company supplier code.
     * @return the company supplier code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Set method for company supplier code.
     * @param sCd the company supplier code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Get method for plant supplier code.
     * @return the plant supplier code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Set method for plant supplier code.
     * @param sPcd the plant supplier code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * Getter method of "effectStart"
     * 
     * @return the effectStart
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart"
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd"
     * 
     * @return the effectEnd
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd"
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Gets the Effect start date for display on screen.
     * 
     * @return the Effect start date for display on screen.
     */
    public String getEffectStartScreen() {
        return effectStartScreen;
    }
    /**
     * Sets the Effect start date for display on screen.
     * 
     * @param effectStartScreen the Effect start date for display on screen.
     */
    public void setEffectStartScreen(String effectStartScreen) {
        this.effectStartScreen = effectStartScreen;
    }
    /**
     * Gets the Effect end date for display on screen.
     * 
     * @return the Effect end date for display on screen.
     */
    public String getEffectEndScreen() {
        return effectEndScreen;
    }
    /**
     * Sets the Effect end date for display on screen.
     * 
     * @param effectEndScreen the Effect end date for display on screen.
     */
    public void setEffectEndScreen(String effectEndScreen) {
        this.effectEndScreen = effectEndScreen;
    }
    /**
     * Gets the role code selected.
     * 
     * @return the role code selected.
     */
    public String getRoleCdSelected() {
        return roleCdSelected;
    }
    /**
     * Sets the role code selected.
     * 
     * @param roleCdSelected the role code selected.
     */
    public void setRoleCdSelected(String roleCdSelected) {
        this.roleCdSelected = roleCdSelected;
    }
    /**
     * Gets the create date for screen.
     * 
     * @return the create date for screen.
     */
    public String getCreateDateScreen() {
        return createDateScreen;
    }
    /**
     * Sets the create date for screen.
     * 
     * @param createDateScreen the create date for screen.
     */
    public void setCreateDateScreen(String createDateScreen) {
        this.createDateScreen = createDateScreen;
    }
    /**
     * Getter method of "firstName"
     * 
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName"
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName"
     * 
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName"
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName"
     * 
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName"
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Getter last update date time for screen.
     * 
     * @return the last update date time for screen.
     */
    public String getLastUpdateDatetimeScreen() {
        return lastUpdateDatetimeScreen;
    }
    /**
     * Setter last update date time for screen.
     * 
     * @param lastUpdateDatetimeScreen Set last update date time for screen.
     */
    public void setLastUpdateDatetimeScreen(String lastUpdateDatetimeScreen) {
        this.lastUpdateDatetimeScreen = lastUpdateDatetimeScreen;
    }
    /**
     * Getter seq no on screen.
     * 
     * @return the seq no on screen.
     */
    public String getSeqNoScreen() {
        return seqNoScreen;
    }
    /**
     * Setter seq no on screen.
     * 
     * @param seqNoScreen the seq no on screen.
     */
    public void setSeqNoScreen(String seqNoScreen) {
        this.seqNoScreen = seqNoScreen;
    }
    
}
