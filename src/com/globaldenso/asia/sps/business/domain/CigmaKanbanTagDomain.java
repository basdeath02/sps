/*
 * ModifyDate Development company     Describe 
 * 2014/08/14 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain;

/**
 * The Class ASN Domain.
 * @author CSI
 */
public class CigmaKanbanTagDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6031529933047869169L;
    
    /** The SPS transaction ASN domain. */
    private SpsTAsnDomain spsTAsnDomain;
    
    /** The SPS transaction ASN detail domain. */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** The SPS transaction ASN detail domain. */
    private SpsTmpUploadInvoiceDomain spsTmpUploadInvoiceDomain;
    
    /** The actual ETD. */
    private String actualEtd;
    
    /** The plan ETA. */
    private String planEta;
    
    /** The supplier code. */
    private String sCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The supplier part code. */
    private String sPn;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The denso part code. */
    private String dPn;
    
    /** The asn no.*/
    private String asnNo;
    
    /** The asn status.*/
    private String asnStatus;
    
    /** The do id.*/
    private String doId;
    
    /** The trip no.*/
    private String tripNo;
    
    /** The denso unit Of measure.*/
    private String densoUnitOfMeasure;
    
    /** The denso unit price.*/
    private String densoUnitPrice;
    
    /** The big decimal denso unit price.*/
    private BigDecimal bdDensoUnitPrice;
    
    /** The supplier unit Of measure.*/
    private String supplierUnitOfMeasure;
    
    /** The supplier unit price.*/
    private String supplierUnitPrice;
    
    /** The big decimal supplier unit price.*/
    private BigDecimal bdSupplierUnitPrice;
    
    /** The denso difference base amount.*/
    private String densoDiffBaseAmount;
    
    /** The difference base amount.*/
    private String diffBaseAmount;
    
    /** The difference base amount.*/
    private String bdDiffBaseAmount;
    
    /** The denso base amount.*/
    private String densoBaseAmount;
    
    /** The denso currency cd.*/
    private String densoCurrencyCd;
    
    /** The shipping qty.*/
    private String shippingQty;
    
    /** The unit of measure.*/
    private String unitOfMeasure;
    
    /** The change cigma do no.*/
    private String changeCigmaDoNo;
    
    /** The created invoice flag.*/
    private String createdInvoiceFlag;
    
    /** The Tempolary Price Flag. */
    private String tempPriceFlag;
    
    /** The SPS D/O No. */
    private String spsDoNo;
    
    /** The Revision. */
    private String revision;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /**
     * Instantiates a new ASN domain.
     */
    public CigmaKanbanTagDomain() {
        super();
        this.spsTAsnDomain = new SpsTAsnDomain();
        this.spsTAsnDetailDomain = new SpsTAsnDetailDomain();
    }
    
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }
    
    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }
    
    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Gets the sps ASN domain.
     * 
     * @return the spsTInvoiceDomain.
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }
    
    /**
     * Sets the sps ASN domain.
     * 
     * @param spsTAsnDomain the sps ASN domain.
     */  
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }
    
    /**
     * Gets the ASN Status.
     * 
     * @return the ASN Status.
     */
    public String getAsnStatus() {
        return asnStatus;
    }
    
    /**
     * Sets the ASN Status.
     * 
     * @param asnStatus the ASN Status.
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }
    
    /**
     * <p>Getter method for doId.</p>
     *
     * @return the doId
     */
    public String getDoId() {
        return doId;
    }

    /**
     * <p>Setter method for doId.</p>
     *
     * @param doId Set for doId
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * <p>Getter method for tripNo.</p>
     *
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * <p>Setter method for tripNo.</p>
     *
     * @param tripNo Set for tripNo
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Gets the actual etd.
     * 
     * @return the actual etd.
     */
    public String getActualEtd() {
        return actualEtd;
    }
    
    /**
     * Sets the actual etd.
     * 
     * @param actualEtd the actual etd.
     */
    public void setActualEtd(String actualEtd) {
        this.actualEtd = actualEtd;
    }
    
    /**
     * Gets the plan eta.
     * 
     * @return the plan eta.
     */
    public String getPlanEta() {
        return planEta;
    }
    
    /**
     * Sets the plan eta.
     * 
     * @param planEta the plan eta.
     */
    public void setPlanEta(String planEta) {
        this.planEta = planEta;
    }
    
    /**
     * Gets the asn detail domain.
     * 
     * @return the asn detail domain.
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }

    /**
     * Sets the asn detail domain.
     * 
     * @param spsTAsnDetailDomain the asn detail domain.
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }
    
    /**
     * <p>Getter method for spsTmpUploadInvoiceDomain.</p>
     *
     * @return the spsTmpUploadInvoiceDomain
     */
    public SpsTmpUploadInvoiceDomain getSpsTmpUploadInvoiceDomain() {
        return spsTmpUploadInvoiceDomain;
    }

    /**
     * <p>Setter method for spsTmpUploadInvoiceDomain.</p>
     *
     * @param spsTmpUploadInvoiceDomain Set for spsTmpUploadInvoiceDomain
     */
    public void setSpsTmpUploadInvoiceDomain(
        SpsTmpUploadInvoiceDomain spsTmpUploadInvoiceDomain) {
        this.spsTmpUploadInvoiceDomain = spsTmpUploadInvoiceDomain;
    }

    /**
     * <p>Getter method for densoUnitOfMeasure.</p>
     *
     * @return the densoUnitOfMeasure
     */
    public String getDensoUnitOfMeasure() {
        return densoUnitOfMeasure;
    }

    /**
     * <p>Setter method for densoUnitOfMeasure.</p>
     *
     * @param densoUnitOfMeasure Set for densoUnitOfMeasure
     */
    public void setDensoUnitOfMeasure(String densoUnitOfMeasure) {
        this.densoUnitOfMeasure = densoUnitOfMeasure;
    }

    /**
     * <p>Getter method for densoUnitPrice.</p>
     *
     * @return the densoUnitPrice
     */
    public String getDensoUnitPrice() {
        return densoUnitPrice;
    }

    /**
     * <p>Setter method for densoUnitPrice.</p>
     *
     * @param densoUnitPrice Set for densoUnitPrice
     */
    public void setDensoUnitPrice(String densoUnitPrice) {
        this.densoUnitPrice = densoUnitPrice;
    }

    /**
     * <p>Getter method for bdDensoUnitPrice.</p>
     *
     * @return the bdDensoUnitPrice
     */
    public BigDecimal getBdDensoUnitPrice() {
        return bdDensoUnitPrice;
    }

    /**
     * <p>Setter method for bdDensoUnitPrice.</p>
     *
     * @param bdDensoUnitPrice Set for bdDensoUnitPrice
     */
    public void setBdDensoUnitPrice(BigDecimal bdDensoUnitPrice) {
        this.bdDensoUnitPrice = bdDensoUnitPrice;
    }

    /**
     * <p>Getter method for supplierUnitOfMeasure.</p>
     *
     * @return the supplierUnitOfMeasure
     */
    public String getSupplierUnitOfMeasure() {
        return supplierUnitOfMeasure;
    }

    /**
     * <p>Setter method for supplierUnitOfMeasure.</p>
     *
     * @param supplierUnitOfMeasure Set for supplierUnitOfMeasure
     */
    public void setSupplierUnitOfMeasure(String supplierUnitOfMeasure) {
        this.supplierUnitOfMeasure = supplierUnitOfMeasure;
    }

    /**
     * <p>Getter method for supplierUnitPrice.</p>
     *
     * @return the supplierUnitPrice
     */
    public String getSupplierUnitPrice() {
        return supplierUnitPrice;
    }

    /**
     * <p>Setter method for supplierUnitPrice.</p>
     *
     * @param supplierUnitPrice Set for supplierUnitPrice
     */
    public void setSupplierUnitPrice(String supplierUnitPrice) {
        this.supplierUnitPrice = supplierUnitPrice;
    }

    /**
     * <p>Getter method for bdSupplierUnitPrice.</p>
     *
     * @return the bdSupplierUnitPrice
     */
    public BigDecimal getBdSupplierUnitPrice() {
        return bdSupplierUnitPrice;
    }

    /**
     * <p>Setter method for bdSupplierUnitPrice.</p>
     *
     * @param bdSupplierUnitPrice Set for bdSupplierUnitPrice
     */
    public void setBdSupplierUnitPrice(BigDecimal bdSupplierUnitPrice) {
        this.bdSupplierUnitPrice = bdSupplierUnitPrice;
    }

    /**
     * <p>Getter method for densoDiffBaseAmount.</p>
     *
     * @return the densoDiffBaseAmount
     */
    public String getDensoDiffBaseAmount() {
        return densoDiffBaseAmount;
    }

    /**
     * <p>Setter method for densoDiffBaseAmount.</p>
     *
     * @param densoDiffBaseAmount Set for densoDiffBaseAmount
     */
    public void setDensoDiffBaseAmount(String densoDiffBaseAmount) {
        this.densoDiffBaseAmount = densoDiffBaseAmount;
    }

    /**
     * <p>Getter method for diffBaseAmount.</p>
     *
     * @return the diffBaseAmount
     */
    public String getDiffBaseAmount() {
        return diffBaseAmount;
    }

    /**
     * <p>Setter method for diffBaseAmount.</p>
     *
     * @param diffBaseAmount Set for diffBaseAmount
     */
    public void setDiffBaseAmount(String diffBaseAmount) {
        this.diffBaseAmount = diffBaseAmount;
    }

    /**
     * <p>Getter method for bdDiffBaseAmount.</p>
     *
     * @return the bdDiffBaseAmount
     */
    public String getBdDiffBaseAmount() {
        return bdDiffBaseAmount;
    }

    /**
     * <p>Setter method for bdDiffBaseAmount.</p>
     *
     * @param bdDiffBaseAmount Set for bdDiffBaseAmount
     */
    public void setBdDiffBaseAmount(String bdDiffBaseAmount) {
        this.bdDiffBaseAmount = bdDiffBaseAmount;
    }

    /**
     * <p>Getter method for densoBaseAmount.</p>
     *
     * @return the densoBaseAmount
     */
    public String getDensoBaseAmount() {
        return densoBaseAmount;
    }

    /**
     * <p>Setter method for densoBaseAmount.</p>
     *
     * @param densoBaseAmount Set for densoBaseAmount
     */
    public void setDensoBaseAmount(String densoBaseAmount) {
        this.densoBaseAmount = densoBaseAmount;
    }

    /**
     * <p>Getter method for densoCurrencyCd.</p>
     *
     * @return the densoCurrencyCd
     */
    public String getDensoCurrencyCd() {
        return densoCurrencyCd;
    }

    /**
     * <p>Setter method for densoCurrencyCd.</p>
     *
     * @param densoCurrencyCd Set for densoCurrencyCd
     */
    public void setDensoCurrencyCd(String densoCurrencyCd) {
        this.densoCurrencyCd = densoCurrencyCd;
    }

    /**
     * <p>Getter method for shippingQty.</p>
     *
     * @return the shippingQty
     */
    public String getShippingQty() {
        return shippingQty;
    }

    /**
     * <p>Setter method for shippingQty.</p>
     *
     * @param shippingQty Set for shippingQty
     */
    public void setShippingQty(String shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * <p>Getter method for unitOfMeasure.</p>
     *
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * <p>Setter method for unitOfMeasure.</p>
     *
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>Getter method for changeCigmaDoNo.</p>
     *
     * @return the changeCigmaDoNo
     */
    public String getChangeCigmaDoNo() {
        return changeCigmaDoNo;
    }

    /**
     * <p>Setter method for changeCigmaDoNo.</p>
     *
     * @param changeCigmaDoNo Set for changeCigmaDoNo
     */
    public void setChangeCigmaDoNo(String changeCigmaDoNo) {
        this.changeCigmaDoNo = changeCigmaDoNo;
    }

    /**
     * <p>Getter method for createdInvoiceFlag.</p>
     *
     * @return the createdInvoiceFlag
     */
    public String getCreatedInvoiceFlag() {
        return createdInvoiceFlag;
    }

    /**
     * <p>Setter method for createdInvoiceFlag.</p>
     *
     * @param createdInvoiceFlag Set for createdInvoiceFlag
     */
    public void setCreatedInvoiceFlag(String createdInvoiceFlag) {
        this.createdInvoiceFlag = createdInvoiceFlag;
    }

    /**
     * <p>Getter method for tempPriceFlag.</p>
     *
     * @return the tempPriceFlag
     */
    public String getTempPriceFlag() {
        return tempPriceFlag;
    }

    /**
     * <p>Setter method for tempPriceFlag.</p>
     *
     * @param tempPriceFlag Set for tempPriceFlag
     */
    public void setTempPriceFlag(String tempPriceFlag) {
        this.tempPriceFlag = tempPriceFlag;
    }

    /**
     * <p>Getter method for spsDoNo.</p>
     *
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>Setter method for spsDoNo.</p>
     *
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>Getter method for revision.</p>
     *
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>Setter method for revision.</p>
     *
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

}