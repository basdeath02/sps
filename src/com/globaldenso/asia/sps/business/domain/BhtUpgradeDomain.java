package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class Upgrade BHT software Domain.
 * @author Netband
 */
public class BhtUpgradeDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3124388929526505074L;

    /** The systemId. */
    private String systemId;
    
    /** The verNo. */
    private String verNo;
    
    /** The binaryFile. */
    private byte[] binaryFile;
    
    /** The verNo. */
    private byte[] utilityFile;
    
    /**
     * Constructor bhtUpgradeDomain.
     */
    public BhtUpgradeDomain(){
    }
    
    /**
     * Gets verNo.
     * 
     * @return the verNo
     */
    public String getVerNo() {
        return verNo;
    }
    
    /**
     * Sets the verNo.
     * 
     * @param verNo the verNo
     */
    public void setVerNo(String verNo) {
        this.verNo = verNo;
    }
    /**
     * Gets binaryFile.
     * 
     * @return the binaryFile
     */

    public byte[] getBinaryFile() {
        return binaryFile;
    }
    /**
     * Sets the binaryFile.
     * 
     * @param binaryFile the binaryFile
     */

    public void setBinaryFile(byte[] binaryFile) {
        this.binaryFile = binaryFile;
    }
    /**
     * Gets systemId.
     * 
     * @return the systemId
     */

    public String getSystemId() {
        return systemId;
    }
    /**
     * Sets the systemId.
     * 
     * @param systemId the systemId
     */

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    /**
     * <p>Getter method for utilityFile.</p>
     *
     * @return the utilityFile
     */
    public byte[] getUtilityFile() {
        return utilityFile;
    }

    /**
     * <p>Setter method for utilityFile.</p>
     *
     * @param utilityFile Set for utilityFile
     */
    public void setUtilityFile(byte[] utilityFile) {
        this.utilityFile = utilityFile;
    }
}
