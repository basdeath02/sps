/*
 * ModifyDate Development company     Describe 
 * 2015/03/11 CSI Akat                Create
 * 2015/10/06 CSI Akat                [IN016]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Transfer Delivery Order Error Email header Domain.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TransferDoErrorEmailDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 3138867394725968428L;

    /** S_CD */
    private String sCd;
    
    // [IN016] Send email to supplier by plant
    /** S_PCD */
    private String sPcd;

    /** VENDOR_CD */
    private String vendorCd;

    /** D_CD */
    private String dCd;

    /** D_PCD */
    private String dPcd;
    
    /** CIGMA_DO_NO */
    private String cigmaDoNo;

    /** ERROR_TYPE_FLG */
    private String errorTypeFlg;

    /** ISSUE_DATE */
    private Date issueDate;

    /** DELIVERY_DATE */
    private Date deliveryDate;

    /** DELIVERY_TIME */
    private String deliveryTime;

    /** List of TransferDoErrorEmailDetailDomain. */
    private List<TransferDoErrorEmailDetailDomain> transferDoErrorEmailDetailList;
    
    /** The default constructor. */
    public TransferDoErrorEmailDomain() {
        this.transferDoErrorEmailDetailList = new ArrayList<TransferDoErrorEmailDetailDomain>();
    }
    
    private BigDecimal currentOrderQty;
    private BigDecimal originalOrderQty;

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>Getter method for errorTypeFlg.</p>
     *
     * @return the errorTypeFlg
     */
    public String getErrorTypeFlg() {
        return errorTypeFlg;
    }

    /**
     * <p>Setter method for errorTypeFlg.</p>
     *
     * @param errorTypeFlg Set for errorTypeFlg
     */
    public void setErrorTypeFlg(String errorTypeFlg) {
        this.errorTypeFlg = errorTypeFlg;
    }

    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * <p>Getter method for transferDoErrorEmailDetailList.</p>
     *
     * @return the transferDoErrorEmailDetailList
     */
    public List<TransferDoErrorEmailDetailDomain> getTransferDoErrorEmailDetailList() {
        return transferDoErrorEmailDetailList;
    }

    /**
     * <p>Setter method for transferDoErrorEmailDetailList.</p>
     *
     * @param transferDoErrorEmailDetailList Set for transferDoErrorEmailDetailList
     */
    public void setTransferDoErrorEmailDetailList(
        List<TransferDoErrorEmailDetailDomain> transferDoErrorEmailDetailList) {
        this.transferDoErrorEmailDetailList = transferDoErrorEmailDetailList;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for deliveryDate.</p>
     *
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * <p>Setter method for deliveryDate.</p>
     *
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * <p>Getter method for deliveryTime.</p>
     *
     * @return the deliveryTime
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * <p>Setter method for deliveryTime.</p>
     *
     * @param deliveryTime Set for deliveryTime
     */
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for currentQty.</p>
     *
     * @return the currentQty
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * <p>Setter method for currentQty.</p>
     *
     * @param currentQty Set for currentQty
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * <p>Getter method for backOrderQty.</p>
     *
     * @return the backOrderQty
     */
    public BigDecimal getOriginalOrderQty() {
        return originalOrderQty;
    }

    /**
     * <p>Setter method for backOrderQty.</p>
     *
     * @param backOrderQty Set for backOrderQty
     */
    public void setOriginalOrderQty(BigDecimal originalOrderQty) {
        this.originalOrderQty = originalOrderQty;
    }
    
}
