/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class DensoUserRoleAssign Domain.
 * @author CSI
 */
public class DensoUserRoleAssignDomain extends BaseDomain implements Serializable {
 
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3736003052624160424L;
    
    /** The Data Scope Control Domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID selected. */
    private String dscIdSelected; 
    
    /** The Company DENSO Code. */
    private String dCd;
   
    /** The Plant DENSO. */
    private String dPcd;
    
    /** The Role Code. */
    private String roleCode;
    
    /** The First Name. */
    private String firstName;
    
    /** The Middle Name. */
    private String middleName;
    
    /** The Last Name. */
    private String lastName;
    
    /** The Employee Code. */
    private String employeeCode;
    
    /** The Is Active. */
    private String isActive;
    
    /** The user Domain.*/
    private UserDomain userDomain;
    
    /** The DENSO user detail domain. */
    private UserDensoDetailDomain userDensoDetailDomain;
    
    /** The user DENSO Domain.*/
    private SpsMUserDensoDomain spsMUserDensoDomain;
    
    /** The User Role Domain.*/
    private SpsMUserRoleDomain spsMUserRoleDomain;
    
    /** The List of User DENSO Role Assignment domain. */
    private List<DensoUserRoleAssignDomain> userDensoRoleAssignList;
    
    /** The List of Company DENSO domain. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of Plant DENSO domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The Date time update. */
    private Timestamp lastUpdateDatetime;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
    
    /** The effect Start Screen. */
    private String effectStartScreen;
    
    /** The effect End Screen. */
    private String effectEndScreen;
    
    /** The effect Start Screen. */
    private String createDatetimeScreen;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    /** The mode. */
    private String mode;
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public DensoUserRoleAssignDomain() {
        super(); 
        userDomain = new UserDomain();
        userDensoDetailDomain = new UserDensoDetailDomain();
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Gets the dscId Selected.
     * 
     * @return the dscId Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }

    /**
     * Sets the dscId Selected.
     * 
     * @param dscIdSelected the new dscId Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    
    /**
     * Gets the role Code
     * 
     * @return the role Code
     */  
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the role Code.
     * 
     * @param roleCode the role Code.
     */ 
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
   
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }

    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the Effect Start Screen.
     * 
     * @return the getEffectStartScreen.
     */ 
    public String getEffectStartScreen() {
        return effectStartScreen;
    }

    /**
     * Sets the effect Start Screen.
     * 
     * @param effectStartScreen the new effect Start Screen.
     */
    public void setEffectStartScreen(String effectStartScreen) {
        this.effectStartScreen = effectStartScreen;
    }

    /**
     * Gets the effect End Screen.
     * 
     * @return the effectEndScreen.
     */ 
    public String getEffectEndScreen() {
        return effectEndScreen;
    }

    /**
     * Sets the effect End Screen.
     * 
     * @param effectEndScreen the new effect End Screen.
     */
    public void setEffectEndScreen(String effectEndScreen) {
        this.effectEndScreen = effectEndScreen;
    }

    /**
     * Gets the create Date time Screen
     * 
     * @return the createDatetimeScreen.
     */ 
    public String getCreateDatetimeScreen() {
        return createDatetimeScreen;
    }

    /**
     * Sets the create Date time Screen.
     * 
     * @param createDatetimeScreen the new create Date time Screen.
     */
    public void setCreateDatetimeScreen(String createDatetimeScreen) {
        this.createDatetimeScreen = createDatetimeScreen;
    }
    
    /**
     * Gets the list of user DENSO role assignment detail.
     * 
     * @return the user DENSO role assignment detail.
     */
    public List<DensoUserRoleAssignDomain> getUserDensoRoleAssignList() {
        return userDensoRoleAssignList;
    }

    /**
     * Sets the user DENSO role assignment detail.
     * 
     * @param userDensoRoleAssignList the user DENSO role assignment detail.
     */
    public void setUserDensoRoleAssignList(List<DensoUserRoleAssignDomain> userDensoRoleAssignList){
        this.userDensoRoleAssignList = userDensoRoleAssignList;
    }
    
    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the DENSO Company code.
     * 
     * @return the DENSO Company code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Sets the Company DENSO code.
     * 
     * @param dCd the Company DENSO code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * Gets the plant DENSO code.
     * 
     * @return the plant DENSO code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the plant DENSO code.
     * 
     * @param dPcd the plant DENSO code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the list of company DENSO.
     * 
     * @return the list of company DENSO.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }
    /**
     * Sets the list of company DENSO.
     * 
     * @param companyDensoList the list of company DENSO.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    /**
     * Gets the list of Plant DENSO.
     * 
     * @return the list of Plant DENSO.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }
    /**
     * Sets the list of Plant DENSO.
     * 
     * @param plantDensoList the list of Plant DENSO.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    /**
     * Gets the First name.
     * 
     * @return the First name.
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * Sets the First name.
     * 
     * @param firstName the First name..
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * Gets the Middle name.
     * 
     * @return the Middle name.
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * Sets the Middle name.
     * 
     * @param middleName the Middle name..
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Gets the Employee code.
     * 
     * @return the Employee code.
     */
    public String getEmployeeCode() {
        return employeeCode;
    }
    /**
     * Sets the Employee code.
     * 
     * @param employeeCode the Employee code.
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the Last Update date.
     * 
     * @return the Last Update date.
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Sets the Last Update date.
     * 
     * @param lastUpdateDatetime the Last Update date.
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Gets the Is Active.
     * 
     * @return the Is Active.
     */
    public String getIsActive() {
        return isActive;
    }
    /**
     * Sets the Is Active.
     * 
     * @param isActive the Is Active.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
   
    /**
     * Gets the DENSO User Domain.
     * 
     * @return the DENSO User Domain.
     */
    public SpsMUserDensoDomain getSpsMUserDensoDomain() {
        return spsMUserDensoDomain;
    }
    /**
     * Sets the DENSO User Domain.
     * 
     * @param spsMUserDensoDomain the DENSO User Domain.
     */
    public void setSpsMUserDensoDomain(SpsMUserDensoDomain spsMUserDensoDomain) {
        this.spsMUserDensoDomain = spsMUserDensoDomain;
    }
    /**
     * Gets the User Role Domain.
     * 
     * @return the User Role Domain.
     */
    public SpsMUserRoleDomain getSpsMUserRoleDomain() {
        return spsMUserRoleDomain;
    }
    /**
     * Sets the User Role Domain.
     * 
     * @param spsMUserRoleDomain the User Role Domain.
     */
    public void setSpsMUserRoleDomain(SpsMUserRoleDomain spsMUserRoleDomain) {
        this.spsMUserRoleDomain = spsMUserRoleDomain;
    }
    
    /**
     * <p>Getter method for DENSO user detail domain.</p>
     *
     * @return the DENSO user detail domain.
     */
    public UserDensoDetailDomain getUserDensoDetailDomain() {
        return userDensoDetailDomain;
    }
    /**
     * <p>Setter method for DENSO user detail domain.</p>
     *
     * @param userDensoDetailDomain Set for DENSO user detail domain.
     */
    public void setUserDensoDetailDomain(UserDensoDetailDomain userDensoDetailDomain) {
        this.userDensoDetailDomain = userDensoDetailDomain;
    }
    /**
     * <p>Getter method for mode.</p>
     *
     * @return the mode.
     */
    public String getMode() {
        return mode;
    }
    /**
     * <p>Setter method for mode.</p>
     *
     * @param mode Set for mode.
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
}