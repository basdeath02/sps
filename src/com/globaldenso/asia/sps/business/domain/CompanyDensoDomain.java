/*
 * ModifyDate Development company Describe 
 * 2014/06/12 Parichat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class CompanyDensoDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class CompanyDensoDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7315029591177127078L;

    /**
     * Company DENSO Code
     */
    private String dCd;

    /**
     * DENSO Company DSC ID
     */
    private String dscIdComCd;

    /**
     * DENSO Company Name
     */
    private String companyName;

    /**
     * CIGMA Schema Code
     */
    private String schemaCd;

    /**
     * DENSO Address 1
     */
    private String address1;

    /**
     * DENSO Address 2
     */
    private String address2;

    /**
     * DENSO Address 3
     */
    private String address3;

    /**
     * DENSO Telephone no.
     */
    private String telephone;

    /**
     * DENSO Fax no.
     */
    private String fax;

    /**
     * Backup status
     */
    private String backupStatus;

    /**
     * Order transaction purge year
     */
    private BigDecimal orderKeepYear;

    /**
     * Invoice transaction purge year
     */
    private BigDecimal invoiceKeepYear;

    /**
     * Announce message file path
     */
    private String filePath;

    /**
     * Temporary file path
     */
    private String tmpPath;

    /**
     * Coordinated Universal Time
     */
    private String utc;

    /**
     * Is active
     */
    private String isActive;

    /**
     * D_TAX_ID
     */
    private String densoTaxId;

    /**
     * Tax code
     */
    private String taxCd;

    /**
     * Currency code
     */
    private String currencyCd;

    /**
     * Group invoice error type
     */
    private String groupInvoiceErrorType;

    /**
     * Group invoice type
     */
    private String groupInvoiceType;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create datetime
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Instantiates a new account domain.
     */
    public CompanyDensoDomain() {
        super();
    }

    /**
     * <p>Getter method for companyDensoCode.</p>
     *
     * @return the companyDensoCode
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for companyDensoCode.</p>
     *
     * @param dCd Set for companyDensoCode
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dscIdComCd.</p>
     *
     * @return the dscIdComCd
     */
    public String getDscIdComCd() {
        return dscIdComCd;
    }

    /**
     * <p>Setter method for dscIdComCd.</p>
     *
     * @param dscIdComCd Set for dscIdComCd
     */
    public void setDscIdComCd(String dscIdComCd) {
        this.dscIdComCd = dscIdComCd;
    }

    /**
     * <p>Getter method for companyName.</p>
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * <p>Setter method for companyName.</p>
     *
     * @param companyName Set for companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * <p>Getter method for schemaCd.</p>
     *
     * @return the schemaCd
     */
    public String getSchemaCd() {
        return schemaCd;
    }

    /**
     * <p>Setter method for schemaCd.</p>
     *
     * @param schemaCd Set for schemaCd
     */
    public void setSchemaCd(String schemaCd) {
        this.schemaCd = schemaCd;
    }

    /**
     * <p>Getter method for address1.</p>
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * <p>Setter method for address1.</p>
     *
     * @param address1 Set for address1
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * <p>Getter method for address2.</p>
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * <p>Setter method for address2.</p>
     *
     * @param address2 Set for address2
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * <p>Getter method for address3.</p>
     *
     * @return the address3
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * <p>Setter method for address3.</p>
     *
     * @param address3 Set for address3
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * <p>Getter method for telephone.</p>
     *
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * <p>Setter method for telephone.</p>
     *
     * @param telephone Set for telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * <p>Getter method for fax.</p>
     *
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * <p>Setter method for fax.</p>
     *
     * @param fax Set for fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * <p>Getter method for backupStatus.</p>
     *
     * @return the backupStatus
     */
    public String getBackupStatus() {
        return backupStatus;
    }

    /**
     * <p>Setter method for backupStatus.</p>
     *
     * @param backupStatus Set for backupStatus
     */
    public void setBackupStatus(String backupStatus) {
        this.backupStatus = backupStatus;
    }

    /**
     * <p>Getter method for orderKeepYear.</p>
     *
     * @return the orderKeepYear
     */
    public BigDecimal getOrderKeepYear() {
        return orderKeepYear;
    }

    /**
     * <p>Setter method for orderKeepYear.</p>
     *
     * @param orderKeepYear Set for orderKeepYear
     */
    public void setOrderKeepYear(BigDecimal orderKeepYear) {
        this.orderKeepYear = orderKeepYear;
    }

    /**
     * <p>Getter method for invoiceKeepYear.</p>
     *
     * @return the invoiceKeepYear
     */
    public BigDecimal getInvoiceKeepYear() {
        return invoiceKeepYear;
    }

    /**
     * <p>Setter method for invoiceKeepYear.</p>
     *
     * @param invoiceKeepYear Set for invoiceKeepYear
     */
    public void setInvoiceKeepYear(BigDecimal invoiceKeepYear) {
        this.invoiceKeepYear = invoiceKeepYear;
    }

    /**
     * <p>Getter method for filePath.</p>
     *
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * <p>Setter method for filePath.</p>
     *
     * @param filePath Set for filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * <p>Getter method for tempPath.</p>
     *
     * @return the tempPath
     */
    public String getTmpPath() {
        return tmpPath;
    }

    /**
     * <p>Setter method for tempPath.</p>
     *
     * @param tmpPath Set for tempPath
     */
    public void setTmpPath(String tmpPath) {
        this.tmpPath = tmpPath;
    }

    /**
     * <p>Getter method for utc.</p>
     *
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * <p>Setter method for utc.</p>
     *
     * @param utc Set for utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * <p>Getter method for isActive.</p>
     *
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * <p>Setter method for isActive.</p>
     *
     * @param isActive Set for isActive
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * <p>Getter method for densoTaxId.</p>
     *
     * @return the densoTaxId
     */
    public String getDensoTaxId() {
        return densoTaxId;
    }

    /**
     * <p>Setter method for densoTaxId.</p>
     *
     * @param densoTaxId Set for densoTaxId
     */
    public void setDensoTaxId(String densoTaxId) {
        this.densoTaxId = densoTaxId;
    }

    /**
     * <p>Getter method for taxCd.</p>
     *
     * @return the taxCd
     */
    public String getTaxCd() {
        return taxCd;
    }

    /**
     * <p>Setter method for taxCd.</p>
     *
     * @param taxCd Set for taxCd
     */
    public void setTaxCd(String taxCd) {
        this.taxCd = taxCd;
    }

    /**
     * <p>Getter method for currencyCd.</p>
     *
     * @return the currencyCd
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * <p>Setter method for currencyCd.</p>
     *
     * @param currencyCd Set for currencyCd
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * <p>Getter method for groupInvoiceErrorType.</p>
     *
     * @return the groupInvoiceErrorType
     */
    public String getGroupInvoiceErrorType() {
        return groupInvoiceErrorType;
    }

    /**
     * <p>Setter method for groupInvoiceErrorType.</p>
     *
     * @param groupInvoiceErrorType Set for groupInvoiceErrorType
     */
    public void setGroupInvoiceErrorType(String groupInvoiceErrorType) {
        this.groupInvoiceErrorType = groupInvoiceErrorType;
    }

    /**
     * <p>Getter method for groupInvoiceType.</p>
     *
     * @return the groupInvoiceType
     */
    public String getGroupInvoiceType() {
        return groupInvoiceType;
    }

    /**
     * <p>Setter method for groupInvoiceType.</p>
     *
     * @param groupInvoiceType Set for groupInvoiceType
     */
    public void setGroupInvoiceType(String groupInvoiceType) {
        this.groupInvoiceType = groupInvoiceType;
    }

    /**
     * <p>Getter method for createDscId.</p>
     *
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * <p>Setter method for createDscId.</p>
     *
     * @param createDscId Set for createDscId
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * <p>Getter method for createDatetime.</p>
     *
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * <p>Setter method for createDatetime.</p>
     *
     * @param createDatetime Set for createDatetime
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * <p>Getter method for lastUpdateDscId.</p>
     *
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>Setter method for lastUpdateDscId.</p>
     *
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>Getter method for lastUpdateDatetime.</p>
     *
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>Setter method for lastUpdateDatetime.</p>
     *
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}