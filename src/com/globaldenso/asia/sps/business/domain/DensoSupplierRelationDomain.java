/*
 * ModifyDate Development company Describe 
 * 2014/07/21 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class DensoSupplierRelationDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class DensoSupplierRelationDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 2686351222114730995L;

    /**
     * company DENSO code
     */
    private String dCd;

    /**
     * company supplier code
     */
    private String sCd;

    /**
     * plant DENSO code
     */
    private String dPcd;

    /**
     * plant supplier code
     */
    private String sPcd;

    /** The default constructor. */
    public DensoSupplierRelationDomain() {
        super();
    }

    /**
     * Get method for dCd.
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Set method for dCd.
     * @param cd the dCd to set
     */
    public void setDCd(String cd) {
        dCd = cd;
    }

    /**
     * Get method for sCd.
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Set method for sCd.
     * @param cd the sCd to set
     */
    public void setSCd(String cd) {
        sCd = cd;
    }

    /**
     * Get method for dPcd.
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Set method for dPcd.
     * @param pcd the dPcd to set
     */
    public void setDPcd(String pcd) {
        dPcd = pcd;
    }

    /**
     * Get method for sPcd.
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Set method for sPcd.
     * @param pcd the sPcd to set
     */
    public void setSPcd(String pcd) {
        sPcd = pcd;
    }

}
