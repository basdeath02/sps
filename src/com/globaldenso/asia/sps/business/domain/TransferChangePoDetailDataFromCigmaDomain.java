package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * <p>TransferPoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferChangePoDetailDataFromCigmaDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 4882871832338378534L;
    /** dPartNo */
    private String dPn;
    /** plannerCode */
    private String plannerCode;
    /** whPrimeReceiving */
    private String whPrimeReceiving;
    /** location */
    private String location;
    /** unitOfMeasure */
    private String unitOfMeasure;
    /** itemDesc */
    private String itemDesc;
    /** model */
    private String model;
    /** Cigma Po Due List */
    private List<TransferChangePoDueDataFromCigmaDomain> cigmaPoDueList;
    /** Supplier Part No */
    private String sPn;
    /** Temp Price Flag */
    private String tmpPriceFlg;
    
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferChangePoDetailDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }
    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    /**
     * <p>Getter method for plannerCode.</p>
     *
     * @return the plannerCode
     */
    public String getPlannerCode() {
        return plannerCode;
    }
    /**
     * <p>Setter method for plannerCode.</p>
     *
     * @param plannerCode Set for plannerCode
     */
    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }
    /**
     * <p>Getter method for whPrimeReceiving.</p>
     *
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }
    /**
     * <p>Setter method for whPrimeReceiving.</p>
     *
     * @param whPrimeReceiving Set for whPrimeReceiving
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }
    /**
     * <p>Getter method for location.</p>
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }
    /**
     * <p>Setter method for location.</p>
     *
     * @param location Set for location
     */
    public void setLocation(String location) {
        this.location = location;
    }
    /**
     * <p>Getter method for unitOfMeasure.</p>
     *
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }
    /**
     * <p>Setter method for unitOfMeasure.</p>
     *
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }
    /**
     * <p>Getter method for itemDesc.</p>
     *
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }
    /**
     * <p>Setter method for itemDesc.</p>
     *
     * @param itemDesc Set for itemDesc
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
    /**
     * <p>Getter method for model.</p>
     *
     * @return the model
     */
    public String getModel() {
        return model;
    }
    /**
     * <p>Setter method for model.</p>
     *
     * @param model Set for model
     */
    public void setModel(String model) {
        this.model = model;
    }
    /**
     * <p>Getter method for cigmaPoDueList.</p>
     *
     * @return the cigmaPoDueList
     */
    public List<TransferChangePoDueDataFromCigmaDomain> getCigmaPoDueList() {
        return cigmaPoDueList;
    }
    /**
     * <p>Setter method for cigmaPoDueList.</p>
     *
     * @param cigmaPoDueList Set for cigmaPoDueList
     */
    public void setCigmaPoDueList(List<TransferChangePoDueDataFromCigmaDomain> cigmaPoDueList) {
        this.cigmaPoDueList = cigmaPoDueList;
    }
    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }
    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    /**
     * <p>Getter method for tmpPriceFlg.</p>
     *
     * @return the tmpPriceFlg
     */
    public String getTmpPriceFlg() {
        return tmpPriceFlg;
    }
    /**
     * <p>Setter method for tmpPriceFlg.</p>
     *
     * @param tmpPriceFlg Set for tmpPriceFlg
     */
    public void setTmpPriceFlg(String tmpPriceFlg) {
        this.tmpPriceFlg = tmpPriceFlg;
    }

}
