/*
 * ModifyDate Development company     Describe 
 * 2014/04/23 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Role Domain.
 * @author CSI
 */
public class RoleDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1354601928735284893L;

    /** The DSC ID. */
    private String roleCode;
       
    /** The first name */
    private String roleName;
    
    /** The Active flag. */
    private String isActive; 
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
            
    /** The Date time update. */
    private Timestamp updateDatetime;
    
    /** The target page. */
    private String targetPage; 
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    
    /**
     * Instantiates a new Role Domain.
     */
    public RoleDomain() {
        super();       
        setTargetPage(Constants.PAGE_FIRST);
    }
       
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Sets the target page.
     * 
     * @param targetPage the target page.
     */  
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * Gets the role Code
     * 
     * @return the role Code
     */  
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the role Code.
     * 
     * @param roleCode the role Code.
     */ 
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    /**
     * Gets the role Name
     * 
     * @return the role Name
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Sets the role Name.
     * 
     * @param roleName the role Name.
     */ 
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Gets the is Active flag.
     * 
     * @return the isActive flag.
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the is Active flag.
     * 
     * @param isActive the is Active flag.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public Timestamp getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(Timestamp updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
}