/*
 * ModifyDate Development company    Describe 
 * 2014/08/1 CSI Chatchai           Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * Kanban Order Information DO Detail Return Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationDoDetailReturnDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serializable.
     * </p>
     */
    private static final long serialVersionUID = -9129725509503350681L;

    /**
     * <p>
     * The Do Id.
     * </p>
     */
    private String doId;

    /**
     * <p>
     * The Sps DO No.
     * </p>
     */
    private String spsDoNo;

    /**
     * <p>
     * The Cigma DO No.
     * </p>
     */
    private String cigmaDoNo;
    
    /**
     * Current CIGMA D/O No.
     */
    private String currentCigmaDoNo;

    /**
     * Current SPS D/O No.
     */
    private String currentSpsDoNo;

    /**
     * Previous SPS D/O No.
     */
    private String previousSpsDoNo;

    /**
     * <p>
     * The Revision.
     * </p>
     */
    private String revision;

    /**
     * <p>
     * The Do Issue Date.
     * </p>
     */
    private Timestamp doIssueDate;

    // FIX : wrong dateformat
    /** The formated D/O Issue date to show in screen */
    private String doIssueDateShow;
    
    /**
     * <p>
     * The Shipment Status.
     * </p>
     */
    private String shipmentStatus;

    /**
     * <p>
     * The Supplier Code.
     * </p>
     */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /**
     * <p>
     * The Supplier Plant Code.
     * </p>
     */
    private String sPcd;

    /**
     * <p>
     * The Denso Code.
     * </p>
     */
    private String dCd;

    /**
     * <p>
     * The Denso Plant Code.
     * </p>
     */
    private String dPcd;

    /**
     * <p>
     * The Tm.
     * </p>
     */
    private String tm;

    /**
     * <p>
     * The Data Type.
     * </p>
     */
    private String dataType;

    /**
     * <p>
     * The Order Method.
     * </p>
     */
    private String orderMethod;

    /**
     * <p>
     * The Ship Date time.
     * </p>
     */
    private Timestamp shipDatetime;
    
    /**
     * <p>
     * The Ship Date time UTC.
     * </p>
     */
    private String shipDatetimeUtc;


    /**
     * <p>
     * The Delivery Date time.
     * </p>
     */
    private Timestamp deliveryDatetime;

    /**
     * <p>
     * The Delivery Date time UTC.
     * </p>
     */
    private String deliveryDatetimeUtc;

    /**
     * <p>
     * The Pdf File Id.
     * </p>
     */
    private String pdfFileId;
    
    /**
     * <p>
     * The pdfFileKbTagPrintDate
     * </p>
     */
    private String pdfFileKbTagPrintDate;

    /**
     * <p>
     * The Truck Route.
     * </p>
     */
    private String truckRoute;

    /**
     * <p>
     * The Del.
     * </p>
     */
    private String del;

    /**
     * <p>
     * The Po Id.
     * </p>
     */
    private String poId;

    /**
     * <p>
     * The WhPrimeReceiving.
     * </p>
     */
    private String whPrimeReceiving;

    /**
     * <p>
     * The WhLocation.
     * </p>
     */
    private String whLocation;

    /**
     * <p>
     * The Last Revision Flg.
     * </p>
     */
    private String lastRevisionFlg;

    /**
     * <p>
     * The Urgent Order Flg.
     * </p>
     */
    private String urgentOrderFlg;

    /**
     * <p>
     * The Clear Urgent Date.
     * </p>
     */
    private Timestamp clearUrgentDate;

    /**
     * <p>
     * The New Truck Route Flg.
     * </p>
     */
    private String newTruckRouteFlg;

    /**
     * <p>
     * The Create Dsc Id.
     * </p>
     */
    private String createDscId;

    /**
     * <p>
     * The Create Date time.
     * </p>
     */
    private Timestamp createDatetime;

    /**
     * <p>
     * The Last Update Dsc Id.
     * </p>
     */
    private String lastUpdateDscId;

    /**
     * <p>
     * The Last Update Date time.
     * </p>
     */
    private Timestamp lastUpdateDatetime;

    /**
     * <p>
     * The Dock Code.
     * </p>
     */
    private String dockCode;

    /**
     * <p>
     * The Cycle.
     * </p>
     */
    private String cycle;

    /**
     * <p>
     * The Trip No.
     * </p>
     */
    private String tripNo;

    /**
     * <p>
     * The Back log.
     * </p>
     */
    private String backlog;

    /**
     * <p>
     * The Receiving Gate.
     * </p>
     */
    private String receivingGate;

    /**
     * <p>
     * The Utc.
     * </p>
     */
    private String utc;
    
    /** 
     * The receiveByScan. 
     */
    private String receiveByScan; 
    
    /** 
     * The oneWayKanbanTagFlag. 
     */
    private String oneWayKanbanTagFlag; 
    
    /** 
     * The tagOutput. 
     */
    private String tagOutput;
    
    /** 
     * The tagOutput. 
     */
    private Integer partTag;
    
    /**
     * <p>
     * The shippingTotal
     * </p>
     */
    private BigDecimal shippingTotal;
    
    /**
     * <p>
     * The shipmentStatus
     * </p>
     */
    private String receiveStatus; 

    /**
     * <p>
     * The Kanban Order Information DO Detail List Return Domain List.
     * </p>
     */
    private List<KanbanOrderInformationDoDetailListReturnDomain>
    kanbanOrderInformationDoDetailListReturnDomain;

    /**
     * <p>
     * the constructor.
     * </p>
     */
    public KanbanOrderInformationDoDetailReturnDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for doId.
     * </p>
     * 
     * @return the doId
     */
    public String getDoId() {
        return doId;
    }

    /**
     * <p>
     * Setter method for doId.
     * </p>
     * 
     * @param doId Set for doId
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * <p>
     * Getter method for spsDoNo.
     * </p>
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>
     * Setter method for spsDoNo.
     * </p>
     * 
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>
     * Getter method for cigmaDoNo.
     * </p>
     * 
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>
     * Setter method for cigmaDoNo.
     * </p>
     * 
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }
    
    /**
     * Getter method of "currentCigmaDoNo"
     * 
     * @return the currentCigmaDoNo
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * Setter method of "currentCigmaDoNo"
     * 
     * @param currentCigmaDoNo Set in "currentCigmaDoNo".
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>
     * Getter method for doIssueDate.
     * </p>
     * 
     * @return the doIssueDate
     */
    public Timestamp getDoIssueDate() {
        return doIssueDate;
    }

    /**
     * <p>
     * Setter method for doIssueDate.
     * </p>
     * 
     * @param doIssueDate Set for doIssueDate
     */
    public void setDoIssueDate(Timestamp doIssueDate) {
        this.doIssueDate = doIssueDate;
    }

    /**
     * <p>
     * Getter method for shipmentStatus.
     * </p>
     * 
     * @return the shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * <p>
     * Setter method for shipmentStatus.
     * </p>
     * 
     * @param shipmentStatus Set for shipmentStatus
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for dCd.
     * </p>
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for dCd.
     * </p>
     * 
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for tm.
     * </p>
     * 
     * @return the tm
     */
    public String getTm() {
        return tm;
    }

    /**
     * <p>
     * Setter method for tm.
     * </p>
     * 
     * @param tm Set for tm
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * <p>
     * Getter method for dataType.
     * </p>
     * 
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * <p>
     * Setter method for dataType.
     * </p>
     * 
     * @param dataType Set for dataType
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * <p>
     * Getter method for orderMethod.
     * </p>
     * 
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * <p>
     * Setter method for orderMethod.
     * </p>
     * 
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * <p>
     * Getter method for shipDatetime.
     * </p>
     * 
     * @return the shipDatetime
     */
    public Timestamp getShipDatetime() {
        return shipDatetime;
    }

    /**
     * <p>
     * Setter method for shipDatetime.
     * </p>
     * 
     * @param shipDatetime Set for shipDatetime
     */
    public void setShipDatetime(Timestamp shipDatetime) {
        this.shipDatetime = shipDatetime;
    }
    
    /**
     * <p>
     * Getter method for shipDatetimeUtc.
     * </p>
     * 
     * @return the shipDatetimeUtc
     */
    public String getShipDatetimeUtc() {
        return shipDatetimeUtc;
    }

    /**
     * <p>
     * Setter method for shipDatetimeUtc.
     * </p>
     * 
     * @param shipDatetimeUtc Set for shipDatetimeUtc
     */
    public void setShipDatetimeUtc(String shipDatetimeUtc) {
        this.shipDatetimeUtc = shipDatetimeUtc;
    }

    /**
     * <p>
     * Getter method for deliveryDatetime.
     * </p>
     * 
     * @return the deliveryDatetime
     */
    public Timestamp getDeliveryDatetime() {
        return deliveryDatetime;
    }

    /**
     * <p>
     * Setter method for deliveryDatetime.
     * </p>
     * 
     * @param deliveryDatetime Set for deliveryDatetime
     */
    public void setDeliveryDatetime(Timestamp deliveryDatetime) {
        this.deliveryDatetime = deliveryDatetime;
    }

    /**
     * <p>
     * Getter method for pdfFileId.
     * </p>
     * 
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>
     * Setter method for pdfFileId.
     * </p>
     * 
     * @param pdfFileId Set for pdfFileId
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * <p>
     * Getter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @return The pdfFileKbTagPrintDate
     */
    public String getPdfFileKbTagPrintDate() {
        return pdfFileKbTagPrintDate;
    }

    /**
     * <p>
     * Setter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @param pdfFileKbTagPrintDate Set for pdfFileKbTagPrintDate
     */
    public void setPdfFileKbTagPrintDate(String pdfFileKbTagPrintDate) {
        this.pdfFileKbTagPrintDate = pdfFileKbTagPrintDate;
    }

    /**
     * <p>
     * Getter method for truckRoute.
     * </p>
     * 
     * @return the truckRoute
     */
    public String getTruckRoute() {
        return truckRoute;
    }

    /**
     * <p>
     * Setter method for truckRoute.
     * </p>
     * 
     * @param truckRoute Set for truckRoute
     */
    public void setTruckRoute(String truckRoute) {
        this.truckRoute = truckRoute;
    }

    /**
     * <p>
     * Getter method for del.
     * </p>
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * <p>
     * Setter method for del.
     * </p>
     * 
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * <p>
     * Getter method for poId.
     * </p>
     * 
     * @return the poId
     */
    public String getPoId() {
        return poId;
    }

    /**
     * <p>
     * Setter method for poId.
     * </p>
     * 
     * @param poId Set for poId
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }

    /**
     * <p>
     * Getter method for whPrimeReceiving.
     * </p>
     * 
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * <p>
     * Setter method for whPrimeReceiving.
     * </p>
     * 
     * @param whPrimeReceiving Set for whPrimeReceiving
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * <p>
     * Getter method for whLocation.
     * </p>
     * 
     * @return the whLocation
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * <p>
     * Setter method for whLocation.
     * </p>
     * 
     * @param whLocation Set for whLocation
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }

    /**
     * <p>
     * Getter method for lastRevisionFlg.
     * </p>
     * 
     * @return the lastRevisionFlg
     */
    public String getLastRevisionFlg() {
        return lastRevisionFlg;
    }

    /**
     * <p>
     * Setter method for lastRevisionFlg.
     * </p>
     * 
     * @param lastRevisionFlg Set for lastRevisionFlg
     */
    public void setLastRevisionFlg(String lastRevisionFlg) {
        this.lastRevisionFlg = lastRevisionFlg;
    }

    /**
     * <p>
     * Getter method for urgentOrderFlg.
     * </p>
     * 
     * @return the urgentOrderFlg
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * <p>
     * Setter method for urgentOrderFlg.
     * </p>
     * 
     * @param urgentOrderFlg Set for urgentOrderFlg
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * <p>
     * Getter method for clearUrgentDate.
     * </p>
     * 
     * @return the clearUrgentDate
     */
    public Timestamp getClearUrgentDate() {
        return clearUrgentDate;
    }

    /**
     * <p>
     * Setter method for clearUrgentDate.
     * </p>
     * 
     * @param clearUrgentDate Set for clearUrgentDate
     */
    public void setClearUrgentDate(Timestamp clearUrgentDate) {
        this.clearUrgentDate = clearUrgentDate;
    }

    /**
     * <p>
     * Getter method for newTruckRouteFlg.
     * </p>
     * 
     * @return the newTruckRouteFlg
     */
    public String getNewTruckRouteFlg() {
        return newTruckRouteFlg;
    }

    /**
     * <p>
     * Setter method for newTruckRouteFlg.
     * </p>
     * 
     * @param newTruckRouteFlg Set for newTruckRouteFlg
     */
    public void setNewTruckRouteFlg(String newTruckRouteFlg) {
        this.newTruckRouteFlg = newTruckRouteFlg;
    }

    /**
     * <p>
     * Getter method for createDscId.
     * </p>
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * <p>
     * Setter method for createDscId.
     * </p>
     * 
     * @param createDscId Set for createDscId
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * <p>
     * Getter method for createDatetime.
     * </p>
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * <p>
     * Setter method for createDatetime.
     * </p>
     * 
     * @param createDatetime Set for createDatetime
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * <p>
     * Getter method for lastUpdateDscId.
     * </p>
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>
     * Setter method for lastUpdateDscId.
     * </p>
     * 
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>
     * Getter method for lastUpdateDatetime.
     * </p>
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>
     * Setter method for lastUpdateDatetime.
     * </p>
     * 
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>
     * Getter method for dockCode.
     * </p>
     * 
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * <p>
     * Setter method for dockCode.
     * </p>
     * 
     * @param dockCode Set for dockCode
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * <p>
     * Getter method for cycle.
     * </p>
     * 
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * <p>
     * Setter method for cycle.
     * </p>
     * 
     * @param cycle Set for cycle
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * <p>
     * Getter method for tripNo.
     * </p>
     * 
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * <p>
     * Setter method for tripNo.
     * </p>
     * 
     * @param tripNo Set for tripNo
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * <p>
     * Getter method for backlog.
     * </p>
     * 
     * @return the backlog
     */
    public String getBacklog() {
        return backlog;
    }

    /**
     * <p>
     * Setter method for backlog.
     * </p>
     * 
     * @param backlog Set for backlog
     */
    public void setBacklog(String backlog) {
        this.backlog = backlog;
    }

    /**
     * <p>
     * Getter method for receivingGate.
     * </p>
     * 
     * @return the receivingGate
     */
    public String getReceivingGate() {
        return receivingGate;
    }

    /**
     * <p>
     * Setter method for receivingGate.
     * </p>
     * 
     * @param receivingGate Set for receivingGate
     */
    public void setReceivingGate(String receivingGate) {
        this.receivingGate = receivingGate;
    }

    /**
     * <p>
     * Getter method for utc.
     * </p>
     * 
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * <p>
     * Setter method for utc.
     * </p>
     * 
     * @param utc Set for utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * <p>
     * Getter method for kanbanOrderInformationDoDetailListReturnDomain.
     * </p>
     * 
     * @return the kanbanOrderInformationDoDetailListReturnDomain
     */
    public List<KanbanOrderInformationDoDetailListReturnDomain>
    getKanbanOrderInformationDoDetailListReturnDomain()
    {
        return kanbanOrderInformationDoDetailListReturnDomain;
    }

    /**
     * <p>
     * Setter method for kanbanOrderInformationDoDetailListReturnDomain.
     * </p>
     * 
     * @param kanbanOrderInformationDoDetailListReturnDomain Set for kanbanOrderInformationDoDetailListReturnDomain
     */
    public void setKanbanOrderInformationDoDetailListReturnDomain(
        List<KanbanOrderInformationDoDetailListReturnDomain>
        kanbanOrderInformationDoDetailListReturnDomain)
    {
        this.kanbanOrderInformationDoDetailListReturnDomain
            = kanbanOrderInformationDoDetailListReturnDomain;
    }

    /**
     * <p>
     * Getter method for deliveryDatetimeUtc.
     * </p>
     * 
     * @return the deliveryDatetimeUtc
     */
    public String getDeliveryDatetimeUtc() {
        return deliveryDatetimeUtc;
    }

    /**
     * <p>
     * Setter method for deliveryDatetimeUtc.
     * </p>
     * 
     * @param deliveryDatetimeUtc Set for deliveryDatetimeUtc
     */
    public void setDeliveryDatetimeUtc(String deliveryDatetimeUtc) {
        this.deliveryDatetimeUtc = deliveryDatetimeUtc;
    }

    /**
     * <p>Getter method for currentSpsDoNo.</p>
     *
     * @return the currentSpsDoNo
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }

    /**
     * <p>Setter method for currentSpsDoNo.</p>
     *
     * @param currentSpsDoNo Set for currentSpsDoNo
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }

    /**
     * <p>Getter method for previousSpsDoNo.</p>
     *
     * @return the previousSpsDoNo
     */
    public String getPreviousSpsDoNo() {
        return previousSpsDoNo;
    }

    /**
     * <p>Setter method for previousSpsDoNo.</p>
     *
     * @param previousSpsDoNo Set for previousSpsDoNo
     */
    public void setPreviousSpsDoNo(String previousSpsDoNo) {
        this.previousSpsDoNo = previousSpsDoNo;
    }

    /**
     * <p>Getter method for doIssueDateShow.</p>
     *
     * @return the doIssueDateShow
     */
    public String getDoIssueDateShow() {
        return doIssueDateShow;
    }

    /**
     * <p>Setter method for doIssueDateShow.</p>
     *
     * @param doIssueDateShow Set for doIssueDateShow
     */
    public void setDoIssueDateShow(String doIssueDateShow) {
        this.doIssueDateShow = doIssueDateShow;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    /**
     * Gets the oneWayKanbanTagFlag.
     * 
     * @return the oneWayKanbanTagFlag
     */

    public String getOneWayKanbanTagFlag() {
        return oneWayKanbanTagFlag;
    }

    /**
     * Sets the oneWayKanbanTagFlag.
     * 
     * @param oneWayKanbanTagFlag the oneWayKanbanTagFlag
     */

    public void setOneWayKanbanTagFlag(String oneWayKanbanTagFlag) {
        this.oneWayKanbanTagFlag = oneWayKanbanTagFlag;
    }

    /**
     * <p>Getter method for tagOutput.</p>
     *
     * @return the tagOutput
     */
    public String getTagOutput() {
        return tagOutput;
    }

    /**
     * <p>Setter method for tagOutput.</p>
     *
     * @param tagOutput Set for tagOutput
     */
    public void setTagOutput(String tagOutput) {
        this.tagOutput = tagOutput;
    }

    /**
     * <p>Getter method for partTag.</p>
     *
     * @return the partTag
     */
    public Integer getPartTag() {
        return partTag;
    }

    /**
     * <p>Setter method for partTag.</p>
     *
     * @param partTag Set for partTag
     */
    public void setPartTag(Integer partTag) {
        this.partTag = partTag;
    }

    /**
     * <p>Getter method for shippingTotal.</p>
     *
     * @return the shippingTotal
     */
    public BigDecimal getShippingTotal() {
        return shippingTotal;
    }

    /**
     * <p>Setter method for shippingTotal.</p>
     *
     * @param shippingTotal Set for shippingTotal
     */
    public void setShippingTotal(BigDecimal shippingTotal) {
        this.shippingTotal = shippingTotal;
    }

    /**
     * <p>Getter method for receiveStatus.</p>
     *
     * @return the receiveStatus
     */
    public String getReceiveStatus() {
        return receiveStatus;
    }

    /**
     * <p>Setter method for receiveStatus.</p>
     *
     * @param receiveStatus Set for receiveStatus
     */
    public void setReceiveStatus(String receiveStatus) {
        this.receiveStatus = receiveStatus;
    }

}
