/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The Class Delivery Order Detail Domain.
 * @author CSI
 */
public class DeliveryOrderDetailDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4213338318059942862L;
    
    /** The Delivery order sub detail domain. */
    private DeliveryOrderSubDetailDomain deliveryOrderSubDetailDomain;

    /** The Delivery order do detail domain. */
    private List<DeliveryOrderDoDetailDomain> deliveryOrderDoDetailDomain;

    /**
     * Instantiates a new delivery order detail domain.
     */
    public DeliveryOrderDetailDomain() {
        super();
    }

    /**
     * Gets the delivery order sub detail domain.
     * 
     * @return the delivery order sub detail domain
     */
    public DeliveryOrderSubDetailDomain getDeliveryOrderSubDetailDomain() {
        return deliveryOrderSubDetailDomain;
    }

    /**
     * Sets the delivery order sub detail domain.
     * 
     * @param deliveryOrderSubDetailDomain the delivery order sub detail domain
     */
    public void setDeliveryOrderSubDetailDomain(
        DeliveryOrderSubDetailDomain deliveryOrderSubDetailDomain) {
        this.deliveryOrderSubDetailDomain = deliveryOrderSubDetailDomain;
    }

    /**
     * Gets the delivery order do detail domain.
     * 
     * @return the delivery order do detail domain
     */
    public List<DeliveryOrderDoDetailDomain> getDeliveryOrderDoDetailDomain() {
        return deliveryOrderDoDetailDomain;
    }

    /**
     * Sets the delivery order do detail domain.
     * 
     * @param deliveryOrderDoDetailDomain the delivery order do detail domain
     */
    public void setDeliveryOrderDoDetailDomain(
        List<DeliveryOrderDoDetailDomain> deliveryOrderDoDetailDomain) {
        this.deliveryOrderDoDetailDomain = deliveryOrderDoDetailDomain;
    }

}
