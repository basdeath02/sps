/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * <p>
 * Kanban Order Information Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serializable.
     * </p>
     */
    private static final long serialVersionUID = 2290320735862945897L;

    /** The List of Denso Supplier Relation. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;

    /** The do Id. */
    private String doId;

    /** The Supplier Code. */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /** The Supplier Plant Code. */
    private String sPcd;

    /** The Denso Code. */
    private String dCd;

    /** The Denso Plant Code. */
    private String dPcd;

    /** The Issue Date From. */
    private String issueDateFrom;

    /** The issue Date To. */
    private String issueDateTo;

    /** The Delivery Date From. */
    private String deliveryDateFrom;

    /** The Delivery Date To. */
    private String deliveryDateTo;

    /** The delivery time from. */
    private String deliveryTimeFrom;
    
    /** The delivery time to */
    private String deliveryTimeTo;
    
    /** The Ship Date From. */
    private String shipDateFrom;

    /** The Ship Date To. */
    private String shipDateTo;

    /** The Revision. */
    private String revision;

    /** The Shipment Status. */
    private String shipmentStatus;

    /** The Transport Mode. */
    private String transportMode;

    /** The Route No. */
    private String routeNo;

    /** The Del. */
    private String del;

    /** The Sps DO No. */
    private String spsDoNo;

    /** The Cigma DO No. */
    private String cigmaDoNo;

    /** The Supplier Code List. */
    private List<SpsMCompanySupplierDomain> sCdList;

    /** The Supplier Plant Code List. */
    private List<SpsMPlantSupplierDomain> sPcdList;

    /** The Denso Code List. */
    private List<SpsMCompanyDensoDomain> dCdList;

    /** The Denso Plant Code List. */
    private List<SpsMPlantDensoDomain> dPcdList;

    /** The Revision List. */
    private List<MiscellaneousDomain> revisionList;

    /** The Shipment Status List. */
    private List<MiscellaneousDomain> shipmentStatusList;

    /** The Transport Mode List. */
    private List<MiscellaneousDomain> transportModeList;

    /** The List of Company Supplier. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The List of Plant Supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of Plant DENSO. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of Company DENSO. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The misc_Revision. */
    private List<MiscellaneousDomain> miscRevision;

    /** The misc_ShipmentStatusCB. */
    private List<MiscellaneousDomain> miscShipmentStatusCb;

    /** The TransportModeCB. */
    private List<MiscellaneousDomain> transportModeCb;

    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The pdf File Id. */
    private String pdfFileId;

    /** The pdf file name. */
    private String pdfFileName;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public KanbanOrderInformationDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for densoSupplierRelationList.
     * </p>
     * 
     * @return the densoSupplierRelationList
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationList.
     * </p>
     * 
     * @param densoSupplierRelationList Set for densoSupplierRelationList
     */
    public void setDensoSupplierRelationList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for dCd.
     * </p>
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for dCd.
     * </p>
     * 
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for issueDateFrom.
     * </p>
     * 
     * @return the issueDateFrom
     */
    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    /**
     * <p>
     * Setter method for issueDateFrom.
     * </p>
     * 
     * @param issueDateFrom Set for issueDateFrom
     */
    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    /**
     * <p>
     * Getter method for issueDateTo.
     * </p>
     * 
     * @return the issueDateTo
     */
    public String getIssueDateTo() {
        return issueDateTo;
    }

    /**
     * <p>
     * Setter method for issueDateTo.
     * </p>
     * 
     * @param issueDateTo Set for issueDateTo
     */
    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    /**
     * <p>
     * Getter method for deliveryDateFrom.
     * </p>
     * 
     * @return the deliveryDateFrom
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * <p>
     * Setter method for deliveryDateFrom.
     * </p>
     * 
     * @param deliveryDateFrom Set for deliveryDateFrom
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * <p>
     * Getter method for deliveryDateTo.
     * </p>
     * 
     * @return the deliveryDateTo
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * <p>
     * Setter method for deliveryDateTo.
     * </p>
     * 
     * @param deliveryDateTo Set for deliveryDateTo
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }

    /**
     * <p>
     * Getter method for shipDateFrom.
     * </p>
     * 
     * @return the shipDateFrom
     */
    public String getShipDateFrom() {
        return shipDateFrom;
    }

    /**
     * <p>
     * Setter method for shipDateFrom.
     * </p>
     * 
     * @param shipDateFrom Set for shipDateFrom
     */
    public void setShipDateFrom(String shipDateFrom) {
        this.shipDateFrom = shipDateFrom;
    }

    /**
     * <p>
     * Getter method for shipDateTo.
     * </p>
     * 
     * @return the shipDateTo
     */
    public String getShipDateTo() {
        return shipDateTo;
    }

    /**
     * <p>
     * Setter method for shipDateTo.
     * </p>
     * 
     * @param shipDateTo Set for shipDateTo
     */
    public void setShipDateTo(String shipDateTo) {
        this.shipDateTo = shipDateTo;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>
     * Getter method for shipmentStatus.
     * </p>
     * 
     * @return the shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * <p>
     * Setter method for shipmentStatus.
     * </p>
     * 
     * @param shipmentStatus Set for shipmentStatus
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * <p>
     * Getter method for transportMode.
     * </p>
     * 
     * @return the transportMode
     */
    public String getTransportMode() {
        return transportMode;
    }

    /**
     * <p>
     * Setter method for transportMode.
     * </p>
     * 
     * @param transportMode Set for transportMode
     */
    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    /**
     * <p>
     * Getter method for routeNo.
     * </p>
     * 
     * @return the routeNo
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * <p>
     * Setter method for routeNo.
     * </p>
     * 
     * @param routeNo Set for routeNo
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * <p>
     * Getter method for del.
     * </p>
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * <p>
     * Setter method for del.
     * </p>
     * 
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * <p>
     * Getter method for spsDoNo.
     * </p>
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>
     * Setter method for spsDoNo.
     * </p>
     * 
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>
     * Getter method for cigmaDoNo.
     * </p>
     * 
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>
     * Setter method for cigmaDoNo.
     * </p>
     * 
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>
     * Getter method for sCdList.
     * </p>
     * 
     * @return the sCdList
     */
    public List<SpsMCompanySupplierDomain> getSCdList() {
        return sCdList;
    }

    /**
     * <p>
     * Setter method for sCdList.
     * </p>
     * 
     * @param sCdList Set for sCdList
     */
    public void setSCdList(
        List<SpsMCompanySupplierDomain> sCdList) {
        this.sCdList = sCdList;
    }

    /**
     * <p>
     * Getter method for sPcdList.
     * </p>
     * 
     * @return the sPcdList
     */
    public List<SpsMPlantSupplierDomain> getSPcdList() {
        return sPcdList;
    }

    /**
     * <p>
     * Setter method for sPcdList.
     * </p>
     * 
     * @param sPcdList Set for sPcdList
     */
    public void setSPcdList(
        List<SpsMPlantSupplierDomain> sPcdList) {
        this.sPcdList = sPcdList;
    }

    /**
     * <p>
     * Getter method for dCdList.
     * </p>
     * 
     * @return the dCdList
     */
    public List<SpsMCompanyDensoDomain> getDCdList() {
        return dCdList;
    }

    /**
     * <p>
     * Setter method for dCdList.
     * </p>
     * 
     * @param dCdList Set for dCdList
     */
    public void setDCdList(List<SpsMCompanyDensoDomain> dCdList) {
        this.dCdList = dCdList;
    }

    /**
     * <p>
     * Getter method for dPcdList.
     * </p>
     * 
     * @return the dPcdList
     */
    public List<SpsMPlantDensoDomain> getDPcdList() {
        return dPcdList;
    }

    /**
     * <p>
     * Setter method for dPcdList.
     * </p>
     * 
     * @param dPcdList Set for dPcdList
     */
    public void setDPcdList(
        List<SpsMPlantDensoDomain> dPcdList) {
        this.dPcdList = dPcdList;
    }

    /**
     * <p>
     * Getter method for revisionList.
     * </p>
     * 
     * @return the revisionList
     */
    public List<MiscellaneousDomain> getRevisionList() {
        return revisionList;
    }

    /**
     * <p>
     * Setter method for revisionList.
     * </p>
     * 
     * @param revisionList Set for revisionList
     */
    public void setRevisionList(List<MiscellaneousDomain> revisionList) {
        this.revisionList = revisionList;
    }

    /**
     * <p>
     * Getter method for shipmentStatusList.
     * </p>
     * 
     * @return the shipmentStatusList
     */
    public List<MiscellaneousDomain> getShipmentStatusList() {
        return shipmentStatusList;
    }

    /**
     * <p>
     * Setter method for shipmentStatusList.
     * </p>
     * 
     * @param shipmentStatusList Set for shipmentStatusList
     */
    public void setShipmentStatusList(List<MiscellaneousDomain> shipmentStatusList) {
        this.shipmentStatusList = shipmentStatusList;
    }

    /**
     * <p>
     * Getter method for transportModeList.
     * </p>
     * 
     * @return the transportModeList
     */
    public List<MiscellaneousDomain> getTransportModeList() {
        return transportModeList;
    }

    /**
     * <p>
     * Setter method for transportModeList.
     * </p>
     * 
     * @param transportModeList Set for transportModeList
     */
    public void setTransportModeList(List<MiscellaneousDomain> transportModeList) {
        this.transportModeList = transportModeList;
    }

    /**
     * <p>
     * Getter method for miscRevision.
     * </p>
     * 
     * @return the miscRevision
     */
    public List<MiscellaneousDomain> getMiscRevision() {
        return miscRevision;
    }

    /**
     * <p>
     * Setter method for miscRevision.
     * </p>
     * 
     * @param miscRevision Set for miscRevision
     */
    public void setMiscRevision(List<MiscellaneousDomain> miscRevision) {
        this.miscRevision = miscRevision;
    }

    /**
     * <p>
     * Getter method for miscShipmentStatusCb.
     * </p>
     * 
     * @return the miscShipmentStatusCb
     */
    public List<MiscellaneousDomain> getMiscShipmentStatusCb() {
        return miscShipmentStatusCb;
    }

    /**
     * <p>
     * Setter method for miscShipmentStatusCb.
     * </p>
     * 
     * @param miscShipmentStatusCb Set for miscShipmentStatusCb
     */
    public void setMiscShipmentStatusCb(List<MiscellaneousDomain> miscShipmentStatusCb) {
        this.miscShipmentStatusCb = miscShipmentStatusCb;
    }

    /**
     * <p>
     * Getter method for transportModeCb.
     * </p>
     * 
     * @return the transportModeCb
     */
    public List<MiscellaneousDomain> getTransportModeCb() {
        return transportModeCb;
    }

    /**
     * <p>
     * Setter method for transportModeCb.
     * </p>
     * 
     * @param transportModeCb Set for transportModeCb
     */
    public void setTransportModeCb(List<MiscellaneousDomain> transportModeCb) {
        this.transportModeCb = transportModeCb;
    }

    /**
     * <p>
     * Getter method for pdfFileId.
     * </p>
     * 
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>
     * Setter method for pdfFileId.
     * </p>
     * 
     * @param pdfFileId Set for pdfFileId
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * <p>
     * Getter method for pdfFileName.
     * </p>
     * 
     * @return the pdfFileName
     */
    public String getPdfFileName() {
        return pdfFileName;
    }

    /**
     * <p>
     * Setter method for pdfFileName.
     * </p>
     * 
     * @param pdfFileName Set for pdfFileName
     */
    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    /**
     * <p>Getter method for deliveryTimeFrom.</p>
     *
     * @return the deliveryTimeFrom
     */
    public String getDeliveryTimeFrom() {
        return deliveryTimeFrom;
    }

    /**
     * <p>Setter method for deliveryTimeFrom.</p>
     *
     * @param deliveryTimeFrom Set for deliveryTimeFrom
     */
    public void setDeliveryTimeFrom(String deliveryTimeFrom) {
        this.deliveryTimeFrom = deliveryTimeFrom;
    }

    /**
     * <p>Getter method for deliveryTimeTo.</p>
     *
     * @return the deliveryTimeTo
     */
    public String getDeliveryTimeTo() {
        return deliveryTimeTo;
    }

    /**
     * <p>Setter method for deliveryTimeTo.</p>
     *
     * @param deliveryTimeTo Set for deliveryTimeTo
     */
    public void setDeliveryTimeTo(String deliveryTimeTo) {
        this.deliveryTimeTo = deliveryTimeTo;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the doId.
     * 
     * @param doId
     * @return the doId.
     */
    public String getDoId() {
        return doId;
    }

    /**
     * Sets doId
     * 
     * @param doId the doId.
     */

    public void setDoId(String doId) {
        this.doId = doId;
    }

}
