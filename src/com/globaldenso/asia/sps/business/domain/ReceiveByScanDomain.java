/*
 * ModifyDate Development company    Describe 
 * 2017/08/17 Netband U.Rungsiwut    Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Class Receive By Scan Information Domain.
 * @author Netband
 */
public class ReceiveByScanDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 303558568853491292L;
    
    /** The asnNo. */
    private String asnNo;
    
    /** The rcvLane. */
    private String rcvLane;
    
    /** The densoCompanyCode. */
    private String densoCompanyCode;
    
    /** The cigmaDoNo. */
    private String cigmaDoNo;
    
    /** The dPn. */
    private String dPn;
    
    /** The kanbanTagSeq. */
    private String kanbanTagSeq;
    
    /** The supplierCode. */
    private String supplierCode;
    
    /** The densoPlantCode. */
    private String densoPlantCode;
    
    /** The wh. */
    private String wh;
    
    /** The kanbanQty. */
    private String kanbanQty;
    
    /** The allocateQty. */
    private String allocateQty;
    
    /** The kanbanType. */
    private String kanbanType;
    
    /** The asnStatus. */
    private String asnStatus;
    
    /** The tagScanDate. */
    private String tagScanDate;
    
    /** The receiveDate. */
    private String receiveDate;
    
    /** The dscId. */
    private String dscId;
    
    /** The deviceId. */
    private String deviceId;
    
    /** The as400 server connection information. */
    private As400ServerConnectionInformationDomain as400ServerInformation;
    
    /** The batchNo. */
    private String batchNo;
    
    /** The textSeqNo. */
    private String textSeqNo;
    
    /** The numOfText. */
    private String numOfText;
    
    /** The serverName. */
    private String serverName;
    
    /** The transmitionDatetime. */
    private Timestamp transmitionDatetime;
    
    /** The asnReceiveDatetime. */
    private Timestamp asnReceiveDatetime;
    
    /** The receiveDataMsg. */
    private String receiveDataMsg;
    
    /** The receiveDatetime. */
    private Timestamp receiveDatetime;

    /** The tagScanDateTime. */
    private Timestamp tagScanDateTime;
    
    /** The asnScanDateTime. */
    private String asnScanDateTime;
    
     /** Instantiates a new Receive By Scan information domain.
     */
    public ReceiveByScanDomain() {
        super();
    }
    
    /**
     * Gets the as400 server information.
     * 
     * @return the as400 server information
     */
    public As400ServerConnectionInformationDomain getAs400ServerInformation() {
        return as400ServerInformation;
    }

    /**
     * Sets the as400 server information.
     * 
     * @param as400ServerInformation the as400 server information
     */
    public void setAs400ServerInformation(
        As400ServerConnectionInformationDomain as400ServerInformation) {
        this.as400ServerInformation = as400ServerInformation;
    }

    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * <p>Getter method for rcvLane.</p>
     *
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>Setter method for rcvLane.</p>
     *
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>Getter method for densoCompanyCode.</p>
     *
     * @return the densoCompanyCode
     */
    public String getDensoCompanyCode() {
        return densoCompanyCode;
    }

    /**
     * <p>Setter method for densoCompanyCode.</p>
     *
     * @param densoCompanyCode Set for densoCompanyCode
     */
    public void setDensoCompanyCode(String densoCompanyCode) {
        this.densoCompanyCode = densoCompanyCode;
    }

    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getdPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setdPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for kanbanTagSeq.</p>
     *
     * @return the kanbanTagSeq
     */
    public String getKanbanTagSeq() {
        return kanbanTagSeq;
    }

    /**
     * <p>Setter method for kanbanTagSeq.</p>
     *
     * @param kanbanTagSeq Set for kanbanTagSeq
     */
    public void setKanbanTagSeq(String kanbanTagSeq) {
        this.kanbanTagSeq = kanbanTagSeq;
    }

    /**
     * <p>Getter method for supplierCode.</p>
     *
     * @return the supplierCode
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * <p>Setter method for supplierCode.</p>
     *
     * @param supplierCode Set for supplierCode
     */
    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    /**
     * <p>Getter method for densoPlantCode.</p>
     *
     * @return the densoPlantCode
     */
    public String getDensoPlantCode() {
        return densoPlantCode;
    }

    /**
     * <p>Setter method for densoPlantCode.</p>
     *
     * @param densoPlantCode Set for densoPlantCode
     */
    public void setDensoPlantCode(String densoPlantCode) {
        this.densoPlantCode = densoPlantCode;
    }

    /**
     * <p>Getter method for wh.</p>
     *
     * @return the wh
     */
    public String getWh() {
        return wh;
    }

    /**
     * <p>Setter method for wh.</p>
     *
     * @param wh Set for wh
     */
    public void setWh(String wh) {
        this.wh = wh;
    }

    /**
     * <p>Getter method for kanbanQty.</p>
     *
     * @return the kanbanQty
     */
    public String getKanbanQty() {
        return kanbanQty;
    }

    /**
     * <p>Setter method for kanbanQty.</p>
     *
     * @param kanbanQty Set for kanbanQty
     */
    public void setKanbanQty(String kanbanQty) {
        this.kanbanQty = kanbanQty;
    }

    /**
     * <p>Getter method for allocateQty.</p>
     *
     * @return the allocateQty
     */
    public String getAllocateQty() {
        return allocateQty;
    }

    /**
     * <p>Setter method for allocateQty.</p>
     *
     * @param allocateQty Set for allocateQty
     */
    public void setAllocateQty(String allocateQty) {
        this.allocateQty = allocateQty;
    }

    /**
     * <p>Getter method for kanbanType.</p>
     *
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }

    /**
     * <p>Setter method for kanbanType.</p>
     *
     * @param kanbanType Set for kanbanType
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }

    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * <p>Getter method for tagScanDateTime.</p>
     *
     * @return the tagScanDateTime
     */
    public Timestamp getTagScanDateTime() {
        return tagScanDateTime;
    }

    /**
     * <p>Setter method for tagScanDateTime.</p>
     *
     * @param tagScanDateTime Set for tagScanDateTime
     */
    public void setTagScanDateTime(Timestamp tagScanDateTime) {
        this.tagScanDateTime = tagScanDateTime;
    }

    /**
     * <p>Getter method for receiveDate.</p>
     *
     * @return the receiveDate
     */
    public String getReceiveDate() {
        return receiveDate;
    }

    /**
     * <p>Setter method for receiveDate.</p>
     *
     * @param receiveDate Set for receiveDate
     */
    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * <p>Getter method for dscId.</p>
     *
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>Setter method for dscId.</p>
     *
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>Getter method for deviceId.</p>
     *
     * @return the deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * <p>Setter method for deviceId.</p>
     *
     * @param deviceId Set for deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * <p>Getter method for batchNo.</p>
     *
     * @return the batchNo
     */
    public String getBatchNo() {
        return batchNo;
    }

    /**
     * <p>Setter method for batchNo.</p>
     *
     * @param batchNo Set for batchNo
     */
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    /**
     * <p>Getter method for textSeqNo.</p>
     *
     * @return the textSeqNo
     */
    public String getTextSeqNo() {
        return textSeqNo;
    }

    /**
     * <p>Setter method for textSeqNo.</p>
     *
     * @param textSeqNo Set for textSeqNo
     */
    public void setTextSeqNo(String textSeqNo) {
        this.textSeqNo = textSeqNo;
    }

    /**
     * <p>Getter method for numOfText.</p>
     *
     * @return the numOfText
     */
    public String getNumOfText() {
        return numOfText;
    }

    /**
     * <p>Setter method for numOfText.</p>
     *
     * @param numOfText Set for numOfText
     */
    public void setNumOfText(String numOfText) {
        this.numOfText = numOfText;
    }

    /**
     * <p>Getter method for serverName.</p>
     *
     * @return the serverName
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * <p>Setter method for serverName.</p>
     *
     * @param serverName Set for serverName
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * <p>Getter method for transmitionDatetime.</p>
     *
     * @return the transmitionDatetime
     */
    public Timestamp getTransmitionDatetime() {
        return transmitionDatetime;
    }

    /**
     * <p>Setter method for transmitionDatetime.</p>
     *
     * @param transmitionDatetime Set for transmitionDatetime
     */
    public void setTransmitionDatetime(Timestamp transmitionDatetime) {
        this.transmitionDatetime = transmitionDatetime;
    }

    /**
     * <p>Getter method for asnReceiveDatetime.</p>
     *
     * @return the asnReceiveDatetime
     */
    public Timestamp getAsnReceiveDatetime() {
        return asnReceiveDatetime;
    }

    /**
     * <p>Setter method for asnReceiveDatetime.</p>
     *
     * @param asnReceiveDatetime Set for asnReceiveDatetime
     */
    public void setAsnReceiveDatetime(Timestamp asnReceiveDatetime) {
        this.asnReceiveDatetime = asnReceiveDatetime;
    }

    /**
     * <p>Getter method for receiveDataMsg.</p>
     *
     * @return the receiveDataMsg
     */
    public String getReceiveDataMsg() {
        return receiveDataMsg;
    }

    /**
     * <p>Setter method for receiveDataMsg.</p>
     *
     * @param receiveDataMsg Set for receiveDataMsg
     */
    public void setReceiveDataMsg(String receiveDataMsg) {
        this.receiveDataMsg = receiveDataMsg;
    }

    /**
     * <p>Getter method for receiveDatetime.</p>
     *
     * @return the receiveDatetime
     */
    public Timestamp getReceiveDatetime() {
        return receiveDatetime;
    }

    /**
     * <p>Setter method for receiveDatetime.</p>
     *
     * @param receiveDatetime Set for receiveDatetime
     */
    public void setReceiveDatetime(Timestamp receiveDatetime) {
        this.receiveDatetime = receiveDatetime;
    }

    /**
     * <p>Getter method for tagScanDate.</p>
     *
     * @return the tagScanDate
     */
    public String getTagScanDate() {
        return tagScanDate;
    }

    /**
     * <p>Setter method for tagScanDate.</p>
     *
     * @param tagScanDate Set for tagScanDate
     */
    public void setTagScanDate(String tagScanDate) {
        this.tagScanDate = tagScanDate;
    }

    /**
     * <p>Getter method for asnScanDateTime.</p>
     *
     * @return the asnScanDateTime
     */
    public String getAsnScanDateTime() {
        return asnScanDateTime;
    }

    /**
     * <p>Setter method for asnScanDateTime.</p>
     *
     * @param asnScanDateTime Set for asnScanDateTime
     */
    public void setAsnScanDateTime(String asnScanDateTime) {
        this.asnScanDateTime = asnScanDateTime;
    }
}