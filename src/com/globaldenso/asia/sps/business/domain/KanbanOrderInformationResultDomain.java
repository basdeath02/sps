/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * Kanban Order Information Result Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationResultDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * Serializable.
     * </p>
     */
    private static final long serialVersionUID = -1970017321768804850L;

    /**
     * <p>
     * The Kanban Order Information DO Detail Return Domain.
     * </p>
     */
    private KanbanOrderInformationDoDetailReturnDomain kanbanOrderInformationDoDetailReturnDomain;

    /**
     * <p>
     * The Kanban Order Information DO Detail List Return Domain.
     * </p>
     */
    private List<KanbanOrderInformationDoDetailListReturnDomain> 
    kanbanOrderInformationDoDetailListReturnDomain;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public KanbanOrderInformationResultDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for kanbanOrderInformationDoDetailReturnDomain.
     * </p>
     * 
     * @return the kanbanOrderInformationDoDetailReturnDomain
     */
    public KanbanOrderInformationDoDetailReturnDomain 
    getKanbanOrderInformationDoDetailReturnDomain() {
        return kanbanOrderInformationDoDetailReturnDomain;
    }

    /**
     * <p>
     * Setter method for kanbanOrderInformationDoDetailReturnDomain.
     * </p>
     * 
     * @param kanbanOrderInformationDoDetailReturnDomain Set for
     *            kanbanOrderInformationDoDetailReturnDomain
     */
    public void setKanbanOrderInformationDoDetailReturnDomain(
        KanbanOrderInformationDoDetailReturnDomain kanbanOrderInformationDoDetailReturnDomain) {
        this.kanbanOrderInformationDoDetailReturnDomain = 
            kanbanOrderInformationDoDetailReturnDomain;
    }

    /**
     * <p>
     * Getter method for kanbanOrderInformationDoDetailListReturnDomain.
     * </p>
     * 
     * @return the kanbanOrderInformationDoDetailListReturnDomain
     */
    public List<KanbanOrderInformationDoDetailListReturnDomain> 
    getKanbanOrderInformationDoDetailListReturnDomain() {
        return kanbanOrderInformationDoDetailListReturnDomain;
    }

    /**
     * <p>
     * Setter method for kanbanOrderInformationDoDetailListReturnDomain.
     * </p>
     * 
     * @param kanbanOrderInformationDoDetailListReturnDomain Set for
     *            kanbanOrderInformationDoDetailListReturnDomain
     */
    public void setKanbanOrderInformationDoDetailListReturnDomain(
        List<KanbanOrderInformationDoDetailListReturnDomain> 
        kanbanOrderInformationDoDetailListReturnDomain) {
        this.kanbanOrderInformationDoDetailListReturnDomain = 
            kanbanOrderInformationDoDetailListReturnDomain;
    }

}
