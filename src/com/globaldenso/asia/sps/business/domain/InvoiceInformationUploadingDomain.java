/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class Invoice Information Uploading Domain.
 * @author CSI
 */
public class InvoiceInformationUploadingDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3857775057146425189L;
    

    /** The File Management Domain. */
    private FileManagementDomain fileManagementDomain;
    
    /** The session Id. */
    private String sessionId;
    
    /** The list of sps temp upload invoice domain. */
    private List<TmpUploadInvoiceResultDomain> tmpUploadInvoiceResultList;
    
    /** The file name. */
    private String fileName;
    
    /** The csv result. */
    private StringBuffer csvResult;
    
    /** The total record. */
    private String totalRecord;
    
    /** The correct record. */
    private String correctRecord;
    
    /** The warning record. */
    private String warningRecord;
    
    /** The error record. */
    private String errorRecord;
    
    /** The invoice record. */
    private String invoiceNo;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier tax ID. */
    private String supplierTaxId;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plan code. */
    private String dPcd;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The company supplier list. */
    private List<String> companySupplierList;
    
    /** The company denso list. */
    private List<String> companyDensoList;
    
    /** The company plan denso list. */
    private Map<String, List<String>> plantDensoList;
    
    /** The company plant supplier list. */
    private Map<String, List<String>> plantSupplierList;
    
    /** The denso supplier relation list. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;
    
    /** The success message list. */
    private List<ApplicationMessageDomain> successMessageList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new Invoice Information Uploading Domain.
     */
    public InvoiceInformationUploadingDomain() {
        super();
        errorMessageList = new ArrayList<ApplicationMessageDomain>();
        successMessageList = new ArrayList<ApplicationMessageDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * <p>Getter method for File Management Domain.</p>
     *
     * @return the File Management Domain
     */
    public FileManagementDomain getFileManagementDomain() {
        return fileManagementDomain;
    }

    /**
     * <p>Setter method for File Management Domain.</p>
     *
     * @param fileManagementDomain Set for File Management Domain
     */
    public void setFileManagementDomain(FileManagementDomain fileManagementDomain) {
        this.fileManagementDomain = fileManagementDomain;
    }
    
    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    
    /**
     * Gets the list of SPS Temp Upload Invoice Domain.
     * 
     * @return the spsTmpUploadInvoiceDomainList.
     */
    public List<TmpUploadInvoiceResultDomain> getTmpUploadInvoiceResultList() {
        return tmpUploadInvoiceResultList;
    }
    
    /**
     * Sets the list of SPS Temp Upload Invoice Domain.
     * 
     * @param tmpUploadInvoiceResultList list of Temp Upload Invoice Result Domain.
     */  
    public void setTmpUploadInvoiceResultList(
        List<TmpUploadInvoiceResultDomain> tmpUploadInvoiceResultList) {
        this.tmpUploadInvoiceResultList = tmpUploadInvoiceResultList;
    }
    

    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for csvResult.</p>
     *
     * @return the csvResult
     */
    public StringBuffer getCsvResult() {
        return csvResult;
    }

    /**
     * <p>Setter method for csvResult.</p>
     *
     * @param csvResult Set for csvResult
     */
    public void setCsvResult(StringBuffer csvResult) {
        this.csvResult = csvResult;
    }

    /**
     * <p>Getter method for totalRecord.</p>
     *
     * @return the totalRecord
     */
    public String getTotalRecord() {
        return totalRecord;
    }

    /**
     * <p>Setter method for totalRecord.</p>
     *
     * @param totalRecord Set for totalRecord
     */
    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    /**
     * <p>Getter method for correctRecord.</p>
     *
     * @return the correctRecord
     */
    public String getCorrectRecord() {
        return correctRecord;
    }

    /**
     * <p>Setter method for correctRecord.</p>
     *
     * @param correctRecord Set for correctRecord
     */
    public void setCorrectRecord(String correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * <p>Getter method for warningRecord.</p>
     *
     * @return the warningRecord
     */
    public String getWarningRecord() {
        return warningRecord;
    }

    /**
     * <p>Setter method for warningRecord.</p>
     *
     * @param warningRecord Set for warningRecord
     */
    public void setWarningRecord(String warningRecord) {
        this.warningRecord = warningRecord;
    }

    /**
     * <p>Getter method for errorRecord.</p>
     *
     * @return the errorRecord
     */
    public String getErrorRecord() {
        return errorRecord;
    }

    /**
     * <p>Setter method for errorRecord.</p>
     *
     * @param errorRecord Set for errorRecord
     */
    public void setErrorRecord(String errorRecord) {
        this.errorRecord = errorRecord;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * Gets the company supplier list.
     * 
     * @return the company supplier list.
     */
    public List<String> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the company supplier list.
     * 
     * @param companySupplierList the company supplier list.
     */
    public void setCompanySupplierList(
        List<String> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the company denso list.
     * 
     * @return the company denso list.
     */
    public List<String> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the company denso list.
     * 
     * @param companyDensoList the company denso list.
     */
    public void setCompanyDensoList(List<String> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso list.
     * 
     * @return the company denso list.
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * Sets the denso supplier relation list.
     * 
     * @param densoSupplierRelationList the denso supplier relation list.
     */
    public void setDensoSupplierRelationList(List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }
    
    /**
     * Gets the success message list.
     * 
     * @return the success message list.
     */
    public List<ApplicationMessageDomain> getSuccessMessageList() {
        return successMessageList;
    }

    /**
     * Sets the success message list.
     * 
     * @param successMessageList the success message list.
     */
    public void setSuccessMessageList(List<ApplicationMessageDomain> successMessageList) {
        this.successMessageList = successMessageList;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * Gets the company plan denso list.
     * 
     * @return the company plan denso list.
     */
    public Map<String, List<String>> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the company plan denso list.
     * 
     * @param plantDensoList the company plan denso list.
     */
    public void setPlantDensoList(Map<String, List<String>> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the company plant supplier list.
     * 
     * @return the company plant supplier list.
     */
    public Map<String, List<String>> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the company plant supplier list.
     * 
     * @param plantSupplierList the company plant supplier list.
     */
    public void setPlantSupplierList(Map<String, List<String>> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
}