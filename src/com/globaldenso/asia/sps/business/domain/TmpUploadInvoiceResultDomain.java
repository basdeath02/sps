/*
 * ModifyDate Development company     Describe 
 * 2014/08/18 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class TmpUploadInvoiceResultDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;
    
    /** The actual ETD. */
    private String actualEtd;
    
    /** The plan ETA. */
    private String planEta;
    
    /** The shipping Qty. */
    private String shippingQty;
    
    /** The SPS transaction ASN domain. */
    private SpsTmpUploadInvoiceDomain spsTmpUploadInvoice;

    /**
     * Instantiates a new file upload domain.
     */
    public TmpUploadInvoiceResultDomain() {
        super();
        this.spsTmpUploadInvoice = new SpsTmpUploadInvoiceDomain();
    }
    
    /**
     * Gets the actual ETD.
     * 
     * @return the actual ETD.
     */
    public String getActualEtd() {
        return actualEtd;
    }
    
    /**
     * Sets the actual ETD.
     * 
     * @param actualEtd the actual ETD.
     */
    public void setActualEtd(String actualEtd) {
        this.actualEtd = actualEtd;
    }
    
    /**
     * Gets the plan ETA.
     * 
     * @return the plan ETA.
     */
    public String getPlanEta() {
        return planEta;
    }
    
    /**
     * Sets the plan ETA.
     * 
     * @param planEta the plan ETA.
     */
    public void setPlanEta(String planEta) {
        this.planEta = planEta;
    }

    /**
     * <p>Getter method for shippingQty.</p>
     *
     * @return the shippingQty
     */
    public String getShippingQty() {
        return shippingQty;
    }

    /**
     * <p>Setter method for shippingQty.</p>
     *
     * @param shippingQty Set for shippingQty
     */
    public void setShippingQty(String shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * <p>Getter method for spsTmpUploadInvoiceDomain.</p>
     *
     * @return the spsTmpUploadInvoiceDomain
     */
    public SpsTmpUploadInvoiceDomain getSpsTmpUploadInvoice() {
        return spsTmpUploadInvoice;
    }

    /**
     * <p>Setter method for spsTmpUploadInvoiceDomain.</p>
     *
     * @param spsTmpUploadInvoice Set for spsTmpUploadInvoiceDomain
     */
    public void setSpsTmpUploadInvoice(SpsTmpUploadInvoiceDomain spsTmpUploadInvoice) {
        this.spsTmpUploadInvoice = spsTmpUploadInvoice;
    }
    

}