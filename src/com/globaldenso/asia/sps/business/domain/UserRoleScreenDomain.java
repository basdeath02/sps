/*
 * ModifyDate Development company Describe 
 * 2014/07/16 Akat               Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;


/**
 * The Class RoleUserScreenDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserRoleScreenDomain  extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 7438090417281573400L;

    /** The domain for SPS_M_USER_ROLE. */
    private SpsMUserRoleDomain spsMUserRoleDomain;
    
    /** The domain for SPS_M_SCREEN */
    private SpsMScreenDomain spsMScreenDomain;

    /** The current date time. */
    private Timestamp currentDatetime;
    
    /** The default constructor. */
    public UserRoleScreenDomain() {
        super();
        this.spsMUserRoleDomain = new SpsMUserRoleDomain();
        this.spsMScreenDomain = new SpsMScreenDomain();
    }

    /**
     * <p>Getter method for spsMScreenDomain.</p>
     *
     * @return the spsMScreenDomain
     */
    public SpsMScreenDomain getSpsMScreenDomain() {
        return spsMScreenDomain;
    }

    /**
     * <p>Setter method for spsMScreenDomain.</p>
     *
     * @param spsMScreenDomain Set for spsMScreenDomain
     */
    public void setSpsMScreenDomain(SpsMScreenDomain spsMScreenDomain) {
        this.spsMScreenDomain = spsMScreenDomain;
    }

    /**
     * <p>Getter method for currentDatetime.</p>
     *
     * @return the currentDatetime
     */
    public Timestamp getCurrentDatetime() {
        return currentDatetime;
    }

    /**
     * <p>Setter method for currentDatetime.</p>
     *
     * @param currentDatetime Set for currentDatetime
     */
    public void setCurrentDatetime(Timestamp currentDatetime) {
        this.currentDatetime = currentDatetime;
    }

    /**
     * <p>Getter method for spsMUserRoleDomain.</p>
     *
     * @return the spsMUserRoleDomain
     */
    public SpsMUserRoleDomain getSpsMUserRoleDomain() {
        return spsMUserRoleDomain;
    }

    /**
     * <p>Setter method for spsMUserRoleDomain.</p>
     *
     * @param spsMUserRoleDomain Set for spsMUserRoleDomain
     */
    public void setSpsMUserRoleDomain(SpsMUserRoleDomain spsMUserRoleDomain) {
        this.spsMUserRoleDomain = spsMUserRoleDomain;
    }

}
