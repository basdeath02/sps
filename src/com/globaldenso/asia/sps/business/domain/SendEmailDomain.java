/*
 * ModifyDate Development company     Describe 
 * 2014/07/10 CSI Parichat            Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * The Class SendEmailDomain.
 * @author CSI
 * @version 1.00
 */
public class SendEmailDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID */
    private static final long serialVersionUID = 614863998325328562L;
    
    /** The case type */
    private String caseType;
    
    /** The contents. */
    private String contents;
    
    /** The email from. */
    private String emailFrom;
    
    /** The email to. */
    private String emailTo;
    
    /** The from. */
    private String from;
    
    /** The header. */
    private String header;
    
    /** The log smtp. */
    private String emailSmtp;
    
    /** The locale. */
    private Locale locale;
    
    /** The email cc. */
    private String emailCc;
    
    /** The to list. */
    private List<String> toList;

    /** The Default constructor. */
    public SendEmailDomain() {
    }

    /**
     * Gets the case type.
     * 
     * @return the case type
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the case type.
     * 
     * @param caseType the new case type
     */
    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    /**
     * Gets the contents.
     * 
     * @return the contents
     */
    public String getContents() {
        return contents;
    }

    /**
     * Sets the contents.
     * 
     * @param contents the new contents
     */
    public void setContents(String contents) {
        this.contents = contents;
    }

    /**
     * Gets the email from.
     * 
     * @return the email from
     */
    public String getEmailFrom() {
        return emailFrom;
    }

    /**
     * Sets the email from.
     * 
     * @param emailFrom the new  email from
     */
    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    /**
     * Gets the email to.
     * 
     * @return the email to
     */
    public String getEmailTo() {
        return emailTo;
    }

    /**
     * Sets the email to.
     * 
     * @param emailTo the new email to
     */
    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    /**
     * Gets the from.
     * 
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the from.
     * 
     * @param from the new from
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * Gets the header.
     * 
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the header.
     * 
     * @param header the new header
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Gets the email smtp.
     * 
     * @return the email smtp
     */
    public String getEmailSmtp() {
        return emailSmtp;
    }

    /**
     * Sets the email smtp.
     * 
     * @param emailSmtp the new email smtp
     */
    public void setEmailSmtp(String emailSmtp) {
        this.emailSmtp = emailSmtp;
    }

    /**
     * Gets the locale.
     * 
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Sets the locale.
     * 
     * @param locale the new locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * Gets the email cc.
     * 
     * @return the email cc
     */
    public String getEmailCc() {
        return emailCc;
    }

    /**
     * Sets the email cc.
     * 
     * @param emailCc the new email cc
     */
    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    /**
     * Gets the to list.
     * 
     * @return the to list
     */
    public List<String> getToList() {
        return toList;
    }

    /**
     * Sets the to list.
     * 
     * @param toList the new to list
     */
    public void setToList(List<String> toList) {
        this.toList = toList;
    }
}
