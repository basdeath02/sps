/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/12 Parichat           Create   
 * 2015/09/17 CSI Akat           FIX wrong dateformat
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The Class AnnounceMessageDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class AnnounceMessageDomain extends BaseDomain implements Serializable{
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2568445462735967379L;
    
    /** The seq no. */
    private int seqNo;
    
    /** The D CD. */
    private String dCd;
   
    /** The effect start. */
    private Timestamp effectStart;
    
    /** The effect end. */
    private Timestamp effectEnd;
    
    /** The announce message. */
    private String announceMessage;
    
    /** The create user. */
    private String createUser;
    
    /** The create date time. */
    private Timestamp createDateTime;
    
    /** The Message List. */
    private List<String> MessageList;
    
    /** The File Path. */
    private String filePath;
    
    /** The Temp File Path. */
    private String tempPath;
    
    // FIX : wrong dateformat
    /** Formated effectStart for show in screen. */
    private String effectStartShow;

    /**
     * Instantiates a new account domain.
     */
    public AnnounceMessageDomain() {
        super();
    }

    /**
     * Gets the seq no.
     * 
     * @return the seq no.
     */
    public int getSeqNo() {
        return seqNo;
    }
    
    /**
     * Sets the seq no.
     * 
     * @param seqNo the seq no.
     */
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Gets the d cd.
     * 
     * @return the d cd.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the d cd.
     * 
     * @param dCd the d cd.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the effect start.
     * 
     * @return the effect start.
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Sets the effect start.
     * 
     * @param effectStart the effect start.
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Gets the effect end.
     * 
     * @return the effect end.
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Sets the effect end.
     * 
     * @param effectEnd the effect end.
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Gets the announce message.
     * 
     * @return the announce message.
     */
    public String getAnnounceMessage() {
        return announceMessage;
    }

    /**
     * Sets the announce message.
     * 
     * @param announceMessage the announce message.
     */
    public void setAnnounceMessage(String announceMessage) {
        this.announceMessage = announceMessage;
    }

    /**
     * Gets the create user.
     * 
     * @return the create user.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create user.
     * 
     * @param createUser the create user.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create date time.
     * 
     * @return the create date time.
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the create date time.
     * 
     * @param createDateTime the create date time.
     */
    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * <p>Getter method for messageList.</p>
     *
     * @return the messageList
     */
    public List<String> getMessageList() {
        return MessageList;
    }

    /**
     * <p>Setter method for messageList.</p>
     *
     * @param messageList Set for messageList
     */
    public void setMessageList(List<String> messageList) {
        MessageList = messageList;
    }

    /**
     * <p>Getter method for filePath.</p>
     *
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * <p>Setter method for filePath.</p>
     *
     * @param filePath Set for filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * <p>Getter method for tempPath.</p>
     *
     * @return the tempPath
     */
    public String getTempPath() {
        return tempPath;
    }

    /**
     * <p>Setter method for tempPath.</p>
     *
     * @param tempPath Set for tempPath
     */
    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    /**
     * <p>Getter method for effectStartShow.</p>
     *
     * @return the effectStartShow
     */
    public String getEffectStartShow() {
        return effectStartShow;
    }

    /**
     * <p>Setter method for effectStartShow.</p>
     *
     * @param effectStartShow Set for effectStartShow
     */
    public void setEffectStartShow(String effectStartShow) {
        this.effectStartShow = effectStartShow;
    }

}