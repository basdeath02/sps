/*
 * ModifyDate Development company     Describe 
 * 2014/07/27 CSI Parichat            Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * Base domain for define property that will use in all Domain.
 */
public class ApplicationMessageDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID */
    private static final long serialVersionUID = -5445444525621623643L;

    /** The message type. */
    private String messageType;
    
    /** The message list. */
    private String message;
    
    /** The Default constructor. */
    public ApplicationMessageDomain() {
    }

    /**
     * <p>The Constructor for setting message type and message.</p>
     *
     * @param messageType the String
     * @param message the String
     * 
     */
    public ApplicationMessageDomain(String messageType, String message) {
        this.messageType = messageType;
        this.message = message;
    }
    
    /**
     * <p>Getter method for message type.</p>
     *
     * @return the message type
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * <p>Setter method for message type.</p>
     *
     * @param messageType Set for message type
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * <p>Getter method for message.</p>
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * <p>Setter method for message.</p>
     *
     * @param message Set for message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
