/*
 * ModifyDate Development company Describe 
 * 2014/06/12 Parichat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class CompanyPlantAuthenDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class CompanyPlantAuthenDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7933310381541172548L;
    
    /** The company supplier code. */
    private String sCd;
    
    /** The plant supplier code. */
    private String sPcd;
    
    /** The company DENSO code. */
    private String dCd;
    
    /** The plant DENSO code. */
    private String dPcd;
    
    
    /**
     * Instantiates a new account domain.
     */
    public CompanyPlantAuthenDomain() {
        super();
    }

    /**
     * Gets the company supplier code.
     * 
     * @return the company supplier code.
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the supplier company code.
     * 
     * @param sCd the supplier company code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Gets the plant supplier code.
     * 
     * @return the plant supplier code.
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the plant supplier code.
     * 
     * @param sPcd the plant supplier code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }


    /**
     * Gets the DENSO company code.
     * 
     * @return the DENSO company code.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the DENSO company code.
     * 
     * @param dCd the DENSO company code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the plant DENSO code.
     * 
     * @return the plant DENSO code.
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the plant DENSO code.
     * 
     * @param dPcd the plant DENSO code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

}