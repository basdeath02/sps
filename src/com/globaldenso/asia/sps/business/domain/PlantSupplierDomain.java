/*
 * ModifyDate Development company Describe 
 * 2014/08/02 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class PlantSupplierDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class PlantSupplierDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID. */
    private static final long serialVersionUID = -5764879946034208002L;

    /**
     * Plant Supplier.
     */
    private String sPcd;

    /**
     * Company supplier code.
     */
    private String sCd;
    
    /**
     * Vendor Code.
     * */
    private String vendorCd;

    /**
     * Supplier plant name
     */
    private String plantName;

    /**
     * Address
     */
    private String address;

    /**
     * Telephone no.
     */
    private String telephone;

    /**
     * Fax no.
     */
    private String fax;

    /**
     * Payment term
     */
    private BigDecimal paymentTerm;

    /**
     * Contact person name
     */
    private String contactPerson;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create date time
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update date time
     */
    private Timestamp lastUpdateDatetime;

    /** The default constructor. */
    public PlantSupplierDomain() {
        super();
    }

    /**
     * <p>Getter method for plant supplier.</p>
     *
     * @return the plant supplier.
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for plant supplier.</p>
     *
     * @param sPcd Set for plant supplier.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for company supplier code.</p>
     *
     * @return the company supplier code.
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for company supplier code.</p>
     *
     * @param sCd Set for company supplier code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for plantName.</p>
     *
     * @return the plantName
     */
    public String getPlantName() {
        return plantName;
    }

    /**
     * <p>Setter method for plantName.</p>
     *
     * @param plantName Set for plantName
     */
    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    /**
     * <p>Getter method for address.</p>
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * <p>Setter method for address.</p>
     *
     * @param address Set for address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * <p>Getter method for telephone.</p>
     *
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * <p>Setter method for telephone.</p>
     *
     * @param telephone Set for telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * <p>Getter method for fax.</p>
     *
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * <p>Setter method for fax.</p>
     *
     * @param fax Set for fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * <p>Getter method for paymentTerm.</p>
     *
     * @return the paymentTerm
     */
    public BigDecimal getPaymentTerm() {
        return paymentTerm;
    }

    /**
     * <p>Setter method for paymentTerm.</p>
     *
     * @param paymentTerm Set for paymentTerm
     */
    public void setPaymentTerm(BigDecimal paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     * <p>Getter method for contactPerson.</p>
     *
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * <p>Setter method for contactPerson.</p>
     *
     * @param contactPerson Set for contactPerson
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * <p>Getter method for isActive.</p>
     *
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * <p>Setter method for isActive.</p>
     *
     * @param isActive Set for isActive
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * <p>Getter method for createDscId.</p>
     *
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * <p>Setter method for createDscId.</p>
     *
     * @param createDscId Set for createDscId
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * <p>Getter method for createDatetime.</p>
     *
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * <p>Setter method for createDatetime.</p>
     *
     * @param createDatetime Set for createDatetime
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * <p>Getter method for lastUpdateDscId.</p>
     *
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>Setter method for lastUpdateDscId.</p>
     *
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>Getter method for lastUpdateDatetime.</p>
     *
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>Setter method for lastUpdateDatetime.</p>
     *
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
}
