/*
 * ModifyDate Developmentcompany    Describe
 * 2014/06/24 Phakaporn P.           Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class TempUserSupplierDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class TmpSupplierInfoDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 163831453934363379L;

    /**
     * User DSC ID
     */
    private String userDscId;

    /**
     * Session code
     */
    private String sessionCd;

    /**
     * CSV line no.
     */
    private BigDecimal lineNo;

    /**
     * DENSO company code
     */
    private String dCd;

    /**
     * Supplier company code
     */
    private String sCd;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /**
     * DENSO part no.
     */
    private String dPn;

    /**
     * Supplier part no.
     */
    private String sPn;
    
    /**
     * Flag.
     */
    private String flag;
    
    /**
     * VENDOR_CD
     */
    private String vendorCd;
    
    /** The Company name. */
    private String companyName;

    /**
     * Effective date
     */
    private Timestamp effectDate;

    /**
     * Upload date time
     */
    private Timestamp uploadDatetime;

    /**
     * Relation last update date time
     */
    private Timestamp relationLastUpdate;

    /**
     * Part last update date time
     */
    private Timestamp partLastUpdate;

    /**
     * Move to actual table flag
     */
    private String isActualRegister;

    /**
     * Move to actual table date time
     */
    private Timestamp toActualDatetime;
    
    /** The To Actual Error Code. */
    private String toActualErrorCd;
    
    /** The To Actual Error Description. */
    private String toActualErrorDesc;

    /**
     * Default constructor
     */
    public TmpSupplierInfoDomain() {
    }

    /**
     * Getter method of "userDscId"
     * 
     * @return the userDscId
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId"
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd"
     * 
     * @return the sessionCd
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd"
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo"
     * 
     * @return the lineNo
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo"
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "effectDate"
     * 
     * @return the effectDate
     */
    public Timestamp getEffectDate() {
        return effectDate;
    }

    /**
     * Setter method of "effectDate"
     * 
     * @param effectDate Set in "effectDate".
     */
    public void setEffectDate(Timestamp effectDate) {
        this.effectDate = effectDate;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "relationLastUpdate"
     * 
     * @return the relationLastUpdate
     */
    public Timestamp getRelationLastUpdate() {
        return relationLastUpdate;
    }

    /**
     * Setter method of "relationLastUpdate"
     * 
     * @param relationLastUpdate Set in "relationLastUpdate".
     */
    public void setRelationLastUpdate(Timestamp relationLastUpdate) {
        this.relationLastUpdate = relationLastUpdate;
    }

    /**
     * Getter method of "partLastUpdate"
     * 
     * @return the partLastUpdate
     */
    public Timestamp getPartLastUpdate() {
        return partLastUpdate;
    }

    /**
     * Setter method of "partLastUpdate"
     * 
     * @param partLastUpdate Set in "partLastUpdate".
     */
    public void setPartLastUpdate(Timestamp partLastUpdate) {
        this.partLastUpdate = partLastUpdate;
    }

    /**
     * Getter method of "isActualRegister"
     * 
     * @return the isActualRegister
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister"
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime"
     * 
     * @return the toActualDatetime
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime"
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }
    /**
     * Gets the Actual Error Code.
     * 
     * @return the Actual Error Code.
     */
    public String getToActualErrorCd() {
        return toActualErrorCd;
    }

    /**
     * Sets the Actual Error Code.
     * 
     * @param toActualErrorCd the Actual Error Code.
     */
    public void setToActualErrorCd(String toActualErrorCd) {
        this.toActualErrorCd = toActualErrorCd;
    }

    /**
     * Gets the Actual Error Description.
     * 
     * @return the Actual Error Description.
     */
    public String getToActualErrorDesc() {
        return toActualErrorDesc;
    }

    /**
     * Sets the Actual Error Description.
     * 
     * @param toActualErrorDesc the Actual Error Description.
     */
    public void setToActualErrorDesc(String toActualErrorDesc) {
        this.toActualErrorDesc = toActualErrorDesc;
    }
    /**
     * Gets the Flag.
     * 
     * @return the Flag.
     */
    public String getFlag() {
        return flag;
    }
    /**
     * Sets the Flag.
     * 
     * @param flag the Flag.
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }
    
    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Gets the Company name.
     * 
     * @return the Company name.
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the Company name.
     * 
     * @param companyName the Company name.
     */ 
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}