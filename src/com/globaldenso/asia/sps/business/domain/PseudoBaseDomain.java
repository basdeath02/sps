/*
 * ModifyDate Development company     Describe 
 * 2014/03/13 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * <p>Base domain for convert from WebService REST.</p>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "baseDomain")
public class PseudoBaseDomain implements Serializable {

    /** Generated Serial Version UID */
    private static final long serialVersionUID = -406282836667714854L;

    /** The row number from. */
    @XmlElement(name = "rowNumFrom")
    private int pseudoRowNumFrom;

    /** The row number to. */
    @XmlElement(name = "rowNumTo")
    private int pseudoRowNumTo;

    /** The row count. */
    @XmlElement(name = "rowCnt")
    private int pseudoRowCnt;

    /** The locale. */
    @XmlElement(name = "locale")
    private String pseudoLocale;

    /** The DSC-ID(user id for Siteminder). */
    @XmlElement(name = "uid")
    private String pseudoUid;

    /** The Default constructor. */
    public PseudoBaseDomain() {
    }

    /**
     * <p>Getter method for row number from.</p>
     *
     * @return the row number from
     */
    public int getPseudoRowNumFrom() {
        return pseudoRowNumFrom;
    }

    /**
     * <p>Setter method for row number from.</p>
     *
     * @param pseudoRowNumFrom Set for row number from
     */
    public void setPseudoRowNumFrom(int pseudoRowNumFrom) {
        this.pseudoRowNumFrom = pseudoRowNumFrom;
    }

    /**
     * <p>Getter method for row number to.</p>
     *
     * @return the row number to
     */
    public int getPseudoRowNumTo() {
        return pseudoRowNumTo;
    }

    /**
     * <p>Setter method for row number to.</p>
     *
     * @param pseudoRowNumTo Set for row number to
     */
    public void setPseudoRowNumTo(int pseudoRowNumTo) {
        this.pseudoRowNumTo = pseudoRowNumTo;
    }

    /**
     * <p>Getter method for row count.</p>
     *
     * @return the row count
     */
    public int getPseudoRowCnt() {
        return pseudoRowCnt;
    }

    /**
     * <p>Setter method for row count.</p>
     *
     * @param pseudoRowCnt Set for row count
     */
    public void setPseudoRowCnt(int pseudoRowCnt) {
        this.pseudoRowCnt = pseudoRowCnt;
    }

    /**
     * <p>Getter method for locale.</p>
     *
     * @return the locale
     */
    public String getPseudoLocale() {
        return pseudoLocale;
    }

    /**
     * <p>Setter method for locale.</p>
     *
     * @param pseudoLocale Set for locale
     */
    public void setPseudoLocale(String pseudoLocale) {
        this.pseudoLocale = pseudoLocale;
    }

    /**
     * <p>Getter method for DSC-ID(user id for Siteminder).</p>
     *
     * @return the DSC-ID(user id for Siteminder)
     */
    public String getPseudoUid() {
        return pseudoUid;
    }

    /**
     * <p>Setter method for DSC-ID(user id for Siteminder).</p>
     *
     * @param pseudoUid Set for DSC-ID(user id for Siteminder)
     */
    public void setPseudoUid(String pseudoUid) {
        this.pseudoUid = pseudoUid;
    }

}
