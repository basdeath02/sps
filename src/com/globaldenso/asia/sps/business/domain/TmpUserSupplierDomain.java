/*
 * ModifyDate Development company    Describe
 * 2014/06/24 Phakaporn P.           Create
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class TempUserSupplierDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class TmpUserSupplierDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3935827237501551534L;

    /** The User DSC ID. */
    private String userDscId;
    
    /** The Session ID. */
    private String sessionCode;
    
    /** The Role Code. */
    private String roleCode;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The Line No. **/
    private BigDecimal lineNo;
    
    /** The Line No. **/
    private Integer seqNo;
    
    /** The DENSO Owner. */
    private String densoOwner;
    
    /** The Supplier Company Code */
    private String sCd;
    
    /** The Supplier Plant Code */
    private String sPcd;
    
    /** The Department Code. */
    private String departmentCode;
    
    /** The first name. */
    private String firstName;
    
    /** The middle name. */
    private String middleName;
    
    /** The last name. */
    private String lastName;
    
    /** The email. */
    private String email;
    
    /** The telephone. */
    private String telephone;
    
    /** The Email urgent order. */
    private String emlUrgentOrder;
    
    /** The Email Supplier Info Not Found. */
    private String emlSInfoNotfound;
    
    /** The Email allow revise flag. */
    private String emlAllowRevise;
    
    /** The Email cancel invoice flag. */
    private String emlCancelInvoice;
    
    /** The Role Company Code. */
    private String roleCompanyCode;
    
    /** The Role Supplier Plant Code. */
    private String roleSupplierPlantCode;
    
    /** The Effect Start. */
    private Timestamp effectStart;
    
    /** The Effect end. */
    private Timestamp effectEnd;
    
    /** The Information flag. */
    private String informationFlag;
    
    /** The role flag. */
    private String roleFlag;
    
    /** The Actual register flag. */
    private String isActualRegister;
    
    /** The Upload Date time. */
    private Timestamp uploadDatetime;
    
    /** The To Actual Date time. */
    private Timestamp toActualDatetime;
    
    /** The To Actual Error Code. */
    private String toActualErrorCd;
    
    /** The To Actual Error Description. */
    private String toActualErrorDesc;
    
    /** The Role Last Update. */
    private Timestamp roleLastUpdate;
    
    /** The User Last Update. */
    private Timestamp userLastUpdate;
    
    /** The Supplier Last Update. */
    private Timestamp supplierLastUpdate;
    
    /**
     * Instantiates a new Temp User Supplier domain.
     */
    public TmpUserSupplierDomain() {
        super();
    }


    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Gets the User DSC ID.
     * 
     * @return the User DSC ID.
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Sets the User DSC ID.
     * 
     * @param userDscId the User DSC ID.
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Gets the Line No.
     * 
     * @return the Line No.
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Sets the Line No.
     * 
     * @param lineNo the Line No.
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Gets the DENSO Owner.
     * 
     * @return the DENSO Owner.
     */
    public String getDensoOwner() {
        return densoOwner;
    }

    /**
     * Sets the DENSO Owner.
     * 
     * @param densoOwner the DENSO Owner.
     */
    public void setDensoOwner(String densoOwner) {
        this.densoOwner = densoOwner;
    }

    /**
     * Gets the supplier Company Code.
     * 
     * @return the supplier Company Code.
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the supplier Company Code.
     * 
     * @param sCd the supplier Company Code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Gets the supplier Plant Code.
     * 
     * @return the supplier Plant Code.
     */
    public String getSPcd() {
        return sPcd;
    }


    /**
     * Sets the supplier Plant Code.
     * 
     * @param sPcd the supplier Plant Code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }


    /**
     * Gets the department Code.
     * 
     * @return the department Code.
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /**
     * Sets the department Code.
     * 
     * @param departmentCode the department Code.
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    /**
     * Gets the first Name.
     * 
     * @return the first Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first Name.
     * 
     * @param firstName the first Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the middle name.
     * 
     * @return the middle name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the middle name.
     * 
     * @param middleName the middle name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the last name.
     * 
     * @return the last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     * 
     * @param lastName the last name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Gets the email Urgent Order.
     * 
     * @return the email Urgent Order.
     */
    public String getEmlUrgentOrder() {
        return emlUrgentOrder;
    }

    /**
     * Sets the email Urgent Order.
     * 
     * @param emlUrgentOrder the email Urgent Order.
     */
    public void setEmlUrgentOrder(String emlUrgentOrder) {
        this.emlUrgentOrder = emlUrgentOrder;
    }

    /**
     * Gets the email Supplier Info Not found.
     * 
     * @return the email Supplier Info Not found.
     */
    public String getEmlSInfoNotfound() {
        return emlSInfoNotfound;
    }

    /**
     * Sets the email Supplier Info Not found.
     * 
     * @param emlSInfoNotfound the email Supplier Info Not found.
     */
    public void setEmlSInfoNotfound(String emlSInfoNotfound) {
        this.emlSInfoNotfound = emlSInfoNotfound;
    }
    
    /**
     * Gets the email allow revise flag.
     * 
     * @return the email allow revise flag
     */
    public String getEmlAllowRevise() {
        return emlAllowRevise;
    }

    /**
     * Sets the email allow revise flag.
     * 
     * @param emlAllowRevise the email allow revise flag
     */
    public void setEmlAllowRevise(String emlAllowRevise) {
        this.emlAllowRevise = emlAllowRevise;
    }

    /**
     * Gets the email cancel invoice flag.
     * 
     * @return the email cancel invoice flag
     */
    public String getEmlCancelInvoice() {
        return emlCancelInvoice;
    }

    /**
     * Sets the email cancel invoice flag.
     * 
     * @param emlCancelInvoice the email cancel invoice flag
     */
    public void setEmlCancelInvoice(String emlCancelInvoice) {
        this.emlCancelInvoice = emlCancelInvoice;
    }

    /**
     * Gets the role Company Code.
     * 
     * @return the role Company Code.
     */
    public String getRoleCompanyCode() {
        return roleCompanyCode;
    }

    /**
     * Sets the role Company Code.
     * 
     * @param roleCompanyCode the role Company Code.
     */
    public void setRoleCompanyCode(String roleCompanyCode) {
        this.roleCompanyCode = roleCompanyCode;
    }

    /**
     * Gets the role Supplier Plant Code.
     * 
     * @return the role Supplier Plant Code.
     */
    public String getRoleSupplierPlantCode() {
        return roleSupplierPlantCode;
    }

    /**
     * Sets the role Supplier Plant Code.
     * 
     * @param roleSupplierPlantCode the role Supplier Plant Code.
     */
    public void setRoleSupplierPlantCode(String roleSupplierPlantCode) {
        this.roleSupplierPlantCode = roleSupplierPlantCode;
    }

    /**
     * Gets the effect Start.
     * 
     * @return the effect Start.
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Sets the effect Start.
     * 
     * @param effectStart the effect Start.
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Gets the effect End.
     * 
     * @return the effect End.
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Sets the effect End.
     * 
     * @param effectEnd the effect End.
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Gets the upload Date time.
     * 
     * @return the upload Date time.
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Sets the upload Date time.
     * 
     * @param uploadDatetime the upload Date time.
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Gets the sequence number.
     * 
     * @return the sequence number.
     */
    public Integer getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the sequence number.
     * 
     * @param seqNo the sequence number.
     */
    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Gets the Session ID.
     * 
     * @return the Session ID.
     */
    public String getSessionCode() {
        return sessionCode;
    }

    /**
     * Sets the Session ID.
     * 
     * @param sessionCode the Session ID.
     */
    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    /**
     * Gets the information flag.
     * 
     * @return the information flag.
     */
    public String getInformationFlag() {
        return informationFlag;
    }

    /**
     * Sets the information flag.
     * 
     * @param informationFlag the information flag.
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }

    /**
     * Gets the role flag.
     * 
     * @return the role flag.
     */
    public String getRoleFlag() {
        return roleFlag;
    }

    /**
     * Sets the role flag.
     * 
     * @param roleFlag the role flag.
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Gets the Actual Register Flag.
     * 
     * @return the Actual Register Flag.
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Sets the Actual Register Flag.
     * 
     * @param isActualRegister the Actual Register Flag.
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Gets the Actual Date time.
     * 
     * @return the Actual Date time.
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Sets the Actual Date time.
     * 
     * @param toActualDatetime the Actual Date time.
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

    /**
     * Gets the Actual Error Code.
     * 
     * @return the Actual Error Code.
     */
    public String getToActualErrorCd() {
        return toActualErrorCd;
    }

    /**
     * Sets the Actual Error Code.
     * 
     * @param toActualErrorCd the Actual Error Code.
     */
    public void setToActualErrorCd(String toActualErrorCd) {
        this.toActualErrorCd = toActualErrorCd;
    }

    /**
     * Gets the Actual Error Description.
     * 
     * @return the Actual Error Description.
     */
    public String getToActualErrorDesc() {
        return toActualErrorDesc;
    }

    /**
     * Sets the Actual Error Description.
     * 
     * @param toActualErrorDesc the Actual Error Description.
     */
    public void setToActualErrorDesc(String toActualErrorDesc) {
        this.toActualErrorDesc = toActualErrorDesc;
    }
    /**
     * Gets the Role Code.
     * 
     * @return the Role Code.
     */

    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the Role Code.
     * 
     * @param roleCode the Role Code.
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    /**
     * Gets the Role last update.
     * 
     * @return the Role last update.
     */
    public Timestamp getRoleLastUpdate() {
        return roleLastUpdate;
    }

    /**
     * Sets the Role last update.
     * 
     * @param roleLastUpdate the Role last update.
     */
    public void setRoleLastUpdate(Timestamp roleLastUpdate) {
        this.roleLastUpdate = roleLastUpdate;
    }
    
    /**
     * Getter method of "userLastUpdate"
     * 
     * @return the userLastUpdate
     */
    public Timestamp getUserLastUpdate() {
        return userLastUpdate;
    }

    /**
     * Setter method of "userLastUpdate"
     * 
     * @param userLastUpdate Set in "userLastUpdate".
     */
    public void setUserLastUpdate(Timestamp userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    /**
     * Getter method of "supplierLastUpdate"
     * 
     * @return the supplierLastUpdate
     */
    public Timestamp getSupplierLastUpdate() {
        return supplierLastUpdate;
    }

    /**
     * Setter method of "supplierLastUpdate"
     * 
     * @param supplierLastUpdate Set in "supplierLastUpdate".
     */
    public void setSupplierLastUpdate(Timestamp supplierLastUpdate) {
        this.supplierLastUpdate = supplierLastUpdate;
    }


}