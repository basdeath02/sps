/*
 * ModifyDate Development company    Describe 
 * 2014/07/22 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class ASN Progress Information Domain.
 * @author CSI
 */
public class AsnProgressInformationDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7537369753241198349L;
    
    /** The file id. */
    private String fileId;
    
    /** The file name. */
    private String fileName;
    
    /** The mode. */
    private String mode;
    
    /** The Actual Etd From. */
    private String actualEtdFrom;
    
    /** The Actual Etd To. */
    private String actualEtdTo;
    
    /** The Actual Eta From. */
    private String actualEtaFrom;
    
    /** The Actual Eta To. */
    private String actualEtaTo;
    
    /** The Invoice Date From. */
    private String invoiceDateFrom;
    
    /** The Invoice Date to. */
    private String invoiceDateTo;
    
    /** The plan eta from. */
    private String planEtaFrom;
    
    /** The plan eta to. */
    private String PlanEtaTo;

    /** The plan ETA time from. */
    private String planEtaTimeFrom;
    
    /** The plan ETA time to. */
    private String planEtaTimeTo;
    
    /** The S.CD. */
    private String sCd;
    
    /** The Vendor.CD. */
    private String vendorCd;
    
    /** The S.P.CD. */
    private String sPcd;
    
    /** The D.CD. */
    private String dCd;
    
    /** The D.P.CD. */
    private String dPcd;
    
    /** The ASN Status. */
    private String asnStatus;
    
    /** The ASN No. */
    private String asnNo;
    
    /** The Invoice No. */
    private String invoiceNo;
    
    /** The Invoice Status. */
    private String invoiceStatus;
    
    /** The S.Part No. */
    private String sPn;
    
    /** The D. Part No. */
    private String dPn;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The trans. */
    private String trans;
    
    /** The s tax id.*/
    private String sTaxId;
    
    /** The created invoice flag.*/
    private String createdInvoiceFlag;
    
    /** The data scope control list. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The supplier code list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The supplier plant code list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The Denso Code list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The Denso Plant Code list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The ASN Status list. */
    private List<MiscellaneousDomain> asnStatusList;
    
    /** The Invoice Status list. */
    private List<MiscellaneousDomain> invoiceStatusList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The ASN Progress Information  list. */
    private List<AsnProgressInformationReturnDomain> asnProgressInformationReturnDomains;
    
    /**
     * Instantiates a new Asn Progress Information domain.
     */
    public AsnProgressInformationDomain() {
        super();
    }


    /**
     * <p>Getter method for fileId.</p>
     *
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }


    /**
     * <p>Setter method for fileId.</p>
     *
     * @param fileId Set for fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }


    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }


    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    /**
     * <p>Getter method for mode.</p>
     *
     * @return the mode
     */
    public String getMode() {
        return mode;
    }


    /**
     * <p>Setter method for mode.</p>
     *
     * @param mode Set for mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }


    /**
     * <p>Getter method for actualEtdFrom.</p>
     *
     * @return the actualEtdFrom
     */
    public String getActualEtdFrom() {
        return actualEtdFrom;
    }


    /**
     * <p>Setter method for actualEtdFrom.</p>
     *
     * @param actualEtdFrom Set for actualEtdFrom
     */
    public void setActualEtdFrom(String actualEtdFrom) {
        this.actualEtdFrom = actualEtdFrom;
    }


    /**
     * <p>Getter method for actualEtdTo.</p>
     *
     * @return the actualEtdTo
     */
    public String getActualEtdTo() {
        return actualEtdTo;
    }


    /**
     * <p>Setter method for actualEtdTo.</p>
     *
     * @param actualEtdTo Set for actualEtdTo
     */
    public void setActualEtdTo(String actualEtdTo) {
        this.actualEtdTo = actualEtdTo;
    }


    /**
     * <p>Getter method for actualEtaFrom.</p>
     *
     * @return the actualEtaFrom
     */
    public String getActualEtaFrom() {
        return actualEtaFrom;
    }


    /**
     * <p>Setter method for actualEtaFrom.</p>
     *
     * @param actualEtaFrom Set for actualEtaFrom
     */
    public void setActualEtaFrom(String actualEtaFrom) {
        this.actualEtaFrom = actualEtaFrom;
    }


    /**
     * <p>Getter method for actualEtaTo.</p>
     *
     * @return the actualEtaTo
     */
    public String getActualEtaTo() {
        return actualEtaTo;
    }


    /**
     * <p>Setter method for actualEtaTo.</p>
     *
     * @param actualEtaTo Set for actualEtaTo
     */
    public void setActualEtaTo(String actualEtaTo) {
        this.actualEtaTo = actualEtaTo;
    }


    /**
     * <p>Getter method for invoiceDateFrom.</p>
     *
     * @return the invoiceDateFrom
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }


    /**
     * <p>Setter method for invoiceDateFrom.</p>
     *
     * @param invoiceDateFrom Set for invoiceDateFrom
     */
    public void setInvoiceDateFrom(String invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }


    /**
     * <p>Getter method for invoiceDateTo.</p>
     *
     * @return the invoiceDateTo
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }


    /**
     * <p>Setter method for invoiceDateTo.</p>
     *
     * @param invoiceDateTo Set for invoiceDateTo
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }

    /**
     * <p>Getter method for plan eta from.</p>
     *
     * @return the plan eta from
     */
    public String getPlanEtaFrom() {
        return planEtaFrom;
    }

    /**
     * <p>Setter method for plan eta from.</p>
     *
     * @param planEtaFrom Set for plan eta from
     */
    public void setPlanEtaFrom(String planEtaFrom) {
        this.planEtaFrom = planEtaFrom;
    }

    /**
     * <p>Getter method for plan eta to.</p>
     *
     * @return the plan eta to
     */
    public String getPlanEtaTo() {
        return PlanEtaTo;
    }

    /**
     * <p>Setter method for plan eta to.</p>
     *
     * @param planEtaTo Set for plan eta to
     */
    public void setPlanEtaTo(String planEtaTo) {
        PlanEtaTo = planEtaTo;
    }

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }


    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }


    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }


    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }


    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }


    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }


    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }


    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }


    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }


    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }


    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }


    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }


    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }


    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * <p>Getter method for invoiceStatus.</p>
     *
     * @return the invoiceStatus
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * <p>Setter method for invoiceStatus.</p>
     *
     * @param invoiceStatus Set for invoiceStatus
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * <p>Getter method for supplier part no.</p>
     *
     * @return the supplier part no
     */
    public String getSPn() {
        return sPn;
    }


    /**
     * <p>Setter method for supplier part no.</p>
     *
     * @param sPn Set for supplier part no
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }


    /**
     * <p>Getter method for denso part no.</p>
     *
     * @return the denso part no
     */
    public String getDPn() {
        return dPn;
    }


    /**
     * <p>Setter method for denso part no.</p>
     *
     * @param dPn Set for denso part no
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for sps do no.</p>
     *
     * @return the sps do no
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>Setter method for sps do no.</p>
     *
     * @param spsDoNo Set for sps do no
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>Getter method for trans.</p>
     *
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * <p>Setter method for trans.</p>
     *
     * @param trans Set for trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }
    
    /**
     * <p>Getter method for supplier tax id.</p>
     *
     * @return the supplier tax id
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * <p>Setter method for supplier tax id.</p>
     *
     * @param sTaxId Set for supplier tax id
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }
    
    /**
     * <p>Getter method for createdInvoiceFlag.</p>
     *
     * @return the createdInvoiceFlag
     */
    public String getCreatedInvoiceFlag() {
        return createdInvoiceFlag;
    }


    /**
     * <p>Setter method for createdInvoiceFlag.</p>
     *
     * @param createdInvoiceFlag Set for createdInvoiceFlag
     */
    public void setCreatedInvoiceFlag(String createdInvoiceFlag) {
        this.createdInvoiceFlag = createdInvoiceFlag;
    }


    /**
     * <p>Getter method for dataScopeControlDomain.</p>
     *
     * @return the dataScopeControlDomain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }


    /**
     * <p>Setter method for dataScopeControlDomain.</p>
     *
     * @param dataScopeControlDomain Set for dataScopeControlDomain
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }


    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }


    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }


    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }


    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }


    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }


    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }


    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }


    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }


    /**
     * <p>Getter method for asnStatusList.</p>
     *
     * @return the asnStatusList
     */
    public List<MiscellaneousDomain> getAsnStatusList() {
        return asnStatusList;
    }


    /**
     * <p>Setter method for asnStatusList.</p>
     *
     * @param asnStatusList Set for asnStatusList
     */
    public void setAsnStatusList(List<MiscellaneousDomain> asnStatusList) {
        this.asnStatusList = asnStatusList;
    }
    
    /**
     * <p>Getter method for invoiceStatusList.</p>
     *
     * @return the invoiceStatusList
     */
    public List<MiscellaneousDomain> getInvoiceStatusList() {
        return invoiceStatusList;
    }

    /**
     * <p>Setter method for invoiceStatusList.</p>
     *
     * @param invoiceStatusList Set for invoiceStatusList
     */
    public void setInvoiceStatusList(List<MiscellaneousDomain> invoiceStatusList) {
        this.invoiceStatusList = invoiceStatusList;
    }

    /**
     * <p>Getter method for errorMessageList.</p>
     *
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }


    /**
     * <p>Setter method for errorMessageList.</p>
     *
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }


    /**
     * <p>Getter method for warningMessageList.</p>
     *
     * @return the warningMessageList
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }


    /**
     * <p>Setter method for warningMessageList.</p>
     *
     * @param warningMessageList Set for warningMessageList
     */
    public void setWarningMessageList(
        List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }


    /**
     * <p>Getter method for asnProgressInformationReturnDomains.</p>
     *
     * @return the asnProgressInformationReturnDomains
     */
    public List<AsnProgressInformationReturnDomain> getAsnProgressInformationReturnDomains() {
        return asnProgressInformationReturnDomains;
    }


    /**
     * <p>Setter method for asnProgressInformationReturnDomains.</p>
     *
     * @param asnProgressInformationReturnDomains Set for asnProgressInformationReturnDomains
     */
    public void setAsnProgressInformationReturnDomains(
        List<AsnProgressInformationReturnDomain> asnProgressInformationReturnDomains) {
        this.asnProgressInformationReturnDomains = asnProgressInformationReturnDomains;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }


    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }


    /**
     * <p>Getter method for planEtaTimeFrom.</p>
     *
     * @return the planEtaTimeFrom
     */
    public String getPlanEtaTimeFrom() {
        return planEtaTimeFrom;
    }


    /**
     * <p>Setter method for planEtaTimeFrom.</p>
     *
     * @param planEtaTimeFrom Set for planEtaTimeFrom
     */
    public void setPlanEtaTimeFrom(String planEtaTimeFrom) {
        this.planEtaTimeFrom = planEtaTimeFrom;
    }


    /**
     * <p>Getter method for planEtaTimeTo.</p>
     *
     * @return the planEtaTimeTo
     */
    public String getPlanEtaTimeTo() {
        return planEtaTimeTo;
    }

    /**
     * <p>Setter method for planEtaTimeTo.</p>
     *
     * @param planEtaTimeTo Set for planEtaTimeTo
     */
    public void setPlanEtaTimeTo(String planEtaTimeTo) {
        this.planEtaTimeTo = planEtaTimeTo;
    }
    
}