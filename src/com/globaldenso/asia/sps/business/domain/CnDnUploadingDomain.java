/*
 * ModifyDate Development company    Describe 
 * 2014/10/14 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * The Class CnDnUploading Domain.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class CnDnUploadingDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -4522374210684845646L;
    
    /** The company supplier list. */
    private List<MiscellaneousDomain> companySupplierList;

    /** The company denso list. */
    private List<MiscellaneousDomain> companyDensoList;

    /** The plant supplier list. */
    private List<PlantSupplierDomain> plantSupplierList;

    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;

    /**
     * <p>The constructor.</p>
     *
     */
    public CnDnUploadingDomain() {
        super();
        this.companyDensoList = new ArrayList<MiscellaneousDomain>();
        this.companySupplierList = new ArrayList<MiscellaneousDomain>();
        this.plantDensoList = new ArrayList<PlantDensoDomain>();
        this.plantSupplierList = new ArrayList<PlantSupplierDomain>();
    }

    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<MiscellaneousDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(List<MiscellaneousDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<MiscellaneousDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<MiscellaneousDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for errorMessageList.</p>
     *
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * <p>Setter method for errorMessageList.</p>
     *
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    
}
