/*
 * ModifyDate Development company     Describe 
 * 2023/03/16 CTC Rungsiwut           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class File Upload Domain.
 * 
 * @author CSI
 */
public class PurgingBhtTransmitLogDomain extends BaseDomain implements
        Serializable {

    /**
     * <p>
     * Type in the role of the field.
     * </p>
     */
    private static final long serialVersionUID = 2431986119030104188L;

    /** The date start purge. */
    private Date dateStartPurge;

    /** dCd */
    private String dCd;

    /**
     * Instantiates a new file upload domain.
     */
    public PurgingBhtTransmitLogDomain() {
        super();
    }


    /**
     * <p>Getter method for dateStartPurge.</p>
     *
     * @return the dateStartPurge
     */
    public Date getDateStartPurge() {
        return dateStartPurge;
    }


    /**
     * <p>Setter method for dateStartPurge.</p>
     *
     * @param dateStartPurge Set for dateStartPurge
     */
    public void setDateStartPurge(Date dateStartPurge) {
        this.dateStartPurge = dateStartPurge;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
}