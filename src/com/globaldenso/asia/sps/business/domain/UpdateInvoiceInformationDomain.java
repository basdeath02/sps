/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain;

/**
 * The class UpdateInvoiceInformationDomain.
 * Domain for Update Invoice Information.
 * 
 * @author CSI
 * */
public class UpdateInvoiceInformationDomain extends BaseDomain implements Serializable {

    /** The Generated Serial Version UID. */
    private static final long serialVersionUID = -3344544049237030911L;
    
    /** The Date Scope Date. */
    private String dataScopeDate;
    
    /** The DENSO company code. */
    private String dCd;
    
    /** The SPS_M_AS400_LPAR. */
    private SpsMAs400LparDomain spsMAs400LparDomain;

    /** The SPS_M_AS400_SCHEMA. */
    private SpsMAs400SchemaDomain spsMAs400SchemaDomain;
    
    /** The Default Constructor. */
    public UpdateInvoiceInformationDomain() {
        super();
    }

    /**
     * <p>Getter method for dataScopeDate.</p>
     *
     * @return the dataScopeDate
     */
    public String getDataScopeDate() {
        return dataScopeDate;
    }

    /**
     * <p>Setter method for dataScopeDate.</p>
     *
     * @param dataScopeDate Set for dataScopeDate
     */
    public void setDataScopeDate(String dataScopeDate) {
        this.dataScopeDate = dataScopeDate;
    }

    /**
     * <p>Getter method for spsMAs400LparDomain.</p>
     *
     * @return the spsMAs400LparDomain
     */
    public SpsMAs400LparDomain getSpsMAs400LparDomain() {
        return spsMAs400LparDomain;
    }

    /**
     * <p>Setter method for spsMAs400LparDomain.</p>
     *
     * @param spsMAs400LparDomain Set for spsMAs400LparDomain
     */
    public void setSpsMAs400LparDomain(SpsMAs400LparDomain spsMAs400LparDomain) {
        this.spsMAs400LparDomain = spsMAs400LparDomain;
    }

    /**
     * <p>Getter method for spsMAs400SchemaDomain.</p>
     *
     * @return the spsMAs400SchemaDomain
     */
    public SpsMAs400SchemaDomain getSpsMAs400SchemaDomain() {
        return spsMAs400SchemaDomain;
    }

    /**
     * <p>Setter method for spsMAs400SchemaDomain.</p>
     *
     * @param spsMAs400SchemaDomain Set for spsMAs400SchemaDomain
     */
    public void setSpsMAs400SchemaDomain(SpsMAs400SchemaDomain spsMAs400SchemaDomain) {
        this.spsMAs400SchemaDomain = spsMAs400SchemaDomain;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

}
