/*
 * ModifyDate Development company Describe 
 * 2014/07/21 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class UserRoleDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserRoleDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -241185620092791005L;

    /**
     * Role code
     */
    private String roleCd;

    /**
     * DENSO user DSC ID
     */
    private String dscId;

    /**
     * Sequence Number
     */
    private BigDecimal seqNo;

    /**
     * DENSO company code
     */
    private String dCd;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * Supplier company code
     */
    private String sCd;

    /**
     * Supplier plant code
     */
    private String sPcd;
    
    /**
     * Effective start date
     */
    private Timestamp effectStart;

    /**
     * Effective end date
     */
    private Timestamp effectEnd;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create datetime
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;

    /** The default constructor. */
    public UserRoleDomain() {
        super();
    }

    /**
     * Get method for roleCd.
     * @return the roleCd
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Set method for roleCd.
     * @param roleCd the roleCd to set
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Get method for dscId.
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Set method for dscId.
     * @param dscId the dscId to set
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Get method for seqNo.
     * @return the seqNo
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Set method for seqNo.
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Get method for DENSO company code.
     * @return the DENSO company code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Set method for DENSO company code.
     * @param dCd the DENSO company code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * Get method for DENSO plant code.
     * @return the DENSO plant code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Set method for DENSO plant code.
     * @param dPcd the DENSO plant code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * Get method for supplier company code.
     * @return the supplier company code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Set method for supplier company code.
     * @param sCd the supplier company code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Get method for supplier plant code.
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Set method for supplier plant code.
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * Getter method of "effectStart"
     * 
     * @return the effectStart
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart"
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd"
     * 
     * @return the effectEnd
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd"
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
