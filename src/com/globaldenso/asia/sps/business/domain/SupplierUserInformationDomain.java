/*
 * ModifyDate Development company     Describe 
 * 2014/04/23 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Supplier User Information Domain.
 * @author CSI
 */
public class SupplierUserInformationDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1402927522052346251L;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID selected. */
    private String dscIdSelected;
    
    /** The Company Supplier Code. */
    private String sCd;
   
    /** The Plant Supplier . */
    private String sPcd;
    
    /** The Vendor Code. */
    private String vendorCd;
    
    /** The first name */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private String createDatetime;
    
    /** The Last update User. */
    private String updateUser;
            
    /** The Date time update. */
    private String updateDatetime;
    
    /** The Information Flag. */
    private String informationFlag;
            
    /** The Role Flag. */
    private String roleFlag;
    
    /** The Date time update. */
    private Timestamp lastUpdateDatetime;
    
    /** The Data Scope Control Domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The supplier user Detail Domain.*/
    private UserSupplierDetailDomain userSupplierDetailDomain;
    
    /** The user supplier Domain.*/
    private UserSupplierDomain userSupplierDomain;
    
    /** The user Domain.*/
    private UserDomain userDomain;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /** The List of Company Supplier domain. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The List of Plant Supplier domain. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of Supplier User Info domain. */
    private List<SupplierUserInformationDomain> userSupplierInfoList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    /** The department name. */
    private String departmentName; 
       
    /** The Role Code. */
    private String roleCode; 
    
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The Active flag. */
    private String isActive; 
    
    /** The DENSO owner. */
    private String densoOwner;
    
    /** The Email urgent order flag. */
    private String emlUrgentFlag;
    
    /** The Email supplier info not found.*/
    private String emlInfoFlag;
    
    /** The Email allow revise flag. */
    private String emlAllowRevise;
    
    /** The Email cancel invoice flag. */
    private String emlCancelInvoice;
    
    /** The role Domain.*/
    private RoleDomain roleDomain;
    
    /** The effect Start Screen. */
    private String effectStartScreen;
    
    /** The effect End Screen. */
    private String effectEndScreen;
    
    /** The effect Start Screen. */
    private String createDatetimeScreen;
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public SupplierUserInformationDomain() {
        super();       
        userDomain = new UserDomain();
        userSupplierDetailDomain = new UserSupplierDetailDomain();
        roleDomain = new RoleDomain();
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the Company Supplier Code.
     * 
     * @return the Company Supplier Code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Sets the Company Supplier Code.
     * 
     * @param sCd the Company Supplier Code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Gets the Plant Supplier Code.
     * 
     * @return the Plant Supplier Code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the Plant Supplier Code.
     * 
     * @param sPcd the Plant Supplier Code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    /**
     * Gets the DENSO Owner.
     * 
     * @return the DENSO Owner.
     */
    public String getDensoOwner() {
        return densoOwner;
    }

    /**
     * Sets the DENSO Owner.
     * 
     * @param densoOwner the DENSO Owner.
     */
    public void setDensoOwner(String densoOwner) {
        this.densoOwner = densoOwner;
    }
    
    /**
     * Gets the department Name.
     * 
     * @return the department Name.
     */
    public String getDepartmentName() {
        return departmentName;
    }
    /**
     * Sets the department Name.
     * 
     * @param departmentName the department Name.
     */

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets the is Active flag.
     * 
     * @return the isActive flag.
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the is Active flag.
     * 
     * @param isActive the is Active flag.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public String getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public String getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(String updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    /**
     * Gets the email urgent flag.
     * 
     * @return the email urgent flag.
     */
    public String getEmlUrgentFlag() {
        return emlUrgentFlag;
    }

    /**
     * Sets the email urgent flag.
     * 
     * @param emlUrgentFlag the email urgent flag.
     */
    public void setEmlUrgentFlag(String emlUrgentFlag) {
        this.emlUrgentFlag = emlUrgentFlag;
    }

    /**
     * Gets the Email supplier info not found flag.
     * 
     * @return the Email supplier info not found flag.
     */
    public String getEmlInfoFlag() {
        return emlInfoFlag;
    }

    /**
     * Sets the Email supplier info not found flag.
     * 
     * @param emlInfoFlag the Email supplier info not found flag.
     */
    public void setEmlInfoFlag(String emlInfoFlag) {
        this.emlInfoFlag = emlInfoFlag;
    }
    
    /**
     * Gets the email allow revise flag.
     * 
     * @return the email allow revise flag
     */
    public String getEmlAllowRevise() {
        return emlAllowRevise;
    }

    /**
     * Sets the email allow revise flag.
     * 
     * @param emlAllowRevise the email allow revise flag
     */
    public void setEmlAllowRevise(String emlAllowRevise) {
        this.emlAllowRevise = emlAllowRevise;
    }

    /**
     * Gets the email cancel invoice flag.
     * 
     * @return the email cancel invoice flag
     */
    public String getEmlCancelInvoice() {
        return emlCancelInvoice;
    }

    /**
     * Sets the email cancel invoice flag.
     * 
     * @param emlCancelInvoice the email cancel invoice flag
     */
    public void setEmlCancelInvoice(String emlCancelInvoice) {
        this.emlCancelInvoice = emlCancelInvoice;
    }

    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }
    
    /**
     * Gets the role Domain.
     * 
     * @return the roleDomain.
     */  
    public RoleDomain getRoleDomain() {
        return roleDomain;
    }

    /**
     * Sets the role Domain.
     * 
     * @param roleDomain the role Domain.
     */
    public void setRoleDomain(RoleDomain roleDomain) {
        this.roleDomain = roleDomain;
    }
    
    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }

    /**
     * Gets the Effect Start Screen.
     * 
     * @return the getEffectStartScreen.
     */ 
    public String getEffectStartScreen() {
        return effectStartScreen;
    }

    /**
     * Sets the effect Start Screen.
     * 
     * @param effectStartScreen the new effect Start Screen.
     */
    public void setEffectStartScreen(String effectStartScreen) {
        this.effectStartScreen = effectStartScreen;
    }

    /**
     * Gets the effect End Screen.
     * 
     * @return the effectEndScreen.
     */ 
    public String getEffectEndScreen() {
        return effectEndScreen;
    }

    /**
     * Sets the effect End Screen.
     * 
     * @param effectEndScreen the new effect End Screen.
     */
    public void setEffectEndScreen(String effectEndScreen) {
        this.effectEndScreen = effectEndScreen;
    }

    /**
     * Gets the create Date time Screen
     * 
     * @return the createDatetimeScreen.
     */ 
    public String getCreateDatetimeScreen() {
        return createDatetimeScreen;
    }

    /**
     * Sets the create Date time Screen.
     * 
     * @param createDatetimeScreen the new create Date time Screen.
     */
    public void setCreateDatetimeScreen(String createDatetimeScreen) {
        this.createDatetimeScreen = createDatetimeScreen;
    }

    /**
     * Gets the role Code.
     * 
     * @return the role Code.
     */ 
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the role Code.
     * 
     * @param roleCode the new role Code.
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    /**
     * Gets the dscId Selected.
     * 
     * @return the dscId Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }

    /**
     * Sets the dscId Selected.
     * 
     * @param dscIdSelected the new dscId Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    
    /**
     * Gets the list of user supplier information.
     * 
     * @return the list of user supplier information.
     */
    public List<SupplierUserInformationDomain> getUserSupplierInfoList() {
        return userSupplierInfoList;
    }

    /**
     * Sets the list of user supplier information.
     * 
     * @param userSupplierInfoList the list of user supplier information.
     */
    public void setUserSupplierInfoList(List<SupplierUserInformationDomain> userSupplierInfoList){
        this.userSupplierInfoList = userSupplierInfoList;
    }

    /**
     * Gets the supplier plant code
     * 
     * @return the supplier plant code
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }
    
    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }
    
    /**
     * Gets the Company Supplier list.
     * 
     * @return the Company Supplier list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }
    /**
     * Sets the Company Supplier list.
     * 
     * @param companySupplierList the Company Supplier list.
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }
    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    
    /**
     * Gets the Last Update date.
     * 
     * @return the Last Update date.
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Sets the Last Update date.
     * 
     * @param lastUpdateDatetime the Last Update date.
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Gets the information flag.
     * 
     * @return the information flag.
     */
    public String getInformationFlag() {
        return informationFlag;
    }
    /**
     * Sets the information flag.
     * 
     * @param informationFlag the information flag.
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }
    /**
     * Gets the role flag.
     * 
     * @return the role flag.
     */
    public String getRoleFlag() {
        return roleFlag;
    }
    /**
     * Sets the role flag.
     * 
     * @param roleFlag the role flag.
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Gets the User Supplier Domain.
     * 
     * @return the User Supplier Domain.
     */
    public UserSupplierDomain getUserSupplierDomain() {
        return userSupplierDomain;
    }
    /**
     * Sets the User Supplier Domain.
     * 
     * @param userSupplierDomain the User Supplier Domain.
     */
    public void setUserSupplierDomain(UserSupplierDomain userSupplierDomain) {
        this.userSupplierDomain = userSupplierDomain;
    }
    /**
     * Gets the User Supplier Detail Domain.
     * 
     * @return the User Supplier Detail Domain.
     */
    public UserSupplierDetailDomain getUserSupplierDetailDomain() {
        return userSupplierDetailDomain;
    }
    
    /**
     * Sets the User Supplier Detail Domain.
     * 
     * @param userSupplierDetailDomain the User Supplier Detail Domain.
     */
    public void setUserSupplierDetailDomain(
        UserSupplierDetailDomain userSupplierDetailDomain) {
        this.userSupplierDetailDomain = userSupplierDetailDomain;
    }
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(
        List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }
    
    /**
     * Gets the Vendor Code.
     * 
     * @return the Vendor Code.
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * Sets the Vendor Code.
     * 
     * @param vendorCd the Vendor Code.
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    /**
     * Gets the Plant Supplier.
     * 
     * @return the Plant Supplier.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }
    /**
     * Sets the Plant Supplier.
     * 
     * @param plantSupplierList the Plant Supplier.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

}