/*
 * ModifyDate Development company Describe 
 * 2014/07/02 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;

/**
 * The Class UserLoginDomain. Domain for keep login user information.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserLoginDomain extends BaseDomain implements Serializable {
    
    /** The generated serial version UID. */
    private static final long serialVersionUID = -7240549242732592401L;

    /** The user type. */
    private String userType;

    /** The login user information. */
    private SpsMUserDomain spsMUserDomain;
    
    /** The more information if user from DENSO company. */
    private UserDensoDetailDomain userDensoDetailDomain;

    /** The more information if user from Supplier company. */
    private UserSupplierDetailDomain userSupplierDetailDomain;
    
    /** The domain of data scope control. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The list of screens (and roles) that user has permission. */
    private List<RoleScreenDomain> roleScreenDomainList;
    
    /** The list of menu that user has permission. */
    private List<MenuWithItemDomain> menuWithItemDoaminList;
    
    /** The list of menu that menu that user has permission. */
    private List<SpsMMenuDomain> spsMMenuDomainlist;
    
    /** The list of Button/Link Name that user can operate. */
    private List<String> buttonLinkNameList;
    
    /** The default constructor. */
    public UserLoginDomain() {
        this.spsMUserDomain = new SpsMUserDomain();
        this.userDensoDetailDomain = new UserDensoDetailDomain();
        this.userSupplierDetailDomain = new UserSupplierDetailDomain();
    }
    
    /**
     * <p>Getter method for user type.</p>
     *
     * @return the user type
     */
    public String getUserType() {
        return userType;
    }

    /**
     * <p>Setter method for user type.</p>
     *
     * @param userType Set for user type
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * <p>Getter method for spsMUserDomain.</p>
     *
     * @return the spsMUserDomain
     */
    public SpsMUserDomain getSpsMUserDomain() {
        return spsMUserDomain;
    }

    /**
     * <p>Setter method for spsMUserDomain.</p>
     *
     * @param spsMUserDomain Set for spsMUserDomain
     */
    public void setSpsMUserDomain(SpsMUserDomain spsMUserDomain) {
        this.spsMUserDomain = spsMUserDomain;
    }

    /**
     * <p>Getter method for userDensoDetailDomain.</p>
     *
     * @return the userDensoDetailDomain
     */
    public UserDensoDetailDomain getUserDensoDetailDomain() {
        return userDensoDetailDomain;
    }

    /**
     * <p>Setter method for userDensoDetailDomain.</p>
     *
     * @param userDensoDetailDomain Set for userDensoDetailDomain
     */
    public void setUserDensoDetailDomain(UserDensoDetailDomain userDensoDetailDomain) {
        this.userDensoDetailDomain = userDensoDetailDomain;
    }

    /**
     * <p>Getter method for userSupplierDetailDomain.</p>
     *
     * @return the userSupplierDetailDomain
     */
    public UserSupplierDetailDomain getUserSupplierDetailDomain() {
        return userSupplierDetailDomain;
    }

    /**
     * <p>Setter method for userSupplierDetailDomain.</p>
     *
     * @param userSupplierDetailDomain Set for userSupplierDetailDomain
     */
    public void setUserSupplierDetailDomain(
        UserSupplierDetailDomain userSupplierDetailDomain) {
        this.userSupplierDetailDomain = userSupplierDetailDomain;
    }
    
    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }

    /**
     * <p>Getter method for roleScreenDomainList.</p>
     *
     * @return the roleScreenDomainList
     */
    public List<RoleScreenDomain> getRoleScreenDomainList() {
        return roleScreenDomainList;
    }

    /**
     * <p>Setter method for roleScreenDomainList.</p>
     *
     * @param roleScreenDomainList Set for roleScreenDomainList
     */
    public void setRoleScreenDomainList(List<RoleScreenDomain> roleScreenDomainList) {
        this.roleScreenDomainList = roleScreenDomainList;
    }

    /**
     * Get method for menuWithItemDoaminList.
     * @return the menuWithItemDoaminList
     */
    public List<MenuWithItemDomain> getMenuWithItemDoaminList() {
        return menuWithItemDoaminList;
    }

    /**
     * Set method for menuWithItemDoaminList.
     * @param menuWithItemDoaminList the menuWithItemDoaminList to set
     */
    public void setMenuWithItemDoaminList(
        List<MenuWithItemDomain> menuWithItemDoaminList) {
        this.menuWithItemDoaminList = menuWithItemDoaminList;
    }
    /**
     * Get method for menu domain List.
     * @return the spsMMenuDomainlist
     */
    public List<SpsMMenuDomain> getSpsMMenuDomainlist() {
        return spsMMenuDomainlist;
    }
    /**
     * Set method for menu domain List.
     * @param spsMMenuDomainlist the menu domain List.
     */
    public void setSpsMMenuDomainlist(List<SpsMMenuDomain> spsMMenuDomainlist) {
        this.spsMMenuDomainlist = spsMMenuDomainlist;
    }

    /**
     * <p>Getter method for buttonLinkNameList.</p>
     *
     * @return the buttonLinkNameList
     */
    public List<String> getButtonLinkNameList() {
        return buttonLinkNameList;
    }

    /**
     * <p>Setter method for buttonLinkNameList.</p>
     *
     * @param buttonLinkNameList Set for buttonLinkNameList
     */
    public void setButtonLinkNameList(List<String> buttonLinkNameList) {
        this.buttonLinkNameList = buttonLinkNameList;
    }

}
