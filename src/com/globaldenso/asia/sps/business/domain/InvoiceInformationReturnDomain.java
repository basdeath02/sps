/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Invoice Information Return Domain.
 * @author CSI
 */
public class InvoiceInformationReturnDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3857775057146425189L;
    
    /** The list of invoice Information domain. */
    private List<InvoiceInformationDomain> invoiceInformationDomainList;
    
    /** The file name. */
    private String fileName;
    
    /** The csv result. */
    private StringBuffer csvResult;
    
    /** The invoice no error result. */
    private String invNoErrorResult;
    
    /** The supplier code authen list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The denso code authen list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The denso supplier relation list. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;
    
    /** The invoice status list. */
    private List<MiscellaneousDomain> invoiceStatusList;
    
    /** The invoice rcv status list. */
    private List<MiscellaneousDomain> invoiceRcvStatusList;
    
    /** The success message list. */
    private List<ApplicationMessageDomain> successMessageList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new Invoice Information Return Domain.
     */
    public InvoiceInformationReturnDomain() {
        super();
        errorMessageList = new ArrayList<ApplicationMessageDomain>();
        successMessageList = new ArrayList<ApplicationMessageDomain>();
    }
    
    /**
     * Gets the list of Invoice Information Domain.
     * 
     * @return the asnInformationDomainList.
     */
    public List<InvoiceInformationDomain> getInvoiceInformationDomainList() {
        return invoiceInformationDomainList;
    }
    
    /**
     * Sets the list of Invoice Information Domain.
     * 
     * @param invoiceInformationDomainList list of Invoice Information Domain.
     */  
    public void setInvoiceInformationDomainList(List<InvoiceInformationDomain> invoiceInformationDomainList) {
        this.invoiceInformationDomainList = invoiceInformationDomainList;
    }

    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for csvResult.</p>
     *
     * @return the csvResult
     */
    public StringBuffer getCsvResult() {
        return csvResult;
    }

    /**
     * <p>Setter method for csvResult.</p>
     *
     * @param csvResult Set for csvResult
     */
    public void setCsvResult(StringBuffer csvResult) {
        this.csvResult = csvResult;
    }
    
    /**
     * <p>Getter method for invNoErrorResult.</p>
     *
     * @return the invNoErrorResult
     */
    public String getInvNoErrorResult() {
        return invNoErrorResult;
    }

    /**
     * <p>Setter method for invNoErrorResult.</p>
     *
     * @param invNoErrorResult Set for invNoErrorResult
     */
    public void setInvNoErrorResult(String invNoErrorResult) {
        this.invNoErrorResult = invNoErrorResult;
    }

    /**
     * Gets the supplier code list.
     * 
     * @return the supplier code list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the supplier code list.
     * 
     * @param companySupplierList the supplier code list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the denso code list.
     * 
     * @return the denso code list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso code list.
     * 
     * @param companyDensoList the denso code list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso list.
     * 
     * @return the company denso list.
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * Sets the denso supplier relation list.
     * 
     * @param densoSupplierRelationList the denso supplier relation list.
     */
    public void setDensoSupplierRelationList(List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * Gets the invoice status list.
     * 
     * @return the invoice status list.
     */
    public List<MiscellaneousDomain> getInvoiceStatusList() {
        return invoiceStatusList;
    }

    /**
     * Sets the invoice status list.
     * 
     * @param invoiceStatusList the invoice status list.
     */
    public void setInvoiceStatusList(List<MiscellaneousDomain> invoiceStatusList) {
        this.invoiceStatusList = invoiceStatusList;
    }
    
    /**
     * Gets the invoice rcv status list.
     * 
     * @return the invoice rcv status list.
     */
    public List<MiscellaneousDomain> getInvoiceRcvStatusList() {
        return invoiceRcvStatusList;
    }

    /**
     * Sets the invoice rcv status list.
     * 
     * @param invoiceRcvStatusList the invoice rcv status list.
     */
    public void setInvoiceRcvStatusList(List<MiscellaneousDomain> invoiceRcvStatusList) {
        this.invoiceRcvStatusList = invoiceRcvStatusList;
    }
    
    /**
     * Gets the success message list.
     * 
     * @return the success message list.
     */
    public List<ApplicationMessageDomain> getSuccessMessageList() {
        return successMessageList;
    }

    /**
     * Sets the success message list.
     * 
     * @param successMessageList the success message list.
     */
    public void setSuccessMessageList(List<ApplicationMessageDomain> successMessageList) {
        this.successMessageList = successMessageList;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * Gets the plant denso list.
     * 
     * @return the plant denso  list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the plant denso  list.
     * 
     * @param plantDensoList the plant denso  list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
}