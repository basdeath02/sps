/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class SupplierUserRegistrationDomain.
 * @author CSI
 */
public class SupplierUserRoleAssignmentDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7030294736003208555L;

    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID selected. */
    private String dscIdSelected; 
    
    /** The Company Supplier Code. */
    private String sCd;
   
    /** The Plant Supplier. */
    private String sPcd;
    
    /** The Vendor Code. */
    private String vendorCd;
    
    /** The The effect start for screen. */
    private String effectStartScreen;
    
    /** The The effect end for screen. */
    private String effectEndScreen;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private String createDatetime;
    
    /** The Last update User. */
    private String updateUser;
    
    /** The Date time update. */
    private String updateDatetime;
    
    /** The Role Flag. */
    private String roleFlag;
    
    /** The Active flag. */
    private String isActive;
    
    /** The Role Code. */
    private String roleCode;
    
    /** The Date time update. */
    private Timestamp lastUpdateDatetime;
    
    /** The isDcompany flag. */
    private Boolean isDCompany;
    
    /** The mode. */
    private String mode;
    
    /** The Data Scope Control Domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The user supplier Detail Domain.*/
    private UserSupplierDetailDomain userSupplierDetailDomain;
    
    /** The user supplier Domain.*/
    private UserSupplierDomain userSupplierDomain;
    
    /** The email user Domain.*/
    private UserDomain userDomain;
    
    /** The Plant Supplier With Scope Domain.*/
    private PlantSupplierWithScopeDomain plantSupplierWithScopeDomain;
    
    /** The list of department name.*/
    private List<String> DepartmentNameList;
    
    /** The List of Company Supplier domain. */
    private List<SpsMCompanySupplierDomain> companySupplierList;
    
    /** The List of Plant Supplier domain. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of Company DENSO domain. */
    private List<SpsMCompanyDensoDomain> spsMCompanyDensoList;
    
    /** The List of Supplier User RoleAssignment domain. */
    private List<SupplierUserRoleAssignmentDomain> userSupplierRoleAssignList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;

    /**
     * Instantiates a new Supplier user domain.
     */
    public SupplierUserRoleAssignmentDomain() {
        super();       
        userDomain = new UserDomain();
        userSupplierDetailDomain = new UserSupplierDetailDomain();
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the Company Supplier Code.
     * 
     * @return the Company Supplier Code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Sets the Company Supplier Code.
     * 
     * @param sCd the Company Supplier Code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Gets the Plant Supplier Code.
     * 
     * @return the Plant Supplier Code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the Plant Supplier Code.
     * 
     * @param sPcd the Plant Supplier Code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
 
    /**
     * Gets the is Active flag.
     * 
     * @return the isActive flag.
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the is Active flag.
     * 
     * @param isActive the is Active flag.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public String getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public String getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(String updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }
    
    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
    /**
     * Gets the dscId Selected.
     * 
     * @return the dscId Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }

    /**
     * Sets the dscId Selected.
     * 
     * @param dscIdSelected the new dscId Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    
    /**
     * Gets the list of user supplier role assignment detail.
     * 
     * @return the list of user supplier role assignment detail.
     */
    public List<SupplierUserRoleAssignmentDomain> getUserSupplierRoleAssignList() {
        return userSupplierRoleAssignList;
    }

    /**
     * Sets the list of user supplier role assignment detail.
     * 
     * @param userSupplierRoleAssignList the list of user supplier role assignment detail.
     */
    public void setUserSupplierRoleAssignList(
        List<SupplierUserRoleAssignmentDomain> userSupplierRoleAssignList) {
        this.userSupplierRoleAssignList = userSupplierRoleAssignList;
    }
    
    /**
     * Gets the Company Supplier list.
     * 
     * @return the Company Supplier list.
     */
    public List<SpsMCompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }
    /**
     * Sets the Company Supplier list.
     * 
     * @param companySupplierList the Company Supplier list.
     */
    public void setCompanySupplierList(
        List<SpsMCompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }
    /**
     * Gets the Plant Supplier list.
     * 
     * @return the Plant Supplier list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }
    /**
     * Sets the Plant Supplier list.
     * 
     * @param plantSupplierList the Plant Supplier list.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * Gets the Company DENSO list.
     * 
     * @return the Company DENSO list.
     */
    public List<SpsMCompanyDensoDomain> getSpsMCompanyDensoList() {
        return spsMCompanyDensoList;
    }

    /**
     * Sets the Company DENSO list.
     * 
     * @param spsMCompanyDensoList the Company DENSO list.
     */
    public void setSpsMCompanyDensoList(
        List<SpsMCompanyDensoDomain> spsMCompanyDensoList) {
        this.spsMCompanyDensoList = spsMCompanyDensoList;
    }

    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    
    /**
     * Gets the Last Update date.
     * 
     * @return the Last Update date.
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Sets the Last Update date.
     * 
     * @param lastUpdateDatetime the Last Update date.
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Gets the role flag.
     * 
     * @return the role flag.
     */
    public String getRoleFlag() {
        return roleFlag;
    }
    /**
     * Sets the role flag.
     * 
     * @param roleFlag the role flag.
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Gets the User Supplier Domain.
     * 
     * @return the User Supplier Domain.
     */
    public UserSupplierDomain getUserSupplierDomain() {
        return userSupplierDomain;
    }
    /**
     * Sets the User Supplier Domain.
     * 
     * @param userSupplierDomain the User Supplier Domain.
     */
    public void setUserSupplierDomain(UserSupplierDomain userSupplierDomain) {
        this.userSupplierDomain = userSupplierDomain;
    }
    /**
     * Gets the User Supplier Detail Domain.
     * 
     * @return the User Supplier Detail Domain.
     */
    public UserSupplierDetailDomain getUserSupplierDetailDomain() {
        return userSupplierDetailDomain;
    }
    
    /**
     * Sets the User Supplier Detail Domain.
     * 
     * @param userSupplierDetailDomain the User Supplier Detail Domain.
     */
    public void setUserSupplierDetailDomain(UserSupplierDetailDomain userSupplierDetailDomain) {
        this.userSupplierDetailDomain = userSupplierDetailDomain;
    }
    /**
     * Gets the list of department name.
     * 
     * @return the list of department name.
     */
    public List<String> getDepartmentNameList() {
        return DepartmentNameList;
    }
    
    /**
     * Sets the list of department name.
     * 
     * @param departmentNameList the list of department name.
     */
    public void setDepartmentNameList(List<String> departmentNameList) {
        DepartmentNameList = departmentNameList;
    }

    /**
     * Gets the Plant Supplier With Scope Domain.
     * 
     * @return the Plant Supplier With Scope Domain.
     */
    public PlantSupplierWithScopeDomain getPlantSupplierWithScopeDomain() {
        return plantSupplierWithScopeDomain;
    }

    /**
     * Sets the Plant Supplier With Scope Domain.
     * 
     * @param plantSupplierWithScopeDomain the Plant Supplier With Scope Domain.
     */
    public void setPlantSupplierWithScopeDomain(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain) {
        this.plantSupplierWithScopeDomain = plantSupplierWithScopeDomain;
    }
    /**
     * Gets the Effect start date for display on screen.
     * 
     * @return the Effect start date for display on screen.
     */
    public String getEffectStartScreen() {
        return effectStartScreen;
    }
    /**
     * Sets the Effect start date for display on screen.
     * 
     * @param effectStartScreen the Effect start date for display on screen.
     */
    public void setEffectStartScreen(String effectStartScreen) {
        this.effectStartScreen = effectStartScreen;
    }
    /**
     * Gets the Effect end date for display on screen.
     * 
     * @return the Effect end date for display on screen.
     */
    public String getEffectEndScreen() {
        return effectEndScreen;
    }
    /**
     * Sets the Effect end date for display on screen.
     * 
     * @param effectEndScreen the Effect end date for display on screen.
     */
    public void setEffectEndScreen(String effectEndScreen) {
        this.effectEndScreen = effectEndScreen;
    }
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(
        List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    /**
     * Gets the role code.
     * 
     * @return the role code.
     */
    public String getRoleCode() {
        return roleCode;
    }
    /**
     * Sets the role code.
     * 
     * @param roleCode the role code.
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
    
    /**
     * Gets the Vendor Code.
     * 
     * @return the Vendor Code.
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * Sets the Vendor Code.
     * 
     * @param vendorCd the Vendor Code.
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Gets the isDcompany flag.
     * 
     * @return the isDcompany flag.
     */
    public Boolean getIsDCompany() {
        return isDCompany;
    }
    /**
     * Sets the isDcompany flag.
     * 
     * @param isDCompany the isDcompany flag.
     */
    public void setIsDCompany(Boolean isDCompany) {
        this.isDCompany = isDCompany;
    }
    /**
     * Gets the mode flag.
     * 
     * @return the mode flag.
     */
    public String getMode() {
        return mode;
    }
    /**
     * Sets the mode flag.
     * 
     * @param mode the mode flag.
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
}