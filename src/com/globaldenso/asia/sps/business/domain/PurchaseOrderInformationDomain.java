/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;

/**
 * The Class Purchase Order Information Domain.
 * @author CSI
 */
public class PurchaseOrderInformationDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3707882764066589551L;

    /** The List of DENSO Supplier Relation.  */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;
    
    /** The List of PO Status */
    private List<MiscellaneousDomain> poStatusList;
    
    /** The List of PO Type */
    private List<MiscellaneousDomain> poTypeList;
    
    /** The List of Period Type */
    private List<MiscellaneousDomain> periodTypeList;
    
    /** The List of View PDF */
    private List<MiscellaneousDomain> viewPdfList;
    
    /** The List of Company Supplier. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The List of Company DENSO. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of PO domain. */
    private List<PurchaseOrderInformationDomain> poList;
    
    /** The List of Plant Supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of Plant DENSO. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The method*/
    private String method;
    
    /** The purchase order ID Selected*/
    private String poIdSelected;
    
    /** The purchase order status Selected*/
    private String poStatusSelected;
    
    /** The file Id that user selected from screen. */
    private String fileIdSelected;
    
    ///** Last update datetime Selected*/
    //private Timestamp lastUpdateDatetimeSelected;
    
    /** The issued date from. */
    private String issuedDateFrom;
    
    /** The issued date to. */
    private String issuedDateTo;
    
    // FIX : wrong format number
    /** The formated issue date to show in screen. */
    private String poIssuedDateShow;
    
    /** The PDF Original Flag. */
    private String pdfOriginal;
    
    /** The PDF Change Flag. */
    private String pdfChange;
    
    /** The view PDF */
    private String viewPdf;
    
    /** The view PDF Flag.*/
    private String viewPdfFlag;
    
    /** The SPS T PO Domain */
    private SpsTPoDomain spsTPoDomain;
    
    /** The SPS T PO Detail Domain */
    private SpsTPoDetailDomain spsTPoDetailDomain;

    /** The SPS PO Due Domain */
    private SpsTPoDueDomain spsTPoDueDomain;

    /** The purchase order ID */
    private String poId;

    /** The purchase order Issue Date */
    private Timestamp poIssueDate;

    /** The original SPS PO Number. */
    private String originalSpsPoNo;
    
    /** The previous SPS PO Number. */
    private String previousSpsPoNo;

    /** The period Type Name */
    private String periodTypeName;
    
    /** The release No. */
    private String releaseNo;
    
    /** The Transportation Mode. */
    private String tm;
    
    /** The Date time that get from System. */
    private String getDatetime;
    
    /** The List of Purchase Order Information Detail Domain. */
    private List<PurchaseOrderDetailDomain> purchaseOrderDetailList;
    
    /** Create user. */
    private String createDscId;

    /** Create date time. */
    private Timestamp createDatetime;

    /** Last update user. */
    private String lastUpdateDscId;

    /** Last update date time. */
    private Timestamp lastUpdateDatetime;
    
    /** The PO Issue Date in String type. */
    private String stringPoIssueDate;
    
    /** The Release Number in String type. */
    private String stringReleaseNo;
    
    /** The Data Type */
    private String dataType;

    /**
     * Instantiates a new Purchase Order Information Domain.
     */
    public PurchaseOrderInformationDomain() {
        super();
        this.spsTPoDomain = new SpsTPoDomain();
        this.spsTPoDetailDomain = new SpsTPoDetailDomain();
        this.spsTPoDueDomain = new SpsTPoDueDomain();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * <p>Getter method for densoSupplierRelationList.</p>
     *
     * @return the densoSupplierRelationList
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * <p>Setter method for densoSupplierRelationList.</p>
     *
     * @param densoSupplierRelationList Set for densoSupplierRelationList
     */
    public void setDensoSupplierRelationList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * Gets the list of purchase order status combo box.
     * 
     * @return the list of purchase order status combo box.
     */
    public List<MiscellaneousDomain> getPoStatusList() {
        return poStatusList;
    }
    /**
     * Sets the list of purchase order status combo box.
     * 
     * @param poStatusList the list of purchase order status combo box.
     */
    public void setPoStatusList(List<MiscellaneousDomain> poStatusList) {
        this.poStatusList = poStatusList;
    }

    /**
     * Gets the list of Purchase order type combo box.
     * 
     * @return the list of Purchase order type combo box.
     */
    public List<MiscellaneousDomain> getPoTypeList() {
        return poTypeList;
    }
    /**
     * Sets the list of Purchase order type combo box.
     * 
     * @param poTypeList the list of Purchase order type combo box.
     */
    public void setPoTypeList(List<MiscellaneousDomain> poTypeList) {
        this.poTypeList = poTypeList;
    }

    /**
     * Gets the list of period type combo box.
     * 
     * @return the list of period type combo box.
     */
    public List<MiscellaneousDomain> getPeriodTypeList() {
        return periodTypeList;
    }
    /**
     * Sets the list of period type combo box.
     * 
     * @param periodTypeList the list of period type combo box.
     */
    public void setPeriodTypeList(List<MiscellaneousDomain> periodTypeList) {
        this.periodTypeList = periodTypeList;
    }

    /**
     * Gets the list of View PDF combo box.
     * 
     * @return the list of View PDF combo box.
     */
    public List<MiscellaneousDomain> getViewPdfList() {
        return viewPdfList;
    }
    /**
     * Sets the list of View PDF combo box.
     * 
     * @param viewPdfList the list of View PDF combo box.
     */
    public void setViewPdfList(List<MiscellaneousDomain> viewPdfList) {
        this.viewPdfList = viewPdfList;
    }

    /**
     * Gets the issue date.  
     * 
     * @return the issue date.  
     */
    public String getIssuedDateFrom() {
        return issuedDateFrom;
    }
    /**
     * Sets the issue date.  
     * 
     * @param issuedDateFrom the issue date.  
     */
    public void setIssuedDateFrom(String issuedDateFrom) {
        this.issuedDateFrom = issuedDateFrom;
    }

    /**
     * Gets the issue date.  
     * 
     * @return the issue date.  
     */
    public String getIssuedDateTo() {
        return issuedDateTo;
    }
    /**
     * Sets the issue date.  
     * 
     * @param issuedDateTo the issue date.  
     */
    public void setIssuedDateTo(String issuedDateTo) {
        this.issuedDateTo = issuedDateTo;
    }
    /**
     * Gets the Period Type Name.
     * 
     * @return the Period Type Name.
     */
    public String getPeriodTypeName() {
        return periodTypeName;
    }

    /**
     * Sets the Period Type Name.
     * 
     * @param periodTypeName the Period Type Name.
     */
    public void setPeriodTypeName(String periodTypeName) {
        this.periodTypeName = periodTypeName;
    }
    /**
     * Gets the Purchase Order ID.
     * 
     * @return the Purchase Order ID.
     */
    public String getPoId() {
        return poId;
    }
    /**
     * Sets the Purchase Order ID.
     * 
     * @param poId the Purchase Order ID.
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }
    /**
     * Gets the list of purchase order.
     * 
     * @return the list of purchase order.
     */
    public List<PurchaseOrderInformationDomain> getPoList() {
        return poList;
    }
    /**
     * Sets the list of purchase order.
     * 
     * @param poList the list of purchase order.
     */
    public void setPoList(List<PurchaseOrderInformationDomain> poList) {
        this.poList = poList;
    }
    /**
     * Gets the create DSC ID.
     * 
     * @return the create DSC ID.
     */
    public String getCreateDscId() {
        return createDscId;
    }
    /**
     * Sets the create DSC ID.
     * 
     * @param createDscId the create DSC ID.
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }
    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }
    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }
    /**
     * Gets the Update DSC ID.
     * 
     * @return the Update DSC ID.
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }
    /**
     * Sets the Update DSC ID.
     * 
     * @param lastUpdateDscId the Update DSC ID.
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }
    /**
     * Gets the Update datetime.
     * 
     * @return the Update datetime.
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }
    /**
     * Sets the Update datetime.
     * 
     * @param lastUpdateDatetime the Update datetime.
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Gets the purchase order status selected.
     * 
     * @return the purchase order status selected.
     */
    public String getPoStatusSelected() {
        return poStatusSelected;
    }
    /**
     * Sets the purchase order status selected.
     * 
     * @param poStatusSelected the purchase order status selected.
     */
    public void setPoStatusSelected(String poStatusSelected) {
        this.poStatusSelected = poStatusSelected;
    }
    /**
     * Gets the Purchase order ID selected.
     * 
     * @return the Purchase order ID selected.
     */
    public String getPoIdSelected() {
        return poIdSelected;
    }
    /**
     * Sets the Purchase order ID selected.
     * 
     * @param poIdSelected the Purchase order ID selected.
     */
    public void setPoIdSelected(String poIdSelected) {
        this.poIdSelected = poIdSelected;
    }
//    /**
//     * Gets the Update datetime selected.
//     * 
//     * @return the Update datetime selected.
//     */
//    public Timestamp getLastUpdateDatetimeSelected() {
//        return lastUpdateDatetimeSelected;
//    }
//    /**
//     * Sets the Update datetime selected.
//     * 
//     * @param lastUpdateDatetimeSelected the Update datetime selected.
//     */
//    public void setLastUpdateDatetimeSelected(Timestamp lastUpdateDatetimeSelected) {
//        this.lastUpdateDatetimeSelected = lastUpdateDatetimeSelected;
//    }
    /**
     * Gets the method.
     * 
     * @return the method
     */
    public String getMethod() {
        return method;
    }
    /**
     * Sets the method
     * 
     * @param method the method.
     */
    public void setMethod(String method) {
        this.method = method;
    }
    
    /**
     * Gets the View PDF Flag.
     * 
     * @return the View PDF Flag.
     */
    public String getViewPdfFlag() {
        return viewPdfFlag;
    }
    /**
     * Sets the View PDF Flag.
     * 
     * @param viewPdfFlag the View PDF Flag.
     */
    public void setViewPdfFlag(String viewPdfFlag) {
        this.viewPdfFlag = viewPdfFlag;
    }
    /**
     * Gets the Purchase Order Issue Date.
     * 
     * @return the Purchase Order Issue Date.
     */
    public Timestamp getPoIssueDate() {
        return poIssueDate;
    }
    /**
     * Sets the Purchase Order Issue Date.
     * 
     * @param poIssueDate the Purchase Order Issue Date.
     */
    public void setPoIssueDate(Timestamp poIssueDate) {
        this.poIssueDate = poIssueDate;
    }
    /**
     * Gets the Release No.
     * 
     * @return the Release No.
     */
    public String getReleaseNo() {
        return releaseNo;
    }
    /**
     * Sets the Release No.
     * 
     * @param releaseNo the Release No.
     */
    public void setReleaseNo(String releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * Gets the Transportation Mode.
     * 
     * @return the Transportation Mode.
     */
    public String getTm() {
        return tm;
    }
    /**
     * Sets the Transportation Mode.
     * 
     * @param tm the Transportation Mode.
     */
    public void setTm(String tm) {
        this.tm = tm;
    }
    /**
     * Gets the Date time from system.
     * 
     * @return the Date time from system.
     */
    public String getGetDatetime() {
        return getDatetime;
    }
    /**
     * Sets the Date time from system.
     * 
     * @param getDatetime the Date time from system.
     */
    public void setGetDatetime(String getDatetime) {
        this.getDatetime = getDatetime;
    }
    /**
     * Gets the PDF Original.
     * 
     * @return the PDF Original.
     */
    public String getPdfOriginal() {
        return pdfOriginal;
    }
    /**
     * Sets the PDF Original.
     * 
     * @param pdfOriginal the PDF Original.
     */
    public void setPdfOriginal(String pdfOriginal) {
        this.pdfOriginal = pdfOriginal;
    }
    /**
     * Gets the PDF Changed.
     * 
     * @return the PDF Changed.
     */
    public String getPdfChange() {
        return pdfChange;
    }
    /**
     * Sets the PDF Changed.
     * 
     * @param pdfChange the PDF Changed.
     */
    public void setPdfChange(String pdfChange) {
        this.pdfChange = pdfChange;
    }
    /**
     * Gets the view PDF.
     * 
     * @return the view PDF.
     */
    public String getViewPdf() {
        return viewPdf;
    }
    /**
     * Sets the view PDF.
     * 
     * @param viewPdf the view PDF.
     */
    public void setViewPdf(String viewPdf) {
        this.viewPdf = viewPdf;
    }

    /**
     * Get method for companySupplierList.
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Set method for companySupplierList.
     * @param companySupplierList the companySupplierList to set
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Get method for companyDensoList.
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Set method for companyDensoList.
     * @param companyDensoList the companyDensoList to set
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for fileIdSelected.</p>
     *
     * @return the fileIdSelected
     */
    public String getFileIdSelected() {
        return fileIdSelected;
    }

    /**
     * <p>Setter method for fileIdSelected.</p>
     *
     * @param fileIdSelected Set for fileIdSelected
     */
    public void setFileIdSelected(String fileIdSelected) {
        this.fileIdSelected = fileIdSelected;
    }

    /**
     * <p>Getter method for originalSPSPONo.</p>
     *
     * @return the originalSPSPONo
     */
    public String getOriginalSpsPoNo() {
        return originalSpsPoNo;
    }

    /**
     * <p>Setter method for originalSPSPONo.</p>
     *
     * @param originalSpsPoNo Set for originalSpsPoNo
     */
    public void setOriginalSpsPoNo(String originalSpsPoNo) {
        this.originalSpsPoNo = originalSpsPoNo;
    }

    /**
     * <p>Getter method for previousSPSPONo.</p>
     *
     * @return the previousSpsPoNo
     */
    public String getPreviousSpsPoNo() {
        return previousSpsPoNo;
    }

    /**
     * <p>Setter method for previousSPSPONo.</p>
     *
     * @param previousSpsPoNo Set for previousSpsPoNo
     */
    public void setPreviousSpsPoNo(String previousSpsPoNo) {
        this.previousSpsPoNo = previousSpsPoNo;
    }

    /**
     * <p>Getter method for purchaseOrderDetailList.</p>
     *
     * @return the purchaseOrderDetailList
     */
    public List<PurchaseOrderDetailDomain> getPurchaseOrderDetailList() {
        return purchaseOrderDetailList;
    }

    /**
     * <p>Setter method for purchaseOrderDetailList.</p>
     *
     * @param purchaseOrderDetailList Set for purchaseOrderDetailList
     */
    public void setPurchaseOrderDetailList(
        List<PurchaseOrderDetailDomain> purchaseOrderDetailList) {
        this.purchaseOrderDetailList = purchaseOrderDetailList;
    }

    /**
     * <p>Getter method for spsTPoDomain.</p>
     *
     * @return the spsTPoDomain
     */
    public SpsTPoDomain getSpsTPoDomain() {
        return spsTPoDomain;
    }

    /**
     * <p>Setter method for spsTPoDomain.</p>
     *
     * @param spsTPoDomain Set for spsTPoDomain
     */
    public void setSpsTPoDomain(SpsTPoDomain spsTPoDomain) {
        this.spsTPoDomain = spsTPoDomain;
    }

    /**
     * <p>Getter method for spsTPoDetailDomain.</p>
     *
     * @return the spsTPoDetailDomain
     */
    public SpsTPoDetailDomain getSpsTPoDetailDomain() {
        return spsTPoDetailDomain;
    }

    /**
     * <p>Setter method for spsTPoDetailDomain.</p>
     *
     * @param spsTPoDetailDomain Set for spsTPoDetailDomain
     */
    public void setSpsTPoDetailDomain(SpsTPoDetailDomain spsTPoDetailDomain) {
        this.spsTPoDetailDomain = spsTPoDetailDomain;
    }

    /**
     * <p>Getter method for spsTPoDueDomain.</p>
     *
     * @return the spsTPoDueDomain
     */
    public SpsTPoDueDomain getSpsTPoDueDomain() {
        return spsTPoDueDomain;
    }

    /**
     * <p>Setter method for spsTPoDueDomain.</p>
     *
     * @param spsTPoDueDomain Set for spsTPoDueDomain
     */
    public void setSpsTPoDueDomain(SpsTPoDueDomain spsTPoDueDomain) {
        this.spsTPoDueDomain = spsTPoDueDomain;
    }

    /**
     * <p>Getter method for stringPoIssueDate.</p>
     *
     * @return the stringPoIssueDate
     */
    public String getStringPoIssueDate() {
        return stringPoIssueDate;
    }

    /**
     * <p>Setter method for stringPoIssueDate.</p>
     *
     * @param stringPoIssueDate Set for stringPoIssueDate
     */
    public void setStringPoIssueDate(String stringPoIssueDate) {
        this.stringPoIssueDate = stringPoIssueDate;
    }

    /**
     * <p>Getter method for stringReleaseNo.</p>
     *
     * @return the stringReleaseNo
     */
    public String getStringReleaseNo() {
        return stringReleaseNo;
    }

    /**
     * <p>Setter method for stringReleaseNo.</p>
     *
     * @param stringReleaseNo Set for stringReleaseNo
     */
    public void setStringReleaseNo(String stringReleaseNo) {
        this.stringReleaseNo = stringReleaseNo;
    }

    /**
     * <p>Getter method for dataType.</p>
     *
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * <p>Setter method for dataType.</p>
     *
     * @param dataType Set for dataType
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for poIssuedDateShow.</p>
     *
     * @return the poIssuedDateShow
     */
    public String getPoIssuedDateShow() {
        return poIssuedDateShow;
    }

    /**
     * <p>Setter method for poIssuedDateShow.</p>
     *
     * @param poIssuedDateShow Set for poIssuedDateShow
     */
    public void setPoIssuedDateShow(String poIssuedDateShow) {
        this.poIssuedDateShow = poIssuedDateShow;
    }

}