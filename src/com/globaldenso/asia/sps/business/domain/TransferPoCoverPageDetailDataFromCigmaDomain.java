package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Date;
/**
 * 
 * <p>TransferPoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferPoCoverPageDetailDataFromCigmaDomain extends BaseDomain
    implements Serializable
{
    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = 8422789033849495639L;
    /** dueDate */
    private Date dueDate;
    /** doNo */
    private String doNo;
    /** createBy */
    private String createBy;
    /** createDate */
    private String createDate;
    /** createTime */
    private String createTime;
    /** updateBy */
    private String updateBy;
    /** updateDate */
    private String updateDate;
    /** updateTime */
    private String updateTime;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferPoCoverPageDetailDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for due date.</p>
     *
     * @return the due date
     */
    public Date getDueDate() {
        return dueDate;
    }
    /**
     * <p>Setter method for due date.</p>
     *
     * @param dueDate Set for due date
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    /**
     * <p>Getter method for dono.</p>
     *
     * @return the dono
     */
    public String getDoNo() {
        return doNo;
    }
    /**
     * <p>Setter method for dono.</p>
     *
     * @param doNo Set for dono
     */
    public void setDoNo(String doNo) {
        this.doNo = doNo;
    }
    /**
     * <p>Getter method for createby.</p>
     *
     * @return the createby
     */
    public String getCreateBy() {
        return createBy;
    }
    /**
     * <p>Setter method for createby.</p>
     *
     * @param createBy Set for createby
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    /**
     * <p>Getter method for createdate.</p>
     *
     * @return the createdate
     */
    public String getCreateDate() {
        return createDate;
    }
    /**
     * <p>Setter method for createdate.</p>
     *
     * @param createDate Set for createdate
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    /**
     * <p>Getter method for createtime.</p>
     *
     * @return the createtime
     */
    public String getCreateTime() {
        return createTime;
    }
    /**
     * <p>Setter method for createtime.</p>
     *
     * @param createTime Set for createtime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    /**
     * <p>Getter method for updateby.</p>
     *
     * @return the updateby
     */
    public String getUpdateBy() {
        return updateBy;
    }
    /**
     * <p>Setter method for updateby.</p>
     *
     * @param updateBy Set for updateby
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    /**
     * <p>Getter method for updatedate.</p>
     *
     * @return the updatedate
     */
    public String getUpdateDate() {
        return updateDate;
    }
    /**
     * <p>Setter method for updatedate.</p>
     *
     * @param updateDate Set for updatedate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    /**
     * <p>Getter method for updatetime.</p>
     *
     * @return the updatetime
     */
    public String getUpdateTime() {
        return updateTime;
    }
    /**
     * <p>Setter method for updatetime.</p>
     *
     * @param updateTime Set for updatetime
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
