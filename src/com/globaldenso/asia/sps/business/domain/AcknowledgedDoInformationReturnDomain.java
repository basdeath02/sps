/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Parichat           Create
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * The Class Acknowledged DO Information Domain.
 * @author CSI
 */
public class AcknowledgedDoInformationReturnDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2440700236350813567L;
    
    /** The ship notice. */
    private String shipNotice;
    
    /** The supplier code. */
    private String sCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The trans. */
    private String trans;
    
    /** The delivery date str. */
    private String deliveryDateStr;
    
    /** The shipment date str. */
    private String shipmentDateStr;
    
    /** The issued date str. */
    private String issuedDateStr;
    
    /** The selected do id. */
    private String selectedDoId;
    
    /** The utc. */
    private String utc;    
    
    /** The receiveByScan. */
    private String receiveByScan;    
    
    /** The isPrintOneWayKanbanTag. */
    private String isPrintOneWayKanbanTag; 
    
    /** 
     * The tagOutput. 
     */
    private String tagOutput;
    
    /** 
     * The tagOutput. 
     */
    private Integer partTag;
    
    /**
     * <p>
     * The shippingTotal
     * </p>
     */
    private BigDecimal shippingTotal;
    
    /**
     * <p>
     * The shipmentStatus
     * </p>
     */
    private String receiveStatus; 
    
    /** The delivery order domain. */
    private SpsTDoDomain deliveryOrderDomain;
    
    /** The misc domain. */
    private MiscellaneousDomain miscDomain;
    
    /** The asn detail domain. */
    private List<SpsTAsnDetailDomain> asnDetailDomainList;
    
    /**
     * Instantiates a new DO domain.
     */
    public AcknowledgedDoInformationReturnDomain() {
        super();
        this.deliveryOrderDomain = new SpsTDoDomain();
        this.miscDomain = new MiscellaneousDomain();
        this.asnDetailDomainList = new ArrayList<SpsTAsnDetailDomain>();
    }

    /**
     * Gets the delivery order domain.
     * 
     * @return the delivery order domain.
     */
    public SpsTDoDomain getDeliveryOrderDomain() {
        return deliveryOrderDomain;
    }

    /**
     * Sets the delivery order domain.
     * 
     * @param deliveryOrderDomain the delivery order domain.
     */
    public void setDeliveryOrderDomain(SpsTDoDomain deliveryOrderDomain) {
        this.deliveryOrderDomain = deliveryOrderDomain;
    }

    /**
     * Gets the misc domain.
     * 
     * @return the misc domain.
     */
    public MiscellaneousDomain getMiscDomain() {
        return miscDomain;
    }

    /**
     * Sets the misc domain.
     * 
     * @param miscDomain the misc domain.
     */
    public void setMiscDomain(MiscellaneousDomain miscDomain) {
        this.miscDomain = miscDomain;
    }

    /**
     * Gets the asn detail domain list.
     * 
     * @return the asn detail domain list.
     */
    public List<SpsTAsnDetailDomain> getAsnDetailDomainList() {
        return asnDetailDomainList;
    }

    /**
     * Sets the asn detail domain list.
     * 
     * @param asnDetailDomainList the asn detail domain list.
     */
    public void setAsnDetailDomainList(List<SpsTAsnDetailDomain> asnDetailDomainList) {
        this.asnDetailDomainList = asnDetailDomainList;
    }

    /**
     * Gets the ship notice.
     * 
     * @return the ship notice.
     */
    public String getShipNotice() {
        return shipNotice;
    }

    /**
     * Sets the ship notice.
     * 
     * @param shipNotice the ship notice.
     */
    public void setShipNotice(String shipNotice) {
        this.shipNotice = shipNotice;
    }

    /**
     * Gets the supplier code.
     * 
     * @return the supplier code.
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the supplier code.
     * 
     * @param sCd the supplier code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso code.
     * 
     * @return the denso code.
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * Sets the denso code.
     * 
     * @param dCd the denso code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the denso plant code.
     * 
     * @return the denso plant code.
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the denso plant code.
     * 
     * @param dPcd the denso plant code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the trans.
     * 
     * @return the trans.
     */
    public String getTrans() {
        return trans;
    }

    /**
     * Sets the trans.
     * 
     * @param trans the trans.
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * Gets the delivery date str.
     * 
     * @return the delivery date str.
     */
    public String getDeliveryDateStr() {
        return deliveryDateStr;
    }
    
    /**
     * Sets the delivery date str.
     * 
     * @param deliveryDateStr the delivery date str.
     */
    public void setDeliveryDateStr(String deliveryDateStr) {
        this.deliveryDateStr = deliveryDateStr;
    }

    /**
     * Gets the shipment date str.
     * 
     * @return the shipment date str.
     */
    public String getShipmentDateStr() {
        return shipmentDateStr;
    }

    /**
     * Sets the shipment date str.
     * 
     * @param shipmentDateStr the shipment date str.
     */
    public void setShipmentDateStr(String shipmentDateStr) {
        this.shipmentDateStr = shipmentDateStr;
    }

    /**
     * Gets the selected do id .
     * 
     * @return the selected do id .
     */
    public String getSelectedDoId() {
        return selectedDoId;
    }

    /**
     * Sets the selected do id.
     * 
     * @param selectedDoId the selected do id.
     */
    public void setSelectedDoId(String selectedDoId) {
        this.selectedDoId = selectedDoId;
    }

    /**
     * Gets the issued date str.
     * 
     * @return the issued date str.
     */
    public String getIssuedDateStr() {
        return issuedDateStr;
    }

    /**
     * Sets the issued date str.
     * 
     * @param issuedDateStr the issued date str.
     */
    public void setIssuedDateStr(String issuedDateStr) {
        this.issuedDateStr = issuedDateStr;
    }

    /**
     * Gets the utc.
     * 
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Sets the utc.
     * 
     * @param utc the utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    /**
     * Gets the isPrintOneWayKanbanTag.
     * 
     * @return the isPrintOneWayKanbanTag
     */
    public String getIsPrintOneWayKanbanTag() {
        return isPrintOneWayKanbanTag;
    }

    /**
     * Sets the isPrintOneWayKanbanTag.
     * 
     * @param isPrintOneWayKanbanTag the isPrintOneWayKanbanTag
     */
    public void setIsPrintOneWayKanbanTag(String isPrintOneWayKanbanTag) {
        this.isPrintOneWayKanbanTag = isPrintOneWayKanbanTag;
    }

    /**
     * <p>Getter method for tagOutput.</p>
     *
     * @return the tagOutput
     */
    public String getTagOutput() {
        return tagOutput;
    }

    /**
     * <p>Setter method for tagOutput.</p>
     *
     * @param tagOutput Set for tagOutput
     */
    public void setTagOutput(String tagOutput) {
        this.tagOutput = tagOutput;
    }

    /**
     * <p>Getter method for partTag.</p>
     *
     * @return the partTag
     */
    public Integer getPartTag() {
        return partTag;
    }

    /**
     * <p>Setter method for partTag.</p>
     *
     * @param partTag Set for partTag
     */
    public void setPartTag(Integer partTag) {
        this.partTag = partTag;
    }

    /**
     * <p>Getter method for shippingTotal.</p>
     *
     * @return the shippingTotal
     */
    public BigDecimal getShippingTotal() {
        return shippingTotal;
    }

    /**
     * <p>Setter method for shippingTotal.</p>
     *
     * @param shippingTotal Set for shippingTotal
     */
    public void setShippingTotal(BigDecimal shippingTotal) {
        this.shippingTotal = shippingTotal;
    }

    /**
     * <p>Getter method for receiveStatus.</p>
     *
     * @return the receiveStatus
     */
    public String getReceiveStatus() {
        return receiveStatus;
    }

    /**
     * <p>Setter method for receiveStatus.</p>
     *
     * @param receiveStatus Set for receiveStatus
     */
    public void setReceiveStatus(String receiveStatus) {
        this.receiveStatus = receiveStatus;
    }
    
}