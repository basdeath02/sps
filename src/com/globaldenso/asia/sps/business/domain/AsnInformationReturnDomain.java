/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ASN Information Return Domain.
 * @author CSI
 */
public class AsnInformationReturnDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4706851609942365323L;
    
    /** The list of ASN Information domain. */
    private List<AsnInformationDomain> asnInformationDomainList;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant tax ID. */
    private String supplierPlantTaxId;
    
    /** The denso code. */
    private String dCd;
    
    /** The supplier plant code. */
    private String dPcd;
    
    /** The company supplier list. */
    private List<CompanySupplierDomain> companySupplierList;

    /** The company denso list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The plant supplier list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The denso supplier relation list. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;
    
    /** The asn status list. */
    private List<MiscellaneousDomain> asnStatusList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new file upload domain.
     */
    public AsnInformationReturnDomain() {
        super();
        errorMessageList = new ArrayList<ApplicationMessageDomain>();
    }
    
    /**
     * Gets the list of ASN Information Domain.
     * 
     * @return the asnInformationDomainList.
     */
    public List<AsnInformationDomain> getAsnInformationDomainList() {
        return asnInformationDomainList;
    }
    
    /**
     * Sets the sps ASN domain.
     * 
     * @param asnInformationDomainList the list of ASN Information Domain.
     */  
    public void setAsnInformationDomainList(List<AsnInformationDomain> asnInformationDomainList) {
        this.asnInformationDomainList = asnInformationDomainList;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierPlantTaxId.</p>
     *
     * @return the supplierPlantTaxId
     */
    public String getSupplierPlantTaxId() {
        return supplierPlantTaxId;
    }

    /**
     * <p>Setter method for supplierPlantTaxId.</p>
     *
     * @param supplierPlantTaxId Set for supplierPlantTaxId
     */
    public void setSupplierPlantTaxId(String supplierPlantTaxId) {
        this.supplierPlantTaxId = supplierPlantTaxId;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the plant denso list.
     * 
     * @return the plant denso  list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the plant denso  list.
     * 
     * @param plantDensoList the plant denso  list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the plant supplier list.
     * 
     * @return the plant supplier list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the plant supplier list.
     * 
     * @param plantSupplierList the plant supplier list.
     */
    public void setPlantSupplierList(
        List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    
    /**
     * Gets the supplier code list.
     * 
     * @return the supplier code list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the supplier code list.
     * 
     * @param companySupplierList the supplier code list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the denso code list.
     * 
     * @return the denso code list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso code list.
     * 
     * @param companyDensoList the denso code list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso list.
     * 
     * @return the company denso list.
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * Sets the denso supplier relation list.
     * 
     * @param densoSupplierRelationList the denso supplier relation list.
     */
    public void setDensoSupplierRelationList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * Gets the asn status list.
     * 
     * @return the asn status list.
     */
    public List<MiscellaneousDomain> getAsnStatusList() {
        return asnStatusList;
    }

    /**
     * Sets the asn statusn list.
     * 
     * @param asnStatusList the asn status list.
     */
    public void setAsnStatusList(List<MiscellaneousDomain> asnStatusList) {
        this.asnStatusList = asnStatusList;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }

}