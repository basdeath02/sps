/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/23 CSI PJ           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */

package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
/**
 * <p>The Class ImportMessageToSystemFacadeServiceImpl.</p>
 * <p>Manage import message for batch.</p>
 * <li>Method update : transactNotificationAbnormalOrder</li>
 *
 * @author CSI
 * @version 1.00
 */
public class PoForceAcknowledgeDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>The Serial Version.</p>
     */
    private static final long serialVersionUID = -1296168851970605859L;
    
    /** periodType */ 
    private String periodType;
    
    /** poStatus */
    private String poStatus;
    
    /** spsT Po Criteria Domain */
    private SpsTPoCriteriaDomain spsTPoCriteriaDomain;
    
    /** PO ID List */
    private List<String> poIdList;
    
    /** SpsT Po Domain List */
    private List<SpsTPoDomain> SpsTPoDomainList;
    
    /** spsT Po Criteria Domain List */
    private List<SpsTPoCriteriaDomain> spsTPoCriteriaDomainList;
    
    
    /** spsT Po Detail Domain List */
    private List<SpsTPoDetailDomain> spsTPoDetailDomainList;
    
    /** spsT Po Detail Criteria Domain List */
    private List<SpsTPoDetailCriteriaDomain> spsTPoDetailCriteriaDomainList;
    
    /** Constructed */
    public PoForceAcknowledgeDomain(){
        
    }

    /**
     * <p>Getter method for periodType.</p>
     *
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }


    /**
     * <p>Setter method for periodType.</p>
     *
     * @param periodType Set for periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }


    /**
     * <p>Getter method for poStatus.</p>
     *
     * @return the poStatus
     */
    public String getPoStatus() {
        return poStatus;
    }


    /**
     * <p>Setter method for poStatus.</p>
     *
     * @param poStatus Set for poStatus
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }


    /**
     * <p>Getter method for poIdList.</p>
     *
     * @return the poIdList
     */
    public List<String> getPoIdList() {
        return poIdList;
    }


    /**
     * <p>Setter method for poIdList.</p>
     *
     * @param poIdList Set for poIdList
     */
    public void setPoIdList(List<String> poIdList) {
        this.poIdList = poIdList;
    }


    /**
     * <p>Getter method for spsTPoDomainList.</p>
     *
     * @return the spsTPoDomainList
     */
    public List<SpsTPoDomain> getSpsTPoDomainList() {
        return SpsTPoDomainList;
    }


    /**
     * <p>Setter method for spsTPoDomainList.</p>
     *
     * @param spsTPoDomainList Set for spsTPoDomainList
     */
    public void setSpsTPoDomainList(List<SpsTPoDomain> spsTPoDomainList) {
        SpsTPoDomainList = spsTPoDomainList;
    }


    /**
     * <p>Getter method for spsTPoCriteriaDomainList.</p>
     *
     * @return the spsTPoCriteriaDomainList
     */
    public List<SpsTPoCriteriaDomain> getSpsTPoCriteriaDomainList() {
        return spsTPoCriteriaDomainList;
    }


    /**
     * <p>Setter method for spsTPoCriteriaDomainList.</p>
     *
     * @param spsTPoCriteriaDomainList Set for spsTPoCriteriaDomainList
     */
    public void setSpsTPoCriteriaDomainList(
        List<SpsTPoCriteriaDomain> spsTPoCriteriaDomainList) {
        this.spsTPoCriteriaDomainList = spsTPoCriteriaDomainList;
    }


    /**
     * <p>Getter method for spsTPoDetailDomainList.</p>
     *
     * @return the spsTPoDetailDomainList
     */
    public List<SpsTPoDetailDomain> getSpsTPoDetailDomainList() {
        return spsTPoDetailDomainList;
    }


    /**
     * <p>Setter method for spsTPoDetailDomainList.</p>
     *
     * @param spsTPoDetailDomainList Set for spsTPoDetailDomainList
     */
    public void setSpsTPoDetailDomainList(
        List<SpsTPoDetailDomain> spsTPoDetailDomainList) {
        this.spsTPoDetailDomainList = spsTPoDetailDomainList;
    }


    /**
     * <p>Getter method for spsTPoDetailCriteriaDomainList.</p>
     *
     * @return the spsTPoDetailCriteriaDomainList
     */
    public List<SpsTPoDetailCriteriaDomain> getSpsTPoDetailCriteriaDomainList() {
        return spsTPoDetailCriteriaDomainList;
    }


    /**
     * <p>Setter method for spsTPoDetailCriteriaDomainList.</p>
     *
     * @param spsTPoDetailCriteriaDomainList Set for spsTPoDetailCriteriaDomainList
     */
    public void setSpsTPoDetailCriteriaDomainList(
        List<SpsTPoDetailCriteriaDomain> spsTPoDetailCriteriaDomainList) {
        this.spsTPoDetailCriteriaDomainList = spsTPoDetailCriteriaDomainList;
    }


    /**
     * <p>Getter method for spsTPoCriteriaDomain.</p>
     *
     * @return the spsTPoCriteriaDomain
     */
    public SpsTPoCriteriaDomain getSpsTPoCriteriaDomain() {
        return spsTPoCriteriaDomain;
    }


    /**
     * <p>Setter method for spsTPoCriteriaDomain.</p>
     *
     * @param spsTPoCriteriaDomain Set for spsTPoCriteriaDomain
     */
    public void setSpsTPoCriteriaDomain(SpsTPoCriteriaDomain spsTPoCriteriaDomain) {
        this.spsTPoCriteriaDomain = spsTPoCriteriaDomain;
    }
    
    
    
}
