/*
 * ModifyDate Development company    Describe 
 * 2014/08/04 CSI Parichat           Create
 * 2015/08/24 CSI Akat               [IN012]
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;

/**
 * The Class ASN Maintenance Domain.
 * @author CSI
 */
public class AsnMaintenanceDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 131875035709363209L;
    
    /** The do id. */
    private String doId;
    
    /** The utc. */
    private String utc;
    
    /** The temporary mode. */
    private String temporaryMode;
    
    /** The session id. */
    private String sessionId;
    
    /** The asn no. */
    private String asnNo;
    
    /** The company denso code. */
    private String dCd;
    
    /** The asn rcv status. */
    private String asnRcvStatus;
    
    /** The invoice status. */
    private String invoiceStatus;
    
    /** The last modified. */
    private String lastModified;
    
    /** The actual etd date. */
    private String actualEtdDate;
    
    /** The actual etd time. */
    private String actualEtdTime;
    
    /** The plan eta date. */
    private String planEtaDate;
    
    /** The plan eta time. */
    private String planEtaTime;
    
    /** The no of pallet. */
    private String noOfPallet;
    
    /** The trip no. */
    private String tripNo;
    
    /** The shipment status. */
    private String shipmentStatus;
    
    /** The last update date time. */
    private Timestamp lastUpdateDatetime;
    
    /** The file id. */
    private String fileId;
    
    /** The Denso Part No. */
    private String dPn;
    
    /** The allow revise qty flag. */
    private String allowReviseQtyFlag;
    
    /** The skip validate lot size flag, 0: not skip, 1: skip. */
    private String skipValidateLotSizeFlag;
    
    /** The unmatched lot size flag, 0: matched, 1: unmatched. */
    private String unmatchedLotSizeFlag;

    /** The acknowledged do information list. */
    private List<AcknowledgedDoInformationReturnDomain> doInfoList;
    
    /** The do group asn list. */
    private List<AsnMaintenanceReturnDomain> doGroupAsnList;
    
    /** The asn short ship list. */
    private List<AsnMaintenanceReturnDomain> asnShortShipList;
    
    /** The change reason list. */
    private List<SpsMMiscDomain> changeReasonList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;

    // [IN012] Start : add property for send constant to SqlMap
    /** Shipment Status Cancel. */
    private String shipmentStatusCancel;
    
    /** Shipment Status Complete Shipped. */
    private String shipmentStatusComplete;
    // [IN012] End : add property for send constant to SqlMap
    /**
     * Instantiates a new ASN maintenance domain.
     */
    public AsnMaintenanceDomain() {
        super();
    }
    
    /**
     * Gets the do id.
     * 
     * @return the do id
     */
    public String getDoId() {
        return doId;
    }

    /**
     * Sets the do id.
     * 
     * @param doId the do id
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }
    
    /**
     * Gets the utc.
     * 
     * @return the do utc.
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Sets the utc.
     * 
     * @param utc the utc.
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }
    
    /**
     * Gets the asn no.
     * 
     * @return the asn no.
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Sets the asn no.
     * 
     * @param asnNo the asn no.
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }
    
    /**
     * Gets the dCd.
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the dCd.
     * 
     * @param dCd the dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the temporary mode.
     * 
     * @return the temporary mode.
     */
    public String getTemporaryMode() {
        return temporaryMode;
    }

    /**
     * Sets the temporary mode.
     * 
     * @param temporaryMode the temporary mode.
     */
    public void setTemporaryMode(String temporaryMode) {
        this.temporaryMode = temporaryMode;
    }

    /**
     * Gets the session id.
     * 
     * @return the session id.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the session id.
     * 
     * @param sessionId the session id.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Gets the asn rcv status.
     * 
     * @return the asn rcv status.
     */
    public String getAsnRcvStatus() {
        return asnRcvStatus;
    }

    /**
     * Sets the asn rcv status.
     * 
     * @param asnRcvStatus the asn rcv status.
     */
    public void setAsnRcvStatus(String asnRcvStatus) {
        this.asnRcvStatus = asnRcvStatus;
    }

    /**
     * Gets the invoice status.
     * 
     * @return the invoice status.
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * Sets the invoice status.
     * 
     * @param invoiceStatus the invoice status.
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * Gets the last modified.
     * 
     * @return the last modified.
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * Sets the last modified.
     * 
     * @param lastModified the last modified.
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
    
    /**
     * Gets the actual etd date.
     * 
     * @return the actual etd date.
     */
    public String getActualEtdDate() {
        return actualEtdDate;
    }

    /**
     * Sets the actual etd date.
     * 
     * @param actualEtdDate the actual etd date.
     */
    public void setActualEtdDate(String actualEtdDate) {
        this.actualEtdDate = actualEtdDate;
    }

    /**
     * Gets the actual etd time.
     * 
     * @return the actual etd time.
     */
    public String getActualEtdTime() {
        return actualEtdTime;
    }

    /**
     * Sets the actual etd time.
     * 
     * @param actualEtdTime the actual etd time.
     */
    public void setActualEtdTime(String actualEtdTime) {
        this.actualEtdTime = actualEtdTime;
    }

    /**
     * Gets the plan etd date.
     * 
     * @return the plan etd date.
     */
    public String getPlanEtaDate() {
        return planEtaDate;
    }

    /**
     * Sets the plan etd date.
     * 
     * @param planEtaDate the plan etd date.
     */
    public void setPlanEtaDate(String planEtaDate) {
        this.planEtaDate = planEtaDate;
    }

    /**
     * Gets the plan etd time.
     * 
     * @return the plan etd time.
     */
    public String getPlanEtaTime() {
        return planEtaTime;
    }

    /**
     * Sets the plan etd time.
     * 
     * @param planEtaTime the plan etd time.
     */
    public void setPlanEtaTime(String planEtaTime) {
        this.planEtaTime = planEtaTime;
    }

    /**
     * Gets the no of pallet.
     * 
     * @return the no of pallet.
     */
    public String getNoOfPallet() {
        return noOfPallet;
    }

    /**
     * Sets the no of pallet.
     * 
     * @param noOfPallet the no of pallet.
     */
    public void setNoOfPallet(String noOfPallet) {
        this.noOfPallet = noOfPallet;
    }

    /**
     * Gets the trip no.
     * 
     * @return the trip no.
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Sets the trip no.
     * 
     * @param tripNo the trip no.
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }
    
    /**
     * Gets the shipment status.
     * 
     * @return the shipment status.
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * Sets the shipment status.
     * 
     * @param shipmentStatus the shipment status.
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * Gets the do info list.
     * 
     * @return the do info list
     */
    public List<AcknowledgedDoInformationReturnDomain> getDoInfoList() {
        return doInfoList;
    }

    /**
     * Sets the do info list.
     * 
     * @param doInfoList the do info list
     */
    public void setDoInfoList(List<AcknowledgedDoInformationReturnDomain> doInfoList) {
        this.doInfoList = doInfoList;
    }

    /**
     * Gets the do group asn list.
     * 
     * @return the do group asn list
     */
    public List<AsnMaintenanceReturnDomain> getDoGroupAsnList() {
        return doGroupAsnList;
    }

    /**
     * Sets the do group asn list.
     * 
     * @param doGroupAsnList the do group asn list
     */
    public void setDoGroupAsnList(List<AsnMaintenanceReturnDomain> doGroupAsnList) {
        this.doGroupAsnList = doGroupAsnList;
    }
    
    /**
     * Gets the asnShortShipList.
     * 
     * @return the asnShortShipList
     */
    public List<AsnMaintenanceReturnDomain> getAsnShortShipList() {
        return asnShortShipList;
    }

    /**
     * Sets the asnShortShipList.
     * 
     * @param asnShortShipList the asnShortShipList
     */
    public void setAsnShortShipList(
        List<AsnMaintenanceReturnDomain> asnShortShipList) {
        this.asnShortShipList = asnShortShipList;
    }

    /**
     * Gets the change reason list.
     * 
     * @return the do change reason
     */
    public List<SpsMMiscDomain> getChangeReasonList() {
        return changeReasonList;
    }

    /**
     * Sets the change reason.
     * 
     * @param changeReasonList the change reason
     */
    public void setChangeReasonList(List<SpsMMiscDomain> changeReasonList) {
        this.changeReasonList = changeReasonList;
    }

    /**
     * Gets the error message list.
     * 
     * @return the error message list
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list
     */
    public void setWarningMessageList(
        List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }

    /**
     * Gets the last update date time.
     * 
     * @return the last update date time
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Sets the last update date time.
     * 
     * @param lastUpdateDatetime the last update date time
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Gets the file id.
     * 
     * @return the file id
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Sets the file id.
     * 
     * @param fileId the file id
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    
    /**
      * <p>Getter method for allowReviseQtyFlag.</p>
      *
      * @return the allowReviseQtyFlag
      */
    public String getAllowReviseQtyFlag() {
        return allowReviseQtyFlag;
    }
    
    /**
      * <p>Setter method for allowReviseQtyFlag.</p>
      *
      * @param allowReviseQtyFlag Set for allowReviseQtyFlag
      */
    public void setAllowReviseQtyFlag(String allowReviseQtyFlag) {
        this.allowReviseQtyFlag = allowReviseQtyFlag;
    }

    /**
     * <p>Getter method for skipValidateLotSizeFlag.</p>
     *
     * @return the skipValidateLotSizeFlag
     */
    public String getSkipValidateLotSizeFlag() {
        return skipValidateLotSizeFlag;
    }

    /**
     * <p>Setter method for skipValidateLotSizeFlag.</p>
     *
     * @param skipValidateLotSizeFlag Set for skipValidateLotSizeFlag
     */
    public void setSkipValidateLotSizeFlag(String skipValidateLotSizeFlag) {
        this.skipValidateLotSizeFlag = skipValidateLotSizeFlag;
    }

    /**
     * <p>Getter method for unmatchedLotSizeFlag.</p>
     *
     * @return the unmatchedLotSizeFlag
     */
    public String getUnmatchedLotSizeFlag() {
        return unmatchedLotSizeFlag;
    }

    /**
     * <p>Setter method for unmatchedLotSizeFlag.</p>
     *
     * @param unmatchedLotSizeFlag Set for unmatchedLotSizeFlag
     */
    public void setUnmatchedLotSizeFlag(String unmatchedLotSizeFlag) {
        this.unmatchedLotSizeFlag = unmatchedLotSizeFlag;
    }

    /**
     * <p>Getter method for shipmentStatusCancel.</p>
     *
     * @return the shipmentStatusCancel
     */
    public String getShipmentStatusCancel() {
        return shipmentStatusCancel;
    }

    /**
     * <p>Setter method for shipmentStatusCancel.</p>
     *
     * @param shipmentStatusCancel Set for shipmentStatusCancel
     */
    public void setShipmentStatusCancel(String shipmentStatusCancel) {
        this.shipmentStatusCancel = shipmentStatusCancel;
    }

    /**
     * <p>Getter method for shipmentStatusComplete.</p>
     *
     * @return the shipmentStatusComplete
     */
    public String getShipmentStatusComplete() {
        return shipmentStatusComplete;
    }

    /**
     * <p>Setter method for shipmentStatusComplete.</p>
     *
     * @param shipmentStatusComplete Set for shipmentStatusComplete
     */
    public void setShipmentStatusComplete(String shipmentStatusComplete) {
        this.shipmentStatusComplete = shipmentStatusComplete;
    }
    
}