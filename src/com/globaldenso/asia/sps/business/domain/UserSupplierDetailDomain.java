/*
 * ModifyDate Development company Describe 
 * 2014/07/03 CSI Akat           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;

/**
 * The Class UserSupplierDetailDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserSupplierDetailDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 3260697091712121016L;

    /** The information for user from Supplier. */
    private SpsMUserSupplierDomain spsMUserSupplierDomain;
    
    /** The company Supplier information. */
    private SpsMCompanySupplierDomain spsMCompanySupplierDomain;
    
    /** The plant Supplier information. */
    private SpsMPlantSupplierDomain spsMPlantSupplierDomain;
    
    /** The information of user. */
    private SpsMUserDomain spsMUserDomain;
    
    /** The email user Domain.*/
    private UserDomain userDomain;
    
    /** The User Role Detail Domain.*/
    private UserRoleDetailDomain userRoleDetailDomain;
    
    /** The User Role Detail Domain.*/
    private List<UserRoleDetailDomain> userRoleDetailList;
    
    /** The list of Role Domain.*/
    private List<SpsMRoleDomain> SpsMRoleList; 
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The default constructor. */
    public UserSupplierDetailDomain() {
        this.spsMUserSupplierDomain = new SpsMUserSupplierDomain();
        this.spsMCompanySupplierDomain = new SpsMCompanySupplierDomain();
        this.spsMPlantSupplierDomain = new SpsMPlantSupplierDomain();
        this.userRoleDetailDomain = new UserRoleDetailDomain();
        this.userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
    }

    /**
     * <p>Getter method for spsMUserSupplierDomain.</p>
     *
     * @return the spsMUserSupplierDomain
     */
    public SpsMUserSupplierDomain getSpsMUserSupplierDomain() {
        return spsMUserSupplierDomain;
    }

    /**
     * <p>Setter method for spsMUserSupplierDomain.</p>
     *
     * @param spsMUserSupplierDomain Set for spsMUserSupplierDomain
     */
    public void setSpsMUserSupplierDomain(
        SpsMUserSupplierDomain spsMUserSupplierDomain) {
        this.spsMUserSupplierDomain = spsMUserSupplierDomain;
    }

    /**
     * <p>Getter method for spsMCompanySupplierDomain.</p>
     *
     * @return the spsMCompanySupplierDomain
     */
    public SpsMCompanySupplierDomain getSpsMCompanySupplierDomain() {
        return spsMCompanySupplierDomain;
    }

    /**
     * <p>Setter method for spsMCompanySupplierDomain.</p>
     *
     * @param spsMCompanySupplierDomain Set for spsMCompanySupplierDomain
     */
    public void setSpsMCompanySupplierDomain(
        SpsMCompanySupplierDomain spsMCompanySupplierDomain) {
        this.spsMCompanySupplierDomain = spsMCompanySupplierDomain;
    }

    /**
     * <p>Getter method for spsMPlantSupplierDomain.</p>
     *
     * @return the spsMPlantSupplierDomain
     */
    public SpsMPlantSupplierDomain getSpsMPlantSupplierDomain() {
        return spsMPlantSupplierDomain;
    }

    /**
     * <p>Setter method for spsMPlantSupplierDomain.</p>
     *
     * @param spsMPlantSupplierDomain Set for spsMPlantSupplierDomain
     */
    public void setSpsMPlantSupplierDomain(
        SpsMPlantSupplierDomain spsMPlantSupplierDomain) {
        this.spsMPlantSupplierDomain = spsMPlantSupplierDomain;
    }
    
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public SpsMUserDomain getSpsMUserDomain() {
        return spsMUserDomain;
    }
    /**
     * Sets the user Domain.
     * 
     * @param spsMUserDomain the userDomain.
     */
    public void setSpsMUserDomain(SpsMUserDomain spsMUserDomain) {
        this.spsMUserDomain = spsMUserDomain;
    }
    /**
     * Gets the user role detail Domain.
     * 
     * @return the userRoleDetailDomain.
     */
    public UserRoleDetailDomain getUserRoleDetailDomain() {
        return userRoleDetailDomain;
    }
    /**
     * Sets the user role detail Domain.
     * 
     * @param userRoleDetailDomain the user role detail Domain.
     */
    public void setUserRoleDetailDomain(UserRoleDetailDomain userRoleDetailDomain) {
        this.userRoleDetailDomain = userRoleDetailDomain;
    }
    /**
     * Gets the list of user role detail Domain.
     * 
     * @return the userRoleDetailList.
     */
    public List<UserRoleDetailDomain> getUserRoleDetailList() {
        return userRoleDetailList;
    }
    /**
     * Sets the list of user role detail Domain.
     * 
     * @param userRoleDetailList the list of user role detail Domain.
     */
    public void setUserRoleDetailList(List<UserRoleDetailDomain> userRoleDetailList) {
        this.userRoleDetailList = userRoleDetailList;
    }
    /**
     * Gets the list of role Domain.
     * 
     * @return the spsMRoleList.
     */
    public List<SpsMRoleDomain> getSpsMRoleList() {
        return SpsMRoleList;
    }
    /**
     * Sets the list of role Domain.
     * 
     * @param spsMRoleList the list of role Domain.
     */
    public void setSpsMRoleList(List<SpsMRoleDomain> spsMRoleList) {
        SpsMRoleList = spsMRoleList;
    }
    /**
     * Gets the list of error message.
     * 
     * @return the errorMessageList.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }
    /**
     * Sets the list of error message.
     * 
     * @param errorMessageList the list of error message.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }
    
    
}
