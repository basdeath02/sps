/*
 * ModifyDate Development company Describe 
 * 2014/05/14 Parichat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ar.com.fdvs.dj.domain.DynamicReport;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class MaintenanceDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class MaintenanceDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6609674518902893438L;

    /** The table physical. */
    private String tablePhysical;
    
    /** The column physical. */
    private String columnPhysical;
    
    /** The criteria. */
    private String criteria;
    
    /** The sql statement. */
    private String sqlStatement;
    
    /** The order by. */
    private String orderBy;
    
    /** The table physical update. */
    private String tablePhysicalUpdate;
    
    /** The criteria update. */
    private String criteriaUpdate;
    
    /** The where criteria update. */
    private String whereCriteriaUpdate;

    /** The key for check. */
    private String keyForCheck;
    
    /** The key column for check. */
    private String keyColumnForCheck;
    
    /** The met column domain. */
    private MetColumnDomain metColumnDomain;
    
    /** The m column list. */
    private List<MetColumnDomain> mColumnList;
    
    /** The file name. */
    private String fileName;
    
    /** The file size. */
    private int fileSize;
    
    /** The data field. */
    private String[] dataFieldArr = new String[]{};
    
    /** The flag transaction. */
    private String[] flagTransactionArr = new String[]{};
    
    /** The update date. */
    private String[] updateDateArr = new String[]{};
    
    /** The primary key. */
    private String[] primaryKeyArr = new String[]{};
    
    /** The key for check. */
    private MetTableDomain metTableDomain;
    
    /** The maintenance form. */
    /** The criteria. */
    private String[] criteriaArr = new String[Constants.TEN];
    
    /** The criteria support date. */
    private String[] criteriaSupportDate = new String[Constants.TEN];
    
    /** The result list. */
    private List<Map<String, Object>> resultList;
    
    /** The dynamic report. */
    private DynamicReport  dynamicReport;
    
    /** The import byte arr. */
    private ByteArrayOutputStream importByteArr;
    
    /** The first row. */
    private Integer firstRow;
    
    /** The last row. */
    private Integer lastRow;
    
    /** The target page. */
    private String targetPage;
    
    /** The flag transaction. */
    private String flagTransaction;
    
    /** The create date. */
    private Timestamp createDate;
    
    /** The create by. */
    private String createBy;
    
    /** The update date. */
    private Timestamp updateDate;
    
    /** The update by. */
    private String updateBy;
    
    /** The active flag. */
    private String activeFlag;
    
    /** The sort by. */
    private String sortBy;
    
    /** The current date. */
    private Timestamp currentDate;
    
    /** The locale. */
    private Locale locale;
    
    //Added by Parichat K. 2014-05-14
    /** The header Flag. */
    private Boolean headerFlag;
    
    /** The header. */
    private String[] headerArr = new String[]{};
    
    /**
     * Instantiates a new maintenance domain.
     */
    public MaintenanceDomain() {
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the table physical.
     * 
     * @return the table physical
     */
    public String getTablePhysical() {
        return tablePhysical;
    }

    /**
     * Sets the table physical.
     * 
     * @param tablePhysical the new table physical
     */
    public void setTablePhysical(String tablePhysical) {
        this.tablePhysical = tablePhysical;
    }

    /**
     * Gets the column physical.
     * 
     * @return the column physical
     */
    public String getColumnPhysical() {
        return columnPhysical;
    }

    /**
     * Sets the column physical.
     * 
     * @param columnPhysical the new column physical
     */
    public void setColumnPhysical(String columnPhysical) {
        this.columnPhysical = columnPhysical;
    }

    /**
     * Gets the criteria.
     * 
     * @return the criteria
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * Sets the criteria.
     * 
     * @param criteria the new criteria
     */
    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    /**
     * Gets the sql statement.
     * 
     * @return the sql statement
     */
    public String getSqlStatement() {
        return sqlStatement;
    }

    /**
     * Sets the sql statement.
     * 
     * @param sqlStatement the new sql statement
     */
    public void setSqlStatement(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }

    /**
     * Gets the table physical update.
     * 
     * @return the table physical update
     */
    public String getTablePhysicalUpdate() {
        return tablePhysicalUpdate;
    }

    /**
     * Sets the table physical update.
     * 
     * @param tablePhysicalUpdate the new table physical update
     */
    public void setTablePhysicalUpdate(String tablePhysicalUpdate) {
        this.tablePhysicalUpdate = tablePhysicalUpdate;
    }

    /**
     * Gets the criteria update.
     * 
     * @return the criteria update
     */
    public String getCriteriaUpdate() {
        return criteriaUpdate;
    }

    /**
     * Sets the criteria update.
     * 
     * @param criteriaUpdate the new criteria update
     */
    public void setCriteriaUpdate(String criteriaUpdate) {
        this.criteriaUpdate = criteriaUpdate;
    }

    /**
     * Gets the where criteria update.
     * 
     * @return the where criteria update
     */
    public String getWhereCriteriaUpdate() {
        return whereCriteriaUpdate;
    }

    /**
     * Sets the where criteria update.
     * 
     * @param whereCriteriaUpdate the new where criteria update
     */
    public void setWhereCriteriaUpdate(String whereCriteriaUpdate) {
        this.whereCriteriaUpdate = whereCriteriaUpdate;
    }

    /**
     * Gets the order by.
     * 
     * @return the order by
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Sets the order by.
     * 
     * @param orderBy the new order by
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * Gets the key for check.
     * 
     * @return the key for check
     */
    public String getKeyForCheck() {
        return keyForCheck;
    }

    /**
     * Sets the key for check.
     * 
     * @param keyForCheck the new key for check
     */
    public void setKeyForCheck(String keyForCheck) {
        this.keyForCheck = keyForCheck;
    }

    /**
     * Gets the key column for check.
     * 
     * @return the key column for check
     */
    public String getKeyColumnForCheck() {
        return keyColumnForCheck;
    }

    /**
     * Sets the key column for check.
     * 
     * @param keyColumnForCheck the new key column for check
     */
    public void setKeyColumnForCheck(String keyColumnForCheck) {
        this.keyColumnForCheck = keyColumnForCheck;
    }

    /**
     * Gets the met column domain.
     * 
     * @return the met column domain
     */
    public MetColumnDomain getMetColumnDomain() {
        return metColumnDomain;
    }

    /**
     * Sets the met column domain.
     * 
     * @param metColumnDomain the new met column domain
     */
    public void setMetColumnDomain(MetColumnDomain metColumnDomain) {
        this.metColumnDomain = metColumnDomain;
    }

    /**
     * Gets the m column list.
     * 
     * @return the m column list
     */
    public List<MetColumnDomain> getMColumnList() {
        return mColumnList;
    }

    /**
     * Sets the m column list.
     * 
     * @param columnList the new m column list
     */
    public void setMColumnList(List<MetColumnDomain> columnList) {
        mColumnList = columnList;
    }

    /**
     * Gets the form.
     * 
     * @return the form
     */
//    public CoreActionForm getForm() {
//        return form;
//    }

    /**
     * Sets the form.
     * 
     * @param form the new form
     */
//    public void setForm(CoreActionForm form) {
//        this.form = form;
//    }

    /**
     * Gets the form file.
     * 
     * @return the form file
     */
//    public FormFile getFormFile() {
//        return formFile;
//    }

    /**
     * Sets the form file.
     * 
     * @param formFile the new form file
     */
//    public void setFormFile(FormFile formFile) {
//        this.formFile = formFile;
//    }


    /**
     * Gets the met table domain.
     * 
     * @return the met table domain
     */
    public MetTableDomain getMetTableDomain() {
        return metTableDomain;
    }

    /**
     * Sets the met table domain.
     * 
     * @param metTableDomain the new met table domain
     */
    public void setMetTableDomain(MetTableDomain metTableDomain) {
        this.metTableDomain = metTableDomain;
    }

    /**
     * Gets the data field arr.
     * 
     * @return the data field arr
     */
    public String[] getDataFieldArr() {
        return dataFieldArr;
    }

    /**
     * Sets the data field arr.
     * 
     * @param dataFieldArr the new data field arr
     */
    public void setDataFieldArr(String[] dataFieldArr) {
        this.dataFieldArr = dataFieldArr;
    }

    /**
     * Gets the flag transaction arr.
     * 
     * @return the flag transaction arr
     */
    public String[] getFlagTransactionArr() {
        return flagTransactionArr;
    }

    /**
     * Sets the flag transaction arr.
     * 
     * @param flagTransactionArr the new flag transaction arr
     */
    public void setFlagTransactionArr(String[] flagTransactionArr) {
        this.flagTransactionArr = flagTransactionArr;
    }

    /**
     * Gets the update date arr.
     * 
     * @return the update date arr
     */
    public String[] getUpdateDateArr() {
        return updateDateArr;
    }

    /**
     * Sets the update date arr.
     * 
     * @param updateDateArr the new update date arr
     */
    public void setUpdateDateArr(String[] updateDateArr) {
        this.updateDateArr = updateDateArr;
    }

    /**
     * Gets the primary key arr.
     * 
     * @return the primary key arr
     */
    public String[] getPrimaryKeyArr() {
        return primaryKeyArr;
    }

    /**
     * Sets the primary key arr.
     * 
     * @param primaryKeyArr the new primary key arr
     */
    public void setPrimaryKeyArr(String[] primaryKeyArr) {
        this.primaryKeyArr = primaryKeyArr;
    }

    /**
     * Gets the file name.
     * 
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the file name.
     * 
     * @param fileName the new file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    /**
     * Gets the criteria array.
     * 
     * @return the criteria array
     */
    public String[] getCriteriaArr() {
        return criteriaArr;
    }

    /**
     * Sets the criteria array.
     * 
     * @param criteriaArr the new criteria array
     */
    public void setCriteriaArr(String[] criteriaArr) {
        this.criteriaArr = criteriaArr;
    }

    /**
     * Gets the criteria support date.
     * 
     * @return the criteria support date
     */
    public String[] getCriteriaSupportDate() {
        return criteriaSupportDate;
    }

    /**
     * Sets the criteria support date.
     * 
     * @param criteriaSupportDate the new criteria support date
     */
    public void setCriteriaSupportDate(String[] criteriaSupportDate) {
        this.criteriaSupportDate = criteriaSupportDate;
    }

    /**
     * Gets the result list.
     * 
     * @return the result list
     */
    public List<Map<String, Object>> getResultList() {
        return resultList;
    }

    /**
     * Sets the result list.
     * 
     * @param resultList the result list
     */
    public void setResultList(List<Map<String, Object>> resultList) {
        this.resultList = resultList;
    }

    /**
     * Gets the dynamic report.
     * 
     * @return the dynamic report
     */
    public DynamicReport getDynamicReport() {
        return dynamicReport;
    }

    /**
     * Sets the dynamic report.
     * 
     * @param dynamicReport the new dynamic report
     */
    public void setDynamicReport(DynamicReport dynamicReport) {
        this.dynamicReport = dynamicReport;
    }

    /**
     * Gets the import byte arr.
     * 
     * @return the import byte arr
     */
    public ByteArrayOutputStream getImportByteArr() {
        return importByteArr;
    }

    /**
     * Sets the import byte arr.
     * 
     * @param importByteArr the new import byte arr
     */
    public void setImportByteArr(ByteArrayOutputStream importByteArr) {
        this.importByteArr = importByteArr;
    }

    /**
     * Gets the m column list.
     * 
     * @return the m column list
     */
    public List<MetColumnDomain> getmColumnList() {
        return mColumnList;
    }

    /**
     * Sets the m column list.
     * 
     * @param mColumnList the new m column list
     */
    public void setmColumnList(List<MetColumnDomain> mColumnList) {
        this.mColumnList = mColumnList;
    }

    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }

    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    /**
     * Gets the flag transaction.
     * 
     * @return the flag transaction
     */
    public String getFlagTransaction() {
        return flagTransaction;
    }

    /**
     * Sets the flag transaction.
     * 
     * @param flagTransaction the new flag transaction
     */
    public void setFlagTransaction(String flagTransaction) {
        this.flagTransaction = flagTransaction;
    }

    /**
     * Gets the creates the date.
     * 
     * @return the creates the date
     */
    public Timestamp getCreateDate() {
        return createDate;
    }

    /**
     * Sets the creates the date.
     * 
     * @param createDate the new creates the date
     */
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    /**
     * Gets the creates the by.
     * 
     * @return the creates the by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * Sets the creates the by.
     * 
     * @param createBy the new creates the by
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * Gets the update date.
     * 
     * @return the update date
     */
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the update date.
     * 
     * @param updateDate the new update date
     */
    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Gets the update by.
     * 
     * @return the update by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * Sets the update by.
     * 
     * @param updateBy the new update by
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * Gets the active flag.
     * 
     * @return the active flag
     */
    public String getActiveFlag() {
        return activeFlag;
    }

    /**
     * Sets the active flag.
     * 
     * @param activeFlag the new active flag
     */
    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    /**
     * Gets the sort by.
     * 
     * @return the sort by
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Sets the sort by.
     * 
     * @param sortBy the new sort by
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * Gets the current date.
     * 
     * @return the current date
     */
    public Timestamp getCurrentDate() {
        return currentDate;
    }

    /**
     * Sets the current date.
     * 
     * @param currentDate the new current date
     */
    public void setCurrentDate(Timestamp currentDate) {
        this.currentDate = currentDate;
    }

    /**
     * Gets the locale.
     * 
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Sets the locale.
     * 
     * @param locale the new locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
    
    /**
     * Gets the first row.
     * 
     * @return the first row
     */
    public Integer getFirstRow() {
        return firstRow;
    }

    /**
     * Sets the first row.
     * 
     * @param firstRow the new first row
     */
    public void setFirstRow(Integer firstRow) {
        this.firstRow = firstRow;
    }

    /**
     * Gets the last row.
     * 
     * @return the last row
     */
    public Integer getLastRow() {
        return lastRow;
    }

    /**
     * Sets the last row.
     * 
     * @param lastRow the new last row
     */
    public void setLastRow(Integer lastRow) {
        this.lastRow = lastRow;
    }

    /**
     * Gets the file size.
     * 
     * @return the file size
     */
    public int getFileSize() {
        return fileSize;
    }

    /**
     * Sets the file size.
     * 
     * @param fileSize the new file size
     */
    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * Gets the header Flag.
     * 
     * @return the header Flag
     */
    public Boolean getHeaderFlag() {
        return headerFlag;
    }

    /**
     * Sets the header Flag.
     * 
     * @param headerFlag the new header Flag
     */
    public void setHeaderFlag(Boolean headerFlag) {
        this.headerFlag = headerFlag;
    }

    /**
     * Gets the header arr.
     * 
     * @return the header arr
     */
    public String[] getHeaderArr() {
        return headerArr;
    }

    /**
     * Sets the header arr.
     * 
     * @param headerArr the new header arr
     */
    public void setHeaderArr(String[] headerArr) {
        this.headerArr = headerArr;
    }
}