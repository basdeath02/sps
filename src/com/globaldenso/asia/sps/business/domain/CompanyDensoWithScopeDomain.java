/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Akat                Create
 * 
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class DensoSupplierRelationDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class CompanyDensoWithScopeDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 4750695616245775398L;
    
    /** The Company DENSO domain. */
    private CompanyDensoDomain companyDensoDomain;
    
    /** The Data Scope Control domain */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The default constructor. */
    public CompanyDensoWithScopeDomain() {
        super();
        this.dataScopeControlDomain = new DataScopeControlDomain();
        this.companyDensoDomain = new CompanyDensoDomain();
    }

    /**
     * Get method for companyDensoDomain.
     * @return the companyDensoDomain
     */
    public CompanyDensoDomain getCompanyDensoDomain() {
        return companyDensoDomain;
    }

    /**
     * Set method for companyDensoDomain.
     * @param companyDensoDomain the companyDensoDomain to set
     */
    public void setCompanyDensoDomain(CompanyDensoDomain companyDensoDomain) {
        this.companyDensoDomain = companyDensoDomain;
    }

    /**
     * Get method for dataScopeControlDomain.
     * @return the dataScopeControlDomain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Set method for dataScopeControlDomain.
     * @param dataScopeControlDomain the dataScopeControlDomain to set
     */
    public void setDataScopeControlDomain(DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }

}
