/*
 * ModifyDate Development company     Describe 
 * 2014/09/01 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;

/**
 * <p>Class D/O Created ASN Domain.</p>
 *
 * @author CSI
 */
public class DoCreatedAsnDomain extends BaseDomain implements Serializable {

    /**
     * <p>The Constant serialVersionUID..</p>
     */
    private static final long serialVersionUID = -5226059355457287656L;

    /** ASN Domain */
    private SpsTAsnDomain spsTAsnDomain;
    
    /** ASN Domain */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** User Domain */
    private SpsMUserDomain spsMUserDomain;
    
    /**
     * 
     * <p>The default constructor.</p>
     *
     */
    public DoCreatedAsnDomain() {
        super();
        spsTAsnDomain = new SpsTAsnDomain();
        spsTAsnDetailDomain = new SpsTAsnDetailDomain();
        spsMUserDomain = new SpsMUserDomain();
    }

    /**
     * <p>Getter method for spsTAsnDomain.</p>
     *
     * @return the spsTAsnDomain
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }

    /**
     * <p>Setter method for spsTAsnDomain.</p>
     *
     * @param spsTAsnDomain Set for spsTAsnDomain
     */
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }

    /**
     * <p>Getter method for spsMUserDomain.</p>
     *
     * @return the spsMUserDomain
     */
    public SpsMUserDomain getSpsMUserDomain() {
        return spsMUserDomain;
    }

    /**
     * <p>Setter method for spsMUserDomain.</p>
     *
     * @param spsMUserDomain Set for spsMUserDomain
     */
    public void setSpsMUserDomain(SpsMUserDomain spsMUserDomain) {
        this.spsMUserDomain = spsMUserDomain;
    }

    /**
     * <p>Getter method for spsTAsnDetailDomain.</p>
     *
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }

    /**
     * <p>Setter method for spsTAsnDetailDomain.</p>
     *
     * @param spsTAsnDetailDomain Set for spsTAsnDetailDomain
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }
}
