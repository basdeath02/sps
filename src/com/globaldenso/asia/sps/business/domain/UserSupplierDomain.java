/*
 * ModifyDate Development company     Describe 
 * 2014/04/23 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class User Supplier Domain.
 * @author CSI
 */
public class UserSupplierDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5256814318966032432L;

    /** The Company Supplier Code. */
    private String sCd;
   
    /** The Plant Supplier. */
    private String sPcd;
    
    /** The DENSO owner. */
    private String densoOwner;
       
    /** The Email urgent order flag. */
    private String emlUrgentFlag;
    
    /** The email supplier info not found.*/
    private String emlInfoFlag;
    
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The Active flag. */
    private String isActive;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
            
    /** The Date time update. */
    private Timestamp updateDatetime;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    
    /**
     * Instantiates a new user Supplier domain.
     */
    public UserSupplierDomain() {
        super();       
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Gets the Company supplier code
     * 
     * @return the Company supplier code
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the Company supplier code
     * 
     * @param sCd the Company supplier code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * Gets the Plant supplier code
     * 
     * @return the Plant supplier code
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the Plant supplier code
     * 
     * @param sPcd the Plant supplier code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * Gets the DENSO Owner.
     * 
     * @return the DENSO Owner.
     */
    public String getDensoOwner() {
        return densoOwner;
    }

    /**
     * Sets the DENSO Owner.
     * 
     * @param densoOwner the DENSO Owner.
     */
    public void setDensoOwner(String densoOwner) {
        this.densoOwner = densoOwner;
    }

    /**
     * Gets the email urgent flag.
     * 
     * @return the email urgent flag.
     */
    public String getEmlUrgentFlag() {
        return emlUrgentFlag;
    }

    /**
     * Sets the email urgent flag.
     * 
     * @param emlUrgentFlag the email urgent flag.
     */
    public void setEmlUrgentFlag(String emlUrgentFlag) {
        this.emlUrgentFlag = emlUrgentFlag;
    }

    /**
     * Gets the Email supplier info not found flag.
     * 
     * @return the Email supplier info not found flag.
     */
    public String getEmlInfoFlag() {
        return emlInfoFlag;
    }

    /**
     * Sets the Email supplier info not found flag.
     * 
     * @param emlInfoFlag the Email supplier info not found flag.
     */
    public void setEmlInfoFlag(String emlInfoFlag) {
        this.emlInfoFlag = emlInfoFlag;
    }
    
    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public Timestamp getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(Timestamp updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
    
    /**
     * Gets the supplier plant code
     * 
     * @return the supplier plant code
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }

    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }
    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }

    /**
     * Gets the Active Flag.
     * 
     * @return the  Active Flag.
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the  Active Flag.
     * 
     * @param isActive the  Active Flag.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    
}