/*
 * ModifyDate Development company     Describe 
 * 2014/08/26 CSI Karnrawee           Create
 * 2015/08/24 CSI Akat                [IN012]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class PurgingPoDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 294892872809778523L;
    
    /** The company denso code. */
    private String dCd;
    
    /** The date start purge. */
    private String dateStartPurge;
    
    /** The is completee. */
    private boolean isComplete;
    
    /** The purchase order id. */
    private String purchaseOrderId;
    
    /** The shipment status. */
    private String shipmentStatus;
    
    /** The list of pdf original file id. */
    private String pdfOriginalFileId;
    
    /** The list of pdf change file id. */
    private String pdfChangeFileId;
    
    /** The is dummy. */
    private boolean isDummy;
    
    /** The list of Purging DO Domain. */
    private List<PurgingDoDomain> purgingDoList;
    
    // [IN012] Start : add property for send constant to SqlMap
    /** Shipment Status Cancel. */
    private String shipmentStatusCancel;
    
    /** Shipment Status Complete Shipped. */
    private String shipmentStatusComplete;
    // [IN012] End : add property for send constant to SqlMap
    
    /**
     * Instantiates a new file upload domain.
     */
    public PurgingPoDomain() {
        super();
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dateStartPurge.</p>
     *
     * @return the dateStartPurge
     */
    public String getDateStartPurge() {
        return dateStartPurge;
    }

    /**
     * <p>Setter method for dateStartPurge.</p>
     *
     * @param dateStartPurge Set for dateStartPurge
     */
    public void setDateStartPurge(String dateStartPurge) {
        this.dateStartPurge = dateStartPurge;
    }

    /**
     * <p>Getter method for isComplete.</p>
     *
     * @return the isComplete
     */
    public boolean getIsComplete() {
        return isComplete;
    }

    /**
     * <p>Setter method for isComplete.</p>
     *
     * @param isComplete Set for isComplete
     */
    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    /**
     * <p>Getter method for purchaseOrderId.</p>
     *
     * @return the purchaseOrderId
     */
    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    /**
     * <p>Setter method for purchaseOrderId.</p>
     *
     * @param purchaseOrderId Set for purchaseOrderId
     */
    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    /**
     * <p>Getter method for shipmentStatus.</p>
     *
     * @return the shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * <p>Setter method for shipmentStatus.</p>
     *
     * @param shipmentStatus Set for shipmentStatus
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * <p>Getter method for pdfOriginalFileId.</p>
     *
     * @return the pdfOriginalFileId
     */
    public String getPdfOriginalFileId() {
        return pdfOriginalFileId;
    }

    /**
     * <p>Setter method for pdfOriginalFileId.</p>
     *
     * @param pdfOriginalFileId Set for pdfOriginalFileId
     */
    public void setPdfOriginalFileId(String pdfOriginalFileId) {
        this.pdfOriginalFileId = pdfOriginalFileId;
    }

    /**
     * <p>Getter method for pdfChangeFileId.</p>
     *
     * @return the pdfChangeFileId
     */
    public String getPdfChangeFileId() {
        return pdfChangeFileId;
    }

    /**
     * <p>Setter method for pdfChangeFileId.</p>
     *
     * @param pdfChangeFileId Set for pdfChangeFileId
     */
    public void setPdfChangeFileId(String pdfChangeFileId) {
        this.pdfChangeFileId = pdfChangeFileId;
    }

    /**
     * <p>Getter method for isDummy.</p>
     *
     * @return the isDummy
     */
    public boolean getIsDummy() {
        return isDummy;
    }

    /**
     * <p>Setter method for isDummy.</p>
     *
     * @param isDummy Set for isDummy
     */
    public void setIsDummy(boolean isDummy) {
        this.isDummy = isDummy;
    }

    /**
     * <p>Getter method for purgingDoList.</p>
     *
     * @return the purgingDoList
     */
    public List<PurgingDoDomain> getPurgingDoList() {
        return purgingDoList;
    }

    /**
     * <p>Setter method for purgingDoList.</p>
     *
     * @param purgingDoList Set for purgingDoList
     */
    public void setPurgingDoList(List<PurgingDoDomain> purgingDoList) {
        this.purgingDoList = purgingDoList;
    }

    /**
     * <p>Getter method for shipmentStatusCancel.</p>
     *
     * @return the shipmentStatusCancel
     */
    public String getShipmentStatusCancel() {
        return shipmentStatusCancel;
    }

    /**
     * <p>Setter method for shipmentStatusCancel.</p>
     *
     * @param shipmentStatusCancel Set for shipmentStatusCancel
     */
    public void setShipmentStatusCancel(String shipmentStatusCancel) {
        this.shipmentStatusCancel = shipmentStatusCancel;
    }

    /**
     * <p>Getter method for shipmentStatusComplete.</p>
     *
     * @return the shipmentStatusComplete
     */
    public String getShipmentStatusComplete() {
        return shipmentStatusComplete;
    }

    /**
     * <p>Setter method for shipmentStatusComplete.</p>
     *
     * @param shipmentStatusComplete Set for shipmentStatusComplete
     */
    public void setShipmentStatusComplete(String shipmentStatusComplete) {
        this.shipmentStatusComplete = shipmentStatusComplete;
    }
    
}