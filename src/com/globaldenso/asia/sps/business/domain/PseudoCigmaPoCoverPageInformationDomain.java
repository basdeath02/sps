/*
 * ModifyDate Development company     Describe 
 * 2014/07/24  CSI Karnrawee          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/**
 * Pseudo PO Domain for receive value from Web Service
 * 
 * @author CSI Arnon
 * @version 1.0.0
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaPoCoverPageInformationDomain")
public class PseudoCigmaPoCoverPageInformationDomain extends PseudoBaseDomain
    implements Serializable
{
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -4851732967396360995L;
    /** The pseudo of Data Type */
    @XmlElement(name = "dataType")
    private String pseudoDataType;
    /** The pseudo of P/O No. */
    @XmlElement(name = "poNo")
    private String pseudoPoNo;
    /** The pseudo of Release No. */
    @XmlElement(name = "releaseNo")
    private String pseudoReleaseNo;
    /** The pseudo of Issue date */
    @XmlElement(name = "issueDate")
    private String pseudoIssueDate;
    /** The pseudo of Seller Contract Name */
    @XmlElement(name = "sellerContractName")
    private String pseudoSellerContractName;
    /** The pseudo of Seller Name */
    @XmlElement(name = "sellerName")
    private String pseudoSellerName;
    /** The pseudo of Seller Address1 */
    @XmlElement(name = "sellerAddress1")
    private String pseudoSellerAddress1;
    /** The pseudo of Seller Address2 */
    @XmlElement(name = "sellerAddress2")
    private String pseudoSellerAddress2;
    /** The pseudo of Seller Address3 */
    @XmlElement(name = "sellerAddress3")
    private String pseudoSellerAddress3;
    /** The pseudo of Seller Fax Number */
    @XmlElement(name = "sellerFaxNumber")
    private String pseudoSellerFaxNumber;
    /** The pseudo of Supplier Code */
    @XmlElement(name = "sCd")
    private String pseudoVendorCd;
    /** The pseudo of Payment Term */
    @XmlElement(name = "paymentTerm")
    private String pseudoPaymentTerm;
    /** The pseudo of Ship VIA */
    @XmlElement(name = "shipVia")
    private String pseudoShipVia;
    /** The pseudo of Price Term */
    @XmlElement(name = "priceTerm")
    private String pseudoPriceTerm;
    /** The pseudo of Purchaser Contract Name */
    @XmlElement(name = "purchaserContractName")
    private String pseudoPurchaserContractName;
    /** The pseudo of Purchaser Name */
    @XmlElement(name = "purchaserName")
    private String pseudoPurchaserName;
    /** The pseudo of Purchaser Address1 */
    @XmlElement(name = "purchaserAddress1")
    private String pseudoPurchaserAddress1;
    /** The pseudo of Purchaser Address2 */
    @XmlElement(name = "purchaserAddress2")
    private String pseudoPurchaserAddress2;
    /** The pseudo of Purchaser Address3 */
    @XmlElement(name = "purchaserAddress3")
    private String pseudoPurchaserAddress3;
    /** The pseudo of Due date */
    @XmlElement(name = "dueDate")
    private String pseudoDueDate;
    /** The pseudo of D/O Number */
    @XmlElement(name = "doNumber")
    private String pseudoDoNumber;
    /** The pseudo of SPS Flag */
    @XmlElement(name = "spsFlag")
    private String pseudoSpsFlag;
    /** The pseudo of Create by PGM ID */
    @XmlElement(name = "createByPgmId")
    private String pseudoCreateByPgmId;
    /** The pseudo of Create Date */
    @XmlElement(name = "createDate")
    private String pseudoCreateDate;
    /** The pseudo of Create Time */
    @XmlElement(name = "createTime")
    private String pseudoCreateTime;
    /** The pseudo of Update by PGM ID */
    @XmlElement(name = "updateByPgmId")
    private String pseudoUpdateByPgmId;
    /** The pseudo of Update Date */
    @XmlElement(name = "updateDate")
    private String pseudoUpdateDate;
    /** The pseudo of Update Time */
    @XmlElement(name = "updateTime")
    private String pseudoUpdateTime;
    /** The pseudo of P/O Force Ack knowledge */
    @XmlElement(name = "poAckDate")
    private String pseudoPoAckDate;
    /** Denso Plant Code */
    @XmlElement(name = "dPcd")
    private String pseudoDPcd;

    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public PseudoCigmaPoCoverPageInformationDomain() {
    }

    /**
     * <p>Getter method for pseudoDataType.</p>
     *
     * @return the pseudoDataType
     */
    public String getPseudoDataType() {
        return pseudoDataType;
    }

    /**
     * <p>Setter method for pseudoDataType.</p>
     *
     * @param pseudoDataType Set for pseudoDataType
     */
    public void setPseudoDataType(String pseudoDataType) {
        this.pseudoDataType = pseudoDataType;
    }

    /**
     * <p>Getter method for pseudoPONo.</p>
     *
     * @return the pseudoPONo
     */
    public String getPseudoPoNo() {
        return pseudoPoNo;
    }

    /**
     * <p>Setter method for pseudoPONo.</p>
     *
     * @param pseudoPoNo Set for pseudoPoNo
     */
    public void setPseudoPoNo(String pseudoPoNo) {
        this.pseudoPoNo = pseudoPoNo;
    }

    /**
     * <p>Getter method for pseudoReleaseNo.</p>
     *
     * @return the pseudoReleaseNo
     */
    public String getPseudoReleaseNo() {
        return pseudoReleaseNo;
    }

    /**
     * <p>Setter method for pseudoReleaseNo.</p>
     *
     * @param pseudoReleaseNo Set for pseudoReleaseNo
     */
    public void setPseudoReleaseNo(String pseudoReleaseNo) {
        this.pseudoReleaseNo = pseudoReleaseNo;
    }

    /**
     * <p>Getter method for pseudoIssueDate.</p>
     *
     * @return the pseudoIssueDate
     */
    public String getPseudoIssueDate() {
        return pseudoIssueDate;
    }

    /**
     * <p>Setter method for pseudoIssueDate.</p>
     *
     * @param pseudoIssueDate Set for pseudoIssueDate
     */
    public void setPseudoIssueDate(String pseudoIssueDate) {
        this.pseudoIssueDate = pseudoIssueDate;
    }

    /**
     * <p>Getter method for pseudoSellerContractName.</p>
     *
     * @return the pseudoSellerContractName
     */
    public String getPseudoSellerContractName() {
        return pseudoSellerContractName;
    }

    /**
     * <p>Setter method for pseudoSellerContractName.</p>
     *
     * @param pseudoSellerContractName Set for pseudoSellerContractName
     */
    public void setPseudoSellerContractName(String pseudoSellerContractName) {
        this.pseudoSellerContractName = pseudoSellerContractName;
    }

    /**
     * <p>Getter method for pseudoSellerName.</p>
     *
     * @return the pseudoSellerName
     */
    public String getPseudoSellerName() {
        return pseudoSellerName;
    }

    /**
     * <p>Setter method for pseudoSellerName.</p>
     *
     * @param pseudoSellerName Set for pseudoSellerName
     */
    public void setPseudoSellerName(String pseudoSellerName) {
        this.pseudoSellerName = pseudoSellerName;
    }

    /**
     * <p>Getter method for pseudoSellerAddress1.</p>
     *
     * @return the pseudoSellerAddress1
     */
    public String getPseudoSellerAddress1() {
        return pseudoSellerAddress1;
    }

    /**
     * <p>Setter method for pseudoSellerAddress1.</p>
     *
     * @param pseudoSellerAddress1 Set for pseudoSellerAddress1
     */
    public void setPseudoSellerAddress1(String pseudoSellerAddress1) {
        this.pseudoSellerAddress1 = pseudoSellerAddress1;
    }

    /**
     * <p>Getter method for pseudoSellerAddress2.</p>
     *
     * @return the pseudoSellerAddress2
     */
    public String getPseudoSellerAddress2() {
        return pseudoSellerAddress2;
    }

    /**
     * <p>Setter method for pseudoSellerAddress2.</p>
     *
     * @param pseudoSellerAddress2 Set for pseudoSellerAddress2
     */
    public void setPseudoSellerAddress2(String pseudoSellerAddress2) {
        this.pseudoSellerAddress2 = pseudoSellerAddress2;
    }

    /**
     * <p>Getter method for pseudoSellerAddress3.</p>
     *
     * @return the pseudoSellerAddress3
     */
    public String getPseudoSellerAddress3() {
        return pseudoSellerAddress3;
    }

    /**
     * <p>Setter method for pseudoSellerAddress3.</p>
     *
     * @param pseudoSellerAddress3 Set for pseudoSellerAddress3
     */
    public void setPseudoSellerAddress3(String pseudoSellerAddress3) {
        this.pseudoSellerAddress3 = pseudoSellerAddress3;
    }

    /**
     * <p>Getter method for pseudoSellerFaxNumber.</p>
     *
     * @return the pseudoSellerFaxNumber
     */
    public String getPseudoSellerFaxNumber() {
        return pseudoSellerFaxNumber;
    }

    /**
     * <p>Setter method for pseudoSellerFaxNumber.</p>
     *
     * @param pseudoSellerFaxNumber Set for pseudoSellerFaxNumber
     */
    public void setPseudoSellerFaxNumber(String pseudoSellerFaxNumber) {
        this.pseudoSellerFaxNumber = pseudoSellerFaxNumber;
    }

    /**
     * <p>Getter method for pseudoSupplierCode.</p>
     *
     * @return the pseudoSupplierCode
     */
    public String getPseudoVendorCd() {
        return pseudoVendorCd;
    }

    /**
     * <p>Setter method for pseudoSupplierCode.</p>
     *
     * @param vendorCd Set for pseudoSCd
     */
    public void setPseudoVendorCd(String vendorCd) {
        this.pseudoVendorCd = vendorCd;
    }

    /**
     * <p>Getter method for pseudoPaymentTerm.</p>
     *
     * @return the pseudoPaymentTerm
     */
    public String getPseudoPaymentTerm() {
        return pseudoPaymentTerm;
    }

    /**
     * <p>Setter method for pseudoPaymentTerm.</p>
     *
     * @param pseudoPaymentTerm Set for pseudoPaymentTerm
     */
    public void setPseudoPaymentTerm(String pseudoPaymentTerm) {
        this.pseudoPaymentTerm = pseudoPaymentTerm;
    }

    /**
     * <p>Getter method for pseudoShipVia.</p>
     *
     * @return the pseudoShipVia
     */
    public String getPseudoShipVia() {
        return pseudoShipVia;
    }

    /**
     * <p>Setter method for pseudoShipVia.</p>
     *
     * @param pseudoShipVia Set for pseudoShipVia
     */
    public void setPseudoShipVia(String pseudoShipVia) {
        this.pseudoShipVia = pseudoShipVia;
    }

    /**
     * <p>Getter method for pseudoPriceTerm.</p>
     *
     * @return the pseudoPriceTerm
     */
    public String getPseudoPriceTerm() {
        return pseudoPriceTerm;
    }

    /**
     * <p>Setter method for pseudoPriceTerm.</p>
     *
     * @param pseudoPriceTerm Set for pseudoPriceTerm
     */
    public void setPseudoPriceTerm(String pseudoPriceTerm) {
        this.pseudoPriceTerm = pseudoPriceTerm;
    }

    /**
     * <p>Getter method for pseudoPurchaserContractName.</p>
     *
     * @return the pseudoPurchaserContractName
     */
    public String getPseudoPurchaserContractName() {
        return pseudoPurchaserContractName;
    }

    /**
     * <p>Setter method for pseudoPurchaserContractName.</p>
     *
     * @param pseudoPurchaserContractName Set for pseudoPurchaserContractName
     */
    public void setPseudoPurchaserContractName(String pseudoPurchaserContractName) {
        this.pseudoPurchaserContractName = pseudoPurchaserContractName;
    }

    /**
     * <p>Getter method for pseudoPurchaserName.</p>
     *
     * @return the pseudoPurchaserName
     */
    public String getPseudoPurchaserName() {
        return pseudoPurchaserName;
    }

    /**
     * <p>Setter method for pseudoPurchaserName.</p>
     *
     * @param pseudoPurchaserName Set for pseudoPurchaserName
     */
    public void setPseudoPurchaserName(String pseudoPurchaserName) {
        this.pseudoPurchaserName = pseudoPurchaserName;
    }

    /**
     * <p>Getter method for pseudoPurchaserAddress1.</p>
     *
     * @return the pseudoPurchaserAddress1
     */
    public String getPseudoPurchaserAddress1() {
        return pseudoPurchaserAddress1;
    }

    /**
     * <p>Setter method for pseudoPurchaserAddress1.</p>
     *
     * @param pseudoPurchaserAddress1 Set for pseudoPurchaserAddress1
     */
    public void setPseudoPurchaserAddress1(String pseudoPurchaserAddress1) {
        this.pseudoPurchaserAddress1 = pseudoPurchaserAddress1;
    }

    /**
     * <p>Getter method for pseudoPurchaserAddress2.</p>
     *
     * @return the pseudoPurchaserAddress2
     */
    public String getPseudoPurchaserAddress2() {
        return pseudoPurchaserAddress2;
    }

    /**
     * <p>Setter method for pseudoPurchaserAddress2.</p>
     *
     * @param pseudoPurchaserAddress2 Set for pseudoPurchaserAddress2
     */
    public void setPseudoPurchaserAddress2(String pseudoPurchaserAddress2) {
        this.pseudoPurchaserAddress2 = pseudoPurchaserAddress2;
    }

    /**
     * <p>Getter method for pseudoPurchaserAddress3.</p>
     *
     * @return the pseudoPurchaserAddress3
     */
    public String getPseudoPurchaserAddress3() {
        return pseudoPurchaserAddress3;
    }

    /**
     * <p>Setter method for pseudoPurchaserAddress3.</p>
     *
     * @param pseudoPurchaserAddress3 Set for pseudoPurchaserAddress3
     */
    public void setPseudoPurchaserAddress3(String pseudoPurchaserAddress3) {
        this.pseudoPurchaserAddress3 = pseudoPurchaserAddress3;
    }

    /**
     * <p>Getter method for pseudoDueDate.</p>
     *
     * @return the pseudoDueDate
     */
    public String getPseudoDueDate() {
        return pseudoDueDate;
    }

    /**
     * <p>Setter method for pseudoDueDate.</p>
     *
     * @param pseudoDueDate Set for pseudoDueDate
     */
    public void setPseudoDueDate(String pseudoDueDate) {
        this.pseudoDueDate = pseudoDueDate;
    }

    /**
     * <p>Getter method for pseudoDONumber.</p>
     *
     * @return the pseudoDONumber
     */
    public String getPseudoDoNumber() {
        return pseudoDoNumber;
    }

    /**
     * <p>Setter method for pseudoDONumber.</p>
     *
     * @param pseudoDoNumber Set for pseudoDoNumber
     */
    public void setPseudoDoNumber(String pseudoDoNumber) {
        this.pseudoDoNumber = pseudoDoNumber;
    }

    /**
     * <p>Getter method for pseudoSpsFlag.</p>
     *
     * @return the pseudoSpsFlag
     */
    public String getPseudoSpsFlag() {
        return pseudoSpsFlag;
    }

    /**
     * <p>Setter method for pseudoSpsFlag.</p>
     *
     * @param pseudoSpsFlag Set for pseudoSpsFlag
     */
    public void setPseudoSpsFlag(String pseudoSpsFlag) {
        this.pseudoSpsFlag = pseudoSpsFlag;
    }

    /**
     * <p>Getter method for pseudoCreateByPgmId.</p>
     *
     * @return the pseudoCreateByPgmId
     */
    public String getPseudoCreateByPgmId() {
        return pseudoCreateByPgmId;
    }

    /**
     * <p>Setter method for pseudoCreateByPgmId.</p>
     *
     * @param pseudoCreateByPgmId Set for pseudoCreateByPgmId
     */
    public void setPseudoCreateByPgmId(String pseudoCreateByPgmId) {
        this.pseudoCreateByPgmId = pseudoCreateByPgmId;
    }

    /**
     * <p>Getter method for pseudoCreateDate.</p>
     *
     * @return the pseudoCreateDate
     */
    public String getPseudoCreateDate() {
        return pseudoCreateDate;
    }

    /**
     * <p>Setter method for pseudoCreateDate.</p>
     *
     * @param pseudoCreateDate Set for pseudoCreateDate
     */
    public void setPseudoCreateDate(String pseudoCreateDate) {
        this.pseudoCreateDate = pseudoCreateDate;
    }

    /**
     * <p>Getter method for pseudoCreateTime.</p>
     *
     * @return the pseudoCreateTime
     */
    public String getPseudoCreateTime() {
        return pseudoCreateTime;
    }

    /**
     * <p>Setter method for pseudoCreateTime.</p>
     *
     * @param pseudoCreateTime Set for pseudoCreateTime
     */
    public void setPseudoCreateTime(String pseudoCreateTime) {
        this.pseudoCreateTime = pseudoCreateTime;
    }

    /**
     * <p>Getter method for pseudoUpdateByPgmId.</p>
     *
     * @return the pseudoUpdateByPgmId
     */
    public String getPseudoUpdateByPgmId() {
        return pseudoUpdateByPgmId;
    }

    /**
     * <p>Setter method for pseudoUpdateByPgmId.</p>
     *
     * @param pseudoUpdateByPgmId Set for pseudoUpdateByPgmId
     */
    public void setPseudoUpdateByPgmId(String pseudoUpdateByPgmId) {
        this.pseudoUpdateByPgmId = pseudoUpdateByPgmId;
    }

    /**
     * <p>Getter method for pseudoUpdateDate.</p>
     *
     * @return the pseudoUpdateDate
     */
    public String getPseudoUpdateDate() {
        return pseudoUpdateDate;
    }

    /**
     * <p>Setter method for pseudoUpdateDate.</p>
     *
     * @param pseudoUpdateDate Set for pseudoUpdateDate
     */
    public void setPseudoUpdateDate(String pseudoUpdateDate) {
        this.pseudoUpdateDate = pseudoUpdateDate;
    }

    /**
     * <p>Getter method for pseudoUpdateTime.</p>
     *
     * @return the pseudoUpdateTime
     */
    public String getPseudoUpdateTime() {
        return pseudoUpdateTime;
    }

    /**
     * <p>Setter method for pseudoUpdateTime.</p>
     *
     * @param pseudoUpdateTime Set for pseudoUpdateTime
     */
    public void setPseudoUpdateTime(String pseudoUpdateTime) {
        this.pseudoUpdateTime = pseudoUpdateTime;
    }

    /**
     * <p>Getter method for pseudoPoAckDate.</p>
     *
     * @return the pseudoPoAckDate
     */
    public String getPseudoPoAckDate() {
        return pseudoPoAckDate;
    }

    /**
     * <p>Setter method for pseudoPoAckDate.</p>
     *
     * @param pseudoPoAckDate Set for pseudoPoAckDate
     */
    public void setPseudoPoAckDate(String pseudoPoAckDate) {
        this.pseudoPoAckDate = pseudoPoAckDate;
    }

    /**
     * <p>Getter method for pseudoDPcd.</p>
     *
     * @return the pseudoDPcd
     */
    public String getPseudoDPcd() {
        return pseudoDPcd;
    }

    /**
     * <p>Setter method for pseudoDPcd.</p>
     *
     * @param pseudoDPcd Set for pseudoDPcd
     */
    public void setPseudoDPcd(String pseudoDPcd) {
        this.pseudoDPcd = pseudoDPcd;
    }
}
