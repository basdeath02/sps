/*
 * ModifyDate Development company Describe 
 * 2014/07/15 Akat               Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;

/**
 * The Class UserMenuDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserMenuDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -4316941307663360823L;

    /** The domain for SPS_M_USER */
    private SpsMUserDomain spsMUserDomain;

    /** The domain for SPS_M_MENU */
    private SpsMMenuDomain spsMMenuDomain;
    
    /** The current date time. */
    private Timestamp currentDatetime;
    
    /**
     * The Default constructor.
     * */
    public UserMenuDomain() {
        super();
        this.spsMMenuDomain = new SpsMMenuDomain();
        this.spsMUserDomain = new SpsMUserDomain();
    }

    /**
     * Get method for spsMUserDomain.
     * @return the spsMUserDomain
     */
    public SpsMUserDomain getSpsMUserDomain() {
        return spsMUserDomain;
    }

    /**
     * Set method for spsMUserDomain.
     * @param spsMUserDomain the spsMUserDomain to set
     */
    public void setSpsMUserDomain(SpsMUserDomain spsMUserDomain) {
        this.spsMUserDomain = spsMUserDomain;
    }

    /**
     * Get method for spsMMenuDomain.
     * @return the spsMMenuDomain
     */
    public SpsMMenuDomain getSpsMMenuDomain() {
        return spsMMenuDomain;
    }

    /**
     * Set method for spsMMenuDomain.
     * @param spsMMenuDomain the spsMMenuDomain to set
     */
    public void setSpsMMenuDomain(SpsMMenuDomain spsMMenuDomain) {
        this.spsMMenuDomain = spsMMenuDomain;
    }

    /**
     * <p>Getter method for currentDatetime.</p>
     *
     * @return the currentDatetime
     */
    public Timestamp getCurrentDatetime() {
        return currentDatetime;
    }

    /**
     * <p>Setter method for currentDatetime.</p>
     *
     * @param currentDatetime Set for currentDatetime
     */
    public void setCurrentDatetime(Timestamp currentDatetime) {
        this.currentDatetime = currentDatetime;
    }
    
}
