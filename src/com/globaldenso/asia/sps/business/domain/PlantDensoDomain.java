/*
 * ModifyDate Development company Describe 
 * 2014/08/02 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Class PlantDensoDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class PlantDensoDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID. */
    private static final long serialVersionUID = 7948874298468915668L;

    /**
     * plant DENSO
     */
    private String dPcd;

    /**
     * Company DENSO code
     */
    private String dCd;

    /**
     * DENSO plant name
     */
    private String plantName;

    /**
     * DENSO plant address
     */
    private String address;

    /**
     * DENSO plant telephone no.
     */
    private String telephone;

    /**
     * DENSO Plant fax no.
     */
    private String fax;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create date time
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update date time
     */
    private Timestamp lastUpdateDatetime;

    /** The default constructor. */
    public PlantDensoDomain() {
        super();
    }

    /**
     * <p>Getter method for plant DENSO.</p>
     *
     * @return the plant DENSO
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for plant DENSO.</p>
     *
     * @param dPcd Set for plant DENSO
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for Company DENSO Code.</p>
     *
     * @return the Company DENSO Code.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for Company DENSO Code.</p>
     *
     * @param dCd Set for Company DENSO Code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for plantName.</p>
     *
     * @return the plantName
     */
    public String getPlantName() {
        return plantName;
    }

    /**
     * <p>Setter method for plantName.</p>
     *
     * @param plantName Set for plantName
     */
    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    /**
     * <p>Getter method for address.</p>
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * <p>Setter method for address.</p>
     *
     * @param address Set for address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * <p>Getter method for telephone.</p>
     *
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * <p>Setter method for telephone.</p>
     *
     * @param telephone Set for telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * <p>Getter method for fax.</p>
     *
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * <p>Setter method for fax.</p>
     *
     * @param fax Set for fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * <p>Getter method for isActive.</p>
     *
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * <p>Setter method for isActive.</p>
     *
     * @param isActive Set for isActive
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * <p>Getter method for createDscId.</p>
     *
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * <p>Setter method for createDscId.</p>
     *
     * @param createDscId Set for createDscId
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * <p>Getter method for createDatetime.</p>
     *
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * <p>Setter method for createDatetime.</p>
     *
     * @param createDatetime Set for createDatetime
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * <p>Getter method for lastUpdateDscId.</p>
     *
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>Setter method for lastUpdateDscId.</p>
     *
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>Getter method for lastUpdateDatetime.</p>
     *
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>Setter method for lastUpdateDatetime.</p>
     *
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    
}
