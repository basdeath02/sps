/*
 * ModifyDate Development company Describe 
 * 2014/05/14 Parichat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.fw.ContextParams;

/**
 * The Class MetTable.
 * 
 * @author CSI
 * @version 1.00
 */
public class MetTableDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1365583480769229949L;

    /** The table id. */
    private String tableId;

    /** The page name. */
    private String pageName;

    /** The table logical. */
    private String tableLogical;

    /** The table physical. */
    private String tablePhysical;
    
    /** The module. */
    private List<String> module;
    
    /** The row. */
    private Integer row;
    
    /** The target page. */
    private String targetPage;
    
    /** The flag transaction. */
    private String flagTransaction;
    
    /** The create date. */
    private Timestamp createDate;
    
    /** The create by. */
    private String createBy;
    
    /** The update date. */
    private Timestamp updateDate;
    
    /** The update by. */
    private String updateBy;
    
    /** The active flag. */
    private String activeFlag;
    
    /** The sort by. */
    private String sortBy;
    
    /** The current date. */
    private Timestamp currentDate;
    
    /** The locale. */
    private Locale locale;

    /**
     * Instantiates a new met table domain.
     */
    public MetTableDomain() {
        setRow(ContextParams.getDefaultRowPerPage());
        setTargetPage(Constants.PAGE_FIRST);
    }

    /**
     * Gets the table id.
     * 
     * @return the table id
     */
    public String getTableId() {
        return tableId;
    }

    /**
     * Sets the table id.
     * 
     * @param tableId the new table id
     */
    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    /**
     * Gets the page name.
     * 
     * @return the page name
     */
    public String getPageName() {
        return pageName;
    }

    /**
     * Sets the page name.
     * 
     * @param pageName the new page name
     */
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    /**
     * Gets the table logical.
     * 
     * @return the table logical
     */
    public String getTableLogical() {
        return tableLogical;
    }

    /**
     * Sets the table logical.
     * 
     * @param tableLogical the new table logical
     */
    public void setTableLogical(String tableLogical) {
        this.tableLogical = tableLogical;
    }

    /**
     * Gets the table physical.
     * 
     * @return the table physical
     */
    public String getTablePhysical() {
        return tablePhysical;
    }

    /**
     * Sets the table physical.
     * 
     * @param tablePhysical the new table physical
     */
    public void setTablePhysical(String tablePhysical) {
        this.tablePhysical = tablePhysical;
    }

    /**
     * Gets the module.
     * 
     * @return the module
     */
    public List<String> getModule() {
        return module;
    }

    /**
     * Sets the module.
     * 
     * @param module the new module
     */
    public void setModule(List<String> module) {
        this.module = module;
    }

    /**
     * Gets the row.
     * 
     * @return the row
     */
    public Integer getRow() {
        return row;
    }

    /**
     * Sets the row.
     * 
     * @param row the new row
     */
    public void setRow(Integer row) {
        this.row = row;
    }

    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }

    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    /**
     * Gets the flag transaction.
     * 
     * @return the flag transaction
     */
    public String getFlagTransaction() {
        return flagTransaction;
    }

    /**
     * Sets the flag transaction.
     * 
     * @param flagTransaction the new flag transaction
     */
    public void setFlagTransaction(String flagTransaction) {
        this.flagTransaction = flagTransaction;
    }

    /**
     * Gets the creates the date.
     * 
     * @return the creates the date
     */
    public Timestamp getCreateDate() {
        return createDate;
    }

    /**
     * Sets the creates the date.
     * 
     * @param createDate the new creates the date
     */
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    /**
     * Gets the creates the by.
     * 
     * @return the creates the by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * Sets the creates the by.
     * 
     * @param createBy the new creates the by
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * Gets the update date.
     * 
     * @return the update date
     */
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the update date.
     * 
     * @param updateDate the new update date
     */
    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Gets the update by.
     * 
     * @return the update by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * Sets the update by.
     * 
     * @param updateBy the new update by
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * Gets the active flag.
     * 
     * @return the active flag
     */
    public String getActiveFlag() {
        return activeFlag;
    }

    /**
     * Sets the active flag.
     * 
     * @param activeFlag the new active flag
     */
    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    /**
     * Gets the sort by.
     * 
     * @return the sort by
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Sets the sort by.
     * 
     * @param sortBy the new sort by
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * Gets the current date.
     * 
     * @return the current date
     */
    public Timestamp getCurrentDate() {
        return currentDate;
    }

    /**
     * Sets the current date.
     * 
     * @param currentDate the new current date
     */
    public void setCurrentDate(Timestamp currentDate) {
        this.currentDate = currentDate;
    }

    /**
     * Gets the locale.
     * 
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Sets the locale.
     * 
     * @param locale the new locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}