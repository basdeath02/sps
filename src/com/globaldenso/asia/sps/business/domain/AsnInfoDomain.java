/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;

/**
 * The Class ASN Info Domain.
 * @author CSI
 */
public class AsnInfoDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4595070392156863247L;

    /** The domain for SPS_T_ASN. */
    private SpsTAsnDomain spsTAsnDomain;
    
    /** The QR Type. */
    private String qrType;
    
    /** The DENSO company code. */
    private String dCd;
    
    /** The DENSO company name. */
    private String densoName;
    
    /** Supplier Company Name. */
    private String supplierName;
    
    /** Plan ETD in String type. */
    private String stringPlanEtd;
    
    /** Plan ETA in String type */
    private String stringPlanEta;
    
    /** Number of Pallet in String type. */
    private String stringNumberOfPallet;
    
    /** Create Datetime in String type. */
    private String stringCreateDatetime;
    
    /** Last Update Datetime in String type. */
    private String stringLastUpdateDatetime;
    
    /** The ASN info detail list. */
    private List<AsnInfoDetailDomain> asnInfoDetailList;

    /**
     * Instantiates a new ASN info domain.
     */
    public AsnInfoDomain() {
        super();
        this.spsTAsnDomain = new SpsTAsnDomain();
        this.asnInfoDetailList = new ArrayList<AsnInfoDetailDomain>();
    }

    /**
     * Gets the asn info detail list.
     * 
     * @return the asn info detail list.
     */
    public List<AsnInfoDetailDomain> getAsnInfoDetailList() {
        return asnInfoDetailList;
    }

    /**
     * Sets the asn info detail list.
     * 
     * @param asnInfoDetailList the asn info detail list.
     */
    public void setAsnInfoDetailList(List<AsnInfoDetailDomain> asnInfoDetailList) {
        this.asnInfoDetailList = asnInfoDetailList;
    }

    /**
     * <p>Getter method for spsTAsnDomain.</p>
     *
     * @return the spsTAsnDomain
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }

    /**
     * <p>Setter method for spsTAsnDomain.</p>
     *
     * @param spsTAsnDomain Set for spsTAsnDomain
     */
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }
    
    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * <p>Getter method for stringPlanEtd.</p>
     *
     * @return the stringPlanEtd
     */
    public String getStringPlanEtd() {
        return stringPlanEtd;
    }

    /**
     * <p>Setter method for stringPlanEtd.</p>
     *
     * @param stringPlanEtd Set for stringPlanEtd
     */
    public void setStringPlanEtd(String stringPlanEtd) {
        this.stringPlanEtd = stringPlanEtd;
    }

    /**
     * <p>Getter method for stringPlanEta.</p>
     *
     * @return the stringPlanEta
     */
    public String getStringPlanEta() {
        return stringPlanEta;
    }

    /**
     * <p>Setter method for stringPlanEta.</p>
     *
     * @param stringPlanEta Set for stringPlanEta
     */
    public void setStringPlanEta(String stringPlanEta) {
        this.stringPlanEta = stringPlanEta;
    }

    /**
     * <p>Getter method for stringNumberOfPallet.</p>
     *
     * @return the stringNumberOfPallet
     */
    public String getStringNumberOfPallet() {
        return stringNumberOfPallet;
    }

    /**
     * <p>Setter method for stringNumberOfPallet.</p>
     *
     * @param stringNumberOfPallet Set for stringNumberOfPallet
     */
    public void setStringNumberOfPallet(String stringNumberOfPallet) {
        this.stringNumberOfPallet = stringNumberOfPallet;
    }

    /**
     * <p>Getter method for stringCreateDatetime.</p>
     *
     * @return the stringCreateDatetime
     */
    public String getStringCreateDatetime() {
        return stringCreateDatetime;
    }

    /**
     * <p>Setter method for stringCreateDatetime.</p>
     *
     * @param stringCreateDatetime Set for stringCreateDatetime
     */
    public void setStringCreateDatetime(String stringCreateDatetime) {
        this.stringCreateDatetime = stringCreateDatetime;
    }

    /**
     * <p>Getter method for stringLastUpdateDatetime.</p>
     *
     * @return the stringLastUpdateDatetime
     */
    public String getStringLastUpdateDatetime() {
        return stringLastUpdateDatetime;
    }

    /**
     * <p>Setter method for stringLastUpdateDatetime.</p>
     *
     * @param stringLastUpdateDatetime Set for stringLastUpdateDatetime
     */
    public void setStringLastUpdateDatetime(String stringLastUpdateDatetime) {
        this.stringLastUpdateDatetime = stringLastUpdateDatetime;
    }

    /**
     * <p>Getter method for qrType.</p>
     *
     * @return the qrType
     */
    public String getQrType() {
        return qrType;
    }

    /**
     * <p>Setter method for qrType.</p>
     *
     * @param qrType Set for qrType
     */
    public void setQrType(String qrType) {
        this.qrType = qrType;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for densoName.</p>
     *
     * @return the densoName
     */
    public String getDensoName() {
        return densoName;
    }

    /**
     * <p>Setter method for densoName.</p>
     *
     * @param densoName Set for densoName
     */
    public void setDensoName(String densoName) {
        this.densoName = densoName;
    }
    
}