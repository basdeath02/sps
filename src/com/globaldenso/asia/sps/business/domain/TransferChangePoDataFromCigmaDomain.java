package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * 
 * <p>TransferPoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferChangePoDataFromCigmaDomain extends BaseDomain implements Serializable {
    /** Default Serial Version ID. */
    private static final long serialVersionUID = 3147991534259734705L;

    /** poType */
    private String poType;
    /** companyName */
    private String companyName;
    /** supplierCode */
    private String vendorCd;
    /** supplierName */
    private String supplierName;
    /** cigmaPoNo */
    private String cigmaPoNo;
    /** issueDate */
    private Date issueDate;
    /** dPlantCode */
    private String dPcd;
    /** dueDateFrom */
    private Date dueDateFrom;
    /** dueDateTo */
    private Date dueDateTo;
    /** releaseNo */
    private BigDecimal releaseNo;
    /** addTruckRouteFlag */
    private String addTruckRouteFlag;
    /** fax */
    private String fax;
    /** orderType */
    private String orderType;
    /** tr */
    private String tr;
    /** sellerName */
    private String sellerName;
    /** forceAckDate */
    private Date forceAckDate;
    /** tm */
    private String tm;
    /** Cigma Po Detail List */
    private List<TransferChangePoDetailDataFromCigmaDomain> cigmaPoDetailList;
    
    /** Supplier Plant Code(SPS) */
    private String sPcd;
    
    /** P/O Id(SPS) */
    private BigDecimal poId;
    
    /** Supplier Code(SPS) */
    private String sCd;
    
    /** Maximium Create Date Time */
    private String maxCreateDateTime;
    
    /** Maximium Update Date Time */
    private String maxUpdateDateTime;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferChangePoDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for poType.</p>
     *
     * @return the poType
     */
    public String getPoType() {
        return poType;
    }
    /**
     * <p>Setter method for poType.</p>
     *
     * @param poType Set for poType
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }
    /**
     * <p>Getter method for companyName.</p>
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }
    /**
     * <p>Setter method for companyName.</p>
     *
     * @param companyName Set for companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * <p>Setter method for sCd.</p>
     *
     * @param vendorCd Set for sCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }
    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    /**
     * <p>Getter method for cigmaPoNo.</p>
     *
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * <p>Setter method for cigmaPoNo.</p>
     *
     * @param cigmaPoNo Set for cigmaPoNo
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }
    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }
    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPcd Set for dPn
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * <p>Getter method for dueDateFrom.</p>
     *
     * @return the dueDateFrom
     */
    public Date getDueDateFrom() {
        return dueDateFrom;
    }
    /**
     * <p>Setter method for dueDateFrom.</p>
     *
     * @param dueDateFrom Set for dueDateFrom
     */
    public void setDueDateFrom(Date dueDateFrom) {
        this.dueDateFrom = dueDateFrom;
    }
    /**
     * <p>Getter method for dueDateTo.</p>
     *
     * @return the dueDateTo
     */
    public Date getDueDateTo() {
        return dueDateTo;
    }
    /**
     * <p>Setter method for dueDateTo.</p>
     *
     * @param dueDateTo Set for dueDateTo
     */
    public void setDueDateTo(Date dueDateTo) {
        this.dueDateTo = dueDateTo;
    }
    /**
     * <p>Getter method for releaseNo.</p>
     *
     * @return the releaseNo
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }
    /**
     * <p>Setter method for releaseNo.</p>
     *
     * @param releaseNo Set for releaseNo
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * <p>Getter method for fax.</p>
     *
     * @return the fax
     */
    public String getFax() {
        return fax;
    }
    /**
     * <p>Setter method for fax.</p>
     *
     * @param fax Set for fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }
    /**
     * <p>Getter method for orderType.</p>
     *
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }
    /**
     * <p>Setter method for orderType.</p>
     *
     * @param orderType Set for orderType
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    /**
     * <p>Getter method for tr.</p>
     *
     * @return the tr
     */
    public String getTr() {
        return tr;
    }
    /**
     * <p>Setter method for tr.</p>
     *
     * @param tr Set for tr
     */
    public void setTr(String tr) {
        this.tr = tr;
    }
    /**
     * <p>Getter method for sellerName.</p>
     *
     * @return the sellerName
     */
    public String getSellerName() {
        return sellerName;
    }
    /**
     * <p>Setter method for sellerName.</p>
     *
     * @param sellerName Set for sellerName
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
    /**
     * <p>Getter method for forceAckDate.</p>
     *
     * @return the forceAckDate
     */
    public Date getForceAckDate() {
        return forceAckDate;
    }
    /**
     * <p>Setter method for forceAckDate.</p>
     *
     * @param forceAckDate Set for forceAckDate
     */
    public void setForceAckDate(Date forceAckDate) {
        this.forceAckDate = forceAckDate;
    }
    /**
     * <p>Getter method for tm.</p>
     *
     * @return the tm
     */
    public String getTm() {
        return tm;
    }
    /**
     * <p>Setter method for tm.</p>
     *
     * @param tm Set for tm
     */
    public void setTm(String tm) {
        this.tm = tm;
    }
    /**
     * <p>Getter method for cigmaPoDetailList.</p>
     *
     * @return the cigmaPoDetailList
     */
    public List<TransferChangePoDetailDataFromCigmaDomain> getCigmaPoDetailList() {
        return cigmaPoDetailList;
    }
    /**
     * <p>Setter method for cigmaPoDetailList.</p>
     *
     * @param cigmaPoDetailList Set for cigmaPoDetailList
     */
    public void setCigmaPoDetailList(
        List<TransferChangePoDetailDataFromCigmaDomain> cigmaPoDetailList) {
        this.cigmaPoDetailList = cigmaPoDetailList;
    }
    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }
    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }
    /**
     * <p>Getter method for spsSCd.</p>
     *
     * @return the spsSCd
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * <p>Setter method for spsSCd.</p>
     *
     * @param sCd Set for spsSCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * <p>Getter method for maxCreateDateTime.</p>
     *
     * @return the maxCreateDateTime
     */
    public String getMaxCreateDateTime() {
        return maxCreateDateTime;
    }
    /**
     * <p>Setter method for maxCreateDateTime.</p>
     *
     * @param maxCreateDateTime Set for maxCreateDateTime
     */
    public void setMaxCreateDateTime(String maxCreateDateTime) {
        this.maxCreateDateTime = maxCreateDateTime;
    }
    /**
     * <p>Getter method for maxUpdateDateTime.</p>
     *
     * @return the maxUpdateDateTime
     */
    public String getMaxUpdateDateTime() {
        return maxUpdateDateTime;
    }
    /**
     * <p>Setter method for maxUpdateDateTime.</p>
     *
     * @param maxUpdateDateTime Set for maxUpdateDateTime
     */
    public void setMaxUpdateDateTime(String maxUpdateDateTime) {
        this.maxUpdateDateTime = maxUpdateDateTime;
    }
    /**
     * <p>Getter method for addTruckRouteFlag.</p>
     *
     * @return the addTruckRouteFlag
     */
    public String getAddTruckRouteFlag() {
        return addTruckRouteFlag;
    }
    /**
     * <p>Setter method for addTruckRouteFlag.</p>
     *
     * @param addTruckRouteFlag Set for addTruckRouteFlag
     */
    public void setAddTruckRouteFlag(String addTruckRouteFlag) {
        this.addTruckRouteFlag = addTruckRouteFlag;
    }
}
