/*
 * ModifyDate Development company Describe 
 * 2014/08/02 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Class CompanySupplierDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class CompanySupplierDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID. */
    private static final long serialVersionUID = -88428466006683884L;

    /**
     * Company supplier code.
     */
    private String sCd;

    /** Vendor code. */
    private String vendorCd;
    
    /**
     * Plant supplier code.
     */
    private String sPcd;
    
    /**
     * Company denso code.
     */
    private String dCd;
    
    /**
     * supplier tax id.
     */
    private String sTaxId;
    
    /**
     * Supplier company DSC ID
     */
    private String dscIdComCd;

    /**
     * Supplier company name
     */
    private String companyName;

    /**
     * Supplier address 1
     */
    private String address1;

    /**
     * Supplier address 2
     */
    private String address2;

    /**
     * Supplier address 3
     */
    private String address3;

    /**
     * Supplier telephone no.
     */
    private String telephone;

    /**
     * Supplier fax no.
     */
    private String fax;

    /**
     * Contact person name
     */
    private String contactPerson;

    /**
     * S_TAX_ID
     */
    private String supplierTaxId;

    /**
     * Tax code
     */
    private String taxCd;

    /**
     * Currency code
     */
    private String currencyCd;

    /**
     * Is active
     */
    private String isActive;
    
    /**
     * Decimal disp
     */
    private String decimalDisp;
    
    /**
     * Tax rate
     */
    private String taxRate;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create date time
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update date time
     */
    private Timestamp lastUpdateDatetime;
    
    /** The default constructor. */
    public CompanySupplierDomain() {
        super();
    }

    /**
     * <p>Getter method for company supplier code.</p>
     *
     * @return the company supplier code
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for company supplier code.</p>
     *
     * @param sCd Set for company supplier code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for sTaxId.</p>
     *
     * @return the sTaxId
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * <p>Setter method for sTaxId.</p>
     *
     * @param sTaxId Set for sTaxId
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * <p>Getter method for dscIdComCd.</p>
     *
     * @return the dscIdComCd
     */
    public String getDscIdComCd() {
        return dscIdComCd;
    }

    /**
     * <p>Setter method for dscIdComCd.</p>
     *
     * @param dscIdComCd Set for dscIdComCd
     */
    public void setDscIdComCd(String dscIdComCd) {
        this.dscIdComCd = dscIdComCd;
    }

    /**
     * <p>Getter method for companyName.</p>
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * <p>Setter method for companyName.</p>
     *
     * @param companyName Set for companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * <p>Getter method for address1.</p>
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * <p>Setter method for address1.</p>
     *
     * @param address1 Set for address1
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * <p>Getter method for address2.</p>
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * <p>Setter method for address2.</p>
     *
     * @param address2 Set for address2
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * <p>Getter method for address3.</p>
     *
     * @return the address3
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * <p>Setter method for address3.</p>
     *
     * @param address3 Set for address3
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * <p>Getter method for telephone.</p>
     *
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * <p>Setter method for telephone.</p>
     *
     * @param telephone Set for telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * <p>Getter method for fax.</p>
     *
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * <p>Setter method for fax.</p>
     *
     * @param fax Set for fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * <p>Getter method for contactPerson.</p>
     *
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * <p>Setter method for contactPerson.</p>
     *
     * @param contactPerson Set for contactPerson
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }

    /**
     * <p>Getter method for taxCd.</p>
     *
     * @return the taxCd
     */
    public String getTaxCd() {
        return taxCd;
    }

    /**
     * <p>Setter method for taxCd.</p>
     *
     * @param taxCd Set for taxCd
     */
    public void setTaxCd(String taxCd) {
        this.taxCd = taxCd;
    }

    /**
     * <p>Getter method for currencyCd.</p>
     *
     * @return the currencyCd
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * <p>Setter method for currencyCd.</p>
     *
     * @param currencyCd Set for currencyCd
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * <p>Getter method for isActive.</p>
     *
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * <p>Setter method for isActive.</p>
     *
     * @param isActive Set for isActive
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * <p>Getter method for decimalDisp.</p>
     *
     * @return the decimalDisp
     */
    public String getDecimalDisp() {
        return decimalDisp;
    }

    /**
     * <p>Setter method for decimalDisp.</p>
     *
     * @param decimalDisp Set for decimalDisp
     */
    public void setDecimalDisp(String decimalDisp) {
        this.decimalDisp = decimalDisp;
    }

    /**
     * <p>Getter method for taxRate.</p>
     *
     * @return the taxRate
     */
    public String getTaxRate() {
        return taxRate;
    }

    /**
     * <p>Setter method for taxRate.</p>
     *
     * @param taxRate Set for taxRate
     */
    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * <p>Getter method for createDscId.</p>
     *
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * <p>Setter method for createDscId.</p>
     *
     * @param createDscId Set for createDscId
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * <p>Getter method for createDatetime.</p>
     *
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * <p>Setter method for createDatetime.</p>
     *
     * @param createDatetime Set for createDatetime
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * <p>Getter method for lastUpdateDscId.</p>
     *
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>Setter method for lastUpdateDscId.</p>
     *
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>Getter method for lastUpdateDatetime.</p>
     *
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>Setter method for lastUpdateDatetime.</p>
     *
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
}
