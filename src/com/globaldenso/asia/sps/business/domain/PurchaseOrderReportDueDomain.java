/*
 * ModifyDate Development company     Describe 
 * 2015/02/13 CSI Akat               Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * 
 * <p>Purchase Order Report Due Domain.</p>
 *
 * @author CSI
 */
public class PurchaseOrderReportDueDomain extends BaseDomain implements Serializable {

    /** Generated serial version UID. */
    private static final long serialVersionUID = 4860330833659869636L;

    /**
     * Due Date
     */
    private Date dueDate;

    /**
     * Order Qty
     */
    private BigDecimal orderQty;

    /**
     * ETD Date
     */
    private Date etd;

    /** Period Type */
    private String periodType;
    
    /** The default constructor. */
    public PurchaseOrderReportDueDomain() {
    }

    /**
     * <p>Getter method for dueDate.</p>
     *
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * <p>Setter method for dueDate.</p>
     *
     * @param dueDate Set for dueDate
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * <p>Getter method for orderQty.</p>
     *
     * @return the orderQty
     */
    public BigDecimal getOrderQty() {
        return orderQty;
    }

    /**
     * <p>Setter method for orderQty.</p>
     *
     * @param orderQty Set for orderQty
     */
    public void setOrderQty(BigDecimal orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * <p>Getter method for etd.</p>
     *
     * @return the etd
     */
    public Date getEtd() {
        return etd;
    }

    /**
     * <p>Setter method for etd.</p>
     *
     * @param etd Set for etd
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }

    /**
     * <p>Getter method for periodType.</p>
     *
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * <p>Setter method for periodType.</p>
     *
     * @param periodType Set for periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }
    
}
