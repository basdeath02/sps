/*
 * ModifyDate Development company     Describe 
 * 2014/08/21 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * The Class Asn Detail Information Return Domain.
 * @author CSI
 */
public class AsnDetailInformationReturnDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7725186117862961273L;
    
    /** The invoice no. */
    private String invoiceNo;
    
    /** The invoice date. */
    private String invoiceDate;
    
    /** The supplier tax id. */
    private String sTaxId;
    
    /** The utc. */
    private String utc;
    
    /** The actual etd str. */
    private String actualEtdStr;
    
    /** The plan eta str. */
    private String planEtaStr;
    
    /** The no of str. */
    private String noOfBoxStr;
    
    /** The shipping qty str. */
    private String shippingQtyStr;
    
    /** The kanban seq no. */
    private String kanbanSeqNo;
    
    /** The asn domain. */
    private SpsTAsnDomain asnDomain;
    
    /** The asn detail domain. */
    private SpsTAsnDetailDomain asnDetailDomain;
    
    /** The do domain. */
    private SpsTDoDomain doDomain;
    
    /** The do detail domain. */
    private SpsTDoDetailDomain doDetailDomain;
    
    /** The current timestmap. */
    private Timestamp currentTimeStamp;
    
    /**
     * Instantiates a new Asn detail information domain.
     */
    public AsnDetailInformationReturnDomain() {
        super();
        asnDomain = new SpsTAsnDomain();
        asnDetailDomain = new SpsTAsnDetailDomain();
        doDetailDomain = new SpsTDoDetailDomain();
        doDomain = new SpsTDoDomain();
    }

    /**
     * Gets the invoice no.
     * 
     * @return the invoice no
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Sets the invoice no.
     * 
     * @param invoiceNo the invoice no
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * Gets the invoice date.
     * 
     * @return the invoice date
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the invoice date.
     * 
     * @param invoiceDate the invoice date
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * Gets the supplier tax id.
     * 
     * @return the supplier tax id
     */
    public String getsTaxId() {
        return sTaxId;
    }

    /**
     * Sets the supplier tax id.
     * 
     * @param sTaxId the supplier tax id
     */
    public void setsTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Gets the utc.
     * 
     * @return the utc.
     */
    public String getUtc() {
        return utc;
    }
    
    /**
     * Sets the utc.
     * 
     * @param utc the utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }
    
    /**
     * Gets the actual etd str.
     * 
     * @return the actual etd str
     */
    public String getActualEtdStr() {
        return actualEtdStr;
    }

    /**
     * Sets the actual etd str.
     * 
     * @param actualEtdStr the actual etd str
     */
    public void setActualEtdStr(String actualEtdStr) {
        this.actualEtdStr = actualEtdStr;
    }

    /**
     * Gets the plan eta str.
     * 
     * @return the plan eta str
     */
    public String getPlanEtaStr() {
        return planEtaStr;
    }

    /**
     * Sets the plan eta str.
     * 
     * @param planEtaStr the plan eta str
     */
    public void setPlanEtaStr(String planEtaStr) {
        this.planEtaStr = planEtaStr;
    }

    /**
     * Gets the no of box str.
     * 
     * @return the no of box str
     */
    public String getNoOfBoxStr() {
        return noOfBoxStr;
    }

    /**
     * Sets the no of box str.
     * 
     * @param noOfBoxStr the no of box str
     */
    public void setNoOfBoxStr(String noOfBoxStr) {
        this.noOfBoxStr = noOfBoxStr;
    }

    /**
     * Gets the shipping qty str.
     * 
     * @return the shipping qty str
     */
    public String getShippingQtyStr() {
        return shippingQtyStr;
    }

    /**
     * Sets the shipping qty str.
     * 
     * @param shippingQtyStr the shipping qty str
     */
    public void setShippingQtyStr(String shippingQtyStr) {
        this.shippingQtyStr = shippingQtyStr;
    }
    
    /**
     * Gets the kanban seq no.
     * 
     * @return the kanban seq no
     */
    public String getKanbanSeqNo() {
        return kanbanSeqNo;
    }

    /**
     * Sets the kanban seq no.
     * 
     * @param kanbanSeqNo the kanban seq no
     */
    public void setKanbanSeqNo(String kanbanSeqNo) {
        this.kanbanSeqNo = kanbanSeqNo;
    }

    /**
     * Gets the asn domain.
     * 
     * @return the asn domain
     */
    public SpsTAsnDomain getAsnDomain() {
        return asnDomain;
    }

    /**
     * Sets the asn domain.
     * 
     * @param asnDomain the asn domain
     */
    public void setAsnDomain(SpsTAsnDomain asnDomain) {
        this.asnDomain = asnDomain;
    }

    /**
     * Gets the asn detail domain.
     * 
     * @return the asn detail domain
     */
    public SpsTAsnDetailDomain getAsnDetailDomain() {
        return asnDetailDomain;
    }

    /**
     * Sets the asn detail domain.
     * 
     * @param asnDetailDomain the asn detail domain
     */
    public void setAsnDetailDomain(SpsTAsnDetailDomain asnDetailDomain) {
        this.asnDetailDomain = asnDetailDomain;
    }

    /**
     * Gets the do domain.
     * 
     * @return the do domain
     */
    public SpsTDoDomain getDoDomain() {
        return doDomain;
    }

    /**
     * Sets the do domain.
     * 
     * @param doDomain the do domain
     */
    public void setDoDomain(SpsTDoDomain doDomain) {
        this.doDomain = doDomain;
    }
    
    /**
     * Gets the do detail domain.
     * 
     * @return the do detail domain
     */
    public SpsTDoDetailDomain getDoDetailDomain() {
        return doDetailDomain;
    }

    /**
     * Sets the do detail domain.
     * 
     * @param doDetailDomain the do detail domain
     */
    public void setDoDetailDomain(SpsTDoDetailDomain doDetailDomain) {
        this.doDetailDomain = doDetailDomain;
    }

    /**
     * Gets the current timestamp.
     * 
     * @return the current timestamp
     */
    public Timestamp getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    /**
     * Sets the current timestamp.
     * 
     * @param currentTimeStamp the current timestamp
     */
    public void setCurrentTimeStamp(Timestamp currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }
}