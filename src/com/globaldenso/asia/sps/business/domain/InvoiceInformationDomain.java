/*
 * ModifyDate Development company     Describe 
 * 2014/07/02 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;

/**
 * The Class Invoice Information Domain.
 * @author CSI
 */
public class InvoiceInformationDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;

    /** The invoice date from. */
    private String invoiceDateFrom;
    
    /** The invoice date from. */
    private String invoiceDateTo;
    
    /** The cn date from. */
    private String cnDateFrom;
    
    /** The cn date from. */
    private String cnDateTo;
    
    /** The invoice date.*/
    private String invoiceDate;
    
    /** The invoice rcv status. */
    private String invoiceRcvStatus;
    
    /** The vendor code. */
    private String vendorCd;

    /** The denso code. */
    private String dCd;
    
    /** The supplier plant code. */
    private String dPcd;
    
    /** The payment date.*/
    private String paymentDate;
    
    /** The cn date.*/
    private String cnDate;
    
    /** The base amount.*/
    private String baseAmount;
    
    /** The vat amount.*/
    private String vatAmount;
    
    /** The total amount.*/
    private String totalAmount;
    
    /** The Generate Date.*/
    private String generateDate;
    
    /** The file Id.*/
    private String fileId;
    
    /** The decimal disp. */
    private String decimalDisp;
    
    /** The denso amount by PN. */
    private String densoAmtByPn;
    
    /** The ASN status. */
    private String asnStatus;
    
    /** The cancel flag. */
    private boolean cancelFlag;
    
    /** The cancel flag. */
    private boolean pdfFlag;
    
    /** The sps invoice domain. */
    private SpsTInvoiceDomain spsTInvoiceDomain;
    
    /** The domain for SPS_M_AS400_VENDOR (for transfer to JDE). */
    private SpsMAs400VendorDomain spsMAs400VendorDomain;
    
    /** The sps cn domain. */
    private SpsTCnDomain spsTCnDomain;
    
    /** The List of Invoice Information Detail Domain. */
    private List<InvoiceDetailDomain> invoiceDetailDomainList;
    
    /** The invoice id selected. */
    private String invoiceIdSelected;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /**
     * Instantiates a new Invoice Information Domain.
     */
    public InvoiceInformationDomain() {
        super();
        this.spsTInvoiceDomain = new SpsTInvoiceDomain();
        this.spsTCnDomain = new SpsTCnDomain();
        this.spsMAs400VendorDomain = new SpsMAs400VendorDomain();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * Getter the invoice date from.
     * 
     * @return the invoiceDateFrom
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }

    /**
     * Sets the invoice date from.
     * 
     * @param invoiceDateFrom the invoice date from.
     */
    public void setInvoiceDateFrom(String invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }
    
    /**
     * Getter the invoice date to.
     * 
     * @return the invoiceDateTo
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }

    /**
     * Sets the invoice date to.
     * 
     * @param invoiceDateTo the invoice date to.
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }
    
    /**
     * Getter the cn date from.
     * 
     * @return the cnDateFrom
     */
    public String getCnDateFrom() {
        return cnDateFrom;
    }

    /**
     * Sets the cn date from.
     * 
     * @param cnDateFrom the cn date from.
     */
    public void setCnDateFrom(String cnDateFrom) {
        this.cnDateFrom = cnDateFrom;
    }
    
    /**
     * Getter the cn date to.
     * 
     * @return the cnDateTo
     */
    public String getCnDateTo() {
        return cnDateTo;
    }

    /**
     * Sets the cn date to.
     * 
     * @param cnDateTo the cn date to.
     */
    public void setCnDateTo(String cnDateTo) {
        this.cnDateTo = cnDateTo;
    }
    
    /**
     * Getter the invoice date.
     * 
     * @return the invoiceDate
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the invoice date.
     * 
     * @param invoiceDate the invoice date.
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }
    
    /**
     * Getter the invoice rcv status.
     * 
     * @return the ivnoiceRcvStatus
     */
    public String getInvoiceRcvStatus() {
        return invoiceRcvStatus;
    }

    /**
     * Sets the invoice rcv status.
     * 
     * @param invoiceRcvStatus the invoice rcv status.
     */
    public void setInvoiceRcvStatus(String invoiceRcvStatus) {
        this.invoiceRcvStatus = invoiceRcvStatus;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for denso code.</p>
     *
     * @return the denso code
     */
    public String getDCd() {
        return dCd;
    }
    
    
    /**
     * <p>Setter method for denso code.</p>
     *
     * @param dCd Set for denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    
    /**
     * <p>Getter method for plan denso code.</p>
     *
     * @return the plan supplier code
     */
    public String getDPcd() {
        return dPcd;
    }
    
    /**
     * <p>Setter method for denso plan code.</p>
     *
     * @param dPcd Set for denso plan code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * Gets the cn date.
     * 
     * @return the cn date.
     */
    public String getCnDate() {
        return cnDate;
    }

    /**
     * Sets the cn date.
     * 
     * @param cnDate the cn date.
     */
    public void setCnDate(String cnDate) {
        this.cnDate = cnDate;
    }
    
    /**
     * Gets the payment date.
     * 
     * @return the payment date.
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the payment date.
     * 
     * @param paymentDate the payment date.
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }
    
    /**
     * Gets the base amount.
     * 
     * @return the base amount.
     */
    public String getBaseAmount() {
        return baseAmount;
    }

    /**
     * Sets the base amount
     * 
     * @param baseAmount the base amount.
     */
    public void setBaseAmount(String baseAmount) {
        this.baseAmount = baseAmount;
    }
    
    /**
     * Gets the vat amount.
     * 
     * @return the vat amount.
     */
    public String getVatAmount() {
        return vatAmount;
    }

    /**
     * Sets the vat amount
     * 
     * @param vatAmount the vat amount.
     */
    public void setVatAmount(String vatAmount) {
        this.vatAmount = vatAmount;
    }
    
    /**
     * Gets the total amount.
     * 
     * @return the total amount.
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the total amount
     * 
     * @param totalAmount the total amount.
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
    
    /**
     * Gets the generate date.
     * 
     * @return the generate date.
     */
    public String getGenerateDate() {
        return generateDate;
    }

    /**
     * Sets the generate date.
     * 
     * @param generateDate the generate date.
     */
    public void setGenerateDate(String generateDate) {
        this.generateDate = generateDate;
    }
    
    /**
     * Gets the file Id.
     * 
     * @return the file Id.
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Sets the file Id.
     * 
     * @param fileId the file Id.
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * <p>Getter method for decimalDisp.</p>
     *
     * @return the decimalDisp
     */
    public String getDecimalDisp() {
        return decimalDisp;
    }

    /**
     * <p>Setter method for decimalDisp.</p>
     *
     * @param decimalDisp Set for decimalDisp
     */
    public void setDecimalDisp(String decimalDisp) {
        this.decimalDisp = decimalDisp;
    }

    /**
     * <p>Getter method for densoAmtByPn.</p>
     *
     * @return the densoAmtByPn
     */
    public String getDensoAmtByPn() {
        return densoAmtByPn;
    }

    /**
     * <p>Setter method for densoAmtByPn.</p>
     *
     * @param densoAmtByPn Set for densoAmtByPn
     */
    public void setDensoAmtByPn(String densoAmtByPn) {
        this.densoAmtByPn = densoAmtByPn;
    }

    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * <p>Getter method for cancelFlag.</p>
     *
     * @return the cancelFlag
     */
    public boolean getCancelFlag() {
        return cancelFlag;
    }

    /**
     * <p>Setter method for cancelFlag.</p>
     *
     * @param cancelFlag Set for cancelFlag
     */
    public void setCancelFlag(boolean cancelFlag) {
        this.cancelFlag = cancelFlag;
    }

    /**
     * <p>Getter method for pdfFlag.</p>
     *
     * @return the pdfFlag
     */
    public boolean getPdfFlag() {
        return pdfFlag;
    }

    /**
     * <p>Setter method for pdfFlag.</p>
     *
     * @param pdfFlag Set for pdfFlag
     */
    public void setPdfFlag(boolean pdfFlag) {
        this.pdfFlag = pdfFlag;
    }

    /**
     * Gets the sps invoice domain.
     * 
     * @return the spsTInvoiceDomain.
     */
    public SpsTInvoiceDomain getSpsTInvoiceDomain() {
        return spsTInvoiceDomain;
    }
    
    /**
     * Sets the sps invoice domain.
     * 
     * @param spsTInvoiceDomain the sps invoice domain.
     */  
    public void setSpsTInvoiceDomain(SpsTInvoiceDomain spsTInvoiceDomain) {
        this.spsTInvoiceDomain = spsTInvoiceDomain;
    }
    
    /**
     * Gets the sps cn domain.
     * 
     * @return the spsTCnDomain.
     */
    public SpsTCnDomain getSpsTCnDomain() {
        return spsTCnDomain;
    }
    
    /**
     * Sets the sps cn domain.
     * 
     * @param spsTCnDomain the sps cn domain.
     */
    public void setSpsTCnDomain(SpsTCnDomain spsTCnDomain) {
        this.spsTCnDomain = spsTCnDomain;
    }

    /**
     * <p>Getter method for invoiceDetailDomainList.</p>
     *
     * @return the invoiceDetailDomainList
     */
    public List<InvoiceDetailDomain> getInvoiceDetailDomainList() {
        return invoiceDetailDomainList;
    }

    /**
     * <p>Setter method for invoiceDetailDomainList.</p>
     *
     * @param invoiceDetailDomainList Set for invoiceDetailDomainList
     */
    public void setInvoiceDetailDomainList(
        List<InvoiceDetailDomain> invoiceDetailDomainList) {
        this.invoiceDetailDomainList = invoiceDetailDomainList;
    }
    
    /**
     * <p>Getter method for spsMAs400VendorDomain.</p>
     *
     * @return the spsMAs400VendorDomain
     */
    public SpsMAs400VendorDomain getSpsMAs400VendorDomain() {
        return spsMAs400VendorDomain;
    }

    /**
     * <p>Setter method for spsMAs400VendorDomain.</p>
     *
     * @param spsMAs400VendorDomain Set for spsMAs400VendorDomain
     */
    public void setSpsMAs400VendorDomain(SpsMAs400VendorDomain spsMAs400VendorDomain) {
        this.spsMAs400VendorDomain = spsMAs400VendorDomain;
    }

    /**
     * <p>Getter method for invoiceIdSelected.</p>
     *
     * @return the invoiceIdSelected
     */
    public String getInvoiceIdSelected() {
        return invoiceIdSelected;
    }

    /**
     * <p>Setter method for invoiceIdSelected.</p>
     *
     * @param invoiceIdSelected Set for invoiceIdSelected
     */
    public void setInvoiceIdSelected(String invoiceIdSelected) {
        this.invoiceIdSelected = invoiceIdSelected;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
}