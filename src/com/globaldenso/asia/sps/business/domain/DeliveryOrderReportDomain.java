/*
 * ModifyDate Development company     Describe 
 * 2014/08/18 CSI Chatchai                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * <p>
 * Delivery Order Report Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class DeliveryOrderReportDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serial.
     * </p>
     */
    private static final long serialVersionUID = 3503171629256690742L;

    /**
     * <p>
     * The Truck Route.
     * </p>
     */
    private String routeNo;

    /**
     * <p>
     * The Truck Sequence.
     * </p>
     */
    private String delNo;

    /**
     * <p>
     * The Supplier Code.
     * </p>
     */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /**
     * <p>
     * The Supplier Company Name.
     * </p>
     */
    private String supplierName;

    /**
     * <p>
     * The Supplier Location.
     * </p>
     */
    private String supplierLocation;

    /**
     * <p>
     * The Supplier Plant Code.
     * </p>
     */
    private String sPcd;

    /**
     * <p>
     * The Issue Date of Delivery Order String. .
     * </p>
     */
    private String issueDateString;

    /**
     * <p>
     * The Issue Date of Delivery Order Date. .
     * </p>
     */
    private Date issueDate;

    /**
     * <p>
     * The Page Number.
     * </p>
     */
    private String page;

    /**
     * <p>
     * The Current SPS D/O No.
     * </p>
     */
    private String currentSpsDoNo;

    /**
     * <p>
     * The SPS D/O No.
     * </p>
     */
    private String originalSpsDoNo;

    /**
     * <p>
     * The Ship Date (Estimate Depart date).
     * </p>
     */
    private String etdString;

    /**
     * <p>
     * The Ship Date (Estimate Depart date).
     * </p>
     */
    private Timestamp etd;

    /**
     * <p>
     * The Ship Time (Estimate Depart time).
     * </p>
     */
    private String etdTime;

    /**
     * <p>
     * The Delivery Date (Estimate Arrival date) String.
     * </p>
     */
    private String etaString;

    /**
     * <p>
     * The Delivery Date (Estimate Arrival date) Date.
     * </p>
     */
    private Timestamp eta;

    /**
     * <p>
     * The Delivery Time (Estimate Arrival time).
     * </p>
     */
    private String etaTime;

    /**
     * <p>
     * The Denso Plant Code.
     * </p>
     */
    private String dPcd;

    /**
     * <p>
     * The Warehouse For Prime Receiving.
     * </p>
     */
    private String warehouse;

    /**
     * <p>
     * The Dock Code.
     * </p>
     */
    private String dockCode;

    /**
     * <p>
     * The Receiving Lane.
     * </p>
     */
    private String rcvLane;

    /**
     * <p>
     * The Transport Mode.
     * </p>
     */
    private String trans;

    /**
     * <p>
     * The Control Number.
     * </p>
     */
    private String ctrlNo;

    /**
     * <p>
     * The Denso Part Number.
     * </p>
     */
    private String dPn;

    /**
     * <p>
     * The Supplier Part Number.
     * </p>
     */
    private String sPn;

    /**
     * <p>
     * The Item Description.
     * </p>
     */
    private String description;

    /**
     * <p>
     * The Unit of Measure.
     * </p>
     */
    private String unitofMeasure;

    /**
     * <p>
     * The Quantity per Box.
     * </p>
     */
    private String quantityperBox;

    /**
     * <p>
     * The Quantity per Box BigDecimal.
     * </p>
     */
    private BigDecimal quantityperBoxBigDec;

    /**
     * <p>
     * The Number of Shipping Box.
     * </p>
     */
    private String noofBox;

    /**
     * <p>
     * The No of Box BigDecimal.
     * </p>
     */
    private BigDecimal noofBoxBigDec;

    /**
     * <p>
     * The Previous QTY.
     * </p>
     */
    private String previousQty;

    /**
     * <p>
     * The Previous QTY BigDecimal.
     * </p>
     */
    private BigDecimal previousQtyBigDec;

    /**
     * <p>
     * The Current Order QTY.
     * </p>
     */
    private String currentQty;

    /**
     * <p>
     * The Current Order QTY BigDecimal.
     * </p>
     */
    private BigDecimal currentQtyBigDec;

    /**
     * <p>
     * The Differrence QTY.
     * </p>
     */
    private String diffQty;

    /**
     * <p>
     * The Differrence QTY BigDecimal.
     * </p>
     */
    private BigDecimal diffQtyBigDec;

    /**
     * <p>
     * The Reason for change.
     * </p>
     */
    private String reasonforChange;

    /**
     * <p>
     * The Change CIGMA D/O No.
     * </p>
     */
    private String currentCigmaDoNo;

    /**
     * <p>
     * The Delivery Date String.
     * </p>
     */
    private String deliveryDateString;

    /**
     * <p>
     * The Delivery Date Date.
     * </p>
     */
    private Date deliveryDate;

    /**
     * <p>
     * The Delivery Time.
     * </p>
     */
    private String deliveryTime;

    /**
     * <p>
     * The Supplier Company Name.
     * </p>
     */
    private String companySupplierName;

    /**
     * <p>
     * The Denso Company Name.
     * </p>
     */
    private String companyDensoName;

    /**
     * <p>
     * The Revision.
     * </p>
     */
    private String revision;
    
    /**
     * <p>The Total Page.</p>
     */
    private String totalPage;
    
    /**
     * <p>The Text Delivery Order.</p>
     */
    private String txtDeliveryOrder;
    
    /**
     * <p>The Text Route No.</p>
     */
    private String txtRouteNo;
    
    /**
     * <p>The Text Del No.</p>
     */
    private String txtDelNo;
    
    /**
     * <p>The Text SCd.</p>
     */
    private String txtSCd;
    
    /**
     * <p>The Text Sup Name.</p>
     */
    private String txtSupName;
    
    /**
     * <p>The Text Sup Loc.</p>
     */
    private String txtSupLoc;
    
    /**
     * <p>The Text Sup Plant.</p>
     */
    private String txtSupPlant;
    
    /**
     * <p>The Text Issue Date.</p>
     */
    private String txtIssueDate;
    
    /**
     * <p>The Text Page.</p>
     */
    private String txtPage;
    
    /**
     * <p>The Text Currrent.</p>
     */
    private String txtCurrrent;
    
    /**
     * <p>The Text Original.</p>
     */
    private String txtOriginal;
    
    /**
     * <p>The Text Etd.</p>
     */
    private String txtEtd;
    
    /**
     * <p>The Text Ship Date.</p>
     */
    private String txtShipDate;
    
    /**
     * <p>The Text Time.</p>
     */
    private String txtTime;
    
    /**
     * <p>The Text Eta.</p>
     */
    private String txtEta;
    
    /**
     * <p>The Text Del Date.</p>
     */
    private String txtDelDate;
    
    /**
     * <p>The Text SPcd.</p>
     */
    private String txtSPcd;
    
    /**
     * <p>The Text Ware House.</p>
     */
    private String txtWareHouse;
    
    /**
     * <p>The Text Dock Code.</p>
     */
    private String txtDockCode;
    
    /**
     * <p>The Text Rcv Lane.</p>
     */
    private String txtRcvLane;
    
    /**
     * <p>The Text Tran.</p>
     */
    private String txtTran;
    
    /**
     * <p>The Text Ctrl No.</p>
     */
    private String txtCtrlNo;
    
    /**
     * <p>The Text dPn.</p>
     */
    private String txtdPn;
    
    /**
     * <p>The Text sPn.</p>
     */
    private String txtsPn;
    
    /**
     * <p>The Text Desc.</p>
     */
    private String txtDesc;
    
    /**
     * <p>The Text Um.</p>
     */
    private String txtUm;
    
    /**
     * <p>The Text Qty Per Box.</p>
     */
    private String txtQtyPerBox;
    
    /**
     * <p>The Text No Of Box.</p>
     */
    private String txtNoOfBox;
    
    /**
     * <p>The Text Pre Qty.</p>
     */
    private String txtPreQty;
    
    /**
     * <p>The Text Curr Qty.</p>
     */
    private String txtCurrQty;
    
    /**
     * <p>The Text Diff Qty.</p>
     */
    private String txtDiffQty;
    
    /**
     * <p>The Text Rsn.</p>
     */
    private String txtRsn;
    
    /**
     * <p>The Text Curr Cigma.</p>
     */
    private String txtCurrCigma;
    
    /**
     * <p>The Text Title.</p>
     */
    private String txtTitle;
    
    /**
     * <p>The Text As.</p>
     */
    private String txtAs;
    
    /**
     * <p>The Text Comment.</p>
     */
    private String txtComment;
    
    /**
     * <p>The Text Back Order.</p>
     */
    private String txtBackOrder;
    
    /**
     * <p>The Text Wrong Order.</p>
     */
    private String txtWrongOrder;
    
    /**
     * <p>The Text Prod.</p>
     */
    private String txtProd;
    
    /**
     * <p>The Text Cust.</p>
     */
    private String txtCust;
    
    /**
     * <p>The Text QtyPro.</p>
     */
    private String txtQtyPro;
    
    /**
     * <p>The Text Oth.</p>
     */
    private String txtOth;
    
    /**
     * <p>The Text Supplier.</p>
     */
    private String txtSupplier;
    
    /**
     * <p>The Text Sing.</p>
     */
    private String txtSing;
    
    /**
     * <p>The Denso Code.</p>
     */
    private String dCd;

    /**
     * <p>The constructor.</p>
     *
     */
    public DeliveryOrderReportDomain(){
        super();
    }
    
    /**
     * <p>
     * Getter method for routeNo.
     * </p>
     * 
     * @return the routeNo
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * <p>
     * Setter method for routeNo.
     * </p>
     * 
     * @param routeNo Set for routeNo
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * <p>
     * Getter method for delNo.
     * </p>
     * 
     * @return the delNo
     */
    public String getDelNo() {
        return delNo;
    }

    /**
     * <p>
     * Setter method for delNo.
     * </p>
     * 
     * @param delNo Set for delNo
     */
    public void setDelNo(String delNo) {
        this.delNo = delNo;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Getter method for supplierName.
     * </p>
     * 
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>
     * Setter method for supplierName.
     * </p>
     * 
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * <p>
     * Getter method for supplierLocation.
     * </p>
     * 
     * @return the supplierLocation
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * <p>
     * Setter method for supplierLocation.
     * </p>
     * 
     * @param supplierLocation Set for supplierLocation
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for issueDateString.
     * </p>
     * 
     * @return the issueDateString
     */
    public String getIssueDateString() {
        return issueDateString;
    }

    /**
     * <p>
     * Setter method for issueDateString.
     * </p>
     * 
     * @param issueDateString Set for issueDateString
     */
    public void setIssueDateString(String issueDateString) {
        this.issueDateString = issueDateString;
    }

    /**
     * <p>
     * Getter method for issueDate.
     * </p>
     * 
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * <p>
     * Setter method for issueDate.
     * </p>
     * 
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * <p>
     * Getter method for page.
     * </p>
     * 
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * <p>
     * Setter method for page.
     * </p>
     * 
     * @param page Set for page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * <p>
     * Getter method for currentSpsDoNo.
     * </p>
     * 
     * @return the currentSpsDoNo
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }

    /**
     * <p>
     * Setter method for currentSpsDoNo.
     * </p>
     * 
     * @param currentSpsDoNo Set for currentSpsDoNo
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }

    /**
     * <p>
     * Getter method for originalSpsDoNo.
     * </p>
     * 
     * @return the originalSpsDoNo
     */
    public String getOriginalSpsDoNo() {
        return originalSpsDoNo;
    }

    /**
     * <p>
     * Setter method for originalSpsDoNo.
     * </p>
     * 
     * @param originalSpsDoNo Set for originalSpsDoNo
     */
    public void setOriginalSpsDoNo(String originalSpsDoNo) {
        this.originalSpsDoNo = originalSpsDoNo;
    }

    /**
     * <p>
     * Getter method for etdString.
     * </p>
     * 
     * @return the etdString
     */
    public String getEtdString() {
        return etdString;
    }

    /**
     * <p>
     * Setter method for etdString.
     * </p>
     * 
     * @param etdString Set for etdString
     */
    public void setEtdString(String etdString) {
        this.etdString = etdString;
    }

    /**
     * <p>
     * Getter method for etd.
     * </p>
     * 
     * @return the etd
     */
    public Timestamp getEtd() {
        return etd;
    }

    /**
     * <p>
     * Setter method for etd.
     * </p>
     * 
     * @param etd Set for etd
     */
    public void setEtd(Timestamp etd) {
        this.etd = etd;
    }

    /**
     * <p>
     * Getter method for etdTime.
     * </p>
     * 
     * @return the etdTime
     */
    public String getEtdTime() {
        return etdTime;
    }

    /**
     * <p>
     * Setter method for etdTime.
     * </p>
     * 
     * @param etdTime Set for etdTime
     */
    public void setEtdTime(String etdTime) {
        this.etdTime = etdTime;
    }

    /**
     * <p>
     * Getter method for etaString.
     * </p>
     * 
     * @return the etaString
     */
    public String getEtaString() {
        return etaString;
    }

    /**
     * <p>
     * Setter method for etaString.
     * </p>
     * 
     * @param etaString Set for etaString
     */
    public void setEtaString(String etaString) {
        this.etaString = etaString;
    }

    /**
     * <p>
     * Getter method for eta.
     * </p>
     * 
     * @return the eta
     */
    public Timestamp getEta() {
        return eta;
    }

    /**
     * <p>
     * Setter method for eta.
     * </p>
     * 
     * @param eta Set for eta
     */
    public void setEta(Timestamp eta) {
        this.eta = eta;
    }

    /**
     * <p>
     * Getter method for etaTime.
     * </p>
     * 
     * @return the etaTime
     */
    public String getEtaTime() {
        return etaTime;
    }

    /**
     * <p>
     * Setter method for etaTime.
     * </p>
     * 
     * @param etaTime Set for etaTime
     */
    public void setEtaTime(String etaTime) {
        this.etaTime = etaTime;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for warehouse.
     * </p>
     * 
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * <p>
     * Setter method for warehouse.
     * </p>
     * 
     * @param warehouse Set for warehouse
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * <p>
     * Getter method for dockCode.
     * </p>
     * 
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * <p>
     * Setter method for dockCode.
     * </p>
     * 
     * @param dockCode Set for dockCode
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * <p>
     * Getter method for rcvLane.
     * </p>
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>
     * Setter method for rcvLane.
     * </p>
     * 
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>
     * Getter method for trans.
     * </p>
     * 
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * <p>
     * Setter method for trans.
     * </p>
     * 
     * @param trans Set for trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * <p>
     * Getter method for ctrlNo.
     * </p>
     * 
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * <p>
     * Setter method for ctrlNo.
     * </p>
     * 
     * @param ctrlNo Set for ctrlNo
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * <p>
     * Getter method for dPn.
     * </p>
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>
     * Setter method for dPn.
     * </p>
     * 
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>
     * Getter method for sPn.
     * </p>
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>
     * Setter method for sPn.
     * </p>
     * 
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>
     * Getter method for description.
     * </p>
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>
     * Setter method for description.
     * </p>
     * 
     * @param description Set for description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>
     * Getter method for unitofMeasure.
     * </p>
     * 
     * @return the unitofMeasure
     */
    public String getUnitofMeasure() {
        return unitofMeasure;
    }

    /**
     * <p>
     * Setter method for unitofMeasure.
     * </p>
     * 
     * @param unitofMeasure Set for unitofMeasure
     */
    public void setUnitofMeasure(String unitofMeasure) {
        this.unitofMeasure = unitofMeasure;
    }

    /**
     * <p>
     * Getter method for quantityperBox.
     * </p>
     * 
     * @return the quantityperBox
     */
    public String getQuantityperBox() {
        return quantityperBox;
    }

    /**
     * <p>
     * Setter method for quantityperBox.
     * </p>
     * 
     * @param quantityperBox Set for quantityperBox
     */
    public void setQuantityperBox(String quantityperBox) {
        this.quantityperBox = quantityperBox;
    }

    /**
     * <p>
     * Getter method for quantityperBoxBigDec.
     * </p>
     * 
     * @return the quantityperBoxBigDec
     */
    public BigDecimal getQuantityperBoxBigDec() {
        return quantityperBoxBigDec;
    }

    /**
     * <p>
     * Setter method for quantityperBoxBigDec.
     * </p>
     * 
     * @param quantityperBoxBigDec Set for quantityperBoxBigDec
     */
    public void setQuantityperBoxBigDec(BigDecimal quantityperBoxBigDec) {
        this.quantityperBoxBigDec = quantityperBoxBigDec;
    }

    /**
     * <p>
     * Getter method for noofBox.
     * </p>
     * 
     * @return the noofBox
     */
    public String getNoofBox() {
        return noofBox;
    }

    /**
     * <p>
     * Setter method for noofBox.
     * </p>
     * 
     * @param noofBox Set for noofBox
     */
    public void setNoofBox(String noofBox) {
        this.noofBox = noofBox;
    }

    /**
     * <p>
     * Getter method for noofBoxBigDec.
     * </p>
     * 
     * @return the noofBoxBigDec
     */
    public BigDecimal getNoofBoxBigDec() {
        return noofBoxBigDec;
    }

    /**
     * <p>
     * Setter method for noofBoxBigDec.
     * </p>
     * 
     * @param noofBoxBigDec Set for noofBoxBigDec
     */
    public void setNoofBoxBigDec(BigDecimal noofBoxBigDec) {
        this.noofBoxBigDec = noofBoxBigDec;
    }

    /**
     * <p>
     * Getter method for previousQty.
     * </p>
     * 
     * @return the previousQty
     */
    public String getPreviousQty() {
        return previousQty;
    }

    /**
     * <p>
     * Setter method for previousQty.
     * </p>
     * 
     * @param previousQty Set for previousQty
     */
    public void setPreviousQty(String previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * <p>
     * Getter method for previousQtyBigDec.
     * </p>
     * 
     * @return the previousQtyBigDec
     */
    public BigDecimal getPreviousQtyBigDec() {
        return previousQtyBigDec;
    }

    /**
     * <p>
     * Setter method for previousQtyBigDec.
     * </p>
     * 
     * @param previousQtyBigDec Set for previousQtyBigDec
     */
    public void setPreviousQtyBigDec(BigDecimal previousQtyBigDec) {
        this.previousQtyBigDec = previousQtyBigDec;
    }

    /**
     * <p>
     * Getter method for currentQty.
     * </p>
     * 
     * @return the currentQty
     */
    public String getCurrentQty() {
        return currentQty;
    }

    /**
     * <p>
     * Setter method for currentQty.
     * </p>
     * 
     * @param currentQty Set for currentQty
     */
    public void setCurrentQty(String currentQty) {
        this.currentQty = currentQty;
    }

    /**
     * <p>
     * Getter method for currentQtyBigDec.
     * </p>
     * 
     * @return the currentQtyBigDec
     */
    public BigDecimal getCurrentQtyBigDec() {
        return currentQtyBigDec;
    }

    /**
     * <p>
     * Setter method for currentQtyBigDec.
     * </p>
     * 
     * @param currentQtyBigDec Set for currentQtyBigDec
     */
    public void setCurrentQtyBigDec(BigDecimal currentQtyBigDec) {
        this.currentQtyBigDec = currentQtyBigDec;
    }

    /**
     * <p>
     * Getter method for diffQty.
     * </p>
     * 
     * @return the diffQty
     */
    public String getDiffQty() {
        return diffQty;
    }

    /**
     * <p>
     * Setter method for diffQty.
     * </p>
     * 
     * @param diffQty Set for diffQty
     */
    public void setDiffQty(String diffQty) {
        this.diffQty = diffQty;
    }

    /**
     * <p>
     * Getter method for diffQtyBigDec.
     * </p>
     * 
     * @return the diffQtyBigDec
     */
    public BigDecimal getDiffQtyBigDec() {
        return diffQtyBigDec;
    }

    /**
     * <p>
     * Setter method for diffQtyBigDec.
     * </p>
     * 
     * @param diffQtyBigDec Set for diffQtyBigDec
     */
    public void setDiffQtyBigDec(BigDecimal diffQtyBigDec) {
        this.diffQtyBigDec = diffQtyBigDec;
    }

    /**
     * <p>
     * Getter method for reasonforChange.
     * </p>
     * 
     * @return the reasonforChange
     */
    public String getReasonforChange() {
        return reasonforChange;
    }

    /**
     * <p>
     * Setter method for reasonforChange.
     * </p>
     * 
     * @param reasonforChange Set for reasonforChange
     */
    public void setReasonforChange(String reasonforChange) {
        this.reasonforChange = reasonforChange;
    }

    /**
     * <p>
     * Getter method for currentCigmaDoNo.
     * </p>
     * 
     * @return the currentCigmaDoNo
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * <p>
     * Setter method for currentCigmaDoNo.
     * </p>
     * 
     * @param currentCigmaDoNo Set for currentCigmaDoNo
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * <p>
     * Getter method for deliveryDateString.
     * </p>
     * 
     * @return the deliveryDateString
     */
    public String getDeliveryDateString() {
        return deliveryDateString;
    }

    /**
     * <p>
     * Setter method for deliveryDateString.
     * </p>
     * 
     * @param deliveryDateString Set for deliveryDateString
     */
    public void setDeliveryDateString(String deliveryDateString) {
        this.deliveryDateString = deliveryDateString;
    }

    /**
     * <p>
     * Getter method for deliveryDate.
     * </p>
     * 
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * <p>
     * Setter method for deliveryDate.
     * </p>
     * 
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * <p>
     * Getter method for deliveryTime.
     * </p>
     * 
     * @return the deliveryTime
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * <p>
     * Setter method for deliveryTime.
     * </p>
     * 
     * @param deliveryTime Set for deliveryTime
     */
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * <p>
     * Getter method for companySupplierName.
     * </p>
     * 
     * @return the companySupplierName
     */
    public String getCompanySupplierName() {
        return companySupplierName;
    }

    /**
     * <p>
     * Setter method for companySupplierName.
     * </p>
     * 
     * @param companySupplierName Set for companySupplierName
     */
    public void setCompanySupplierName(String companySupplierName) {
        this.companySupplierName = companySupplierName;
    }

    /**
     * <p>
     * Getter method for companyDensoName.
     * </p>
     * 
     * @return the companyDensoName
     */
    public String getCompanyDensoName() {
        return companyDensoName;
    }

    /**
     * <p>
     * Setter method for companyDensoName.
     * </p>
     * 
     * @param companyDensoName Set for companyDensoName
     */
    public void setCompanyDensoName(String companyDensoName) {
        this.companyDensoName = companyDensoName;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>Getter method for totalPage.</p>
     *
     * @return the totalPage
     */
    public String getTotalPage() {
        return totalPage;
    }

    /**
     * <p>Setter method for totalPage.</p>
     *
     * @param totalPage Set for totalPage
     */
    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * <p>Getter method for txtDeliveryOrder.</p>
     *
     * @return the txtDeliveryOrder
     */
    public String getTxtDeliveryOrder() {
        return txtDeliveryOrder;
    }

    /**
     * <p>Setter method for txtDeliveryOrder.</p>
     *
     * @param txtDeliveryOrder Set for txtDeliveryOrder
     */
    public void setTxtDeliveryOrder(String txtDeliveryOrder) {
        this.txtDeliveryOrder = txtDeliveryOrder;
    }

    /**
     * <p>Getter method for txtRouteNo.</p>
     *
     * @return the txtRouteNo
     */
    public String getTxtRouteNo() {
        return txtRouteNo;
    }

    /**
     * <p>Setter method for txtRouteNo.</p>
     *
     * @param txtRouteNo Set for txtRouteNo
     */
    public void setTxtRouteNo(String txtRouteNo) {
        this.txtRouteNo = txtRouteNo;
    }

    /**
     * <p>Getter method for txtDelNo.</p>
     *
     * @return the txtDelNo
     */
    public String getTxtDelNo() {
        return txtDelNo;
    }

    /**
     * <p>Setter method for txtDelNo.</p>
     *
     * @param txtDelNo Set for txtDelNo
     */
    public void setTxtDelNo(String txtDelNo) {
        this.txtDelNo = txtDelNo;
    }

    /**
     * <p>Getter method for txtSCd.</p>
     *
     * @return the txtSCd
     */
    public String getTxtSCd() {
        return txtSCd;
    }

    /**
     * <p>Setter method for txtSCd.</p>
     *
     * @param txtSCd Set for txtSCd
     */
    public void setTxtSCd(String txtSCd) {
        this.txtSCd = txtSCd;
    }

    /**
     * <p>Getter method for txtSupName.</p>
     *
     * @return the txtSupName
     */
    public String getTxtSupName() {
        return txtSupName;
    }

    /**
     * <p>Setter method for txtSupName.</p>
     *
     * @param txtSupName Set for txtSupName
     */
    public void setTxtSupName(String txtSupName) {
        this.txtSupName = txtSupName;
    }

    /**
     * <p>Getter method for txtSupLoc.</p>
     *
     * @return the txtSupLoc
     */
    public String getTxtSupLoc() {
        return txtSupLoc;
    }

    /**
     * <p>Setter method for txtSupLoc.</p>
     *
     * @param txtSupLoc Set for txtSupLoc
     */
    public void setTxtSupLoc(String txtSupLoc) {
        this.txtSupLoc = txtSupLoc;
    }

    /**
     * <p>Getter method for txtSupPlant.</p>
     *
     * @return the txtSupPlant
     */
    public String getTxtSupPlant() {
        return txtSupPlant;
    }

    /**
     * <p>Setter method for txtSupPlant.</p>
     *
     * @param txtSupPlant Set for txtSupPlant
     */
    public void setTxtSupPlant(String txtSupPlant) {
        this.txtSupPlant = txtSupPlant;
    }

    /**
     * <p>Getter method for txtIssueDate.</p>
     *
     * @return the txtIssueDate
     */
    public String getTxtIssueDate() {
        return txtIssueDate;
    }

    /**
     * <p>Setter method for txtIssueDate.</p>
     *
     * @param txtIssueDate Set for txtIssueDate
     */
    public void setTxtIssueDate(String txtIssueDate) {
        this.txtIssueDate = txtIssueDate;
    }

    /**
     * <p>Getter method for txtPage.</p>
     *
     * @return the txtPage
     */
    public String getTxtPage() {
        return txtPage;
    }

    /**
     * <p>Setter method for txtPage.</p>
     *
     * @param txtPage Set for txtPage
     */
    public void setTxtPage(String txtPage) {
        this.txtPage = txtPage;
    }

    /**
     * <p>Getter method for txtCurrrent.</p>
     *
     * @return the txtCurrrent
     */
    public String getTxtCurrrent() {
        return txtCurrrent;
    }

    /**
     * <p>Setter method for txtCurrrent.</p>
     *
     * @param txtCurrrent Set for txtCurrrent
     */
    public void setTxtCurrrent(String txtCurrrent) {
        this.txtCurrrent = txtCurrrent;
    }

    /**
     * <p>Getter method for txtOriginal.</p>
     *
     * @return the txtOriginal
     */
    public String getTxtOriginal() {
        return txtOriginal;
    }

    /**
     * <p>Setter method for txtOriginal.</p>
     *
     * @param txtOriginal Set for txtOriginal
     */
    public void setTxtOriginal(String txtOriginal) {
        this.txtOriginal = txtOriginal;
    }

    /**
     * <p>Getter method for txtEtd.</p>
     *
     * @return the txtEtd
     */
    public String getTxtEtd() {
        return txtEtd;
    }

    /**
     * <p>Setter method for txtEtd.</p>
     *
     * @param txtEtd Set for txtEtd
     */
    public void setTxtEtd(String txtEtd) {
        this.txtEtd = txtEtd;
    }

    /**
     * <p>Getter method for txtShipDate.</p>
     *
     * @return the txtShipDate
     */
    public String getTxtShipDate() {
        return txtShipDate;
    }

    /**
     * <p>Setter method for txtShipDate.</p>
     *
     * @param txtShipDate Set for txtShipDate
     */
    public void setTxtShipDate(String txtShipDate) {
        this.txtShipDate = txtShipDate;
    }

    /**
     * <p>Getter method for txtTime.</p>
     *
     * @return the txtTime
     */
    public String getTxtTime() {
        return txtTime;
    }

    /**
     * <p>Setter method for txtTime.</p>
     *
     * @param txtTime Set for txtTime
     */
    public void setTxtTime(String txtTime) {
        this.txtTime = txtTime;
    }

    /**
     * <p>Getter method for txtEta.</p>
     *
     * @return the txtEta
     */
    public String getTxtEta() {
        return txtEta;
    }

    /**
     * <p>Setter method for txtEta.</p>
     *
     * @param txtEta Set for txtEta
     */
    public void setTxtEta(String txtEta) {
        this.txtEta = txtEta;
    }

    /**
     * <p>Getter method for txtDelDate.</p>
     *
     * @return the txtDelDate
     */
    public String getTxtDelDate() {
        return txtDelDate;
    }

    /**
     * <p>Setter method for txtDelDate.</p>
     *
     * @param txtDelDate Set for txtDelDate
     */
    public void setTxtDelDate(String txtDelDate) {
        this.txtDelDate = txtDelDate;
    }

    /**
     * <p>Getter method for txtSPcd.</p>
     *
     * @return the txtSPcd
     */
    public String getTxtSPcd() {
        return txtSPcd;
    }

    /**
     * <p>Setter method for txtSPcd.</p>
     *
     * @param txtSPcd Set for txtSPcd
     */
    public void setTxtSPcd(String txtSPcd) {
        this.txtSPcd = txtSPcd;
    }

    /**
     * <p>Getter method for txtWareHouse.</p>
     *
     * @return the txtWareHouse
     */
    public String getTxtWareHouse() {
        return txtWareHouse;
    }

    /**
     * <p>Setter method for txtWareHouse.</p>
     *
     * @param txtWareHouse Set for txtWareHouse
     */
    public void setTxtWareHouse(String txtWareHouse) {
        this.txtWareHouse = txtWareHouse;
    }

    /**
     * <p>Getter method for txtDockCode.</p>
     *
     * @return the txtDockCode
     */
    public String getTxtDockCode() {
        return txtDockCode;
    }

    /**
     * <p>Setter method for txtDockCode.</p>
     *
     * @param txtDockCode Set for txtDockCode
     */
    public void setTxtDockCode(String txtDockCode) {
        this.txtDockCode = txtDockCode;
    }

    /**
     * <p>Getter method for txtRcvLane.</p>
     *
     * @return the txtRcvLane
     */
    public String getTxtRcvLane() {
        return txtRcvLane;
    }

    /**
     * <p>Setter method for txtRcvLane.</p>
     *
     * @param txtRcvLane Set for txtRcvLane
     */
    public void setTxtRcvLane(String txtRcvLane) {
        this.txtRcvLane = txtRcvLane;
    }

    /**
     * <p>Getter method for txtTran.</p>
     *
     * @return the txtTran
     */
    public String getTxtTran() {
        return txtTran;
    }

    /**
     * <p>Setter method for txtTran.</p>
     *
     * @param txtTran Set for txtTran
     */
    public void setTxtTran(String txtTran) {
        this.txtTran = txtTran;
    }

    /**
     * <p>Getter method for txtCtrlNo.</p>
     *
     * @return the txtCtrlNo
     */
    public String getTxtCtrlNo() {
        return txtCtrlNo;
    }

    /**
     * <p>Setter method for txtCtrlNo.</p>
     *
     * @param txtCtrlNo Set for txtCtrlNo
     */
    public void setTxtCtrlNo(String txtCtrlNo) {
        this.txtCtrlNo = txtCtrlNo;
    }

    /**
     * <p>Getter method for txtdPn.</p>
     *
     * @return the txtdPn
     */
    public String getTxtdPn() {
        return txtdPn;
    }

    /**
     * <p>Setter method for txtdPn.</p>
     *
     * @param txtdPn Set for txtdPn
     */
    public void setTxtdPn(String txtdPn) {
        this.txtdPn = txtdPn;
    }

    /**
     * <p>Getter method for txtsPn.</p>
     *
     * @return the txtsPn
     */
    public String getTxtsPn() {
        return txtsPn;
    }

    /**
     * <p>Setter method for txtsPn.</p>
     *
     * @param txtsPn Set for txtsPn
     */
    public void setTxtsPn(String txtsPn) {
        this.txtsPn = txtsPn;
    }

    /**
     * <p>Getter method for txtDesc.</p>
     *
     * @return the txtDesc
     */
    public String getTxtDesc() {
        return txtDesc;
    }

    /**
     * <p>Setter method for txtDesc.</p>
     *
     * @param txtDesc Set for txtDesc
     */
    public void setTxtDesc(String txtDesc) {
        this.txtDesc = txtDesc;
    }

    /**
     * <p>Getter method for txtUm.</p>
     *
     * @return the txtUm
     */
    public String getTxtUm() {
        return txtUm;
    }

    /**
     * <p>Setter method for txtUm.</p>
     *
     * @param txtUm Set for txtUm
     */
    public void setTxtUm(String txtUm) {
        this.txtUm = txtUm;
    }

    /**
     * <p>Getter method for txtQtyPerBox.</p>
     *
     * @return the txtQtyPerBox
     */
    public String getTxtQtyPerBox() {
        return txtQtyPerBox;
    }

    /**
     * <p>Setter method for txtQtyPerBox.</p>
     *
     * @param txtQtyPerBox Set for txtQtyPerBox
     */
    public void setTxtQtyPerBox(String txtQtyPerBox) {
        this.txtQtyPerBox = txtQtyPerBox;
    }

    /**
     * <p>Getter method for txtNoOfBox.</p>
     *
     * @return the txtNoOfBox
     */
    public String getTxtNoOfBox() {
        return txtNoOfBox;
    }

    /**
     * <p>Setter method for txtNoOfBox.</p>
     *
     * @param txtNoOfBox Set for txtNoOfBox
     */
    public void setTxtNoOfBox(String txtNoOfBox) {
        this.txtNoOfBox = txtNoOfBox;
    }

    /**
     * <p>Getter method for txtPreQty.</p>
     *
     * @return the txtPreQty
     */
    public String getTxtPreQty() {
        return txtPreQty;
    }

    /**
     * <p>Setter method for txtPreQty.</p>
     *
     * @param txtPreQty Set for txtPreQty
     */
    public void setTxtPreQty(String txtPreQty) {
        this.txtPreQty = txtPreQty;
    }

    /**
     * <p>Getter method for txtCurrQty.</p>
     *
     * @return the txtCurrQty
     */
    public String getTxtCurrQty() {
        return txtCurrQty;
    }

    /**
     * <p>Setter method for txtCurrQty.</p>
     *
     * @param txtCurrQty Set for txtCurrQty
     */
    public void setTxtCurrQty(String txtCurrQty) {
        this.txtCurrQty = txtCurrQty;
    }

    /**
     * <p>Getter method for txtDiffQty.</p>
     *
     * @return the txtDiffQty
     */
    public String getTxtDiffQty() {
        return txtDiffQty;
    }

    /**
     * <p>Setter method for txtDiffQty.</p>
     *
     * @param txtDiffQty Set for txtDiffQty
     */
    public void setTxtDiffQty(String txtDiffQty) {
        this.txtDiffQty = txtDiffQty;
    }

    /**
     * <p>Getter method for txtRsn.</p>
     *
     * @return the txtRsn
     */
    public String getTxtRsn() {
        return txtRsn;
    }

    /**
     * <p>Setter method for txtRsn.</p>
     *
     * @param txtRsn Set for txtRsn
     */
    public void setTxtRsn(String txtRsn) {
        this.txtRsn = txtRsn;
    }

    /**
     * <p>Getter method for txtCurrCigma.</p>
     *
     * @return the txtCurrCigma
     */
    public String getTxtCurrCigma() {
        return txtCurrCigma;
    }

    /**
     * <p>Setter method for txtCurrCigma.</p>
     *
     * @param txtCurrCigma Set for txtCurrCigma
     */
    public void setTxtCurrCigma(String txtCurrCigma) {
        this.txtCurrCigma = txtCurrCigma;
    }

    /**
     * <p>Getter method for txtTitle.</p>
     *
     * @return the txtTitle
     */
    public String getTxtTitle() {
        return txtTitle;
    }

    /**
     * <p>Setter method for txtTitle.</p>
     *
     * @param txtTitle Set for txtTitle
     */
    public void setTxtTitle(String txtTitle) {
        this.txtTitle = txtTitle;
    }

    /**
     * <p>Getter method for txtAs.</p>
     *
     * @return the txtAs
     */
    public String getTxtAs() {
        return txtAs;
    }

    /**
     * <p>Setter method for txtAs.</p>
     *
     * @param txtAs Set for txtAs
     */
    public void setTxtAs(String txtAs) {
        this.txtAs = txtAs;
    }

    /**
     * <p>Getter method for txtComment.</p>
     *
     * @return the txtComment
     */
    public String getTxtComment() {
        return txtComment;
    }

    /**
     * <p>Setter method for txtComment.</p>
     *
     * @param txtComment Set for txtComment
     */
    public void setTxtComment(String txtComment) {
        this.txtComment = txtComment;
    }

    /**
     * <p>Getter method for txtBackOrder.</p>
     *
     * @return the txtBackOrder
     */
    public String getTxtBackOrder() {
        return txtBackOrder;
    }

    /**
     * <p>Setter method for txtBackOrder.</p>
     *
     * @param txtBackOrder Set for txtBackOrder
     */
    public void setTxtBackOrder(String txtBackOrder) {
        this.txtBackOrder = txtBackOrder;
    }

    /**
     * <p>Getter method for txtWrongOrder.</p>
     *
     * @return the txtWrongOrder
     */
    public String getTxtWrongOrder() {
        return txtWrongOrder;
    }

    /**
     * <p>Setter method for txtWrongOrder.</p>
     *
     * @param txtWrongOrder Set for txtWrongOrder
     */
    public void setTxtWrongOrder(String txtWrongOrder) {
        this.txtWrongOrder = txtWrongOrder;
    }

    /**
     * <p>Getter method for txtProd.</p>
     *
     * @return the txtProd
     */
    public String getTxtProd() {
        return txtProd;
    }

    /**
     * <p>Setter method for txtProd.</p>
     *
     * @param txtProd Set for txtProd
     */
    public void setTxtProd(String txtProd) {
        this.txtProd = txtProd;
    }

    /**
     * <p>Getter method for txtCust.</p>
     *
     * @return the txtCust
     */
    public String getTxtCust() {
        return txtCust;
    }

    /**
     * <p>Setter method for txtCust.</p>
     *
     * @param txtCust Set for txtCust
     */
    public void setTxtCust(String txtCust) {
        this.txtCust = txtCust;
    }

    /**
     * <p>Getter method for txtQtyPro.</p>
     *
     * @return the txtQtyPro
     */
    public String getTxtQtyPro() {
        return txtQtyPro;
    }

    /**
     * <p>Setter method for txtQtyPro.</p>
     *
     * @param txtQtyPro Set for txtQtyPro
     */
    public void setTxtQtyPro(String txtQtyPro) {
        this.txtQtyPro = txtQtyPro;
    }

    /**
     * <p>Getter method for txtOth.</p>
     *
     * @return the txtOth
     */
    public String getTxtOth() {
        return txtOth;
    }

    /**
     * <p>Setter method for txtOth.</p>
     *
     * @param txtOth Set for txtOth
     */
    public void setTxtOth(String txtOth) {
        this.txtOth = txtOth;
    }

    /**
     * <p>Getter method for txtSupplier.</p>
     *
     * @return the txtSupplier
     */
    public String getTxtSupplier() {
        return txtSupplier;
    }

    /**
     * <p>Setter method for txtSupplier.</p>
     *
     * @param txtSupplier Set for txtSupplier
     */
    public void setTxtSupplier(String txtSupplier) {
        this.txtSupplier = txtSupplier;
    }

    /**
     * <p>Getter method for txtSing.</p>
     *
     * @return the txtSing
     */
    public String getTxtSing() {
        return txtSing;
    }

    /**
     * <p>Setter method for txtSing.</p>
     *
     * @param txtSing Set for txtSing
     */
    public void setTxtSing(String txtSing) {
        this.txtSing = txtSing;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

}
