/*
 * ModifyDate Development company     Describe 
 * 2015/02/13 CSI Akat               Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <p>Purchase Order Report Region Domain.</p>
 *
 * @author CSI
 */
public class PurchaseOrderReportRegionDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID. */
    private static final long serialVersionUID = -7095216024042678153L;

    /**
     * Report Type Flag
0 : D -> Daily
1 : W -> Weekly
2 : M -> Monthly
     */
    private String reportTypeFlg;

    /** List of Purchase order Due */
    private List<PurchaseOrderReportDueDomain> poDueList;
    
    /** The default constructor. */
    public PurchaseOrderReportRegionDomain() {
        this.poDueList = new ArrayList<PurchaseOrderReportDueDomain>();
    }

    /**
     * <p>Getter method for reportTypeFlg.</p>
     *
     * @return the reportTypeFlg
     */
    public String getReportTypeFlg() {
        return reportTypeFlg;
    }

    /**
     * <p>Setter method for reportTypeFlg.</p>
     *
     * @param reportTypeFlg Set for reportTypeFlg
     */
    public void setReportTypeFlg(String reportTypeFlg) {
        this.reportTypeFlg = reportTypeFlg;
    }

    /**
     * <p>Getter method for poDueList.</p>
     *
     * @return the poDueList
     */
    public List<PurchaseOrderReportDueDomain> getPoDueList() {
        return poDueList;
    }

    /**
     * <p>Setter method for poDueList.</p>
     *
     * @param poDueList Set for poDueList
     */
    public void setPoDueList(List<PurchaseOrderReportDueDomain> poDueList) {
        this.poDueList = poDueList;
    }
    
    
}
