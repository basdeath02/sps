package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * <p>TransferPoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferPoCoverPageDataFromCigmaDomain extends BaseDomain implements Serializable {
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 5512922044060908076L;
    /** poType */
    private String poType;
    /** cigmaPoNo */
    private String cigmaPoNo;
    /** releaseNo */
    private BigDecimal releaseNo;
    /** issueDate */
    private String issueDate;
    /** sellerContractName */
    private String sellerContractName;
    /** sellerName */
    private String sellerName;
    /** sellerAddress1 */
    private String sellerAddress1;
    /** sellerAddress2 */
    private String sellerAddress2;
    /** sellerAddress3 */
    private String sellerAddress3;
    /** sellerFaxNumber */
    private String sellerFaxNumber;
    /** supplierCode */
    private String vendorCd;
    /** paymentTerm */
    private String paymentTerm;
    /** shipVIA */
    private String shipVia;
    /** priceTerm */
    private String priceTerm;
    /** purchaserContractName */
    private String purchaserContractName;
    /** purchaserName */
    private String purchaserName;
    /** purchaserAddress1 */
    private String purchaserAddress1;
    /** purchaserAddress2 */
    private String purchaserAddress2;
    /** purchaserAddress3 */
    private String purchaserAddress3;
    /** P/O Ack Knowledge */
    private String poAckKnowledge;
    /** Denso Plant Code */
    private String dPcd;
    /** Cigma Po Detail List */
    private List<TransferPoCoverPageDetailDataFromCigmaDomain> cigmaPoCoverPageDetailList;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferPoCoverPageDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for poType.</p>
     *
     * @return the poType
     */
    public String getPoType() {
        return poType;
    }
    /**
     * <p>Setter method for poType.</p>
     *
     * @param poType Set for poType
     */
    public void setPotype(String poType) {
        this.poType = poType;
    }
    /**
     * <p>Getter method for cigmaPoNo.</p>
     *
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * <p>Setter method for cigmaPoNo.</p>
     *
     * @param cigmaPoNo Set for cigmaPoNo
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }
    /**
     * <p>Getter method for releaseNo.</p>
     *
     * @return the releaseNo
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }
    /**
     * <p>Setter method for releaseNo.</p>
     *
     * @param releaseNo Set for releaseNo
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }
    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }
    /**
     * <p>Getter method for sellerContractName.</p>
     *
     * @return the sellerContractName
     */
    public String getSellerContractName() {
        return sellerContractName;
    }
    /**
     * <p>Setter method for sellerContractName.</p>
     *
     * @param sellerContractName Set for sellerContractName
     */
    public void setSellerContractName(String sellerContractName) {
        this.sellerContractName = sellerContractName;
    }
    /**
     * <p>Getter method for sellerName.</p>
     *
     * @return the sellerName
     */
    public String getSellerName() {
        return sellerName;
    }
    /**
     * <p>Setter method for sellerName.</p>
     *
     * @param sellerName Set for sellerName
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
    /**
     * <p>Getter method for sellerAddress1.</p>
     *
     * @return the sellerAddress1
     */
    public String getSellerAddress1() {
        return sellerAddress1;
    }
    /**
     * <p>Setter method for sellerAddress1.</p>
     *
     * @param sellerAddress1 Set for sellerAddress1
     */
    public void setSellerAddress1(String sellerAddress1) {
        this.sellerAddress1 = sellerAddress1;
    }
    /**
     * <p>Getter method for sellerAddress2.</p>
     *
     * @return the sellerAddress2
     */
    public String getSellerAddress2() {
        return sellerAddress2;
    }
    /**
     * <p>Setter method for sellerAddress2.</p>
     *
     * @param sellerAddress2 Set for sellerAddress2
     */
    public void setSellerAddress2(String sellerAddress2) {
        this.sellerAddress2 = sellerAddress2;
    }
    /**
     * <p>Getter method for sellerAddress3.</p>
     *
     * @return the sellerAddress3
     */
    public String getSellerAddress3() {
        return sellerAddress3;
    }
    /**
     * <p>Setter method for sellerAddress3.</p>
     *
     * @param sellerAddress3 Set for sellerAddress3
     */
    public void setSellerAddress3(String sellerAddress3) {
        this.sellerAddress3 = sellerAddress3;
    }
    /**
     * <p>Getter method for sellerFaxNumber.</p>
     *
     * @return the sellerFaxNumber
     */
    public String getSellerFaxNumber() {
        return sellerFaxNumber;
    }
    /**
     * <p>Setter method for sellerFaxNumber.</p>
     *
     * @param sellerFaxNumber Set for sellerFaxNumber
     */
    public void setSellerFaxNumber(String sellerFaxNumber) {
        this.sellerFaxNumber = sellerFaxNumber;
    }
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setVendorCd(String sCd) {
        this.vendorCd = sCd;
    }
    /**
     * <p>Getter method for paymentTerm.</p>
     *
     * @return the paymentTerm
     */
    public String getPaymentTerm() {
        return paymentTerm;
    }
    /**
     * <p>Setter method for paymentTerm.</p>
     *
     * @param paymentTerm Set for paymentTerm
     */
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }
    /**
     * <p>Getter method for shipVia.</p>
     *
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }
    /**
     * <p>Setter method for shipVia.</p>
     *
     * @param shipVia Set for shipVia
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }
    /**
     * <p>Getter method for priceTerm.</p>
     *
     * @return the priceTerm
     */
    public String getPriceTerm() {
        return priceTerm;
    }
    /**
     * <p>Setter method for priceTerm.</p>
     *
     * @param priceTerm Set for priceTerm
     */
    public void setPriceTerm(String priceTerm) {
        this.priceTerm = priceTerm;
    }
    /**
     * <p>Getter method for purchaserContractName.</p>
     *
     * @return the purchaserContractName
     */
    public String getPurchaserContractName() {
        return purchaserContractName;
    }
    /**
     * <p>Setter method for purchaserContractName.</p>
     *
     * @param purchaserContractName Set for purchaserContractName
     */
    public void setPurchaserContractName(String purchaserContractName) {
        this.purchaserContractName = purchaserContractName;
    }
    /**
     * <p>Getter method for purchaserName.</p>
     *
     * @return the purchaserName
     */
    public String getPurchaserName() {
        return purchaserName;
    }
    /**
     * <p>Setter method for purchaserName.</p>
     *
     * @param purchaserName Set for purchaserName
     */
    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }
    /**
     * <p>Getter method for purchaserAddress1.</p>
     *
     * @return the purchaserAddress1
     */
    public String getPurchaserAddress1() {
        return purchaserAddress1;
    }
    /**
     * <p>Setter method for purchaserAddress1.</p>
     *
     * @param purchaserAddress1 Set for purchaserAddress1
     */
    public void setPurchaserAddress1(String purchaserAddress1) {
        this.purchaserAddress1 = purchaserAddress1;
    }
    /**
     * <p>Getter method for purchaserAddress2.</p>
     *
     * @return the purchaserAddress2
     */
    public String getPurchaserAddress2() {
        return purchaserAddress2;
    }
    /**
     * <p>Setter method for purchaserAddress2.</p>
     *
     * @param purchaserAddress2 Set for purchaserAddress2
     */
    public void setPurchaserAddress2(String purchaserAddress2) {
        this.purchaserAddress2 = purchaserAddress2;
    }
    /**
     * <p>Getter method for purchaserAddress3.</p>
     *
     * @return the purchaserAddress3
     */
    public String getPurchaserAddress3() {
        return purchaserAddress3;
    }
    /**
     * <p>Setter method for purchaserAddress3.</p>
     *
     * @param purchaserAddress3 Set for purchaserAddress3
     */
    public void setPurchaserAddress3(String purchaserAddress3) {
        this.purchaserAddress3 = purchaserAddress3;
    }
    /**
     * <p>Getter method for cigmaPoCoverPageDetailList.</p>
     *
     * @return the cigmaPoCoverPageDetailList
     */
    public List<TransferPoCoverPageDetailDataFromCigmaDomain> getCigmaPoCoverPageDetailList() {
        return cigmaPoCoverPageDetailList;
    }
    /**
     * <p>Setter method for cigmaPoCoverPageDetailList.</p>
     *
     * @param cigmaPoCoverPageDetailList Set for cigmaPoCoverPageDetailList
     */
    public void setCigmaPoCoverPageDetailList(
        List<TransferPoCoverPageDetailDataFromCigmaDomain> cigmaPoCoverPageDetailList) {
        this.cigmaPoCoverPageDetailList = cigmaPoCoverPageDetailList;
    }
    /**
     * <p>Getter method for poAckKnowledge.</p>
     *
     * @return the poAckKnowledge
     */
    public String getPoAckKnowledge() {
        return poAckKnowledge;
    }
    /**
     * <p>Setter method for poAckKnowledge.</p>
     *
     * @param poAckKnowledge Set for poAckKnowledge
     */
    public void setPoAckKnowledge(String poAckKnowledge) {
        this.poAckKnowledge = poAckKnowledge;
    }
    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
}
