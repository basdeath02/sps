/*
 * ModifyDate Development company     Describe 
 * 2014/07/24  CSI Karnrawee          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Pseudo PO Domain for receive value from Web Service
 * 
 * @author CSI Arnon
 * @version 1.0.0
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaPoInformationDomain")
public class PseudoCigmaPoInformationDomain extends PseudoBaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6725237044323325583L;
    /** The pseudo of Data Type */
    @XmlElement(name = "dataType")
    private String pseudoDataType;
    /** The pseudo of Company Name */
    @XmlElement(name = "companyName")
    private String pseudoCompanyName;
    /** The pseudo of Supplier Code */
    @XmlElement(name = "sCd")
    private String pseudoVendorCd;
    /** The pseudo of Supplier Name */
    @XmlElement(name = "supplierName")
    private String pseudoSupplierName;
    /** The pseudo of P/O Number */
    @XmlElement(name = "poNumber")
    private String pseudoPoNumber;
    /** The pseudo of Issued Date */
    @XmlElement(name = "issuedDate")
    private String pseudoIssuedDate;
    /** The pseudo of Part Number */
    @XmlElement(name = "dPn")
    private String pseudoPartNumber;
    /** The pseudo of Item Description */
    @XmlElement(name = "itemDescription")
    private String pseudoItemDescription;
    /** The pseudo of Due Date From */
    @XmlElement(name = "dueDateFrom")
    private String pseudoDueDateFrom;
    /** The pseudo of Due Date To */
    @XmlElement(name = "dueDateTo")
    private String pseudoDueDateTo;
    /** The pseudo of Release No. */
    @XmlElement(name = "releaseNo")
    private String pseudoReleaseNo;
    /** The pseudo of UNIT PRICE */
    @XmlElement(name = "unitPrice")
    private String pseudoUnitPrice;
    /** The pseudo of CURRENCY CODE */
    @XmlElement(name = "currencyCode")
    private String pseudoCurrencyCode;
    /** The pseudo of RECEIVING DOCK */
    @XmlElement(name = "receivingDock")
    private String pseudoReceivingDock;
    /** The pseudo of ORDER LOT */
    @XmlElement(name = "orderLot")
    private String pseudoOrderLot;
    /** The pseudo of QTY/BOX */
    @XmlElement(name = "qtyBox")
    private String pseudoQtyBox;
    /** The pseudo of UNIT OF MEASURE */
    @XmlElement(name = "unitOfMeasure")
    private String pseudoUnitOfMeasure;
    /** The pseudo of VARIABLE QTY CODE */
    @XmlElement(name = "variableQtyCode")
    private String pseudoVariableQtyCode;
    /** The pseudo of PART LABEL PRINT REMARKS */
    @XmlElement(name = "partLabelPrintRemarks")
    private String pseudoPartLabelPrintRemarks;
    /** The pseudo of Plant Code */
    @XmlElement(name = "dPcd")
    private String pseudoDPcd;
    /** The pseudo of Transportation Code */
    @XmlElement(name = "transportationCode")
    private String pseudoTransportationCode;
    /** The pseudo of PHASE IN/OUT CODE */
    @XmlElement(name = "phaseInOutCode")
    private String pseudoPhaseInOutCode;
    /** The pseudo of ETD  */
    @XmlElement(name = "etd")
    private String pseudoEtd;
    /** The pseudo of Due Date */
    @XmlElement(name = "dueDate")
    private String pseudoDueDate;
    /** The pseudo of Order Qty */
    @XmlElement(name = "orderQty")
    private String pseudoOrderQty;
    /** The pseudo of Planner Code */
    @XmlElement(name = "plannerCode")
    private String pseudoPlannerCode;
    /** The pseudo of Order Type */
    @XmlElement(name = "orderType")
    private String pseudoOrderType;
    /** The pseudo of Report Type */
    @XmlElement(name = "reportType")
    private String pseudoReportType;
    /** The pseudo of Force Acknowledge Date */
    @XmlElement(name = "forceAcknowledgeDate")
    private String pseudoForceAcknowledgeDate;
    /** The pseudo of TM */
    @XmlElement(name = "tm")
    private String pseudoTm;
    /** The pseudo of Add Truck Route Flag */
    @XmlElement(name = "addTruckRouteFlag")
    private String pseudoAddTruckRouteFlag;
    /** The pseudo of Blanket P/O Number */
    @XmlElement(name = "blanketPoNumber")
    private String pseudoBlanketPoNumber;
    /** The pseudo of SPS Flag */
    @XmlElement(name = "spsFlag")
    private String pseudoSpsFlag;
    /** The pseudo of Create by USER ID */
    @XmlElement(name = "createByUserId")
    private String pseudoCreateByUserId;
    /** The pseudo of Create Date */
    @XmlElement(name = "createDate")
    private String pseudoCreateDate;
    /** The pseudo of Create Time */
    @XmlElement(name = "createTime")
    private String pseudoCreateTime;
    /** The pseudo of Update by USER ID */
    @XmlElement(name = "updateByUserId")
    private String pseudoUpdateByUserId;
    /** The pseudo of Update Date */
    @XmlElement(name = "updateDate")
    private String pseudoUpdateDate;
    /** The pseudo of Update Time */
    @XmlElement(name = "updateTime")
    private String pseudoUpdateTime;
    /** The pseudo of Temp Price Flag */
    @XmlElement(name = "tmpPriceFlg")
    private String pseudoTmpPriceFlg;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public PseudoCigmaPoInformationDomain() {
    }
    /**
     * <p>Getter method for pseudoDataType.</p>
     *
     * @return the pseudoDataType
     */
    public String getPseudoDataType() {
        return pseudoDataType;
    }
    /**
     * <p>Setter method for pseudoDataType.</p>
     *
     * @param pseudoDataType Set for pseudoDataType
     */
    public void setPseudoDataType(String pseudoDataType) {
        this.pseudoDataType = pseudoDataType;
    }
    /**
     * <p>Getter method for pseudoCompanyName.</p>
     *
     * @return the pseudoCompanyName
     */
    public String getPseudoCompanyName() {
        return pseudoCompanyName;
    }
    /**
     * <p>Setter method for pseudoCompanyName.</p>
     *
     * @param pseudoCompanyName Set for pseudoCompanyName
     */
    public void setPseudoCompanyName(String pseudoCompanyName) {
        this.pseudoCompanyName = pseudoCompanyName;
    }
    /**
     * <p>Getter method for pseudoSupplierCode.</p>
     *
     * @return the pseudoSupplierCode
     */
    public String getPseudoVendorCd() {
        return pseudoVendorCd;
    }
    /**
     * <p>Setter method for pseudoSupplierCode.</p>
     *
     * @param vendorCd Set for pseudoSCd
     */
    public void setPseudoVendorCd(String vendorCd) {
        this.pseudoVendorCd = vendorCd;
    }
    /**
     * <p>Getter method for pseudoSupplierName.</p>
     *
     * @return the pseudoSupplierName
     */
    public String getPseudoSupplierName() {
        return pseudoSupplierName;
    }
    /**
     * <p>Setter method for pseudoSupplierName.</p>
     *
     * @param pseudoSupplierName Set for pseudoSupplierName
     */
    public void setPseudoSupplierName(String pseudoSupplierName) {
        this.pseudoSupplierName = pseudoSupplierName;
    }
    /**
     * <p>Getter method for pseudoPONumber.</p>
     *
     * @return the pseudoPONumber
     */
    public String getPseudoPoNumber() {
        return pseudoPoNumber;
    }
    /**
     * <p>Setter method for pseudoPONumber.</p>
     *
     * @param pseudoPoNumber Set for pseudoPoNumber
     */
    public void setPseudoPoNumber(String pseudoPoNumber) {
        this.pseudoPoNumber = pseudoPoNumber;
    }
    /**
     * <p>Getter method for pseudoIssuedDate.</p>
     *
     * @return the pseudoIssuedDate
     */
    public String getPseudoIssuedDate() {
        return pseudoIssuedDate;
    }
    /**
     * <p>Setter method for pseudoIssuedDate.</p>
     *
     * @param pseudoIssuedDate Set for pseudoIssuedDate
     */
    public void setPseudoIssuedDate(String pseudoIssuedDate) {
        this.pseudoIssuedDate = pseudoIssuedDate;
    }
    /**
     * <p>Getter method for pseudoPartNumber.</p>
     *
     * @return the pseudoPartNumber
     */
    public String getPseudoPartNumber() {
        return pseudoPartNumber;
    }
    /**
     * <p>Setter method for pseudoPartNumber.</p>
     *
     * @param pseudoPartNumber Set for pseudoPartNumber
     */
    public void setPseudoPartNumber(String pseudoPartNumber) {
        this.pseudoPartNumber = pseudoPartNumber;
    }
    /**
     * <p>Getter method for pseudoItemDescription.</p>
     *
     * @return the pseudoItemDescription
     */
    public String getPseudoItemDescription() {
        return pseudoItemDescription;
    }
    /**
     * <p>Setter method for pseudoItemDescription.</p>
     *
     * @param pseudoItemDescription Set for pseudoItemDescription
     */
    public void setPseudoItemDescription(String pseudoItemDescription) {
        this.pseudoItemDescription = pseudoItemDescription;
    }
    /**
     * <p>Getter method for pseudoDueDateFrom.</p>
     *
     * @return the pseudoDueDateFrom
     */
    public String getPseudoDueDateFrom() {
        return pseudoDueDateFrom;
    }
    /**
     * <p>Setter method for pseudoDueDateFrom.</p>
     *
     * @param pseudoDueDateFrom Set for pseudoDueDateFrom
     */
    public void setPseudoDueDateFrom(String pseudoDueDateFrom) {
        this.pseudoDueDateFrom = pseudoDueDateFrom;
    }
    /**
     * <p>Getter method for pseudoDueDateTo.</p>
     *
     * @return the pseudoDueDateTo
     */
    public String getPseudoDueDateTo() {
        return pseudoDueDateTo;
    }
    /**
     * <p>Setter method for pseudoDueDateTo.</p>
     *
     * @param pseudoDueDateTo Set for pseudoDueDateTo
     */
    public void setPseudoDueDateTo(String pseudoDueDateTo) {
        this.pseudoDueDateTo = pseudoDueDateTo;
    }
    /**
     * <p>Getter method for pseudoReleaseNo.</p>
     *
     * @return the pseudoReleaseNo
     */
    public String getPseudoReleaseNo() {
        return pseudoReleaseNo;
    }
    /**
     * <p>Setter method for pseudoReleaseNo.</p>
     *
     * @param pseudoReleaseNo Set for pseudoReleaseNo
     */
    public void setPseudoReleaseNo(String pseudoReleaseNo) {
        this.pseudoReleaseNo = pseudoReleaseNo;
    }
    /**
     * <p>Getter method for pseudoUnitPrice.</p>
     *
     * @return the pseudoUnitPrice
     */
    public String getPseudoUnitPrice() {
        return pseudoUnitPrice;
    }
    /**
     * <p>Setter method for pseudoUnitPrice.</p>
     *
     * @param pseudoUnitPrice Set for pseudoUnitPrice
     */
    public void setPseudoUnitPrice(String pseudoUnitPrice) {
        this.pseudoUnitPrice = pseudoUnitPrice;
    }
    /**
     * <p>Getter method for pseudoCurrencyCode.</p>
     *
     * @return the pseudoCurrencyCode
     */
    public String getPseudoCurrencyCode() {
        return pseudoCurrencyCode;
    }
    /**
     * <p>Setter method for pseudoCurrencyCode.</p>
     *
     * @param pseudoCurrencyCode Set for pseudoCurrencyCode
     */
    public void setPseudoCurrencyCode(String pseudoCurrencyCode) {
        this.pseudoCurrencyCode = pseudoCurrencyCode;
    }
    /**
     * <p>Getter method for pseudoReceivingDock.</p>
     *
     * @return the pseudoReceivingDock
     */
    public String getPseudoReceivingDock() {
        return pseudoReceivingDock;
    }
    /**
     * <p>Setter method for pseudoReceivingDock.</p>
     *
     * @param pseudoReceivingDock Set for pseudoReceivingDock
     */
    public void setPseudoReceivingDock(String pseudoReceivingDock) {
        this.pseudoReceivingDock = pseudoReceivingDock;
    }
    /**
     * <p>Getter method for pseudoOrderLot.</p>
     *
     * @return the pseudoOrderLot
     */
    public String getPseudoOrderLot() {
        return pseudoOrderLot;
    }
    /**
     * <p>Setter method for pseudoOrderLot.</p>
     *
     * @param pseudoOrderLot Set for pseudoOrderLot
     */
    public void setPseudoOrderLot(String pseudoOrderLot) {
        this.pseudoOrderLot = pseudoOrderLot;
    }
    /**
     * <p>Getter method for pseudoQtyBox.</p>
     *
     * @return the pseudoQtyBox
     */
    public String getPseudoQtyBox() {
        return pseudoQtyBox;
    }
    /**
     * <p>Setter method for pseudoQtyBox.</p>
     *
     * @param pseudoQtyBox Set for pseudoQtyBox
     */
    public void setPseudoQtyBox(String pseudoQtyBox) {
        this.pseudoQtyBox = pseudoQtyBox;
    }
    /**
     * <p>Getter method for pseudoUnitOfMeasure.</p>
     *
     * @return the pseudoUnitOfMeasure
     */
    public String getPseudoUnitOfMeasure() {
        return pseudoUnitOfMeasure;
    }
    /**
     * <p>Setter method for pseudoUnitOfMeasure.</p>
     *
     * @param pseudoUnitOfMeasure Set for pseudoUnitOfMeasure
     */
    public void setPseudoUnitOfMeasure(String pseudoUnitOfMeasure) {
        this.pseudoUnitOfMeasure = pseudoUnitOfMeasure;
    }
    /**
     * <p>Getter method for pseudoVariableQtyCode.</p>
     *
     * @return the pseudoVariableQtyCode
     */
    public String getPseudoVariableQtyCode() {
        return pseudoVariableQtyCode;
    }
    /**
     * <p>Setter method for pseudoVariableQtyCode.</p>
     *
     * @param pseudoVariableQtyCode Set for pseudoVariableQtyCode
     */
    public void setPseudoVariableQtyCode(String pseudoVariableQtyCode) {
        this.pseudoVariableQtyCode = pseudoVariableQtyCode;
    }
    /**
     * <p>Getter method for pseudoPartLabelPrintRemarks.</p>
     *
     * @return the pseudoPartLabelPrintRemarks
     */
    public String getPseudoPartLabelPrintRemarks() {
        return pseudoPartLabelPrintRemarks;
    }
    /**
     * <p>Setter method for pseudoPartLabelPrintRemarks.</p>
     *
     * @param pseudoPartLabelPrintRemarks Set for pseudoPartLabelPrintRemarks
     */
    public void setPseudoPartLabelPrintRemarks(String pseudoPartLabelPrintRemarks) {
        this.pseudoPartLabelPrintRemarks = pseudoPartLabelPrintRemarks;
    }
    /**
     * <p>Getter method for pseudoPlantCode.</p>
     *
     * @return the pseudoPlantCode
     */
    public String getPseudoDPcd() {
        return pseudoDPcd;
    }
    /**
     * <p>Setter method for pseudoPlantCode.</p>
     *
     * @param pseudoDPcd Set for pseudoDPcd
     */
    public void setPseudoDPcd(String pseudoDPcd) {
        this.pseudoDPcd = pseudoDPcd;
    }
    /**
     * <p>Getter method for pseudoTransportationCode.</p>
     *
     * @return the pseudoTransportationCode
     */
    public String getPseudoTransportationCode() {
        return pseudoTransportationCode;
    }
    /**
     * <p>Setter method for pseudoTransportationCode.</p>
     *
     * @param pseudoTransportationCode Set for pseudoTransportationCode
     */
    public void setPseudoTransportationCode(String pseudoTransportationCode) {
        this.pseudoTransportationCode = pseudoTransportationCode;
    }
    /**
     * <p>Getter method for pseudoPhaseInOutCode.</p>
     *
     * @return the pseudoPhaseInOutCode
     */
    public String getPseudoPhaseInOutCode() {
        return pseudoPhaseInOutCode;
    }
    /**
     * <p>Setter method for pseudoPhaseInOutCode.</p>
     *
     * @param pseudoPhaseInOutCode Set for pseudoPhaseInOutCode
     */
    public void setPseudoPhaseInOutCode(String pseudoPhaseInOutCode) {
        this.pseudoPhaseInOutCode = pseudoPhaseInOutCode;
    }
    /**
     * <p>Getter method for pseudoEtd.</p>
     *
     * @return the pseudoEtd
     */
    public String getPseudoEtd() {
        return pseudoEtd;
    }
    /**
     * <p>Setter method for pseudoEtd.</p>
     *
     * @param pseudoEtd Set for pseudoEtd
     */
    public void setPseudoEtd(String pseudoEtd) {
        this.pseudoEtd = pseudoEtd;
    }
    /**
     * <p>Getter method for pseudoDueDate.</p>
     *
     * @return the pseudoDueDate
     */
    public String getPseudoDueDate() {
        return pseudoDueDate;
    }
    /**
     * <p>Setter method for pseudoDueDate.</p>
     *
     * @param pseudoDueDate Set for pseudoDueDate
     */
    public void setPseudoDueDate(String pseudoDueDate) {
        this.pseudoDueDate = pseudoDueDate;
    }
    /**
     * <p>Getter method for pseudoOrderQty.</p>
     *
     * @return the pseudoOrderQty
     */
    public String getPseudoOrderQty() {
        return pseudoOrderQty;
    }
    /**
     * <p>Setter method for pseudoOrderQty.</p>
     *
     * @param pseudoOrderQty Set for pseudoOrderQty
     */
    public void setPseudoOrderQty(String pseudoOrderQty) {
        this.pseudoOrderQty = pseudoOrderQty;
    }
    /**
     * <p>Getter method for pseudoPlannerCode.</p>
     *
     * @return the pseudoPlannerCode
     */
    public String getPseudoPlannerCode() {
        return pseudoPlannerCode;
    }
    /**
     * <p>Setter method for pseudoPlannerCode.</p>
     *
     * @param pseudoPlannerCode Set for pseudoPlannerCode
     */
    public void setPseudoPlannerCode(String pseudoPlannerCode) {
        this.pseudoPlannerCode = pseudoPlannerCode;
    }
    /**
     * <p>Getter method for pseudoOrderType.</p>
     *
     * @return the pseudoOrderType
     */
    public String getPseudoOrderType() {
        return pseudoOrderType;
    }
    /**
     * <p>Setter method for pseudoOrderType.</p>
     *
     * @param pseudoOrderType Set for pseudoOrderType
     */
    public void setPseudoOrderType(String pseudoOrderType) {
        this.pseudoOrderType = pseudoOrderType;
    }
    /**
     * <p>Getter method for pseudoReportType.</p>
     *
     * @return the pseudoReportType
     */
    public String getPseudoReportType() {
        return pseudoReportType;
    }
    /**
     * <p>Setter method for pseudoReportType.</p>
     *
     * @param pseudoReportType Set for pseudoReportType
     */
    public void setPseudoReportType(String pseudoReportType) {
        this.pseudoReportType = pseudoReportType;
    }
    /**
     * <p>Getter method for pseudoForceAcknowledgeDate.</p>
     *
     * @return the pseudoForceAcknowledgeDate
     */
    public String getPseudoForceAcknowledgeDate() {
        return pseudoForceAcknowledgeDate;
    }
    /**
     * <p>Setter method for pseudoForceAcknowledgeDate.</p>
     *
     * @param pseudoForceAcknowledgeDate Set for pseudoForceAcknowledgeDate
     */
    public void setPseudoForceAcknowledgeDate(String pseudoForceAcknowledgeDate) {
        this.pseudoForceAcknowledgeDate = pseudoForceAcknowledgeDate;
    }
    /**
     * <p>Getter method for pseudoTm.</p>
     *
     * @return the pseudoTm
     */
    public String getPseudoTm() {
        return pseudoTm;
    }
    /**
     * <p>Setter method for pseudoTm.</p>
     *
     * @param pseudoTm Set for pseudoTm
     */
    public void setPseudoTm(String pseudoTm) {
        this.pseudoTm = pseudoTm;
    }
    /**
     * <p>Getter method for pseudoAddTruckRouteFlag.</p>
     *
     * @return the pseudoAddTruckRouteFlag
     */
    public String getPseudoAddTruckRouteFlag() {
        return pseudoAddTruckRouteFlag;
    }
    /**
     * <p>Setter method for pseudoAddTruckRouteFlag.</p>
     *
     * @param pseudoAddTruckRouteFlag Set for pseudoAddTruckRouteFlag
     */
    public void setPseudoAddTruckRouteFlag(String pseudoAddTruckRouteFlag) {
        this.pseudoAddTruckRouteFlag = pseudoAddTruckRouteFlag;
    }
    /**
     * <p>Getter method for pseudoBlanketPONumber.</p>
     *
     * @return the pseudoBlanketPoNumber
     */
    public String getPseudoBlanketPoNumber() {
        return pseudoBlanketPoNumber;
    }
    /**
     * <p>Setter method for pseudoBlanketPONumber.</p>
     *
     * @param pseudoBlanketPoNumber Set for pseudoBlanketPoNumber
     */
    public void setPseudoBlanketPoNumber(String pseudoBlanketPoNumber) {
        this.pseudoBlanketPoNumber = pseudoBlanketPoNumber;
    }
    /**
     * <p>Getter method for pseudoSpsFlag.</p>
     *
     * @return the pseudoSpsFlag
     */
    public String getPseudoSpsFlag() {
        return pseudoSpsFlag;
    }
    /**
     * <p>Setter method for pseudoSpsFlag.</p>
     *
     * @param pseudoSpsFlag Set for pseudoSpsFlag
     */
    public void setPseudoSpsFlag(String pseudoSpsFlag) {
        this.pseudoSpsFlag = pseudoSpsFlag;
    }
    /**
     * <p>Getter method for pseudoCreateByUserId.</p>
     *
     * @return the pseudoCreateByUserId
     */
    public String getPseudoCreateByUserId() {
        return pseudoCreateByUserId;
    }
    /**
     * <p>Setter method for pseudoCreateByUserId.</p>
     *
     * @param pseudoCreateByUserId Set for pseudoCreateByUserId
     */
    public void setPseudoCreateByUserId(String pseudoCreateByUserId) {
        this.pseudoCreateByUserId = pseudoCreateByUserId;
    }
    /**
     * <p>Getter method for pseudoCreateDate.</p>
     *
     * @return the pseudoCreateDate
     */
    public String getPseudoCreateDate() {
        return pseudoCreateDate;
    }
    /**
     * <p>Setter method for pseudoCreateDate.</p>
     *
     * @param pseudoCreateDate Set for pseudoCreateDate
     */
    public void setPseudoCreateDate(String pseudoCreateDate) {
        this.pseudoCreateDate = pseudoCreateDate;
    }
    /**
     * <p>Getter method for pseudoCreateTime.</p>
     *
     * @return the pseudoCreateTime
     */
    public String getPseudoCreateTime() {
        return pseudoCreateTime;
    }
    /**
     * <p>Setter method for pseudoCreateTime.</p>
     *
     * @param pseudoCreateTime Set for pseudoCreateTime
     */
    public void setPseudoCreateTime(String pseudoCreateTime) {
        this.pseudoCreateTime = pseudoCreateTime;
    }
    /**
     * <p>Getter method for pseudoUpdateByUserId.</p>
     *
     * @return the pseudoUpdateByUserId
     */
    public String getPseudoUpdateByUserId() {
        return pseudoUpdateByUserId;
    }
    /**
     * <p>Setter method for pseudoUpdateByUserId.</p>
     *
     * @param pseudoUpdateByUserId Set for pseudoUpdateByUserId
     */
    public void setPseudoUpdateByUserId(String pseudoUpdateByUserId) {
        this.pseudoUpdateByUserId = pseudoUpdateByUserId;
    }
    /**
     * <p>Getter method for pseudoUpdateDate.</p>
     *
     * @return the pseudoUpdateDate
     */
    public String getPseudoUpdateDate() {
        return pseudoUpdateDate;
    }
    /**
     * <p>Setter method for pseudoUpdateDate.</p>
     *
     * @param pseudoUpdateDate Set for pseudoUpdateDate
     */
    public void setPseudoUpdateDate(String pseudoUpdateDate) {
        this.pseudoUpdateDate = pseudoUpdateDate;
    }
    /**
     * <p>Getter method for pseudoUpdateTime.</p>
     *
     * @return the pseudoUpdateTime
     */
    public String getPseudoUpdateTime() {
        return pseudoUpdateTime;
    }
    /**
     * <p>Setter method for pseudoUpdateTime.</p>
     *
     * @param pseudoUpdateTime Set for pseudoUpdateTime
     */
    public void setPseudoUpdateTime(String pseudoUpdateTime) {
        this.pseudoUpdateTime = pseudoUpdateTime;
    }
    /**
     * <p>Getter method for pseudoTmpPriceFlg.</p>
     *
     * @return the pseudoTmpPriceFlg
     */
    public String getPseudoTmpPriceFlg() {
        return pseudoTmpPriceFlg;
    }
    /**
     * <p>Setter method for pseudoTmpPriceFlg.</p>
     *
     * @param pseudoTmpPriceFlg Set for pseudoTmpPriceFlg
     */
    public void setPseudoTmpPriceFlg(String pseudoTmpPriceFlg) {
        this.pseudoTmpPriceFlg = pseudoTmpPriceFlg;
    }
}
