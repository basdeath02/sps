/*
 * ModifyDate Development company     Describe 
 * 2014/08/14 CSI Akat                Create
 * 2016/02/23 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class SupplierPromisedDueSubmittionDomain for submit propose due date and quantity.
 * @author CSI
 */
public class SupplierPromisedDueSubmittionDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 8237907756745102908L;

    /** The PO ID. */
    private String poId;
    
    /** The Supplier Part No. */
    private String sPn;
    
    /** The DENSO Part No. */
    private String dPn;
    
    /** The file ID for download from FileManagementStream. */
    private String fileId;
    
    /** List of Purchase Order Due Domain. */
    private List<PurchaseOrderDueDomain> purchaseOrderDueList;
    
    /** List of Misc Domain (pending reason code). */
    private List<MiscellaneousDomain> pendingReasonCodeList;
    
    // [IN054] Add list of Misc Type for new combo box
    /** List of Misc Domain (Accept Reject Code). */
    private List<MiscellaneousDomain> acceptRejectCodeList;

    // [IN054] Check Action Mode when get Data
    /** The Action Mode. */
    private String actionMode;

    /** The default constructor. */
    public SupplierPromisedDueSubmittionDomain() {
        this.purchaseOrderDueList = new ArrayList<PurchaseOrderDueDomain>();
    }

    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public String getPoId() {
        return poId;
    }

    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }

    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    
    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for purchaseOrderDueList.</p>
     *
     * @return the purchaseOrderDueList
     */
    public List<PurchaseOrderDueDomain> getPurchaseOrderDueList() {
        return purchaseOrderDueList;
    }

    /**
     * <p>Setter method for purchaseOrderDueList.</p>
     *
     * @param purchaseOrderDueList Set for purchaseOrderDueList
     */
    public void setPurchaseOrderDueList(
        List<PurchaseOrderDueDomain> purchaseOrderDueList) {
        this.purchaseOrderDueList = purchaseOrderDueList;
    }

    /**
     * <p>Getter method for pendingReasonCodeList.</p>
     *
     * @return the pendingReasonCodeList
     */
    public List<MiscellaneousDomain> getPendingReasonCodeList() {
        return pendingReasonCodeList;
    }

    /**
     * <p>Setter method for pendingReasonCodeList.</p>
     *
     * @param pendingReasonCodeList Set for pendingReasonCodeList
     */
    public void setPendingReasonCodeList(List<MiscellaneousDomain> pendingReasonCodeList) {
        this.pendingReasonCodeList = pendingReasonCodeList;
    }

    /**
     * <p>Getter method for fileId.</p>
     *
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Setter method for fileId.</p>
     *
     * @param fileId Set for fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    // Start : [IN054]
    /**
     * <p>Getter method for acceptRejectCodeList.</p>
     *
     * @return the acceptRejectCodeList
     */
    public List<MiscellaneousDomain> getAcceptRejectCodeList() {
        return acceptRejectCodeList;
    }

    /**
     * <p>Setter method for acceptRejectCodeList.</p>
     *
     * @param acceptRejectCodeList Set for acceptRejectCodeList
     */
    public void setAcceptRejectCodeList(
        List<MiscellaneousDomain> acceptRejectCodeList) {
        this.acceptRejectCodeList = acceptRejectCodeList;
    }

    /**
     * <p>Getter method for actionMode.</p>
     *
     * @return the actionMode
     */
    public String getActionMode() {
        return actionMode;
    }

    /**
     * <p>Setter method for actionMode.</p>
     *
     * @param actionMode Set for actionMode
     */
    public void setActionMode(String actionMode) {
        this.actionMode = actionMode;
    }
    // End : [IN054]
}
