/*
 * ModifyDate Development company     Describe 
 * 2014/07/29 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * Delivery Order Information Result Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class DeliveryOrderInformationResultDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serializable.
     * </p>
     */
    private static final long serialVersionUID = -1237605472741628357L;

    /**
     * <p>
     * The Delivery Order Information Domain.
     * </p>
     */
    private DeliveryOrderInformationDomain deliveryOrderInformationDomain;

    /**
     * <p>
     * The Delivery Order Detail Domain.
     * </p>
     */
    private List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain;

    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;

    /** The Stream of CSV file. */
    private StringBuffer resultString;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public DeliveryOrderInformationResultDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for deliveryOrderInformationDomain.
     * </p>
     * 
     * @return the deliveryOrderInformationDomain
     */
    public DeliveryOrderInformationDomain getDeliveryOrderInformationDomain() {
        return deliveryOrderInformationDomain;
    }

    /**
     * <p>
     * Setter method for deliveryOrderInformationDomain.
     * </p>
     * 
     * @param deliveryOrderInformationDomain Set for
     *            deliveryOrderInformationDomain
     */
    public void setDeliveryOrderInformationDomain(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        this.deliveryOrderInformationDomain = deliveryOrderInformationDomain;
    }

    /**
     * <p>
     * Getter method for deliveryOrderDetailDomain.
     * </p>
     * 
     * @return the deliveryOrderDetailDomain
     */
    public List<DeliveryOrderDetailDomain> getDeliveryOrderDetailDomain() {
        return deliveryOrderDetailDomain;
    }

    /**
     * <p>
     * Setter method for deliveryOrderDetailDomain.
     * </p>
     * 
     * @param deliveryOrderDetailDomain Set for deliveryOrderDetailDomain
     */
    public void setDeliveryOrderDetailDomain(
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain) {
        this.deliveryOrderDetailDomain = deliveryOrderDetailDomain;
    }

    /**
     * <p>
     * Getter method for errorMessageList.
     * </p>
     * 
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * <p>
     * Setter method for errorMessageList.
     * </p>
     * 
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(
        List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * <p>
     * Getter method for resultString.
     * </p>
     * 
     * @return the resultString
     */
    public StringBuffer getResultString() {
        return resultString;
    }

    /**
     * <p>
     * Setter method for resultString.
     * </p>
     * 
     * @param resultString Set for resultString
     */
    public void setResultString(StringBuffer resultString) {
        this.resultString = resultString;
    }

}
