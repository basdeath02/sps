/*
 * ModifyDate Development company    Describe 
 * 2014/08/04 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;

/**
 * The Class Do Detail Info Domain.
 * @author CSI
 */
public class DoInformationDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5140011111456288855L;
    
    /** The do detail domain. */
    private SpsTDoDetailDomain doDetailDomain;
    
    /** The asn detail domain. */
    private SpsTAsnDetailDomain asnDetailDomain;
    
    /** The denso part no. */
    private String dPn;
    
    /** The supplier part no. */
    private String sPn;
    
    /** The asn status. */
    private String asnStatus;
    
    /** The asn pn receiving status. */
    private String asnPnReceivingStatus;
    
    /** The total shipped qty. */
    private BigDecimal totalShippedQty;
    
    /** The total remaining qty. */
    private BigDecimal totalRemainingQty;
    
    /** The asn received qty. */
    private BigDecimal asnReceivedQty;
    
    /** The row count. */
    private String rowCount;
    
    //Start : For display on screen
    /** The total shipped qty str. */
    private String totalShippedQtyStr;
    
    /** The total remaining qty str. */
    private String totalRemainingQtyStr;
    
    /** The shipping qty str. */
    private String shippingQtyStr;
    
    /** The qty box str. */
    private String qtyBoxStr;
    
    /** The shipping box qty str. */
    private String shippingBoxQtyStr;
    
    /** The asn received qty str. */
    private String asnReceivedQtyStr;
    
    /** The order qty str. */
    private String orderQtyStr;
    //End : For display on screen
    
    /**
     * Instantiates a new DO domain.
     */
    public DoInformationDomain() {
        super();
        doDetailDomain = new SpsTDoDetailDomain();
        asnDetailDomain = new SpsTAsnDetailDomain();
    }

    /**
     * Gets the do detail domain.
     * 
     * @return the do detail domain.
     */
    public SpsTDoDetailDomain getDoDetailDomain() {
        return doDetailDomain;
    }
    
    /**
     * Sets the do detail domain.
     * 
     * @param doDetailDomain the do detail domain.
     */
    public void setDoDetailDomain(SpsTDoDetailDomain doDetailDomain) {
        this.doDetailDomain = doDetailDomain;
    }
    
    /**
     * Gets the asn detail domain.
     * 
     * @return the asn detail domain.
     */
    public SpsTAsnDetailDomain getAsnDetailDomain() {
        return asnDetailDomain;
    }

    /**
     * Sets the asn detail domain.
     * 
     * @param asnDetailDomain the asn detail domain.
     */
    public void setAsnDetailDomain(SpsTAsnDetailDomain asnDetailDomain) {
        this.asnDetailDomain = asnDetailDomain;
    }

    /**
     * Gets the denso part no.
     * 
     * @return the denso part no.
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Sets the denso part no.
     * 
     * @param dPn the denso part no.
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    
    /**
     * Gets the supplier part no.
     * 
     * @return the supplier part no.
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Sets the supplier part no.
     * 
     * @param sPn the supplier part no.
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Gets the total shipped qty.
     * 
     * @return the total shipped qty.
     */
    public BigDecimal getTotalShippedQty() {
        return totalShippedQty;
    }

    /**
     * Sets the total shipped qty.
     * 
     * @param totalShippedQty the total shipped qty.
     */
    public void setTotalShippedQty(BigDecimal totalShippedQty) {
        this.totalShippedQty = totalShippedQty;
    }

    /**
     * Gets the asn status.
     * 
     * @return the asn status
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * Sets the asn status.
     * 
     * @param asnStatus the asn status
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * Gets the asn pn receiving status.
     * 
     * @return the asn pn receiving status
     */
    public String getAsnPnReceivingStatus() {
        return asnPnReceivingStatus;
    }


    /**
     * Sets the asn pn receiving status.
     * 
     * @param asnPnReceivingStatus the asn pn receiving status
     */
    public void setAsnPnReceivingStatus(String asnPnReceivingStatus) {
        this.asnPnReceivingStatus = asnPnReceivingStatus;
    }

    /**
     * Gets the total remaining qty.
     * 
     * @return the total remaining qty
     */
    public BigDecimal getTotalRemainingQty() {
        return totalRemainingQty;
    }

    /**
     * Sets the total remaining qty.
     * 
     * @param totalRemainingQty the total remaining qty
     */
    public void setTotalRemainingQty(BigDecimal totalRemainingQty) {
        this.totalRemainingQty = totalRemainingQty;
    }

    /**
     * Gets the asn received qty.
     * 
     * @return the asn received qty
     */
    public BigDecimal getAsnReceivedQty() {
        return asnReceivedQty;
    }

    /**
     * Sets the asn received qty.
     * 
     * @param asnReceivedQty the asn received qty
     */
    public void setAsnReceivedQty(BigDecimal asnReceivedQty) {
        this.asnReceivedQty = asnReceivedQty;
    }

    /**
     * Gets the total shipped qty str.
     * 
     * @return the total shipped qty str
     */
    public String getTotalShippedQtyStr() {
        return totalShippedQtyStr;
    }

    /**
     * Sets the total shipped qty str.
     * 
     * @param totalShippedQtyStr the total shipped qty str
     */
    public void setTotalShippedQtyStr(String totalShippedQtyStr) {
        this.totalShippedQtyStr = totalShippedQtyStr;
    }

    /**
     * Gets the total remaining qty str.
     * 
     * @return the total remaining qty str
     */
    public String getTotalRemainingQtyStr() {
        return totalRemainingQtyStr;
    }

    /**
     * Sets the total remaining qty str.
     * 
     * @param totalRemainingQtyStr the total remaining qty str
     */
    public void setTotalRemainingQtyStr(String totalRemainingQtyStr) {
        this.totalRemainingQtyStr = totalRemainingQtyStr;
    }

    /**
     * Gets the shipping qty str.
     * 
     * @return the shipping qty str
     */
    public String getShippingQtyStr() {
        return shippingQtyStr;
    }

    /**
     * Sets the shipping qty str.
     * 
     * @param shippingQtyStr the shipping qty str
     */
    public void setShippingQtyStr(String shippingQtyStr) {
        this.shippingQtyStr = shippingQtyStr;
    }

    /**
     * Gets the qty box str.
     * 
     * @return the qty box str
     */
    public String getQtyBoxStr() {
        return qtyBoxStr;
    }

    /**
     * Sets the qty box str.
     * 
     * @param qtyBoxStr the qty box str
     */
    public void setQtyBoxStr(String qtyBoxStr) {
        this.qtyBoxStr = qtyBoxStr;
    }

    /**
     * Gets the shipping box qty str.
     * 
     * @return the shipping box qty str
     */
    public String getShippingBoxQtyStr() {
        return shippingBoxQtyStr;
    }

    /**
     * Sets the shipping box qty str.
     * 
     * @param shippingBoxQtyStr the shipping box qty str
     */
    public void setShippingBoxQtyStr(String shippingBoxQtyStr) {
        this.shippingBoxQtyStr = shippingBoxQtyStr;
    }

    /**
     * Gets the asn received qty str.
     * 
     * @return the asn received qty str
     */
    public String getAsnReceivedQtyStr() {
        return asnReceivedQtyStr;
    }

    /**
     * Sets the asn received qty str.
     * 
     * @param asnReceivedQtyStr the asn received qty str
     */
    public void setAsnReceivedQtyStr(String asnReceivedQtyStr) {
        this.asnReceivedQtyStr = asnReceivedQtyStr;
    }

    /**
     * Gets the order qty str.
     * 
     * @return the order qty str
     */
    public String getOrderQtyStr() {
        return orderQtyStr;
    }

    /**
     * Sets the order qty str.
     * 
     * @param orderQtyStr the order qty str
     */
    public void setOrderQtyStr(String orderQtyStr) {
        this.orderQtyStr = orderQtyStr;
    }

    /**
     * Gets the row count.
     * 
     * @return the row count
     */
    public String getRowCount() {
        return rowCount;
    }

    /**
     * Sets the row count.
     * 
     * @param rowCount the row count
     */
    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }
    
}