/*
 * ModifyDate Development company     Describe 
 * 2014/06/11 CSI PJ_Tiger           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class PlantSupplierWithScopeDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class PlantSupplierWithScopeDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -6264214463243864267L;

    /** The SpsM Plant Supplier Domain */
    private PlantSupplierDomain plantSupplierDomain; 
    
    /** The Data Scope Control domain */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The default constructor. */
    public PlantSupplierWithScopeDomain(){
        super();
        this.plantSupplierDomain = new PlantSupplierDomain();
        this.dataScopeControlDomain = new DataScopeControlDomain();
    }
    /**
     * <p>Getter method for dataScopeControlDomain.</p>
     *
     * @return the dataScopeControlDomain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * <p>Setter method for dataScopeControlDomain.</p>
     *
     * @param dataScopeControlDomain Set for dataScopeControlDomain
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    /**
     * <p>Getter method for plantSupplierDomain.</p>
     *
     * @return the plantSupplierDomain
     */
    public PlantSupplierDomain getPlantSupplierDomain() {
        return plantSupplierDomain;
    }
    /**
     * <p>Setter method for plantSupplierDomain.</p>
     *
     * @param plantSupplierDomain Set for plantSupplierDomain
     */
    public void setPlantSupplierDomain(PlantSupplierDomain plantSupplierDomain) {
        this.plantSupplierDomain = plantSupplierDomain;
    }

}
