package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * <p>TransferChangeDoDetailDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferChangeDoDetailDataFromCigmaDomain extends BaseDomain implements Serializable {
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6977327449622436846L;

    /** partNo */
    private String partNo;
    
    /** Lot Size */
    private String lotSize;
    
    /** NoOfBoxes */
    private String noOfBoxes;
    
    /** current order Qty */
    private String currentOrderQty;
    
    /** original order qty */
    private String originalOrderQty;
    
    /** unit of measure */
    private String unitOfMeasure;
    
    /** reason for change */
    private String reasonForChange;
    
    /** Item Description */
    private String itemDescription;
    
//    /** rcvLane */
//    private String rcvLane;
    /** Model */
    private String model;
    
    /** spsFlag */
    private String spsFlag;
    
    /** createBy */
    private String createBy;
    
    /** createDate */
    private String createDate;
    
    /** createTime */
    private String createTime;
    
    /** updateBy */
    private String updateBy;
    
    /** updateDate */
    private String updateDate;
    
    /** updateTime */
    private String updateTime;

    /** rcvLane */
    private String rcvLane;
    
    /** whLocation */
    private String whLocation;
    
    /** controlNo */
    private String controlNo;

    /** Supplier Plant (for send email). */
    private String sPcd;
    
    /** Kanban Cigma */
    private List<TransferDoKanbanlDataFromCigmaDomain> cigmaDoKanbanSeq;
    
    /** Supplier Part No */
    private String sPn;
    /** Kanban Type  */
    private String kanbanType;
    /** remark1  */
    private String remark1;
    /** remark2  */
    private String remark2;
    /** remark3  */
    private String remark3;
    /** D Cust. Part No. (STK Out, A Part QR)  */
    private String dCustomerPartNo;
    /** Supp Proc Code (STK Out QR)  */
    private String sProcessCode;

    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferChangeDoDetailDataFromCigmaDomain(){
        super();
    }
    
    /**
     * <p>Getter method for partNo.</p>
     *
     * @return the partNo
     */
    public String getPartNo() {
        return partNo;
    }

    /**
     * <p>Setter method for partNo.</p>
     *
     * @param partNo Set for partNo
     */
    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    /**
     * <p>Getter method for lotSize.</p>
     *
     * @return the lotSize
     */
    public String getLotSize() {
        return lotSize;
    }

    /**
     * <p>Setter method for lotSize.</p>
     *
     * @param lotSize Set for lotSize
     */
    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    /**
     * <p>Getter method for currentOrderQty.</p>
     *
     * @return the currentOrderQty
     */
    public String getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * <p>Setter method for currentOrderQty.</p>
     *
     * @param currentOrderQty Set for currentOrderQty
     */
    public void setCurrentOrderQty(String currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * <p>Getter method for originalOrderQty.</p>
     *
     * @return the originalOrderQty
     */
    public String getOriginalOrderQty() {
        return originalOrderQty;
    }

    /**
     * <p>Setter method for originalOrderQty.</p>
     *
     * @param originalOrderQty Set for originalOrderQty
     */
    public void setOriginalOrderQty(String originalOrderQty) {
        this.originalOrderQty = originalOrderQty;
    }

    /**
     * <p>Getter method for UnitOfMeasure.</p>
     *
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * <p>Setter method for UnitOfMeasure.</p>
     *
     * @param unitOfMeasure Set for UnitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>Getter method for reasonForChange.</p>
     *
     * @return the reasonForChange
     */
    public String getReasonForChange() {
        return reasonForChange;
    }

    /**
     * <p>Setter method for reasonForChange.</p>
     *
     * @param reasonForChange Set for reasonForChange
     */
    public void setReasonForChange(String reasonForChange) {
        this.reasonForChange = reasonForChange;
    }

    /**
     * <p>Getter method for itemDescription.</p>
     *
     * @return the itemDescription
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * <p>Setter method for itemDescription.</p>
     *
     * @param itemDescription Set for itemDescription
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * <p>Getter method for model.</p>
     *
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * <p>Setter method for model.</p>
     *
     * @param model Set for model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * <p>Getter method for spsFlag.</p>
     *
     * @return the spsFlag
     */
    public String getSpsFlag() {
        return spsFlag;
    }

    /**
     * <p>Setter method for spsFlag.</p>
     *
     * @param spsFlag Set for spsFlag
     */
    public void setSpsFlag(String spsFlag) {
        this.spsFlag = spsFlag;
    }

    /**
     * <p>Getter method for createBy.</p>
     *
     * @return the createBy
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * <p>Setter method for createBy.</p>
     *
     * @param createBy Set for createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * <p>Getter method for createDate.</p>
     *
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * <p>Setter method for createDate.</p>
     *
     * @param createDate Set for createDate
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * <p>Getter method for createTime.</p>
     *
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * <p>Setter method for createTime.</p>
     *
     * @param createTime Set for createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * <p>Getter method for updateBy.</p>
     *
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * <p>Setter method for updateBy.</p>
     *
     * @param updateBy Set for updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * <p>Getter method for updateDate.</p>
     *
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * <p>Setter method for updateDate.</p>
     *
     * @param updateDate Set for updateDate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * <p>Getter method for updateTime.</p>
     *
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * <p>Setter method for updateTime.</p>
     *
     * @param updateTime Set for updateTime
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * <p>Getter method for cigmaDoKanbanSeq.</p>
     *
     * @return the cigmaDoKanbanSeq
     */
    public List<TransferDoKanbanlDataFromCigmaDomain> getCigmaDoKanbanSeq() {
        return cigmaDoKanbanSeq;
    }

    /**
     * <p>Setter method for cigmaDoKanbanSeq.</p>
     *
     * @param cigmaDoKanbanSeq Set for cigmaDoKanbanSeq
     */
    public void setCigmaDoKanbanSeq(
        List<TransferDoKanbanlDataFromCigmaDomain> cigmaDoKanbanSeq) {
        this.cigmaDoKanbanSeq = cigmaDoKanbanSeq;
    }

    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for noOfBoxes.</p>
     *
     * @return the noOfBoxes
     */
    public String getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * <p>Setter method for noOfBoxes.</p>
     *
     * @param noOfBoxes Set for noOfBoxes
     */
    public void setNoOfBoxes(String noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * <p>Getter method for whLocation.</p>
     *
     * @return the whLocation
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * <p>Setter method for whLocation.</p>
     *
     * @param whLocation Set for whLocation
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }

    /**
     * <p>Getter method for controlNo.</p>
     *
     * @return the controlNo
     */
    public String getControlNo() {
        return controlNo;
    }

    /**
     * <p>Setter method for controlNo.</p>
     *
     * @param controlNo Set for controlNo
     */
    public void setControlNo(String controlNo) {
        this.controlNo = controlNo;
    }
    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for rcvLane.</p>
     *
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>Setter method for rcvLane.</p>
     *
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>Getter method for kanbanType.</p>
     *
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }

    /**
     * <p>Setter method for kanbanType.</p>
     *
     * @param kanbanType Set for kanbanType
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }

    /**
     * <p>Getter method for remark1.</p>
     *
     * @return the remark1
     */
    public String getRemark1() {
        return remark1;
    }

    /**
     * <p>Setter method for remark1.</p>
     *
     * @param remark1 Set for remark1
     */
    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    /**
     * <p>Getter method for remark2.</p>
     *
     * @return the remark2
     */
    public String getRemark2() {
        return remark2;
    }

    /**
     * <p>Setter method for remark2.</p>
     *
     * @param remark2 Set for remark2
     */
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    /**
     * <p>Getter method for remark3.</p>
     *
     * @return the remark3
     */
    public String getRemark3() {
        return remark3;
    }

    /**
     * <p>Setter method for remark3.</p>
     *
     * @param remark3 Set for remark3
     */
    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    /**
     * <p>Getter method for dCustomerPartNo.</p>
     *
     * @return the dCustomerPartNo
     */
    public String getdCustomerPartNo() {
        return dCustomerPartNo;
    }

    /**
     * <p>Setter method for dCustomerPartNo.</p>
     *
     * @param dCustomerPartNo Set for dCustomerPartNo
     */
    public void setdCustomerPartNo(String dCustomerPartNo) {
        this.dCustomerPartNo = dCustomerPartNo;
    }

    /**
     * <p>Getter method for sProcessCode.</p>
     *
     * @return the sProcessCode
     */
    public String getsProcessCode() {
        return sProcessCode;
    }

    /**
     * <p>Setter method for sProcessCode.</p>
     *
     * @param sProcessCode Set for sProcessCode
     */
    public void setsProcessCode(String sProcessCode) {
        this.sProcessCode = sProcessCode;
    }
    
}
