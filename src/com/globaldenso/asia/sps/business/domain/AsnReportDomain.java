/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class ASN Report Domain
 * @author CSI
 */
public class AsnReportDomain extends BaseDomain implements Serializable {
    
    /** The Generated Serial Version UID. */
    private static final long serialVersionUID = 7737162369322914608L;

    /** The ASN Number. */
    private String asnNo;
    
    /** The Supplier Company Code. */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;
    
    /** The Supplier Company Name. */
    private String supplierName;
    
    /** The Supplier Plant Code. */
    private String sPcd;
    
    /** The ETD Date. */
    private String etdDate;
    
    /** The ETD Time. */ 
    private String etdTime;
    
    /** The ETA Date. */
    private String etaDate;
    
    /** The ETA Time. */
    private String etaTime;
    
    /** The DENSO Plant Code. */
    private String dPcd;
    
    /** The Trip Number. */
    private String tripNo;
    
    /** The Number of Pallet. */
    private String numberOfPallet;
    
    /** The Issue Date (createDatetime). */
    private String issueDate;
    
    /** The Last Modified (lastUpdateDatetime). */
    private String lastModified;
    
    /** The Total Number of Boxes. */
    private String totalNoOfBoxes;
    
    /** The Receiving Lane. */
    private String rcvLane;
    
    /** The Receiving Lane Count. */
    private Integer rcvLaneCount;
    
    /** The Flag (reviseShippingQTYFlag). */
    private String flag;
    
    /** The DENSO Part Number. */
    private String dPn;
    
    /** The Supplier Part Number. */
    private String sPn;
    
    /** The UM (unitOfMeasure). */
    private String um;
    
    /** The SPS DO Number. */
    private String spsDoNo;
    
    /** The CIGMA DO Number. */
    private String cigmaDoNo;
    
    /** The DO ID. */
    private String doId;
    
    /** The Order Method. */
    private String orderMethod;
    
    /** The Route Number (truckRoute). */
    private String routeNo;
    
    /** The Del Number (truckSeq). */
    private String delNo;
    
    /** The Supplier Location. */
    private String supplierLocation;
    
    /** The Warehouse (whPrimeReceiving). */
    private String warehouse;
    
    /** The Dock Code. */
    private String dockCode;
    
    /** The Trans (tm). */
    private String trans;
    
    /** The Cycle. */
    private String cycle;
    
    /** The Control Number. */
    private String ctrlNo;
    
    /** The Description (itemDesc). */
    private String description;
    
    /** The QTY Box. */
    private BigDecimal qtyBox;
    
    /** The QTY Box in String type. */
    private String stringQtyBox;

    /** The Number of Boxes. */
    private BigDecimal noOfBoxes;

    /** The Number of Boxes in String type. */
    private String stringNoOfBoxes;
    
    /** The Current Order Quantity. */
    private String currentOrderQty;
    
    /** The Current Order Quantity without comma. */
    private String currentOrderQtyNoComma;
    
    /** Change Reason Code. */
    private String rsn;
    
    /** The KANBAN Sequence Number. */
    private String kanbanSeqNo;
    
    /** Data in QR Code impage. */
    private InputStream qrCode;
    
    /** QR warning */
    private String qrWarning;
    
    // [IN014] Keep Shipping QTY for Preview Report
    /** Temp Shipping Qty for Preview Report */
    private BigDecimal tempShippingQty;
    
    /** qrCodeReceiveByScan1 */
    private InputStream qrCodeReceiveByScan1;
    
    /** qrCodeReceiveByScan2 */
    private InputStream qrCodeReceiveByScan2;
    
    /** qrCodeReceiveByScan3 */
    private InputStream qrCodeReceiveByScan3;
    
    /** qrCodeReceiveByScan4 */
    private InputStream qrCodeReceiveByScan4;
    
    /** The default constructor. */
    public AsnReportDomain() {
        super();
    }

    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for getDPcd.</p>
     *
     * @return the getDPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for getDPcd.</p>
     *
     * @param dPcd Set for getDPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for tripNo.</p>
     *
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * <p>Setter method for tripNo.</p>
     *
     * @param tripNo Set for tripNo
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * <p>Getter method for numberOfPallet.</p>
     *
     * @return the numberOfPallet
     */
    public String getNumberOfPallet() {
        return numberOfPallet;
    }

    /**
     * <p>Setter method for numberOfPallet.</p>
     *
     * @param numberOfPallet Set for numberOfPallet
     */
    public void setNumberOfPallet(String numberOfPallet) {
        this.numberOfPallet = numberOfPallet;
    }

    /**
     * <p>Getter method for totalNoOfBoxes.</p>
     *
     * @return the totalNoOfBoxes
     */
    public String getTotalNoOfBoxes() {
        return totalNoOfBoxes;
    }

    /**
     * <p>Setter method for totalNoOfBoxes.</p>
     *
     * @param totalNoOfBoxes Set for totalNoOfBoxes
     */
    public void setTotalNoOfBoxes(String totalNoOfBoxes) {
        this.totalNoOfBoxes = totalNoOfBoxes;
    }

    /**
     * <p>Getter method for rcvLane.</p>
     *
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>Setter method for rcvLane.</p>
     *
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for doId.</p>
     *
     * @return the doId
     */
    public String getDoId() {
        return doId;
    }

    /**
     * <p>Setter method for doId.</p>
     *
     * @param doId Set for doId
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * <p>Getter method for orderMethod.</p>
     *
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * <p>Setter method for orderMethod.</p>
     *
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * <p>Getter method for supplierLocation.</p>
     *
     * @return the supplierLocation
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * <p>Setter method for supplierLocation.</p>
     *
     * @param supplierLocation Set for supplierLocation
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * <p>Getter method for dockCode.</p>
     *
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * <p>Setter method for dockCode.</p>
     *
     * @param dockCode Set for dockCode
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * <p>Getter method for cycle.</p>
     *
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * <p>Setter method for cycle.</p>
     *
     * @param cycle Set for cycle
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * <p>Getter method for ctrlNo.</p>
     *
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * <p>Setter method for ctrlNo.</p>
     *
     * @param ctrlNo Set for ctrlNo
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * <p>Getter method for qtyBox.</p>
     *
     * @return the qtyBox
     */
    public BigDecimal getQtyBox() {
        return qtyBox;
    }

    /**
     * <p>Setter method for qtyBox.</p>
     *
     * @param qtyBox Set for qtyBox
     */
    public void setQtyBox(BigDecimal qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * <p>Getter method for noOfBoxes.</p>
     *
     * @return the noOfBoxes
     */
    public BigDecimal getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * <p>Setter method for noOfBoxes.</p>
     *
     * @param noOfBoxes Set for noOfBoxes
     */
    public void setNoOfBoxes(BigDecimal noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }
    
    /**
     * <p>Getter method for stringNoOfBoxes.</p>
     *
     * @return the stringNoOfBoxes
     */
    public String getStringNoOfBoxes() {
        return stringNoOfBoxes;
    }

    /**
     * <p>Setter method for stringNoOfBoxes.</p>
     *
     * @param stringNoOfBoxes Set for stringNoOfBoxes
     */
    public void setStringNoOfBoxes(String stringNoOfBoxes) {
        this.stringNoOfBoxes = stringNoOfBoxes;
    }

    /**
     * <p>Getter method for currentOrderQTY.</p>
     *
     * @return the currentOrderQTY
     */
    public String getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * <p>Setter method for currentOrderQTY.</p>
     *
     * @param currentOrderQty Set for currentOrderQTY
     */
    public void setCurrentOrderQty(String currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * <p>Getter method for kanbanSeqNo.</p>
     *
     * @return the kanbanSeqNo
     */
    public String getKanbanSeqNo() {
        return kanbanSeqNo;
    }

    /**
     * <p>Setter method for kanbanSeqNo.</p>
     *
     * @param kanbanSeqNo Set for kanbanSeqNo
     */
    public void setKanbanSeqNo(String kanbanSeqNo) {
        this.kanbanSeqNo = kanbanSeqNo;
    }

    /**
     * <p>Getter method for etdDate.</p>
     *
     * @return the etdDate
     */
    public String getEtdDate() {
        return etdDate;
    }

    /**
     * <p>Setter method for etdDate.</p>
     *
     * @param etdDate Set for etdDate
     */
    public void setEtdDate(String etdDate) {
        this.etdDate = etdDate;
    }

    /**
     * <p>Getter method for etdTime.</p>
     *
     * @return the etdTime
     */
    public String getEtdTime() {
        return etdTime;
    }

    /**
     * <p>Setter method for etdTime.</p>
     *
     * @param etdTime Set for etdTime
     */
    public void setEtdTime(String etdTime) {
        this.etdTime = etdTime;
    }

    /**
     * <p>Getter method for etaDate.</p>
     *
     * @return the etaDate
     */
    public String getEtaDate() {
        return etaDate;
    }

    /**
     * <p>Setter method for etaDate.</p>
     *
     * @param etaDate Set for etaDate
     */
    public void setEtaDate(String etaDate) {
        this.etaDate = etaDate;
    }

    /**
     * <p>Getter method for etaTime.</p>
     *
     * @return the etaTime
     */
    public String getEtaTime() {
        return etaTime;
    }

    /**
     * <p>Setter method for etaTime.</p>
     *
     * @param etaTime Set for etaTime
     */
    public void setEtaTime(String etaTime) {
        this.etaTime = etaTime;
    }

    /**
     * <p>Getter method for flag.</p>
     *
     * @return the flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * <p>Setter method for flag.</p>
     *
     * @param flag Set for flag
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * <p>Getter method for um.</p>
     *
     * @return the um
     */
    public String getUm() {
        return um;
    }

    /**
     * <p>Setter method for um.</p>
     *
     * @param um Set for um
     */
    public void setUm(String um) {
        this.um = um;
    }

    /**
     * <p>Getter method for routeNo.</p>
     *
     * @return the routeNo
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * <p>Setter method for routeNo.</p>
     *
     * @param routeNo Set for routeNo
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * <p>Getter method for delNo.</p>
     *
     * @return the delNo
     */
    public String getDelNo() {
        return delNo;
    }

    /**
     * <p>Setter method for delNo.</p>
     *
     * @param delNo Set for delNo
     */
    public void setDelNo(String delNo) {
        this.delNo = delNo;
    }

    /**
     * <p>Getter method for warehouse.</p>
     *
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * <p>Setter method for warehouse.</p>
     *
     * @param warehouse Set for warehouse
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * <p>Getter method for trans.</p>
     *
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * <p>Setter method for trans.</p>
     *
     * @param trans Set for trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * <p>Getter method for description.</p>
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter method for description.</p>
     *
     * @param description Set for description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * <p>Getter method for lastModified.</p>
     *
     * @return the lastModified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * <p>Setter method for lastModified.</p>
     *
     * @param lastModified Set for lastModified
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * <p>Getter method for stringQtyBox.</p>
     *
     * @return the stringQtyBox
     */
    public String getStringQtyBox() {
        return stringQtyBox;
    }

    /**
     * <p>Setter method for stringQtyBox.</p>
     *
     * @param stringQtyBox Set for stringQtyBox
     */
    public void setStringQtyBox(String stringQtyBox) {
        this.stringQtyBox = stringQtyBox;
    }

    /**
     * <p>Getter method for spsDoNo.</p>
     *
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>Setter method for spsDoNo.</p>
     *
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for currentOrderQtyNoComma.</p>
     *
     * @return the currentOrderQtyNoComma
     */
    public String getCurrentOrderQtyNoComma() {
        return currentOrderQtyNoComma;
    }

    /**
     * <p>Setter method for currentOrderQtyNoComma.</p>
     *
     * @param currentOrderQtyNoComma Set for currentOrderQtyNoComma
     */
    public void setCurrentOrderQtyNoComma(String currentOrderQtyNoComma) {
        this.currentOrderQtyNoComma = currentOrderQtyNoComma;
    }

    /**
     * <p>Getter method for rcvLaneCount.</p>
     *
     * @return the rcvLaneCount
     */
    public Integer getRcvLaneCount() {
        return rcvLaneCount;
    }

    /**
     * <p>Setter method for rcvLaneCount.</p>
     *
     * @param rcvLaneCount Set for rcvLaneCount
     */
    public void setRcvLaneCount(Integer rcvLaneCount) {
        this.rcvLaneCount = rcvLaneCount;
    }

    /**
     * <p>Getter method for qrCode.</p>
     *
     * @return the qrCode
     */
    public InputStream getQrCode() {
        return qrCode;
    }

    /**
     * <p>Setter method for qrCode.</p>
     *
     * @param qrCode Set for qrCode
     */
    public void setQrCode(InputStream qrCode) {
        this.qrCode = qrCode;
    }

    /**
     * <p>Getter method for qrWarning.</p>
     *
     * @return the qrWarning
     */
    public String getQrWarning() {
        return qrWarning;
    }

    /**
     * <p>Setter method for qrWarning.</p>
     *
     * @param qrWarning Set for qrWarning
     */
    public void setQrWarning(String qrWarning) {
        this.qrWarning = qrWarning;
    }

    /**
     * <p>Getter method for rsn.</p>
     *
     * @return the rsn
     */
    public String getRsn() {
        return rsn;
    }

    /**
     * <p>Setter method for rsn.</p>
     *
     * @param rsn Set for rsn
     */
    public void setRsn(String rsn) {
        this.rsn = rsn;
    }

    /**
     * <p>Getter method for tempShippingQty.</p>
     *
     * @return the tempShippingQty
     */
    public BigDecimal getTempShippingQty() {
        return tempShippingQty;
    }

    /**
     * <p>Setter method for tempShippingQty.</p>
     *
     * @param tempShippingQty Set for tempShippingQty
     */
    public void setTempShippingQty(BigDecimal tempShippingQty) {
        this.tempShippingQty = tempShippingQty;
    }

    /**
     * <p>Getter method for qrCodeReceiveByScan1.</p>
     *
     * @return the qrCodeReceiveByScan1
     */
    public InputStream getQrCodeReceiveByScan1() {
        return qrCodeReceiveByScan1;
    }

    /**
     * <p>Setter method for qrCodeReceiveByScan1.</p>
     *
     * @param qrCodeReceiveByScan1 Set for qrCodeReceiveByScan1
     */
    public void setQrCodeReceiveByScan1(InputStream qrCodeReceiveByScan1) {
        this.qrCodeReceiveByScan1 = qrCodeReceiveByScan1;
    }

    /**
     * <p>Getter method for qrCodeReceiveByScan2.</p>
     *
     * @return the qrCodeReceiveByScan2
     */
    public InputStream getQrCodeReceiveByScan2() {
        return qrCodeReceiveByScan2;
    }

    /**
     * <p>Setter method for qrCodeReceiveByScan2.</p>
     *
     * @param qrCodeReceiveByScan2 Set for qrCodeReceiveByScan2
     */
    public void setQrCodeReceiveByScan2(InputStream qrCodeReceiveByScan2) {
        this.qrCodeReceiveByScan2 = qrCodeReceiveByScan2;
    }

    /**
     * <p>Getter method for qrCodeReceiveByScan3.</p>
     *
     * @return the qrCodeReceiveByScan3
     */
    public InputStream getQrCodeReceiveByScan3() {
        return qrCodeReceiveByScan3;
    }

    /**
     * <p>Setter method for qrCodeReceiveByScan3.</p>
     *
     * @param qrCodeReceiveByScan3 Set for qrCodeReceiveByScan3
     */
    public void setQrCodeReceiveByScan3(InputStream qrCodeReceiveByScan3) {
        this.qrCodeReceiveByScan3 = qrCodeReceiveByScan3;
    }

    /**
     * <p>Getter method for qrCodeReceiveByScan4.</p>
     *
     * @return the qrCodeReceiveByScan4
     */
    public InputStream getQrCodeReceiveByScan4() {
        return qrCodeReceiveByScan4;
    }

    /**
     * <p>Setter method for qrCodeReceiveByScan4.</p>
     *
     * @param qrCodeReceiveByScan4 Set for qrCodeReceiveByScan4
     */
    public void setQrCodeReceiveByScan4(InputStream qrCodeReceiveByScan4) {
        this.qrCodeReceiveByScan4 = qrCodeReceiveByScan4;
    }
    
}
