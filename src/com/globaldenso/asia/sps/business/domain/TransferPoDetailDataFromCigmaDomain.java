package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * 
 * <p>TransferPoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferPoDetailDataFromCigmaDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -8884499246535931916L;
    /** dPartNo */
    private String dPn;
    /** itemDesc */
    private String itemdesc;
    /** unitPrice */
    private BigDecimal unitprice;
    /** currencyCode */
    private String currencycode;
    /** receivingDock */
    private String receivingdock;
    /** orderLot */
    private BigDecimal orderlot;
    /** qtyBox */
    private BigDecimal qtybox;
    /** unitOfMeasure */
    private String unitofmeasure;
    /** variableQtyCode */
    private String variableqtycode;
    /** partLblPrintRemarks */
    private String partlblprintremarks;
    /** phaseCode */
    private String phasecode;
    /** plannerCode */
    private String plannercode;
    /** Cigma Po Due List */
    private List<TransferPoDueDataFromCigmaDomain> cigmaPoDueList;
    /** Supplier Part No. */
    private String sPn;
    /** Maximium Create Date Time */
    private String maxCreateDateTime;
    /** Maximium Update Date Time */
    private String maxUpdateDateTime;
    /** Maximium Due Date Time(Report Type : M) */
    private Date maxDueDateTimeMonth;
    /** Minimium Due Date Time(Report Type : M) */
    private Date minDueDateTimeMonth;
    /** End period type Firm date. */
    private Date endFirm;
    /** Start Period Date. */
    private Date startPeriodDate;
    /** End Period Date. */
    private Date endPeriodDate;
    /** Temp Price Flag */
    private String tmpPriceFlg;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferPoDetailDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for dpartno.</p>
     *
     * @return the dpartno
     */
    public String getDPn() {
        return dPn;
    }
    /**
     * <p>Setter method for dpartno.</p>
     *
     * @param dPn Set for dpartno
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    /**
     * <p>Getter method for itemdesc.</p>
     *
     * @return the itemdesc
     */
    public String getItemDesc() {
        return itemdesc;
    }
    /**
     * <p>Setter method for itemdesc.</p>
     *
     * @param itemdesc Set for itemdesc
     */
    public void setItemDesc(String itemdesc) {
        this.itemdesc = itemdesc;
    }
    /**
     * <p>Getter method for unitprice.</p>
     *
     * @return the unitprice
     */
    public BigDecimal getUnitPrice() {
        return unitprice;
    }
    /**
     * <p>Setter method for unitprice.</p>
     *
     * @param unitprice Set for unitprice
     */
    public void setUnitPrice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }
    /**
     * <p>Getter method for currencycode.</p>
     *
     * @return the currencycode
     */
    public String getCurrencyCode() {
        return currencycode;
    }
    /**
     * <p>Setter method for currencycode.</p>
     *
     * @param currencycode Set for currencycode
     */
    public void setCurrencyCode(String currencycode) {
        this.currencycode = currencycode;
    }
    /**
     * <p>Getter method for receivingdock.</p>
     *
     * @return the receivingdock
     */
    public String getReceivingDock() {
        return receivingdock;
    }
    /**
     * <p>Setter method for receivingdock.</p>
     *
     * @param receivingdock Set for receivingdock
     */
    public void setReceivingDock(String receivingdock) {
        this.receivingdock = receivingdock;
    }
    /**
     * <p>Getter method for orderlot.</p>
     *
     * @return the orderlot
     */
    public BigDecimal getOrderLot() {
        return orderlot;
    }
    /**
     * <p>Setter method for orderlot.</p>
     *
     * @param orderlot Set for orderlot
     */
    public void setOrderLot(BigDecimal orderlot) {
        this.orderlot = orderlot;
    }
    /**
     * <p>Getter method for qtybox.</p>
     *
     * @return the qtybox
     */
    public BigDecimal getQtyBox() {
        return qtybox;
    }
    /**
     * <p>Setter method for qtybox.</p>
     *
     * @param qtybox Set for qtybox
     */
    public void setQtyBox(BigDecimal qtybox) {
        this.qtybox = qtybox;
    }
    /**
     * <p>Getter method for unitofmeasure.</p>
     *
     * @return the unitofmeasure
     */
    public String getUnitOfMeasure() {
        return unitofmeasure;
    }
    /**
     * <p>Setter method for unitofmeasure.</p>
     *
     * @param unitofmeasure Set for unitofmeasure
     */
    public void setUnitOfMeasure(String unitofmeasure) {
        this.unitofmeasure = unitofmeasure;
    }
    /**
     * <p>Getter method for variableqtycode.</p>
     *
     * @return the variableqtycode
     */
    public String getVariableQtyCode() {
        return variableqtycode;
    }
    /**
     * <p>Setter method for variableqtycode.</p>
     *
     * @param variableqtycode Set for variableqtycode
     */
    public void setVariableQtyCode(String variableqtycode) {
        this.variableqtycode = variableqtycode;
    }
    /**
     * <p>Getter method for partlblprintremarks.</p>
     *
     * @return the partlblprintremarks
     */
    public String getPartLblPrintRemarks() {
        return partlblprintremarks;
    }
    /**
     * <p>Setter method for partlblprintremarks.</p>
     *
     * @param partlblprintremarks Set for partlblprintremarks
     */
    public void setPartLblPrintRemarks(String partlblprintremarks) {
        this.partlblprintremarks = partlblprintremarks;
    }
    /**
     * <p>Getter method for phasecode.</p>
     *
     * @return the phasecode
     */
    public String getPhaseCode() {
        return phasecode;
    }
    /**
     * <p>Setter method for phasecode.</p>
     *
     * @param phasecode Set for phasecode
     */
    public void setPhaseCode(String phasecode) {
        this.phasecode = phasecode;
    }
    /**
     * <p>Getter method for plannercode.</p>
     *
     * @return the plannercode
     */
    public String getPlannerCode() {
        return plannercode;
    }
    /**
     * <p>Setter method for plannercode.</p>
     *
     * @param plannercode Set for plannercode
     */
    public void setPlannerCode(String plannercode) {
        this.plannercode = plannercode;
    }
    /**
     * <p>Getter method for cigmaPoDue.</p>
     *
     * @return the cigmaPoDue
     */
    public List<TransferPoDueDataFromCigmaDomain> getCigmaPoDueList() {
        return cigmaPoDueList;
    }
    /**
     * <p>Setter method for cigmaPoDue.</p>
     *
     * @param cigmaPoDueList Set for cigmaPoDue
     */
    public void setCigmaPoDueList(List<TransferPoDueDataFromCigmaDomain> cigmaPoDueList) {
        this.cigmaPoDueList = cigmaPoDueList;
    }
    /**
     * <p>Getter method for supplierPartNo.</p>
     *
     * @return the supplierPartNo
     */
    public String getSPn() {
        return sPn;
    }
    /**
     * <p>Setter method for supplierPartNo.</p>
     *
     * @param sPn Set for supplierPartNo
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    /**
     * <p>Getter method for maxDueDateTimeMonth.</p>
     *
     * @return the maxDueDateTimeMonth
     */
    public Date getMaxDueDateTimeMonth() {
        return maxDueDateTimeMonth;
    }
    /**
     * <p>Setter method for maxDueDateTimeMonth.</p>
     *
     * @param maxDueDateTimeMonth Set for maxDueDateTimeMonth
     */
    public void setMaxDueDateTimeMonth(Date maxDueDateTimeMonth) {
        this.maxDueDateTimeMonth = maxDueDateTimeMonth;
    }
    /**
     * <p>Getter method for minDueDateTimeMonth.</p>
     *
     * @return the minDueDateTimeMonth
     */
    public Date getMinDueDateTimeMonth() {
        return minDueDateTimeMonth;
    }
    /**
     * <p>Setter method for minDueDateTimeMonth.</p>
     *
     * @param minDueDateTimeMonth Set for minDueDateTimeMonth
     */
    public void setMinDueDateTimeMonth(Date minDueDateTimeMonth) {
        this.minDueDateTimeMonth = minDueDateTimeMonth;
    }
    /**
     * <p>Getter method for maxCreateDateTime.</p>
     *
     * @return the maxCreateDateTime
     */
    public String getMaxCreateDateTime() {
        return maxCreateDateTime;
    }
    /**
     * <p>Setter method for maxCreateDateTime.</p>
     *
     * @param maxCreateDateTime Set for maxCreateDateTime
     */
    public void setMaxCreateDateTime(String maxCreateDateTime) {
        this.maxCreateDateTime = maxCreateDateTime;
    }
    /**
     * <p>Getter method for maxUpdateDateTime.</p>
     *
     * @return the maxUpdateDateTime
     */
    public String getMaxUpdateDateTime() {
        return maxUpdateDateTime;
    }
    /**
     * <p>Setter method for maxUpdateDateTime.</p>
     *
     * @param maxUpdateDateTime Set for maxUpdateDateTime
     */
    public void setMaxUpdateDateTime(String maxUpdateDateTime) {
        this.maxUpdateDateTime = maxUpdateDateTime;
    }
    /**
     * <p>Getter method for endFirm.</p>
     *
     * @return the endFirm
     */
    public Date getEndFirm() {
        return endFirm;
    }
    /**
     * <p>Setter method for endFirm.</p>
     *
     * @param endFirm Set for endFirm
     */
    public void setEndFirm(Date endFirm) {
        this.endFirm = endFirm;
    }
    /**
     * <p>Getter method for startPeriodDate.</p>
     *
     * @return the startPeriodDate
     */
    public Date getStartPeriodDate() {
        return startPeriodDate;
    }
    /**
     * <p>Setter method for startPeriodDate.</p>
     *
     * @param startPeriodDate Set for startPeriodDate
     */
    public void setStartPeriodDate(Date startPeriodDate) {
        this.startPeriodDate = startPeriodDate;
    }
    /**
     * <p>Getter method for endPeriodDate.</p>
     *
     * @return the endPeriodDate
     */
    public Date getEndPeriodDate() {
        return endPeriodDate;
    }
    /**
     * <p>Setter method for endPeriodDate.</p>
     *
     * @param endPeriodDate Set for endPeriodDate
     */
    public void setEndPeriodDate(Date endPeriodDate) {
        this.endPeriodDate = endPeriodDate;
    }
    /**
     * <p>Getter method for tmpPriceFlg.</p>
     *
     * @return the tmpPriceFlg
     */
    public String getTmpPriceFlg() {
        return tmpPriceFlg;
    }
    /**
     * <p>Setter method for tmpPriceFlg.</p>
     *
     * @param tmpPriceFlg Set for tmpPriceFlg
     */
    public void setTmpPriceFlg(String tmpPriceFlg) {
        this.tmpPriceFlg = tmpPriceFlg;
    }
}
