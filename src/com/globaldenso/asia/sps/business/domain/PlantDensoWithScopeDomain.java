/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class PlantDensoWithScopeDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class PlantDensoWithScopeDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 7034591826971852333L;

    /** The domain for SPS_M_PLANT_DENSO. */
    private PlantDensoDomain plantDensoDomain;

    /** The Data Scope Control domain */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The default constructor. */
    public PlantDensoWithScopeDomain() {
        super();
        this.plantDensoDomain = new PlantDensoDomain(); 
        this.dataScopeControlDomain = new DataScopeControlDomain();
    }

    /**
     * Get method for plantDensoDomain.
     * @return the plantDensoDomain
     */
    public PlantDensoDomain getPlantDensoDomain() {
        return plantDensoDomain;
    }

    /**
     * Set method for plantDensoDomain.
     * @param plantDensoDomain the plantDensoDomain to set
     */
    public void setPlantDensoDomain(PlantDensoDomain plantDensoDomain) {
        this.plantDensoDomain = plantDensoDomain;
    }

    /**
     * Get method for dataScopeControlDomain.
     * @return the dataScopeControlDomain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Set method for dataScopeControlDomain.
     * @param dataScopeControlDomain the dataScopeControlDomain to set
     */
    public void setDataScopeControlDomain(DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    
}
