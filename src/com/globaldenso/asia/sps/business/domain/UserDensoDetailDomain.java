/*
 * ModifyDate Development company Describe 
 * 2014/07/02 CSI Akat           Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;

/**
 * The Class DensoUserDetailDomain. Domain for DENSO user information.
 * 
 * @author CSI
 * @version 1.00
 */
public class UserDensoDetailDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -983986646909833718L;

    /** The information for user from DENSO. */
    private SpsMUserDensoDomain spsMUserDensoDomain;
    
    /** The company DENSO information. */
    private SpsMCompanyDensoDomain spsMCompanyDensoDomain;
    
    /** The plant DENSO information. */
    private SpsMPlantDensoDomain spsMPlantDensoDomain;
    
    /** The information of user. */
    private SpsMUserDomain spsMUserDomain;
    
    /** The email user Domain.*/
    private UserDomain userDomain;
    
    /** The User Role Detail Domain.*/
    private UserRoleDetailDomain userRoleDetailDomain;
    
    /** The User Role Detail Domain.*/
    private List<UserRoleDetailDomain> userRoleDetailList;
    
    /** The list of Role Domain.*/
    private List<SpsMRoleDomain> SpsMRoleList; 
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The default constructor. */
    public UserDensoDetailDomain() {
        this.spsMUserDensoDomain = new SpsMUserDensoDomain();
        this.spsMCompanyDensoDomain = new SpsMCompanyDensoDomain();
        this.spsMPlantDensoDomain = new SpsMPlantDensoDomain();
        this.userRoleDetailDomain = new UserRoleDetailDomain();
        this.userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
        this.spsMUserDomain = new SpsMUserDomain();
        this.userDomain = new UserDomain();
    }

    /**
     * <p>Getter method for spsMUserDensoDomain.</p>
     *
     * @return the spsMUserDensoDomain
     */
    public SpsMUserDensoDomain getSpsMUserDensoDomain() {
        return spsMUserDensoDomain;
    }

    /**
     * <p>Setter method for spsMUserDensoDomain.</p>
     *
     * @param spsMUserDensoDomain Set for spsMUserDensoDomain
     */
    public void setSpsMUserDensoDomain(SpsMUserDensoDomain spsMUserDensoDomain) {
        this.spsMUserDensoDomain = spsMUserDensoDomain;
    }

    /**
     * <p>Getter method for spsMCompanyDensoDomain.</p>
     *
     * @return the spsMCompanyDensoDomain
     */
    public SpsMCompanyDensoDomain getSpsMCompanyDensoDomain() {
        return spsMCompanyDensoDomain;
    }

    /**
     * <p>Setter method for spsMCompanyDensoDomain.</p>
     *
     * @param spsMCompanyDensoDomain Set for spsMCompanyDensoDomain
     */
    public void setSpsMCompanyDensoDomain(
        SpsMCompanyDensoDomain spsMCompanyDensoDomain) {
        this.spsMCompanyDensoDomain = spsMCompanyDensoDomain;
    }

    /**
     * <p>Getter method for spsMPlantDensoDomain.</p>
     *
     * @return the spsMPlantDensoDomain
     */
    public SpsMPlantDensoDomain getSpsMPlantDensoDomain() {
        return spsMPlantDensoDomain;
    }

    /**
     * <p>Setter method for spsMPlantDensoDomain.</p>
     *
     * @param spsMPlantDensoDomain Set for spsMPlantDensoDomain
     */
    public void setSpsMPlantDensoDomain(SpsMPlantDensoDomain spsMPlantDensoDomain) {
        this.spsMPlantDensoDomain = spsMPlantDensoDomain;
    }
    
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public SpsMUserDomain getSpsMUserDomain() {
        return spsMUserDomain;
    }
    /**
     * Sets the user Domain.
     * 
     * @param spsMUserDomain the userDomain.
     */
    public void setSpsMUserDomain(SpsMUserDomain spsMUserDomain) {
        this.spsMUserDomain = spsMUserDomain;
    }
    /**
     * Gets the user role detail Domain.
     * 
     * @return the userRoleDetailDomain.
     */
    public UserRoleDetailDomain getUserRoleDetailDomain() {
        return userRoleDetailDomain;
    }
    /**
     * Sets the user role detail Domain.
     * 
     * @param userRoleDetailDomain the user role detail Domain.
     */
    public void setUserRoleDetailDomain(UserRoleDetailDomain userRoleDetailDomain) {
        this.userRoleDetailDomain = userRoleDetailDomain;
    }
    /**
     * Gets the list of user role detail Domain.
     * 
     * @return the userRoleDetailList.
     */
    public List<UserRoleDetailDomain> getUserRoleDetailList() {
        return userRoleDetailList;
    }
    /**
     * Sets the list of user role detail Domain.
     * 
     * @param userRoleDetailList the list of user role detail Domain.
     */
    public void setUserRoleDetailList(List<UserRoleDetailDomain> userRoleDetailList) {
        this.userRoleDetailList = userRoleDetailList;
    }
    /**
     * Gets the list of role Domain.
     * 
     * @return the spsMRoleList.
     */
    public List<SpsMRoleDomain> getSpsMRoleList() {
        return SpsMRoleList;
    }
    /**
     * Sets the list of role Domain.
     * 
     * @param spsMRoleList the list of role Domain.
     */
    public void setSpsMRoleList(List<SpsMRoleDomain> spsMRoleList) {
        SpsMRoleList = spsMRoleList;
    }
    /**
     * Gets the list of error message.
     * 
     * @return the errorMessageList.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }
    /**
     * Sets the list of error message.
     * 
     * @param errorMessageList the list of error message.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }
}
