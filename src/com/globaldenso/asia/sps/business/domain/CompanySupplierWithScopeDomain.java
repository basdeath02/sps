/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class CompanySupplierWithScopeDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class CompanySupplierWithScopeDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -1630521314900343970L;

    /** The domain for SPS_M_COMPANY_SUPPLIER. */
    private CompanySupplierDomain companySupplierDomain;

    /** The Data Scope Control domain */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The default constructor. */
    public CompanySupplierWithScopeDomain() {
        super();
        this.companySupplierDomain = new CompanySupplierDomain();
        this.dataScopeControlDomain = new DataScopeControlDomain();
    }

    /**
     * Get method for companySupplierDomain.
     * @return the companySupplierDomain
     */
    public CompanySupplierDomain getCompanySupplierDomain() {
        return companySupplierDomain;
    }

    /**
     * Set method for companySupplierDomain.
     * @param companySupplierDomain the companySupplierDomain to set
     */
    public void setCompanySupplierDomain(CompanySupplierDomain companySupplierDomain) {
        this.companySupplierDomain = companySupplierDomain;
    }

    /**
     * Get method for dataScopeControlDomain.
     * @return the dataScopeControlDomain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Set method for dataScopeControlDomain.
     * @param dataScopeControlDomain the dataScopeControlDomain to set
     */
    public void setDataScopeControlDomain(DataScopeControlDomain dataScopeControlDomain)
    {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    
    
}
