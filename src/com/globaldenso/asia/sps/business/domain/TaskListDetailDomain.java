package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * <p>
 * Task List Detail Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TaskListDetailDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serial.
     * </p>
     */
    private static final long serialVersionUID = -5247423688841732923L;

    /**
     * <p>
     * The Denso Code.
     * </p>
     */
    private String dCd;

    /**
     * <p>
     * The Category.
     * </p>
     */
    private String category;

    /**
     * <p>
     * The Quantity.
     * </p>
     */
    private String quantity;

    /**
     * <p>
     * The Constructor.
     * </p>
     */
    public TaskListDetailDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for densoCode.
     * </p>
     * 
     * @return the densoCode
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for densoCode.
     * </p>
     * 
     * @param dCd Set for densoCode
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for category.
     * </p>
     * 
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * <p>
     * Setter method for category.
     * </p>
     * 
     * @param category Set for category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * <p>
     * Getter method for quantity.
     * </p>
     * 
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * <p>
     * Setter method for quantity.
     * </p>
     * 
     * @param quantity Set for quantity
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
