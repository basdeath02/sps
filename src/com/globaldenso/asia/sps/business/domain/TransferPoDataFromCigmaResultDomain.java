package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * <p>TransferPoDataFromCigmaResultDomain class.</p>
 *
 * @author CSI
 */
public class TransferPoDataFromCigmaResultDomain extends BaseDomain implements Serializable {

    /** The Serial Version UID */
    private static final long serialVersionUID = 6466953113016909828L;
    /** Result Map */
    private Map<TransferPoDataFromCigmaDomain, TransferPoCoverPageDataFromCigmaDomain> resultMap
        = new HashMap<TransferPoDataFromCigmaDomain, TransferPoCoverPageDataFromCigmaDomain>();
    
    /** Result P/O Map */
    private Map<String, List<TransferPoDataFromCigmaDomain>> poDataMap 
        = new TreeMap<String, List<TransferPoDataFromCigmaDomain>>();
    
    /** Result P/O Cover Page Map */
    private Map<String, List<TransferPoCoverPageDataFromCigmaDomain>> coverPageMap
        = new TreeMap<String, List<TransferPoCoverPageDataFromCigmaDomain>>();
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferPoDataFromCigmaResultDomain() {
        super();
    }

    /**
     * <p>Getter method for resultMap.</p>
     *
     * @return the resultMap
     */
    public Map<TransferPoDataFromCigmaDomain, TransferPoCoverPageDataFromCigmaDomain> getResultMap() {
        return resultMap;
    }

    /**
     * <p>Setter method for resultMap.</p>
     *
     * @param resultMap Set for resultMap
     */
    public void setResultMap(
        Map<TransferPoDataFromCigmaDomain, TransferPoCoverPageDataFromCigmaDomain> resultMap) {
        this.resultMap = resultMap;
    }

    /**
     * <p>Getter method for poDataMap.</p>
     *
     * @return the poDataMap
     */
    public Map<String, List<TransferPoDataFromCigmaDomain>> getPoDataMap() {
        return poDataMap;
    }

    /**
     * <p>Setter method for poDataMap.</p>
     *
     * @param poDataMap Set for poDataMap
     */
    public void setPoDataMap(
        Map<String, List<TransferPoDataFromCigmaDomain>> poDataMap) {
        this.poDataMap = poDataMap;
    }

    /**
     * <p>Getter method for coverPageMap.</p>
     *
     * @return the coverPageMap
     */
    public Map<String, List<TransferPoCoverPageDataFromCigmaDomain>> getCoverPageMap() {
        return coverPageMap;
    }

    /**
     * <p>Setter method for coverPageMap.</p>
     *
     * @param coverPageMap Set for coverPageMap
     */
    public void setCoverPageMap(
        Map<String, List<TransferPoCoverPageDataFromCigmaDomain>> coverPageMap) {
        this.coverPageMap = coverPageMap;
    }
}
