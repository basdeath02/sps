/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The Class Invoice Maintenance Domain.
 * @author CSI
 */
public class InvoiceMaintenanceDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3857775057146425189L;
    
    /** The plant supplier tax ID. */
    private String supplierPlantTaxId;
    
    /** The session Id. */
    private String sessionId;
    
    /** The screen ID. */
    private String screenId;
    
    /** The invoice Id. */
    private String invoiceId;
    
    /** The ASN selected Map. */
    private Map<String, AsnInformationDomain> asnSelectedMap;
    
    /** The denso name. */
    private String densoName;
    
    /** The denso code. */
    private String dCd;
    
    /** The plant denso code. */
    private String dPcd;
    
    /** The denso tax id. */
    private String densoTaxId;
    
    /** The denso address. */
    private String densoAddress;
    
    /** The denso currency. */
    private String densoCurrency;
    
    /** The supplier name. */
    private String supplierName;
    
    /** The supplier code. */
    private String sCd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier tax id. */
    private String supplierTaxId;
    
    /** The supplier address. */
    private String supplierAddress;
    
    /** The supplier address1. */
    private String supplierAddress1;
    
    /** The supplier address2. */
    private String supplierAddress2;
    
    /** The supplier address3. */
    private String supplierAddress3;
    
    /** The supplier currency. */
    private String supplierCurrency;
    
    /** The supplier base amount*/
    private String supplierBaseAmount;
    
    /** The supplier vat amount */
    private String supplierVatAmount;
    
    /** The supplier total amount */
    private String supplierTotalAmount;
    
    /** The decimal disp. */
    private String decimalDisp;
    
    /** The vat rate */
    private String vatRate;
    
    /** The vat type */
    private String vatType;
    
    /** The invoice no. */
    private String invoiceNo;
    
    /** The invoice date. */
    private String invoiceDate;
    
    /** The total diff base amount */
    private String totalDiffBaseAmount;
    
    /** The total diff vat amount */
    private String totalDiffVatAmount;
    
    /** The total diff amount */
    private String totalDiffAmount;
    
    /** The total base amount */
    private String totalBaseAmount;
    
    /** The total vat amount */
    private String totalVatAmount;
    
    /** The total amount */
    private String totalAmount;
    
    /** The system date */
    private String sysDate;
    
    /** The cn no */
    private String cnNo;
    
    /** The cn date */
    private String cnDate;
    
    /** The cn base amount */
    private String cnBaseAmount;
    
    /** The cn vat amount */
    private String cnVatAmount;
    
    /** The cn total amount */
    private String cnTotalAmount;
    
    /** The invoice cover page */
    private InputStream invoiceCoverPage;
    
    /** The csv result. */
    private StringBuffer csvResult;
    
    /** The warning message. */
    private String warningMessage;
    
    /** The file id. */
    private String fileId;
    
    /** The file name. */
    private String fileName;
    
    /** The difference dPcd flag. */
    private String diffDPcdFlag;
    
    /** The ASN No register. */
    private Set<String> asnNoRegister;
    
    /** The ASN detail list. */
    private List<AsnDomain> asnDetailList;
    
    /** The success message list. */
    private List<ApplicationMessageDomain> successMessageList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new Invoice Maintenance Domain.
     */
    public InvoiceMaintenanceDomain() {
        super();
        errorMessageList = new ArrayList<ApplicationMessageDomain>();
        successMessageList = new ArrayList<ApplicationMessageDomain>();
    }
    
    /**
     * Gets the plant supplier tax id.
     * 
     * @return the plant supplier tax id.
     */
    public String getSupplierPlantTaxId() {
        return supplierPlantTaxId;
    }
    
    /**
     * Sets the plant supplier tax id.
     * 
     * @param supplierPlantTaxId the plant supplier tax id.
     */
    public void setSupplierPlantTaxId(String supplierPlantTaxId) {
        this.supplierPlantTaxId = supplierPlantTaxId;
    }
    
    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
    /**
     * <p>Getter method for screenId.</p>
     *
     * @return the screenId
     */
    public String getScreenId() {
        return screenId;
    }

    /**
     * <p>Setter method for screenId.</p>
     *
     * @param screenId Set for screenId
     */
    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }

    /**
     * <p>Getter method for invoiceId.</p>
     *
     * @return the invoiceId
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * <p>Setter method for invoiceId.</p>
     *
     * @param invoiceId Set for invoiceId
     */
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * <p>Getter method for asnSelectedMap.</p>
     *
     * @return the asnSelectedMap
     */
    public Map<String, AsnInformationDomain> getAsnSelectedMap() {
        return asnSelectedMap;
    }

    /**
     * <p>Setter method for asnSelectedMap.</p>
     *
     * @param asnSelectedMap Set for asnSelectedMap
     */
    public void setAsnSelectedMap(Map<String, AsnInformationDomain> asnSelectedMap) {
        this.asnSelectedMap = asnSelectedMap;
    }
    
    /**
     * <p>Getter method for densoName.</p>
     *
     * @return the densoName
     */
    public String getDensoName() {
        return densoName;
    }

    /**
     * <p>Setter method for densoName.</p>
     *
     * @param densoName Set for densoName
     */
    public void setDensoName(String densoName) {
        this.densoName = densoName;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for densoTaxId.</p>
     *
     * @return the densoTaxId
     */
    public String getDensoTaxId() {
        return densoTaxId;
    }

    /**
     * <p>Setter method for densoTaxId.</p>
     *
     * @param densoTaxId Set for densoTaxId
     */
    public void setDensoTaxId(String densoTaxId) {
        this.densoTaxId = densoTaxId;
    }

    /**
     * <p>Getter method for densoAddress.</p>
     *
     * @return the densoAddress
     */
    public String getDensoAddress() {
        return densoAddress;
    }

    /**
     * <p>Setter method for densoAddress.</p>
     *
     * @param densoAddress Set for densoAddress
     */
    public void setDensoAddress(String densoAddress) {
        this.densoAddress = densoAddress;
    }

    /**
     * <p>Getter method for densoCurrency.</p>
     *
     * @return the densoCurrency
     */
    public String getDensoCurrency() {
        return densoCurrency;
    }

    /**
     * <p>Setter method for densoCurrency.</p>
     *
     * @param densoCurrency Set for densoCurrency
     */
    public void setDensoCurrency(String densoCurrency) {
        this.densoCurrency = densoCurrency;
    }

    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }

    /**
     * <p>Getter method for supplierAddress.</p>
     *
     * @return the supplierAddress
     */
    public String getSupplierAddress() {
        return supplierAddress;
    }

    /**
     * <p>Setter method for supplierAddress.</p>
     *
     * @param supplierAddress Set for supplierAddress
     */
    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    /**
     * <p>Getter method for supplierAddress1.</p>
     *
     * @return the supplierAddress1
     */
    public String getSupplierAddress1() {
        return supplierAddress1;
    }

    /**
     * <p>Setter method for supplierAddress1.</p>
     *
     * @param supplierAddress1 Set for supplierAddress1
     */
    public void setSupplierAddress1(String supplierAddress1) {
        this.supplierAddress1 = supplierAddress1;
    }

    /**
     * <p>Getter method for supplierAddress2.</p>
     *
     * @return the supplierAddress2
     */
    public String getSupplierAddress2() {
        return supplierAddress2;
    }

    /**
     * <p>Setter method for supplierAddress2.</p>
     *
     * @param supplierAddress2 Set for supplierAddress2
     */
    public void setSupplierAddress2(String supplierAddress2) {
        this.supplierAddress2 = supplierAddress2;
    }

    /**
     * <p>Getter method for supplierAddress3.</p>
     *
     * @return the supplierAddress3
     */
    public String getSupplierAddress3() {
        return supplierAddress3;
    }

    /**
     * <p>Setter method for supplierAddress3.</p>
     *
     * @param supplierAddress3 Set for supplierAddress3
     */
    public void setSupplierAddress3(String supplierAddress3) {
        this.supplierAddress3 = supplierAddress3;
    }

    /**
     * <p>Getter method for supplierCurrency.</p>
     *
     * @return the supplierCurrency
     */
    public String getSupplierCurrency() {
        return supplierCurrency;
    }

    /**
     * <p>Setter method for supplierCurrency.</p>
     *
     * @param supplierCurrency Set for supplierCurrency
     */
    public void setSupplierCurrency(String supplierCurrency) {
        this.supplierCurrency = supplierCurrency;
    }

    /**
     * <p>Getter method for supplierBaseAmount.</p>
     *
     * @return the supplierBaseAmount
     */
    public String getSupplierBaseAmount() {
        return supplierBaseAmount;
    }

    /**
     * <p>Setter method for supplierBaseAmount.</p>
     *
     * @param supplierBaseAmount Set for supplierBaseAmount
     */
    public void setSupplierBaseAmount(String supplierBaseAmount) {
        this.supplierBaseAmount = supplierBaseAmount;
    }

    /**
     * <p>Getter method for supplierVatAmount.</p>
     *
     * @return the supplierVatAmount
     */
    public String getSupplierVatAmount() {
        return supplierVatAmount;
    }

    /**
     * <p>Setter method for supplierVatAmount.</p>
     *
     * @param supplierVatAmount Set for supplierVatAmount
     */
    public void setSupplierVatAmount(String supplierVatAmount) {
        this.supplierVatAmount = supplierVatAmount;
    }

    /**
     * <p>Getter method for supplierTotalAmount.</p>
     *
     * @return the supplierTotalAmount
     */
    public String getSupplierTotalAmount() {
        return supplierTotalAmount;
    }

    /**
     * <p>Setter method for supplierTotalAmount.</p>
     *
     * @param supplierTotalAmount Set for supplierTotalAmount
     */
    public void setSupplierTotalAmount(String supplierTotalAmount) {
        this.supplierTotalAmount = supplierTotalAmount;
    }

    /**
     * <p>Getter method for decimalDisp.</p>
     *
     * @return the decimalDisp
     */
    public String getDecimalDisp() {
        return decimalDisp;
    }

    /**
     * <p>Setter method for decimalDisp.</p>
     *
     * @param decimalDisp Set for decimalDisp
     */
    public void setDecimalDisp(String decimalDisp) {
        this.decimalDisp = decimalDisp;
    }

    /**
     * <p>Getter method for vatRate.</p>
     *
     * @return the vatRate
     */
    public String getVatRate() {
        return vatRate;
    }

    /**
     * <p>Setter method for vatRate.</p>
     *
     * @param vatRate Set for vatRate
     */
    public void setVatRate(String vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * <p>Getter method for vatType.</p>
     *
     * @return the vatType
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * <p>Setter method for vatType.</p>
     *
     * @param vatType Set for vatType
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * <p>Getter method for invoiceDate.</p>
     *
     * @return the invoiceDate
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * <p>Setter method for invoiceDate.</p>
     *
     * @param invoiceDate Set for invoiceDate
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * <p>Getter method for totalDiffBaseAmount.</p>
     *
     * @return the totalDiffBaseAmount
     */
    public String getTotalDiffBaseAmount() {
        return totalDiffBaseAmount;
    }

    /**
     * <p>Setter method for totalDiffBaseAmount.</p>
     *
     * @param totalDiffBaseAmount Set for totalDiffBaseAmount
     */
    public void setTotalDiffBaseAmount(String totalDiffBaseAmount) {
        this.totalDiffBaseAmount = totalDiffBaseAmount;
    }

    /**
     * <p>Getter method for totalDiffVatAmount.</p>
     *
     * @return the totalDiffVatAmount
     */
    public String getTotalDiffVatAmount() {
        return totalDiffVatAmount;
    }

    /**
     * <p>Setter method for totalDiffVatAmount.</p>
     *
     * @param totalDiffVatAmount Set for totalDiffVatAmount
     */
    public void setTotalDiffVatAmount(String totalDiffVatAmount) {
        this.totalDiffVatAmount = totalDiffVatAmount;
    }

    /**
     * <p>Getter method for totalDiffAmount.</p>
     *
     * @return the totalDiffAmount
     */
    public String getTotalDiffAmount() {
        return totalDiffAmount;
    }

    /**
     * <p>Setter method for totalDiffAmount.</p>
     *
     * @param totalDiffAmount Set for totalDiffAmount
     */
    public void setTotalDiffAmount(String totalDiffAmount) {
        this.totalDiffAmount = totalDiffAmount;
    }

    /**
     * <p>Getter method for totalBaseAmount.</p>
     *
     * @return the totalBaseAmount
     */
    public String getTotalBaseAmount() {
        return totalBaseAmount;
    }

    /**
     * <p>Setter method for totalBaseAmount.</p>
     *
     * @param totalBaseAmount Set for totalBaseAmount
     */
    public void setTotalBaseAmount(String totalBaseAmount) {
        this.totalBaseAmount = totalBaseAmount;
    }

    /**
     * <p>Getter method for totalVatAmount.</p>
     *
     * @return the totalVatAmount
     */
    public String getTotalVatAmount() {
        return totalVatAmount;
    }

    /**
     * <p>Setter method for totalVatAmount.</p>
     *
     * @param totalVatAmount Set for totalVatAmount
     */
    public void setTotalVatAmount(String totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    /**
     * <p>Getter method for totalAmount.</p>
     *
     * @return the totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * <p>Setter method for totalAmount.</p>
     *
     * @param totalAmount Set for totalAmount
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
    

    /**
     * <p>Getter method for sysDate.</p>
     *
     * @return the sysDate
     */
    public String getSysDate() {
        return sysDate;
    }

    /**
     * <p>Setter method for sysDate.</p>
     *
     * @param sysDate Set for sysDate
     */
    public void setSysDate(String sysDate) {
        this.sysDate = sysDate;
    }
    
    /**
     * <p>Getter method for cnNo.</p>
     *
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * <p>Setter method for cnNo.</p>
     *
     * @param cnNo Set for cnNo
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * <p>Getter method for cnDate.</p>
     *
     * @return the cnDate
     */
    public String getCnDate() {
        return cnDate;
    }

    /**
     * <p>Setter method for cnDate.</p>
     *
     * @param cnDate Set for cnDate
     */
    public void setCnDate(String cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * <p>Getter method for cnBaseAmount.</p>
     *
     * @return the cnBaseAmount
     */
    public String getCnBaseAmount() {
        return cnBaseAmount;
    }

    /**
     * <p>Setter method for cnBaseAmount.</p>
     *
     * @param cnBaseAmount Set for cnBaseAmount
     */
    public void setCnBaseAmount(String cnBaseAmount) {
        this.cnBaseAmount = cnBaseAmount;
    }

    /**
     * <p>Getter method for cnVatAmount.</p>
     *
     * @return the cnVatAmount
     */
    public String getCnVatAmount() {
        return cnVatAmount;
    }

    /**
     * <p>Setter method for cnVatAmount.</p>
     *
     * @param cnVatAmount Set for cnVatAmount
     */
    public void setCnVatAmount(String cnVatAmount) {
        this.cnVatAmount = cnVatAmount;
    }

    /**
     * <p>Getter method for cnTotalAmount.</p>
     *
     * @return the cnTotalAmount
     */
    public String getCnTotalAmount() {
        return cnTotalAmount;
    }

    /**
     * <p>Setter method for cnTotalAmount.</p>
     *
     * @param cnTotalAmount Set for cnTotalAmount
     */
    public void setCnTotalAmount(String cnTotalAmount) {
        this.cnTotalAmount = cnTotalAmount;
    }

    /**
     * <p>Getter method for invoiceCoverPage.</p>
     *
     * @return the invoiceCoverPage
     */
    public InputStream getInvoiceCoverPage() {
        return invoiceCoverPage;
    }

    /**
     * <p>Setter method for invoiceCoverPage.</p>
     *
     * @param invoiceCoverPage Set for invoiceCoverPage
     */
    public void setInvoiceCoverPage(InputStream invoiceCoverPage) {
        this.invoiceCoverPage = invoiceCoverPage;
    }

    /**
     * <p>Getter method for csvResult.</p>
     *
     * @return the csvResult
     */
    public StringBuffer getCsvResult() {
        return csvResult;
    }

    /**
     * <p>Setter method for csvResult.</p>
     *
     * @param csvResult Set for csvResult
     */
    public void setCsvResult(StringBuffer csvResult) {
        this.csvResult = csvResult;
    }

    /**
     * <p>Getter method for warningMessage.</p>
     *
     * @return the warningMessage
     */
    public String getWarningMessage() {
        return warningMessage;
    }

    /**
     * <p>Setter method for warningMessage.</p>
     *
     * @param warningMessage Set for warningMessage
     */
    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    /**
     * <p>Getter method for fileId.</p>
     *
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Setter method for fileId.</p>
     *
     * @param fileId Set for fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    /**
     * <p>Getter method for diffDPcdFlag.</p>
     *
     * @return the diffDPcdFlag
     */
    public String getDiffDPcdFlag() {
        return diffDPcdFlag;
    }

    /**
     * <p>Setter method for diffDPcdFlag.</p>
     *
     * @param diffDPcdFlag Set for diffDPcdFlag
     */
    public void setDiffDPcdFlag(String diffDPcdFlag) {
        this.diffDPcdFlag = diffDPcdFlag;
    }

    /**
     * <p>Getter method for asnNoRegister.</p>
     *
     * @return the asnNoRegister
     */
    public Set<String> getAsnNoRegister() {
        return asnNoRegister;
    }

    /**
     * <p>Setter method for asnNoRegister.</p>
     *
     * @param asnNoRegister Set for asnNoRegister
     */
    public void setAsnNoRegister(Set<String> asnNoRegister) {
        this.asnNoRegister = asnNoRegister;
    }

    /**
     * <p>Getter method for asnDetailList.</p>
     *
     * @return the asnDetailList
     */
    public List<AsnDomain> getAsnDetailList() {
        return asnDetailList;
    }

    /**
     * <p>Setter method for asnDetailList.</p>
     *
     * @param asnDetailList Set for asnDetailList
     */
    public void setAsnDetailList(List<AsnDomain> asnDetailList) {
        this.asnDetailList = asnDetailList;
    }
    
    /**
     * Gets the success message list.
     * 
     * @return the success message list.
     */
    public List<ApplicationMessageDomain> getSuccessMessageList() {
        return successMessageList;
    }

    /**
     * Sets the success message list.
     * 
     * @param successMessageList the success message list.
     */
    public void setSuccessMessageList(List<ApplicationMessageDomain> successMessageList) {
        this.successMessageList = successMessageList;
    }
    

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }
}