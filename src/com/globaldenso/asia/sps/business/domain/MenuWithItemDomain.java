/*
 * ModifyDate Development company Describe 
 * 2014/07/15 Akat               Create   
 *
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;

/**
 * The Class MenuWithItemDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class MenuWithItemDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -2494198833034942785L;

    /** The domain for SPS_M_MENU. */
    private SpsMMenuDomain spsMMenuDomain;
    
    /** The list of domain for SPS_M_MENU(child menu). */
    private List<SpsMMenuDomain> spsMMenuDomainList;
    
    /**
     * The default constructor.
     * */
    public MenuWithItemDomain() {
        super();
        this.spsMMenuDomain = new SpsMMenuDomain();
        this.spsMMenuDomainList = new ArrayList<SpsMMenuDomain>();
    }

    /**
     * Get method for spsMMenuDomain.
     * @return the spsMMenuDomain
     */
    public SpsMMenuDomain getSpsMMenuDomain() {
        return spsMMenuDomain;
    }

    /**
     * Set method for spsMMenuDomain.
     * @param spsMMenuDomain the spsMMenuDomain to set
     */
    public void setSpsMMenuDomain(SpsMMenuDomain spsMMenuDomain) {
        this.spsMMenuDomain = spsMMenuDomain;
    }

    /**
     * Get method for spsMMenuDomainList.
     * @return the spsMMenuDomainList
     */
    public List<SpsMMenuDomain> getSpsMMenuDomainList() {
        return spsMMenuDomainList;
    }

    /**
     * Set method for spsMMenuDomainList.
     * @param spsMMenuDomainList the spsMMenuDomainList to set
     */
    public void setSpsMMenuDomainList(List<SpsMMenuDomain> spsMMenuDomainList) {
        this.spsMMenuDomainList = spsMMenuDomainList;
    }

}
