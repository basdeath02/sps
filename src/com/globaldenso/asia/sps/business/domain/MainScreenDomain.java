/*
 * ModifyDate Development company     Describe 
 * 2014/08/15 CSI Chatchai            Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;

/**
 * <p>
 * Main Screen Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class MainScreenDomain extends BaseDomain implements Serializable{

    /**
     * <p>
     * The Serial.
     * </p>
     */
    private static final long serialVersionUID = 8987569074240789504L;

    /**
     * <p>
     * SpsM Announce Message Domain List.
     * </p>
     */
    private List<SpsMAnnounceMessageDomain> spsMAnnounceMessageDomainList;

    // FIX : wrong dateformat
    /** To format effect start date and show in screen */
    private List<AnnounceMessageDomain> announceMessageDomainList;
    
    /**
     * <p>
     * Main Screen Result Domain List.
     * </p>
     */
    private List<MainScreenResultDomain> mainScreenResultDomain;

    /**
     * <p>
     * Task List Detail Domain List.
     * </p>
     */
    private List<TaskListDetailDomain> taskListDetailDomainList;

    /**
     * <p>
     * The Constructor.
     * </p>
     */
    public MainScreenDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for spsMAnnounceMessageDomainList.
     * </p>
     * 
     * @return the spsMAnnounceMessageDomainList
     */
    public List<SpsMAnnounceMessageDomain> getSpsMAnnounceMessageDomainList() {
        return spsMAnnounceMessageDomainList;
    }

    /**
     * <p>
     * Setter method for spsMAnnounceMessageDomainList.
     * </p>
     * 
     * @param spsMAnnounceMessageDomainList Set for
     *            spsMAnnounceMessageDomainList
     */
    public void setSpsMAnnounceMessageDomainList(
        List<SpsMAnnounceMessageDomain> spsMAnnounceMessageDomainList) {
        this.spsMAnnounceMessageDomainList = spsMAnnounceMessageDomainList;
    }

    /**
     * <p>
     * Getter method for mainScreenResultDomain.
     * </p>
     * 
     * @return the mainScreenResultDomain
     */
    public List<MainScreenResultDomain> getMainScreenResultDomain() {
        return mainScreenResultDomain;
    }

    /**
     * <p>
     * Setter method for mainScreenResultDomain.
     * </p>
     * 
     * @param mainScreenResultDomain Set for mainScreenResultDomain
     */
    public void setMainScreenResultDomain(
        List<MainScreenResultDomain> mainScreenResultDomain) {
        this.mainScreenResultDomain = mainScreenResultDomain;
    }

    /**
     * <p>
     * Getter method for taskListDetailDomainList.
     * </p>
     * 
     * @return the taskListDetailDomainList
     */
    public List<TaskListDetailDomain> getTaskListDetailDomainList() {
        return taskListDetailDomainList;
    }

    /**
     * <p>
     * Setter method for taskListDetailDomainList.
     * </p>
     * 
     * @param taskListDetailDomainList Set for taskListDetailDomainList
     */
    public void setTaskListDetailDomainList(
        List<TaskListDetailDomain> taskListDetailDomainList) {
        this.taskListDetailDomainList = taskListDetailDomainList;
    }

    /**
     * <p>Getter method for announceMessageDomainList.</p>
     *
     * @return the announceMessageDomainList
     */
    public List<AnnounceMessageDomain> getAnnounceMessageDomainList() {
        return announceMessageDomainList;
    }

    /**
     * <p>Setter method for announceMessageDomainList.</p>
     *
     * @param announceMessageDomainList Set for announceMessageDomainList
     */
    public void setAnnounceMessageDomainList(
        List<AnnounceMessageDomain> announceMessageDomainList) {
        this.announceMessageDomainList = announceMessageDomainList;
    }

}
