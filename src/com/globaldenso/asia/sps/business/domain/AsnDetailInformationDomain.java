/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class Asn Detail Information Domain.
 * @author CSI
 */
public class AsnDetailInformationDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3124388929526505074L;
     
    /** The asnNo. */
    private String asnNo;
    
    /** The asn status. */
    private String asnStatus;
    
    /** The company denso code. */
    private String dCd;
    
    /** The company denso plant code. */
    private String dPcd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The company supplier plant code. */
    private String sPcd;
    
    /** The file id. */
    private String fileId;
    
    /** The actual etd from. */
    private String actualEtdFrom;
    
    /** The actual etd to. */
    private String actualEtdTo;
    
    /** The plan eta from. */
    private String planEtaFrom;
    
    /** The plan eta to. */
    private String planEtaTo;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The trans. */
    private String trans;
    
    /** The StringBuffer to create CSV file. */
    private StringBuffer csvBuffer;
    
    /** The data scope control domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The trans list. */
    private List<MiscellaneousDomain> transList;
    
    /** The asn status list. */
    private List<MiscellaneousDomain> asnStatusList;
    
    /** The List of Company Supplier. */
    private List<CompanySupplierDomain> companySupplierList;

    /** The List of Company Denso. */
    private List<CompanyDensoDomain> companyDensoList;

    /** The List of Plant Denso. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of Plant Supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
     
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessgeList; 
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The asn information list. */
    private List<AsnDetailInformationReturnDomain> asnInformationList;
    
    /**
     * Instantiates a new ASN info domain.
     */
    public AsnDetailInformationDomain() {
        super();
    }

    /**
     * Gets the asn no.
     * 
     * @return the asn no
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Sets the asn no.
     * 
     * @param asnNo the asn no
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Gets the asn status.
     * 
     * @return the asn status
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * Sets the asn status.
     * 
     * @param asnStatus the asn status
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * Gets the company denso code.
     * 
     * @return the company denso code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the company denso code.
     * 
     * @param dCd the company denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * Gets the company denso plant code.
     * 
     * @return the company denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the company denso plant code.
     * 
     * @param dPcd the company denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * Gets the vendor code.
     * 
     * @return the vendor code
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Sets the vendor code.
     * 
     * @param vendorCd the vendor code
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Gets the company supplier plant code.
     * 
     * @return the company supplier plant code
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the company supplier plant code.
     * 
     * @param sPcd the company supplier plant code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the file id.
     * 
     * @return the file id
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Sets the file id.
     * 
     * @param fileId the file id
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * Gets the actual etd from.
     * 
     * @return the actual etd from
     */
    public String getActualEtdFrom() {
        return actualEtdFrom;
    }

    /**
     * Sets the actual etd from.
     * 
     * @param actualEtdFrom the actual etd from
     */
    public void setActualEtdFrom(String actualEtdFrom) {
        this.actualEtdFrom = actualEtdFrom;
    }

    /**
     * Gets the actual etd to.
     * 
     * @return the actual etd to
     */
    public String getActualEtdTo() {
        return actualEtdTo;
    }

    /**
     * Sets the actual etd to.
     * 
     * @param actualEtdTo the actual etd to
     */
    public void setActualEtdTo(String actualEtdTo) {
        this.actualEtdTo = actualEtdTo;
    }

    /**
     * Gets the plant eta from.
     * 
     * @return the plant eta from
     */
    public String getPlanEtaFrom() {
        return planEtaFrom;
    }

    /**
     * Sets the plant eta from.
     * 
     * @param planEtaFrom the plant eta from
     */
    public void setPlanEtaFrom(String planEtaFrom) {
        this.planEtaFrom = planEtaFrom;
    }

    /**
     * Gets the plant eta to.
     * 
     * @return the plant eta to
     */
    public String getPlanEtaTo() {
        return planEtaTo;
    }

    /**
     * Sets the plant eta to.
     * 
     * @param planEtaTo the plant eta to
     */
    public void setPlanEtaTo(String planEtaTo) {
        this.planEtaTo = planEtaTo;
    }

    /**
     * Gets the sps do no.
     * 
     * @return the sps do no
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Sets the sps do no.
     * 
     * @param spsDoNo the sps do no
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Gets the trans.
     * 
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * Sets the trans.
     * 
     * @param trans the trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }
    
    /**
     * <p>Getter method for csvBufffer.</p>
     *
     * @return the csvBufffer
     */
    public StringBuffer getCsvBuffer() {
        return csvBuffer;
    }

    /**
     * <p>Setter method for csvBuffer.</p>
     *
     * @param csvBuffer Set for csvBuffer
     */
    public void setCsvBuffer(StringBuffer csvBuffer) {
        this.csvBuffer = csvBuffer;
    }
    
    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain
     */
    public void setDataScopeControlDomain(DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }

    /**
     * Gets the asn status list.
     * 
     * @return the asn status list
     */
    public List<MiscellaneousDomain> getAsnStatusList() {
        return asnStatusList;
    }

    /**
     * Sets the asn status list.
     * 
     * @param asnStatusList the asn status list
     */
    public void setAsnStatusList(List<MiscellaneousDomain> asnStatusList) {
        this.asnStatusList = asnStatusList;
    }

    /**
     * Gets the supplier company code list.
     * 
     * @return the supplier company code list
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the company supplier code list.
     * 
     * @param companySupplierList the company supplier code list
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the company denso list.
     * 
     * @return the company denso list
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso company list.
     * 
     * @param companyDensoList the denso company list
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * Gets the plant denso list.
     * 
     * @return the plant denso list
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the plant denso list.
     * 
     * @param plantDensoList the plant denso list
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the plant supplier list.
     * 
     * @return the plant supplier list
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the plant supplier list.
     * 
     * @param plantSupplierList the plant supplier list
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * Gets the trans list.
     * 
     * @return the trans list
     */
    public List<MiscellaneousDomain> getTransList() {
        return transList;
    }

    /**
     * Sets the trans list.
     * 
     * @param transList the trans list
     */
    public void setTransList(List<MiscellaneousDomain> transList) {
        this.transList = transList;
    }

    /**
     * Gets the error message list.
     * 
     * @return the error message list
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list
     */
    public List<ApplicationMessageDomain> getWarningMessgeList() {
        return warningMessgeList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessgeList the warning message list
     */
    public void setWarningMessgeList(List<ApplicationMessageDomain> warningMessgeList) {
        this.warningMessgeList = warningMessgeList;
    }
    
    /**
     * Gets the supplier authen list.
     * 
     * @return the supplier authen list
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * Sets the supplier authen list.
     * 
     * @param supplierAuthenList the supplier authen list
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * Gets the denso authen list.
     * 
     * @return the denso authen list
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * Sets the denso authen list.
     * 
     * @param densoAuthenList the denso authen list
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Gets the asn information list.
     * 
     * @return the asn information list
     */
    public List<AsnDetailInformationReturnDomain> getAsnInformationList() {
        return asnInformationList;
    }

    /**
     * Sets the asn information list.
     * 
     * @param asnInformationList the asn information list
     */
    public void setAsnInformationList(List<AsnDetailInformationReturnDomain> asnInformationList) {
        this.asnInformationList = asnInformationList;
    }
    
}