package com.globaldenso.asia.sps.business.domain;

import java.io.InputStream;
import java.io.Serializable;

/**
 * The Class One Way Kanban Tag Report Domain.
 * @author Netband
 */
public class OneWayKanbanTagReportDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3124388929526505074L;
    
    /** The printDate. */
    private String printDate;
    
    /** The tagSeqNo. */
    private String tagSeqNo;
    
    /** The numberOfBox. */
    private String numberOfBox;
    
    /** The dCd. */
    private String dCd;
    
    /** The dPCd. */
    private String dPCd;
    
    /** The dPn. */
    private String dPn;
    
    /** The cPn. */
    private String cPn;
    
    /** The itemDesc. */
    private String itemDesc;
    
    /** The remark1. */
    private String remark1;
    
    /** The remark2. */
    private String remark2;
    
    /** The remark3. */
    private String remark3;
    
    /** The kanbanType. */
    private String kanbanType;
    
    /** The qrContent. */
    private String qrContent;
    
    /** The qrContent. */
    private InputStream qrContentInputStream;
    
    /** The whPrimeReceiving. */
    private String whPrimeReceiving;
    
    /** The whLocation. */
    private String whLocation;
    
    /** The qty. */
    private String qty;
    
    /** The currentOrderQty. */
    private String rcvLane;
    
    /** The sCd. */
    private String sCd;
    
    /** The sName. */
    private String sName;
    
    /** The sPn. */
    private String sPn;
    
    /** The currentCigmaDoNo. */
    private String currentCigmaDoNo;
    
    /** The shipDateTime. */
    private String shipDateTime;
    
    /** The deliveryDateTime. */
    private String deliveryDateTime;
    
    /** The partialFlag. */
    private String partialFlag;
    
    /** The oneWayKanbanTagPrintDate. */
    private String oneWayKanbanTagPrintDate;
    
    /** The ctrlNo. */
    private String ctrlNo;
    
    /**
     * Constructor OneWayKanbanTagReportDomain.
     */
    public OneWayKanbanTagReportDomain(){
    }
    
    /** The sName. */
    private String dName;
    
    /**
     * Gets printDate.
     * 
     * @return the printDate
     */
    public String getPrintDate() {
        return printDate;
    }
    
    /**
     * Sets the printDate.
     * 
     * @param printDate the printDate
     */
    public void setPrintDate(String printDate) {
        this.printDate = printDate;
    }

    /**
     * Gets numberOfBox.
     * 
     * @return the numberOfBox
     */
    public String getNumberOfBox() {
        return numberOfBox;
    }

    /**
     * Sets the numberOfBox.
     * 
     * @param numberOfBox the numberOfBox
     */
    public void setNumberOfBox(String numberOfBox) {
        this.numberOfBox = numberOfBox;
    }

    /**
     * Gets dCd.
     * 
     * @return the dCd
     */
    public String getdCd() {
        return dCd;
    }

    /**
     * Sets the dCd.
     * 
     * @param dCd the dCd
     */
    public void setdCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets dPCd.
     * 
     * @return the dPCd
     */
    public String getdPCd() {
        return dPCd;
    }

    /**
     * Sets the dPCd.
     * 
     * @param dPCd the dPCd
     */
    public void setdPCd(String dPCd) {
        this.dPCd = dPCd;
    }

    /**
     * Gets dPn.
     * 
     * @return the dPn
     */
    public String getdPn() {
        return dPn;
    }

    /**
     * Sets the dPn.
     * 
     * @param dPn the dPn
     */
    public void setdPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Gets cPn.
     * 
     * @return the cPn
     */
    public String getcPn() {
        return cPn;
    }

    /**
     * Sets the cPn.
     * 
     * @param cPn the cPn
     */
    public void setcPn(String cPn) {
        this.cPn = cPn;
    }

    /**
     * Gets itemDesc.
     * 
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Sets the itemDesc.
     * 
     * @param itemDesc the itemDesc
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * Gets remark1.
     * 
     * @return the remark1
     */
    public String getRemark1() {
        return remark1;
    }

    /**
     * Sets the remark1.
     * 
     * @param remark1 the remark1
     */
    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    /**
     * Gets remark2.
     * 
     * @return the remark2
     */
    public String getRemark2() {
        return remark2;
    }

    /**
     * Sets the remark2.
     * 
     * @param remark2 the remark2
     */
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    /**
     * Gets remark3.
     * 
     * @return the remark3
     */
    public String getRemark3() {
        return remark3;
    }

    /**
     * Sets the remark3.
     * 
     * @param remark3 the remark3
     */
    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }
    /**
     * Gets kanbanType.
     * 
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }
    /**
     * Sets the kanbanType.
     * 
     * @param kanbanType the kanbanType
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }
    /**
     * Gets qrContent.
     * 
     * @return the qrContent
     */
    public String getQrContent() {
        return qrContent;
    }
    /**
     * Sets the qrContent.
     * 
     * @param qrContent the qrContent
     */
    public void setQrContent(String qrContent) {
        this.qrContent = qrContent;
    }
    /**
     * Gets whPrimeReceiving.
     * 
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }
    /**
     * Sets the whPrimeReceiving.
     * 
     * @param whPrimeReceiving the whPrimeReceiving
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }
    /**
     * Gets qty.
     * 
     * @return the qty
     */
    public String getQty() {
        return qty;
    }
    /**
     * Sets the qty.
     * 
     * @param qty the qty
     */
    public void setQty(String qty) {
        this.qty = qty;
    }
    /**
     * Gets rcvLane.
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }
    /**
     * Sets the rcvLane.
     * 
     * @param rcvLane the rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }
    /**
     * Gets sCd.
     * 
     * @return the sCd
     */
    public String getsCd() {
        return sCd;
    }
    /**
     * Sets the sCd.
     * 
     * @param sCd the sCd
     */
    public void setsCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Gets sName.
     * 
     * @return the sName
     */
    public String getsName() {
        return sName;
    }
    /**
     * Sets the sName.
     * 
     * @param sName the sName
     */
    public void setsName(String sName) {
        this.sName = sName;
    }
    /**
     * Gets sPn.
     * 
     * @return the sPn
     */
    public String getsPn() {
        return sPn;
    }
    /**
     * Sets the sPn.
     * 
     * @param sPn the sPn
     */
    public void setsPn(String sPn) {
        this.sPn = sPn;
    }
    /**
     * Gets tagSeqNo.
     * 
     * @return the tagSeqNo
     */

    public String getTagSeqNo() {
        return tagSeqNo;
    }
    /**
     * Sets the tagSeqNo.
     * 
     * @param tagSeqNo the tagSeqNo
     */

    public void setTagSeqNo(String tagSeqNo) {
        this.tagSeqNo = tagSeqNo;
    }
    /**
     * Gets qrContentInputStream.
     * 
     * @return the qrContentInputStream
     */

    public InputStream getQrContentInputStream() {
        return qrContentInputStream;
    }

    /**
     * Sets the qrContentInputStream.
     * 
     * @param qrContentInputStream the qrContentInputStream
     */
    public void setQrContentInputStream(InputStream qrContentInputStream) {
        this.qrContentInputStream = qrContentInputStream;
    }
    /**
     * Gets currentCigmaDoNo.
     * 
     * @return the currentCigmaDoNo
     */

    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }
    /**
     * Sets the currentCigmaDoNo.
     * 
     * @param currentCigmaDoNo the currentCigmaDoNo
     */

    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }
    /**
     * Gets shipDateTime.
     * 
     * @return the shipDateTime
     */

    public String getShipDateTime() {
        return shipDateTime;
    }
    /**
     * Sets the shipDateTime.
     * 
     * @param shipDateTime the shipDateTime
     */

    public void setShipDateTime(String shipDateTime) {
        this.shipDateTime = shipDateTime;
    }
    /**
     * Gets deliveryDateTime.
     * 
     * @return the deliveryDateTime
     */

    public String getDeliveryDateTime() {
        return deliveryDateTime;
    }
    /**
     * Sets the deliveryDateTime.
     * 
     * @param deliveryDateTime the deliveryDateTime
     */

    public void setDeliveryDateTime(String deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }
    /**
     * Gets partialFlag.
     * 
     * @return the partialFlag
     */

    public String getPartialFlag() {
        return partialFlag;
    }
    /**
     * Sets the partialFlag.
     * 
     * @param partialFlag the partialFlag
     */

    public void setPartialFlag(String partialFlag) {
        this.partialFlag = partialFlag;
    }
    /**
     * Gets oneWayKanbanTagPrintDate.
     * 
     * @return the oneWayKanbanTagPrintDate
     */

    public String getOneWayKanbanTagPrintDate() {
        return oneWayKanbanTagPrintDate;
    }
    /**
     * Sets the oneWayKanbanTagPrintDate.
     * 
     * @param oneWayKanbanTagPrintDate the oneWayKanbanTagPrintDate
     */

    public void setOneWayKanbanTagPrintDate(String oneWayKanbanTagPrintDate) {
        this.oneWayKanbanTagPrintDate = oneWayKanbanTagPrintDate;
    }
    /**
     * Gets ctrlNo.
     * 
     * @return the ctrlNo
     */

    public String getCtrlNo() {
        return ctrlNo;
    }
    /**
     * Sets the ctrlNo.
     * 
     * @param ctrlNo the ctrlNo
     */

    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * <p>Getter method for whLocation.</p>
     *
     * @return the whLocation
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * <p>Setter method for whLocation.</p>
     *
     * @param whLocation Set for whLocation
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }
    
    /**
     * Gets dName.
     * 
     * @return the dName
     */
    public String getdName() {
        return dName;
    }
    /**
     * Sets the dName.
     * 
     * @param dName the dName
     */
    public void setdName(String dName) {
        this.dName = dName;
    }
}
