/*
 * ModifyDate Development company     Describe 
 * 2014/07/28 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * The class PurchaseOrderInformationResultDomain.
 * @author CSI
 * */
public class PurchaseOrderInformationResultDomain extends BaseDomain implements Serializable {

    /** The generated Serial Version UID. */
    private static final long serialVersionUID = 7362015938151428301L;
    
    /** The list of PurchaseOrderInformationDomain. */
    private List<PurchaseOrderInformationDomain> purchaseOrderInformationDomainList;
    
    /** The List of ApplicationMessage. */
    private Set<String> applicationMessageSet;
    
    /** The StringBuffer to create CSV file. */
    private StringBuffer csvBufffer;
    
    /** The default constructor. */
    public PurchaseOrderInformationResultDomain() {
        super();
    }

    /**
     * Get method for purchaseOrderInformationDomainList.
     * @return the purchaseOrderInformationDomainList
     */
    public List<PurchaseOrderInformationDomain> getPurchaseOrderInformationDomainList() {
        return purchaseOrderInformationDomainList;
    }

    /**
     * Set method for purchaseOrderInformationDomainList.
     * @param purchaseOrderInformationDomainList the purchaseOrderInformationDomainList to set
     */
    public void setPurchaseOrderInformationDomainList(
            List<PurchaseOrderInformationDomain> purchaseOrderInformationDomainList) {
        this.purchaseOrderInformationDomainList = purchaseOrderInformationDomainList;
    }

    /**
     * <p>Getter method for applicationMessageSet.</p>
     *
     * @return the applicationMessageSet
     */
    public Set<String> getApplicationMessageSet() {
        return applicationMessageSet;
    }

    /**
     * <p>Setter method for applicationMessageSet.</p>
     *
     * @param applicationMessageSet Set for applicationMessageSet
     */
    public void setApplicationMessageSet(Set<String> applicationMessageSet) {
        this.applicationMessageSet = applicationMessageSet;
    }

    /**
     * <p>Getter method for csvBufffer.</p>
     *
     * @return the csvBufffer
     */
    public StringBuffer getCsvBufffer() {
        return csvBufffer;
    }

    /**
     * <p>Setter method for csvBufffer.</p>
     *
     * @param csvBufffer Set for csvBufffer
     */
    public void setCsvBufffer(StringBuffer csvBufffer) {
        this.csvBufffer = csvBufffer;
    }
    
}
