/*
 * ModifyDate Development company     Describe 
 * 2014/03/13 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.Locale;

/**
 * Base domain for define property that will use in all Domain.
 */
public class BaseDomain implements Serializable {

    /** Generated Serial Version UID */
    private static final long serialVersionUID = -4037750410662038709L;

    /** The row number from. */
    private int rowNumFrom;

    /** The row number to. */
    private int rowNumTo;

    /** The row count. */
    private int rowCnt;

    /** The locale. */
    private Locale locale;

    /** The DSC-ID(user id for Siteminder). */
    private String dscId;
    
    /** The database name for SQLMap. */
    private String databaseName;
    
    /** The max row per page. */
    private int maxRowPerPage;
    
    /** The total count records. */
    private int totalCount;
    
    /** The current page number. */
    private int pageNumber;
    
    /** To set order by use in SACT. */
    private String preferredOrder;
    
    /** The Default constructor. */
    public BaseDomain() {
        
    }

    /**
     * <p>Getter method for row number from.</p>
     *
     * @return the row number from
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for row number from.</p>
     *
     * @param rowNumFrom Set for row number from
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for row number to.</p>
     *
     * @return the row number to
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for row number to.</p>
     *
     * @param rowNumTo Set for row number to
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }

    /**
     * <p>Getter method for row count.</p>
     *
     * @return the row count
     */
    public int getRowCnt() {
        return rowCnt;
    }

    /**
     * <p>Setter method for row count.</p>
     *
     * @param rowCnt Set for row count
     */
    public void setRowCnt(int rowCnt) {
        this.rowCnt = rowCnt;
    }

    /**
     * <p>Getter method for locale.</p>
     *
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * <p>Setter method for locale.</p>
     *
     * @param locale Set for locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * <p>Getter method for dscId.</p>
     *
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>Setter method for dscId.</p>
     *
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>Getter method for database name for SQLMap.</p>
     *
     * @return the database name for SQLMap
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * <p>Setter method for database name for SQLMap.</p>
     *
     * @param databaseName Set for database name for SQLMap
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * Get method for maxRowPerPage.
     * @return the maxRowPerPage
     */
    public int getMaxRowPerPage() {
        return maxRowPerPage;
    }

    /**
     * Set method for maxRowPerPage.
     * @param maxRowPerPage the maxRowPerPage to set
     */
    public void setMaxRowPerPage(int maxRowPerPage) {
        this.maxRowPerPage = maxRowPerPage;
    }

    /**
     * Get method for totalCount.
     * @return the totalCount
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * Set method for totalCount.
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * Get method for pageNumber.
     * @return the pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Set method for pageNumber.
     * @param pageNumber the pageNumber to set
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * <p>Getter method for preferredOrder.</p>
     *
     * @return the preferredOrder
     */
    public String getPreferredOrder() {
        return preferredOrder;
    }

    /**
     * <p>Setter method for preferredOrder.</p>
     *
     * @param preferredOrder Set for preferredOrder
     */
    public void setPreferredOrder(String preferredOrder) {
        this.preferredOrder = preferredOrder;
    }

}
