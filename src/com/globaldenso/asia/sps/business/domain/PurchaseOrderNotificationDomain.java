/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;

//import org.apache.tomcat.util.buf.TimeStamp;

/**
 * The Class Purchase Order Notification Domain.
 * @author CSI
 */
public class PurchaseOrderNotificationDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -4150661788300279345L;

    /** The Mark Pending Flag. */
    private String markPendingFlag;
    
    /** The Due Date in String type. */
    private String stringDueDate;
    
    /** The Order QTY in String type. */
    private String stringOrderQty;
    
    /** The Proposed Due Date in String type. */
    private String stringProposedDueDate;
    
    /** The Proposed QTY in String type. */
    private String stringProposedQty;
    
    /** The Pending Reason. */
    private String pendingReason;
    
    /** The PO Issue Date in String type. */
    private String stringPoIssueDate;
    
    /** The ETD in String type. */
    private String stringEtd;
    
    /** The domain for SPS_T_PO. */
    private SpsTPoDomain spsTPo;
    
    /** The domain for SPS_T_PO_DETAIL. */
    private SpsTPoDetailDomain spsTPoDetail;
    
    /** The domain for SPS_T_PO_DUE. */
    private SpsTPoDueDomain spsTPoDue;
    
    /**
     * Instantiates a new Purchase Order Acknowledge Domain.
     */
    public PurchaseOrderNotificationDomain() {
        super();
        this.spsTPo = new SpsTPoDomain();
        this.spsTPoDetail = new SpsTPoDetailDomain();
        this.spsTPoDue = new SpsTPoDueDomain();
    }

    /**
     * <p>Getter method for stringEtd.</p>
     *
     * @return the stringEtd
     */
    public String getStringEtd() {
        return stringEtd;
    }

    /**
     * <p>Setter method for stringEtd.</p>
     *
     * @param stringEtd Set for stringEtd
     */
    public void setStringEtd(String stringEtd) {
        this.stringEtd = stringEtd;
    }

    /**
     * <p>Getter method for stringPoIssueDate.</p>
     *
     * @return the stringPoIssueDate
     */
    public String getStringPoIssueDate() {
        return stringPoIssueDate;
    }

    /**
     * <p>Setter method for stringPoIssueDate.</p>
     *
     * @param stringPoIssueDate Set for stringPoIssueDate
     */
    public void setStringPoIssueDate(String stringPoIssueDate) {
        this.stringPoIssueDate = stringPoIssueDate;
    }

    /**
     * <p>Getter method for markPendingFlag.</p>
     *
     * @return the markPendingFlag
     */
    public String getMarkPendingFlag() {
        return markPendingFlag;
    }

    /**
     * <p>Setter method for markPendingFlag.</p>
     *
     * @param markPendingFlag Set for markPendingFlag
     */
    public void setMarkPendingFlag(String markPendingFlag) {
        this.markPendingFlag = markPendingFlag;
    }

    /**
     * <p>Getter method for stringDueDate.</p>
     *
     * @return the stringDueDate
     */
    public String getStringDueDate() {
        return stringDueDate;
    }

    /**
     * <p>Setter method for stringDueDate.</p>
     *
     * @param stringDueDate Set for stringDueDate
     */
    public void setStringDueDate(String stringDueDate) {
        this.stringDueDate = stringDueDate;
    }

    /**
     * <p>Getter method for stringOrderQty.</p>
     *
     * @return the stringOrderQty
     */
    public String getStringOrderQty() {
        return stringOrderQty;
    }

    /**
     * <p>Setter method for stringOrderQty.</p>
     *
     * @param stringOrderQty Set for stringOrderQty
     */
    public void setStringOrderQty(String stringOrderQty) {
        this.stringOrderQty = stringOrderQty;
    }

    /**
     * <p>Getter method for stringProposedDueDate.</p>
     *
     * @return the stringProposedDueDate
     */
    public String getStringProposedDueDate() {
        return stringProposedDueDate;
    }

    /**
     * <p>Setter method for stringProposedDueDate.</p>
     *
     * @param stringProposedDueDate Set for stringProposedDueDate
     */
    public void setStringProposedDueDate(String stringProposedDueDate) {
        this.stringProposedDueDate = stringProposedDueDate;
    }

    /**
     * <p>Getter method for stringProposedQty.</p>
     *
     * @return the stringProposedQty
     */
    public String getStringProposedQty() {
        return stringProposedQty;
    }

    /**
     * <p>Setter method for stringProposedQty.</p>
     *
     * @param stringProposedQty Set for stringProposedQty
     */
    public void setStringProposedQty(String stringProposedQty) {
        this.stringProposedQty = stringProposedQty;
    }

    /**
     * <p>Getter method for pendingReason.</p>
     *
     * @return the pendingReason
     */
    public String getPendingReason() {
        return pendingReason;
    }

    /**
     * <p>Setter method for pendingReason.</p>
     *
     * @param pendingReason Set for pendingReason
     */
    public void setPendingReason(String pendingReason) {
        this.pendingReason = pendingReason;
    }

    /**
     * <p>Getter method for spsTPo.</p>
     *
     * @return the spsTPo
     */
    public SpsTPoDomain getSpsTPo() {
        return spsTPo;
    }

    /**
     * <p>Setter method for spsTPo.</p>
     *
     * @param spsTPo Set for spsTPo
     */
    public void setSpsTPo(SpsTPoDomain spsTPo) {
        this.spsTPo = spsTPo;
    }

    /**
     * <p>Getter method for spsTPoDetail.</p>
     *
     * @return the spsTPoDetail
     */
    public SpsTPoDetailDomain getSpsTPoDetail() {
        return spsTPoDetail;
    }

    /**
     * <p>Setter method for spsTPoDetail.</p>
     *
     * @param spsTPoDetail Set for spsTPoDetail
     */
    public void setSpsTPoDetail(SpsTPoDetailDomain spsTPoDetail) {
        this.spsTPoDetail = spsTPoDetail;
    }

    /**
     * <p>Getter method for spsTPoDue.</p>
     *
     * @return the spsTPoDue
     */
    public SpsTPoDueDomain getSpsTPoDue() {
        return spsTPoDue;
    }

    /**
     * <p>Setter method for spsTPoDue.</p>
     *
     * @param spsTPoDue Set for spsTPoDue
     */
    public void setSpsTPoDue(SpsTPoDueDomain spsTPoDue) {
        this.spsTPoDue = spsTPoDue;
    }

}