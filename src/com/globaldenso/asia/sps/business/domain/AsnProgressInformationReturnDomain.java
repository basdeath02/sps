/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Parichat            Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * The Class ASN Progress Information Return Domain.
 * @author CSI
 */
public class AsnProgressInformationReturnDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 844802002003767261L;
    
    /** The No. */
    private String no;
    
    /** The ASN No.*/
    private String asnNo;
    
    /** The D.CD*/
    private String dCd;
    
    /** The D.P.CD*/
    private String dPcd;
    
    /** The D. Part No.*/
    private String dPn;
    
    /** The U/M*/
    private String um;
    
    /** The Shipping QTY*/
    private BigDecimal shippingQty;
    
    /** The Shipping Box QTY*/
    private BigDecimal shippingBoxQty;
    
    /** The Shipping QTY String*/
    private String shippingQtyStr;
    
    /** The Actual ETA*/
    private Timestamp actualEta;
    
    /** The Actual ETD*/
    private Timestamp actualEtd;
    
    /** The Plan ETA*/
    private Timestamp planEta;
    
    /** The Plan ETD*/
    private Timestamp planEtd;
    
    /** The Receiving Date*/
    private String receivingDate;
    
    /** The Receiving QTY*/
    private BigDecimal receivedQty;
    
    /** The Receiving QTY String*/
    private String receivedQtyStr;
    
    /** The PN RCV Status*/
    private String pnRcvStatus;
    
    /** The ASN Status*/
    private String asnStatus;
    
    /** The Invoice No.*/
    private String invoiceNo;
    
    /** The Invoice Status.*/
    private String invoiceStatus;
    
    /** The Invoice Date*/
    private Date invoiceDate;
    
    // FIX : wrong dateformat
    /** The formated Invoice Date to show in screen. */
    private String invoiceDateShow;
    
    /** The S.CD*/
    private String sCd;
    
    /** The Vendor.CD. */
    private String vendorCd;
    
    /** The S.P.CD*/
    private String sPcd;
    
    /** The S. Part No.*/
    private String sPn;
    
    /** The S. Part No.*/
    private String invoiceId;
    
    /** The S. Part No.*/
    private String cnDate;
    
    /** The S. Part No.*/
    private String cnNo;
    
    /** The timestamp.*/
    private Timestamp currentTimeStamp;
    
    /** The do id.*/
    private String doId;
    
    /** The S. Part No.*/
    private String invRcvSta;
    
    /** The create date time.*/
    private Timestamp createDateTime;
    
    /** The last update date time.*/
    private Timestamp lastUpdateDateTime;
    
    /** The no. of Boxes*/
    private BigDecimal noOfBoxes;
    
    /** The number of pallet*/
    private BigDecimal numberOfPallet;
    
    /** The sps do no.*/
    private String spsDoNo;
    
    /** The s tax id.*/
    private String sTaxId;
    
    /** The pdf file id.*/
    private String pdfFileId;
    
    /** The revision.*/
    private String revision;
    
    /** The trans.*/
    private String trans;
    
    /** The trip no.*/
    private String tripNo;
    
    /** The truck route.*/
    private String truckRoute;

    /** The truck sequence.*/
    private BigDecimal truckSeq;
    
    /** The utc.*/
    private String utc;
    
    /** The supplierCodeAuthen*/
    private String sCdAuthen;
    
    /** The supplierPlantAuthen*/
    private String sPcdAuthen;
    
    /** The densoCodeAuthen*/
    private String dCdAuthen;
    
    /** The densoPlantAuthen*/
    private String dPcdAuthen;
    
    /** The chgCigmaDoNo*/
    private String chgCigmaDoNo;
    
    /** The cigmaDoNo*/
    private String cigmaDoNo;
    
    /** The actual etd str*/
    private String actualEtdStr;
    
    /** The actual eta str*/
    private String actualEtaStr;
    
    /** The change reason cd*/
    private String changeReasonCd;
    
    /** The revise shipping qty flag*/
    private String reviseShippingQtyFlag;
    
    /** The current sps do no*/
    private String currentSpsDoNo;
    
    /** The current cigma do no*/
    private String currentCigmaDoNo;
    
    /** The order method*/
    private String orderMethod;
    
    /** The dock code*/
    private String dockCode;
    
    /** The cycle*/
    private String cycle;
    
    /** The wh prime receiving*/
    private String whPrimeReceiving;
    
    /** The ctrl no*/
    private String ctrlNo;
    
    /** The item desc*/
    private String itemDesc;
    
    /** The rcv lane*/
    private String rcvLane;
    
    /** The kanban seq no*/
    private String kanbanSeqNo;
    
    /**
     * Instantiates a new ASN Progress Information Return domain.
     */
    public AsnProgressInformationReturnDomain() {
        super();
    }

    /**
     * <p>Getter method for no.</p>
     *
     * @return the no
     */
    public String getNo() {
        return no;
    }


    /**
     * <p>Setter method for no.</p>
     *
     * @param no Set for no
     */
    public void setNo(String no) {
        this.no = no;
    }


    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }


    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }


    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }


    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }


    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }


    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }


    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }


    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }


    /**
     * <p>Getter method for um.</p>
     *
     * @return the um
     */
    public String getUm() {
        return um;
    }


    /**
     * <p>Setter method for um.</p>
     *
     * @param um Set for um
     */
    public void setUm(String um) {
        this.um = um;
    }


    /**
     * <p>Getter method for shippingQty.</p>
     *
     * @return the shippingQty
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * <p>Setter method for shippingQty.</p>
     *
     * @param shippingQty Set for shippingQty
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }
    
    /**
     * <p>Getter method for shippingBoxQty.</p>
     *
     * @return the shippingBoxQty
     */
    public BigDecimal getShippingBoxQty() {
        return shippingBoxQty;
    }

    /**
     * <p>Setter method for shippingBoxQty.</p>
     *
     * @param shippingBoxQty Set for shippingBoxQty
     */
    public void setShippingBoxQty(BigDecimal shippingBoxQty) {
        this.shippingBoxQty = shippingBoxQty;
    }

    /**
     * <p>Getter method for shippingQtyStr.</p>
     *
     * @return the shippingQtyStr
     */
    public String getShippingQtyStr() {
        return shippingQtyStr;
    }

    /**
     * <p>Setter method for shippingQtyStr.</p>
     *
     * @param shippingQtyStr Set for shippingQtyStr
     */
    public void setShippingQtyStr(String shippingQtyStr) {
        this.shippingQtyStr = shippingQtyStr;
    }

    /**
     * <p>Getter method for actualEtd.</p>
     *
     * @return the actualEtd
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * <p>Setter method for actualEtd.</p>
     *
     * @param actualEtd Set for actualEtd
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }
    
    /**
     * <p>Getter method for actualEta.</p>
     *
     * @return the actualEta
     */
    public Timestamp getActualEta() {
        return actualEta;
    }

    /**
     * <p>Setter method for actualEta.</p>
     *
     * @param actualEta Set for actualEta
     */
    public void setActualEta(Timestamp actualEta) {
        this.actualEta = actualEta;
    }
    
    /**
     * <p>Getter method for plan eta.</p>
     *
     * @return the plan eta
     */
    public Timestamp getPlanEta() {
        return planEta;
    }
    
    /**
     * <p>Setter method for plan eta.</p>
     *
     * @param planEta Set for plan eta
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }
    
    /**
     * <p>Getter method for plan etd.</p>
     *
     * @return the plan etd
     */
    public Timestamp getPlanEtd() {
        return planEtd;
    }

    /**
     * <p>Setter method for plan etd.</p>
     *
     * @param planEtd Set for plan etd
     */
    public void setPlanEtd(Timestamp planEtd) {
        this.planEtd = planEtd;
    }
    
    /**
     * <p>Getter method for receivingDate.</p>
     *
     * @return the receivingDate
     */
    public String getReceivingDate() {
        return receivingDate;
    }

    /**
     * <p>Setter method for receivingDate.</p>
     *
     * @param receivingDate Set for receivingDate
     */
    public void setReceivingDate(String receivingDate) {
        this.receivingDate = receivingDate;
    }

    /**
     * <p>Getter method for receivedQty.</p>
     *
     * @return the receivedQty
     */
    public BigDecimal getReceivedQty() {
        return receivedQty;
    }

    /**
     * <p>Setter method for receivedQty.</p>
     *
     * @param receivedQty Set for receivedQty
     */
    public void setReceivedQty(BigDecimal receivedQty) {
        this.receivedQty = receivedQty;
    }

    /**
     * <p>Getter method for receivedQtyStr.</p>
     *
     * @return the receivedQtyStr
     */
    public String getReceivedQtyStr() {
        return receivedQtyStr;
    }

    /**
     * <p>Setter method for receivedQtyStr.</p>
     *
     * @param receivedQtyStr Set for receivedQtyStr
     */
    public void setReceivedQtyStr(String receivedQtyStr) {
        this.receivedQtyStr = receivedQtyStr;
    }

    /**
     * <p>Getter method for pnRcvStatus.</p>
     *
     * @return the pnRcvStatus
     */
    public String getPnRcvStatus() {
        return pnRcvStatus;
    }

    /**
     * <p>Setter method for pnRcvStatus.</p>
     *
     * @param pnRcvStatus Set for pnRcvStatus
     */
    public void setPnRcvStatus(String pnRcvStatus) {
        this.pnRcvStatus = pnRcvStatus;
    }


    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }


    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }


    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }


    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
    
    
    /**
     * <p>Getter method for invoiceStatus.</p>
     *
     * @return the invoiceStatus
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * <p>Setter method for invoiceStatus.</p>
     *
     * @param invoiceStatus Set for invoiceStatus
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * <p>Getter method for invoiceDate.</p>
     *
     * @return the invoiceDate
     */
    public Date getInvoiceDate() {
        return invoiceDate;
    }


    /**
     * <p>Setter method for invoiceDate.</p>
     *
     * @param invoiceDate Set for invoiceDate
     */
    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }


    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }


    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }


    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }


    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }


    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }


    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }


    /**
     * <p>Getter method for sCdAuthen.</p>
     *
     * @return the sCdAuthen
     */
    public String getSCdAuthen() {
        return sCdAuthen;
    }


    /**
     * <p>Setter method for sCdAuthen.</p>
     *
     * @param sCdAuthen Set for sCdAuthen
     */
    public void setSCdAuthen(String sCdAuthen) {
        this.sCdAuthen = sCdAuthen;
    }


    /**
     * <p>Getter method for sPcdAuthen.</p>
     *
     * @return the sPcdAuthen
     */
    public String getSPcdAuthen() {
        return sPcdAuthen;
    }


    /**
     * <p>Setter method for sPcdAuthen.</p>
     *
     * @param sPcdAuthen Set for sPcdAuthen
     */
    public void setSPcdAuthen(String sPcdAuthen) {
        this.sPcdAuthen = sPcdAuthen;
    }


    /**
     * <p>Getter method for densoCodeAuthen.</p>
     *
     * @return the densoCodeAuthen
     */
    public String getDCdAuthen() {
        return dCdAuthen;
    }


    /**
     * <p>Setter method for dCdAuthen.</p>
     *
     * @param dCdAuthen Set for dCdAuthen
     */
    public void setDCdAuthen(String dCdAuthen) {
        this.dCdAuthen = dCdAuthen;
    }


    /**
     * <p>Getter method for dPcdAuthen.</p>
     *
     * @return the dPcdAuthen
     */
    public String getDPcdAuthen() {
        return dPcdAuthen;
    }


    /**
     * <p>Setter method for dPcdAuthen.</p>
     *
     * @param dPcdAuthen Set for dPcdAuthen
     */
    public void setDPcdAuthen(String dPcdAuthen) {
        this.dPcdAuthen = dPcdAuthen;
    }


    /**
     * <p>Getter method for invoiceId.</p>
     *
     * @return the invoiceId
     */
    public String getInvoiceId() {
        return invoiceId;
    }


    /**
     * <p>Setter method for invoiceId.</p>
     *
     * @param invoiceId Set for invoiceId
     */
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }


    /**
     * <p>Getter method for cnDate.</p>
     *
     * @return the cnDate
     */
    public String getCnDate() {
        return cnDate;
    }


    /**
     * <p>Setter method for cnDate.</p>
     *
     * @param cnDate Set for cnDate
     */
    public void setCnDate(String cnDate) {
        this.cnDate = cnDate;
    }


    /**
     * <p>Getter method for invRcvSta.</p>
     *
     * @return the invRcvSta
     */
    public String getInvRcvSta() {
        return invRcvSta;
    }


    /**
     * <p>Setter method for invRcvSta.</p>
     *
     * @param invRcvSta Set for invRcvSta
     */
    public void setInvRcvSta(String invRcvSta) {
        this.invRcvSta = invRcvSta;
    }
    
    /**
     * <p>Getter method for last update datetime.</p>
     *
     * @return the last update datetime
     */
    public Timestamp getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    /**
     * <p>Setter method for last update datetime.</p>
     *
     * @param lastUpdateDateTime Set for last update datetime
     */
    public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }
    
    /**
     * Getter method of "noOfBoxes"
     * 
     * @return the noOfBoxes
     */
    public BigDecimal getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * Setter method of "noOfBoxes"
     * 
     * @param noOfBoxes Set in "noOfBoxes".
     */
    public void setNoOfBoxes(BigDecimal noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }
    
    /**
     * Getter method of "numberOfPallet"
     * 
     * @return the numberOfPallet
     */
    public BigDecimal getNumberOfPallet() {
        return numberOfPallet;
    }

    /**
     * Setter method of "numberOfPallet"
     * 
     * @param numberOfPallet Set in "numberOfPallet".
     */
    public void setNumberOfPallet(BigDecimal numberOfPallet) {
        this.numberOfPallet = numberOfPallet;
    }

    /**
     * <p>Getter method for cnNo.</p>
     *
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * <p>Setter method for cnNo.</p>
     *
     * @param cnNo Set for cnNo
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }
    
    /**
     * <p>Getter method for current time stamp.</p>
     *
     * @return the current time stamp
     */
    public Timestamp getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    /**
     * <p>Setter method for current time stamp.</p>
     *
     * @param currentTimeStamp Set for current time stamp
     */
    public void setCurrentTimeStamp(Timestamp currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }

    /**
     * <p>Getter method for do id.</p>
     *
     * @return the do id
     */
    public String getDoId() {
        return doId;
    }

    /**
     * <p>Setter method for do id.</p>
     *
     * @param doId Set for do id
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * <p>Getter method for sps do no.</p>
     *
     * @return the sps do no
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>Setter method for sps do no.</p>
     *
     * @param spsDoNo Set for sps do no
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }
    
    /**
     * <p>Getter method for supplier tax id.</p>
     *
     * @return the supplier tax id
     */
    public String getsTaxId() {
        return sTaxId;
    }

    /**
     * <p>Setter method for supplier tax id.</p>
     *
     * @param sTaxId Set for supplier tax id
     */
    public void setsTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * <p>Getter method for pdf file id.</p>
     *
     * @return the pdf file id
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>Setter method for pdf file id.</p>
     *
     * @param pdfFileId Set for pdf file id
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }
    
    /**
     * <p>Getter method of revision.</p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>Setter method of revision.</p>
     * 
     * @param revision Set in revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }
    
    /**
     * <p>Getter method for trans.</p>
     *
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * <p>Setter method for trans.</p>
     *
     * @param trans Set for trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * <p>Getter method for trip no.</p>
     *
     * @return the trip no
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * <p>Setter method for trip no.</p>
     *
     * @param tripNo Set for trip no
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }
    
    /**
     * <p>Getter method for truck route.</p>
     *
     * @return the truck route
     */
    public String getTruckRoute() {
        return truckRoute;
    }

    /**
     * <p>Setter method for truck route.</p>
     *
     * @param truckRoute Set for truck route
     */
    public void setTruckRoute(String truckRoute) {
        this.truckRoute = truckRoute;
    }

    /**
     * <p>Getter method for truck seq.</p>
     *
     * @return the truck seq
     */
    public BigDecimal getTruckSeq() {
        return truckSeq;
    }

    /**
     * <p>Setter method for truck seq.</p>
     *
     * @param truckSeq Set for truck seq
     */
    public void setTruckSeq(BigDecimal truckSeq) {
        this.truckSeq = truckSeq;
    }

    /**
     * <p>Getter method for utc.</p>
     *
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * <p>Setter method for utc.</p>
     *
     * @param utc Set for utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }
    
    /**
     * <p>Getter chgCigmaDoNo.</p>
     *
     * @return the chgCigmaDoNo
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * <p>Setter method for chgCigmaDoNo.</p>
     *
     * @param chgCigmaDoNo Set for chgCigmaDoNo
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }
    
    /**
     * <p>Getter cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>Getter actualEtdStr.</p>
     *
     * @return the actualEtdStr
     */
    public String getActualEtdStr() {
        return actualEtdStr;
    }

    /**
     * <p>Setter method for actualEtdStr.</p>
     *
     * @param actualEtdStr Set for actualEtdStr
     */
    public void setActualEtdStr(String actualEtdStr) {
        this.actualEtdStr = actualEtdStr;
    }

    /**
     * <p>Getter actualEtaStr.</p>
     *
     * @return the actualEtaStr
     */
    public String getActualEtaStr() {
        return actualEtaStr;
    }

    /**
     * <p>Setter method for actualEtaStr.</p>
     *
     * @param actualEtaStr Set for actualEtaStr
     */
    public void setActualEtaStr(String actualEtaStr) {
        this.actualEtaStr = actualEtaStr;
    }

    /**
     * <p>Getter createDateTime.</p>
     *
     * @return the createDateTime
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * <p>Setter method for createDateTime.</p>
     *
     * @param createDateTime Set for createDateTime
     */
    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }


    /**
     * <p>Getter changeReasonCd.</p>
     *
     * @return the changeReasonCd
     */
    public String getChangeReasonCd() {
        return changeReasonCd;
    }

    /**
     * <p>Setter method for changeReasonCd.</p>
     *
     * @param changeReasonCd Set for changeReasonCd
     */
    public void setChangeReasonCd(String changeReasonCd) {
        this.changeReasonCd = changeReasonCd;
    }

    /**
     * <p>Getter reviseShippingQtyFlag.</p>
     *
     * @return the reviseShippingQtyFlag
     */
    public String getReviseShippingQtyFlag() {
        return reviseShippingQtyFlag;
    }

    /**
     * <p>Setter method for reviseShippingQtyFlag.</p>
     *
     * @param reviseShippingQtyFlag Set for reviseShippingQtyFlag
     */
    public void setReviseShippingQtyFlag(String reviseShippingQtyFlag) {
        this.reviseShippingQtyFlag = reviseShippingQtyFlag;
    }

    /**
     * <p>Getter currentSpsDoNo.</p>
     *
     * @return the currentSpsDoNo
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }

    /**
     * <p>Setter method for currentSpsDoNo.</p>
     *
     * @param currentSpsDoNo Set for currentSpsDoNo
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }

    /**
     * <p>Getter currentCigmaDoNo.</p>
     *
     * @return the currentCigmaDoNo
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * <p>Setter method for currentCigmaDoNo.</p>
     *
     * @param currentCigmaDoNo Set for currentCigmaDoNo
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * <p>Getter orderMethod.</p>
     *
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * <p>Setter method for orderMethod.</p>
     *
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * <p>Getter dockCode.</p>
     *
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * <p>Setter method for dockCode.</p>
     *
     * @param dockCode Set for dockCode
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * <p>Getter cycle.</p>
     *
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * <p>Setter method for cycle.</p>
     *
     * @param cycle Set for cycle
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * <p>Getter whPrimeReceiving.</p>
     *
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * <p>Setter method for whPrimeReceiving.</p>
     *
     * @param whPrimeReceiving Set for whPrimeReceiving
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * <p>Getter ctrlNo.</p>
     *
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * <p>Setter method for ctrlNo.</p>
     *
     * @param ctrlNo Set for ctrlNo
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
    * <p>Getter itemDesc.</p>
    *
    * @return the itemDesc
    */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * <p>Setter method for itemDesc.</p>
     *
     * @param itemDesc Set for itemDesc
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * <p>Getter rcvLane.</p>
     *
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>Setter method for rcvLane.</p>
     *
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>Getter kanbanSeqNo.</p>
     *
     * @return the kanbanSeqNo
     */
    public String getKanbanSeqNo() {
        return kanbanSeqNo;
    }

    /**
     * <p>Setter method for kanbanSeqNo.</p>
     *
     * @param kanbanSeqNo Set for kanbanSeqNo
     */
    public void setKanbanSeqNo(String kanbanSeqNo) {
        this.kanbanSeqNo = kanbanSeqNo;
    }

    /**
     * <p>Getter method for invoiceDateShow.</p>
     *
     * @return the invoiceDateShow
     */
    public String getInvoiceDateShow() {
        return invoiceDateShow;
    }

    /**
     * <p>Setter method for invoiceDateShow.</p>
     *
     * @param invoiceDateShow Set for invoiceDateShow
     */
    public void setInvoiceDateShow(String invoiceDateShow) {
        this.invoiceDateShow = invoiceDateShow;
    }
    
}