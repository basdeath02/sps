/*
 * ModifyDate Development company     Describe 
 * 2014/08/15 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * <p>
 * Task List Criteria Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TaskListDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1453198681017720527L;

    /** The supplier code. */
    private String sCd;

    /** The company denso code. */
    private String dCd;

    /** The p/o Issue Date. */
    private Date issueDate;
    
    /** The notice Do Urgent Period. */
    private int noticeDoUrgentPeriod;

    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /**
     * Instantiates a new Task list domain.
     */
    public TaskListDomain() {
        super();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }

    /**
     * <p>
     * Getter method for Supplier code.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for Supplier code.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>
     * Getter method for dcd.
     * </p>
     * 
     * @return the dcd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for dcd.
     * </p>
     * 
     * @param dcd Set for dcd
     */
    public void setDCd(String dcd) {
        this.dCd = dcd;
    }

    /**
     * <p>
     * Getter method for issueDate.
     * </p>
     * 
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * <p>
     * Setter method for issueDate.
     * </p>
     * 
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * <p>
     * Getter method for noticeDoUrgentPeriod.
     * </p>
     * 
     * @return the noticeDoUrgentPeriod
     */
    public int getNoticeDoUrgentPeriod() {
        return noticeDoUrgentPeriod;
    }

    /**
     * <p>
     * Setter method for noticeDoUrgentPeriod.
     * </p>
     * 
     * @param noticeDoUrgentPeriod Set for noticeDoUrgentPeriod
     */
    public void setNoticeDoUrgentPeriod(int noticeDoUrgentPeriod) {
        this.noticeDoUrgentPeriod = noticeDoUrgentPeriod;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
}
