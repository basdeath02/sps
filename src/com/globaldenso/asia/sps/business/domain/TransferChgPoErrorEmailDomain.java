/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;

/**
 * <p>
 * Transfer Change Purchase Order Error Email detail.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TransferChgPoErrorEmailDomain extends BaseDomain implements Serializable {

    /** Generated serial version UID. */
    private static final long serialVersionUID = -5091522294895068085L;

    /** The SPS_CIGMA_CHG_PO_ERROR. */
    private SpsCigmaChgPoErrorDomain cigmaChgPoError;

    /** SPS_M_DENSO_SUPPLIER_PARTS data. */
    private SpsMDensoSupplierPartsDomain densoSupplerParts;
    
    /** The default constructor. */
    public TransferChgPoErrorEmailDomain() {
        this.cigmaChgPoError = new SpsCigmaChgPoErrorDomain();
        this.densoSupplerParts = new SpsMDensoSupplierPartsDomain();
    }

    /**
     * <p>Getter method for cigmaChgPoError.</p>
     *
     * @return the cigmaChgPoError
     */
    public SpsCigmaChgPoErrorDomain getCigmaChgPoError() {
        return cigmaChgPoError;
    }

    /**
     * <p>Setter method for cigmaChgPoError.</p>
     *
     * @param cigmaChgPoError Set for cigmaChgPoError
     */
    public void setCigmaChgPoError(SpsCigmaChgPoErrorDomain cigmaChgPoError) {
        this.cigmaChgPoError = cigmaChgPoError;
    }

    /**
     * <p>Getter method for densoSupplerParts.</p>
     *
     * @return the densoSupplerParts
     */
    public SpsMDensoSupplierPartsDomain getDensoSupplerParts() {
        return densoSupplerParts;
    }

    /**
     * <p>Setter method for densoSupplerParts.</p>
     *
     * @param densoSupplerParts Set for densoSupplerParts
     */
    public void setDensoSupplerParts(SpsMDensoSupplierPartsDomain densoSupplerParts) {
        this.densoSupplerParts = densoSupplerParts;
    }
    
}
