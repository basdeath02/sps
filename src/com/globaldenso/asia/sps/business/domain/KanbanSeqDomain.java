/*
 * ModifyDate Development company     Describe 
 * 2014/08/15 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * <p>Kanban Seq Domain class.</p>
 *
 * @author CSI
 * @version 1.00
 */
public class KanbanSeqDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serial.
     * </p>
     */
    private static final long serialVersionUID = -5093397881974853589L;
   
    /**
     * <p>
     * Kanban Seq.
     * </p>
     */
    private String kanbanSeq;

    /**
     * <p>
     * The Constructor.
     * </p>
     */
    public KanbanSeqDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for kanbanSeq.
     * </p>
     * 
     * @return the kanbanSeq
     */
    public String getKanbanSeq() {
        return kanbanSeq;
    }

    /**
     * <p>
     * Setter method for kanbanSeq.
     * </p>
     * 
     * @param kanbanSeq Set for kanbanSeq
     */
    public void setKanbanSeq(String kanbanSeq) {
        this.kanbanSeq = kanbanSeq;
    }

}
