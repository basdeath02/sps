/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;

/**
 * <p>
 * Transfer Purchase Order Error Email detail.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TransferPoErrorEmailDomain extends BaseDomain implements Serializable {
    
    /** Generated serial version UID. */
    private static final long serialVersionUID = 4291020546333931890L;

    /** SPS_CIGMA_PO_ERROR data. */
    private SpsCigmaPoErrorDomain cigmaPoError;
    
    /** SPS_M_DENSO_SUPPLIER_PARTS data. */
    private SpsMDensoSupplierPartsDomain densoSupplerParts;
    
    /** The default constructor. */
    public TransferPoErrorEmailDomain() {
        this.cigmaPoError = new SpsCigmaPoErrorDomain();
        this.densoSupplerParts = new SpsMDensoSupplierPartsDomain();
    }

    /**
     * <p>Getter method for cigmaPoError.</p>
     *
     * @return the cigmaPoError
     */
    public SpsCigmaPoErrorDomain getCigmaPoError() {
        return cigmaPoError;
    }

    /**
     * <p>Setter method for cigmaPoError.</p>
     *
     * @param cigmaPoError Set for cigmaPoError
     */
    public void setCigmaPoError(SpsCigmaPoErrorDomain cigmaPoError) {
        this.cigmaPoError = cigmaPoError;
    }

    /**
     * <p>Getter method for densoSupplerParts.</p>
     *
     * @return the densoSupplerParts
     */
    public SpsMDensoSupplierPartsDomain getDensoSupplerParts() {
        return densoSupplerParts;
    }

    /**
     * <p>Setter method for densoSupplerParts.</p>
     *
     * @param densoSupplerParts Set for densoSupplerParts
     */
    public void setDensoSupplerParts(SpsMDensoSupplierPartsDomain densoSupplerParts) {
        this.densoSupplerParts = densoSupplerParts;
    }
    
    
}
