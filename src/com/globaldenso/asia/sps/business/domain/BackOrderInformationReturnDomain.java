/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class BackOrderInformationReturnDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3857775057146425189L;
    
    /** The list of back order Information domain. */
    private List<BackOrderInformationDomain> backOrderInformationList;
    
    /** The file name. */
    private String fileName;
    
    /** The csv result. */
    private StringBuffer csvResult;
    
    /** The company supplier list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The company denso list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The plant supplier list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The denso supplier relation list. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;
    
    /** The transport mode cb list. */
    private List<MiscellaneousDomain> transportModeCbList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new file upload domain.
     */
    public BackOrderInformationReturnDomain() {
        super();
        errorMessageList = new ArrayList<ApplicationMessageDomain>();
    }
    
    /**
     * <p>Getter method for backOrderInformationList.</p>
     *
     * @return the backOrderInformationList
     */
    public List<BackOrderInformationDomain> getBackOrderInformationList() {
        return backOrderInformationList;
    }

    /**
     * <p>Setter method for backOrderInformationList.</p>
     *
     * @param backOrderInformationList Set for backOrderInformationList
     */
    public void setBackOrderInformationList(
        List<BackOrderInformationDomain> backOrderInformationList) {
        this.backOrderInformationList = backOrderInformationList;
    }

    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for csvResult.</p>
     *
     * @return the csvResult
     */
    public StringBuffer getCsvResult() {
        return csvResult;
    }

    /**
     * <p>Setter method for csvResult.</p>
     *
     * @param csvResult Set for csvResult
     */
    public void setCsvResult(StringBuffer csvResult) {
        this.csvResult = csvResult;
    }
    
    /**
     * Gets the company denso list.
     * 
     * @return the company denso list.
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * Sets the denso supplier relation list.
     * 
     * @param densoSupplierRelationList the denso supplier relation list.
     */
    public void setDensoSupplierRelationList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * Gets the transport mode cb list.
     * 
     * @return the transport mode cb list.
     */
    public List<MiscellaneousDomain> getTransportModeCbList() {
        return transportModeCbList;
    }

    /**
     * Sets the transport mode cb list.
     * 
     * @param transportModeCbList the transport mode cb list.
     */
    public void setTransportModeCbList(List<MiscellaneousDomain> transportModeCbList) {
        this.transportModeCbList = transportModeCbList;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * Gets the plant denso list.
     * 
     * @return the plant denso  list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the plant denso  list.
     * 
     * @param plantDensoList the plant denso  list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the plant supplier list.
     * 
     * @return the plant supplier list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the plant supplier list.
     * 
     * @param plantSupplierList the plant supplier list.
     */
    public void setPlantSupplierList(
        List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    
    /**
     * Gets the supplier code list.
     * 
     * @return the supplier code list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the supplier code list.
     * 
     * @param companySupplierList the supplier code list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the denso code list.
     * 
     * @return the denso code list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso code list.
     * 
     * @param companyDensoList the denso code list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
}