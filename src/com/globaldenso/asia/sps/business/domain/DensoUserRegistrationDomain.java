/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class DENSO User Domain.
 * @author CSI
 */
public class DensoUserRegistrationDomain extends BaseDomain implements Serializable {
 
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8255147521509317665L;

    /** The Data Scope Control Domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID selected. */
    private String dscIdSelected; 
    
    /** The company DENSO Code. */
    private String dCd;
   
    /** The Plant DENSO. */
    private String dPcd;
    
    /** The Role Code. */
    private String roleCode;
    
    /** The First Name. */
    private String firstName;
    
    /** The Middle Name. */
    private String middleName;
    
    /** The Last Name. */
    private String lastName;
    
    /** The Employee Code. */
    private String employeeCode;
    
    /** The department name. */
    private String departmentName; 
    
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone;
    
    /** The Is Active. */
    private String isActive;
    
    /** The DENSO user detail domain. */
    private UserDensoDetailDomain userDensoDetailDomain;
    
    /** The user Domain.*/
    private UserDomain userDomain;
    
    /** The user DENSO Domain.*/
    private SpsMUserDensoDomain spsMUserDensoDomain;
    
    /** The User Role Domain.*/
    private SpsMUserRoleDomain spsMUserRoleDomain;
    
    /** The List of DENSO User Info domain. */
    private List<DensoUserRegistrationDomain> userDensoRegistrationList;
    
    /** The List of Company DENSO domain. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of Plant DENSO domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The list of department name.*/
    private List<SpsMUserDomain> departmentList;
    
    /** The Date time update. */
    private Timestamp lastUpdateDatetime;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    /** The Date time update. */
    private String updateDatetime;
    
    /** The Date time update. */
    private String updateDatetimeDenso;
    
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public DensoUserRegistrationDomain() {
        super(); 
        userDomain = new UserDomain();
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Gets the dscId Selected.
     * 
     * @return the dscId Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }

    /**
     * Sets the dscId Selected.
     * 
     * @param dscIdSelected the new dscId Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    
    /**
     * Gets the role Code
     * 
     * @return the role Code
     */  
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the role Code.
     * 
     * @param roleCode the role Code.
     */ 
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
   
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }

    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the list of user DENSO Registration detail.
     * 
     * @return the list of user DENSO Registration detail.
     */
    public List<DensoUserRegistrationDomain> getUserDensoRegistrationList() {
        return userDensoRegistrationList;
    }

    /**
     * Sets the list of user DENSO Registration detail.
     * 
     * @param userDensoRegistrationList the list of user DENSO Registration detail.
     */
    public void setUserDensoRegistrationList(List<DensoUserRegistrationDomain> 
        userDensoRegistrationList) {
        this.userDensoRegistrationList = userDensoRegistrationList;
    }
    
    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the Company DENSO code.
     * 
     * @return the Company DENSO code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Sets the Company DENSO code.
     * 
     * @param dCd the Company DENSO code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * Gets the plant DENSO code.
     * 
     * @return the plant DENSO code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the plant DENSO code.
     * 
     * @param dPcd the plant DENSO code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * Gets the list of company DENSO Domain.
     * 
     * @return the list of company DENSO Domain.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }
    /**
     * Sets the list of company DENSO Domain.
     * 
     * @param companyDensoList the list of company DENSO Domain.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    /**
     * Gets the list of Plant DENSO Domain.
     * 
     * @return the list of Plant DENSO Domain.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }
    /**
     * Sets the list of Plant DENSO Domain.
     * 
     * @param plantDensoList the list of Plant DENSO Domain.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    /**
     * Gets the First name.
     * 
     * @return the First name.
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * Sets the First name.
     * 
     * @param firstName the First name..
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * Gets the Middle name.
     * 
     * @return the Middle name.
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * Sets the Middle name.
     * 
     * @param middleName the Middle name..
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Gets the Employee code.
     * 
     * @return the Employee code.
     */
    public String getEmployeeCode() {
        return employeeCode;
    }
    /**
     * Sets the Employee code.
     * 
     * @param employeeCode the Employee code.
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the Last Update date.
     * 
     * @return the Last Update date.
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Sets the Last Update date.
     * 
     * @param lastUpdateDatetime the Last Update date.
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Gets the Is Active.
     * 
     * @return the Is Active.
     */
    public String getIsActive() {
        return isActive;
    }
    /**
     * Sets the Is Active.
     * 
     * @param isActive the Is Active.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    
    /**
     * Gets the user DENSO Domain.
     * 
     * @return the user DENSO Domain.
     */
    public SpsMUserDensoDomain getSpsMUserDensoDomain() {
        return spsMUserDensoDomain;
    }
    /**
     * Sets the user DENSO Domain.
     * 
     * @param spsMUserDensoDomain the user DENSO Domain.
     */
    public void setSpsMUserDensoDomain(SpsMUserDensoDomain spsMUserDensoDomain) {
        this.spsMUserDensoDomain = spsMUserDensoDomain;
    }
    /**
     * Gets the User Role Domain.
     * 
     * @return the User Role Domain.
     */
    public SpsMUserRoleDomain getSpsMUserRoleDomain() {
        return spsMUserRoleDomain;
    }
    /**
     * Sets the User Role Domain.
     * 
     * @param spsMUserRoleDomain the User Role Domain.
     */
    public void setSpsMUserRoleDomain(SpsMUserRoleDomain spsMUserRoleDomain) {
        this.spsMUserRoleDomain = spsMUserRoleDomain;
    }
    /**
     * <p>Getter method for user DENSO detail domain.</p>
     *
     * @return the user DENSO detail domain.
     */
    public UserDensoDetailDomain getUserDensoDetailDomain() {
        return userDensoDetailDomain;
    }
    /**
     * <p>Setter method for user DENSO  detail domain.</p>
     *
     * @param userDensoDetailDomain Set for user DENSO detail domain.
     */
    public void setUserDensoDetailDomain(UserDensoDetailDomain userDensoDetailDomain) {
        this.userDensoDetailDomain = userDensoDetailDomain;
    }
    
    /**
     * Gets the list of department name.
     * 
     * @return the list of department name.
     */
    public List<SpsMUserDomain> getDepartmentList() {
        return departmentList;
    }
    
    /**
     * Sets the list of department name.
     * 
     * @param departmentList the list of department name.
     */
    public void setDepartmentList(List<SpsMUserDomain> departmentList) {
        this.departmentList = departmentList;
    }
    
    /**
     * Gets the department Name.
     * 
     * @return the department Name.
     */
    public String getDepartmentName() {
        return departmentName;
    }
    

    /**
     * Sets the department Name.
     * 
     * @param departmentName the department Name.
     */

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    /**
     * Gets the updateDatetime.
     * 
     * @return the updateDatetime.
     */
    public String getUpdateDatetime() {
        return updateDatetime;
    }
    /**
     * Sets the updateDatetime.
     * 
     * @param updateDatetime the updateDatetime.
     */
    public void setUpdateDatetime(String updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
    /**
     * Gets the updateDatetimeDenso.
     * 
     * @return the updateDatetimeDenso.
     */
    public String getUpdateDatetimeDenso() {
        return updateDatetimeDenso;
    }
    /**
     * Sets the updateDatetimeDenso.
     * 
     * @param updateDatetimeDenso the updateDatetimeDenso.
     */
    public void setUpdateDatetimeDenso(String updateDatetimeDenso) {
        this.updateDatetimeDenso = updateDatetimeDenso;
    }
}