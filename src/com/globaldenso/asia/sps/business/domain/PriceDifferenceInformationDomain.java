/*
 * ModifyDate Development company     Describe 
 * 2014/07/02 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;

/**
 * The Class Price Difference Information Domain.
 * @author CSI
 */
public class PriceDifferenceInformationDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;

    /** The sps invoice domain. */
    private SpsTInvoiceDomain spsTInvoiceDomain;
    
    /** The sps invoice detail domain. */
    private SpsTInvoiceDetailDomain spsTInvoiceDetailDomain;
    
    /** The sps cn domain. */
    private SpsTCnDomain spsTCnDomain;
    
    /** The sps cn domain. */
    private SpsTCnDetailDomain spsTCnDetailDomain;
    
    /** The spsT Cn Criteria Domain. */
    private SpsTCnCriteriaDomain spsTCnCriteriaDomain;
    
    /** The SpsT Invoice Criteria Domain. */
    private SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain;
     
    /** The file Id */
    private String fileId;

    /** The supplier code */
    private String sCd;
    
    /** The vendor code */
    private String vendorCd;
    
    /** The supplier plant code.*/
    private String sPcd;
    
    /** The denso code */
    private String dCd;
    
    /** The denso plant code.*/
    private String dPcd;
    
    /** The denso part no.*/
    private String dPn;
    
    /** The supplier part no.*/
    private String sPn;
    
    /** The supplier price unit.*/
    private String supplierPriceUnit;
    
    /** The denso price unit.*/
    private String densoPriceUnit;
    
    /** The difference price unit.*/
    private String differencePriceUnit;
    
    /** The invoice dateForm.*/
    private String invoiceDateFrom;
    
    /** The invoice dateTo.*/
    private String invoiceDateTo;
    
    /** The invoice status.*/
    private String invoiceStatus;
    
    /** The invoice no.*/
    private String invoiceNo;
    
    /** The invoice dateForm.*/
    private String cnDateFrom;
    
    /** The invoice dateTo.*/
    private String cnDateTo;
    
    /** The invoice date.*/
    private String invoiceDate;
    
    /** The cn date.*/
    private String cnDate;
    
    /** The cn no.*/
    private String cnNo;
    
    /** The asn no.*/
    private String asnNo;
    
    /** The asn date.*/
    private Timestamp asnDate;
    
    /** The plan eta.*/
    private Timestamp planEta;
    
    /** The Generate Date.*/
    private String generateDate;
    
    /** The get Date time.*/
    private Date getDatetime;
    
    /** The decimal Disp.*/
    private String decimalDisp;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The Denso Supplier Relation Domain. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationDomainList;
    
    /** The SpsM Company Supplier Domain. */
    private List<CompanySupplierDomain> companySupplierDomainList;
    
    /** The SpsM Company Denso Domain. */
    private List<CompanyDensoDomain> companyDensoDomainList;
    
    /** The SpsM Plant Supplie rDomain. */
    private List<PlantSupplierDomain> plantSupplierDomainList;
    
    /** The SpsM Plant Denso Domain. */
    private List<PlantDensoDomain> plantDensoDomainList;

    /** The SpsM Plant Denso Domain. */
    private List<SpsMMiscDomain> plantDensoMiscList;

    /** The SpsM Plant Denso Domain. */
    private List<SpsMMiscDomain> plantSupplierMiscList;
    
    /** The Misc Domain. */
    private List<MiscellaneousDomain> invoiceStatusList;
    
    /**
     * Instantiates a new file upload domain.
     */
    public PriceDifferenceInformationDomain() {
        super();
        this.spsTInvoiceDomain = new SpsTInvoiceDomain();
        this.spsTCnDomain = new SpsTCnDomain();
        this.spsTCnDetailDomain = new SpsTCnDetailDomain();
        this.spsTCnCriteriaDomain = new SpsTCnCriteriaDomain();
        this.spsTInvoiceCriteriaDomain = new SpsTInvoiceCriteriaDomain();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }

    /**
     * <p>Getter method for spsTInvoiceDomain.</p>
     *
     * @return the spsTInvoiceDomain
     */
    public SpsTInvoiceDomain getSpsTInvoiceDomain() {
        return spsTInvoiceDomain;
    }

    /**
     * <p>Setter method for spsTInvoiceDomain.</p>
     *
     * @param spsTInvoiceDomain Set for spsTInvoiceDomain
     */
    public void setSpsTInvoiceDomain(SpsTInvoiceDomain spsTInvoiceDomain) {
        this.spsTInvoiceDomain = spsTInvoiceDomain;
    }
    
    /**
     * <p>Getter method for spsTInvoiceDetailDomain.</p>
     *
     * @return the spsTInvoiceDetailDomain
     */
    public SpsTInvoiceDetailDomain getSpsTInvoiceDetailDomain() {
        return spsTInvoiceDetailDomain;
    }

    /**
     * <p>Setter method for spsTInvoiceDetailDomain.</p>
     *
     * @param spsTInvoiceDetailDomain Set for spsTInvoiceDetailDomain
     */
    public void setSpsTInvoiceDetailDomain(
        SpsTInvoiceDetailDomain spsTInvoiceDetailDomain) {
        this.spsTInvoiceDetailDomain = spsTInvoiceDetailDomain;
    }

    /**
     * <p>Getter method for spsTCnDomain.</p>
     *
     * @return the spsTCnDomain
     */
    public SpsTCnDomain getSpsTCnDomain() {
        return spsTCnDomain;
    }

    /**
     * <p>Setter method for spsTCnDomain.</p>
     *
     * @param spsTCnDomain Set for spsTCnDomain
     */
    public void setSpsTCnDomain(SpsTCnDomain spsTCnDomain) {
        this.spsTCnDomain = spsTCnDomain;
    }

    /**
     * <p>Getter method for spsTCnDetailDomain.</p>
     *
     * @return the spsTCnDetailDomain
     */
    public SpsTCnDetailDomain getSpsTCnDetailDomain() {
        return spsTCnDetailDomain;
    }

    /**
     * <p>Setter method for spsTCnDetailDomain.</p>
     *
     * @param spsTCnDetailDomain Set for spsTCnDetailDomain
     */
    public void setSpsTCnDetailDomain(SpsTCnDetailDomain spsTCnDetailDomain) {
        this.spsTCnDetailDomain = spsTCnDetailDomain;
    }

    /**
     * <p>Getter method for s_CD.</p>
     *
     * @return the s_CD
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for s_CD.</p>
     *
     * @param sCd Set for s_CD
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for s_PCD.</p>
     *
     * @return the s_PCD
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for s_PCD.</p>
     *
     * @param sPcd Set for s_PCD
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for d_CD.</p>
     *
     * @return the d_CD
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for d_CD.</p>
     *
     * @param dCd Set for d_CD
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for d_PCD.</p>
     *
     * @return the d_PCD
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for d_PCD.</p>
     *
     * @param dPcd Set for d_PCD
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for d_PN.</p>
     *
     * @return the d_PN
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for d_PN.</p>
     *
     * @param dPn Set for d_PN
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for s_PN.</p>
     *
     * @return the s_PN
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for s_PN.</p>
     *
     * @param sPn Set for s_PN
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for supplierPriceUnit.</p>
     *
     * @return the supplierPriceUnit
     */
    public String getSupplierPriceUnit() {
        return supplierPriceUnit;
    }

    /**
     * <p>Setter method for supplierPriceUnit.</p>
     *
     * @param supplierPriceUnit Set for supplierPriceUnit
     */
    public void setSupplierPriceUnit(String supplierPriceUnit) {
        this.supplierPriceUnit = supplierPriceUnit;
    }

    /**
     * <p>Getter method for densoPriceUnit.</p>
     *
     * @return the densoPriceUnit
     */
    public String getDensoPriceUnit() {
        return densoPriceUnit;
    }

    /**
     * <p>Setter method for densoPriceUnit.</p>
     *
     * @param densoPriceUnit Set for densoPriceUnit
     */
    public void setDensoPriceUnit(String densoPriceUnit) {
        this.densoPriceUnit = densoPriceUnit;
    }

    /**
     * <p>Getter method for differencePriceUnit.</p>
     *
     * @return the differencePriceUnit
     */
    public String getDifferencePriceUnit() {
        return differencePriceUnit;
    }

    /**
     * <p>Setter method for differencePriceUnit.</p>
     *
     * @param differencePriceUnit Set for differencePriceUnit
     */
    public void setDifferencePriceUnit(String differencePriceUnit) {
        this.differencePriceUnit = differencePriceUnit;
    }

    /**
     * <p>Getter method for invoiceDate.</p>
     *
     * @return the invoiceDate
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * <p>Setter method for invoiceDate.</p>
     *
     * @param invoiceDate Set for invoiceDate
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * <p>Getter method for cnDate.</p>
     *
     * @return the cnDate
     */
    public String getCnDate() {
        return cnDate;
    }

    /**
     * <p>Setter method for cnDate.</p>
     *
     * @param cnDate Set for cnDate
     */
    public void setCnDate(String cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * <p>Getter method for generateDate.</p>
     *
     * @return the generateDate
     */
    public String getGenerateDate() {
        return generateDate;
    }

    /**
     * <p>Setter method for generateDate.</p>
     *
     * @param generateDate Set for generateDate
     */
    public void setGenerateDate(String generateDate) {
        this.generateDate = generateDate;
    }

    /**
     * <p>Getter method for densoSupplierRelationDomainList.</p>
     *
     * @return the densoSupplierRelationDomainList
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationDomainList() {
        return densoSupplierRelationDomainList;
    }

    /**
     * <p>Setter method for densoSupplierRelationDomainList.</p>
     *
     * @param densoSupplierRelationDomainList Set for densoSupplierRelationDomainList
     */
    public void setDensoSupplierRelationDomainList(
        List<DensoSupplierRelationDomain> densoSupplierRelationDomainList) {
        this.densoSupplierRelationDomainList = densoSupplierRelationDomainList;
    }

    /**
     * <p>Getter method for companySupplierDomainList.</p>
     *
     * @return the companySupplierDomainList
     */
    public List<CompanySupplierDomain> getCompanySupplierDomainList() {
        return companySupplierDomainList;
    }

    /**
     * <p>Setter method for companySupplierDomainList.</p>
     *
     * @param companySupplierDomainList Set for companySupplierDomainList
     */
    public void setCompanySupplierDomainList(
        List<CompanySupplierDomain> companySupplierDomainList) {
        this.companySupplierDomainList = companySupplierDomainList;
    }

    /**
     * <p>Getter method for companyDensoDomainList.</p>
     *
     * @return the companyDensoDomainList
     */
    public List<CompanyDensoDomain> getCompanyDensoDomainList() {
        return companyDensoDomainList;
    }

    /**
     * <p>Setter method for companyDensoDomainList.</p>
     *
     * @param companyDensoDomainList Set for companyDensoDomainList
     */
    public void setCompanyDensoDomainList(
        List<CompanyDensoDomain> companyDensoDomainList) {
        this.companyDensoDomainList = companyDensoDomainList;
    }

    /**
     * <p>Getter method for plantDensoDomainList.</p>
     *
     * @return the plantDensoDomainList
     */
    public List<PlantDensoDomain> getPlantDensoDomainList() {
        return plantDensoDomainList;
    }

    /**
     * <p>Setter method for plantDensoDomainList.</p>
     *
     * @param plantDensoDomainList Set for plantDensoDomainList
     */
    public void setPlantDensoDomainList(List<PlantDensoDomain> plantDensoDomainList) {
        this.plantDensoDomainList = plantDensoDomainList;
    }

    /**
     * <p>Getter method for invoiceStatusList.</p>
     *
     * @return the invoiceStatusList
     */
    public List<MiscellaneousDomain> getInvoiceStatusList() {
        return invoiceStatusList;
    }

    /**
     * <p>Setter method for invoiceStatusList.</p>
     *
     * @param invoiceStatusList Set for invoiceStatusList
     */
    public void setInvoiceStatusList(List<MiscellaneousDomain> invoiceStatusList) {
        this.invoiceStatusList = invoiceStatusList;
    }
    
    /**
     * <p>Getter method for plantSupplierDomainList.</p>
     *
     * @return the plantSupplierDomainList
     */
    public List<PlantSupplierDomain> getPlantSupplierDomainList() {
        return plantSupplierDomainList;
    }

    /**
     * <p>Setter method for plantSupplierDomainList.</p>
     *
     * @param plantSupplierDomainList Set for plantSupplierDomainList
     */
    public void setPlantSupplierDomainList(
        List<PlantSupplierDomain> plantSupplierDomainList) {
        this.plantSupplierDomainList = plantSupplierDomainList;
    }
    
    /**
     * <p>Getter method for fileId.</p>
     *
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Setter method for fileId.</p>
     *
     * @param fileId Set for fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * <p>Getter method for spsTCnCriteriaDomain.</p>
     *
     * @return the spsTCnCriteriaDomain
     */
    public SpsTCnCriteriaDomain getSpsTCnCriteriaDomain() {
        return spsTCnCriteriaDomain;
    }

    /**
     * <p>Setter method for spsTCnCriteriaDomain.</p>
     *
     * @param spsTCnCriteriaDomain Set for spsTCnCriteriaDomain
     */
    public void setSpsTCnCriteriaDomain(SpsTCnCriteriaDomain spsTCnCriteriaDomain) {
        this.spsTCnCriteriaDomain = spsTCnCriteriaDomain;
    }

    /**
     * <p>Getter method for spsTInvoiceCriteriaDomain.</p>
     *
     * @return the spsTInvoiceCriteriaDomain
     */
    public SpsTInvoiceCriteriaDomain getSpsTInvoiceCriteriaDomain() {
        return spsTInvoiceCriteriaDomain;
    }

    /**
     * <p>Setter method for spsTInvoiceCriteriaDomain.</p>
     *
     * @param spsTInvoiceCriteriaDomain Set for spsTInvoiceCriteriaDomain
     */
    public void setSpsTInvoiceCriteriaDomain(
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain) {
        this.spsTInvoiceCriteriaDomain = spsTInvoiceCriteriaDomain;
    }

    /**
     * <p>Getter method for invoiceDateForm.</p>
     *
     * @return the invoiceDateForm
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }

    /**
     * <p>Setter method for invoiceDateForm.</p>
     *
     * @param invoiceDateForm Set for invoiceDateForm
     */
    public void setInvoiceDateFrom(String invoiceDateForm) {
        this.invoiceDateFrom = invoiceDateForm;
    }

    /**
     * <p>Getter method for invoiceDateTo.</p>
     *
     * @return the invoiceDateTo
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }

    /**
     * <p>Setter method for invoiceDateTo.</p>
     *
     * @param invoiceDateTo Set for invoiceDateTo
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }

    /**
     * <p>Getter method for cnDateTo.</p>
     *
     * @return the cnDateTo
     */
    public String getCnDateTo() {
        return cnDateTo;
    }

    /**
     * <p>Setter method for cnDateTo.</p>
     *
     * @param cnDateTo Set for cnDateTo
     */
    public void setCnDateTo(String cnDateTo) {
        this.cnDateTo = cnDateTo;
    }

    /**
     * <p>Getter method for invoiceStatus.</p>
     *
     * @return the invoiceStatus
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * <p>Setter method for invoiceStatus.</p>
     *
     * @param invoiceStatus Set for invoiceStatus
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * <p>Getter method for cnNo.</p>
     *
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * <p>Setter method for cnNo.</p>
     *
     * @param cnNo Set for cnNo
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }
    
    /**
     * <p>Getter method for planEta.</p>
     *
     * @return the planEta
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * <p>Setter method for planEta.</p>
     *
     * @param planEta Set for planEta
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * <p>Getter method for asnDate.</p>
     *
     * @return the asnDate
     */
    public Timestamp getAsnDate() {
        return asnDate;
    }

    /**
     * <p>Setter method for asnDate.</p>
     *
     * @param asnDate Set for asnDate
     */
    public void setAsnDate(Timestamp asnDate) {
        this.asnDate = asnDate;
    }

    /**
     * <p>Getter method for getDatetime.</p>
     *
     * @return the getDatetime
     */
    public Date getGetDatetime() {
        return getDatetime;
    }

    /**
     * <p>Setter method for getDatetime.</p>
     *
     * @param getDatetime Set for getDatetime
     */
    public void setGetDatetime(Date getDatetime) {
        this.getDatetime = getDatetime;
    }

    /**
     * <p>Getter method for cnDateFrom.</p>
     *
     * @return the cnDateFrom
     */
    public String getCnDateFrom() {
        return cnDateFrom;
    }

    /**
     * <p>Setter method for cnDateFrom.</p>
     *
     * @param cnDateFrom Set for cnDateFrom
     */
    public void setCnDateFrom(String cnDateFrom) {
        this.cnDateFrom = cnDateFrom;
    }

    /**
     * <p>Getter method for decimalDisp.</p>
     *
     * @return the decimalDisp
     */
    public String getDecimalDisp() {
        return decimalDisp;
    }

    /**
     * <p>Setter method for decimalDisp.</p>
     *
     * @param decimalDisp Set for decimalDisp
     */
    public void setDecimalDisp(String decimalDisp) {
        this.decimalDisp = decimalDisp;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for plantDensoMiscList.</p>
     *
     * @return the plantDensoMiscList
     */
    public List<SpsMMiscDomain> getPlantDensoMiscList() {
        return plantDensoMiscList;
    }

    /**
     * <p>Setter method for plantDensoMiscList.</p>
     *
     * @param plantDensoMiscList Set for plantDensoMiscList
     */
    public void setPlantDensoMiscList(List<SpsMMiscDomain> plantDensoMiscList) {
        this.plantDensoMiscList = plantDensoMiscList;
    }

    /**
     * <p>Getter method for plantSupplierMiscList.</p>
     *
     * @return the plantSupplierMiscList
     */
    public List<SpsMMiscDomain> getPlantSupplierMiscList() {
        return plantSupplierMiscList;
    }

    /**
     * <p>Setter method for plantSupplierMiscList.</p>
     *
     * @param plantSupplierMiscList Set for plantSupplierMiscList
     */
    public void setPlantSupplierMiscList(List<SpsMMiscDomain> plantSupplierMiscList) {
        this.plantSupplierMiscList = plantSupplierMiscList;
    }
}