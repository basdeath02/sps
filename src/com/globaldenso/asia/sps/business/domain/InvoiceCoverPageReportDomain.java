/*
 * ModifyDate Development company     Describe 
 * 2014/07/21 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The Invoice Cover Page Report Domain.
 * @author CSI
 */
public class InvoiceCoverPageReportDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3857775057146425189L;
    
    /** The flag to identify record is Invoice or CN. */
    private String flag;
    
    /** The page no. */
    private String pageNo;
    
    /** The cover page no. */
    private String coverPageNo;
    
    /** The company supplier name. */
    private String companySupplierName;
    
    /** The supplier tax id. */
    private String supplierTaxId;
    
    /** The supplier address1. */
    private String supplierAddress1;
    
    /** The supplier address2. */
    private String supplierAddress2;
    
    /** The supplier address3. */
    private String supplierAddress3;
    
    /** The invoice currency. */
    private String invoiceCurrency;
    
    /** The company denso name. */
    private String companyDensoName;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The denso tax id. */
    private String densoTaxId;
    
    /** The denso address. */
    private String densoAddress;
    
    /** The invoice running no. */
    private String invoiceRunningNo;
    
    /** The timestamp invoice running no. */
    private Timestamp tsInvoiceRunningNo;
    
    /** The document type. */
    private String documentType;
    
    /** The document no. */
    private String documentNo;
    
    /** The document date. */
    private String documentDate;
    
    /** The timestamp invoice date. */
    private Timestamp tsInvoiceDate;
    
    /** The invoice amount. */
    private String invoiceAmount;
    
    /** The big decimal invoice amount. */
    private BigDecimal bdInvoiceAmount;
    
    /** The invoice base amount. */
    private String invoiceBaseAmount;
    
    /** The big decimal invoice base amount. */
    private BigDecimal bdInvoiceBaseAmount;
    
    /** The invoice vat amount. */
    private String invoiceVatAmount;
    
    /** The big decimal invoice vat amount. */
    private BigDecimal bdInvoiceVatAmount;
    
    /** The invoice total amount. */
    private String invoiceTotalAmount;
    
    /** The big decimal invoice total amount. */
    private BigDecimal bdInvoiceTotalAmount;
    
    /** The timestamp asn date. */
    private Timestamp tsAsnDate;
    
    /** The asn base amount. */
    private String asnBaseAmount;
    
    /** The big decimal asn base amount. */
    private BigDecimal bdAsnBaseAmount;
    
    /** The asn difference amount. */
    private String asnDifferenceAmount;
    
    /** The big decimal asn difference amount. */
    private String bdAsnDifferenceAmount;
    
    /** The tax rate */
    private String taxRate;
    
    /** The big decimal tax rate */
    private BigDecimal bdTaxRate;
    
    /** The tax type */
    private String taxType;
    
    /** The cn running no. */
    private String cnRunningNo;
    
    /** The timestamp cn date. */
    private Timestamp tsCnDate;
    
    /** The cn amount. */
    private String cnAmount;
    
    /** The big decimal cn amount. */
    private Timestamp bdCnAmount;
    
    /** The cn base amount */
    private String cnBaseAmount;
    
    /** The big decimal cn base amount */
    private Timestamp bdCnBaseAmount;
    
    /** The cn vat amount */
    private String cnVatAmount;
    
    /** The big decimal cn vat amount */
    private Timestamp bdCnVatAmount;
    
    /** The cn total amount */
    private String cnTotalAmount;
    
    /** The big decimal cn total amount */
    private Timestamp bdCnTotalAmount;
    
    /**
     * Instantiates a new Invoice Cover Page Report domain.
     */
    public InvoiceCoverPageReportDomain() {
        super();
    }
    
    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for densoTaxId.</p>
     *
     * @return the densoTaxId
     */
    public String getDensoTaxId() {
        return densoTaxId;
    }

    /**
     * <p>Setter method for densoTaxId.</p>
     *
     * @param densoTaxId Set for densoTaxId
     */
    public void setDensoTaxId(String densoTaxId) {
        this.densoTaxId = densoTaxId;
    }
    
    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }
    
    /**
     * <p>Getter method for cnBaseAmount.</p>
     *
     * @return the cnBaseAmount
     */
    public String getCnBaseAmount() {
        return cnBaseAmount;
    }

    /**
     * <p>Setter method for cnBaseAmount.</p>
     *
     * @param cnBaseAmount Set for cnBaseAmount
     */
    public void setCnBaseAmount(String cnBaseAmount) {
        this.cnBaseAmount = cnBaseAmount;
    }

    /**
     * <p>Getter method for cnVatAmount.</p>
     *
     * @return the cnVatAmount
     */
    public String getCnVatAmount() {
        return cnVatAmount;
    }

    /**
     * <p>Setter method for cnVatAmount.</p>
     *
     * @param cnVatAmount Set for cnVatAmount
     */
    public void setCnVatAmount(String cnVatAmount) {
        this.cnVatAmount = cnVatAmount;
    }

    /**
     * <p>Getter method for cnTotalAmount.</p>
     *
     * @return the cnTotalAmount
     */
    public String getCnTotalAmount() {
        return cnTotalAmount;
    }

    /**
     * <p>Setter method for cnTotalAmount.</p>
     *
     * @param cnTotalAmount Set for cnTotalAmount
     */
    public void setCnTotalAmount(String cnTotalAmount) {
        this.cnTotalAmount = cnTotalAmount;
    }

    /**
     * <p>Getter method for pageNo.</p>
     *
     * @return the pageNo
     */
    public String getPageNo() {
        return pageNo;
    }

    /**
     * <p>Setter method for pageNo.</p>
     *
     * @param pageNo Set for pageNo
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * <p>Getter method for coverPageNo.</p>
     *
     * @return the coverPageNo
     */
    public String getCoverPageNo() {
        return coverPageNo;
    }

    /**
     * <p>Setter method for coverPageNo.</p>
     *
     * @param coverPageNo Set for coverPageNo
     */
    public void setCoverPageNo(String coverPageNo) {
        this.coverPageNo = coverPageNo;
    }

    /**
     * <p>Getter method for companySupplierName.</p>
     *
     * @return the companySupplierName
     */
    public String getCompanySupplierName() {
        return companySupplierName;
    }

    /**
     * <p>Setter method for companySupplierName.</p>
     *
     * @param companySupplierName Set for companySupplierName
     */
    public void setCompanySupplierName(String companySupplierName) {
        this.companySupplierName = companySupplierName;
    }

    /**
     * <p>Getter method for invoiceCurrency.</p>
     *
     * @return the invoiceCurrency
     */
    public String getInvoiceCurrency() {
        return invoiceCurrency;
    }

    /**
     * <p>Setter method for invoiceCurrency.</p>
     *
     * @param invoiceCurrency Set for invoiceCurrency
     */
    public void setInvoiceCurrency(String invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    /**
     * <p>Getter method for companyDensoName.</p>
     *
     * @return the companyDensoName
     */
    public String getCompanyDensoName() {
        return companyDensoName;
    }

    /**
     * <p>Setter method for companyDensoName.</p>
     *
     * @param companyDensoName Set for companyDensoName
     */
    public void setCompanyDensoName(String companyDensoName) {
        this.companyDensoName = companyDensoName;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for invoiceRunningNo.</p>
     *
     * @return the invoiceRunningNo
     */
    public String getInvoiceRunningNo() {
        return invoiceRunningNo;
    }

    /**
     * <p>Setter method for invoiceRunningNo.</p>
     *
     * @param invoiceRunningNo Set for invoiceRunningNo
     */
    public void setInvoiceRunningNo(String invoiceRunningNo) {
        this.invoiceRunningNo = invoiceRunningNo;
    }

    /**
     * <p>Getter method for invoiceAmount.</p>
     *
     * @return the invoiceAmount
     */
    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * <p>Setter method for invoiceAmount.</p>
     *
     * @param invoiceAmount Set for invoiceAmount
     */
    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    /**
     * <p>Getter method for invoiceBaseAmount.</p>
     *
     * @return the invoiceBaseAmount
     */
    public String getInvoiceBaseAmount() {
        return invoiceBaseAmount;
    }

    /**
     * <p>Setter method for invoiceBaseAmount.</p>
     *
     * @param invoiceBaseAmount Set for invoiceBaseAmount
     */
    public void setInvoiceBaseAmount(String invoiceBaseAmount) {
        this.invoiceBaseAmount = invoiceBaseAmount;
    }

    /**
     * <p>Getter method for invoiceVatAmount.</p>
     *
     * @return the invoiceVatAmount
     */
    public String getInvoiceVatAmount() {
        return invoiceVatAmount;
    }

    /**
     * <p>Setter method for invoiceVatAmount.</p>
     *
     * @param invoiceVatAmount Set for invoiceVatAmount
     */
    public void setInvoiceVatAmount(String invoiceVatAmount) {
        this.invoiceVatAmount = invoiceVatAmount;
    }

    /**
     * <p>Getter method for bdInvoiceVatAmount.</p>
     *
     * @return the bdInvoiceVatAmount
     */
    public BigDecimal getBdInvoiceVatAmount() {
        return bdInvoiceVatAmount;
    }

    /**
     * <p>Setter method for bdInvoiceVatAmount.</p>
     *
     * @param bdInvoiceVatAmount Set for bdInvoiceVatAmount
     */
    public void setBdInvoiceVatAmount(BigDecimal bdInvoiceVatAmount) {
        this.bdInvoiceVatAmount = bdInvoiceVatAmount;
    }

    /**
     * <p>Getter method for invoiceTotalAmount.</p>
     *
     * @return the invoiceTotalAmount
     */
    public String getInvoiceTotalAmount() {
        return invoiceTotalAmount;
    }

    /**
     * <p>Setter method for invoiceTotalAmount.</p>
     *
     * @param invoiceTotalAmount Set for invoiceTotalAmount
     */
    public void setInvoiceTotalAmount(String invoiceTotalAmount) {
        this.invoiceTotalAmount = invoiceTotalAmount;
    }

    /**
     * <p>Getter method for bdInvoiceTotalAmount.</p>
     *
     * @return the bdInvoiceTotalAmount
     */
    public BigDecimal getBdInvoiceTotalAmount() {
        return bdInvoiceTotalAmount;
    }

    /**
     * <p>Setter method for bdInvoiceTotalAmount.</p>
     *
     * @param bdInvoiceTotalAmount Set for bdInvoiceTotalAmount
     */
    public void setBdInvoiceTotalAmount(BigDecimal bdInvoiceTotalAmount) {
        this.bdInvoiceTotalAmount = bdInvoiceTotalAmount;
    }
    
    /**
     * <p>Getter method for asnBaseAmount.</p>
     *
     * @return the asnBaseAmount
     */
    public String getAsnBaseAmount() {
        return asnBaseAmount;
    }

    /**
     * <p>Setter method for asnBaseAmount.</p>
     *
     * @param asnBaseAmount Set for asnBaseAmount
     */
    public void setAsnBaseAmount(String asnBaseAmount) {
        this.asnBaseAmount = asnBaseAmount;
    }

    /**
     * <p>Getter method for asnDifferenceAmount.</p>
     *
     * @return the asnDifferenceAmount
     */
    public String getAsnDifferenceAmount() {
        return asnDifferenceAmount;
    }

    /**
     * <p>Setter method for asnDifferenceAmount.</p>
     *
     * @param asnDifferenceAmount Set for asnDifferenceAmount
     */
    public void setAsnDifferenceAmount(String asnDifferenceAmount) {
        this.asnDifferenceAmount = asnDifferenceAmount;
    }

    /**
     * <p>Getter method for taxRate.</p>
     *
     * @return the taxRate
     */
    public String getTaxRate() {
        return taxRate;
    }

    /**
     * <p>Setter method for taxRate.</p>
     *
     * @param taxRate Set for taxRate
     */
    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * <p>Getter method for taxType.</p>
     *
     * @return the taxType
     */
    public String getTaxType() {
        return taxType;
    }

    /**
     * <p>Setter method for taxType.</p>
     *
     * @param taxType Set for taxType
     */
    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    /**
     * <p>Getter method for cnRunningNo.</p>
     *
     * @return the cnRunningNo
     */
    public String getCnRunningNo() {
        return cnRunningNo;
    }

    /**
     * <p>Setter method for cnRunningNo.</p>
     *
     * @param cnRunningNo Set for cnRunningNo
     */
    public void setCnRunningNo(String cnRunningNo) {
        this.cnRunningNo = cnRunningNo;
    }

    /**
     * <p>Getter method for cnAmount.</p>
     *
     * @return the cnAmount
     */
    public String getCnAmount() {
        return cnAmount;
    }

    /**
     * <p>Setter method for cnAmount.</p>
     *
     * @param cnAmount Set for cnAmount
     */
    public void setCnAmount(String cnAmount) {
        this.cnAmount = cnAmount;
    }

    /**
     * <p>Getter method for tsInvoiceRunningNo.</p>
     *
     * @return the tsInvoiceRunningNo
     */
    public Timestamp getTsInvoiceRunningNo() {
        return tsInvoiceRunningNo;
    }

    /**
     * <p>Setter method for tsInvoiceRunningNo.</p>
     *
     * @param tsInvoiceRunningNo Set for tsInvoiceRunningNo
     */
    public void setTsInvoiceRunningNo(Timestamp tsInvoiceRunningNo) {
        this.tsInvoiceRunningNo = tsInvoiceRunningNo;
    }

    /**
     * <p>Getter method for tsInvoiceDate.</p>
     *
     * @return the tsInvoiceDate
     */
    public Timestamp getTsInvoiceDate() {
        return tsInvoiceDate;
    }

    /**
     * <p>Setter method for tsInvoiceDate.</p>
     *
     * @param tsInvoiceDate Set for tsInvoiceDate
     */
    public void setTsInvoiceDate(Timestamp tsInvoiceDate) {
        this.tsInvoiceDate = tsInvoiceDate;
    }

    /**
     * <p>Getter method for bdInvoiceAmount.</p>
     *
     * @return the bdInvoiceAmount
     */
    public BigDecimal getBdInvoiceAmount() {
        return bdInvoiceAmount;
    }

    /**
     * <p>Setter method for bdInvoiceAmount.</p>
     *
     * @param bdInvoiceAmount Set for bdInvoiceAmount
     */
    public void setBdInvoiceAmount(BigDecimal bdInvoiceAmount) {
        this.bdInvoiceAmount = bdInvoiceAmount;
    }

    /**
     * <p>Getter method for bdInvoiceBaseAmount.</p>
     *
     * @return the bdInvoiceBaseAmount
     */
    public BigDecimal getBdInvoiceBaseAmount() {
        return bdInvoiceBaseAmount;
    }

    /**
     * <p>Setter method for bdInvoiceBaseAmount.</p>
     *
     * @param bdInvoiceBaseAmount Set for bdInvoiceBaseAmount
     */
    public void setBdInvoiceBaseAmount(BigDecimal bdInvoiceBaseAmount) {
        this.bdInvoiceBaseAmount = bdInvoiceBaseAmount;
    }

    /**
     * <p>Getter method for tsAsnDate.</p>
     *
     * @return the tsAsnDate
     */
    public Timestamp getTsAsnDate() {
        return tsAsnDate;
    }

    /**
     * <p>Setter method for tsAsnDate.</p>
     *
     * @param tsAsnDate Set for tsAsnDate
     */
    public void setTsAsnDate(Timestamp tsAsnDate) {
        this.tsAsnDate = tsAsnDate;
    }

    /**
     * <p>Getter method for bdAsnBaseAmount.</p>
     *
     * @return the bdAsnBaseAmount
     */
    public BigDecimal getBdAsnBaseAmount() {
        return bdAsnBaseAmount;
    }

    /**
     * <p>Setter method for bdAsnBaseAmount.</p>
     *
     * @param bdAsnBaseAmount Set for bdAsnBaseAmount
     */
    public void setBdAsnBaseAmount(BigDecimal bdAsnBaseAmount) {
        this.bdAsnBaseAmount = bdAsnBaseAmount;
    }

    /**
     * <p>Getter method for bdAsnDifferenceAmount.</p>
     *
     * @return the bdAsnDifferenceAmount
     */
    public String getBdAsnDifferenceAmount() {
        return bdAsnDifferenceAmount;
    }

    /**
     * <p>Setter method for bdAsnDifferenceAmount.</p>
     *
     * @param bdAsnDifferenceAmount Set for bdAsnDifferenceAmount
     */
    public void setBdAsnDifferenceAmount(String bdAsnDifferenceAmount) {
        this.bdAsnDifferenceAmount = bdAsnDifferenceAmount;
    }

    /**
     * <p>Getter method for bdTaxRate.</p>
     *
     * @return the bdTaxRate
     */
    public BigDecimal getBdTaxRate() {
        return bdTaxRate;
    }

    /**
     * <p>Setter method for bdTaxRate.</p>
     *
     * @param bdTaxRate Set for bdTaxRate
     */
    public void setBdTaxRate(BigDecimal bdTaxRate) {
        this.bdTaxRate = bdTaxRate;
    }

    /**
     * <p>Getter method for tsCnDate.</p>
     *
     * @return the tsCnDate
     */
    public Timestamp getTsCnDate() {
        return tsCnDate;
    }

    /**
     * <p>Setter method for tsCnDate.</p>
     *
     * @param tsCnDate Set for tsCnDate
     */
    public void setTsCnDate(Timestamp tsCnDate) {
        this.tsCnDate = tsCnDate;
    }

    /**
     * <p>Getter method for bdCnAmount.</p>
     *
     * @return the bdCnAmount
     */
    public Timestamp getBdCnAmount() {
        return bdCnAmount;
    }

    /**
     * <p>Setter method for bdCnAmount.</p>
     *
     * @param bdCnAmount Set for bdCnAmount
     */
    public void setBdCnAmount(Timestamp bdCnAmount) {
        this.bdCnAmount = bdCnAmount;
    }

    /**
     * <p>Getter method for bdCnBaseAmount.</p>
     *
     * @return the bdCnBaseAmount
     */
    public Timestamp getBdCnBaseAmount() {
        return bdCnBaseAmount;
    }

    /**
     * <p>Setter method for bdCnBaseAmount.</p>
     *
     * @param bdCnBaseAmount Set for bdCnBaseAmount
     */
    public void setBdCnBaseAmount(Timestamp bdCnBaseAmount) {
        this.bdCnBaseAmount = bdCnBaseAmount;
    }

    /**
     * <p>Getter method for bdCnVatAmount.</p>
     *
     * @return the bdCnVatAmount
     */
    public Timestamp getBdCnVatAmount() {
        return bdCnVatAmount;
    }

    /**
     * <p>Setter method for bdCnVatAmount.</p>
     *
     * @param bdCnVatAmount Set for bdCnVatAmount
     */
    public void setBdCnVatAmount(Timestamp bdCnVatAmount) {
        this.bdCnVatAmount = bdCnVatAmount;
    }

    /**
     * <p>Getter method for bdCnTotalAmount.</p>
     *
     * @return the bdCnTotalAmount
     */
    public Timestamp getBdCnTotalAmount() {
        return bdCnTotalAmount;
    }

    /**
     * <p>Setter method for bdCnTotalAmount.</p>
     *
     * @param bdCnTotalAmount Set for bdCnTotalAmount
     */
    public void setBdCnTotalAmount(Timestamp bdCnTotalAmount) {
        this.bdCnTotalAmount = bdCnTotalAmount;
    }

    /**
     * <p>Getter method for flag.</p>
     *
     * @return the flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * <p>Setter method for flag.</p>
     *
     * @param flag Set for flag
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * <p>Getter method for supplierAddress1.</p>
     *
     * @return the supplierAddress1
     */
    public String getSupplierAddress1() {
        return supplierAddress1;
    }

    /**
     * <p>Setter method for supplierAddress1.</p>
     *
     * @param supplierAddress1 Set for supplierAddress1
     */
    public void setSupplierAddress1(String supplierAddress1) {
        this.supplierAddress1 = supplierAddress1;
    }

    /**
     * <p>Getter method for supplierAddress2.</p>
     *
     * @return the supplierAddress2
     */
    public String getSupplierAddress2() {
        return supplierAddress2;
    }

    /**
     * <p>Setter method for supplierAddress2.</p>
     *
     * @param supplierAddress2 Set for supplierAddress2
     */
    public void setSupplierAddress2(String supplierAddress2) {
        this.supplierAddress2 = supplierAddress2;
    }

    /**
     * <p>Getter method for supplierAddress3.</p>
     *
     * @return the supplierAddress3
     */
    public String getSupplierAddress3() {
        return supplierAddress3;
    }

    /**
     * <p>Setter method for supplierAddress3.</p>
     *
     * @param supplierAddress3 Set for supplierAddress3
     */
    public void setSupplierAddress3(String supplierAddress3) {
        this.supplierAddress3 = supplierAddress3;
    }

    /**
     * <p>Getter method for documentType.</p>
     *
     * @return the documentType
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * <p>Setter method for documentType.</p>
     *
     * @param documentType Set for documentType
     */
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    /**
     * <p>Getter method for documentDate.</p>
     *
     * @return the documentDate
     */
    public String getDocumentDate() {
        return documentDate;
    }

    /**
     * <p>Setter method for documentDate.</p>
     *
     * @param documentDate Set for documentDate
     */
    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    /**
     * <p>Getter method for documentNo.</p>
     *
     * @return the documentNo
     */
    public String getDocumentNo() {
        return documentNo;
    }

    /**
     * <p>Setter method for documentNo.</p>
     *
     * @param documentNo Set for documentNo
     */
    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    /**
     * <p>Getter method for densoAddress.</p>
     *
     * @return the densoAddress
     */
    public String getDensoAddress() {
        return densoAddress;
    }

    /**
     * <p>Setter method for densoAddress.</p>
     *
     * @param densoAddress Set for densoAddress
     */
    public void setDensoAddress(String densoAddress) {
        this.densoAddress = densoAddress;
    }

}