/*
 * ModifyDate Development company Describe 
 * 2014/07/03 CSI Akat           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain;

/**
 * The Class RoleScreenDomain. Domain for mapping between Role and Screen.
 * 
 * @author CSI
 * @version 1.00
 */
public class RoleScreenDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -7849259585129279916L;

    /** The screen that user has role. */
    private SpsMScreenDomain spsMScreenDomain;
    
    /** List of Role User. */
    private List<UserRoleDomain> userRoleDomainList;
    
    /** The default constructor. */
    public RoleScreenDomain() {
        super();
        this.spsMScreenDomain = new SpsMScreenDomain();
        this.userRoleDomainList = new ArrayList<UserRoleDomain>();
    }

    /**
     * <p>Getter method for spsMScreenDomain.</p>
     *
     * @return the spsMScreenDomain
     */
    public SpsMScreenDomain getSpsMScreenDomain() {
        return spsMScreenDomain;
    }

    /**
     * <p>Setter method for spsMScreenDomain.</p>
     *
     * @param spsMScreenDomain Set for spsMScreenDomain
     */
    public void setSpsMScreenDomain(SpsMScreenDomain spsMScreenDomain) {
        this.spsMScreenDomain = spsMScreenDomain;
    }

    /**
     * Get method for list of user role domain.
     * @return the list of user role domain.
     */
    public List<UserRoleDomain> getUserRoleDomainList() {
        return userRoleDomainList;
    }

    /**
     * Set method for list of user role domain.
     * @param userRoleDomainList the list of user role domain.
     */
    public void setUserRoleDomainList(List<UserRoleDomain> userRoleDomainList) {
        this.userRoleDomainList = userRoleDomainList;
    }

}
