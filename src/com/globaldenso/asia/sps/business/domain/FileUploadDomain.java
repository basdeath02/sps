/*
 * ModifyDate Development company     Describe 
 * 2014/06/11 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class FileUploadDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = 3326568522154732562L;

    /** The file upload ID. */
    private Integer fileUploadId;
    
    /** The supplier code */
    private String sCd;
    
    /** The vendor code */
    private String vendorCd;
    
    /** The supplier plant code.*/
    private String sPcd;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The file name.*/
    private String fileName;
    
    /** The file comment. */
    private String comment; 
    
    /** The file ID of file manager. */
    private String fileId; 
    
    /** The create DSC ID. */
    private String createDscId; 
    
    /** The create date time. */
    private Timestamp createDateTime; 
    
    /** The last update DSC ID. */
    private String lastUpdateDscId;
    
    /** The last update date time. */
    private Timestamp lastUpdateDateTime;
    
    /** The last update date. */
    private String lastUpdateDate;
    
    /** The Application Message Domain. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The DENSO code. */
    private String dCd;
    
    /** The DENSO plant code. */
    private String dPcd;
    
    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private String createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private String createDatetimeLessThanEqual;
    
    /**
     * Instantiates a new file upload domain.
     */
    public FileUploadDomain() {
        super();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
    }
     
    /**
     * <p>Getter method for errorMessageList.</p>
     *
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * <p>Setter method for errorMessageList.</p>
     *
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }
    
    /**
     * Gets the file upload ID.
     * 
     * @return the file upload ID.
     */
    public Integer getFileUploadId() {
        return fileUploadId;
    }
    
    /**
     * Sets the file upload ID.
     * 
     * @param fileUploadId the file upload ID.
     */  
    public void setFileUploadId(Integer fileUploadId) {
        this.fileUploadId = fileUploadId;
    }
    
    /**
     * Gets the supplier code.
     * 
     * @return the supplier code.
     */
    public String getSCd() {
        return sCd;
    }
    
    /**
     * Sets the supplier code.
     * 
     * @param sCd the supplier code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * Gets the file name.
     * 
     * @return the file name.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the file name.
     * 
     * @param fileName the file name.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets the file comment.
     * 
     * @return the file comment.
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the file comment.
     * 
     * @param comment the file comment.
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Gets the file ID of file manager.
     * 
     * @return the file ID of file manager.
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Sets the file ID of file manager.
     * 
     * @param fileId the file ID of file manager.
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * Gets the create DSC ID.
     * 
     * @return the create DSC ID.
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Sets the create DSC ID.
     * 
     * @param createDscId the create DSC ID.
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Gets the create date time.
     * 
     * @return the create date time.
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }
    
    /**
     * Sets the create date time.
     * 
     * @param createDateTime the create date time.
     */

    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * Gets the last update DSC ID.
     * 
     * @return the last update DSC ID.
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Sets the last update DSC ID.
     * 
     * @param lastUpdateDscId the last update DSC ID.
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Gets the last update date time.
     * 
     * @return the last update date time.
     */
    public Timestamp getLastUpdateDateTime(){
        return lastUpdateDateTime;
    }
    
    /** Sets the last update date time.
     * 
     * @param lastUpdateDateTime the last update date time.
     */
    public void setLastUpdateDateTime(Timestamp lastUpdateDateTime){
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

    /**
     * <p>Getter method for lastUpdateDate.</p>
     *
     * @return the lastUpdateDate
     */
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * <p>Setter method for lastUpdateDate.</p>
     *
     * @param lastUpdateDate Set for lastUpdateDate
     */
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    /**
     * <p>Getter method for createDatetimeGreaterThanEqual.</p>
     *
     * @return the createDatetimeGreaterThanEqual
     */
    public String getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * <p>Setter method for createDatetimeGreaterThanEqual.</p>
     *
     * @param createDatetimeGreaterThanEqual Set for createDatetimeGreaterThanEqual
     */
    public void setCreateDatetimeGreaterThanEqual(
        String createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * <p>Getter method for createDatetimeLessThanEqual.</p>
     *
     * @return the createDatetimeLessThanEqual
     */
    public String getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * <p>Setter method for createDatetimeLessThanEqual.</p>
     *
     * @param createDatetimeLessThanEqual Set for createDatetimeLessThanEqual
     */
    public void setCreateDatetimeLessThanEqual(String createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }
    
    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
}   
    