/*
 * ModifyDate Development company    Describe 
 * 2014/08/15 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class AsnUploadingDomain.
 * @author CSI
 */
public class AsnUploadingDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5559956350562806449L;

    /** The file data. */
    private InputStream fileInputStream;
    
    /** The upload dsc id. */
    private String uploadDscId;
    
    /** The session id. */
    private String sessionId;
    
    /** The asn no. */
    private String asnNo;
    
    /** The vendor cd. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The correct record. */
    private int correctRecord;
    
    /** The error record. */
    private int errorRecord;
    
    /** The warning record. */
    private int warningRecord;
    
    /** The asn detail domain. */
    private int totalRecord;
    
    /** The asn information for csv. */
    private StringBuffer asnInformationForCsv;
    
    /** The data scope control domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The tmp upload asn result list. */
    private List<AsnUploadingReturnDomain> tmpUploadAsnResultList;
    
    /**
     * Instantiates a new ASN info domain.
     */
    public AsnUploadingDomain() {
        super();
    }
    
    /**
     * Gets the file input stream.
     * 
     * @return the file input stream..
     */
    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    /**
     * Sets the file input stream..
     * 
     * @param fileInputStream the file input stream..
     */
    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    /**
     * Gets the session id.
     * 
     * @return the session id.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the session id.
     * 
     * @param sessionId the session id.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Gets the upload dsc id.
     * 
     * @return the upload dsc id
     */
    public String getUploadDscId() {
        return uploadDscId;
    }

    /**
     * Sets the upload dsc id.
     * 
     * @param uploadDscId the upload dsc id
     */
    public void setUploadDscId(String uploadDscId) {
        this.uploadDscId = uploadDscId;
    }
    
    /**
     * Gets the asn no.
     * 
     * @return the asn no
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Sets the asn no.
     * 
     * @param asnNo the asn no
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Gets the vendor cd.
     * 
     * @return the vendor cd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Sets the vendor cd.
     * 
     * @param vendorCd the vendor cd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso code.
     * 
     * @return the denso code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso code.
     * 
     * @param dCd the denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the denso plant code.
     * 
     * @return the denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the denso plant code.
     * 
     * @param dPcd the denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the correct record.
     * 
     * @return the correct record
     */
    public int getCorrectRecord() {
        return correctRecord;
    }

    /**
     * Sets the correct record.
     * 
     * @param correctRecord the correct record
     */
    public void setCorrectRecord(int correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * Gets the error record.
     * 
     * @return the error record
     */
    public int getErrorRecord() {
        return errorRecord;
    }

    /**
     * Sets the error record.
     * 
     * @param errorRecord the error record
     */
    public void setErrorRecord(int errorRecord) {
        this.errorRecord = errorRecord;
    }

    /**
     * Gets the warning record.
     * 
     * @return the warning record
     */
    public int getWarningRecord() {
        return warningRecord;
    }

    /**
     * Sets the warning record.
     * 
     * @param warningRecord the warning record
     */
    public void setWarningRecord(int warningRecord) {
        this.warningRecord = warningRecord;
    }
    
    /**
     * Gets the total record.
     * 
     * @return the total record
     */
    public int getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the total record.
     * 
     * @param totalRecord the total record
     */
    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }
    
    /**
     * Gets the asn information for csv.
     * 
     * @return the asn information for csv
     */
    public StringBuffer getAsnInformationForCsv() {
        return asnInformationForCsv;
    }

    /**
     * Sets the asn information for csv.
     * 
     * @param asnInformationForCsv the asn information for csv
     */
    public void setAsnInformationForCsv(StringBuffer asnInformationForCsv) {
        this.asnInformationForCsv = asnInformationForCsv;
    }

    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain
     */
    public void setDataScopeControlDomain(DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }

    /**
     * Gets the error message list.
     * 
     * @return the error message list
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list
     */
    public void setWarningMessageList(List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Gets the temp upload asn result list.
     * 
     * @return the temp upload asn result list
     */
    public List<AsnUploadingReturnDomain> getTmpUploadAsnResultList() {
        return tmpUploadAsnResultList;
    }

    /**
     * Sets the temp upload asn result list.
     * 
     * @param tmpUploadAsnResultList the temp upload asn result list
     */
    public void setTmpUploadAsnResultList(List<AsnUploadingReturnDomain> tmpUploadAsnResultList) {
        this.tmpUploadAsnResultList = tmpUploadAsnResultList;
    }
}