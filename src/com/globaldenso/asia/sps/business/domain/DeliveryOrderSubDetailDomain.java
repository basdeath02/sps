/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai            Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * <p>
 * Delivery Order Sub Detail Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class DeliveryOrderSubDetailDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serializable.
     * </p>
     */
    private static final long serialVersionUID = -3601461991915324495L;

    /**
     * <p>
     * The doId
     * </p>
     */
    private String doId;

    /**
     * <p>
     * The spsDoNo
     * </p>
     */
    private String spsDoNo;

    /**
     * <p>
     * The cigmaDoNo
     * </p>
     */
    private String cigmaDoNo;

    /**
     * Current SPS D/O No.
     */
    private String currentSpsDoNo;
    
    /**
     * Current CIGMA D/O No.
     */
    private String currentCigmaDoNo;

    /**
     * Previous SPS D/O No.
     */
    private String previousSpsDoNo;

    /**
     * <p>
     * The revision
     * </p>
     */
    private String revision;

    /**
     * <p>
     * The doIssueDate
     * </p>
     */
    private Date doIssueDate;
    
    // FIX : wrong dateformat
    /** The formated D/O Issue date to show in screen */
    private String doIssueDateShow;
    
    /**
     * <p>
     * The shipmentStatus
     * </p>
     */
    private String shipmentStatus;

    /**
     * <p>
     * The sCd
     * </p>
     */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /**
     * <p>
     * The sPcd
     * </p>
     */
    private String sPcd;

    /**
     * <p>
     * The DENSOCode
     * </p>
     */
    private String dCd;

    /**
     * <p>
     * The DENSOPlantCode
     * </p>
     */
    private String dPcd;

    /**
     * <p>
     * The tm
     * </p>
     */
    private String tm;

    /**
     * <p>
     * The dataType
     * </p>
     */
    private String dataType;

    /**
     * <p>
     * The orderMethod
     * </p>
     */
    private String orderMethod;

    /**
     * <p>
     * The shipDatetime
     * </p>
     */
    private Timestamp shipDatetime;

    /**
     * <p>
     * The Ship Date Time Utc.
     * </p>
     */
    private String shipDatetimeUtc;

    /**
     * <p>
     * The deliveryDatetime
     * </p>
     */
    private Timestamp deliveryDatetime;

    /**
     * <p>
     * The Delivery Date Time Utc.
     * </p>
     */
    private String deliveryDatetimeUtc;

    /**
     * <p>
     * The pdfFileId
     * </p>
     */
    private String pdfFileId;
    
    /**
     * <p>
     * The pdfFileKbTagPrintDate
     * </p>
     */
    private String pdfFileKbTagPrintDate;

    /**
     * <p>
     * The truckRoute
     * </p>
     */
    private String truckRoute;

    /**
     * <p>
     * The del
     * </p>
     */
    private String del;

    /**
     * <p>
     * The poId
     * </p>
     */
    private String poId;

    /**
     * <p>
     * The whPrimeReceiving
     * </p>
     */
    private String whPrimeReceiving;

    /**
     * <p>
     * The whLocation
     * </p>
     */
    private String whLocation;

    /**
     * <p>
     * The lastRevisionFlg
     * </p>
     */
    private String lastRevisionFlg;

    /**
     * <p>
     * The urgentOrderFlg
     * </p>
     */
    private String urgentOrderFlg;

    /**
     * <p>
     * The clearUrgentDate
     * </p>
     */
    private Date clearUrgentDate;

    /**
     * <p>
     * The newTruckRouteFlg
     * </p>
     */
    private String newTruckRouteFlg;

    /**
     * <p>
     * The createDscId
     * </p>
     */
    private String createDscId;

    /**
     * <p>
     * The createDatetime
     * </p>
     */
    private Timestamp createDatetime;

    /**
     * <p>
     * The lastUpdateDscId
     * </p>
     */
    private String lastUpdateDscId;

    /**
     * <p>
     * The lastUpdateDatetime
     * </p>
     */
    private Timestamp lastUpdateDatetime;

    /**
     * <p>
     * The dockCode
     * </p>
     */
    private String dockCode;

    /**
     * <p>
     * The cycle
     * </p>
     */
    private String cycle;

    /**
     * <p>
     * The tripNo
     * </p>
     */
    private String tripNo;

    /**
     * <p>
     * The backlog
     * </p>
     */
    private String backlog;

    /**
     * <p>
     * The receivingGate
     * </p>
     */
    private String receivingGate;

    /**
     * <p>
     * The utc
     * </p>
     */
    private String utc;

    /**
     * <p>
     * The utc
     * </p>
     */
    private BigDecimal asnNoList;

    /**
     * The Truck Sequence
     */
    private BigDecimal truckSeq;

    /**
     * The Supplier Company Name
     */
    private String supplierCompanyName;

    /**
     * The Denso Company Name
     */
    private String densoCompanyName;

    /**
     * The Supplier Location
     */
    private String supplierLocation;

    /**
     * The Rcv Lane
     */
    private String rcvLane;    
    /** 
     * The receiveByScan. 
     */
    private String receiveByScan;    
    
    /** 
     * The isPrintOneWayKanbanTag. 
     */
    private String isPrintOneWayKanbanTag;
    
    /** 
     * The oneWayKanbanTagFlag. 
     */
    private String oneWayKanbanTagFlag;
    
    /** 
     * The tagOutput. 
     */
    private String tagOutput;
    
    /** 
     * The tagOutput. 
     */
    private Integer partTag;
    
    /**
     * <p>
     * The shippingTotal
     * </p>
     */
    private BigDecimal shippingTotal;
    
    /**
     * <p>
     * The shipmentStatus
     * </p>
     */
    private String receiveStatus;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public DeliveryOrderSubDetailDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for doId.
     * </p>
     * 
     * @return The doId
     */
    public String getDoId() {
        return doId;
    }

    /**
     * <p>
     * Setter method for doId.
     * </p>
     * 
     * @param doId Set for doId
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * <p>
     * Getter method for spsDoNo.
     * </p>
     * 
     * @return The spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>
     * Setter method for spsDoNo.
     * </p>
     * 
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>
     * Getter method for cigmaDoNo.
     * </p>
     * 
     * @return The cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>
     * Setter method for cigmaDoNo.
     * </p>
     * 
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return The revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>
     * Getter method for doIssueDate.
     * </p>
     * 
     * @return The doIssueDate
     */
    public Date getDoIssueDate() {
        return doIssueDate;
    }

    /**
     * <p>
     * Setter method for doIssueDate.
     * </p>
     * 
     * @param doIssueDate Set for doIssueDate
     */
    public void setDoIssueDate(Date doIssueDate) {
        this.doIssueDate = doIssueDate;
    }

    /**
     * <p>
     * Getter method for shipmentStatus.
     * </p>
     * 
     * @return The shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * <p>
     * Setter method for shipmentStatus.
     * </p>
     * 
     * @param shipmentStatus Set for shipmentStatus
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return The sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return The sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for dCd.
     * </p>
     * 
     * @return The dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for dCd.
     * </p>
     * 
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return The dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for tm.
     * </p>
     * 
     * @return The tm
     */
    public String getTm() {
        return tm;
    }

    /**
     * <p>
     * Setter method for tm.
     * </p>
     * 
     * @param tm Set for tm
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * <p>
     * Getter method for dataType.
     * </p>
     * 
     * @return The dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * <p>
     * Setter method for dataType.
     * </p>
     * 
     * @param dataType Set for dataType
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * <p>
     * Getter method for orderMethod.
     * </p>
     * 
     * @return The orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * <p>
     * Setter method for orderMethod.
     * </p>
     * 
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * <p>
     * Getter method for shipDatetime.
     * </p>
     * 
     * @return The shipDatetime
     */
    public Timestamp getShipDatetime() {
        return shipDatetime;
    }

    /**
     * <p>
     * Setter method for shipDatetime.
     * </p>
     * 
     * @param shipDatetime Set for shipDatetime
     */
    public void setShipDatetime(Timestamp shipDatetime) {
        this.shipDatetime = shipDatetime;
    }

    /**
     * <p>
     * Getter method for deliveryDatetime.
     * </p>
     * 
     * @return The deliveryDatetime
     */
    public Timestamp getDeliveryDatetime() {
        return deliveryDatetime;
    }

    /**
     * <p>
     * Setter method for deliveryDatetime.
     * </p>
     * 
     * @param deliveryDatetime Set for deliveryDatetime
     */
    public void setDeliveryDatetime(Timestamp deliveryDatetime) {
        this.deliveryDatetime = deliveryDatetime;
    }

    /**
     * <p>
     * Getter method for pdfFileId.
     * </p>
     * 
     * @return The pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>
     * Setter method for pdfFileId.
     * </p>
     * 
     * @param pdfFileId Set for pdfFileId
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * <p>
     * Getter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @return The pdfFileKbTagPrintDate
     */
    public String getPdfFileKbTagPrintDate() {
        return pdfFileKbTagPrintDate;
    }

    /**
     * <p>
     * Setter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @param pdfFileKbTagPrintDate Set for pdfFileKbTagPrintDate
     */
    public void setPdfFileKbTagPrintDate(String pdfFileKbTagPrintDate) {
        this.pdfFileKbTagPrintDate = pdfFileKbTagPrintDate;
    }
    
    /**
     * <p>
     * Getter method for truckRoute.
     * </p>
     * 
     * @return The truckRoute
     */
    public String getTruckRoute() {
        return truckRoute;
    }

    /**
     * <p>
     * Setter method for truckRoute.
     * </p>
     * 
     * @param truckRoute Set for truckRoute
     */
    public void setTruckRoute(String truckRoute) {
        this.truckRoute = truckRoute;
    }

    /**
     * <p>
     * Getter method for del.
     * </p>
     * 
     * @return The del
     */
    public String getDel() {
        return del;
    }

    /**
     * <p>
     * Setter method for del.
     * </p>
     * 
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * <p>
     * Getter method for poId.
     * </p>
     * 
     * @return The poId
     */
    public String getPoId() {
        return poId;
    }

    /**
     * <p>
     * Setter method for poId.
     * </p>
     * 
     * @param poId Set for poId
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }

    /**
     * <p>
     * Getter method for whPrimeReceiving.
     * </p>
     * 
     * @return The whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * <p>
     * Setter method for whPrimeReceiving.
     * </p>
     * 
     * @param whPrimeReceiving Set for whPrimeReceiving
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * <p>
     * Getter method for whLocation.
     * </p>
     * 
     * @return The whLocation
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * <p>
     * Setter method for whLocation.
     * </p>
     * 
     * @param whLocation Set for whLocation
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }

    /**
     * <p>
     * Getter method for lastRevisionFlg.
     * </p>
     * 
     * @return The lastRevisionFlg
     */
    public String getLastRevisionFlg() {
        return lastRevisionFlg;
    }

    /**
     * <p>
     * Setter method for lastRevisionFlg.
     * </p>
     * 
     * @param lastRevisionFlg Set for lastRevisionFlg
     */
    public void setLastRevisionFlg(String lastRevisionFlg) {
        this.lastRevisionFlg = lastRevisionFlg;
    }

    /**
     * <p>
     * Getter method for urgentOrderFlg.
     * </p>
     * 
     * @return The urgentOrderFlg
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * <p>
     * Setter method for urgentOrderFlg.
     * </p>
     * 
     * @param urgentOrderFlg Set for urgentOrderFlg
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * <p>
     * Getter method for clearUrgentDate.
     * </p>
     * 
     * @return The clearUrgentDate
     */
    public Date getClearUrgentDate() {
        return clearUrgentDate;
    }

    /**
     * <p>
     * Setter method for clearUrgentDate.
     * </p>
     * 
     * @param clearUrgentDate Set for clearUrgentDate
     */
    public void setClearUrgentDate(Date clearUrgentDate) {
        this.clearUrgentDate = clearUrgentDate;
    }

    /**
     * <p>
     * Getter method for newTruckRouteFlg.
     * </p>
     * 
     * @return The newTruckRouteFlg
     */
    public String getNewTruckRouteFlg() {
        return newTruckRouteFlg;
    }

    /**
     * <p>
     * Setter method for newTruckRouteFlg.
     * </p>
     * 
     * @param newTruckRouteFlg Set for newTruckRouteFlg
     */
    public void setNewTruckRouteFlg(String newTruckRouteFlg) {
        this.newTruckRouteFlg = newTruckRouteFlg;
    }

    /**
     * <p>
     * Getter method for createDscId.
     * </p>
     * 
     * @return The createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * <p>
     * Setter method for createDscId.
     * </p>
     * 
     * @param createDscId Set for createDscId
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * <p>
     * Getter method for createDatetime.
     * </p>
     * 
     * @return The createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * <p>
     * Setter method for createDatetime.
     * </p>
     * 
     * @param createDatetime Set for createDatetime
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * <p>
     * Getter method for lastUpdateDscId.
     * </p>
     * 
     * @return The lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>
     * Setter method for lastUpdateDscId.
     * </p>
     * 
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>
     * Getter method for lastUpdateDatetime.
     * </p>
     * 
     * @return The lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>
     * Setter method for lastUpdateDatetime.
     * </p>
     * 
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>
     * Getter method for dockCode.
     * </p>
     * 
     * @return The dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * <p>
     * Setter method for dockCode.
     * </p>
     * 
     * @param dockCode Set for dockCode
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * <p>
     * Getter method for cycle.
     * </p>
     * 
     * @return The cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * <p>
     * Setter method for cycle.
     * </p>
     * 
     * @param cycle Set for cycle
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * <p>
     * Getter method for tripNo.
     * </p>
     * 
     * @return The tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * <p>
     * Setter method for tripNo.
     * </p>
     * 
     * @param tripNo Set for tripNo
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * <p>
     * Getter method for backlog.
     * </p>
     * 
     * @return The backlog
     */
    public String getBacklog() {
        return backlog;
    }

    /**
     * <p>
     * Setter method for backlog.
     * </p>
     * 
     * @param backlog Set for backlog
     */
    public void setBacklog(String backlog) {
        this.backlog = backlog;
    }

    /**
     * <p>
     * Getter method for receivingGate.
     * </p>
     * 
     * @return The receivingGate
     */
    public String getReceivingGate() {
        return receivingGate;
    }

    /**
     * <p>
     * Setter method for receivingGate.
     * </p>
     * 
     * @param receivingGate Set for receivingGate
     */
    public void setReceivingGate(String receivingGate) {
        this.receivingGate = receivingGate;
    }

    /**
     * <p>
     * Getter method for utc.
     * </p>
     * 
     * @return The utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * <p>
     * Setter method for utc.
     * </p>
     * 
     * @param utc Set for utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * <p>
     * Getter method for asnNoList.
     * </p>
     * 
     * @return The asnNoList
     */
    public BigDecimal getAsnNoList() {
        return asnNoList;
    }

    /**
     * <p>
     * Setter method for asnNoList.
     * </p>
     * 
     * @param asnNoList Set for asnNoList
     */
    public void setAsnNoList(BigDecimal asnNoList) {
        this.asnNoList = asnNoList;
    }

    /**
     * <p>
     * Getter method for truckSeq.
     * </p>
     * 
     * @return the truckSeq
     */
    public BigDecimal getTruckSeq() {
        return truckSeq;
    }

    /**
     * <p>
     * Setter method for truckSeq.
     * </p>
     * 
     * @param truckSeq Set for truckSeq
     */
    public void setTruckSeq(BigDecimal truckSeq) {
        this.truckSeq = truckSeq;
    }

    /**
     * <p>
     * Getter method for supplierCompanyName.
     * </p>
     * 
     * @return the supplierCompanyName
     */
    public String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    /**
     * <p>
     * Setter method for supplierCompanyName.
     * </p>
     * 
     * @param supplierCompanyName Set for supplierCompanyName
     */
    public void setSupplierCompanyName(String supplierCompanyName) {
        this.supplierCompanyName = supplierCompanyName;
    }

    /**
     * <p>
     * Getter method for densoCompanyName.
     * </p>
     * 
     * @return the densoCompanyName
     */
    public String getDensoCompanyName() {
        return densoCompanyName;
    }

    /**
     * <p>
     * Setter method for densoCompanyName.
     * </p>
     * 
     * @param densoCompanyName Set for densoCompanyName
     */
    public void setDensoCompanyName(String densoCompanyName) {
        this.densoCompanyName = densoCompanyName;
    }

    /**
     * <p>
     * Getter method for supplierLocation.
     * </p>
     * 
     * @return the supplierLocation
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * <p>
     * Setter method for supplierLocation.
     * </p>
     * 
     * @param supplierLocation Set for supplierLocation
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * <p>
     * Getter method for rcvLane.
     * </p>
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>
     * Setter method for rcvLane.
     * </p>
     * 
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>
     * Getter method for shipDatetimeUtc.
     * </p>
     * 
     * @return the shipDatetimeUtc
     */
    public String getShipDatetimeUtc() {
        return shipDatetimeUtc;
    }

    /**
     * <p>
     * Setter method for shipDatetimeUtc.
     * </p>
     * 
     * @param shipDatetimeUtc Set for shipDatetimeUtc
     */
    public void setShipDatetimeUtc(String shipDatetimeUtc) {
        this.shipDatetimeUtc = shipDatetimeUtc;
    }

    /**
     * <p>
     * Getter method for deliveryDatetimeUtc.
     * </p>
     * 
     * @return the deliveryDatetimeUtc
     */
    public String getDeliveryDatetimeUtc() {
        return deliveryDatetimeUtc;
    }

    /**
     * <p>
     * Setter method for deliveryDatetimeUtc.
     * </p>
     * 
     * @param deliveryDatetimeUtc Set for deliveryDatetimeUtc
     */
    public void setDeliveryDatetimeUtc(String deliveryDatetimeUtc) {
        this.deliveryDatetimeUtc = deliveryDatetimeUtc;
    }

    /**
     * <p>Getter method for currentSpsDoNo.</p>
     *
     * @return the currentSpsDoNo
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }
    
    /**
     * <p>Setter method for currentSpsDoNo.</p>
     *
     * @param currentSpsDoNo Set for currentSpsDoNo
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }
    
    /**
     * <p>Getter method for currentCigmaDoNo.</p>
     *
     * @return the currentCigmaDoNo
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * <p>Setter method for currentCigmaDoNo.</p>
     *
     * @param currentCigmaDoNo Set for currentCigmaDoNo
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * <p>Getter method for previousSpsDoNo.</p>
     *
     * @return the previousSpsDoNo
     */
    public String getPreviousSpsDoNo() {
        return previousSpsDoNo;
    }

    /**
     * <p>Setter method for previousSpsDoNo.</p>
     *
     * @param previousSpsDoNo Set for previousSpsDoNo
     */
    public void setPreviousSpsDoNo(String previousSpsDoNo) {
        this.previousSpsDoNo = previousSpsDoNo;
    }

    /**
     * <p>Getter method for doIssueDateShow.</p>
     *
     * @return the doIssueDateShow
     */
    public String getDoIssueDateShow() {
        return doIssueDateShow;
    }

    /**
     * <p>Setter method for doIssueDateShow.</p>
     *
     * @param doIssueDateShow Set for doIssueDateShow
     */
    public void setDoIssueDateShow(String doIssueDateShow) {
        this.doIssueDateShow = doIssueDateShow;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    /**
     * Gets the isPrintOneWayKanbanTag.
     * 
     * @return the isPrintOneWayKanbanTag
     */
    public String getIsPrintOneWayKanbanTag() {
        return isPrintOneWayKanbanTag;
    }

    /**
     * Sets the isPrintOneWayKanbanTag.
     * 
     * @param isPrintOneWayKanbanTag the isPrintOneWayKanbanTag
     */
    public void setIsPrintOneWayKanbanTag(String isPrintOneWayKanbanTag) {
        this.isPrintOneWayKanbanTag = isPrintOneWayKanbanTag;
    }
    /**
     * Gets the oneWayKanbanTagFlag.
     * 
     * @return the oneWayKanbanTagFlag
     */

    public String getOneWayKanbanTagFlag() {
        return oneWayKanbanTagFlag;
    }

    /**
     * Sets the oneWayKanbanTagFlag.
     * 
     * @param oneWayKanbanTagFlag the oneWayKanbanTagFlag
     */

    public void setOneWayKanbanTagFlag(String oneWayKanbanTagFlag) {
        this.oneWayKanbanTagFlag = oneWayKanbanTagFlag;
    }

    /**
     * <p>Getter method for tagOutput.</p>
     *
     * @return the tagOutput
     */
    public String getTagOutput() {
        return tagOutput;
    }

    /**
     * <p>Setter method for tagOutput.</p>
     *
     * @param tagOutput Set for tagOutput
     */
    public void setTagOutput(String tagOutput) {
        this.tagOutput = tagOutput;
    }

    /**
     * <p>Getter method for partTag.</p>
     *
     * @return the partTag
     */
    public Integer getPartTag() {
        return partTag;
    }

    /**
     * <p>Setter method for partTag.</p>
     *
     * @param partTag Set for partTag
     */
    public void setPartTag(Integer partTag) {
        this.partTag = partTag;
    }

    /**
     * <p>Getter method for receiveStatus.</p>
     *
     * @return the receiveStatus
     */
    public String getReceiveStatus() {
        return receiveStatus;
    }

    /**
     * <p>Setter method for receiveStatus.</p>
     *
     * @param receiveStatus Set for receiveStatus
     */
    public void setReceiveStatus(String receiveStatus) {
        this.receiveStatus = receiveStatus;
    }

    /**
     * <p>Getter method for shippingTotal.</p>
     *
     * @return the shippingTotal
     */
    public BigDecimal getShippingTotal() {
        return shippingTotal;
    }

    /**
     * <p>Setter method for shippingTotal.</p>
     *
     * @param shippingTotal Set for shippingTotal
     */
    public void setShippingTotal(BigDecimal shippingTotal) {
        this.shippingTotal = shippingTotal;
    }

}
