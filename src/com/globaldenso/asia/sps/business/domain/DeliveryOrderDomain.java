package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * 
 * <p>Type in the functional overview of the class.</p>
 *
 * @author CSI
 */
public class DeliveryOrderDomain extends BaseDomain implements Serializable {
    /**
     */
    private static final long serialVersionUID = -2252624258870803846L;
    /** D/O Header */
    private SpsTDoDomain doHeader;
    /** D/O Detail */
    private SpsTDoDetailDomain doDetail;
    /** D/O Company */
    private SpsMCompanyDensoDomain company;
    /**
     * 
     * <p>the constructor.</p>
     *
     */
    public DeliveryOrderDomain() {
        super();
    }
    /**
     * <p>Getter method for doHeader.</p>
     *
     * @return the doHeader
     */
    public SpsTDoDomain getDoHeader() {
        return doHeader;
    }
    /**
     * <p>Setter method for doHeader.</p>
     *
     * @param doHeader Set for doHeader
     */
    public void setDoHeader(SpsTDoDomain doHeader) {
        this.doHeader = doHeader;
    }
    /**
     * <p>Getter method for doDetail.</p>
     *
     * @return the doDetail
     */
    public SpsTDoDetailDomain getDoDetail() {
        return doDetail;
    }
    /**
     * <p>Setter method for doDetail.</p>
     *
     * @param doDetail Set for doDetail
     */
    public void setDoDetail(SpsTDoDetailDomain doDetail) {
        this.doDetail = doDetail;
    }
    /**
     * <p>Getter method for company.</p>
     *
     * @return the company
     */
    public SpsMCompanyDensoDomain getCompany() {
        return company;
    }
    /**
     * <p>Setter method for company.</p>
     *
     * @param company Set for company
     */
    public void setCompany(SpsMCompanyDensoDomain company) {
        this.company = company;
    }
}
