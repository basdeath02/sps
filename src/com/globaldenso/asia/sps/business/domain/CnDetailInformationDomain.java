/*
 * ModifyDate Development company     Describe 
 * 2014/08/29 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;

/**
 * The Class CN Detail Information.
 * @author CSI
 */
public class CnDetailInformationDomain extends BaseDomain implements Serializable {

    /** The generated Serial Version UID. */
    private static final long serialVersionUID = -6762724251239120365L;

    /** The class for SPS_T_CN_DETAIL. */
    private SpsTCnDetailDomain spsTCnDetailDomain;
    
    /** The class for SPS_T_ASN_DETAIL. */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** The default constructor. */
    public CnDetailInformationDomain() {
        super();
    }

    /**
     * <p>Getter method for spsTCnDetailDomain.</p>
     *
     * @return the spsTCnDetailDomain
     */
    public SpsTCnDetailDomain getSpsTCnDetailDomain() {
        return spsTCnDetailDomain;
    }

    /**
     * <p>Setter method for spsTCnDetailDomain.</p>
     *
     * @param spsTCnDetailDomain Set for spsTCnDetailDomain
     */
    public void setSpsTCnDetailDomain(SpsTCnDetailDomain spsTCnDetailDomain) {
        this.spsTCnDetailDomain = spsTCnDetailDomain;
    }

    /**
     * <p>Getter method for spsTAsnDetailDomain.</p>
     *
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }

    /**
     * <p>Setter method for spsTAsnDetailDomain.</p>
     *
     * @param spsTAsnDetailDomain Set for spsTAsnDetailDomain
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }
    
}
