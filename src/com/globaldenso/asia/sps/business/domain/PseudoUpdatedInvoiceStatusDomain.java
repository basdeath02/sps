/*
 * ModifyDate Development company     Describe 
 * 2014/08/25 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Pseudo Updated Invoice Status Domain for receive value from Web Service
 * 
 * @author CSI
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "UpdatedInvoiceStatusDomain")
public class PseudoUpdatedInvoiceStatusDomain extends PseudoBaseDomain implements Serializable {

    /** The generated serial Version UID. */
    private static final long serialVersionUID = -6236708077298001300L;

    /** The pseudo of Data scope date. */
    @XmlElement(name = "dataScopeDate")
    private String pseudoDataScopeDate;
    
    /** The pseudo of Invoice Number. */
    @XmlElement(name = "invoiceNo")
    private String  pseudoInvoiceNo;
    
    /** The pseudo of Supplier Company Code. */
    @XmlElement(name = "sCd")
    private String  pseudoSCd;
    
    /** The pseudo of Invoice Status */
    @XmlElement(name = "invoiceStatus")
    private String  pseudoInvoiceStatus;
    
    /** The pseudo of Payment Date. */
    @XmlElement(name = "paymentDate")
    private String  pseudoPaymentDate;
    
    /** The pseudo of Status Code. */
    @XmlElement(name = "statusCode")
    private String  pseudoStatusCode;

    /** The Default Constructor. */
    public PseudoUpdatedInvoiceStatusDomain() {
        super();
    }

    /**
     * <p>Getter method for pseudoDataScopeDate.</p>
     *
     * @return the pseudoDataScopeDate
     */
    public String getPseudoDataScopeDate() {
        return pseudoDataScopeDate;
    }

    /**
     * <p>Setter method for pseudoDataScopeDate.</p>
     *
     * @param pseudoDataScopeDate Set for pseudoDataScopeDate
     */
    public void setPseudoDataScopeDate(String pseudoDataScopeDate) {
        this.pseudoDataScopeDate = pseudoDataScopeDate;
    }

    /**
     * <p>Getter method for pseudoInvoiceNo.</p>
     *
     * @return the pseudoInvoiceNo
     */
    public String getPseudoInvoiceNo() {
        return pseudoInvoiceNo;
    }

    /**
     * <p>Setter method for pseudoInvoiceNo.</p>
     *
     * @param pseudoInvoiceNo Set for pseudoInvoiceNo
     */
    public void setPseudoInvoiceNo(String pseudoInvoiceNo) {
        this.pseudoInvoiceNo = pseudoInvoiceNo;
    }

    /**
     * <p>Getter method for pseudoSCd.</p>
     *
     * @return the pseudoSCd
     */
    public String getPseudoSCd() {
        return pseudoSCd;
    }

    /**
     * <p>Setter method for pseudoSCd.</p>
     *
     * @param pseudoSCd Set for pseudoSCd
     */
    public void setPseudoSCd(String pseudoSCd) {
        this.pseudoSCd = pseudoSCd;
    }

    /**
     * <p>Getter method for pseudoInvoiceStatus.</p>
     *
     * @return the pseudoInvoiceStatus
     */
    public String getPseudoInvoiceStatus() {
        return pseudoInvoiceStatus;
    }

    /**
     * <p>Setter method for pseudoInvoiceStatus.</p>
     *
     * @param pseudoInvoiceStatus Set for pseudoInvoiceStatus
     */
    public void setPseudoInvoiceStatus(String pseudoInvoiceStatus) {
        this.pseudoInvoiceStatus = pseudoInvoiceStatus;
    }

    /**
     * <p>Getter method for pseudoPaymentDate.</p>
     *
     * @return the pseudoPaymentDate
     */
    public String getPseudoPaymentDate() {
        return pseudoPaymentDate;
    }

    /**
     * <p>Setter method for pseudoPaymentDate.</p>
     *
     * @param pseudoPaymentDate Set for pseudoPaymentDate
     */
    public void setPseudoPaymentDate(String pseudoPaymentDate) {
        this.pseudoPaymentDate = pseudoPaymentDate;
    }

    /**
     * <p>Getter method for pseudoStatusCode.</p>
     *
     * @return the pseudoStatusCode
     */
    public String getPseudoStatusCode() {
        return pseudoStatusCode;
    }

    /**
     * <p>Setter method for pseudoStatusCode.</p>
     *
     * @param pseudoStatusCode Set for pseudoStatusCode
     */
    public void setPseudoStatusCode(String pseudoStatusCode) {
        this.pseudoStatusCode = pseudoStatusCode;
    }
    
}
