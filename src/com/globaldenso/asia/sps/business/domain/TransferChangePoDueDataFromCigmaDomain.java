package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
/**
 * 
 * <p>TransferPoDueDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferChangePoDueDataFromCigmaDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6135022531502538732L;
    /** etd */
    private Date etd;
    /** dueDate */
    private Date dueDate;
    /** oldQty */
    private BigDecimal oldQty;
    /** newQty */
    private BigDecimal newQty;
    /** differenceQty */
    private BigDecimal differenceQty;
    /** reportType */
    private String reportType;
    /** reason */
    private String reason;
    /** del */
    private String del;
    /** seq */
    private BigDecimal seq;
    /** spsFlag */
    private String spsFlag;
    /** createBy */
    private String createBy;
    /** createDate */
    private String createDate;
    /** createTime */
    private String createTime;
    /** updateBy */
    private String updateBy;
    /** updateDate */
    private String updateDate;
    /** updateTime */
    private String updateTime;

    /** Supplier Plant (for send email). */
    private String sPcd;
    
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferChangePoDueDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for etd.</p>
     *
     * @return the etd
     */
    public Date getEtd() {
        return etd;
    }
    /**
     * <p>Setter method for etd.</p>
     *
     * @param etd Set for etd
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }
    /**
     * <p>Getter method for dueDate.</p>
     *
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }
    /**
     * <p>Setter method for dueDate.</p>
     *
     * @param dueDate Set for dueDate
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    /**
     * <p>Getter method for oldQty.</p>
     *
     * @return the oldQty
     */
    public BigDecimal getOldQty() {
        return oldQty;
    }
    /**
     * <p>Setter method for oldQty.</p>
     *
     * @param oldQty Set for oldQty
     */
    public void setOldQty(BigDecimal oldQty) {
        this.oldQty = oldQty;
    }
    /**
     * <p>Getter method for newQty.</p>
     *
     * @return the newQty
     */
    public BigDecimal getNewQty() {
        return newQty;
    }
    /**
     * <p>Setter newQty for newQty.</p>
     *
     * @param newQty Set for newQty
     */
    public void setNewQty(BigDecimal newQty) {
        this.newQty = newQty;
    }
    /**
     * <p>Getter method for differenceQty.</p>
     *
     * @return the differenceQty
     */
    public BigDecimal getDifferenceQty() {
        return differenceQty;
    }
    /**
     * <p>Setter method for differenceQty.</p>
     *
     * @param differenceQty Set for differenceQty
     */
    public void setDifferenceQty(BigDecimal differenceQty) {
        this.differenceQty = differenceQty;
    }
    /**
     * <p>Getter method for reportType.</p>
     *
     * @return the reportType
     */
    public String getReportType() {
        return reportType;
    }
    /**
     * <p>Setter method for reportType.</p>
     *
     * @param reportType Set for reportType
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
    /**
     * <p>Getter method for reason.</p>
     *
     * @return the reason
     */
    public String getReason() {
        return reason;
    }
    /**
     * <p>Setter method for reason.</p>
     *
     * @param reason Set for reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
    /**
     * <p>Getter method for spsFlag.</p>
     *
     * @return the spsFlag
     */
    public String getSpsFlag() {
        return spsFlag;
    }
    /**
     * <p>Setter method for spsFlag.</p>
     *
     * @param spsFlag Set for spsFlag
     */
    public void setSpsFlag(String spsFlag) {
        this.spsFlag = spsFlag;
    }
    /**
     * <p>Getter method for createBy.</p>
     *
     * @return the createBy
     */
    public String getCreateBy() {
        return createBy;
    }
    /**
     * <p>Setter method for createBy.</p>
     *
     * @param createBy Set for createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    /**
     * <p>Getter method for createDate.</p>
     *
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }
    /**
     * <p>Setter method for createDate.</p>
     *
     * @param createDate Set for createDate
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    /**
     * <p>Getter method for createTime.</p>
     *
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }
    /**
     * <p>Setter method for createTime.</p>
     *
     * @param createTime Set for createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    /**
     * <p>Getter method for updateBy.</p>
     *
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }
    /**
     * <p>Setter method for updateBy.</p>
     *
     * @param updateBy Set for updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    /**
     * <p>Getter method for updateDate.</p>
     *
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }
    /**
     * <p>Setter method for updateDate.</p>
     *
     * @param updateDate Set for updateDate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    /**
     * <p>Getter method for updateTime.</p>
     *
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }
    /**
     * <p>Setter method for updateTime.</p>
     *
     * @param updateTime Set for updateTime
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * <p>Getter method for del.</p>
     *
     * @return the del
     */
    public String getDel() {
        return del;
    }
    /**
     * <p>Setter method for del.</p>
     *
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }
    /**
     * <p>Getter method for seq.</p>
     *
     * @return the seq
     */
    public BigDecimal getSeq() {
        return seq;
    }
    /**
     * <p>Setter method for seq.</p>
     *
     * @param seq Set for seq
     */
    public void setSeq(BigDecimal seq) {
        this.seq = seq;
    }
}
