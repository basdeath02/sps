/*
 * ModifyDate Development company     Describe 
 * 2014/07/24  CSI Karnrawee          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/**
 * Pseudo PO Domain for receive value from Web Service
 * 
 * @author CSI Arnon
 * @version 1.0.0
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaDoInformationDomain")
public class PseudoCigmaDoInformationDomain extends PseudoBaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 4001258904053660611L;
    /** The pseudo of Data Type */
    @XmlElement(name = "dataType")
    private String pseudoDataType;
    /** The pseudo of Company Name */
    @XmlElement(name = "companyName")
    private String pseudoCompanyName;
    /** The pseudo of Company Address */
    @XmlElement(name = "companyAddress")
    private String pseudoCompanyAddress;
    /** The pseudo of TRUCK ROUTE # */
    @XmlElement(name = "truckRoute")
    private String pseudoTruckRoute;
    /** The pseudo of TRUCK SEQ # */
    @XmlElement(name = "truckSeq")
    private String pseudoTruckSeq;
    /** The pseudo of Supplier Code(Header) */
    @XmlElement(name = "doSCd")
    private String pseudoHeaderVendorCd;
    /** The pseudo of Supplier Name */
    @XmlElement(name = "supplierName")
    private String pseudoSupplierName;
    /** The pseudo of Supplier Location */
    @XmlElement(name = "supplierLocation")
    private String pseudoSupplierLocation;
    /** The pseudo of Issued Date */
    @XmlElement(name = "issuedDate")
    private String pseudoIssuedDate;
    /** The pseudo of DO Number */
    @XmlElement(name = "doNumber")
    private String pseudoDoNumber;
    /** The pseudo of Ship Date */
    @XmlElement(name = "shipDate")
    private String pseudoShipDate;
    /** The pseudo of Ship Time */
    @XmlElement(name = "shipTime")
    private String pseudoShipTime;
    /** The pseudo of Delivery Date */
    @XmlElement(name = "deliveryDate")
    private String pseudoDeliveryDate;
    /** The pseudo of Delivery Time */
    @XmlElement(name = "deliveryTime")
    private String pseudoDeliveryTime;
    /** The pseudo of Plant Code */
    @XmlElement(name = "dPcd")
    private String pseudoPlantCode;
    /** The pseudo of Warehouse for Prime Receiving */
    @XmlElement(name = "warehouseForPrimeReceiving")
    private String pseudoWarehouseForPrimeReceiving;
    /** The pseudo of Werehouse Location */
    @XmlElement(name = "werehouseLocation")
    private String pseudoWerehouseLocation;
    /** The pseudo of Receiving Dock */
    @XmlElement(name = "receivingDock")
    private String pseudoReceivingDock;
    /** The pseudo of Receiving Gate */
    @XmlElement(name = "receivingGate")
    private String pseudoReceivingGate;
    /** The pseudo of TM */
    @XmlElement(name = "tm")
    private String pseudoTm;
    /** The pseudo of Cycle */
    @XmlElement(name = "cycle")
    private String pseudoCycle;
    /** The pseudo of Control No. */
    @XmlElement(name = "controlNo")
    private String pseudoControlNo;
    /** The pseudo of Part Number(Header) */
    @XmlElement(name = "doDPn")
    private String pseudoDoPartNumber;
    /** The pseudo of Part Name */
    @XmlElement(name = "partName")
    private String pseudoPartName;
    /** The pseudo of Unit of Measure */
    @XmlElement(name = "unitOfMeasure")
    private String pseudoUnitOfMeasure;
    /** The pseudo of Lot size */
    @XmlElement(name = "lotSize")
    private String pseudoLotSize;
    /** The pseudo of No. of Boxes */
    @XmlElement(name = "noOfBoxes")
    private String pseudoNoOfBoxes;
    /** The pseudo of Current Order Qty. */
    @XmlElement(name = "currentOrderQty")
    private String pseudoCurrentOrderQty;
    /** The pseudo of RCV Lane */
    @XmlElement(name = "rcvLane")
    private String pseudoRcvLane;
    /** The pseudo of Trip No. */
    @XmlElement(name = "tripNo")
    private String pseudoTripNo;
    /** The pseudo of Backlog */
    @XmlElement(name = "backlog")
    private String pseudoBacklog;
    /** The pseudo of PO Number */
    @XmlElement(name = "poNumber")
    private String pseudoPoNumber;
    /** The pseudo of Add Truck Route Flag */
    @XmlElement(name = "addTruckRouteFlag")
    private String pseudoAddTruckRouteFlag;
    /** The pseudo of SPS Flag */
    @XmlElement(name = "spsFlag")
    private String pseudoSpsFlag;
    /** The pseudo of Create by USER ID */
    @XmlElement(name = "createByUserId")
    private String pseudoCreateByUserId;
    /** The pseudo of Create Date */
    @XmlElement(name = "createDate")
    private String pseudoCreateDate;
    /** The pseudo of Create Time */
    @XmlElement(name = "createTime")
    private String pseudoCreateTime;
    /** The pseudo of Update by USER ID */
    @XmlElement(name = "updateByUserId")
    private String pseudoUpdateByUserId;
    /** The pseudo of Update Date */
    @XmlElement(name = "updateDate")
    private String pseudoUpdateDate;
    /** The pseudo of Update Time */
    @XmlElement(name = "updateTime")
    private String pseudoUpdateTime;
    /** The pseudo of Plant Cd */
    @XmlElement(name = "sPcd")
    private String pseudoSPcd;
    /** The pseudo of SUPPLIER CODE */
    @XmlElement(name = "sCd")
    private String pseudoKanbanVendorCd;
    /** The pseudo of Delivery Run Date */
    @XmlElement(name = "deliveryRunDate")
    private String pseudoDeliveryRunDate;
    /** The pseudo of Delivery Run No. */
    @XmlElement(name = "deliveryRunNo")
    private String pseudoDeliveryRunNo;
    /** The pseudo of PART NUMBER */
    @XmlElement(name = "DPn")
    private String pseudoPartNumber;
    /** The pseudo of Kanban Sequence No. */
    @XmlElement(name = "kanbanSequenceNo")
    private String pseudoKanbanSequenceNo;
    /** The pseudo of Kanban Control No. */
    @XmlElement(name = "kanbanControlNo")
    private String pseudoKanbanControlNo;
    /** The pseudo of Kanban Lot Size */
    @XmlElement(name = "kanbanLotSize")
    private String pseudoKanbanLotSize;
    /** The pseudo of Temporary Kanban Tag */
    @XmlElement(name = "temporaryKanbanTag")
    private String pseudoTemporaryKanbanTag;
    /** The pseudo of Delivery Order Number */
    @XmlElement(name = "deliveryOrderNumber")
    private String pseudoDeliveryOrderNumber;
    /** The pseudo of PURCHASE ORDER NO. */
    @XmlElement(name = "purchaseOrderNo")
    private String pseudoPurchaseOrderNo;
    /** The pseudo of Delivery Due Time */
    @XmlElement(name = "deliveryDueTime")
    private String pseudoDeliveryDueTime;
    /** The pseudo of Delivery Run Type */
    @XmlElement(name = "deliveryRunType")
    private String pseudoDeliveryRunType;
    /** The pseudo of Run Schedule Date */
    @XmlElement(name = "runScheduleDate")
    private String pseudoRunScheduleDate;
    /** The pseudo of QR Recv Batch No. */
    @XmlElement(name = "qrRecvBatchNo")
    private String pseudoQrRecvBatchNo;
    /** The pseudo of QR Scan Time */
    @XmlElement(name = "qrScanTime")
    private String pseudoQrScanTime;
    /** Scan to receive yes/no. */
    @XmlElement(name = "scanReceiveFlag")
    private String pseudoScanReceiveFlag;
    /** Kanban Type  */
    @XmlElement(name = "kanbanType")
    private String pseudoKanbanType;
    /** remark1  */
    @XmlElement(name = "remark1")
    private String pseudoRemark1;
    /** remark2  */
    @XmlElement(name = "remark2")
    private String pseudoRemark2;
    /** remark3  */
    @XmlElement(name = "remark3")
    private String pseudoRemark3;
    /** D Cust. Part No. (STK Out, A Part QR)  */
    @XmlElement(name = "dCustomerPartNo")
    private String pseudoDCustomerPartNo;
    /** Supp Proc Code (STK Out QR)  */
    @XmlElement(name = "sProcessCode")
    private String pseudoSProcessCode;
    /** tagOutput */
    @XmlElement(name = "tagOutput")
    private String pseudoTagOutput;
    
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public PseudoCigmaDoInformationDomain() {
    }

    /**
     * <p>Getter method for pseudoDataType.</p>
     *
     * @return the pseudoDataType
     */
    public String getPseudoDataType() {
        return pseudoDataType;
    }

    /**
     * <p>Setter method for pseudoDataType.</p>
     *
     * @param pseudoDataType Set for pseudoDataType
     */
    public void setPseudoDataType(String pseudoDataType) {
        this.pseudoDataType = pseudoDataType;
    }

    /**
     * <p>Getter method for pseudoCompanyName.</p>
     *
     * @return the pseudoCompanyName
     */
    public String getPseudoCompanyName() {
        return pseudoCompanyName;
    }

    /**
     * <p>Setter method for pseudoCompanyName.</p>
     *
     * @param pseudoCompanyName Set for pseudoCompanyName
     */
    public void setPseudoCompanyName(String pseudoCompanyName) {
        this.pseudoCompanyName = pseudoCompanyName;
    }

    /**
     * <p>Getter method for pseudoCompanyAddress.</p>
     *
     * @return the pseudoCompanyAddress
     */
    public String getPseudoCompanyAddress() {
        return pseudoCompanyAddress;
    }

    /**
     * <p>Setter method for pseudoCompanyAddress.</p>
     *
     * @param pseudoCompanyAddress Set for pseudoCompanyAddress
     */
    public void setPseudoCompanyAddress(String pseudoCompanyAddress) {
        this.pseudoCompanyAddress = pseudoCompanyAddress;
    }

    /**
     * <p>Getter method for pseudoTruckRoute.</p>
     *
     * @return the pseudoTruckRoute
     */
    public String getPseudoTruckRoute() {
        return pseudoTruckRoute;
    }

    /**
     * <p>Setter method for pseudoTruckRoute.</p>
     *
     * @param pseudoTruckRoute Set for pseudoTruckRoute
     */
    public void setPseudoTruckRoute(String pseudoTruckRoute) {
        this.pseudoTruckRoute = pseudoTruckRoute;
    }

    /**
     * <p>Getter method for pseudoTruckSeq.</p>
     *
     * @return the pseudoTruckSeq
     */
    public String getPseudoTruckSeq() {
        return pseudoTruckSeq;
    }

    /**
     * <p>Setter method for pseudoTruckSeq.</p>
     *
     * @param pseudoTruckSeq Set for pseudoTruckSeq
     */
    public void setPseudoTruckSeq(String pseudoTruckSeq) {
        this.pseudoTruckSeq = pseudoTruckSeq;
    }

    /**
     * <p>Getter method for pseudoDoSCd.</p>
     *
     * @return the pseudoDoSCd
     */
    public String getPseudoHeaderVendorCd() {
        return pseudoHeaderVendorCd;
    }

    /**
     * <p>Setter method for pseudoDoSCd.</p>
     *
     * @param pseudoHeaderVendorCd Set for pseudoDoSCd
     */
    public void setPseudoHeaderVendorCd(String pseudoHeaderVendorCd) {
        this.pseudoHeaderVendorCd = pseudoHeaderVendorCd;
    }

    /**
     * <p>Getter method for pseudoSupplierName.</p>
     *
     * @return the pseudoSupplierName
     */
    public String getPseudoSupplierName() {
        return pseudoSupplierName;
    }

    /**
     * <p>Setter method for pseudoSupplierName.</p>
     *
     * @param pseudoSupplierName Set for pseudoSupplierName
     */
    public void setPseudoSupplierName(String pseudoSupplierName) {
        this.pseudoSupplierName = pseudoSupplierName;
    }

    /**
     * <p>Getter method for pseudoSupplierLocation.</p>
     *
     * @return the pseudoSupplierLocation
     */
    public String getPseudoSupplierLocation() {
        return pseudoSupplierLocation;
    }

    /**
     * <p>Setter method for pseudoSupplierLocation.</p>
     *
     * @param pseudoSupplierLocation Set for pseudoSupplierLocation
     */
    public void setPseudoSupplierLocation(String pseudoSupplierLocation) {
        this.pseudoSupplierLocation = pseudoSupplierLocation;
    }

    /**
     * <p>Getter method for pseudoIssuedDate.</p>
     *
     * @return the pseudoIssuedDate
     */
    public String getPseudoIssuedDate() {
        return pseudoIssuedDate;
    }

    /**
     * <p>Setter method for pseudoIssuedDate.</p>
     *
     * @param pseudoIssuedDate Set for pseudoIssuedDate
     */
    public void setPseudoIssuedDate(String pseudoIssuedDate) {
        this.pseudoIssuedDate = pseudoIssuedDate;
    }

    /**
     * <p>Getter method for pseudoDoNumber.</p>
     *
     * @return the pseudoDoNumber
     */
    public String getPseudoDoNumber() {
        return pseudoDoNumber;
    }

    /**
     * <p>Setter method for pseudoDoNumber.</p>
     *
     * @param pseudoDoNumber Set for pseudoDoNumber
     */
    public void setPseudoDoNumber(String pseudoDoNumber) {
        this.pseudoDoNumber = pseudoDoNumber;
    }

    /**
     * <p>Getter method for pseudoShipDate.</p>
     *
     * @return the pseudoShipDate
     */
    public String getPseudoShipDate() {
        return pseudoShipDate;
    }

    /**
     * <p>Setter method for pseudoShipDate.</p>
     *
     * @param pseudoShipDate Set for pseudoShipDate
     */
    public void setPseudoShipDate(String pseudoShipDate) {
        this.pseudoShipDate = pseudoShipDate;
    }

    /**
     * <p>Getter method for pseudoShipTime.</p>
     *
     * @return the pseudoShipTime
     */
    public String getPseudoShipTime() {
        return pseudoShipTime;
    }

    /**
     * <p>Setter method for pseudoShipTime.</p>
     *
     * @param pseudoShipTime Set for pseudoShipTime
     */
    public void setPseudoShipTime(String pseudoShipTime) {
        this.pseudoShipTime = pseudoShipTime;
    }

    /**
     * <p>Getter method for pseudoDeliveryDate.</p>
     *
     * @return the pseudoDeliveryDate
     */
    public String getPseudoDeliveryDate() {
        return pseudoDeliveryDate;
    }

    /**
     * <p>Setter method for pseudoDeliveryDate.</p>
     *
     * @param pseudoDeliveryDate Set for pseudoDeliveryDate
     */
    public void setPseudoDeliveryDate(String pseudoDeliveryDate) {
        this.pseudoDeliveryDate = pseudoDeliveryDate;
    }

    /**
     * <p>Getter method for pseudoDeliveryTime.</p>
     *
     * @return the pseudoDeliveryTime
     */
    public String getPseudoDeliveryTime() {
        return pseudoDeliveryTime;
    }

    /**
     * <p>Setter method for pseudoDeliveryTime.</p>
     *
     * @param pseudoDeliveryTime Set for pseudoDeliveryTime
     */
    public void setPseudoDeliveryTime(String pseudoDeliveryTime) {
        this.pseudoDeliveryTime = pseudoDeliveryTime;
    }

    /**
     * <p>Getter method for pseudoPlantCode.</p>
     *
     * @return the pseudoPlantCode
     */
    public String getPseudoPlantCode() {
        return pseudoPlantCode;
    }

    /**
     * <p>Setter method for pseudoPlantCode.</p>
     *
     * @param pseudoPlantCode Set for pseudoPlantCode
     */
    public void setPseudoPlantCode(String pseudoPlantCode) {
        this.pseudoPlantCode = pseudoPlantCode;
    }

    /**
     * <p>Getter method for pseudoWarehouseForPrimeReceiving.</p>
     *
     * @return the pseudoWarehouseForPrimeReceiving
     */
    public String getPseudoWarehouseForPrimeReceiving() {
        return pseudoWarehouseForPrimeReceiving;
    }

    /**
     * <p>Setter method for pseudoWarehouseForPrimeReceiving.</p>
     *
     * @param pseudoWarehouseForPrimeReceiving Set for pseudoWarehouseForPrimeReceiving
     */
    public void setPseudoWarehouseForPrimeReceiving(
        String pseudoWarehouseForPrimeReceiving) {
        this.pseudoWarehouseForPrimeReceiving = pseudoWarehouseForPrimeReceiving;
    }

    /**
     * <p>Getter method for pseudoWerehouseLocation.</p>
     *
     * @return the pseudoWerehouseLocation
     */
    public String getPseudoWerehouseLocation() {
        return pseudoWerehouseLocation;
    }

    /**
     * <p>Setter method for pseudoWerehouseLocation.</p>
     *
     * @param pseudoWerehouseLocation Set for pseudoWerehouseLocation
     */
    public void setPseudoWerehouseLocation(String pseudoWerehouseLocation) {
        this.pseudoWerehouseLocation = pseudoWerehouseLocation;
    }

    /**
     * <p>Getter method for pseudoReceivingDock.</p>
     *
     * @return the pseudoReceivingDock
     */
    public String getPseudoReceivingDock() {
        return pseudoReceivingDock;
    }

    /**
     * <p>Setter method for pseudoReceivingDock.</p>
     *
     * @param pseudoReceivingDock Set for pseudoReceivingDock
     */
    public void setPseudoReceivingDock(String pseudoReceivingDock) {
        this.pseudoReceivingDock = pseudoReceivingDock;
    }

    /**
     * <p>Getter method for pseudoReceivingGate.</p>
     *
     * @return the pseudoReceivingGate
     */
    public String getPseudoReceivingGate() {
        return pseudoReceivingGate;
    }

    /**
     * <p>Setter method for pseudoReceivingGate.</p>
     *
     * @param pseudoReceivingGate Set for pseudoReceivingGate
     */
    public void setPseudoReceivingGate(String pseudoReceivingGate) {
        this.pseudoReceivingGate = pseudoReceivingGate;
    }

    /**
     * <p>Getter method for pseudoTm.</p>
     *
     * @return the pseudoTm
     */
    public String getPseudoTm() {
        return pseudoTm;
    }

    /**
     * <p>Setter method for pseudoTm.</p>
     *
     * @param pseudoTm Set for pseudoTm
     */
    public void setPseudoTm(String pseudoTm) {
        this.pseudoTm = pseudoTm;
    }

    /**
     * <p>Getter method for pseudoCycle.</p>
     *
     * @return the pseudoCycle
     */
    public String getPseudoCycle() {
        return pseudoCycle;
    }

    /**
     * <p>Setter method for pseudoCycle.</p>
     *
     * @param pseudoCycle Set for pseudoCycle
     */
    public void setPseudoCycle(String pseudoCycle) {
        this.pseudoCycle = pseudoCycle;
    }

    /**
     * <p>Getter method for pseudoControlNo.</p>
     *
     * @return the pseudoControlNo
     */
    public String getPseudoControlNo() {
        return pseudoControlNo;
    }

    /**
     * <p>Setter method for pseudoControlNo.</p>
     *
     * @param pseudoControlNo Set for pseudoControlNo
     */
    public void setPseudoControlNo(String pseudoControlNo) {
        this.pseudoControlNo = pseudoControlNo;
    }

    /**
     * <p>Getter method for pseudoDoPartNumber.</p>
     *
     * @return the pseudoDoPartNumber
     */
    public String getPseudoDoPartNumber() {
        return pseudoDoPartNumber;
    }

    /**
     * <p>Setter method for pseudoDoPartNumber.</p>
     *
     * @param pseudoDoPartNumber Set for pseudoDoPartNumber
     */
    public void setPseudoDoPartNumber(String pseudoDoPartNumber) {
        this.pseudoDoPartNumber = pseudoDoPartNumber;
    }

    /**
     * <p>Getter method for pseudoPartName.</p>
     *
     * @return the pseudoPartName
     */
    public String getPseudoPartName() {
        return pseudoPartName;
    }

    /**
     * <p>Setter method for pseudoPartName.</p>
     *
     * @param pseudoPartName Set for pseudoPartName
     */
    public void setPseudoPartName(String pseudoPartName) {
        this.pseudoPartName = pseudoPartName;
    }

    /**
     * <p>Getter method for pseudoUnitOfMeasure.</p>
     *
     * @return the pseudoUnitOfMeasure
     */
    public String getPseudoUnitOfMeasure() {
        return pseudoUnitOfMeasure;
    }

    /**
     * <p>Setter method for pseudoUnitOfMeasure.</p>
     *
     * @param pseudoUnitOfMeasure Set for pseudoUnitOfMeasure
     */
    public void setPseudoUnitOfMeasure(String pseudoUnitOfMeasure) {
        this.pseudoUnitOfMeasure = pseudoUnitOfMeasure;
    }

    /**
     * <p>Getter method for pseudoLotSize.</p>
     *
     * @return the pseudoLotSize
     */
    public String getPseudoLotSize() {
        return pseudoLotSize;
    }

    /**
     * <p>Setter method for pseudoLotSize.</p>
     *
     * @param pseudoLotSize Set for pseudoLotSize
     */
    public void setPseudoLotSize(String pseudoLotSize) {
        this.pseudoLotSize = pseudoLotSize;
    }

    /**
     * <p>Getter method for pseudoNoOfBoxes.</p>
     *
     * @return the pseudoNoOfBoxes
     */
    public String getPseudoNoOfBoxes() {
        return pseudoNoOfBoxes;
    }

    /**
     * <p>Setter method for pseudoNoOfBoxes.</p>
     *
     * @param pseudoNoOfBoxes Set for pseudoNoOfBoxes
     */
    public void setPseudoNoOfBoxes(String pseudoNoOfBoxes) {
        this.pseudoNoOfBoxes = pseudoNoOfBoxes;
    }

    /**
     * <p>Getter method for pseudoCurrentOrderQty.</p>
     *
     * @return the pseudoCurrentOrderQty
     */
    public String getPseudoCurrentOrderQty() {
        return pseudoCurrentOrderQty;
    }

    /**
     * <p>Setter method for pseudoCurrentOrderQty.</p>
     *
     * @param pseudoCurrentOrderQty Set for pseudoCurrentOrderQty
     */
    public void setPseudoCurrentOrderQty(String pseudoCurrentOrderQty) {
        this.pseudoCurrentOrderQty = pseudoCurrentOrderQty;
    }

    /**
     * <p>Getter method for pseudoRcvLane.</p>
     *
     * @return the pseudoRcvLane
     */
    public String getPseudoRcvLane() {
        return pseudoRcvLane;
    }

    /**
     * <p>Setter method for pseudoRcvLane.</p>
     *
     * @param pseudoRcvLane Set for pseudoRcvLane
     */
    public void setPseudoRcvLane(String pseudoRcvLane) {
        this.pseudoRcvLane = pseudoRcvLane;
    }

    /**
     * <p>Getter method for pseudoTripNo.</p>
     *
     * @return the pseudoTripNo
     */
    public String getPseudoTripNo() {
        return pseudoTripNo;
    }

    /**
     * <p>Setter method for pseudoTripNo.</p>
     *
     * @param pseudoTripNo Set for pseudoTripNo
     */
    public void setPseudoTripNo(String pseudoTripNo) {
        this.pseudoTripNo = pseudoTripNo;
    }

    /**
     * <p>Getter method for pseudoBacklog.</p>
     *
     * @return the pseudoBacklog
     */
    public String getPseudoBacklog() {
        return pseudoBacklog;
    }

    /**
     * <p>Setter method for pseudoBacklog.</p>
     *
     * @param pseudoBacklog Set for pseudoBacklog
     */
    public void setPseudoBacklog(String pseudoBacklog) {
        this.pseudoBacklog = pseudoBacklog;
    }

    /**
     * <p>Getter method for pseudoPoNumber.</p>
     *
     * @return the pseudoPoNumber
     */
    public String getPseudoPoNumber() {
        return pseudoPoNumber;
    }

    /**
     * <p>Setter method for pseudoPoNumber.</p>
     *
     * @param pseudoPoNumber Set for pseudoPoNumber
     */
    public void setPseudoPoNumber(String pseudoPoNumber) {
        this.pseudoPoNumber = pseudoPoNumber;
    }

    /**
     * <p>Getter method for pseudoAddTruckRouteFlag.</p>
     *
     * @return the pseudoAddTruckRouteFlag
     */
    public String getPseudoAddTruckRouteFlag() {
        return pseudoAddTruckRouteFlag;
    }

    /**
     * <p>Setter method for pseudoAddTruckRouteFlag.</p>
     *
     * @param pseudoAddTruckRouteFlag Set for pseudoAddTruckRouteFlag
     */
    public void setPseudoAddTruckRouteFlag(String pseudoAddTruckRouteFlag) {
        this.pseudoAddTruckRouteFlag = pseudoAddTruckRouteFlag;
    }

    /**
     * <p>Getter method for pseudoSpsFlag.</p>
     *
     * @return the pseudoSpsFlag
     */
    public String getPseudoSpsFlag() {
        return pseudoSpsFlag;
    }

    /**
     * <p>Setter method for pseudoSpsFlag.</p>
     *
     * @param pseudoSpsFlag Set for pseudoSpsFlag
     */
    public void setPseudoSpsFlag(String pseudoSpsFlag) {
        this.pseudoSpsFlag = pseudoSpsFlag;
    }

    /**
     * <p>Getter method for pseudoCreateByUserId.</p>
     *
     * @return the pseudoCreateByUserId
     */
    public String getPseudoCreateByUserId() {
        return pseudoCreateByUserId;
    }

    /**
     * <p>Setter method for pseudoCreateByUserId.</p>
     *
     * @param pseudoCreateByUserId Set for pseudoCreateByUserId
     */
    public void setPseudoCreateByUserId(String pseudoCreateByUserId) {
        this.pseudoCreateByUserId = pseudoCreateByUserId;
    }

    /**
     * <p>Getter method for pseudoCreateDate.</p>
     *
     * @return the pseudoCreateDate
     */
    public String getPseudoCreateDate() {
        return pseudoCreateDate;
    }

    /**
     * <p>Setter method for pseudoCreateDate.</p>
     *
     * @param pseudoCreateDate Set for pseudoCreateDate
     */
    public void setPseudoCreateDate(String pseudoCreateDate) {
        this.pseudoCreateDate = pseudoCreateDate;
    }

    /**
     * <p>Getter method for pseudoCreateTime.</p>
     *
     * @return the pseudoCreateTime
     */
    public String getPseudoCreateTime() {
        return pseudoCreateTime;
    }

    /**
     * <p>Setter method for pseudoCreateTime.</p>
     *
     * @param pseudoCreateTime Set for pseudoCreateTime
     */
    public void setPseudoCreateTime(String pseudoCreateTime) {
        this.pseudoCreateTime = pseudoCreateTime;
    }

    /**
     * <p>Getter method for pseudoUpdateByUserId.</p>
     *
     * @return the pseudoUpdateByUserId
     */
    public String getPseudoUpdateByUserId() {
        return pseudoUpdateByUserId;
    }

    /**
     * <p>Setter method for pseudoUpdateByUserId.</p>
     *
     * @param pseudoUpdateByUserId Set for pseudoUpdateByUserId
     */
    public void setPseudoUpdateByUserId(String pseudoUpdateByUserId) {
        this.pseudoUpdateByUserId = pseudoUpdateByUserId;
    }

    /**
     * <p>Getter method for pseudoUpdateDate.</p>
     *
     * @return the pseudoUpdateDate
     */
    public String getPseudoUpdateDate() {
        return pseudoUpdateDate;
    }

    /**
     * <p>Setter method for pseudoUpdateDate.</p>
     *
     * @param pseudoUpdateDate Set for pseudoUpdateDate
     */
    public void setPseudoUpdateDate(String pseudoUpdateDate) {
        this.pseudoUpdateDate = pseudoUpdateDate;
    }

    /**
     * <p>Getter method for pseudoUpdateTime.</p>
     *
     * @return the pseudoUpdateTime
     */
    public String getPseudoUpdateTime() {
        return pseudoUpdateTime;
    }

    /**
     * <p>Setter method for pseudoUpdateTime.</p>
     *
     * @param pseudoUpdateTime Set for pseudoUpdateTime
     */
    public void setPseudoUpdateTime(String pseudoUpdateTime) {
        this.pseudoUpdateTime = pseudoUpdateTime;
    }

    /**
     * <p>Getter method for pseudoPlantCd.</p>
     *
     * @return the pseudoPlantCd
     */
    public String getPseudoSPcd() {
        return pseudoSPcd;
    }

    /**
     * <p>Setter method for pseudoPlantCd.</p>
     *
     * @param pseudoSPcd Set for pseudoPlantCd
     */
    public void setPseudoSPcd(String pseudoSPcd) {
        this.pseudoSPcd = pseudoSPcd;
    }

    /**
     * <p>Getter method for pseudoSCd.</p>
     *
     * @return the pseudoSCd
     */
    public String getPseudoKanbanVendorCd() {
        return pseudoKanbanVendorCd;
    }

    /**
     * <p>Setter method for pseudoSCd.</p>
     *
     * @param pseudoKanbanVendorCd Set for pseudoSCd
     */
    public void setPseudoKanbanVendorCd(String pseudoKanbanVendorCd) {
        this.pseudoKanbanVendorCd = pseudoKanbanVendorCd;
    }

    /**
     * <p>Getter method for pseudoDeliveryRunDate.</p>
     *
     * @return the pseudoDeliveryRunDate
     */
    public String getPseudoDeliveryRunDate() {
        return pseudoDeliveryRunDate;
    }

    /**
     * <p>Setter method for pseudoDeliveryRunDate.</p>
     *
     * @param pseudoDeliveryRunDate Set for pseudoDeliveryRunDate
     */
    public void setPseudoDeliveryRunDate(String pseudoDeliveryRunDate) {
        this.pseudoDeliveryRunDate = pseudoDeliveryRunDate;
    }

    /**
     * <p>Getter method for pseudoDeliveryRunNo.</p>
     *
     * @return the pseudoDeliveryRunNo
     */
    public String getPseudoDeliveryRunNo() {
        return pseudoDeliveryRunNo;
    }

    /**
     * <p>Setter method for pseudoDeliveryRunNo.</p>
     *
     * @param pseudoDeliveryRunNo Set for pseudoDeliveryRunNo
     */
    public void setPseudoDeliveryRunNo(String pseudoDeliveryRunNo) {
        this.pseudoDeliveryRunNo = pseudoDeliveryRunNo;
    }

    /**
     * <p>Getter method for pseudoPartNumber.</p>
     *
     * @return the pseudoPartNumber
     */
    public String getPseudoPartNumber() {
        return pseudoPartNumber;
    }

    /**
     * <p>Setter method for pseudoPartNumber.</p>
     *
     * @param pseudoPartNumber Set for pseudoPartNumber
     */
    public void setPseudoPartNumber(String pseudoPartNumber) {
        this.pseudoPartNumber = pseudoPartNumber;
    }

    /**
     * <p>Getter method for pseudoKanbanSequenceNo.</p>
     *
     * @return the pseudoKanbanSequenceNo
     */
    public String getPseudoKanbanSequenceNo() {
        return pseudoKanbanSequenceNo;
    }

    /**
     * <p>Setter method for pseudoKanbanSequenceNo.</p>
     *
     * @param pseudoKanbanSequenceNo Set for pseudoKanbanSequenceNo
     */
    public void setPseudoKanbanSequenceNo(String pseudoKanbanSequenceNo) {
        this.pseudoKanbanSequenceNo = pseudoKanbanSequenceNo;
    }

    /**
     * <p>Getter method for pseudoKanbanControlNo.</p>
     *
     * @return the pseudoKanbanControlNo
     */
    public String getPseudoKanbanControlNo() {
        return pseudoKanbanControlNo;
    }

    /**
     * <p>Setter method for pseudoKanbanControlNo.</p>
     *
     * @param pseudoKanbanControlNo Set for pseudoKanbanControlNo
     */
    public void setPseudoKanbanControlNo(String pseudoKanbanControlNo) {
        this.pseudoKanbanControlNo = pseudoKanbanControlNo;
    }

    /**
     * <p>Getter method for pseudoKanbanLotSize.</p>
     *
     * @return the pseudoKanbanLotSize
     */
    public String getPseudoKanbanLotSize() {
        return pseudoKanbanLotSize;
    }

    /**
     * <p>Setter method for pseudoKanbanLotSize.</p>
     *
     * @param pseudoKanbanLotSize Set for pseudoKanbanLotSize
     */
    public void setPseudoKanbanLotSize(String pseudoKanbanLotSize) {
        this.pseudoKanbanLotSize = pseudoKanbanLotSize;
    }

    /**
     * <p>Getter method for pseudoTemporaryKanbanTag.</p>
     *
     * @return the pseudoTemporaryKanbanTag
     */
    public String getPseudoTemporaryKanbanTag() {
        return pseudoTemporaryKanbanTag;
    }

    /**
     * <p>Setter method for pseudoTemporaryKanbanTag.</p>
     *
     * @param pseudoTemporaryKanbanTag Set for pseudoTemporaryKanbanTag
     */
    public void setPseudoTemporaryKanbanTag(String pseudoTemporaryKanbanTag) {
        this.pseudoTemporaryKanbanTag = pseudoTemporaryKanbanTag;
    }

    /**
     * <p>Getter method for pseudoDeliveryOrderNumber.</p>
     *
     * @return the pseudoDeliveryOrderNumber
     */
    public String getPseudoDeliveryOrderNumber() {
        return pseudoDeliveryOrderNumber;
    }

    /**
     * <p>Setter method for pseudoDeliveryOrderNumber.</p>
     *
     * @param pseudoDeliveryOrderNumber Set for pseudoDeliveryOrderNumber
     */
    public void setPseudoDeliveryOrderNumber(String pseudoDeliveryOrderNumber) {
        this.pseudoDeliveryOrderNumber = pseudoDeliveryOrderNumber;
    }

    /**
     * <p>Getter method for pseudoPurchaseOrderNo.</p>
     *
     * @return the pseudoPurchaseOrderNo
     */
    public String getPseudoPurchaseOrderNo() {
        return pseudoPurchaseOrderNo;
    }

    /**
     * <p>Setter method for pseudoPurchaseOrderNo.</p>
     *
     * @param pseudoPurchaseOrderNo Set for pseudoPurchaseOrderNo
     */
    public void setPseudoPurchaseOrderNo(String pseudoPurchaseOrderNo) {
        this.pseudoPurchaseOrderNo = pseudoPurchaseOrderNo;
    }

    /**
     * <p>Getter method for pseudoDeliveryDueTime.</p>
     *
     * @return the pseudoDeliveryDueTime
     */
    public String getPseudoDeliveryDueTime() {
        return pseudoDeliveryDueTime;
    }

    /**
     * <p>Setter method for pseudoDeliveryDueTime.</p>
     *
     * @param pseudoDeliveryDueTime Set for pseudoDeliveryDueTime
     */
    public void setPseudoDeliveryDueTime(String pseudoDeliveryDueTime) {
        this.pseudoDeliveryDueTime = pseudoDeliveryDueTime;
    }

    /**
     * <p>Getter method for pseudoDeliveryRunType.</p>
     *
     * @return the pseudoDeliveryRunType
     */
    public String getPseudoDeliveryRunType() {
        return pseudoDeliveryRunType;
    }

    /**
     * <p>Setter method for pseudoDeliveryRunType.</p>
     *
     * @param pseudoDeliveryRunType Set for pseudoDeliveryRunType
     */
    public void setPseudoDeliveryRunType(String pseudoDeliveryRunType) {
        this.pseudoDeliveryRunType = pseudoDeliveryRunType;
    }

    /**
     * <p>Getter method for pseudoRunScheduleDate.</p>
     *
     * @return the pseudoRunScheduleDate
     */
    public String getPseudoRunScheduleDate() {
        return pseudoRunScheduleDate;
    }

    /**
     * <p>Setter method for pseudoRunScheduleDate.</p>
     *
     * @param pseudoRunScheduleDate Set for pseudoRunScheduleDate
     */
    public void setPseudoRunScheduleDate(String pseudoRunScheduleDate) {
        this.pseudoRunScheduleDate = pseudoRunScheduleDate;
    }

    /**
     * <p>Getter method for pseudoQrRecvBatchNo.</p>
     *
     * @return the pseudoQrRecvBatchNo
     */
    public String getPseudoQrRecvBatchNo() {
        return pseudoQrRecvBatchNo;
    }

    /**
     * <p>Setter method for pseudoQrRecvBatchNo.</p>
     *
     * @param pseudoQrRecvBatchNo Set for pseudoQrRecvBatchNo
     */
    public void setPseudoQrRecvBatchNo(String pseudoQrRecvBatchNo) {
        this.pseudoQrRecvBatchNo = pseudoQrRecvBatchNo;
    }

    /**
     * <p>Getter method for pseudoQrScanTime.</p>
     *
     * @return the pseudoQrScanTime
     */
    public String getPseudoQrScanTime() {
        return pseudoQrScanTime;
    }

    /**
     * <p>Setter method for pseudoQrScanTime.</p>
     *
     * @param pseudoQrScanTime Set for pseudoQrScanTime
     */
    public void setPseudoQrScanTime(String pseudoQrScanTime) {
        this.pseudoQrScanTime = pseudoQrScanTime;
    }

    /**
     * <p>Getter method for pseudoScanReceiveFlag.</p>
     *
     * @return the pseudoScanReceiveFlag
     */
    public String getPseudoScanReceiveFlag() {
        return pseudoScanReceiveFlag;
    }

    /**
     * <p>Setter method for pseudoScanReceiveFlag.</p>
     *
     * @param pseudoScanReceiveFlag Set for pseudoScanReceiveFlag
     */
    public void setPseudoScanReceiveFlag(String pseudoScanReceiveFlag) {
        this.pseudoScanReceiveFlag = pseudoScanReceiveFlag;
    }

    /**
     * <p>Getter method for pseudoKanbanType.</p>
     *
     * @return the pseudoKanbanType
     */
    public String getPseudoKanbanType() {
        return pseudoKanbanType;
    }

    /**
     * <p>Setter method for pseudoKanbanType.</p>
     *
     * @param pseudoKanbanType Set for pseudoKanbanType
     */
    public void setPseudoKanbanType(String pseudoKanbanType) {
        this.pseudoKanbanType = pseudoKanbanType;
    }

    /**
     * <p>Getter method for pseudoRemark1.</p>
     *
     * @return the pseudoRemark1
     */
    public String getPseudoRemark1() {
        return pseudoRemark1;
    }

    /**
     * <p>Setter method for pseudoRemark1.</p>
     *
     * @param pseudoRemark1 Set for pseudoRemark1
     */
    public void setPseudoRemark1(String pseudoRemark1) {
        this.pseudoRemark1 = pseudoRemark1;
    }

    /**
     * <p>Getter method for pseudoRemark2.</p>
     *
     * @return the pseudoRemark2
     */
    public String getPseudoRemark2() {
        return pseudoRemark2;
    }

    /**
     * <p>Setter method for pseudoRemark2.</p>
     *
     * @param pseudoRemark2 Set for pseudoRemark2
     */
    public void setPseudoRemark2(String pseudoRemark2) {
        this.pseudoRemark2 = pseudoRemark2;
    }

    /**
     * <p>Getter method for pseudoRemark3.</p>
     *
     * @return the pseudoRemark3
     */
    public String getPseudoRemark3() {
        return pseudoRemark3;
    }

    /**
     * <p>Setter method for pseudoRemark3.</p>
     *
     * @param pseudoRemark3 Set for pseudoRemark3
     */
    public void setPseudoRemark3(String pseudoRemark3) {
        this.pseudoRemark3 = pseudoRemark3;
    }

    /**
     * <p>Getter method for pseudoDCustomerPartNo.</p>
     *
     * @return the pseudoDCustomerPartNo
     */
    public String getPseudoDCustomerPartNo() {
        return pseudoDCustomerPartNo;
    }

    /**
     * <p>Setter method for pseudoDCustomerPartNo.</p>
     *
     * @param pseudoDCustomerPartNo Set for pseudoDCustomerPartNo
     */
    public void setPseudoDCustomerPartNo(String pseudoDCustomerPartNo) {
        this.pseudoDCustomerPartNo = pseudoDCustomerPartNo;
    }

    /**
     * <p>Getter method for pseudoSProcessCode.</p>
     *
     * @return the pseudoSProcessCode
     */
    public String getPseudoSProcessCode() {
        return pseudoSProcessCode;
    }

    /**
     * <p>Setter method for pseudoSProcessCode.</p>
     *
     * @param pseudoSProcessCode Set for pseudoSProcessCode
     */
    public void setPseudoSProcessCode(String pseudoSProcessCode) {
        this.pseudoSProcessCode = pseudoSProcessCode;
    }

    /**
     * <p>Getter method for pseudoTagOutput.</p>
     *
     * @return the pseudoTagOutput
     */
    public String getPseudoTagOutput() {
        return pseudoTagOutput;
    }

    /**
     * <p>Setter method for pseudoTagOutput.</p>
     *
     * @param pseudoTagOutput Set for pseudoTagOutput
     */
    public void setPseudoTagOutput(String pseudoTagOutput) {
        this.pseudoTagOutput = pseudoTagOutput;
    }
}
