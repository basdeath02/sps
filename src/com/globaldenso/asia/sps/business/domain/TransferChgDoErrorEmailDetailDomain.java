/*
 * ModifyDate Development company     Describe 
 * 2015/03/19 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;

/**
 * <p>
 * Transfer Change Delivery Order Error Email detail Domain.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class TransferChgDoErrorEmailDetailDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 2902804841339513015L;

    /** SPS_CIGMA_CHG_DO_ERROR data. */
    private SpsCigmaChgDoErrorDomain cigmaChgDoError;
    
    /** SPS_M_DENSO_SUPPLIER_PARTS data. */
    private SpsMDensoSupplierPartsDomain densoSupplerParts;
    
    /** The default constructor. */
    public TransferChgDoErrorEmailDetailDomain() {
        this.cigmaChgDoError = new SpsCigmaChgDoErrorDomain();
        this.densoSupplerParts = new SpsMDensoSupplierPartsDomain();
    }

    /**
     * <p>Getter method for densoSupplerParts.</p>
     *
     * @return the densoSupplerParts
     */
    public SpsMDensoSupplierPartsDomain getDensoSupplerParts() {
        return densoSupplerParts;
    }

    /**
     * <p>Setter method for densoSupplerParts.</p>
     *
     * @param densoSupplerParts Set for densoSupplerParts
     */
    public void setDensoSupplerParts(SpsMDensoSupplierPartsDomain densoSupplerParts) {
        this.densoSupplerParts = densoSupplerParts;
    }

    /**
     * <p>Getter method for cigmaChgDoError.</p>
     *
     * @return the cigmaChgDoError
     */
    public SpsCigmaChgDoErrorDomain getCigmaChgDoError() {
        return cigmaChgDoError;
    }

    /**
     * <p>Setter method for cigmaChgDoError.</p>
     *
     * @param cigmaChgDoError Set for cigmaChgDoError
     */
    public void setCigmaChgDoError(SpsCigmaChgDoErrorDomain cigmaChgDoError) {
        this.cigmaChgDoError = cigmaChgDoError;
    }

}
