/*
 * ModifyDate Development company    Describe 
 * 2014/08/15 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain;

/**
 * The Class AsnUploadingDomain.
 * @author CSI
 */
public class AsnUploadingReturnDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3934700137035979127L;
    
    /** The temp upload asn domain */
    private SpsTmpUploadAsnDomain tmpUploadAsnDomain;
    
    /** The shipping qty str */
    private String shippingQtyStr;
    
    /** The receiveByScan */
    private String receiveByScan;
    
    /** The whPrimeReceiving */
    private String whPrimeReceiving;

    /**
     * Instantiates a new ASN info domain.
     */
    public AsnUploadingReturnDomain() {
        super();
        tmpUploadAsnDomain = new SpsTmpUploadAsnDomain();
    }

    /**
     * Gets the tmp upload asn domain.
     * 
     * @return the tmp upload asn domain
     */
    public SpsTmpUploadAsnDomain getTmpUploadAsnDomain() {
        return tmpUploadAsnDomain;
    }

    /**
     * Sets the tmp upload asn domain.
     * 
     * @param tmpUploadAsnDomain the tmp upload asn domain
     */
    public void setTmpUploadAsnDomain(SpsTmpUploadAsnDomain tmpUploadAsnDomain) {
        this.tmpUploadAsnDomain = tmpUploadAsnDomain;
    }

    /**
     * Gets the shipping qty str.
     * 
     * @return the shipping qty str
     */
    public String getShippingQtyStr() {
        return shippingQtyStr;
    }

    /**
     * Sets the shipping qty str.
     * 
     * @param shippingQtyStr the shipping qty str
     */
    public void setShippingQtyStr(String shippingQtyStr) {
        this.shippingQtyStr = shippingQtyStr;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */

    public String getReceiveByScan() {
        return receiveByScan;
    }
    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */

    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    /**
     * Gets the whPrimeReceiving.
     * 
     * @return the whPrimeReceiving
     */

    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }
    /**
     * Sets the whPrimeReceiving.
     * 
     * @param whPrimeReceiving the whPrimeReceiving
     */

    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }
}