/*
 * ModifyDate Development company     Describe 
 * 2014/08/22 CSI Karnrawee           Create
 * 2015/08/06 CSI Akat                [IN010]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Invoice Cover Page Domain.
 * @author CSI
 */
public class InvoiceCoverPageDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;

    /** The sps transaction invoice domain. */
    private SpsTInvoiceDomain spsTInvoiceDomain;
    
    /** The List of Invoice Cover Page Detail Domain. */
    private List<InvoiceCoverPageDetailDomain> invoiceCoverPageDetailDomainList;
    
    // [IN010]
    /** Invoice Decimal Digit for Amount. */
    private String decimalDigit;
    
    /**
     * Instantiates a new Invoice Cover Page Domain.
     */
    public InvoiceCoverPageDomain() {
        super();
        this.decimalDigit = Constants.STR_ZERO;
        this.spsTInvoiceDomain = new SpsTInvoiceDomain();
    }
    
    /**
     * Gets the sps invoice domain.
     * 
     * @return the spsTInvoiceDomain.
     */
    public SpsTInvoiceDomain getSpsTInvoiceDomain() {
        return spsTInvoiceDomain;
    }
    
    /**
     * Sets the sps invoice domain.
     * 
     * @param spsTInvoiceDomain the sps invoice domain.
     */  
    public void setSpsTInvoiceDomain(SpsTInvoiceDomain spsTInvoiceDomain) {
        this.spsTInvoiceDomain = spsTInvoiceDomain;
    }

    /**
     * <p>Getter method for invoiceCoverPageDetailDomainList.</p>
     *
     * @return the invoiceCoverPageDetailDomainList
     */
    public List<InvoiceCoverPageDetailDomain> getInvoiceCoverPageDetailDomainList() {
        return invoiceCoverPageDetailDomainList;
    }

    /**
     * <p>Setter method for invoiceCoverPageDetailDomainList.</p>
     *
     * @param invoiceCoverPageDetailDomainList Set for invoiceCoverPageDetailDomainList
     */
    public void setInvoiceCoverPageDetailDomainList(
        List<InvoiceCoverPageDetailDomain> invoiceCoverPageDetailDomainList) {
        this.invoiceCoverPageDetailDomainList = invoiceCoverPageDetailDomainList;
    }

    /**
     * <p>Getter method for decimalDigit.</p>
     *
     * @return the decimalDigit
     */
    public String getDecimalDigit() {
        return decimalDigit;
    }

    /**
     * <p>Setter method for decimalDigit.</p>
     *
     * @param decimalDigit Set for decimalDigit
     */
    public void setDecimalDigit(String decimalDigit) {
        this.decimalDigit = decimalDigit;
    }

}