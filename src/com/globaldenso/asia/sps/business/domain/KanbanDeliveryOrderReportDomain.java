/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Chatchai                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * <p>
 * Kanban Delivery Order Report Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanDeliveryOrderReportDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The serial.
     * </p>
     */
    private static final long serialVersionUID = 5308626346334270165L;

    /**
     * <p>
     * The truck route.
     * </p>
     */
    private String routeNo;

    /**
     * <p>
     * The truck sequence.
     * </p>
     */
    private String delNo;

    /**
     * <p>
     * The supplier code.
     * </p>
     */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /**
     * <p>
     * The company name.
     * </p>
     */
    private String supplierName;

    /**
     * <p>
     * The supplier location.
     * </p>
     */
    private String supplierLocation;

    /**
     * <p>
     * The Supplier Plant Code.
     * </p>
     */
    private String sPcd;

    /**
     * <p>
     * The issue date of delivery order String.
     * </p>
     */
    private String issueDateString;

    /**
     * <p>
     * The issue date of delivery order Date.
     * </p>
     */
    private Date issueDate;

    /**
     * <p>
     * The page number.
     * </p>
     */
    private String page;

    /**
     * <p>
     * The current SPS D/O no.
     * </p>
     */
    private String currentSpsDoNo;

    /**
     * <p>
     * The SPS D/O no.
     * </p>
     */
    private String originalSpsDoNo;

    /**
     * <p>
     * The ship date (Estimate depart date) String.
     * </p>
     */
    private String etdString;

    /**
     * <p>
     * The ship date (Estimate depart date) Date.
     * </p>
     */
    private Timestamp etd;

    /**
     * <p>
     * The ship time (Estimate depart time).
     * </p>
     */
    private String etdTime;

    /**
     * <p>
     * The delivery date (Estimate arrival date) String.
     * </p>
     */
    private String etaString;

    /**
     * <p>
     * The delivery date (Estimate arrival date) Date.
     * </p>
     */
    private Timestamp eta;

    /**
     * <p>
     * The delivery time(Estimate arrival time).
     * </p>
     */
    private String etaTime;

    /**
     * <p>
     * The Denso plant code.
     * </p>
     */
    private String dPcd;

    /**
     * <p>
     * The warehouse for prime receiving.
     * </p>
     */
    private String warehouse;

    /**
     * <p>
     * The dock code.
     * </p>
     */
    private String dockCode;

    /**
     * <p>
     * The receiving lane.
     * </p>
     */
    private String rcvLane;

    /**
     * <p>
     * The transport mode.
     * </p>
     */
    private String trans;

    /**
     * <p>
     * The cycle.
     * </p>
     */
    private String cycle;

    /**
     * <p>
     * The control number.
     * </p>
     */
    private String ctrlNo;

    /**
     * <p>
     * The Denso part number.
     * </p>
     */
    private String dPn;

    /**
     * <p>
     * The supplier part number.
     * </p>
     */
    private String sPn;

    /**
     * <p>
     * The item description.
     * </p>
     */
    private String description;

    /**
     * <p>
     * The unit of measure.
     * </p>
     */
    private String unitOfMeasure;

    /**
     * <p>
     * The kanban sequence number.
     * </p>
     */
    private String kanbanSequence;

    /**
     * <p>
     * The quantity per box.
     * </p>
     */
    private String quantityPerBox;

    /**
     * <p>
     * The quantity per box BigDecimal.
     * </p>
     */
    private BigDecimal quantityPerBoxBigDec;

    /**
     * <p>
     * The number of box.
     * </p>
     */
    private String noOfBox;

    /**
     * <p>
     * The number of box BigDecimal.
     * </p>
     */
    private BigDecimal noOfBoxBigDec;

    /**
     * <p>
     * The current order qty.
     * </p>
     */
    private String currentQty;

    /**
     * <p>
     * The current order qty BigDecimal.
     * </p>
     */
    private BigDecimal currentQtyBigDec;

    /**
     * <p>
     * The change cigma D/O no.
     * </p>
     */
    private String currentCigmaDoNo;

    /**
     * <p>
     * The supplier company name.
     * </p>
     */
    private String companySupplierName;

    /**
     * <p>
     * The Denso company name.
     * </p>
     */
    private String companyDensoName;

    /**
     * <p>
     * The Revision.
     * </p>
     */
    private String revision;
    
    /**
     * <p>The Total Page.</p>
     */
    private String totalPage;
    
    /**
     * <p>The Text Report Name.</p>
     */
    private String txtReportName;
    
    /**
     * <p>The Text Route No.</p>
     */
    private String txtRouteNo;
    
    /**
     * <p>The Text Del No.</p>
     */
    private String txtDelNo;
    
    /**
     * <p>The Text SCd.</p>
     */
    private String txtSCd;
    
    /**
     * <p>The Text Sup Name.</p>
     */
    private String txtSupName;
    
    /**
     * <p>The Text Sup Loc.</p>
     */
    private String txtSupLoc;
    
    /**
     * <p>The Text Sup Plant.</p>
     */
    private String txtSupPlant;
    
    /**
     * <p>The Text Issue Date.</p>
     */
    private String txtIssueDate;
    
    /**
     * <p>The Text Page.</p>
     */
    private String txtPage;
    
    /**
     * <p>The Text Currrent.</p>
     */
    private String txtCurrrent;
    
    /**
     * <p>The Text Original.</p>
     */
    private String txtOriginal;
    
    /**
     * <p>The Text Etd.</p>
     */
    private String txtEtd;
    
    /**
     * <p>The Text Ship Date.</p>
     */
    private String txtShipDate;
    
    /**
     * <p>The Text Time.</p>
     */
    private String txtTime;
    
    /**
     * <p>The Text Eta.</p>
     */
    private String txtEta;
    
    /**
     * <p>The Text Del Date.</p>
     */
    private String txtDelDate;
    
    /**
     * <p>The Text SPcd.</p>
     */
    private String txtSPcd;
    
    /**
     * <p>The Text Ware House.</p>
     */
    private String txtWareHouse;
    
    /**
     * <p>The Text Dock Code.</p>
     */
    private String txtDockCode;
    
    /**
     * <p>The Text Rcv Lane.</p>
     */
    private String txtRcvLane;
    
    /**
     * <p>The Text Tran.</p>
     */
    private String txtTran;
    
    /**
     * <p>The Text Cycle.</p>
     */
    private String txtCycle;
    
    /**
     * <p>The Text Ctrl No.</p>
     */
    private String txtCtrlNo;
    
    /**
     * <p>The Text dPn.</p>
     */
    private String txtdPn;
    
    /**
     * <p>The Text sPn.</p>
     */
    private String txtsPn;
    
    /**
     * <p>The Text Desc.</p>
     */
    private String txtDesc;
    
    /**
     * <p>The Text Um.</p>
     */
    private String txtUm;
    
    /**
     * <p>The Text Kanban Seq No.</p>
     */
    private String txtKanbanSeqNo;
    
    /**
     * <p>The Text Qty Per Box.</p>
     */
    private String txtQtyPerBox;
    
    /**
     * <p>The Text No Of Box.</p>
     */
    private String txtNoOfBox;
    
    /**
     * <p>The Text Curr Qty.</p>
     */
    private String txtCurrQty;
    
    /**
     * <p>The Text Curr Cigma.</p>
     */
    private String txtCurrCigma;
    
    /**
     * <p>The Text Supplier.</p>
     */
    private String txtSupplier;
    
    /**
     * <p>The Text Sing.</p>
     */
    private String txtSing;
    
    /**
     * <p>The Denso Code.</p>
     */
    private String dCd;
    
    /** Flag for show KANBAN SEQ in report. */
    private String kanbanSeqFlag;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public KanbanDeliveryOrderReportDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for routeNo.
     * </p>
     * 
     * @return the routeNo
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * <p>
     * Setter method for routeNo.
     * </p>
     * 
     * @param routeNo Set for routeNo
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * <p>
     * Getter method for delNo.
     * </p>
     * 
     * @return the delNo
     */
    public String getDelNo() {
        return delNo;
    }

    /**
     * <p>
     * Setter method for delNo.
     * </p>
     * 
     * @param delNo Set for delNo
     */
    public void setDelNo(String delNo) {
        this.delNo = delNo;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Getter method for supplierName.
     * </p>
     * 
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>
     * Setter method for supplierName.
     * </p>
     * 
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * <p>
     * Getter method for supplierLocation.
     * </p>
     * 
     * @return the supplierLocation
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * <p>
     * Setter method for supplierLocation.
     * </p>
     * 
     * @param supplierLocation Set for supplierLocation
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for issueDateString.
     * </p>
     * 
     * @return the issueDateString
     */
    public String getIssueDateString() {
        return issueDateString;
    }

    /**
     * <p>
     * Setter method for issueDateString.
     * </p>
     * 
     * @param issueDateString Set for issueDateString
     */
    public void setIssueDateString(String issueDateString) {
        this.issueDateString = issueDateString;
    }

    /**
     * <p>
     * Getter method for issueDate.
     * </p>
     * 
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * <p>
     * Setter method for issueDate.
     * </p>
     * 
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * <p>
     * Getter method for page.
     * </p>
     * 
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * <p>
     * Setter method for page.
     * </p>
     * 
     * @param page Set for page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * <p>
     * Getter method for currentSpsDoNo.
     * </p>
     * 
     * @return the currentSpsDoNo
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }

    /**
     * <p>
     * Setter method for currentSpsDoNo.
     * </p>
     * 
     * @param currentSpsDoNo Set for currentSpsDoNo
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }

    /**
     * <p>
     * Getter method for originalSpsDoNo.
     * </p>
     * 
     * @return the originalSpsDoNo
     */
    public String getOriginalSpsDoNo() {
        return originalSpsDoNo;
    }

    /**
     * <p>
     * Setter method for originalSpsDoNo.
     * </p>
     * 
     * @param originalSpsDoNo Set for originalSpsDoNo
     */
    public void setOriginalSpsDoNo(String originalSpsDoNo) {
        this.originalSpsDoNo = originalSpsDoNo;
    }

    /**
     * <p>
     * Getter method for etdString.
     * </p>
     * 
     * @return the etdString
     */
    public String getEtdString() {
        return etdString;
    }

    /**
     * <p>
     * Setter method for etdString.
     * </p>
     * 
     * @param etdString Set for etdString
     */
    public void setEtdString(String etdString) {
        this.etdString = etdString;
    }

    /**
     * <p>
     * Getter method for etd.
     * </p>
     * 
     * @return the etd
     */
    public Timestamp getEtd() {
        return etd;
    }

    /**
     * <p>
     * Setter method for etd.
     * </p>
     * 
     * @param etd Set for etd
     */
    public void setEtd(Timestamp etd) {
        this.etd = etd;
    }

    /**
     * <p>
     * Getter method for etdTime.
     * </p>
     * 
     * @return the etdTime
     */
    public String getEtdTime() {
        return etdTime;
    }

    /**
     * <p>
     * Setter method for etdTime.
     * </p>
     * 
     * @param etdTime Set for etdTime
     */
    public void setEtdTime(String etdTime) {
        this.etdTime = etdTime;
    }

    /**
     * <p>
     * Getter method for etaString.
     * </p>
     * 
     * @return the etaString
     */
    public String getEtaString() {
        return etaString;
    }

    /**
     * <p>
     * Setter method for etaString.
     * </p>
     * 
     * @param etaString Set for etaString
     */
    public void setEtaString(String etaString) {
        this.etaString = etaString;
    }

    /**
     * <p>
     * Getter method for eta.
     * </p>
     * 
     * @return the eta
     */
    public Timestamp getEta() {
        return eta;
    }

    /**
     * <p>
     * Setter method for eta.
     * </p>
     * 
     * @param eta Set for eta
     */
    public void setEta(Timestamp eta) {
        this.eta = eta;
    }

    /**
     * <p>
     * Getter method for etaTime.
     * </p>
     * 
     * @return the etaTime
     */
    public String getEtaTime() {
        return etaTime;
    }

    /**
     * <p>
     * Setter method for etaTime.
     * </p>
     * 
     * @param etaTime Set for etaTime
     */
    public void setEtaTime(String etaTime) {
        this.etaTime = etaTime;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for warehouse.
     * </p>
     * 
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * <p>
     * Setter method for warehouse.
     * </p>
     * 
     * @param warehouse Set for warehouse
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * <p>
     * Getter method for dockCode.
     * </p>
     * 
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * <p>
     * Setter method for dockCode.
     * </p>
     * 
     * @param dockCode Set for dockCode
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * <p>
     * Getter method for rcvLane.
     * </p>
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>
     * Setter method for rcvLane.
     * </p>
     * 
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>
     * Getter method for trans.
     * </p>
     * 
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * <p>
     * Setter method for trans.
     * </p>
     * 
     * @param trans Set for trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * <p>
     * Getter method for cycle.
     * </p>
     * 
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * <p>
     * Setter method for cycle.
     * </p>
     * 
     * @param cycle Set for cycle
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * <p>
     * Getter method for ctrlNo.
     * </p>
     * 
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * <p>
     * Setter method for ctrlNo.
     * </p>
     * 
     * @param ctrlNo Set for ctrlNo
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * <p>
     * Getter method for dPn.
     * </p>
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>
     * Setter method for dPn.
     * </p>
     * 
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>
     * Getter method for sPn.
     * </p>
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>
     * Setter method for sPn.
     * </p>
     * 
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>
     * Getter method for description.
     * </p>
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>
     * Setter method for description.
     * </p>
     * 
     * @param description Set for description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>
     * Getter method for unitOfMeasure.
     * </p>
     * 
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * <p>
     * Setter method for unitOfMeasure.
     * </p>
     * 
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>
     * Getter method for kanbanSequence.
     * </p>
     * 
     * @return the kanbanSequence
     */
    public String getKanbanSequence() {
        return kanbanSequence;
    }

    /**
     * <p>
     * Setter method for kanbanSequence.
     * </p>
     * 
     * @param kanbanSequence Set for kanbanSequence
     */
    public void setKanbanSequence(String kanbanSequence) {
        this.kanbanSequence = kanbanSequence;
    }

    /**
     * <p>
     * Getter method for quantityPerBox.
     * </p>
     * 
     * @return the quantityPerBox
     */
    public String getQuantityPerBox() {
        return quantityPerBox;
    }

    /**
     * <p>
     * Setter method for quantityPerBox.
     * </p>
     * 
     * @param quantityPerBox Set for quantityPerBox
     */
    public void setQuantityPerBox(String quantityPerBox) {
        this.quantityPerBox = quantityPerBox;
    }

    /**
     * <p>
     * Getter method for quantityPerBoxBigDec.
     * </p>
     * 
     * @return the quantityPerBoxBigDec
     */
    public BigDecimal getQuantityPerBoxBigDec() {
        return quantityPerBoxBigDec;
    }

    /**
     * <p>
     * Setter method for quantityPerBoxBigDec.
     * </p>
     * 
     * @param quantityPerBoxBigDec Set for quantityPerBoxBigDec
     */
    public void setQuantityPerBoxBigDec(BigDecimal quantityPerBoxBigDec) {
        this.quantityPerBoxBigDec = quantityPerBoxBigDec;
    }

    /**
     * <p>
     * Getter method for noOfBox.
     * </p>
     * 
     * @return the noOfBox
     */
    public String getNoOfBox() {
        return noOfBox;
    }

    /**
     * <p>
     * Setter method for noOfBox.
     * </p>
     * 
     * @param noOfBox Set for noOfBox
     */
    public void setNoOfBox(String noOfBox) {
        this.noOfBox = noOfBox;
    }

    /**
     * <p>
     * Getter method for noOfBoxBigDec.
     * </p>
     * 
     * @return the noOfBoxBigDec
     */
    public BigDecimal getNoOfBoxBigDec() {
        return noOfBoxBigDec;
    }

    /**
     * <p>
     * Setter method for noOfBoxBigDec.
     * </p>
     * 
     * @param noOfBoxBigDec Set for noOfBoxBigDec
     */
    public void setNoOfBoxBigDec(BigDecimal noOfBoxBigDec) {
        this.noOfBoxBigDec = noOfBoxBigDec;
    }

    /**
     * <p>
     * Getter method for currentQty.
     * </p>
     * 
     * @return the currentQty
     */
    public String getCurrentQty() {
        return currentQty;
    }

    /**
     * <p>
     * Setter method for currentQty.
     * </p>
     * 
     * @param currentQty Set for currentQty
     */
    public void setCurrentQty(String currentQty) {
        this.currentQty = currentQty;
    }

    /**
     * <p>
     * Getter method for currentQtyBigDec.
     * </p>
     * 
     * @return the currentQtyBigDec
     */
    public BigDecimal getCurrentQtyBigDec() {
        return currentQtyBigDec;
    }

    /**
     * <p>
     * Setter method for currentQtyBigDec.
     * </p>
     * 
     * @param currentQtyBigDec Set for currentQtyBigDec
     */
    public void setCurrentQtyBigDec(BigDecimal currentQtyBigDec) {
        this.currentQtyBigDec = currentQtyBigDec;
    }

    /**
     * <p>
     * Getter method for currentCigmaDoNo.
     * </p>
     * 
     * @return the currentCigmaDoNo
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * <p>
     * Setter method for currentCigmaDoNo.
     * </p>
     * 
     * @param currentCigmaDoNo Set for currentCigmaDoNo
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * <p>
     * Getter method for companySupplierName.
     * </p>
     * 
     * @return the companySupplierName
     */
    public String getCompanySupplierName() {
        return companySupplierName;
    }

    /**
     * <p>
     * Setter method for companySupplierName.
     * </p>
     * 
     * @param companySupplierName Set for companySupplierName
     */
    public void setCompanySupplierName(String companySupplierName) {
        this.companySupplierName = companySupplierName;
    }

    /**
     * <p>
     * Getter method for companyDensoName.
     * </p>
     * 
     * @return the companyDensoName
     */
    public String getCompanyDensoName() {
        return companyDensoName;
    }

    /**
     * <p>
     * Setter method for companyDensoName.
     * </p>
     * 
     * @param companyDensoName Set for companyDensoName
     */
    public void setCompanyDensoName(String companyDensoName) {
        this.companyDensoName = companyDensoName;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>Getter method for totalPage.</p>
     *
     * @return the totalPage
     */
    public String getTotalPage() {
        return totalPage;
    }

    /**
     * <p>Setter method for totalPage.</p>
     *
     * @param totalPage Set for totalPage
     */
    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * <p>Getter method for txtReportName.</p>
     *
     * @return the txtReportName
     */
    public String getTxtReportName() {
        return txtReportName;
    }

    /**
     * <p>Setter method for txtReportName.</p>
     *
     * @param txtReportName Set for txtReportName
     */
    public void setTxtReportName(String txtReportName) {
        this.txtReportName = txtReportName;
    }

    /**
     * <p>Getter method for txtRouteNo.</p>
     *
     * @return the txtRouteNo
     */
    public String getTxtRouteNo() {
        return txtRouteNo;
    }

    /**
     * <p>Setter method for txtRouteNo.</p>
     *
     * @param txtRouteNo Set for txtRouteNo
     */
    public void setTxtRouteNo(String txtRouteNo) {
        this.txtRouteNo = txtRouteNo;
    }

    /**
     * <p>Getter method for txtDelNo.</p>
     *
     * @return the txtDelNo
     */
    public String getTxtDelNo() {
        return txtDelNo;
    }

    /**
     * <p>Setter method for txtDelNo.</p>
     *
     * @param txtDelNo Set for txtDelNo
     */
    public void setTxtDelNo(String txtDelNo) {
        this.txtDelNo = txtDelNo;
    }

    /**
     * <p>Getter method for txtSCd.</p>
     *
     * @return the txtSCd
     */
    public String getTxtSCd() {
        return txtSCd;
    }

    /**
     * <p>Setter method for txtSCd.</p>
     *
     * @param txtSCd Set for txtSCd
     */
    public void setTxtSCd(String txtSCd) {
        this.txtSCd = txtSCd;
    }

    /**
     * <p>Getter method for txtSupName.</p>
     *
     * @return the txtSupName
     */
    public String getTxtSupName() {
        return txtSupName;
    }

    /**
     * <p>Setter method for txtSupName.</p>
     *
     * @param txtSupName Set for txtSupName
     */
    public void setTxtSupName(String txtSupName) {
        this.txtSupName = txtSupName;
    }

    /**
     * <p>Getter method for txtSupLoc.</p>
     *
     * @return the txtSupLoc
     */
    public String getTxtSupLoc() {
        return txtSupLoc;
    }

    /**
     * <p>Setter method for txtSupLoc.</p>
     *
     * @param txtSupLoc Set for txtSupLoc
     */
    public void setTxtSupLoc(String txtSupLoc) {
        this.txtSupLoc = txtSupLoc;
    }

    /**
     * <p>Getter method for txtSupPlant.</p>
     *
     * @return the txtSupPlant
     */
    public String getTxtSupPlant() {
        return txtSupPlant;
    }

    /**
     * <p>Setter method for txtSupPlant.</p>
     *
     * @param txtSupPlant Set for txtSupPlant
     */
    public void setTxtSupPlant(String txtSupPlant) {
        this.txtSupPlant = txtSupPlant;
    }

    /**
     * <p>Getter method for txtIssueDate.</p>
     *
     * @return the txtIssueDate
     */
    public String getTxtIssueDate() {
        return txtIssueDate;
    }

    /**
     * <p>Setter method for txtIssueDate.</p>
     *
     * @param txtIssueDate Set for txtIssueDate
     */
    public void setTxtIssueDate(String txtIssueDate) {
        this.txtIssueDate = txtIssueDate;
    }

    /**
     * <p>Getter method for txtPage.</p>
     *
     * @return the txtPage
     */
    public String getTxtPage() {
        return txtPage;
    }

    /**
     * <p>Setter method for txtPage.</p>
     *
     * @param txtPage Set for txtPage
     */
    public void setTxtPage(String txtPage) {
        this.txtPage = txtPage;
    }

    /**
     * <p>Getter method for txtCurrrent.</p>
     *
     * @return the txtCurrrent
     */
    public String getTxtCurrrent() {
        return txtCurrrent;
    }

    /**
     * <p>Setter method for txtCurrrent.</p>
     *
     * @param txtCurrrent Set for txtCurrrent
     */
    public void setTxtCurrrent(String txtCurrrent) {
        this.txtCurrrent = txtCurrrent;
    }

    /**
     * <p>Getter method for txtOriginal.</p>
     *
     * @return the txtOriginal
     */
    public String getTxtOriginal() {
        return txtOriginal;
    }

    /**
     * <p>Setter method for txtOriginal.</p>
     *
     * @param txtOriginal Set for txtOriginal
     */
    public void setTxtOriginal(String txtOriginal) {
        this.txtOriginal = txtOriginal;
    }

    /**
     * <p>Getter method for txtEtd.</p>
     *
     * @return the txtEtd
     */
    public String getTxtEtd() {
        return txtEtd;
    }

    /**
     * <p>Setter method for txtEtd.</p>
     *
     * @param txtEtd Set for txtEtd
     */
    public void setTxtEtd(String txtEtd) {
        this.txtEtd = txtEtd;
    }

    /**
     * <p>Getter method for txtShipDate.</p>
     *
     * @return the txtShipDate
     */
    public String getTxtShipDate() {
        return txtShipDate;
    }

    /**
     * <p>Setter method for txtShipDate.</p>
     *
     * @param txtShipDate Set for txtShipDate
     */
    public void setTxtShipDate(String txtShipDate) {
        this.txtShipDate = txtShipDate;
    }

    /**
     * <p>Getter method for txtTime.</p>
     *
     * @return the txtTime
     */
    public String getTxtTime() {
        return txtTime;
    }

    /**
     * <p>Setter method for txtTime.</p>
     *
     * @param txtTime Set for txtTime
     */
    public void setTxtTime(String txtTime) {
        this.txtTime = txtTime;
    }

    /**
     * <p>Getter method for txtEta.</p>
     *
     * @return the txtEta
     */
    public String getTxtEta() {
        return txtEta;
    }

    /**
     * <p>Setter method for txtEta.</p>
     *
     * @param txtEta Set for txtEta
     */
    public void setTxtEta(String txtEta) {
        this.txtEta = txtEta;
    }

    /**
     * <p>Getter method for txtDelDate.</p>
     *
     * @return the txtDelDate
     */
    public String getTxtDelDate() {
        return txtDelDate;
    }

    /**
     * <p>Setter method for txtDelDate.</p>
     *
     * @param txtDelDate Set for txtDelDate
     */
    public void setTxtDelDate(String txtDelDate) {
        this.txtDelDate = txtDelDate;
    }

    /**
     * <p>Getter method for txtSPcd.</p>
     *
     * @return the txtSPcd
     */
    public String getTxtSPcd() {
        return txtSPcd;
    }

    /**
     * <p>Setter method for txtSPcd.</p>
     *
     * @param txtSPcd Set for txtSPcd
     */
    public void setTxtSPcd(String txtSPcd) {
        this.txtSPcd = txtSPcd;
    }

    /**
     * <p>Getter method for txtWareHouse.</p>
     *
     * @return the txtWareHouse
     */
    public String getTxtWareHouse() {
        return txtWareHouse;
    }

    /**
     * <p>Setter method for txtWareHouse.</p>
     *
     * @param txtWareHouse Set for txtWareHouse
     */
    public void setTxtWareHouse(String txtWareHouse) {
        this.txtWareHouse = txtWareHouse;
    }

    /**
     * <p>Getter method for txtDockCode.</p>
     *
     * @return the txtDockCode
     */
    public String getTxtDockCode() {
        return txtDockCode;
    }

    /**
     * <p>Setter method for txtDockCode.</p>
     *
     * @param txtDockCode Set for txtDockCode
     */
    public void setTxtDockCode(String txtDockCode) {
        this.txtDockCode = txtDockCode;
    }

    /**
     * <p>Getter method for txtRcvLane.</p>
     *
     * @return the txtRcvLane
     */
    public String getTxtRcvLane() {
        return txtRcvLane;
    }

    /**
     * <p>Setter method for txtRcvLane.</p>
     *
     * @param txtRcvLane Set for txtRcvLane
     */
    public void setTxtRcvLane(String txtRcvLane) {
        this.txtRcvLane = txtRcvLane;
    }

    /**
     * <p>Getter method for txtTran.</p>
     *
     * @return the txtTran
     */
    public String getTxtTran() {
        return txtTran;
    }

    /**
     * <p>Setter method for txtTran.</p>
     *
     * @param txtTran Set for txtTran
     */
    public void setTxtTran(String txtTran) {
        this.txtTran = txtTran;
    }

    /**
     * <p>Getter method for txtCycle.</p>
     *
     * @return the txtCycle
     */
    public String getTxtCycle() {
        return txtCycle;
    }

    /**
     * <p>Setter method for txtCycle.</p>
     *
     * @param txtCycle Set for txtCycle
     */
    public void setTxtCycle(String txtCycle) {
        this.txtCycle = txtCycle;
    }

    /**
     * <p>Getter method for txtCtrlNo.</p>
     *
     * @return the txtCtrlNo
     */
    public String getTxtCtrlNo() {
        return txtCtrlNo;
    }

    /**
     * <p>Setter method for txtCtrlNo.</p>
     *
     * @param txtCtrlNo Set for txtCtrlNo
     */
    public void setTxtCtrlNo(String txtCtrlNo) {
        this.txtCtrlNo = txtCtrlNo;
    }

    /**
     * <p>Getter method for txtdPn.</p>
     *
     * @return the txtdPn
     */
    public String getTxtdPn() {
        return txtdPn;
    }

    /**
     * <p>Setter method for txtdPn.</p>
     *
     * @param txtdPn Set for txtdPn
     */
    public void setTxtdPn(String txtdPn) {
        this.txtdPn = txtdPn;
    }

    /**
     * <p>Getter method for txtsPn.</p>
     *
     * @return the txtsPn
     */
    public String getTxtsPn() {
        return txtsPn;
    }

    /**
     * <p>Setter method for txtsPn.</p>
     *
     * @param txtsPn Set for txtsPn
     */
    public void setTxtsPn(String txtsPn) {
        this.txtsPn = txtsPn;
    }

    /**
     * <p>Getter method for txtDesc.</p>
     *
     * @return the txtDesc
     */
    public String getTxtDesc() {
        return txtDesc;
    }

    /**
     * <p>Setter method for txtDesc.</p>
     *
     * @param txtDesc Set for txtDesc
     */
    public void setTxtDesc(String txtDesc) {
        this.txtDesc = txtDesc;
    }

    /**
     * <p>Getter method for txtUm.</p>
     *
     * @return the txtUm
     */
    public String getTxtUm() {
        return txtUm;
    }

    /**
     * <p>Setter method for txtUm.</p>
     *
     * @param txtUm Set for txtUm
     */
    public void setTxtUm(String txtUm) {
        this.txtUm = txtUm;
    }

    /**
     * <p>Getter method for txtKanbanSeqNo.</p>
     *
     * @return the txtKanbanSeqNo
     */
    public String getTxtKanbanSeqNo() {
        return txtKanbanSeqNo;
    }

    /**
     * <p>Setter method for txtKanbanSeqNo.</p>
     *
     * @param txtKanbanSeqNo Set for txtKanbanSeqNo
     */
    public void setTxtKanbanSeqNo(String txtKanbanSeqNo) {
        this.txtKanbanSeqNo = txtKanbanSeqNo;
    }

    /**
     * <p>Getter method for txtQtyPerBox.</p>
     *
     * @return the txtQtyPerBox
     */
    public String getTxtQtyPerBox() {
        return txtQtyPerBox;
    }

    /**
     * <p>Setter method for txtQtyPerBox.</p>
     *
     * @param txtQtyPerBox Set for txtQtyPerBox
     */
    public void setTxtQtyPerBox(String txtQtyPerBox) {
        this.txtQtyPerBox = txtQtyPerBox;
    }

    /**
     * <p>Getter method for txtNoOfBox.</p>
     *
     * @return the txtNoOfBox
     */
    public String getTxtNoOfBox() {
        return txtNoOfBox;
    }

    /**
     * <p>Setter method for txtNoOfBox.</p>
     *
     * @param txtNoOfBox Set for txtNoOfBox
     */
    public void setTxtNoOfBox(String txtNoOfBox) {
        this.txtNoOfBox = txtNoOfBox;
    }

    /**
     * <p>Getter method for txtCurrQty.</p>
     *
     * @return the txtCurrQty
     */
    public String getTxtCurrQty() {
        return txtCurrQty;
    }

    /**
     * <p>Setter method for txtCurrQty.</p>
     *
     * @param txtCurrQty Set for txtCurrQty
     */
    public void setTxtCurrQty(String txtCurrQty) {
        this.txtCurrQty = txtCurrQty;
    }

    /**
     * <p>Getter method for txtCurrCigma.</p>
     *
     * @return the txtCurrCigma
     */
    public String getTxtCurrCigma() {
        return txtCurrCigma;
    }

    /**
     * <p>Setter method for txtCurrCigma.</p>
     *
     * @param txtCurrCigma Set for txtCurrCigma
     */
    public void setTxtCurrCigma(String txtCurrCigma) {
        this.txtCurrCigma = txtCurrCigma;
    }

    /**
     * <p>Getter method for txtSupplier.</p>
     *
     * @return the txtSupplier
     */
    public String getTxtSupplier() {
        return txtSupplier;
    }

    /**
     * <p>Setter method for txtSupplier.</p>
     *
     * @param txtSupplier Set for txtSupplier
     */
    public void setTxtSupplier(String txtSupplier) {
        this.txtSupplier = txtSupplier;
    }

    /**
     * <p>Getter method for txtSing.</p>
     *
     * @return the txtSing
     */
    public String getTxtSing() {
        return txtSing;
    }

    /**
     * <p>Setter method for txtSing.</p>
     *
     * @param txtSing Set for txtSing
     */
    public void setTxtSing(String txtSing) {
        this.txtSing = txtSing;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for kanbanSeqFlag.</p>
     *
     * @return the kanbanSeqFlag
     */
    public String getKanbanSeqFlag() {
        return kanbanSeqFlag;
    }

    /**
     * <p>Setter method for kanbanSeqFlag.</p>
     *
     * @param kanbanSeqFlag Set for kanbanSeqFlag
     */
    public void setKanbanSeqFlag(String kanbanSeqFlag) {
        this.kanbanSeqFlag = kanbanSeqFlag;
    }

}
