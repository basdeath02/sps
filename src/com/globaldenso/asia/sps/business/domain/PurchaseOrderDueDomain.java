/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2015/09/17 CSI Akat                FIX wrong format
 * 2016/02/23 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;

/**
 * The Class Purchase Order Acknowledge Domain.
 * @author CSI
 */
public class PurchaseOrderDueDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3707882764066589551L;

    /** The flag for identify which PO Due has been modified. */
    private String changePoDueFlag;
    
    /** The DENSO Part No. */
    private String dPn;
    
    /** The supplier Part No. */
    private String sPn;
    
    /** The Last update date time for detail. */
    private Timestamp detailUpdateDatetime;

    /** The Pending Reason. */
    private String pendingReason;

    /** The Report Type Flag Display. */
    private String reportTypeFlgDisplay;
    
    /** Proposed Date in String type. */
    private String stringProposedDate;
    
    /** Proposed Quantity in String type. */
    private String stringProposedQty;

    /** Proposed Date in String type for backup when submit. */
    private String stringProposedDateBackup;
    
    /** Proposed Quantity in String type for backup when submit. */
    private String stringProposedQtyBackup;

    /** The Pending Reason for backup when submit. */
    private String pendingReasonBackup;
    
    /** The domain for SPS_T_PO. */
    private SpsTPoDomain spsTPoDomain;
    
    /** The domain for SPS_T_PO_DUE. */
    private SpsTPoDueDomain spsTPoDueDomain;
    
    // FIX : wrong dateformat
    /** The formated ETD to show in screen. */
    private String etdShow;
    
    // Start : [IN054] Add new column Accept/Reject for input
    /** Accept or Reject. */
    private String acceptReject;
    
    /** Backup for Accept or Reject. */
    private String acceptRejectBackup;
    // Start : [IN054] Add new column Accept/Reject for input
    
    /**
     * Instantiates a new Purchase Order Acknowledge Domain.
     */
    public PurchaseOrderDueDomain() {
        super();
        this.spsTPoDomain = new SpsTPoDomain();
        this.spsTPoDueDomain = new SpsTPoDueDomain();
    }

    /**
     * Gets the DENSO Part No.
     * 
     * @return the DENSO Part No.
     */
    public String getDPn() {
        return dPn;
    }
    /**
     * Sets the DENSO Part No.
     * 
     * @param dPn the DENSO Part No.
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    /**
     * Gets the Supplier Part No.
     * 
     * @return the Supplier Part No.
     */
    public String getSPn() {
        return sPn;
    }
    /**
     * Sets the Supplier Part No.
     * 
     * @param sPn the Supplier Part No.
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    /**
     * Gets the Pending Reason.
     * 
     * @return the Pending Reason.
     */
    public String getPendingReason() {
        return pendingReason;
    }
    /**
     * Sets the Pending Reason.
     * 
     * @param pendingReason the Pending Reason.
     */
    public void setPendingReason(String pendingReason) {
        this.pendingReason = pendingReason;
    }
    /**
     * Gets the Report Type Flag Displayed.
     * 
     * @return the Report Type Flag Displayed.
     */
    public String getReportTypeFlgDisplay() {
        return reportTypeFlgDisplay;
    }
    /**
     * Sets the Report Type Flag Displayed.
     * 
     * @param reportTypeFlgDisplay the Report Type Flag Displayed.
     */
    public void setReportTypeFlgDisplay(String reportTypeFlgDisplay) {
        this.reportTypeFlgDisplay = reportTypeFlgDisplay;
    }

    /**
     * <p>Getter method for spsTPoDomain.</p>
     *
     * @return the spsTPoDomain
     */
    public SpsTPoDomain getSpsTPoDomain() {
        return spsTPoDomain;
    }

    /**
     * <p>Setter method for spsTPoDomain.</p>
     *
     * @param spsTPoDomain Set for spsTPoDomain
     */
    public void setSpsTPoDomain(SpsTPoDomain spsTPoDomain) {
        this.spsTPoDomain = spsTPoDomain;
    }

    /**
     * <p>Getter method for spsTPoDueDomain.</p>
     *
     * @return the spsTPoDueDomain
     */
    public SpsTPoDueDomain getSpsTPoDueDomain() {
        return spsTPoDueDomain;
    }

    /**
     * <p>Setter method for spsTPoDueDomain.</p>
     *
     * @param spsTPoDueDomain Set for spsTPoDueDomain
     */
    public void setSpsTPoDueDomain(SpsTPoDueDomain spsTPoDueDomain) {
        this.spsTPoDueDomain = spsTPoDueDomain;
    }

    /**
     * <p>Getter method for stringProposedDate.</p>
     *
     * @return the stringProposedDate
     */
    public String getStringProposedDate() {
        return stringProposedDate;
    }

    /**
     * <p>Setter method for stringProposedDate.</p>
     *
     * @param stringProposedDate Set for stringProposedDate
     */
    public void setStringProposedDate(String stringProposedDate) {
        this.stringProposedDate = stringProposedDate;
    }

    /**
     * <p>Getter method for stringProposedQty.</p>
     *
     * @return the stringProposedQty
     */
    public String getStringProposedQty() {
        return stringProposedQty;
    }

    /**
     * <p>Setter method for stringProposedQty.</p>
     *
     * @param stringProposedQty Set for stringProposedQty
     */
    public void setStringProposedQty(String stringProposedQty) {
        this.stringProposedQty = stringProposedQty;
    }

    /**
     * <p>Getter method for changePoDueFlag.</p>
     *
     * @return the changePoDueFlag
     */
    public String getChangePoDueFlag() {
        return changePoDueFlag;
    }

    /**
     * <p>Setter method for changePoDueFlag.</p>
     *
     * @param changePoDueFlag Set for changePoDueFlag
     */
    public void setChangePoDueFlag(String changePoDueFlag) {
        this.changePoDueFlag = changePoDueFlag;
    }

    /**
     * <p>Getter method for stringProposedDateBackup.</p>
     *
     * @return the stringProposedDateBackup
     */
    public String getStringProposedDateBackup() {
        return stringProposedDateBackup;
    }

    /**
     * <p>Setter method for stringProposedDateBackup.</p>
     *
     * @param stringProposedDateBackup Set for stringProposedDateBackup
     */
    public void setStringProposedDateBackup(String stringProposedDateBackup) {
        this.stringProposedDateBackup = stringProposedDateBackup;
    }

    /**
     * <p>Getter method for stringProposedQtyBackup.</p>
     *
     * @return the stringProposedQtyBackup
     */
    public String getStringProposedQtyBackup() {
        return stringProposedQtyBackup;
    }

    /**
     * <p>Setter method for stringProposedQtyBackup.</p>
     *
     * @param stringProposedQtyBackup Set for stringProposedQtyBackup
     */
    public void setStringProposedQtyBackup(String stringProposedQtyBackup) {
        this.stringProposedQtyBackup = stringProposedQtyBackup;
    }

    /**
     * <p>Getter method for pendingReasonBackup.</p>
     *
     * @return the pendingReasonBackup
     */
    public String getPendingReasonBackup() {
        return pendingReasonBackup;
    }

    /**
     * <p>Setter method for pendingReasonBackup.</p>
     *
     * @param pendingReasonBackup Set for pendingReasonBackup
     */
    public void setPendingReasonBackup(String pendingReasonBackup) {
        this.pendingReasonBackup = pendingReasonBackup;
    }

    /**
     * <p>Getter method for detailUpdateDatetime.</p>
     *
     * @return the detailUpdateDatetime
     */
    public Timestamp getDetailUpdateDatetime() {
        return detailUpdateDatetime;
    }

    /**
     * <p>Setter method for detailUpdateDatetime.</p>
     *
     * @param detailUpdateDatetime Set for detailUpdateDatetime
     */
    public void setDetailUpdateDatetime(Timestamp detailUpdateDatetime) {
        this.detailUpdateDatetime = detailUpdateDatetime;
    }

    /**
     * <p>Getter method for etdShow.</p>
     *
     * @return the etdShow
     */
    public String getEtdShow() {
        return etdShow;
    }

    /**
     * <p>Setter method for etdShow.</p>
     *
     * @param etdShow Set for etdShow
     */
    public void setEtdShow(String etdShow) {
        this.etdShow = etdShow;
    }

    // Start : [IN054] Add new column Accept/Reject for input
    /**
     * <p>Getter method for acceptReject.</p>
     *
     * @return the acceptReject
     */
    public String getAcceptReject() {
        return acceptReject;
    }

    /**
     * <p>Setter method for acceptReject.</p>
     *
     * @param acceptReject Set for acceptReject
     */
    public void setAcceptReject(String acceptReject) {
        this.acceptReject = acceptReject;
    }

    /**
     * <p>Getter method for acceptRejectBackup.</p>
     *
     * @return the acceptRejectBackup
     */
    public String getAcceptRejectBackup() {
        return acceptRejectBackup;
    }

    /**
     * <p>Setter method for acceptRejectBackup.</p>
     *
     * @param acceptRejectBackup Set for acceptRejectBackup
     */
    public void setAcceptRejectBackup(String acceptRejectBackup) {
        this.acceptRejectBackup = acceptRejectBackup;
    }
    // End : [IN054] Add new column Accept/Reject for input
}