/*
 * ModifyDate Development company     Describe 
 * 2014/08/15 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * <p>
 * Main Screen Result Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class MainScreenResultDomain extends BaseDomain implements Serializable{

    /**
     * <p>
     * The Serial.
     * </p>
     */
    private static final long serialVersionUID = -8928879763958994851L;

    /**
     * <p>
     * Company DENSO Code.
     * </p>
     */
    private String dCd;

    /**
     * <p>
     * The Category.
     * </p>
     */
    private String category;

    /**
     * <p>
     * Quantity.
     * </p>
     */
    private String quantity;

    /**
     * <p>
     * The Constructor.
     * </p>
     */
    public MainScreenResultDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for Company DENSO Code.
     * </p>
     * 
     * @return the Company DENSO Code.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for Company DENSO Code.
     * </p>
     * 
     * @param dCd Set for Company DENSO Code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for category.
     * </p>
     * 
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * <p>
     * Setter method for category.
     * </p>
     * 
     * @param category Set for category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * <p>
     * Getter method for quantity.
     * </p>
     * 
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * <p>
     * Setter method for quantity.
     * </p>
     * 
     * @param quantity Set for quantity
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
