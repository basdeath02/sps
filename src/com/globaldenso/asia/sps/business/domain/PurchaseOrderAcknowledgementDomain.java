/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2016/02/16 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Purchase Order Acknowledge Domain.
 * @author CSI
 */
public class PurchaseOrderAcknowledgementDomain extends BaseDomain implements Serializable {
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 4913797403637868168L;

    /** The List of PO domain. */
    private List<PurchaseOrderDetailDomain> poDetailList;
    
    /** The purchase order ID */
    private String poId;
    
    /** The SPS PO number. */
    private String spsPoNo;

    /** The DENSO Company Code. */
    private String dCd;

    /** The DENSO Plant code. */
    private String dPcd;
    
    /** The Due Date. */
    private String dueDate;
    
    /** The Order QTY. */
    private String orderQty;
    
    /** The Order QTY. */
    private String firmQty;
    
    /** The Order QTY. */
    private String forecastQty;
    
    /** The period Type */
    private String periodType;
    
    /** The purchase order status */
    private String poStatus;
    
    /** The purchase order Type */
    private String poType;
    
    /** The Order Method. */
    private String orderMethod;
    
    /** The Action Mode. */
    private String actionMode;
    
    /** The Miscellaneous Value QTY. */
    private String miscValue;
    
    /** Last update user. */
    private String lastUpdateDscId;

    /** Last update date time. */
    private Timestamp lastUpdateDatetime;

    /** The file Id that user selected from screen. */
    private String fileIdSelected;

    /** The Mark Pending Flag. */
    private String markPendingFlag;
    
    // [IN054] Add user type for check action mode.
    /** The user type. */
    private String userType;

    /**
     * Instantiates a new Purchase Order Acknowledge Domain.
     */
    public PurchaseOrderAcknowledgementDomain() {
        super();
        this.poDetailList = new ArrayList<PurchaseOrderDetailDomain>();
    }

    /**
     * Gets the SPS Purchase Order number.
     * 
     * @return the SPS Purchase Order number.
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }
    /**
     * Sets the SPS Purchase Order number.
     * 
     * @param spsPoNo the SPS Purchase Order number.
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }

    /**
     * Gets the Purchase Order ID.
     * 
     * @return the Purchase Order ID.
     */
    public String getPoId() {
        return poId;
    }
    
    /**
     * Sets the Purchase Order ID.
     * 
     * @param poId the Purchase Order ID.
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }
    /**
     * Gets the Due date.
     * 
     * @return the Due date.
     */
    public String getDueDate() {
        return dueDate;
    }
    /**
     * Sets the Due date.
     * 
     * @param dueDate the Due date.
     */
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    /**
     * Gets the Order QTY.
     * 
     * @return the Order QTY.
     */
    public String getOrderQty() {
        return orderQty;
    }
    /**
     * Sets the Order QTY.
     * 
     * @param orderQty the Order QTY.
     */
    public void setOrderQty(String orderQty) {
        this.orderQty = orderQty;
    }
    /**
     * Gets the Order Method.
     * 
     * @return the Order Method.
     */
    public String getOrderMethod() {
        return orderMethod;
    }
    /**
     * Sets the Order Method.
     * 
     * @param orderMethod the Order Method.
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }
    /**
     * Gets the period type.
     * 
     * @return the period type.  
     */
    public String getPeriodType() {
        return periodType;
    }
    /**
     * Sets the period type.
     * 
     * @param periodType the period type.  
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }
    /**
     * Gets the Firm QTY.
     * 
     * @return the Firm QTY.
     */
    public String getFirmQty() {
        return firmQty;
    }
    /**
     * Sets the Firm QTY.
     * 
     * @param firmQty the Firm QTY. 
     */
    public void setFirmQty(String firmQty) {
        this.firmQty = firmQty;
    }
    /**
     * Gets the Forecast QTY.
     * 
     * @return the Forecast QTY.
     */
    public String getForecastQty() {
        return forecastQty;
    }
    /**
     * Sets the Forecast QTY.
     * 
     * @param forecastQty the Forecast QTY.
     */
    public void setForecastQty(String forecastQty) {
        this.forecastQty = forecastQty;
    }
    /**
     * Gets the Action Mode.
     * 
     * @return the Action Mode.
     */
    public String getActionMode() {
        return actionMode;
    }
    /**
     * Sets the Action Mode.
     * 
     * @param actionMode the Action Mode.
     */
    public void setActionMode(String actionMode) {
        this.actionMode = actionMode;
    }
    /**
     * Gets the purchase order status.
     * 
     * @return the purchase order status.
     */
    public String getPoStatus() {
        return poStatus;
    }

    /**
     * Sets the purchase order status.
     * 
     * @param poStatus the purchase order status.
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }
    /**
     * Gets the purchase order type.
     * 
     * @return the purchase order type.
     */
    public String getPoType() {
        return poType;
    }
    /**
     * Sets the purchase order type.
     * 
     * @param poType the purchase order type.
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }
    /**
     * Gets the miscellaneous value.
     * 
     * @return the miscellaneous value.
     */
    public String getMiscValue() {
        return miscValue;
    }
    /**
     * Sets the miscellaneous value.
     * 
     * @param miscValue the miscellaneous value.
     */
    public void setMiscValue(String miscValue) {
        this.miscValue = miscValue;
    }

    /**
     * <p>Getter method for fileIdSelected.</p>
     *
     * @return the fileIdSelected
     */
    public String getFileIdSelected() {
        return fileIdSelected;
    }

    /**
     * <p>Setter method for fileIdSelected.</p>
     *
     * @param fileIdSelected Set for fileIdSelected
     */
    public void setFileIdSelected(String fileIdSelected) {
        this.fileIdSelected = fileIdSelected;
    }

    /**
     * <p>Getter method for poDetailList.</p>
     *
     * @return the poDetailList
     */
    public List<PurchaseOrderDetailDomain> getPoDetailList() {
        return poDetailList;
    }

    /**
     * <p>Setter method for poDetailList.</p>
     *
     * @param poDetailList Set for poDetailList
     */
    public void setPoDetailList(List<PurchaseOrderDetailDomain> poDetailList) {
        this.poDetailList = poDetailList;
    }

    /**
     * <p>Getter method for lastUpdateDscId.</p>
     *
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>Setter method for lastUpdateDscId.</p>
     *
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>Getter method for lastUpdateDatetime.</p>
     *
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>Setter method for lastUpdateDatetime.</p>
     *
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>Getter method for densoCode.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for densoCode.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for densoPlantCode.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for densoPlantCode.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for markPendingFlag.</p>
     *
     * @return the markPendingFlag
     */
    public String getMarkPendingFlag() {
        return markPendingFlag;
    }

    /**
     * <p>Setter method for markPendingFlag.</p>
     *
     * @param markPendingFlag Set for markPendingFlag
     */
    public void setMarkPendingFlag(String markPendingFlag) {
        this.markPendingFlag = markPendingFlag;
    }

    // Start : [IN054] Add user type for check action mode.
    /**
     * <p>Getter method for userType.</p>
     *
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * <p>Setter method for userType.</p>
     *
     * @param userType Set for userType
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }
    // End : [IN054] Add user type for check action mode.
}