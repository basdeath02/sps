/*
 * ModifyDate Development company    Describe 
 * 2014/08/04 CSI Parichat           Create
 * 2016/01/05 CSI Akat               [IN039]
 * 2017/08/30 Netband U.Rungsiwut    Modify
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * The Class ASN maintenance return Domain.
 * @author CSI
 */
public class AsnMaintenanceReturnDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3348947801247121577L;
    
    /** The asn domain. */
    private SpsTAsnDomain asnDomain;
    
    /** The asn detail info. */
    private SpsTAsnDetailDomain asnDetailDomain;
    
    /** The do domain. */
    private SpsTDoDomain groupDoDomain;
    
    /** The do detail info. */
    private SpsTDoDetailDomain groupDoDetailDomain;
    
    /** The cigma asn domain. */
    private PseudoCigmaAsnDomain cigmaAsnDomain;
    
    /** The as400 vendor domain. */
    private SpsMAs400VendorDomain as400VendorDomain;
    
    /** The user domain. */
    private SpsMUserDomain userDomain;
    
    /** The company DENSO code. */
    private String dCd;
    
    /** The plant DENSO code. */
    private String dPcd;
    
    /** The company supplier code. */
    private String sCd;
    
    /** The plant supplier code. */
    private String sPcd;
    
    /** The DENSO part no. */
    private String dPn;
    
    /** The supplier part no. */
    private String sPn;
    
    /** The utc. */
    private String utc;
    
    /** The row count. */
    private String rowCount;
    
    /** The row count by rcvLane. */
    private String rowCountByRcvLane;
    
    /** The record group. */
    private String recordGroup;
    
    /** The record group by rcvLane. */
    private String recordGroupByLane;
    
    /** The invoice status. */
    private String invoiceStatus;
    
    /** The asn status. */
    private String asnStatus;
    
    /** The asn pn receiving status. */
    private String asnPnReceivingStatus;
    
    /** The total shipped qty. */
    private BigDecimal totalShippedQty;
    
    /** The total remaining qty. */
    private BigDecimal totalRemainingQty;
    
    /** The asn received qty. */
    private BigDecimal asnReceivedQty;
    
    /** The asn received qty by part. */
    private BigDecimal asnReceivedQtyByPart;
    
    /** The shipping box qty by part. */
    private BigDecimal shippingBoxQtyByPart;
    
    /** The shipping qty by part. */
    private BigDecimal shippingQtyByPart;
    
    /** The shipping qty by part str. */
    private String shippingQtyByPartStr;
    
    /** The total shipped qty str. */
    private String totalShippedQtyStr;
    
    /** The total remaining qty str. */
    private String totalRemainingQtyStr;
    
    /** The shipping qty str. */
    private String shippingQtyStr;
    
    /** The qty box str. */
    private String qtyBoxStr;
    
    /** The shipping box qty str. */
    private String shippingBoxQtyStr;
    
    /** The shipping box qty by part str. */
    private String shippingBoxQtyByPartStr;
    
    // Start : [IN039] user can edit shipping box quantity when unit is KG or MT
    /** The flag for edit Shipping Box Qty. */
    private String editShippingBoxQtyFlg;
    
    /** The Shipping Box Quantity by Part for user input */
    private String shippingBoxQtyByPartInput;
    // End : [IN039] user can edit shipping box quantity when unit is KG or MT
    
    /** The asn received qty by part str. */
    private String asnReceivedQtyStr;
    
    /** The order qty str. */
    private String orderQtyStr;
    
    /** The revising flag. */
    private String revisingFlag;
    
    /** The change reason cd. */
    private String changeReasonCd;
    
    /** The change reason cd backup. */
    private String changeReasonCdBackUp;
    
    /** The change reason name. */
    private String changeReasonName;
    
    /** The total remaining qty by part. */
    private BigDecimal totalRemainingQtyByPart;
    
    /** The revise asn status. */
    private String reviseAsnDetailStatus;
    
    /** The lot size not match flag. */
    private String lotSizeNotMatchFlag;
    
    /** The allow revise flag backup. */
    private String allowReviseFlagBackUp;
    
    /** The revise complete flag. */
    private String reviseCompleteFlag;
    
    /** The receiveByScan. */
    private String receiveByScan; 
    
    /**
     * Instantiates a new ASN maintenance domain.
     */
    public AsnMaintenanceReturnDomain() {
        super();
        asnDomain = new SpsTAsnDomain();
        asnDetailDomain = new SpsTAsnDetailDomain();
        groupDoDomain = new SpsTDoDomain();
        groupDoDetailDomain = new SpsTDoDetailDomain();
        cigmaAsnDomain = new PseudoCigmaAsnDomain();
        as400VendorDomain = new SpsMAs400VendorDomain();
        userDomain = new SpsMUserDomain();
    }

    /**
     * Gets the asn domain.
     * 
     * @return the asn domain.
     */
    public SpsTAsnDomain getAsnDomain() {
        return asnDomain;
    }

    /**
     * Sets the asn domain.
     * 
     * @param asnDomain the asn domain.
     */
    public void setAsnDomain(SpsTAsnDomain asnDomain) {
        this.asnDomain = asnDomain;
    }
    
    /**
     * Gets the asn detail domain.
     * 
     * @return the asn detail domain.
     */
    public SpsTAsnDetailDomain getAsnDetailDomain() {
        return asnDetailDomain;
    }

    /**
     * Sets the asn detail domain.
     * 
     * @param asnDetailDomain the asn detail domain.
     */
    public void setAsnDetailDomain(SpsTAsnDetailDomain asnDetailDomain) {
        this.asnDetailDomain = asnDetailDomain;
    }
    
    /**
     * Gets the group do domain.
     * 
     * @return the group do domain.
     */
    public SpsTDoDomain getGroupDoDomain() {
        return groupDoDomain;
    }

    /**
     * Sets the group do domain.
     * 
     * @param groupDoDomain the group do domain.
     */
    public void setGroupDoDomain(SpsTDoDomain groupDoDomain) {
        this.groupDoDomain = groupDoDomain;
    }

    
    /**
     * Gets the group do detail domain.
     * 
     * @return the group do detail domain.
     */
    public SpsTDoDetailDomain getGroupDoDetailDomain() {
        return groupDoDetailDomain;
    }

    /**
     * Sets the group do detail domain.
     * 
     * @param groupDoDetailDomain the group do detail domain.
     */
    public void setGroupDoDetailDomain(SpsTDoDetailDomain groupDoDetailDomain) {
        this.groupDoDetailDomain = groupDoDetailDomain;
    }
    
    /**
     * Gets the cigma asn domain.
     * 
     * @return the cigma asn domain.
     */
    public PseudoCigmaAsnDomain getCigmaAsnDomain() {
        return cigmaAsnDomain;
    }

    /**
     * Sets the cigma asn domain.
     * 
     * @param cigmaAsnDomain the cigma asn domain.
     */
    public void setCigmaAsnDomain(PseudoCigmaAsnDomain cigmaAsnDomain) {
        this.cigmaAsnDomain = cigmaAsnDomain;
    }
    
    /**
     * Gets the as400 vendor domain.
     * 
     * @return the as400 vendor domain.
     */
    public SpsMAs400VendorDomain getAs400VendorDomain() {
        return as400VendorDomain;
    }

    /**
     * Sets the as400 vendor domain.
     * 
     * @param as400VendorDomain the as400 vendor domain.
     */
    public void setAs400VendorDomain(SpsMAs400VendorDomain as400VendorDomain) {
        this.as400VendorDomain = as400VendorDomain;
    }
    
    /**
     * Gets the user domain.
     * 
     * @return the user domain.
     */
    public SpsMUserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user domain.
     * 
     * @param userDomain the user domain.
     */
    public void setUserDomain(SpsMUserDomain userDomain) {
        this.userDomain = userDomain;
    }

    /**
     * Gets the utc.
     * 
     * @return the do utc.
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Sets the utc.
     * 
     * @param utc the utc.
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * Gets the dCd.
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the dCd.
     * 
     * @param dCd the dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the dPcd.
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the dPcd.
     * 
     * @param dPcd the dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the sCd.
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the sCd.
     * 
     * @param sCd the sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Gets the sPcd.
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the sPcd.
     * 
     * @param sPcd the sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * Gets the dPn.
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Sets the dPn.
     * 
     * @param dPn the dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    
    /**
     * Gets the sPn.
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Sets the sPn.
     * 
     * @param sPn the sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Gets the total shipped qty.
     * 
     * @return the total shipped qty.
     */
    public BigDecimal getTotalShippedQty() {
        return totalShippedQty;
    }

    /**
     * Sets the total shipped qty.
     * 
     * @param totalShippedQty the total shipped qty.
     */
    public void setTotalShippedQty(BigDecimal totalShippedQty) {
        this.totalShippedQty = totalShippedQty;
    }
    
    /**
     * Gets the invoice status.
     * 
     * @return the invoice status
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * Sets the invoice status.
     * 
     * @param invoiceStatus the invoice status
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * Gets the asn status.
     * 
     * @return the asn status
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * Sets the asn status.
     * 
     * @param asnStatus the asn status
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * Gets the asn pn receiving status.
     * 
     * @return the asn pn receiving status
     */
    public String getAsnPnReceivingStatus() {
        return asnPnReceivingStatus;
    }


    /**
     * Sets the asn pn receiving status.
     * 
     * @param asnPnReceivingStatus the asn pn receiving status
     */
    public void setAsnPnReceivingStatus(String asnPnReceivingStatus) {
        this.asnPnReceivingStatus = asnPnReceivingStatus;
    }

    /**
     * Gets the total remaining qty.
     * 
     * @return the total remaining qty
     */
    public BigDecimal getTotalRemainingQty() {
        return totalRemainingQty;
    }

    /**
     * Sets the total remaining qty.
     * 
     * @param totalRemainingQty the total remaining qty
     */
    public void setTotalRemainingQty(BigDecimal totalRemainingQty) {
        this.totalRemainingQty = totalRemainingQty;
    }

    /**
     * Gets the asn received qty.
     * 
     * @return the asn received qty
     */
    public BigDecimal getAsnReceivedQty() {
        return asnReceivedQty;
    }

    /**
     * Sets the asn received qty.
     * 
     * @param asnReceivedQty the asn received qty
     */
    public void setAsnReceivedQty(BigDecimal asnReceivedQty) {
        this.asnReceivedQty = asnReceivedQty;
    }
    
    /**
     * Gets the asn received qty by part.
     * 
     * @return the asn received qty by part
     */
    public BigDecimal getAsnReceivedQtyByPart() {
        return asnReceivedQtyByPart;
    }

    /**
     * Sets the asn received qty by part.
     * 
     * @param asnReceivedQtyByPart the asn received qty by part
     */
    public void setAsnReceivedQtyByPart(BigDecimal asnReceivedQtyByPart) {
        this.asnReceivedQtyByPart = asnReceivedQtyByPart;
    }

    /**
     * Gets the total shipped qty str.
     * 
     * @return the total shipped qty str
     */
    public String getTotalShippedQtyStr() {
        return totalShippedQtyStr;
    }

    /**
     * Sets the total shipped qty str.
     * 
     * @param totalShippedQtyStr the total shipped qty str
     */
    public void setTotalShippedQtyStr(String totalShippedQtyStr) {
        this.totalShippedQtyStr = totalShippedQtyStr;
    }

    /**
     * Gets the total remaining qty str.
     * 
     * @return the total remaining qty str
     */
    public String getTotalRemainingQtyStr() {
        return totalRemainingQtyStr;
    }

    /**
     * Sets the total remaining qty str.
     * 
     * @param totalRemainingQtyStr the total remaining qty str
     */
    public void setTotalRemainingQtyStr(String totalRemainingQtyStr) {
        this.totalRemainingQtyStr = totalRemainingQtyStr;
    }

    /**
     * Gets the shipping qty str.
     * 
     * @return the shipping qty str
     */
    public String getShippingQtyStr() {
        return shippingQtyStr;
    }

    /**
     * Sets the shipping qty str.
     * 
     * @param shippingQtyStr the shipping qty str
     */
    public void setShippingQtyStr(String shippingQtyStr) {
        this.shippingQtyStr = shippingQtyStr;
    }

    /**
     * Gets the qty box str.
     * 
     * @return the qty box str
     */
    public String getQtyBoxStr() {
        return qtyBoxStr;
    }

    /**
     * Sets the qty box str.
     * 
     * @param qtyBoxStr the qty box str
     */
    public void setQtyBoxStr(String qtyBoxStr) {
        this.qtyBoxStr = qtyBoxStr;
    }

    /**
     * Gets the shipping box qty str.
     * 
     * @return the shipping box qty str
     */
    public String getShippingBoxQtyStr() {
        return shippingBoxQtyStr;
    }

    /**
     * Sets the shipping box qty str.
     * 
     * @param shippingBoxQtyStr the shipping box qty str
     */
    public void setShippingBoxQtyStr(String shippingBoxQtyStr) {
        this.shippingBoxQtyStr = shippingBoxQtyStr;
    }

    /**
     * Gets the order qty str.
     * 
     * @return the order qty str
     */
    public String getOrderQtyStr() {
        return orderQtyStr;
    }

    /**
     * Sets the order qty str.
     * 
     * @param orderQtyStr the order qty str
     */
    public void setOrderQtyStr(String orderQtyStr) {
        this.orderQtyStr = orderQtyStr;
    }

    /**
     * Gets the row count.
     * 
     * @return the row count
     */
    public String getRowCount() {
        return rowCount;
    }

    /**
     * Sets the row count.
     * 
     * @param rowCount the row count
     */
    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }
    
    /**
     * Gets the row count by rcvLane.
     * 
     * @return the row count by rcvLane
     */
    public String getRowCountByRcvLane() {
        return rowCountByRcvLane;
    }

    /**
     * Sets the row count by rcvLane.
     * 
     * @param rowCountByRcvLane the row count by rcvLane
     */
    public void setRowCountByRcvLane(String rowCountByRcvLane) {
        this.rowCountByRcvLane = rowCountByRcvLane;
    }

    /**
     * Gets the record group.
     * 
     * @return the record group
     */
    public String getRecordGroup() {
        return recordGroup;
    }

    /**
     * Sets the record group.
     * 
     * @param recordGroup the record group
     */
    public void setRecordGroup(String recordGroup) {
        this.recordGroup = recordGroup;
    }
    
    /**
     * Gets the record group by rcvLane.
     * 
     * @return the record group by rcvLane
     */
    public String getRecordGroupByLane() {
        return recordGroupByLane;
    }

    /**
     * Sets the record group by rcvLane.
     * 
     * @param recordGroupByLane the record group by rcvLane
     */
    public void setRecordGroupByLane(String recordGroupByLane) {
        this.recordGroupByLane = recordGroupByLane;
    }

    /**
     * Gets the revising flag.
     * 
     * @return the revising flag.
     */
    public String getRevisingFlag() {
        return revisingFlag;
    }

    /**
     * Sets the revising flag.
     * 
     * @param revisingFlag the revising flag.
     */
    public void setRevisingFlag(String revisingFlag) {
        this.revisingFlag = revisingFlag;
    }

    /**
     * Gets the shipping box qty by part.
     * 
     * @return the shipping box qty by part.
     */
    public BigDecimal getShippingBoxQtyByPart() {
        return shippingBoxQtyByPart;
    }

    /**
     * Sets the shipping box qty by part.
     * 
     * @param shippingBoxQtyByPart the shipping box qty by part.
     */
    public void setShippingBoxQtyByPart(BigDecimal shippingBoxQtyByPart) {
        this.shippingBoxQtyByPart = shippingBoxQtyByPart;
    }
    
    /**
     * Gets the shipping qty by part.
     * 
     * @return the shipping qty by part.
     */
    public BigDecimal getShippingQtyByPart() {
        return shippingQtyByPart;
    }

    /**
     * Sets the shipping qty by part.
     * 
     * @param shippingQtyByPart the shipping qty by part.
     */
    public void setShippingQtyByPart(BigDecimal shippingQtyByPart) {
        this.shippingQtyByPart = shippingQtyByPart;
    }
    
    /**
     * Gets the shippingQtyByPartStr.
     * 
     * @return shippingQtyByPartStr
     */
    public String getShippingQtyByPartStr() {
        return shippingQtyByPartStr;
    }

    /**
     * Sets the shippingQtyByPartStr.
     * 
     * @param shippingQtyByPartStr the shippingQtyByPartStr
     */
    public void setShippingQtyByPartStr(String shippingQtyByPartStr) {
        this.shippingQtyByPartStr = shippingQtyByPartStr;
    }

    /**
     * Gets the shipping box qty by part str.
     * 
     * @return the shipping box qty by part str.
     */
    public String getShippingBoxQtyByPartStr() {
        return shippingBoxQtyByPartStr;
    }

    /**
     * Sets the shipping box qty by part str.
     * 
     * @param shippingBoxQtyByPartStr the shipping box qty by part str.
     */
    public void setShippingBoxQtyByPartStr(String shippingBoxQtyByPartStr) {
        this.shippingBoxQtyByPartStr = shippingBoxQtyByPartStr;
    }

    /**
     * Gets the asn received qty str.
     * 
     * @return the asn received qty str.
     */
    public String getAsnReceivedQtyStr() {
        return asnReceivedQtyStr;
    }

    /**
     * Sets the asn received qty str.
     * 
     * @param asnReceivedQtyStr the asn received qty str.
     */
    public void setAsnReceivedQtyStr(String asnReceivedQtyStr) {
        this.asnReceivedQtyStr = asnReceivedQtyStr;
    }
    
    /**
     * Gets the changeReasonCd.
     * 
     * @return the changeReasonCd
     */
    public String getChangeReasonCd() {
        return changeReasonCd;
    }

    /**
     * Sets the changeReasonCd.
     * 
     * @param changeReasonCd the changeReasonCd
     */
    public void setChangeReasonCd(String changeReasonCd) {
        this.changeReasonCd = changeReasonCd;
    }
    
    /**
     * Gets the changeReasonCdBackUp.
     * 
     * @return the changeReasonCdBackUp
     */
    public String getChangeReasonCdBackUp() {
        return changeReasonCdBackUp;
    }

    /**
     * Sets the changeReasonCdBackUp.
     * 
     * @param changeReasonCdBackUp the changeReasonCdBackUp
     */
    public void setChangeReasonCdBackUp(String changeReasonCdBackUp) {
        this.changeReasonCdBackUp = changeReasonCdBackUp;
    }

    /**
     * <p>Getter method for change reason name.</p>
     *
     * @return the change reason name
     */
    public String getChangeReasonName() {
        return changeReasonName;
    }

    /**
     * <p>Setter method for change reason name.</p>
     *
     * @param changeReasonName Set for change reason name
     */
    public void setChangeReasonName(String changeReasonName) {
        this.changeReasonName = changeReasonName;
    }

    /**
     * <p>Getter method for totalRemainingQtyByPart.</p>
     *
     * @return the totalRemainingQtyByPart
     */
    public BigDecimal getTotalRemainingQtyByPart() {
        return totalRemainingQtyByPart;
    }

    /**
     * <p>Setter method for totalRemainingQtyByPart.</p>
     *
     * @param totalRemainingQtyByPart Set for totalRemainingQtyByPart
     */
    public void setTotalRemainingQtyByPart(BigDecimal totalRemainingQtyByPart) {
        this.totalRemainingQtyByPart = totalRemainingQtyByPart;
    }

    /**
     * <p>Getter method for reviseAsnDetailStatus.</p>
     *
     * @return the reviseAsnDetailStatus
     */
    public String getReviseAsnDetailStatus() {
        return reviseAsnDetailStatus;
    }

    /**
     * <p>Setter method for reviseAsnDetailStatus.</p>
     *
     * @param reviseAsnDetailStatus Set for reviseAsnDetailStatus
     */
    public void setReviseAsnDetailStatus(String reviseAsnDetailStatus) {
        this.reviseAsnDetailStatus = reviseAsnDetailStatus;
    }

    /**
     * <p>Getter method for lotSizeNotMatchFlag.</p>
     *
     * @return the lotSizeNotMatchFlag
     */
    public String getLotSizeNotMatchFlag() {
        return lotSizeNotMatchFlag;
    }

    /**
     * <p>Setter method for lotSizeNotMatchFlag.</p>
     *
     * @param lotSizeNotMatchFlag Set for lotSizeNotMatchFlag
     */
    public void setLotSizeNotMatchFlag(String lotSizeNotMatchFlag) {
        this.lotSizeNotMatchFlag = lotSizeNotMatchFlag;
    }

    /**
     * <p>Getter method for allowReviseFlagBackUp.</p>
     *
     * @return the allowReviseFlagBackUp
     */
    public String getAllowReviseFlagBackUp() {
        return allowReviseFlagBackUp;
    }

    /**
     * <p>Setter method for allowReviseFlagBackUp.</p>
     *
     * @param allowReviseFlagBackUp Set for allowReviseFlagBackUp
     */
    public void setAllowReviseFlagBackUp(String allowReviseFlagBackUp) {
        this.allowReviseFlagBackUp = allowReviseFlagBackUp;
    }

    /**
     * <p>Getter method for reviseCompleteFlag.</p>
     *
     * @return the reviseCompleteFlag
     */
    public String getReviseCompleteFlag() {
        return reviseCompleteFlag;
    }

    /**
     * <p>Setter method for reviseCompleteFlag.</p>
     *
     * @param reviseCompleteFlag Set for reviseCompleteFlag
     */
    public void setReviseCompleteFlag(String reviseCompleteFlag) {
        this.reviseCompleteFlag = reviseCompleteFlag;
    }

    // Start : [IN039] user can edit shipping box quantity when unit is KG or MT
    /**
     * <p>Getter method for editShippingBoxQtyFlg.</p>
     *
     * @return the editShippingBoxQtyFlg
     */
    public String getEditShippingBoxQtyFlg() {
        return editShippingBoxQtyFlg;
    }

    /**
     * <p>Setter method for editShippingBoxQtyFlg.</p>
     *
     * @param editShippingBoxQtyFlg Set for editShippingBoxQtyFlg
     */
    public void setEditShippingBoxQtyFlg(String editShippingBoxQtyFlg) {
        this.editShippingBoxQtyFlg = editShippingBoxQtyFlg;
    }
    
    /**
     * <p>Getter method for shippingBoxQtyByPartInput.</p>
     *
     * @return the shippingBoxQtyByPartInput
     */
    public String getShippingBoxQtyByPartInput() {
        return shippingBoxQtyByPartInput;
    }

    /**
     * <p>Setter method for shippingBoxQtyByPartInput.</p>
     *
     * @param shippingBoxQtyByPartInput Set for shippingBoxQtyByPartInput
     */
    public void setShippingBoxQtyByPartInput(String shippingBoxQtyByPartInput) {
        this.shippingBoxQtyByPartInput = shippingBoxQtyByPartInput;
    }
    // End : [IN039] user can edit shipping box quantity when unit is KG or MT
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }

}