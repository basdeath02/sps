/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class SendAsnDataToCigmaDomain.
 * @author CSI
 */
public class SendAsnDataToCigmaDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1505720635237693080L;

    /** The last update dsc id. */
    private String lastUpdateDscId;
    
    /** The as400 server connection information. */
    private As400ServerConnectionInformationDomain as400ServerInformation;
    
    /** The asn information domain. */
    private AsnInfoDomain asnInformationDomain;

    /**
     * Instantiates a new Sending ASN domain.
     */
    public SendAsnDataToCigmaDomain() {
        super();
    }

    /**
     * Gets the last update dsc id.
     * 
     * @return the last update dsc id
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Sets the last update dsc id.
     * 
     * @param lastUpdateDscId the last update dsc id
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Gets the as400 server information.
     * 
     * @return the as400 server information
     */
    public As400ServerConnectionInformationDomain getAs400ServerInformation() {
        return as400ServerInformation;
    }

    /**
     * Sets the as400 server information.
     * 
     * @param as400ServerInformation the as400 server information
     */
    public void setAs400ServerInformation(
        As400ServerConnectionInformationDomain as400ServerInformation) {
        this.as400ServerInformation = as400ServerInformation;
    }

    /**
     * Gets the asn information domain.
     * 
     * @return the asn information domain
     */
    public AsnInfoDomain getAsnInformationDomain() {
        return asnInformationDomain;
    }

    /**
     * Sets the asn information domain.
     * 
     * @param asnInformationDomain the asn information domain
     */
    public void setAsnInformationDomain(AsnInfoDomain asnInformationDomain) {
        this.asnInformationDomain = asnInformationDomain;
    }
}