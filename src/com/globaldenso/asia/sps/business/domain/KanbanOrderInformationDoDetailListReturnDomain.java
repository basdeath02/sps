/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * Kanban Order Information DO Detail List Return Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationDoDetailListReturnDomain extends BaseDomain implements
    Serializable {

    /**
     * <p>
     * The serial.
     * </p>
     */
    private static final long serialVersionUID = -8066498130282837252L;
    
    /**
     * The do id
     */
    private BigDecimal doId;

    /**
     * <p>
     * The sPn.
     * </p>
     */
    private String sPn;

    /**
     * <p>
     * The dPn.
     * </p>
     */
    private String dPn;
    
    /**
     * Item Description
     */
    private String itemDesc;

    /**
     * <p>
     * The pnRevision.
     * </p>
     */
    private String pnRevision;

    /**
     * <p>
     * The chgCigmaDoNo.
     * </p>
     */
    private String chgCigmaDoNo;

    /**
     * <p>
     * The ctrlNo.
     * </p>
     */
    private String ctrlNo;

    /**
     * <p>
     * The currentOrderQty.
     * </p>
     */
    private String currentOrderQty;

    /**
     * <p>
     * The qtyBox.
     * </p>
     */
    private String qtyBox;

    /**
     * <p>
     * The chgReason.
     * </p>
     */
    private String chgReason;

    /**
     * <p>
     * The unitOfMeasure.
     * </p>
     */
    private String unitOfMeasure;

    /**
     * <p>
     * The model.
     * </p>
     */
    private String model;

    /**
     * <p>
     * The originalQty.
     * </p>
     */
    private String originalQty;

    /**
     * <p>
     * The previousQty.
     * </p>
     */
    private String previousQty;

    /**
     * <p>
     * The noOfBoxes.
     * </p>
     */
    private String noOfBoxes;

    /**
     * <p>
     * The rcvLane.
     * </p>
     */
    private String rcvLane;

    /**
     * <p>
     * The lastUpdateDscId.
     * </p>
     */
    private String lastUpdateDscId;

    /**
     * <p>
     * The lastUpdateDatetime.
     * </p>
     */
    private Timestamp lastUpdateDatetime;

    /**
     * <p>
     * The KanbanSeq.
     * </p>
     */
    private List<KanbanSeqDomain> kanbanSeq;

    /**
     * The kanbanType
     */
    private String kanbanType;

    /**
     * The kanbanTagSequence
     */
    private String kanbanTagSequence;
    
    /**
     * <p>
     * The constructor.
     * </p>
     */
    public KanbanOrderInformationDoDetailListReturnDomain() {

    }
    
    /**
     * Getter method of doId.
     * 
     * @return the doId
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of doId.
     * 
     * @param doId Set for doId
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * <p>
     * Getter method for sPn.
     * </p>
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>
     * Setter method for sPn.
     * </p>
     * 
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>
     * Getter method for dPn.
     * </p>
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>
     * Setter method for dPn.
     * </p>
     * 
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    
    /**
     * Getter method of "itemDesc"
     * 
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Setter method of "itemDesc"
     * 
     * @param itemDesc Set in "itemDesc".
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * <p>
     * Getter method for pnRevision.
     * </p>
     * 
     * @return the pnRevision
     */
    public String getPnRevision() {
        return pnRevision;
    }

    /**
     * <p>
     * Setter method for pnRevision.
     * </p>
     * 
     * @param pnRevision Set for pnRevision
     */
    public void setPnRevision(String pnRevision) {
        this.pnRevision = pnRevision;
    }

    /**
     * <p>
     * Getter method for chgCigmaDoNo.
     * </p>
     * 
     * @return the chgCigmaDoNo
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * <p>
     * Setter method for chgCigmaDoNo.
     * </p>
     * 
     * @param chgCigmaDoNo Set for chgCigmaDoNo
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * <p>
     * Getter method for ctrlNo.
     * </p>
     * 
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * <p>
     * Setter method for ctrlNo.
     * </p>
     * 
     * @param ctrlNo Set for ctrlNo
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * <p>
     * Getter method for currentOrderQty.
     * </p>
     * 
     * @return the currentOrderQty
     */
    public String getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * <p>
     * Setter method for currentOrderQty.
     * </p>
     * 
     * @param currentOrderQty Set for currentOrderQty
     */
    public void setCurrentOrderQty(String currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * <p>
     * Getter method for qtyBox.
     * </p>
     * 
     * @return the qtyBox
     */
    public String getQtyBox() {
        return qtyBox;
    }

    /**
     * <p>
     * Setter method for qtyBox.
     * </p>
     * 
     * @param qtyBox Set for qtyBox
     */
    public void setQtyBox(String qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * <p>
     * Getter method for chgReason.
     * </p>
     * 
     * @return the chgReason
     */
    public String getChgReason() {
        return chgReason;
    }

    /**
     * <p>
     * Setter method for chgReason.
     * </p>
     * 
     * @param chgReason Set for chgReason
     */
    public void setChgReason(String chgReason) {
        this.chgReason = chgReason;
    }

    /**
     * <p>
     * Getter method for unitOfMeasure.
     * </p>
     * 
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * <p>
     * Setter method for unitOfMeasure.
     * </p>
     * 
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>
     * Getter method for model.
     * </p>
     * 
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * <p>
     * Setter method for model.
     * </p>
     * 
     * @param model Set for model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * <p>
     * Getter method for originalQty.
     * </p>
     * 
     * @return the originalQty
     */
    public String getOriginalQty() {
        return originalQty;
    }

    /**
     * <p>
     * Setter method for originalQty.
     * </p>
     * 
     * @param originalQty Set for originalQty
     */
    public void setOriginalQty(String originalQty) {
        this.originalQty = originalQty;
    }

    /**
     * <p>
     * Getter method for previousQty.
     * </p>
     * 
     * @return the previousQty
     */
    public String getPreviousQty() {
        return previousQty;
    }

    /**
     * <p>
     * Setter method for previousQty.
     * </p>
     * 
     * @param previousQty Set for previousQty
     */
    public void setPreviousQty(String previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * <p>
     * Getter method for noOfBoxes.
     * </p>
     * 
     * @return the noOfBoxes
     */
    public String getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * <p>
     * Setter method for noOfBoxes.
     * </p>
     * 
     * @param noOfBoxes Set for noOfBoxes
     */
    public void setNoOfBoxes(String noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * <p>
     * Getter method for rcvLane.
     * </p>
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * <p>
     * Setter method for rcvLane.
     * </p>
     * 
     * @param rcvLane Set for rcvLane
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * <p>
     * Getter method for lastUpdateDscId.
     * </p>
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * <p>
     * Setter method for lastUpdateDscId.
     * </p>
     * 
     * @param lastUpdateDscId Set for lastUpdateDscId
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * <p>
     * Getter method for lastUpdateDatetime.
     * </p>
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>
     * Setter method for lastUpdateDatetime.
     * </p>
     * 
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>
     * Getter method for kanbanSeq.
     * </p>
     * 
     * @return the kanbanSeq
     */
    public List<KanbanSeqDomain> getKanbanSeq() {
        return kanbanSeq;
    }

    /**
     * <p>
     * Setter method for kanbanSeq.
     * </p>
     * 
     * @param kanbanSeq Set for kanbanSeq
     */
    public void setKanbanSeq(List<KanbanSeqDomain> kanbanSeq) {
        this.kanbanSeq = kanbanSeq;
    }

    /**
     * Getter method of "kanbanType"
     * 
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }

    /**
     * Setter method of "kanbanType"
     * 
     * @param kanbanType Set in "kanbanType".
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }

    /**
     * Getter method of "kanbanTagSequence"
     * 
     * @return the kanbanTagSequence
     */

    public String getKanbanTagSequence() {
        return kanbanTagSequence;
    }

    /**
     * Setter method of "kanbanTagSequence"
     * 
     * @param kanbanTagSequence Set in "kanbanTagSequence".
     */

    public void setKanbanTagSequence(String kanbanTagSequence) {
        this.kanbanTagSequence = kanbanTagSequence;
    }

}
