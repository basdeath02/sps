/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.sql.Date;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class TmpUploadInvoiceDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6687789822977055112L;
    
    /** The session Id. */
    private String sessionId;
    
    /** The denso company name. */
    private String companyDensoName;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The denso tax id. */
    private String densoTaxId;
    
    /** The denso address. */
    private String densoAddress;

    /** The denso address1. */
    private String densoAddress1;

    /** The denso address2. */
    private String densoAddress2;

    /** The denso address3. */
    private String densoAddress3;
    
    /** The utc. */
    private String utc;
    
    /** The denso currency cd. */
    private String densoCurrencyCd;
    
    /** The supplier company name. */
    private String companySupplierName;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier code. */
    private String sCd;
    
    /** The supplier tax id. */
    private String supplierTaxId;
    
    /** The supplier address1. */
    private String supplierAddress1;
    
    /** The supplier address2. */
    private String supplierAddress2;
    
    /** The supplier address3. */
    private String supplierAddress3;
    
     /** The supplier currency cd. */
    private String supplierCurrencyCd;
    
    /** The decimal disp. */
    private String decimalDisp;
    
    /** The vat type */
    private String vatType;
    
    /** The vat rate */
    private String vatRate;
    
    /** The invoice no. */
    private String invoiceNo;
    
    /** The invoice date. */
    private String invoiceDate;
    
    /** The invoice date type date. */
    private Date invoiceDateTd;
    
    /** The base amount*/
    private BigDecimal baseAmount;
    
    /** The vat amount */
    private BigDecimal vatAmount;
    
    /** The total amount */
    private BigDecimal totalAmount;
    
    /** The cn no */
    private String cnNo;
    
    /** The cn date */
    private String cnDate;
    
    /** The cn date type date */
    private Date cnDateTd;
    
    /** The cn base amount */
    private BigDecimal cnBaseAmount;
    
    /** The cn vat amount */
    private BigDecimal cnVatAmount;
    
    /** The cn total amount */
    private BigDecimal cnTotalAmount;
    
    /** The SPS transaction ASN domain. */
    private List<AsnDomain> asnDtailList;

    /**
     * Instantiates a new file upload domain.
     */
    public TmpUploadInvoiceDomain() {
        super();
    }

    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * <p>Getter method for densoCompanyName.</p>
     *
     * @return the densoCompanyName
     */
    public String getCompanyDensoName() {
        return companyDensoName;
    }

    /**
     * <p>Setter method for densoCompanyName.</p>
     *
     * @param companyDensoName Set for densoCompanyName
     */
    public void setCompanyDensoName(String companyDensoName) {
        this.companyDensoName = companyDensoName;
    }

    /**
     * <p>Getter method for densoCode.</p>
     *
     * @return the densoCode
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for densoCode.</p>
     *
     * @param dCd Set for densoCode
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for densoPlantCode.</p>
     *
     * @return the densoPlantCode
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for densoPlantCode.</p>
     *
     * @param dPcd Set for densoPlantCode
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for densoTaxId.</p>
     *
     * @return the densoTaxId
     */
    public String getDensoTaxId() {
        return densoTaxId;
    }

    /**
     * <p>Setter method for densoTaxId.</p>
     *
     * @param densoTaxId Set for densoTaxId
     */
    public void setDensoTaxId(String densoTaxId) {
        this.densoTaxId = densoTaxId;
    }

    /**
     * <p>Getter method for utc.</p>
     *
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * <p>Setter method for utc.</p>
     *
     * @param utc Set for utc
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * <p>Getter method for densoCurrencyCd.</p>
     *
     * @return the densoCurrencyCd
     */
    public String getDensoCurrencyCd() {
        return densoCurrencyCd;
    }

    /**
     * <p>Setter method for densoCurrencyCd.</p>
     *
     * @param densoCurrencyCd Set for densoCurrencyCd
     */
    public void setDensoCurrencyCd(String densoCurrencyCd) {
        this.densoCurrencyCd = densoCurrencyCd;
    }

    /**
     * <p>Getter method for supplierCompanyName.</p>
     *
     * @return the supplierCompanyName
     */
    public String getCompanySupplierName() {
        return companySupplierName;
    }

    /**
     * <p>Setter method for supplierCompanyName.</p>
     *
     * @param companySupplierName Set for companySupplierName
     */
    public void setCompanySupplierName(String companySupplierName) {
        this.companySupplierName = companySupplierName;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }

    /**
     * <p>Getter method for supplierAddress1.</p>
     *
     * @return the supplierAddress1
     */
    public String getSupplierAddress1() {
        return supplierAddress1;
    }

    /**
     * <p>Setter method for supplierAddress1.</p>
     *
     * @param supplierAddress1 Set for supplierAddress1
     */
    public void setSupplierAddress1(String supplierAddress1) {
        this.supplierAddress1 = supplierAddress1;
    }

    /**
     * <p>Getter method for supplierAddress2.</p>
     *
     * @return the supplierAddress2
     */
    public String getSupplierAddress2() {
        return supplierAddress2;
    }

    /**
     * <p>Setter method for supplierAddress2.</p>
     *
     * @param supplierAddress2 Set for supplierAddress2
     */
    public void setSupplierAddress2(String supplierAddress2) {
        this.supplierAddress2 = supplierAddress2;
    }

    /**
     * <p>Getter method for supplierAddress3.</p>
     *
     * @return the supplierAddress3
     */
    public String getSupplierAddress3() {
        return supplierAddress3;
    }

    /**
     * <p>Setter method for supplierAddress3.</p>
     *
     * @param supplierAddress3 Set for supplierAddress3
     */
    public void setSupplierAddress3(String supplierAddress3) {
        this.supplierAddress3 = supplierAddress3;
    }

    /**
     * <p>Getter method for supplierCurrencyCd.</p>
     *
     * @return the supplierCurrencyCd
     */
    public String getSupplierCurrencyCd() {
        return supplierCurrencyCd;
    }

    /**
     * <p>Setter method for supplierCurrencyCd.</p>
     *
     * @param supplierCurrencyCd Set for supplierCurrencyCd
     */
    public void setSupplierCurrencyCd(String supplierCurrencyCd) {
        this.supplierCurrencyCd = supplierCurrencyCd;
    }

    /**
     * <p>Getter method for decimalDisp.</p>
     *
     * @return the decimalDisp
     */
    public String getDecimalDisp() {
        return decimalDisp;
    }

    /**
     * <p>Setter method for decimalDisp.</p>
     *
     * @param decimalDisp Set for decimalDisp
     */
    public void setDecimalDisp(String decimalDisp) {
        this.decimalDisp = decimalDisp;
    }

    /**
     * <p>Getter method for vatType.</p>
     *
     * @return the vatType
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * <p>Setter method for vatType.</p>
     *
     * @param vatType Set for vatType
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }

    /**
     * <p>Getter method for vatRate.</p>
     *
     * @return the vatRate
     */
    public String getVatRate() {
        return vatRate;
    }

    /**
     * <p>Setter method for vatRate.</p>
     *
     * @param vatRate Set for vatRate
     */
    public void setVatRate(String vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
    
    /**
     * <p>Getter method for invoiceDate.</p>
     *
     * @return the invoiceDate
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * <p>Setter method for invoiceDate.</p>
     *
     * @param invoiceDate Set for invoiceDate
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * <p>Getter method for invoiceDateTd.</p>
     *
     * @return the invoiceDateTd
     */
    public Date getInvoiceDateTd() {
        return invoiceDateTd;
    }

    /**
     * <p>Setter method for invoiceDateTd.</p>
     *
     * @param invoiceDateTd Set for invoiceDateTd
     */
    public void setInvoiceDateTd(Date invoiceDateTd) {
        this.invoiceDateTd = invoiceDateTd;
    }

    /**
     * <p>Getter method for cnNo.</p>
     *
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * <p>Setter method for cnNo.</p>
     *
     * @param cnNo Set for cnNo
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * <p>Getter method for cnDate.</p>
     *
     * @return the cnDate
     */
    public String getCnDate() {
        return cnDate;
    }

    /**
     * <p>Setter method for cnDate.</p>
     *
     * @param cnDate Set for cnDate
     */
    public void setCnDate(String cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * <p>Getter method for cnDateTd.</p>
     *
     * @return the cnDateTd
     */
    public Date getCnDateTd() {
        return cnDateTd;
    }

    /**
     * <p>Setter method for cnDateTd.</p>
     *
     * @param cnDateTd Set for cnDateTd
     */
    public void setCnDateTd(Date cnDateTd) {
        this.cnDateTd = cnDateTd;
    }

    /**
     * <p>Getter method for baseAmount.</p>
     *
     * @return the baseAmount
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /**
     * <p>Setter method for baseAmount.</p>
     *
     * @param baseAmount Set for baseAmount
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /**
     * <p>Getter method for vatAmount.</p>
     *
     * @return the vatAmount
     */
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    /**
     * <p>Setter method for vatAmount.</p>
     *
     * @param vatAmount Set for vatAmount
     */
    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    /**
     * <p>Getter method for totalAmount.</p>
     *
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * <p>Setter method for totalAmount.</p>
     *
     * @param totalAmount Set for totalAmount
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * <p>Getter method for cnBaseAmount.</p>
     *
     * @return the cnBaseAmount
     */
    public BigDecimal getCnBaseAmount() {
        return cnBaseAmount;
    }

    /**
     * <p>Setter method for cnBaseAmount.</p>
     *
     * @param cnBaseAmount Set for cnBaseAmount
     */
    public void setCnBaseAmount(BigDecimal cnBaseAmount) {
        this.cnBaseAmount = cnBaseAmount;
    }

    /**
     * <p>Getter method for cnVatAmount.</p>
     *
     * @return the cnVatAmount
     */
    public BigDecimal getCnVatAmount() {
        return cnVatAmount;
    }

    /**
     * <p>Setter method for cnVatAmount.</p>
     *
     * @param cnVatAmount Set for cnVatAmount
     */
    public void setCnVatAmount(BigDecimal cnVatAmount) {
        this.cnVatAmount = cnVatAmount;
    }

    /**
     * <p>Getter method for cnTotalAmount.</p>
     *
     * @return the cnTotalAmount
     */
    public BigDecimal getCnTotalAmount() {
        return cnTotalAmount;
    }

    /**
     * <p>Setter method for cnTotalAmount.</p>
     *
     * @param cnTotalAmount Set for cnTotalAmount
     */
    public void setCnTotalAmount(BigDecimal cnTotalAmount) {
        this.cnTotalAmount = cnTotalAmount;
    }

    /**
     * <p>Getter method for asnDtailList.</p>
     *
     * @return the asnDtailList
     */
    public List<AsnDomain> getAsnDtailList() {
        return asnDtailList;
    }

    /**
     * <p>Setter method for asnDtailList.</p>
     *
     * @param asnDtailList Set for asnDtailList
     */
    public void setAsnDtailList(List<AsnDomain> asnDtailList) {
        this.asnDtailList = asnDtailList;
    }

    /**
     * <p>Getter method for densoAddress.</p>
     *
     * @return the densoAddress
     */
    public String getDensoAddress() {
        return densoAddress;
    }

    /**
     * <p>Setter method for densoAddress.</p>
     *
     * @param densoAddress Set for densoAddress
     */
    public void setDensoAddress(String densoAddress) {
        this.densoAddress = densoAddress;
    }

    /**
     * <p>Getter method for densoAddress1.</p>
     *
     * @return the densoAddress1
     */
    public String getDensoAddress1() {
        return densoAddress1;
    }

    /**
     * <p>Setter method for densoAddress1.</p>
     *
     * @param densoAddress1 Set for densoAddress1
     */
    public void setDensoAddress1(String densoAddress1) {
        this.densoAddress1 = densoAddress1;
    }

    /**
     * <p>Getter method for densoAddress2.</p>
     *
     * @return the densoAddress2
     */
    public String getDensoAddress2() {
        return densoAddress2;
    }

    /**
     * <p>Setter method for densoAddress2.</p>
     *
     * @param densoAddress2 Set for densoAddress2
     */
    public void setDensoAddress2(String densoAddress2) {
        this.densoAddress2 = densoAddress2;
    }

    /**
     * <p>Getter method for densoAddress3.</p>
     *
     * @return the densoAddress3
     */
    public String getDensoAddress3() {
        return densoAddress3;
    }

    /**
     * <p>Setter method for densoAddress3.</p>
     *
     * @param densoAddress3 Set for densoAddress3
     */
    public void setDensoAddress3(String densoAddress3) {
        this.densoAddress3 = densoAddress3;
    }
   
}