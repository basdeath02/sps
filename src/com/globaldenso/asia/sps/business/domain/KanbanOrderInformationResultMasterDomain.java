/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * Kanban Order Information Result Master Domain class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationResultMasterDomain extends BaseDomain implements Serializable {

    /**
     * <p>
     * The Serializable.
     * </p>
     */
    private static final long serialVersionUID = -8953812446928644799L;

    /**
     * <p>
     * The Kanban Order Information Domain.
     * </p>
     */
    private KanbanOrderInformationDomain kanbanOrderInformationDomain;

    /**
     * <p>
     * The Kanban Order Information Result Domain.
     * </p>
     */
    private List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain;

    /**
     * <p>
     * The Error Message List.
     * </p>
     */
    private List<ApplicationMessageDomain> errorMessageList;

    /**
     * <p>
     * The Result String.
     * </p>
     */
    private StringBuffer resultString;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public KanbanOrderInformationResultMasterDomain() {
        super();
    }

    /**
     * <p>
     * Getter method for kanbanOrderInformationDomain.
     * </p>
     * 
     * @return the kanbanOrderInformationDomain
     */
    public KanbanOrderInformationDomain getKanbanOrderInformationDomain() {
        return kanbanOrderInformationDomain;
    }

    /**
     * <p>
     * Setter method for kanbanOrderInformationDomain.
     * </p>
     * 
     * @param kanbanOrderInformationDomain Set for kanbanOrderInformationDomain
     */
    public void setKanbanOrderInformationDomain(
        KanbanOrderInformationDomain kanbanOrderInformationDomain) {
        this.kanbanOrderInformationDomain = kanbanOrderInformationDomain;
    }

    /**
     * <p>
     * Getter method for kanbanOrderInformationResultDomain.
     * </p>
     * 
     * @return the kanbanOrderInformationResultDomain
     */
    public List<KanbanOrderInformationResultDomain> getKanbanOrderInformationResultDomain() {
        return kanbanOrderInformationResultDomain;
    }

    /**
     * <p>
     * Setter method for kanbanOrderInformationResultDomain.
     * </p>
     * 
     * @param kanbanOrderInformationResultDomain Set for
     *            kanbanOrderInformationResultDomain
     */
    public void setKanbanOrderInformationResultDomain(
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain) {
        this.kanbanOrderInformationResultDomain = kanbanOrderInformationResultDomain;
    }

    /**
     * <p>
     * Getter method for errorMessageList.
     * </p>
     * 
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * <p>
     * Setter method for errorMessageList.
     * </p>
     * 
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(
        List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * <p>
     * Getter method for resultString.
     * </p>
     * 
     * @return the resultString
     */
    public StringBuffer getResultString() {
        return resultString;
    }

    /**
     * <p>
     * Setter method for resultString.
     * </p>
     * 
     * @param resultString Set for resultString
     */
    public void setResultString(StringBuffer resultString) {
        this.resultString = resultString;
    }

}
