/*
 * ModifyDate Development company     Describe 
 * 2017/10/12 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class CigmaOneWayKanbanInformationDomain.
 * @author Netband
 */
public class TransferOneWayKanbanDataFromCigmaDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -8307071399533125718L;

    /** cigmaDoNo */
    private String cigmaDoNo;
    /** sCd */
    private String sCd;
    /** deliveryDate */
    private String deliveryDate;
    /** dnItemNumber */
    private String dnItemNumber;
    /** tagSeqNumber */
    private String tagSeqNumber;
    /** kanbanType */
    private String kanbanType;
    /** capacity */
    private String capacity;
    /** partialFlag 
     * "Y" = Partial Lot
     * "N" = Full Lot
     * */
    private String partialFlag;
    /** kanbanInformation */
    private String kanbanInformation;
    /** createByPgmID */
    private String createByPgmID;
    /** createDate */
    private String createDate;
    /** createTime */
    private String createTime;
    /** updateByPgmID */
    private String updateByPgmID;
    /** updateDate */
    private String updateDate;
    /** updateTime */
    private String updateTime;
    /** spsFlag */
    private String spsFlag;
    /** kanbanSeqFlg */
    private String kanbanSeqFlg;
    /** kanbanDataType */
    private String kanbanDataType;
    
    /**
     * 
     * <p>The constructor.</p>
     *
     */
    public TransferOneWayKanbanDataFromCigmaDomain() {
    }
    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }
    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getsCd() {
        return sCd;
    }
    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setsCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * <p>Getter method for deliveryDate.</p>
     *
     * @return the deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }
    /**
     * <p>Setter method for deliveryDate.</p>
     *
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    /**
     * <p>Getter method for dnItemNumber.</p>
     *
     * @return the dnItemNumber
     */
    public String getDnItemNumber() {
        return dnItemNumber;
    }
    /**
     * <p>Setter method for dnItemNumber.</p>
     *
     * @param dnItemNumber Set for dnItemNumber
     */
    public void setDnItemNumber(String dnItemNumber) {
        this.dnItemNumber = dnItemNumber;
    }
    /**
     * <p>Getter method for tagSeqNumber.</p>
     *
     * @return the tagSeqNumber
     */
    public String getTagSeqNumber() {
        return tagSeqNumber;
    }
    /**
     * <p>Setter method for tagSeqNumber.</p>
     *
     * @param tagSeqNumber Set for tagSeqNumber
     */
    public void setTagSeqNumber(String tagSeqNumber) {
        this.tagSeqNumber = tagSeqNumber;
    }
    /**
     * <p>Getter method for kanbanType.</p>
     *
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }
    /**
     * <p>Setter method for kanbanType.</p>
     *
     * @param kanbanType Set for kanbanType
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }
    /**
     * <p>Getter method for capacity.</p>
     *
     * @return the capacity
     */
    public String getCapacity() {
        return capacity;
    }
    /**
     * <p>Setter method for capacity.</p>
     *
     * @param capacity Set for capacity
     */
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
    /**
     * <p>Getter method for partialFlag.</p>
     *
     * @return the partialFlag
     */
    public String getPartialFlag() {
        return partialFlag;
    }
    /**
     * <p>Setter method for partialFlag.</p>
     *
     * @param partialFlag Set for partialFlag
     */
    public void setPartialFlag(String partialFlag) {
        this.partialFlag = partialFlag;
    }
    /**
     * <p>Getter method for kanbanInformation.</p>
     *
     * @return the kanbanInformation
     */
    public String getKanbanInformation() {
        return kanbanInformation;
    }
    /**
     * <p>Setter method for kanbanInformation.</p>
     *
     * @param kanbanInformation Set for kanbanInformation
     */
    public void setKanbanInformation(String kanbanInformation) {
        this.kanbanInformation = kanbanInformation;
    }
    /**
     * <p>Getter method for createByPgmID.</p>
     *
     * @return the createByPgmID
     */
    public String getCreateByPgmID() {
        return createByPgmID;
    }
    /**
     * <p>Setter method for createByPgmID.</p>
     *
     * @param createByPgmID Set for createByPgmID
     */
    public void setCreateByPgmID(String createByPgmID) {
        this.createByPgmID = createByPgmID;
    }
    /**
     * <p>Getter method for createDate.</p>
     *
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }
    /**
     * <p>Setter method for createDate.</p>
     *
     * @param createDate Set for createDate
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    /**
     * <p>Getter method for createTime.</p>
     *
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }
    /**
     * <p>Setter method for createTime.</p>
     *
     * @param createTime Set for createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    /**
     * <p>Getter method for updateByPgmID.</p>
     *
     * @return the updateByPgmID
     */
    public String getUpdateByPgmID() {
        return updateByPgmID;
    }
    /**
     * <p>Setter method for updateByPgmID.</p>
     *
     * @param updateByPgmID Set for updateByPgmID
     */
    public void setUpdateByPgmID(String updateByPgmID) {
        this.updateByPgmID = updateByPgmID;
    }
    /**
     * <p>Getter method for updateDate.</p>
     *
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }
    /**
     * <p>Setter method for updateDate.</p>
     *
     * @param updateDate Set for updateDate
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    /**
     * <p>Getter method for updateTime.</p>
     *
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }
    /**
     * <p>Setter method for updateTime.</p>
     *
     * @param updateTime Set for updateTime
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    /**
     * <p>Getter method for spsFlag.</p>
     *
     * @return the spsFlag
     */
    public String getSpsFlag() {
        return spsFlag;
    }
    /**
     * <p>Setter method for spsFlag.</p>
     *
     * @param spsFlag Set for spsFlag
     */
    public void setSpsFlag(String spsFlag) {
        this.spsFlag = spsFlag;
    }
    /**
     * <p>Getter method for kanbanSeqFlg.</p>
     *
     * @return the kanbanSeqFlg
     */
    public String getKanbanSeqFlg() {
        return kanbanSeqFlg;
    }
    /**
     * <p>Setter method for kanbanSeqFlg.</p>
     *
     * @param kanbanSeqFlg Set for kanbanSeqFlg
     */
    public void setKanbanSeqFlg(String kanbanSeqFlg) {
        this.kanbanSeqFlg = kanbanSeqFlg;
    }
    /**
     * <p>Getter method for kanbanDataType.</p>
     *
     * @return the kanbanDataType
     */
    public String getKanbanDataType() {
        return kanbanDataType;
    }
    /**
     * <p>Setter method for kanbanDataType.</p>
     *
     * @param kanbanDataType Set for kanbanDataType
     */
    public void setKanbanDataType(String kanbanDataType) {
        this.kanbanDataType = kanbanDataType;
    }
}