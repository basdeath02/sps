/*
 * ModifyDate Development company     Describe 
 * 2014/08/05 CSI Akat                Create
 * 2015/09/17 CSI Akat                FIX wrong format
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;

/**
 * The Class Purchase Order Detail Domain.
 * @author CSI
 */
public class PurchaseOrderDetailDomain extends BaseDomain implements Serializable {

    /** Generated Serial Version UID. */
    private static final long serialVersionUID = -4608123550779549228L;

    /** The DENSO Company Code. */
    private String dCd;

    /** The DENSO Plant code. */
    private String dPcd;
    
    /** The DENSO Part No. */
    private String dPn;

    /** The supplier Part No. */
    private String sPn;

    /** The PN Status. */
    private String pnStatus;

    /** The Order QTY. */
    private String orderQty;

    /** The PO ID. */
    private BigDecimal poId;

    /** The PO Status. */
    private String poStatus;

    /** The Unit Of Measure. */
    private String unitOfMeasure;

    /** The Start Peroid Date. */
    private Timestamp startPeriodDate;
    
    /** The End Peroid Date. */
    private Timestamp endPeriodDate;
    
    // FIX : Start : wrong dateformat
    /** The formated start period date to show in screen */
    private String startPeriodDateShow;

    /** The formated end period date to show in screen */
    private String endPeriodDateShow;
    // FIX : End : wrong data format
    
    /** The Last Update Date Time. */
    private Timestamp lastUpdateDatetime;

    /** The Order Method. */
    private String orderMethod;
    
    /** The name of Order Method. */
    private String orderMethodName;
    
    /** The Order Lot in String type. */
    private String stringOrderLot;
    
    /** The Due Date in String type. */
    private String stringDueDate;
    
    /** The ETD in String type. */
    private String stringEtd;
    
    /** The Order Qty in String type. */
    private String stringOrderQty;
    
    /** The Unit Price in String type. */
    private String stringUnitPrice;
    
    /** The Previous Qty in String type. */
    private String stringPreviousQty;
    
    /** The Difference Qty in String type. */
    private String stringDifferenceQty;
    
    /** The Qty Box in String type. */
    private String stringQtyBox;
    
    /** The domain for SPS_T_PO_DETAIL. */
    private SpsTPoDetailDomain spsTPoDetailDomain;
    
    /** The domain for SPS_T_PO_DUE. */
    private SpsTPoDueDomain spsTPoDueDomain;

    /** The Order QTY. */
    private String firmQty;
    
    /** The Order QTY. */
    private String forecastQty;
    
    /** The Action Mode. */
    private String actionMode;

    /** The default constructor. */
    public PurchaseOrderDetailDomain() {
        this.spsTPoDetailDomain = new SpsTPoDetailDomain();
        this.spsTPoDueDomain = new SpsTPoDueDomain();
    }

    /**
     * <p>Getter method for densoPartNo.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for densoPartNo.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for supplierPartNo.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for supplierPartNo.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for pnStatus.</p>
     *
     * @return the pnStatus
     */
    public String getPnStatus() {
        return pnStatus;
    }

    /**
     * <p>Setter method for pnStatus.</p>
     *
     * @param pnStatus Set for pnStatus
     */
    public void setPnStatus(String pnStatus) {
        this.pnStatus = pnStatus;
    }

    /**
     * <p>Getter method for orderQty.</p>
     *
     * @return the orderQty
     */
    public String getOrderQty() {
        return orderQty;
    }

    /**
     * <p>Setter method for orderQty.</p>
     *
     * @param orderQty Set for orderQty
     */
    public void setOrderQty(String orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * <p>Getter method for unitOfMeasure.</p>
     *
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * <p>Setter method for unitOfMeasure.</p>
     *
     * @param unitOfMeasure Set for unitOfMeasure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * <p>Getter method for orderMethod.</p>
     *
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * <p>Setter method for orderMethod.</p>
     *
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * <p>Getter method for orderMethodName.</p>
     *
     * @return the orderMethodName
     */
    public String getOrderMethodName() {
        return orderMethodName;
    }

    /**
     * <p>Setter method for orderMethodName.</p>
     *
     * @param orderMethodName Set for orderMethodName
     */
    public void setOrderMethodName(String orderMethodName) {
        this.orderMethodName = orderMethodName;
    }

    /**
     * <p>Getter method for poStatus.</p>
     *
     * @return the poStatus
     */
    public String getPoStatus() {
        return poStatus;
    }

    /**
     * <p>Setter method for poStatus.</p>
     *
     * @param poStatus Set for poStatus
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }

    /**
     * <p>Getter method for startPeriodDate.</p>
     *
     * @return the startPeriodDate
     */
    public Timestamp getStartPeriodDate() {
        return startPeriodDate;
    }

    /**
     * <p>Setter method for startPeriodDate.</p>
     *
     * @param startPeriodDate Set for startPeriodDate
     */
    public void setStartPeriodDate(Timestamp startPeriodDate) {
        this.startPeriodDate = startPeriodDate;
    }

    /**
     * <p>Getter method for endPeriodDate.</p>
     *
     * @return the endPeriodDate
     */
    public Timestamp getEndPeriodDate() {
        return endPeriodDate;
    }

    /**
     * <p>Setter method for endPeriodDate.</p>
     *
     * @param endPeriodDate Set for endPeriodDate
     */
    public void setEndPeriodDate(Timestamp endPeriodDate) {
        this.endPeriodDate = endPeriodDate;
    }

    /**
     * <p>Getter method for lastUpdateDatetime.</p>
     *
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * <p>Setter method for lastUpdateDatetime.</p>
     *
     * @param lastUpdateDatetime Set for lastUpdateDatetime
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>Getter method for firmQty.</p>
     *
     * @return the firmQty
     */
    public String getFirmQty() {
        return firmQty;
    }

    /**
     * <p>Setter method for firmQty.</p>
     *
     * @param firmQty Set for firmQty
     */
    public void setFirmQty(String firmQty) {
        this.firmQty = firmQty;
    }

    /**
     * <p>Getter method for forecastQty.</p>
     *
     * @return the forecastQty
     */
    public String getForecastQty() {
        return forecastQty;
    }

    /**
     * <p>Setter method for forecastQty.</p>
     *
     * @param forecastQty Set for forecastQty
     */
    public void setForecastQty(String forecastQty) {
        this.forecastQty = forecastQty;
    }

    /**
     * <p>Getter method for actionMode.</p>
     *
     * @return the actionMode
     */
    public String getActionMode() {
        return actionMode;
    }

    /**
     * <p>Setter method for actionMode.</p>
     *
     * @param actionMode Set for actionMode
     */
    public void setActionMode(String actionMode) {
        this.actionMode = actionMode;
    }

    /**
     * <p>Getter method for densoCode.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for densoCode.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for densoPlantCode.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for densoPlantCode.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for spsTPoDetailDomain.</p>
     *
     * @return the spsTPoDetailDomain
     */
    public SpsTPoDetailDomain getSpsTPoDetailDomain() {
        return spsTPoDetailDomain;
    }

    /**
     * <p>Setter method for spsTPoDetailDomain.</p>
     *
     * @param spsTPoDetailDomain Set for spsTPoDetailDomain
     */
    public void setSpsTPoDetailDomain(SpsTPoDetailDomain spsTPoDetailDomain) {
        this.spsTPoDetailDomain = spsTPoDetailDomain;
    }

    /**
     * <p>Getter method for spsTPoDueDomain.</p>
     *
     * @return the spsTPoDueDomain
     */
    public SpsTPoDueDomain getSpsTPoDueDomain() {
        return spsTPoDueDomain;
    }

    /**
     * <p>Setter method for spsTPoDueDomain.</p>
     *
     * @param spsTPoDueDomain Set for spsTPoDueDomain
     */
    public void setSpsTPoDueDomain(SpsTPoDueDomain spsTPoDueDomain) {
        this.spsTPoDueDomain = spsTPoDueDomain;
    }

    /**
     * <p>Getter method for stringDueDate.</p>
     *
     * @return the stringDueDate
     */
    public String getStringDueDate() {
        return stringDueDate;
    }

    /**
     * <p>Setter method for stringDueDate.</p>
     *
     * @param stringDueDate Set for stringDueDate
     */
    public void setStringDueDate(String stringDueDate) {
        this.stringDueDate = stringDueDate;
    }

    /**
     * <p>Getter method for stringEtd.</p>
     *
     * @return the stringEtd
     */
    public String getStringEtd() {
        return stringEtd;
    }

    /**
     * <p>Setter method for stringEtd.</p>
     *
     * @param stringEtd Set for stringEtd
     */
    public void setStringEtd(String stringEtd) {
        this.stringEtd = stringEtd;
    }

    /**
     * <p>Getter method for stringOrderQty.</p>
     *
     * @return the stringOrderQty
     */
    public String getStringOrderQty() {
        return stringOrderQty;
    }

    /**
     * <p>Setter method for stringOrderQty.</p>
     *
     * @param stringOrderQty Set for stringOrderQty
     */
    public void setStringOrderQty(String stringOrderQty) {
        this.stringOrderQty = stringOrderQty;
    }

    /**
     * <p>Getter method for stringUnitPrice.</p>
     *
     * @return the stringUnitPrice
     */
    public String getStringUnitPrice() {
        return stringUnitPrice;
    }

    /**
     * <p>Setter method for stringUnitPrice.</p>
     *
     * @param stringUnitPrice Set for stringUnitPrice
     */
    public void setStringUnitPrice(String stringUnitPrice) {
        this.stringUnitPrice = stringUnitPrice;
    }

    /**
     * <p>Getter method for stringPreviousQty.</p>
     *
     * @return the stringPreviousQty
     */
    public String getStringPreviousQty() {
        return stringPreviousQty;
    }

    /**
     * <p>Setter method for stringPreviousQty.</p>
     *
     * @param stringPreviousQty Set for stringPreviousQty
     */
    public void setStringPreviousQty(String stringPreviousQty) {
        this.stringPreviousQty = stringPreviousQty;
    }

    /**
     * <p>Getter method for stringDifferenceQty.</p>
     *
     * @return the stringDifferenceQty
     */
    public String getStringDifferenceQty() {
        return stringDifferenceQty;
    }

    /**
     * <p>Setter method for stringDifferenceQty.</p>
     *
     * @param stringDifferenceQty Set for stringDifferenceQty
     */
    public void setStringDifferenceQty(String stringDifferenceQty) {
        this.stringDifferenceQty = stringDifferenceQty;
    }
    
    /**
     * <p>Getter method for stringQtyBox.</p>
     *
     * @return the stringQtyBox
     */
    public String getStringQtyBox() {
        return stringQtyBox;
    }

    /**
     * <p>Setter method for stringQtyBox.</p>
     *
     * @param stringQtyBox Set for stringQtyBox
     */
    public void setStringQtyBox(String stringQtyBox) {
        this.stringQtyBox = stringQtyBox;
    }

    /**
     * <p>Getter method for stringOrderLot.</p>
     *
     * @return the stringOrderLot
     */
    public String getStringOrderLot() {
        return stringOrderLot;
    }

    /**
     * <p>Setter method for stringOrderLot.</p>
     *
     * @param stringOrderLot Set for stringOrderLot
     */
    public void setStringOrderLot(String stringOrderLot) {
        this.stringOrderLot = stringOrderLot;
    }

    /**
     * <p>Getter method for startPeriodDateShow.</p>
     *
     * @return the startPeriodDateShow
     */
    public String getStartPeriodDateShow() {
        return startPeriodDateShow;
    }

    /**
     * <p>Setter method for startPeriodDateShow.</p>
     *
     * @param startPeriodDateShow Set for startPeriodDateShow
     */
    public void setStartPeriodDateShow(String startPeriodDateShow) {
        this.startPeriodDateShow = startPeriodDateShow;
    }

    /**
     * <p>Getter method for endPeriodDateShow.</p>
     *
     * @return the endPeriodDateShow
     */
    public String getEndPeriodDateShow() {
        return endPeriodDateShow;
    }

    /**
     * <p>Setter method for endPeriodDateShow.</p>
     *
     * @param endPeriodDateShow Set for endPeriodDateShow
     */
    public void setEndPeriodDateShow(String endPeriodDateShow) {
        this.endPeriodDateShow = endPeriodDateShow;
    }

}
