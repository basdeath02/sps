/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Class Miscellaneous Domain.
 * @author CSI
 */
public class MiscellaneousDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1909547300134743436L;

    /** The Miscellaneous type. */
    private String miscType;
    
    /** The Miscellaneous code. */
    private String miscCode;
    
    /** The Miscellaneous value. */
    private String miscValue;
    
    /** The Miscellaneous sort no. */
    private Integer miscSortNo;
    
    /** The Miscellaneous value2. */
    private String miscValue2;
    
    /** The create user. */
    private String createUser; 
    
    /** The create date time. */
    private Timestamp createDateTime; 
    
    /** The last update user. */
    private String lastUpdateUser;
    
    /** The last update date time. */
    private Timestamp lastUpdateDateTime;
    
    /**
     * Instantiates a new Miscellaneous domain.
     */
    public MiscellaneousDomain() {
        super();
    }
    
    /**
     * Gets the Miscellaneous type.
     * 
     * @return the Miscellaneous type.
     */
    public String getMiscType() {
        return miscType;
    }
    
    /**
     * Sets the Miscellaneous type.
     * 
     * @param miscType the Miscellaneous type.
     */  
    public void setMiscType(String miscType) {
        this.miscType = miscType;
    }
    
    /**
     * Gets the Miscellaneous code.
     * 
     * @return the Miscellaneous code.
     */
    public String getMiscCode() {
        return miscCode;
    }
    
    /**
     * Sets the Miscellaneous code.
     * 
     * @param miscCode the Miscellaneous code.
     */
    public void setMiscCode(String miscCode) {
        this.miscCode = miscCode;
    }
    
    /**
     * Gets the Miscellaneous value.
     * 
     * @return the Miscellaneous value.
     */
    public String getMiscValue() {
        return miscValue;
    }

    /**
     * Sets the Miscellaneous value.
     * 
     * @param miscValue the Miscellaneous value
     */
    public void setMiscValue(String miscValue) {
        this.miscValue = miscValue;
    }
    
    /**
     * Gets the Miscellaneous sort no.
     * 
     * @return the  Miscellaneous sort no.
     */
    public Integer getMiscSortNo() {
        return miscSortNo;
    }

    /**
     * Sets the  Miscellaneous sort no.
     * 
     * @param miscSortNo the  Miscellaneous sort no.
     */
    public void setMiscSortNo(Integer miscSortNo) {
        this.miscSortNo = miscSortNo;
    }

    /**
     * Gets the Miscellaneous value2.
     * 
     * @return the Miscellaneous value2.
     */
    public String getMiscValue2() {
        return miscValue2;
    }

    /**
     * Sets the Miscellaneous value2.
     * 
     * @param miscValue2 the Miscellaneous value2.
     */
    public void setMiscValue2(String miscValue2) {
        this.miscValue2 = miscValue2;
    }
    
    /**
     * Gets the create user.
     * 
     * @return the create user.
     */
    public String getCreateUsere() {
        return createUser;
    }

    /**
     * Sets the create user.
     * 
     * @param createUser the create user.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create date time.
     * 
     * @return the create date time.
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }
    
    /**
     * Sets the create date time.
     * 
     * @param createDateTime the create date time.
     */

    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * Gets the last update user.
     * 
     * @return the last update user.
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the last update user.
     * 
     * @param lastUpdateUser the last update user.
     */
    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    /**
     * Gets the last update date time.
     * 
     * @return the last update date time.
     */
    public Timestamp getLastUpdateDateTime(){
        return lastUpdateDateTime;
    }
    
    /** Sets the last update date time.
     * 
     * @param lastUpdateDateTime the last update date time.
     */
    public void setLastUpdateDateTime(Timestamp lastUpdateDateTime){
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

}