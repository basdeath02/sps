package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * 
 * <p>TransferDoKanbanlDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferDoKanbanlDataFromCigmaDomain extends BaseDomain implements Serializable {
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 1337191457950432752L;

    /** plantCode */
    private String plantCode;
    
    /** sCd */
    private String vendorCd;
    
    /** deliveryRunDate */
    private String deliveryRunDate;
    
    /** deliveryRunNo */
    private String deliveryRunNo;
    
    /** partNo */
    private String partNo;
    
    /** kanbanSeqNo */
    private String kanbanSeqNo;
    
    /** kanbanCtrlNo */
    private String kanbanCtrlNo;
    
    /** kanbanLotSize */
    private String kanbanLotSize;
    
    /** tempKanbanTag */
    private String tempKanbanTag;
    
    /** cigmaDoNo */
    private String cigmaDoNo;
    
    /** cigmaPoNo */
    private String cigmaPoNo;
    
    /** deliveryDueTime */
    private String deliveryDueTime;
    
    /** deliveryRunType */
    private String deliveryRunType;
    
    /** runScheduleDate */
    private String runScheduleDate;
    
    /** qrRecvBatchNo */
    private String qrRecvBatchNo;
    
    /** qrScanTime */
    private String qrScanTime;

    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferDoKanbanlDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for plantCode.</p>
     *
     * @return the plantCode
     */
    public String getPlantCode() {
        return plantCode;
    }

    /**
     * <p>Setter method for plantCode.</p>
     *
     * @param plantCode Set for plantCode
     */
    public void setPlantCode(String plantCode) {
        this.plantCode = plantCode;
    }

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param vendorCd Set for sCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for deliveryRunDate.</p>
     *
     * @return the deliveryRunDate
     */
    public String getDeliveryRunDate() {
        return deliveryRunDate;
    }

    /**
     * <p>Setter method for deliveryRunDate.</p>
     *
     * @param deliveryRunDate Set for deliveryRunDate
     */
    public void setDeliveryRunDate(String deliveryRunDate) {
        this.deliveryRunDate = deliveryRunDate;
    }

    /**
     * <p>Getter method for deliveryRunNo.</p>
     *
     * @return the deliveryRunNo
     */
    public String getDeliveryRunNo() {
        return deliveryRunNo;
    }

    /**
     * <p>Setter method for deliveryRunNo.</p>
     *
     * @param deliveryRunNo Set for deliveryRunNo
     */
    public void setDeliveryRunNo(String deliveryRunNo) {
        this.deliveryRunNo = deliveryRunNo;
    }

    /**
     * <p>Getter method for partNo.</p>
     *
     * @return the partNo
     */
    public String getPartNo() {
        return partNo;
    }

    /**
     * <p>Setter method for partNo.</p>
     *
     * @param partNo Set for partNo
     */
    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    /**
     * <p>Getter method for kanbanSeqNo.</p>
     *
     * @return the kanbanSeqNo
     */
    public String getKanbanSeqNo() {
        return kanbanSeqNo;
    }

    /**
     * <p>Setter method for kanbanSeqNo.</p>
     *
     * @param kanbanSeqNo Set for kanbanSeqNo
     */
    public void setKanbanSeqNo(String kanbanSeqNo) {
        this.kanbanSeqNo = kanbanSeqNo;
    }

    /**
     * <p>Getter method for kanbanCtrlNo.</p>
     *
     * @return the kanbanCtrlNo
     */
    public String getKanbanCtrlNo() {
        return kanbanCtrlNo;
    }

    /**
     * <p>Setter method for kanbanCtrlNo.</p>
     *
     * @param kanbanCtrlNo Set for kanbanCtrlNo
     */
    public void setKanbanCtrlNo(String kanbanCtrlNo) {
        this.kanbanCtrlNo = kanbanCtrlNo;
    }

    /**
     * <p>Getter method for kanbanLotSize.</p>
     *
     * @return the kanbanLotSize
     */
    public String getKanbanLotSize() {
        return kanbanLotSize;
    }

    /**
     * <p>Setter method for kanbanLotSize.</p>
     *
     * @param kanbanLotSize Set for kanbanLotSize
     */
    public void setKanbanLotSize(String kanbanLotSize) {
        this.kanbanLotSize = kanbanLotSize;
    }

    /**
     * <p>Getter method for tempKanbanTag.</p>
     *
     * @return the tempKanbanTag
     */
    public String getTempKanbanTag() {
        return tempKanbanTag;
    }

    /**
     * <p>Setter method for tempKanbanTag.</p>
     *
     * @param tempKanbanTag Set for tempKanbanTag
     */
    public void setTempKanbanTag(String tempKanbanTag) {
        this.tempKanbanTag = tempKanbanTag;
    }

    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>Getter method for cigmaPoNo.</p>
     *
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }

    /**
     * <p>Setter method for cigmaPoNo.</p>
     *
     * @param cigmaPoNo Set for cigmaPoNo
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * <p>Getter method for deliveryDueTime.</p>
     *
     * @return the deliveryDueTime
     */
    public String getDeliveryDueTime() {
        return deliveryDueTime;
    }

    /**
     * <p>Setter method for deliveryDueTime.</p>
     *
     * @param deliveryDueTime Set for deliveryDueTime
     */
    public void setDeliveryDueTime(String deliveryDueTime) {
        this.deliveryDueTime = deliveryDueTime;
    }

    /**
     * <p>Getter method for deliveryRunType.</p>
     *
     * @return the deliveryRunType
     */
    public String getDeliveryRunType() {
        return deliveryRunType;
    }

    /**
     * <p>Setter method for deliveryRunType.</p>
     *
     * @param deliveryRunType Set for deliveryRunType
     */
    public void setDeliveryRunType(String deliveryRunType) {
        this.deliveryRunType = deliveryRunType;
    }

    /**
     * <p>Getter method for runScheduleDate.</p>
     *
     * @return the runScheduleDate
     */
    public String getRunScheduleDate() {
        return runScheduleDate;
    }

    /**
     * <p>Setter method for runScheduleDate.</p>
     *
     * @param runScheduleDate Set for runScheduleDate
     */
    public void setRunScheduleDate(String runScheduleDate) {
        this.runScheduleDate = runScheduleDate;
    }

    /**
     * <p>Getter method for qrRecvBatchNo.</p>
     *
     * @return the qrRecvBatchNo
     */
    public String getQrRecvBatchNo() {
        return qrRecvBatchNo;
    }

    /**
     * <p>Setter method for qrRecvBatchNo.</p>
     *
     * @param qrRecvBatchNo Set for qrRecvBatchNo
     */
    public void setQrRecvBatchNo(String qrRecvBatchNo) {
        this.qrRecvBatchNo = qrRecvBatchNo;
    }

    /**
     * <p>Getter method for qrScanTime.</p>
     *
     * @return the qrScanTime
     */
    public String getQrScanTime() {
        return qrScanTime;
    }

    /**
     * <p>Setter method for qrScanTime.</p>
     *
     * @param qrScanTime Set for qrScanTime
     */
    public void setQrScanTime(String qrScanTime) {
        this.qrScanTime = qrScanTime;
    }
    

}
