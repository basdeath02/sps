/*
 * ModifyDate Development company     Describe 
 * 2014/06/07 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class DENSO User Domain.
 * @author CSI
 */
public class UserDensoDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4692116690381357271L;
    
    /** The Company DENSO Code. */
    private String dCd;
   
    /** The Plant DENSO. */
    private String dPcd;
      
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The register date. */
    private String registerDate;
       
    /** The department code. */
    private String departmentCode; 
    
    /** The Employee code. */
    private String employeeCode;
    
    /** The email flag for create ASN */
    private String emlCreateAsnFixed;
    
    /** The email flag for create ASN. */
    private String emlCreateAsnKanBan;
    
    /** The email flag for create Pending PO. */
    private String emlPendingPo;
    
    /** The email flag for change ship QTY. */
    private String emlChgShipQty;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
            
    /** The Date time update. */
    private Timestamp updateDatetime;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    
    /**
     * Instantiates a new User DENSO domain.
     */
    public UserDensoDomain() {
        super();       
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Gets the Company DENSO Code.
     * 
     * @return the Company DENSO Code.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the Company DENSO Code.
     * 
     * @param dCd the Company DENSO Code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the plant DENSO code
     * 
     * @return the plant DENSO code
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the plant DENSO code
     * 
     * @param dPcd the plant DENSO code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the Register Date from.
     * 
     * @return the Register Date from.
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }

    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }

    /**
     * Gets the register Date.
     * 
     * @return the register Date.
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * Sets the register Date.
     * 
     * @param registerDate the register Date.
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * Gets the Department code.
     * 
     * @return the Department code.
     */
    public String getDepartmentCode() {
        return departmentCode;
    }
    /**
     * Sets the Department code.
     * 
     * @param departmentCode the Department code.
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }
    
    /**
     * Gets the employee Code.
     * 
     * @return the employee Code.
     */
    public String getEmployeeCode() {
        return employeeCode;
    }

    /**
     * Sets the employee Code.
     * 
     * @param employeeCode the employee Code.
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
   
    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
    /**
     * Gets the email create ASN Fixed flag.
     * 
     * @return the email create ASN Fixed flag.
     */
    public String getEmlCreateAsnFixed() {
        return emlCreateAsnFixed;
    }
    
    /**
     * Sets the email create ASN Fixed flag.
     * 
     * @param emlCreateAsnFixed the new target page
     */
    public void setEmlCreateAsnFixed(String emlCreateAsnFixed) {
        this.emlCreateAsnFixed = emlCreateAsnFixed;
    }

    /**
     * Gets the email create ASN KANBAN flag.
     * 
     * @return the email create ASN KANBAN flag.
     */
    public String getEmlCreateAsnKanBan() {
        return emlCreateAsnKanBan;
    }

    /**
     * Sets the email create ASN KANBAN flag.
     * 
     * @param emlCreateAsnKanBan the email create ASN KANBAN flag.
     */
    public void setEmlCreateAsnKanBan(String emlCreateAsnKanBan) {
        this.emlCreateAsnKanBan = emlCreateAsnKanBan;
    }

    /**
     * Gets the email pending PO flag.
     * 
     * @return the email pending PO flag.
     */
    public String getEmlPendingPo() {
        return emlPendingPo;
    }

    /**
     * Sets the email pending PO flag.
     * 
     * @param emlPendingPo the email pending PO flag.
     */
    public void setEmlPendingPo(String emlPendingPo) {
        this.emlPendingPo = emlPendingPo;
    }

    /**
     * Gets the email change ship QTY flag.
     * 
     * @return the email change ship QTY flag.
     */
    public String getEmlChgShipQty() {
        return emlChgShipQty;
    }


    /**
     * Sets the email change ship QTY flag.
     * 
     * @param emlChgShipQty the email change ship QTY flag.
     */
    public void setEmlChgShipQty(String emlChgShipQty) {
        this.emlChgShipQty = emlChgShipQty;
    } 
    
    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public Timestamp getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(Timestamp updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
}