/*
 * ModifyDate Development company Describe 
 * 2014/08/20 Phakaporn P.           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
/**
 * The Class DensoSupplierPartDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class DensoSupplierPartDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1637687254079950978L;

    /**
     * company DENSO  code
     */
    private String dCd;

    /**
     * company Supplier  code
     */
    private String sCd;
    
    /**
     * vendor code
     */
    private String vendorCd;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /**
     * DENSO part no.
     */
    private String dPn;

    /**
     * Supplier part no.
     */
    private String sPn;

    /**
     * Effective date
     */
    private Timestamp effectDate;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create date time
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update date time
     */
    private Timestamp lastUpdateDatetime;
    
    /**
     * Create date time for screen.
     */
    private String createDatetimeScreen;
    
    /**
     * Effective date for Screen. 
     */
    private String effectDateScreen;

    /**
     * Default constructor
     */
    public DensoSupplierPartDomain() {
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "effectDate"
     * 
     * @return the effectDate
     */
    public Timestamp getEffectDate() {
        return effectDate;
    }

    /**
     * Setter method of "effectDate"
     * 
     * @param effectDate Set in "effectDate".
     */
    public void setEffectDate(Timestamp effectDate) {
        this.effectDate = effectDate;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Getter method of Create date time for Screen.
     * 
     * @return the createDatetimeScreen
     */   
    public String getCreateDatetimeScreen() {
        return createDatetimeScreen;
    }
    /**
     * Setter method of Create date time for Screen.
     * 
     * @param createDatetimeScreen Set Create date time for Screen.
     */
    public void setCreateDatetimeScreen(String createDatetimeScreen) {
        this.createDatetimeScreen = createDatetimeScreen;
    }
    /**
     * Getter method of Effective Date for Screen.
     * 
     * @return the effectDateScreen
     */
    public String getEffectDateScreen() {
        return effectDateScreen;
    }
    /**
     * Setter Effective Date for Screen.
     * 
     * @param effectDateScreen Set Effective Date for Screen.
     */
    public void setEffectDateScreen(String effectDateScreen) {
        this.effectDateScreen = effectDateScreen;
    }
    /**
     * Getter method of vendor code
     * 
     * @return the vendor code
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * Setter vendor code
     * 
     * @param vendorCd Set vendor code
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

}