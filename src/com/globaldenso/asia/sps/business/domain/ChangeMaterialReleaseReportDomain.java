/*
 * ModifyDate Development company     Describe 
 * 2014/09/01 CSI Arnon           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Date;

/** 
 * <p>Change Material Release Result Report Domain.</p>
 *
 * @author CSI
   */
public class ChangeMaterialReleaseReportDomain extends BaseDomain implements Serializable {

    /** * Serializable ID  */
    private static final long serialVersionUID = -8490840613356988483L;
    
    /** Supplier Code */
    private String sCd;
    
    /** Supplier Name */
    private String supplierName;
    
    /** Plant Code */
    private String dPcd;
    
    /** Warehouse For Prime Receiving */
    private String warehouseForPrimeReceiving;
    
    /** Location  */
    private String location;
    
    /** DEL */
    private String del;
    
    /** SEQ */
    private String seq;
    
    /** Fax Telephone No */
    private String faxTelephoneNo;
    
    /** Supplier Plant Code */
    private String sPcd;
    
    /** Transport */
    private String transport;
    
    /** Release No */
    private String releaseNo;
    
    /** Planner Code */
    private String plannerCode;
    
    /** Issue Date Domain */
    private Date poIssueDate;
    
    /** Issue Date Report */
    private String issueDate;
    
    /** Due Date Form Domain*/
    private Date dueDateFromDate;
    
    /** Due Date Form Report*/
    private String dueDateFrom;
    
    /** Due Date To Domain*/
    private Date dueDateToDate;
    
    /** Due Date To Report*/
    private String dueDateTo;
    
    /** D. Part Number */
    private String dPn;
    
    /** S. Part Number */
    private String sPn;
    
    /** Item Description */
    private String itemDescription;
    
    /** ETA Domain*/
    private Date etaDate;
    
    /** ETA Report*/
    private String eta;
    
    /** ETD Domain*/
    private Date etdDate;
    
    /** ETD Report*/
    private String etd;
    
    /** Order Method */
    private String orderMethod;
    
    /** Order Type */
    private String orderType;
    
    /** Period Type */
    private String periodType;
    
    /** CD */
    private String cd;
    
    /** Report Type */
    private String reportType;
    
    /** Reason */
    private String reason;
    
    /** Current */
    private String current;
    
    /** Previous */
    private String previous;
    
    /** Difference Qty */
    private String differenceQty;
    
    /** Unit Of Measure */
    private String um;
    
    /** Currunt SPS P/O No. */
    private String spsPoNo;
    
    /** Model */
    private String model; 
    
    /** Page No */
    private String pageNo;
    
    /** Company name */
    private String companyName;
    
    /** Company Denso Code */
    private String dCd;
    
    /**
     * 
     * <p>Change Material Release Domain.</p>
     *
     */
    public ChangeMaterialReleaseReportDomain() {
    }
    /**
     * <p>Getter method for supplierCode.</p>
     *
     * @return the supplierCode
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * <p>Setter method for supplierCode.</p>
     *
     * @param sCd Set for supplierCode
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }
    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    /**
     * <p>Getter method for plantCode.</p>
     *
     * @return the plantCode
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for plantCode.</p>
     *
     * @param dPcd Set for plantCode
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * <p>Getter method for warehouseForPrimeReceiving.</p>
     *
     * @return the warehouseForPrimeReceiving
     */
    public String getWarehouseForPrimeReceiving() {
        return warehouseForPrimeReceiving;
    }
    /**
     * <p>Setter method for warehouseForPrimeReceiving.</p>
     *
     * @param warehouseForPrimeReceiving Set for warehouseForPrimeReceiving
     */
    public void setWarehouseForPrimeReceiving(String warehouseForPrimeReceiving) {
        this.warehouseForPrimeReceiving = warehouseForPrimeReceiving;
    }
    /**
     * <p>Getter method for location.</p>
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }
    /**
     * <p>Setter method for location.</p>
     *
     * @param location Set for location
     */
    public void setLocation(String location) {
        this.location = location;
    }
    /**
     * <p>Getter method for del.</p>
     *
     * @return the del
     */
    public String getDel() {
        return del;
    }
    /**
     * <p>Setter method for del.</p>
     *
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }
    /**
     * <p>Getter method for seq.</p>
     *
     * @return the seq
     */
    public String getSeq() {
        return seq;
    }
    /**
     * <p>Setter method for seq.</p>
     *
     * @param seq Set for seq
     */
    public void setSeq(String seq) {
        this.seq = seq;
    }
    /**
     * <p>Getter method for faxTelephoneNo.</p>
     *
     * @return the faxTelephoneNo
     */
    public String getFaxTelephoneNo() {
        return faxTelephoneNo;
    }
    /**
     * <p>Setter method for faxTelephoneNo.</p>
     *
     * @param faxTelephoneNo Set for faxTelephoneNo
     */
    public void setFaxTelephoneNo(String faxTelephoneNo) {
        this.faxTelephoneNo = faxTelephoneNo;
    }
    /**
     * <p>Getter method for supplierPlantCode.</p>
     *
     * @return the supplierPlantCode
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplierPlantCode.</p>
     *
     * @param sPcd Set for supplierPlantCode
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * <p>Getter method for transport.</p>
     *
     * @return the transport
     */
    public String getTransport() {
        return transport;
    }
    /**
     * <p>Setter method for transport.</p>
     *
     * @param transport Set for transport
     */
    public void setTransport(String transport) {
        this.transport = transport;
    }
    /**
     * <p>Getter method for releaseNo.</p>
     *
     * @return the releaseNo
     */
    public String getReleaseNo() {
        return releaseNo;
    }
    /**
     * <p>Setter method for releaseNo.</p>
     *
     * @param releaseNo Set for releaseNo
     */
    public void setReleaseNo(String releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * <p>Getter method for plannerCode.</p>
     *
     * @return the plannerCode
     */
    public String getPlannerCode() {
        return plannerCode;
    }
    /**
     * <p>Setter method for plannerCode.</p>
     *
     * @param plannerCode Set for plannerCode
     */
    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }
    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }
    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }
    /**
     * <p>Getter method for dueDateFrom.</p>
     *
     * @return the dueDateFrom
     */
    public String getDueDateFrom() {
        return dueDateFrom;
    }
    /**
     * <p>Setter method for dueDateFrom.</p>
     *
     * @param dueDateFrom Set for dueDateFrom
     */
    public void setDueDateFrom(String dueDateFrom) {
        this.dueDateFrom = dueDateFrom;
    }
    /**
     * <p>Getter method for dueDateTo.</p>
     *
     * @return the dueDateTo
     */
    public String getDueDateTo() {
        return dueDateTo;
    }
    /**
     * <p>Setter method for dueDateTo.</p>
     *
     * @param dueDateTo Set for dueDateTo
     */
    public void setDueDateTo(String dueDateTo) {
        this.dueDateTo = dueDateTo;
    }
    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }
    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }
    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    /**
     * <p>Getter method for itemDescription.</p>
     *
     * @return the itemDescription
     */
    public String getItemDescription() {
        return itemDescription;
    }
    /**
     * <p>Setter method for itemDescription.</p>
     *
     * @param itemDescription Set for itemDescription
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }
    /**
     * <p>Getter method for eta.</p>
     *
     * @return the eta
     */
    public String getEta() {
        return eta;
    }
    /**
     * <p>Setter method for eta.</p>
     *
     * @param eta Set for eta
     */
    public void setEta(String eta) {
        this.eta = eta;
    }
    /**
     * <p>Getter method for etd.</p>
     *
     * @return the etd
     */
    public String getEtd() {
        return etd;
    }
    /**
     * <p>Setter method for etd.</p>
     *
     * @param etd Set for etd
     */
    public void setEtd(String etd) {
        this.etd = etd;
    }
    /**
     * <p>Getter method for orderType.</p>
     *
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }
    /**
     * <p>Setter method for orderType.</p>
     *
     * @param orderType Set for orderType
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    /**
     * <p>Getter method for cd.</p>
     *
     * @return the cd
     */
    public String getCd() {
        return cd;
    }
    /**
     * <p>Setter method for cd.</p>
     *
     * @param cd Set for cd
     */
    public void setCd(String cd) {
        this.cd = cd;
    }
    /**
     * <p>Getter method for reportType.</p>
     *
     * @return the reportType
     */
    public String getReportType() {
        return reportType;
    }
    /**
     * <p>Setter method for reportType.</p>
     *
     * @param reportType Set for reportType
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
    /**
     * <p>Getter method for reason.</p>
     *
     * @return the reason
     */
    public String getReason() {
        return reason;
    }
    /**
     * <p>Setter method for reason.</p>
     *
     * @param reason Set for reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
    /**
     * <p>Getter method for current.</p>
     *
     * @return the current
     */
    public String getCurrent() {
        return current;
    }
    /**
     * <p>Setter method for current.</p>
     *
     * @param current Set for current
     */
    public void setCurrent(String current) {
        this.current = current;
    }
    /**
     * <p>Getter method for previous.</p>
     *
     * @return the previous
     */
    public String getPrevious() {
        return previous;
    }
    /**
     * <p>Setter method for previous.</p>
     *
     * @param previous Set for previous
     */
    public void setPrevious(String previous) {
        this.previous = previous;
    }
    /**
     * <p>Getter method for differenceQty.</p>
     *
     * @return the differenceQty
     */
    public String getDifferenceQty() {
        return differenceQty;
    }
    /**
     * <p>Setter method for differenceQty.</p>
     *
     * @param differenceQty Set for differenceQty
     */
    public void setDifferenceQty(String differenceQty) {
        this.differenceQty = differenceQty;
    }
    /**
     * <p>Getter method for um.</p>
     *
     * @return the um
     */
    public String getUm() {
        return um;
    }
    /**
     * <p>Setter method for um.</p>
     *
     * @param um Set for um
     */
    public void setUm(String um) {
        this.um = um;
    }
    /**
     * <p>Getter method for spsPoNo.</p>
     *
     * @return the spsPoNo
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }
    /**
     * <p>Setter method for spsPoNo.</p>
     *
     * @param spsPoNo Set for spsPoNo
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }
    /**
     * <p>Getter method for model.</p>
     *
     * @return the model
     */
    public String getModel() {
        return model;
    }
    /**
     * <p>Setter method for model.</p>
     *
     * @param model Set for model
     */
    public void setModel(String model) {
        this.model = model;
    }
    /**
     * <p>Getter method for pageNo.</p>
     *
     * @return the pageNo
     */
    public String getPageNo() {
        return pageNo;
    }
    /**
     * <p>Setter method for pageNo.</p>
     *
     * @param pageNo Set for pageNo
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }
    /**
     * <p>Getter method for companyName.</p>
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }
    /**
     * <p>Setter method for companyName.</p>
     *
     * @param companyName Set for companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    /**
     * <p>Getter method for poIssueDate.</p>
     *
     * @return the poIssueDate
     */
    public Date getPoIssueDate() {
        return poIssueDate;
    }
    /**
     * <p>Setter method for poIssueDate.</p>
     *
     * @param poIssueDate Set for poIssueDate
     */
    public void setPoIssueDate(Date poIssueDate) {
        this.poIssueDate = poIssueDate;
    }
    /**
     * <p>Getter method for dueDateFromDate.</p>
     *
     * @return the dueDateFromDate
     */
    public Date getDueDateFromDate() {
        return dueDateFromDate;
    }
    /**
     * <p>Setter method for dueDateFromDate.</p>
     *
     * @param dueDateFromDate Set for dueDateFromDate
     */
    public void setDueDateFromDate(Date dueDateFromDate) {
        this.dueDateFromDate = dueDateFromDate;
    }
    /**
     * <p>Getter method for dueDateToDate.</p>
     *
     * @return the dueDateToDate
     */
    public Date getDueDateToDate() {
        return dueDateToDate;
    }
    /**
     * <p>Setter method for dueDateToDate.</p>
     *
     * @param dueDateToDate Set for dueDateToDate
     */
    public void setDueDateToDate(Date dueDateToDate) {
        this.dueDateToDate = dueDateToDate;
    }
    /**
     * <p>Getter method for etaDate.</p>
     *
     * @return the etaDate
     */
    public Date getEtaDate() {
        return etaDate;
    }
    /**
     * <p>Setter method for etaDate.</p>
     *
     * @param etaDate Set for etaDate
     */
    public void setEtaDate(Date etaDate) {
        this.etaDate = etaDate;
    }
    /**
     * <p>Getter method for etdDate.</p>
     *
     * @return the etdDate
     */
    public Date getEtdDate() {
        return etdDate;
    }
    /**
     * <p>Setter method for etdDate.</p>
     *
     * @param etdDate Set for etdDate
     */
    public void setEtdDate(Date etdDate) {
        this.etdDate = etdDate;
    }
    /**
     * <p>Getter method for orderMethod.</p>
     *
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }
    /**
     * <p>Setter method for orderMethod.</p>
     *
     * @param orderMethod Set for orderMethod
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }
    /**
     * <p>Getter method for periodType.</p>
     *
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }
    /**
     * <p>Setter method for periodType.</p>
     *
     * @param periodType Set for periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }
    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
}
