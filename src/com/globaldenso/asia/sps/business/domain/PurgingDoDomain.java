/*
 * ModifyDate Development company     Describe 
 * 2014/08/27 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The Class File Upload Domain.
 * @author CSI
 */
public class PurgingDoDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 2431986119030104188L;
    
    /** The delivery order id. */
    private String deliveryOrderId;
    
    /** The date start purge. */
    private String dateStartPurge;
    
    /** The list of pdf file id. */
    private String pdfFileId;
    
    // [IN012] Check status of D/O for purge cancel D/O
    /** D/O Ship Status */
    private String doShipStatus;
    
    /** The list of Purging ASN Domain. */
    private List<PurgingAsnDomain> purgingAsnList;
    
    // [PURGE] add new attribute for purge process
    /** dCd */
    private String dCd;
    
    /**
     * Instantiates a new file upload domain.
     */
    public PurgingDoDomain() {
        super();
    }
    
    /**
     * <p>Getter method for deliveryOrderId.</p>
     *
     * @return the deliveryOrderId
     */
    public String getDeliveryOrderId() {
        return deliveryOrderId;
    }

    /**
     * <p>Setter method for deliveryOrderId.</p>
     *
     * @param deliveryOrderId Set for deliveryOrderId
     */
    public void setDeliveryOrderId(String deliveryOrderId) {
        this.deliveryOrderId = deliveryOrderId;
    }

    /**
     * <p>Getter method for dateStartPurge.</p>
     *
     * @return the dateStartPurge
     */
    public String getDateStartPurge() {
        return dateStartPurge;
    }

    /**
     * <p>Setter method for dateStartPurge.</p>
     *
     * @param dateStartPurge Set for dateStartPurge
     */
    public void setDateStartPurge(String dateStartPurge) {
        this.dateStartPurge = dateStartPurge;
    }

    /**
     * <p>Getter method for pdfFileId.</p>
     *
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>Setter method for pdfFileId.</p>
     *
     * @param pdfFileId Set for pdfFileId
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * <p>Getter method for purgingAsnList.</p>
     *
     * @return the purgingAsnList
     */
    public List<PurgingAsnDomain> getPurgingAsnList() {
        return purgingAsnList;
    }

    /**
     * <p>Setter method for purgingAsnList.</p>
     *
     * @param purgingAsnList Set for purgingAsnList
     */
    public void setPurgingAsnList(List<PurgingAsnDomain> purgingAsnList) {
        this.purgingAsnList = purgingAsnList;
    }

    /**
     * <p>Getter method for doShipStatus.</p>
     *
     * @return the doShipStatus
     */
    public String getDoShipStatus() {
        return doShipStatus;
    }

    /**
     * <p>Setter method for doShipStatus.</p>
     *
     * @param doShipStatus Set for doShipStatus
     */
    public void setDoShipStatus(String doShipStatus) {
        this.doShipStatus = doShipStatus;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
}