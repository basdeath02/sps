/*
 * ModifyDate Development company     Describe 
 * 2014/09/08 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;

/**
 * The Class Purchase Order Domain.
 * @author CSI
 */
public class PurchaseOrderDomain extends BaseDomain implements Serializable {
    
    /** Generated serial version UID. */
    private static final long serialVersionUID = -5907143279211886643L;

    /** P/O Header */
    private SpsTPoDomain poHeader;
    
    /** P/O Detail */
    private SpsTPoDetailDomain poDetail;
    
    /** P/O Due */
    private SpsTPoDueDomain poDue;
    
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public PurchaseOrderDomain() {
        super();
        poHeader = new SpsTPoDomain();
        poDetail = new SpsTPoDetailDomain();
        poDue = new SpsTPoDueDomain();
    }
    /**
     * <p>Getter method for poHeader.</p>
     *
     * @return the poHeader
     */
    public SpsTPoDomain getPoHeader() {
        return poHeader;
    }
    /**
     * <p>Setter method for poHeader.</p>
     *
     * @param poHeader Set for poHeader
     */
    public void setPoHeader(SpsTPoDomain poHeader) {
        this.poHeader = poHeader;
    }
    /**
     * <p>Getter method for poDetail.</p>
     *
     * @return the poDetail
     */
    public SpsTPoDetailDomain getPoDetail() {
        return poDetail;
    }
    /**
     * <p>Setter method for poDetail.</p>
     *
     * @param poDetail Set for poDetail
     */
    public void setPoDetail(SpsTPoDetailDomain poDetail) {
        this.poDetail = poDetail;
    }
    /**
     * <p>Getter method for poDue.</p>
     *
     * @return the poDue
     */
    public SpsTPoDueDomain getPoDue() {
        return poDue;
    }
    /**
     * <p>Setter method for poDue.</p>
     *
     * @param poDue Set for poDue
     */
    public void setPoDue(SpsTPoDueDomain poDue) {
        this.poDue = poDue;
    }
}
