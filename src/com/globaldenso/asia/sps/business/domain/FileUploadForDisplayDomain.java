/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

/**
 * The Class File Upload For Display Domain.
 * @author CSI
 */
public class FileUploadForDisplayDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = 3326568522154732562L;

    /** The file upload ID. */
    private FileUploadDomain fileUploadDomain;
    
    /** The last update date. */
    private String lastUpdateDate;
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public FileUploadForDisplayDomain() {
        super();
    }
    
    /**
     * Gets the file upload domain.
     * 
     * @return the file upload domain.
     */
    public FileUploadDomain getFileUploadDomain() {
        return fileUploadDomain;
    }

    /**
     * Sets the file upload domain.
     * 
     * @param fileUploadDomain the file upload domain.
     */
    public void setFileUploadDomain(FileUploadDomain fileUploadDomain) {
        this.fileUploadDomain = fileUploadDomain;
    }

    /**
     * Gets the last update date.
     * 
     * @return the last update date.
     */
    public String getLastUpdateDate(){
        return lastUpdateDate;
    }
    
    /** Sets the last update date.
     * 
     * @param lastUpdateDate the last update date.
     */
    public void setLastUpdateDate(String lastUpdateDate){
        this.lastUpdateDate = lastUpdateDate;
    }

}