/*
 * ModifyDate Development company     Describe 
 * 2014/07/25 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The Class Supplier User Information Return Domain.
 * @author CSI
 */
public class SupplierUserInformationReturnDomain extends BaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -3857987981399884958L;
    
    /** The list of Supplier User Information domain. */
    private List<SupplierUserInformationDomain> supplierUserInfoDomainList;
    
    /** The Supplier User Information domain. */
    private SupplierUserInformationDomain supplierUserInformationDomain;
    
    /** The Stream of CSV file. */
    private StringBuffer resultString;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /**
     * Instantiates a new file upload domain.
     */
    public SupplierUserInformationReturnDomain() {
        super();
    }

    /**
     * Gets the list of Supplier User Information Domain.
     * 
     * @return the supplierUserInfoDomain.
     */
    public List<SupplierUserInformationDomain> getSupplierUserInfoDomainList() {
        return supplierUserInfoDomainList;
    }

    /**
     * Sets the Supplier User Information Domain.
     * 
     * @param supplierUserInfoDomainList the list of Supplier User Information Domain.
     */  
    public void setSupplierUserInfoDomainList(
        List<SupplierUserInformationDomain> supplierUserInfoDomainList) {
        this.supplierUserInfoDomainList = supplierUserInfoDomainList;
    }
    /**
     * Gets the Stream of CSV file.
     * 
     * @return the Stream of CSV file.
     */
    public StringBuffer getResultString() {
        return resultString;
    }
    /**
     * Sets the Stream of CSV file.
     * 
     * @param resultString the Stream of CSV file.
     */ 
    public void setResultString(StringBuffer resultString) {
        this.resultString = resultString;
    }
    /**
     * Gets the Supplier User Info Domain.
     * 
     * @return the Supplier User Info Domain.
     */
    public SupplierUserInformationDomain getSupplierUserInfomationDomain() {
        return supplierUserInformationDomain;
    }
    /**
     * Sets the Supplier User Info Domain.
     * 
     * @param supplierUserInfoDomain the Supplier User Info Domain.
     */
    public void setSupplierUserInfomationDomain(
        SupplierUserInformationDomain supplierUserInfoDomain) {
        this.supplierUserInformationDomain = supplierUserInfoDomain;
    }
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(
        List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
   
    
}