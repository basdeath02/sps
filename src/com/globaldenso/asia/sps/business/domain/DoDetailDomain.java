/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The Class ASN Detail Domain.
 * @author CSI
 */
public class DoDetailDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 303558568853491292L;
    
    /** The DO ID. */
    private BigDecimal doId;
    
    /** The S PN. */
    private String sPn;
    
    /** The D PN. */
    private String dPn;
    
    /** The PN Revision. */
    private String pnRevision;
    
    /** The PN Shipment Status. */
    private String pnShipmentStatus;
    
    /** The chg cigma do no. */
    private String chgCigmaDoNo;
    
    /** The ctrl no. */
    private String ctrlNo;
    
    /** The current order qty. */
    private BigDecimal currentOrderQty;
    
    /** The qty box. */
    private BigDecimal qtyBox;
    
    /** The chg reason. */
    private String chgReason;
    
    /** The item desc. */
    private String itemDesc;
    
    /** The unit of measure. */
    private String unitOfMeasure;
    
    /** The model. */
    private String model;
    
    /** The original qty. */
    private BigDecimal originalQty;
    
    /** The shipping qty. */
    private BigDecimal shippingQty;
    
    /** The rcv lane. */
    private String rcvLane;
    
    /** The urgent order flg. */
    private String urgentOrderFlg;
    
    /** The mail flg. */
    private String mailFlg;
    
    /** The create dsc id. */
    private String createDscId;
    
    /** The create date time. */
    private Timestamp createDateTime;
    
    /** The last update dsc id. */
    private String lastUpdateDscId;
    
    /** The last update date time. */
    private Timestamp lastUpdateDateTime;
    
    /**
     * Instantiates a new DO domain.
     */
    public DoDetailDomain() {
        super();
    }
    
    /**
     * Gets the do id.
     * 
     * @return the do id.
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Sets the do id.
     * 
     * @param doId the do id.
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Gets the s pn.
     * 
     * @return the s pn.
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Sets the s pn.
     * 
     * @param sPn the s pn.
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Gets the d pn.
     * 
     * @return the d pn.
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Sets the d pn.
     * 
     * @param dPn the d pn.
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Gets the pn revision.
     * 
     * @return the pn revision.
     */
    public String getPnRevision() {
        return pnRevision;
    }

    /**
     * Sets the pn revision.
     * 
     * @param pnRevision the pn revision.
     */
    public void setPnRevision(String pnRevision) {
        this.pnRevision = pnRevision;
    }

    /**
     * Gets the pn shipment status.
     * 
     * @return the pn shipment status.
     */
    public String getPnShipmentStatus() {
        return pnShipmentStatus;
    }

    /**
     * Sets the pn shipment status.
     * 
     * @param pnShipmentStatus the pn shipment status.
     */
    public void setPnShipmentStatus(String pnShipmentStatus) {
        this.pnShipmentStatus = pnShipmentStatus;
    }

    /**
     * Gets the chg cigma do no.
     * 
     * @return the chg cigma do no.
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * Sets the chg cigma do no.
     * 
     * @param chgCigmaDoNo the chg cigma do no.
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * Gets the ctrl no.
     * 
     * @return the ctrl no.
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * Sets the ctrl no.
     * 
     * @param ctrlNo the ctrl no.
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * Gets the current order qty.
     * 
     * @return the current order qty.
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Sets the current order qty.
     * 
     * @param currentOrderQty the current order qty.
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Gets the qty box.
     * 
     * @return the qty box.
     */
    public BigDecimal getQtyBox() {
        return qtyBox;
    }

    /**
     * Sets the qty box.
     * 
     * @param qtyBox the qty box.
     */
    public void setQtyBox(BigDecimal qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * Gets the chg reason.
     * 
     * @return the chg reason.
     */
    public String getChgReason() {
        return chgReason;
    }

    /**
     * Sets the chg reason.
     * 
     * @param chgReason the chg reason.
     */
    public void setChgReason(String chgReason) {
        this.chgReason = chgReason;
    }

    /**
     * Gets the item desc.
     * 
     * @return the item desc.
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Sets the item desc.
     * 
     * @param itemDesc the item desc.
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * Gets the unit of measure.
     * 
     * @return the unit of measure.
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the unit of measure.
     * 
     * @param unitOfMeasure the unit of measure.
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Gets the model.
     * 
     * @return the model.
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets the model.
     * 
     * @param model the model.
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Gets the original qty.
     * 
     * @return the original qty.
     */
    public BigDecimal getOriginalQty() {
        return originalQty;
    }

    /**
     * Sets the original qty.
     * 
     * @param originalQty the original qty.
     */
    public void setOriginalQty(BigDecimal originalQty) {
        this.originalQty = originalQty;
    }

    /**
     * Gets the shipping qty.
     * 
     * @return the shipping qty.
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Sets the shipping qty.
     * 
     * @param shippingQty the shipping qty.
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Gets the rcv lane.
     * 
     * @return the rcv lane.
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Sets the rcv lane.
     * 
     * @param rcvLane the rcv lane.
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Gets the urgent order flg.
     * 
     * @return the urgent order flg.
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * Sets the urgent order flg.
     * 
     * @param urgentOrderFlg the urgent order flg.
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * Gets the mail flg.
     * 
     * @return the mail flg.
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Sets the mail flg.
     * 
     * @param mailFlg the mail flg.
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Gets the create dsc id.
     * 
     * @return the create dsc id.
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Sets the create dsc id.
     * 
     * @param createDscId the create dsc id.
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Gets the create date time.
     * 
     * @return the create date time.
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the create date time.
     * 
     * @param createDateTime the create date time.
     */
    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
     * Gets the last update dsc id.
     * 
     * @return the last update dsc id.
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Sets the last update dsc id.
     * 
     * @param lastUpdateDscId the last update dsc id.
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Gets the last update date time.
     * 
     * @return the last update date time.
     */
    public Timestamp getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    /**
     * Sets the last update date time.
     * 
     * @param lastUpdateDateTime the last update date time.
     */
    public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }
    
}