/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;

/**
 * The Class ASN Info Detail Domain.
 * @author CSI
 */
public class AsnInfoDetailDomain extends BaseDomain implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -54897197420370842L;

    /** The domain for SPS_T_ASN. */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** The domain for SPS_T_DO. */
    private SpsTDoDomain spsTDoDomain;
    
    /** The domain for SPS_T_DO_DETAIL. */
    private SpsTDoDetailDomain spsTDoDetailDomain;
    
    /** Truck Seq in String type. */
    private String stringTruckSeq;
    
    /** Qty Box in String type. */
    private String stringQtyBox;
    
    /** Number of Boxes in String type. */
    private String stringNoOfBoxes;
    
    /** Current Order Qty in Strint type. */
    private String stringCurrentOrderQty;
    
    /** Kanban Seq Number. */
    private String kanbanSeqNo;
    
    /**
     * Instantiates a new ASN info detail domain.
     */
    public AsnInfoDetailDomain() {
        super();
    }

    /**
     * <p>Getter method for spsTAsnDetailDomain.</p>
     *
     * @return the spsTAsnDetailDomain
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }

    /**
     * <p>Setter method for spsTAsnDetailDomain.</p>
     *
     * @param spsTAsnDetailDomain Set for spsTAsnDetailDomain
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }

    /**
     * <p>Getter method for spsTDoDomain.</p>
     *
     * @return the spsTDoDomain
     */
    public SpsTDoDomain getSpsTDoDomain() {
        return spsTDoDomain;
    }

    /**
     * <p>Setter method for spsTDoDomain.</p>
     *
     * @param spsTDoDomain Set for spsTDoDomain
     */
    public void setSpsTDoDomain(SpsTDoDomain spsTDoDomain) {
        this.spsTDoDomain = spsTDoDomain;
    }

    /**
     * <p>Getter method for spsTDoDetailDomain.</p>
     *
     * @return the spsTDoDetailDomain
     */
    public SpsTDoDetailDomain getSpsTDoDetailDomain() {
        return spsTDoDetailDomain;
    }

    /**
     * <p>Setter method for spsTDoDetailDomain.</p>
     *
     * @param spsTDoDetailDomain Set for spsTDoDetailDomain
     */
    public void setSpsTDoDetailDomain(SpsTDoDetailDomain spsTDoDetailDomain) {
        this.spsTDoDetailDomain = spsTDoDetailDomain;
    }

    /**
     * <p>Getter method for stringTruckSeq.</p>
     *
     * @return the stringTruckSeq
     */
    public String getStringTruckSeq() {
        return stringTruckSeq;
    }

    /**
     * <p>Setter method for stringTruckSeq.</p>
     *
     * @param stringTruckSeq Set for stringTruckSeq
     */
    public void setStringTruckSeq(String stringTruckSeq) {
        this.stringTruckSeq = stringTruckSeq;
    }

    /**
     * <p>Getter method for stringQtyBox.</p>
     *
     * @return the stringQtyBox
     */
    public String getStringQtyBox() {
        return stringQtyBox;
    }

    /**
     * <p>Setter method for stringQtyBox.</p>
     *
     * @param stringQtyBox Set for stringQtyBox
     */
    public void setStringQtyBox(String stringQtyBox) {
        this.stringQtyBox = stringQtyBox;
    }

    /**
     * <p>Getter method for stringNoOfBoxes.</p>
     *
     * @return the stringNoOfBoxes
     */
    public String getStringNoOfBoxes() {
        return stringNoOfBoxes;
    }

    /**
     * <p>Setter method for stringNoOfBoxes.</p>
     *
     * @param stringNoOfBoxes Set for stringNoOfBoxes
     */
    public void setStringNoOfBoxes(String stringNoOfBoxes) {
        this.stringNoOfBoxes = stringNoOfBoxes;
    }

    /**
     * <p>Getter method for stringCurrentOrderQty.</p>
     *
     * @return the stringCurrentOrderQty
     */
    public String getStringCurrentOrderQty() {
        return stringCurrentOrderQty;
    }

    /**
     * <p>Setter method for stringCurrentOrderQty.</p>
     *
     * @param stringCurrentOrderQty Set for stringCurrentOrderQty
     */
    public void setStringCurrentOrderQty(String stringCurrentOrderQty) {
        this.stringCurrentOrderQty = stringCurrentOrderQty;
    }

    /**
     * <p>Getter method for kanbanSeqNo.</p>
     *
     * @return the kanbanSeqNo
     */
    public String getKanbanSeqNo() {
        return kanbanSeqNo;
    }

    /**
     * <p>Setter method for kanbanSeqNo.</p>
     *
     * @param kanbanSeqNo Set for kanbanSeqNo
     */
    public void setKanbanSeqNo(String kanbanSeqNo) {
        this.kanbanSeqNo = kanbanSeqNo;
    }

}