/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class DENSO User Domain.
 * @author CSI
 */
public class DensoUserInformationDomain extends BaseDomain implements Serializable {
 
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3736003052624160424L;
    
    /** The Data Scope Control Domain. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID selected. */
    private String dscIdSelected; 
    
    /** The Company DENSO Code. */
    private String dCd;
   
    /** The plant DENSO. */
    private String dPcd;
    
    /** The Role Code. */
    private String roleCode;
    
    /** The First Name. */
    private String firstName;
    
    /** The Middle Name. */
    private String middleName;
    
    /** The Last Name. */
    private String lastName;
    
    /** The Employee Code. */
    private String employeeCode;
    
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The Is Active. */
    private String isActive;
    
    /** The role Flag. */
    private String roleFlag;
    
    /** The information Flag. */
    private String informationFlag;
    
    /** The Stream of CSV file. */
    private StringBuffer resultString;
    
    /** The user Domain.*/
    private UserDomain userDomain;
    
    /** The user DENSO Domain.*/
    private SpsMUserDensoDomain spsMUserDensoDomain;
    
    /** The User Role Domain.*/
    private SpsMUserRoleDomain spsMUserRoleDomain;
    
    /** The role Domain.*/
    private RoleDomain roleDomain;
    
    /** The The DENSO User Domain. */
    private UserDensoDomain userDensoDomain;
    
    /** The List of DENSO User Info domain. */
    private List<DensoUserInformationDomain> userDensoInfoList;
    
    /** The List of Company DENSO domain. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of Plant DENSO domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The Date time update. */
    private Timestamp lastUpdateDatetime;
    
    /** The Date time update Screen. */
    private String lastUpdateDatetimeScreen;
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
            
    /** The Date time update. */
    private Timestamp updateDatetime;
    
    /** The effect Start Screen. */
    private String effectStartScreen;
    
    /** The effect End Screen. */
    private String effectEndScreen;
    
    /** The effect Start Screen. */
    private String createDatetimeScreen;
    
    /** The target page. */
    private String targetPage;
    
    /** The Row Number From. */
    private int rowNumFrom;
    
    /** The Row Number To. */
    private int rowNumTo;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public DensoUserInformationDomain() {
        super(); 
        userDomain = new UserDomain();
        userDensoDomain = new UserDensoDomain();
        roleDomain = new RoleDomain();
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Gets the dscId Selected.
     * 
     * @return the dscId Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }

    /**
     * Sets the dscId Selected.
     * 
     * @param dscIdSelected the new dscId Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    
    /**
     * Gets the role Code
     * 
     * @return the role Code
     */  
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the role Code.
     * 
     * @param roleCode the role Code.
     */ 
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
   
    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public UserDomain getUserDomain() {
        return userDomain;
    }

    /**
     * Sets the user Domain.
     * 
     * @param userDomain the userDomain.
     */
    public void setUserDomain(UserDomain userDomain) {
        this.userDomain = userDomain;
    }

    /**
     * Gets the user Domain.
     * 
     * @return the userDomain.
     */
    public RoleDomain getRoleDomain() {
        return roleDomain;
    }

    /**
     * Sets the role Domain.
     * 
     * @param roleDomain the role Domain.
     */
    public void setRoleDomain(RoleDomain roleDomain) {
        this.roleDomain = roleDomain;
    }

    /**
     * Gets the user DENSO Domain.
     * 
     * @return the user DENSO Domain.
     */
    
    public UserDensoDomain getUserDensoDomain() {
        return userDensoDomain;
    }

    /**
     * Sets the user DENSO Domain.
     * 
     * @param userDensoDomain the user DENSO Domain.
     */
    public void setUserDensoDomain(UserDensoDomain userDensoDomain) {
        this.userDensoDomain = userDensoDomain;
    }
 
    /**
     * Sets the target page.
     * 
     * @param targetPage the new target page
     */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public Timestamp getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(Timestamp updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
    /**
     * Gets the Effect Start Screen.
     * 
     * @return the getEffectStartScreen.
     */ 
    public String getEffectStartScreen() {
        return effectStartScreen;
    }

    /**
     * Sets the effect Start Screen.
     * 
     * @param effectStartScreen the new effect Start Screen.
     */
    public void setEffectStartScreen(String effectStartScreen) {
        this.effectStartScreen = effectStartScreen;
    }

    /**
     * Gets the effect End Screen.
     * 
     * @return the effectEndScreen.
     */ 
    public String getEffectEndScreen() {
        return effectEndScreen;
    }

    /**
     * Sets the effect End Screen.
     * 
     * @param effectEndScreen the new effect End Screen.
     */
    public void setEffectEndScreen(String effectEndScreen) {
        this.effectEndScreen = effectEndScreen;
    }

    /**
     * Gets the create Datetime Screen
     * 
     * @return the createDatetimeScreen.
     */ 
    public String getCreateDatetimeScreen() {
        return createDatetimeScreen;
    }

    /**
     * Sets the create Date time Screen.
     * 
     * @param createDatetimeScreen the new create Date time Screen.
     */
    public void setCreateDatetimeScreen(String createDatetimeScreen) {
        this.createDatetimeScreen = createDatetimeScreen;
    }
    
    /**
     * Gets the list of user DENSO information.
     * 
     * @return the list of user DENSO information.
     */
    public List<DensoUserInformationDomain> getUserDensoInfoList() {
        return userDensoInfoList;
    }

    /**
     * Sets the list of user DENSO information.
     * 
     * @param userDensoInfoList the list of user DENSO information.
     */
    public void setUserDensoInfoList(List<DensoUserInformationDomain> userDensoInfoList) {
        this.userDensoInfoList = userDensoInfoList;
    }
    
    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }
    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the Company DENSO code.
     * 
     * @return the Company DENSO code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Sets the DENSO Company code.
     * 
     * @param dCd the DENSO Company code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * Gets the DENSO plant code.
     * 
     * @return the DENSO plant code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the DENSO plant code.
     * 
     * @param dPcd the DENSO plant code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the list of company DENSO.
     * 
     * @return the list of company DENSO.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }
    /**
     * Sets the list of company DENSO.
     * 
     * @param companyDensoList the list of company DENSO.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    /**
     * Gets the list of Plant DENSO.
     * 
     * @return the list of Plant DENSO.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }
    /**
     * Sets the list of Plant DENSO.
     * 
     * @param plantDensoList the list of Plant DENSO.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    /**
     * Gets the First name.
     * 
     * @return the First name.
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * Sets the First name.
     * 
     * @param firstName the First name..
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * Gets the Middle name.
     * 
     * @return the Middle name.
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * Sets the Middle name.
     * 
     * @param middleName the Middle name..
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Gets the Employee code.
     * 
     * @return the Employee code.
     */
    public String getEmployeeCode() {
        return employeeCode;
    }
    /**
     * Sets the Employee code.
     * 
     * @param employeeCode the Employee code.
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    
    /**
     * Gets the supplier plant code
     * 
     * @return the supplier plant code
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }
    
    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }
    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the Last Update date.
     * 
     * @return the Last Update date.
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Sets the Last Update date.
     * 
     * @param lastUpdateDatetime the Last Update date.
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    
    /**
     * Gets the Is Active.
     * 
     * @return the Is Active.
     */
    public String getIsActive() {
        return isActive;
    }
    
    /**
     * Sets the Is Active.
     * 
     * @param isActive the Is Active.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    
    /**
     * Gets the result string.
     * 
     * @return the result string.
     */
    public StringBuffer getResultString() {
        return resultString;
    }
    
    /**
     * Sets the result string.
     * 
     * @param resultString the result string.
     */
    public void setResultString(StringBuffer resultString) {
        this.resultString = resultString;
    }
    
    /**
     * Gets the DENSO User Domain.
     * 
     * @return the DENSO User Domain.
     */
    public SpsMUserDensoDomain getSpsMUserDensoDomain() {
        return spsMUserDensoDomain;
    }
    
    /**
     * Sets the DENSO User Domain.
     * 
     * @param spsMUserDensoDomain the DENSO User Domain.
     */
    public void setSpsMUserDensoDomain(SpsMUserDensoDomain spsMUserDensoDomain) {
        this.spsMUserDensoDomain = spsMUserDensoDomain;
    }
    
    /**
     * Gets the User Role Domain.
     * 
     * @return the User Role Domain.
     */
    public SpsMUserRoleDomain getSpsMUserRoleDomain() {
        return spsMUserRoleDomain;
    }
    
    /**
     * Sets the User Role Domain.
     * 
     * @param spsMUserRoleDomain the User Role Domain.
     */
    public void setSpsMUserRoleDomain(SpsMUserRoleDomain spsMUserRoleDomain) {
        this.spsMUserRoleDomain = spsMUserRoleDomain;
    }
    
    /**
     * Gets the role flag.
     * 
     * @return the role flag.
     */
    public String getRoleFlag() {
        return roleFlag;
    }
    
    /**
     * Sets the role flag.
     * 
     * @param roleFlag the role flag.
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }
    
    /**
     * Gets the information flag.
     * 
     * @return the information flag.
     */
    public String getInformationFlag() {
        return informationFlag;
    }
    
    /**
     * Sets the information flag.
     * 
     * @param informationFlag the information flag.
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }
    
    /**
     * <p>Getter method for last update screen.</p>
     *
     * @return the last update screen.
     */
    public String getLastUpdateDatetimeScreen() {
        return lastUpdateDatetimeScreen;
    }
    /**
     * <p>Setter method for last update screen.</p>
     *
     * @param lastUpdateDatetimeScreen Set for last update screen.
     */
    public void setLastUpdateDatetimeScreen(String lastUpdateDatetimeScreen) {
        this.lastUpdateDatetimeScreen = lastUpdateDatetimeScreen;
    }
    
    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
}