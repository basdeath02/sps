/*
 * ModifyDate Development company     Describe 
 * 2014/06/18 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class Supplier User Domain.
 * @author CSI
 */
public class FileUploadCriteriaDomain extends BaseDomain implements Serializable {

    /**
     * <p>Serial Version UID.</p>
     */
    private static final long serialVersionUID = -4662049369872694075L;
    
    /** The update date. */
    private String updateDateFrom;
    
    /** The update date. */
    private String updateDateTo;
    
    /** The supplier code. */
    private String sCd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The file name.*/
    private String fileName;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The DENSO code. */
    private String dCd;
    
    /** The DENSO plant code. */
    private String dPcd;
    
    /**
     * Instantiates a new Supplier user domain.
     */
    public FileUploadCriteriaDomain() {
        super();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
    }
       
    /**
     * Gets the update date from.
     * 
     * @return the update date from.
     */
    public String getUpdateDateFrom() {
        return updateDateFrom;
    }
    
    /**
     * Sets the update date from.
     * 
     * @param updateDateFrom the pdate date from.
     */  
    public void setUpdateDateFrom(String updateDateFrom) {
        this.updateDateFrom = updateDateFrom;
    }

    /**
     * Gets the update date to.
     * 
     * @return the update date to.
     */
    public String getUpdateDateTo() {
        return updateDateTo;
    }

    /**
     * Sets the update date to.
     * 
     * @param updateDateTo the update date to.
     */
    public void setUpdateDateTo(String updateDateTo) {
        this.updateDateTo = updateDateTo;
    }
    
    /**
     * Gets the supplier code.
     * 
     * @return the supplier code.
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the supplier code.
     * 
     * @param sCd the supplier code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    
    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * Gets the file name.
     * 
     * @return the file name.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the file name.
     * 
     * @param fileName the file name.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
}