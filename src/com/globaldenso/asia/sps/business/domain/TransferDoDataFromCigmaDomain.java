package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * 
 * <p>TransferDoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferDoDataFromCigmaDomain extends BaseDomain implements Serializable {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -2925920697247847760L;

    /** dataType */
    private String dataType;
    
    /** companyName */
    private String companyName;
    
    /** companyAddress */
    private String companyAddress;
    
    /** truckRoute */
    private String truckRoute;
    
    /** truckSeq */
    private String truckSeq;
    
    /** sCd */
    private String vendorCd;
    
    /** supplierName */
    private String supplierName;
    
    /** supplierLocation */
    private String supplierLocation;
    
    /** issueDate */
    private Date issueDate;
    
    /** cigmaDoNo */
    private String cigmaDoNo;
    
    /** shipDate */
    private String shipDate;
    
    /** shipTime */
    private String shipTime;
    
    /** deliveryDate */
    private String deliveryDate;
    
    /** deliveryTime */
    private String deliveryTime;
    
    /** plantCode */
    private String plantCode;
    
    /** whPrimeReceiving */
    private String whPrimeReceiving;
    
    /** whLocation */
    private String whLocation;
    
    /** receivingDock */
    private String receivingDock;
    
    /** receivingGate */
    private String receivingGate;
    
    /** tm */
    private String tm;
    
    /** cycle */
    private String cycle;
    
    /** controlNo */
    private String controlNo;
    
    /** tripNo */
    private String tripNo;
    
    /** backlog */
    private String backlog;
    
    /** cigmaPoNo */
    private String cigmaPoNo;
    
    /** addTruckRouteFlag */
    private String addTruckRouteFlag;
    
    /** cigmaDoDetail */
    private List<TransferDoDetailDataFromCigmaDomain> cigmaDoDetail;

    /** Supplier Plant Code(SPS) */
    private String sPcd;
    
    /** P/O Id(SPS) */
    private BigDecimal poId;
    
    /** Supplier Code(SPS) */
    private String sCd;
    
    /** tagOutput */
    private String tagOutput;
    
    /** Maximium Create Date Time */
    private String maxCreateDateTime;
    
    /** Maximium Update Date Time */
    private String maxUpdateDateTime;
    
    /** Update By User ID. */
    private String updateByUserId;

    /** Update scanReceiveFlag. */
    private String scanReceiveFlag;

    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferDoDataFromCigmaDomain() {
        super();
    }
    
    /**
     * <p>Getter method for dataType.</p>
     *
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * <p>Setter method for dataType.</p>
     *
     * @param dataType Set for dataType
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * <p>Getter method for companyName.</p>
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * <p>Setter method for companyName.</p>
     *
     * @param companyName Set for companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * <p>Getter method for companyAddress.</p>
     *
     * @return the companyAddress
     */
    public String getCompanyAddress() {
        return companyAddress;
    }

    /**
     * <p>Setter method for companyAddress.</p>
     *
     * @param companyAddress Set for companyAddress
     */
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * <p>Getter method for truckRoute.</p>
     *
     * @return the truckRoute
     */
    public String getTruckRoute() {
        return truckRoute;
    }

    /**
     * <p>Setter method for truckRoute.</p>
     *
     * @param truckRoute Set for truckRoute
     */
    public void setTruckRoute(String truckRoute) {
        this.truckRoute = truckRoute;
    }

    /**
     * <p>Getter method for truckSeq.</p>
     *
     * @return the truckSeq
     */
    public String getTruckSeq() {
        return truckSeq;
    }

    /**
     * <p>Setter method for truckSeq.</p>
     *
     * @param truckSeq Set for truckSeq
     */
    public void setTruckSeq(String truckSeq) {
        this.truckSeq = truckSeq;
    }

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param vendorCd Set for sCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * <p>Getter method for supplierLocation.</p>
     *
     * @return the supplierLocation
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * <p>Setter method for supplierLocation.</p>
     *
     * @param supplierLocation Set for supplierLocation
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * <p>Getter method for issueDate.</p>
     *
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * <p>Setter method for issueDate.</p>
     *
     * @param issueDate Set for issueDate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * <p>Getter method for cigmaDoNo.</p>
     *
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>Setter method for cigmaDoNo.</p>
     *
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>Getter method for shipDate.</p>
     *
     * @return the shipDate
     */
    public String getShipDate() {
        return shipDate;
    }

    /**
     * <p>Setter method for shipDate.</p>
     *
     * @param shipDate Set for shipDate
     */
    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    /**
     * <p>Getter method for shipTime.</p>
     *
     * @return the shipTime
     */
    public String getShipTime() {
        return shipTime;
    }

    /**
     * <p>Setter method for shipTime.</p>
     *
     * @param shipTime Set for shipTime
     */
    public void setShipTime(String shipTime) {
        this.shipTime = shipTime;
    }

    /**
     * <p>Getter method for deliveryDate.</p>
     *
     * @return the deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * <p>Setter method for deliveryDate.</p>
     *
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * <p>Getter method for deliveryTime.</p>
     *
     * @return the deliveryTime
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * <p>Setter method for deliveryTime.</p>
     *
     * @param deliveryTime Set for deliveryTime
     */
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * <p>Getter method for plantCode.</p>
     *
     * @return the plantCode
     */
    public String getPlantCode() {
        return plantCode;
    }

    /**
     * <p>Setter method for plantCode.</p>
     *
     * @param plantCode Set for plantCode
     */
    public void setPlantCode(String plantCode) {
        this.plantCode = plantCode;
    }

    /**
     * <p>Getter method for whPrimeReceiving.</p>
     *
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * <p>Setter method for whPrimeReceiving.</p>
     *
     * @param whPrimeReceiving Set for whPrimeReceiving
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * <p>Getter method for whLocation.</p>
     *
     * @return the whLocation
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * <p>Setter method for whLocation.</p>
     *
     * @param whLocation Set for whLocation
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }

    /**
     * <p>Getter method for receivingDock.</p>
     *
     * @return the receivingDock
     */
    public String getReceivingDock() {
        return receivingDock;
    }

    /**
     * <p>Setter method for receivingDock.</p>
     *
     * @param receivingDock Set for receivingDock
     */
    public void setReceivingDock(String receivingDock) {
        this.receivingDock = receivingDock;
    }

    /**
     * <p>Getter method for receivingGate.</p>
     *
     * @return the receivingGate
     */
    public String getReceivingGate() {
        return receivingGate;
    }

    /**
     * <p>Setter method for receivingGate.</p>
     *
     * @param receivingGate Set for receivingGate
     */
    public void setReceivingGate(String receivingGate) {
        this.receivingGate = receivingGate;
    }

    /**
     * <p>Getter method for tm.</p>
     *
     * @return the tm
     */
    public String getTm() {
        return tm;
    }

    /**
     * <p>Setter method for tm.</p>
     *
     * @param tm Set for tm
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * <p>Getter method for cycle.</p>
     *
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * <p>Setter method for cycle.</p>
     *
     * @param cycle Set for cycle
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * <p>Getter method for controlNo.</p>
     *
     * @return the controlNo
     */
    public String getControlNo() {
        return controlNo;
    }

    /**
     * <p>Setter method for controlNo.</p>
     *
     * @param controlNo Set for controlNo
     */
    public void setControlNo(String controlNo) {
        this.controlNo = controlNo;
    }

    /**
     * <p>Getter method for tripNo.</p>
     *
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * <p>Setter method for tripNo.</p>
     *
     * @param tripNo Set for tripNo
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * <p>Getter method for backlog.</p>
     *
     * @return the backlog
     */
    public String getBacklog() {
        return backlog;
    }

    /**
     * <p>Setter method for backlog.</p>
     *
     * @param backlog Set for backlog
     */
    public void setBacklog(String backlog) {
        this.backlog = backlog;
    }

    /**
     * <p>Getter method for cigmaPoNo.</p>
     *
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }

    /**
     * <p>Setter method for cigmaPoNo.</p>
     *
     * @param cigmaPoNo Set for cigmaPoNo
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * <p>Getter method for addTruckRouteFlag.</p>
     *
     * @return the addTruckRouteFlag
     */
    public String getAddTruckRouteFlag() {
        return addTruckRouteFlag;
    }

    /**
     * <p>Setter method for addTruckRouteFlag.</p>
     *
     * @param addTruckRouteFlag Set for addTruckRouteFlag
     */
    public void setAddTruckRouteFlag(String addTruckRouteFlag) {
        this.addTruckRouteFlag = addTruckRouteFlag;
    }

    /**
     * <p>Getter method for cigmaDoDetail.</p>
     *
     * @return the cigmaDoDetail
     */
    public List<TransferDoDetailDataFromCigmaDomain> getCigmaDoDetail() {
        return cigmaDoDetail;
    }

    /**
     * <p>Setter method for cigmaDoDetail.</p>
     *
     * @param cigmaDoDetail Set for cigmaDoDetail
     */
    public void setCigmaDoDetail(
        List<TransferDoDetailDataFromCigmaDomain> cigmaDoDetail) {
        this.cigmaDoDetail = cigmaDoDetail;
    }

    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * <p>Getter method for spsSCd.</p>
     *
     * @return the spsSCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for spsSCd.</p>
     *
     * @param sCd Set for spsSCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for maxCreateDateTime.</p>
     *
     * @return the maxCreateDateTime
     */
    public String getMaxCreateDateTime() {
        return maxCreateDateTime;
    }

    /**
     * <p>Setter method for maxCreateDateTime.</p>
     *
     * @param maxCreateDateTime Set for maxCreateDateTime
     */
    public void setMaxCreateDateTime(String maxCreateDateTime) {
        this.maxCreateDateTime = maxCreateDateTime;
    }

    /**
     * <p>Getter method for maxUpdateDateTime.</p>
     *
     * @return the maxUpdateDateTime
     */
    public String getMaxUpdateDateTime() {
        return maxUpdateDateTime;
    }

    /**
     * <p>Setter method for maxUpdateDateTime.</p>
     *
     * @param maxUpdateDateTime Set for maxUpdateDateTime
     */
    public void setMaxUpdateDateTime(String maxUpdateDateTime) {
        this.maxUpdateDateTime = maxUpdateDateTime;
    }

    /**
     * <p>Getter method for updateByUserId.</p>
     *
     * @return the updateByUserId
     */
    public String getUpdateByUserId() {
        return updateByUserId;
    }

    /**
     * <p>Setter method for updateByUserId.</p>
     *
     * @param updateByUserId Set for updateByUserId
     */
    public void setUpdateByUserId(String updateByUserId) {
        this.updateByUserId = updateByUserId;
    }

    /**
     * <p>Getter method for scanReceiveFlag.</p>
     *
     * @return the scanReceiveFlag
     */
    public String getScanReceiveFlag() {
        return scanReceiveFlag;
    }

    /**
     * <p>Setter method for scanReceiveFlag.</p>
     *
     * @param scanReceiveFlag Set for scanReceiveFlag
     */
    public void setScanReceiveFlag(String scanReceiveFlag) {
        this.scanReceiveFlag = scanReceiveFlag;
    }

    /**
     * <p>Getter method for tagOutput.</p>
     *
     * @return the tagOutput
     */
    public String getTagOutput() {
        return tagOutput;
    }

    /**
     * <p>Setter method for tagOutput.</p>
     *
     * @param tagOutput Set for tagOutput
     */
    public void setTagOutput(String tagOutput) {
        this.tagOutput = tagOutput;
    }
    
}
