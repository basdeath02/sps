/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/21 CSI Akat           Create   
 *
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class RoleUserDomain.
 * 
 * @author CSI
 * @version 1.00
 */
public class RoleUserDomain extends BaseDomain implements Serializable {

    /** The generated serial version UID. */
    private static final long serialVersionUID = -241185620092791005L;

    /**
     * Role code
     */
    private String roleCd;

    /**
     * DENSO user DSC ID
     */
    private String dscId;

    /**
     * Sequence Number
     */
    private BigDecimal seqNo;

    /**
     * DENSO company code
     */
    private String dCd;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * Supplier conpany code
     */
    private String sCd;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /** The default constructor. */
    public RoleUserDomain() {
        super();
    }

    /**
     * Get method for roleCd.
     * @return the roleCd
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Set method for roleCd.
     * @param roleCd the roleCd to set
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Get method for dscId.
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Set method for dscId.
     * @param dscId the dscId to set
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Get method for seqNo.
     * @return the seqNo
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Set method for seqNo.
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Get method for dCd.
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Set method for dCd.
     * @param cd the dCd to set
     */
    public void setDCd(String cd) {
        dCd = cd;
    }

    /**
     * Get method for dPcd.
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Set method for dPcd.
     * @param pcd the dPcd to set
     */
    public void setDPcd(String pcd) {
        dPcd = pcd;
    }

    /**
     * Get method for sCd.
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Set method for sCd.
     * @param cd the sCd to set
     */
    public void setSCd(String cd) {
        sCd = cd;
    }

    /**
     * Get method for sPcd.
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Set method for sPcd.
     * @param pcd the sPcd to set
     */
    public void setSPcd(String pcd) {
        sPcd = pcd;
    }
    
}
