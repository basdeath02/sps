/*
 * ModifyDate Development company     Describe 
 * 2014/07/02 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;

/**
 * The Class ASN Information Domain.
 * @author CSI
 */
public class AsnInformationDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4709754216444188102L;
    
    /** The plan ETA date from. */
    private String planEtaDateFrom;
    
    /** The plan ETA month from. */
    private int planEtaMonthFrom;
    
    /** The plan ETA week from. */
    private int planEtaWeekFrom;
    
    /** The plan ETA date to. */
    private String planEtaDateTo;
    
    /** The plan ETA month to. */
    private int planEtaMonthTo;
    
    /** The plan ETA week to. */
    private int planEtaWeekTo;
    
    /** The plan ETA time from. */
    private String planEtaTimeFrom;
    
    /** The plan ETA time to. */
    private String planEtaTimeTo;
    
    /** The actual ETd date from. */
    private String actualEtdDateFrom;
    
    /** The actual ETd date to. */
    private String actualEtdDateTo;
    
    /** The supplier tax ID. */
    private String sTaxId;
    
    /** The actual ETD. */
    private String actualEtd;
    
    /** The plan ETA. */
    private String planEta;
    
    /** The asn Selected. */
    private String asnSelected;
    
    /** The file ID of file manager. */
    private String fileId; 
    
    /** The D/O last update date time. */
    private Timestamp doLastUpdateDatetime;
    
    /** The pn D/O last update date time. */
    private Timestamp pnDoLastUpdateDatetime;
    
    /** The ship date time. */
    private Timestamp shipDatetime;
    
    /** The delivery date time. */
    private Timestamp deliveryDatetime;
    
    /** The order method. */
    private String orderMethod;
    
    /** The chg cigma do no. */
    private String chgCigmaDoNo;
    
    /** The current order qty. */
    private BigDecimal currentOrderQty;
    
    /** The total shipped qty. */
    private BigDecimal totalShippedQty;
    
    /** The row count. */
    private String rowCount;
    
    /** The record group. */
    private String recordGroup;
    
    /** The change reason name. */
    private String changeReasonName;
    
    /** The SPS transaction ASN domain. */
    private SpsTAsnDomain spsTAsnDomain;
    
    /** The SPS transaction ASN detail domain. */
    private SpsTAsnDetailDomain spsTAsnDetailDomain;
    
    /** The SPS master company DENSO domain. */
    private SpsMCompanyDensoDomain spsMCompanyDensoDomain;
    
    /** The SPS master user domain. */
    private SpsMUserDomain spsMUserDomain;
    
    /** The SPS master misc domain. */
    private SpsMMiscDomain spsMMiscDomain;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /**
     * Instantiates a new ASN Information domain.
     */
    public AsnInformationDomain() {
        super();
        this.spsTAsnDomain = new SpsTAsnDomain();
        this.spsTAsnDetailDomain = new SpsTAsnDetailDomain();
        this.spsMMiscDomain = new SpsMMiscDomain();
        this.spsMUserDomain = new SpsMUserDomain();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * <p>Getter method for plan ETA date from.</p>
     *
     * @return the plan ETA date from
     */
    public String getPlanEtaDateFrom() {
        return planEtaDateFrom;
    }
    
    /**
     * <p>Setter method for plan ETA date from.</p>
     *
     * @param planEtaDateFrom Set for plan ETA date from
     */
    public void setPlanEtaDateFrom(String planEtaDateFrom) {
        this.planEtaDateFrom = planEtaDateFrom;
    }
    
    /**
     * <p>Getter method for plan ETA month from.</p>
     *
     * @return the plan ETA month from
     */
    public int  getPlanEtaMonthFrom() {
        return planEtaMonthFrom;
    }
    
    /**
     * <p>Setter method for plan ETA month from.</p>
     *
     * @param planEtaMonthFrom Set for plan ETA month from
     */
    public void setPlanEtaMonthFrom(int planEtaMonthFrom) {
        this.planEtaMonthFrom = planEtaMonthFrom;
    }
    
    /**
     * <p>Getter method for plan ETA week from.</p>
     *
     * @return the plan ETA week from
     */
    public int  getPlanEtaWeekFrom() {
        return planEtaWeekFrom;
    }
    
    /**
     * <p>Setter method for plan ETA week from.</p>
     *
     * @param planEtaWeekFrom Set for plan ETA week from
     */
    public void setPlanEtaWeekFrom(int planEtaWeekFrom) {
        this.planEtaWeekFrom = planEtaWeekFrom;
    }
    
    /**
     * <p>Getter method for plan ETA date to.</p>
     *
     * @return the plan ETA date to
     */
    public String getPlanEtaDateTo() {
        return planEtaDateTo;
    }

    /**
     * <p>Setter method for plan ETA date to.</p>
     *
     * @param planEtaDateTo Set for plan ETA date to
     */
    public void setPlanEtaDateTo(String planEtaDateTo) {
        this.planEtaDateTo = planEtaDateTo;
    }
    
    /**
     * <p>Getter method for plan ETA month to.</p>
     *
     * @return the plan ETA month to
     */
    public int getPlanEtaMonthTo() {
        return planEtaMonthTo;
    }
    
    /**
     * <p>Setter method for plan ETA month to.</p>
     *
     * @param planEtaMonthTo Set for plan ETA month to
     */
    public void setPlanEtaMonthTo(int planEtaMonthTo) {
        this.planEtaMonthTo = planEtaMonthTo;
    }
    
    /**
     * <p>Getter method for plan ETA week to.</p>
     *
     * @return the plan ETA week to
     */
    public int  getPlanEtaWeekTo() {
        return planEtaWeekTo;
    }
    
    /**
     * <p>Setter method for plan ETA week to.</p>
     *
     * @param planEtaWeekTo Set for plan ETA week to
     */
    public void setPlanEtaWeekTo(int planEtaWeekTo) {
        this.planEtaWeekTo = planEtaWeekTo;
    }
    
    /**
     * Gets the sps ASN domain.
     * 
     * @return the spsTInvoiceDomain.
     */
    public SpsTAsnDomain getSpsTAsnDomain() {
        return spsTAsnDomain;
    }
    
    /**
     * Sets the sps ASN domain.
     * 
     * @param spsTAsnDomain the sps ASN domain.
     */  
    public void setSpsTAsnDomain(SpsTAsnDomain spsTAsnDomain) {
        this.spsTAsnDomain = spsTAsnDomain;
    }
    
    /**
     * <p>Getter method for actual ETD date from.</p>
     *
     * @return the actual ETD date from
     */
    public String getActualEtdDateFrom() {
        return actualEtdDateFrom;
    }
    
    /**
     * <p>Setter method for actual ETD date from.</p>
     *
     * @param actualEtdDateFrom Set for actual ETD date from
     */
    public void setActualEtdDateFrom(String actualEtdDateFrom) {
        this.actualEtdDateFrom = actualEtdDateFrom;
    }
    
    /**
     * <p>Getter method for actual ETD date to.</p>
     *
     * @return the actual ETD date to
     */
    public String getActualEtdDateTo() {
        return actualEtdDateTo;
    }
    
    /**
     * <p>Setter method for actual ETD date to.</p>
     *
     * @param actualEtdDateTo Set for actual ETD date to
     */
    public void setActualEtdDateTo(String actualEtdDateTo) {
        this.actualEtdDateTo = actualEtdDateTo;
    }
    
    /**
     * Gets the supplier tax id.
     * 
     * @return the supplier tax id.
     */
    public String getSTaxId() {
        return sTaxId;
    }
    
    /**
     * Sets the supplier tax id.
     * 
     * @param sTaxId the supplier tax id.
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }
    
    /**
     * Gets the actual Etd.
     * 
     * @return the actual Etd.
     */
    public String getActualEtd() {
        return actualEtd;
    }
    
    /**
     * Sets the actual Etd.
     * 
     * @param actualEtd the actual Etd.
     */
    public void setActualEtd(String actualEtd) {
        this.actualEtd = actualEtd;
    }
    
    /**
     * Gets the plan Eta.
     * 
     * @return the plan Eta.
     */
    public String getPlanEta() {
        return planEta;
    }
    
    /**
     * Sets the plan Eta.
     * 
     * @param planEta the plan Eta.
     */
    public void setPlanEta(String planEta) {
        this.planEta = planEta;
    }
    
    
    /**
     * Gets the ASN selected.
     * 
     * @return the ASN selected.
     */
    public String getAsnSelected() {
        return asnSelected;
    }
    
    /**
     * Sets the ASN selected.
     * 
     * @param asnSelected the ASN selected.
     */
    public void setAsnSelected(String asnSelected) {
        this.asnSelected = asnSelected;
    }
    
    /**
     * Gets the file ID of file manager.
     * 
     * @return the file ID of file manager.
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Sets the file ID of file manager.
     * 
     * @param fileId the file ID of file manager.
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * Gets the SPS master company DENSO domain.
     * 
     * @return the SPS master company DENSO domain.
     */
    public SpsMCompanyDensoDomain  getSpsMCompanyDensoDomain () {
        return spsMCompanyDensoDomain;
    }

    /**
     * Sets the SPS master company DENSO domain.
     * 
     * @param spsMCompanyDensoDomain the SPS master company DENSO domain.
     */
    public void setSpsMCompanyDensoDomain (SpsMCompanyDensoDomain  spsMCompanyDensoDomain) {
        this.spsMCompanyDensoDomain = spsMCompanyDensoDomain;
    }
    
    /**
     * Gets the sps m user domain.
     * 
     * @return the sps m user domain.
     */
    public SpsMUserDomain getSpsMUserDomain() {
        return spsMUserDomain;
    }

    /**
     * Sets the sps m userdomain.
     * 
     * @param spsMUserDomain the sps m user domain.
     */
    public void setSpsMUserDomain(SpsMUserDomain spsMUserDomain) {
        this.spsMUserDomain = spsMUserDomain;
    }

    /**
     * Gets the asn detail domain.
     * 
     * @return the asn detail domain.
     */
    public SpsTAsnDetailDomain getSpsTAsnDetailDomain() {
        return spsTAsnDetailDomain;
    }

    /**
     * Sets the asn detail domain.
     * 
     * @param spsTAsnDetailDomain the asn detail domain.
     */
    public void setSpsTAsnDetailDomain(SpsTAsnDetailDomain spsTAsnDetailDomain) {
        this.spsTAsnDetailDomain = spsTAsnDetailDomain;
    }

    /**
     * Gets the misc domain.
     * 
     * @return the misc domain.
     */
    public SpsMMiscDomain getSpsMMiscDomain() {
        return spsMMiscDomain;
    }

    /**
     * Sets the misc domain.
     * 
     * @param spsMMiscDomain the misc domain.
     */
    public void setSpsMMiscDomain(SpsMMiscDomain spsMMiscDomain) {
        this.spsMMiscDomain = spsMMiscDomain;
    }
    
    /**
     * Gets the D/O last update date time.
     * 
     * @return the D/O last update date time.
     */
    public Timestamp getDoLastUpdateDatetime() {
        return doLastUpdateDatetime;
    }

    /**
     * Sets the D/O last update date time.
     * 
     * @param doLastUpdateDatetime the D/O last update date time.
     */
    public void setDoLastUpdateDatetime(Timestamp doLastUpdateDatetime) {
        this.doLastUpdateDatetime = doLastUpdateDatetime;
    }
    
    /**
     * Gets the pn D/O last update date time.
     * 
     * @return the pn D/O last update date time.
     */
    public Timestamp getPnDoLastUpdateDatetime() {
        return pnDoLastUpdateDatetime;
    }

    /**
     * Sets the pn D/O last update date time.
     * 
     * @param pnDoLastUpdateDatetime the pn D/O last update date time.
     */
    public void setPnDoLastUpdateDatetime(Timestamp pnDoLastUpdateDatetime) {
        this.pnDoLastUpdateDatetime = pnDoLastUpdateDatetime;
    }

    /**
     * Gets the ship date time.
     * 
     * @return the ship date time.
     */
    public Timestamp getShipDatetime() {
        return shipDatetime;
    }

    /**
     * Sets the ship date time.
     * 
     * @param shipDatetime the ship date time.
     */
    public void setShipDatetime(Timestamp shipDatetime) {
        this.shipDatetime = shipDatetime;
    }
    
    /**
     * Gets the delivery date time.
     * 
     * @return the delivery date time.
     */
    public Timestamp getDeliveryDatetime() {
        return deliveryDatetime;
    }

    /**
     * Sets the delivery date time.
     * 
     * @param deliveryDatetime the delivery date time.
     */
    public void setDeliveryDatetime(Timestamp deliveryDatetime) {
        this.deliveryDatetime = deliveryDatetime;
    }

    /**
     * Gets the order method.
     * 
     * @return the order method.
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Sets the order method.
     * 
     * @param orderMethod the order method.
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Gets the chg cigma do no.
     * 
     * @return the chg cigma do no.
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * Sets the chg cigma do no.
     * 
     * @param chgCigmaDoNo the chg cigma do no.
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * Gets the current order qty.
     * 
     * @return the current order qty.
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Sets the current order qty.
     * 
     * @param currentOrderQty the current order qty.
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Gets the total shipped qty.
     * 
     * @return the total shipped qty.
     */
    public BigDecimal getTotalShippedQty() {
        return totalShippedQty;
    }

    /**
     * Sets the total shipped qty.
     * 
     * @param totalShippedQty the total shipped qty.
     */
    public void setTotalShippedQty(BigDecimal totalShippedQty) {
        this.totalShippedQty = totalShippedQty;
    }
    
    /**
     * Gets the row count.
     * 
     * @return the row count.
     */
    public String getRowCount() {
        return rowCount;
    }

    /**
     * Sets the row count.
     * 
     * @param rowCount the row count.
     */
    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }
    
    /**
     * Gets the record group.
     * 
     * @return the record group
     */
    public String getRecordGroup() {
        return recordGroup;
    }

    /**
     * Sets the record group.
     * 
     * @param recordGroup the record group
     */
    public void setRecordGroup(String recordGroup) {
        this.recordGroup = recordGroup;
    }

    /**
     * <p>Getter method for change reason name.</p>
     *
     * @return the change reason name
     */
    public String getChangeReasonName() {
        return changeReasonName;
    }

    /**
     * <p>Setter method for change reason name.</p>
     *
     * @param changeReasonName Set for change reason name
     */
    public void setChangeReasonName(String changeReasonName) {
        this.changeReasonName = changeReasonName;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for planEtaTimeFrom.</p>
     *
     * @return the planEtaTimeFrom
     */
    public String getPlanEtaTimeFrom() {
        return planEtaTimeFrom;
    }

    /**
     * <p>Setter method for planEtaTimeFrom.</p>
     *
     * @param planEtaTimeFrom Set for planEtaTimeFrom
     */
    public void setPlanEtaTimeFrom(String planEtaTimeFrom) {
        this.planEtaTimeFrom = planEtaTimeFrom;
    }

    /**
     * <p>Getter method for planEtaTimeTo.</p>
     *
     * @return the planEtaTimeTo
     */
    public String getPlanEtaTimeTo() {
        return planEtaTimeTo;
    }

    /**
     * <p>Setter method for planEtaTimeTo.</p>
     *
     * @param planEtaTimeTo Set for planEtaTimeTo
     */
    public void setPlanEtaTimeTo(String planEtaTimeTo) {
        this.planEtaTimeTo = planEtaTimeTo;
    }
    
}