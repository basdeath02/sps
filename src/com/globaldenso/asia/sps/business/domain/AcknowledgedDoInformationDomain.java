/*
 * ModifyDate Development company    Describe 
 * 2014/07/22 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * The Class Acknowledged DO Information Domain.
 * @author CSI
 */
public class AcknowledgedDoInformationDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 303558568853491292L;
    
    /** The delivery date from. */
    private String deliveryDateFrom;
    
    /** The delivery date to. */
    private String deliveryDateTo;
    
    /** The delivery time from. */
    private String deliveryTimeFrom;
    
    /** The delivery time to. */
    private String deliveryTimeTo;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The shipment status. */
    private String shipmentStatus;
    
    /** The trans. */
    private String trans;
    
    /** The route no. */
    private String routeNo;
    
    /** The del. */
    private String del;
    
    /** The ship date from. */
    private String shipDateFrom;
    
    /** The ship date to. */
    private String shipDateTo;
    
    /** The issue date from. */
    private String issueDateFrom;
    
    /** The issue date to. */
    private String issueDateTo;
    
    /** The order method. */
    private String orderMethod;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The cigma do no. */
    private String cigmaDoNo;
    
    /** The pdf file id. */
    private String pdfFileId;
    
    /** The mode. */
    private String mode;
    
    /** The receiveByScan. */
    private String receiveByScan;
    
    /** The ltFlag. */
    private String ltFlag;
    
    /** The doId. */
    private String doId;
    
    /** The data scope control list. */
    private DataScopeControlDomain dataScopeControlDomain;
    
    /** The shipment status list. */
    private List<MiscellaneousDomain> shipmentStatusList;
    
    /** The trans list. */
    private List<MiscellaneousDomain> transList;
    
    /** The order method list. */
    private List<MiscellaneousDomain> orderMethodList;
    
    /** The receive By Scan List. */
    private List<MiscellaneousDomain> receiveByScanList;
    
    /** The Leadtime flag List. */
    private List<MiscellaneousDomain> ltFlagList;
    
    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;
    
    /** The warning message list. */
    private List<ApplicationMessageDomain> warningMessageList;
    
    /** The company supplier list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The company denso list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The plant supplier list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The acknowledged do information list. */
    private List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList;
    
    /**
     * Instantiates a new acknowledged do information domain.
     */
    public AcknowledgedDoInformationDomain() {
        super();
    }
    
    /**
     * Gets the shipment status list.
     * 
     * @return the shipment status list.
     */
    public List<MiscellaneousDomain> getShipmentStatusList() {
        return shipmentStatusList;
    }

    /**
     * Sets the shipment status list.
     * 
     * @param shipmentStatusList the shipment status list.
     */
    public void setShipmentStatusList(List<MiscellaneousDomain> shipmentStatusList) {
        this.shipmentStatusList = shipmentStatusList;
    }

    /**
     * Gets the trans list.
     * 
     * @return the trans list.
     */
    public List<MiscellaneousDomain> getTransList() {
        return transList;
    }

    /**
     * Sets the trans list.
     * 
     * @param transList the trans list.
     */
    public void setTransList(List<MiscellaneousDomain> transList) {
        this.transList = transList;
    }

    /**
     * Gets the order method list.
     * 
     * @return the order method list.
     */
    public List<MiscellaneousDomain> getOrderMethodList() {
        return orderMethodList;
    }

    /**
     * Sets the order method list.
     * 
     * @param orderMethodList the order method list.
     */
    public void setOrderMethodList(List<MiscellaneousDomain> orderMethodList) {
        this.orderMethodList = orderMethodList;
    }

    /**
     * Gets the data scope control domain.
     * 
     * @return the data scope control domain.
     */
    public DataScopeControlDomain getDataScopeControlDomain() {
        return dataScopeControlDomain;
    }

    /**
     * Sets the data scope control domain.
     * 
     * @param dataScopeControlDomain the data scope control domain.
     */
    public void setDataScopeControlDomain(
        DataScopeControlDomain dataScopeControlDomain) {
        this.dataScopeControlDomain = dataScopeControlDomain;
    }

//    /**
//     * Gets the company supplier list.
//     * 
//     * @return the company supplier list.
//     */
//    public List<MiscellaneousDomain> getCompanySupplierList() {
//        return companySupplierList;
//    }
//
//    /**
//     * Sets the company supplier list.
//     * 
//     * @param companySupplierList the company supplier list.
//     */
//    public void setCompanySupplierList(
//        List<MiscellaneousDomain> companySupplierList) {
//        this.companySupplierList = companySupplierList;
//    }
//
//    /**
//     * Gets the company denso list.
//     * 
//     * @return the company denso list.
//     */
//    public List<MiscellaneousDomain> getCompanyDensoList() {
//        return companyDensoList;
//    }
//
//    /**
//     * Sets the company denso list.
//     * 
//     * @param companyDensoList the company denso list.
//     */
//    public void setCompanyDensoList(List<MiscellaneousDomain> companyDensoList) {
//        this.companyDensoList = companyDensoList;
//    }

    /**
     * Gets the delivery date from.
     * 
     * @return the delivery date from.
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * Sets the delivery date from.
     * 
     * @param deliveryDateFrom the delivery date from.
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * Gets the delivery date to.
     * 
     * @return the delivery date to.
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * Sets the delivery date to.
     * 
     * @param deliveryDateTo the delivery date to.
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }
    
    /**
     * Gets the delivery time from.
     * 
     * @return the delivery time from
     */
    public String getDeliveryTimeFrom() {
        return deliveryTimeFrom;
    }

    /**
     * Sets the delivery time from.
     * 
     * @param deliveryTimeFrom the delivery time from
     */
    public void setDeliveryTimeFrom(String deliveryTimeFrom) {
        this.deliveryTimeFrom = deliveryTimeFrom;
    }

    /**
     * Gets the delivery time to.
     * 
     * @return the delivery time to
     */
    public String getDeliveryTimeTo() {
        return deliveryTimeTo;
    }

    /**
     * Sets the delivery time to.
     * 
     * @param deliveryTimeTo the delivery time to
     */
    public void setDeliveryTimeTo(String deliveryTimeTo) {
        this.deliveryTimeTo = deliveryTimeTo;
    }

    /**
     * Gets the vendor code.
     * 
     * @return the vendor code.
     */
    public String getVendorCd() {
        return vendorCd;
    }
    
    /**
     * Sets the vendor code.
     * 
     * @param vendorCd the vendor code.
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso code.
     * 
     * @return the denso code.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso code.
     * 
     * @param dCd the denso code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the denso plant code.
     * 
     * @return the denso plant code.
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the denso plant code.
     * 
     * @param dPcd the denso plant code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the shipment status.
     * 
     * @return the shipment status.
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * Sets the shipment status.
     * 
     * @param shipmentStatus the shipment status.
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * Gets the trans.
     * 
     * @return the trans.
     */
    public String getTrans() {
        return trans;
    }

    /**
     * Sets the trans.
     * 
     * @param trans the trans.
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * Gets the route no.
     * 
     * @return the route no.
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * Sets the route no.
     * 
     * @param routeNo the route no.
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * Gets the del.
     * 
     * @return the del.
     */
    public String getDel() {
        return del;
    }

    /**
     * Sets the del.
     * 
     * @param del the del.
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * Gets the ship date from.
     * 
     * @return the ship date from.
     */
    public String getShipDateFrom() {
        return shipDateFrom;
    }

    /**
     * Sets the ship date from.
     * 
     * @param shipDateFrom the ship date from.
     */
    public void setShipDateFrom(String shipDateFrom) {
        this.shipDateFrom = shipDateFrom;
    }

    /**
     * Gets the ship date to.
     * 
     * @return the ship date to.
     */
    public String getShipDateTo() {
        return shipDateTo;
    }

    /**
     * Sets the ship date to.
     * 
     * @param shipDateTo the ship date to.
     */
    public void setShipDateTo(String shipDateTo) {
        this.shipDateTo = shipDateTo;
    }

    /**
     * Gets the issue date from.
     * 
     * @return the issue date from.
     */
    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    /**
     * Sets the issue date from.
     * 
     * @param issueDateFrom the issue date from.
     */
    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    /**
     * Gets the issue date to.
     * 
     * @return the issue date to.
     */
    public String getIssueDateTo() {
        return issueDateTo;
    }

    /**
     * Sets the issue date to.
     * 
     * @param issueDateTo the issue date to.
     */
    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    /**
     * Gets the order method.
     * 
     * @return the order method.
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Sets the order method.
     * 
     * @param orderMethod the order method.
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Gets the sps do no.
     * 
     * @return the sps do no.
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }
    
    /**
     * Sets the sps do no.
     * 
     * @param spsDoNo the sps do no.
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }
    
    /**
     * Gets the cigma do no.
     * 
     * @return the cigma do no.
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }
    
    /**
     * Sets the cigma do no.
     * 
     * @param cigmaDoNo the cigma do no.
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }
    
    /**
     * Gets the pdf file id.
     * 
     * @return the pdf file id.
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Sets the pdf file id.
     * 
     * @param pdfFileId the pdf file id.
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Gets the acknowledged do information list.
     * 
     * @return the acknowledged do information list.
     */
    public List<AcknowledgedDoInformationReturnDomain> getAcknowledgedDoInformationList() {
        return acknowledgedDoInformationList;
    }

    /**
     * Sets the acknowledged do information list.
     * 
     * @param acknowledgedDoInformationList the acknowledged do information list.
     */
    public void setAcknowledgedDoInformationList(
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList) {
        this.acknowledgedDoInformationList = acknowledgedDoInformationList;
    }
    
    /**
     * Gets the company supplier list.
     * 
     * @return the company supplier  list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the company supplier  list.
     * 
     * @param companySupplierList the company supplier list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }
    
    /**
     * Gets the company denso list.
     * 
     * @return the company denso list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the company denso list.
     * 
     * @param companyDensoList the company denso list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso plant list.
     * 
     * @return the company denso plant list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the company denso plant list.
     * 
     * @param plantDensoList the company denso plant list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the company supplier plant list.
     * 
     * @return the company supplier plant list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the company supplier plant list.
     * 
     * @param plantSupplierList the company supplier plant list.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * Gets the mode.
     * 
     * @return the mode.
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode.
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the error message list.
     * 
     * @return the error message list.
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     * 
     * @param errorMessageList the error message list.
     */
    public void setErrorMessageList(List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the warning message list.
     * 
     * @return the warning message list.
     */
    public List<ApplicationMessageDomain> getWarningMessageList() {
        return warningMessageList;
    }

    /**
     * Sets the warning message list.
     * 
     * @param warningMessageList the warning message list.
     */
    public void setWarningMessageList(
        List<ApplicationMessageDomain> warningMessageList) {
        this.warningMessageList = warningMessageList;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for receiveByScanList.</p>
     *
     * @return the receiveByScanList
     */
    public List<MiscellaneousDomain> getReceiveByScanList() {
        return receiveByScanList;
    }

    /**
     * <p>Setter method for receiveByScanList.</p>
     *
     * @param receiveByScanList Set for receiveByScanList
     */
    public void setReceiveByScanList(List<MiscellaneousDomain> receiveByScanList) {
        this.receiveByScanList = receiveByScanList;
    }

    /**
     * <p>Getter method for receiveByScan.</p>
     *
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * <p>Setter method for receiveByScan.</p>
     *
     * @param receiveByScan Set for receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    /**
     * <p>Getter method for ltFlag.</p>
     *
     * @return the ltFlag
     */
    public String getLtFlag() {
        return ltFlag;
    }

    /**
     * <p>Setter method for ltFlag.</p>
     *
     * @param ltFlag Set for ltFlag
     */
    public void setLtFlag(String ltFlag) {
        this.ltFlag = ltFlag;
    }

    /**
     * <p>Getter method for ltFlagList.</p>
     *
     * @return the ltFlagList
     */
    public List<MiscellaneousDomain> getLtFlagList() {
        return ltFlagList;
    }

    /**
     * <p>Setter method for ltFlagList.</p>
     *
     * @param ltFlagList Set for ltFlagList
     */
    public void setLtFlagList(List<MiscellaneousDomain> ltFlagList) {
        this.ltFlagList = ltFlagList;
    }

    /**
     * <p>Getter method for doId.</p>
     *
     * @return the doId
     */

    public String getDoId() {
        return doId;
    }

    /**
     * <p>Setter method for doId.</p>
     *
     * @param doId Set for doId
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

}