/*
 * ModifyDate Development company    Describe 
 * 2014/07/29 CSI Chatchai           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;

/**
 * <p>
 * The Class DeliveryOrderInformationDomain.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class DeliveryOrderInformationDomain extends BaseDomain implements Serializable {
    
    /** The serialVersionUID. */
    private static final long serialVersionUID = 3864271285148937010L;

    /** The List of Denso Supplier Relation. */
    private List<DensoSupplierRelationDomain> densoSupplierRelationList;

    /** The do Id. */
    private String doId;

    /** The Supplier Code. */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /** The Supplier Plant Code. */
    private String sPcd;

    /** The Denso Code. */
    private String dCd;

    /** The Denso Plant Code. */
    private String dPcd;

    /** The issue Date From. */
    private String issueDateFrom;

    /** The issue Date To. */
    private String issueDateTo;

    /** The delivery Date From. */
    private String deliveryDateFrom;

    /** The delivery Date To. */
    private String deliveryDateTo;
    
    /** The delivery time from. */
    private String deliveryTimeFrom;
    
    /** The delivery time to */
    private String deliveryTimeTo;
    
    /** The ship Date From. */
    private String shipDateFrom;

    /** The ship Date To. */
    private String shipDateTo;

    /** The revision. */
    private String revision;

    /** The shipment Status. */
    private String shipmentStatus;

    /** The transport Mode. */
    private String transportMode;

    /** The route No. */
    private String routeNo;

    /** The del. */
    private String del;

    /** The sps DO No. */
    private String spsDoNo;

    /** The cigma DO No. */
    private String cigmaDoNo;

    /** The pdf File Id. */
    private String pdfFileId;

    /** The pdf file name. */
    private String pdfFileName;
    
    /** The order method. */
    private String orderMethod;
    
    /** The denso part no. */
    private String dPn;
    
    /** The supplier part no. */
    private String sPn;
    
    /** The unit of measure. */
    private String unitOfMeasure;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The company supplier domain. */
    private List<CompanySupplierDomain> companySupplierList;

    /** The plant supplier domain. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The company denso domain. */
    private List<CompanyDensoDomain> companyDensoList;

    /** The plant denso domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The misc revision. */
    private List<MiscellaneousDomain> miscRevision;

    /** The misc shipment status cb. */
    private List<MiscellaneousDomain> miscShipmentStatusCb;

    /** The transport mode cb. */
    private List<MiscellaneousDomain> transportModeCb;

    /** The error message list. */
    private List<ApplicationMessageDomain> errorMessageList;

    /**
     * Instantiates a new delivery order information domain.
     */
    public DeliveryOrderInformationDomain() {
        super();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }

    /**
     * <p>
     * Getter method for densoSupplierRelationList.
     * </p>
     * 
     * @return the densoSupplierRelationList
     */
    public List<DensoSupplierRelationDomain> getDensoSupplierRelationList() {
        return densoSupplierRelationList;
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationList.
     * </p>
     * 
     * @param densoSupplierRelationList Set for densoSupplierRelationList
     */
    public void setDensoSupplierRelationList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList) {
        this.densoSupplierRelationList = densoSupplierRelationList;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for dCd.
     * </p>
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for dCd.
     * </p>
     * 
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for issueDateFrom.
     * </p>
     * 
     * @return the issueDateFrom
     */
    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    /**
     * <p>
     * Setter method for issueDateFrom.
     * </p>
     * 
     * @param issueDateFrom Set for issueDateFrom
     */
    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    /**
     * <p>
     * Getter method for issueDateTo.
     * </p>
     * 
     * @return the issueDateTo
     */
    public String getIssueDateTo() {
        return issueDateTo;
    }

    /**
     * <p>
     * Setter method for issueDateTo.
     * </p>
     * 
     * @param issueDateTo Set for issueDateTo
     */
    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    /**
     * <p>
     * Getter method for deliveryDateFrom.
     * </p>
     * 
     * @return the deliveryDateFrom
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * <p>
     * Setter method for deliveryDateFrom.
     * </p>
     * 
     * @param deliveryDateFrom Set for deliveryDateFrom
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * <p>
     * Getter method for deliveryDateTo.
     * </p>
     * 
     * @return the deliveryDateTo
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * <p>
     * Setter method for deliveryDateTo.
     * </p>
     * 
     * @param deliveryDateTo Set for deliveryDateTo
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }

    /**
     * <p>
     * Getter method for shipDateFrom.
     * </p>
     * 
     * @return the shipDateFrom
     */
    public String getShipDateFrom() {
        return shipDateFrom;
    }

    /**
     * <p>
     * Setter method for shipDateFrom.
     * </p>
     * 
     * @param shipDateFrom Set for shipDateFrom
     */
    public void setShipDateFrom(String shipDateFrom) {
        this.shipDateFrom = shipDateFrom;
    }

    /**
     * <p>
     * Getter method for shipDateTo.
     * </p>
     * 
     * @return the shipDateTo
     */
    public String getShipDateTo() {
        return shipDateTo;
    }

    /**
     * <p>
     * Setter method for shipDateTo.
     * </p>
     * 
     * @param shipDateTo Set for shipDateTo
     */
    public void setShipDateTo(String shipDateTo) {
        this.shipDateTo = shipDateTo;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>
     * Getter method for shipmentStatus.
     * </p>
     * 
     * @return the shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * <p>
     * Setter method for shipmentStatus.
     * </p>
     * 
     * @param shipmentStatus Set for shipmentStatus
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * <p>
     * Getter method for transportMode.
     * </p>
     * 
     * @return the transportMode
     */
    public String getTransportMode() {
        return transportMode;
    }

    /**
     * <p>
     * Setter method for transportMode.
     * </p>
     * 
     * @param transportMode Set for transportMode
     */
    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    /**
     * <p>
     * Getter method for routeNo.
     * </p>
     * 
     * @return the routeNo
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * <p>
     * Setter method for routeNo.
     * </p>
     * 
     * @param routeNo Set for routeNo
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * <p>
     * Getter method for del.
     * </p>
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * <p>
     * Setter method for del.
     * </p>
     * 
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * <p>
     * Getter method for spsDoNo.
     * </p>
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>
     * Setter method for spsDoNo.
     * </p>
     * 
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>
     * Getter method for cigmaDoNo.
     * </p>
     * 
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>
     * Setter method for cigmaDoNo.
     * </p>
     * 
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>
     * Getter method for pdfFileId.
     * </p>
     * 
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>
     * Setter method for pdfFileId.
     * </p>
     * 
     * @param pdfFileId Set for pdfFileId
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * <p>
     * Getter method for pdfFileName.
     * </p>
     * 
     * @return the pdfFileName
     */
    public String getPdfFileName() {
        return pdfFileName;
    }

    /**
     * <p>
     * Setter method for pdfFileName.
     * </p>
     * 
     * @param pdfFileName Set for pdfFileName
     */
    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }
    
    /**
     * <p>
     * Getter method for miscRevision.
     * </p>
     * 
     * @return the miscRevision
     */
    public List<MiscellaneousDomain> getMiscRevision() {
        return miscRevision;
    }

    /**
     * <p>
     * Setter method for miscRevision.
     * </p>
     * 
     * @param miscRevision Set for miscRevision
     */
    public void setMiscRevision(List<MiscellaneousDomain> miscRevision) {
        this.miscRevision = miscRevision;
    }

    /**
     * <p>
     * Getter method for miscShipmentStatusCb.
     * </p>
     * 
     * @return the miscShipmentStatusCb
     */
    public List<MiscellaneousDomain> getMiscShipmentStatusCb() {
        return miscShipmentStatusCb;
    }

    /**
     * <p>
     * Setter method for miscShipmentStatusCb.
     * </p>
     * 
     * @param miscShipmentStatusCb Set for miscShipmentStatusCb
     */
    public void setMiscShipmentStatusCb(List<MiscellaneousDomain> miscShipmentStatusCb) {
        this.miscShipmentStatusCb = miscShipmentStatusCb;
    }

    /**
     * <p>
     * Getter method for transportModeCb.
     * </p>
     * 
     * @return the transportModeCb
     */
    public List<MiscellaneousDomain> getTransportModeCb() {
        return transportModeCb;
    }

    /**
     * <p>
     * Setter method for transportModeCb.
     * </p>
     * 
     * @param transportModeCb Set for transportModeCb
     */
    public void setTransportModeCb(List<MiscellaneousDomain> transportModeCb) {
        this.transportModeCb = transportModeCb;
    }

    /**
     * <p>
     * Getter method for errorMessageList.
     * </p>
     * 
     * @return the errorMessageList
     */
    public List<ApplicationMessageDomain> getErrorMessageList() {
        return errorMessageList;
    }

    /**
     * <p>
     * Setter method for errorMessageList.
     * </p>
     * 
     * @param errorMessageList Set for errorMessageList
     */
    public void setErrorMessageList(
        List<ApplicationMessageDomain> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    /**
     * Gets the order method.
     * 
     * @return the order method
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Sets the order method.
     * 
     * @param orderMethod the order method
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }
    
    /**
     * Gets the denso part no.
     * 
     * @return the denso part no
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Sets the denso part no.
     * 
     * @param dPn the denso part no
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Gets the denso part no.
     * 
     * @return the denso part no
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Sets the supplier part no.
     * 
     * @param sPn the supplier part no
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Gets the unit of measure.
     * 
     * @return the unit of measure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the unit of measure.
     * 
     * @param unitOfMeasure the unit of measure
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * <p>Getter method for deliveryTimeFrom.</p>
     *
     * @return the deliveryTimeFrom
     */
    public String getDeliveryTimeFrom() {
        return deliveryTimeFrom;
    }

    /**
     * <p>Setter method for deliveryTimeFrom.</p>
     *
     * @param deliveryTimeFrom Set for deliveryTimeFrom
     */
    public void setDeliveryTimeFrom(String deliveryTimeFrom) {
        this.deliveryTimeFrom = deliveryTimeFrom;
    }

    /**
     * <p>Getter method for deliveryTimeTo.</p>
     *
     * @return the deliveryTimeTo
     */
    public String getDeliveryTimeTo() {
        return deliveryTimeTo;
    }

    /**
     * <p>Setter method for deliveryTimeTo.</p>
     *
     * @param deliveryTimeTo Set for deliveryTimeTo
     */
    public void setDeliveryTimeTo(String deliveryTimeTo) {
        this.deliveryTimeTo = deliveryTimeTo;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    
    /**
     * Gets the doId.
     * 
     * @param doId
     * @return the doId.
     */
    public String getDoId() {
        return doId;
    }

    /**
     * Sets doId
     * 
     * @param doId the doId.
     */

    public void setDoId(String doId) {
        this.doId = doId;
    }
    
}