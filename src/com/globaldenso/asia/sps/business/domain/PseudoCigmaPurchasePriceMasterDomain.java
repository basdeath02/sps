/*
 * ModifyDate Development company     Describe 
 * 2014/08/19  CSI Karnrawee          Create
 * 2015/10/21  CSI Akat               [IN030]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Pseudo PO Domain for receive value from Web Service
 * 
 * @author CSI Karnrawee
 * @version 1.0.0
 * */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CigmaPurchasePriceMasterDomain")
public class PseudoCigmaPurchasePriceMasterDomain extends PseudoBaseDomain implements Serializable {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = -8310933603648144656L;

    /** The pseudo of part no. */
    @XmlElement(name = "partNo")
    private String pseudoPartNo;
    
    /** The pseudo of supplier cd. */
    @XmlElement(name = "sCd")
    private String pseudoSCd;
    
    /** The pseudo of replicate. */
    @XmlElement(name = "priceClass")
    private String pseudoPriceClass;
    
    /** The pseudo of sps asn no. */
    @XmlElement(name = "sPn")
    private String pseudoSPn;

    /** The pseudo of asn status. */
    @XmlElement(name = "dateLastMaintained")
    private String pseudoDateLastMaintained;

    /** The pseudo of CIGMA current D/O no. */
    @XmlElement(name = "unitPrice")
    private String pseudoUnitPrice;

    /** The pseudo of CIGMA original D/O no. */
    @XmlElement(name = "currencyCd")
    private String pseudoCurrencyCd;
    
    /** The pseudo of Tempolary Price Flag. */
    @XmlElement(name = "tempPriceFlag")
    private String pseudoTempPriceFlag;
    
    // [IN030] for check effective date
    /** The pseudo of Effective Date. */
    @XmlElement(name = "effectiveDate")
    private String pseudoEffectiveDate;
    
    /**
     * The default constructor.
     * */
    public PseudoCigmaPurchasePriceMasterDomain() {
    }

    /**
     * <p>Getter method for pseudoPartNo.</p>
     *
     * @return the pseudoPartNo
     */
    public String getPseudoPartNo() {
        return pseudoPartNo;
    }

    /**
     * <p>Setter method for pseudoPartNo.</p>
     *
     * @param pseudoPartNo Set for pseudoPartNo
     */
    public void setPseudoPartNo(String pseudoPartNo) {
        this.pseudoPartNo = pseudoPartNo;
    }

    /**
     * <p>Getter method for pseudoSupplierCd.</p>
     *
     * @return the pseudoSCd
     */
    public String getPseudoSCd() {
        return pseudoSCd;
    }

    /**
     * <p>Setter method for pseudoSupplierCd.</p>
     *
     * @param pseudoSCd Set for pseudoSCd
     */
    public void setPseudoSCd(String pseudoSCd) {
        this.pseudoSCd = pseudoSCd;
    }

    /**
     * <p>Getter method for pseudoPriceClass.</p>
     *
     * @return the pseudoPriceClass
     */
    public String getPseudoPriceClass() {
        return pseudoPriceClass;
    }

    /**
     * <p>Setter method for pseudoPriceClass.</p>
     *
     * @param pseudoPriceClass Set for pseudoPriceClass
     */
    public void setPseudoPriceClass(String pseudoPriceClass) {
        this.pseudoPriceClass = pseudoPriceClass;
    }

    /**
     * <p>Getter method for pseudoSupplierPartNo.</p>
     *
     * @return the pseudoSPn
     */
    public String getPseudoSPn() {
        return pseudoSPn;
    }

    /**
     * <p>Setter method for pseudoSupplierPartNo.</p>
     *
     * @param pseudoSPn Set for pseudoSPn
     */
    public void setPseudoSPn(String pseudoSPn) {
        this.pseudoSPn = pseudoSPn;
    }

    /**
     * <p>Getter method for pseudoDateLastMaintained.</p>
     *
     * @return the pseudoDateLastMaintained
     */
    public String getPseudoDateLastMaintained() {
        return pseudoDateLastMaintained;
    }

    /**
     * <p>Setter method for pseudoDateLastMaintained.</p>
     *
     * @param pseudoDateLastMaintained Set for pseudoDateLastMaintained
     */
    public void setPseudoDateLastMaintained(String pseudoDateLastMaintained) {
        this.pseudoDateLastMaintained = pseudoDateLastMaintained;
    }

    /**
     * <p>Getter method for pseudoUnitPrice.</p>
     *
     * @return the pseudoUnitPrice
     */
    public String getPseudoUnitPrice() {
        return pseudoUnitPrice;
    }

    /**
     * <p>Setter method for pseudoUnitPrice.</p>
     *
     * @param pseudoUnitPrice Set for pseudoUnitPrice
     */
    public void setPseudoUnitPrice(String pseudoUnitPrice) {
        this.pseudoUnitPrice = pseudoUnitPrice;
    }

    /**
     * <p>Getter method for pseudoCurrencyCd.</p>
     *
     * @return the pseudoCurrencyCd
     */
    public String getPseudoCurrencyCd() {
        return pseudoCurrencyCd;
    }

    /**
     * <p>Setter method for pseudoCurrencyCd.</p>
     *
     * @param pseudoCurrencyCd Set for pseudoCurrencyCd
     */
    public void setPseudoCurrencyCd(String pseudoCurrencyCd) {
        this.pseudoCurrencyCd = pseudoCurrencyCd;
    }

    /**
     * <p>Getter method for pseudoTempPriceFlag.</p>
     *
     * @return the pseudoTempPriceFlag
     */
    public String getPseudoTempPriceFlag() {
        return pseudoTempPriceFlag;
    }

    /**
     * <p>Setter method for pseudoTempPriceFlag.</p>
     *
     * @param pseudoTempPriceFlag Set for pseudoTempPriceFlag
     */
    public void setPseudoTempPriceFlag(String pseudoTempPriceFlag) {
        this.pseudoTempPriceFlag = pseudoTempPriceFlag;
    }

    /**
     * <p>Getter method for pseudoEffectiveDate.</p>
     *
     * @return the pseudoEffectiveDate
     */
    public String getPseudoEffectiveDate() {
        return pseudoEffectiveDate;
    }

    /**
     * <p>Setter method for pseudoEffectiveDate.</p>
     *
     * @param pseudoEffectiveDate Set for pseudoEffectiveDate
     */
    public void setPseudoEffectiveDate(String pseudoEffectiveDate) {
        this.pseudoEffectiveDate = pseudoEffectiveDate;
    }

}
