/*
 * ModifyDate Development company     Describe 
 * 2014/04/23 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class User Domain.
 * @author CSI
 */
public class UserDomain extends BaseDomain implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -126749241277689378L;
    
    /** The DSC ID. */
    private String dscId;
       
    /** The first name */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
       
    /** The department name. */
    private String departmentName; 
       
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The Active flag. */
    private String isActive; 
    
    /** The Create User. */
    private String createUser;
            
    /** The Date time create. */
    private Timestamp createDatetime;
    
    /** The Last update User. */
    private String updateUser;
            
    /** The Date time update. */
    private Timestamp updateDatetime;
    
    /** The target page. */
    private String targetPage; 
    
    /** The Row Number From. */
    private int rowNumFrom = 0;
    
    /** The Row Number To. */
    private int rowNumTo = 0;
    
    
    /**
     * Instantiates a new user domain.
     */
    public UserDomain() {
        super();       
        setTargetPage(Constants.PAGE_FIRST);
    }
    
    /**
     * Gets the target page.
     * 
     * @return the target page
     */
    public String getTargetPage() {
        return targetPage;
    }
    
    /**
     * Sets the target page.
     * 
     * @param targetPage the target page.
     */  
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    /**
     * Gets the DSCID
     * 
     * @return the DSCID
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Sets the supplier DSCID
     * 
     * @param dscId the supplier plant code
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    
    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Gets the department Name.
     * 
     * @return the department Name.
     */
    public String getDepartmentName() {
        return departmentName;
    }
    /**
     * Sets the department Name.
     * 
     * @param departmentName the department Name.
     */

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets the is Active flag.
     * 
     * @return the isActive flag.
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the is Active flag.
     * 
     * @param isActive the is Active flag.
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Gets the create User.
     * 
     * @return the createUser.
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Sets the create User.
     * 
     * @param createUser the create User.
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the create Date time.
     * 
     * @return the create Date time.
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Sets the create Date time.
     * 
     * @param createDatetime the create Date time.
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Gets the update User.
     * 
     * @return the updateUser.
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * Sets the update User.
     * 
     * @param updateUser the update User.
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * Gets the update Date time.
     * 
     * @return the updateDatetime.
     */
    public Timestamp getUpdateDatetime() {
        return updateDatetime;
    }

    /**
     * Sets the update Date time.
     * 
     * @param updateDatetime the update Date time.
     */
    public void setUpdateDatetime(Timestamp updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    /**
     * <p>Getter method for Row Number From.</p>
     *
     * @return the Row Number From
     */
    public int getRowNumFrom() {
        return rowNumFrom;
    }

    /**
     * <p>Setter method for Row Number From.</p>
     *
     * @param rowNumFrom Set for Row Number From
     */
    public void setRowNumFrom(int rowNumFrom) {
        this.rowNumFrom = rowNumFrom;
    }

    /**
     * <p>Getter method for Row Number To.</p>
     *
     * @return the Row Number To
     */
    public int getRowNumTo() {
        return rowNumTo;
    }

    /**
     * <p>Setter method for Row Number To.</p>
     *
     * @param rowNumTo Set for Row Number To
     */
    public void setRowNumTo(int rowNumTo) {
        this.rowNumTo = rowNumTo;
    }
    
}