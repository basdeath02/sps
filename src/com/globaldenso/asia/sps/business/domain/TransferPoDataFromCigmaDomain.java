package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * 
 * <p>TransferPoDataFromCigmaDomain class.</p>
 *
 * @author CSI
 */
public class TransferPoDataFromCigmaDomain extends BaseDomain implements Serializable {
    /** Default Serial Version ID. */
    private static final long serialVersionUID = 3147991534259734705L;

    /** poType */
    private String poType;
    /** companyName */
    private String companyName;
    /** supplierCode */
    private String vendorCd;
    /** supplierName */
    private String supplierName;
    /** cigmaPoNo */
    private String cigmaPoNo;
    /** issueDate */
    private Date issueDate;
    /** dueDateFrom */
    private Date dueDateFrom;
    /** dueDateTo */
    private Date dueDateTo;
    /** releaseNo */
    private BigDecimal releaseNo;
    /** dPlantCode */
    private String dPcd;
    /** tr */
    private String tr;
    /** forceAckDate */
    private Date forceAckDate;
    /** tm */
    private String tm;
    /** addTruckRouteFlag */
    private String addTruckRouteFlag;
    /** blanketPoNo */
    private String blanketPoNo;
    /** Cigma Po Detail List */
    private List<TransferPoDetailDataFromCigmaDomain> cigmaPoDetailList;
    
    /** Supplier Plant Code(SPS) */
    private String sPcd;
    
    /** P/O Id(SPS) */
    private BigDecimal poId;
    
    /** Supplier Code(SPS) */
    private String sCd;
    /** Maximium Create Date Time */
    private String maxCreateDateTime;
    /** Maximium Update Date Time */
    private String maxUpdateDateTime;
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public TransferPoDataFromCigmaDomain() {
        super();
    }
    /**
     * <p>Getter method for potype.</p>
     *
     * @return the potype
     */
    public String getPoType() {
        return poType;
    }
    /**
     * <p>Setter method for potype.</p>
     *
     * @param poType Set for potype
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }
    /**
     * <p>Getter method for companyname.</p>
     *
     * @return the companyname
     */
    public String getCompanyName() {
        return companyName;
    }
    /**
     * <p>Setter method for companyname.</p>
     *
     * @param companyName Set for companyname
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    /**
     * <p>Getter method for suppliercode.</p>
     *
     * @return the suppliercode
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * <p>Setter method for suppliercode.</p>
     *
     * @param vendorCd Set for suppliercode
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    /**
     * <p>Getter method for suppliername.</p>
     *
     * @return the suppliername
     */
    public String getSupplierName() {
        return supplierName;
    }
    /**
     * <p>Setter method for suppliername.</p>
     *
     * @param supplierName Set for suppliername
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    /**
     * <p>Getter method for cigmapono.</p>
     *
     * @return the cigmapono
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * <p>Setter method for cigmapono.</p>
     *
     * @param cigmaPoNo Set for cigmapono
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }
    /**
     * <p>Getter method for issuedate.</p>
     *
     * @return the issuedate
     */
    public Date getIssueDate() {
        return issueDate;
    }
    /**
     * <p>Setter method for issuedate.</p>
     *
     * @param issueDate Set for issuedate
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
    /**
     * <p>Getter method for duedatefrom.</p>
     *
     * @return the duedatefrom
     */
    public Date getDueDateFrom() {
        return dueDateFrom;
    }
    /**
     * <p>Setter method for duedatefrom.</p>
     *
     * @param dueDateFrom Set for duedatefrom
     */
    public void setDueDateFrom(Date dueDateFrom) {
        this.dueDateFrom = dueDateFrom;
    }
    /**
     * <p>Getter method for duedateto.</p>
     *
     * @return the duedateto
     */
    public Date getDueDateTo() {
        return dueDateTo;
    }
    /**
     * <p>Setter method for duedateto.</p>
     *
     * @param dueDateTo Set for duedateto
     */
    public void setDueDateTo(Date dueDateTo) {
        this.dueDateTo = dueDateTo;
    }
    /**
     * <p>Getter method for releaseno.</p>
     *
     * @return the releaseno
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }
    /**
     * <p>Setter method for releaseno.</p>
     *
     * @param releaseNo Set for releaseno
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }
    /**
     * <p>Getter method for dplantcode.</p>
     *
     * @return the dplantcode
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for dplantcode.</p>
     *
     * @param dPcd Set for dplantcode
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * <p>Getter method for tr.</p>
     *
     * @return the tr
     */
    public String getTr() {
        return tr;
    }
    /**
     * <p>Setter method for tr.</p>
     *
     * @param tr Set for tr
     */
    public void setTr(String tr) {
        this.tr = tr;
    }
    /**
     * <p>Getter method for forceackdate.</p>
     *
     * @return the forceackdate
     */
    public Date getForceAckDate() {
        return forceAckDate;
    }
    /**
     * <p>Setter method for forceackdate.</p>
     *
     * @param forceAckDate Set for forceackdate
     */
    public void setForceAckDate(Date forceAckDate) {
        this.forceAckDate = forceAckDate;
    }
    /**
     * <p>Getter method for tm.</p>
     *
     * @return the tm
     */
    public String getTm() {
        return tm;
    }
    /**
     * <p>Setter method for tm.</p>
     *
     * @param tm Set for tm
     */
    public void setTm(String tm) {
        this.tm = tm;
    }
    /**
     * <p>Getter method for addtruckrouteflag.</p>
     *
     * @return the addtruckrouteflag
     */
    public String getAddTruckRouteFlag() {
        return addTruckRouteFlag;
    }
    /**
     * <p>Setter method for addtruckrouteflag.</p>
     *
     * @param addTruckRouteFlag Set for addtruckrouteflag
     */
    public void setAddTruckRouteFlag(String addTruckRouteFlag) {
        this.addTruckRouteFlag = addTruckRouteFlag;
    }
    /**
     * <p>Getter method for blanketpono.</p>
     *
     * @return the blanketpono
     */
    public String getBlanketPoNo() {
        return blanketPoNo;
    }
    /**
     * <p>Setter method for blanketpono.</p>
     *
     * @param blanketPoNo Set for blanketpono
     */
    public void setBlanketPoNo(String blanketPoNo) {
        this.blanketPoNo = blanketPoNo;
    }
    /**
     * <p>Getter method for cigmaPoDetail.</p>
     *
     * @return the cigmaPoDetail
     */
    public List<TransferPoDetailDataFromCigmaDomain> getCigmaPoDetailList() {
        return cigmaPoDetailList;
    }
    /**
     * <p>Setter method for cigmaPoDetail.</p>
     *
     * @param cigmaPoDetailList Set for cigmaPoDetail
     */
    public void setCigmaPoDetailList(
        List<TransferPoDetailDataFromCigmaDomain> cigmaPoDetailList) {
        this.cigmaPoDetailList = cigmaPoDetailList;
    }
    /**
     * <p>Getter method for supplierPlantCode.</p>
     *
     * @return the supplierPlantCode
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplierPlantCode.</p>
     *
     * @param sPcd Set for supplierPlantCode
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }
    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }
    /**
     * <p>Getter method for spsSupplierCode.</p>
     *
     * @return the spsSupplierCode
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * <p>Setter method for spsSupplierCode.</p>
     *
     * @param sCd Set for spsSupplierCode
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * <p>Getter method for maxCreateDateTime.</p>
     *
     * @return the maxCreateDateTime
     */
    public String getMaxCreateDateTime() {
        return maxCreateDateTime;
    }
    /**
     * <p>Setter method for maxCreateDateTime.</p>
     *
     * @param maxCreateDateTime Set for maxCreateDateTime
     */
    public void setMaxCreateDateTime(String maxCreateDateTime) {
        this.maxCreateDateTime = maxCreateDateTime;
    }
    /**
     * <p>Getter method for maxUpdateDateTime.</p>
     *
     * @return the maxUpdateDateTime
     */
    public String getMaxUpdateDateTime() {
        return maxUpdateDateTime;
    }
    /**
     * <p>Setter method for maxUpdateDateTime.</p>
     *
     * @param maxUpdateDateTime Set for maxUpdateDateTime
     */
    public void setMaxUpdateDateTime(String maxUpdateDateTime) {
        this.maxUpdateDateTime = maxUpdateDateTime;
    }
}
