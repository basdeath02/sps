/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.business.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SendingAsnDomain.
 * @author CSI
 */
public class SendingAsnDomain extends BaseDomain implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1698145488430769485L;
    
    /** The company denso code. */
    private String dCd;
    
    /** The asn information list. */
    private List<AsnInfoDomain> asnInformationList;

    /**
     * Instantiates a new Sending ASN domain.
     */
    public SendingAsnDomain() {
        super();
    }

    /**
     * Gets the denso company.
     * 
     * @return the denso company.
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso company.
     * 
     * @param dCd the denso company.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the asn information list.
     * 
     * @return the asn information list
     */
    public List<AsnInfoDomain> getAsnInformationList() {
        return asnInformationList;
    }

    /**
     * Sets the asn information list.
     * 
     * @param asnInformationList the asn information list
     */
    public void setAsnInformationList(List<AsnInfoDomain> asnInformationList) {
        this.asnInformationList = asnInformationList;
    }
}