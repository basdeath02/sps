package com.globaldenso.asia.sps.ws.rest.resource;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.webservicecallrest.WebServiceCallerRest;
import com.globaldenso.ai.library.webservicecallrest.domain.WebServiceCallerRestDomain;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.asia.sps.common.webserviceutil.WebServiceConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * <p>Type in the functional overview of the class.</p>
 *
 * @author XXXXXXXXXXX
 * @version 1.00
 */
public class test {
    
    /**
     * <p>Type in the functional overview of the constructor.</p>
     *
     */
    public test(){
    }
    /**
     * <p>Type in the functional overview of the method.</p>
     *
     * @param args args
     * @throws ApplicationException ApplicationException
     */
    public static void main(String[] args) throws ApplicationException {
        WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();
        StringBuffer url = new StringBuffer();
//        url.append("http://10.74.201.184:8091/SPS/rest/");
//        url.append("http://10.74.201.202:8092/SPS2/rest/");
        url.append("http://localhost:8086/SPS/rest/");
        url.append(WebServiceConstants.SPS_RECEIVE);
        domain.setUrl(url.toString());
        domain.setUserName("user1");
        domain.setPassword("password1");
        domain.setParamMap(WebServiceConstants.PARAM_ASN_NO,
            "C7Z23E1M01C07,C7Z23E1M01C07,C7Z23E1M01C07,C7Z23E1M01C07,C7Z23E1M02C07,C7Z23E1M02C07,C7Z23E1M03C07,C7Z23E1M03C07,C7Z23E1M03C07");
        domain.setParamMap(WebServiceConstants.PARAM_RCV_LANE,
            "PEM,PEM,PEM,PEM,PEM,PEM,PEM,PEM,PEM");
        domain.setParamMap(WebServiceConstants.PARAM_DENSO_COMPANY_CODE,
//            "DNTH,DNTH,DNTH,DNTH,DNTH,DNTH,DNTH,DNTH,DNTH");
            "DNTH,DNTH,DNTH,DNTH,DNTH,DNTH,DNTH,DNTH,DNTH");
        domain.setParamMap(WebServiceConstants.PARAM_CIGMA_DO_NO,
            "71002046,71002046,71002046,71002046,71002046,71002046,71002046,71002046,71002046");
        domain.setParamMap(WebServiceConstants.PARAM_DENSO_PART_CODE,
            "TGY241001,TGY241001,TGY241001,TGY241001,TGY241001,TGY241001,TGY241001,TGY241001,TGY241001");
        domain.setParamMap(WebServiceConstants.PARAM_KANBAN_TAG_SEQ,
            "9000007,9000005,9000006,9000003,9000003,9000001,9000001,9000002,9000004");
        domain.setParamMap(WebServiceConstants.PARAM_SUPPLIER_CODE,
            "C07,C07,C07,C07,C07,C07,C07,C07,C07");
        domain.setParamMap(WebServiceConstants.PARAM_DENSO_PLANT_CODE,
            "E1,E1,E1,E1,E1,E1,E1,E1,E1");
        domain.setParamMap(WebServiceConstants.PARAM_WH,
            "1,1,1,1,1,1,1,1,1");
        domain.setParamMap(WebServiceConstants.PARAM_KANBAN_QTY,
            "500,500,500,500,500,500,500,500,500");
        domain.setParamMap(WebServiceConstants.PARAM_ALLOCATE_QTY,
            "500,500,500,254.49,245.51,354.49,145.51,500,500");
        domain.setParamMap(WebServiceConstants.PARAM_KANBAN_TYPE,
            "02,02,02,02,02,02,02,02,02");
        domain.setParamMap(WebServiceConstants.PARAM_ASN_STATUS,
            "PTS,PTS,PTS,PTS,PTS,PTS,PTS,PTS,PTS");
        domain.setParamMap(WebServiceConstants.PARAM_TAG_SCAN_DATETIME,
            "201712231512,201712231512,201712231512,201712231513,201712231513,201712231513,201712231513,201712231513,201712231513");
        domain.setParamMap(WebServiceConstants.PARAM_RECEIVE_DATE,
            "20170918000000,20170918000000,20170918000000,20170918000000,20170918000000,20170918000000,20170918000000,20170918000000,20170918000000");
        domain.setParamMap(WebServiceConstants.PARAM_DSC_ID,
            "40061600016088880098,40061600016088880098,40061600016088880098,40061600016088880098,40061600016088880098,40061600016088880098,40061600016088880098,40061600016088880098,40061600016088880098");
        domain.setParamMap(WebServiceConstants.PARAM_DEVICE_ID,
            "01,01,01,01,01,01,01,01,01");
        domain.setParamMap(WebServiceConstants.PARAM_ASN_SCAN_DATETIME,
            "201712231511,201712231511,201712231511,201712231511,201712231512,201712231512,201712231512,201712231512,201712231512");
        
        String webServiceResult = null;
        try {
            webServiceResult
                = WebServiceCallerRest.post(domain, new GenericType<String>(){});
        } catch(WebServiceCallerRestException e) {
            //throw new ApplicationException(e.getMessage()); 
        }
        
        //SPS Upgrade
//        WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();
//        StringBuffer url = new StringBuffer();
//        url.append("http://10.74.201.182:8091/SPS/rest/");
//        url.append(WebServiceConstants.SPS_UPGRADE);
//        domain.setUrl(url.toString());
//        domain.setUserName("user1");
//        domain.setPassword("password1");
//        domain.setParamMap(WebServiceConstants.PARAM_SYSTEM_ID,
//            "SPSBHT");
//        domain.setParamMap(WebServiceConstants.PARAM_VER_NO,
//            "0.0.0.0");
//        String webServiceResult = null;
//        try {
//            webServiceResult
//                = WebServiceCallerRest.get(domain, new GenericType<String>(){});
//        }catch(WebServiceCallerRestException e){
//            throw new ApplicationException(e.getMessage()); 
//        }

        System.out.println(webServiceResult);
        
        
    }
}
