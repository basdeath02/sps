/*
 * ModifyDate Development company     Describe 
 * 2017/09/26 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.ws.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.globaldenso.ai.aiws.exception.AiwsApplicationException;
import com.globaldenso.asia.sps.common.webserviceutil.WebServiceConstants;

/**
 * <p>The Interface ApInterfaceResource.</p>
 * <p>Resource for Web Service AP Interface.</p>
 * <ul>
 * <li>Method get : search Bht Upgrade.</li>
 * </ul>
 *
 * @author Netband
 */
@Path(WebServiceConstants.SPS_UPGRADE)
public interface UpgradeResource {
    /**
     * create receive upgrade software via BHT scanning Information.
     * 
     * @param systemId System ID.
     * @param verNo Version No.
     * @return String
     * @throws AiwsApplicationException Exception for AIWS caller
     * */
    @GET
    @Produces(MediaType.APPLICATION_JSON + WebServiceConstants.TYPE_CHRSET_UTF8)
    public String searchBhtUpgrade(
        @QueryParam(WebServiceConstants.PARAM_SYSTEM_ID) String systemId,
        @QueryParam(WebServiceConstants.PARAM_VER_NO) String verNo)
        throws AiwsApplicationException;
}
