/*
 * ModifyDate Development company     Describe 
 * 2017/08/15 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.ws.rest.resource;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.aiws.exception.AiwsApplicationException;
import com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain;
import com.globaldenso.asia.sps.business.service.ReceiveResourceFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * <p>The Class CigmaDoResourceImpl.</p>
 * <p>Resource for Web Service AP Interface.</p>
 * <ul>
 * <li>Method insert : receive by scan.</li>
 * </ul>
 *
 * @author Netband
 */
public class ReceiveResourceImpl implements ReceiveResource {

    /**
     * Log from apache commen.
     * */
    //1.1       keep request log by write argument value to log file
    private static Log log = LogFactory.getLog(ReceiveResource.class);
    
   /** ReceiveResourceFacadeService */
    ReceiveResourceFacadeService receiveResourceFacadeService;
    
    /** The default constructor. */
    public ReceiveResourceImpl() {
        super();
    }
    /**
     * <p>Setter method for ReceiveResourceService.</p>
     *
     * @param receiveResourceFacadeService Set for receiveResourceFacadeService
     */
    public void setReceiveResourceFacadeService(ReceiveResourceFacadeService receiveResourceFacadeService) {
        this.receiveResourceFacadeService = receiveResourceFacadeService;
    }
    /** {@inheritDoc}
     * @throws AiwsApplicationException 
     * @see com.globaldenso.asia.sps.ws.rest.resource.ReceiveResource#createReceiveByScan(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String)
     */
    public String createReceiveByScan(String asnNo, 
        String rcvLane,
        String densoCompanyCode, 
        String cigmaDoNo, 
        String dPn,
        String kanbanTagSeq, 
        String supplierCode, 
        String densoPlantCode,
        String wh, 
        String kanbanQty, 
        String allocateQty, 
        String kanbanType,
        String asnStatus, 
        String tagScanDateTime, 
        String receiveDate,
        String dscId, 
        String deviceId, 
        String asnScanDateTime) throws AiwsApplicationException {
        String resultCode = RECEIVE_BY_SCAN_SERVICE_SUCCESS;
        
        try {  
            //1.2       validate value of argument have to submit.
            if(Constants.EMPTY_STRING.equals(asnNo)
                || Constants.EMPTY_STRING.equals(densoCompanyCode)
                || Constants.EMPTY_STRING.equals(cigmaDoNo)
                || Constants.EMPTY_STRING.equals(kanbanTagSeq)
                || Constants.EMPTY_STRING.equals(supplierCode)
                || Constants.EMPTY_STRING.equals(densoPlantCode)
                || Constants.EMPTY_STRING.equals(wh)
                || Constants.EMPTY_STRING.equals(kanbanQty)
                || Constants.EMPTY_STRING.equals(allocateQty)
                || Constants.EMPTY_STRING.equals(kanbanType)
                || Constants.EMPTY_STRING.equals(asnStatus)
                || Constants.EMPTY_STRING.equals(tagScanDateTime)
                || Constants.EMPTY_STRING.equals(receiveDate)
                || Constants.EMPTY_STRING.equals(dscId)
                || Constants.EMPTY_STRING.equals(deviceId)
                || Constants.EMPTY_STRING.equals(asnScanDateTime)){
                log.error(RECEIVE_BY_SCAN_INVALID_PARAMETER);
                resultCode = RECEIVE_BY_SCAN_INVALID_PARAMETER;
                return resultCode;
            }
            
            //2   convert each argument to ReceiveByScanDomain
            //2.1 split contents of each arguments  
            String[] asnNoSplit = asnNo.split(Constants.SYMBOL_COMMA);
            String[] rcvLaneSplit = rcvLane.split(Constants.SYMBOL_COMMA);
            String[] densoCompanyCodeSplit = densoCompanyCode.split(Constants.SYMBOL_COMMA);
            String[] cigmaDoNoSplit = cigmaDoNo.split(Constants.SYMBOL_COMMA);
            String[] dPnSplit = dPn.split(Constants.SYMBOL_COMMA);
            String[] kanbanTagSeqSplit = kanbanTagSeq.split(Constants.SYMBOL_COMMA);
            String[] supplierCodeSplit = supplierCode.split(Constants.SYMBOL_COMMA);
            String[] densoPlantCodeSplit = densoPlantCode.split(Constants.SYMBOL_COMMA);
            String[] whSplit = wh.split(Constants.SYMBOL_COMMA);
            String[] kanbanQtySplit = kanbanQty.split(Constants.SYMBOL_COMMA);
            String[] allocateQtySplit = allocateQty.split(Constants.SYMBOL_COMMA);
            String[] kanbanTypeSplit = kanbanType.split(Constants.SYMBOL_COMMA);
            String[] asnStatusSplit = asnStatus.split(Constants.SYMBOL_COMMA);
            String[] tagScanDateTimeSplit = tagScanDateTime.split(Constants.SYMBOL_COMMA);
            String[] receiveDateSplit = receiveDate.split(Constants.SYMBOL_COMMA);
            String[] dscIdSplit = dscId.split(Constants.SYMBOL_COMMA);
            String[] deviceIdSplit = deviceId.split(Constants.SYMBOL_COMMA);
            String[] asnScanDateTimeSplit = asnScanDateTime.split(Constants.SYMBOL_COMMA);
            //2.2    Validate length from array from 2.1
            /*Start to check amount the information that transfer via BHT*/
            int informationSize =  asnNoSplit.length;
            if(informationSize != asnNoSplit.length
                || informationSize != rcvLaneSplit.length
                || informationSize != densoCompanyCodeSplit.length
                || informationSize != cigmaDoNoSplit.length
                || informationSize != dPnSplit.length
                || informationSize != kanbanTagSeqSplit.length
                || informationSize != supplierCodeSplit.length
                || informationSize != densoPlantCodeSplit.length
                || informationSize != whSplit.length
                || informationSize != kanbanQtySplit.length
                || informationSize != allocateQtySplit.length
                || informationSize != kanbanTypeSplit.length
                || informationSize != asnStatusSplit.length
                || informationSize != tagScanDateTimeSplit.length
                || informationSize != receiveDateSplit.length
                || informationSize != dscIdSplit.length
                || informationSize != deviceIdSplit.length
                || informationSize != asnScanDateTimeSplit.length){
                log.error(RECEIVE_BY_SCAN_INVALID_PARAMETER);
                resultCode = RECEIVE_BY_SCAN_INVALID_PARAMETER;
                return resultCode;
            }
            //2.3   create List<ReceiveByScanDomain> by reading each index from 2.1 and convert to domain
            List<ReceiveByScanDomain> receiveByScanDomainList = new ArrayList<ReceiveByScanDomain>();
            ReceiveByScanDomain receiveByScanDomain = null;
            for(Integer i = 0 ; i < informationSize ; i++){
                receiveByScanDomain = new ReceiveByScanDomain();
                receiveByScanDomain.setAsnNo(asnNoSplit[i]);
                receiveByScanDomain.setRcvLane(rcvLaneSplit[i]);
                receiveByScanDomain.setDensoCompanyCode(densoCompanyCodeSplit[i]);
                receiveByScanDomain.setCigmaDoNo(cigmaDoNoSplit[i]);
                receiveByScanDomain.setdPn(dPnSplit[i]);
                receiveByScanDomain.setSupplierCode(supplierCodeSplit[i]);
                receiveByScanDomain.setKanbanTagSeq(kanbanTagSeqSplit[i]);
                receiveByScanDomain.setDensoPlantCode(densoPlantCodeSplit[i]);
                receiveByScanDomain.setWh(whSplit[i]);
                receiveByScanDomain.setKanbanQty(kanbanQtySplit[i]);
                receiveByScanDomain.setAllocateQty(allocateQtySplit[i]);
                receiveByScanDomain.setKanbanType(kanbanTypeSplit[i]);
                receiveByScanDomain.setAsnStatus(asnStatusSplit[i].substring(Constants.ZERO, Constants.ONE));
                receiveByScanDomain.setTagScanDate(tagScanDateTimeSplit[i]);
                receiveByScanDomain.setReceiveDate(receiveDateSplit[i]);
                receiveByScanDomain.setDscId(dscIdSplit[i]);
                receiveByScanDomain.setDeviceId(deviceIdSplit[i]);
                receiveByScanDomain.setAsnScanDateTime(asnScanDateTimeSplit[i]);
                
                receiveByScanDomainList.add(receiveByScanDomain);
            }
            
            //3 Call ReceiveService createReceiveByScan()
            try{
                resultCode = receiveResourceFacadeService.createReceiveByScan(receiveByScanDomainList);
            } catch (Exception e){
                resultCode = RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
                log.error(resultCode);
            }
        } catch (Exception e) {
            resultCode = RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR;
            log.error(resultCode);
        }
        return resultCode;
    }
}