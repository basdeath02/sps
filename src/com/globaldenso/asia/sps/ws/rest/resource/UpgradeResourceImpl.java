/*
 * ModifyDate Development company     Describe 
 * 2017/09/26 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.ws.rest.resource;

import com.globaldenso.ai.aiws.exception.AiwsApplicationException;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.BhtUpgradeDomain;
import com.globaldenso.asia.sps.business.service.UpgradeResourceFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * <p>The Interface ApInterfaceResource.</p>
 * <p>Resource for Web Service AP Interface.</p>
 * <ul>
 * <li>Method get : search Bht Upgrade.</li>
 * </ul>
 *
 * @author Netband
 */
public class UpgradeResourceImpl implements UpgradeResource {
    
    /** The upgradeResourceFacadeService. */
    UpgradeResourceFacadeService upgradeResourceFacadeService;
    
    /** The default constructor. */
    public UpgradeResourceImpl() {
        super();
    }
    /**
     * Sets the upgradeResourceFacadeService.
     * 
     * @param upgradeResourceFacadeService the upgradeResourceFacadeService.
     */
    public void setUpgradeResourceFacadeService(UpgradeResourceFacadeService upgradeResourceFacadeService) {
        this.upgradeResourceFacadeService = upgradeResourceFacadeService;
    }
    /**
     * create receive upgrade software via BHT scanning Information.
     * 
     * @param systemId System ID.
     * @param verNo Version No.
     * @return bhtUpgradeDomain
     * @throws AiwsApplicationException Exception for AIWS caller
     * */
    public String searchBhtUpgrade(String systemId, String verNo)
        throws AiwsApplicationException {
        BhtUpgradeDomain bhtUpgradeDomain = new BhtUpgradeDomain();
        bhtUpgradeDomain.setSystemId(systemId);
        bhtUpgradeDomain.setVerNo(verNo);
        BhtUpgradeDomain bhtUpgradeDomainResult = null;
        StringBuffer strReturn = new StringBuffer();
        sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
        
        try {
            bhtUpgradeDomainResult = upgradeResourceFacadeService.searchBhtUpgrade(bhtUpgradeDomain);
        } catch (ApplicationException e) {
            e.printStackTrace();
        }
        if(bhtUpgradeDomainResult != null ){
            try{
                strReturn.append(bhtUpgradeDomainResult.getVerNo().toString())
                    .append(Constants.SYMBOL_COMMA)
                    .append(encoder.encode(bhtUpgradeDomainResult.getBinaryFile()).toString())
                    .append(Constants.SYMBOL_COMMA)
                    .append(encoder.encode(bhtUpgradeDomainResult.getUtilityFile()).toString());
            } catch (Exception e){
                strReturn = new StringBuffer();
                strReturn.append(Constants.STR_NINE);
            }
        } else {
            strReturn.append(Constants.EMPTY_STRING);
        }
        return strReturn.toString();
    }
   
}