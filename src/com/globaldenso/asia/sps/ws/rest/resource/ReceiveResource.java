/*
 * ModifyDate Development company     Describe 
 * 2017/08/15 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.ws.rest.resource;

//import java.util.List;
//
//import javax.ws.rs.FormParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.globaldenso.ai.aiws.exception.AiwsApplicationException;
import com.globaldenso.asia.sps.common.webserviceutil.WebServiceConstants;

/**
 * <p>The Interface ApInterfaceResource.</p>
 * <p>Resource for Web Service AP Interface.</p>
 * <ul>
 * <li>Method insert : receive by scan.</li>
 * </ul>
 *
 * @author Netband
 */
@Path(WebServiceConstants.SPS_RECEIVE)
public interface ReceiveResource {
    /**
     * RECEIVE_BY_SCAN_SERVICE_SUCCESS
     */
    public static String RECEIVE_BY_SCAN_SERVICE_SUCCESS = "00";
    /**
     * RECEIVE_BY_SCAN_SERVICE_CIGMA_BUSY
     */
    public static String RECEIVE_BY_SCAN_SERVICE_CIGMA_BUSY = "01";
    /**
     * RECEIVE_BY_SCAN_INVALID_PARAMETER
     */
    public static String RECEIVE_BY_SCAN_INVALID_PARAMETER = "02";
    /**
     * RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR
     */
    public static String RECEIVE_BY_SCAN_SERVICE_SYSTEM_ERROR = "03";
    /**
     * RECEIVE_BY_SCAN_RECEIVE_ERROR_FROM_CIGMA
     */
    public static String RECEIVE_BY_SCAN_RECEIVE_ERROR_FROM_CIGMA = "04";
    /**
     * <p>Resource for BHT call web service to SPS at transmit process.</p>
     *
     * @param asnNo asnNo
     * @param rcvLane rcvLane
     * @param densoCompanyCode densoCompanyCode
     * @param doNumber doNumber
     * @param dPn dPn
     * @param kanbanTagSeq kanbanTagSeq
     * @param supplierCode supplierCode
     * @param densoPlantCode densoPlantCode
     * @param wh wh
     * @param kanbanQty kanbanQty
     * @param allocateQty allocateQty
     * @param kanbanType kanbanType
     * @param asnStatus asnStatus
     * @param tagScanDateTime tagScanDateTime
     * @param receiveDate receiveDate
     * @param dscId dscId
     * @param deviceId deviceId
     * @param asnScanDateTime asnScanDateTime
     * @return String
     * @throws AiwsApplicationException AiwsApplicationException
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN + WebServiceConstants.TYPE_CHRSET_UTF8)
    public String createReceiveByScan(
        @FormParam(WebServiceConstants.PARAM_ASN_NO) String asnNo,
        @FormParam(WebServiceConstants.PARAM_RCV_LANE) String rcvLane,
        @FormParam(WebServiceConstants.PARAM_DENSO_COMPANY_CODE) String densoCompanyCode,
        @FormParam(WebServiceConstants.PARAM_CIGMA_DO_NO) String doNumber,
        @FormParam(WebServiceConstants.PARAM_DENSO_PART_CODE) String dPn,
        @FormParam(WebServiceConstants.PARAM_KANBAN_TAG_SEQ) String kanbanTagSeq,
        @FormParam(WebServiceConstants.PARAM_SUPPLIER_CODE) String supplierCode,
        @FormParam(WebServiceConstants.PARAM_DENSO_PLANT_CODE) String densoPlantCode,
        @FormParam(WebServiceConstants.PARAM_WH) String wh,
        @FormParam(WebServiceConstants.PARAM_KANBAN_QTY) String kanbanQty,
        @FormParam(WebServiceConstants.PARAM_ALLOCATE_QTY) String allocateQty,
        @FormParam(WebServiceConstants.PARAM_KANBAN_TYPE) String kanbanType,
        @FormParam(WebServiceConstants.PARAM_ASN_STATUS) String asnStatus,
        @FormParam(WebServiceConstants.PARAM_TAG_SCAN_DATETIME) String tagScanDateTime,
        @FormParam(WebServiceConstants.PARAM_RECEIVE_DATE) String receiveDate,
        @FormParam(WebServiceConstants.PARAM_DSC_ID) String dscId,
        @FormParam(WebServiceConstants.PARAM_DEVICE_ID) String deviceId,
        @FormParam(WebServiceConstants.PARAM_ASN_SCAN_DATETIME) String asnScanDateTime
        ) throws AiwsApplicationException;
}
