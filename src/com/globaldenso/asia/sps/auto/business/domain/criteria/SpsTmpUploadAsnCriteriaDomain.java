/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/17       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpUploadAsn".<br />
 * Table overview: SPS_TMP_UPLOAD_ASN<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/17 15:14:35<br />
 * 
 * This module generated automatically in 2015/03/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadAsnCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No of Upload ASN record
     */
    private BigDecimal uploadAsnId;

    /**
     * Login user Session ID
     */
    private String sessionId;

    /**
     * CSV Line No
     */
    private String csvLineNo;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * SPS D/O No
     */
    private String spsDoNo;

    /**
     * Latest Revision of D/O
     */
    private String revision;

    /**
     * Order Method
     */
    private String orderMethod;

    /**
     * Transportation Mode
0 : DENSO Truck
1 : Supplier Truck
     */
    private String tm;

    /**
     * Plan ETA
     */
    private Timestamp planEta;

    /**
     * Actual ETD
     */
    private Timestamp actualEtd;

    /**
     * Trip No
     */
    private String tripNo;

    /**
     * Number of Pallet
     */
    private BigDecimal numberOfPallet;

    /**
     * DENSO Part Number
     */
    private String dPn;

    /**
     * SupplierPart Number
     */
    private String sPn;

    /**
     * Shipping Qty
     */
    private BigDecimal shippingQty;

    /**
     * Unit of Measure
     */
    private String unitOfMeasure;

    /**
     * Receiving Lane
     */
    private String rcvLane;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * Error Flag
0 : Not error
1 : Warning
2 : Error
     */
    private String isErrorFlg;

    /**
     * Error Code category
     */
    private String errorCode;

    /**
     * DSC ID of Uploaded data
     */
    private String uploadDscId;

    /**
     * Datetime of Uploaded data
     */
    private Timestamp uploadDatetime;

    /**
     * Login user Session ID(condition whether the column value at the beginning is equal to the value)
     */
    private String sessionIdLikeFront;

    /**
     * CSV Line No(condition whether the column value at the beginning is equal to the value)
     */
    private String csvLineNoLikeFront;

    /**
     * AS/400 Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * Supplier Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * DENSO Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * SPS D/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String spsDoNoLikeFront;

    /**
     * Latest Revision of D/O(condition whether the column value at the beginning is equal to the value)
     */
    private String revisionLikeFront;

    /**
     * Order Method(condition whether the column value at the beginning is equal to the value)
     */
    private String orderMethodLikeFront;

    /**
     * Transportation Mode
0 : DENSO Truck
1 : Supplier Truck(condition whether the column value at the beginning is equal to the value)
     */
    private String tmLikeFront;

    /**
     * Plan ETA(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp planEtaGreaterThanEqual;

    /**
     * Plan ETA(condition whether the column value is less than or equal to the value)
     */
    private Timestamp planEtaLessThanEqual;

    /**
     * Actual ETD(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp actualEtdGreaterThanEqual;

    /**
     * Actual ETD(condition whether the column value is less than or equal to the value)
     */
    private Timestamp actualEtdLessThanEqual;

    /**
     * Trip No(condition whether the column value at the beginning is equal to the value)
     */
    private String tripNoLikeFront;

    /**
     * DENSO Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * SupplierPart Number(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * Unit of Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String unitOfMeasureLikeFront;

    /**
     * Receiving Lane(condition whether the column value at the beginning is equal to the value)
     */
    private String rcvLaneLikeFront;

    /**
     * ASN Number(condition whether the column value at the beginning is equal to the value)
     */
    private String asnNoLikeFront;

    /**
     * Error Flag
0 : Not error
1 : Warning
2 : Error(condition whether the column value at the beginning is equal to the value)
     */
    private String isErrorFlgLikeFront;

    /**
     * Error Code category(condition whether the column value at the beginning is equal to the value)
     */
    private String errorCodeLikeFront;

    /**
     * DSC ID of Uploaded data(condition whether the column value at the beginning is equal to the value)
     */
    private String uploadDscIdLikeFront;

    /**
     * Datetime of Uploaded data(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp uploadDatetimeGreaterThanEqual;

    /**
     * Datetime of Uploaded data(condition whether the column value is less than or equal to the value)
     */
    private Timestamp uploadDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTmpUploadAsnCriteriaDomain() {
    }

    /**
     * Getter method of "uploadAsnId".
     * 
     * @return the "uploadAsnId"
     */
    public BigDecimal getUploadAsnId() {
        return uploadAsnId;
    }

    /**
     * Setter method of "uploadAsnId".
     * 
     * @param uploadAsnId Set in "uploadAsnId".
     */
    public void setUploadAsnId(BigDecimal uploadAsnId) {
        this.uploadAsnId = uploadAsnId;
    }

    /**
     * Getter method of "sessionId".
     * 
     * @return the "sessionId"
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Setter method of "sessionId".
     * 
     * @param sessionId Set in "sessionId".
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Getter method of "csvLineNo".
     * 
     * @return the "csvLineNo"
     */
    public String getCsvLineNo() {
        return csvLineNo;
    }

    /**
     * Setter method of "csvLineNo".
     * 
     * @param csvLineNo Set in "csvLineNo".
     */
    public void setCsvLineNo(String csvLineNo) {
        this.csvLineNo = csvLineNo;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "spsDoNo".
     * 
     * @return the "spsDoNo"
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo".
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision".
     * 
     * @return the "revision"
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision".
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "orderMethod".
     * 
     * @return the "orderMethod"
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Setter method of "orderMethod".
     * 
     * @param orderMethod Set in "orderMethod".
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Getter method of "tm".
     * 
     * @return the "tm"
     */
    public String getTm() {
        return tm;
    }

    /**
     * Setter method of "tm".
     * 
     * @param tm Set in "tm".
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * Getter method of "planEta".
     * 
     * @return the "planEta"
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * Setter method of "planEta".
     * 
     * @param planEta Set in "planEta".
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * Getter method of "actualEtd".
     * 
     * @return the "actualEtd"
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * Setter method of "actualEtd".
     * 
     * @param actualEtd Set in "actualEtd".
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }

    /**
     * Getter method of "tripNo".
     * 
     * @return the "tripNo"
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo".
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "numberOfPallet".
     * 
     * @return the "numberOfPallet"
     */
    public BigDecimal getNumberOfPallet() {
        return numberOfPallet;
    }

    /**
     * Setter method of "numberOfPallet".
     * 
     * @param numberOfPallet Set in "numberOfPallet".
     */
    public void setNumberOfPallet(BigDecimal numberOfPallet) {
        this.numberOfPallet = numberOfPallet;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "shippingQty".
     * 
     * @return the "shippingQty"
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty".
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "unitOfMeasure".
     * 
     * @return the "unitOfMeasure"
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Setter method of "unitOfMeasure".
     * 
     * @param unitOfMeasure Set in "unitOfMeasure".
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter method of "rcvLane".
     * 
     * @return the "rcvLane"
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Setter method of "rcvLane".
     * 
     * @param rcvLane Set in "rcvLane".
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Getter method of "asnNo".
     * 
     * @return the "asnNo"
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo".
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "isErrorFlg".
     * 
     * @return the "isErrorFlg"
     */
    public String getIsErrorFlg() {
        return isErrorFlg;
    }

    /**
     * Setter method of "isErrorFlg".
     * 
     * @param isErrorFlg Set in "isErrorFlg".
     */
    public void setIsErrorFlg(String isErrorFlg) {
        this.isErrorFlg = isErrorFlg;
    }

    /**
     * Getter method of "errorCode".
     * 
     * @return the "errorCode"
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Setter method of "errorCode".
     * 
     * @param errorCode Set in "errorCode".
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Getter method of "uploadDscId".
     * 
     * @return the "uploadDscId"
     */
    public String getUploadDscId() {
        return uploadDscId;
    }

    /**
     * Setter method of "uploadDscId".
     * 
     * @param uploadDscId Set in "uploadDscId".
     */
    public void setUploadDscId(String uploadDscId) {
        this.uploadDscId = uploadDscId;
    }

    /**
     * Getter method of "uploadDatetime".
     * 
     * @return the "uploadDatetime"
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime".
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "sessionIdLikeFront".
     * 
     * @return the "sessionIdLikeFront"
     */
    public String getSessionIdLikeFront() {
        return sessionIdLikeFront;
    }

    /**
     * Setter method of "sessionIdLikeFront".
     * 
     * @param sessionIdLikeFront Set in "sessionIdLikeFront".
     */
    public void setSessionIdLikeFront(String sessionIdLikeFront) {
        this.sessionIdLikeFront = sessionIdLikeFront;
    }

    /**
     * Getter method of "csvLineNoLikeFront".
     * 
     * @return the "csvLineNoLikeFront"
     */
    public String getCsvLineNoLikeFront() {
        return csvLineNoLikeFront;
    }

    /**
     * Setter method of "csvLineNoLikeFront".
     * 
     * @param csvLineNoLikeFront Set in "csvLineNoLikeFront".
     */
    public void setCsvLineNoLikeFront(String csvLineNoLikeFront) {
        this.csvLineNoLikeFront = csvLineNoLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "spsDoNoLikeFront".
     * 
     * @return the "spsDoNoLikeFront"
     */
    public String getSpsDoNoLikeFront() {
        return spsDoNoLikeFront;
    }

    /**
     * Setter method of "spsDoNoLikeFront".
     * 
     * @param spsDoNoLikeFront Set in "spsDoNoLikeFront".
     */
    public void setSpsDoNoLikeFront(String spsDoNoLikeFront) {
        this.spsDoNoLikeFront = spsDoNoLikeFront;
    }

    /**
     * Getter method of "revisionLikeFront".
     * 
     * @return the "revisionLikeFront"
     */
    public String getRevisionLikeFront() {
        return revisionLikeFront;
    }

    /**
     * Setter method of "revisionLikeFront".
     * 
     * @param revisionLikeFront Set in "revisionLikeFront".
     */
    public void setRevisionLikeFront(String revisionLikeFront) {
        this.revisionLikeFront = revisionLikeFront;
    }

    /**
     * Getter method of "orderMethodLikeFront".
     * 
     * @return the "orderMethodLikeFront"
     */
    public String getOrderMethodLikeFront() {
        return orderMethodLikeFront;
    }

    /**
     * Setter method of "orderMethodLikeFront".
     * 
     * @param orderMethodLikeFront Set in "orderMethodLikeFront".
     */
    public void setOrderMethodLikeFront(String orderMethodLikeFront) {
        this.orderMethodLikeFront = orderMethodLikeFront;
    }

    /**
     * Getter method of "tmLikeFront".
     * 
     * @return the "tmLikeFront"
     */
    public String getTmLikeFront() {
        return tmLikeFront;
    }

    /**
     * Setter method of "tmLikeFront".
     * 
     * @param tmLikeFront Set in "tmLikeFront".
     */
    public void setTmLikeFront(String tmLikeFront) {
        this.tmLikeFront = tmLikeFront;
    }

    /**
     * Getter method of "planEtaGreaterThanEqual".
     * 
     * @return the "planEtaGreaterThanEqual"
     */
    public Timestamp getPlanEtaGreaterThanEqual() {
        return planEtaGreaterThanEqual;
    }

    /**
     * Setter method of "planEtaGreaterThanEqual".
     * 
     * @param planEtaGreaterThanEqual Set in "planEtaGreaterThanEqual".
     */
    public void setPlanEtaGreaterThanEqual(Timestamp planEtaGreaterThanEqual) {
        this.planEtaGreaterThanEqual = planEtaGreaterThanEqual;
    }

    /**
     * Getter method of "planEtaLessThanEqual".
     * 
     * @return the "planEtaLessThanEqual"
     */
    public Timestamp getPlanEtaLessThanEqual() {
        return planEtaLessThanEqual;
    }

    /**
     * Setter method of "planEtaLessThanEqual".
     * 
     * @param planEtaLessThanEqual Set in "planEtaLessThanEqual".
     */
    public void setPlanEtaLessThanEqual(Timestamp planEtaLessThanEqual) {
        this.planEtaLessThanEqual = planEtaLessThanEqual;
    }

    /**
     * Getter method of "actualEtdGreaterThanEqual".
     * 
     * @return the "actualEtdGreaterThanEqual"
     */
    public Timestamp getActualEtdGreaterThanEqual() {
        return actualEtdGreaterThanEqual;
    }

    /**
     * Setter method of "actualEtdGreaterThanEqual".
     * 
     * @param actualEtdGreaterThanEqual Set in "actualEtdGreaterThanEqual".
     */
    public void setActualEtdGreaterThanEqual(Timestamp actualEtdGreaterThanEqual) {
        this.actualEtdGreaterThanEqual = actualEtdGreaterThanEqual;
    }

    /**
     * Getter method of "actualEtdLessThanEqual".
     * 
     * @return the "actualEtdLessThanEqual"
     */
    public Timestamp getActualEtdLessThanEqual() {
        return actualEtdLessThanEqual;
    }

    /**
     * Setter method of "actualEtdLessThanEqual".
     * 
     * @param actualEtdLessThanEqual Set in "actualEtdLessThanEqual".
     */
    public void setActualEtdLessThanEqual(Timestamp actualEtdLessThanEqual) {
        this.actualEtdLessThanEqual = actualEtdLessThanEqual;
    }

    /**
     * Getter method of "tripNoLikeFront".
     * 
     * @return the "tripNoLikeFront"
     */
    public String getTripNoLikeFront() {
        return tripNoLikeFront;
    }

    /**
     * Setter method of "tripNoLikeFront".
     * 
     * @param tripNoLikeFront Set in "tripNoLikeFront".
     */
    public void setTripNoLikeFront(String tripNoLikeFront) {
        this.tripNoLikeFront = tripNoLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "unitOfMeasureLikeFront".
     * 
     * @return the "unitOfMeasureLikeFront"
     */
    public String getUnitOfMeasureLikeFront() {
        return unitOfMeasureLikeFront;
    }

    /**
     * Setter method of "unitOfMeasureLikeFront".
     * 
     * @param unitOfMeasureLikeFront Set in "unitOfMeasureLikeFront".
     */
    public void setUnitOfMeasureLikeFront(String unitOfMeasureLikeFront) {
        this.unitOfMeasureLikeFront = unitOfMeasureLikeFront;
    }

    /**
     * Getter method of "rcvLaneLikeFront".
     * 
     * @return the "rcvLaneLikeFront"
     */
    public String getRcvLaneLikeFront() {
        return rcvLaneLikeFront;
    }

    /**
     * Setter method of "rcvLaneLikeFront".
     * 
     * @param rcvLaneLikeFront Set in "rcvLaneLikeFront".
     */
    public void setRcvLaneLikeFront(String rcvLaneLikeFront) {
        this.rcvLaneLikeFront = rcvLaneLikeFront;
    }

    /**
     * Getter method of "asnNoLikeFront".
     * 
     * @return the "asnNoLikeFront"
     */
    public String getAsnNoLikeFront() {
        return asnNoLikeFront;
    }

    /**
     * Setter method of "asnNoLikeFront".
     * 
     * @param asnNoLikeFront Set in "asnNoLikeFront".
     */
    public void setAsnNoLikeFront(String asnNoLikeFront) {
        this.asnNoLikeFront = asnNoLikeFront;
    }

    /**
     * Getter method of "isErrorFlgLikeFront".
     * 
     * @return the "isErrorFlgLikeFront"
     */
    public String getIsErrorFlgLikeFront() {
        return isErrorFlgLikeFront;
    }

    /**
     * Setter method of "isErrorFlgLikeFront".
     * 
     * @param isErrorFlgLikeFront Set in "isErrorFlgLikeFront".
     */
    public void setIsErrorFlgLikeFront(String isErrorFlgLikeFront) {
        this.isErrorFlgLikeFront = isErrorFlgLikeFront;
    }

    /**
     * Getter method of "errorCodeLikeFront".
     * 
     * @return the "errorCodeLikeFront"
     */
    public String getErrorCodeLikeFront() {
        return errorCodeLikeFront;
    }

    /**
     * Setter method of "errorCodeLikeFront".
     * 
     * @param errorCodeLikeFront Set in "errorCodeLikeFront".
     */
    public void setErrorCodeLikeFront(String errorCodeLikeFront) {
        this.errorCodeLikeFront = errorCodeLikeFront;
    }

    /**
     * Getter method of "uploadDscIdLikeFront".
     * 
     * @return the "uploadDscIdLikeFront"
     */
    public String getUploadDscIdLikeFront() {
        return uploadDscIdLikeFront;
    }

    /**
     * Setter method of "uploadDscIdLikeFront".
     * 
     * @param uploadDscIdLikeFront Set in "uploadDscIdLikeFront".
     */
    public void setUploadDscIdLikeFront(String uploadDscIdLikeFront) {
        this.uploadDscIdLikeFront = uploadDscIdLikeFront;
    }

    /**
     * Getter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @return the "uploadDatetimeGreaterThanEqual"
     */
    public Timestamp getUploadDatetimeGreaterThanEqual() {
        return uploadDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @param uploadDatetimeGreaterThanEqual Set in "uploadDatetimeGreaterThanEqual".
     */
    public void setUploadDatetimeGreaterThanEqual(Timestamp uploadDatetimeGreaterThanEqual) {
        this.uploadDatetimeGreaterThanEqual = uploadDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeLessThanEqual".
     * 
     * @return the "uploadDatetimeLessThanEqual"
     */
    public Timestamp getUploadDatetimeLessThanEqual() {
        return uploadDatetimeLessThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeLessThanEqual".
     * 
     * @param uploadDatetimeLessThanEqual Set in "uploadDatetimeLessThanEqual".
     */
    public void setUploadDatetimeLessThanEqual(Timestamp uploadDatetimeLessThanEqual) {
        this.uploadDatetimeLessThanEqual = uploadDatetimeLessThanEqual;
    }

}
