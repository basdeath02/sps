/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;
import java.math.BigDecimal;

/**
 * A search criteria "Domain" class of "SpsTAsn".<br />
 * Table overview: SPS_T_ASN<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ASN_NO
     */
    private String asnNo;

    /**
     * ASN_STATUS
     */
    private String asnStatus;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * PLAN_ETD
     */
    private Timestamp planEtd;

    /**
     * PLAN_ETA
     */
    private Timestamp planEta;

    /**
     * ACTUAL_ETD
     */
    private Timestamp actualEtd;

    /**
     * ACTUAL_ETA
     */
    private Timestamp actualEta;

    /**
     * TRANSFER_CIGMA_FLG
     */
    private String transferCigmaFlg;

    /**
     * NUMBER_OF_PALLET
     */
    private BigDecimal numberOfPallet;

    /**
     * TRIP_NO
     */
    private String tripNo;

    /**
     * PDF_FILE_ID
     */
    private String pdfFileId;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * CREATED_INVOICE_FLAG
     */
    private String createdInvoiceFlag;

    /**
     * VENDOR_CD
     */
    private String vendorCd;

    /**
     * ASN_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String asnNoLikeFront;

    /**
     * ASN_STATUS(condition whether the column value at the beginning is equal to the value)
     */
    private String asnStatusLikeFront;

    /**
     * S_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * S_PCD(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * D_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * D_PCD(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * PLAN_ETD(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp planEtdGreaterThanEqual;

    /**
     * PLAN_ETD(condition whether the column value is less than or equal to the value)
     */
    private Timestamp planEtdLessThanEqual;

    /**
     * PLAN_ETA(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp planEtaGreaterThanEqual;

    /**
     * PLAN_ETA(condition whether the column value is less than or equal to the value)
     */
    private Timestamp planEtaLessThanEqual;

    /**
     * ACTUAL_ETD(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp actualEtdGreaterThanEqual;

    /**
     * ACTUAL_ETD(condition whether the column value is less than or equal to the value)
     */
    private Timestamp actualEtdLessThanEqual;

    /**
     * ACTUAL_ETA(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp actualEtaGreaterThanEqual;

    /**
     * ACTUAL_ETA(condition whether the column value is less than or equal to the value)
     */
    private Timestamp actualEtaLessThanEqual;

    /**
     * TRANSFER_CIGMA_FLG(condition whether the column value at the beginning is equal to the value)
     */
    private String transferCigmaFlgLikeFront;

    /**
     * TRIP_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String tripNoLikeFront;

    /**
     * PDF_FILE_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String pdfFileIdLikeFront;

    /**
     * CREATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * LAST_UPDATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * CREATED_INVOICE_FLAG(condition whether the column value at the beginning is equal to the value)
     */
    private String createdInvoiceFlagLikeFront;

    /**
     * VENDOR_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * Default constructor
     */
    public SpsTAsnCriteriaDomain() {
    }

    /**
     * Getter method of "asnNo".
     * 
     * @return the "asnNo"
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo".
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "asnStatus".
     * 
     * @return the "asnStatus"
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * Setter method of "asnStatus".
     * 
     * @param asnStatus Set in "asnStatus".
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "planEtd".
     * 
     * @return the "planEtd"
     */
    public Timestamp getPlanEtd() {
        return planEtd;
    }

    /**
     * Setter method of "planEtd".
     * 
     * @param planEtd Set in "planEtd".
     */
    public void setPlanEtd(Timestamp planEtd) {
        this.planEtd = planEtd;
    }

    /**
     * Getter method of "planEta".
     * 
     * @return the "planEta"
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * Setter method of "planEta".
     * 
     * @param planEta Set in "planEta".
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * Getter method of "actualEtd".
     * 
     * @return the "actualEtd"
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * Setter method of "actualEtd".
     * 
     * @param actualEtd Set in "actualEtd".
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }

    /**
     * Getter method of "actualEta".
     * 
     * @return the "actualEta"
     */
    public Timestamp getActualEta() {
        return actualEta;
    }

    /**
     * Setter method of "actualEta".
     * 
     * @param actualEta Set in "actualEta".
     */
    public void setActualEta(Timestamp actualEta) {
        this.actualEta = actualEta;
    }

    /**
     * Getter method of "transferCigmaFlg".
     * 
     * @return the "transferCigmaFlg"
     */
    public String getTransferCigmaFlg() {
        return transferCigmaFlg;
    }

    /**
     * Setter method of "transferCigmaFlg".
     * 
     * @param transferCigmaFlg Set in "transferCigmaFlg".
     */
    public void setTransferCigmaFlg(String transferCigmaFlg) {
        this.transferCigmaFlg = transferCigmaFlg;
    }

    /**
     * Getter method of "numberOfPallet".
     * 
     * @return the "numberOfPallet"
     */
    public BigDecimal getNumberOfPallet() {
        return numberOfPallet;
    }

    /**
     * Setter method of "numberOfPallet".
     * 
     * @param numberOfPallet Set in "numberOfPallet".
     */
    public void setNumberOfPallet(BigDecimal numberOfPallet) {
        this.numberOfPallet = numberOfPallet;
    }

    /**
     * Getter method of "tripNo".
     * 
     * @return the "tripNo"
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo".
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "pdfFileId".
     * 
     * @return the "pdfFileId"
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Setter method of "pdfFileId".
     * 
     * @param pdfFileId Set in "pdfFileId".
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "createdInvoiceFlag".
     * 
     * @return the "createdInvoiceFlag"
     */
    public String getCreatedInvoiceFlag() {
        return createdInvoiceFlag;
    }

    /**
     * Setter method of "createdInvoiceFlag".
     * 
     * @param createdInvoiceFlag Set in "createdInvoiceFlag".
     */
    public void setCreatedInvoiceFlag(String createdInvoiceFlag) {
        this.createdInvoiceFlag = createdInvoiceFlag;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "asnNoLikeFront".
     * 
     * @return the "asnNoLikeFront"
     */
    public String getAsnNoLikeFront() {
        return asnNoLikeFront;
    }

    /**
     * Setter method of "asnNoLikeFront".
     * 
     * @param asnNoLikeFront Set in "asnNoLikeFront".
     */
    public void setAsnNoLikeFront(String asnNoLikeFront) {
        this.asnNoLikeFront = asnNoLikeFront;
    }

    /**
     * Getter method of "asnStatusLikeFront".
     * 
     * @return the "asnStatusLikeFront"
     */
    public String getAsnStatusLikeFront() {
        return asnStatusLikeFront;
    }

    /**
     * Setter method of "asnStatusLikeFront".
     * 
     * @param asnStatusLikeFront Set in "asnStatusLikeFront".
     */
    public void setAsnStatusLikeFront(String asnStatusLikeFront) {
        this.asnStatusLikeFront = asnStatusLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "planEtdGreaterThanEqual".
     * 
     * @return the "planEtdGreaterThanEqual"
     */
    public Timestamp getPlanEtdGreaterThanEqual() {
        return planEtdGreaterThanEqual;
    }

    /**
     * Setter method of "planEtdGreaterThanEqual".
     * 
     * @param planEtdGreaterThanEqual Set in "planEtdGreaterThanEqual".
     */
    public void setPlanEtdGreaterThanEqual(Timestamp planEtdGreaterThanEqual) {
        this.planEtdGreaterThanEqual = planEtdGreaterThanEqual;
    }

    /**
     * Getter method of "planEtdLessThanEqual".
     * 
     * @return the "planEtdLessThanEqual"
     */
    public Timestamp getPlanEtdLessThanEqual() {
        return planEtdLessThanEqual;
    }

    /**
     * Setter method of "planEtdLessThanEqual".
     * 
     * @param planEtdLessThanEqual Set in "planEtdLessThanEqual".
     */
    public void setPlanEtdLessThanEqual(Timestamp planEtdLessThanEqual) {
        this.planEtdLessThanEqual = planEtdLessThanEqual;
    }

    /**
     * Getter method of "planEtaGreaterThanEqual".
     * 
     * @return the "planEtaGreaterThanEqual"
     */
    public Timestamp getPlanEtaGreaterThanEqual() {
        return planEtaGreaterThanEqual;
    }

    /**
     * Setter method of "planEtaGreaterThanEqual".
     * 
     * @param planEtaGreaterThanEqual Set in "planEtaGreaterThanEqual".
     */
    public void setPlanEtaGreaterThanEqual(Timestamp planEtaGreaterThanEqual) {
        this.planEtaGreaterThanEqual = planEtaGreaterThanEqual;
    }

    /**
     * Getter method of "planEtaLessThanEqual".
     * 
     * @return the "planEtaLessThanEqual"
     */
    public Timestamp getPlanEtaLessThanEqual() {
        return planEtaLessThanEqual;
    }

    /**
     * Setter method of "planEtaLessThanEqual".
     * 
     * @param planEtaLessThanEqual Set in "planEtaLessThanEqual".
     */
    public void setPlanEtaLessThanEqual(Timestamp planEtaLessThanEqual) {
        this.planEtaLessThanEqual = planEtaLessThanEqual;
    }

    /**
     * Getter method of "actualEtdGreaterThanEqual".
     * 
     * @return the "actualEtdGreaterThanEqual"
     */
    public Timestamp getActualEtdGreaterThanEqual() {
        return actualEtdGreaterThanEqual;
    }

    /**
     * Setter method of "actualEtdGreaterThanEqual".
     * 
     * @param actualEtdGreaterThanEqual Set in "actualEtdGreaterThanEqual".
     */
    public void setActualEtdGreaterThanEqual(Timestamp actualEtdGreaterThanEqual) {
        this.actualEtdGreaterThanEqual = actualEtdGreaterThanEqual;
    }

    /**
     * Getter method of "actualEtdLessThanEqual".
     * 
     * @return the "actualEtdLessThanEqual"
     */
    public Timestamp getActualEtdLessThanEqual() {
        return actualEtdLessThanEqual;
    }

    /**
     * Setter method of "actualEtdLessThanEqual".
     * 
     * @param actualEtdLessThanEqual Set in "actualEtdLessThanEqual".
     */
    public void setActualEtdLessThanEqual(Timestamp actualEtdLessThanEqual) {
        this.actualEtdLessThanEqual = actualEtdLessThanEqual;
    }

    /**
     * Getter method of "actualEtaGreaterThanEqual".
     * 
     * @return the "actualEtaGreaterThanEqual"
     */
    public Timestamp getActualEtaGreaterThanEqual() {
        return actualEtaGreaterThanEqual;
    }

    /**
     * Setter method of "actualEtaGreaterThanEqual".
     * 
     * @param actualEtaGreaterThanEqual Set in "actualEtaGreaterThanEqual".
     */
    public void setActualEtaGreaterThanEqual(Timestamp actualEtaGreaterThanEqual) {
        this.actualEtaGreaterThanEqual = actualEtaGreaterThanEqual;
    }

    /**
     * Getter method of "actualEtaLessThanEqual".
     * 
     * @return the "actualEtaLessThanEqual"
     */
    public Timestamp getActualEtaLessThanEqual() {
        return actualEtaLessThanEqual;
    }

    /**
     * Setter method of "actualEtaLessThanEqual".
     * 
     * @param actualEtaLessThanEqual Set in "actualEtaLessThanEqual".
     */
    public void setActualEtaLessThanEqual(Timestamp actualEtaLessThanEqual) {
        this.actualEtaLessThanEqual = actualEtaLessThanEqual;
    }

    /**
     * Getter method of "transferCigmaFlgLikeFront".
     * 
     * @return the "transferCigmaFlgLikeFront"
     */
    public String getTransferCigmaFlgLikeFront() {
        return transferCigmaFlgLikeFront;
    }

    /**
     * Setter method of "transferCigmaFlgLikeFront".
     * 
     * @param transferCigmaFlgLikeFront Set in "transferCigmaFlgLikeFront".
     */
    public void setTransferCigmaFlgLikeFront(String transferCigmaFlgLikeFront) {
        this.transferCigmaFlgLikeFront = transferCigmaFlgLikeFront;
    }

    /**
     * Getter method of "tripNoLikeFront".
     * 
     * @return the "tripNoLikeFront"
     */
    public String getTripNoLikeFront() {
        return tripNoLikeFront;
    }

    /**
     * Setter method of "tripNoLikeFront".
     * 
     * @param tripNoLikeFront Set in "tripNoLikeFront".
     */
    public void setTripNoLikeFront(String tripNoLikeFront) {
        this.tripNoLikeFront = tripNoLikeFront;
    }

    /**
     * Getter method of "pdfFileIdLikeFront".
     * 
     * @return the "pdfFileIdLikeFront"
     */
    public String getPdfFileIdLikeFront() {
        return pdfFileIdLikeFront;
    }

    /**
     * Setter method of "pdfFileIdLikeFront".
     * 
     * @param pdfFileIdLikeFront Set in "pdfFileIdLikeFront".
     */
    public void setPdfFileIdLikeFront(String pdfFileIdLikeFront) {
        this.pdfFileIdLikeFront = pdfFileIdLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Getter method of "createdInvoiceFlagLikeFront".
     * 
     * @return the "createdInvoiceFlagLikeFront"
     */
    public String getCreatedInvoiceFlagLikeFront() {
        return createdInvoiceFlagLikeFront;
    }

    /**
     * Setter method of "createdInvoiceFlagLikeFront".
     * 
     * @param createdInvoiceFlagLikeFront Set in "createdInvoiceFlagLikeFront".
     */
    public void setCreatedInvoiceFlagLikeFront(String createdInvoiceFlagLikeFront) {
        this.createdInvoiceFlagLikeFront = createdInvoiceFlagLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

}
