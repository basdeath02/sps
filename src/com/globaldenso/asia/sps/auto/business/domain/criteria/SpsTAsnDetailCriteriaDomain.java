/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTAsnDetail".<br />
 * Table overview: SPS_T_ASN_DETAIL<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnDetailCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Reference Delivery Order ID
     */
    private BigDecimal doId;

    /**
     * SPS D/O No.
     */
    private String spsDoNo;

    /**
     * Latest D/O Revision
     */
    private String revision;

    /**
     * CIGMA D/O No
     */
    private String cigmaDoNo;

    /**
     * Shipping Qty
     */
    private BigDecimal shippingQty;

    /**
     * Shipping QTY with removing decimal digit
     */
    private BigDecimal shippingRoundQty;

    /**
     * QTY/Box
     */
    private BigDecimal qtyBox;

    /**
     * Shipping Box Qty
     */
    private BigDecimal shippingBoxQty;

    /**
     * Receiving Lane
     */
    private String rcvLane;

    /**
     * Unit of Measure
     */
    private String unitOfMeasure;

    /**
     * Change Reason when cannot ship completely
     */
    private String changeReasonCd;

    /**
     * Revise Shipping Qty Flag
0 : None, 1 : Revised
     */
    private String reviseShippingQtyFlag;

    /**
     * Allow to revise shipping qty flag, 0 : Not allow, 1 : allow
     */
    private String allowReviseFlag;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * ASN Number(condition whether the column value at the beginning is equal to the value)
     */
    private String asnNoLikeFront;

    /**
     * DENSO Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * Supplier Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * DENSO Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * SPS D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String spsDoNoLikeFront;

    /**
     * Latest D/O Revision(condition whether the column value at the beginning is equal to the value)
     */
    private String revisionLikeFront;

    /**
     * CIGMA D/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaDoNoLikeFront;

    /**
     * Receiving Lane(condition whether the column value at the beginning is equal to the value)
     */
    private String rcvLaneLikeFront;

    /**
     * Unit of Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String unitOfMeasureLikeFront;

    /**
     * Change Reason when cannot ship completely(condition whether the column value at the beginning is equal to the value)
     */
    private String changeReasonCdLikeFront;

    /**
     * Revise Shipping Qty Flag
0 : None, 1 : Revised(condition whether the column value at the beginning is equal to the value)
     */
    private String reviseShippingQtyFlagLikeFront;

    /**
     * Allow to revise shipping qty flag, 0 : Not allow, 1 : allow(condition whether the column value at the beginning is equal to the value)
     */
    private String allowReviseFlagLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTAsnDetailCriteriaDomain() {
    }

    /**
     * Getter method of "asnNo".
     * 
     * @return the "asnNo"
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo".
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "doId".
     * 
     * @return the "doId"
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId".
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "spsDoNo".
     * 
     * @return the "spsDoNo"
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo".
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision".
     * 
     * @return the "revision"
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision".
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "cigmaDoNo".
     * 
     * @return the "cigmaDoNo"
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo".
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "shippingQty".
     * 
     * @return the "shippingQty"
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty".
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "shippingRoundQty".
     * 
     * @return the "shippingRoundQty"
     */
    public BigDecimal getShippingRoundQty() {
        return shippingRoundQty;
    }

    /**
     * Setter method of "shippingRoundQty".
     * 
     * @param shippingRoundQty Set in "shippingRoundQty".
     */
    public void setShippingRoundQty(BigDecimal shippingRoundQty) {
        this.shippingRoundQty = shippingRoundQty;
    }

    /**
     * Getter method of "qtyBox".
     * 
     * @return the "qtyBox"
     */
    public BigDecimal getQtyBox() {
        return qtyBox;
    }

    /**
     * Setter method of "qtyBox".
     * 
     * @param qtyBox Set in "qtyBox".
     */
    public void setQtyBox(BigDecimal qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * Getter method of "shippingBoxQty".
     * 
     * @return the "shippingBoxQty"
     */
    public BigDecimal getShippingBoxQty() {
        return shippingBoxQty;
    }

    /**
     * Setter method of "shippingBoxQty".
     * 
     * @param shippingBoxQty Set in "shippingBoxQty".
     */
    public void setShippingBoxQty(BigDecimal shippingBoxQty) {
        this.shippingBoxQty = shippingBoxQty;
    }

    /**
     * Getter method of "rcvLane".
     * 
     * @return the "rcvLane"
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Setter method of "rcvLane".
     * 
     * @param rcvLane Set in "rcvLane".
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Getter method of "unitOfMeasure".
     * 
     * @return the "unitOfMeasure"
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Setter method of "unitOfMeasure".
     * 
     * @param unitOfMeasure Set in "unitOfMeasure".
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter method of "changeReasonCd".
     * 
     * @return the "changeReasonCd"
     */
    public String getChangeReasonCd() {
        return changeReasonCd;
    }

    /**
     * Setter method of "changeReasonCd".
     * 
     * @param changeReasonCd Set in "changeReasonCd".
     */
    public void setChangeReasonCd(String changeReasonCd) {
        this.changeReasonCd = changeReasonCd;
    }

    /**
     * Getter method of "reviseShippingQtyFlag".
     * 
     * @return the "reviseShippingQtyFlag"
     */
    public String getReviseShippingQtyFlag() {
        return reviseShippingQtyFlag;
    }

    /**
     * Setter method of "reviseShippingQtyFlag".
     * 
     * @param reviseShippingQtyFlag Set in "reviseShippingQtyFlag".
     */
    public void setReviseShippingQtyFlag(String reviseShippingQtyFlag) {
        this.reviseShippingQtyFlag = reviseShippingQtyFlag;
    }

    /**
     * Getter method of "allowReviseFlag".
     * 
     * @return the "allowReviseFlag"
     */
    public String getAllowReviseFlag() {
        return allowReviseFlag;
    }

    /**
     * Setter method of "allowReviseFlag".
     * 
     * @param allowReviseFlag Set in "allowReviseFlag".
     */
    public void setAllowReviseFlag(String allowReviseFlag) {
        this.allowReviseFlag = allowReviseFlag;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "asnNoLikeFront".
     * 
     * @return the "asnNoLikeFront"
     */
    public String getAsnNoLikeFront() {
        return asnNoLikeFront;
    }

    /**
     * Setter method of "asnNoLikeFront".
     * 
     * @param asnNoLikeFront Set in "asnNoLikeFront".
     */
    public void setAsnNoLikeFront(String asnNoLikeFront) {
        this.asnNoLikeFront = asnNoLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "spsDoNoLikeFront".
     * 
     * @return the "spsDoNoLikeFront"
     */
    public String getSpsDoNoLikeFront() {
        return spsDoNoLikeFront;
    }

    /**
     * Setter method of "spsDoNoLikeFront".
     * 
     * @param spsDoNoLikeFront Set in "spsDoNoLikeFront".
     */
    public void setSpsDoNoLikeFront(String spsDoNoLikeFront) {
        this.spsDoNoLikeFront = spsDoNoLikeFront;
    }

    /**
     * Getter method of "revisionLikeFront".
     * 
     * @return the "revisionLikeFront"
     */
    public String getRevisionLikeFront() {
        return revisionLikeFront;
    }

    /**
     * Setter method of "revisionLikeFront".
     * 
     * @param revisionLikeFront Set in "revisionLikeFront".
     */
    public void setRevisionLikeFront(String revisionLikeFront) {
        this.revisionLikeFront = revisionLikeFront;
    }

    /**
     * Getter method of "cigmaDoNoLikeFront".
     * 
     * @return the "cigmaDoNoLikeFront"
     */
    public String getCigmaDoNoLikeFront() {
        return cigmaDoNoLikeFront;
    }

    /**
     * Setter method of "cigmaDoNoLikeFront".
     * 
     * @param cigmaDoNoLikeFront Set in "cigmaDoNoLikeFront".
     */
    public void setCigmaDoNoLikeFront(String cigmaDoNoLikeFront) {
        this.cigmaDoNoLikeFront = cigmaDoNoLikeFront;
    }

    /**
     * Getter method of "rcvLaneLikeFront".
     * 
     * @return the "rcvLaneLikeFront"
     */
    public String getRcvLaneLikeFront() {
        return rcvLaneLikeFront;
    }

    /**
     * Setter method of "rcvLaneLikeFront".
     * 
     * @param rcvLaneLikeFront Set in "rcvLaneLikeFront".
     */
    public void setRcvLaneLikeFront(String rcvLaneLikeFront) {
        this.rcvLaneLikeFront = rcvLaneLikeFront;
    }

    /**
     * Getter method of "unitOfMeasureLikeFront".
     * 
     * @return the "unitOfMeasureLikeFront"
     */
    public String getUnitOfMeasureLikeFront() {
        return unitOfMeasureLikeFront;
    }

    /**
     * Setter method of "unitOfMeasureLikeFront".
     * 
     * @param unitOfMeasureLikeFront Set in "unitOfMeasureLikeFront".
     */
    public void setUnitOfMeasureLikeFront(String unitOfMeasureLikeFront) {
        this.unitOfMeasureLikeFront = unitOfMeasureLikeFront;
    }

    /**
     * Getter method of "changeReasonCdLikeFront".
     * 
     * @return the "changeReasonCdLikeFront"
     */
    public String getChangeReasonCdLikeFront() {
        return changeReasonCdLikeFront;
    }

    /**
     * Setter method of "changeReasonCdLikeFront".
     * 
     * @param changeReasonCdLikeFront Set in "changeReasonCdLikeFront".
     */
    public void setChangeReasonCdLikeFront(String changeReasonCdLikeFront) {
        this.changeReasonCdLikeFront = changeReasonCdLikeFront;
    }

    /**
     * Getter method of "reviseShippingQtyFlagLikeFront".
     * 
     * @return the "reviseShippingQtyFlagLikeFront"
     */
    public String getReviseShippingQtyFlagLikeFront() {
        return reviseShippingQtyFlagLikeFront;
    }

    /**
     * Setter method of "reviseShippingQtyFlagLikeFront".
     * 
     * @param reviseShippingQtyFlagLikeFront Set in "reviseShippingQtyFlagLikeFront".
     */
    public void setReviseShippingQtyFlagLikeFront(String reviseShippingQtyFlagLikeFront) {
        this.reviseShippingQtyFlagLikeFront = reviseShippingQtyFlagLikeFront;
    }

    /**
     * Getter method of "allowReviseFlagLikeFront".
     * 
     * @return the "allowReviseFlagLikeFront"
     */
    public String getAllowReviseFlagLikeFront() {
        return allowReviseFlagLikeFront;
    }

    /**
     * Setter method of "allowReviseFlagLikeFront".
     * 
     * @param allowReviseFlagLikeFront Set in "allowReviseFlagLikeFront".
     */
    public void setAllowReviseFlagLikeFront(String allowReviseFlagLikeFront) {
        this.allowReviseFlagLikeFront = allowReviseFlagLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
