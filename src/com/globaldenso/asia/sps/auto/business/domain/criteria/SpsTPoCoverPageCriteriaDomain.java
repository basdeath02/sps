/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/11/28       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTPoCoverPage".<br />
 * Table overview: SPS_T_PO_COVER_PAGE<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/11/28 09:54:32<br />
 * 
 * This module generated automatically in 2014/11/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from SPS_T_PO
     */
    private BigDecimal poId;

    /**
     * Release No
     */
    private BigDecimal releaseNo;

    /**
     * P/O Issue Date
     */
    private Date poIssueDate;

    /**
     * Seller Contract Name
     */
    private String sellerContractName;

    /**
     * Seller Name
     */
    private String sellerName;

    /**
     * Seller Address1
     */
    private String sellerAddress1;

    /**
     * Seller Address2
     */
    private String sellerAddress2;

    /**
     * Seller Address3
     */
    private String sellerAddress3;

    /**
     * Seller Fax Number
     */
    private String sellerFaxNumber;

    /**
     * Payment Term
     */
    private String paymentTerm;

    /**
     * Ship Via
     */
    private String shipVia;

    /**
     * Price Term
     */
    private String priceTerm;

    /**
     * Purchaser Contract Name
     */
    private String purchaseContractName;

    /**
     * Purchaser Name
     */
    private String purchaseName;

    /**
     * Purchaser Address1
     */
    private String purchaseAddress1;

    /**
     * Purchaser Address2
     */
    private String purchaseAddress2;

    /**
     * Purchaser Address3
     */
    private String purchaseAddress3;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * P/O Issue Date(condition whether the column value is greater than or equal to the value)
     */
    private Date poIssueDateGreaterThanEqual;

    /**
     * P/O Issue Date(condition whether the column value is less than or equal to the value)
     */
    private Date poIssueDateLessThanEqual;

    /**
     * Seller Contract Name(condition whether the column value at the beginning is equal to the value)
     */
    private String sellerContractNameLikeFront;

    /**
     * Seller Name(condition whether the column value at the beginning is equal to the value)
     */
    private String sellerNameLikeFront;

    /**
     * Seller Address1(condition whether the column value at the beginning is equal to the value)
     */
    private String sellerAddress1LikeFront;

    /**
     * Seller Address2(condition whether the column value at the beginning is equal to the value)
     */
    private String sellerAddress2LikeFront;

    /**
     * Seller Address3(condition whether the column value at the beginning is equal to the value)
     */
    private String sellerAddress3LikeFront;

    /**
     * Seller Fax Number(condition whether the column value at the beginning is equal to the value)
     */
    private String sellerFaxNumberLikeFront;

    /**
     * Payment Term(condition whether the column value at the beginning is equal to the value)
     */
    private String paymentTermLikeFront;

    /**
     * Ship Via(condition whether the column value at the beginning is equal to the value)
     */
    private String shipViaLikeFront;

    /**
     * Price Term(condition whether the column value at the beginning is equal to the value)
     */
    private String priceTermLikeFront;

    /**
     * Purchaser Contract Name(condition whether the column value at the beginning is equal to the value)
     */
    private String purchaseContractNameLikeFront;

    /**
     * Purchaser Name(condition whether the column value at the beginning is equal to the value)
     */
    private String purchaseNameLikeFront;

    /**
     * Purchaser Address1(condition whether the column value at the beginning is equal to the value)
     */
    private String purchaseAddress1LikeFront;

    /**
     * Purchaser Address2(condition whether the column value at the beginning is equal to the value)
     */
    private String purchaseAddress2LikeFront;

    /**
     * Purchaser Address3(condition whether the column value at the beginning is equal to the value)
     */
    private String purchaseAddress3LikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTPoCoverPageCriteriaDomain() {
    }

    /**
     * Getter method of "poId".
     * 
     * @return the "poId"
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId".
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "releaseNo".
     * 
     * @return the "releaseNo"
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }

    /**
     * Setter method of "releaseNo".
     * 
     * @param releaseNo Set in "releaseNo".
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }

    /**
     * Getter method of "poIssueDate".
     * 
     * @return the "poIssueDate"
     */
    public Date getPoIssueDate() {
        return poIssueDate;
    }

    /**
     * Setter method of "poIssueDate".
     * 
     * @param poIssueDate Set in "poIssueDate".
     */
    public void setPoIssueDate(Date poIssueDate) {
        this.poIssueDate = poIssueDate;
    }

    /**
     * Getter method of "sellerContractName".
     * 
     * @return the "sellerContractName"
     */
    public String getSellerContractName() {
        return sellerContractName;
    }

    /**
     * Setter method of "sellerContractName".
     * 
     * @param sellerContractName Set in "sellerContractName".
     */
    public void setSellerContractName(String sellerContractName) {
        this.sellerContractName = sellerContractName;
    }

    /**
     * Getter method of "sellerName".
     * 
     * @return the "sellerName"
     */
    public String getSellerName() {
        return sellerName;
    }

    /**
     * Setter method of "sellerName".
     * 
     * @param sellerName Set in "sellerName".
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    /**
     * Getter method of "sellerAddress1".
     * 
     * @return the "sellerAddress1"
     */
    public String getSellerAddress1() {
        return sellerAddress1;
    }

    /**
     * Setter method of "sellerAddress1".
     * 
     * @param sellerAddress1 Set in "sellerAddress1".
     */
    public void setSellerAddress1(String sellerAddress1) {
        this.sellerAddress1 = sellerAddress1;
    }

    /**
     * Getter method of "sellerAddress2".
     * 
     * @return the "sellerAddress2"
     */
    public String getSellerAddress2() {
        return sellerAddress2;
    }

    /**
     * Setter method of "sellerAddress2".
     * 
     * @param sellerAddress2 Set in "sellerAddress2".
     */
    public void setSellerAddress2(String sellerAddress2) {
        this.sellerAddress2 = sellerAddress2;
    }

    /**
     * Getter method of "sellerAddress3".
     * 
     * @return the "sellerAddress3"
     */
    public String getSellerAddress3() {
        return sellerAddress3;
    }

    /**
     * Setter method of "sellerAddress3".
     * 
     * @param sellerAddress3 Set in "sellerAddress3".
     */
    public void setSellerAddress3(String sellerAddress3) {
        this.sellerAddress3 = sellerAddress3;
    }

    /**
     * Getter method of "sellerFaxNumber".
     * 
     * @return the "sellerFaxNumber"
     */
    public String getSellerFaxNumber() {
        return sellerFaxNumber;
    }

    /**
     * Setter method of "sellerFaxNumber".
     * 
     * @param sellerFaxNumber Set in "sellerFaxNumber".
     */
    public void setSellerFaxNumber(String sellerFaxNumber) {
        this.sellerFaxNumber = sellerFaxNumber;
    }

    /**
     * Getter method of "paymentTerm".
     * 
     * @return the "paymentTerm"
     */
    public String getPaymentTerm() {
        return paymentTerm;
    }

    /**
     * Setter method of "paymentTerm".
     * 
     * @param paymentTerm Set in "paymentTerm".
     */
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     * Getter method of "shipVia".
     * 
     * @return the "shipVia"
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * Setter method of "shipVia".
     * 
     * @param shipVia Set in "shipVia".
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * Getter method of "priceTerm".
     * 
     * @return the "priceTerm"
     */
    public String getPriceTerm() {
        return priceTerm;
    }

    /**
     * Setter method of "priceTerm".
     * 
     * @param priceTerm Set in "priceTerm".
     */
    public void setPriceTerm(String priceTerm) {
        this.priceTerm = priceTerm;
    }

    /**
     * Getter method of "purchaseContractName".
     * 
     * @return the "purchaseContractName"
     */
    public String getPurchaseContractName() {
        return purchaseContractName;
    }

    /**
     * Setter method of "purchaseContractName".
     * 
     * @param purchaseContractName Set in "purchaseContractName".
     */
    public void setPurchaseContractName(String purchaseContractName) {
        this.purchaseContractName = purchaseContractName;
    }

    /**
     * Getter method of "purchaseName".
     * 
     * @return the "purchaseName"
     */
    public String getPurchaseName() {
        return purchaseName;
    }

    /**
     * Setter method of "purchaseName".
     * 
     * @param purchaseName Set in "purchaseName".
     */
    public void setPurchaseName(String purchaseName) {
        this.purchaseName = purchaseName;
    }

    /**
     * Getter method of "purchaseAddress1".
     * 
     * @return the "purchaseAddress1"
     */
    public String getPurchaseAddress1() {
        return purchaseAddress1;
    }

    /**
     * Setter method of "purchaseAddress1".
     * 
     * @param purchaseAddress1 Set in "purchaseAddress1".
     */
    public void setPurchaseAddress1(String purchaseAddress1) {
        this.purchaseAddress1 = purchaseAddress1;
    }

    /**
     * Getter method of "purchaseAddress2".
     * 
     * @return the "purchaseAddress2"
     */
    public String getPurchaseAddress2() {
        return purchaseAddress2;
    }

    /**
     * Setter method of "purchaseAddress2".
     * 
     * @param purchaseAddress2 Set in "purchaseAddress2".
     */
    public void setPurchaseAddress2(String purchaseAddress2) {
        this.purchaseAddress2 = purchaseAddress2;
    }

    /**
     * Getter method of "purchaseAddress3".
     * 
     * @return the "purchaseAddress3"
     */
    public String getPurchaseAddress3() {
        return purchaseAddress3;
    }

    /**
     * Setter method of "purchaseAddress3".
     * 
     * @param purchaseAddress3 Set in "purchaseAddress3".
     */
    public void setPurchaseAddress3(String purchaseAddress3) {
        this.purchaseAddress3 = purchaseAddress3;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "poIssueDateGreaterThanEqual".
     * 
     * @return the "poIssueDateGreaterThanEqual"
     */
    public Date getPoIssueDateGreaterThanEqual() {
        return poIssueDateGreaterThanEqual;
    }

    /**
     * Setter method of "poIssueDateGreaterThanEqual".
     * 
     * @param poIssueDateGreaterThanEqual Set in "poIssueDateGreaterThanEqual".
     */
    public void setPoIssueDateGreaterThanEqual(Date poIssueDateGreaterThanEqual) {
        this.poIssueDateGreaterThanEqual = poIssueDateGreaterThanEqual;
    }

    /**
     * Getter method of "poIssueDateLessThanEqual".
     * 
     * @return the "poIssueDateLessThanEqual"
     */
    public Date getPoIssueDateLessThanEqual() {
        return poIssueDateLessThanEqual;
    }

    /**
     * Setter method of "poIssueDateLessThanEqual".
     * 
     * @param poIssueDateLessThanEqual Set in "poIssueDateLessThanEqual".
     */
    public void setPoIssueDateLessThanEqual(Date poIssueDateLessThanEqual) {
        this.poIssueDateLessThanEqual = poIssueDateLessThanEqual;
    }

    /**
     * Getter method of "sellerContractNameLikeFront".
     * 
     * @return the "sellerContractNameLikeFront"
     */
    public String getSellerContractNameLikeFront() {
        return sellerContractNameLikeFront;
    }

    /**
     * Setter method of "sellerContractNameLikeFront".
     * 
     * @param sellerContractNameLikeFront Set in "sellerContractNameLikeFront".
     */
    public void setSellerContractNameLikeFront(String sellerContractNameLikeFront) {
        this.sellerContractNameLikeFront = sellerContractNameLikeFront;
    }

    /**
     * Getter method of "sellerNameLikeFront".
     * 
     * @return the "sellerNameLikeFront"
     */
    public String getSellerNameLikeFront() {
        return sellerNameLikeFront;
    }

    /**
     * Setter method of "sellerNameLikeFront".
     * 
     * @param sellerNameLikeFront Set in "sellerNameLikeFront".
     */
    public void setSellerNameLikeFront(String sellerNameLikeFront) {
        this.sellerNameLikeFront = sellerNameLikeFront;
    }

    /**
     * Getter method of "sellerAddress1LikeFront".
     * 
     * @return the "sellerAddress1LikeFront"
     */
    public String getSellerAddress1LikeFront() {
        return sellerAddress1LikeFront;
    }

    /**
     * Setter method of "sellerAddress1LikeFront".
     * 
     * @param sellerAddress1LikeFront Set in "sellerAddress1LikeFront".
     */
    public void setSellerAddress1LikeFront(String sellerAddress1LikeFront) {
        this.sellerAddress1LikeFront = sellerAddress1LikeFront;
    }

    /**
     * Getter method of "sellerAddress2LikeFront".
     * 
     * @return the "sellerAddress2LikeFront"
     */
    public String getSellerAddress2LikeFront() {
        return sellerAddress2LikeFront;
    }

    /**
     * Setter method of "sellerAddress2LikeFront".
     * 
     * @param sellerAddress2LikeFront Set in "sellerAddress2LikeFront".
     */
    public void setSellerAddress2LikeFront(String sellerAddress2LikeFront) {
        this.sellerAddress2LikeFront = sellerAddress2LikeFront;
    }

    /**
     * Getter method of "sellerAddress3LikeFront".
     * 
     * @return the "sellerAddress3LikeFront"
     */
    public String getSellerAddress3LikeFront() {
        return sellerAddress3LikeFront;
    }

    /**
     * Setter method of "sellerAddress3LikeFront".
     * 
     * @param sellerAddress3LikeFront Set in "sellerAddress3LikeFront".
     */
    public void setSellerAddress3LikeFront(String sellerAddress3LikeFront) {
        this.sellerAddress3LikeFront = sellerAddress3LikeFront;
    }

    /**
     * Getter method of "sellerFaxNumberLikeFront".
     * 
     * @return the "sellerFaxNumberLikeFront"
     */
    public String getSellerFaxNumberLikeFront() {
        return sellerFaxNumberLikeFront;
    }

    /**
     * Setter method of "sellerFaxNumberLikeFront".
     * 
     * @param sellerFaxNumberLikeFront Set in "sellerFaxNumberLikeFront".
     */
    public void setSellerFaxNumberLikeFront(String sellerFaxNumberLikeFront) {
        this.sellerFaxNumberLikeFront = sellerFaxNumberLikeFront;
    }

    /**
     * Getter method of "paymentTermLikeFront".
     * 
     * @return the "paymentTermLikeFront"
     */
    public String getPaymentTermLikeFront() {
        return paymentTermLikeFront;
    }

    /**
     * Setter method of "paymentTermLikeFront".
     * 
     * @param paymentTermLikeFront Set in "paymentTermLikeFront".
     */
    public void setPaymentTermLikeFront(String paymentTermLikeFront) {
        this.paymentTermLikeFront = paymentTermLikeFront;
    }

    /**
     * Getter method of "shipViaLikeFront".
     * 
     * @return the "shipViaLikeFront"
     */
    public String getShipViaLikeFront() {
        return shipViaLikeFront;
    }

    /**
     * Setter method of "shipViaLikeFront".
     * 
     * @param shipViaLikeFront Set in "shipViaLikeFront".
     */
    public void setShipViaLikeFront(String shipViaLikeFront) {
        this.shipViaLikeFront = shipViaLikeFront;
    }

    /**
     * Getter method of "priceTermLikeFront".
     * 
     * @return the "priceTermLikeFront"
     */
    public String getPriceTermLikeFront() {
        return priceTermLikeFront;
    }

    /**
     * Setter method of "priceTermLikeFront".
     * 
     * @param priceTermLikeFront Set in "priceTermLikeFront".
     */
    public void setPriceTermLikeFront(String priceTermLikeFront) {
        this.priceTermLikeFront = priceTermLikeFront;
    }

    /**
     * Getter method of "purchaseContractNameLikeFront".
     * 
     * @return the "purchaseContractNameLikeFront"
     */
    public String getPurchaseContractNameLikeFront() {
        return purchaseContractNameLikeFront;
    }

    /**
     * Setter method of "purchaseContractNameLikeFront".
     * 
     * @param purchaseContractNameLikeFront Set in "purchaseContractNameLikeFront".
     */
    public void setPurchaseContractNameLikeFront(String purchaseContractNameLikeFront) {
        this.purchaseContractNameLikeFront = purchaseContractNameLikeFront;
    }

    /**
     * Getter method of "purchaseNameLikeFront".
     * 
     * @return the "purchaseNameLikeFront"
     */
    public String getPurchaseNameLikeFront() {
        return purchaseNameLikeFront;
    }

    /**
     * Setter method of "purchaseNameLikeFront".
     * 
     * @param purchaseNameLikeFront Set in "purchaseNameLikeFront".
     */
    public void setPurchaseNameLikeFront(String purchaseNameLikeFront) {
        this.purchaseNameLikeFront = purchaseNameLikeFront;
    }

    /**
     * Getter method of "purchaseAddress1LikeFront".
     * 
     * @return the "purchaseAddress1LikeFront"
     */
    public String getPurchaseAddress1LikeFront() {
        return purchaseAddress1LikeFront;
    }

    /**
     * Setter method of "purchaseAddress1LikeFront".
     * 
     * @param purchaseAddress1LikeFront Set in "purchaseAddress1LikeFront".
     */
    public void setPurchaseAddress1LikeFront(String purchaseAddress1LikeFront) {
        this.purchaseAddress1LikeFront = purchaseAddress1LikeFront;
    }

    /**
     * Getter method of "purchaseAddress2LikeFront".
     * 
     * @return the "purchaseAddress2LikeFront"
     */
    public String getPurchaseAddress2LikeFront() {
        return purchaseAddress2LikeFront;
    }

    /**
     * Setter method of "purchaseAddress2LikeFront".
     * 
     * @param purchaseAddress2LikeFront Set in "purchaseAddress2LikeFront".
     */
    public void setPurchaseAddress2LikeFront(String purchaseAddress2LikeFront) {
        this.purchaseAddress2LikeFront = purchaseAddress2LikeFront;
    }

    /**
     * Getter method of "purchaseAddress3LikeFront".
     * 
     * @return the "purchaseAddress3LikeFront"
     */
    public String getPurchaseAddress3LikeFront() {
        return purchaseAddress3LikeFront;
    }

    /**
     * Setter method of "purchaseAddress3LikeFront".
     * 
     * @param purchaseAddress3LikeFront Set in "purchaseAddress3LikeFront".
     */
    public void setPurchaseAddress3LikeFront(String purchaseAddress3LikeFront) {
        this.purchaseAddress3LikeFront = purchaseAddress3LikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
