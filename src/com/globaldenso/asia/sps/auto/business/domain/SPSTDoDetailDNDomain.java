/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


public class SPSTDoDetailDNDomain   {
	private static final long serialVersionUID = 1L;
	 private String databaseName;
	private BigDecimal doId;
	private String sPn;
	private String dPn;
	private String pnRevision;
	private String pnShipmentStatus;
	private String chgCigmaDoNo;
	private String ctrlNo;
	private BigDecimal currentOrderQty;
	private BigDecimal qtyBox;
	private String chgReason;
	private String itemDesc;
	private String unitOfMeasure;
	private String model;
	private BigDecimal originalQty;
	private BigDecimal previousQty;
	private BigDecimal noOfBoxes;
	private String rcvLane;
	private String whLocation;
	private String urgentOrderFlg;
	private String mailFlg;
	private String createDscId;
	private String createDate;
	private String createTime;
	private String lastUpdateDscId;
	private String lastUpdateDate;
	private String lastUpdateTime;
	private String kanbanType;
	private String remark1;
	private String remark2;
	private String remark3;
	private String cPn;
	private String sProcCd;
	private String kanbanCycle;
	
	private String deliveryDate;
	
	private String spsDoNo;
	
	public BigDecimal getDoId() {
		return doId;
	}
	public void setDoId(BigDecimal doId) {
		this.doId = doId;
	}
	public String getsPn() {
		return sPn;
	}
	public void setsPn(String sPn) {
		this.sPn = sPn;
	}
	public String getdPn() {
		return dPn;
	}
	public void setdPn(String dPn) {
		this.dPn = dPn;
	}
	public String getPnRevision() {
		return pnRevision;
	}
	public void setPnRevision(String pnRevision) {
		this.pnRevision = pnRevision;
	}
	public String getPnShipmentStatus() {
		return pnShipmentStatus;
	}
	public void setPnShipmentStatus(String pnShipmentStatus) {
		this.pnShipmentStatus = pnShipmentStatus;
	}
	public String getChgCigmaDoNo() {
		return chgCigmaDoNo;
	}
	public void setChgCigmaDoNo(String chgCigmaDoNo) {
		this.chgCigmaDoNo = chgCigmaDoNo;
	}
	public String getCtrlNo() {
		return ctrlNo;
	}
	public void setCtrlNo(String ctrlNo) {
		this.ctrlNo = ctrlNo;
	}
	public BigDecimal getCurrentOrderQty() {
		return currentOrderQty;
	}
	public void setCurrentOrderQty(BigDecimal currentOrderQty) {
		this.currentOrderQty = currentOrderQty;
	}
	public BigDecimal getQtyBox() {
		return qtyBox;
	}
	public void setQtyBox(BigDecimal qtyBox) {
		this.qtyBox = qtyBox;
	}
	public String getChgReason() {
		return chgReason;
	}
	public void setChgReason(String chgReason) {
		this.chgReason = chgReason;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public BigDecimal getOriginalQty() {
		return originalQty;
	}
	public void setOriginalQty(BigDecimal originalQty) {
		this.originalQty = originalQty;
	}
	public BigDecimal getPreviousQty() {
		return previousQty;
	}
	public void setPreviousQty(BigDecimal previousQty) {
		this.previousQty = previousQty;
	}
	public BigDecimal getNoOfBoxes() {
		return noOfBoxes;
	}
	public void setNoOfBoxes(BigDecimal noOfBoxes) {
		this.noOfBoxes = noOfBoxes;
	}
	public String getRcvLane() {
		return rcvLane;
	}
	public void setRcvLane(String rcvLane) {
		this.rcvLane = rcvLane;
	}
	public String getWhLocation() {
		return whLocation;
	}
	public void setWhLocation(String whLocation) {
		this.whLocation = whLocation;
	}
	public String getUrgentOrderFlg() {
		return urgentOrderFlg;
	}
	public void setUrgentOrderFlg(String urgentOrderFlg) {
		this.urgentOrderFlg = urgentOrderFlg;
	}
	public String getMailFlg() {
		return mailFlg;
	}
	public void setMailFlg(String mailFlg) {
		this.mailFlg = mailFlg;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	
	public String getKanbanType() {
		return kanbanType;
	}
	public void setKanbanType(String kanbanType) {
		this.kanbanType = kanbanType;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getRemark3() {
		return remark3;
	}
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	public String getcPn() {
		return cPn;
	}
	public void setcPn(String cPn) {
		this.cPn = cPn;
	}
	public String getsProcCd() {
		return sProcCd;
	}
	public void setsProcCd(String sProcCd) {
		this.sProcCd = sProcCd;
	}
	public String getKanbanCycle() {
		return kanbanCycle;
	}
	public void setKanbanCycle(String kanbanCycle) {
		this.kanbanCycle = kanbanCycle;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
    /**
     * <p>Getter method for spsDoNo.</p>
     *
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }
    /**
     * <p>Setter method for spsDoNo.</p>
     *
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }
    /**
     * <p>Getter method for deliveryDate.</p>
     *
     * @return the deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }
    /**
     * <p>Setter method for deliveryDate.</p>
     *
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
	
}
