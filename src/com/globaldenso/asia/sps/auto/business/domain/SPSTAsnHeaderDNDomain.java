/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


public class SPSTAsnHeaderDNDomain   {
	private static final long serialVersionUID = 1L;
	private String asnNo;
	private String asnStatus;
	private String vendorCd;
	private String sCd;
	private String sPcd;
	private String dCd;
	private String dPcd;
	private String planEtd;
	private String planEta;
	private String actualEtd;
	private String actualEta;
	private String transferCigmaFlg;
	private String numberOfPallet;
	private String tripNo;
	private String truckNo;
	private String pdfFileId;
	private String createdInvoiceFlag;
	private String createDatetime;
	private String lastUpdateDscId;
	//private String lastUpdateDatetime;
	private String receiveByScanFlag;
	private String dnDnTrnFlg;
	private String createDscId;
    private Boolean isSuccess;
    private String msgError;
	private String databaseName;
	private String actualEtdTime;
	private String planEtaTime;
	private String actualEtaTime;
	private String planEtdTime;
	private String receivingStatus;
	private String tm;
	private String mode;
	private String processFlag;
    private List<SPSTAsnDetailDNDomain> listAsnDet;
    
    private String lastUpdateDate;
    private String lastUpdateTime;
    
	public String getAsnNo() {
		return asnNo;
	}
	public void setAsnNo(String asnNo) {
		this.asnNo = asnNo;
	}
	public String getAsnStatus() {
		return asnStatus;
	}
	public void setAsnStatus(String asnStatus) {
		this.asnStatus = asnStatus;
	}
	public String getVendorCd() {
		return vendorCd;
	}
	public void setVendorCd(String vendorCd) {
		this.vendorCd = vendorCd;
	}
	public String getsCd() {
		return sCd;
	}
	public void setsCd(String sCd) {
		this.sCd = sCd;
	}
	public String getsPcd() {
		return sPcd;
	}
	public void setsPcd(String sPcd) {
		this.sPcd = sPcd;
	}
	public String getdCd() {
		return dCd;
	}
	public void setdCd(String dCd) {
		this.dCd = dCd;
	}
	public String getdPcd() {
		return dPcd;
	}
	public void setdPcd(String dPcd) {
		this.dPcd = dPcd;
	}
	public String getPlanEtd() {
		return planEtd;
	}
	public void setPlanEtd(String planEtd) {
		this.planEtd = planEtd;
	}
	public String getPlanEta() {
		return planEta;
	}
	public void setPlanEta(String planEta) {
		this.planEta = planEta;
	}
	public String getActualEtd() {
		return actualEtd;
	}
	public void setActualEtd(String actualEtd) {
		this.actualEtd = actualEtd;
	}
	public String getActualEta() {
		return actualEta;
	}
	public void setActualEta(String actualEta) {
		this.actualEta = actualEta;
	}
	public String getTransferCigmaFlg() {
		return transferCigmaFlg;
	}
	public void setTransferCigmaFlg(String transferCigmaFlg) {
		this.transferCigmaFlg = transferCigmaFlg;
	}
	public String getNumberOfPallet() {
		return numberOfPallet;
	}
	public void setNumberOfPallet(String numberOfPallet) {
		this.numberOfPallet = numberOfPallet;
	}
	public String getTripNo() {
		return tripNo;
	}
	public String getTruckNo() {
	    return truckNo;
	}
	public void setTripNo(String tripNo) {
		this.tripNo = tripNo;
	}
	public String getPdfFileId() {
		return pdfFileId;
	}
	public void setPdfFileId(String pdfFileId) {
		this.pdfFileId = pdfFileId;
	}
	public String getCreatedInvoiceFlag() {
		return createdInvoiceFlag;
	}
	public void setCreatedInvoiceFlag(String createdInvoiceFlag) {
		this.createdInvoiceFlag = createdInvoiceFlag;
	}
	public String getCreateDatetime() {
		return createDatetime;
	}
	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	/*
	public String getLastUpdateDatetime() {
		return lastUpdateDatetime;
	}
	public void setLastUpdateDatetime(String lastUpdateDatetime) {
		this.lastUpdateDatetime = lastUpdateDatetime;
	}
	*/
	public String getReceiveByScanFlag() {
		return receiveByScanFlag;
	}
	public void setReceiveByScanFlag(String receiveByScanFlag) {
		this.receiveByScanFlag = receiveByScanFlag;
	}
	public String getDnDnTrnFlg() {
		return dnDnTrnFlg;
	}
	public void setDnDnTrnFlg(String dnDnTrnFlg) {
		this.dnDnTrnFlg = dnDnTrnFlg;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<SPSTAsnDetailDNDomain> getListAsnDet() {
		return listAsnDet;
	}
	public void setListAsnDet(List<SPSTAsnDetailDNDomain> listAsnDet) {
		this.listAsnDet = listAsnDet;
	}
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getActualEtdTime() {
		return actualEtdTime;
	}
	public void setActualEtdTime(String actualEtdTime) {
		this.actualEtdTime = actualEtdTime;
	}
	public String getPlanEtdTime() {
		return planEtdTime;
	}
	public void setPlanEtdTime(String planEtdTime) {
		this.planEtdTime = planEtdTime;
	}
	public String getReceivingStatus() {
		return receivingStatus;
	}
	public void setReceivingStatus(String receivingStatus) {
		this.receivingStatus = receivingStatus;
	}
	public String getTm() {
		return tm;
	}
	public void setTm(String tm) {
		this.tm = tm;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}
	public String getPlanEtaTime() {
		return planEtaTime;
	}
	public void setPlanEtaTime(String planEtaTime) {
		this.planEtaTime = planEtaTime;
	}
	public String getActualEtaTime() {
		return actualEtaTime;
	}
	public void setActualEtaTime(String actualEtaTime) {
		this.actualEtaTime = actualEtaTime;
	}
    /**
     * <p>Getter method for lastUpdateDate.</p>
     *
     * @return the lastUpdateDate
     */
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }
    /**
     * <p>Setter method for lastUpdateDate.</p>
     *
     * @param lastUpdateDate Set for lastUpdateDate
     */
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
    /**
     * <p>Getter method for lastUpdateTime.</p>
     *
     * @return the lastUpdateTime
     */
    public String getLastUpdateTime() {
        return lastUpdateTime;
    }
    /**
     * <p>Setter method for lastUpdateTime.</p>
     *
     * @param lastUpdateTime Set for lastUpdateTime
     */
    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
    
}
