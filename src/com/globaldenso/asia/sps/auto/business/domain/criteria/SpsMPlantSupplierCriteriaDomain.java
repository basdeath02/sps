/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMPlantSupplier".<br />
 * Table overview: SPS_M_PLANT_SUPPLIER<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMPlantSupplierCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /**
     * Supplier company DSC ID
     */
    private String sCd;

    /**
     * Supplier plant name
     */
    private String plantName;

    /**
     * Address
     */
    private String address;

    /**
     * Telephone no.
     */
    private String telephone;

    /**
     * Fax no.
     */
    private String fax;

    /**
     * Payment term
     */
    private BigDecimal paymentTerm;

    /**
     * Supplier user DSC ID
     */
    private String contactPerson;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create datetime
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Supplier plant code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * Supplier company DSC ID(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier plant name(condition whether the column value at the beginning is equal to the value)
     */
    private String plantNameLikeFront;

    /**
     * Address(condition whether the column value at the beginning is equal to the value)
     */
    private String addressLikeFront;

    /**
     * Telephone no.(condition whether the column value at the beginning is equal to the value)
     */
    private String telephoneLikeFront;

    /**
     * Fax no.(condition whether the column value at the beginning is equal to the value)
     */
    private String faxLikeFront;

    /**
     * Supplier user DSC ID(condition whether the column value at the beginning is equal to the value)
     */
    private String contactPersonLikeFront;

    /**
     * Is active(condition whether the column value at the beginning is equal to the value)
     */
    private String isActiveLikeFront;

    /**
     * Create user(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Create datetime(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Create datetime(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * Last update user(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Last update datetime(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Last update datetime(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMPlantSupplierCriteriaDomain() {
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "plantName".
     * 
     * @return the "plantName"
     */
    public String getPlantName() {
        return plantName;
    }

    /**
     * Setter method of "plantName".
     * 
     * @param plantName Set in "plantName".
     */
    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    /**
     * Getter method of "address".
     * 
     * @return the "address"
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter method of "address".
     * 
     * @param address Set in "address".
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Getter method of "telephone".
     * 
     * @return the "telephone"
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone".
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "fax".
     * 
     * @return the "fax"
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax".
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "paymentTerm".
     * 
     * @return the "paymentTerm"
     */
    public BigDecimal getPaymentTerm() {
        return paymentTerm;
    }

    /**
     * Setter method of "paymentTerm".
     * 
     * @param paymentTerm Set in "paymentTerm".
     */
    public void setPaymentTerm(BigDecimal paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     * Getter method of "contactPerson".
     * 
     * @return the "contactPerson"
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Setter method of "contactPerson".
     * 
     * @param contactPerson Set in "contactPerson".
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Getter method of "isActive".
     * 
     * @return the "isActive"
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive".
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "plantNameLikeFront".
     * 
     * @return the "plantNameLikeFront"
     */
    public String getPlantNameLikeFront() {
        return plantNameLikeFront;
    }

    /**
     * Setter method of "plantNameLikeFront".
     * 
     * @param plantNameLikeFront Set in "plantNameLikeFront".
     */
    public void setPlantNameLikeFront(String plantNameLikeFront) {
        this.plantNameLikeFront = plantNameLikeFront;
    }

    /**
     * Getter method of "addressLikeFront".
     * 
     * @return the "addressLikeFront"
     */
    public String getAddressLikeFront() {
        return addressLikeFront;
    }

    /**
     * Setter method of "addressLikeFront".
     * 
     * @param addressLikeFront Set in "addressLikeFront".
     */
    public void setAddressLikeFront(String addressLikeFront) {
        this.addressLikeFront = addressLikeFront;
    }

    /**
     * Getter method of "telephoneLikeFront".
     * 
     * @return the "telephoneLikeFront"
     */
    public String getTelephoneLikeFront() {
        return telephoneLikeFront;
    }

    /**
     * Setter method of "telephoneLikeFront".
     * 
     * @param telephoneLikeFront Set in "telephoneLikeFront".
     */
    public void setTelephoneLikeFront(String telephoneLikeFront) {
        this.telephoneLikeFront = telephoneLikeFront;
    }

    /**
     * Getter method of "faxLikeFront".
     * 
     * @return the "faxLikeFront"
     */
    public String getFaxLikeFront() {
        return faxLikeFront;
    }

    /**
     * Setter method of "faxLikeFront".
     * 
     * @param faxLikeFront Set in "faxLikeFront".
     */
    public void setFaxLikeFront(String faxLikeFront) {
        this.faxLikeFront = faxLikeFront;
    }

    /**
     * Getter method of "contactPersonLikeFront".
     * 
     * @return the "contactPersonLikeFront"
     */
    public String getContactPersonLikeFront() {
        return contactPersonLikeFront;
    }

    /**
     * Setter method of "contactPersonLikeFront".
     * 
     * @param contactPersonLikeFront Set in "contactPersonLikeFront".
     */
    public void setContactPersonLikeFront(String contactPersonLikeFront) {
        this.contactPersonLikeFront = contactPersonLikeFront;
    }

    /**
     * Getter method of "isActiveLikeFront".
     * 
     * @return the "isActiveLikeFront"
     */
    public String getIsActiveLikeFront() {
        return isActiveLikeFront;
    }

    /**
     * Setter method of "isActiveLikeFront".
     * 
     * @param isActiveLikeFront Set in "isActiveLikeFront".
     */
    public void setIsActiveLikeFront(String isActiveLikeFront) {
        this.isActiveLikeFront = isActiveLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
