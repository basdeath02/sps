/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMUserDenso"<br />
 * Table overview: SPS_M_USER_DENSO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 11:25:05<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserDensoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User identify number
     */
    private String dscId;

    /**
     * User belong to DENSO company
     */
    private String dCd;

    /**
     * User belong to DENSO plant
     */
    private String dPcd;

    /**
     * User employee number
     */
    private String employeeCd;

    /**
     * Create ASN (Shot Shipment) and Cancel ASN, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlCreateCancelAsnFlag;

    /**
     * Create Invoice with CN or Use Temp Price, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlCreateInvoiceFlag;

    /**
     * Abnormal Transfer data(Pending P/O, D/O aleady create ASN, Supplier Information not found), 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlPedpoDoalcasnUnmatpnFlg;

    /**
     * Revise Shipping QTY, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlReviseAsnFlag;

    /**
     * Other case of abnormal Transfer data, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlAbnormalTransferFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMUserDensoDomain() {
    }

    /**
     * Getter method of "dscId"
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId"
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "employeeCd"
     * 
     * @return the employeeCd
     */
    public String getEmployeeCd() {
        return employeeCd;
    }

    /**
     * Setter method of "employeeCd"
     * 
     * @param employeeCd Set in "employeeCd".
     */
    public void setEmployeeCd(String employeeCd) {
        this.employeeCd = employeeCd;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlag"
     * 
     * @return the emlCreateCancelAsnFlag
     */
    public String getEmlCreateCancelAsnFlag() {
        return emlCreateCancelAsnFlag;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlag"
     * 
     * @param emlCreateCancelAsnFlag Set in "emlCreateCancelAsnFlag".
     */
    public void setEmlCreateCancelAsnFlag(String emlCreateCancelAsnFlag) {
        this.emlCreateCancelAsnFlag = emlCreateCancelAsnFlag;
    }

    /**
     * Getter method of "emlCreateInvoiceFlag"
     * 
     * @return the emlCreateInvoiceFlag
     */
    public String getEmlCreateInvoiceFlag() {
        return emlCreateInvoiceFlag;
    }

    /**
     * Setter method of "emlCreateInvoiceFlag"
     * 
     * @param emlCreateInvoiceFlag Set in "emlCreateInvoiceFlag".
     */
    public void setEmlCreateInvoiceFlag(String emlCreateInvoiceFlag) {
        this.emlCreateInvoiceFlag = emlCreateInvoiceFlag;
    }

    /**
     * Getter method of "emlPedpoDoalcasnUnmatpnFlg"
     * 
     * @return the emlPedpoDoalcasnUnmatpnFlg
     */
    public String getEmlPedpoDoalcasnUnmatpnFlg() {
        return emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Setter method of "emlPedpoDoalcasnUnmatpnFlg"
     * 
     * @param emlPedpoDoalcasnUnmatpnFlg Set in "emlPedpoDoalcasnUnmatpnFlg".
     */
    public void setEmlPedpoDoalcasnUnmatpnFlg(String emlPedpoDoalcasnUnmatpnFlg) {
        this.emlPedpoDoalcasnUnmatpnFlg = emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Getter method of "emlReviseAsnFlag"
     * 
     * @return the emlReviseAsnFlag
     */
    public String getEmlReviseAsnFlag() {
        return emlReviseAsnFlag;
    }

    /**
     * Setter method of "emlReviseAsnFlag"
     * 
     * @param emlReviseAsnFlag Set in "emlReviseAsnFlag".
     */
    public void setEmlReviseAsnFlag(String emlReviseAsnFlag) {
        this.emlReviseAsnFlag = emlReviseAsnFlag;
    }

    /**
     * Getter method of "emlAbnormalTransferFlg"
     * 
     * @return the emlAbnormalTransferFlg
     */
    public String getEmlAbnormalTransferFlg() {
        return emlAbnormalTransferFlg;
    }

    /**
     * Setter method of "emlAbnormalTransferFlg"
     * 
     * @param emlAbnormalTransferFlg Set in "emlAbnormalTransferFlg".
     */
    public void setEmlAbnormalTransferFlg(String emlAbnormalTransferFlg) {
        this.emlAbnormalTransferFlg = emlAbnormalTransferFlg;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
