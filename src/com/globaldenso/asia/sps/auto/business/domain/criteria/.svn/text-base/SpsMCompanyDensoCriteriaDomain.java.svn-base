/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/26       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMCompanyDenso".<br />
 * Table overview: SPS_M_COMPANY_DENSO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/26 14:21:53<br />
 * 
 * This module generated automatically in 2015/05/26 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanyDensoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * DENSO company identify number
     */
    private String dCd;

    /**
     * DENSO company DSC ID number
     */
    private String dscIdComCd;

    /**
     * Company name
     */
    private String companyName;

    /**
     * CIGMA Schema Code
     */
    private String schemaCd;

    /**
     * DENSO company address 1
     */
    private String address1;

    /**
     * DENSO company address 2
     */
    private String address2;

    /**
     * DENSO company address 3
     */
    private String address3;

    /**
     * Telephone number
     */
    private String telephone;

    /**
     * Fax number
     */
    private String fax;

    /**
     * Limitation years for purge order transaction
     */
    private BigDecimal orderKeepYear;

    /**
     * Limitation years for purge invoice transaction
     */
    private BigDecimal invoiceKeepYear;

    /**
     * File path value for annoucement message source file
     */
    private String filePath;

    /**
     * Temporary path for keep backup file after import message complete
     */
    private String tempPath;

    /**
     * Coordinated Universal Time
     */
    private String utc;

    /**
     * 0 : Inactive, 1 : Active
     */
    private String isActive;

    /**
     * DENSO tax id
     */
    private String dTaxId;

    /**
     * Tax code
     */
    private String taxCd;

    /**
     * Refer to currency master table to keep a currency type value
     */
    private String currencyCd;

    /**
     * 0 : Warning, 1 : Error
     */
    private String groupInvoiceErrorType;

    /**
     * D: Date, M: Month, Y: Year
     */
    private String groupInvoiceType;

    /**
     * 1: Not generate 2: Generate by D/O No. 3: Generate by DENSO Part No.
     */
    private String qrType;

    /**
     * Notice P/O Perio
     */
    private BigDecimal noticePoPeriod;

    /**
     * Flag for show/hide KANBAN Sequence in KANBAN Delivery Order report; 1: show, 2: not show
     */
    private String reportKanbanSeqFlg;

    /**
     * Flag for show/hide Terms and Condition of P/O report
     */
    private String poTermConditionFlg;

    /**
     * Flag for show/hide DENSO Plant on report; 0: Warning, 1: Error
     */
    private String groupAsnErrorType;

    /**
     * Flag for show/hide DENSO Plant Code of P/O report; 0: not show, 1: show
     */
    private String showDensoPlantFlg;

    /**
     * Flag allow to group invoice difference plant; 0: Not Allow, 1: Allow
     */
    private String groupInvoiceDiffPlantDenso;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * DENSO company identify number(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO company DSC ID number(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdComCdLikeFront;

    /**
     * Company name(condition whether the column value at the beginning is equal to the value)
     */
    private String companyNameLikeFront;

    /**
     * CIGMA Schema Code(condition whether the column value at the beginning is equal to the value)
     */
    private String schemaCdLikeFront;

    /**
     * DENSO company address 1(condition whether the column value at the beginning is equal to the value)
     */
    private String address1LikeFront;

    /**
     * DENSO company address 2(condition whether the column value at the beginning is equal to the value)
     */
    private String address2LikeFront;

    /**
     * DENSO company address 3(condition whether the column value at the beginning is equal to the value)
     */
    private String address3LikeFront;

    /**
     * Telephone number(condition whether the column value at the beginning is equal to the value)
     */
    private String telephoneLikeFront;

    /**
     * Fax number(condition whether the column value at the beginning is equal to the value)
     */
    private String faxLikeFront;

    /**
     * File path value for annoucement message source file(condition whether the column value at the beginning is equal to the value)
     */
    private String filePathLikeFront;

    /**
     * Temporary path for keep backup file after import message complete(condition whether the column value at the beginning is equal to the value)
     */
    private String tempPathLikeFront;

    /**
     * Coordinated Universal Time(condition whether the column value at the beginning is equal to the value)
     */
    private String utcLikeFront;

    /**
     * 0 : Inactive, 1 : Active(condition whether the column value at the beginning is equal to the value)
     */
    private String isActiveLikeFront;

    /**
     * DENSO tax id(condition whether the column value at the beginning is equal to the value)
     */
    private String dTaxIdLikeFront;

    /**
     * Tax code(condition whether the column value at the beginning is equal to the value)
     */
    private String taxCdLikeFront;

    /**
     * Refer to currency master table to keep a currency type value(condition whether the column value at the beginning is equal to the value)
     */
    private String currencyCdLikeFront;

    /**
     * 0 : Warning, 1 : Error(condition whether the column value at the beginning is equal to the value)
     */
    private String groupInvoiceErrorTypeLikeFront;

    /**
     * D: Date, M: Month, Y: Year(condition whether the column value at the beginning is equal to the value)
     */
    private String groupInvoiceTypeLikeFront;

    /**
     * 1: Not generate 2: Generate by D/O No. 3: Generate by DENSO Part No.(condition whether the column value at the beginning is equal to the value)
     */
    private String qrTypeLikeFront;

    /**
     * Flag for show/hide KANBAN Sequence in KANBAN Delivery Order report; 1: show, 2: not show(condition whether the column value at the beginning is equal to the value)
     */
    private String reportKanbanSeqFlgLikeFront;

    /**
     * Flag for show/hide Terms and Condition of P/O report(condition whether the column value at the beginning is equal to the value)
     */
    private String poTermConditionFlgLikeFront;

    /**
     * Flag for show/hide DENSO Plant on report; 0: Warning, 1: Error(condition whether the column value at the beginning is equal to the value)
     */
    private String groupAsnErrorTypeLikeFront;

    /**
     * Flag for show/hide DENSO Plant Code of P/O report; 0: not show, 1: show(condition whether the column value at the beginning is equal to the value)
     */
    private String showDensoPlantFlgLikeFront;

    /**
     * Flag allow to group invoice difference plant; 0: Not Allow, 1: Allow(condition whether the column value at the beginning is equal to the value)
     */
    private String groupInvoiceDiffPlantDensoLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMCompanyDensoCriteriaDomain() {
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dscIdComCd".
     * 
     * @return the "dscIdComCd"
     */
    public String getDscIdComCd() {
        return dscIdComCd;
    }

    /**
     * Setter method of "dscIdComCd".
     * 
     * @param dscIdComCd Set in "dscIdComCd".
     */
    public void setDscIdComCd(String dscIdComCd) {
        this.dscIdComCd = dscIdComCd;
    }

    /**
     * Getter method of "companyName".
     * 
     * @return the "companyName"
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Setter method of "companyName".
     * 
     * @param companyName Set in "companyName".
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Getter method of "schemaCd".
     * 
     * @return the "schemaCd"
     */
    public String getSchemaCd() {
        return schemaCd;
    }

    /**
     * Setter method of "schemaCd".
     * 
     * @param schemaCd Set in "schemaCd".
     */
    public void setSchemaCd(String schemaCd) {
        this.schemaCd = schemaCd;
    }

    /**
     * Getter method of "address1".
     * 
     * @return the "address1"
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Setter method of "address1".
     * 
     * @param address1 Set in "address1".
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * Getter method of "address2".
     * 
     * @return the "address2"
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Setter method of "address2".
     * 
     * @param address2 Set in "address2".
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * Getter method of "address3".
     * 
     * @return the "address3"
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Setter method of "address3".
     * 
     * @param address3 Set in "address3".
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * Getter method of "telephone".
     * 
     * @return the "telephone"
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone".
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "fax".
     * 
     * @return the "fax"
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax".
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "orderKeepYear".
     * 
     * @return the "orderKeepYear"
     */
    public BigDecimal getOrderKeepYear() {
        return orderKeepYear;
    }

    /**
     * Setter method of "orderKeepYear".
     * 
     * @param orderKeepYear Set in "orderKeepYear".
     */
    public void setOrderKeepYear(BigDecimal orderKeepYear) {
        this.orderKeepYear = orderKeepYear;
    }

    /**
     * Getter method of "invoiceKeepYear".
     * 
     * @return the "invoiceKeepYear"
     */
    public BigDecimal getInvoiceKeepYear() {
        return invoiceKeepYear;
    }

    /**
     * Setter method of "invoiceKeepYear".
     * 
     * @param invoiceKeepYear Set in "invoiceKeepYear".
     */
    public void setInvoiceKeepYear(BigDecimal invoiceKeepYear) {
        this.invoiceKeepYear = invoiceKeepYear;
    }

    /**
     * Getter method of "filePath".
     * 
     * @return the "filePath"
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Setter method of "filePath".
     * 
     * @param filePath Set in "filePath".
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Getter method of "tempPath".
     * 
     * @return the "tempPath"
     */
    public String getTempPath() {
        return tempPath;
    }

    /**
     * Setter method of "tempPath".
     * 
     * @param tempPath Set in "tempPath".
     */
    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    /**
     * Getter method of "utc".
     * 
     * @return the "utc"
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Setter method of "utc".
     * 
     * @param utc Set in "utc".
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * Getter method of "isActive".
     * 
     * @return the "isActive"
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive".
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "dTaxId".
     * 
     * @return the "dTaxId"
     */
    public String getDTaxId() {
        return dTaxId;
    }

    /**
     * Setter method of "dTaxId".
     * 
     * @param dTaxId Set in "dTaxId".
     */
    public void setDTaxId(String dTaxId) {
        this.dTaxId = dTaxId;
    }

    /**
     * Getter method of "taxCd".
     * 
     * @return the "taxCd"
     */
    public String getTaxCd() {
        return taxCd;
    }

    /**
     * Setter method of "taxCd".
     * 
     * @param taxCd Set in "taxCd".
     */
    public void setTaxCd(String taxCd) {
        this.taxCd = taxCd;
    }

    /**
     * Getter method of "currencyCd".
     * 
     * @return the "currencyCd"
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * Setter method of "currencyCd".
     * 
     * @param currencyCd Set in "currencyCd".
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * Getter method of "groupInvoiceErrorType".
     * 
     * @return the "groupInvoiceErrorType"
     */
    public String getGroupInvoiceErrorType() {
        return groupInvoiceErrorType;
    }

    /**
     * Setter method of "groupInvoiceErrorType".
     * 
     * @param groupInvoiceErrorType Set in "groupInvoiceErrorType".
     */
    public void setGroupInvoiceErrorType(String groupInvoiceErrorType) {
        this.groupInvoiceErrorType = groupInvoiceErrorType;
    }

    /**
     * Getter method of "groupInvoiceType".
     * 
     * @return the "groupInvoiceType"
     */
    public String getGroupInvoiceType() {
        return groupInvoiceType;
    }

    /**
     * Setter method of "groupInvoiceType".
     * 
     * @param groupInvoiceType Set in "groupInvoiceType".
     */
    public void setGroupInvoiceType(String groupInvoiceType) {
        this.groupInvoiceType = groupInvoiceType;
    }

    /**
     * Getter method of "qrType".
     * 
     * @return the "qrType"
     */
    public String getQrType() {
        return qrType;
    }

    /**
     * Setter method of "qrType".
     * 
     * @param qrType Set in "qrType".
     */
    public void setQrType(String qrType) {
        this.qrType = qrType;
    }

    /**
     * Getter method of "noticePoPeriod".
     * 
     * @return the "noticePoPeriod"
     */
    public BigDecimal getNoticePoPeriod() {
        return noticePoPeriod;
    }

    /**
     * Setter method of "noticePoPeriod".
     * 
     * @param noticePoPeriod Set in "noticePoPeriod".
     */
    public void setNoticePoPeriod(BigDecimal noticePoPeriod) {
        this.noticePoPeriod = noticePoPeriod;
    }

    /**
     * Getter method of "reportKanbanSeqFlg".
     * 
     * @return the "reportKanbanSeqFlg"
     */
    public String getReportKanbanSeqFlg() {
        return reportKanbanSeqFlg;
    }

    /**
     * Setter method of "reportKanbanSeqFlg".
     * 
     * @param reportKanbanSeqFlg Set in "reportKanbanSeqFlg".
     */
    public void setReportKanbanSeqFlg(String reportKanbanSeqFlg) {
        this.reportKanbanSeqFlg = reportKanbanSeqFlg;
    }

    /**
     * Getter method of "poTermConditionFlg".
     * 
     * @return the "poTermConditionFlg"
     */
    public String getPoTermConditionFlg() {
        return poTermConditionFlg;
    }

    /**
     * Setter method of "poTermConditionFlg".
     * 
     * @param poTermConditionFlg Set in "poTermConditionFlg".
     */
    public void setPoTermConditionFlg(String poTermConditionFlg) {
        this.poTermConditionFlg = poTermConditionFlg;
    }

    /**
     * Getter method of "groupAsnErrorType".
     * 
     * @return the "groupAsnErrorType"
     */
    public String getGroupAsnErrorType() {
        return groupAsnErrorType;
    }

    /**
     * Setter method of "groupAsnErrorType".
     * 
     * @param groupAsnErrorType Set in "groupAsnErrorType".
     */
    public void setGroupAsnErrorType(String groupAsnErrorType) {
        this.groupAsnErrorType = groupAsnErrorType;
    }

    /**
     * Getter method of "showDensoPlantFlg".
     * 
     * @return the "showDensoPlantFlg"
     */
    public String getShowDensoPlantFlg() {
        return showDensoPlantFlg;
    }

    /**
     * Setter method of "showDensoPlantFlg".
     * 
     * @param showDensoPlantFlg Set in "showDensoPlantFlg".
     */
    public void setShowDensoPlantFlg(String showDensoPlantFlg) {
        this.showDensoPlantFlg = showDensoPlantFlg;
    }

    /**
     * Getter method of "groupInvoiceDiffPlantDenso".
     * 
     * @return the "groupInvoiceDiffPlantDenso"
     */
    public String getGroupInvoiceDiffPlantDenso() {
        return groupInvoiceDiffPlantDenso;
    }

    /**
     * Setter method of "groupInvoiceDiffPlantDenso".
     * 
     * @param groupInvoiceDiffPlantDenso Set in "groupInvoiceDiffPlantDenso".
     */
    public void setGroupInvoiceDiffPlantDenso(String groupInvoiceDiffPlantDenso) {
        this.groupInvoiceDiffPlantDenso = groupInvoiceDiffPlantDenso;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dscIdComCdLikeFront".
     * 
     * @return the "dscIdComCdLikeFront"
     */
    public String getDscIdComCdLikeFront() {
        return dscIdComCdLikeFront;
    }

    /**
     * Setter method of "dscIdComCdLikeFront".
     * 
     * @param dscIdComCdLikeFront Set in "dscIdComCdLikeFront".
     */
    public void setDscIdComCdLikeFront(String dscIdComCdLikeFront) {
        this.dscIdComCdLikeFront = dscIdComCdLikeFront;
    }

    /**
     * Getter method of "companyNameLikeFront".
     * 
     * @return the "companyNameLikeFront"
     */
    public String getCompanyNameLikeFront() {
        return companyNameLikeFront;
    }

    /**
     * Setter method of "companyNameLikeFront".
     * 
     * @param companyNameLikeFront Set in "companyNameLikeFront".
     */
    public void setCompanyNameLikeFront(String companyNameLikeFront) {
        this.companyNameLikeFront = companyNameLikeFront;
    }

    /**
     * Getter method of "schemaCdLikeFront".
     * 
     * @return the "schemaCdLikeFront"
     */
    public String getSchemaCdLikeFront() {
        return schemaCdLikeFront;
    }

    /**
     * Setter method of "schemaCdLikeFront".
     * 
     * @param schemaCdLikeFront Set in "schemaCdLikeFront".
     */
    public void setSchemaCdLikeFront(String schemaCdLikeFront) {
        this.schemaCdLikeFront = schemaCdLikeFront;
    }

    /**
     * Getter method of "address1LikeFront".
     * 
     * @return the "address1LikeFront"
     */
    public String getAddress1LikeFront() {
        return address1LikeFront;
    }

    /**
     * Setter method of "address1LikeFront".
     * 
     * @param address1LikeFront Set in "address1LikeFront".
     */
    public void setAddress1LikeFront(String address1LikeFront) {
        this.address1LikeFront = address1LikeFront;
    }

    /**
     * Getter method of "address2LikeFront".
     * 
     * @return the "address2LikeFront"
     */
    public String getAddress2LikeFront() {
        return address2LikeFront;
    }

    /**
     * Setter method of "address2LikeFront".
     * 
     * @param address2LikeFront Set in "address2LikeFront".
     */
    public void setAddress2LikeFront(String address2LikeFront) {
        this.address2LikeFront = address2LikeFront;
    }

    /**
     * Getter method of "address3LikeFront".
     * 
     * @return the "address3LikeFront"
     */
    public String getAddress3LikeFront() {
        return address3LikeFront;
    }

    /**
     * Setter method of "address3LikeFront".
     * 
     * @param address3LikeFront Set in "address3LikeFront".
     */
    public void setAddress3LikeFront(String address3LikeFront) {
        this.address3LikeFront = address3LikeFront;
    }

    /**
     * Getter method of "telephoneLikeFront".
     * 
     * @return the "telephoneLikeFront"
     */
    public String getTelephoneLikeFront() {
        return telephoneLikeFront;
    }

    /**
     * Setter method of "telephoneLikeFront".
     * 
     * @param telephoneLikeFront Set in "telephoneLikeFront".
     */
    public void setTelephoneLikeFront(String telephoneLikeFront) {
        this.telephoneLikeFront = telephoneLikeFront;
    }

    /**
     * Getter method of "faxLikeFront".
     * 
     * @return the "faxLikeFront"
     */
    public String getFaxLikeFront() {
        return faxLikeFront;
    }

    /**
     * Setter method of "faxLikeFront".
     * 
     * @param faxLikeFront Set in "faxLikeFront".
     */
    public void setFaxLikeFront(String faxLikeFront) {
        this.faxLikeFront = faxLikeFront;
    }

    /**
     * Getter method of "filePathLikeFront".
     * 
     * @return the "filePathLikeFront"
     */
    public String getFilePathLikeFront() {
        return filePathLikeFront;
    }

    /**
     * Setter method of "filePathLikeFront".
     * 
     * @param filePathLikeFront Set in "filePathLikeFront".
     */
    public void setFilePathLikeFront(String filePathLikeFront) {
        this.filePathLikeFront = filePathLikeFront;
    }

    /**
     * Getter method of "tempPathLikeFront".
     * 
     * @return the "tempPathLikeFront"
     */
    public String getTempPathLikeFront() {
        return tempPathLikeFront;
    }

    /**
     * Setter method of "tempPathLikeFront".
     * 
     * @param tempPathLikeFront Set in "tempPathLikeFront".
     */
    public void setTempPathLikeFront(String tempPathLikeFront) {
        this.tempPathLikeFront = tempPathLikeFront;
    }

    /**
     * Getter method of "utcLikeFront".
     * 
     * @return the "utcLikeFront"
     */
    public String getUtcLikeFront() {
        return utcLikeFront;
    }

    /**
     * Setter method of "utcLikeFront".
     * 
     * @param utcLikeFront Set in "utcLikeFront".
     */
    public void setUtcLikeFront(String utcLikeFront) {
        this.utcLikeFront = utcLikeFront;
    }

    /**
     * Getter method of "isActiveLikeFront".
     * 
     * @return the "isActiveLikeFront"
     */
    public String getIsActiveLikeFront() {
        return isActiveLikeFront;
    }

    /**
     * Setter method of "isActiveLikeFront".
     * 
     * @param isActiveLikeFront Set in "isActiveLikeFront".
     */
    public void setIsActiveLikeFront(String isActiveLikeFront) {
        this.isActiveLikeFront = isActiveLikeFront;
    }

    /**
     * Getter method of "dTaxIdLikeFront".
     * 
     * @return the "dTaxIdLikeFront"
     */
    public String getDTaxIdLikeFront() {
        return dTaxIdLikeFront;
    }

    /**
     * Setter method of "dTaxIdLikeFront".
     * 
     * @param dTaxIdLikeFront Set in "dTaxIdLikeFront".
     */
    public void setDTaxIdLikeFront(String dTaxIdLikeFront) {
        this.dTaxIdLikeFront = dTaxIdLikeFront;
    }

    /**
     * Getter method of "taxCdLikeFront".
     * 
     * @return the "taxCdLikeFront"
     */
    public String getTaxCdLikeFront() {
        return taxCdLikeFront;
    }

    /**
     * Setter method of "taxCdLikeFront".
     * 
     * @param taxCdLikeFront Set in "taxCdLikeFront".
     */
    public void setTaxCdLikeFront(String taxCdLikeFront) {
        this.taxCdLikeFront = taxCdLikeFront;
    }

    /**
     * Getter method of "currencyCdLikeFront".
     * 
     * @return the "currencyCdLikeFront"
     */
    public String getCurrencyCdLikeFront() {
        return currencyCdLikeFront;
    }

    /**
     * Setter method of "currencyCdLikeFront".
     * 
     * @param currencyCdLikeFront Set in "currencyCdLikeFront".
     */
    public void setCurrencyCdLikeFront(String currencyCdLikeFront) {
        this.currencyCdLikeFront = currencyCdLikeFront;
    }

    /**
     * Getter method of "groupInvoiceErrorTypeLikeFront".
     * 
     * @return the "groupInvoiceErrorTypeLikeFront"
     */
    public String getGroupInvoiceErrorTypeLikeFront() {
        return groupInvoiceErrorTypeLikeFront;
    }

    /**
     * Setter method of "groupInvoiceErrorTypeLikeFront".
     * 
     * @param groupInvoiceErrorTypeLikeFront Set in "groupInvoiceErrorTypeLikeFront".
     */
    public void setGroupInvoiceErrorTypeLikeFront(String groupInvoiceErrorTypeLikeFront) {
        this.groupInvoiceErrorTypeLikeFront = groupInvoiceErrorTypeLikeFront;
    }

    /**
     * Getter method of "groupInvoiceTypeLikeFront".
     * 
     * @return the "groupInvoiceTypeLikeFront"
     */
    public String getGroupInvoiceTypeLikeFront() {
        return groupInvoiceTypeLikeFront;
    }

    /**
     * Setter method of "groupInvoiceTypeLikeFront".
     * 
     * @param groupInvoiceTypeLikeFront Set in "groupInvoiceTypeLikeFront".
     */
    public void setGroupInvoiceTypeLikeFront(String groupInvoiceTypeLikeFront) {
        this.groupInvoiceTypeLikeFront = groupInvoiceTypeLikeFront;
    }

    /**
     * Getter method of "qrTypeLikeFront".
     * 
     * @return the "qrTypeLikeFront"
     */
    public String getQrTypeLikeFront() {
        return qrTypeLikeFront;
    }

    /**
     * Setter method of "qrTypeLikeFront".
     * 
     * @param qrTypeLikeFront Set in "qrTypeLikeFront".
     */
    public void setQrTypeLikeFront(String qrTypeLikeFront) {
        this.qrTypeLikeFront = qrTypeLikeFront;
    }

    /**
     * Getter method of "reportKanbanSeqFlgLikeFront".
     * 
     * @return the "reportKanbanSeqFlgLikeFront"
     */
    public String getReportKanbanSeqFlgLikeFront() {
        return reportKanbanSeqFlgLikeFront;
    }

    /**
     * Setter method of "reportKanbanSeqFlgLikeFront".
     * 
     * @param reportKanbanSeqFlgLikeFront Set in "reportKanbanSeqFlgLikeFront".
     */
    public void setReportKanbanSeqFlgLikeFront(String reportKanbanSeqFlgLikeFront) {
        this.reportKanbanSeqFlgLikeFront = reportKanbanSeqFlgLikeFront;
    }

    /**
     * Getter method of "poTermConditionFlgLikeFront".
     * 
     * @return the "poTermConditionFlgLikeFront"
     */
    public String getPoTermConditionFlgLikeFront() {
        return poTermConditionFlgLikeFront;
    }

    /**
     * Setter method of "poTermConditionFlgLikeFront".
     * 
     * @param poTermConditionFlgLikeFront Set in "poTermConditionFlgLikeFront".
     */
    public void setPoTermConditionFlgLikeFront(String poTermConditionFlgLikeFront) {
        this.poTermConditionFlgLikeFront = poTermConditionFlgLikeFront;
    }

    /**
     * Getter method of "groupAsnErrorTypeLikeFront".
     * 
     * @return the "groupAsnErrorTypeLikeFront"
     */
    public String getGroupAsnErrorTypeLikeFront() {
        return groupAsnErrorTypeLikeFront;
    }

    /**
     * Setter method of "groupAsnErrorTypeLikeFront".
     * 
     * @param groupAsnErrorTypeLikeFront Set in "groupAsnErrorTypeLikeFront".
     */
    public void setGroupAsnErrorTypeLikeFront(String groupAsnErrorTypeLikeFront) {
        this.groupAsnErrorTypeLikeFront = groupAsnErrorTypeLikeFront;
    }

    /**
     * Getter method of "showDensoPlantFlgLikeFront".
     * 
     * @return the "showDensoPlantFlgLikeFront"
     */
    public String getShowDensoPlantFlgLikeFront() {
        return showDensoPlantFlgLikeFront;
    }

    /**
     * Setter method of "showDensoPlantFlgLikeFront".
     * 
     * @param showDensoPlantFlgLikeFront Set in "showDensoPlantFlgLikeFront".
     */
    public void setShowDensoPlantFlgLikeFront(String showDensoPlantFlgLikeFront) {
        this.showDensoPlantFlgLikeFront = showDensoPlantFlgLikeFront;
    }

    /**
     * Getter method of "groupInvoiceDiffPlantDensoLikeFront".
     * 
     * @return the "groupInvoiceDiffPlantDensoLikeFront"
     */
    public String getGroupInvoiceDiffPlantDensoLikeFront() {
        return groupInvoiceDiffPlantDensoLikeFront;
    }

    /**
     * Setter method of "groupInvoiceDiffPlantDensoLikeFront".
     * 
     * @param groupInvoiceDiffPlantDensoLikeFront Set in "groupInvoiceDiffPlantDensoLikeFront".
     */
    public void setGroupInvoiceDiffPlantDensoLikeFront(String groupInvoiceDiffPlantDensoLikeFront) {
        this.groupInvoiceDiffPlantDensoLikeFront = groupInvoiceDiffPlantDensoLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
