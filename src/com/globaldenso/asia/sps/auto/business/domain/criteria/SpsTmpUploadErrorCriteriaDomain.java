/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/04/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpUploadError".<br />
 * Table overview: SPS_TMP_UPLOAD_ERROR<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/04/07 12:25:27<br />
 * 
 * This module generated automatically in 2559/04/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadErrorCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User DSC ID that upload data to system
     */
    private String userDscId;

    /**
     * User login session code
     */
    private String sessionCd;

    /**
     * Source file line number
     */
    private BigDecimal lineNo;

    /**
     * Upload data error group
     */
    private String miscType;

    /**
     * Upload data error code
     */
    private String miscCd;

    /**
     * 0 : Import DENSO user
1 : Import supplier user
2 : Import supplier information
     */
    private String functionCode;

    /**
     * Message parameter
     */
    private String msgParam;

    /**
     * Upload operation timestamp
     */
    private Timestamp uploadDatetime;

    /**
     * User DSC ID that upload data to system(condition whether the column value at the beginning is equal to the value)
     */
    private String userDscIdLikeFront;

    /**
     * User login session code(condition whether the column value at the beginning is equal to the value)
     */
    private String sessionCdLikeFront;

    /**
     * Upload data error group(condition whether the column value at the beginning is equal to the value)
     */
    private String miscTypeLikeFront;

    /**
     * Upload data error code(condition whether the column value at the beginning is equal to the value)
     */
    private String miscCdLikeFront;

    /**
     * 0 : Import DENSO user
1 : Import supplier user
2 : Import supplier information(condition whether the column value at the beginning is equal to the value)
     */
    private String functionCodeLikeFront;

    /**
     * Message parameter(condition whether the column value at the beginning is equal to the value)
     */
    private String msgParamLikeFront;

    /**
     * Upload operation timestamp(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp uploadDatetimeGreaterThanEqual;

    /**
     * Upload operation timestamp(condition whether the column value is less than or equal to the value)
     */
    private Timestamp uploadDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTmpUploadErrorCriteriaDomain() {
    }

    /**
     * Getter method of "userDscId".
     * 
     * @return the "userDscId"
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId".
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd".
     * 
     * @return the "sessionCd"
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd".
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo".
     * 
     * @return the "lineNo"
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo".
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "miscType".
     * 
     * @return the "miscType"
     */
    public String getMiscType() {
        return miscType;
    }

    /**
     * Setter method of "miscType".
     * 
     * @param miscType Set in "miscType".
     */
    public void setMiscType(String miscType) {
        this.miscType = miscType;
    }

    /**
     * Getter method of "miscCd".
     * 
     * @return the "miscCd"
     */
    public String getMiscCd() {
        return miscCd;
    }

    /**
     * Setter method of "miscCd".
     * 
     * @param miscCd Set in "miscCd".
     */
    public void setMiscCd(String miscCd) {
        this.miscCd = miscCd;
    }

    /**
     * Getter method of "functionCode".
     * 
     * @return the "functionCode"
     */
    public String getFunctionCode() {
        return functionCode;
    }

    /**
     * Setter method of "functionCode".
     * 
     * @param functionCode Set in "functionCode".
     */
    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    /**
     * Getter method of "msgParam".
     * 
     * @return the "msgParam"
     */
    public String getMsgParam() {
        return msgParam;
    }

    /**
     * Setter method of "msgParam".
     * 
     * @param msgParam Set in "msgParam".
     */
    public void setMsgParam(String msgParam) {
        this.msgParam = msgParam;
    }

    /**
     * Getter method of "uploadDatetime".
     * 
     * @return the "uploadDatetime"
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime".
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "userDscIdLikeFront".
     * 
     * @return the "userDscIdLikeFront"
     */
    public String getUserDscIdLikeFront() {
        return userDscIdLikeFront;
    }

    /**
     * Setter method of "userDscIdLikeFront".
     * 
     * @param userDscIdLikeFront Set in "userDscIdLikeFront".
     */
    public void setUserDscIdLikeFront(String userDscIdLikeFront) {
        this.userDscIdLikeFront = userDscIdLikeFront;
    }

    /**
     * Getter method of "sessionCdLikeFront".
     * 
     * @return the "sessionCdLikeFront"
     */
    public String getSessionCdLikeFront() {
        return sessionCdLikeFront;
    }

    /**
     * Setter method of "sessionCdLikeFront".
     * 
     * @param sessionCdLikeFront Set in "sessionCdLikeFront".
     */
    public void setSessionCdLikeFront(String sessionCdLikeFront) {
        this.sessionCdLikeFront = sessionCdLikeFront;
    }

    /**
     * Getter method of "miscTypeLikeFront".
     * 
     * @return the "miscTypeLikeFront"
     */
    public String getMiscTypeLikeFront() {
        return miscTypeLikeFront;
    }

    /**
     * Setter method of "miscTypeLikeFront".
     * 
     * @param miscTypeLikeFront Set in "miscTypeLikeFront".
     */
    public void setMiscTypeLikeFront(String miscTypeLikeFront) {
        this.miscTypeLikeFront = miscTypeLikeFront;
    }

    /**
     * Getter method of "miscCdLikeFront".
     * 
     * @return the "miscCdLikeFront"
     */
    public String getMiscCdLikeFront() {
        return miscCdLikeFront;
    }

    /**
     * Setter method of "miscCdLikeFront".
     * 
     * @param miscCdLikeFront Set in "miscCdLikeFront".
     */
    public void setMiscCdLikeFront(String miscCdLikeFront) {
        this.miscCdLikeFront = miscCdLikeFront;
    }

    /**
     * Getter method of "functionCodeLikeFront".
     * 
     * @return the "functionCodeLikeFront"
     */
    public String getFunctionCodeLikeFront() {
        return functionCodeLikeFront;
    }

    /**
     * Setter method of "functionCodeLikeFront".
     * 
     * @param functionCodeLikeFront Set in "functionCodeLikeFront".
     */
    public void setFunctionCodeLikeFront(String functionCodeLikeFront) {
        this.functionCodeLikeFront = functionCodeLikeFront;
    }

    /**
     * Getter method of "msgParamLikeFront".
     * 
     * @return the "msgParamLikeFront"
     */
    public String getMsgParamLikeFront() {
        return msgParamLikeFront;
    }

    /**
     * Setter method of "msgParamLikeFront".
     * 
     * @param msgParamLikeFront Set in "msgParamLikeFront".
     */
    public void setMsgParamLikeFront(String msgParamLikeFront) {
        this.msgParamLikeFront = msgParamLikeFront;
    }

    /**
     * Getter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @return the "uploadDatetimeGreaterThanEqual"
     */
    public Timestamp getUploadDatetimeGreaterThanEqual() {
        return uploadDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @param uploadDatetimeGreaterThanEqual Set in "uploadDatetimeGreaterThanEqual".
     */
    public void setUploadDatetimeGreaterThanEqual(Timestamp uploadDatetimeGreaterThanEqual) {
        this.uploadDatetimeGreaterThanEqual = uploadDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeLessThanEqual".
     * 
     * @return the "uploadDatetimeLessThanEqual"
     */
    public Timestamp getUploadDatetimeLessThanEqual() {
        return uploadDatetimeLessThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeLessThanEqual".
     * 
     * @param uploadDatetimeLessThanEqual Set in "uploadDatetimeLessThanEqual".
     */
    public void setUploadDatetimeLessThanEqual(Timestamp uploadDatetimeLessThanEqual) {
        this.uploadDatetimeLessThanEqual = uploadDatetimeLessThanEqual;
    }

}
