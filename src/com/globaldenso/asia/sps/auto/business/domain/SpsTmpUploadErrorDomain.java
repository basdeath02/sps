/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/04/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTmpUploadError"<br />
 * Table overview: SPS_TMP_UPLOAD_ERROR<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/04/07 12:25:27<br />
 * 
 * This module generated automatically in 2559/04/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadErrorDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User DSC ID that upload data to system
     */
    private String userDscId;

    /**
     * User login session code
     */
    private String sessionCd;

    /**
     * Source file line number
     */
    private BigDecimal lineNo;

    /**
     * Upload data error group
     */
    private String miscType;

    /**
     * Upload data error code
     */
    private String miscCd;

    /**
     * 0 : Import DENSO user
1 : Import supplier user
2 : Import supplier information
     */
    private String functionCode;

    /**
     * Message parameter
     */
    private String msgParam;

    /**
     * Upload operation timestamp
     */
    private Timestamp uploadDatetime;

    /**
     * Default constructor
     */
    public SpsTmpUploadErrorDomain() {
    }

    /**
     * Getter method of "userDscId"
     * 
     * @return the userDscId
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId"
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd"
     * 
     * @return the sessionCd
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd"
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo"
     * 
     * @return the lineNo
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo"
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "miscType"
     * 
     * @return the miscType
     */
    public String getMiscType() {
        return miscType;
    }

    /**
     * Setter method of "miscType"
     * 
     * @param miscType Set in "miscType".
     */
    public void setMiscType(String miscType) {
        this.miscType = miscType;
    }

    /**
     * Getter method of "miscCd"
     * 
     * @return the miscCd
     */
    public String getMiscCd() {
        return miscCd;
    }

    /**
     * Setter method of "miscCd"
     * 
     * @param miscCd Set in "miscCd".
     */
    public void setMiscCd(String miscCd) {
        this.miscCd = miscCd;
    }

    /**
     * Getter method of "functionCode"
     * 
     * @return the functionCode
     */
    public String getFunctionCode() {
        return functionCode;
    }

    /**
     * Setter method of "functionCode"
     * 
     * @param functionCode Set in "functionCode".
     */
    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    /**
     * Getter method of "msgParam"
     * 
     * @return the msgParam
     */
    public String getMsgParam() {
        return msgParam;
    }

    /**
     * Setter method of "msgParam"
     * 
     * @param msgParam Set in "msgParam".
     */
    public void setMsgParam(String msgParam) {
        this.msgParam = msgParam;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

}
