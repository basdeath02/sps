/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/07/09       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTInvoice"<br />
 * Table overview: SPS_T_INVOICE<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/07/09 18:24:35<br />
 * 
 * This module generated automatically in 2015/07/09 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running number of Invoice
     */
    private BigDecimal invoiceId;

    /**
     * DENSO Company Name
     */
    private String dCompanyName;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * DENSO TAX Identify
     */
    private String dTaxId;

    /**
     * DENSO Plant Address
     */
    private String dpAddress;

    /**
     * DENSO Currency Code
     */
    private String dCurrencyCd;

    /**
     * Supplier Company Name
     */
    private String sCompanyName;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * Supplier Company Code
     */
    private String sCd;

    /**
     * Supplier TAX Identify
     */
    private String sTaxId;

    /**
     * Supplier Company Address 1
     */
    private String sAddress1;

    /**
     * Supplier Company Address 2
     */
    private String sAddress2;

    /**
     * Supplier Company Address 3
     */
    private String sAddress3;

    /**
     * Supplier Currency Code
     */
    private String sCurrencyCd;

    /**
     * Invoice Number
     */
    private String invoiceNo;

    /**
     * Invoice Issued Date
     */
    private Date invoiceDate;

    /**
     * ISS : Issued, ISC : Issued with CN, CCL : Cancel Invoice, PGL : Post to GL, HOP : Hold payment, PDP : Paid
     */
    private String invoiceStatus;

    /**
     * "1": VAT/GST , "0": None VAT/GST
     */
    private String vatType;

    /**
     * Invoice VAT Rate 
     */
    private BigDecimal vatRate;

    /**
     * Invoice Base Amount
     */
    private BigDecimal sBaseAmount;

    /**
     * Invoice VAT Amount
     */
    private BigDecimal sVatAmount;

    /**
     * Invoice Total Amount
     */
    private BigDecimal sTotalAmount;

    /**
     * Difference Base Amount of DENSO price and Supplier price
     */
    private BigDecimal totalDiffBaseAmount;

    /**
     * Difference Total Amount of DENSO price and Supplier price
     */
    private BigDecimal totalDiffAmount;

    /**
     * Invoice Base Amount
     */
    private BigDecimal totalBaseAmount;

    /**
     * Difference VAT Amount of DENSO price and Supplier price
     */
    private BigDecimal totalDiffVatAmount;

    /**
     * Invoice VAT Amount
     */
    private BigDecimal totalVatAmount;

    /**
     * Invoice Total Amount
     */
    private BigDecimal totalAmount;

    /**
     * Payment Date from JDE
     */
    private Date paymentDate;

    /**
     * "1": Already transfer to JDE , "0": not yet transfer
     */
    private String transferJdeFlag;

    /**
     * Cover Page Document File ID in File Manager
     */
    private String coverPageFileId;

    /**
     * Cover Page No
     */
    private String coverPageNo;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTInvoiceDomain() {
    }

    /**
     * Getter method of "invoiceId"
     * 
     * @return the invoiceId
     */
    public BigDecimal getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method of "invoiceId"
     * 
     * @param invoiceId Set in "invoiceId".
     */
    public void setInvoiceId(BigDecimal invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * Getter method of "dCompanyName"
     * 
     * @return the dCompanyName
     */
    public String getDCompanyName() {
        return dCompanyName;
    }

    /**
     * Setter method of "dCompanyName"
     * 
     * @param dCompanyName Set in "dCompanyName".
     */
    public void setDCompanyName(String dCompanyName) {
        this.dCompanyName = dCompanyName;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "dTaxId"
     * 
     * @return the dTaxId
     */
    public String getDTaxId() {
        return dTaxId;
    }

    /**
     * Setter method of "dTaxId"
     * 
     * @param dTaxId Set in "dTaxId".
     */
    public void setDTaxId(String dTaxId) {
        this.dTaxId = dTaxId;
    }

    /**
     * Getter method of "dpAddress"
     * 
     * @return the dpAddress
     */
    public String getDpAddress() {
        return dpAddress;
    }

    /**
     * Setter method of "dpAddress"
     * 
     * @param dpAddress Set in "dpAddress".
     */
    public void setDpAddress(String dpAddress) {
        this.dpAddress = dpAddress;
    }

    /**
     * Getter method of "dCurrencyCd"
     * 
     * @return the dCurrencyCd
     */
    public String getDCurrencyCd() {
        return dCurrencyCd;
    }

    /**
     * Setter method of "dCurrencyCd"
     * 
     * @param dCurrencyCd Set in "dCurrencyCd".
     */
    public void setDCurrencyCd(String dCurrencyCd) {
        this.dCurrencyCd = dCurrencyCd;
    }

    /**
     * Getter method of "sCompanyName"
     * 
     * @return the sCompanyName
     */
    public String getSCompanyName() {
        return sCompanyName;
    }

    /**
     * Setter method of "sCompanyName"
     * 
     * @param sCompanyName Set in "sCompanyName".
     */
    public void setSCompanyName(String sCompanyName) {
        this.sCompanyName = sCompanyName;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sTaxId"
     * 
     * @return the sTaxId
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * Setter method of "sTaxId"
     * 
     * @param sTaxId Set in "sTaxId".
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Getter method of "sAddress1"
     * 
     * @return the sAddress1
     */
    public String getSAddress1() {
        return sAddress1;
    }

    /**
     * Setter method of "sAddress1"
     * 
     * @param sAddress1 Set in "sAddress1".
     */
    public void setSAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    /**
     * Getter method of "sAddress2"
     * 
     * @return the sAddress2
     */
    public String getSAddress2() {
        return sAddress2;
    }

    /**
     * Setter method of "sAddress2"
     * 
     * @param sAddress2 Set in "sAddress2".
     */
    public void setSAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    /**
     * Getter method of "sAddress3"
     * 
     * @return the sAddress3
     */
    public String getSAddress3() {
        return sAddress3;
    }

    /**
     * Setter method of "sAddress3"
     * 
     * @param sAddress3 Set in "sAddress3".
     */
    public void setSAddress3(String sAddress3) {
        this.sAddress3 = sAddress3;
    }

    /**
     * Getter method of "sCurrencyCd"
     * 
     * @return the sCurrencyCd
     */
    public String getSCurrencyCd() {
        return sCurrencyCd;
    }

    /**
     * Setter method of "sCurrencyCd"
     * 
     * @param sCurrencyCd Set in "sCurrencyCd".
     */
    public void setSCurrencyCd(String sCurrencyCd) {
        this.sCurrencyCd = sCurrencyCd;
    }

    /**
     * Getter method of "invoiceNo"
     * 
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Setter method of "invoiceNo"
     * 
     * @param invoiceNo Set in "invoiceNo".
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * Getter method of "invoiceDate"
     * 
     * @return the invoiceDate
     */
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Setter method of "invoiceDate"
     * 
     * @param invoiceDate Set in "invoiceDate".
     */
    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * Getter method of "invoiceStatus"
     * 
     * @return the invoiceStatus
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * Setter method of "invoiceStatus"
     * 
     * @param invoiceStatus Set in "invoiceStatus".
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * Getter method of "vatType"
     * 
     * @return the vatType
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * Setter method of "vatType"
     * 
     * @param vatType Set in "vatType".
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }

    /**
     * Getter method of "vatRate"
     * 
     * @return the vatRate
     */
    public BigDecimal getVatRate() {
        return vatRate;
    }

    /**
     * Setter method of "vatRate"
     * 
     * @param vatRate Set in "vatRate".
     */
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * Getter method of "sBaseAmount"
     * 
     * @return the sBaseAmount
     */
    public BigDecimal getSBaseAmount() {
        return sBaseAmount;
    }

    /**
     * Setter method of "sBaseAmount"
     * 
     * @param sBaseAmount Set in "sBaseAmount".
     */
    public void setSBaseAmount(BigDecimal sBaseAmount) {
        this.sBaseAmount = sBaseAmount;
    }

    /**
     * Getter method of "sVatAmount"
     * 
     * @return the sVatAmount
     */
    public BigDecimal getSVatAmount() {
        return sVatAmount;
    }

    /**
     * Setter method of "sVatAmount"
     * 
     * @param sVatAmount Set in "sVatAmount".
     */
    public void setSVatAmount(BigDecimal sVatAmount) {
        this.sVatAmount = sVatAmount;
    }

    /**
     * Getter method of "sTotalAmount"
     * 
     * @return the sTotalAmount
     */
    public BigDecimal getSTotalAmount() {
        return sTotalAmount;
    }

    /**
     * Setter method of "sTotalAmount"
     * 
     * @param sTotalAmount Set in "sTotalAmount".
     */
    public void setSTotalAmount(BigDecimal sTotalAmount) {
        this.sTotalAmount = sTotalAmount;
    }

    /**
     * Getter method of "totalDiffBaseAmount"
     * 
     * @return the totalDiffBaseAmount
     */
    public BigDecimal getTotalDiffBaseAmount() {
        return totalDiffBaseAmount;
    }

    /**
     * Setter method of "totalDiffBaseAmount"
     * 
     * @param totalDiffBaseAmount Set in "totalDiffBaseAmount".
     */
    public void setTotalDiffBaseAmount(BigDecimal totalDiffBaseAmount) {
        this.totalDiffBaseAmount = totalDiffBaseAmount;
    }

    /**
     * Getter method of "totalDiffAmount"
     * 
     * @return the totalDiffAmount
     */
    public BigDecimal getTotalDiffAmount() {
        return totalDiffAmount;
    }

    /**
     * Setter method of "totalDiffAmount"
     * 
     * @param totalDiffAmount Set in "totalDiffAmount".
     */
    public void setTotalDiffAmount(BigDecimal totalDiffAmount) {
        this.totalDiffAmount = totalDiffAmount;
    }

    /**
     * Getter method of "totalBaseAmount"
     * 
     * @return the totalBaseAmount
     */
    public BigDecimal getTotalBaseAmount() {
        return totalBaseAmount;
    }

    /**
     * Setter method of "totalBaseAmount"
     * 
     * @param totalBaseAmount Set in "totalBaseAmount".
     */
    public void setTotalBaseAmount(BigDecimal totalBaseAmount) {
        this.totalBaseAmount = totalBaseAmount;
    }

    /**
     * Getter method of "totalDiffVatAmount"
     * 
     * @return the totalDiffVatAmount
     */
    public BigDecimal getTotalDiffVatAmount() {
        return totalDiffVatAmount;
    }

    /**
     * Setter method of "totalDiffVatAmount"
     * 
     * @param totalDiffVatAmount Set in "totalDiffVatAmount".
     */
    public void setTotalDiffVatAmount(BigDecimal totalDiffVatAmount) {
        this.totalDiffVatAmount = totalDiffVatAmount;
    }

    /**
     * Getter method of "totalVatAmount"
     * 
     * @return the totalVatAmount
     */
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    /**
     * Setter method of "totalVatAmount"
     * 
     * @param totalVatAmount Set in "totalVatAmount".
     */
    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    /**
     * Getter method of "totalAmount"
     * 
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Setter method of "totalAmount"
     * 
     * @param totalAmount Set in "totalAmount".
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Getter method of "paymentDate"
     * 
     * @return the paymentDate
     */
    public Date getPaymentDate() {
        return paymentDate;
    }

    /**
     * Setter method of "paymentDate"
     * 
     * @param paymentDate Set in "paymentDate".
     */
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * Getter method of "transferJdeFlag"
     * 
     * @return the transferJdeFlag
     */
    public String getTransferJdeFlag() {
        return transferJdeFlag;
    }

    /**
     * Setter method of "transferJdeFlag"
     * 
     * @param transferJdeFlag Set in "transferJdeFlag".
     */
    public void setTransferJdeFlag(String transferJdeFlag) {
        this.transferJdeFlag = transferJdeFlag;
    }

    /**
     * Getter method of "coverPageFileId"
     * 
     * @return the coverPageFileId
     */
    public String getCoverPageFileId() {
        return coverPageFileId;
    }

    /**
     * Setter method of "coverPageFileId"
     * 
     * @param coverPageFileId Set in "coverPageFileId".
     */
    public void setCoverPageFileId(String coverPageFileId) {
        this.coverPageFileId = coverPageFileId;
    }

    /**
     * Getter method of "coverPageNo"
     * 
     * @return the coverPageNo
     */
    public String getCoverPageNo() {
        return coverPageNo;
    }

    /**
     * Setter method of "coverPageNo"
     * 
     * @param coverPageNo Set in "coverPageNo".
     */
    public void setCoverPageNo(String coverPageNo) {
        this.coverPageNo = coverPageNo;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
