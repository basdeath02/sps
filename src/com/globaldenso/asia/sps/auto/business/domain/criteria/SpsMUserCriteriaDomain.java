/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMUser".<br />
 * Table overview: SPS_M_USER<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * DSC_ID
     */
    private String dscId;

    /**
     * DEPARTMENT_NAME
     */
    private String departmentName;

    /**
     * FIRST_NAME
     */
    private String firstName;

    /**
     * MIDDLE_NAME
     */
    private String middleName;

    /**
     * LAST_NAME
     */
    private String lastName;

    /**
     * EMAIL
     */
    private String email;

    /**
     * TELEPHONE
     */
    private String telephone;

    /**
     * IS_ACTIVE
     */
    private String isActive;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * USER_TYPE
     */
    private String userType;

    /**
     * DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdLikeFront;

    /**
     * DEPARTMENT_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String departmentNameLikeFront;

    /**
     * FIRST_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String firstNameLikeFront;

    /**
     * MIDDLE_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String middleNameLikeFront;

    /**
     * LAST_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String lastNameLikeFront;

    /**
     * EMAIL(condition whether the column value at the beginning is equal to the value)
     */
    private String emailLikeFront;

    /**
     * TELEPHONE(condition whether the column value at the beginning is equal to the value)
     */
    private String telephoneLikeFront;

    /**
     * IS_ACTIVE(condition whether the column value at the beginning is equal to the value)
     */
    private String isActiveLikeFront;

    /**
     * CREATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * LAST_UPDATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * USER_TYPE(condition whether the column value at the beginning is equal to the value)
     */
    private String userTypeLikeFront;

    /**
     * Default constructor
     */
    public SpsMUserCriteriaDomain() {
    }

    /**
     * Getter method of "dscId".
     * 
     * @return the "dscId"
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId".
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "departmentName".
     * 
     * @return the "departmentName"
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Setter method of "departmentName".
     * 
     * @param departmentName Set in "departmentName".
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Getter method of "firstName".
     * 
     * @return the "firstName"
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName".
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName".
     * 
     * @return the "middleName"
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName".
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName".
     * 
     * @return the "lastName"
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName".
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email".
     * 
     * @return the "email"
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email".
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone".
     * 
     * @return the "telephone"
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone".
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "isActive".
     * 
     * @return the "isActive"
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive".
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "userType".
     * 
     * @return the "userType"
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Setter method of "userType".
     * 
     * @param userType Set in "userType".
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * Getter method of "dscIdLikeFront".
     * 
     * @return the "dscIdLikeFront"
     */
    public String getDscIdLikeFront() {
        return dscIdLikeFront;
    }

    /**
     * Setter method of "dscIdLikeFront".
     * 
     * @param dscIdLikeFront Set in "dscIdLikeFront".
     */
    public void setDscIdLikeFront(String dscIdLikeFront) {
        this.dscIdLikeFront = dscIdLikeFront;
    }

    /**
     * Getter method of "departmentNameLikeFront".
     * 
     * @return the "departmentNameLikeFront"
     */
    public String getDepartmentNameLikeFront() {
        return departmentNameLikeFront;
    }

    /**
     * Setter method of "departmentNameLikeFront".
     * 
     * @param departmentNameLikeFront Set in "departmentNameLikeFront".
     */
    public void setDepartmentNameLikeFront(String departmentNameLikeFront) {
        this.departmentNameLikeFront = departmentNameLikeFront;
    }

    /**
     * Getter method of "firstNameLikeFront".
     * 
     * @return the "firstNameLikeFront"
     */
    public String getFirstNameLikeFront() {
        return firstNameLikeFront;
    }

    /**
     * Setter method of "firstNameLikeFront".
     * 
     * @param firstNameLikeFront Set in "firstNameLikeFront".
     */
    public void setFirstNameLikeFront(String firstNameLikeFront) {
        this.firstNameLikeFront = firstNameLikeFront;
    }

    /**
     * Getter method of "middleNameLikeFront".
     * 
     * @return the "middleNameLikeFront"
     */
    public String getMiddleNameLikeFront() {
        return middleNameLikeFront;
    }

    /**
     * Setter method of "middleNameLikeFront".
     * 
     * @param middleNameLikeFront Set in "middleNameLikeFront".
     */
    public void setMiddleNameLikeFront(String middleNameLikeFront) {
        this.middleNameLikeFront = middleNameLikeFront;
    }

    /**
     * Getter method of "lastNameLikeFront".
     * 
     * @return the "lastNameLikeFront"
     */
    public String getLastNameLikeFront() {
        return lastNameLikeFront;
    }

    /**
     * Setter method of "lastNameLikeFront".
     * 
     * @param lastNameLikeFront Set in "lastNameLikeFront".
     */
    public void setLastNameLikeFront(String lastNameLikeFront) {
        this.lastNameLikeFront = lastNameLikeFront;
    }

    /**
     * Getter method of "emailLikeFront".
     * 
     * @return the "emailLikeFront"
     */
    public String getEmailLikeFront() {
        return emailLikeFront;
    }

    /**
     * Setter method of "emailLikeFront".
     * 
     * @param emailLikeFront Set in "emailLikeFront".
     */
    public void setEmailLikeFront(String emailLikeFront) {
        this.emailLikeFront = emailLikeFront;
    }

    /**
     * Getter method of "telephoneLikeFront".
     * 
     * @return the "telephoneLikeFront"
     */
    public String getTelephoneLikeFront() {
        return telephoneLikeFront;
    }

    /**
     * Setter method of "telephoneLikeFront".
     * 
     * @param telephoneLikeFront Set in "telephoneLikeFront".
     */
    public void setTelephoneLikeFront(String telephoneLikeFront) {
        this.telephoneLikeFront = telephoneLikeFront;
    }

    /**
     * Getter method of "isActiveLikeFront".
     * 
     * @return the "isActiveLikeFront"
     */
    public String getIsActiveLikeFront() {
        return isActiveLikeFront;
    }

    /**
     * Setter method of "isActiveLikeFront".
     * 
     * @param isActiveLikeFront Set in "isActiveLikeFront".
     */
    public void setIsActiveLikeFront(String isActiveLikeFront) {
        this.isActiveLikeFront = isActiveLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Getter method of "userTypeLikeFront".
     * 
     * @return the "userTypeLikeFront"
     */
    public String getUserTypeLikeFront() {
        return userTypeLikeFront;
    }

    /**
     * Setter method of "userTypeLikeFront".
     * 
     * @param userTypeLikeFront Set in "userTypeLikeFront".
     */
    public void setUserTypeLikeFront(String userTypeLikeFront) {
        this.userTypeLikeFront = userTypeLikeFront;
    }

}
