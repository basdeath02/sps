/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/01/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMUnitOfMeasure"<br />
 * Table overview: SPS_M_UNIT_OF_MEASURE<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/01/05 09:25:07<br />
 * 
 * This module generated automatically in 2016/01/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUnitOfMeasureDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No for Unit of Measure
     */
    private String unitOfMeasureId;

    /**
     * Unit of Measure Name
     */
    private String unitOfMeasureName;

    /**
     * 0 : Not display decimal point (On ASN Maintenance screen)
1 : Display decimal point (On ASN Maintenance screen)
     */
    private String displayDecimalPointFlg;

    /**
     * 0 : Disable to input actual Shipping Box Qty
1 : Enable to input actual Shipping Box Qty
     */
    private String editShippingBoxFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMUnitOfMeasureDomain() {
    }

    /**
     * Getter method of "unitOfMeasureId"
     * 
     * @return the unitOfMeasureId
     */
    public String getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    /**
     * Setter method of "unitOfMeasureId"
     * 
     * @param unitOfMeasureId Set in "unitOfMeasureId".
     */
    public void setUnitOfMeasureId(String unitOfMeasureId) {
        this.unitOfMeasureId = unitOfMeasureId;
    }

    /**
     * Getter method of "unitOfMeasureName"
     * 
     * @return the unitOfMeasureName
     */
    public String getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    /**
     * Setter method of "unitOfMeasureName"
     * 
     * @param unitOfMeasureName Set in "unitOfMeasureName".
     */
    public void setUnitOfMeasureName(String unitOfMeasureName) {
        this.unitOfMeasureName = unitOfMeasureName;
    }

    /**
     * Getter method of "displayDecimalPointFlg"
     * 
     * @return the displayDecimalPointFlg
     */
    public String getDisplayDecimalPointFlg() {
        return displayDecimalPointFlg;
    }

    /**
     * Setter method of "displayDecimalPointFlg"
     * 
     * @param displayDecimalPointFlg Set in "displayDecimalPointFlg".
     */
    public void setDisplayDecimalPointFlg(String displayDecimalPointFlg) {
        this.displayDecimalPointFlg = displayDecimalPointFlg;
    }

    /**
     * Getter method of "editShippingBoxFlg"
     * 
     * @return the editShippingBoxFlg
     */
    public String getEditShippingBoxFlg() {
        return editShippingBoxFlg;
    }

    /**
     * Setter method of "editShippingBoxFlg"
     * 
     * @param editShippingBoxFlg Set in "editShippingBoxFlg".
     */
    public void setEditShippingBoxFlg(String editShippingBoxFlg) {
        this.editShippingBoxFlg = editShippingBoxFlg;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
