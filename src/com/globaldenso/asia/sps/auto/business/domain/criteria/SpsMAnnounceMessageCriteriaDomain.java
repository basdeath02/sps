/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMAnnounceMessage".<br />
 * Table overview: SPS_M_ANNOUNCE_MESSAGE<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAnnounceMessageCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * EFFECT_START
     */
    private Timestamp effectStart;

    /**
     * EFFECT_END
     */
    private Timestamp effectEnd;

    /**
     * ANNOUNCE_MESSAGE
     */
    private String announceMessage;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * D_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * EFFECT_START(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectStartGreaterThanEqual;

    /**
     * EFFECT_START(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectStartLessThanEqual;

    /**
     * EFFECT_END(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectEndGreaterThanEqual;

    /**
     * EFFECT_END(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectEndLessThanEqual;

    /**
     * ANNOUNCE_MESSAGE(condition whether the column value at the beginning is equal to the value)
     */
    private String announceMessageLikeFront;

    /**
     * CREATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMAnnounceMessageCriteriaDomain() {
    }

    /**
     * Getter method of "seqNo".
     * 
     * @return the "seqNo"
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo".
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "effectStart".
     * 
     * @return the "effectStart"
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart".
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd".
     * 
     * @return the "effectEnd"
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd".
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "announceMessage".
     * 
     * @return the "announceMessage"
     */
    public String getAnnounceMessage() {
        return announceMessage;
    }

    /**
     * Setter method of "announceMessage".
     * 
     * @param announceMessage Set in "announceMessage".
     */
    public void setAnnounceMessage(String announceMessage) {
        this.announceMessage = announceMessage;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "effectStartGreaterThanEqual".
     * 
     * @return the "effectStartGreaterThanEqual"
     */
    public Timestamp getEffectStartGreaterThanEqual() {
        return effectStartGreaterThanEqual;
    }

    /**
     * Setter method of "effectStartGreaterThanEqual".
     * 
     * @param effectStartGreaterThanEqual Set in "effectStartGreaterThanEqual".
     */
    public void setEffectStartGreaterThanEqual(Timestamp effectStartGreaterThanEqual) {
        this.effectStartGreaterThanEqual = effectStartGreaterThanEqual;
    }

    /**
     * Getter method of "effectStartLessThanEqual".
     * 
     * @return the "effectStartLessThanEqual"
     */
    public Timestamp getEffectStartLessThanEqual() {
        return effectStartLessThanEqual;
    }

    /**
     * Setter method of "effectStartLessThanEqual".
     * 
     * @param effectStartLessThanEqual Set in "effectStartLessThanEqual".
     */
    public void setEffectStartLessThanEqual(Timestamp effectStartLessThanEqual) {
        this.effectStartLessThanEqual = effectStartLessThanEqual;
    }

    /**
     * Getter method of "effectEndGreaterThanEqual".
     * 
     * @return the "effectEndGreaterThanEqual"
     */
    public Timestamp getEffectEndGreaterThanEqual() {
        return effectEndGreaterThanEqual;
    }

    /**
     * Setter method of "effectEndGreaterThanEqual".
     * 
     * @param effectEndGreaterThanEqual Set in "effectEndGreaterThanEqual".
     */
    public void setEffectEndGreaterThanEqual(Timestamp effectEndGreaterThanEqual) {
        this.effectEndGreaterThanEqual = effectEndGreaterThanEqual;
    }

    /**
     * Getter method of "effectEndLessThanEqual".
     * 
     * @return the "effectEndLessThanEqual"
     */
    public Timestamp getEffectEndLessThanEqual() {
        return effectEndLessThanEqual;
    }

    /**
     * Setter method of "effectEndLessThanEqual".
     * 
     * @param effectEndLessThanEqual Set in "effectEndLessThanEqual".
     */
    public void setEffectEndLessThanEqual(Timestamp effectEndLessThanEqual) {
        this.effectEndLessThanEqual = effectEndLessThanEqual;
    }

    /**
     * Getter method of "announceMessageLikeFront".
     * 
     * @return the "announceMessageLikeFront"
     */
    public String getAnnounceMessageLikeFront() {
        return announceMessageLikeFront;
    }

    /**
     * Setter method of "announceMessageLikeFront".
     * 
     * @param announceMessageLikeFront Set in "announceMessageLikeFront".
     */
    public void setAnnounceMessageLikeFront(String announceMessageLikeFront) {
        this.announceMessageLikeFront = announceMessageLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

}
