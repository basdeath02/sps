/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/27       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMUserSupplier".<br />
 * Table overview: SPS_M_USER_SUPPLIER<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/27 17:41:40<br />
 * 
 * This module generated automatically in 2015/03/27 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserSupplierCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User identify number
     */
    private String dscId;

    /**
     * Main Desnso company to handle this record (feture use)
     */
    private String dOwner;

    /**
     * User belong to Supplier plant
     */
    private String sPcd;

    /**
     * User belong to Supplier company
     */
    private String sCd;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlUrgentOrderFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlSInfoNotfoundFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlAllowReviseFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlCancelInvoiceFlag;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * User identify number(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdLikeFront;

    /**
     * Main Desnso company to handle this record (feture use)(condition whether the column value at the beginning is equal to the value)
     */
    private String dOwnerLikeFront;

    /**
     * User belong to Supplier plant(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * User belong to Supplier company(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlUrgentOrderFlagLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlSInfoNotfoundFlagLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlAllowReviseFlagLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlCancelInvoiceFlagLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMUserSupplierCriteriaDomain() {
    }

    /**
     * Getter method of "dscId".
     * 
     * @return the "dscId"
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId".
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dOwner".
     * 
     * @return the "dOwner"
     */
    public String getDOwner() {
        return dOwner;
    }

    /**
     * Setter method of "dOwner".
     * 
     * @param dOwner Set in "dOwner".
     */
    public void setDOwner(String dOwner) {
        this.dOwner = dOwner;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "emlUrgentOrderFlag".
     * 
     * @return the "emlUrgentOrderFlag"
     */
    public String getEmlUrgentOrderFlag() {
        return emlUrgentOrderFlag;
    }

    /**
     * Setter method of "emlUrgentOrderFlag".
     * 
     * @param emlUrgentOrderFlag Set in "emlUrgentOrderFlag".
     */
    public void setEmlUrgentOrderFlag(String emlUrgentOrderFlag) {
        this.emlUrgentOrderFlag = emlUrgentOrderFlag;
    }

    /**
     * Getter method of "emlSInfoNotfoundFlag".
     * 
     * @return the "emlSInfoNotfoundFlag"
     */
    public String getEmlSInfoNotfoundFlag() {
        return emlSInfoNotfoundFlag;
    }

    /**
     * Setter method of "emlSInfoNotfoundFlag".
     * 
     * @param emlSInfoNotfoundFlag Set in "emlSInfoNotfoundFlag".
     */
    public void setEmlSInfoNotfoundFlag(String emlSInfoNotfoundFlag) {
        this.emlSInfoNotfoundFlag = emlSInfoNotfoundFlag;
    }

    /**
     * Getter method of "emlAllowReviseFlag".
     * 
     * @return the "emlAllowReviseFlag"
     */
    public String getEmlAllowReviseFlag() {
        return emlAllowReviseFlag;
    }

    /**
     * Setter method of "emlAllowReviseFlag".
     * 
     * @param emlAllowReviseFlag Set in "emlAllowReviseFlag".
     */
    public void setEmlAllowReviseFlag(String emlAllowReviseFlag) {
        this.emlAllowReviseFlag = emlAllowReviseFlag;
    }

    /**
     * Getter method of "emlCancelInvoiceFlag".
     * 
     * @return the "emlCancelInvoiceFlag"
     */
    public String getEmlCancelInvoiceFlag() {
        return emlCancelInvoiceFlag;
    }

    /**
     * Setter method of "emlCancelInvoiceFlag".
     * 
     * @param emlCancelInvoiceFlag Set in "emlCancelInvoiceFlag".
     */
    public void setEmlCancelInvoiceFlag(String emlCancelInvoiceFlag) {
        this.emlCancelInvoiceFlag = emlCancelInvoiceFlag;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dscIdLikeFront".
     * 
     * @return the "dscIdLikeFront"
     */
    public String getDscIdLikeFront() {
        return dscIdLikeFront;
    }

    /**
     * Setter method of "dscIdLikeFront".
     * 
     * @param dscIdLikeFront Set in "dscIdLikeFront".
     */
    public void setDscIdLikeFront(String dscIdLikeFront) {
        this.dscIdLikeFront = dscIdLikeFront;
    }

    /**
     * Getter method of "dOwnerLikeFront".
     * 
     * @return the "dOwnerLikeFront"
     */
    public String getDOwnerLikeFront() {
        return dOwnerLikeFront;
    }

    /**
     * Setter method of "dOwnerLikeFront".
     * 
     * @param dOwnerLikeFront Set in "dOwnerLikeFront".
     */
    public void setDOwnerLikeFront(String dOwnerLikeFront) {
        this.dOwnerLikeFront = dOwnerLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "emlUrgentOrderFlagLikeFront".
     * 
     * @return the "emlUrgentOrderFlagLikeFront"
     */
    public String getEmlUrgentOrderFlagLikeFront() {
        return emlUrgentOrderFlagLikeFront;
    }

    /**
     * Setter method of "emlUrgentOrderFlagLikeFront".
     * 
     * @param emlUrgentOrderFlagLikeFront Set in "emlUrgentOrderFlagLikeFront".
     */
    public void setEmlUrgentOrderFlagLikeFront(String emlUrgentOrderFlagLikeFront) {
        this.emlUrgentOrderFlagLikeFront = emlUrgentOrderFlagLikeFront;
    }

    /**
     * Getter method of "emlSInfoNotfoundFlagLikeFront".
     * 
     * @return the "emlSInfoNotfoundFlagLikeFront"
     */
    public String getEmlSInfoNotfoundFlagLikeFront() {
        return emlSInfoNotfoundFlagLikeFront;
    }

    /**
     * Setter method of "emlSInfoNotfoundFlagLikeFront".
     * 
     * @param emlSInfoNotfoundFlagLikeFront Set in "emlSInfoNotfoundFlagLikeFront".
     */
    public void setEmlSInfoNotfoundFlagLikeFront(String emlSInfoNotfoundFlagLikeFront) {
        this.emlSInfoNotfoundFlagLikeFront = emlSInfoNotfoundFlagLikeFront;
    }

    /**
     * Getter method of "emlAllowReviseFlagLikeFront".
     * 
     * @return the "emlAllowReviseFlagLikeFront"
     */
    public String getEmlAllowReviseFlagLikeFront() {
        return emlAllowReviseFlagLikeFront;
    }

    /**
     * Setter method of "emlAllowReviseFlagLikeFront".
     * 
     * @param emlAllowReviseFlagLikeFront Set in "emlAllowReviseFlagLikeFront".
     */
    public void setEmlAllowReviseFlagLikeFront(String emlAllowReviseFlagLikeFront) {
        this.emlAllowReviseFlagLikeFront = emlAllowReviseFlagLikeFront;
    }

    /**
     * Getter method of "emlCancelInvoiceFlagLikeFront".
     * 
     * @return the "emlCancelInvoiceFlagLikeFront"
     */
    public String getEmlCancelInvoiceFlagLikeFront() {
        return emlCancelInvoiceFlagLikeFront;
    }

    /**
     * Setter method of "emlCancelInvoiceFlagLikeFront".
     * 
     * @param emlCancelInvoiceFlagLikeFront Set in "emlCancelInvoiceFlagLikeFront".
     */
    public void setEmlCancelInvoiceFlagLikeFront(String emlCancelInvoiceFlagLikeFront) {
        this.emlCancelInvoiceFlagLikeFront = emlCancelInvoiceFlagLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
