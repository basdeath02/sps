/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMMisc"<br />
 * Table overview: SPS_M_MISC<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMMiscDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * MISC_TYPE
     */
    private String miscType;

    /**
     * MISC_CD
     */
    private String miscCd;

    /**
     * MISC_VALUE
     */
    private String miscValue;

    /**
     * MISC_DESCRIPTION
     */
    private String miscDescription;

    /**
     * MISC_SORT_NO
     */
    private BigDecimal miscSortNo;

    /**
     * MISC_VALUE2
     */
    private String miscValue2;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMMiscDomain() {
    }

    /**
     * Getter method of "miscType"
     * 
     * @return the miscType
     */
    public String getMiscType() {
        return miscType;
    }

    /**
     * Setter method of "miscType"
     * 
     * @param miscType Set in "miscType".
     */
    public void setMiscType(String miscType) {
        this.miscType = miscType;
    }

    /**
     * Getter method of "miscCd"
     * 
     * @return the miscCd
     */
    public String getMiscCd() {
        return miscCd;
    }

    /**
     * Setter method of "miscCd"
     * 
     * @param miscCd Set in "miscCd".
     */
    public void setMiscCd(String miscCd) {
        this.miscCd = miscCd;
    }

    /**
     * Getter method of "miscValue"
     * 
     * @return the miscValue
     */
    public String getMiscValue() {
        return miscValue;
    }

    /**
     * Setter method of "miscValue"
     * 
     * @param miscValue Set in "miscValue".
     */
    public void setMiscValue(String miscValue) {
        this.miscValue = miscValue;
    }

    /**
     * Getter method of "miscDescription"
     * 
     * @return the miscDescription
     */
    public String getMiscDescription() {
        return miscDescription;
    }

    /**
     * Setter method of "miscDescription"
     * 
     * @param miscDescription Set in "miscDescription".
     */
    public void setMiscDescription(String miscDescription) {
        this.miscDescription = miscDescription;
    }

    /**
     * Getter method of "miscSortNo"
     * 
     * @return the miscSortNo
     */
    public BigDecimal getMiscSortNo() {
        return miscSortNo;
    }

    /**
     * Setter method of "miscSortNo"
     * 
     * @param miscSortNo Set in "miscSortNo".
     */
    public void setMiscSortNo(BigDecimal miscSortNo) {
        this.miscSortNo = miscSortNo;
    }

    /**
     * Getter method of "miscValue2"
     * 
     * @return the miscValue2
     */
    public String getMiscValue2() {
        return miscValue2;
    }

    /**
     * Setter method of "miscValue2"
     * 
     * @param miscValue2 Set in "miscValue2".
     */
    public void setMiscValue2(String miscValue2) {
        this.miscValue2 = miscValue2;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
