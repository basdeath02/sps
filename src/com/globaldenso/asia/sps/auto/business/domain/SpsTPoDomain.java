/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTPo"<br />
 * Table overview: SPS_T_PO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No of Purchase Order
     */
    private BigDecimal poId;

    /**
     * SPS P/O Number
     */
    private String spsPoNo;

    /**
     * CIGMA P/O Number
     */
    private String cigmaPoNo;

    /**
     * REF_CIGMA_PO_NO
     */
    private String refCigmaPoNo;

    /**
     * Data Type from CIGMA
DPO : Domestic Fixed Order
IPO : Import Fixed Order
KPO : Domestic KANBAN Order
IKP : Import KANBAN Order
     */
    private String poType;

    /**
     * P/O Issue date
     */
    private Date poIssueDate;

    /**
     * SPS P/O Status ;
ISS : Issued
PED : Pending
ACK : Acknowledge
FAC : Force Acknowledge
     */
    private String poStatus;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * SPS Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Transportation Mode (TRANs)
0 : DENSO Truck
1 : Supplier Truck
     */
    private String tm;

    /**
     * Period Type (Order Type from CIGMA)
0 : Firm
1 : Forecast
     */
    private String periodType;

    /**
     * Order Method
0 : Fixed Order
1 : KANBAN Order
     */
    private String orderMethod;

    /**
     * Date which SPS Sytem force P/O Status to "FAC" automatically
     */
    private Date forceAckDate;

    /**
     * Flag to inform Change P/O or not
0 : Not Change, 1 : Change
     */
    private String changeFlg;

    /**
     * File Id which identify Original P/O Report PDF file are saved in file manager table
     */
    private String pdfOriginalFileId;

    /**
     * File Id which identify Change Material Release Report PDF file are saved in file manager table
     */
    private String pdfChangeFileId;

    /**
     * Release No
     */
    private BigDecimal releaseNo;

    /**
     * Due Date From
     */
    private Date dueDateFrom;

    /**
     * Due Date To
     */
    private Date dueDateTo;

    /**
     * ax Telephone No.
     */
    private String fax;

    /**
     * Add Truck Route Flag
0 : None
1 : Add New Truck Route
     */
    private String newTruckRouteFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTPoDomain() {
    }

    /**
     * Getter method of "poId"
     * 
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId"
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "spsPoNo"
     * 
     * @return the spsPoNo
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }

    /**
     * Setter method of "spsPoNo"
     * 
     * @param spsPoNo Set in "spsPoNo".
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }

    /**
     * Getter method of "cigmaPoNo"
     * 
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }

    /**
     * Setter method of "cigmaPoNo"
     * 
     * @param cigmaPoNo Set in "cigmaPoNo".
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * Getter method of "refCigmaPoNo"
     * 
     * @return the refCigmaPoNo
     */
    public String getRefCigmaPoNo() {
        return refCigmaPoNo;
    }

    /**
     * Setter method of "refCigmaPoNo"
     * 
     * @param refCigmaPoNo Set in "refCigmaPoNo".
     */
    public void setRefCigmaPoNo(String refCigmaPoNo) {
        this.refCigmaPoNo = refCigmaPoNo;
    }

    /**
     * Getter method of "poType"
     * 
     * @return the poType
     */
    public String getPoType() {
        return poType;
    }

    /**
     * Setter method of "poType"
     * 
     * @param poType Set in "poType".
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }

    /**
     * Getter method of "poIssueDate"
     * 
     * @return the poIssueDate
     */
    public Date getPoIssueDate() {
        return poIssueDate;
    }

    /**
     * Setter method of "poIssueDate"
     * 
     * @param poIssueDate Set in "poIssueDate".
     */
    public void setPoIssueDate(Date poIssueDate) {
        this.poIssueDate = poIssueDate;
    }

    /**
     * Getter method of "poStatus"
     * 
     * @return the poStatus
     */
    public String getPoStatus() {
        return poStatus;
    }

    /**
     * Setter method of "poStatus"
     * 
     * @param poStatus Set in "poStatus".
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "tm"
     * 
     * @return the tm
     */
    public String getTm() {
        return tm;
    }

    /**
     * Setter method of "tm"
     * 
     * @param tm Set in "tm".
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * Getter method of "periodType"
     * 
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * Setter method of "periodType"
     * 
     * @param periodType Set in "periodType".
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    /**
     * Getter method of "orderMethod"
     * 
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Setter method of "orderMethod"
     * 
     * @param orderMethod Set in "orderMethod".
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Getter method of "forceAckDate"
     * 
     * @return the forceAckDate
     */
    public Date getForceAckDate() {
        return forceAckDate;
    }

    /**
     * Setter method of "forceAckDate"
     * 
     * @param forceAckDate Set in "forceAckDate".
     */
    public void setForceAckDate(Date forceAckDate) {
        this.forceAckDate = forceAckDate;
    }

    /**
     * Getter method of "changeFlg"
     * 
     * @return the changeFlg
     */
    public String getChangeFlg() {
        return changeFlg;
    }

    /**
     * Setter method of "changeFlg"
     * 
     * @param changeFlg Set in "changeFlg".
     */
    public void setChangeFlg(String changeFlg) {
        this.changeFlg = changeFlg;
    }

    /**
     * Getter method of "pdfOriginalFileId"
     * 
     * @return the pdfOriginalFileId
     */
    public String getPdfOriginalFileId() {
        return pdfOriginalFileId;
    }

    /**
     * Setter method of "pdfOriginalFileId"
     * 
     * @param pdfOriginalFileId Set in "pdfOriginalFileId".
     */
    public void setPdfOriginalFileId(String pdfOriginalFileId) {
        this.pdfOriginalFileId = pdfOriginalFileId;
    }

    /**
     * Getter method of "pdfChangeFileId"
     * 
     * @return the pdfChangeFileId
     */
    public String getPdfChangeFileId() {
        return pdfChangeFileId;
    }

    /**
     * Setter method of "pdfChangeFileId"
     * 
     * @param pdfChangeFileId Set in "pdfChangeFileId".
     */
    public void setPdfChangeFileId(String pdfChangeFileId) {
        this.pdfChangeFileId = pdfChangeFileId;
    }

    /**
     * Getter method of "releaseNo"
     * 
     * @return the releaseNo
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }

    /**
     * Setter method of "releaseNo"
     * 
     * @param releaseNo Set in "releaseNo".
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }

    /**
     * Getter method of "dueDateFrom"
     * 
     * @return the dueDateFrom
     */
    public Date getDueDateFrom() {
        return dueDateFrom;
    }

    /**
     * Setter method of "dueDateFrom"
     * 
     * @param dueDateFrom Set in "dueDateFrom".
     */
    public void setDueDateFrom(Date dueDateFrom) {
        this.dueDateFrom = dueDateFrom;
    }

    /**
     * Getter method of "dueDateTo"
     * 
     * @return the dueDateTo
     */
    public Date getDueDateTo() {
        return dueDateTo;
    }

    /**
     * Setter method of "dueDateTo"
     * 
     * @param dueDateTo Set in "dueDateTo".
     */
    public void setDueDateTo(Date dueDateTo) {
        this.dueDateTo = dueDateTo;
    }

    /**
     * Getter method of "fax"
     * 
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax"
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "newTruckRouteFlg"
     * 
     * @return the newTruckRouteFlg
     */
    public String getNewTruckRouteFlg() {
        return newTruckRouteFlg;
    }

    /**
     * Setter method of "newTruckRouteFlg"
     * 
     * @param newTruckRouteFlg Set in "newTruckRouteFlg".
     */
    public void setNewTruckRouteFlg(String newTruckRouteFlg) {
        this.newTruckRouteFlg = newTruckRouteFlg;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
