/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 2.0.0      2018/01/10       Netband U. Rungsiwut            SPS Phase II
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTDoDetail"<br />
 * Table overview: SPS_T_DO_DETAIL<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoDetailDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from S_T_DO
     */
    private BigDecimal doId;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Part No Revision
     */
    private String pnRevision;

    /**
     * Part No Shipment Status
ISS : Issued
PTS : Partial-Ship
CPS : Complete-Ship
     */
    private String pnShipmentStatus;

    /**
     * Change CIGMA D/O No
     */
    private String chgCigmaDoNo;

    /**
     * Control No
     */
    private String ctrlNo;

    /**
     * Current Order Qty
     */
    private BigDecimal currentOrderQty;

    /**
     * QTY/BOX (Lot Size)
     */
    private BigDecimal qtyBox;

    /**
     * Reason for change
     */
    private String chgReason;

    /**
     * Item Description
     */
    private String itemDesc;

    /**
     * Unit of Measure
     */
    private String unitOfMeasure;

    /**
     * Engineer Drawing No (Model)
     */
    private String model;

    /**
     * D/O Original Qty
     */
    private BigDecimal originalQty;

    /**
     * D/O Previous Qty
     */
    private BigDecimal previousQty;

    /**
     * No. of Boxes
     */
    private BigDecimal noOfBoxes;

    /**
     * Receiving Lane
     */
    private String rcvLane;

    /**
     * Warehouse Location
     */
    private String whLocation;

    /**
     * Flag to inform D/O is urgent or not
0 : Not Urgent, 1 : Urgent
     */
    private String urgentOrderFlg;

    /**
     * Mail Send Flag
0 : Do not send, 1: Send
     */
    private String mailFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * The kanbanType
     */
    private String kanbanType;

    /**
     * The remark1
     */
    private String remark1;

    /**
     * The remark2
     */
    private String remark2;

    /**
     * The remark3
     */
    private String remark3;

    /**
     * The cPn
     */
    private String cPn;

    /**
     * The sProcCd
     */
    private String sProcCd;
    
    /**
     * The backorderFlg
     */
    private String backorderFlg;

    /**
     * Default constructor
     */
    public SpsTDoDetailDomain() {
    }

    /**
     * Getter method of "doId"
     * 
     * @return the doId
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId"
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "pnRevision"
     * 
     * @return the pnRevision
     */
    public String getPnRevision() {
        return pnRevision;
    }

    /**
     * Setter method of "pnRevision"
     * 
     * @param pnRevision Set in "pnRevision".
     */
    public void setPnRevision(String pnRevision) {
        this.pnRevision = pnRevision;
    }

    /**
     * Getter method of "pnShipmentStatus"
     * 
     * @return the pnShipmentStatus
     */
    public String getPnShipmentStatus() {
        return pnShipmentStatus;
    }

    /**
     * Setter method of "pnShipmentStatus"
     * 
     * @param pnShipmentStatus Set in "pnShipmentStatus".
     */
    public void setPnShipmentStatus(String pnShipmentStatus) {
        this.pnShipmentStatus = pnShipmentStatus;
    }

    /**
     * Getter method of "chgCigmaDoNo"
     * 
     * @return the chgCigmaDoNo
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * Setter method of "chgCigmaDoNo"
     * 
     * @param chgCigmaDoNo Set in "chgCigmaDoNo".
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * Getter method of "ctrlNo"
     * 
     * @return the ctrlNo
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * Setter method of "ctrlNo"
     * 
     * @param ctrlNo Set in "ctrlNo".
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * Getter method of "currentOrderQty"
     * 
     * @return the currentOrderQty
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Setter method of "currentOrderQty"
     * 
     * @param currentOrderQty Set in "currentOrderQty".
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Getter method of "qtyBox"
     * 
     * @return the qtyBox
     */
    public BigDecimal getQtyBox() {
        return qtyBox;
    }

    /**
     * Setter method of "qtyBox"
     * 
     * @param qtyBox Set in "qtyBox".
     */
    public void setQtyBox(BigDecimal qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * Getter method of "chgReason"
     * 
     * @return the chgReason
     */
    public String getChgReason() {
        return chgReason;
    }

    /**
     * Setter method of "chgReason"
     * 
     * @param chgReason Set in "chgReason".
     */
    public void setChgReason(String chgReason) {
        this.chgReason = chgReason;
    }

    /**
     * Getter method of "itemDesc"
     * 
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Setter method of "itemDesc"
     * 
     * @param itemDesc Set in "itemDesc".
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * Getter method of "unitOfMeasure"
     * 
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Setter method of "unitOfMeasure"
     * 
     * @param unitOfMeasure Set in "unitOfMeasure".
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter method of "model"
     * 
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * Setter method of "model"
     * 
     * @param model Set in "model".
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Getter method of "originalQty"
     * 
     * @return the originalQty
     */
    public BigDecimal getOriginalQty() {
        return originalQty;
    }

    /**
     * Setter method of "originalQty"
     * 
     * @param originalQty Set in "originalQty".
     */
    public void setOriginalQty(BigDecimal originalQty) {
        this.originalQty = originalQty;
    }

    /**
     * Getter method of "previousQty"
     * 
     * @return the previousQty
     */
    public BigDecimal getPreviousQty() {
        return previousQty;
    }

    /**
     * Setter method of "previousQty"
     * 
     * @param previousQty Set in "previousQty".
     */
    public void setPreviousQty(BigDecimal previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * Getter method of "noOfBoxes"
     * 
     * @return the noOfBoxes
     */
    public BigDecimal getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * Setter method of "noOfBoxes"
     * 
     * @param noOfBoxes Set in "noOfBoxes".
     */
    public void setNoOfBoxes(BigDecimal noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * Getter method of "rcvLane"
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Setter method of "rcvLane"
     * 
     * @param rcvLane Set in "rcvLane".
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Getter method of "whLocation"
     * 
     * @return the whLocation
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * Setter method of "whLocation"
     * 
     * @param whLocation Set in "whLocation".
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }

    /**
     * Getter method of "urgentOrderFlg"
     * 
     * @return the urgentOrderFlg
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * Setter method of "urgentOrderFlg"
     * 
     * @param urgentOrderFlg Set in "urgentOrderFlg".
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * Getter method of "mailFlg"
     * 
     * @return the mailFlg
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg"
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "kanbanType"
     * 
     * @return the kanbanType
     */
    public String getKanbanType() {
        return kanbanType;
    }

    /**
     * Setter method of "kanbanType"
     * 
     * @param kanbanType Set in "kanbanType".
     */
    public void setKanbanType(String kanbanType) {
        this.kanbanType = kanbanType;
    }

    /**
     * Getter method of "remark1"
     * 
     * @return the remark1
     */
    public String getRemark1() {
        return remark1;
    }

    /**
     * Setter method of "remark1"
     * 
     * @param remark1 Set in "remark1".
     */
    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    /**
     * Getter method of "remark2"
     * 
     * @return the remark2
     */
    public String getRemark2() {
        return remark2;
    }

    /**
     * Setter method of "remark2"
     * 
     * @param remark2 Set in "remark2".
     */
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    /**
     * Getter method of "remark3"
     * 
     * @return the remark3
     */
    public String getRemark3() {
        return remark3;
    }

    /**
     * Setter method of "remark3"
     * 
     * @param remark3 Set in "remark3".
     */
    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    /**
     * Getter method of "cPn"
     * 
     * @return the cPn
     */
    public String getcPn() {
        return cPn;
    }

    /**
     * Setter method of "cPn"
     * 
     * @param cPn Set in "cPn".
     */
    public void setcPn(String cPn) {
        this.cPn = cPn;
    }

    /**
     * Getter method of "sProcCd"
     * 
     * @return the sProcCd
     */
    public String getsProcCd() {
        return sProcCd;
    }

    /**
     * Setter method of "sProcCd"
     * 
     * @param sProcCd Set in "sProcCd".
     */
    public void setsProcCd(String sProcCd) {
        this.sProcCd = sProcCd;
    }
    
    /**
     * <p>Getter method for backorderFlg.</p>
     *
     * @return the backorderFlg
     */
    public String getBackorderFlg() {
        return backorderFlg;
    }

    /**
     * <p>Setter method for backorderFlg.</p>
     *
     * @param backorderFlg Set for backorderFlg
     */
    public void setBackorderFlg(String backorderFlg) {
        this.backorderFlg = backorderFlg;
    }

}
