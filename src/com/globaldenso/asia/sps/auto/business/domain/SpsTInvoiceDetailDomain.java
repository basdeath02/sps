/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/04/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTInvoiceDetail"<br />
 * Table overview: SPS_T_INVOICE_DETAIL<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/04/28 15:59:11<br />
 * 
 * This module generated automatically in 2015/04/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceDetailDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running Number of Invoice
     */
    private BigDecimal invoiceId;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * Running Number of Delivery Order
     */
    private BigDecimal doId;

    /**
     * SPS D/O No
     */
    private String spsDoNo;

    /**
     * D/O Revision
     */
    private String revision;

    /**
     * Trip Number
     */
    private String tripNo;

    /**
     * DENSO Part Number
     */
    private String dPn;

    /**
     * DENSO Unit of Measure
     */
    private String dUnitOfMeasure;

    /**
     * DENSO Unit Price
     */
    private BigDecimal dUnitPrice;

    /**
     * Temp Price Flag
     */
    private String tmpPriceFlg;

    /**
     * Supplier Part Number
     */
    private String sPn;

    /**
     * Supplier Unit of Measure
     */
    private String sUnitOfMeasure;

    /**
     * Supplier Unit Price
     */
    private BigDecimal sUnitPrice;

    /**
     * Shipping Quantity
     */
    private BigDecimal shippingQty;

    /**
     * Difference Base Amount of each part
     */
    private BigDecimal diffBaseAmt;

    /**
     * Base Amount of each part
     */
    private BigDecimal baseAmt;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTInvoiceDetailDomain() {
    }

    /**
     * Getter method of "invoiceId"
     * 
     * @return the invoiceId
     */
    public BigDecimal getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method of "invoiceId"
     * 
     * @param invoiceId Set in "invoiceId".
     */
    public void setInvoiceId(BigDecimal invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * Getter method of "asnNo"
     * 
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo"
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "doId"
     * 
     * @return the doId
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId"
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "spsDoNo"
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo"
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision"
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision"
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "tripNo"
     * 
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo"
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "dUnitOfMeasure"
     * 
     * @return the dUnitOfMeasure
     */
    public String getDUnitOfMeasure() {
        return dUnitOfMeasure;
    }

    /**
     * Setter method of "dUnitOfMeasure"
     * 
     * @param dUnitOfMeasure Set in "dUnitOfMeasure".
     */
    public void setDUnitOfMeasure(String dUnitOfMeasure) {
        this.dUnitOfMeasure = dUnitOfMeasure;
    }

    /**
     * Getter method of "dUnitPrice"
     * 
     * @return the dUnitPrice
     */
    public BigDecimal getDUnitPrice() {
        return dUnitPrice;
    }

    /**
     * Setter method of "dUnitPrice"
     * 
     * @param dUnitPrice Set in "dUnitPrice".
     */
    public void setDUnitPrice(BigDecimal dUnitPrice) {
        this.dUnitPrice = dUnitPrice;
    }

    /**
     * Getter method of "tmpPriceFlg"
     * 
     * @return the tmpPriceFlg
     */
    public String getTmpPriceFlg() {
        return tmpPriceFlg;
    }

    /**
     * Setter method of "tmpPriceFlg"
     * 
     * @param tmpPriceFlg Set in "tmpPriceFlg".
     */
    public void setTmpPriceFlg(String tmpPriceFlg) {
        this.tmpPriceFlg = tmpPriceFlg;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "sUnitOfMeasure"
     * 
     * @return the sUnitOfMeasure
     */
    public String getSUnitOfMeasure() {
        return sUnitOfMeasure;
    }

    /**
     * Setter method of "sUnitOfMeasure"
     * 
     * @param sUnitOfMeasure Set in "sUnitOfMeasure".
     */
    public void setSUnitOfMeasure(String sUnitOfMeasure) {
        this.sUnitOfMeasure = sUnitOfMeasure;
    }

    /**
     * Getter method of "sUnitPrice"
     * 
     * @return the sUnitPrice
     */
    public BigDecimal getSUnitPrice() {
        return sUnitPrice;
    }

    /**
     * Setter method of "sUnitPrice"
     * 
     * @param sUnitPrice Set in "sUnitPrice".
     */
    public void setSUnitPrice(BigDecimal sUnitPrice) {
        this.sUnitPrice = sUnitPrice;
    }

    /**
     * Getter method of "shippingQty"
     * 
     * @return the shippingQty
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty"
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "diffBaseAmt"
     * 
     * @return the diffBaseAmt
     */
    public BigDecimal getDiffBaseAmt() {
        return diffBaseAmt;
    }

    /**
     * Setter method of "diffBaseAmt"
     * 
     * @param diffBaseAmt Set in "diffBaseAmt".
     */
    public void setDiffBaseAmt(BigDecimal diffBaseAmt) {
        this.diffBaseAmt = diffBaseAmt;
    }

    /**
     * Getter method of "baseAmt"
     * 
     * @return the baseAmt
     */
    public BigDecimal getBaseAmt() {
        return baseAmt;
    }

    /**
     * Setter method of "baseAmt"
     * 
     * @param baseAmt Set in "baseAmt".
     */
    public void setBaseAmt(BigDecimal baseAmt) {
        this.baseAmt = baseAmt;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
