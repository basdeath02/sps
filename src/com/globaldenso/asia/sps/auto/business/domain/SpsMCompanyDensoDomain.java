/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/26       CSI                             New
 * 2.0.0      2018/01/10       Netband U. Rungsiwut            SPS Phase II
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMCompanyDenso"<br />
 * Table overview: SPS_M_COMPANY_DENSO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/26 14:21:53<br />
 * 
 * This module generated automatically in 2015/05/26 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanyDensoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * DENSO company identify number
     */
    private String dCd;

    /**
     * DENSO company DSC ID number
     */
    private String dscIdComCd;

    /**
     * Company name
     */
    private String companyName;

    /**
     * CIGMA Schema Code
     */
    private String schemaCd;

    /**
     * DENSO company address 1
     */
    private String address1;

    /**
     * DENSO company address 2
     */
    private String address2;

    /**
     * DENSO company address 3
     */
    private String address3;

    /**
     * Telephone number
     */
    private String telephone;

    /**
     * Fax number
     */
    private String fax;

    /**
     * Limitation years for purge order transaction
     */
    private BigDecimal orderKeepYear;

    /**
     * Limitation years for purge invoice transaction
     */
    private BigDecimal invoiceKeepYear;

    /**
     * File path value for annoucement message source file
     */
    private String filePath;

    /**
     * Temporary path for keep backup file after import message complete
     */
    private String tempPath;

    /**
     * Coordinated Universal Time
     */
    private String utc;

    /**
     * 0 : Inactive, 1 : Active
     */
    private String isActive;

    /**
     * DENSO tax id
     */
    private String dTaxId;

    /**
     * Tax code
     */
    private String taxCd;

    /**
     * Refer to currency master table to keep a currency type value
     */
    private String currencyCd;

    /**
     * 0 : Warning, 1 : Error
     */
    private String groupInvoiceErrorType;

    /**
     * D: Date, M: Month, Y: Year
     */
    private String groupInvoiceType;

    /**
     * 1: Not generate 2: Generate by D/O No. 3: Generate by DENSO Part No.
     */
    private String qrType;

    /**
     * Notice P/O Perio
     */
    private BigDecimal noticePoPeriod;

    /**
     * Flag for show/hide KANBAN Sequence in KANBAN Delivery Order report; 1: show, 2: not show
     */
    private String reportKanbanSeqFlg;

    /**
     * Flag for show/hide Terms and Condition of P/O report
     */
    private String poTermConditionFlg;

    /**
     * Flag for show/hide DENSO Plant on report; 0: Warning, 1: Error
     */
    private String groupAsnErrorType;

    /**
     * Flag for show/hide DENSO Plant Code of P/O report; 0: not show, 1: show
     */
    private String showDensoPlantFlg;

    /**
     * Flag allow to group invoice difference plant; 0: Not Allow, 1: Allow
     */
    private String groupInvoiceDiffPlantDenso;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * File name for ASN template
     */
    private String asnTempleteFilename;

    /**
     * Default constructor
     */
    public SpsMCompanyDensoDomain() {
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dscIdComCd"
     * 
     * @return the dscIdComCd
     */
    public String getDscIdComCd() {
        return dscIdComCd;
    }

    /**
     * Setter method of "dscIdComCd"
     * 
     * @param dscIdComCd Set in "dscIdComCd".
     */
    public void setDscIdComCd(String dscIdComCd) {
        this.dscIdComCd = dscIdComCd;
    }

    /**
     * Getter method of "companyName"
     * 
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Setter method of "companyName"
     * 
     * @param companyName Set in "companyName".
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Getter method of "schemaCd"
     * 
     * @return the schemaCd
     */
    public String getSchemaCd() {
        return schemaCd;
    }

    /**
     * Setter method of "schemaCd"
     * 
     * @param schemaCd Set in "schemaCd".
     */
    public void setSchemaCd(String schemaCd) {
        this.schemaCd = schemaCd;
    }

    /**
     * Getter method of "address1"
     * 
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Setter method of "address1"
     * 
     * @param address1 Set in "address1".
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * Getter method of "address2"
     * 
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Setter method of "address2"
     * 
     * @param address2 Set in "address2".
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * Getter method of "address3"
     * 
     * @return the address3
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Setter method of "address3"
     * 
     * @param address3 Set in "address3".
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "fax"
     * 
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax"
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "orderKeepYear"
     * 
     * @return the orderKeepYear
     */
    public BigDecimal getOrderKeepYear() {
        return orderKeepYear;
    }

    /**
     * Setter method of "orderKeepYear"
     * 
     * @param orderKeepYear Set in "orderKeepYear".
     */
    public void setOrderKeepYear(BigDecimal orderKeepYear) {
        this.orderKeepYear = orderKeepYear;
    }

    /**
     * Getter method of "invoiceKeepYear"
     * 
     * @return the invoiceKeepYear
     */
    public BigDecimal getInvoiceKeepYear() {
        return invoiceKeepYear;
    }

    /**
     * Setter method of "invoiceKeepYear"
     * 
     * @param invoiceKeepYear Set in "invoiceKeepYear".
     */
    public void setInvoiceKeepYear(BigDecimal invoiceKeepYear) {
        this.invoiceKeepYear = invoiceKeepYear;
    }

    /**
     * Getter method of "filePath"
     * 
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Setter method of "filePath"
     * 
     * @param filePath Set in "filePath".
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Getter method of "tempPath"
     * 
     * @return the tempPath
     */
    public String getTempPath() {
        return tempPath;
    }

    /**
     * Setter method of "tempPath"
     * 
     * @param tempPath Set in "tempPath".
     */
    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    /**
     * Getter method of "utc"
     * 
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Setter method of "utc"
     * 
     * @param utc Set in "utc".
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "dTaxId"
     * 
     * @return the dTaxId
     */
    public String getDTaxId() {
        return dTaxId;
    }

    /**
     * Setter method of "dTaxId"
     * 
     * @param dTaxId Set in "dTaxId".
     */
    public void setDTaxId(String dTaxId) {
        this.dTaxId = dTaxId;
    }

    /**
     * Getter method of "taxCd"
     * 
     * @return the taxCd
     */
    public String getTaxCd() {
        return taxCd;
    }

    /**
     * Setter method of "taxCd"
     * 
     * @param taxCd Set in "taxCd".
     */
    public void setTaxCd(String taxCd) {
        this.taxCd = taxCd;
    }

    /**
     * Getter method of "currencyCd"
     * 
     * @return the currencyCd
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * Setter method of "currencyCd"
     * 
     * @param currencyCd Set in "currencyCd".
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * Getter method of "groupInvoiceErrorType"
     * 
     * @return the groupInvoiceErrorType
     */
    public String getGroupInvoiceErrorType() {
        return groupInvoiceErrorType;
    }

    /**
     * Setter method of "groupInvoiceErrorType"
     * 
     * @param groupInvoiceErrorType Set in "groupInvoiceErrorType".
     */
    public void setGroupInvoiceErrorType(String groupInvoiceErrorType) {
        this.groupInvoiceErrorType = groupInvoiceErrorType;
    }

    /**
     * Getter method of "groupInvoiceType"
     * 
     * @return the groupInvoiceType
     */
    public String getGroupInvoiceType() {
        return groupInvoiceType;
    }

    /**
     * Setter method of "groupInvoiceType"
     * 
     * @param groupInvoiceType Set in "groupInvoiceType".
     */
    public void setGroupInvoiceType(String groupInvoiceType) {
        this.groupInvoiceType = groupInvoiceType;
    }

    /**
     * Getter method of "qrType"
     * 
     * @return the qrType
     */
    public String getQrType() {
        return qrType;
    }

    /**
     * Setter method of "qrType"
     * 
     * @param qrType Set in "qrType".
     */
    public void setQrType(String qrType) {
        this.qrType = qrType;
    }

    /**
     * Getter method of "noticePoPeriod"
     * 
     * @return the noticePoPeriod
     */
    public BigDecimal getNoticePoPeriod() {
        return noticePoPeriod;
    }

    /**
     * Setter method of "noticePoPeriod"
     * 
     * @param noticePoPeriod Set in "noticePoPeriod".
     */
    public void setNoticePoPeriod(BigDecimal noticePoPeriod) {
        this.noticePoPeriod = noticePoPeriod;
    }

    /**
     * Getter method of "reportKanbanSeqFlg"
     * 
     * @return the reportKanbanSeqFlg
     */
    public String getReportKanbanSeqFlg() {
        return reportKanbanSeqFlg;
    }

    /**
     * Setter method of "reportKanbanSeqFlg"
     * 
     * @param reportKanbanSeqFlg Set in "reportKanbanSeqFlg".
     */
    public void setReportKanbanSeqFlg(String reportKanbanSeqFlg) {
        this.reportKanbanSeqFlg = reportKanbanSeqFlg;
    }

    /**
     * Getter method of "poTermConditionFlg"
     * 
     * @return the poTermConditionFlg
     */
    public String getPoTermConditionFlg() {
        return poTermConditionFlg;
    }

    /**
     * Setter method of "poTermConditionFlg"
     * 
     * @param poTermConditionFlg Set in "poTermConditionFlg".
     */
    public void setPoTermConditionFlg(String poTermConditionFlg) {
        this.poTermConditionFlg = poTermConditionFlg;
    }

    /**
     * Getter method of "groupAsnErrorType"
     * 
     * @return the groupAsnErrorType
     */
    public String getGroupAsnErrorType() {
        return groupAsnErrorType;
    }

    /**
     * Setter method of "groupAsnErrorType"
     * 
     * @param groupAsnErrorType Set in "groupAsnErrorType".
     */
    public void setGroupAsnErrorType(String groupAsnErrorType) {
        this.groupAsnErrorType = groupAsnErrorType;
    }

    /**
     * Getter method of "showDensoPlantFlg"
     * 
     * @return the showDensoPlantFlg
     */
    public String getShowDensoPlantFlg() {
        return showDensoPlantFlg;
    }

    /**
     * Setter method of "showDensoPlantFlg"
     * 
     * @param showDensoPlantFlg Set in "showDensoPlantFlg".
     */
    public void setShowDensoPlantFlg(String showDensoPlantFlg) {
        this.showDensoPlantFlg = showDensoPlantFlg;
    }

    /**
     * Getter method of "groupInvoiceDiffPlantDenso"
     * 
     * @return the groupInvoiceDiffPlantDenso
     */
    public String getGroupInvoiceDiffPlantDenso() {
        return groupInvoiceDiffPlantDenso;
    }

    /**
     * Setter method of "groupInvoiceDiffPlantDenso"
     * 
     * @param groupInvoiceDiffPlantDenso Set in "groupInvoiceDiffPlantDenso".
     */
    public void setGroupInvoiceDiffPlantDenso(String groupInvoiceDiffPlantDenso) {
        this.groupInvoiceDiffPlantDenso = groupInvoiceDiffPlantDenso;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "asnTempleteFilename"
     * 
     * @return the asnTempleteFilename
     */
    public String getAsnTempleteFilename() {
        return asnTempleteFilename;
    }

    /**
     * Setter method of "asnTempleteFilename"
     * 
     * @param asnTempleteFilename Set in "asnTempleteFilename".
     */
    public void setAsnTempleteFilename(String asnTempleteFilename) {
        this.asnTempleteFilename = asnTempleteFilename;
    }

}
