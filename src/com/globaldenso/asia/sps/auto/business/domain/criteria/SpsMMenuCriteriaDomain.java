/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMMenu".<br />
 * Table overview: SPS_M_MENU<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMMenuCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * MENU_CD
     */
    private String menuCd;

    /**
     * MENU_NAME
     */
    private String menuName;

    /**
     * PARENT_MENU
     */
    private String parentMenu;

    /**
     * RANK
     */
    private BigDecimal rank;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * IS_ACTIVE
     */
    private String isActive;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * MENU_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String menuCdLikeFront;

    /**
     * MENU_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String menuNameLikeFront;

    /**
     * PARENT_MENU(condition whether the column value at the beginning is equal to the value)
     */
    private String parentMenuLikeFront;

    /**
     * IS_ACTIVE(condition whether the column value at the beginning is equal to the value)
     */
    private String isActiveLikeFront;

    /**
     * CREATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * LAST_UPDATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMMenuCriteriaDomain() {
    }

    /**
     * Getter method of "menuCd".
     * 
     * @return the "menuCd"
     */
    public String getMenuCd() {
        return menuCd;
    }

    /**
     * Setter method of "menuCd".
     * 
     * @param menuCd Set in "menuCd".
     */
    public void setMenuCd(String menuCd) {
        this.menuCd = menuCd;
    }

    /**
     * Getter method of "menuName".
     * 
     * @return the "menuName"
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * Setter method of "menuName".
     * 
     * @param menuName Set in "menuName".
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     * Getter method of "parentMenu".
     * 
     * @return the "parentMenu"
     */
    public String getParentMenu() {
        return parentMenu;
    }

    /**
     * Setter method of "parentMenu".
     * 
     * @param parentMenu Set in "parentMenu".
     */
    public void setParentMenu(String parentMenu) {
        this.parentMenu = parentMenu;
    }

    /**
     * Getter method of "rank".
     * 
     * @return the "rank"
     */
    public BigDecimal getRank() {
        return rank;
    }

    /**
     * Setter method of "rank".
     * 
     * @param rank Set in "rank".
     */
    public void setRank(BigDecimal rank) {
        this.rank = rank;
    }

    /**
     * Getter method of "seqNo".
     * 
     * @return the "seqNo"
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo".
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "isActive".
     * 
     * @return the "isActive"
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive".
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "menuCdLikeFront".
     * 
     * @return the "menuCdLikeFront"
     */
    public String getMenuCdLikeFront() {
        return menuCdLikeFront;
    }

    /**
     * Setter method of "menuCdLikeFront".
     * 
     * @param menuCdLikeFront Set in "menuCdLikeFront".
     */
    public void setMenuCdLikeFront(String menuCdLikeFront) {
        this.menuCdLikeFront = menuCdLikeFront;
    }

    /**
     * Getter method of "menuNameLikeFront".
     * 
     * @return the "menuNameLikeFront"
     */
    public String getMenuNameLikeFront() {
        return menuNameLikeFront;
    }

    /**
     * Setter method of "menuNameLikeFront".
     * 
     * @param menuNameLikeFront Set in "menuNameLikeFront".
     */
    public void setMenuNameLikeFront(String menuNameLikeFront) {
        this.menuNameLikeFront = menuNameLikeFront;
    }

    /**
     * Getter method of "parentMenuLikeFront".
     * 
     * @return the "parentMenuLikeFront"
     */
    public String getParentMenuLikeFront() {
        return parentMenuLikeFront;
    }

    /**
     * Setter method of "parentMenuLikeFront".
     * 
     * @param parentMenuLikeFront Set in "parentMenuLikeFront".
     */
    public void setParentMenuLikeFront(String parentMenuLikeFront) {
        this.parentMenuLikeFront = parentMenuLikeFront;
    }

    /**
     * Getter method of "isActiveLikeFront".
     * 
     * @return the "isActiveLikeFront"
     */
    public String getIsActiveLikeFront() {
        return isActiveLikeFront;
    }

    /**
     * Setter method of "isActiveLikeFront".
     * 
     * @param isActiveLikeFront Set in "isActiveLikeFront".
     */
    public void setIsActiveLikeFront(String isActiveLikeFront) {
        this.isActiveLikeFront = isActiveLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
