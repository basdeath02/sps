/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMDensoSupplierPlant"<br />
 * Table overview: SPS_M_DENSO_SUPPLIER_PLANT<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierPlantDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * DENSO company code
     */
    private String dCd;

    /**
     * Supplier company DSC ID
     */
    private String sDscId;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /**
     * DENSO part no.
     */
    private String dPn;

    /**
     * Supplier part no.
     */
    private String sPn;

    /**
     * Effective date
     */
    private Timestamp effectDate;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createUser;

    /**
     * Create datetime
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateUser;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMDensoSupplierPlantDomain() {
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sDscId"
     * 
     * @return the sDscId
     */
    public String getSDscId() {
        return sDscId;
    }

    /**
     * Setter method of "sDscId"
     * 
     * @param sDscId Set in "sDscId".
     */
    public void setSDscId(String sDscId) {
        this.sDscId = sDscId;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "effectDate"
     * 
     * @return the effectDate
     */
    public Timestamp getEffectDate() {
        return effectDate;
    }

    /**
     * Setter method of "effectDate"
     * 
     * @param effectDate Set in "effectDate".
     */
    public void setEffectDate(Timestamp effectDate) {
        this.effectDate = effectDate;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createUser"
     * 
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method of "createUser"
     * 
     * @param createUser Set in "createUser".
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateUser"
     * 
     * @return the lastUpdateUser
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Setter method of "lastUpdateUser"
     * 
     * @param lastUpdateUser Set in "lastUpdateUser".
     */
    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
