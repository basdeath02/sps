/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTDoDetail".<br />
 * Table overview: SPS_T_DO_DETAIL<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoDetailCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from S_T_DO
     */
    private BigDecimal doId;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Part No Revision
     */
    private String pnRevision;

    /**
     * Part No Shipment Status
ISS : Issued
PTS : Partial-Ship
CPS : Complete-Ship
     */
    private String pnShipmentStatus;

    /**
     * Change CIGMA D/O No
     */
    private String chgCigmaDoNo;

    /**
     * Control No
     */
    private String ctrlNo;

    /**
     * Current Order Qty
     */
    private BigDecimal currentOrderQty;

    /**
     * QTY/BOX (Lot Size)
     */
    private BigDecimal qtyBox;

    /**
     * Reason for change
     */
    private String chgReason;

    /**
     * Item Description
     */
    private String itemDesc;

    /**
     * Unit of Measure
     */
    private String unitOfMeasure;

    /**
     * Engineer Drawing No (Model)
     */
    private String model;

    /**
     * D/O Original Qty
     */
    private BigDecimal originalQty;

    /**
     * D/O Previous Qty
     */
    private BigDecimal previousQty;

    /**
     * No. of Boxes
     */
    private BigDecimal noOfBoxes;

    /**
     * Receiving Lane
     */
    private String rcvLane;

    /**
     * Warehouse Location
     */
    private String whLocation;

    /**
     * Flag to inform D/O is urgent or not
0 : Not Urgent, 1 : Urgent
     */
    private String urgentOrderFlg;

    /**
     * Mail Send Flag
0 : Do not send, 1: Send
     */
    private String mailFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Supplier Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * DENSO Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Part No Revision(condition whether the column value at the beginning is equal to the value)
     */
    private String pnRevisionLikeFront;

    /**
     * Part No Shipment Status
ISS : Issued
PTS : Partial-Ship
CPS : Complete-Ship(condition whether the column value at the beginning is equal to the value)
     */
    private String pnShipmentStatusLikeFront;

    /**
     * Change CIGMA D/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String chgCigmaDoNoLikeFront;

    /**
     * Control No(condition whether the column value at the beginning is equal to the value)
     */
    private String ctrlNoLikeFront;

    /**
     * Reason for change(condition whether the column value at the beginning is equal to the value)
     */
    private String chgReasonLikeFront;

    /**
     * Item Description(condition whether the column value at the beginning is equal to the value)
     */
    private String itemDescLikeFront;

    /**
     * Unit of Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String unitOfMeasureLikeFront;

    /**
     * Engineer Drawing No (Model)(condition whether the column value at the beginning is equal to the value)
     */
    private String modelLikeFront;

    /**
     * Receiving Lane(condition whether the column value at the beginning is equal to the value)
     */
    private String rcvLaneLikeFront;

    /**
     * Warehouse Location(condition whether the column value at the beginning is equal to the value)
     */
    private String whLocationLikeFront;

    /**
     * Flag to inform D/O is urgent or not
0 : Not Urgent, 1 : Urgent(condition whether the column value at the beginning is equal to the value)
     */
    private String urgentOrderFlgLikeFront;

    /**
     * Mail Send Flag
0 : Do not send, 1: Send(condition whether the column value at the beginning is equal to the value)
     */
    private String mailFlgLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTDoDetailCriteriaDomain() {
    }

    /**
     * Getter method of "doId".
     * 
     * @return the "doId"
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId".
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "pnRevision".
     * 
     * @return the "pnRevision"
     */
    public String getPnRevision() {
        return pnRevision;
    }

    /**
     * Setter method of "pnRevision".
     * 
     * @param pnRevision Set in "pnRevision".
     */
    public void setPnRevision(String pnRevision) {
        this.pnRevision = pnRevision;
    }

    /**
     * Getter method of "pnShipmentStatus".
     * 
     * @return the "pnShipmentStatus"
     */
    public String getPnShipmentStatus() {
        return pnShipmentStatus;
    }

    /**
     * Setter method of "pnShipmentStatus".
     * 
     * @param pnShipmentStatus Set in "pnShipmentStatus".
     */
    public void setPnShipmentStatus(String pnShipmentStatus) {
        this.pnShipmentStatus = pnShipmentStatus;
    }

    /**
     * Getter method of "chgCigmaDoNo".
     * 
     * @return the "chgCigmaDoNo"
     */
    public String getChgCigmaDoNo() {
        return chgCigmaDoNo;
    }

    /**
     * Setter method of "chgCigmaDoNo".
     * 
     * @param chgCigmaDoNo Set in "chgCigmaDoNo".
     */
    public void setChgCigmaDoNo(String chgCigmaDoNo) {
        this.chgCigmaDoNo = chgCigmaDoNo;
    }

    /**
     * Getter method of "ctrlNo".
     * 
     * @return the "ctrlNo"
     */
    public String getCtrlNo() {
        return ctrlNo;
    }

    /**
     * Setter method of "ctrlNo".
     * 
     * @param ctrlNo Set in "ctrlNo".
     */
    public void setCtrlNo(String ctrlNo) {
        this.ctrlNo = ctrlNo;
    }

    /**
     * Getter method of "currentOrderQty".
     * 
     * @return the "currentOrderQty"
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Setter method of "currentOrderQty".
     * 
     * @param currentOrderQty Set in "currentOrderQty".
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Getter method of "qtyBox".
     * 
     * @return the "qtyBox"
     */
    public BigDecimal getQtyBox() {
        return qtyBox;
    }

    /**
     * Setter method of "qtyBox".
     * 
     * @param qtyBox Set in "qtyBox".
     */
    public void setQtyBox(BigDecimal qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * Getter method of "chgReason".
     * 
     * @return the "chgReason"
     */
    public String getChgReason() {
        return chgReason;
    }

    /**
     * Setter method of "chgReason".
     * 
     * @param chgReason Set in "chgReason".
     */
    public void setChgReason(String chgReason) {
        this.chgReason = chgReason;
    }

    /**
     * Getter method of "itemDesc".
     * 
     * @return the "itemDesc"
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Setter method of "itemDesc".
     * 
     * @param itemDesc Set in "itemDesc".
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * Getter method of "unitOfMeasure".
     * 
     * @return the "unitOfMeasure"
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Setter method of "unitOfMeasure".
     * 
     * @param unitOfMeasure Set in "unitOfMeasure".
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter method of "model".
     * 
     * @return the "model"
     */
    public String getModel() {
        return model;
    }

    /**
     * Setter method of "model".
     * 
     * @param model Set in "model".
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Getter method of "originalQty".
     * 
     * @return the "originalQty"
     */
    public BigDecimal getOriginalQty() {
        return originalQty;
    }

    /**
     * Setter method of "originalQty".
     * 
     * @param originalQty Set in "originalQty".
     */
    public void setOriginalQty(BigDecimal originalQty) {
        this.originalQty = originalQty;
    }

    /**
     * Getter method of "previousQty".
     * 
     * @return the "previousQty"
     */
    public BigDecimal getPreviousQty() {
        return previousQty;
    }

    /**
     * Setter method of "previousQty".
     * 
     * @param previousQty Set in "previousQty".
     */
    public void setPreviousQty(BigDecimal previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * Getter method of "noOfBoxes".
     * 
     * @return the "noOfBoxes"
     */
    public BigDecimal getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * Setter method of "noOfBoxes".
     * 
     * @param noOfBoxes Set in "noOfBoxes".
     */
    public void setNoOfBoxes(BigDecimal noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * Getter method of "rcvLane".
     * 
     * @return the "rcvLane"
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Setter method of "rcvLane".
     * 
     * @param rcvLane Set in "rcvLane".
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Getter method of "whLocation".
     * 
     * @return the "whLocation"
     */
    public String getWhLocation() {
        return whLocation;
    }

    /**
     * Setter method of "whLocation".
     * 
     * @param whLocation Set in "whLocation".
     */
    public void setWhLocation(String whLocation) {
        this.whLocation = whLocation;
    }

    /**
     * Getter method of "urgentOrderFlg".
     * 
     * @return the "urgentOrderFlg"
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * Setter method of "urgentOrderFlg".
     * 
     * @param urgentOrderFlg Set in "urgentOrderFlg".
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * Getter method of "mailFlg".
     * 
     * @return the "mailFlg"
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg".
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "pnRevisionLikeFront".
     * 
     * @return the "pnRevisionLikeFront"
     */
    public String getPnRevisionLikeFront() {
        return pnRevisionLikeFront;
    }

    /**
     * Setter method of "pnRevisionLikeFront".
     * 
     * @param pnRevisionLikeFront Set in "pnRevisionLikeFront".
     */
    public void setPnRevisionLikeFront(String pnRevisionLikeFront) {
        this.pnRevisionLikeFront = pnRevisionLikeFront;
    }

    /**
     * Getter method of "pnShipmentStatusLikeFront".
     * 
     * @return the "pnShipmentStatusLikeFront"
     */
    public String getPnShipmentStatusLikeFront() {
        return pnShipmentStatusLikeFront;
    }

    /**
     * Setter method of "pnShipmentStatusLikeFront".
     * 
     * @param pnShipmentStatusLikeFront Set in "pnShipmentStatusLikeFront".
     */
    public void setPnShipmentStatusLikeFront(String pnShipmentStatusLikeFront) {
        this.pnShipmentStatusLikeFront = pnShipmentStatusLikeFront;
    }

    /**
     * Getter method of "chgCigmaDoNoLikeFront".
     * 
     * @return the "chgCigmaDoNoLikeFront"
     */
    public String getChgCigmaDoNoLikeFront() {
        return chgCigmaDoNoLikeFront;
    }

    /**
     * Setter method of "chgCigmaDoNoLikeFront".
     * 
     * @param chgCigmaDoNoLikeFront Set in "chgCigmaDoNoLikeFront".
     */
    public void setChgCigmaDoNoLikeFront(String chgCigmaDoNoLikeFront) {
        this.chgCigmaDoNoLikeFront = chgCigmaDoNoLikeFront;
    }

    /**
     * Getter method of "ctrlNoLikeFront".
     * 
     * @return the "ctrlNoLikeFront"
     */
    public String getCtrlNoLikeFront() {
        return ctrlNoLikeFront;
    }

    /**
     * Setter method of "ctrlNoLikeFront".
     * 
     * @param ctrlNoLikeFront Set in "ctrlNoLikeFront".
     */
    public void setCtrlNoLikeFront(String ctrlNoLikeFront) {
        this.ctrlNoLikeFront = ctrlNoLikeFront;
    }

    /**
     * Getter method of "chgReasonLikeFront".
     * 
     * @return the "chgReasonLikeFront"
     */
    public String getChgReasonLikeFront() {
        return chgReasonLikeFront;
    }

    /**
     * Setter method of "chgReasonLikeFront".
     * 
     * @param chgReasonLikeFront Set in "chgReasonLikeFront".
     */
    public void setChgReasonLikeFront(String chgReasonLikeFront) {
        this.chgReasonLikeFront = chgReasonLikeFront;
    }

    /**
     * Getter method of "itemDescLikeFront".
     * 
     * @return the "itemDescLikeFront"
     */
    public String getItemDescLikeFront() {
        return itemDescLikeFront;
    }

    /**
     * Setter method of "itemDescLikeFront".
     * 
     * @param itemDescLikeFront Set in "itemDescLikeFront".
     */
    public void setItemDescLikeFront(String itemDescLikeFront) {
        this.itemDescLikeFront = itemDescLikeFront;
    }

    /**
     * Getter method of "unitOfMeasureLikeFront".
     * 
     * @return the "unitOfMeasureLikeFront"
     */
    public String getUnitOfMeasureLikeFront() {
        return unitOfMeasureLikeFront;
    }

    /**
     * Setter method of "unitOfMeasureLikeFront".
     * 
     * @param unitOfMeasureLikeFront Set in "unitOfMeasureLikeFront".
     */
    public void setUnitOfMeasureLikeFront(String unitOfMeasureLikeFront) {
        this.unitOfMeasureLikeFront = unitOfMeasureLikeFront;
    }

    /**
     * Getter method of "modelLikeFront".
     * 
     * @return the "modelLikeFront"
     */
    public String getModelLikeFront() {
        return modelLikeFront;
    }

    /**
     * Setter method of "modelLikeFront".
     * 
     * @param modelLikeFront Set in "modelLikeFront".
     */
    public void setModelLikeFront(String modelLikeFront) {
        this.modelLikeFront = modelLikeFront;
    }

    /**
     * Getter method of "rcvLaneLikeFront".
     * 
     * @return the "rcvLaneLikeFront"
     */
    public String getRcvLaneLikeFront() {
        return rcvLaneLikeFront;
    }

    /**
     * Setter method of "rcvLaneLikeFront".
     * 
     * @param rcvLaneLikeFront Set in "rcvLaneLikeFront".
     */
    public void setRcvLaneLikeFront(String rcvLaneLikeFront) {
        this.rcvLaneLikeFront = rcvLaneLikeFront;
    }

    /**
     * Getter method of "whLocationLikeFront".
     * 
     * @return the "whLocationLikeFront"
     */
    public String getWhLocationLikeFront() {
        return whLocationLikeFront;
    }

    /**
     * Setter method of "whLocationLikeFront".
     * 
     * @param whLocationLikeFront Set in "whLocationLikeFront".
     */
    public void setWhLocationLikeFront(String whLocationLikeFront) {
        this.whLocationLikeFront = whLocationLikeFront;
    }

    /**
     * Getter method of "urgentOrderFlgLikeFront".
     * 
     * @return the "urgentOrderFlgLikeFront"
     */
    public String getUrgentOrderFlgLikeFront() {
        return urgentOrderFlgLikeFront;
    }

    /**
     * Setter method of "urgentOrderFlgLikeFront".
     * 
     * @param urgentOrderFlgLikeFront Set in "urgentOrderFlgLikeFront".
     */
    public void setUrgentOrderFlgLikeFront(String urgentOrderFlgLikeFront) {
        this.urgentOrderFlgLikeFront = urgentOrderFlgLikeFront;
    }

    /**
     * Getter method of "mailFlgLikeFront".
     * 
     * @return the "mailFlgLikeFront"
     */
    public String getMailFlgLikeFront() {
        return mailFlgLikeFront;
    }

    /**
     * Setter method of "mailFlgLikeFront".
     * 
     * @param mailFlgLikeFront Set in "mailFlgLikeFront".
     */
    public void setMailFlgLikeFront(String mailFlgLikeFront) {
        this.mailFlgLikeFront = mailFlgLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
