/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


public class SPSTDoKanbanSeqDNDomain   {
	private static final long serialVersionUID = 1L;
	 private String databaseName;
	private BigDecimal doId;
	private String sPn;
	private String dPn;
	private BigDecimal kanbanSeqId;
	private String deliveryRunDate;
	private BigDecimal deliveryRunno;
	private BigDecimal kanbanSeqno;
	private String kanbanCtrlno;
	private BigDecimal kanbanLotSize;
	private String tempKanbanTag;
	private BigDecimal deliveryDueTime;
	private String deliveryRunType;
	private String runScheduleDate;
	private BigDecimal qrRcvBatchNo;
	private BigDecimal qrScanTime;
	private String createDscId;
	private String createDate;
	private String createTime;
	private String lastUpdateDscId;
	private String lastUpdateDate;
	private String lastUpdateTime;
	public BigDecimal getDoId() {
		return doId;
	}
	public void setDoId(BigDecimal doId) {
		this.doId = doId;
	}
	public String getsPn() {
		return sPn;
	}
	public void setsPn(String sPn) {
		this.sPn = sPn;
	}
	public String getdPn() {
		return dPn;
	}
	public void setdPn(String dPn) {
		this.dPn = dPn;
	}
	public BigDecimal getKanbanSeqId() {
		return kanbanSeqId;
	}
	public void setKanbanSeqId(BigDecimal kanbanSeqId) {
		this.kanbanSeqId = kanbanSeqId;
	}
	public String getDeliveryRunDate() {
		return deliveryRunDate;
	}
	public void setDeliveryRunDate(String deliveryRunDate) {
		this.deliveryRunDate = deliveryRunDate;
	}
	public BigDecimal getDeliveryRunno() {
		return deliveryRunno;
	}
	public void setDeliveryRunno(BigDecimal deliveryRunno) {
		this.deliveryRunno = deliveryRunno;
	}
	public BigDecimal getKanbanSeqno() {
		return kanbanSeqno;
	}
	public void setKanbanSeqno(BigDecimal kanbanSeqno) {
		this.kanbanSeqno = kanbanSeqno;
	}
	public String getKanbanCtrlno() {
		return kanbanCtrlno;
	}
	public void setKanbanCtrlno(String kanbanCtrlno) {
		this.kanbanCtrlno = kanbanCtrlno;
	}
	public BigDecimal getKanbanLotSize() {
		return kanbanLotSize;
	}
	public void setKanbanLotSize(BigDecimal kanbanLotSize) {
		this.kanbanLotSize = kanbanLotSize;
	}
	public String getTempKanbanTag() {
		return tempKanbanTag;
	}
	public void setTempKanbanTag(String tempKanbanTag) {
		this.tempKanbanTag = tempKanbanTag;
	}
	public BigDecimal getDeliveryDueTime() {
		return deliveryDueTime;
	}
	public void setDeliveryDueTime(BigDecimal deliveryDueTime) {
		this.deliveryDueTime = deliveryDueTime;
	}
	public String getDeliveryRunType() {
		return deliveryRunType;
	}
	public void setDeliveryRunType(String deliveryRunType) {
		this.deliveryRunType = deliveryRunType;
	}
	public String getRunScheduleDate() {
		return runScheduleDate;
	}
	public void setRunScheduleDate(String runScheduleDate) {
		this.runScheduleDate = runScheduleDate;
	}
	public BigDecimal getQrRcvBatchNo() {
		return qrRcvBatchNo;
	}
	public void setQrRcvBatchNo(BigDecimal qrRcvBatchNo) {
		this.qrRcvBatchNo = qrRcvBatchNo;
	}
	public BigDecimal getQrScanTime() {
		return qrScanTime;
	}
	public void setQrScanTime(BigDecimal qrScanTime) {
		this.qrScanTime = qrScanTime;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	
	
}
