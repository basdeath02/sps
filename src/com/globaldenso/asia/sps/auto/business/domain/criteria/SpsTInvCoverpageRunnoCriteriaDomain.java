/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/18       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTInvCoverpageRunno".<br />
 * Table overview: SPS_T_INV_COVERPAGE_RUNNO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/18 14:44:35<br />
 * 
 * This module generated automatically in 2015/05/18 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvCoverpageRunnoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Year of running number
     */
    private String runningYear;

    /**
     * Month of running number
     */
    private String runningMonth;

    /**
     * date of running number
     */
    private String runningDate;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Running Number
     */
    private BigDecimal runno;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * Year of running number(condition whether the column value at the beginning is equal to the value)
     */
    private String runningYearLikeFront;

    /**
     * Month of running number(condition whether the column value at the beginning is equal to the value)
     */
    private String runningMonthLikeFront;

    /**
     * date of running number(condition whether the column value at the beginning is equal to the value)
     */
    private String runningDateLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * D_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * Default constructor
     */
    public SpsTInvCoverpageRunnoCriteriaDomain() {
    }

    /**
     * Getter method of "runningYear".
     * 
     * @return the "runningYear"
     */
    public String getRunningYear() {
        return runningYear;
    }

    /**
     * Setter method of "runningYear".
     * 
     * @param runningYear Set in "runningYear".
     */
    public void setRunningYear(String runningYear) {
        this.runningYear = runningYear;
    }

    /**
     * Getter method of "runningMonth".
     * 
     * @return the "runningMonth"
     */
    public String getRunningMonth() {
        return runningMonth;
    }

    /**
     * Setter method of "runningMonth".
     * 
     * @param runningMonth Set in "runningMonth".
     */
    public void setRunningMonth(String runningMonth) {
        this.runningMonth = runningMonth;
    }

    /**
     * Getter method of "runningDate".
     * 
     * @return the "runningDate"
     */
    public String getRunningDate() {
        return runningDate;
    }

    /**
     * Setter method of "runningDate".
     * 
     * @param runningDate Set in "runningDate".
     */
    public void setRunningDate(String runningDate) {
        this.runningDate = runningDate;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "runno".
     * 
     * @return the "runno"
     */
    public BigDecimal getRunno() {
        return runno;
    }

    /**
     * Setter method of "runno".
     * 
     * @param runno Set in "runno".
     */
    public void setRunno(BigDecimal runno) {
        this.runno = runno;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "runningYearLikeFront".
     * 
     * @return the "runningYearLikeFront"
     */
    public String getRunningYearLikeFront() {
        return runningYearLikeFront;
    }

    /**
     * Setter method of "runningYearLikeFront".
     * 
     * @param runningYearLikeFront Set in "runningYearLikeFront".
     */
    public void setRunningYearLikeFront(String runningYearLikeFront) {
        this.runningYearLikeFront = runningYearLikeFront;
    }

    /**
     * Getter method of "runningMonthLikeFront".
     * 
     * @return the "runningMonthLikeFront"
     */
    public String getRunningMonthLikeFront() {
        return runningMonthLikeFront;
    }

    /**
     * Setter method of "runningMonthLikeFront".
     * 
     * @param runningMonthLikeFront Set in "runningMonthLikeFront".
     */
    public void setRunningMonthLikeFront(String runningMonthLikeFront) {
        this.runningMonthLikeFront = runningMonthLikeFront;
    }

    /**
     * Getter method of "runningDateLikeFront".
     * 
     * @return the "runningDateLikeFront"
     */
    public String getRunningDateLikeFront() {
        return runningDateLikeFront;
    }

    /**
     * Setter method of "runningDateLikeFront".
     * 
     * @param runningDateLikeFront Set in "runningDateLikeFront".
     */
    public void setRunningDateLikeFront(String runningDateLikeFront) {
        this.runningDateLikeFront = runningDateLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

}
