/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * A "Domain" class of "SpsCigmaPoError"<br />
 * Table overview: SPS_CIGMA_PO_ERROR<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaPoErrorDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No for P/O Error
     */
    private BigDecimal poErrorId;

    /**
     * CIGMA P/O No
     */
    private String cigmaPoNo;

    /**
     * Data Type from CIGMA
     */
    private String dataType;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * P/O Issue Date
     */
    private Date issueDate;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Calendar Period
     */
    private Date etd;

    /**
     * Order Qty
     */
    private BigDecimal orderQty;

    /**
     * Send Mail Flag ;
0 : Do not send, 1: Send
     */
    private String mailFlg;

    /**
     * Update by User ID
     */
    private String updateByUserId;

    /**
     * Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Due Date
     */
    private Date dueDate;

    /**
     * SPS Error Type Flag
1 : VENDOR_CD not found in SPS system
2 : PARTS_NO not found in SPS system
     */
    private String errorTypeFlg;

    /**
     * Default constructor
     */
    public SpsCigmaPoErrorDomain() {
    }

    /**
     * Getter method of "poErrorId"
     * 
     * @return the poErrorId
     */
    public BigDecimal getPoErrorId() {
        return poErrorId;
    }

    /**
     * Setter method of "poErrorId"
     * 
     * @param poErrorId Set in "poErrorId".
     */
    public void setPoErrorId(BigDecimal poErrorId) {
        this.poErrorId = poErrorId;
    }

    /**
     * Getter method of "cigmaPoNo"
     * 
     * @return the cigmaPoNo
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }

    /**
     * Setter method of "cigmaPoNo"
     * 
     * @param cigmaPoNo Set in "cigmaPoNo".
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * Getter method of "dataType"
     * 
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType"
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "issueDate"
     * 
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Setter method of "issueDate"
     * 
     * @param issueDate Set in "issueDate".
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "etd"
     * 
     * @return the etd
     */
    public Date getEtd() {
        return etd;
    }

    /**
     * Setter method of "etd"
     * 
     * @param etd Set in "etd".
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }

    /**
     * Getter method of "orderQty"
     * 
     * @return the orderQty
     */
    public BigDecimal getOrderQty() {
        return orderQty;
    }

    /**
     * Setter method of "orderQty"
     * 
     * @param orderQty Set in "orderQty".
     */
    public void setOrderQty(BigDecimal orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * Getter method of "mailFlg"
     * 
     * @return the mailFlg
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg"
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "updateByUserId"
     * 
     * @return the updateByUserId
     */
    public String getUpdateByUserId() {
        return updateByUserId;
    }

    /**
     * Setter method of "updateByUserId"
     * 
     * @param updateByUserId Set in "updateByUserId".
     */
    public void setUpdateByUserId(String updateByUserId) {
        this.updateByUserId = updateByUserId;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "dueDate"
     * 
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Setter method of "dueDate"
     * 
     * @param dueDate Set in "dueDate".
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Getter method of "errorTypeFlg"
     * 
     * @return the errorTypeFlg
     */
    public String getErrorTypeFlg() {
        return errorTypeFlg;
    }

    /**
     * Setter method of "errorTypeFlg"
     * 
     * @param errorTypeFlg Set in "errorTypeFlg".
     */
    public void setErrorTypeFlg(String errorTypeFlg) {
        this.errorTypeFlg = errorTypeFlg;
    }

}
