/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/10/30       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpUploadInvoice".<br />
 * Table overview: SPS_TMP_UPLOAD_INVOICE<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/10/30 14:41:58<br />
 * 
 * This module generated automatically in 2015/10/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadInvoiceCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running number of Upload Invoice
     */
    private BigDecimal uploadInvoiceId;

    /**
     * Session Identity
     */
    private String sessionId;

    /**
     * AS400 Vendor Code
     */
    private String vendorCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * Supplier TAX Identity
     */
    private String sTaxId;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Company Plant Code
     */
    private String dPcd;

    /**
     * Invoice Number
     */
    private String invoiceNo;

    /**
     * Invoice Date
     */
    private Date invoiceDate;

    /**
     * "1": VAT/GST , "0": None VAT/GST
     */
    private String vatType;

    /**
     * Invoice VAT Rate
     */
    private BigDecimal vatRate;

    /**
     * Supplier Currency Code
     */
    private String currencyCd;

    /**
     * Invoice Base Amount
     */
    private BigDecimal baseAmount;

    /**
     * Invoice VAT Amount
     */
    private BigDecimal vatAmount;

    /**
     * Invoice Total Amount
     */
    private BigDecimal totalAmount;

    /**
     * Credit Note Number
     */
    private String cnNo;

    /**
     * Credit Note Date
     */
    private Date cnDate;

    /**
     * Credit Note Base Amount
     */
    private BigDecimal cnBaseAmount;

    /**
     * Credit Note VAT Amount
     */
    private BigDecimal cnVatAmount;

    /**
     * Credit Note Total Amount
     */
    private BigDecimal cnTotalAmount;

    /**
     * 0 : Not error
1 : Error
2 : Warning
     */
    private String uploadResult;

    /**
     * Error code
     */
    private String errorCode;

    /**
     * CSV Line Number
     */
    private BigDecimal csvLineNo;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * Trip Number
     */
    private String tripNo;

    /**
     * SPS D/O No.
     */
    private String spsDoNo;

    /**
     * Latest D/O Revision
     */
    private String revision;

    /**
     * Plan ETA
     */
    private Timestamp planEta;

    /**
     * Actual ETD
     */
    private Timestamp actualEtd;

    /**
     * Coordinated Universal Time
     */
    private String utc;

    /**
     * DESNO Part Number
     */
    private String dPn;

    /**
     * Supplier Part Number
     */
    private String sPn;

    /**
     * Shipping Quantity
     */
    private BigDecimal shippingQty;

    /**
     * Supplier Unit ot Measure
     */
    private String sUnitOfMeasure;

    /**
     * Supplier Unit Price
     */
    private BigDecimal sPriceUnit;

    /**
     * DSC ID of Uploaded data
     */
    private String uploadDscId;

    /**
     * Datetime of Uploaded data
     */
    private Timestamp uploadDatetime;

    /**
     * Session Identity(condition whether the column value at the beginning is equal to the value)
     */
    private String sessionIdLikeFront;

    /**
     * AS400 Vendor Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * Supplier Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * Supplier TAX Identity(condition whether the column value at the beginning is equal to the value)
     */
    private String sTaxIdLikeFront;

    /**
     * DENSO Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Company Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * Invoice Number(condition whether the column value at the beginning is equal to the value)
     */
    private String invoiceNoLikeFront;

    /**
     * Invoice Date(condition whether the column value is greater than or equal to the value)
     */
    private Date invoiceDateGreaterThanEqual;

    /**
     * Invoice Date(condition whether the column value is less than or equal to the value)
     */
    private Date invoiceDateLessThanEqual;

    /**
     * "1": VAT/GST , "0": None VAT/GST(condition whether the column value at the beginning is equal to the value)
     */
    private String vatTypeLikeFront;

    /**
     * Supplier Currency Code(condition whether the column value at the beginning is equal to the value)
     */
    private String currencyCdLikeFront;

    /**
     * Credit Note Number(condition whether the column value at the beginning is equal to the value)
     */
    private String cnNoLikeFront;

    /**
     * Credit Note Date(condition whether the column value is greater than or equal to the value)
     */
    private Date cnDateGreaterThanEqual;

    /**
     * Credit Note Date(condition whether the column value is less than or equal to the value)
     */
    private Date cnDateLessThanEqual;

    /**
     * 0 : Not error
1 : Error
2 : Warning(condition whether the column value at the beginning is equal to the value)
     */
    private String uploadResultLikeFront;

    /**
     * Error code(condition whether the column value at the beginning is equal to the value)
     */
    private String errorCodeLikeFront;

    /**
     * ASN Number(condition whether the column value at the beginning is equal to the value)
     */
    private String asnNoLikeFront;

    /**
     * Trip Number(condition whether the column value at the beginning is equal to the value)
     */
    private String tripNoLikeFront;

    /**
     * SPS D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String spsDoNoLikeFront;

    /**
     * Latest D/O Revision(condition whether the column value at the beginning is equal to the value)
     */
    private String revisionLikeFront;

    /**
     * Plan ETA(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp planEtaGreaterThanEqual;

    /**
     * Plan ETA(condition whether the column value is less than or equal to the value)
     */
    private Timestamp planEtaLessThanEqual;

    /**
     * Actual ETD(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp actualEtdGreaterThanEqual;

    /**
     * Actual ETD(condition whether the column value is less than or equal to the value)
     */
    private Timestamp actualEtdLessThanEqual;

    /**
     * Coordinated Universal Time(condition whether the column value at the beginning is equal to the value)
     */
    private String utcLikeFront;

    /**
     * DESNO Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Supplier Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * Supplier Unit ot Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String sUnitOfMeasureLikeFront;

    /**
     * DSC ID of Uploaded data(condition whether the column value at the beginning is equal to the value)
     */
    private String uploadDscIdLikeFront;

    /**
     * Datetime of Uploaded data(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp uploadDatetimeGreaterThanEqual;

    /**
     * Datetime of Uploaded data(condition whether the column value is less than or equal to the value)
     */
    private Timestamp uploadDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTmpUploadInvoiceCriteriaDomain() {
    }

    /**
     * Getter method of "uploadInvoiceId".
     * 
     * @return the "uploadInvoiceId"
     */
    public BigDecimal getUploadInvoiceId() {
        return uploadInvoiceId;
    }

    /**
     * Setter method of "uploadInvoiceId".
     * 
     * @param uploadInvoiceId Set in "uploadInvoiceId".
     */
    public void setUploadInvoiceId(BigDecimal uploadInvoiceId) {
        this.uploadInvoiceId = uploadInvoiceId;
    }

    /**
     * Getter method of "sessionId".
     * 
     * @return the "sessionId"
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Setter method of "sessionId".
     * 
     * @param sessionId Set in "sessionId".
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "sTaxId".
     * 
     * @return the "sTaxId"
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * Setter method of "sTaxId".
     * 
     * @param sTaxId Set in "sTaxId".
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "invoiceNo".
     * 
     * @return the "invoiceNo"
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Setter method of "invoiceNo".
     * 
     * @param invoiceNo Set in "invoiceNo".
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * Getter method of "invoiceDate".
     * 
     * @return the "invoiceDate"
     */
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Setter method of "invoiceDate".
     * 
     * @param invoiceDate Set in "invoiceDate".
     */
    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * Getter method of "vatType".
     * 
     * @return the "vatType"
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * Setter method of "vatType".
     * 
     * @param vatType Set in "vatType".
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }

    /**
     * Getter method of "vatRate".
     * 
     * @return the "vatRate"
     */
    public BigDecimal getVatRate() {
        return vatRate;
    }

    /**
     * Setter method of "vatRate".
     * 
     * @param vatRate Set in "vatRate".
     */
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * Getter method of "currencyCd".
     * 
     * @return the "currencyCd"
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * Setter method of "currencyCd".
     * 
     * @param currencyCd Set in "currencyCd".
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * Getter method of "baseAmount".
     * 
     * @return the "baseAmount"
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /**
     * Setter method of "baseAmount".
     * 
     * @param baseAmount Set in "baseAmount".
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /**
     * Getter method of "vatAmount".
     * 
     * @return the "vatAmount"
     */
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    /**
     * Setter method of "vatAmount".
     * 
     * @param vatAmount Set in "vatAmount".
     */
    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    /**
     * Getter method of "totalAmount".
     * 
     * @return the "totalAmount"
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Setter method of "totalAmount".
     * 
     * @param totalAmount Set in "totalAmount".
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Getter method of "cnNo".
     * 
     * @return the "cnNo"
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * Setter method of "cnNo".
     * 
     * @param cnNo Set in "cnNo".
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * Getter method of "cnDate".
     * 
     * @return the "cnDate"
     */
    public Date getCnDate() {
        return cnDate;
    }

    /**
     * Setter method of "cnDate".
     * 
     * @param cnDate Set in "cnDate".
     */
    public void setCnDate(Date cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * Getter method of "cnBaseAmount".
     * 
     * @return the "cnBaseAmount"
     */
    public BigDecimal getCnBaseAmount() {
        return cnBaseAmount;
    }

    /**
     * Setter method of "cnBaseAmount".
     * 
     * @param cnBaseAmount Set in "cnBaseAmount".
     */
    public void setCnBaseAmount(BigDecimal cnBaseAmount) {
        this.cnBaseAmount = cnBaseAmount;
    }

    /**
     * Getter method of "cnVatAmount".
     * 
     * @return the "cnVatAmount"
     */
    public BigDecimal getCnVatAmount() {
        return cnVatAmount;
    }

    /**
     * Setter method of "cnVatAmount".
     * 
     * @param cnVatAmount Set in "cnVatAmount".
     */
    public void setCnVatAmount(BigDecimal cnVatAmount) {
        this.cnVatAmount = cnVatAmount;
    }

    /**
     * Getter method of "cnTotalAmount".
     * 
     * @return the "cnTotalAmount"
     */
    public BigDecimal getCnTotalAmount() {
        return cnTotalAmount;
    }

    /**
     * Setter method of "cnTotalAmount".
     * 
     * @param cnTotalAmount Set in "cnTotalAmount".
     */
    public void setCnTotalAmount(BigDecimal cnTotalAmount) {
        this.cnTotalAmount = cnTotalAmount;
    }

    /**
     * Getter method of "uploadResult".
     * 
     * @return the "uploadResult"
     */
    public String getUploadResult() {
        return uploadResult;
    }

    /**
     * Setter method of "uploadResult".
     * 
     * @param uploadResult Set in "uploadResult".
     */
    public void setUploadResult(String uploadResult) {
        this.uploadResult = uploadResult;
    }

    /**
     * Getter method of "errorCode".
     * 
     * @return the "errorCode"
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Setter method of "errorCode".
     * 
     * @param errorCode Set in "errorCode".
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Getter method of "csvLineNo".
     * 
     * @return the "csvLineNo"
     */
    public BigDecimal getCsvLineNo() {
        return csvLineNo;
    }

    /**
     * Setter method of "csvLineNo".
     * 
     * @param csvLineNo Set in "csvLineNo".
     */
    public void setCsvLineNo(BigDecimal csvLineNo) {
        this.csvLineNo = csvLineNo;
    }

    /**
     * Getter method of "asnNo".
     * 
     * @return the "asnNo"
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo".
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "tripNo".
     * 
     * @return the "tripNo"
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo".
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "spsDoNo".
     * 
     * @return the "spsDoNo"
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo".
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision".
     * 
     * @return the "revision"
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision".
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "planEta".
     * 
     * @return the "planEta"
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * Setter method of "planEta".
     * 
     * @param planEta Set in "planEta".
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * Getter method of "actualEtd".
     * 
     * @return the "actualEtd"
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * Setter method of "actualEtd".
     * 
     * @param actualEtd Set in "actualEtd".
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }

    /**
     * Getter method of "utc".
     * 
     * @return the "utc"
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Setter method of "utc".
     * 
     * @param utc Set in "utc".
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "shippingQty".
     * 
     * @return the "shippingQty"
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty".
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "sUnitOfMeasure".
     * 
     * @return the "sUnitOfMeasure"
     */
    public String getSUnitOfMeasure() {
        return sUnitOfMeasure;
    }

    /**
     * Setter method of "sUnitOfMeasure".
     * 
     * @param sUnitOfMeasure Set in "sUnitOfMeasure".
     */
    public void setSUnitOfMeasure(String sUnitOfMeasure) {
        this.sUnitOfMeasure = sUnitOfMeasure;
    }

    /**
     * Getter method of "sPriceUnit".
     * 
     * @return the "sPriceUnit"
     */
    public BigDecimal getSPriceUnit() {
        return sPriceUnit;
    }

    /**
     * Setter method of "sPriceUnit".
     * 
     * @param sPriceUnit Set in "sPriceUnit".
     */
    public void setSPriceUnit(BigDecimal sPriceUnit) {
        this.sPriceUnit = sPriceUnit;
    }

    /**
     * Getter method of "uploadDscId".
     * 
     * @return the "uploadDscId"
     */
    public String getUploadDscId() {
        return uploadDscId;
    }

    /**
     * Setter method of "uploadDscId".
     * 
     * @param uploadDscId Set in "uploadDscId".
     */
    public void setUploadDscId(String uploadDscId) {
        this.uploadDscId = uploadDscId;
    }

    /**
     * Getter method of "uploadDatetime".
     * 
     * @return the "uploadDatetime"
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime".
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "sessionIdLikeFront".
     * 
     * @return the "sessionIdLikeFront"
     */
    public String getSessionIdLikeFront() {
        return sessionIdLikeFront;
    }

    /**
     * Setter method of "sessionIdLikeFront".
     * 
     * @param sessionIdLikeFront Set in "sessionIdLikeFront".
     */
    public void setSessionIdLikeFront(String sessionIdLikeFront) {
        this.sessionIdLikeFront = sessionIdLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "sTaxIdLikeFront".
     * 
     * @return the "sTaxIdLikeFront"
     */
    public String getSTaxIdLikeFront() {
        return sTaxIdLikeFront;
    }

    /**
     * Setter method of "sTaxIdLikeFront".
     * 
     * @param sTaxIdLikeFront Set in "sTaxIdLikeFront".
     */
    public void setSTaxIdLikeFront(String sTaxIdLikeFront) {
        this.sTaxIdLikeFront = sTaxIdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "invoiceNoLikeFront".
     * 
     * @return the "invoiceNoLikeFront"
     */
    public String getInvoiceNoLikeFront() {
        return invoiceNoLikeFront;
    }

    /**
     * Setter method of "invoiceNoLikeFront".
     * 
     * @param invoiceNoLikeFront Set in "invoiceNoLikeFront".
     */
    public void setInvoiceNoLikeFront(String invoiceNoLikeFront) {
        this.invoiceNoLikeFront = invoiceNoLikeFront;
    }

    /**
     * Getter method of "invoiceDateGreaterThanEqual".
     * 
     * @return the "invoiceDateGreaterThanEqual"
     */
    public Date getInvoiceDateGreaterThanEqual() {
        return invoiceDateGreaterThanEqual;
    }

    /**
     * Setter method of "invoiceDateGreaterThanEqual".
     * 
     * @param invoiceDateGreaterThanEqual Set in "invoiceDateGreaterThanEqual".
     */
    public void setInvoiceDateGreaterThanEqual(Date invoiceDateGreaterThanEqual) {
        this.invoiceDateGreaterThanEqual = invoiceDateGreaterThanEqual;
    }

    /**
     * Getter method of "invoiceDateLessThanEqual".
     * 
     * @return the "invoiceDateLessThanEqual"
     */
    public Date getInvoiceDateLessThanEqual() {
        return invoiceDateLessThanEqual;
    }

    /**
     * Setter method of "invoiceDateLessThanEqual".
     * 
     * @param invoiceDateLessThanEqual Set in "invoiceDateLessThanEqual".
     */
    public void setInvoiceDateLessThanEqual(Date invoiceDateLessThanEqual) {
        this.invoiceDateLessThanEqual = invoiceDateLessThanEqual;
    }

    /**
     * Getter method of "vatTypeLikeFront".
     * 
     * @return the "vatTypeLikeFront"
     */
    public String getVatTypeLikeFront() {
        return vatTypeLikeFront;
    }

    /**
     * Setter method of "vatTypeLikeFront".
     * 
     * @param vatTypeLikeFront Set in "vatTypeLikeFront".
     */
    public void setVatTypeLikeFront(String vatTypeLikeFront) {
        this.vatTypeLikeFront = vatTypeLikeFront;
    }

    /**
     * Getter method of "currencyCdLikeFront".
     * 
     * @return the "currencyCdLikeFront"
     */
    public String getCurrencyCdLikeFront() {
        return currencyCdLikeFront;
    }

    /**
     * Setter method of "currencyCdLikeFront".
     * 
     * @param currencyCdLikeFront Set in "currencyCdLikeFront".
     */
    public void setCurrencyCdLikeFront(String currencyCdLikeFront) {
        this.currencyCdLikeFront = currencyCdLikeFront;
    }

    /**
     * Getter method of "cnNoLikeFront".
     * 
     * @return the "cnNoLikeFront"
     */
    public String getCnNoLikeFront() {
        return cnNoLikeFront;
    }

    /**
     * Setter method of "cnNoLikeFront".
     * 
     * @param cnNoLikeFront Set in "cnNoLikeFront".
     */
    public void setCnNoLikeFront(String cnNoLikeFront) {
        this.cnNoLikeFront = cnNoLikeFront;
    }

    /**
     * Getter method of "cnDateGreaterThanEqual".
     * 
     * @return the "cnDateGreaterThanEqual"
     */
    public Date getCnDateGreaterThanEqual() {
        return cnDateGreaterThanEqual;
    }

    /**
     * Setter method of "cnDateGreaterThanEqual".
     * 
     * @param cnDateGreaterThanEqual Set in "cnDateGreaterThanEqual".
     */
    public void setCnDateGreaterThanEqual(Date cnDateGreaterThanEqual) {
        this.cnDateGreaterThanEqual = cnDateGreaterThanEqual;
    }

    /**
     * Getter method of "cnDateLessThanEqual".
     * 
     * @return the "cnDateLessThanEqual"
     */
    public Date getCnDateLessThanEqual() {
        return cnDateLessThanEqual;
    }

    /**
     * Setter method of "cnDateLessThanEqual".
     * 
     * @param cnDateLessThanEqual Set in "cnDateLessThanEqual".
     */
    public void setCnDateLessThanEqual(Date cnDateLessThanEqual) {
        this.cnDateLessThanEqual = cnDateLessThanEqual;
    }

    /**
     * Getter method of "uploadResultLikeFront".
     * 
     * @return the "uploadResultLikeFront"
     */
    public String getUploadResultLikeFront() {
        return uploadResultLikeFront;
    }

    /**
     * Setter method of "uploadResultLikeFront".
     * 
     * @param uploadResultLikeFront Set in "uploadResultLikeFront".
     */
    public void setUploadResultLikeFront(String uploadResultLikeFront) {
        this.uploadResultLikeFront = uploadResultLikeFront;
    }

    /**
     * Getter method of "errorCodeLikeFront".
     * 
     * @return the "errorCodeLikeFront"
     */
    public String getErrorCodeLikeFront() {
        return errorCodeLikeFront;
    }

    /**
     * Setter method of "errorCodeLikeFront".
     * 
     * @param errorCodeLikeFront Set in "errorCodeLikeFront".
     */
    public void setErrorCodeLikeFront(String errorCodeLikeFront) {
        this.errorCodeLikeFront = errorCodeLikeFront;
    }

    /**
     * Getter method of "asnNoLikeFront".
     * 
     * @return the "asnNoLikeFront"
     */
    public String getAsnNoLikeFront() {
        return asnNoLikeFront;
    }

    /**
     * Setter method of "asnNoLikeFront".
     * 
     * @param asnNoLikeFront Set in "asnNoLikeFront".
     */
    public void setAsnNoLikeFront(String asnNoLikeFront) {
        this.asnNoLikeFront = asnNoLikeFront;
    }

    /**
     * Getter method of "tripNoLikeFront".
     * 
     * @return the "tripNoLikeFront"
     */
    public String getTripNoLikeFront() {
        return tripNoLikeFront;
    }

    /**
     * Setter method of "tripNoLikeFront".
     * 
     * @param tripNoLikeFront Set in "tripNoLikeFront".
     */
    public void setTripNoLikeFront(String tripNoLikeFront) {
        this.tripNoLikeFront = tripNoLikeFront;
    }

    /**
     * Getter method of "spsDoNoLikeFront".
     * 
     * @return the "spsDoNoLikeFront"
     */
    public String getSpsDoNoLikeFront() {
        return spsDoNoLikeFront;
    }

    /**
     * Setter method of "spsDoNoLikeFront".
     * 
     * @param spsDoNoLikeFront Set in "spsDoNoLikeFront".
     */
    public void setSpsDoNoLikeFront(String spsDoNoLikeFront) {
        this.spsDoNoLikeFront = spsDoNoLikeFront;
    }

    /**
     * Getter method of "revisionLikeFront".
     * 
     * @return the "revisionLikeFront"
     */
    public String getRevisionLikeFront() {
        return revisionLikeFront;
    }

    /**
     * Setter method of "revisionLikeFront".
     * 
     * @param revisionLikeFront Set in "revisionLikeFront".
     */
    public void setRevisionLikeFront(String revisionLikeFront) {
        this.revisionLikeFront = revisionLikeFront;
    }

    /**
     * Getter method of "planEtaGreaterThanEqual".
     * 
     * @return the "planEtaGreaterThanEqual"
     */
    public Timestamp getPlanEtaGreaterThanEqual() {
        return planEtaGreaterThanEqual;
    }

    /**
     * Setter method of "planEtaGreaterThanEqual".
     * 
     * @param planEtaGreaterThanEqual Set in "planEtaGreaterThanEqual".
     */
    public void setPlanEtaGreaterThanEqual(Timestamp planEtaGreaterThanEqual) {
        this.planEtaGreaterThanEqual = planEtaGreaterThanEqual;
    }

    /**
     * Getter method of "planEtaLessThanEqual".
     * 
     * @return the "planEtaLessThanEqual"
     */
    public Timestamp getPlanEtaLessThanEqual() {
        return planEtaLessThanEqual;
    }

    /**
     * Setter method of "planEtaLessThanEqual".
     * 
     * @param planEtaLessThanEqual Set in "planEtaLessThanEqual".
     */
    public void setPlanEtaLessThanEqual(Timestamp planEtaLessThanEqual) {
        this.planEtaLessThanEqual = planEtaLessThanEqual;
    }

    /**
     * Getter method of "actualEtdGreaterThanEqual".
     * 
     * @return the "actualEtdGreaterThanEqual"
     */
    public Timestamp getActualEtdGreaterThanEqual() {
        return actualEtdGreaterThanEqual;
    }

    /**
     * Setter method of "actualEtdGreaterThanEqual".
     * 
     * @param actualEtdGreaterThanEqual Set in "actualEtdGreaterThanEqual".
     */
    public void setActualEtdGreaterThanEqual(Timestamp actualEtdGreaterThanEqual) {
        this.actualEtdGreaterThanEqual = actualEtdGreaterThanEqual;
    }

    /**
     * Getter method of "actualEtdLessThanEqual".
     * 
     * @return the "actualEtdLessThanEqual"
     */
    public Timestamp getActualEtdLessThanEqual() {
        return actualEtdLessThanEqual;
    }

    /**
     * Setter method of "actualEtdLessThanEqual".
     * 
     * @param actualEtdLessThanEqual Set in "actualEtdLessThanEqual".
     */
    public void setActualEtdLessThanEqual(Timestamp actualEtdLessThanEqual) {
        this.actualEtdLessThanEqual = actualEtdLessThanEqual;
    }

    /**
     * Getter method of "utcLikeFront".
     * 
     * @return the "utcLikeFront"
     */
    public String getUtcLikeFront() {
        return utcLikeFront;
    }

    /**
     * Setter method of "utcLikeFront".
     * 
     * @param utcLikeFront Set in "utcLikeFront".
     */
    public void setUtcLikeFront(String utcLikeFront) {
        this.utcLikeFront = utcLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "sUnitOfMeasureLikeFront".
     * 
     * @return the "sUnitOfMeasureLikeFront"
     */
    public String getSUnitOfMeasureLikeFront() {
        return sUnitOfMeasureLikeFront;
    }

    /**
     * Setter method of "sUnitOfMeasureLikeFront".
     * 
     * @param sUnitOfMeasureLikeFront Set in "sUnitOfMeasureLikeFront".
     */
    public void setSUnitOfMeasureLikeFront(String sUnitOfMeasureLikeFront) {
        this.sUnitOfMeasureLikeFront = sUnitOfMeasureLikeFront;
    }

    /**
     * Getter method of "uploadDscIdLikeFront".
     * 
     * @return the "uploadDscIdLikeFront"
     */
    public String getUploadDscIdLikeFront() {
        return uploadDscIdLikeFront;
    }

    /**
     * Setter method of "uploadDscIdLikeFront".
     * 
     * @param uploadDscIdLikeFront Set in "uploadDscIdLikeFront".
     */
    public void setUploadDscIdLikeFront(String uploadDscIdLikeFront) {
        this.uploadDscIdLikeFront = uploadDscIdLikeFront;
    }

    /**
     * Getter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @return the "uploadDatetimeGreaterThanEqual"
     */
    public Timestamp getUploadDatetimeGreaterThanEqual() {
        return uploadDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @param uploadDatetimeGreaterThanEqual Set in "uploadDatetimeGreaterThanEqual".
     */
    public void setUploadDatetimeGreaterThanEqual(Timestamp uploadDatetimeGreaterThanEqual) {
        this.uploadDatetimeGreaterThanEqual = uploadDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeLessThanEqual".
     * 
     * @return the "uploadDatetimeLessThanEqual"
     */
    public Timestamp getUploadDatetimeLessThanEqual() {
        return uploadDatetimeLessThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeLessThanEqual".
     * 
     * @param uploadDatetimeLessThanEqual Set in "uploadDatetimeLessThanEqual".
     */
    public void setUploadDatetimeLessThanEqual(Timestamp uploadDatetimeLessThanEqual) {
        this.uploadDatetimeLessThanEqual = uploadDatetimeLessThanEqual;
    }

}
