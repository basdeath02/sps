/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpSupplierInfo".<br />
 * Table overview: SPS_TMP_SUPPLIER_INFO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpSupplierInfoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * USER_DSC_ID
     */
    private String userDscId;

    /**
     * SESSION_CD
     */
    private String sessionCd;

    /**
     * LINE_NO
     */
    private BigDecimal lineNo;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * D_PN
     */
    private String dPn;

    /**
     * S_PN
     */
    private String sPn;

    /**
     * EFFECT_DATE
     */
    private Timestamp effectDate;

    /**
     * UPLOAD_DATETIME
     */
    private Timestamp uploadDatetime;

    /**
     * RELATION_LAST_UPDATE
     */
    private Timestamp relationLastUpdate;

    /**
     * PART_LAST_UPDATE
     */
    private Timestamp partLastUpdate;

    /**
     * IS_ACTUAL_REGISTER
     */
    private String isActualRegister;

    /**
     * TO_ACTUAL_DATETIME
     */
    private Timestamp toActualDatetime;

    /**
     * FLAG
     */
    private String flag;

    /**
     * VENDOR_CD
     */
    private String vendorCd;

    /**
     * USER_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String userDscIdLikeFront;

    /**
     * SESSION_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String sessionCdLikeFront;

    /**
     * D_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * S_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * D_PCD(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * S_PCD(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * D_PN(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * S_PN(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * EFFECT_DATE(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectDateGreaterThanEqual;

    /**
     * EFFECT_DATE(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectDateLessThanEqual;

    /**
     * UPLOAD_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp uploadDatetimeGreaterThanEqual;

    /**
     * UPLOAD_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp uploadDatetimeLessThanEqual;

    /**
     * RELATION_LAST_UPDATE(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp relationLastUpdateGreaterThanEqual;

    /**
     * RELATION_LAST_UPDATE(condition whether the column value is less than or equal to the value)
     */
    private Timestamp relationLastUpdateLessThanEqual;

    /**
     * PART_LAST_UPDATE(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp partLastUpdateGreaterThanEqual;

    /**
     * PART_LAST_UPDATE(condition whether the column value is less than or equal to the value)
     */
    private Timestamp partLastUpdateLessThanEqual;

    /**
     * IS_ACTUAL_REGISTER(condition whether the column value at the beginning is equal to the value)
     */
    private String isActualRegisterLikeFront;

    /**
     * TO_ACTUAL_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp toActualDatetimeGreaterThanEqual;

    /**
     * TO_ACTUAL_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp toActualDatetimeLessThanEqual;

    /**
     * FLAG(condition whether the column value at the beginning is equal to the value)
     */
    private String flagLikeFront;

    /**
     * VENDOR_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * Default constructor
     */
    public SpsTmpSupplierInfoCriteriaDomain() {
    }

    /**
     * Getter method of "userDscId".
     * 
     * @return the "userDscId"
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId".
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd".
     * 
     * @return the "sessionCd"
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd".
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo".
     * 
     * @return the "lineNo"
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo".
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "effectDate".
     * 
     * @return the "effectDate"
     */
    public Timestamp getEffectDate() {
        return effectDate;
    }

    /**
     * Setter method of "effectDate".
     * 
     * @param effectDate Set in "effectDate".
     */
    public void setEffectDate(Timestamp effectDate) {
        this.effectDate = effectDate;
    }

    /**
     * Getter method of "uploadDatetime".
     * 
     * @return the "uploadDatetime"
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime".
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "relationLastUpdate".
     * 
     * @return the "relationLastUpdate"
     */
    public Timestamp getRelationLastUpdate() {
        return relationLastUpdate;
    }

    /**
     * Setter method of "relationLastUpdate".
     * 
     * @param relationLastUpdate Set in "relationLastUpdate".
     */
    public void setRelationLastUpdate(Timestamp relationLastUpdate) {
        this.relationLastUpdate = relationLastUpdate;
    }

    /**
     * Getter method of "partLastUpdate".
     * 
     * @return the "partLastUpdate"
     */
    public Timestamp getPartLastUpdate() {
        return partLastUpdate;
    }

    /**
     * Setter method of "partLastUpdate".
     * 
     * @param partLastUpdate Set in "partLastUpdate".
     */
    public void setPartLastUpdate(Timestamp partLastUpdate) {
        this.partLastUpdate = partLastUpdate;
    }

    /**
     * Getter method of "isActualRegister".
     * 
     * @return the "isActualRegister"
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister".
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime".
     * 
     * @return the "toActualDatetime"
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime".
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

    /**
     * Getter method of "flag".
     * 
     * @return the "flag"
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Setter method of "flag".
     * 
     * @param flag Set in "flag".
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "userDscIdLikeFront".
     * 
     * @return the "userDscIdLikeFront"
     */
    public String getUserDscIdLikeFront() {
        return userDscIdLikeFront;
    }

    /**
     * Setter method of "userDscIdLikeFront".
     * 
     * @param userDscIdLikeFront Set in "userDscIdLikeFront".
     */
    public void setUserDscIdLikeFront(String userDscIdLikeFront) {
        this.userDscIdLikeFront = userDscIdLikeFront;
    }

    /**
     * Getter method of "sessionCdLikeFront".
     * 
     * @return the "sessionCdLikeFront"
     */
    public String getSessionCdLikeFront() {
        return sessionCdLikeFront;
    }

    /**
     * Setter method of "sessionCdLikeFront".
     * 
     * @param sessionCdLikeFront Set in "sessionCdLikeFront".
     */
    public void setSessionCdLikeFront(String sessionCdLikeFront) {
        this.sessionCdLikeFront = sessionCdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "effectDateGreaterThanEqual".
     * 
     * @return the "effectDateGreaterThanEqual"
     */
    public Timestamp getEffectDateGreaterThanEqual() {
        return effectDateGreaterThanEqual;
    }

    /**
     * Setter method of "effectDateGreaterThanEqual".
     * 
     * @param effectDateGreaterThanEqual Set in "effectDateGreaterThanEqual".
     */
    public void setEffectDateGreaterThanEqual(Timestamp effectDateGreaterThanEqual) {
        this.effectDateGreaterThanEqual = effectDateGreaterThanEqual;
    }

    /**
     * Getter method of "effectDateLessThanEqual".
     * 
     * @return the "effectDateLessThanEqual"
     */
    public Timestamp getEffectDateLessThanEqual() {
        return effectDateLessThanEqual;
    }

    /**
     * Setter method of "effectDateLessThanEqual".
     * 
     * @param effectDateLessThanEqual Set in "effectDateLessThanEqual".
     */
    public void setEffectDateLessThanEqual(Timestamp effectDateLessThanEqual) {
        this.effectDateLessThanEqual = effectDateLessThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @return the "uploadDatetimeGreaterThanEqual"
     */
    public Timestamp getUploadDatetimeGreaterThanEqual() {
        return uploadDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @param uploadDatetimeGreaterThanEqual Set in "uploadDatetimeGreaterThanEqual".
     */
    public void setUploadDatetimeGreaterThanEqual(Timestamp uploadDatetimeGreaterThanEqual) {
        this.uploadDatetimeGreaterThanEqual = uploadDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeLessThanEqual".
     * 
     * @return the "uploadDatetimeLessThanEqual"
     */
    public Timestamp getUploadDatetimeLessThanEqual() {
        return uploadDatetimeLessThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeLessThanEqual".
     * 
     * @param uploadDatetimeLessThanEqual Set in "uploadDatetimeLessThanEqual".
     */
    public void setUploadDatetimeLessThanEqual(Timestamp uploadDatetimeLessThanEqual) {
        this.uploadDatetimeLessThanEqual = uploadDatetimeLessThanEqual;
    }

    /**
     * Getter method of "relationLastUpdateGreaterThanEqual".
     * 
     * @return the "relationLastUpdateGreaterThanEqual"
     */
    public Timestamp getRelationLastUpdateGreaterThanEqual() {
        return relationLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "relationLastUpdateGreaterThanEqual".
     * 
     * @param relationLastUpdateGreaterThanEqual Set in "relationLastUpdateGreaterThanEqual".
     */
    public void setRelationLastUpdateGreaterThanEqual(Timestamp relationLastUpdateGreaterThanEqual) {
        this.relationLastUpdateGreaterThanEqual = relationLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "relationLastUpdateLessThanEqual".
     * 
     * @return the "relationLastUpdateLessThanEqual"
     */
    public Timestamp getRelationLastUpdateLessThanEqual() {
        return relationLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "relationLastUpdateLessThanEqual".
     * 
     * @param relationLastUpdateLessThanEqual Set in "relationLastUpdateLessThanEqual".
     */
    public void setRelationLastUpdateLessThanEqual(Timestamp relationLastUpdateLessThanEqual) {
        this.relationLastUpdateLessThanEqual = relationLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "partLastUpdateGreaterThanEqual".
     * 
     * @return the "partLastUpdateGreaterThanEqual"
     */
    public Timestamp getPartLastUpdateGreaterThanEqual() {
        return partLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "partLastUpdateGreaterThanEqual".
     * 
     * @param partLastUpdateGreaterThanEqual Set in "partLastUpdateGreaterThanEqual".
     */
    public void setPartLastUpdateGreaterThanEqual(Timestamp partLastUpdateGreaterThanEqual) {
        this.partLastUpdateGreaterThanEqual = partLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "partLastUpdateLessThanEqual".
     * 
     * @return the "partLastUpdateLessThanEqual"
     */
    public Timestamp getPartLastUpdateLessThanEqual() {
        return partLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "partLastUpdateLessThanEqual".
     * 
     * @param partLastUpdateLessThanEqual Set in "partLastUpdateLessThanEqual".
     */
    public void setPartLastUpdateLessThanEqual(Timestamp partLastUpdateLessThanEqual) {
        this.partLastUpdateLessThanEqual = partLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "isActualRegisterLikeFront".
     * 
     * @return the "isActualRegisterLikeFront"
     */
    public String getIsActualRegisterLikeFront() {
        return isActualRegisterLikeFront;
    }

    /**
     * Setter method of "isActualRegisterLikeFront".
     * 
     * @param isActualRegisterLikeFront Set in "isActualRegisterLikeFront".
     */
    public void setIsActualRegisterLikeFront(String isActualRegisterLikeFront) {
        this.isActualRegisterLikeFront = isActualRegisterLikeFront;
    }

    /**
     * Getter method of "toActualDatetimeGreaterThanEqual".
     * 
     * @return the "toActualDatetimeGreaterThanEqual"
     */
    public Timestamp getToActualDatetimeGreaterThanEqual() {
        return toActualDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "toActualDatetimeGreaterThanEqual".
     * 
     * @param toActualDatetimeGreaterThanEqual Set in "toActualDatetimeGreaterThanEqual".
     */
    public void setToActualDatetimeGreaterThanEqual(Timestamp toActualDatetimeGreaterThanEqual) {
        this.toActualDatetimeGreaterThanEqual = toActualDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "toActualDatetimeLessThanEqual".
     * 
     * @return the "toActualDatetimeLessThanEqual"
     */
    public Timestamp getToActualDatetimeLessThanEqual() {
        return toActualDatetimeLessThanEqual;
    }

    /**
     * Setter method of "toActualDatetimeLessThanEqual".
     * 
     * @param toActualDatetimeLessThanEqual Set in "toActualDatetimeLessThanEqual".
     */
    public void setToActualDatetimeLessThanEqual(Timestamp toActualDatetimeLessThanEqual) {
        this.toActualDatetimeLessThanEqual = toActualDatetimeLessThanEqual;
    }

    /**
     * Getter method of "flagLikeFront".
     * 
     * @return the "flagLikeFront"
     */
    public String getFlagLikeFront() {
        return flagLikeFront;
    }

    /**
     * Setter method of "flagLikeFront".
     * 
     * @param flagLikeFront Set in "flagLikeFront".
     */
    public void setFlagLikeFront(String flagLikeFront) {
        this.flagLikeFront = flagLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

}
