/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMUser"<br />
 * Table overview: SPS_M_USER<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * DSC_ID
     */
    private String dscId;

    /**
     * DEPARTMENT_NAME
     */
    private String departmentName;

    /**
     * FIRST_NAME
     */
    private String firstName;

    /**
     * MIDDLE_NAME
     */
    private String middleName;

    /**
     * LAST_NAME
     */
    private String lastName;

    /**
     * EMAIL
     */
    private String email;

    /**
     * TELEPHONE
     */
    private String telephone;

    /**
     * IS_ACTIVE
     */
    private String isActive;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * USER_TYPE
     */
    private String userType;

    /**
     * Default constructor
     */
    public SpsMUserDomain() {
    }

    /**
     * Getter method of "dscId"
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId"
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "departmentName"
     * 
     * @return the departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Setter method of "departmentName"
     * 
     * @param departmentName Set in "departmentName".
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Getter method of "firstName"
     * 
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName"
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName"
     * 
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName"
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName"
     * 
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName"
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email"
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email"
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "userType"
     * 
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Setter method of "userType"
     * 
     * @param userType Set in "userType".
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

}
