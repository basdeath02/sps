/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * A search criteria "Domain" class of "SpsCigmaPoError".<br />
 * Table overview: SPS_CIGMA_PO_ERROR<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaPoErrorCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No for P/O Error
     */
    private BigDecimal poErrorId;

    /**
     * CIGMA P/O No
     */
    private String cigmaPoNo;

    /**
     * Data Type from CIGMA
     */
    private String dataType;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * P/O Issue Date
     */
    private Date issueDate;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Calendar Period
     */
    private Date etd;

    /**
     * Order Qty
     */
    private BigDecimal orderQty;

    /**
     * Send Mail Flag ;
0 : Do not send, 1: Send
     */
    private String mailFlg;

    /**
     * Update by User ID
     */
    private String updateByUserId;

    /**
     * Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Due Date
     */
    private Date dueDate;

    /**
     * SPS Error Type Flag
1 : VENDOR_CD not found in SPS system
2 : PARTS_NO not found in SPS system
     */
    private String errorTypeFlg;

    /**
     * CIGMA P/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaPoNoLikeFront;

    /**
     * Data Type from CIGMA(condition whether the column value at the beginning is equal to the value)
     */
    private String dataTypeLikeFront;

    /**
     * AS/400 Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * P/O Issue Date(condition whether the column value is greater than or equal to the value)
     */
    private Date issueDateGreaterThanEqual;

    /**
     * P/O Issue Date(condition whether the column value is less than or equal to the value)
     */
    private Date issueDateLessThanEqual;

    /**
     * DENSO Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Calendar Period(condition whether the column value is greater than or equal to the value)
     */
    private Date etdGreaterThanEqual;

    /**
     * Calendar Period(condition whether the column value is less than or equal to the value)
     */
    private Date etdLessThanEqual;

    /**
     * Send Mail Flag ;
0 : Do not send, 1: Send(condition whether the column value at the beginning is equal to the value)
     */
    private String mailFlgLikeFront;

    /**
     * Update by User ID(condition whether the column value at the beginning is equal to the value)
     */
    private String updateByUserIdLikeFront;

    /**
     * Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * DENSO Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * Due Date(condition whether the column value is greater than or equal to the value)
     */
    private Date dueDateGreaterThanEqual;

    /**
     * Due Date(condition whether the column value is less than or equal to the value)
     */
    private Date dueDateLessThanEqual;

    /**
     * SPS Error Type Flag
1 : VENDOR_CD not found in SPS system
2 : PARTS_NO not found in SPS system(condition whether the column value at the beginning is equal to the value)
     */
    private String errorTypeFlgLikeFront;

    /**
     * Default constructor
     */
    public SpsCigmaPoErrorCriteriaDomain() {
    }

    /**
     * Getter method of "poErrorId".
     * 
     * @return the "poErrorId"
     */
    public BigDecimal getPoErrorId() {
        return poErrorId;
    }

    /**
     * Setter method of "poErrorId".
     * 
     * @param poErrorId Set in "poErrorId".
     */
    public void setPoErrorId(BigDecimal poErrorId) {
        this.poErrorId = poErrorId;
    }

    /**
     * Getter method of "cigmaPoNo".
     * 
     * @return the "cigmaPoNo"
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }

    /**
     * Setter method of "cigmaPoNo".
     * 
     * @param cigmaPoNo Set in "cigmaPoNo".
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * Getter method of "dataType".
     * 
     * @return the "dataType"
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType".
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "issueDate".
     * 
     * @return the "issueDate"
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Setter method of "issueDate".
     * 
     * @param issueDate Set in "issueDate".
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "etd".
     * 
     * @return the "etd"
     */
    public Date getEtd() {
        return etd;
    }

    /**
     * Setter method of "etd".
     * 
     * @param etd Set in "etd".
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }

    /**
     * Getter method of "orderQty".
     * 
     * @return the "orderQty"
     */
    public BigDecimal getOrderQty() {
        return orderQty;
    }

    /**
     * Setter method of "orderQty".
     * 
     * @param orderQty Set in "orderQty".
     */
    public void setOrderQty(BigDecimal orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * Getter method of "mailFlg".
     * 
     * @return the "mailFlg"
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg".
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "updateByUserId".
     * 
     * @return the "updateByUserId"
     */
    public String getUpdateByUserId() {
        return updateByUserId;
    }

    /**
     * Setter method of "updateByUserId".
     * 
     * @param updateByUserId Set in "updateByUserId".
     */
    public void setUpdateByUserId(String updateByUserId) {
        this.updateByUserId = updateByUserId;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "dueDate".
     * 
     * @return the "dueDate"
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Setter method of "dueDate".
     * 
     * @param dueDate Set in "dueDate".
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Getter method of "errorTypeFlg".
     * 
     * @return the "errorTypeFlg"
     */
    public String getErrorTypeFlg() {
        return errorTypeFlg;
    }

    /**
     * Setter method of "errorTypeFlg".
     * 
     * @param errorTypeFlg Set in "errorTypeFlg".
     */
    public void setErrorTypeFlg(String errorTypeFlg) {
        this.errorTypeFlg = errorTypeFlg;
    }

    /**
     * Getter method of "cigmaPoNoLikeFront".
     * 
     * @return the "cigmaPoNoLikeFront"
     */
    public String getCigmaPoNoLikeFront() {
        return cigmaPoNoLikeFront;
    }

    /**
     * Setter method of "cigmaPoNoLikeFront".
     * 
     * @param cigmaPoNoLikeFront Set in "cigmaPoNoLikeFront".
     */
    public void setCigmaPoNoLikeFront(String cigmaPoNoLikeFront) {
        this.cigmaPoNoLikeFront = cigmaPoNoLikeFront;
    }

    /**
     * Getter method of "dataTypeLikeFront".
     * 
     * @return the "dataTypeLikeFront"
     */
    public String getDataTypeLikeFront() {
        return dataTypeLikeFront;
    }

    /**
     * Setter method of "dataTypeLikeFront".
     * 
     * @param dataTypeLikeFront Set in "dataTypeLikeFront".
     */
    public void setDataTypeLikeFront(String dataTypeLikeFront) {
        this.dataTypeLikeFront = dataTypeLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "issueDateGreaterThanEqual".
     * 
     * @return the "issueDateGreaterThanEqual"
     */
    public Date getIssueDateGreaterThanEqual() {
        return issueDateGreaterThanEqual;
    }

    /**
     * Setter method of "issueDateGreaterThanEqual".
     * 
     * @param issueDateGreaterThanEqual Set in "issueDateGreaterThanEqual".
     */
    public void setIssueDateGreaterThanEqual(Date issueDateGreaterThanEqual) {
        this.issueDateGreaterThanEqual = issueDateGreaterThanEqual;
    }

    /**
     * Getter method of "issueDateLessThanEqual".
     * 
     * @return the "issueDateLessThanEqual"
     */
    public Date getIssueDateLessThanEqual() {
        return issueDateLessThanEqual;
    }

    /**
     * Setter method of "issueDateLessThanEqual".
     * 
     * @param issueDateLessThanEqual Set in "issueDateLessThanEqual".
     */
    public void setIssueDateLessThanEqual(Date issueDateLessThanEqual) {
        this.issueDateLessThanEqual = issueDateLessThanEqual;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "etdGreaterThanEqual".
     * 
     * @return the "etdGreaterThanEqual"
     */
    public Date getEtdGreaterThanEqual() {
        return etdGreaterThanEqual;
    }

    /**
     * Setter method of "etdGreaterThanEqual".
     * 
     * @param etdGreaterThanEqual Set in "etdGreaterThanEqual".
     */
    public void setEtdGreaterThanEqual(Date etdGreaterThanEqual) {
        this.etdGreaterThanEqual = etdGreaterThanEqual;
    }

    /**
     * Getter method of "etdLessThanEqual".
     * 
     * @return the "etdLessThanEqual"
     */
    public Date getEtdLessThanEqual() {
        return etdLessThanEqual;
    }

    /**
     * Setter method of "etdLessThanEqual".
     * 
     * @param etdLessThanEqual Set in "etdLessThanEqual".
     */
    public void setEtdLessThanEqual(Date etdLessThanEqual) {
        this.etdLessThanEqual = etdLessThanEqual;
    }

    /**
     * Getter method of "mailFlgLikeFront".
     * 
     * @return the "mailFlgLikeFront"
     */
    public String getMailFlgLikeFront() {
        return mailFlgLikeFront;
    }

    /**
     * Setter method of "mailFlgLikeFront".
     * 
     * @param mailFlgLikeFront Set in "mailFlgLikeFront".
     */
    public void setMailFlgLikeFront(String mailFlgLikeFront) {
        this.mailFlgLikeFront = mailFlgLikeFront;
    }

    /**
     * Getter method of "updateByUserIdLikeFront".
     * 
     * @return the "updateByUserIdLikeFront"
     */
    public String getUpdateByUserIdLikeFront() {
        return updateByUserIdLikeFront;
    }

    /**
     * Setter method of "updateByUserIdLikeFront".
     * 
     * @param updateByUserIdLikeFront Set in "updateByUserIdLikeFront".
     */
    public void setUpdateByUserIdLikeFront(String updateByUserIdLikeFront) {
        this.updateByUserIdLikeFront = updateByUserIdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "dueDateGreaterThanEqual".
     * 
     * @return the "dueDateGreaterThanEqual"
     */
    public Date getDueDateGreaterThanEqual() {
        return dueDateGreaterThanEqual;
    }

    /**
     * Setter method of "dueDateGreaterThanEqual".
     * 
     * @param dueDateGreaterThanEqual Set in "dueDateGreaterThanEqual".
     */
    public void setDueDateGreaterThanEqual(Date dueDateGreaterThanEqual) {
        this.dueDateGreaterThanEqual = dueDateGreaterThanEqual;
    }

    /**
     * Getter method of "dueDateLessThanEqual".
     * 
     * @return the "dueDateLessThanEqual"
     */
    public Date getDueDateLessThanEqual() {
        return dueDateLessThanEqual;
    }

    /**
     * Setter method of "dueDateLessThanEqual".
     * 
     * @param dueDateLessThanEqual Set in "dueDateLessThanEqual".
     */
    public void setDueDateLessThanEqual(Date dueDateLessThanEqual) {
        this.dueDateLessThanEqual = dueDateLessThanEqual;
    }

    /**
     * Getter method of "errorTypeFlgLikeFront".
     * 
     * @return the "errorTypeFlgLikeFront"
     */
    public String getErrorTypeFlgLikeFront() {
        return errorTypeFlgLikeFront;
    }

    /**
     * Setter method of "errorTypeFlgLikeFront".
     * 
     * @param errorTypeFlgLikeFront Set in "errorTypeFlgLikeFront".
     */
    public void setErrorTypeFlgLikeFront(String errorTypeFlgLikeFront) {
        this.errorTypeFlgLikeFront = errorTypeFlgLikeFront;
    }

}
