/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/29       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMCompanySupplier".<br />
 * Table overview: SPS_M_COMPANY_SUPPLIER<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/29 18:51:48<br />
 * 
 * This module generated automatically in 2014/10/29 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanySupplierCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * DSC_ID_COM_CD
     */
    private String dscIdComCd;

    /**
     * COMPANY_NAME
     */
    private String companyName;

    /**
     * ADDRESS1
     */
    private String address1;

    /**
     * ADDRESS2
     */
    private String address2;

    /**
     * ADDRESS3
     */
    private String address3;

    /**
     * TELEPHONE
     */
    private String telephone;

    /**
     * FAX
     */
    private String fax;

    /**
     * CONTACT_PERSON
     */
    private String contactPerson;

    /**
     * S_TAX_ID
     */
    private String sTaxId;

    /**
     * TAX_CD
     */
    private String taxCd;

    /**
     * CURRENCY_CD
     */
    private String currencyCd;

    /**
     * IS_ACTIVE
     */
    private String isActive;

    /**
     * NOTICE_DO_URGENT_PERIOD
     */
    private BigDecimal noticeDoUrgentPeriod;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * S_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * DSC_ID_COM_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdComCdLikeFront;

    /**
     * COMPANY_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String companyNameLikeFront;

    /**
     * ADDRESS1(condition whether the column value at the beginning is equal to the value)
     */
    private String address1LikeFront;

    /**
     * ADDRESS2(condition whether the column value at the beginning is equal to the value)
     */
    private String address2LikeFront;

    /**
     * ADDRESS3(condition whether the column value at the beginning is equal to the value)
     */
    private String address3LikeFront;

    /**
     * TELEPHONE(condition whether the column value at the beginning is equal to the value)
     */
    private String telephoneLikeFront;

    /**
     * FAX(condition whether the column value at the beginning is equal to the value)
     */
    private String faxLikeFront;

    /**
     * CONTACT_PERSON(condition whether the column value at the beginning is equal to the value)
     */
    private String contactPersonLikeFront;

    /**
     * S_TAX_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String sTaxIdLikeFront;

    /**
     * TAX_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String taxCdLikeFront;

    /**
     * CURRENCY_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String currencyCdLikeFront;

    /**
     * IS_ACTIVE(condition whether the column value at the beginning is equal to the value)
     */
    private String isActiveLikeFront;

    /**
     * CREATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * LAST_UPDATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMCompanySupplierCriteriaDomain() {
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "dscIdComCd".
     * 
     * @return the "dscIdComCd"
     */
    public String getDscIdComCd() {
        return dscIdComCd;
    }

    /**
     * Setter method of "dscIdComCd".
     * 
     * @param dscIdComCd Set in "dscIdComCd".
     */
    public void setDscIdComCd(String dscIdComCd) {
        this.dscIdComCd = dscIdComCd;
    }

    /**
     * Getter method of "companyName".
     * 
     * @return the "companyName"
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Setter method of "companyName".
     * 
     * @param companyName Set in "companyName".
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Getter method of "address1".
     * 
     * @return the "address1"
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Setter method of "address1".
     * 
     * @param address1 Set in "address1".
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * Getter method of "address2".
     * 
     * @return the "address2"
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Setter method of "address2".
     * 
     * @param address2 Set in "address2".
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * Getter method of "address3".
     * 
     * @return the "address3"
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Setter method of "address3".
     * 
     * @param address3 Set in "address3".
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * Getter method of "telephone".
     * 
     * @return the "telephone"
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone".
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "fax".
     * 
     * @return the "fax"
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax".
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "contactPerson".
     * 
     * @return the "contactPerson"
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Setter method of "contactPerson".
     * 
     * @param contactPerson Set in "contactPerson".
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Getter method of "sTaxId".
     * 
     * @return the "sTaxId"
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * Setter method of "sTaxId".
     * 
     * @param sTaxId Set in "sTaxId".
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Getter method of "taxCd".
     * 
     * @return the "taxCd"
     */
    public String getTaxCd() {
        return taxCd;
    }

    /**
     * Setter method of "taxCd".
     * 
     * @param taxCd Set in "taxCd".
     */
    public void setTaxCd(String taxCd) {
        this.taxCd = taxCd;
    }

    /**
     * Getter method of "currencyCd".
     * 
     * @return the "currencyCd"
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * Setter method of "currencyCd".
     * 
     * @param currencyCd Set in "currencyCd".
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * Getter method of "isActive".
     * 
     * @return the "isActive"
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive".
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "noticeDoUrgentPeriod".
     * 
     * @return the "noticeDoUrgentPeriod"
     */
    public BigDecimal getNoticeDoUrgentPeriod() {
        return noticeDoUrgentPeriod;
    }

    /**
     * Setter method of "noticeDoUrgentPeriod".
     * 
     * @param noticeDoUrgentPeriod Set in "noticeDoUrgentPeriod".
     */
    public void setNoticeDoUrgentPeriod(BigDecimal noticeDoUrgentPeriod) {
        this.noticeDoUrgentPeriod = noticeDoUrgentPeriod;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "dscIdComCdLikeFront".
     * 
     * @return the "dscIdComCdLikeFront"
     */
    public String getDscIdComCdLikeFront() {
        return dscIdComCdLikeFront;
    }

    /**
     * Setter method of "dscIdComCdLikeFront".
     * 
     * @param dscIdComCdLikeFront Set in "dscIdComCdLikeFront".
     */
    public void setDscIdComCdLikeFront(String dscIdComCdLikeFront) {
        this.dscIdComCdLikeFront = dscIdComCdLikeFront;
    }

    /**
     * Getter method of "companyNameLikeFront".
     * 
     * @return the "companyNameLikeFront"
     */
    public String getCompanyNameLikeFront() {
        return companyNameLikeFront;
    }

    /**
     * Setter method of "companyNameLikeFront".
     * 
     * @param companyNameLikeFront Set in "companyNameLikeFront".
     */
    public void setCompanyNameLikeFront(String companyNameLikeFront) {
        this.companyNameLikeFront = companyNameLikeFront;
    }

    /**
     * Getter method of "address1LikeFront".
     * 
     * @return the "address1LikeFront"
     */
    public String getAddress1LikeFront() {
        return address1LikeFront;
    }

    /**
     * Setter method of "address1LikeFront".
     * 
     * @param address1LikeFront Set in "address1LikeFront".
     */
    public void setAddress1LikeFront(String address1LikeFront) {
        this.address1LikeFront = address1LikeFront;
    }

    /**
     * Getter method of "address2LikeFront".
     * 
     * @return the "address2LikeFront"
     */
    public String getAddress2LikeFront() {
        return address2LikeFront;
    }

    /**
     * Setter method of "address2LikeFront".
     * 
     * @param address2LikeFront Set in "address2LikeFront".
     */
    public void setAddress2LikeFront(String address2LikeFront) {
        this.address2LikeFront = address2LikeFront;
    }

    /**
     * Getter method of "address3LikeFront".
     * 
     * @return the "address3LikeFront"
     */
    public String getAddress3LikeFront() {
        return address3LikeFront;
    }

    /**
     * Setter method of "address3LikeFront".
     * 
     * @param address3LikeFront Set in "address3LikeFront".
     */
    public void setAddress3LikeFront(String address3LikeFront) {
        this.address3LikeFront = address3LikeFront;
    }

    /**
     * Getter method of "telephoneLikeFront".
     * 
     * @return the "telephoneLikeFront"
     */
    public String getTelephoneLikeFront() {
        return telephoneLikeFront;
    }

    /**
     * Setter method of "telephoneLikeFront".
     * 
     * @param telephoneLikeFront Set in "telephoneLikeFront".
     */
    public void setTelephoneLikeFront(String telephoneLikeFront) {
        this.telephoneLikeFront = telephoneLikeFront;
    }

    /**
     * Getter method of "faxLikeFront".
     * 
     * @return the "faxLikeFront"
     */
    public String getFaxLikeFront() {
        return faxLikeFront;
    }

    /**
     * Setter method of "faxLikeFront".
     * 
     * @param faxLikeFront Set in "faxLikeFront".
     */
    public void setFaxLikeFront(String faxLikeFront) {
        this.faxLikeFront = faxLikeFront;
    }

    /**
     * Getter method of "contactPersonLikeFront".
     * 
     * @return the "contactPersonLikeFront"
     */
    public String getContactPersonLikeFront() {
        return contactPersonLikeFront;
    }

    /**
     * Setter method of "contactPersonLikeFront".
     * 
     * @param contactPersonLikeFront Set in "contactPersonLikeFront".
     */
    public void setContactPersonLikeFront(String contactPersonLikeFront) {
        this.contactPersonLikeFront = contactPersonLikeFront;
    }

    /**
     * Getter method of "sTaxIdLikeFront".
     * 
     * @return the "sTaxIdLikeFront"
     */
    public String getSTaxIdLikeFront() {
        return sTaxIdLikeFront;
    }

    /**
     * Setter method of "sTaxIdLikeFront".
     * 
     * @param sTaxIdLikeFront Set in "sTaxIdLikeFront".
     */
    public void setSTaxIdLikeFront(String sTaxIdLikeFront) {
        this.sTaxIdLikeFront = sTaxIdLikeFront;
    }

    /**
     * Getter method of "taxCdLikeFront".
     * 
     * @return the "taxCdLikeFront"
     */
    public String getTaxCdLikeFront() {
        return taxCdLikeFront;
    }

    /**
     * Setter method of "taxCdLikeFront".
     * 
     * @param taxCdLikeFront Set in "taxCdLikeFront".
     */
    public void setTaxCdLikeFront(String taxCdLikeFront) {
        this.taxCdLikeFront = taxCdLikeFront;
    }

    /**
     * Getter method of "currencyCdLikeFront".
     * 
     * @return the "currencyCdLikeFront"
     */
    public String getCurrencyCdLikeFront() {
        return currencyCdLikeFront;
    }

    /**
     * Setter method of "currencyCdLikeFront".
     * 
     * @param currencyCdLikeFront Set in "currencyCdLikeFront".
     */
    public void setCurrencyCdLikeFront(String currencyCdLikeFront) {
        this.currencyCdLikeFront = currencyCdLikeFront;
    }

    /**
     * Getter method of "isActiveLikeFront".
     * 
     * @return the "isActiveLikeFront"
     */
    public String getIsActiveLikeFront() {
        return isActiveLikeFront;
    }

    /**
     * Setter method of "isActiveLikeFront".
     * 
     * @param isActiveLikeFront Set in "isActiveLikeFront".
     */
    public void setIsActiveLikeFront(String isActiveLikeFront) {
        this.isActiveLikeFront = isActiveLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
