/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/12       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTDo".<br />
 * Table overview: SPS_T_DO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/12 14:21:17<br />
 * 
 * This module generated automatically in 2015/03/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No. of Delivery Order
     */
    private BigDecimal doId;

    /**
     * SPS D/O No
     */
    private String spsDoNo;

    /**
     * CIGMA D/O No.
     */
    private String cigmaDoNo;

    /**
     * Current SPS D/O No.
     */
    private String currentSpsDoNo;

    /**
     * Current CIGMA D/O No.
     */
    private String currentCigmaDoNo;

    /**
     * Previous SPS D/O No.
     */
    private String previousSpsDoNo;

    /**
     * Previous CIGMA D/O No.
     */
    private String previousCigmaDoNo;

    /**
     * D/O Revision
     */
    private String revision;

    /**
     * D/O Issue Date
     */
    private Date doIssueDate;

    /**
     * ISS : Issued
PTS : Partial-Ship
CPS : Complete-Ship
     */
    private String shipmentStatus;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Transportation Mode
0 : DENSO Truck
1 : Supplier Truck
     */
    private String tm;

    /**
     * Data Type from CIGMA
     */
    private String dataType;

    /**
     * Order Method
0 : Fixed Order
1 : KANBAN Order
     */
    private String orderMethod;

    /**
     * D/O Ship Datetime
     */
    private Timestamp shipDatetime;

    /**
     * D/O Delivery Datetime
     */
    private Timestamp deliveryDatetime;

    /**
     * File Id which identify D/O Report PDF file are saved in file manager table
     */
    private String pdfFileId;

    /**
     * Route
     */
    private String truckRoute;

    /**
     * Run No.
     */
    private BigDecimal truckSeq;

    /**
     * Referenc PO ID
     */
    private BigDecimal poId;

    /**
     * Warehouse Prime Receiving
     */
    private String whPrimeReceiving;

    /**
     * Flag to inform D/O is latest version or not
0 : Not Latest, 1 : Latest
     */
    private String latestRevisionFlg;

    /**
     * Flag to inform D/O is urgent or not
0 : Not Urgent, 1 : Urgent
     */
    private String urgentOrderFlg;

    /**
     * Clear Urgent Order Date
     */
    private Date clearUrgentDate;

    /**
     * Add Truck Route Flag
0 : None
1 : Add New Truck Route
     */
    private String newTruckRouteFlg;

    /**
     * Receiving Dock
     */
    private String dockCode;

    /**
     * Cycle
     */
    private String cycle;

    /**
     * Trip No
     */
    private BigDecimal tripNo;

    /**
     * Backlog
     */
    private BigDecimal backlog;

    /**
     * Receiving Gate
     */
    private String receivingGate;

    /**
     * Supplier Location
     */
    private String supplierLocation;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * SPS D/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String spsDoNoLikeFront;

    /**
     * CIGMA D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaDoNoLikeFront;

    /**
     * Current SPS D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String currentSpsDoNoLikeFront;

    /**
     * Current CIGMA D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String currentCigmaDoNoLikeFront;

    /**
     * Previous SPS D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String previousSpsDoNoLikeFront;

    /**
     * Previous CIGMA D/O No.(condition whether the column value at the beginning is equal to the value)
     */
    private String previousCigmaDoNoLikeFront;

    /**
     * D/O Revision(condition whether the column value at the beginning is equal to the value)
     */
    private String revisionLikeFront;

    /**
     * D/O Issue Date(condition whether the column value is greater than or equal to the value)
     */
    private Date doIssueDateGreaterThanEqual;

    /**
     * D/O Issue Date(condition whether the column value is less than or equal to the value)
     */
    private Date doIssueDateLessThanEqual;

    /**
     * ISS : Issued
PTS : Partial-Ship
CPS : Complete-Ship(condition whether the column value at the beginning is equal to the value)
     */
    private String shipmentStatusLikeFront;

    /**
     * AS/400 Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * DENSO Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * Transportation Mode
0 : DENSO Truck
1 : Supplier Truck(condition whether the column value at the beginning is equal to the value)
     */
    private String tmLikeFront;

    /**
     * Data Type from CIGMA(condition whether the column value at the beginning is equal to the value)
     */
    private String dataTypeLikeFront;

    /**
     * Order Method
0 : Fixed Order
1 : KANBAN Order(condition whether the column value at the beginning is equal to the value)
     */
    private String orderMethodLikeFront;

    /**
     * D/O Ship Datetime(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp shipDatetimeGreaterThanEqual;

    /**
     * D/O Ship Datetime(condition whether the column value is less than or equal to the value)
     */
    private Timestamp shipDatetimeLessThanEqual;

    /**
     * D/O Delivery Datetime(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp deliveryDatetimeGreaterThanEqual;

    /**
     * D/O Delivery Datetime(condition whether the column value is less than or equal to the value)
     */
    private Timestamp deliveryDatetimeLessThanEqual;

    /**
     * File Id which identify D/O Report PDF file are saved in file manager table(condition whether the column value at the beginning is equal to the value)
     */
    private String pdfFileIdLikeFront;

    /**
     * Route(condition whether the column value at the beginning is equal to the value)
     */
    private String truckRouteLikeFront;

    /**
     * Warehouse Prime Receiving(condition whether the column value at the beginning is equal to the value)
     */
    private String whPrimeReceivingLikeFront;

    /**
     * Flag to inform D/O is latest version or not
0 : Not Latest, 1 : Latest(condition whether the column value at the beginning is equal to the value)
     */
    private String latestRevisionFlgLikeFront;

    /**
     * Flag to inform D/O is urgent or not
0 : Not Urgent, 1 : Urgent(condition whether the column value at the beginning is equal to the value)
     */
    private String urgentOrderFlgLikeFront;

    /**
     * Clear Urgent Order Date(condition whether the column value is greater than or equal to the value)
     */
    private Date clearUrgentDateGreaterThanEqual;

    /**
     * Clear Urgent Order Date(condition whether the column value is less than or equal to the value)
     */
    private Date clearUrgentDateLessThanEqual;

    /**
     * Add Truck Route Flag
0 : None
1 : Add New Truck Route(condition whether the column value at the beginning is equal to the value)
     */
    private String newTruckRouteFlgLikeFront;

    /**
     * Receiving Dock(condition whether the column value at the beginning is equal to the value)
     */
    private String dockCodeLikeFront;

    /**
     * Cycle(condition whether the column value at the beginning is equal to the value)
     */
    private String cycleLikeFront;

    /**
     * Receiving Gate(condition whether the column value at the beginning is equal to the value)
     */
    private String receivingGateLikeFront;

    /**
     * Supplier Location(condition whether the column value at the beginning is equal to the value)
     */
    private String supplierLocationLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTDoCriteriaDomain() {
    }

    /**
     * Getter method of "doId".
     * 
     * @return the "doId"
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId".
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "spsDoNo".
     * 
     * @return the "spsDoNo"
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo".
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "cigmaDoNo".
     * 
     * @return the "cigmaDoNo"
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo".
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "currentSpsDoNo".
     * 
     * @return the "currentSpsDoNo"
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }

    /**
     * Setter method of "currentSpsDoNo".
     * 
     * @param currentSpsDoNo Set in "currentSpsDoNo".
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }

    /**
     * Getter method of "currentCigmaDoNo".
     * 
     * @return the "currentCigmaDoNo"
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * Setter method of "currentCigmaDoNo".
     * 
     * @param currentCigmaDoNo Set in "currentCigmaDoNo".
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * Getter method of "previousSpsDoNo".
     * 
     * @return the "previousSpsDoNo"
     */
    public String getPreviousSpsDoNo() {
        return previousSpsDoNo;
    }

    /**
     * Setter method of "previousSpsDoNo".
     * 
     * @param previousSpsDoNo Set in "previousSpsDoNo".
     */
    public void setPreviousSpsDoNo(String previousSpsDoNo) {
        this.previousSpsDoNo = previousSpsDoNo;
    }

    /**
     * Getter method of "previousCigmaDoNo".
     * 
     * @return the "previousCigmaDoNo"
     */
    public String getPreviousCigmaDoNo() {
        return previousCigmaDoNo;
    }

    /**
     * Setter method of "previousCigmaDoNo".
     * 
     * @param previousCigmaDoNo Set in "previousCigmaDoNo".
     */
    public void setPreviousCigmaDoNo(String previousCigmaDoNo) {
        this.previousCigmaDoNo = previousCigmaDoNo;
    }

    /**
     * Getter method of "revision".
     * 
     * @return the "revision"
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision".
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "doIssueDate".
     * 
     * @return the "doIssueDate"
     */
    public Date getDoIssueDate() {
        return doIssueDate;
    }

    /**
     * Setter method of "doIssueDate".
     * 
     * @param doIssueDate Set in "doIssueDate".
     */
    public void setDoIssueDate(Date doIssueDate) {
        this.doIssueDate = doIssueDate;
    }

    /**
     * Getter method of "shipmentStatus".
     * 
     * @return the "shipmentStatus"
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * Setter method of "shipmentStatus".
     * 
     * @param shipmentStatus Set in "shipmentStatus".
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "tm".
     * 
     * @return the "tm"
     */
    public String getTm() {
        return tm;
    }

    /**
     * Setter method of "tm".
     * 
     * @param tm Set in "tm".
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * Getter method of "dataType".
     * 
     * @return the "dataType"
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType".
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "orderMethod".
     * 
     * @return the "orderMethod"
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Setter method of "orderMethod".
     * 
     * @param orderMethod Set in "orderMethod".
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Getter method of "shipDatetime".
     * 
     * @return the "shipDatetime"
     */
    public Timestamp getShipDatetime() {
        return shipDatetime;
    }

    /**
     * Setter method of "shipDatetime".
     * 
     * @param shipDatetime Set in "shipDatetime".
     */
    public void setShipDatetime(Timestamp shipDatetime) {
        this.shipDatetime = shipDatetime;
    }

    /**
     * Getter method of "deliveryDatetime".
     * 
     * @return the "deliveryDatetime"
     */
    public Timestamp getDeliveryDatetime() {
        return deliveryDatetime;
    }

    /**
     * Setter method of "deliveryDatetime".
     * 
     * @param deliveryDatetime Set in "deliveryDatetime".
     */
    public void setDeliveryDatetime(Timestamp deliveryDatetime) {
        this.deliveryDatetime = deliveryDatetime;
    }

    /**
     * Getter method of "pdfFileId".
     * 
     * @return the "pdfFileId"
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Setter method of "pdfFileId".
     * 
     * @param pdfFileId Set in "pdfFileId".
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Getter method of "truckRoute".
     * 
     * @return the "truckRoute"
     */
    public String getTruckRoute() {
        return truckRoute;
    }

    /**
     * Setter method of "truckRoute".
     * 
     * @param truckRoute Set in "truckRoute".
     */
    public void setTruckRoute(String truckRoute) {
        this.truckRoute = truckRoute;
    }

    /**
     * Getter method of "truckSeq".
     * 
     * @return the "truckSeq"
     */
    public BigDecimal getTruckSeq() {
        return truckSeq;
    }

    /**
     * Setter method of "truckSeq".
     * 
     * @param truckSeq Set in "truckSeq".
     */
    public void setTruckSeq(BigDecimal truckSeq) {
        this.truckSeq = truckSeq;
    }

    /**
     * Getter method of "poId".
     * 
     * @return the "poId"
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId".
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "whPrimeReceiving".
     * 
     * @return the "whPrimeReceiving"
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * Setter method of "whPrimeReceiving".
     * 
     * @param whPrimeReceiving Set in "whPrimeReceiving".
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * Getter method of "latestRevisionFlg".
     * 
     * @return the "latestRevisionFlg"
     */
    public String getLatestRevisionFlg() {
        return latestRevisionFlg;
    }

    /**
     * Setter method of "latestRevisionFlg".
     * 
     * @param latestRevisionFlg Set in "latestRevisionFlg".
     */
    public void setLatestRevisionFlg(String latestRevisionFlg) {
        this.latestRevisionFlg = latestRevisionFlg;
    }

    /**
     * Getter method of "urgentOrderFlg".
     * 
     * @return the "urgentOrderFlg"
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * Setter method of "urgentOrderFlg".
     * 
     * @param urgentOrderFlg Set in "urgentOrderFlg".
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * Getter method of "clearUrgentDate".
     * 
     * @return the "clearUrgentDate"
     */
    public Date getClearUrgentDate() {
        return clearUrgentDate;
    }

    /**
     * Setter method of "clearUrgentDate".
     * 
     * @param clearUrgentDate Set in "clearUrgentDate".
     */
    public void setClearUrgentDate(Date clearUrgentDate) {
        this.clearUrgentDate = clearUrgentDate;
    }

    /**
     * Getter method of "newTruckRouteFlg".
     * 
     * @return the "newTruckRouteFlg"
     */
    public String getNewTruckRouteFlg() {
        return newTruckRouteFlg;
    }

    /**
     * Setter method of "newTruckRouteFlg".
     * 
     * @param newTruckRouteFlg Set in "newTruckRouteFlg".
     */
    public void setNewTruckRouteFlg(String newTruckRouteFlg) {
        this.newTruckRouteFlg = newTruckRouteFlg;
    }

    /**
     * Getter method of "dockCode".
     * 
     * @return the "dockCode"
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * Setter method of "dockCode".
     * 
     * @param dockCode Set in "dockCode".
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * Getter method of "cycle".
     * 
     * @return the "cycle"
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * Setter method of "cycle".
     * 
     * @param cycle Set in "cycle".
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * Getter method of "tripNo".
     * 
     * @return the "tripNo"
     */
    public BigDecimal getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo".
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(BigDecimal tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "backlog".
     * 
     * @return the "backlog"
     */
    public BigDecimal getBacklog() {
        return backlog;
    }

    /**
     * Setter method of "backlog".
     * 
     * @param backlog Set in "backlog".
     */
    public void setBacklog(BigDecimal backlog) {
        this.backlog = backlog;
    }

    /**
     * Getter method of "receivingGate".
     * 
     * @return the "receivingGate"
     */
    public String getReceivingGate() {
        return receivingGate;
    }

    /**
     * Setter method of "receivingGate".
     * 
     * @param receivingGate Set in "receivingGate".
     */
    public void setReceivingGate(String receivingGate) {
        this.receivingGate = receivingGate;
    }

    /**
     * Getter method of "supplierLocation".
     * 
     * @return the "supplierLocation"
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * Setter method of "supplierLocation".
     * 
     * @param supplierLocation Set in "supplierLocation".
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "spsDoNoLikeFront".
     * 
     * @return the "spsDoNoLikeFront"
     */
    public String getSpsDoNoLikeFront() {
        return spsDoNoLikeFront;
    }

    /**
     * Setter method of "spsDoNoLikeFront".
     * 
     * @param spsDoNoLikeFront Set in "spsDoNoLikeFront".
     */
    public void setSpsDoNoLikeFront(String spsDoNoLikeFront) {
        this.spsDoNoLikeFront = spsDoNoLikeFront;
    }

    /**
     * Getter method of "cigmaDoNoLikeFront".
     * 
     * @return the "cigmaDoNoLikeFront"
     */
    public String getCigmaDoNoLikeFront() {
        return cigmaDoNoLikeFront;
    }

    /**
     * Setter method of "cigmaDoNoLikeFront".
     * 
     * @param cigmaDoNoLikeFront Set in "cigmaDoNoLikeFront".
     */
    public void setCigmaDoNoLikeFront(String cigmaDoNoLikeFront) {
        this.cigmaDoNoLikeFront = cigmaDoNoLikeFront;
    }

    /**
     * Getter method of "currentSpsDoNoLikeFront".
     * 
     * @return the "currentSpsDoNoLikeFront"
     */
    public String getCurrentSpsDoNoLikeFront() {
        return currentSpsDoNoLikeFront;
    }

    /**
     * Setter method of "currentSpsDoNoLikeFront".
     * 
     * @param currentSpsDoNoLikeFront Set in "currentSpsDoNoLikeFront".
     */
    public void setCurrentSpsDoNoLikeFront(String currentSpsDoNoLikeFront) {
        this.currentSpsDoNoLikeFront = currentSpsDoNoLikeFront;
    }

    /**
     * Getter method of "currentCigmaDoNoLikeFront".
     * 
     * @return the "currentCigmaDoNoLikeFront"
     */
    public String getCurrentCigmaDoNoLikeFront() {
        return currentCigmaDoNoLikeFront;
    }

    /**
     * Setter method of "currentCigmaDoNoLikeFront".
     * 
     * @param currentCigmaDoNoLikeFront Set in "currentCigmaDoNoLikeFront".
     */
    public void setCurrentCigmaDoNoLikeFront(String currentCigmaDoNoLikeFront) {
        this.currentCigmaDoNoLikeFront = currentCigmaDoNoLikeFront;
    }

    /**
     * Getter method of "previousSpsDoNoLikeFront".
     * 
     * @return the "previousSpsDoNoLikeFront"
     */
    public String getPreviousSpsDoNoLikeFront() {
        return previousSpsDoNoLikeFront;
    }

    /**
     * Setter method of "previousSpsDoNoLikeFront".
     * 
     * @param previousSpsDoNoLikeFront Set in "previousSpsDoNoLikeFront".
     */
    public void setPreviousSpsDoNoLikeFront(String previousSpsDoNoLikeFront) {
        this.previousSpsDoNoLikeFront = previousSpsDoNoLikeFront;
    }

    /**
     * Getter method of "previousCigmaDoNoLikeFront".
     * 
     * @return the "previousCigmaDoNoLikeFront"
     */
    public String getPreviousCigmaDoNoLikeFront() {
        return previousCigmaDoNoLikeFront;
    }

    /**
     * Setter method of "previousCigmaDoNoLikeFront".
     * 
     * @param previousCigmaDoNoLikeFront Set in "previousCigmaDoNoLikeFront".
     */
    public void setPreviousCigmaDoNoLikeFront(String previousCigmaDoNoLikeFront) {
        this.previousCigmaDoNoLikeFront = previousCigmaDoNoLikeFront;
    }

    /**
     * Getter method of "revisionLikeFront".
     * 
     * @return the "revisionLikeFront"
     */
    public String getRevisionLikeFront() {
        return revisionLikeFront;
    }

    /**
     * Setter method of "revisionLikeFront".
     * 
     * @param revisionLikeFront Set in "revisionLikeFront".
     */
    public void setRevisionLikeFront(String revisionLikeFront) {
        this.revisionLikeFront = revisionLikeFront;
    }

    /**
     * Getter method of "doIssueDateGreaterThanEqual".
     * 
     * @return the "doIssueDateGreaterThanEqual"
     */
    public Date getDoIssueDateGreaterThanEqual() {
        return doIssueDateGreaterThanEqual;
    }

    /**
     * Setter method of "doIssueDateGreaterThanEqual".
     * 
     * @param doIssueDateGreaterThanEqual Set in "doIssueDateGreaterThanEqual".
     */
    public void setDoIssueDateGreaterThanEqual(Date doIssueDateGreaterThanEqual) {
        this.doIssueDateGreaterThanEqual = doIssueDateGreaterThanEqual;
    }

    /**
     * Getter method of "doIssueDateLessThanEqual".
     * 
     * @return the "doIssueDateLessThanEqual"
     */
    public Date getDoIssueDateLessThanEqual() {
        return doIssueDateLessThanEqual;
    }

    /**
     * Setter method of "doIssueDateLessThanEqual".
     * 
     * @param doIssueDateLessThanEqual Set in "doIssueDateLessThanEqual".
     */
    public void setDoIssueDateLessThanEqual(Date doIssueDateLessThanEqual) {
        this.doIssueDateLessThanEqual = doIssueDateLessThanEqual;
    }

    /**
     * Getter method of "shipmentStatusLikeFront".
     * 
     * @return the "shipmentStatusLikeFront"
     */
    public String getShipmentStatusLikeFront() {
        return shipmentStatusLikeFront;
    }

    /**
     * Setter method of "shipmentStatusLikeFront".
     * 
     * @param shipmentStatusLikeFront Set in "shipmentStatusLikeFront".
     */
    public void setShipmentStatusLikeFront(String shipmentStatusLikeFront) {
        this.shipmentStatusLikeFront = shipmentStatusLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "tmLikeFront".
     * 
     * @return the "tmLikeFront"
     */
    public String getTmLikeFront() {
        return tmLikeFront;
    }

    /**
     * Setter method of "tmLikeFront".
     * 
     * @param tmLikeFront Set in "tmLikeFront".
     */
    public void setTmLikeFront(String tmLikeFront) {
        this.tmLikeFront = tmLikeFront;
    }

    /**
     * Getter method of "dataTypeLikeFront".
     * 
     * @return the "dataTypeLikeFront"
     */
    public String getDataTypeLikeFront() {
        return dataTypeLikeFront;
    }

    /**
     * Setter method of "dataTypeLikeFront".
     * 
     * @param dataTypeLikeFront Set in "dataTypeLikeFront".
     */
    public void setDataTypeLikeFront(String dataTypeLikeFront) {
        this.dataTypeLikeFront = dataTypeLikeFront;
    }

    /**
     * Getter method of "orderMethodLikeFront".
     * 
     * @return the "orderMethodLikeFront"
     */
    public String getOrderMethodLikeFront() {
        return orderMethodLikeFront;
    }

    /**
     * Setter method of "orderMethodLikeFront".
     * 
     * @param orderMethodLikeFront Set in "orderMethodLikeFront".
     */
    public void setOrderMethodLikeFront(String orderMethodLikeFront) {
        this.orderMethodLikeFront = orderMethodLikeFront;
    }

    /**
     * Getter method of "shipDatetimeGreaterThanEqual".
     * 
     * @return the "shipDatetimeGreaterThanEqual"
     */
    public Timestamp getShipDatetimeGreaterThanEqual() {
        return shipDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "shipDatetimeGreaterThanEqual".
     * 
     * @param shipDatetimeGreaterThanEqual Set in "shipDatetimeGreaterThanEqual".
     */
    public void setShipDatetimeGreaterThanEqual(Timestamp shipDatetimeGreaterThanEqual) {
        this.shipDatetimeGreaterThanEqual = shipDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "shipDatetimeLessThanEqual".
     * 
     * @return the "shipDatetimeLessThanEqual"
     */
    public Timestamp getShipDatetimeLessThanEqual() {
        return shipDatetimeLessThanEqual;
    }

    /**
     * Setter method of "shipDatetimeLessThanEqual".
     * 
     * @param shipDatetimeLessThanEqual Set in "shipDatetimeLessThanEqual".
     */
    public void setShipDatetimeLessThanEqual(Timestamp shipDatetimeLessThanEqual) {
        this.shipDatetimeLessThanEqual = shipDatetimeLessThanEqual;
    }

    /**
     * Getter method of "deliveryDatetimeGreaterThanEqual".
     * 
     * @return the "deliveryDatetimeGreaterThanEqual"
     */
    public Timestamp getDeliveryDatetimeGreaterThanEqual() {
        return deliveryDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "deliveryDatetimeGreaterThanEqual".
     * 
     * @param deliveryDatetimeGreaterThanEqual Set in "deliveryDatetimeGreaterThanEqual".
     */
    public void setDeliveryDatetimeGreaterThanEqual(Timestamp deliveryDatetimeGreaterThanEqual) {
        this.deliveryDatetimeGreaterThanEqual = deliveryDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "deliveryDatetimeLessThanEqual".
     * 
     * @return the "deliveryDatetimeLessThanEqual"
     */
    public Timestamp getDeliveryDatetimeLessThanEqual() {
        return deliveryDatetimeLessThanEqual;
    }

    /**
     * Setter method of "deliveryDatetimeLessThanEqual".
     * 
     * @param deliveryDatetimeLessThanEqual Set in "deliveryDatetimeLessThanEqual".
     */
    public void setDeliveryDatetimeLessThanEqual(Timestamp deliveryDatetimeLessThanEqual) {
        this.deliveryDatetimeLessThanEqual = deliveryDatetimeLessThanEqual;
    }

    /**
     * Getter method of "pdfFileIdLikeFront".
     * 
     * @return the "pdfFileIdLikeFront"
     */
    public String getPdfFileIdLikeFront() {
        return pdfFileIdLikeFront;
    }

    /**
     * Setter method of "pdfFileIdLikeFront".
     * 
     * @param pdfFileIdLikeFront Set in "pdfFileIdLikeFront".
     */
    public void setPdfFileIdLikeFront(String pdfFileIdLikeFront) {
        this.pdfFileIdLikeFront = pdfFileIdLikeFront;
    }

    /**
     * Getter method of "truckRouteLikeFront".
     * 
     * @return the "truckRouteLikeFront"
     */
    public String getTruckRouteLikeFront() {
        return truckRouteLikeFront;
    }

    /**
     * Setter method of "truckRouteLikeFront".
     * 
     * @param truckRouteLikeFront Set in "truckRouteLikeFront".
     */
    public void setTruckRouteLikeFront(String truckRouteLikeFront) {
        this.truckRouteLikeFront = truckRouteLikeFront;
    }

    /**
     * Getter method of "whPrimeReceivingLikeFront".
     * 
     * @return the "whPrimeReceivingLikeFront"
     */
    public String getWhPrimeReceivingLikeFront() {
        return whPrimeReceivingLikeFront;
    }

    /**
     * Setter method of "whPrimeReceivingLikeFront".
     * 
     * @param whPrimeReceivingLikeFront Set in "whPrimeReceivingLikeFront".
     */
    public void setWhPrimeReceivingLikeFront(String whPrimeReceivingLikeFront) {
        this.whPrimeReceivingLikeFront = whPrimeReceivingLikeFront;
    }

    /**
     * Getter method of "latestRevisionFlgLikeFront".
     * 
     * @return the "latestRevisionFlgLikeFront"
     */
    public String getLatestRevisionFlgLikeFront() {
        return latestRevisionFlgLikeFront;
    }

    /**
     * Setter method of "latestRevisionFlgLikeFront".
     * 
     * @param latestRevisionFlgLikeFront Set in "latestRevisionFlgLikeFront".
     */
    public void setLatestRevisionFlgLikeFront(String latestRevisionFlgLikeFront) {
        this.latestRevisionFlgLikeFront = latestRevisionFlgLikeFront;
    }

    /**
     * Getter method of "urgentOrderFlgLikeFront".
     * 
     * @return the "urgentOrderFlgLikeFront"
     */
    public String getUrgentOrderFlgLikeFront() {
        return urgentOrderFlgLikeFront;
    }

    /**
     * Setter method of "urgentOrderFlgLikeFront".
     * 
     * @param urgentOrderFlgLikeFront Set in "urgentOrderFlgLikeFront".
     */
    public void setUrgentOrderFlgLikeFront(String urgentOrderFlgLikeFront) {
        this.urgentOrderFlgLikeFront = urgentOrderFlgLikeFront;
    }

    /**
     * Getter method of "clearUrgentDateGreaterThanEqual".
     * 
     * @return the "clearUrgentDateGreaterThanEqual"
     */
    public Date getClearUrgentDateGreaterThanEqual() {
        return clearUrgentDateGreaterThanEqual;
    }

    /**
     * Setter method of "clearUrgentDateGreaterThanEqual".
     * 
     * @param clearUrgentDateGreaterThanEqual Set in "clearUrgentDateGreaterThanEqual".
     */
    public void setClearUrgentDateGreaterThanEqual(Date clearUrgentDateGreaterThanEqual) {
        this.clearUrgentDateGreaterThanEqual = clearUrgentDateGreaterThanEqual;
    }

    /**
     * Getter method of "clearUrgentDateLessThanEqual".
     * 
     * @return the "clearUrgentDateLessThanEqual"
     */
    public Date getClearUrgentDateLessThanEqual() {
        return clearUrgentDateLessThanEqual;
    }

    /**
     * Setter method of "clearUrgentDateLessThanEqual".
     * 
     * @param clearUrgentDateLessThanEqual Set in "clearUrgentDateLessThanEqual".
     */
    public void setClearUrgentDateLessThanEqual(Date clearUrgentDateLessThanEqual) {
        this.clearUrgentDateLessThanEqual = clearUrgentDateLessThanEqual;
    }

    /**
     * Getter method of "newTruckRouteFlgLikeFront".
     * 
     * @return the "newTruckRouteFlgLikeFront"
     */
    public String getNewTruckRouteFlgLikeFront() {
        return newTruckRouteFlgLikeFront;
    }

    /**
     * Setter method of "newTruckRouteFlgLikeFront".
     * 
     * @param newTruckRouteFlgLikeFront Set in "newTruckRouteFlgLikeFront".
     */
    public void setNewTruckRouteFlgLikeFront(String newTruckRouteFlgLikeFront) {
        this.newTruckRouteFlgLikeFront = newTruckRouteFlgLikeFront;
    }

    /**
     * Getter method of "dockCodeLikeFront".
     * 
     * @return the "dockCodeLikeFront"
     */
    public String getDockCodeLikeFront() {
        return dockCodeLikeFront;
    }

    /**
     * Setter method of "dockCodeLikeFront".
     * 
     * @param dockCodeLikeFront Set in "dockCodeLikeFront".
     */
    public void setDockCodeLikeFront(String dockCodeLikeFront) {
        this.dockCodeLikeFront = dockCodeLikeFront;
    }

    /**
     * Getter method of "cycleLikeFront".
     * 
     * @return the "cycleLikeFront"
     */
    public String getCycleLikeFront() {
        return cycleLikeFront;
    }

    /**
     * Setter method of "cycleLikeFront".
     * 
     * @param cycleLikeFront Set in "cycleLikeFront".
     */
    public void setCycleLikeFront(String cycleLikeFront) {
        this.cycleLikeFront = cycleLikeFront;
    }

    /**
     * Getter method of "receivingGateLikeFront".
     * 
     * @return the "receivingGateLikeFront"
     */
    public String getReceivingGateLikeFront() {
        return receivingGateLikeFront;
    }

    /**
     * Setter method of "receivingGateLikeFront".
     * 
     * @param receivingGateLikeFront Set in "receivingGateLikeFront".
     */
    public void setReceivingGateLikeFront(String receivingGateLikeFront) {
        this.receivingGateLikeFront = receivingGateLikeFront;
    }

    /**
     * Getter method of "supplierLocationLikeFront".
     * 
     * @return the "supplierLocationLikeFront"
     */
    public String getSupplierLocationLikeFront() {
        return supplierLocationLikeFront;
    }

    /**
     * Setter method of "supplierLocationLikeFront".
     * 
     * @param supplierLocationLikeFront Set in "supplierLocationLikeFront".
     */
    public void setSupplierLocationLikeFront(String supplierLocationLikeFront) {
        this.supplierLocationLikeFront = supplierLocationLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
