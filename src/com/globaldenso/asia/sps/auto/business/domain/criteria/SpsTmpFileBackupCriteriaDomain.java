/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpFileBackup".<br />
 * Table overview: SPS_TMP_FILE_BACKUP<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpFileBackupCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * FILE_PATH
     */
    private String filePath;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * FILE_PATH(condition whether the column value at the beginning is equal to the value)
     */
    private String filePathLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTmpFileBackupCriteriaDomain() {
    }

    /**
     * Getter method of "seqNo".
     * 
     * @return the "seqNo"
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo".
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "filePath".
     * 
     * @return the "filePath"
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Setter method of "filePath".
     * 
     * @param filePath Set in "filePath".
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "filePathLikeFront".
     * 
     * @return the "filePathLikeFront"
     */
    public String getFilePathLikeFront() {
        return filePathLikeFront;
    }

    /**
     * Setter method of "filePathLikeFront".
     * 
     * @param filePathLikeFront Set in "filePathLikeFront".
     */
    public void setFilePathLikeFront(String filePathLikeFront) {
        this.filePathLikeFront = filePathLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

}
