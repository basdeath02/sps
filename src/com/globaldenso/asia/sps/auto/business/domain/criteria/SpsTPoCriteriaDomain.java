/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTPo".<br />
 * Table overview: SPS_T_PO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No of Purchase Order
     */
    private BigDecimal poId;

    /**
     * SPS P/O Number
     */
    private String spsPoNo;

    /**
     * CIGMA P/O Number
     */
    private String cigmaPoNo;

    /**
     * REF_CIGMA_PO_NO
     */
    private String refCigmaPoNo;

    /**
     * Data Type from CIGMA
DPO : Domestic Fixed Order
IPO : Import Fixed Order
KPO : Domestic KANBAN Order
IKP : Import KANBAN Order
     */
    private String poType;

    /**
     * P/O Issue date
     */
    private Date poIssueDate;

    /**
     * SPS P/O Status ;
ISS : Issued
PED : Pending
ACK : Acknowledge
FAC : Force Acknowledge
     */
    private String poStatus;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * SPS Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Transportation Mode (TRANs)
0 : DENSO Truck
1 : Supplier Truck
     */
    private String tm;

    /**
     * Period Type (Order Type from CIGMA)
0 : Firm
1 : Forecast
     */
    private String periodType;

    /**
     * Order Method
0 : Fixed Order
1 : KANBAN Order
     */
    private String orderMethod;

    /**
     * Date which SPS Sytem force P/O Status to "FAC" automatically
     */
    private Date forceAckDate;

    /**
     * Flag to inform Change P/O or not
0 : Not Change, 1 : Change
     */
    private String changeFlg;

    /**
     * File Id which identify Original P/O Report PDF file are saved in file manager table
     */
    private String pdfOriginalFileId;

    /**
     * File Id which identify Change Material Release Report PDF file are saved in file manager table
     */
    private String pdfChangeFileId;

    /**
     * Release No
     */
    private BigDecimal releaseNo;

    /**
     * Due Date From
     */
    private Date dueDateFrom;

    /**
     * Due Date To
     */
    private Date dueDateTo;

    /**
     * ax Telephone No.
     */
    private String fax;

    /**
     * Add Truck Route Flag
0 : None
1 : Add New Truck Route
     */
    private String newTruckRouteFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * SPS P/O Number(condition whether the column value at the beginning is equal to the value)
     */
    private String spsPoNoLikeFront;

    /**
     * CIGMA P/O Number(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaPoNoLikeFront;

    /**
     * REF_CIGMA_PO_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String refCigmaPoNoLikeFront;

    /**
     * Data Type from CIGMA
DPO : Domestic Fixed Order
IPO : Import Fixed Order
KPO : Domestic KANBAN Order
IKP : Import KANBAN Order(condition whether the column value at the beginning is equal to the value)
     */
    private String poTypeLikeFront;

    /**
     * P/O Issue date(condition whether the column value is greater than or equal to the value)
     */
    private Date poIssueDateGreaterThanEqual;

    /**
     * P/O Issue date(condition whether the column value is less than or equal to the value)
     */
    private Date poIssueDateLessThanEqual;

    /**
     * SPS P/O Status ;
ISS : Issued
PED : Pending
ACK : Acknowledge
FAC : Force Acknowledge(condition whether the column value at the beginning is equal to the value)
     */
    private String poStatusLikeFront;

    /**
     * AS/400 Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * SPS Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * DENSO Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * Transportation Mode (TRANs)
0 : DENSO Truck
1 : Supplier Truck(condition whether the column value at the beginning is equal to the value)
     */
    private String tmLikeFront;

    /**
     * Period Type (Order Type from CIGMA)
0 : Firm
1 : Forecast(condition whether the column value at the beginning is equal to the value)
     */
    private String periodTypeLikeFront;

    /**
     * Order Method
0 : Fixed Order
1 : KANBAN Order(condition whether the column value at the beginning is equal to the value)
     */
    private String orderMethodLikeFront;

    /**
     * Date which SPS Sytem force P/O Status to "FAC" automatically(condition whether the column value is greater than or equal to the value)
     */
    private Date forceAckDateGreaterThanEqual;

    /**
     * Date which SPS Sytem force P/O Status to "FAC" automatically(condition whether the column value is less than or equal to the value)
     */
    private Date forceAckDateLessThanEqual;

    /**
     * Flag to inform Change P/O or not
0 : Not Change, 1 : Change(condition whether the column value at the beginning is equal to the value)
     */
    private String changeFlgLikeFront;

    /**
     * File Id which identify Original P/O Report PDF file are saved in file manager table(condition whether the column value at the beginning is equal to the value)
     */
    private String pdfOriginalFileIdLikeFront;

    /**
     * File Id which identify Change Material Release Report PDF file are saved in file manager table(condition whether the column value at the beginning is equal to the value)
     */
    private String pdfChangeFileIdLikeFront;

    /**
     * Due Date From(condition whether the column value is greater than or equal to the value)
     */
    private Date dueDateFromGreaterThanEqual;

    /**
     * Due Date From(condition whether the column value is less than or equal to the value)
     */
    private Date dueDateFromLessThanEqual;

    /**
     * Due Date To(condition whether the column value is greater than or equal to the value)
     */
    private Date dueDateToGreaterThanEqual;

    /**
     * Due Date To(condition whether the column value is less than or equal to the value)
     */
    private Date dueDateToLessThanEqual;

    /**
     * ax Telephone No.(condition whether the column value at the beginning is equal to the value)
     */
    private String faxLikeFront;

    /**
     * Add Truck Route Flag
0 : None
1 : Add New Truck Route(condition whether the column value at the beginning is equal to the value)
     */
    private String newTruckRouteFlgLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTPoCriteriaDomain() {
    }

    /**
     * Getter method of "poId".
     * 
     * @return the "poId"
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId".
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "spsPoNo".
     * 
     * @return the "spsPoNo"
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }

    /**
     * Setter method of "spsPoNo".
     * 
     * @param spsPoNo Set in "spsPoNo".
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }

    /**
     * Getter method of "cigmaPoNo".
     * 
     * @return the "cigmaPoNo"
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }

    /**
     * Setter method of "cigmaPoNo".
     * 
     * @param cigmaPoNo Set in "cigmaPoNo".
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * Getter method of "refCigmaPoNo".
     * 
     * @return the "refCigmaPoNo"
     */
    public String getRefCigmaPoNo() {
        return refCigmaPoNo;
    }

    /**
     * Setter method of "refCigmaPoNo".
     * 
     * @param refCigmaPoNo Set in "refCigmaPoNo".
     */
    public void setRefCigmaPoNo(String refCigmaPoNo) {
        this.refCigmaPoNo = refCigmaPoNo;
    }

    /**
     * Getter method of "poType".
     * 
     * @return the "poType"
     */
    public String getPoType() {
        return poType;
    }

    /**
     * Setter method of "poType".
     * 
     * @param poType Set in "poType".
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }

    /**
     * Getter method of "poIssueDate".
     * 
     * @return the "poIssueDate"
     */
    public Date getPoIssueDate() {
        return poIssueDate;
    }

    /**
     * Setter method of "poIssueDate".
     * 
     * @param poIssueDate Set in "poIssueDate".
     */
    public void setPoIssueDate(Date poIssueDate) {
        this.poIssueDate = poIssueDate;
    }

    /**
     * Getter method of "poStatus".
     * 
     * @return the "poStatus"
     */
    public String getPoStatus() {
        return poStatus;
    }

    /**
     * Setter method of "poStatus".
     * 
     * @param poStatus Set in "poStatus".
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "tm".
     * 
     * @return the "tm"
     */
    public String getTm() {
        return tm;
    }

    /**
     * Setter method of "tm".
     * 
     * @param tm Set in "tm".
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * Getter method of "periodType".
     * 
     * @return the "periodType"
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * Setter method of "periodType".
     * 
     * @param periodType Set in "periodType".
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    /**
     * Getter method of "orderMethod".
     * 
     * @return the "orderMethod"
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Setter method of "orderMethod".
     * 
     * @param orderMethod Set in "orderMethod".
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Getter method of "forceAckDate".
     * 
     * @return the "forceAckDate"
     */
    public Date getForceAckDate() {
        return forceAckDate;
    }

    /**
     * Setter method of "forceAckDate".
     * 
     * @param forceAckDate Set in "forceAckDate".
     */
    public void setForceAckDate(Date forceAckDate) {
        this.forceAckDate = forceAckDate;
    }

    /**
     * Getter method of "changeFlg".
     * 
     * @return the "changeFlg"
     */
    public String getChangeFlg() {
        return changeFlg;
    }

    /**
     * Setter method of "changeFlg".
     * 
     * @param changeFlg Set in "changeFlg".
     */
    public void setChangeFlg(String changeFlg) {
        this.changeFlg = changeFlg;
    }

    /**
     * Getter method of "pdfOriginalFileId".
     * 
     * @return the "pdfOriginalFileId"
     */
    public String getPdfOriginalFileId() {
        return pdfOriginalFileId;
    }

    /**
     * Setter method of "pdfOriginalFileId".
     * 
     * @param pdfOriginalFileId Set in "pdfOriginalFileId".
     */
    public void setPdfOriginalFileId(String pdfOriginalFileId) {
        this.pdfOriginalFileId = pdfOriginalFileId;
    }

    /**
     * Getter method of "pdfChangeFileId".
     * 
     * @return the "pdfChangeFileId"
     */
    public String getPdfChangeFileId() {
        return pdfChangeFileId;
    }

    /**
     * Setter method of "pdfChangeFileId".
     * 
     * @param pdfChangeFileId Set in "pdfChangeFileId".
     */
    public void setPdfChangeFileId(String pdfChangeFileId) {
        this.pdfChangeFileId = pdfChangeFileId;
    }

    /**
     * Getter method of "releaseNo".
     * 
     * @return the "releaseNo"
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }

    /**
     * Setter method of "releaseNo".
     * 
     * @param releaseNo Set in "releaseNo".
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }

    /**
     * Getter method of "dueDateFrom".
     * 
     * @return the "dueDateFrom"
     */
    public Date getDueDateFrom() {
        return dueDateFrom;
    }

    /**
     * Setter method of "dueDateFrom".
     * 
     * @param dueDateFrom Set in "dueDateFrom".
     */
    public void setDueDateFrom(Date dueDateFrom) {
        this.dueDateFrom = dueDateFrom;
    }

    /**
     * Getter method of "dueDateTo".
     * 
     * @return the "dueDateTo"
     */
    public Date getDueDateTo() {
        return dueDateTo;
    }

    /**
     * Setter method of "dueDateTo".
     * 
     * @param dueDateTo Set in "dueDateTo".
     */
    public void setDueDateTo(Date dueDateTo) {
        this.dueDateTo = dueDateTo;
    }

    /**
     * Getter method of "fax".
     * 
     * @return the "fax"
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax".
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "newTruckRouteFlg".
     * 
     * @return the "newTruckRouteFlg"
     */
    public String getNewTruckRouteFlg() {
        return newTruckRouteFlg;
    }

    /**
     * Setter method of "newTruckRouteFlg".
     * 
     * @param newTruckRouteFlg Set in "newTruckRouteFlg".
     */
    public void setNewTruckRouteFlg(String newTruckRouteFlg) {
        this.newTruckRouteFlg = newTruckRouteFlg;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "spsPoNoLikeFront".
     * 
     * @return the "spsPoNoLikeFront"
     */
    public String getSpsPoNoLikeFront() {
        return spsPoNoLikeFront;
    }

    /**
     * Setter method of "spsPoNoLikeFront".
     * 
     * @param spsPoNoLikeFront Set in "spsPoNoLikeFront".
     */
    public void setSpsPoNoLikeFront(String spsPoNoLikeFront) {
        this.spsPoNoLikeFront = spsPoNoLikeFront;
    }

    /**
     * Getter method of "cigmaPoNoLikeFront".
     * 
     * @return the "cigmaPoNoLikeFront"
     */
    public String getCigmaPoNoLikeFront() {
        return cigmaPoNoLikeFront;
    }

    /**
     * Setter method of "cigmaPoNoLikeFront".
     * 
     * @param cigmaPoNoLikeFront Set in "cigmaPoNoLikeFront".
     */
    public void setCigmaPoNoLikeFront(String cigmaPoNoLikeFront) {
        this.cigmaPoNoLikeFront = cigmaPoNoLikeFront;
    }

    /**
     * Getter method of "refCigmaPoNoLikeFront".
     * 
     * @return the "refCigmaPoNoLikeFront"
     */
    public String getRefCigmaPoNoLikeFront() {
        return refCigmaPoNoLikeFront;
    }

    /**
     * Setter method of "refCigmaPoNoLikeFront".
     * 
     * @param refCigmaPoNoLikeFront Set in "refCigmaPoNoLikeFront".
     */
    public void setRefCigmaPoNoLikeFront(String refCigmaPoNoLikeFront) {
        this.refCigmaPoNoLikeFront = refCigmaPoNoLikeFront;
    }

    /**
     * Getter method of "poTypeLikeFront".
     * 
     * @return the "poTypeLikeFront"
     */
    public String getPoTypeLikeFront() {
        return poTypeLikeFront;
    }

    /**
     * Setter method of "poTypeLikeFront".
     * 
     * @param poTypeLikeFront Set in "poTypeLikeFront".
     */
    public void setPoTypeLikeFront(String poTypeLikeFront) {
        this.poTypeLikeFront = poTypeLikeFront;
    }

    /**
     * Getter method of "poIssueDateGreaterThanEqual".
     * 
     * @return the "poIssueDateGreaterThanEqual"
     */
    public Date getPoIssueDateGreaterThanEqual() {
        return poIssueDateGreaterThanEqual;
    }

    /**
     * Setter method of "poIssueDateGreaterThanEqual".
     * 
     * @param poIssueDateGreaterThanEqual Set in "poIssueDateGreaterThanEqual".
     */
    public void setPoIssueDateGreaterThanEqual(Date poIssueDateGreaterThanEqual) {
        this.poIssueDateGreaterThanEqual = poIssueDateGreaterThanEqual;
    }

    /**
     * Getter method of "poIssueDateLessThanEqual".
     * 
     * @return the "poIssueDateLessThanEqual"
     */
    public Date getPoIssueDateLessThanEqual() {
        return poIssueDateLessThanEqual;
    }

    /**
     * Setter method of "poIssueDateLessThanEqual".
     * 
     * @param poIssueDateLessThanEqual Set in "poIssueDateLessThanEqual".
     */
    public void setPoIssueDateLessThanEqual(Date poIssueDateLessThanEqual) {
        this.poIssueDateLessThanEqual = poIssueDateLessThanEqual;
    }

    /**
     * Getter method of "poStatusLikeFront".
     * 
     * @return the "poStatusLikeFront"
     */
    public String getPoStatusLikeFront() {
        return poStatusLikeFront;
    }

    /**
     * Setter method of "poStatusLikeFront".
     * 
     * @param poStatusLikeFront Set in "poStatusLikeFront".
     */
    public void setPoStatusLikeFront(String poStatusLikeFront) {
        this.poStatusLikeFront = poStatusLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "tmLikeFront".
     * 
     * @return the "tmLikeFront"
     */
    public String getTmLikeFront() {
        return tmLikeFront;
    }

    /**
     * Setter method of "tmLikeFront".
     * 
     * @param tmLikeFront Set in "tmLikeFront".
     */
    public void setTmLikeFront(String tmLikeFront) {
        this.tmLikeFront = tmLikeFront;
    }

    /**
     * Getter method of "periodTypeLikeFront".
     * 
     * @return the "periodTypeLikeFront"
     */
    public String getPeriodTypeLikeFront() {
        return periodTypeLikeFront;
    }

    /**
     * Setter method of "periodTypeLikeFront".
     * 
     * @param periodTypeLikeFront Set in "periodTypeLikeFront".
     */
    public void setPeriodTypeLikeFront(String periodTypeLikeFront) {
        this.periodTypeLikeFront = periodTypeLikeFront;
    }

    /**
     * Getter method of "orderMethodLikeFront".
     * 
     * @return the "orderMethodLikeFront"
     */
    public String getOrderMethodLikeFront() {
        return orderMethodLikeFront;
    }

    /**
     * Setter method of "orderMethodLikeFront".
     * 
     * @param orderMethodLikeFront Set in "orderMethodLikeFront".
     */
    public void setOrderMethodLikeFront(String orderMethodLikeFront) {
        this.orderMethodLikeFront = orderMethodLikeFront;
    }

    /**
     * Getter method of "forceAckDateGreaterThanEqual".
     * 
     * @return the "forceAckDateGreaterThanEqual"
     */
    public Date getForceAckDateGreaterThanEqual() {
        return forceAckDateGreaterThanEqual;
    }

    /**
     * Setter method of "forceAckDateGreaterThanEqual".
     * 
     * @param forceAckDateGreaterThanEqual Set in "forceAckDateGreaterThanEqual".
     */
    public void setForceAckDateGreaterThanEqual(Date forceAckDateGreaterThanEqual) {
        this.forceAckDateGreaterThanEqual = forceAckDateGreaterThanEqual;
    }

    /**
     * Getter method of "forceAckDateLessThanEqual".
     * 
     * @return the "forceAckDateLessThanEqual"
     */
    public Date getForceAckDateLessThanEqual() {
        return forceAckDateLessThanEqual;
    }

    /**
     * Setter method of "forceAckDateLessThanEqual".
     * 
     * @param forceAckDateLessThanEqual Set in "forceAckDateLessThanEqual".
     */
    public void setForceAckDateLessThanEqual(Date forceAckDateLessThanEqual) {
        this.forceAckDateLessThanEqual = forceAckDateLessThanEqual;
    }

    /**
     * Getter method of "changeFlgLikeFront".
     * 
     * @return the "changeFlgLikeFront"
     */
    public String getChangeFlgLikeFront() {
        return changeFlgLikeFront;
    }

    /**
     * Setter method of "changeFlgLikeFront".
     * 
     * @param changeFlgLikeFront Set in "changeFlgLikeFront".
     */
    public void setChangeFlgLikeFront(String changeFlgLikeFront) {
        this.changeFlgLikeFront = changeFlgLikeFront;
    }

    /**
     * Getter method of "pdfOriginalFileIdLikeFront".
     * 
     * @return the "pdfOriginalFileIdLikeFront"
     */
    public String getPdfOriginalFileIdLikeFront() {
        return pdfOriginalFileIdLikeFront;
    }

    /**
     * Setter method of "pdfOriginalFileIdLikeFront".
     * 
     * @param pdfOriginalFileIdLikeFront Set in "pdfOriginalFileIdLikeFront".
     */
    public void setPdfOriginalFileIdLikeFront(String pdfOriginalFileIdLikeFront) {
        this.pdfOriginalFileIdLikeFront = pdfOriginalFileIdLikeFront;
    }

    /**
     * Getter method of "pdfChangeFileIdLikeFront".
     * 
     * @return the "pdfChangeFileIdLikeFront"
     */
    public String getPdfChangeFileIdLikeFront() {
        return pdfChangeFileIdLikeFront;
    }

    /**
     * Setter method of "pdfChangeFileIdLikeFront".
     * 
     * @param pdfChangeFileIdLikeFront Set in "pdfChangeFileIdLikeFront".
     */
    public void setPdfChangeFileIdLikeFront(String pdfChangeFileIdLikeFront) {
        this.pdfChangeFileIdLikeFront = pdfChangeFileIdLikeFront;
    }

    /**
     * Getter method of "dueDateFromGreaterThanEqual".
     * 
     * @return the "dueDateFromGreaterThanEqual"
     */
    public Date getDueDateFromGreaterThanEqual() {
        return dueDateFromGreaterThanEqual;
    }

    /**
     * Setter method of "dueDateFromGreaterThanEqual".
     * 
     * @param dueDateFromGreaterThanEqual Set in "dueDateFromGreaterThanEqual".
     */
    public void setDueDateFromGreaterThanEqual(Date dueDateFromGreaterThanEqual) {
        this.dueDateFromGreaterThanEqual = dueDateFromGreaterThanEqual;
    }

    /**
     * Getter method of "dueDateFromLessThanEqual".
     * 
     * @return the "dueDateFromLessThanEqual"
     */
    public Date getDueDateFromLessThanEqual() {
        return dueDateFromLessThanEqual;
    }

    /**
     * Setter method of "dueDateFromLessThanEqual".
     * 
     * @param dueDateFromLessThanEqual Set in "dueDateFromLessThanEqual".
     */
    public void setDueDateFromLessThanEqual(Date dueDateFromLessThanEqual) {
        this.dueDateFromLessThanEqual = dueDateFromLessThanEqual;
    }

    /**
     * Getter method of "dueDateToGreaterThanEqual".
     * 
     * @return the "dueDateToGreaterThanEqual"
     */
    public Date getDueDateToGreaterThanEqual() {
        return dueDateToGreaterThanEqual;
    }

    /**
     * Setter method of "dueDateToGreaterThanEqual".
     * 
     * @param dueDateToGreaterThanEqual Set in "dueDateToGreaterThanEqual".
     */
    public void setDueDateToGreaterThanEqual(Date dueDateToGreaterThanEqual) {
        this.dueDateToGreaterThanEqual = dueDateToGreaterThanEqual;
    }

    /**
     * Getter method of "dueDateToLessThanEqual".
     * 
     * @return the "dueDateToLessThanEqual"
     */
    public Date getDueDateToLessThanEqual() {
        return dueDateToLessThanEqual;
    }

    /**
     * Setter method of "dueDateToLessThanEqual".
     * 
     * @param dueDateToLessThanEqual Set in "dueDateToLessThanEqual".
     */
    public void setDueDateToLessThanEqual(Date dueDateToLessThanEqual) {
        this.dueDateToLessThanEqual = dueDateToLessThanEqual;
    }

    /**
     * Getter method of "faxLikeFront".
     * 
     * @return the "faxLikeFront"
     */
    public String getFaxLikeFront() {
        return faxLikeFront;
    }

    /**
     * Setter method of "faxLikeFront".
     * 
     * @param faxLikeFront Set in "faxLikeFront".
     */
    public void setFaxLikeFront(String faxLikeFront) {
        this.faxLikeFront = faxLikeFront;
    }

    /**
     * Getter method of "newTruckRouteFlgLikeFront".
     * 
     * @return the "newTruckRouteFlgLikeFront"
     */
    public String getNewTruckRouteFlgLikeFront() {
        return newTruckRouteFlgLikeFront;
    }

    /**
     * Setter method of "newTruckRouteFlgLikeFront".
     * 
     * @param newTruckRouteFlgLikeFront Set in "newTruckRouteFlgLikeFront".
     */
    public void setNewTruckRouteFlgLikeFront(String newTruckRouteFlgLikeFront) {
        this.newTruckRouteFlgLikeFront = newTruckRouteFlgLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
