/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * A search criteria "Domain" class of "SpsCigmaChgDoError".<br />
 * Table overview: SPS_CIGMA_CHG_DO_ERROR<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaChgDoErrorCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * CHG_DO_ERROR_ID
     */
    private BigDecimal chgDoErrorId;

    /**
     * CIGMA_DO_NO
     */
    private String cigmaDoNo;

    /**
     * DATA_TYPE
     */
    private String dataType;

    /**
     * VENDOR_CD
     */
    private String vendorCd;

    /**
     * ISSUE_DATE
     */
    private Date issueDate;

    /**
     * D_PN
     */
    private String dPn;

    /**
     * ORIGINAL_CIGMA_DO_NO
     */
    private String originalCigmaDoNo;

    /**
     * PREV_CIGMA_DO_NO
     */
    private String prevCigmaDoNo;

    /**
     * CURRENT_ORDER_QTY
     */
    private BigDecimal currentOrderQty;

    /**
     * ORIGINAL_ORDER_QTY
     */
    private BigDecimal originalOrderQty;

    /**
     * MAIL_FLG
     */
    private String mailFlg;

    /**
     * ERROR_TYPE_FLG
     */
    private String errorTypeFlg;

    /**
     * UPDATE_BY_USER_ID
     */
    private String updateByUserId;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * DELIVERY_DATE
     */
    private Date deliveryDate;

    /**
     * DELIVERY_TIME
     */
    private String deliveryTime;

    /**
     * WH
     */
    private String wh;

    /**
     * DOCK_CODE
     */
    private String dockCode;

    /**
     * CIGMA_DO_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaDoNoLikeFront;

    /**
     * DATA_TYPE(condition whether the column value at the beginning is equal to the value)
     */
    private String dataTypeLikeFront;

    /**
     * VENDOR_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * ISSUE_DATE(condition whether the column value is greater than or equal to the value)
     */
    private Date issueDateGreaterThanEqual;

    /**
     * ISSUE_DATE(condition whether the column value is less than or equal to the value)
     */
    private Date issueDateLessThanEqual;

    /**
     * D_PN(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * ORIGINAL_CIGMA_DO_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String originalCigmaDoNoLikeFront;

    /**
     * PREV_CIGMA_DO_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String prevCigmaDoNoLikeFront;

    /**
     * MAIL_FLG(condition whether the column value at the beginning is equal to the value)
     */
    private String mailFlgLikeFront;

    /**
     * ERROR_TYPE_FLG(condition whether the column value at the beginning is equal to the value)
     */
    private String errorTypeFlgLikeFront;

    /**
     * UPDATE_BY_USER_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String updateByUserIdLikeFront;

    /**
     * S_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * S_PCD(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * D_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * D_PCD(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * DELIVERY_DATE(condition whether the column value is greater than or equal to the value)
     */
    private Date deliveryDateGreaterThanEqual;

    /**
     * DELIVERY_DATE(condition whether the column value is less than or equal to the value)
     */
    private Date deliveryDateLessThanEqual;

    /**
     * DELIVERY_TIME(condition whether the column value at the beginning is equal to the value)
     */
    private String deliveryTimeLikeFront;

    /**
     * WH(condition whether the column value at the beginning is equal to the value)
     */
    private String whLikeFront;

    /**
     * DOCK_CODE(condition whether the column value at the beginning is equal to the value)
     */
    private String dockCodeLikeFront;

    /**
     * Default constructor
     */
    public SpsCigmaChgDoErrorCriteriaDomain() {
    }

    /**
     * Getter method of "chgDoErrorId".
     * 
     * @return the "chgDoErrorId"
     */
    public BigDecimal getChgDoErrorId() {
        return chgDoErrorId;
    }

    /**
     * Setter method of "chgDoErrorId".
     * 
     * @param chgDoErrorId Set in "chgDoErrorId".
     */
    public void setChgDoErrorId(BigDecimal chgDoErrorId) {
        this.chgDoErrorId = chgDoErrorId;
    }

    /**
     * Getter method of "cigmaDoNo".
     * 
     * @return the "cigmaDoNo"
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo".
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "dataType".
     * 
     * @return the "dataType"
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType".
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "issueDate".
     * 
     * @return the "issueDate"
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Setter method of "issueDate".
     * 
     * @param issueDate Set in "issueDate".
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "originalCigmaDoNo".
     * 
     * @return the "originalCigmaDoNo"
     */
    public String getOriginalCigmaDoNo() {
        return originalCigmaDoNo;
    }

    /**
     * Setter method of "originalCigmaDoNo".
     * 
     * @param originalCigmaDoNo Set in "originalCigmaDoNo".
     */
    public void setOriginalCigmaDoNo(String originalCigmaDoNo) {
        this.originalCigmaDoNo = originalCigmaDoNo;
    }

    /**
     * Getter method of "prevCigmaDoNo".
     * 
     * @return the "prevCigmaDoNo"
     */
    public String getPrevCigmaDoNo() {
        return prevCigmaDoNo;
    }

    /**
     * Setter method of "prevCigmaDoNo".
     * 
     * @param prevCigmaDoNo Set in "prevCigmaDoNo".
     */
    public void setPrevCigmaDoNo(String prevCigmaDoNo) {
        this.prevCigmaDoNo = prevCigmaDoNo;
    }

    /**
     * Getter method of "currentOrderQty".
     * 
     * @return the "currentOrderQty"
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Setter method of "currentOrderQty".
     * 
     * @param currentOrderQty Set in "currentOrderQty".
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Getter method of "originalOrderQty".
     * 
     * @return the "originalOrderQty"
     */
    public BigDecimal getOriginalOrderQty() {
        return originalOrderQty;
    }

    /**
     * Setter method of "originalOrderQty".
     * 
     * @param originalOrderQty Set in "originalOrderQty".
     */
    public void setOriginalOrderQty(BigDecimal originalOrderQty) {
        this.originalOrderQty = originalOrderQty;
    }

    /**
     * Getter method of "mailFlg".
     * 
     * @return the "mailFlg"
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg".
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "errorTypeFlg".
     * 
     * @return the "errorTypeFlg"
     */
    public String getErrorTypeFlg() {
        return errorTypeFlg;
    }

    /**
     * Setter method of "errorTypeFlg".
     * 
     * @param errorTypeFlg Set in "errorTypeFlg".
     */
    public void setErrorTypeFlg(String errorTypeFlg) {
        this.errorTypeFlg = errorTypeFlg;
    }

    /**
     * Getter method of "updateByUserId".
     * 
     * @return the "updateByUserId"
     */
    public String getUpdateByUserId() {
        return updateByUserId;
    }

    /**
     * Setter method of "updateByUserId".
     * 
     * @param updateByUserId Set in "updateByUserId".
     */
    public void setUpdateByUserId(String updateByUserId) {
        this.updateByUserId = updateByUserId;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "deliveryDate".
     * 
     * @return the "deliveryDate"
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Setter method of "deliveryDate".
     * 
     * @param deliveryDate Set in "deliveryDate".
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * Getter method of "deliveryTime".
     * 
     * @return the "deliveryTime"
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * Setter method of "deliveryTime".
     * 
     * @param deliveryTime Set in "deliveryTime".
     */
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * Getter method of "wh".
     * 
     * @return the "wh"
     */
    public String getWh() {
        return wh;
    }

    /**
     * Setter method of "wh".
     * 
     * @param wh Set in "wh".
     */
    public void setWh(String wh) {
        this.wh = wh;
    }

    /**
     * Getter method of "dockCode".
     * 
     * @return the "dockCode"
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * Setter method of "dockCode".
     * 
     * @param dockCode Set in "dockCode".
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * Getter method of "cigmaDoNoLikeFront".
     * 
     * @return the "cigmaDoNoLikeFront"
     */
    public String getCigmaDoNoLikeFront() {
        return cigmaDoNoLikeFront;
    }

    /**
     * Setter method of "cigmaDoNoLikeFront".
     * 
     * @param cigmaDoNoLikeFront Set in "cigmaDoNoLikeFront".
     */
    public void setCigmaDoNoLikeFront(String cigmaDoNoLikeFront) {
        this.cigmaDoNoLikeFront = cigmaDoNoLikeFront;
    }

    /**
     * Getter method of "dataTypeLikeFront".
     * 
     * @return the "dataTypeLikeFront"
     */
    public String getDataTypeLikeFront() {
        return dataTypeLikeFront;
    }

    /**
     * Setter method of "dataTypeLikeFront".
     * 
     * @param dataTypeLikeFront Set in "dataTypeLikeFront".
     */
    public void setDataTypeLikeFront(String dataTypeLikeFront) {
        this.dataTypeLikeFront = dataTypeLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "issueDateGreaterThanEqual".
     * 
     * @return the "issueDateGreaterThanEqual"
     */
    public Date getIssueDateGreaterThanEqual() {
        return issueDateGreaterThanEqual;
    }

    /**
     * Setter method of "issueDateGreaterThanEqual".
     * 
     * @param issueDateGreaterThanEqual Set in "issueDateGreaterThanEqual".
     */
    public void setIssueDateGreaterThanEqual(Date issueDateGreaterThanEqual) {
        this.issueDateGreaterThanEqual = issueDateGreaterThanEqual;
    }

    /**
     * Getter method of "issueDateLessThanEqual".
     * 
     * @return the "issueDateLessThanEqual"
     */
    public Date getIssueDateLessThanEqual() {
        return issueDateLessThanEqual;
    }

    /**
     * Setter method of "issueDateLessThanEqual".
     * 
     * @param issueDateLessThanEqual Set in "issueDateLessThanEqual".
     */
    public void setIssueDateLessThanEqual(Date issueDateLessThanEqual) {
        this.issueDateLessThanEqual = issueDateLessThanEqual;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "originalCigmaDoNoLikeFront".
     * 
     * @return the "originalCigmaDoNoLikeFront"
     */
    public String getOriginalCigmaDoNoLikeFront() {
        return originalCigmaDoNoLikeFront;
    }

    /**
     * Setter method of "originalCigmaDoNoLikeFront".
     * 
     * @param originalCigmaDoNoLikeFront Set in "originalCigmaDoNoLikeFront".
     */
    public void setOriginalCigmaDoNoLikeFront(String originalCigmaDoNoLikeFront) {
        this.originalCigmaDoNoLikeFront = originalCigmaDoNoLikeFront;
    }

    /**
     * Getter method of "prevCigmaDoNoLikeFront".
     * 
     * @return the "prevCigmaDoNoLikeFront"
     */
    public String getPrevCigmaDoNoLikeFront() {
        return prevCigmaDoNoLikeFront;
    }

    /**
     * Setter method of "prevCigmaDoNoLikeFront".
     * 
     * @param prevCigmaDoNoLikeFront Set in "prevCigmaDoNoLikeFront".
     */
    public void setPrevCigmaDoNoLikeFront(String prevCigmaDoNoLikeFront) {
        this.prevCigmaDoNoLikeFront = prevCigmaDoNoLikeFront;
    }

    /**
     * Getter method of "mailFlgLikeFront".
     * 
     * @return the "mailFlgLikeFront"
     */
    public String getMailFlgLikeFront() {
        return mailFlgLikeFront;
    }

    /**
     * Setter method of "mailFlgLikeFront".
     * 
     * @param mailFlgLikeFront Set in "mailFlgLikeFront".
     */
    public void setMailFlgLikeFront(String mailFlgLikeFront) {
        this.mailFlgLikeFront = mailFlgLikeFront;
    }

    /**
     * Getter method of "errorTypeFlgLikeFront".
     * 
     * @return the "errorTypeFlgLikeFront"
     */
    public String getErrorTypeFlgLikeFront() {
        return errorTypeFlgLikeFront;
    }

    /**
     * Setter method of "errorTypeFlgLikeFront".
     * 
     * @param errorTypeFlgLikeFront Set in "errorTypeFlgLikeFront".
     */
    public void setErrorTypeFlgLikeFront(String errorTypeFlgLikeFront) {
        this.errorTypeFlgLikeFront = errorTypeFlgLikeFront;
    }

    /**
     * Getter method of "updateByUserIdLikeFront".
     * 
     * @return the "updateByUserIdLikeFront"
     */
    public String getUpdateByUserIdLikeFront() {
        return updateByUserIdLikeFront;
    }

    /**
     * Setter method of "updateByUserIdLikeFront".
     * 
     * @param updateByUserIdLikeFront Set in "updateByUserIdLikeFront".
     */
    public void setUpdateByUserIdLikeFront(String updateByUserIdLikeFront) {
        this.updateByUserIdLikeFront = updateByUserIdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "deliveryDateGreaterThanEqual".
     * 
     * @return the "deliveryDateGreaterThanEqual"
     */
    public Date getDeliveryDateGreaterThanEqual() {
        return deliveryDateGreaterThanEqual;
    }

    /**
     * Setter method of "deliveryDateGreaterThanEqual".
     * 
     * @param deliveryDateGreaterThanEqual Set in "deliveryDateGreaterThanEqual".
     */
    public void setDeliveryDateGreaterThanEqual(Date deliveryDateGreaterThanEqual) {
        this.deliveryDateGreaterThanEqual = deliveryDateGreaterThanEqual;
    }

    /**
     * Getter method of "deliveryDateLessThanEqual".
     * 
     * @return the "deliveryDateLessThanEqual"
     */
    public Date getDeliveryDateLessThanEqual() {
        return deliveryDateLessThanEqual;
    }

    /**
     * Setter method of "deliveryDateLessThanEqual".
     * 
     * @param deliveryDateLessThanEqual Set in "deliveryDateLessThanEqual".
     */
    public void setDeliveryDateLessThanEqual(Date deliveryDateLessThanEqual) {
        this.deliveryDateLessThanEqual = deliveryDateLessThanEqual;
    }

    /**
     * Getter method of "deliveryTimeLikeFront".
     * 
     * @return the "deliveryTimeLikeFront"
     */
    public String getDeliveryTimeLikeFront() {
        return deliveryTimeLikeFront;
    }

    /**
     * Setter method of "deliveryTimeLikeFront".
     * 
     * @param deliveryTimeLikeFront Set in "deliveryTimeLikeFront".
     */
    public void setDeliveryTimeLikeFront(String deliveryTimeLikeFront) {
        this.deliveryTimeLikeFront = deliveryTimeLikeFront;
    }

    /**
     * Getter method of "whLikeFront".
     * 
     * @return the "whLikeFront"
     */
    public String getWhLikeFront() {
        return whLikeFront;
    }

    /**
     * Setter method of "whLikeFront".
     * 
     * @param whLikeFront Set in "whLikeFront".
     */
    public void setWhLikeFront(String whLikeFront) {
        this.whLikeFront = whLikeFront;
    }

    /**
     * Getter method of "dockCodeLikeFront".
     * 
     * @return the "dockCodeLikeFront"
     */
    public String getDockCodeLikeFront() {
        return dockCodeLikeFront;
    }

    /**
     * Setter method of "dockCodeLikeFront".
     * 
     * @param dockCodeLikeFront Set in "dockCodeLikeFront".
     */
    public void setDockCodeLikeFront(String dockCodeLikeFront) {
        this.dockCodeLikeFront = dockCodeLikeFront;
    }

}
