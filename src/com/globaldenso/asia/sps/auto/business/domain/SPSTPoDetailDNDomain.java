/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


public class SPSTPoDetailDNDomain   {
	private static final long serialVersionUID = 1L;
	 private String databaseName;
	private BigDecimal poId;
	private String sPn;
	private String dPn;
	private String pnStatus;
	private String itemDesc;
	private BigDecimal qtyBox;
	private BigDecimal unitPrice;
	private String tmpPriceFlg;
	private String currencyCode;
	private String pnLblPrintRemark;
	private String plannerCode;
	private String whPrimeReceiving;
	private String location;
	private String phaseCode;
	private String receivingDock;
	private String variableQtyCode;
	private BigDecimal orderLot;
	private String rcvLane;
	private String startPeriodDate;
	private String endPeriodDate;
	private String endFirmDate;
	private String unitOfMeasure;
	private String model;
	private String createDscId;
	private String createDate;
	private String createTime;
	private String lastUpdateDscId;
	private String lastUpdateDate;
	private String lastUpdateTime;
	public BigDecimal getPoId() {
		return poId;
	}
	public void setPoId(BigDecimal poId) {
		this.poId = poId;
	}
	public String getsPn() {
		return sPn;
	}
	public void setsPn(String sPn) {
		this.sPn = sPn;
	}
	public String getdPn() {
		return dPn;
	}
	public void setdPn(String dPn) {
		this.dPn = dPn;
	}
	public String getPnStatus() {
		return pnStatus;
	}
	public void setPnStatus(String pnStatus) {
		this.pnStatus = pnStatus;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public BigDecimal getQtyBox() {
		return qtyBox;
	}
	public void setQtyBox(BigDecimal qtyBox) {
		this.qtyBox = qtyBox;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getTmpPriceFlg() {
		return tmpPriceFlg;
	}
	public void setTmpPriceFlg(String tmpPriceFlg) {
		this.tmpPriceFlg = tmpPriceFlg;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getPnLblPrintRemark() {
		return pnLblPrintRemark;
	}
	public void setPnLblPrintRemark(String pnLblPrintRemark) {
		this.pnLblPrintRemark = pnLblPrintRemark;
	}
	public String getPlannerCode() {
		return plannerCode;
	}
	public void setPlannerCode(String plannerCode) {
		this.plannerCode = plannerCode;
	}
	public String getWhPrimeReceiving() {
		return whPrimeReceiving;
	}
	public void setWhPrimeReceiving(String whPrimeReceiving) {
		this.whPrimeReceiving = whPrimeReceiving;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPhaseCode() {
		return phaseCode;
	}
	public void setPhaseCode(String phaseCode) {
		this.phaseCode = phaseCode;
	}
	public String getReceivingDock() {
		return receivingDock;
	}
	public void setReceivingDock(String receivingDock) {
		this.receivingDock = receivingDock;
	}
	public String getVariableQtyCode() {
		return variableQtyCode;
	}
	public void setVariableQtyCode(String variableQtyCode) {
		this.variableQtyCode = variableQtyCode;
	}
	public BigDecimal getOrderLot() {
		return orderLot;
	}
	public void setOrderLot(BigDecimal orderLot) {
		this.orderLot = orderLot;
	}
	public String getRcvLane() {
		return rcvLane;
	}
	public void setRcvLane(String rcvLane) {
		this.rcvLane = rcvLane;
	}
	public String getStartPeriodDate() {
		return startPeriodDate;
	}
	public void setStartPeriodDate(String startPeriodDate) {
		this.startPeriodDate = startPeriodDate;
	}
	public String getEndPeriodDate() {
		return endPeriodDate;
	}
	public void setEndPeriodDate(String endPeriodDate) {
		this.endPeriodDate = endPeriodDate;
	}
	public String getEndFirmDate() {
		return endFirmDate;
	}
	public void setEndFirmDate(String endFirmDate) {
		this.endFirmDate = endFirmDate;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	
}
