/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/04/28       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTInvoiceDetail".<br />
 * Table overview: SPS_T_INVOICE_DETAIL<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/04/28 15:59:11<br />
 * 
 * This module generated automatically in 2015/04/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceDetailCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running Number of Invoice
     */
    private BigDecimal invoiceId;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * Running Number of Delivery Order
     */
    private BigDecimal doId;

    /**
     * SPS D/O No
     */
    private String spsDoNo;

    /**
     * D/O Revision
     */
    private String revision;

    /**
     * Trip Number
     */
    private String tripNo;

    /**
     * DENSO Part Number
     */
    private String dPn;

    /**
     * DENSO Unit of Measure
     */
    private String dUnitOfMeasure;

    /**
     * DENSO Unit Price
     */
    private BigDecimal dUnitPrice;

    /**
     * Temp Price Flag
     */
    private String tmpPriceFlg;

    /**
     * Supplier Part Number
     */
    private String sPn;

    /**
     * Supplier Unit of Measure
     */
    private String sUnitOfMeasure;

    /**
     * Supplier Unit Price
     */
    private BigDecimal sUnitPrice;

    /**
     * Shipping Quantity
     */
    private BigDecimal shippingQty;

    /**
     * Difference Base Amount of each part
     */
    private BigDecimal diffBaseAmt;

    /**
     * Base Amount of each part
     */
    private BigDecimal baseAmt;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * ASN Number(condition whether the column value at the beginning is equal to the value)
     */
    private String asnNoLikeFront;

    /**
     * SPS D/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String spsDoNoLikeFront;

    /**
     * D/O Revision(condition whether the column value at the beginning is equal to the value)
     */
    private String revisionLikeFront;

    /**
     * Trip Number(condition whether the column value at the beginning is equal to the value)
     */
    private String tripNoLikeFront;

    /**
     * DENSO Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * DENSO Unit of Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String dUnitOfMeasureLikeFront;

    /**
     * Temp Price Flag(condition whether the column value at the beginning is equal to the value)
     */
    private String tmpPriceFlgLikeFront;

    /**
     * Supplier Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * Supplier Unit of Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String sUnitOfMeasureLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTInvoiceDetailCriteriaDomain() {
    }

    /**
     * Getter method of "invoiceId".
     * 
     * @return the "invoiceId"
     */
    public BigDecimal getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method of "invoiceId".
     * 
     * @param invoiceId Set in "invoiceId".
     */
    public void setInvoiceId(BigDecimal invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * Getter method of "asnNo".
     * 
     * @return the "asnNo"
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo".
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "doId".
     * 
     * @return the "doId"
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId".
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "spsDoNo".
     * 
     * @return the "spsDoNo"
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo".
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision".
     * 
     * @return the "revision"
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision".
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "tripNo".
     * 
     * @return the "tripNo"
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo".
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "dUnitOfMeasure".
     * 
     * @return the "dUnitOfMeasure"
     */
    public String getDUnitOfMeasure() {
        return dUnitOfMeasure;
    }

    /**
     * Setter method of "dUnitOfMeasure".
     * 
     * @param dUnitOfMeasure Set in "dUnitOfMeasure".
     */
    public void setDUnitOfMeasure(String dUnitOfMeasure) {
        this.dUnitOfMeasure = dUnitOfMeasure;
    }

    /**
     * Getter method of "dUnitPrice".
     * 
     * @return the "dUnitPrice"
     */
    public BigDecimal getDUnitPrice() {
        return dUnitPrice;
    }

    /**
     * Setter method of "dUnitPrice".
     * 
     * @param dUnitPrice Set in "dUnitPrice".
     */
    public void setDUnitPrice(BigDecimal dUnitPrice) {
        this.dUnitPrice = dUnitPrice;
    }

    /**
     * Getter method of "tmpPriceFlg".
     * 
     * @return the "tmpPriceFlg"
     */
    public String getTmpPriceFlg() {
        return tmpPriceFlg;
    }

    /**
     * Setter method of "tmpPriceFlg".
     * 
     * @param tmpPriceFlg Set in "tmpPriceFlg".
     */
    public void setTmpPriceFlg(String tmpPriceFlg) {
        this.tmpPriceFlg = tmpPriceFlg;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "sUnitOfMeasure".
     * 
     * @return the "sUnitOfMeasure"
     */
    public String getSUnitOfMeasure() {
        return sUnitOfMeasure;
    }

    /**
     * Setter method of "sUnitOfMeasure".
     * 
     * @param sUnitOfMeasure Set in "sUnitOfMeasure".
     */
    public void setSUnitOfMeasure(String sUnitOfMeasure) {
        this.sUnitOfMeasure = sUnitOfMeasure;
    }

    /**
     * Getter method of "sUnitPrice".
     * 
     * @return the "sUnitPrice"
     */
    public BigDecimal getSUnitPrice() {
        return sUnitPrice;
    }

    /**
     * Setter method of "sUnitPrice".
     * 
     * @param sUnitPrice Set in "sUnitPrice".
     */
    public void setSUnitPrice(BigDecimal sUnitPrice) {
        this.sUnitPrice = sUnitPrice;
    }

    /**
     * Getter method of "shippingQty".
     * 
     * @return the "shippingQty"
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty".
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "diffBaseAmt".
     * 
     * @return the "diffBaseAmt"
     */
    public BigDecimal getDiffBaseAmt() {
        return diffBaseAmt;
    }

    /**
     * Setter method of "diffBaseAmt".
     * 
     * @param diffBaseAmt Set in "diffBaseAmt".
     */
    public void setDiffBaseAmt(BigDecimal diffBaseAmt) {
        this.diffBaseAmt = diffBaseAmt;
    }

    /**
     * Getter method of "baseAmt".
     * 
     * @return the "baseAmt"
     */
    public BigDecimal getBaseAmt() {
        return baseAmt;
    }

    /**
     * Setter method of "baseAmt".
     * 
     * @param baseAmt Set in "baseAmt".
     */
    public void setBaseAmt(BigDecimal baseAmt) {
        this.baseAmt = baseAmt;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "asnNoLikeFront".
     * 
     * @return the "asnNoLikeFront"
     */
    public String getAsnNoLikeFront() {
        return asnNoLikeFront;
    }

    /**
     * Setter method of "asnNoLikeFront".
     * 
     * @param asnNoLikeFront Set in "asnNoLikeFront".
     */
    public void setAsnNoLikeFront(String asnNoLikeFront) {
        this.asnNoLikeFront = asnNoLikeFront;
    }

    /**
     * Getter method of "spsDoNoLikeFront".
     * 
     * @return the "spsDoNoLikeFront"
     */
    public String getSpsDoNoLikeFront() {
        return spsDoNoLikeFront;
    }

    /**
     * Setter method of "spsDoNoLikeFront".
     * 
     * @param spsDoNoLikeFront Set in "spsDoNoLikeFront".
     */
    public void setSpsDoNoLikeFront(String spsDoNoLikeFront) {
        this.spsDoNoLikeFront = spsDoNoLikeFront;
    }

    /**
     * Getter method of "revisionLikeFront".
     * 
     * @return the "revisionLikeFront"
     */
    public String getRevisionLikeFront() {
        return revisionLikeFront;
    }

    /**
     * Setter method of "revisionLikeFront".
     * 
     * @param revisionLikeFront Set in "revisionLikeFront".
     */
    public void setRevisionLikeFront(String revisionLikeFront) {
        this.revisionLikeFront = revisionLikeFront;
    }

    /**
     * Getter method of "tripNoLikeFront".
     * 
     * @return the "tripNoLikeFront"
     */
    public String getTripNoLikeFront() {
        return tripNoLikeFront;
    }

    /**
     * Setter method of "tripNoLikeFront".
     * 
     * @param tripNoLikeFront Set in "tripNoLikeFront".
     */
    public void setTripNoLikeFront(String tripNoLikeFront) {
        this.tripNoLikeFront = tripNoLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "dUnitOfMeasureLikeFront".
     * 
     * @return the "dUnitOfMeasureLikeFront"
     */
    public String getDUnitOfMeasureLikeFront() {
        return dUnitOfMeasureLikeFront;
    }

    /**
     * Setter method of "dUnitOfMeasureLikeFront".
     * 
     * @param dUnitOfMeasureLikeFront Set in "dUnitOfMeasureLikeFront".
     */
    public void setDUnitOfMeasureLikeFront(String dUnitOfMeasureLikeFront) {
        this.dUnitOfMeasureLikeFront = dUnitOfMeasureLikeFront;
    }

    /**
     * Getter method of "tmpPriceFlgLikeFront".
     * 
     * @return the "tmpPriceFlgLikeFront"
     */
    public String getTmpPriceFlgLikeFront() {
        return tmpPriceFlgLikeFront;
    }

    /**
     * Setter method of "tmpPriceFlgLikeFront".
     * 
     * @param tmpPriceFlgLikeFront Set in "tmpPriceFlgLikeFront".
     */
    public void setTmpPriceFlgLikeFront(String tmpPriceFlgLikeFront) {
        this.tmpPriceFlgLikeFront = tmpPriceFlgLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "sUnitOfMeasureLikeFront".
     * 
     * @return the "sUnitOfMeasureLikeFront"
     */
    public String getSUnitOfMeasureLikeFront() {
        return sUnitOfMeasureLikeFront;
    }

    /**
     * Setter method of "sUnitOfMeasureLikeFront".
     * 
     * @param sUnitOfMeasureLikeFront Set in "sUnitOfMeasureLikeFront".
     */
    public void setSUnitOfMeasureLikeFront(String sUnitOfMeasureLikeFront) {
        this.sUnitOfMeasureLikeFront = sUnitOfMeasureLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
