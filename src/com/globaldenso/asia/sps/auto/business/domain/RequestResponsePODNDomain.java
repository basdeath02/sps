/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * @author PC-SAMSUNG
 *
 */
public class RequestResponsePODNDomain   {
	private static final long serialVersionUID = 1L;
	private List<SpsMDensoDensoRelationWithPODNDomain> list_data ;
	public List<SpsMDensoDensoRelationWithPODNDomain> getList_data() {
		return list_data;
	}
	public void setList_data(List<SpsMDensoDensoRelationWithPODNDomain> list_data) {
		this.list_data = list_data;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
