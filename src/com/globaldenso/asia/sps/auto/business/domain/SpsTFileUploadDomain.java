/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/13       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTFileUpload"<br />
 * Table overview: SPS_T_FILE_UPLOAD<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/13 11:02:03<br />
 * 
 * This module generated automatically in 2014/10/13 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTFileUploadDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * FILE_UPLOAD_ID
     */
    private BigDecimal fileUploadId;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * FILE_NAME
     */
    private String fileName;

    /**
     * FILE_COMMENT
     */
    private String fileComment;

    /**
     * FILE_ID
     */
    private String fileId;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * VENDOR_CD
     */
    private String vendorCd;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * Default constructor
     */
    public SpsTFileUploadDomain() {
    }

    /**
     * Getter method of "fileUploadId"
     * 
     * @return the fileUploadId
     */
    public BigDecimal getFileUploadId() {
        return fileUploadId;
    }

    /**
     * Setter method of "fileUploadId"
     * 
     * @param fileUploadId Set in "fileUploadId".
     */
    public void setFileUploadId(BigDecimal fileUploadId) {
        this.fileUploadId = fileUploadId;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "fileName"
     * 
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Setter method of "fileName"
     * 
     * @param fileName Set in "fileName".
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Getter method of "fileComment"
     * 
     * @return the fileComment
     */
    public String getFileComment() {
        return fileComment;
    }

    /**
     * Setter method of "fileComment"
     * 
     * @param fileComment Set in "fileComment".
     */
    public void setFileComment(String fileComment) {
        this.fileComment = fileComment;
    }

    /**
     * Getter method of "fileId"
     * 
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Setter method of "fileId"
     * 
     * @param fileId Set in "fileId".
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

}
