/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


public class SPSTDoHeaderDNDomain   {
	private static final long serialVersionUID = 1L;
	 private String databaseName;
	private BigDecimal doId;
	private String spsDoNo;
	private String cigmaDoNo;
	private String currentSpsDoNo;
	private String currentCigmaDoNo;
	private String previousSpsDoNo;
	private String previousCigmaDoNo;
	private String revision;
	private String doIssueDate;
	private String shipmentStatus;
	private String vendorCd;
	private String sCd;
	private String sPcd;
	private String dCd;
	private String dPcd;
	private String tm;
	private String dataType;
	private String orderMethod;
	private String shipDate;
	private String shipTime;
	private String deliveryDate;
	private String deliveryTime;
	private String pdfFileId;
	private String truckRoute;
	private BigDecimal truckSeq;
	private BigDecimal poId;
	private String whPrimeReceiving;
	private String latestRevisionFlg;
	private String urgentOrderFlg;
	private String clearUrgentDate;
	private String newTruckRouteFlg;
	private String dockCode;
	private String cycle;
	private BigDecimal tripNo;
	private BigDecimal backlog;
	private String receivingGate;
	private String supplierLocation;
	private String createDscId;
	private String createDate;
	private String createTime;
	private String lastUpdateDscId;
	private String lastUpdateDate;
	private String lastUpdateTime;
	private String receiveByScanFlag;
	private BigDecimal oneWayKanbanTagFlag;
	private String oneWayKanbanTagPrintDate;
	private BigDecimal notificationMailDoFlag;
	private BigDecimal notificationMailKanbanFlag;
	private String tagOutput;
    private String dnDnTrnFlg;
    private Boolean isSuccess;
    private String msgError;
    private List<SPSTDoDetailDNDomain> listDoDet;
    private List<SPSTDoKanbanSeqDNDomain> listDoKanban;
	private BigDecimal cusNo;
	private String shipNo;
	public BigDecimal getDoId() {
		return doId;
	}
	public void setDoId(BigDecimal doId) {
		this.doId = doId;
	}
	public String getSpsDoNo() {
		return spsDoNo;
	}
	public void setSpsDoNo(String spsDoNo) {
		this.spsDoNo = spsDoNo;
	}
	public String getCigmaDoNo() {
		return cigmaDoNo;
	}
	public void setCigmaDoNo(String cigmaDoNo) {
		this.cigmaDoNo = cigmaDoNo;
	}
	public String getCurrentSpsDoNo() {
		return currentSpsDoNo;
	}
	public void setCurrentSpsDoNo(String currentSpsDoNo) {
		this.currentSpsDoNo = currentSpsDoNo;
	}
	public String getCurrentCigmaDoNo() {
		return currentCigmaDoNo;
	}
	public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
		this.currentCigmaDoNo = currentCigmaDoNo;
	}
	public String getPreviousSpsDoNo() {
		return previousSpsDoNo;
	}
	public void setPreviousSpsDoNo(String previousSpsDoNo) {
		this.previousSpsDoNo = previousSpsDoNo;
	}
	public String getPreviousCigmaDoNo() {
		return previousCigmaDoNo;
	}
	public void setPreviousCigmaDoNo(String previousCigmaDoNo) {
		this.previousCigmaDoNo = previousCigmaDoNo;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getDoIssueDate() {
		return doIssueDate;
	}
	public void setDoIssueDate(String doIssueDate) {
		this.doIssueDate = doIssueDate;
	}
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
	public String getVendorCd() {
		return vendorCd;
	}
	public void setVendorCd(String vendorCd) {
		this.vendorCd = vendorCd;
	}
	public String getsCd() {
		return sCd;
	}
	public void setsCd(String sCd) {
		this.sCd = sCd;
	}
	public String getsPcd() {
		return sPcd;
	}
	public void setsPcd(String sPcd) {
		this.sPcd = sPcd;
	}
	public String getdCd() {
		return dCd;
	}
	public void setdCd(String dCd) {
		this.dCd = dCd;
	}
	public String getdPcd() {
		return dPcd;
	}
	public void setdPcd(String dPcd) {
		this.dPcd = dPcd;
	}
	public String getTm() {
		return tm;
	}
	public void setTm(String tm) {
		this.tm = tm;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getOrderMethod() {
		return orderMethod;
	}
	public void setOrderMethod(String orderMethod) {
		this.orderMethod = orderMethod;
	}
	
	public String getPdfFileId() {
		return pdfFileId;
	}
	public void setPdfFileId(String pdfFileId) {
		this.pdfFileId = pdfFileId;
	}
	public String getTruckRoute() {
		return truckRoute;
	}
	public void setTruckRoute(String truckRoute) {
		this.truckRoute = truckRoute;
	}
	public BigDecimal getTruckSeq() {
		return truckSeq;
	}
	public void setTruckSeq(BigDecimal truckSeq) {
		this.truckSeq = truckSeq;
	}
	public BigDecimal getPoId() {
		return poId;
	}
	public void setPoId(BigDecimal poId) {
		this.poId = poId;
	}
	public String getWhPrimeReceiving() {
		return whPrimeReceiving;
	}
	public void setWhPrimeReceiving(String whPrimeReceiving) {
		this.whPrimeReceiving = whPrimeReceiving;
	}
	public String getLatestRevisionFlg() {
		return latestRevisionFlg;
	}
	public void setLatestRevisionFlg(String latestRevisionFlg) {
		this.latestRevisionFlg = latestRevisionFlg;
	}
	public String getUrgentOrderFlg() {
		return urgentOrderFlg;
	}
	public void setUrgentOrderFlg(String urgentOrderFlg) {
		this.urgentOrderFlg = urgentOrderFlg;
	}
	public String getClearUrgentDate() {
		return clearUrgentDate;
	}
	public void setClearUrgentDate(String clearUrgentDate) {
		this.clearUrgentDate = clearUrgentDate;
	}
	public String getNewTruckRouteFlg() {
		return newTruckRouteFlg;
	}
	public void setNewTruckRouteFlg(String newTruckRouteFlg) {
		this.newTruckRouteFlg = newTruckRouteFlg;
	}
	public String getDockCode() {
		return dockCode;
	}
	public void setDockCode(String dockCode) {
		this.dockCode = dockCode;
	}
	public String getCycle() {
		return cycle;
	}
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	public BigDecimal getTripNo() {
		return tripNo;
	}
	public void setTripNo(BigDecimal tripNo) {
		this.tripNo = tripNo;
	}
	public BigDecimal getBacklog() {
		return backlog;
	}
	public void setBacklog(BigDecimal backlog) {
		this.backlog = backlog;
	}
	public String getReceivingGate() {
		return receivingGate;
	}
	public void setReceivingGate(String receivingGate) {
		this.receivingGate = receivingGate;
	}
	public String getSupplierLocation() {
		return supplierLocation;
	}
	public void setSupplierLocation(String supplierLocation) {
		this.supplierLocation = supplierLocation;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	
	public String getReceiveByScanFlag() {
		return receiveByScanFlag;
	}
	public void setReceiveByScanFlag(String receiveByScanFlag) {
		this.receiveByScanFlag = receiveByScanFlag;
	}
	public BigDecimal getOneWayKanbanTagFlag() {
		return oneWayKanbanTagFlag;
	}
	public void setOneWayKanbanTagFlag(BigDecimal oneWayKanbanTagFlag) {
		this.oneWayKanbanTagFlag = oneWayKanbanTagFlag;
	}
	public String getOneWayKanbanTagPrintDate() {
		return oneWayKanbanTagPrintDate;
	}
	public void setOneWayKanbanTagPrintDate(String oneWayKanbanTagPrintDate) {
		this.oneWayKanbanTagPrintDate = oneWayKanbanTagPrintDate;
	}
	public BigDecimal getNotificationMailDoFlag() {
		return notificationMailDoFlag;
	}
	public void setNotificationMailDoFlag(BigDecimal notificationMailDoFlag) {
		this.notificationMailDoFlag = notificationMailDoFlag;
	}
	public BigDecimal getNotificationMailKanbanFlag() {
		return notificationMailKanbanFlag;
	}
	public void setNotificationMailKanbanFlag(BigDecimal notificationMailKanbanFlag) {
		this.notificationMailKanbanFlag = notificationMailKanbanFlag;
	}
	public String getTagOutput() {
		return tagOutput;
	}
	public void setTagOutput(String tagOutput) {
		this.tagOutput = tagOutput;
	}
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public List<SPSTDoDetailDNDomain> getListDoDet() {
		return listDoDet;
	}
	public void setListDoDet(List<SPSTDoDetailDNDomain> listDoDet) {
		this.listDoDet = listDoDet;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDnDnTrnFlg() {
		return dnDnTrnFlg;
	}
	public void setDnDnTrnFlg(String dnDnTrnFlg) {
		this.dnDnTrnFlg = dnDnTrnFlg;
	}
	public List<SPSTDoKanbanSeqDNDomain> getListDoKanban() {
		return listDoKanban;
	}
	public void setListDoKanban(List<SPSTDoKanbanSeqDNDomain> listDoKanban) {
		this.listDoKanban = listDoKanban;
	}
	public BigDecimal getCusNo() {
		return cusNo;
	}
	public void setCusNo(BigDecimal cusNo) {
		this.cusNo = cusNo;
	}
	public String getShipNo() {
		return shipNo;
	}
	public void setShipNo(String shipNo) {
		this.shipNo = shipNo;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getShipDate() {
		return shipDate;
	}
	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}
	public String getShipTime() {
		return shipTime;
	}
	public void setShipTime(String shipTime) {
		this.shipTime = shipTime;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
    
	
}
