/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTmpUserSupplier"<br />
 * Table overview: SPS_TMP_USER_SUPPLIER<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/30 12:07:21<br />
 * 
 * This module generated automatically in 2015/03/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserSupplierDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User DSC ID that upload data to system
     */
    private String userDscId;

    /**
     * Source file line number
     */
    private BigDecimal lineNo;

    /**
     * User identify number (import data)
     */
    private String dscId;

    /**
     * Main Desnso company to handle on this user information  (import data)
     */
    private String dOwner;

    /**
     * Supplier company code (import data)
     */
    private String sCd;

    /**
     * Supplier plant code (import data)
     */
    private String sPcd;

    /**
     * Supplier department name (import data)
     */
    private String departmentCd;

    /**
     * User first name (import data)
     */
    private String firstName;

    /**
     * User middle name (import data)
     */
    private String middleName;

    /**
     * User last name (import data)
     */
    private String lastName;

    /**
     * User email addresses if has email address more than 1 separate each of it by comma ","  (import data)
     */
    private String email;

    /**
     * User telephone number (import data)
     */
    private String telephone;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlUrgentOrderFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlSInfoNotfoundFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlAllowReviseFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlCancelInvoiceFlag;

    /**
     * User role assignment value  (import data)
     */
    private String roleCd;

    /**
     * Scope of user right on Supplier plant  (import data)
     */
    private String roleSPcd;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * User role effective start date  (import data)
     */
    private Timestamp effectStart;

    /**
     * User role effective end date  (import data)
     */
    private Timestamp effectEnd;

    /**
     * A : Add, U : Update, D : Delete
     */
    private String informationFlag;

    /**
     * Upload operation timestamp
     */
    private Timestamp uploadDatetime;

    /**
     * User login session code
     */
    private String sessionCd;

    /**
     * A : Add, U : Update, T : Terminate
     */
    private String roleFlag;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete
     */
    private Timestamp userLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_SUPPLIER) in case of Update/Delete
     */
    private Timestamp supplierLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete
     */
    private Timestamp roleLastUpdate;

    /**
     * 0 : not move yet, 1 : move to actual table
     */
    private String isActualRegister;

    /**
     * Move data from temporary table to actual table timestamp
     */
    private Timestamp toActualDatetime;

    /**
     * Default constructor
     */
    public SpsTmpUserSupplierDomain() {
    }

    /**
     * Getter method of "userDscId"
     * 
     * @return the userDscId
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId"
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "lineNo"
     * 
     * @return the lineNo
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo"
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dscId"
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId"
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dOwner"
     * 
     * @return the dOwner
     */
    public String getDOwner() {
        return dOwner;
    }

    /**
     * Setter method of "dOwner"
     * 
     * @param dOwner Set in "dOwner".
     */
    public void setDOwner(String dOwner) {
        this.dOwner = dOwner;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "departmentCd"
     * 
     * @return the departmentCd
     */
    public String getDepartmentCd() {
        return departmentCd;
    }

    /**
     * Setter method of "departmentCd"
     * 
     * @param departmentCd Set in "departmentCd".
     */
    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    /**
     * Getter method of "firstName"
     * 
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName"
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName"
     * 
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName"
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName"
     * 
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName"
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email"
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email"
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "emlUrgentOrderFlag"
     * 
     * @return the emlUrgentOrderFlag
     */
    public String getEmlUrgentOrderFlag() {
        return emlUrgentOrderFlag;
    }

    /**
     * Setter method of "emlUrgentOrderFlag"
     * 
     * @param emlUrgentOrderFlag Set in "emlUrgentOrderFlag".
     */
    public void setEmlUrgentOrderFlag(String emlUrgentOrderFlag) {
        this.emlUrgentOrderFlag = emlUrgentOrderFlag;
    }

    /**
     * Getter method of "emlSInfoNotfoundFlag"
     * 
     * @return the emlSInfoNotfoundFlag
     */
    public String getEmlSInfoNotfoundFlag() {
        return emlSInfoNotfoundFlag;
    }

    /**
     * Setter method of "emlSInfoNotfoundFlag"
     * 
     * @param emlSInfoNotfoundFlag Set in "emlSInfoNotfoundFlag".
     */
    public void setEmlSInfoNotfoundFlag(String emlSInfoNotfoundFlag) {
        this.emlSInfoNotfoundFlag = emlSInfoNotfoundFlag;
    }

    /**
     * Getter method of "emlAllowReviseFlag"
     * 
     * @return the emlAllowReviseFlag
     */
    public String getEmlAllowReviseFlag() {
        return emlAllowReviseFlag;
    }

    /**
     * Setter method of "emlAllowReviseFlag"
     * 
     * @param emlAllowReviseFlag Set in "emlAllowReviseFlag".
     */
    public void setEmlAllowReviseFlag(String emlAllowReviseFlag) {
        this.emlAllowReviseFlag = emlAllowReviseFlag;
    }

    /**
     * Getter method of "emlCancelInvoiceFlag"
     * 
     * @return the emlCancelInvoiceFlag
     */
    public String getEmlCancelInvoiceFlag() {
        return emlCancelInvoiceFlag;
    }

    /**
     * Setter method of "emlCancelInvoiceFlag"
     * 
     * @param emlCancelInvoiceFlag Set in "emlCancelInvoiceFlag".
     */
    public void setEmlCancelInvoiceFlag(String emlCancelInvoiceFlag) {
        this.emlCancelInvoiceFlag = emlCancelInvoiceFlag;
    }

    /**
     * Getter method of "roleCd"
     * 
     * @return the roleCd
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Setter method of "roleCd"
     * 
     * @param roleCd Set in "roleCd".
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Getter method of "roleSPcd"
     * 
     * @return the roleSPcd
     */
    public String getRoleSPcd() {
        return roleSPcd;
    }

    /**
     * Setter method of "roleSPcd"
     * 
     * @param roleSPcd Set in "roleSPcd".
     */
    public void setRoleSPcd(String roleSPcd) {
        this.roleSPcd = roleSPcd;
    }

    /**
     * Getter method of "seqNo"
     * 
     * @return the seqNo
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo"
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "effectStart"
     * 
     * @return the effectStart
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart"
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd"
     * 
     * @return the effectEnd
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd"
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "informationFlag"
     * 
     * @return the informationFlag
     */
    public String getInformationFlag() {
        return informationFlag;
    }

    /**
     * Setter method of "informationFlag"
     * 
     * @param informationFlag Set in "informationFlag".
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "sessionCd"
     * 
     * @return the sessionCd
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd"
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "roleFlag"
     * 
     * @return the roleFlag
     */
    public String getRoleFlag() {
        return roleFlag;
    }

    /**
     * Setter method of "roleFlag"
     * 
     * @param roleFlag Set in "roleFlag".
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Getter method of "userLastUpdate"
     * 
     * @return the userLastUpdate
     */
    public Timestamp getUserLastUpdate() {
        return userLastUpdate;
    }

    /**
     * Setter method of "userLastUpdate"
     * 
     * @param userLastUpdate Set in "userLastUpdate".
     */
    public void setUserLastUpdate(Timestamp userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    /**
     * Getter method of "supplierLastUpdate"
     * 
     * @return the supplierLastUpdate
     */
    public Timestamp getSupplierLastUpdate() {
        return supplierLastUpdate;
    }

    /**
     * Setter method of "supplierLastUpdate"
     * 
     * @param supplierLastUpdate Set in "supplierLastUpdate".
     */
    public void setSupplierLastUpdate(Timestamp supplierLastUpdate) {
        this.supplierLastUpdate = supplierLastUpdate;
    }

    /**
     * Getter method of "roleLastUpdate"
     * 
     * @return the roleLastUpdate
     */
    public Timestamp getRoleLastUpdate() {
        return roleLastUpdate;
    }

    /**
     * Setter method of "roleLastUpdate"
     * 
     * @param roleLastUpdate Set in "roleLastUpdate".
     */
    public void setRoleLastUpdate(Timestamp roleLastUpdate) {
        this.roleLastUpdate = roleLastUpdate;
    }

    /**
     * Getter method of "isActualRegister"
     * 
     * @return the isActualRegister
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister"
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime"
     * 
     * @return the toActualDatetime
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime"
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

}
