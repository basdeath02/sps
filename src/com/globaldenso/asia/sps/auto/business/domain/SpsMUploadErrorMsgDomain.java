/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMUploadErrorMsg"<br />
 * Table overview: SPS_M_UPLOAD_ERROR_MSG<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUploadErrorMsgDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ERROR_CD
     */
    private String errorCd;

    /**
     * FIELD_NAME
     */
    private String fieldName;

    /**
     * ERROR_MESSAGE
     */
    private String errorMessage;

    /**
     * IS_WARNING
     */
    private String isWarning;

    /**
     * CREATE_USER
     */
    private String createUser;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_USER
     */
    private String lastUpdateUser;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMUploadErrorMsgDomain() {
    }

    /**
     * Getter method of "errorCd"
     * 
     * @return the errorCd
     */
    public String getErrorCd() {
        return errorCd;
    }

    /**
     * Setter method of "errorCd"
     * 
     * @param errorCd Set in "errorCd".
     */
    public void setErrorCd(String errorCd) {
        this.errorCd = errorCd;
    }

    /**
     * Getter method of "fieldName"
     * 
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter method of "fieldName"
     * 
     * @param fieldName Set in "fieldName".
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Getter method of "errorMessage"
     * 
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Setter method of "errorMessage"
     * 
     * @param errorMessage Set in "errorMessage".
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Getter method of "isWarning"
     * 
     * @return the isWarning
     */
    public String getIsWarning() {
        return isWarning;
    }

    /**
     * Setter method of "isWarning"
     * 
     * @param isWarning Set in "isWarning".
     */
    public void setIsWarning(String isWarning) {
        this.isWarning = isWarning;
    }

    /**
     * Getter method of "createUser"
     * 
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method of "createUser"
     * 
     * @param createUser Set in "createUser".
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateUser"
     * 
     * @return the lastUpdateUser
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Setter method of "lastUpdateUser"
     * 
     * @param lastUpdateUser Set in "lastUpdateUser".
     */
    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
