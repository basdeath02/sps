/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/10/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTmpUploadInvoice"<br />
 * Table overview: SPS_TMP_UPLOAD_INVOICE<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/10/30 14:41:58<br />
 * 
 * This module generated automatically in 2015/10/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadInvoiceDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running number of Upload Invoice
     */
    private BigDecimal uploadInvoiceId;

    /**
     * Session Identity
     */
    private String sessionId;

    /**
     * AS400 Vendor Code
     */
    private String vendorCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * Supplier TAX Identity
     */
    private String sTaxId;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Company Plant Code
     */
    private String dPcd;

    /**
     * Invoice Number
     */
    private String invoiceNo;

    /**
     * Invoice Date
     */
    private Date invoiceDate;

    /**
     * "1": VAT/GST , "0": None VAT/GST
     */
    private String vatType;

    /**
     * Invoice VAT Rate
     */
    private BigDecimal vatRate;

    /**
     * Supplier Currency Code
     */
    private String currencyCd;

    /**
     * Invoice Base Amount
     */
    private BigDecimal baseAmount;

    /**
     * Invoice VAT Amount
     */
    private BigDecimal vatAmount;

    /**
     * Invoice Total Amount
     */
    private BigDecimal totalAmount;

    /**
     * Credit Note Number
     */
    private String cnNo;

    /**
     * Credit Note Date
     */
    private Date cnDate;

    /**
     * Credit Note Base Amount
     */
    private BigDecimal cnBaseAmount;

    /**
     * Credit Note VAT Amount
     */
    private BigDecimal cnVatAmount;

    /**
     * Credit Note Total Amount
     */
    private BigDecimal cnTotalAmount;

    /**
     * 0 : Not error
1 : Error
2 : Warning
     */
    private String uploadResult;

    /**
     * Error code
     */
    private String errorCode;

    /**
     * CSV Line Number
     */
    private BigDecimal csvLineNo;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * Trip Number
     */
    private String tripNo;

    /**
     * SPS D/O No.
     */
    private String spsDoNo;

    /**
     * Latest D/O Revision
     */
    private String revision;

    /**
     * Plan ETA
     */
    private Timestamp planEta;

    /**
     * Actual ETD
     */
    private Timestamp actualEtd;

    /**
     * Coordinated Universal Time
     */
    private String utc;

    /**
     * DESNO Part Number
     */
    private String dPn;

    /**
     * Supplier Part Number
     */
    private String sPn;

    /**
     * Shipping Quantity
     */
    private BigDecimal shippingQty;

    /**
     * Supplier Unit ot Measure
     */
    private String sUnitOfMeasure;

    /**
     * Supplier Unit Price
     */
    private BigDecimal sPriceUnit;

    /**
     * DSC ID of Uploaded data
     */
    private String uploadDscId;

    /**
     * Datetime of Uploaded data
     */
    private Timestamp uploadDatetime;

    /**
     * Default constructor
     */
    public SpsTmpUploadInvoiceDomain() {
    }

    /**
     * Getter method of "uploadInvoiceId"
     * 
     * @return the uploadInvoiceId
     */
    public BigDecimal getUploadInvoiceId() {
        return uploadInvoiceId;
    }

    /**
     * Setter method of "uploadInvoiceId"
     * 
     * @param uploadInvoiceId Set in "uploadInvoiceId".
     */
    public void setUploadInvoiceId(BigDecimal uploadInvoiceId) {
        this.uploadInvoiceId = uploadInvoiceId;
    }

    /**
     * Getter method of "sessionId"
     * 
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Setter method of "sessionId"
     * 
     * @param sessionId Set in "sessionId".
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "sTaxId"
     * 
     * @return the sTaxId
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * Setter method of "sTaxId"
     * 
     * @param sTaxId Set in "sTaxId".
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "invoiceNo"
     * 
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Setter method of "invoiceNo"
     * 
     * @param invoiceNo Set in "invoiceNo".
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * Getter method of "invoiceDate"
     * 
     * @return the invoiceDate
     */
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Setter method of "invoiceDate"
     * 
     * @param invoiceDate Set in "invoiceDate".
     */
    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * Getter method of "vatType"
     * 
     * @return the vatType
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * Setter method of "vatType"
     * 
     * @param vatType Set in "vatType".
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }

    /**
     * Getter method of "vatRate"
     * 
     * @return the vatRate
     */
    public BigDecimal getVatRate() {
        return vatRate;
    }

    /**
     * Setter method of "vatRate"
     * 
     * @param vatRate Set in "vatRate".
     */
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * Getter method of "currencyCd"
     * 
     * @return the currencyCd
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * Setter method of "currencyCd"
     * 
     * @param currencyCd Set in "currencyCd".
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * Getter method of "baseAmount"
     * 
     * @return the baseAmount
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /**
     * Setter method of "baseAmount"
     * 
     * @param baseAmount Set in "baseAmount".
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /**
     * Getter method of "vatAmount"
     * 
     * @return the vatAmount
     */
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    /**
     * Setter method of "vatAmount"
     * 
     * @param vatAmount Set in "vatAmount".
     */
    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    /**
     * Getter method of "totalAmount"
     * 
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Setter method of "totalAmount"
     * 
     * @param totalAmount Set in "totalAmount".
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Getter method of "cnNo"
     * 
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * Setter method of "cnNo"
     * 
     * @param cnNo Set in "cnNo".
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * Getter method of "cnDate"
     * 
     * @return the cnDate
     */
    public Date getCnDate() {
        return cnDate;
    }

    /**
     * Setter method of "cnDate"
     * 
     * @param cnDate Set in "cnDate".
     */
    public void setCnDate(Date cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * Getter method of "cnBaseAmount"
     * 
     * @return the cnBaseAmount
     */
    public BigDecimal getCnBaseAmount() {
        return cnBaseAmount;
    }

    /**
     * Setter method of "cnBaseAmount"
     * 
     * @param cnBaseAmount Set in "cnBaseAmount".
     */
    public void setCnBaseAmount(BigDecimal cnBaseAmount) {
        this.cnBaseAmount = cnBaseAmount;
    }

    /**
     * Getter method of "cnVatAmount"
     * 
     * @return the cnVatAmount
     */
    public BigDecimal getCnVatAmount() {
        return cnVatAmount;
    }

    /**
     * Setter method of "cnVatAmount"
     * 
     * @param cnVatAmount Set in "cnVatAmount".
     */
    public void setCnVatAmount(BigDecimal cnVatAmount) {
        this.cnVatAmount = cnVatAmount;
    }

    /**
     * Getter method of "cnTotalAmount"
     * 
     * @return the cnTotalAmount
     */
    public BigDecimal getCnTotalAmount() {
        return cnTotalAmount;
    }

    /**
     * Setter method of "cnTotalAmount"
     * 
     * @param cnTotalAmount Set in "cnTotalAmount".
     */
    public void setCnTotalAmount(BigDecimal cnTotalAmount) {
        this.cnTotalAmount = cnTotalAmount;
    }

    /**
     * Getter method of "uploadResult"
     * 
     * @return the uploadResult
     */
    public String getUploadResult() {
        return uploadResult;
    }

    /**
     * Setter method of "uploadResult"
     * 
     * @param uploadResult Set in "uploadResult".
     */
    public void setUploadResult(String uploadResult) {
        this.uploadResult = uploadResult;
    }

    /**
     * Getter method of "errorCode"
     * 
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Setter method of "errorCode"
     * 
     * @param errorCode Set in "errorCode".
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Getter method of "csvLineNo"
     * 
     * @return the csvLineNo
     */
    public BigDecimal getCsvLineNo() {
        return csvLineNo;
    }

    /**
     * Setter method of "csvLineNo"
     * 
     * @param csvLineNo Set in "csvLineNo".
     */
    public void setCsvLineNo(BigDecimal csvLineNo) {
        this.csvLineNo = csvLineNo;
    }

    /**
     * Getter method of "asnNo"
     * 
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo"
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "tripNo"
     * 
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo"
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "spsDoNo"
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo"
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision"
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision"
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "planEta"
     * 
     * @return the planEta
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * Setter method of "planEta"
     * 
     * @param planEta Set in "planEta".
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * Getter method of "actualEtd"
     * 
     * @return the actualEtd
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * Setter method of "actualEtd"
     * 
     * @param actualEtd Set in "actualEtd".
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }

    /**
     * Getter method of "utc"
     * 
     * @return the utc
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Setter method of "utc"
     * 
     * @param utc Set in "utc".
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "shippingQty"
     * 
     * @return the shippingQty
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty"
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "sUnitOfMeasure"
     * 
     * @return the sUnitOfMeasure
     */
    public String getSUnitOfMeasure() {
        return sUnitOfMeasure;
    }

    /**
     * Setter method of "sUnitOfMeasure"
     * 
     * @param sUnitOfMeasure Set in "sUnitOfMeasure".
     */
    public void setSUnitOfMeasure(String sUnitOfMeasure) {
        this.sUnitOfMeasure = sUnitOfMeasure;
    }

    /**
     * Getter method of "sPriceUnit"
     * 
     * @return the sPriceUnit
     */
    public BigDecimal getSPriceUnit() {
        return sPriceUnit;
    }

    /**
     * Setter method of "sPriceUnit"
     * 
     * @param sPriceUnit Set in "sPriceUnit".
     */
    public void setSPriceUnit(BigDecimal sPriceUnit) {
        this.sPriceUnit = sPriceUnit;
    }

    /**
     * Getter method of "uploadDscId"
     * 
     * @return the uploadDscId
     */
    public String getUploadDscId() {
        return uploadDscId;
    }

    /**
     * Setter method of "uploadDscId"
     * 
     * @param uploadDscId Set in "uploadDscId".
     */
    public void setUploadDscId(String uploadDscId) {
        this.uploadDscId = uploadDscId;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

}
