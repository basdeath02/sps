/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/16       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTPoDue".<br />
 * Table overview: SPS_T_PO_DUE<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/16 14:33:35<br />
 * 
 * This module generated automatically in 2016/02/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDueCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from SPS_T_PO
     */
    private BigDecimal poId;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Running No of Due
     */
    private BigDecimal dueId;

    /**
     * Due Date
     */
    private Date dueDate;

    /**
     * Report Type Flag
0 : D -> Daily
1 : W -> Weekly
2 : M -> Monthly
     */
    private String reportTypeFlg;

    /**
     * Order Qty
     */
    private BigDecimal orderQty;

    /**
     * ETD Date
     */
    private Date etd;

    /**
     * DEL (CIGMA column DEL421)
     */
    private String del;

    /**
     * SEQ (CIGMA column SEQ421)
     */
    private BigDecimal seq;

    /**
     * Previous Order Qty
     */
    private BigDecimal previousQty;

    /**
     * Difference Order Qty
     */
    private BigDecimal differenceQty;

    /**
     * Change Reason
     */
    private String rsn;

    /**
     * SPS Proposed Due Date
     */
    private Date spsProposedDueDate;

    /**
     * SPS Proposed Qty
     */
    private BigDecimal spsProposedQty;

    /**
     * SPS Pending Reason Code
     */
    private String spsPendingReasonCd;

    /**
     * Mark Pending Flag when action from screen WORD004
GO 0 : None, 1 : Pending
     */
    private String markPendingFlg;

    /**
     * Inform Change P/O or not
GO 0 : Not Change, 1 : Change
     */
    private String changeFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * 1 : Accept or 0 : Reject Supplier Promised Due
     */
    private String densoReplyFlg;

    /**
     * Supplier Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * DENSO Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Due Date(condition whether the column value is greater than or equal to the value)
     */
    private Date dueDateGreaterThanEqual;

    /**
     * Due Date(condition whether the column value is less than or equal to the value)
     */
    private Date dueDateLessThanEqual;

    /**
     * Report Type Flag
0 : D -> Daily
1 : W -> Weekly
2 : M -> Monthly(condition whether the column value at the beginning is equal to the value)
     */
    private String reportTypeFlgLikeFront;

    /**
     * ETD Date(condition whether the column value is greater than or equal to the value)
     */
    private Date etdGreaterThanEqual;

    /**
     * ETD Date(condition whether the column value is less than or equal to the value)
     */
    private Date etdLessThanEqual;

    /**
     * DEL (CIGMA column DEL421)(condition whether the column value at the beginning is equal to the value)
     */
    private String delLikeFront;

    /**
     * Change Reason(condition whether the column value at the beginning is equal to the value)
     */
    private String rsnLikeFront;

    /**
     * SPS Proposed Due Date(condition whether the column value is greater than or equal to the value)
     */
    private Date spsProposedDueDateGreaterThanEqual;

    /**
     * SPS Proposed Due Date(condition whether the column value is less than or equal to the value)
     */
    private Date spsProposedDueDateLessThanEqual;

    /**
     * SPS Pending Reason Code(condition whether the column value at the beginning is equal to the value)
     */
    private String spsPendingReasonCdLikeFront;

    /**
     * Mark Pending Flag when action from screen WORD004
GO 0 : None, 1 : Pending(condition whether the column value at the beginning is equal to the value)
     */
    private String markPendingFlgLikeFront;

    /**
     * Inform Change P/O or not
GO 0 : Not Change, 1 : Change(condition whether the column value at the beginning is equal to the value)
     */
    private String changeFlgLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * 1 : Accept or 0 : Reject Supplier Promised Due(condition whether the column value at the beginning is equal to the value)
     */
    private String densoReplyFlgLikeFront;

    /**
     * Default constructor
     */
    public SpsTPoDueCriteriaDomain() {
    }

    /**
     * Getter method of "poId".
     * 
     * @return the "poId"
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId".
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "dueId".
     * 
     * @return the "dueId"
     */
    public BigDecimal getDueId() {
        return dueId;
    }

    /**
     * Setter method of "dueId".
     * 
     * @param dueId Set in "dueId".
     */
    public void setDueId(BigDecimal dueId) {
        this.dueId = dueId;
    }

    /**
     * Getter method of "dueDate".
     * 
     * @return the "dueDate"
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Setter method of "dueDate".
     * 
     * @param dueDate Set in "dueDate".
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Getter method of "reportTypeFlg".
     * 
     * @return the "reportTypeFlg"
     */
    public String getReportTypeFlg() {
        return reportTypeFlg;
    }

    /**
     * Setter method of "reportTypeFlg".
     * 
     * @param reportTypeFlg Set in "reportTypeFlg".
     */
    public void setReportTypeFlg(String reportTypeFlg) {
        this.reportTypeFlg = reportTypeFlg;
    }

    /**
     * Getter method of "orderQty".
     * 
     * @return the "orderQty"
     */
    public BigDecimal getOrderQty() {
        return orderQty;
    }

    /**
     * Setter method of "orderQty".
     * 
     * @param orderQty Set in "orderQty".
     */
    public void setOrderQty(BigDecimal orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * Getter method of "etd".
     * 
     * @return the "etd"
     */
    public Date getEtd() {
        return etd;
    }

    /**
     * Setter method of "etd".
     * 
     * @param etd Set in "etd".
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }

    /**
     * Getter method of "del".
     * 
     * @return the "del"
     */
    public String getDel() {
        return del;
    }

    /**
     * Setter method of "del".
     * 
     * @param del Set in "del".
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * Getter method of "seq".
     * 
     * @return the "seq"
     */
    public BigDecimal getSeq() {
        return seq;
    }

    /**
     * Setter method of "seq".
     * 
     * @param seq Set in "seq".
     */
    public void setSeq(BigDecimal seq) {
        this.seq = seq;
    }

    /**
     * Getter method of "previousQty".
     * 
     * @return the "previousQty"
     */
    public BigDecimal getPreviousQty() {
        return previousQty;
    }

    /**
     * Setter method of "previousQty".
     * 
     * @param previousQty Set in "previousQty".
     */
    public void setPreviousQty(BigDecimal previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * Getter method of "differenceQty".
     * 
     * @return the "differenceQty"
     */
    public BigDecimal getDifferenceQty() {
        return differenceQty;
    }

    /**
     * Setter method of "differenceQty".
     * 
     * @param differenceQty Set in "differenceQty".
     */
    public void setDifferenceQty(BigDecimal differenceQty) {
        this.differenceQty = differenceQty;
    }

    /**
     * Getter method of "rsn".
     * 
     * @return the "rsn"
     */
    public String getRsn() {
        return rsn;
    }

    /**
     * Setter method of "rsn".
     * 
     * @param rsn Set in "rsn".
     */
    public void setRsn(String rsn) {
        this.rsn = rsn;
    }

    /**
     * Getter method of "spsProposedDueDate".
     * 
     * @return the "spsProposedDueDate"
     */
    public Date getSpsProposedDueDate() {
        return spsProposedDueDate;
    }

    /**
     * Setter method of "spsProposedDueDate".
     * 
     * @param spsProposedDueDate Set in "spsProposedDueDate".
     */
    public void setSpsProposedDueDate(Date spsProposedDueDate) {
        this.spsProposedDueDate = spsProposedDueDate;
    }

    /**
     * Getter method of "spsProposedQty".
     * 
     * @return the "spsProposedQty"
     */
    public BigDecimal getSpsProposedQty() {
        return spsProposedQty;
    }

    /**
     * Setter method of "spsProposedQty".
     * 
     * @param spsProposedQty Set in "spsProposedQty".
     */
    public void setSpsProposedQty(BigDecimal spsProposedQty) {
        this.spsProposedQty = spsProposedQty;
    }

    /**
     * Getter method of "spsPendingReasonCd".
     * 
     * @return the "spsPendingReasonCd"
     */
    public String getSpsPendingReasonCd() {
        return spsPendingReasonCd;
    }

    /**
     * Setter method of "spsPendingReasonCd".
     * 
     * @param spsPendingReasonCd Set in "spsPendingReasonCd".
     */
    public void setSpsPendingReasonCd(String spsPendingReasonCd) {
        this.spsPendingReasonCd = spsPendingReasonCd;
    }

    /**
     * Getter method of "markPendingFlg".
     * 
     * @return the "markPendingFlg"
     */
    public String getMarkPendingFlg() {
        return markPendingFlg;
    }

    /**
     * Setter method of "markPendingFlg".
     * 
     * @param markPendingFlg Set in "markPendingFlg".
     */
    public void setMarkPendingFlg(String markPendingFlg) {
        this.markPendingFlg = markPendingFlg;
    }

    /**
     * Getter method of "changeFlg".
     * 
     * @return the "changeFlg"
     */
    public String getChangeFlg() {
        return changeFlg;
    }

    /**
     * Setter method of "changeFlg".
     * 
     * @param changeFlg Set in "changeFlg".
     */
    public void setChangeFlg(String changeFlg) {
        this.changeFlg = changeFlg;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "densoReplyFlg".
     * 
     * @return the "densoReplyFlg"
     */
    public String getDensoReplyFlg() {
        return densoReplyFlg;
    }

    /**
     * Setter method of "densoReplyFlg".
     * 
     * @param densoReplyFlg Set in "densoReplyFlg".
     */
    public void setDensoReplyFlg(String densoReplyFlg) {
        this.densoReplyFlg = densoReplyFlg;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "dueDateGreaterThanEqual".
     * 
     * @return the "dueDateGreaterThanEqual"
     */
    public Date getDueDateGreaterThanEqual() {
        return dueDateGreaterThanEqual;
    }

    /**
     * Setter method of "dueDateGreaterThanEqual".
     * 
     * @param dueDateGreaterThanEqual Set in "dueDateGreaterThanEqual".
     */
    public void setDueDateGreaterThanEqual(Date dueDateGreaterThanEqual) {
        this.dueDateGreaterThanEqual = dueDateGreaterThanEqual;
    }

    /**
     * Getter method of "dueDateLessThanEqual".
     * 
     * @return the "dueDateLessThanEqual"
     */
    public Date getDueDateLessThanEqual() {
        return dueDateLessThanEqual;
    }

    /**
     * Setter method of "dueDateLessThanEqual".
     * 
     * @param dueDateLessThanEqual Set in "dueDateLessThanEqual".
     */
    public void setDueDateLessThanEqual(Date dueDateLessThanEqual) {
        this.dueDateLessThanEqual = dueDateLessThanEqual;
    }

    /**
     * Getter method of "reportTypeFlgLikeFront".
     * 
     * @return the "reportTypeFlgLikeFront"
     */
    public String getReportTypeFlgLikeFront() {
        return reportTypeFlgLikeFront;
    }

    /**
     * Setter method of "reportTypeFlgLikeFront".
     * 
     * @param reportTypeFlgLikeFront Set in "reportTypeFlgLikeFront".
     */
    public void setReportTypeFlgLikeFront(String reportTypeFlgLikeFront) {
        this.reportTypeFlgLikeFront = reportTypeFlgLikeFront;
    }

    /**
     * Getter method of "etdGreaterThanEqual".
     * 
     * @return the "etdGreaterThanEqual"
     */
    public Date getEtdGreaterThanEqual() {
        return etdGreaterThanEqual;
    }

    /**
     * Setter method of "etdGreaterThanEqual".
     * 
     * @param etdGreaterThanEqual Set in "etdGreaterThanEqual".
     */
    public void setEtdGreaterThanEqual(Date etdGreaterThanEqual) {
        this.etdGreaterThanEqual = etdGreaterThanEqual;
    }

    /**
     * Getter method of "etdLessThanEqual".
     * 
     * @return the "etdLessThanEqual"
     */
    public Date getEtdLessThanEqual() {
        return etdLessThanEqual;
    }

    /**
     * Setter method of "etdLessThanEqual".
     * 
     * @param etdLessThanEqual Set in "etdLessThanEqual".
     */
    public void setEtdLessThanEqual(Date etdLessThanEqual) {
        this.etdLessThanEqual = etdLessThanEqual;
    }

    /**
     * Getter method of "delLikeFront".
     * 
     * @return the "delLikeFront"
     */
    public String getDelLikeFront() {
        return delLikeFront;
    }

    /**
     * Setter method of "delLikeFront".
     * 
     * @param delLikeFront Set in "delLikeFront".
     */
    public void setDelLikeFront(String delLikeFront) {
        this.delLikeFront = delLikeFront;
    }

    /**
     * Getter method of "rsnLikeFront".
     * 
     * @return the "rsnLikeFront"
     */
    public String getRsnLikeFront() {
        return rsnLikeFront;
    }

    /**
     * Setter method of "rsnLikeFront".
     * 
     * @param rsnLikeFront Set in "rsnLikeFront".
     */
    public void setRsnLikeFront(String rsnLikeFront) {
        this.rsnLikeFront = rsnLikeFront;
    }

    /**
     * Getter method of "spsProposedDueDateGreaterThanEqual".
     * 
     * @return the "spsProposedDueDateGreaterThanEqual"
     */
    public Date getSpsProposedDueDateGreaterThanEqual() {
        return spsProposedDueDateGreaterThanEqual;
    }

    /**
     * Setter method of "spsProposedDueDateGreaterThanEqual".
     * 
     * @param spsProposedDueDateGreaterThanEqual Set in "spsProposedDueDateGreaterThanEqual".
     */
    public void setSpsProposedDueDateGreaterThanEqual(Date spsProposedDueDateGreaterThanEqual) {
        this.spsProposedDueDateGreaterThanEqual = spsProposedDueDateGreaterThanEqual;
    }

    /**
     * Getter method of "spsProposedDueDateLessThanEqual".
     * 
     * @return the "spsProposedDueDateLessThanEqual"
     */
    public Date getSpsProposedDueDateLessThanEqual() {
        return spsProposedDueDateLessThanEqual;
    }

    /**
     * Setter method of "spsProposedDueDateLessThanEqual".
     * 
     * @param spsProposedDueDateLessThanEqual Set in "spsProposedDueDateLessThanEqual".
     */
    public void setSpsProposedDueDateLessThanEqual(Date spsProposedDueDateLessThanEqual) {
        this.spsProposedDueDateLessThanEqual = spsProposedDueDateLessThanEqual;
    }

    /**
     * Getter method of "spsPendingReasonCdLikeFront".
     * 
     * @return the "spsPendingReasonCdLikeFront"
     */
    public String getSpsPendingReasonCdLikeFront() {
        return spsPendingReasonCdLikeFront;
    }

    /**
     * Setter method of "spsPendingReasonCdLikeFront".
     * 
     * @param spsPendingReasonCdLikeFront Set in "spsPendingReasonCdLikeFront".
     */
    public void setSpsPendingReasonCdLikeFront(String spsPendingReasonCdLikeFront) {
        this.spsPendingReasonCdLikeFront = spsPendingReasonCdLikeFront;
    }

    /**
     * Getter method of "markPendingFlgLikeFront".
     * 
     * @return the "markPendingFlgLikeFront"
     */
    public String getMarkPendingFlgLikeFront() {
        return markPendingFlgLikeFront;
    }

    /**
     * Setter method of "markPendingFlgLikeFront".
     * 
     * @param markPendingFlgLikeFront Set in "markPendingFlgLikeFront".
     */
    public void setMarkPendingFlgLikeFront(String markPendingFlgLikeFront) {
        this.markPendingFlgLikeFront = markPendingFlgLikeFront;
    }

    /**
     * Getter method of "changeFlgLikeFront".
     * 
     * @return the "changeFlgLikeFront"
     */
    public String getChangeFlgLikeFront() {
        return changeFlgLikeFront;
    }

    /**
     * Setter method of "changeFlgLikeFront".
     * 
     * @param changeFlgLikeFront Set in "changeFlgLikeFront".
     */
    public void setChangeFlgLikeFront(String changeFlgLikeFront) {
        this.changeFlgLikeFront = changeFlgLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Getter method of "densoReplyFlgLikeFront".
     * 
     * @return the "densoReplyFlgLikeFront"
     */
    public String getDensoReplyFlgLikeFront() {
        return densoReplyFlgLikeFront;
    }

    /**
     * Setter method of "densoReplyFlgLikeFront".
     * 
     * @param densoReplyFlgLikeFront Set in "densoReplyFlgLikeFront".
     */
    public void setDensoReplyFlgLikeFront(String densoReplyFlgLikeFront) {
        this.densoReplyFlgLikeFront = densoReplyFlgLikeFront;
    }

}
