/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


public class SPSTAsnHeaderDNErrorListDomain   {
	private static final long serialVersionUID = 1L;
    private String asnNo;
    private String msgError;
    private List<String> errorCodeList;
    private List<SPSTAsnDNErrorListDomain> errorListDet;
    private String asnStatus;
	public String getAsnNo() {
		return asnNo;
	}
	public void setAsnNo(String asnNo) {
		this.asnNo = asnNo;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<String> getErrorCodeList() {
		return errorCodeList;
	}
	public void setErrorCodeList(List<String> errorCodeList) {
		this.errorCodeList = errorCodeList;
	}
	public List<SPSTAsnDNErrorListDomain> getErrorListDet() {
		return errorListDet;
	}
	public void setErrorListDet(List<SPSTAsnDNErrorListDomain> errorListDet) {
		this.errorListDet = errorListDet;
	}
    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }
    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }
}
