/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/01/05       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMUnitOfMeasure".<br />
 * Table overview: SPS_M_UNIT_OF_MEASURE<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/01/05 09:25:07<br />
 * 
 * This module generated automatically in 2016/01/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUnitOfMeasureCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No for Unit of Measure
     */
    private String unitOfMeasureId;

    /**
     * Unit of Measure Name
     */
    private String unitOfMeasureName;

    /**
     * 0 : Not display decimal point (On ASN Maintenance screen)
1 : Display decimal point (On ASN Maintenance screen)
     */
    private String displayDecimalPointFlg;

    /**
     * 0 : Disable to input actual Shipping Box Qty
1 : Enable to input actual Shipping Box Qty
     */
    private String editShippingBoxFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Running No for Unit of Measure(condition whether the column value at the beginning is equal to the value)
     */
    private String unitOfMeasureIdLikeFront;

    /**
     * Unit of Measure Name(condition whether the column value at the beginning is equal to the value)
     */
    private String unitOfMeasureNameLikeFront;

    /**
     * 0 : Not display decimal point (On ASN Maintenance screen)
1 : Display decimal point (On ASN Maintenance screen)(condition whether the column value at the beginning is equal to the value)
     */
    private String displayDecimalPointFlgLikeFront;

    /**
     * 0 : Disable to input actual Shipping Box Qty
1 : Enable to input actual Shipping Box Qty(condition whether the column value at the beginning is equal to the value)
     */
    private String editShippingBoxFlgLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMUnitOfMeasureCriteriaDomain() {
    }

    /**
     * Getter method of "unitOfMeasureId".
     * 
     * @return the "unitOfMeasureId"
     */
    public String getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    /**
     * Setter method of "unitOfMeasureId".
     * 
     * @param unitOfMeasureId Set in "unitOfMeasureId".
     */
    public void setUnitOfMeasureId(String unitOfMeasureId) {
        this.unitOfMeasureId = unitOfMeasureId;
    }

    /**
     * Getter method of "unitOfMeasureName".
     * 
     * @return the "unitOfMeasureName"
     */
    public String getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    /**
     * Setter method of "unitOfMeasureName".
     * 
     * @param unitOfMeasureName Set in "unitOfMeasureName".
     */
    public void setUnitOfMeasureName(String unitOfMeasureName) {
        this.unitOfMeasureName = unitOfMeasureName;
    }

    /**
     * Getter method of "displayDecimalPointFlg".
     * 
     * @return the "displayDecimalPointFlg"
     */
    public String getDisplayDecimalPointFlg() {
        return displayDecimalPointFlg;
    }

    /**
     * Setter method of "displayDecimalPointFlg".
     * 
     * @param displayDecimalPointFlg Set in "displayDecimalPointFlg".
     */
    public void setDisplayDecimalPointFlg(String displayDecimalPointFlg) {
        this.displayDecimalPointFlg = displayDecimalPointFlg;
    }

    /**
     * Getter method of "editShippingBoxFlg".
     * 
     * @return the "editShippingBoxFlg"
     */
    public String getEditShippingBoxFlg() {
        return editShippingBoxFlg;
    }

    /**
     * Setter method of "editShippingBoxFlg".
     * 
     * @param editShippingBoxFlg Set in "editShippingBoxFlg".
     */
    public void setEditShippingBoxFlg(String editShippingBoxFlg) {
        this.editShippingBoxFlg = editShippingBoxFlg;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "unitOfMeasureIdLikeFront".
     * 
     * @return the "unitOfMeasureIdLikeFront"
     */
    public String getUnitOfMeasureIdLikeFront() {
        return unitOfMeasureIdLikeFront;
    }

    /**
     * Setter method of "unitOfMeasureIdLikeFront".
     * 
     * @param unitOfMeasureIdLikeFront Set in "unitOfMeasureIdLikeFront".
     */
    public void setUnitOfMeasureIdLikeFront(String unitOfMeasureIdLikeFront) {
        this.unitOfMeasureIdLikeFront = unitOfMeasureIdLikeFront;
    }

    /**
     * Getter method of "unitOfMeasureNameLikeFront".
     * 
     * @return the "unitOfMeasureNameLikeFront"
     */
    public String getUnitOfMeasureNameLikeFront() {
        return unitOfMeasureNameLikeFront;
    }

    /**
     * Setter method of "unitOfMeasureNameLikeFront".
     * 
     * @param unitOfMeasureNameLikeFront Set in "unitOfMeasureNameLikeFront".
     */
    public void setUnitOfMeasureNameLikeFront(String unitOfMeasureNameLikeFront) {
        this.unitOfMeasureNameLikeFront = unitOfMeasureNameLikeFront;
    }

    /**
     * Getter method of "displayDecimalPointFlgLikeFront".
     * 
     * @return the "displayDecimalPointFlgLikeFront"
     */
    public String getDisplayDecimalPointFlgLikeFront() {
        return displayDecimalPointFlgLikeFront;
    }

    /**
     * Setter method of "displayDecimalPointFlgLikeFront".
     * 
     * @param displayDecimalPointFlgLikeFront Set in "displayDecimalPointFlgLikeFront".
     */
    public void setDisplayDecimalPointFlgLikeFront(String displayDecimalPointFlgLikeFront) {
        this.displayDecimalPointFlgLikeFront = displayDecimalPointFlgLikeFront;
    }

    /**
     * Getter method of "editShippingBoxFlgLikeFront".
     * 
     * @return the "editShippingBoxFlgLikeFront"
     */
    public String getEditShippingBoxFlgLikeFront() {
        return editShippingBoxFlgLikeFront;
    }

    /**
     * Setter method of "editShippingBoxFlgLikeFront".
     * 
     * @param editShippingBoxFlgLikeFront Set in "editShippingBoxFlgLikeFront".
     */
    public void setEditShippingBoxFlgLikeFront(String editShippingBoxFlgLikeFront) {
        this.editShippingBoxFlgLikeFront = editShippingBoxFlgLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
