/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


public class SPSTPoHeaderDNDomain   {
	private static final long serialVersionUID = 1L;
	 private String databaseName;
	private BigDecimal poId;
    private String spsPoNo;
    private String cigMaPoNo;
    private String refCigMaPoNo;
    private String poType;
    private String poIssueDate;
    private String poStatus;
    private String vendorCD;
    private String sCD;
    private String sPCD;
    private String dCD;
    private String dPCD;
    private String tm;
    private String periodType;
    private String orderMethod;
    private String forceAckDate;
    private String changeFlg;
    private String pdfOriginalFileId;
    private String pdfChangeFileId;
    private BigDecimal releaseNo;
    private String dueDateFrom;
    private String dueDateTo;
    private String fax;
    private String newTruckRouteFlg;
    private String createDscId;
    private String createDate;
    private String createTime;
    private String lastUpdateDscId;
    private String lastUpdateDate;
    private String lastUpdateTime;
    private String dnDnTrnFlg;
    private Boolean isSuccess;
    private String msgError;
    private List<SPSTPoDetailDNDomain> listPoDet;
    private List<SPSTPoDueDNDomain> listPoDue;
	private BigDecimal cusNo;
	private String shipNo;
	public BigDecimal getPoId() {
		return poId;
	}
	public void setPoId(BigDecimal poId) {
		this.poId = poId;
	}
	public String getSpsPoNo() {
		return spsPoNo;
	}
	public void setSpsPoNo(String spsPoNo) {
		this.spsPoNo = spsPoNo;
	}
	public String getCigMaPoNo() {
		return cigMaPoNo;
	}
	public void setCigMaPoNo(String cigMaPoNo) {
		this.cigMaPoNo = cigMaPoNo;
	}
	public String getRefCigMaPoNo() {
		return refCigMaPoNo;
	}
	public void setRefCigMaPoNo(String refCigMaPoNo) {
		this.refCigMaPoNo = refCigMaPoNo;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public String getPoIssueDate() {
		return poIssueDate;
	}
	public void setPoIssueDate(String poIssueDate) {
		this.poIssueDate = poIssueDate;
	}
	public String getPoStatus() {
		return poStatus;
	}
	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}
	public String getVendorCD() {
		return vendorCD;
	}
	public void setVendorCD(String vendorCD) {
		this.vendorCD = vendorCD;
	}
	public String getsCD() {
		return sCD;
	}
	public void setsCD(String sCD) {
		this.sCD = sCD;
	}
	public String getsPCD() {
		return sPCD;
	}
	public void setsPCD(String sPCD) {
		this.sPCD = sPCD;
	}
	public String getdCD() {
		return dCD;
	}
	public void setdCD(String dCD) {
		this.dCD = dCD;
	}
	public String getdPCD() {
		return dPCD;
	}
	public void setdPCD(String dPCD) {
		this.dPCD = dPCD;
	}
	public String getTm() {
		return tm;
	}
	public void setTm(String tm) {
		this.tm = tm;
	}
	public String getPeriodType() {
		return periodType;
	}
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	public String getOrderMethod() {
		return orderMethod;
	}
	public void setOrderMethod(String orderMethod) {
		this.orderMethod = orderMethod;
	}
	public String getForceAckDate() {
		return forceAckDate;
	}
	public void setForceAckDate(String forceAckDate) {
		this.forceAckDate = forceAckDate;
	}
	public String getChangeFlg() {
		return changeFlg;
	}
	public void setChangeFlg(String changeFlg) {
		this.changeFlg = changeFlg;
	}
	public String getPdfOriginalFileId() {
		return pdfOriginalFileId;
	}
	public void setPdfOriginalFileId(String pdfOriginalFileId) {
		this.pdfOriginalFileId = pdfOriginalFileId;
	}
	public String getPdfChangeFileId() {
		return pdfChangeFileId;
	}
	public void setPdfChangeFileId(String pdfChangeFileId) {
		this.pdfChangeFileId = pdfChangeFileId;
	}
	public BigDecimal getReleaseNo() {
		return releaseNo;
	}
	public void setReleaseNo(BigDecimal releaseNo) {
		this.releaseNo = releaseNo;
	}
	public String getDueDateFrom() {
		return dueDateFrom;
	}
	public void setDueDateFrom(String dueDateFrom) {
		this.dueDateFrom = dueDateFrom;
	}
	public String getDueDateTo() {
		return dueDateTo;
	}
	public void setDueDateTo(String dueDateTo) {
		this.dueDateTo = dueDateTo;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getNewTruckRouteFlg() {
		return newTruckRouteFlg;
	}
	public void setNewTruckRouteFlg(String newTruckRouteFlg) {
		this.newTruckRouteFlg = newTruckRouteFlg;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<SPSTPoDetailDNDomain> getListPoDet() {
		return listPoDet;
	}
	public void setListPoDet(List<SPSTPoDetailDNDomain> listPoDet) {
		this.listPoDet = listPoDet;
	}
	public List<SPSTPoDueDNDomain> getListPoDue() {
		return listPoDue;
	}
	public void setListPoDue(List<SPSTPoDueDNDomain> listPoDue) {
		this.listPoDue = listPoDue;
	}
	public String getDnDnTrnFlg() {
		return dnDnTrnFlg;
	}
	public void setDnDnTrnFlg(String dnDnTrnFlg) {
		this.dnDnTrnFlg = dnDnTrnFlg;
	}
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public BigDecimal getCusNo() {
		return cusNo;
	}
	public void setCusNo(BigDecimal cusNo) {
		this.cusNo = cusNo;
	}
	public String getShipNo() {
		return shipNo;
	}
	public void setShipNo(String shipNo) {
		this.shipNo = shipNo;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	
}
