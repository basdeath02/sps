/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMUserDenso".<br />
 * Table overview: SPS_M_USER_DENSO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 11:25:05<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserDensoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User identify number
     */
    private String dscId;

    /**
     * User belong to DENSO company
     */
    private String dCd;

    /**
     * User belong to DENSO plant
     */
    private String dPcd;

    /**
     * User employee number
     */
    private String employeeCd;

    /**
     * Create ASN (Shot Shipment) and Cancel ASN, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlCreateCancelAsnFlag;

    /**
     * Create Invoice with CN or Use Temp Price, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlCreateInvoiceFlag;

    /**
     * Abnormal Transfer data(Pending P/O, D/O aleady create ASN, Supplier Information not found), 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlPedpoDoalcasnUnmatpnFlg;

    /**
     * Revise Shipping QTY, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlReviseAsnFlag;

    /**
     * Other case of abnormal Transfer data, 0: Determined not to receive this email topic. 1: Determined to get this email topic.
     */
    private String emlAbnormalTransferFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * User identify number(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdLikeFront;

    /**
     * User belong to DENSO company(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * User belong to DENSO plant(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * User employee number(condition whether the column value at the beginning is equal to the value)
     */
    private String employeeCdLikeFront;

    /**
     * Create ASN (Shot Shipment) and Cancel ASN, 0: Determined not to receive this email topic. 1: Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlCreateCancelAsnFlagLikeFront;

    /**
     * Create Invoice with CN or Use Temp Price, 0: Determined not to receive this email topic. 1: Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlCreateInvoiceFlagLikeFront;

    /**
     * Abnormal Transfer data(Pending P/O, D/O aleady create ASN, Supplier Information not found), 0: Determined not to receive this email topic. 1: Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlPedpoDoalcasnUnmatpnFlgLikeFront;

    /**
     * Revise Shipping QTY, 0: Determined not to receive this email topic. 1: Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlReviseAsnFlagLikeFront;

    /**
     * Other case of abnormal Transfer data, 0: Determined not to receive this email topic. 1: Determined to get this email topic.(condition whether the column value at the beginning is equal to the value)
     */
    private String emlAbnormalTransferFlgLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMUserDensoCriteriaDomain() {
    }

    /**
     * Getter method of "dscId".
     * 
     * @return the "dscId"
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId".
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "employeeCd".
     * 
     * @return the "employeeCd"
     */
    public String getEmployeeCd() {
        return employeeCd;
    }

    /**
     * Setter method of "employeeCd".
     * 
     * @param employeeCd Set in "employeeCd".
     */
    public void setEmployeeCd(String employeeCd) {
        this.employeeCd = employeeCd;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlag".
     * 
     * @return the "emlCreateCancelAsnFlag"
     */
    public String getEmlCreateCancelAsnFlag() {
        return emlCreateCancelAsnFlag;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlag".
     * 
     * @param emlCreateCancelAsnFlag Set in "emlCreateCancelAsnFlag".
     */
    public void setEmlCreateCancelAsnFlag(String emlCreateCancelAsnFlag) {
        this.emlCreateCancelAsnFlag = emlCreateCancelAsnFlag;
    }

    /**
     * Getter method of "emlCreateInvoiceFlag".
     * 
     * @return the "emlCreateInvoiceFlag"
     */
    public String getEmlCreateInvoiceFlag() {
        return emlCreateInvoiceFlag;
    }

    /**
     * Setter method of "emlCreateInvoiceFlag".
     * 
     * @param emlCreateInvoiceFlag Set in "emlCreateInvoiceFlag".
     */
    public void setEmlCreateInvoiceFlag(String emlCreateInvoiceFlag) {
        this.emlCreateInvoiceFlag = emlCreateInvoiceFlag;
    }

    /**
     * Getter method of "emlPedpoDoalcasnUnmatpnFlg".
     * 
     * @return the "emlPedpoDoalcasnUnmatpnFlg"
     */
    public String getEmlPedpoDoalcasnUnmatpnFlg() {
        return emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Setter method of "emlPedpoDoalcasnUnmatpnFlg".
     * 
     * @param emlPedpoDoalcasnUnmatpnFlg Set in "emlPedpoDoalcasnUnmatpnFlg".
     */
    public void setEmlPedpoDoalcasnUnmatpnFlg(String emlPedpoDoalcasnUnmatpnFlg) {
        this.emlPedpoDoalcasnUnmatpnFlg = emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Getter method of "emlReviseAsnFlag".
     * 
     * @return the "emlReviseAsnFlag"
     */
    public String getEmlReviseAsnFlag() {
        return emlReviseAsnFlag;
    }

    /**
     * Setter method of "emlReviseAsnFlag".
     * 
     * @param emlReviseAsnFlag Set in "emlReviseAsnFlag".
     */
    public void setEmlReviseAsnFlag(String emlReviseAsnFlag) {
        this.emlReviseAsnFlag = emlReviseAsnFlag;
    }

    /**
     * Getter method of "emlAbnormalTransferFlg".
     * 
     * @return the "emlAbnormalTransferFlg"
     */
    public String getEmlAbnormalTransferFlg() {
        return emlAbnormalTransferFlg;
    }

    /**
     * Setter method of "emlAbnormalTransferFlg".
     * 
     * @param emlAbnormalTransferFlg Set in "emlAbnormalTransferFlg".
     */
    public void setEmlAbnormalTransferFlg(String emlAbnormalTransferFlg) {
        this.emlAbnormalTransferFlg = emlAbnormalTransferFlg;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dscIdLikeFront".
     * 
     * @return the "dscIdLikeFront"
     */
    public String getDscIdLikeFront() {
        return dscIdLikeFront;
    }

    /**
     * Setter method of "dscIdLikeFront".
     * 
     * @param dscIdLikeFront Set in "dscIdLikeFront".
     */
    public void setDscIdLikeFront(String dscIdLikeFront) {
        this.dscIdLikeFront = dscIdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "employeeCdLikeFront".
     * 
     * @return the "employeeCdLikeFront"
     */
    public String getEmployeeCdLikeFront() {
        return employeeCdLikeFront;
    }

    /**
     * Setter method of "employeeCdLikeFront".
     * 
     * @param employeeCdLikeFront Set in "employeeCdLikeFront".
     */
    public void setEmployeeCdLikeFront(String employeeCdLikeFront) {
        this.employeeCdLikeFront = employeeCdLikeFront;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlagLikeFront".
     * 
     * @return the "emlCreateCancelAsnFlagLikeFront"
     */
    public String getEmlCreateCancelAsnFlagLikeFront() {
        return emlCreateCancelAsnFlagLikeFront;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlagLikeFront".
     * 
     * @param emlCreateCancelAsnFlagLikeFront Set in "emlCreateCancelAsnFlagLikeFront".
     */
    public void setEmlCreateCancelAsnFlagLikeFront(String emlCreateCancelAsnFlagLikeFront) {
        this.emlCreateCancelAsnFlagLikeFront = emlCreateCancelAsnFlagLikeFront;
    }

    /**
     * Getter method of "emlCreateInvoiceFlagLikeFront".
     * 
     * @return the "emlCreateInvoiceFlagLikeFront"
     */
    public String getEmlCreateInvoiceFlagLikeFront() {
        return emlCreateInvoiceFlagLikeFront;
    }

    /**
     * Setter method of "emlCreateInvoiceFlagLikeFront".
     * 
     * @param emlCreateInvoiceFlagLikeFront Set in "emlCreateInvoiceFlagLikeFront".
     */
    public void setEmlCreateInvoiceFlagLikeFront(String emlCreateInvoiceFlagLikeFront) {
        this.emlCreateInvoiceFlagLikeFront = emlCreateInvoiceFlagLikeFront;
    }

    /**
     * Getter method of "emlPedpoDoalcasnUnmatpnFlgLikeFront".
     * 
     * @return the "emlPedpoDoalcasnUnmatpnFlgLikeFront"
     */
    public String getEmlPedpoDoalcasnUnmatpnFlgLikeFront() {
        return emlPedpoDoalcasnUnmatpnFlgLikeFront;
    }

    /**
     * Setter method of "emlPedpoDoalcasnUnmatpnFlgLikeFront".
     * 
     * @param emlPedpoDoalcasnUnmatpnFlgLikeFront Set in "emlPedpoDoalcasnUnmatpnFlgLikeFront".
     */
    public void setEmlPedpoDoalcasnUnmatpnFlgLikeFront(String emlPedpoDoalcasnUnmatpnFlgLikeFront) {
        this.emlPedpoDoalcasnUnmatpnFlgLikeFront = emlPedpoDoalcasnUnmatpnFlgLikeFront;
    }

    /**
     * Getter method of "emlReviseAsnFlagLikeFront".
     * 
     * @return the "emlReviseAsnFlagLikeFront"
     */
    public String getEmlReviseAsnFlagLikeFront() {
        return emlReviseAsnFlagLikeFront;
    }

    /**
     * Setter method of "emlReviseAsnFlagLikeFront".
     * 
     * @param emlReviseAsnFlagLikeFront Set in "emlReviseAsnFlagLikeFront".
     */
    public void setEmlReviseAsnFlagLikeFront(String emlReviseAsnFlagLikeFront) {
        this.emlReviseAsnFlagLikeFront = emlReviseAsnFlagLikeFront;
    }

    /**
     * Getter method of "emlAbnormalTransferFlgLikeFront".
     * 
     * @return the "emlAbnormalTransferFlgLikeFront"
     */
    public String getEmlAbnormalTransferFlgLikeFront() {
        return emlAbnormalTransferFlgLikeFront;
    }

    /**
     * Setter method of "emlAbnormalTransferFlgLikeFront".
     * 
     * @param emlAbnormalTransferFlgLikeFront Set in "emlAbnormalTransferFlgLikeFront".
     */
    public void setEmlAbnormalTransferFlgLikeFront(String emlAbnormalTransferFlgLikeFront) {
        this.emlAbnormalTransferFlgLikeFront = emlAbnormalTransferFlgLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
