/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/12       CSI                             New
 * 2.0.0      2018/01/10       Netband U. Rungsiwut            SPS Phase II
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTDo"<br />
 * Table overview: SPS_T_DO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/12 14:21:17<br />
 * 
 * This module generated automatically in 2015/03/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No. of Delivery Order
     */
    private BigDecimal doId;

    /**
     * SPS D/O No
     */
    private String spsDoNo;

    /**
     * CIGMA D/O No.
     */
    private String cigmaDoNo;

    /**
     * Current SPS D/O No.
     */
    private String currentSpsDoNo;

    /**
     * Current CIGMA D/O No.
     */
    private String currentCigmaDoNo;

    /**
     * Previous SPS D/O No.
     */
    private String previousSpsDoNo;

    /**
     * Previous CIGMA D/O No.
     */
    private String previousCigmaDoNo;

    /**
     * D/O Revision
     */
    private String revision;

    /**
     * D/O Issue Date
     */
    private Date doIssueDate;

    /**
     * ISS : Issued
PTS : Partial-Ship
CPS : Complete-Ship
     */
    private String shipmentStatus;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Transportation Mode
0 : DENSO Truck
1 : Supplier Truck
     */
    private String tm;

    /**
     * Data Type from CIGMA
     */
    private String dataType;

    /**
     * Order Method
0 : Fixed Order
1 : KANBAN Order
     */
    private String orderMethod;

    /**
     * D/O Ship Datetime
     */
    private Timestamp shipDatetime;

    /**
     * D/O Delivery Datetime
     */
    private Timestamp deliveryDatetime;

    /**
     * File Id which identify D/O Report PDF file are saved in file manager table
     */
    private String pdfFileId;

    /**
     * Route
     */
    private String truckRoute;

    /**
     * Run No.
     */
    private BigDecimal truckSeq;

    /**
     * Referenc PO ID
     */
    private BigDecimal poId;

    /**
     * Warehouse Prime Receiving
     */
    private String whPrimeReceiving;

    /**
     * Flag to inform D/O is latest version or not
0 : Not Latest, 1 : Latest
     */
    private String latestRevisionFlg;

    /**
     * Flag to inform D/O is urgent or not
0 : Not Urgent, 1 : Urgent
     */
    private String urgentOrderFlg;

    /**
     * Clear Urgent Order Date
     */
    private Date clearUrgentDate;

    /**
     * Add Truck Route Flag
0 : None
1 : Add New Truck Route
     */
    private String newTruckRouteFlg;

    /**
     * Receiving Dock
     */
    private String dockCode;

    /**
     * Cycle
     */
    private String cycle;

    /**
     * Trip No
     */
    private BigDecimal tripNo;

    /**
     * Backlog
     */
    private BigDecimal backlog;

    /**
     * Receiving Gate
     */
    private String receivingGate;

    /**
     * Supplier Location
     */
    private String supplierLocation;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;
    
    /**
     * <p>
     * The pdfFileKbTagPrintDate
     * </p>
     */
    private String pdfFileKbTagPrintDate; 
    
    /** 
     * The receiveByScan. 
     */
    private String receiveByScan;    
    
    /** 
     * The isPrintOneWayKanbanTag. 
     */
    private String isPrintOneWayKanbanTag;    
    
    /** 
     * The oneWayKanbanTagFlag. 
     */
    private String oneWayKanbanTagFlag;
    
    /** 
     * The notificationMailDoFlag.
     * Flag to inform D/O is sent notification email.
     * 0 : Not sent
     * 1 : Sent 
     */
    private String notificationMailDoFlag;
    
    /** 
     * The notificationMailKanbanFlag.
     * Flag to inform One-way kanban tag of D/O is sent notification email.
     * 0 : Not sent
     * 1 : Sent 
     */
    private String notificationMailKanbanFlag;    
    
    /** 
     * The tagOutput. 
     */
    private String tagOutput; 

    /**
     * Default constructor
     */
    public SpsTDoDomain() {
    }

    /**
     * Getter method of "doId"
     * 
     * @return the doId
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId"
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "spsDoNo"
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo"
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "cigmaDoNo"
     * 
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo"
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "currentSpsDoNo"
     * 
     * @return the currentSpsDoNo
     */
    public String getCurrentSpsDoNo() {
        return currentSpsDoNo;
    }

    /**
     * Setter method of "currentSpsDoNo"
     * 
     * @param currentSpsDoNo Set in "currentSpsDoNo".
     */
    public void setCurrentSpsDoNo(String currentSpsDoNo) {
        this.currentSpsDoNo = currentSpsDoNo;
    }

    /**
     * Getter method of "currentCigmaDoNo"
     * 
     * @return the currentCigmaDoNo
     */
    public String getCurrentCigmaDoNo() {
        return currentCigmaDoNo;
    }

    /**
     * Setter method of "currentCigmaDoNo"
     * 
     * @param currentCigmaDoNo Set in "currentCigmaDoNo".
     */
    public void setCurrentCigmaDoNo(String currentCigmaDoNo) {
        this.currentCigmaDoNo = currentCigmaDoNo;
    }

    /**
     * Getter method of "previousSpsDoNo"
     * 
     * @return the previousSpsDoNo
     */
    public String getPreviousSpsDoNo() {
        return previousSpsDoNo;
    }

    /**
     * Setter method of "previousSpsDoNo"
     * 
     * @param previousSpsDoNo Set in "previousSpsDoNo".
     */
    public void setPreviousSpsDoNo(String previousSpsDoNo) {
        this.previousSpsDoNo = previousSpsDoNo;
    }

    /**
     * Getter method of "previousCigmaDoNo"
     * 
     * @return the previousCigmaDoNo
     */
    public String getPreviousCigmaDoNo() {
        return previousCigmaDoNo;
    }

    /**
     * Setter method of "previousCigmaDoNo"
     * 
     * @param previousCigmaDoNo Set in "previousCigmaDoNo".
     */
    public void setPreviousCigmaDoNo(String previousCigmaDoNo) {
        this.previousCigmaDoNo = previousCigmaDoNo;
    }

    /**
     * Getter method of "revision"
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision"
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "doIssueDate"
     * 
     * @return the doIssueDate
     */
    public Date getDoIssueDate() {
        return doIssueDate;
    }

    /**
     * Setter method of "doIssueDate"
     * 
     * @param doIssueDate Set in "doIssueDate".
     */
    public void setDoIssueDate(Date doIssueDate) {
        this.doIssueDate = doIssueDate;
    }

    /**
     * Getter method of "shipmentStatus"
     * 
     * @return the shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * Setter method of "shipmentStatus"
     * 
     * @param shipmentStatus Set in "shipmentStatus".
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "tm"
     * 
     * @return the tm
     */
    public String getTm() {
        return tm;
    }

    /**
     * Setter method of "tm"
     * 
     * @param tm Set in "tm".
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * Getter method of "dataType"
     * 
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType"
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "orderMethod"
     * 
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Setter method of "orderMethod"
     * 
     * @param orderMethod Set in "orderMethod".
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Getter method of "shipDatetime"
     * 
     * @return the shipDatetime
     */
    public Timestamp getShipDatetime() {
        return shipDatetime;
    }

    /**
     * Setter method of "shipDatetime"
     * 
     * @param shipDatetime Set in "shipDatetime".
     */
    public void setShipDatetime(Timestamp shipDatetime) {
        this.shipDatetime = shipDatetime;
    }

    /**
     * Getter method of "deliveryDatetime"
     * 
     * @return the deliveryDatetime
     */
    public Timestamp getDeliveryDatetime() {
        return deliveryDatetime;
    }

    /**
     * Setter method of "deliveryDatetime"
     * 
     * @param deliveryDatetime Set in "deliveryDatetime".
     */
    public void setDeliveryDatetime(Timestamp deliveryDatetime) {
        this.deliveryDatetime = deliveryDatetime;
    }

    /**
     * Getter method of "pdfFileId"
     * 
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Setter method of "pdfFileId"
     * 
     * @param pdfFileId Set in "pdfFileId".
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Getter method of "truckRoute"
     * 
     * @return the truckRoute
     */
    public String getTruckRoute() {
        return truckRoute;
    }

    /**
     * Setter method of "truckRoute"
     * 
     * @param truckRoute Set in "truckRoute".
     */
    public void setTruckRoute(String truckRoute) {
        this.truckRoute = truckRoute;
    }

    /**
     * Getter method of "truckSeq"
     * 
     * @return the truckSeq
     */
    public BigDecimal getTruckSeq() {
        return truckSeq;
    }

    /**
     * Setter method of "truckSeq"
     * 
     * @param truckSeq Set in "truckSeq".
     */
    public void setTruckSeq(BigDecimal truckSeq) {
        this.truckSeq = truckSeq;
    }

    /**
     * Getter method of "poId"
     * 
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId"
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "whPrimeReceiving"
     * 
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * Setter method of "whPrimeReceiving"
     * 
     * @param whPrimeReceiving Set in "whPrimeReceiving".
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * Getter method of "latestRevisionFlg"
     * 
     * @return the latestRevisionFlg
     */
    public String getLatestRevisionFlg() {
        return latestRevisionFlg;
    }

    /**
     * Setter method of "latestRevisionFlg"
     * 
     * @param latestRevisionFlg Set in "latestRevisionFlg".
     */
    public void setLatestRevisionFlg(String latestRevisionFlg) {
        this.latestRevisionFlg = latestRevisionFlg;
    }

    /**
     * Getter method of "urgentOrderFlg"
     * 
     * @return the urgentOrderFlg
     */
    public String getUrgentOrderFlg() {
        return urgentOrderFlg;
    }

    /**
     * Setter method of "urgentOrderFlg"
     * 
     * @param urgentOrderFlg Set in "urgentOrderFlg".
     */
    public void setUrgentOrderFlg(String urgentOrderFlg) {
        this.urgentOrderFlg = urgentOrderFlg;
    }

    /**
     * Getter method of "clearUrgentDate"
     * 
     * @return the clearUrgentDate
     */
    public Date getClearUrgentDate() {
        return clearUrgentDate;
    }

    /**
     * Setter method of "clearUrgentDate"
     * 
     * @param clearUrgentDate Set in "clearUrgentDate".
     */
    public void setClearUrgentDate(Date clearUrgentDate) {
        this.clearUrgentDate = clearUrgentDate;
    }

    /**
     * Getter method of "newTruckRouteFlg"
     * 
     * @return the newTruckRouteFlg
     */
    public String getNewTruckRouteFlg() {
        return newTruckRouteFlg;
    }

    /**
     * Setter method of "newTruckRouteFlg"
     * 
     * @param newTruckRouteFlg Set in "newTruckRouteFlg".
     */
    public void setNewTruckRouteFlg(String newTruckRouteFlg) {
        this.newTruckRouteFlg = newTruckRouteFlg;
    }

    /**
     * Getter method of "dockCode"
     * 
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * Setter method of "dockCode"
     * 
     * @param dockCode Set in "dockCode".
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * Getter method of "cycle"
     * 
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * Setter method of "cycle"
     * 
     * @param cycle Set in "cycle".
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * Getter method of "tripNo"
     * 
     * @return the tripNo
     */
    public BigDecimal getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo"
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(BigDecimal tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "backlog"
     * 
     * @return the backlog
     */
    public BigDecimal getBacklog() {
        return backlog;
    }

    /**
     * Setter method of "backlog"
     * 
     * @param backlog Set in "backlog".
     */
    public void setBacklog(BigDecimal backlog) {
        this.backlog = backlog;
    }

    /**
     * Getter method of "receivingGate"
     * 
     * @return the receivingGate
     */
    public String getReceivingGate() {
        return receivingGate;
    }

    /**
     * Setter method of "receivingGate"
     * 
     * @param receivingGate Set in "receivingGate".
     */
    public void setReceivingGate(String receivingGate) {
        this.receivingGate = receivingGate;
    }

    /**
     * Getter method of "supplierLocation"
     * 
     * @return the supplierLocation
     */
    public String getSupplierLocation() {
        return supplierLocation;
    }

    /**
     * Setter method of "supplierLocation"
     * 
     * @param supplierLocation Set in "supplierLocation".
     */
    public void setSupplierLocation(String supplierLocation) {
        this.supplierLocation = supplierLocation;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * <p>
     * Getter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @return The pdfFileKbTagPrintDate
     */
    public String getPdfFileKbTagPrintDate() {
        return pdfFileKbTagPrintDate;
    }

    /**
     * <p>
     * Setter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @param pdfFileKbTagPrintDate Set for pdfFileKbTagPrintDate
     */
    public void setPdfFileKbTagPrintDate(String pdfFileKbTagPrintDate) {
        this.pdfFileKbTagPrintDate = pdfFileKbTagPrintDate;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    /**
     * Gets the isPrintOneWayKanbanTag.
     * 
     * @return the isPrintOneWayKanbanTag
     */
    public String getIsPrintOneWayKanbanTag() {
        return isPrintOneWayKanbanTag;
    }

    /**
     * Sets the isPrintOneWayKanbanTag.
     * 
     * @param isPrintOneWayKanbanTag the isPrintOneWayKanbanTag
     */
    public void setIsPrintOneWayKanbanTag(String isPrintOneWayKanbanTag) {
        this.isPrintOneWayKanbanTag = isPrintOneWayKanbanTag;
    }
    /**
     * Gets the oneWayKanbanTagFlag.
     * 
     * @return the oneWayKanbanTagFlag
     */

    public String getOneWayKanbanTagFlag() {
        return oneWayKanbanTagFlag;
    }

    /**
     * Sets the oneWayKanbanTagFlag.
     * 
     * @param oneWayKanbanTagFlag the oneWayKanbanTagFlag
     */

    public void setOneWayKanbanTagFlag(String oneWayKanbanTagFlag) {
        this.oneWayKanbanTagFlag = oneWayKanbanTagFlag;
    }
    /**
     * Gets the notificationMailDoFlag.
     * 
     * @return the notificationMailDoFlag
     */

    public String getNotificationMailDoFlag() {
        return notificationMailDoFlag;
    }
    /**
     * Sets the notificationMailDoFlag.
     * 
     * @param notificationMailDoFlag the notificationMailDoFlag
     */

    public void setNotificationMailDoFlag(String notificationMailDoFlag) {
        this.notificationMailDoFlag = notificationMailDoFlag;
    }
    /**
     * Gets the notificationMailKanbanFlag.
     * 
     * @return the notificationMailKanbanFlag
     */

    public String getNotificationMailKanbanFlag() {
        return notificationMailKanbanFlag;
    }
    /**
     * Sets the notificationMailKanbanFlag.
     * 
     * @param notificationMailKanbanFlag the notificationMailKanbanFlag
     */

    public void setNotificationMailKanbanFlag(String notificationMailKanbanFlag) {
        this.notificationMailKanbanFlag = notificationMailKanbanFlag;
    }

    /**
     * <p>Getter method for tagOutput.</p>
     *
     * @return the tagOutput
     */
    public String getTagOutput() {
        return tagOutput;
    }

    /**
     * <p>Setter method for tagOutput.</p>
     *
     * @param tagOutput Set for tagOutput
     */
    public void setTagOutput(String tagOutput) {
        this.tagOutput = tagOutput;
    }

}
