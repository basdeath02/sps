/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


public class SPSTPoDueDNDomain   {
	private static final long serialVersionUID = 1L;
	 private String databaseName;
	private BigDecimal poId;
	private String sPn;
	private String dPn;
	private BigDecimal dueId;
	private String dueDate;
	private String reportTypeFlg;
	private BigDecimal orderQty;
	private String etd;
	private String del;
	private BigDecimal seq;
	private BigDecimal previousQty;
	private BigDecimal differenceQty;
	private String rsn;
	private String spsProposedDueDate;
	private BigDecimal spsProposedQty;
	private String spsPendingReasonCd;
	private String markPendingFlg;
	private String changeFlg;
	private String createDscId;
	private String createDate;
	private String createTime;
	private String lastUpdateDscId;
	private String lastUpdateDate;
	private String lastUpdateTime;
	private String densoReplyFlg;
	public BigDecimal getPoId() {
		return poId;
	}
	public void setPoId(BigDecimal poId) {
		this.poId = poId;
	}
	public String getsPn() {
		return sPn;
	}
	public void setsPn(String sPn) {
		this.sPn = sPn;
	}
	public String getdPn() {
		return dPn;
	}
	public void setdPn(String dPn) {
		this.dPn = dPn;
	}
	public BigDecimal getDueId() {
		return dueId;
	}
	public void setDueId(BigDecimal dueId) {
		this.dueId = dueId;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getReportTypeFlg() {
		return reportTypeFlg;
	}
	public void setReportTypeFlg(String reportTypeFlg) {
		this.reportTypeFlg = reportTypeFlg;
	}
	public BigDecimal getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}
	public String getEtd() {
		return etd;
	}
	public void setEtd(String etd) {
		this.etd = etd;
	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
	public BigDecimal getSeq() {
		return seq;
	}
	public void setSeq(BigDecimal seq) {
		this.seq = seq;
	}
	public BigDecimal getPreviousQty() {
		return previousQty;
	}
	public void setPreviousQty(BigDecimal previousQty) {
		this.previousQty = previousQty;
	}
	public BigDecimal getDifferenceQty() {
		return differenceQty;
	}
	public void setDifferenceQty(BigDecimal differenceQty) {
		this.differenceQty = differenceQty;
	}
	public String getRsn() {
		return rsn;
	}
	public void setRsn(String rsn) {
		this.rsn = rsn;
	}
	public String getSpsProposedDueDate() {
		return spsProposedDueDate;
	}
	public void setSpsProposedDueDate(String spsProposedDueDate) {
		this.spsProposedDueDate = spsProposedDueDate;
	}
	public BigDecimal getSpsProposedQty() {
		return spsProposedQty;
	}
	public void setSpsProposedQty(BigDecimal spsProposedQty) {
		this.spsProposedQty = spsProposedQty;
	}
	public String getSpsPendingReasonCd() {
		return spsPendingReasonCd;
	}
	public void setSpsPendingReasonCd(String spsPendingReasonCd) {
		this.spsPendingReasonCd = spsPendingReasonCd;
	}
	public String getMarkPendingFlg() {
		return markPendingFlg;
	}
	public void setMarkPendingFlg(String markPendingFlg) {
		this.markPendingFlg = markPendingFlg;
	}
	public String getChangeFlg() {
		return changeFlg;
	}
	public void setChangeFlg(String changeFlg) {
		this.changeFlg = changeFlg;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getDensoReplyFlg() {
		return densoReplyFlg;
	}
	public void setDensoReplyFlg(String densoReplyFlg) {
		this.densoReplyFlg = densoReplyFlg;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	
	
}
