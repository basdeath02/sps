/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/28       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTCnDetail".<br />
 * Table overview: SPS_T_CN_DETAIL<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/28 10:05:00<br />
 * 
 * This module generated automatically in 2015/01/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTCnDetailCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running number of Credit Note
     */
    private BigDecimal cnId;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * DENSO Part Number
     */
    private String dPn;

    /**
     * Supplier Part Number
     */
    private String sPn;

    /**
     * Supplier Unit Price
     */
    private BigDecimal sPriceUnit;

    /**
     * DENSO Unit Price
     */
    private BigDecimal dPriceUnit;

    /**
     * Difference Amount of Unit Price
     */
    private BigDecimal diffPriceUnit;

    /**
     * Difference Base Amount
     */
    private BigDecimal diffBaseAmt;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Running Number of Delivery Order
     */
    private BigDecimal doId;

    /**
     * ASN Number(condition whether the column value at the beginning is equal to the value)
     */
    private String asnNoLikeFront;

    /**
     * DENSO Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Supplier Part Number(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTCnDetailCriteriaDomain() {
    }

    /**
     * Getter method of "cnId".
     * 
     * @return the "cnId"
     */
    public BigDecimal getCnId() {
        return cnId;
    }

    /**
     * Setter method of "cnId".
     * 
     * @param cnId Set in "cnId".
     */
    public void setCnId(BigDecimal cnId) {
        this.cnId = cnId;
    }

    /**
     * Getter method of "asnNo".
     * 
     * @return the "asnNo"
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo".
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "sPriceUnit".
     * 
     * @return the "sPriceUnit"
     */
    public BigDecimal getSPriceUnit() {
        return sPriceUnit;
    }

    /**
     * Setter method of "sPriceUnit".
     * 
     * @param sPriceUnit Set in "sPriceUnit".
     */
    public void setSPriceUnit(BigDecimal sPriceUnit) {
        this.sPriceUnit = sPriceUnit;
    }

    /**
     * Getter method of "dPriceUnit".
     * 
     * @return the "dPriceUnit"
     */
    public BigDecimal getDPriceUnit() {
        return dPriceUnit;
    }

    /**
     * Setter method of "dPriceUnit".
     * 
     * @param dPriceUnit Set in "dPriceUnit".
     */
    public void setDPriceUnit(BigDecimal dPriceUnit) {
        this.dPriceUnit = dPriceUnit;
    }

    /**
     * Getter method of "diffPriceUnit".
     * 
     * @return the "diffPriceUnit"
     */
    public BigDecimal getDiffPriceUnit() {
        return diffPriceUnit;
    }

    /**
     * Setter method of "diffPriceUnit".
     * 
     * @param diffPriceUnit Set in "diffPriceUnit".
     */
    public void setDiffPriceUnit(BigDecimal diffPriceUnit) {
        this.diffPriceUnit = diffPriceUnit;
    }

    /**
     * Getter method of "diffBaseAmt".
     * 
     * @return the "diffBaseAmt"
     */
    public BigDecimal getDiffBaseAmt() {
        return diffBaseAmt;
    }

    /**
     * Setter method of "diffBaseAmt".
     * 
     * @param diffBaseAmt Set in "diffBaseAmt".
     */
    public void setDiffBaseAmt(BigDecimal diffBaseAmt) {
        this.diffBaseAmt = diffBaseAmt;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "doId".
     * 
     * @return the "doId"
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId".
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "asnNoLikeFront".
     * 
     * @return the "asnNoLikeFront"
     */
    public String getAsnNoLikeFront() {
        return asnNoLikeFront;
    }

    /**
     * Setter method of "asnNoLikeFront".
     * 
     * @param asnNoLikeFront Set in "asnNoLikeFront".
     */
    public void setAsnNoLikeFront(String asnNoLikeFront) {
        this.asnNoLikeFront = asnNoLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
