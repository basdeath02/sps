/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTCn"<br />
 * Table overview: SPS_T_CN<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTCnDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * CN_ID
     */
    private BigDecimal cnId;

    /**
     * INVOICE_ID
     */
    private BigDecimal invoiceId;

    /**
     * CN_NO
     */
    private String cnNo;

    /**
     * CN_STATUS
     */
    private String cnStatus;

    /**
     * CN_DATE
     */
    private Date cnDate;

    /**
     * BASE_AMOUNT
     */
    private BigDecimal baseAmount;

    /**
     * VAT_AMOUNT
     */
    private BigDecimal vatAmount;

    /**
     * TOTAL_AMOUNT
     */
    private BigDecimal totalAmount;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTCnDomain() {
    }

    /**
     * Getter method of "cnId"
     * 
     * @return the cnId
     */
    public BigDecimal getCnId() {
        return cnId;
    }

    /**
     * Setter method of "cnId"
     * 
     * @param cnId Set in "cnId".
     */
    public void setCnId(BigDecimal cnId) {
        this.cnId = cnId;
    }

    /**
     * Getter method of "invoiceId"
     * 
     * @return the invoiceId
     */
    public BigDecimal getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method of "invoiceId"
     * 
     * @param invoiceId Set in "invoiceId".
     */
    public void setInvoiceId(BigDecimal invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * Getter method of "cnNo"
     * 
     * @return the cnNo
     */
    public String getCnNo() {
        return cnNo;
    }

    /**
     * Setter method of "cnNo"
     * 
     * @param cnNo Set in "cnNo".
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }

    /**
     * Getter method of "cnStatus"
     * 
     * @return the cnStatus
     */
    public String getCnStatus() {
        return cnStatus;
    }

    /**
     * Setter method of "cnStatus"
     * 
     * @param cnStatus Set in "cnStatus".
     */
    public void setCnStatus(String cnStatus) {
        this.cnStatus = cnStatus;
    }

    /**
     * Getter method of "cnDate"
     * 
     * @return the cnDate
     */
    public Date getCnDate() {
        return cnDate;
    }

    /**
     * Setter method of "cnDate"
     * 
     * @param cnDate Set in "cnDate".
     */
    public void setCnDate(Date cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * Getter method of "baseAmount"
     * 
     * @return the baseAmount
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /**
     * Setter method of "baseAmount"
     * 
     * @param baseAmount Set in "baseAmount".
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /**
     * Getter method of "vatAmount"
     * 
     * @return the vatAmount
     */
    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    /**
     * Setter method of "vatAmount"
     * 
     * @param vatAmount Set in "vatAmount".
     */
    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    /**
     * Getter method of "totalAmount"
     * 
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Setter method of "totalAmount"
     * 
     * @param totalAmount Set in "totalAmount".
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
