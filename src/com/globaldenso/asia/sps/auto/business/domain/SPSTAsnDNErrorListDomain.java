/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


public class SPSTAsnDNErrorListDomain   {
	private static final long serialVersionUID = 1L;
    private String asnNo;
    private String dCd;
    private String sPN;
    private String errorType;
    private List<String> errorCodeList;
    private String dPN;
    private String spsDoNo;
    
	public String getAsnNo() {
		return asnNo;
	}
	public void setAsnNo(String asnNo) {
		this.asnNo = asnNo;
	}
	public String getdCd() {
		return dCd;
	}
	public void setdCd(String dCd) {
		this.dCd = dCd;
	}
	public String getsPN() {
		return sPN;
	}
	public void setsPN(String sPN) {
		this.sPN = sPN;
	}
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	
	
	public List<String> getErrorCodeList() {
		return errorCodeList;
	}
	public void setErrorCodeList(List<String> errorCodeList) {
		this.errorCodeList = errorCodeList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    /**
     * <p>Getter method for dPN.</p>
     *
     * @return the dPN
     */
    public String getdPN() {
        return dPN;
    }
    /**
     * <p>Setter method for dPN.</p>
     *
     * @param dPN Set for dPN
     */
    public void setdPN(String dPN) {
        this.dPN = dPN;
    }
    /**
     * <p>Getter method for spsDoNo.</p>
     *
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }
    /**
     * <p>Setter method for spsDoNo.</p>
     *
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }
    
}
