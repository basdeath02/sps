/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTmpUserDenso"<br />
 * Table overview: SPS_TMP_USER_DENSO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 12:59:09<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserDensoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User DSC ID that upload data to system
     */
    private String userDscId;

    /**
     * User login session code
     */
    private String sessionCd;

    /**
     * Source file line number
     */
    private BigDecimal lineNo;

    /**
     * User identify number (import data)
     */
    private String dscId;

    /**
     * DENSO company code (import data)
     */
    private String dCd;

    /**
     * DENSO plant code (import data)
     */
    private String dPcd;

    /**
     * DENSO employee code (import data)
     */
    private String employeeCd;

    /**
     * DENSO department name (import data)
     */
    private String departmentCd;

    /**
     * User first name (import data)
     */
    private String firstName;

    /**
     * User middle name (import data)
     */
    private String middleName;

    /**
     * User last name (import data)
     */
    private String lastName;

    /**
     * User email addresses if has email address more than 1 separate each of it by comma ","  (import data)
     */
    private String email;

    /**
     * User telephone number (import data)
     */
    private String telephone;

    /**
     * Create ASN (Shot Shipment) and Cancel ASN, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlCreateCancelAsnFlag;

    /**
     * Revise Shipping QTY, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlReviseAsnFlag;

    /**
     * Create Invoice with CN or Use Temp Price, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlCreateInvoiceFlag;

    /**
     * Abnormal Transfer data(Pending P/O, D/O aleady create ASN, Supplier Information not found), 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlPedpoDoalcasnUnmatpnFlg;

    /**
     * Other case of abnormal Transfer data, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlAbnormalTransferFlg;

    /**
     * User role assignment value  (import data)
     */
    private String roleCd;

    /**
     * Scope of user right on DENSO company  (import data)
     */
    private String roleDCd;

    /**
     * Scope of user right on DENSO plant  (import data)
     */
    private String roleDPcd;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * User role effective start date  (import data)
     */
    private Timestamp effectStart;

    /**
     * User role effective end date  (import data)
     */
    private Timestamp effectEnd;

    /**
     * A : Add, U : Update, D : Delete
     */
    private String informationFlag;

    /**
     * A : Add, U : Update, T : Terminate
     */
    private String roleFlag;

    /**
     * Upload operation timestamp
     */
    private Timestamp uploadDatetime;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete
     */
    private Timestamp userLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_DENSO) in case of Update/Delete
     */
    private Timestamp densoLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete
     */
    private Timestamp roleLastUpdate;

    /**
     * 0 : not move yet, 1 : move to actual table
     */
    private String isActualRegister;

    /**
     * Move data from temporary table to actual table timestamp
     */
    private Timestamp toActualDatetime;

    /**
     * Default constructor
     */
    public SpsTmpUserDensoDomain() {
    }

    /**
     * Getter method of "userDscId"
     * 
     * @return the userDscId
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId"
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd"
     * 
     * @return the sessionCd
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd"
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo"
     * 
     * @return the lineNo
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo"
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dscId"
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId"
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "employeeCd"
     * 
     * @return the employeeCd
     */
    public String getEmployeeCd() {
        return employeeCd;
    }

    /**
     * Setter method of "employeeCd"
     * 
     * @param employeeCd Set in "employeeCd".
     */
    public void setEmployeeCd(String employeeCd) {
        this.employeeCd = employeeCd;
    }

    /**
     * Getter method of "departmentCd"
     * 
     * @return the departmentCd
     */
    public String getDepartmentCd() {
        return departmentCd;
    }

    /**
     * Setter method of "departmentCd"
     * 
     * @param departmentCd Set in "departmentCd".
     */
    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    /**
     * Getter method of "firstName"
     * 
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName"
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName"
     * 
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName"
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName"
     * 
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName"
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email"
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email"
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlag"
     * 
     * @return the emlCreateCancelAsnFlag
     */
    public String getEmlCreateCancelAsnFlag() {
        return emlCreateCancelAsnFlag;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlag"
     * 
     * @param emlCreateCancelAsnFlag Set in "emlCreateCancelAsnFlag".
     */
    public void setEmlCreateCancelAsnFlag(String emlCreateCancelAsnFlag) {
        this.emlCreateCancelAsnFlag = emlCreateCancelAsnFlag;
    }

    /**
     * Getter method of "emlReviseAsnFlag"
     * 
     * @return the emlReviseAsnFlag
     */
    public String getEmlReviseAsnFlag() {
        return emlReviseAsnFlag;
    }

    /**
     * Setter method of "emlReviseAsnFlag"
     * 
     * @param emlReviseAsnFlag Set in "emlReviseAsnFlag".
     */
    public void setEmlReviseAsnFlag(String emlReviseAsnFlag) {
        this.emlReviseAsnFlag = emlReviseAsnFlag;
    }

    /**
     * Getter method of "emlCreateInvoiceFlag"
     * 
     * @return the emlCreateInvoiceFlag
     */
    public String getEmlCreateInvoiceFlag() {
        return emlCreateInvoiceFlag;
    }

    /**
     * Setter method of "emlCreateInvoiceFlag"
     * 
     * @param emlCreateInvoiceFlag Set in "emlCreateInvoiceFlag".
     */
    public void setEmlCreateInvoiceFlag(String emlCreateInvoiceFlag) {
        this.emlCreateInvoiceFlag = emlCreateInvoiceFlag;
    }

    /**
     * Getter method of "emlPedpoDoalcasnUnmatpnFlg"
     * 
     * @return the emlPedpoDoalcasnUnmatpnFlg
     */
    public String getEmlPedpoDoalcasnUnmatpnFlg() {
        return emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Setter method of "emlPedpoDoalcasnUnmatpnFlg"
     * 
     * @param emlPedpoDoalcasnUnmatpnFlg Set in "emlPedpoDoalcasnUnmatpnFlg".
     */
    public void setEmlPedpoDoalcasnUnmatpnFlg(String emlPedpoDoalcasnUnmatpnFlg) {
        this.emlPedpoDoalcasnUnmatpnFlg = emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Getter method of "emlAbnormalTransferFlg"
     * 
     * @return the emlAbnormalTransferFlg
     */
    public String getEmlAbnormalTransferFlg() {
        return emlAbnormalTransferFlg;
    }

    /**
     * Setter method of "emlAbnormalTransferFlg"
     * 
     * @param emlAbnormalTransferFlg Set in "emlAbnormalTransferFlg".
     */
    public void setEmlAbnormalTransferFlg(String emlAbnormalTransferFlg) {
        this.emlAbnormalTransferFlg = emlAbnormalTransferFlg;
    }

    /**
     * Getter method of "roleCd"
     * 
     * @return the roleCd
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Setter method of "roleCd"
     * 
     * @param roleCd Set in "roleCd".
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Getter method of "roleDCd"
     * 
     * @return the roleDCd
     */
    public String getRoleDCd() {
        return roleDCd;
    }

    /**
     * Setter method of "roleDCd"
     * 
     * @param roleDCd Set in "roleDCd".
     */
    public void setRoleDCd(String roleDCd) {
        this.roleDCd = roleDCd;
    }

    /**
     * Getter method of "roleDPcd"
     * 
     * @return the roleDPcd
     */
    public String getRoleDPcd() {
        return roleDPcd;
    }

    /**
     * Setter method of "roleDPcd"
     * 
     * @param roleDPcd Set in "roleDPcd".
     */
    public void setRoleDPcd(String roleDPcd) {
        this.roleDPcd = roleDPcd;
    }

    /**
     * Getter method of "seqNo"
     * 
     * @return the seqNo
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo"
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "effectStart"
     * 
     * @return the effectStart
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart"
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd"
     * 
     * @return the effectEnd
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd"
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "informationFlag"
     * 
     * @return the informationFlag
     */
    public String getInformationFlag() {
        return informationFlag;
    }

    /**
     * Setter method of "informationFlag"
     * 
     * @param informationFlag Set in "informationFlag".
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }

    /**
     * Getter method of "roleFlag"
     * 
     * @return the roleFlag
     */
    public String getRoleFlag() {
        return roleFlag;
    }

    /**
     * Setter method of "roleFlag"
     * 
     * @param roleFlag Set in "roleFlag".
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "userLastUpdate"
     * 
     * @return the userLastUpdate
     */
    public Timestamp getUserLastUpdate() {
        return userLastUpdate;
    }

    /**
     * Setter method of "userLastUpdate"
     * 
     * @param userLastUpdate Set in "userLastUpdate".
     */
    public void setUserLastUpdate(Timestamp userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    /**
     * Getter method of "densoLastUpdate"
     * 
     * @return the densoLastUpdate
     */
    public Timestamp getDensoLastUpdate() {
        return densoLastUpdate;
    }

    /**
     * Setter method of "densoLastUpdate"
     * 
     * @param densoLastUpdate Set in "densoLastUpdate".
     */
    public void setDensoLastUpdate(Timestamp densoLastUpdate) {
        this.densoLastUpdate = densoLastUpdate;
    }

    /**
     * Getter method of "roleLastUpdate"
     * 
     * @return the roleLastUpdate
     */
    public Timestamp getRoleLastUpdate() {
        return roleLastUpdate;
    }

    /**
     * Setter method of "roleLastUpdate"
     * 
     * @param roleLastUpdate Set in "roleLastUpdate".
     */
    public void setRoleLastUpdate(Timestamp roleLastUpdate) {
        this.roleLastUpdate = roleLastUpdate;
    }

    /**
     * Getter method of "isActualRegister"
     * 
     * @return the isActualRegister
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister"
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime"
     * 
     * @return the toActualDatetime
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime"
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

}
