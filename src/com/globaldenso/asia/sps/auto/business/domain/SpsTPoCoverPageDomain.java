/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/11/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTPoCoverPage"<br />
 * Table overview: SPS_T_PO_COVER_PAGE<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/11/28 09:54:32<br />
 * 
 * This module generated automatically in 2014/11/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from SPS_T_PO
     */
    private BigDecimal poId;

    /**
     * Release No
     */
    private BigDecimal releaseNo;

    /**
     * P/O Issue Date
     */
    private Date poIssueDate;

    /**
     * Seller Contract Name
     */
    private String sellerContractName;

    /**
     * Seller Name
     */
    private String sellerName;

    /**
     * Seller Address1
     */
    private String sellerAddress1;

    /**
     * Seller Address2
     */
    private String sellerAddress2;

    /**
     * Seller Address3
     */
    private String sellerAddress3;

    /**
     * Seller Fax Number
     */
    private String sellerFaxNumber;

    /**
     * Payment Term
     */
    private String paymentTerm;

    /**
     * Ship Via
     */
    private String shipVia;

    /**
     * Price Term
     */
    private String priceTerm;

    /**
     * Purchaser Contract Name
     */
    private String purchaseContractName;

    /**
     * Purchaser Name
     */
    private String purchaseName;

    /**
     * Purchaser Address1
     */
    private String purchaseAddress1;

    /**
     * Purchaser Address2
     */
    private String purchaseAddress2;

    /**
     * Purchaser Address3
     */
    private String purchaseAddress3;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTPoCoverPageDomain() {
    }

    /**
     * Getter method of "poId"
     * 
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId"
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "releaseNo"
     * 
     * @return the releaseNo
     */
    public BigDecimal getReleaseNo() {
        return releaseNo;
    }

    /**
     * Setter method of "releaseNo"
     * 
     * @param releaseNo Set in "releaseNo".
     */
    public void setReleaseNo(BigDecimal releaseNo) {
        this.releaseNo = releaseNo;
    }

    /**
     * Getter method of "poIssueDate"
     * 
     * @return the poIssueDate
     */
    public Date getPoIssueDate() {
        return poIssueDate;
    }

    /**
     * Setter method of "poIssueDate"
     * 
     * @param poIssueDate Set in "poIssueDate".
     */
    public void setPoIssueDate(Date poIssueDate) {
        this.poIssueDate = poIssueDate;
    }

    /**
     * Getter method of "sellerContractName"
     * 
     * @return the sellerContractName
     */
    public String getSellerContractName() {
        return sellerContractName;
    }

    /**
     * Setter method of "sellerContractName"
     * 
     * @param sellerContractName Set in "sellerContractName".
     */
    public void setSellerContractName(String sellerContractName) {
        this.sellerContractName = sellerContractName;
    }

    /**
     * Getter method of "sellerName"
     * 
     * @return the sellerName
     */
    public String getSellerName() {
        return sellerName;
    }

    /**
     * Setter method of "sellerName"
     * 
     * @param sellerName Set in "sellerName".
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    /**
     * Getter method of "sellerAddress1"
     * 
     * @return the sellerAddress1
     */
    public String getSellerAddress1() {
        return sellerAddress1;
    }

    /**
     * Setter method of "sellerAddress1"
     * 
     * @param sellerAddress1 Set in "sellerAddress1".
     */
    public void setSellerAddress1(String sellerAddress1) {
        this.sellerAddress1 = sellerAddress1;
    }

    /**
     * Getter method of "sellerAddress2"
     * 
     * @return the sellerAddress2
     */
    public String getSellerAddress2() {
        return sellerAddress2;
    }

    /**
     * Setter method of "sellerAddress2"
     * 
     * @param sellerAddress2 Set in "sellerAddress2".
     */
    public void setSellerAddress2(String sellerAddress2) {
        this.sellerAddress2 = sellerAddress2;
    }

    /**
     * Getter method of "sellerAddress3"
     * 
     * @return the sellerAddress3
     */
    public String getSellerAddress3() {
        return sellerAddress3;
    }

    /**
     * Setter method of "sellerAddress3"
     * 
     * @param sellerAddress3 Set in "sellerAddress3".
     */
    public void setSellerAddress3(String sellerAddress3) {
        this.sellerAddress3 = sellerAddress3;
    }

    /**
     * Getter method of "sellerFaxNumber"
     * 
     * @return the sellerFaxNumber
     */
    public String getSellerFaxNumber() {
        return sellerFaxNumber;
    }

    /**
     * Setter method of "sellerFaxNumber"
     * 
     * @param sellerFaxNumber Set in "sellerFaxNumber".
     */
    public void setSellerFaxNumber(String sellerFaxNumber) {
        this.sellerFaxNumber = sellerFaxNumber;
    }

    /**
     * Getter method of "paymentTerm"
     * 
     * @return the paymentTerm
     */
    public String getPaymentTerm() {
        return paymentTerm;
    }

    /**
     * Setter method of "paymentTerm"
     * 
     * @param paymentTerm Set in "paymentTerm".
     */
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     * Getter method of "shipVia"
     * 
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * Setter method of "shipVia"
     * 
     * @param shipVia Set in "shipVia".
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * Getter method of "priceTerm"
     * 
     * @return the priceTerm
     */
    public String getPriceTerm() {
        return priceTerm;
    }

    /**
     * Setter method of "priceTerm"
     * 
     * @param priceTerm Set in "priceTerm".
     */
    public void setPriceTerm(String priceTerm) {
        this.priceTerm = priceTerm;
    }

    /**
     * Getter method of "purchaseContractName"
     * 
     * @return the purchaseContractName
     */
    public String getPurchaseContractName() {
        return purchaseContractName;
    }

    /**
     * Setter method of "purchaseContractName"
     * 
     * @param purchaseContractName Set in "purchaseContractName".
     */
    public void setPurchaseContractName(String purchaseContractName) {
        this.purchaseContractName = purchaseContractName;
    }

    /**
     * Getter method of "purchaseName"
     * 
     * @return the purchaseName
     */
    public String getPurchaseName() {
        return purchaseName;
    }

    /**
     * Setter method of "purchaseName"
     * 
     * @param purchaseName Set in "purchaseName".
     */
    public void setPurchaseName(String purchaseName) {
        this.purchaseName = purchaseName;
    }

    /**
     * Getter method of "purchaseAddress1"
     * 
     * @return the purchaseAddress1
     */
    public String getPurchaseAddress1() {
        return purchaseAddress1;
    }

    /**
     * Setter method of "purchaseAddress1"
     * 
     * @param purchaseAddress1 Set in "purchaseAddress1".
     */
    public void setPurchaseAddress1(String purchaseAddress1) {
        this.purchaseAddress1 = purchaseAddress1;
    }

    /**
     * Getter method of "purchaseAddress2"
     * 
     * @return the purchaseAddress2
     */
    public String getPurchaseAddress2() {
        return purchaseAddress2;
    }

    /**
     * Setter method of "purchaseAddress2"
     * 
     * @param purchaseAddress2 Set in "purchaseAddress2".
     */
    public void setPurchaseAddress2(String purchaseAddress2) {
        this.purchaseAddress2 = purchaseAddress2;
    }

    /**
     * Getter method of "purchaseAddress3"
     * 
     * @return the purchaseAddress3
     */
    public String getPurchaseAddress3() {
        return purchaseAddress3;
    }

    /**
     * Setter method of "purchaseAddress3"
     * 
     * @param purchaseAddress3 Set in "purchaseAddress3".
     */
    public void setPurchaseAddress3(String purchaseAddress3) {
        this.purchaseAddress3 = purchaseAddress3;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
