/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTDoKanbanSeq"<br />
 * Table overview: SPS_T_DO_KANBAN_SEQ<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/05 13:51:36<br />
 * 
 * This module generated automatically in 2015/02/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoKanbanSeqDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from S_T_DO
     */
    private BigDecimal doId;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * KANBAN Sequence ID
     */
    private BigDecimal kanbanSeqId;

    /**
     * Delivery Run Date
     */
    private Date deliveryRunDate;

    /**
     * Delivery Run No.
     */
    private BigDecimal deliveryRunNo;

    /**
     * KANBAN Sequence No
     */
    private BigDecimal kanbanSeqNo;

    /**
     * KANBAN Control No
     */
    private String kanbanCtrlNo;

    /**
     * KANBAN Lot Size
     */
    private BigDecimal kanbanLotSize;

    /**
     * Temporary KANBAN Tag
     */
    private String tempKanbanTag;

    /**
     * Delivery Due Time
     */
    private BigDecimal deliveryDueTime;

    /**
     * Delivery Run Type
     */
    private String deliveryRunType;

    /**
     * Run Schedule Date
     */
    private Date runScheduleDate;

    /**
     * QR Receive Batch No.
     */
    private BigDecimal qrRcvBatchNo;

    /**
     * QR Scan Time
     */
    private BigDecimal qrScanTime;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTDoKanbanSeqDomain() {
    }

    /**
     * Getter method of "doId"
     * 
     * @return the doId
     */
    public BigDecimal getDoId() {
        return doId;
    }

    /**
     * Setter method of "doId"
     * 
     * @param doId Set in "doId".
     */
    public void setDoId(BigDecimal doId) {
        this.doId = doId;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "kanbanSeqId"
     * 
     * @return the kanbanSeqId
     */
    public BigDecimal getKanbanSeqId() {
        return kanbanSeqId;
    }

    /**
     * Setter method of "kanbanSeqId"
     * 
     * @param kanbanSeqId Set in "kanbanSeqId".
     */
    public void setKanbanSeqId(BigDecimal kanbanSeqId) {
        this.kanbanSeqId = kanbanSeqId;
    }

    /**
     * Getter method of "deliveryRunDate"
     * 
     * @return the deliveryRunDate
     */
    public Date getDeliveryRunDate() {
        return deliveryRunDate;
    }

    /**
     * Setter method of "deliveryRunDate"
     * 
     * @param deliveryRunDate Set in "deliveryRunDate".
     */
    public void setDeliveryRunDate(Date deliveryRunDate) {
        this.deliveryRunDate = deliveryRunDate;
    }

    /**
     * Getter method of "deliveryRunNo"
     * 
     * @return the deliveryRunNo
     */
    public BigDecimal getDeliveryRunNo() {
        return deliveryRunNo;
    }

    /**
     * Setter method of "deliveryRunNo"
     * 
     * @param deliveryRunNo Set in "deliveryRunNo".
     */
    public void setDeliveryRunNo(BigDecimal deliveryRunNo) {
        this.deliveryRunNo = deliveryRunNo;
    }

    /**
     * Getter method of "kanbanSeqNo"
     * 
     * @return the kanbanSeqNo
     */
    public BigDecimal getKanbanSeqNo() {
        return kanbanSeqNo;
    }

    /**
     * Setter method of "kanbanSeqNo"
     * 
     * @param kanbanSeqNo Set in "kanbanSeqNo".
     */
    public void setKanbanSeqNo(BigDecimal kanbanSeqNo) {
        this.kanbanSeqNo = kanbanSeqNo;
    }

    /**
     * Getter method of "kanbanCtrlNo"
     * 
     * @return the kanbanCtrlNo
     */
    public String getKanbanCtrlNo() {
        return kanbanCtrlNo;
    }

    /**
     * Setter method of "kanbanCtrlNo"
     * 
     * @param kanbanCtrlNo Set in "kanbanCtrlNo".
     */
    public void setKanbanCtrlNo(String kanbanCtrlNo) {
        this.kanbanCtrlNo = kanbanCtrlNo;
    }

    /**
     * Getter method of "kanbanLotSize"
     * 
     * @return the kanbanLotSize
     */
    public BigDecimal getKanbanLotSize() {
        return kanbanLotSize;
    }

    /**
     * Setter method of "kanbanLotSize"
     * 
     * @param kanbanLotSize Set in "kanbanLotSize".
     */
    public void setKanbanLotSize(BigDecimal kanbanLotSize) {
        this.kanbanLotSize = kanbanLotSize;
    }

    /**
     * Getter method of "tempKanbanTag"
     * 
     * @return the tempKanbanTag
     */
    public String getTempKanbanTag() {
        return tempKanbanTag;
    }

    /**
     * Setter method of "tempKanbanTag"
     * 
     * @param tempKanbanTag Set in "tempKanbanTag".
     */
    public void setTempKanbanTag(String tempKanbanTag) {
        this.tempKanbanTag = tempKanbanTag;
    }

    /**
     * Getter method of "deliveryDueTime"
     * 
     * @return the deliveryDueTime
     */
    public BigDecimal getDeliveryDueTime() {
        return deliveryDueTime;
    }

    /**
     * Setter method of "deliveryDueTime"
     * 
     * @param deliveryDueTime Set in "deliveryDueTime".
     */
    public void setDeliveryDueTime(BigDecimal deliveryDueTime) {
        this.deliveryDueTime = deliveryDueTime;
    }

    /**
     * Getter method of "deliveryRunType"
     * 
     * @return the deliveryRunType
     */
    public String getDeliveryRunType() {
        return deliveryRunType;
    }

    /**
     * Setter method of "deliveryRunType"
     * 
     * @param deliveryRunType Set in "deliveryRunType".
     */
    public void setDeliveryRunType(String deliveryRunType) {
        this.deliveryRunType = deliveryRunType;
    }

    /**
     * Getter method of "runScheduleDate"
     * 
     * @return the runScheduleDate
     */
    public Date getRunScheduleDate() {
        return runScheduleDate;
    }

    /**
     * Setter method of "runScheduleDate"
     * 
     * @param runScheduleDate Set in "runScheduleDate".
     */
    public void setRunScheduleDate(Date runScheduleDate) {
        this.runScheduleDate = runScheduleDate;
    }

    /**
     * Getter method of "qrRcvBatchNo"
     * 
     * @return the qrRcvBatchNo
     */
    public BigDecimal getQrRcvBatchNo() {
        return qrRcvBatchNo;
    }

    /**
     * Setter method of "qrRcvBatchNo"
     * 
     * @param qrRcvBatchNo Set in "qrRcvBatchNo".
     */
    public void setQrRcvBatchNo(BigDecimal qrRcvBatchNo) {
        this.qrRcvBatchNo = qrRcvBatchNo;
    }

    /**
     * Getter method of "qrScanTime"
     * 
     * @return the qrScanTime
     */
    public BigDecimal getQrScanTime() {
        return qrScanTime;
    }

    /**
     * Setter method of "qrScanTime"
     * 
     * @param qrScanTime Set in "qrScanTime".
     */
    public void setQrScanTime(BigDecimal qrScanTime) {
        this.qrScanTime = qrScanTime;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
