/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/12/16       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMAs400Schema"<br />
 * Table overview: SPS_M_AS400_SCHEMA<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/12/16 12:06:44<br />
 * 
 * This module generated automatically in 2015/12/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400SchemaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Schema Code
     */
    private String schemaCd;

    /**
     * Reserved schema in feture from Kimura San
     */
    private String schemaNameOther1;

    /**
     * Reserved schema in feture from Kimura San
     */
    private String schemaNameOther2;

    /**
     * Reserved schema in feture from Kimura San
     */
    private String schemaNameOther3;

    /**
     * Order module LPAR code
     */
    private String ordLparCd;

    /**
     * Not use now, but still reserve. Order module Share library name
     */
    private String ordSchemaNameShare;

    /**
     * Order module CIGMA library name
     */
    private String ordSchemaCigma;

    /**
     * Order module REST IP address
     */
    private String ordRestIpAddress;

    /**
     * Order module REST user name
     */
    private String ordRestUserName;

    /**
     * Order module REST password
     */
    private String ordRestPassword;

    /**
     * Order module connection number
     */
    private BigDecimal ordConnectionNo;

    /**
     * AS400 Member Name for separate Production and Trial. Set 999999999 for default file member.
     */
    private String ordMemberName;

    /**
     * ASN and Invoice LPAR code
     */
    private String asnInvLparCd;

    /**
     * Not use now, but still reserve. ASN and Invoice module Share library name
     */
    private String asnInvSchemaNameShare;

    /**
     * ASN and Invoice CIGMA library name
     */
    private String asnInvSchemaCigma;

    /**
     * ASN and Invoice JDE library name
     */
    private String asnInvSchemaJde;

    /**
     * ASN and Invoice CIGMA replicate library name
     */
    private String asnInvSchemaCigmaReplicate;

    /**
     * ASN and Invoice REST IP address
     */
    private String asnInvRestIpAddress;

    /**
     * ASN and Invoice REST user name
     */
    private String asnInvRestUserName;

    /**
     * ASN and Invoice REST password
     */
    private String asnInvRestPassword;

    /**
     * ASN and Invoice connection number
     */
    private BigDecimal asnInvConnectionNo;

    /**
     * 0 : Inactive
1 : Active
     */
    private String isActive;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMAs400SchemaDomain() {
    }

    /**
     * Getter method of "schemaCd"
     * 
     * @return the schemaCd
     */
    public String getSchemaCd() {
        return schemaCd;
    }

    /**
     * Setter method of "schemaCd"
     * 
     * @param schemaCd Set in "schemaCd".
     */
    public void setSchemaCd(String schemaCd) {
        this.schemaCd = schemaCd;
    }

    /**
     * Getter method of "schemaNameOther1"
     * 
     * @return the schemaNameOther1
     */
    public String getSchemaNameOther1() {
        return schemaNameOther1;
    }

    /**
     * Setter method of "schemaNameOther1"
     * 
     * @param schemaNameOther1 Set in "schemaNameOther1".
     */
    public void setSchemaNameOther1(String schemaNameOther1) {
        this.schemaNameOther1 = schemaNameOther1;
    }

    /**
     * Getter method of "schemaNameOther2"
     * 
     * @return the schemaNameOther2
     */
    public String getSchemaNameOther2() {
        return schemaNameOther2;
    }

    /**
     * Setter method of "schemaNameOther2"
     * 
     * @param schemaNameOther2 Set in "schemaNameOther2".
     */
    public void setSchemaNameOther2(String schemaNameOther2) {
        this.schemaNameOther2 = schemaNameOther2;
    }

    /**
     * Getter method of "schemaNameOther3"
     * 
     * @return the schemaNameOther3
     */
    public String getSchemaNameOther3() {
        return schemaNameOther3;
    }

    /**
     * Setter method of "schemaNameOther3"
     * 
     * @param schemaNameOther3 Set in "schemaNameOther3".
     */
    public void setSchemaNameOther3(String schemaNameOther3) {
        this.schemaNameOther3 = schemaNameOther3;
    }

    /**
     * Getter method of "ordLparCd"
     * 
     * @return the ordLparCd
     */
    public String getOrdLparCd() {
        return ordLparCd;
    }

    /**
     * Setter method of "ordLparCd"
     * 
     * @param ordLparCd Set in "ordLparCd".
     */
    public void setOrdLparCd(String ordLparCd) {
        this.ordLparCd = ordLparCd;
    }

    /**
     * Getter method of "ordSchemaNameShare"
     * 
     * @return the ordSchemaNameShare
     */
    public String getOrdSchemaNameShare() {
        return ordSchemaNameShare;
    }

    /**
     * Setter method of "ordSchemaNameShare"
     * 
     * @param ordSchemaNameShare Set in "ordSchemaNameShare".
     */
    public void setOrdSchemaNameShare(String ordSchemaNameShare) {
        this.ordSchemaNameShare = ordSchemaNameShare;
    }

    /**
     * Getter method of "ordSchemaCigma"
     * 
     * @return the ordSchemaCigma
     */
    public String getOrdSchemaCigma() {
        return ordSchemaCigma;
    }

    /**
     * Setter method of "ordSchemaCigma"
     * 
     * @param ordSchemaCigma Set in "ordSchemaCigma".
     */
    public void setOrdSchemaCigma(String ordSchemaCigma) {
        this.ordSchemaCigma = ordSchemaCigma;
    }

    /**
     * Getter method of "ordRestIpAddress"
     * 
     * @return the ordRestIpAddress
     */
    public String getOrdRestIpAddress() {
        return ordRestIpAddress;
    }

    /**
     * Setter method of "ordRestIpAddress"
     * 
     * @param ordRestIpAddress Set in "ordRestIpAddress".
     */
    public void setOrdRestIpAddress(String ordRestIpAddress) {
        this.ordRestIpAddress = ordRestIpAddress;
    }

    /**
     * Getter method of "ordRestUserName"
     * 
     * @return the ordRestUserName
     */
    public String getOrdRestUserName() {
        return ordRestUserName;
    }

    /**
     * Setter method of "ordRestUserName"
     * 
     * @param ordRestUserName Set in "ordRestUserName".
     */
    public void setOrdRestUserName(String ordRestUserName) {
        this.ordRestUserName = ordRestUserName;
    }

    /**
     * Getter method of "ordRestPassword"
     * 
     * @return the ordRestPassword
     */
    public String getOrdRestPassword() {
        return ordRestPassword;
    }

    /**
     * Setter method of "ordRestPassword"
     * 
     * @param ordRestPassword Set in "ordRestPassword".
     */
    public void setOrdRestPassword(String ordRestPassword) {
        this.ordRestPassword = ordRestPassword;
    }

    /**
     * Getter method of "ordConnectionNo"
     * 
     * @return the ordConnectionNo
     */
    public BigDecimal getOrdConnectionNo() {
        return ordConnectionNo;
    }

    /**
     * Setter method of "ordConnectionNo"
     * 
     * @param ordConnectionNo Set in "ordConnectionNo".
     */
    public void setOrdConnectionNo(BigDecimal ordConnectionNo) {
        this.ordConnectionNo = ordConnectionNo;
    }

    /**
     * Getter method of "ordMemberName"
     * 
     * @return the ordMemberName
     */
    public String getOrdMemberName() {
        return ordMemberName;
    }

    /**
     * Setter method of "ordMemberName"
     * 
     * @param ordMemberName Set in "ordMemberName".
     */
    public void setOrdMemberName(String ordMemberName) {
        this.ordMemberName = ordMemberName;
    }

    /**
     * Getter method of "asnInvLparCd"
     * 
     * @return the asnInvLparCd
     */
    public String getAsnInvLparCd() {
        return asnInvLparCd;
    }

    /**
     * Setter method of "asnInvLparCd"
     * 
     * @param asnInvLparCd Set in "asnInvLparCd".
     */
    public void setAsnInvLparCd(String asnInvLparCd) {
        this.asnInvLparCd = asnInvLparCd;
    }

    /**
     * Getter method of "asnInvSchemaNameShare"
     * 
     * @return the asnInvSchemaNameShare
     */
    public String getAsnInvSchemaNameShare() {
        return asnInvSchemaNameShare;
    }

    /**
     * Setter method of "asnInvSchemaNameShare"
     * 
     * @param asnInvSchemaNameShare Set in "asnInvSchemaNameShare".
     */
    public void setAsnInvSchemaNameShare(String asnInvSchemaNameShare) {
        this.asnInvSchemaNameShare = asnInvSchemaNameShare;
    }

    /**
     * Getter method of "asnInvSchemaCigma"
     * 
     * @return the asnInvSchemaCigma
     */
    public String getAsnInvSchemaCigma() {
        return asnInvSchemaCigma;
    }

    /**
     * Setter method of "asnInvSchemaCigma"
     * 
     * @param asnInvSchemaCigma Set in "asnInvSchemaCigma".
     */
    public void setAsnInvSchemaCigma(String asnInvSchemaCigma) {
        this.asnInvSchemaCigma = asnInvSchemaCigma;
    }

    /**
     * Getter method of "asnInvSchemaJde"
     * 
     * @return the asnInvSchemaJde
     */
    public String getAsnInvSchemaJde() {
        return asnInvSchemaJde;
    }

    /**
     * Setter method of "asnInvSchemaJde"
     * 
     * @param asnInvSchemaJde Set in "asnInvSchemaJde".
     */
    public void setAsnInvSchemaJde(String asnInvSchemaJde) {
        this.asnInvSchemaJde = asnInvSchemaJde;
    }

    /**
     * Getter method of "asnInvSchemaCigmaReplicate"
     * 
     * @return the asnInvSchemaCigmaReplicate
     */
    public String getAsnInvSchemaCigmaReplicate() {
        return asnInvSchemaCigmaReplicate;
    }

    /**
     * Setter method of "asnInvSchemaCigmaReplicate"
     * 
     * @param asnInvSchemaCigmaReplicate Set in "asnInvSchemaCigmaReplicate".
     */
    public void setAsnInvSchemaCigmaReplicate(String asnInvSchemaCigmaReplicate) {
        this.asnInvSchemaCigmaReplicate = asnInvSchemaCigmaReplicate;
    }

    /**
     * Getter method of "asnInvRestIpAddress"
     * 
     * @return the asnInvRestIpAddress
     */
    public String getAsnInvRestIpAddress() {
        return asnInvRestIpAddress;
    }

    /**
     * Setter method of "asnInvRestIpAddress"
     * 
     * @param asnInvRestIpAddress Set in "asnInvRestIpAddress".
     */
    public void setAsnInvRestIpAddress(String asnInvRestIpAddress) {
        this.asnInvRestIpAddress = asnInvRestIpAddress;
    }

    /**
     * Getter method of "asnInvRestUserName"
     * 
     * @return the asnInvRestUserName
     */
    public String getAsnInvRestUserName() {
        return asnInvRestUserName;
    }

    /**
     * Setter method of "asnInvRestUserName"
     * 
     * @param asnInvRestUserName Set in "asnInvRestUserName".
     */
    public void setAsnInvRestUserName(String asnInvRestUserName) {
        this.asnInvRestUserName = asnInvRestUserName;
    }

    /**
     * Getter method of "asnInvRestPassword"
     * 
     * @return the asnInvRestPassword
     */
    public String getAsnInvRestPassword() {
        return asnInvRestPassword;
    }

    /**
     * Setter method of "asnInvRestPassword"
     * 
     * @param asnInvRestPassword Set in "asnInvRestPassword".
     */
    public void setAsnInvRestPassword(String asnInvRestPassword) {
        this.asnInvRestPassword = asnInvRestPassword;
    }

    /**
     * Getter method of "asnInvConnectionNo"
     * 
     * @return the asnInvConnectionNo
     */
    public BigDecimal getAsnInvConnectionNo() {
        return asnInvConnectionNo;
    }

    /**
     * Setter method of "asnInvConnectionNo"
     * 
     * @param asnInvConnectionNo Set in "asnInvConnectionNo".
     */
    public void setAsnInvConnectionNo(BigDecimal asnInvConnectionNo) {
        this.asnInvConnectionNo = asnInvConnectionNo;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
