/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMUploadErrorMsg".<br />
 * Table overview: SPS_M_UPLOAD_ERROR_MSG<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUploadErrorMsgCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ERROR_CD
     */
    private String errorCd;

    /**
     * FIELD_NAME
     */
    private String fieldName;

    /**
     * ERROR_MESSAGE
     */
    private String errorMessage;

    /**
     * IS_WARNING
     */
    private String isWarning;

    /**
     * CREATE_USER
     */
    private String createUser;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_USER
     */
    private String lastUpdateUser;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * ERROR_CD(condition whether the column value at the beginning is equal to the value)
     */
    private String errorCdLikeFront;

    /**
     * FIELD_NAME(condition whether the column value at the beginning is equal to the value)
     */
    private String fieldNameLikeFront;

    /**
     * ERROR_MESSAGE(condition whether the column value at the beginning is equal to the value)
     */
    private String errorMessageLikeFront;

    /**
     * IS_WARNING(condition whether the column value at the beginning is equal to the value)
     */
    private String isWarningLikeFront;

    /**
     * CREATE_USER(condition whether the column value at the beginning is equal to the value)
     */
    private String createUserLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * LAST_UPDATE_USER(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateUserLikeFront;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMUploadErrorMsgCriteriaDomain() {
    }

    /**
     * Getter method of "errorCd".
     * 
     * @return the "errorCd"
     */
    public String getErrorCd() {
        return errorCd;
    }

    /**
     * Setter method of "errorCd".
     * 
     * @param errorCd Set in "errorCd".
     */
    public void setErrorCd(String errorCd) {
        this.errorCd = errorCd;
    }

    /**
     * Getter method of "fieldName".
     * 
     * @return the "fieldName"
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter method of "fieldName".
     * 
     * @param fieldName Set in "fieldName".
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Getter method of "errorMessage".
     * 
     * @return the "errorMessage"
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Setter method of "errorMessage".
     * 
     * @param errorMessage Set in "errorMessage".
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Getter method of "isWarning".
     * 
     * @return the "isWarning"
     */
    public String getIsWarning() {
        return isWarning;
    }

    /**
     * Setter method of "isWarning".
     * 
     * @param isWarning Set in "isWarning".
     */
    public void setIsWarning(String isWarning) {
        this.isWarning = isWarning;
    }

    /**
     * Getter method of "createUser".
     * 
     * @return the "createUser"
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method of "createUser".
     * 
     * @param createUser Set in "createUser".
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateUser".
     * 
     * @return the "lastUpdateUser"
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Setter method of "lastUpdateUser".
     * 
     * @param lastUpdateUser Set in "lastUpdateUser".
     */
    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "errorCdLikeFront".
     * 
     * @return the "errorCdLikeFront"
     */
    public String getErrorCdLikeFront() {
        return errorCdLikeFront;
    }

    /**
     * Setter method of "errorCdLikeFront".
     * 
     * @param errorCdLikeFront Set in "errorCdLikeFront".
     */
    public void setErrorCdLikeFront(String errorCdLikeFront) {
        this.errorCdLikeFront = errorCdLikeFront;
    }

    /**
     * Getter method of "fieldNameLikeFront".
     * 
     * @return the "fieldNameLikeFront"
     */
    public String getFieldNameLikeFront() {
        return fieldNameLikeFront;
    }

    /**
     * Setter method of "fieldNameLikeFront".
     * 
     * @param fieldNameLikeFront Set in "fieldNameLikeFront".
     */
    public void setFieldNameLikeFront(String fieldNameLikeFront) {
        this.fieldNameLikeFront = fieldNameLikeFront;
    }

    /**
     * Getter method of "errorMessageLikeFront".
     * 
     * @return the "errorMessageLikeFront"
     */
    public String getErrorMessageLikeFront() {
        return errorMessageLikeFront;
    }

    /**
     * Setter method of "errorMessageLikeFront".
     * 
     * @param errorMessageLikeFront Set in "errorMessageLikeFront".
     */
    public void setErrorMessageLikeFront(String errorMessageLikeFront) {
        this.errorMessageLikeFront = errorMessageLikeFront;
    }

    /**
     * Getter method of "isWarningLikeFront".
     * 
     * @return the "isWarningLikeFront"
     */
    public String getIsWarningLikeFront() {
        return isWarningLikeFront;
    }

    /**
     * Setter method of "isWarningLikeFront".
     * 
     * @param isWarningLikeFront Set in "isWarningLikeFront".
     */
    public void setIsWarningLikeFront(String isWarningLikeFront) {
        this.isWarningLikeFront = isWarningLikeFront;
    }

    /**
     * Getter method of "createUserLikeFront".
     * 
     * @return the "createUserLikeFront"
     */
    public String getCreateUserLikeFront() {
        return createUserLikeFront;
    }

    /**
     * Setter method of "createUserLikeFront".
     * 
     * @param createUserLikeFront Set in "createUserLikeFront".
     */
    public void setCreateUserLikeFront(String createUserLikeFront) {
        this.createUserLikeFront = createUserLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateUserLikeFront".
     * 
     * @return the "lastUpdateUserLikeFront"
     */
    public String getLastUpdateUserLikeFront() {
        return lastUpdateUserLikeFront;
    }

    /**
     * Setter method of "lastUpdateUserLikeFront".
     * 
     * @param lastUpdateUserLikeFront Set in "lastUpdateUserLikeFront".
     */
    public void setLastUpdateUserLikeFront(String lastUpdateUserLikeFront) {
        this.lastUpdateUserLikeFront = lastUpdateUserLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
