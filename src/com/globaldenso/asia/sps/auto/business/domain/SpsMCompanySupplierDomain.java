/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/29       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMCompanySupplier"<br />
 * Table overview: SPS_M_COMPANY_SUPPLIER<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/29 18:51:48<br />
 * 
 * This module generated automatically in 2014/10/29 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanySupplierDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * DSC_ID_COM_CD
     */
    private String dscIdComCd;

    /**
     * COMPANY_NAME
     */
    private String companyName;

    /**
     * ADDRESS1
     */
    private String address1;

    /**
     * ADDRESS2
     */
    private String address2;

    /**
     * ADDRESS3
     */
    private String address3;

    /**
     * TELEPHONE
     */
    private String telephone;

    /**
     * FAX
     */
    private String fax;

    /**
     * CONTACT_PERSON
     */
    private String contactPerson;

    /**
     * S_TAX_ID
     */
    private String sTaxId;

    /**
     * TAX_CD
     */
    private String taxCd;

    /**
     * CURRENCY_CD
     */
    private String currencyCd;

    /**
     * IS_ACTIVE
     */
    private String isActive;

    /**
     * NOTICE_DO_URGENT_PERIOD
     */
    private BigDecimal noticeDoUrgentPeriod;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMCompanySupplierDomain() {
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "dscIdComCd"
     * 
     * @return the dscIdComCd
     */
    public String getDscIdComCd() {
        return dscIdComCd;
    }

    /**
     * Setter method of "dscIdComCd"
     * 
     * @param dscIdComCd Set in "dscIdComCd".
     */
    public void setDscIdComCd(String dscIdComCd) {
        this.dscIdComCd = dscIdComCd;
    }

    /**
     * Getter method of "companyName"
     * 
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Setter method of "companyName"
     * 
     * @param companyName Set in "companyName".
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Getter method of "address1"
     * 
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Setter method of "address1"
     * 
     * @param address1 Set in "address1".
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * Getter method of "address2"
     * 
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Setter method of "address2"
     * 
     * @param address2 Set in "address2".
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * Getter method of "address3"
     * 
     * @return the address3
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Setter method of "address3"
     * 
     * @param address3 Set in "address3".
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "fax"
     * 
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax"
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "contactPerson"
     * 
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Setter method of "contactPerson"
     * 
     * @param contactPerson Set in "contactPerson".
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Getter method of "sTaxId"
     * 
     * @return the sTaxId
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * Setter method of "sTaxId"
     * 
     * @param sTaxId Set in "sTaxId".
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Getter method of "taxCd"
     * 
     * @return the taxCd
     */
    public String getTaxCd() {
        return taxCd;
    }

    /**
     * Setter method of "taxCd"
     * 
     * @param taxCd Set in "taxCd".
     */
    public void setTaxCd(String taxCd) {
        this.taxCd = taxCd;
    }

    /**
     * Getter method of "currencyCd"
     * 
     * @return the currencyCd
     */
    public String getCurrencyCd() {
        return currencyCd;
    }

    /**
     * Setter method of "currencyCd"
     * 
     * @param currencyCd Set in "currencyCd".
     */
    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "noticeDoUrgentPeriod"
     * 
     * @return the noticeDoUrgentPeriod
     */
    public BigDecimal getNoticeDoUrgentPeriod() {
        return noticeDoUrgentPeriod;
    }

    /**
     * Setter method of "noticeDoUrgentPeriod"
     * 
     * @param noticeDoUrgentPeriod Set in "noticeDoUrgentPeriod".
     */
    public void setNoticeDoUrgentPeriod(BigDecimal noticeDoUrgentPeriod) {
        this.noticeDoUrgentPeriod = noticeDoUrgentPeriod;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
