/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/16       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTPoDue"<br />
 * Table overview: SPS_T_PO_DUE<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/16 14:33:35<br />
 * 
 * This module generated automatically in 2016/02/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDueDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from SPS_T_PO
     */
    private BigDecimal poId;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Running No of Due
     */
    private BigDecimal dueId;

    /**
     * Due Date
     */
    private Date dueDate;

    /**
     * Report Type Flag
0 : D -> Daily
1 : W -> Weekly
2 : M -> Monthly
     */
    private String reportTypeFlg;

    /**
     * Order Qty
     */
    private BigDecimal orderQty;

    /**
     * ETD Date
     */
    private Date etd;

    /**
     * DEL (CIGMA column DEL421)
     */
    private String del;

    /**
     * SEQ (CIGMA column SEQ421)
     */
    private BigDecimal seq;

    /**
     * Previous Order Qty
     */
    private BigDecimal previousQty;

    /**
     * Difference Order Qty
     */
    private BigDecimal differenceQty;

    /**
     * Change Reason
     */
    private String rsn;

    /**
     * SPS Proposed Due Date
     */
    private Date spsProposedDueDate;

    /**
     * SPS Proposed Qty
     */
    private BigDecimal spsProposedQty;

    /**
     * SPS Pending Reason Code
     */
    private String spsPendingReasonCd;

    /**
     * Mark Pending Flag when action from screen WORD004
GO 0 : None, 1 : Pending
     */
    private String markPendingFlg;

    /**
     * Inform Change P/O or not
GO 0 : Not Change, 1 : Change
     */
    private String changeFlg;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * 1 : Accept or 0 : Reject Supplier Promised Due
     */
    private String densoReplyFlg;

    /**
     * Default constructor
     */
    public SpsTPoDueDomain() {
    }

    /**
     * Getter method of "poId"
     * 
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId"
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "dueId"
     * 
     * @return the dueId
     */
    public BigDecimal getDueId() {
        return dueId;
    }

    /**
     * Setter method of "dueId"
     * 
     * @param dueId Set in "dueId".
     */
    public void setDueId(BigDecimal dueId) {
        this.dueId = dueId;
    }

    /**
     * Getter method of "dueDate"
     * 
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Setter method of "dueDate"
     * 
     * @param dueDate Set in "dueDate".
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Getter method of "reportTypeFlg"
     * 
     * @return the reportTypeFlg
     */
    public String getReportTypeFlg() {
        return reportTypeFlg;
    }

    /**
     * Setter method of "reportTypeFlg"
     * 
     * @param reportTypeFlg Set in "reportTypeFlg".
     */
    public void setReportTypeFlg(String reportTypeFlg) {
        this.reportTypeFlg = reportTypeFlg;
    }

    /**
     * Getter method of "orderQty"
     * 
     * @return the orderQty
     */
    public BigDecimal getOrderQty() {
        return orderQty;
    }

    /**
     * Setter method of "orderQty"
     * 
     * @param orderQty Set in "orderQty".
     */
    public void setOrderQty(BigDecimal orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * Getter method of "etd"
     * 
     * @return the etd
     */
    public Date getEtd() {
        return etd;
    }

    /**
     * Setter method of "etd"
     * 
     * @param etd Set in "etd".
     */
    public void setEtd(Date etd) {
        this.etd = etd;
    }

    /**
     * Getter method of "del"
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * Setter method of "del"
     * 
     * @param del Set in "del".
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * Getter method of "seq"
     * 
     * @return the seq
     */
    public BigDecimal getSeq() {
        return seq;
    }

    /**
     * Setter method of "seq"
     * 
     * @param seq Set in "seq".
     */
    public void setSeq(BigDecimal seq) {
        this.seq = seq;
    }

    /**
     * Getter method of "previousQty"
     * 
     * @return the previousQty
     */
    public BigDecimal getPreviousQty() {
        return previousQty;
    }

    /**
     * Setter method of "previousQty"
     * 
     * @param previousQty Set in "previousQty".
     */
    public void setPreviousQty(BigDecimal previousQty) {
        this.previousQty = previousQty;
    }

    /**
     * Getter method of "differenceQty"
     * 
     * @return the differenceQty
     */
    public BigDecimal getDifferenceQty() {
        return differenceQty;
    }

    /**
     * Setter method of "differenceQty"
     * 
     * @param differenceQty Set in "differenceQty".
     */
    public void setDifferenceQty(BigDecimal differenceQty) {
        this.differenceQty = differenceQty;
    }

    /**
     * Getter method of "rsn"
     * 
     * @return the rsn
     */
    public String getRsn() {
        return rsn;
    }

    /**
     * Setter method of "rsn"
     * 
     * @param rsn Set in "rsn".
     */
    public void setRsn(String rsn) {
        this.rsn = rsn;
    }

    /**
     * Getter method of "spsProposedDueDate"
     * 
     * @return the spsProposedDueDate
     */
    public Date getSpsProposedDueDate() {
        return spsProposedDueDate;
    }

    /**
     * Setter method of "spsProposedDueDate"
     * 
     * @param spsProposedDueDate Set in "spsProposedDueDate".
     */
    public void setSpsProposedDueDate(Date spsProposedDueDate) {
        this.spsProposedDueDate = spsProposedDueDate;
    }

    /**
     * Getter method of "spsProposedQty"
     * 
     * @return the spsProposedQty
     */
    public BigDecimal getSpsProposedQty() {
        return spsProposedQty;
    }

    /**
     * Setter method of "spsProposedQty"
     * 
     * @param spsProposedQty Set in "spsProposedQty".
     */
    public void setSpsProposedQty(BigDecimal spsProposedQty) {
        this.spsProposedQty = spsProposedQty;
    }

    /**
     * Getter method of "spsPendingReasonCd"
     * 
     * @return the spsPendingReasonCd
     */
    public String getSpsPendingReasonCd() {
        return spsPendingReasonCd;
    }

    /**
     * Setter method of "spsPendingReasonCd"
     * 
     * @param spsPendingReasonCd Set in "spsPendingReasonCd".
     */
    public void setSpsPendingReasonCd(String spsPendingReasonCd) {
        this.spsPendingReasonCd = spsPendingReasonCd;
    }

    /**
     * Getter method of "markPendingFlg"
     * 
     * @return the markPendingFlg
     */
    public String getMarkPendingFlg() {
        return markPendingFlg;
    }

    /**
     * Setter method of "markPendingFlg"
     * 
     * @param markPendingFlg Set in "markPendingFlg".
     */
    public void setMarkPendingFlg(String markPendingFlg) {
        this.markPendingFlg = markPendingFlg;
    }

    /**
     * Getter method of "changeFlg"
     * 
     * @return the changeFlg
     */
    public String getChangeFlg() {
        return changeFlg;
    }

    /**
     * Setter method of "changeFlg"
     * 
     * @param changeFlg Set in "changeFlg".
     */
    public void setChangeFlg(String changeFlg) {
        this.changeFlg = changeFlg;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "densoReplyFlg"
     * 
     * @return the densoReplyFlg
     */
    public String getDensoReplyFlg() {
        return densoReplyFlg;
    }

    /**
     * Setter method of "densoReplyFlg"
     * 
     * @param densoReplyFlg Set in "densoReplyFlg".
     */
    public void setDensoReplyFlg(String densoReplyFlg) {
        this.densoReplyFlg = densoReplyFlg;
    }

}
