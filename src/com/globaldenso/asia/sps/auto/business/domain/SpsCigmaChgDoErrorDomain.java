/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * A "Domain" class of "SpsCigmaChgDoError"<br />
 * Table overview: SPS_CIGMA_CHG_DO_ERROR<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaChgDoErrorDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * CHG_DO_ERROR_ID
     */
    private BigDecimal chgDoErrorId;

    /**
     * CIGMA_DO_NO
     */
    private String cigmaDoNo;

    /**
     * DATA_TYPE
     */
    private String dataType;

    /**
     * VENDOR_CD
     */
    private String vendorCd;

    /**
     * ISSUE_DATE
     */
    private Date issueDate;

    /**
     * D_PN
     */
    private String dPn;

    /**
     * ORIGINAL_CIGMA_DO_NO
     */
    private String originalCigmaDoNo;

    /**
     * PREV_CIGMA_DO_NO
     */
    private String prevCigmaDoNo;

    /**
     * CURRENT_ORDER_QTY
     */
    private BigDecimal currentOrderQty;

    /**
     * ORIGINAL_ORDER_QTY
     */
    private BigDecimal originalOrderQty;

    /**
     * MAIL_FLG
     */
    private String mailFlg;

    /**
     * ERROR_TYPE_FLG
     */
    private String errorTypeFlg;

    /**
     * UPDATE_BY_USER_ID
     */
    private String updateByUserId;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * DELIVERY_DATE
     */
    private Date deliveryDate;

    /**
     * DELIVERY_TIME
     */
    private String deliveryTime;

    /**
     * WH
     */
    private String wh;

    /**
     * DOCK_CODE
     */
    private String dockCode;

    /**
     * Default constructor
     */
    public SpsCigmaChgDoErrorDomain() {
    }

    /**
     * Getter method of "chgDoErrorId"
     * 
     * @return the chgDoErrorId
     */
    public BigDecimal getChgDoErrorId() {
        return chgDoErrorId;
    }

    /**
     * Setter method of "chgDoErrorId"
     * 
     * @param chgDoErrorId Set in "chgDoErrorId".
     */
    public void setChgDoErrorId(BigDecimal chgDoErrorId) {
        this.chgDoErrorId = chgDoErrorId;
    }

    /**
     * Getter method of "cigmaDoNo"
     * 
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo"
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "dataType"
     * 
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType"
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "issueDate"
     * 
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Setter method of "issueDate"
     * 
     * @param issueDate Set in "issueDate".
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "originalCigmaDoNo"
     * 
     * @return the originalCigmaDoNo
     */
    public String getOriginalCigmaDoNo() {
        return originalCigmaDoNo;
    }

    /**
     * Setter method of "originalCigmaDoNo"
     * 
     * @param originalCigmaDoNo Set in "originalCigmaDoNo".
     */
    public void setOriginalCigmaDoNo(String originalCigmaDoNo) {
        this.originalCigmaDoNo = originalCigmaDoNo;
    }

    /**
     * Getter method of "prevCigmaDoNo"
     * 
     * @return the prevCigmaDoNo
     */
    public String getPrevCigmaDoNo() {
        return prevCigmaDoNo;
    }

    /**
     * Setter method of "prevCigmaDoNo"
     * 
     * @param prevCigmaDoNo Set in "prevCigmaDoNo".
     */
    public void setPrevCigmaDoNo(String prevCigmaDoNo) {
        this.prevCigmaDoNo = prevCigmaDoNo;
    }

    /**
     * Getter method of "currentOrderQty"
     * 
     * @return the currentOrderQty
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Setter method of "currentOrderQty"
     * 
     * @param currentOrderQty Set in "currentOrderQty".
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Getter method of "originalOrderQty"
     * 
     * @return the originalOrderQty
     */
    public BigDecimal getOriginalOrderQty() {
        return originalOrderQty;
    }

    /**
     * Setter method of "originalOrderQty"
     * 
     * @param originalOrderQty Set in "originalOrderQty".
     */
    public void setOriginalOrderQty(BigDecimal originalOrderQty) {
        this.originalOrderQty = originalOrderQty;
    }

    /**
     * Getter method of "mailFlg"
     * 
     * @return the mailFlg
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg"
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "errorTypeFlg"
     * 
     * @return the errorTypeFlg
     */
    public String getErrorTypeFlg() {
        return errorTypeFlg;
    }

    /**
     * Setter method of "errorTypeFlg"
     * 
     * @param errorTypeFlg Set in "errorTypeFlg".
     */
    public void setErrorTypeFlg(String errorTypeFlg) {
        this.errorTypeFlg = errorTypeFlg;
    }

    /**
     * Getter method of "updateByUserId"
     * 
     * @return the updateByUserId
     */
    public String getUpdateByUserId() {
        return updateByUserId;
    }

    /**
     * Setter method of "updateByUserId"
     * 
     * @param updateByUserId Set in "updateByUserId".
     */
    public void setUpdateByUserId(String updateByUserId) {
        this.updateByUserId = updateByUserId;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "deliveryDate"
     * 
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Setter method of "deliveryDate"
     * 
     * @param deliveryDate Set in "deliveryDate".
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * Getter method of "deliveryTime"
     * 
     * @return the deliveryTime
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * Setter method of "deliveryTime"
     * 
     * @param deliveryTime Set in "deliveryTime".
     */
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * Getter method of "wh"
     * 
     * @return the wh
     */
    public String getWh() {
        return wh;
    }

    /**
     * Setter method of "wh"
     * 
     * @param wh Set in "wh".
     */
    public void setWh(String wh) {
        this.wh = wh;
    }

    /**
     * Getter method of "dockCode"
     * 
     * @return the dockCode
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * Setter method of "dockCode"
     * 
     * @param dockCode Set in "dockCode".
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

}
