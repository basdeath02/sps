/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpUserDenso".<br />
 * Table overview: SPS_TMP_USER_DENSO<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 12:59:09<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserDensoCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User DSC ID that upload data to system
     */
    private String userDscId;

    /**
     * User login session code
     */
    private String sessionCd;

    /**
     * Source file line number
     */
    private BigDecimal lineNo;

    /**
     * User identify number (import data)
     */
    private String dscId;

    /**
     * DENSO company code (import data)
     */
    private String dCd;

    /**
     * DENSO plant code (import data)
     */
    private String dPcd;

    /**
     * DENSO employee code (import data)
     */
    private String employeeCd;

    /**
     * DENSO department name (import data)
     */
    private String departmentCd;

    /**
     * User first name (import data)
     */
    private String firstName;

    /**
     * User middle name (import data)
     */
    private String middleName;

    /**
     * User last name (import data)
     */
    private String lastName;

    /**
     * User email addresses if has email address more than 1 separate each of it by comma ","  (import data)
     */
    private String email;

    /**
     * User telephone number (import data)
     */
    private String telephone;

    /**
     * Create ASN (Shot Shipment) and Cancel ASN, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlCreateCancelAsnFlag;

    /**
     * Revise Shipping QTY, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlReviseAsnFlag;

    /**
     * Create Invoice with CN or Use Temp Price, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlCreateInvoiceFlag;

    /**
     * Abnormal Transfer data(Pending P/O, D/O aleady create ASN, Supplier Information not found), 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlPedpoDoalcasnUnmatpnFlg;

    /**
     * Other case of abnormal Transfer data, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)
     */
    private String emlAbnormalTransferFlg;

    /**
     * User role assignment value  (import data)
     */
    private String roleCd;

    /**
     * Scope of user right on DENSO company  (import data)
     */
    private String roleDCd;

    /**
     * Scope of user right on DENSO plant  (import data)
     */
    private String roleDPcd;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * User role effective start date  (import data)
     */
    private Timestamp effectStart;

    /**
     * User role effective end date  (import data)
     */
    private Timestamp effectEnd;

    /**
     * A : Add, U : Update, D : Delete
     */
    private String informationFlag;

    /**
     * A : Add, U : Update, T : Terminate
     */
    private String roleFlag;

    /**
     * Upload operation timestamp
     */
    private Timestamp uploadDatetime;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete
     */
    private Timestamp userLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_DENSO) in case of Update/Delete
     */
    private Timestamp densoLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete
     */
    private Timestamp roleLastUpdate;

    /**
     * 0 : not move yet, 1 : move to actual table
     */
    private String isActualRegister;

    /**
     * Move data from temporary table to actual table timestamp
     */
    private Timestamp toActualDatetime;

    /**
     * User DSC ID that upload data to system(condition whether the column value at the beginning is equal to the value)
     */
    private String userDscIdLikeFront;

    /**
     * User login session code(condition whether the column value at the beginning is equal to the value)
     */
    private String sessionCdLikeFront;

    /**
     * User identify number (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdLikeFront;

    /**
     * DENSO company code (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO plant code (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * DENSO employee code (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String employeeCdLikeFront;

    /**
     * DENSO department name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String departmentCdLikeFront;

    /**
     * User first name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String firstNameLikeFront;

    /**
     * User middle name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String middleNameLikeFront;

    /**
     * User last name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String lastNameLikeFront;

    /**
     * User email addresses if has email address more than 1 separate each of it by comma ","  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emailLikeFront;

    /**
     * User telephone number (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String telephoneLikeFront;

    /**
     * Create ASN (Shot Shipment) and Cancel ASN, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlCreateCancelAsnFlagLikeFront;

    /**
     * Revise Shipping QTY, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlReviseAsnFlagLikeFront;

    /**
     * Create Invoice with CN or Use Temp Price, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlCreateInvoiceFlagLikeFront;

    /**
     * Abnormal Transfer data(Pending P/O, D/O aleady create ASN, Supplier Information not found), 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlPedpoDoalcasnUnmatpnFlgLikeFront;

    /**
     * Other case of abnormal Transfer data, 0: Determined not to receive this email topic. 1: Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlAbnormalTransferFlgLikeFront;

    /**
     * User role assignment value  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String roleCdLikeFront;

    /**
     * Scope of user right on DENSO company  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String roleDCdLikeFront;

    /**
     * Scope of user right on DENSO plant  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String roleDPcdLikeFront;

    /**
     * User role effective start date  (import data)(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectStartGreaterThanEqual;

    /**
     * User role effective start date  (import data)(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectStartLessThanEqual;

    /**
     * User role effective end date  (import data)(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectEndGreaterThanEqual;

    /**
     * User role effective end date  (import data)(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectEndLessThanEqual;

    /**
     * A : Add, U : Update, D : Delete(condition whether the column value at the beginning is equal to the value)
     */
    private String informationFlagLikeFront;

    /**
     * A : Add, U : Update, T : Terminate(condition whether the column value at the beginning is equal to the value)
     */
    private String roleFlagLikeFront;

    /**
     * Upload operation timestamp(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp uploadDatetimeGreaterThanEqual;

    /**
     * Upload operation timestamp(condition whether the column value is less than or equal to the value)
     */
    private Timestamp uploadDatetimeLessThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp userLastUpdateGreaterThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete(condition whether the column value is less than or equal to the value)
     */
    private Timestamp userLastUpdateLessThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_DENSO) in case of Update/Delete(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp densoLastUpdateGreaterThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_DENSO) in case of Update/Delete(condition whether the column value is less than or equal to the value)
     */
    private Timestamp densoLastUpdateLessThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp roleLastUpdateGreaterThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete(condition whether the column value is less than or equal to the value)
     */
    private Timestamp roleLastUpdateLessThanEqual;

    /**
     * 0 : not move yet, 1 : move to actual table(condition whether the column value at the beginning is equal to the value)
     */
    private String isActualRegisterLikeFront;

    /**
     * Move data from temporary table to actual table timestamp(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp toActualDatetimeGreaterThanEqual;

    /**
     * Move data from temporary table to actual table timestamp(condition whether the column value is less than or equal to the value)
     */
    private Timestamp toActualDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTmpUserDensoCriteriaDomain() {
    }

    /**
     * Getter method of "userDscId".
     * 
     * @return the "userDscId"
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId".
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd".
     * 
     * @return the "sessionCd"
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd".
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo".
     * 
     * @return the "lineNo"
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo".
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dscId".
     * 
     * @return the "dscId"
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId".
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "employeeCd".
     * 
     * @return the "employeeCd"
     */
    public String getEmployeeCd() {
        return employeeCd;
    }

    /**
     * Setter method of "employeeCd".
     * 
     * @param employeeCd Set in "employeeCd".
     */
    public void setEmployeeCd(String employeeCd) {
        this.employeeCd = employeeCd;
    }

    /**
     * Getter method of "departmentCd".
     * 
     * @return the "departmentCd"
     */
    public String getDepartmentCd() {
        return departmentCd;
    }

    /**
     * Setter method of "departmentCd".
     * 
     * @param departmentCd Set in "departmentCd".
     */
    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    /**
     * Getter method of "firstName".
     * 
     * @return the "firstName"
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName".
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName".
     * 
     * @return the "middleName"
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName".
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName".
     * 
     * @return the "lastName"
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName".
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email".
     * 
     * @return the "email"
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email".
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone".
     * 
     * @return the "telephone"
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone".
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlag".
     * 
     * @return the "emlCreateCancelAsnFlag"
     */
    public String getEmlCreateCancelAsnFlag() {
        return emlCreateCancelAsnFlag;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlag".
     * 
     * @param emlCreateCancelAsnFlag Set in "emlCreateCancelAsnFlag".
     */
    public void setEmlCreateCancelAsnFlag(String emlCreateCancelAsnFlag) {
        this.emlCreateCancelAsnFlag = emlCreateCancelAsnFlag;
    }

    /**
     * Getter method of "emlReviseAsnFlag".
     * 
     * @return the "emlReviseAsnFlag"
     */
    public String getEmlReviseAsnFlag() {
        return emlReviseAsnFlag;
    }

    /**
     * Setter method of "emlReviseAsnFlag".
     * 
     * @param emlReviseAsnFlag Set in "emlReviseAsnFlag".
     */
    public void setEmlReviseAsnFlag(String emlReviseAsnFlag) {
        this.emlReviseAsnFlag = emlReviseAsnFlag;
    }

    /**
     * Getter method of "emlCreateInvoiceFlag".
     * 
     * @return the "emlCreateInvoiceFlag"
     */
    public String getEmlCreateInvoiceFlag() {
        return emlCreateInvoiceFlag;
    }

    /**
     * Setter method of "emlCreateInvoiceFlag".
     * 
     * @param emlCreateInvoiceFlag Set in "emlCreateInvoiceFlag".
     */
    public void setEmlCreateInvoiceFlag(String emlCreateInvoiceFlag) {
        this.emlCreateInvoiceFlag = emlCreateInvoiceFlag;
    }

    /**
     * Getter method of "emlPedpoDoalcasnUnmatpnFlg".
     * 
     * @return the "emlPedpoDoalcasnUnmatpnFlg"
     */
    public String getEmlPedpoDoalcasnUnmatpnFlg() {
        return emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Setter method of "emlPedpoDoalcasnUnmatpnFlg".
     * 
     * @param emlPedpoDoalcasnUnmatpnFlg Set in "emlPedpoDoalcasnUnmatpnFlg".
     */
    public void setEmlPedpoDoalcasnUnmatpnFlg(String emlPedpoDoalcasnUnmatpnFlg) {
        this.emlPedpoDoalcasnUnmatpnFlg = emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * Getter method of "emlAbnormalTransferFlg".
     * 
     * @return the "emlAbnormalTransferFlg"
     */
    public String getEmlAbnormalTransferFlg() {
        return emlAbnormalTransferFlg;
    }

    /**
     * Setter method of "emlAbnormalTransferFlg".
     * 
     * @param emlAbnormalTransferFlg Set in "emlAbnormalTransferFlg".
     */
    public void setEmlAbnormalTransferFlg(String emlAbnormalTransferFlg) {
        this.emlAbnormalTransferFlg = emlAbnormalTransferFlg;
    }

    /**
     * Getter method of "roleCd".
     * 
     * @return the "roleCd"
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Setter method of "roleCd".
     * 
     * @param roleCd Set in "roleCd".
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Getter method of "roleDCd".
     * 
     * @return the "roleDCd"
     */
    public String getRoleDCd() {
        return roleDCd;
    }

    /**
     * Setter method of "roleDCd".
     * 
     * @param roleDCd Set in "roleDCd".
     */
    public void setRoleDCd(String roleDCd) {
        this.roleDCd = roleDCd;
    }

    /**
     * Getter method of "roleDPcd".
     * 
     * @return the "roleDPcd"
     */
    public String getRoleDPcd() {
        return roleDPcd;
    }

    /**
     * Setter method of "roleDPcd".
     * 
     * @param roleDPcd Set in "roleDPcd".
     */
    public void setRoleDPcd(String roleDPcd) {
        this.roleDPcd = roleDPcd;
    }

    /**
     * Getter method of "seqNo".
     * 
     * @return the "seqNo"
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo".
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "effectStart".
     * 
     * @return the "effectStart"
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart".
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd".
     * 
     * @return the "effectEnd"
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd".
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "informationFlag".
     * 
     * @return the "informationFlag"
     */
    public String getInformationFlag() {
        return informationFlag;
    }

    /**
     * Setter method of "informationFlag".
     * 
     * @param informationFlag Set in "informationFlag".
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }

    /**
     * Getter method of "roleFlag".
     * 
     * @return the "roleFlag"
     */
    public String getRoleFlag() {
        return roleFlag;
    }

    /**
     * Setter method of "roleFlag".
     * 
     * @param roleFlag Set in "roleFlag".
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Getter method of "uploadDatetime".
     * 
     * @return the "uploadDatetime"
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime".
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "userLastUpdate".
     * 
     * @return the "userLastUpdate"
     */
    public Timestamp getUserLastUpdate() {
        return userLastUpdate;
    }

    /**
     * Setter method of "userLastUpdate".
     * 
     * @param userLastUpdate Set in "userLastUpdate".
     */
    public void setUserLastUpdate(Timestamp userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    /**
     * Getter method of "densoLastUpdate".
     * 
     * @return the "densoLastUpdate"
     */
    public Timestamp getDensoLastUpdate() {
        return densoLastUpdate;
    }

    /**
     * Setter method of "densoLastUpdate".
     * 
     * @param densoLastUpdate Set in "densoLastUpdate".
     */
    public void setDensoLastUpdate(Timestamp densoLastUpdate) {
        this.densoLastUpdate = densoLastUpdate;
    }

    /**
     * Getter method of "roleLastUpdate".
     * 
     * @return the "roleLastUpdate"
     */
    public Timestamp getRoleLastUpdate() {
        return roleLastUpdate;
    }

    /**
     * Setter method of "roleLastUpdate".
     * 
     * @param roleLastUpdate Set in "roleLastUpdate".
     */
    public void setRoleLastUpdate(Timestamp roleLastUpdate) {
        this.roleLastUpdate = roleLastUpdate;
    }

    /**
     * Getter method of "isActualRegister".
     * 
     * @return the "isActualRegister"
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister".
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime".
     * 
     * @return the "toActualDatetime"
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime".
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

    /**
     * Getter method of "userDscIdLikeFront".
     * 
     * @return the "userDscIdLikeFront"
     */
    public String getUserDscIdLikeFront() {
        return userDscIdLikeFront;
    }

    /**
     * Setter method of "userDscIdLikeFront".
     * 
     * @param userDscIdLikeFront Set in "userDscIdLikeFront".
     */
    public void setUserDscIdLikeFront(String userDscIdLikeFront) {
        this.userDscIdLikeFront = userDscIdLikeFront;
    }

    /**
     * Getter method of "sessionCdLikeFront".
     * 
     * @return the "sessionCdLikeFront"
     */
    public String getSessionCdLikeFront() {
        return sessionCdLikeFront;
    }

    /**
     * Setter method of "sessionCdLikeFront".
     * 
     * @param sessionCdLikeFront Set in "sessionCdLikeFront".
     */
    public void setSessionCdLikeFront(String sessionCdLikeFront) {
        this.sessionCdLikeFront = sessionCdLikeFront;
    }

    /**
     * Getter method of "dscIdLikeFront".
     * 
     * @return the "dscIdLikeFront"
     */
    public String getDscIdLikeFront() {
        return dscIdLikeFront;
    }

    /**
     * Setter method of "dscIdLikeFront".
     * 
     * @param dscIdLikeFront Set in "dscIdLikeFront".
     */
    public void setDscIdLikeFront(String dscIdLikeFront) {
        this.dscIdLikeFront = dscIdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "employeeCdLikeFront".
     * 
     * @return the "employeeCdLikeFront"
     */
    public String getEmployeeCdLikeFront() {
        return employeeCdLikeFront;
    }

    /**
     * Setter method of "employeeCdLikeFront".
     * 
     * @param employeeCdLikeFront Set in "employeeCdLikeFront".
     */
    public void setEmployeeCdLikeFront(String employeeCdLikeFront) {
        this.employeeCdLikeFront = employeeCdLikeFront;
    }

    /**
     * Getter method of "departmentCdLikeFront".
     * 
     * @return the "departmentCdLikeFront"
     */
    public String getDepartmentCdLikeFront() {
        return departmentCdLikeFront;
    }

    /**
     * Setter method of "departmentCdLikeFront".
     * 
     * @param departmentCdLikeFront Set in "departmentCdLikeFront".
     */
    public void setDepartmentCdLikeFront(String departmentCdLikeFront) {
        this.departmentCdLikeFront = departmentCdLikeFront;
    }

    /**
     * Getter method of "firstNameLikeFront".
     * 
     * @return the "firstNameLikeFront"
     */
    public String getFirstNameLikeFront() {
        return firstNameLikeFront;
    }

    /**
     * Setter method of "firstNameLikeFront".
     * 
     * @param firstNameLikeFront Set in "firstNameLikeFront".
     */
    public void setFirstNameLikeFront(String firstNameLikeFront) {
        this.firstNameLikeFront = firstNameLikeFront;
    }

    /**
     * Getter method of "middleNameLikeFront".
     * 
     * @return the "middleNameLikeFront"
     */
    public String getMiddleNameLikeFront() {
        return middleNameLikeFront;
    }

    /**
     * Setter method of "middleNameLikeFront".
     * 
     * @param middleNameLikeFront Set in "middleNameLikeFront".
     */
    public void setMiddleNameLikeFront(String middleNameLikeFront) {
        this.middleNameLikeFront = middleNameLikeFront;
    }

    /**
     * Getter method of "lastNameLikeFront".
     * 
     * @return the "lastNameLikeFront"
     */
    public String getLastNameLikeFront() {
        return lastNameLikeFront;
    }

    /**
     * Setter method of "lastNameLikeFront".
     * 
     * @param lastNameLikeFront Set in "lastNameLikeFront".
     */
    public void setLastNameLikeFront(String lastNameLikeFront) {
        this.lastNameLikeFront = lastNameLikeFront;
    }

    /**
     * Getter method of "emailLikeFront".
     * 
     * @return the "emailLikeFront"
     */
    public String getEmailLikeFront() {
        return emailLikeFront;
    }

    /**
     * Setter method of "emailLikeFront".
     * 
     * @param emailLikeFront Set in "emailLikeFront".
     */
    public void setEmailLikeFront(String emailLikeFront) {
        this.emailLikeFront = emailLikeFront;
    }

    /**
     * Getter method of "telephoneLikeFront".
     * 
     * @return the "telephoneLikeFront"
     */
    public String getTelephoneLikeFront() {
        return telephoneLikeFront;
    }

    /**
     * Setter method of "telephoneLikeFront".
     * 
     * @param telephoneLikeFront Set in "telephoneLikeFront".
     */
    public void setTelephoneLikeFront(String telephoneLikeFront) {
        this.telephoneLikeFront = telephoneLikeFront;
    }

    /**
     * Getter method of "emlCreateCancelAsnFlagLikeFront".
     * 
     * @return the "emlCreateCancelAsnFlagLikeFront"
     */
    public String getEmlCreateCancelAsnFlagLikeFront() {
        return emlCreateCancelAsnFlagLikeFront;
    }

    /**
     * Setter method of "emlCreateCancelAsnFlagLikeFront".
     * 
     * @param emlCreateCancelAsnFlagLikeFront Set in "emlCreateCancelAsnFlagLikeFront".
     */
    public void setEmlCreateCancelAsnFlagLikeFront(String emlCreateCancelAsnFlagLikeFront) {
        this.emlCreateCancelAsnFlagLikeFront = emlCreateCancelAsnFlagLikeFront;
    }

    /**
     * Getter method of "emlReviseAsnFlagLikeFront".
     * 
     * @return the "emlReviseAsnFlagLikeFront"
     */
    public String getEmlReviseAsnFlagLikeFront() {
        return emlReviseAsnFlagLikeFront;
    }

    /**
     * Setter method of "emlReviseAsnFlagLikeFront".
     * 
     * @param emlReviseAsnFlagLikeFront Set in "emlReviseAsnFlagLikeFront".
     */
    public void setEmlReviseAsnFlagLikeFront(String emlReviseAsnFlagLikeFront) {
        this.emlReviseAsnFlagLikeFront = emlReviseAsnFlagLikeFront;
    }

    /**
     * Getter method of "emlCreateInvoiceFlagLikeFront".
     * 
     * @return the "emlCreateInvoiceFlagLikeFront"
     */
    public String getEmlCreateInvoiceFlagLikeFront() {
        return emlCreateInvoiceFlagLikeFront;
    }

    /**
     * Setter method of "emlCreateInvoiceFlagLikeFront".
     * 
     * @param emlCreateInvoiceFlagLikeFront Set in "emlCreateInvoiceFlagLikeFront".
     */
    public void setEmlCreateInvoiceFlagLikeFront(String emlCreateInvoiceFlagLikeFront) {
        this.emlCreateInvoiceFlagLikeFront = emlCreateInvoiceFlagLikeFront;
    }

    /**
     * Getter method of "emlPedpoDoalcasnUnmatpnFlgLikeFront".
     * 
     * @return the "emlPedpoDoalcasnUnmatpnFlgLikeFront"
     */
    public String getEmlPedpoDoalcasnUnmatpnFlgLikeFront() {
        return emlPedpoDoalcasnUnmatpnFlgLikeFront;
    }

    /**
     * Setter method of "emlPedpoDoalcasnUnmatpnFlgLikeFront".
     * 
     * @param emlPedpoDoalcasnUnmatpnFlgLikeFront Set in "emlPedpoDoalcasnUnmatpnFlgLikeFront".
     */
    public void setEmlPedpoDoalcasnUnmatpnFlgLikeFront(String emlPedpoDoalcasnUnmatpnFlgLikeFront) {
        this.emlPedpoDoalcasnUnmatpnFlgLikeFront = emlPedpoDoalcasnUnmatpnFlgLikeFront;
    }

    /**
     * Getter method of "emlAbnormalTransferFlgLikeFront".
     * 
     * @return the "emlAbnormalTransferFlgLikeFront"
     */
    public String getEmlAbnormalTransferFlgLikeFront() {
        return emlAbnormalTransferFlgLikeFront;
    }

    /**
     * Setter method of "emlAbnormalTransferFlgLikeFront".
     * 
     * @param emlAbnormalTransferFlgLikeFront Set in "emlAbnormalTransferFlgLikeFront".
     */
    public void setEmlAbnormalTransferFlgLikeFront(String emlAbnormalTransferFlgLikeFront) {
        this.emlAbnormalTransferFlgLikeFront = emlAbnormalTransferFlgLikeFront;
    }

    /**
     * Getter method of "roleCdLikeFront".
     * 
     * @return the "roleCdLikeFront"
     */
    public String getRoleCdLikeFront() {
        return roleCdLikeFront;
    }

    /**
     * Setter method of "roleCdLikeFront".
     * 
     * @param roleCdLikeFront Set in "roleCdLikeFront".
     */
    public void setRoleCdLikeFront(String roleCdLikeFront) {
        this.roleCdLikeFront = roleCdLikeFront;
    }

    /**
     * Getter method of "roleDCdLikeFront".
     * 
     * @return the "roleDCdLikeFront"
     */
    public String getRoleDCdLikeFront() {
        return roleDCdLikeFront;
    }

    /**
     * Setter method of "roleDCdLikeFront".
     * 
     * @param roleDCdLikeFront Set in "roleDCdLikeFront".
     */
    public void setRoleDCdLikeFront(String roleDCdLikeFront) {
        this.roleDCdLikeFront = roleDCdLikeFront;
    }

    /**
     * Getter method of "roleDPcdLikeFront".
     * 
     * @return the "roleDPcdLikeFront"
     */
    public String getRoleDPcdLikeFront() {
        return roleDPcdLikeFront;
    }

    /**
     * Setter method of "roleDPcdLikeFront".
     * 
     * @param roleDPcdLikeFront Set in "roleDPcdLikeFront".
     */
    public void setRoleDPcdLikeFront(String roleDPcdLikeFront) {
        this.roleDPcdLikeFront = roleDPcdLikeFront;
    }

    /**
     * Getter method of "effectStartGreaterThanEqual".
     * 
     * @return the "effectStartGreaterThanEqual"
     */
    public Timestamp getEffectStartGreaterThanEqual() {
        return effectStartGreaterThanEqual;
    }

    /**
     * Setter method of "effectStartGreaterThanEqual".
     * 
     * @param effectStartGreaterThanEqual Set in "effectStartGreaterThanEqual".
     */
    public void setEffectStartGreaterThanEqual(Timestamp effectStartGreaterThanEqual) {
        this.effectStartGreaterThanEqual = effectStartGreaterThanEqual;
    }

    /**
     * Getter method of "effectStartLessThanEqual".
     * 
     * @return the "effectStartLessThanEqual"
     */
    public Timestamp getEffectStartLessThanEqual() {
        return effectStartLessThanEqual;
    }

    /**
     * Setter method of "effectStartLessThanEqual".
     * 
     * @param effectStartLessThanEqual Set in "effectStartLessThanEqual".
     */
    public void setEffectStartLessThanEqual(Timestamp effectStartLessThanEqual) {
        this.effectStartLessThanEqual = effectStartLessThanEqual;
    }

    /**
     * Getter method of "effectEndGreaterThanEqual".
     * 
     * @return the "effectEndGreaterThanEqual"
     */
    public Timestamp getEffectEndGreaterThanEqual() {
        return effectEndGreaterThanEqual;
    }

    /**
     * Setter method of "effectEndGreaterThanEqual".
     * 
     * @param effectEndGreaterThanEqual Set in "effectEndGreaterThanEqual".
     */
    public void setEffectEndGreaterThanEqual(Timestamp effectEndGreaterThanEqual) {
        this.effectEndGreaterThanEqual = effectEndGreaterThanEqual;
    }

    /**
     * Getter method of "effectEndLessThanEqual".
     * 
     * @return the "effectEndLessThanEqual"
     */
    public Timestamp getEffectEndLessThanEqual() {
        return effectEndLessThanEqual;
    }

    /**
     * Setter method of "effectEndLessThanEqual".
     * 
     * @param effectEndLessThanEqual Set in "effectEndLessThanEqual".
     */
    public void setEffectEndLessThanEqual(Timestamp effectEndLessThanEqual) {
        this.effectEndLessThanEqual = effectEndLessThanEqual;
    }

    /**
     * Getter method of "informationFlagLikeFront".
     * 
     * @return the "informationFlagLikeFront"
     */
    public String getInformationFlagLikeFront() {
        return informationFlagLikeFront;
    }

    /**
     * Setter method of "informationFlagLikeFront".
     * 
     * @param informationFlagLikeFront Set in "informationFlagLikeFront".
     */
    public void setInformationFlagLikeFront(String informationFlagLikeFront) {
        this.informationFlagLikeFront = informationFlagLikeFront;
    }

    /**
     * Getter method of "roleFlagLikeFront".
     * 
     * @return the "roleFlagLikeFront"
     */
    public String getRoleFlagLikeFront() {
        return roleFlagLikeFront;
    }

    /**
     * Setter method of "roleFlagLikeFront".
     * 
     * @param roleFlagLikeFront Set in "roleFlagLikeFront".
     */
    public void setRoleFlagLikeFront(String roleFlagLikeFront) {
        this.roleFlagLikeFront = roleFlagLikeFront;
    }

    /**
     * Getter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @return the "uploadDatetimeGreaterThanEqual"
     */
    public Timestamp getUploadDatetimeGreaterThanEqual() {
        return uploadDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @param uploadDatetimeGreaterThanEqual Set in "uploadDatetimeGreaterThanEqual".
     */
    public void setUploadDatetimeGreaterThanEqual(Timestamp uploadDatetimeGreaterThanEqual) {
        this.uploadDatetimeGreaterThanEqual = uploadDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeLessThanEqual".
     * 
     * @return the "uploadDatetimeLessThanEqual"
     */
    public Timestamp getUploadDatetimeLessThanEqual() {
        return uploadDatetimeLessThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeLessThanEqual".
     * 
     * @param uploadDatetimeLessThanEqual Set in "uploadDatetimeLessThanEqual".
     */
    public void setUploadDatetimeLessThanEqual(Timestamp uploadDatetimeLessThanEqual) {
        this.uploadDatetimeLessThanEqual = uploadDatetimeLessThanEqual;
    }

    /**
     * Getter method of "userLastUpdateGreaterThanEqual".
     * 
     * @return the "userLastUpdateGreaterThanEqual"
     */
    public Timestamp getUserLastUpdateGreaterThanEqual() {
        return userLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "userLastUpdateGreaterThanEqual".
     * 
     * @param userLastUpdateGreaterThanEqual Set in "userLastUpdateGreaterThanEqual".
     */
    public void setUserLastUpdateGreaterThanEqual(Timestamp userLastUpdateGreaterThanEqual) {
        this.userLastUpdateGreaterThanEqual = userLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "userLastUpdateLessThanEqual".
     * 
     * @return the "userLastUpdateLessThanEqual"
     */
    public Timestamp getUserLastUpdateLessThanEqual() {
        return userLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "userLastUpdateLessThanEqual".
     * 
     * @param userLastUpdateLessThanEqual Set in "userLastUpdateLessThanEqual".
     */
    public void setUserLastUpdateLessThanEqual(Timestamp userLastUpdateLessThanEqual) {
        this.userLastUpdateLessThanEqual = userLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "densoLastUpdateGreaterThanEqual".
     * 
     * @return the "densoLastUpdateGreaterThanEqual"
     */
    public Timestamp getDensoLastUpdateGreaterThanEqual() {
        return densoLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "densoLastUpdateGreaterThanEqual".
     * 
     * @param densoLastUpdateGreaterThanEqual Set in "densoLastUpdateGreaterThanEqual".
     */
    public void setDensoLastUpdateGreaterThanEqual(Timestamp densoLastUpdateGreaterThanEqual) {
        this.densoLastUpdateGreaterThanEqual = densoLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "densoLastUpdateLessThanEqual".
     * 
     * @return the "densoLastUpdateLessThanEqual"
     */
    public Timestamp getDensoLastUpdateLessThanEqual() {
        return densoLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "densoLastUpdateLessThanEqual".
     * 
     * @param densoLastUpdateLessThanEqual Set in "densoLastUpdateLessThanEqual".
     */
    public void setDensoLastUpdateLessThanEqual(Timestamp densoLastUpdateLessThanEqual) {
        this.densoLastUpdateLessThanEqual = densoLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "roleLastUpdateGreaterThanEqual".
     * 
     * @return the "roleLastUpdateGreaterThanEqual"
     */
    public Timestamp getRoleLastUpdateGreaterThanEqual() {
        return roleLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "roleLastUpdateGreaterThanEqual".
     * 
     * @param roleLastUpdateGreaterThanEqual Set in "roleLastUpdateGreaterThanEqual".
     */
    public void setRoleLastUpdateGreaterThanEqual(Timestamp roleLastUpdateGreaterThanEqual) {
        this.roleLastUpdateGreaterThanEqual = roleLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "roleLastUpdateLessThanEqual".
     * 
     * @return the "roleLastUpdateLessThanEqual"
     */
    public Timestamp getRoleLastUpdateLessThanEqual() {
        return roleLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "roleLastUpdateLessThanEqual".
     * 
     * @param roleLastUpdateLessThanEqual Set in "roleLastUpdateLessThanEqual".
     */
    public void setRoleLastUpdateLessThanEqual(Timestamp roleLastUpdateLessThanEqual) {
        this.roleLastUpdateLessThanEqual = roleLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "isActualRegisterLikeFront".
     * 
     * @return the "isActualRegisterLikeFront"
     */
    public String getIsActualRegisterLikeFront() {
        return isActualRegisterLikeFront;
    }

    /**
     * Setter method of "isActualRegisterLikeFront".
     * 
     * @param isActualRegisterLikeFront Set in "isActualRegisterLikeFront".
     */
    public void setIsActualRegisterLikeFront(String isActualRegisterLikeFront) {
        this.isActualRegisterLikeFront = isActualRegisterLikeFront;
    }

    /**
     * Getter method of "toActualDatetimeGreaterThanEqual".
     * 
     * @return the "toActualDatetimeGreaterThanEqual"
     */
    public Timestamp getToActualDatetimeGreaterThanEqual() {
        return toActualDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "toActualDatetimeGreaterThanEqual".
     * 
     * @param toActualDatetimeGreaterThanEqual Set in "toActualDatetimeGreaterThanEqual".
     */
    public void setToActualDatetimeGreaterThanEqual(Timestamp toActualDatetimeGreaterThanEqual) {
        this.toActualDatetimeGreaterThanEqual = toActualDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "toActualDatetimeLessThanEqual".
     * 
     * @return the "toActualDatetimeLessThanEqual"
     */
    public Timestamp getToActualDatetimeLessThanEqual() {
        return toActualDatetimeLessThanEqual;
    }

    /**
     * Setter method of "toActualDatetimeLessThanEqual".
     * 
     * @param toActualDatetimeLessThanEqual Set in "toActualDatetimeLessThanEqual".
     */
    public void setToActualDatetimeLessThanEqual(Timestamp toActualDatetimeLessThanEqual) {
        this.toActualDatetimeLessThanEqual = toActualDatetimeLessThanEqual;
    }

}
