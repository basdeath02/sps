/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/27       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMUserSupplier"<br />
 * Table overview: SPS_M_USER_SUPPLIER<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/27 17:41:40<br />
 * 
 * This module generated automatically in 2015/03/27 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserSupplierDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User identify number
     */
    private String dscId;

    /**
     * Main Desnso company to handle this record (feture use)
     */
    private String dOwner;

    /**
     * User belong to Supplier plant
     */
    private String sPcd;

    /**
     * User belong to Supplier company
     */
    private String sCd;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlUrgentOrderFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlSInfoNotfoundFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlAllowReviseFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic.
     */
    private String emlCancelInvoiceFlag;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMUserSupplierDomain() {
    }

    /**
     * Getter method of "dscId"
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId"
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dOwner"
     * 
     * @return the dOwner
     */
    public String getDOwner() {
        return dOwner;
    }

    /**
     * Setter method of "dOwner"
     * 
     * @param dOwner Set in "dOwner".
     */
    public void setDOwner(String dOwner) {
        this.dOwner = dOwner;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "emlUrgentOrderFlag"
     * 
     * @return the emlUrgentOrderFlag
     */
    public String getEmlUrgentOrderFlag() {
        return emlUrgentOrderFlag;
    }

    /**
     * Setter method of "emlUrgentOrderFlag"
     * 
     * @param emlUrgentOrderFlag Set in "emlUrgentOrderFlag".
     */
    public void setEmlUrgentOrderFlag(String emlUrgentOrderFlag) {
        this.emlUrgentOrderFlag = emlUrgentOrderFlag;
    }

    /**
     * Getter method of "emlSInfoNotfoundFlag"
     * 
     * @return the emlSInfoNotfoundFlag
     */
    public String getEmlSInfoNotfoundFlag() {
        return emlSInfoNotfoundFlag;
    }

    /**
     * Setter method of "emlSInfoNotfoundFlag"
     * 
     * @param emlSInfoNotfoundFlag Set in "emlSInfoNotfoundFlag".
     */
    public void setEmlSInfoNotfoundFlag(String emlSInfoNotfoundFlag) {
        this.emlSInfoNotfoundFlag = emlSInfoNotfoundFlag;
    }

    /**
     * Getter method of "emlAllowReviseFlag"
     * 
     * @return the emlAllowReviseFlag
     */
    public String getEmlAllowReviseFlag() {
        return emlAllowReviseFlag;
    }

    /**
     * Setter method of "emlAllowReviseFlag"
     * 
     * @param emlAllowReviseFlag Set in "emlAllowReviseFlag".
     */
    public void setEmlAllowReviseFlag(String emlAllowReviseFlag) {
        this.emlAllowReviseFlag = emlAllowReviseFlag;
    }

    /**
     * Getter method of "emlCancelInvoiceFlag"
     * 
     * @return the emlCancelInvoiceFlag
     */
    public String getEmlCancelInvoiceFlag() {
        return emlCancelInvoiceFlag;
    }

    /**
     * Setter method of "emlCancelInvoiceFlag"
     * 
     * @param emlCancelInvoiceFlag Set in "emlCancelInvoiceFlag".
     */
    public void setEmlCancelInvoiceFlag(String emlCancelInvoiceFlag) {
        this.emlCancelInvoiceFlag = emlCancelInvoiceFlag;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
