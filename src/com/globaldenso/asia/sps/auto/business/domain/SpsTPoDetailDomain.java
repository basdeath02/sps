/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/31       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTPoDetail"<br />
 * Table overview: SPS_T_PO_DETAIL<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/31 15:36:38<br />
 * 
 * This module generated automatically in 2015/03/31 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDetailDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AUTONUMBER from SPS_T_PO
     */
    private BigDecimal poId;

    /**
     * Supplier Part No
     */
    private String sPn;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * P/O Status by Part No
ISS : Issued
PED : Pending
ACK : Acknowledge
FAC : Force Acknowledge
     */
    private String pnStatus;

    /**
     * Item Description of Part No
     */
    private String itemDesc;

    /**
     * QTY/BOX
     */
    private BigDecimal qtyBox;

    /**
     * Unit Price
     */
    private BigDecimal unitPrice;

    /**
     * Temp Price Flag
     */
    private String tmpPriceFlg;

    /**
     * Currency Code
     */
    private String currencyCode;

    /**
     * Part Label Print Remarks
     */
    private String pnLblPrintRemark;

    /**
     * Planner Code
     */
    private String plannerCode;

    /**
     * Warehouse for prime receiving
     */
    private String whPrimeReceiving;

    /**
     * Warehouse locastion
     */
    private String location;

    /**
     * Phase In/Out Code
     */
    private String phaseCode;

    /**
     * Receiving Dock
     */
    private String receivingDock;

    /**
     * Variable Qty Code
     */
    private String variableQtyCode;

    /**
     * Order Lot
     */
    private BigDecimal orderLot;

    /**
     * Receiving Lane
     */
    private String rcvLane;

    /**
     * Start Period Date of P/O
     */
    private Date startPeriodDate;

    /**
     * End Period Date of P/O
     */
    private Date endPeriodDate;

    /**
     * End Period Type FIRM of P/O
     */
    private Date endFirmDate;

    /**
     * Unit of Measure
     */
    private String unitOfMeasure;

    /**
     * Engineer Drawing No. (Model)
     */
    private String model;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsTPoDetailDomain() {
    }

    /**
     * Getter method of "poId"
     * 
     * @return the poId
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId"
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "pnStatus"
     * 
     * @return the pnStatus
     */
    public String getPnStatus() {
        return pnStatus;
    }

    /**
     * Setter method of "pnStatus"
     * 
     * @param pnStatus Set in "pnStatus".
     */
    public void setPnStatus(String pnStatus) {
        this.pnStatus = pnStatus;
    }

    /**
     * Getter method of "itemDesc"
     * 
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Setter method of "itemDesc"
     * 
     * @param itemDesc Set in "itemDesc".
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * Getter method of "qtyBox"
     * 
     * @return the qtyBox
     */
    public BigDecimal getQtyBox() {
        return qtyBox;
    }

    /**
     * Setter method of "qtyBox"
     * 
     * @param qtyBox Set in "qtyBox".
     */
    public void setQtyBox(BigDecimal qtyBox) {
        this.qtyBox = qtyBox;
    }

    /**
     * Getter method of "unitPrice"
     * 
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * Setter method of "unitPrice"
     * 
     * @param unitPrice Set in "unitPrice".
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * Getter method of "tmpPriceFlg"
     * 
     * @return the tmpPriceFlg
     */
    public String getTmpPriceFlg() {
        return tmpPriceFlg;
    }

    /**
     * Setter method of "tmpPriceFlg"
     * 
     * @param tmpPriceFlg Set in "tmpPriceFlg".
     */
    public void setTmpPriceFlg(String tmpPriceFlg) {
        this.tmpPriceFlg = tmpPriceFlg;
    }

    /**
     * Getter method of "currencyCode"
     * 
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Setter method of "currencyCode"
     * 
     * @param currencyCode Set in "currencyCode".
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Getter method of "pnLblPrintRemark"
     * 
     * @return the pnLblPrintRemark
     */
    public String getPnLblPrintRemark() {
        return pnLblPrintRemark;
    }

    /**
     * Setter method of "pnLblPrintRemark"
     * 
     * @param pnLblPrintRemark Set in "pnLblPrintRemark".
     */
    public void setPnLblPrintRemark(String pnLblPrintRemark) {
        this.pnLblPrintRemark = pnLblPrintRemark;
    }

    /**
     * Getter method of "plannerCode"
     * 
     * @return the plannerCode
     */
    public String getPlannerCode() {
        return plannerCode;
    }

    /**
     * Setter method of "plannerCode"
     * 
     * @param plannerCode Set in "plannerCode".
     */
    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }

    /**
     * Getter method of "whPrimeReceiving"
     * 
     * @return the whPrimeReceiving
     */
    public String getWhPrimeReceiving() {
        return whPrimeReceiving;
    }

    /**
     * Setter method of "whPrimeReceiving"
     * 
     * @param whPrimeReceiving Set in "whPrimeReceiving".
     */
    public void setWhPrimeReceiving(String whPrimeReceiving) {
        this.whPrimeReceiving = whPrimeReceiving;
    }

    /**
     * Getter method of "location"
     * 
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Setter method of "location"
     * 
     * @param location Set in "location".
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Getter method of "phaseCode"
     * 
     * @return the phaseCode
     */
    public String getPhaseCode() {
        return phaseCode;
    }

    /**
     * Setter method of "phaseCode"
     * 
     * @param phaseCode Set in "phaseCode".
     */
    public void setPhaseCode(String phaseCode) {
        this.phaseCode = phaseCode;
    }

    /**
     * Getter method of "receivingDock"
     * 
     * @return the receivingDock
     */
    public String getReceivingDock() {
        return receivingDock;
    }

    /**
     * Setter method of "receivingDock"
     * 
     * @param receivingDock Set in "receivingDock".
     */
    public void setReceivingDock(String receivingDock) {
        this.receivingDock = receivingDock;
    }

    /**
     * Getter method of "variableQtyCode"
     * 
     * @return the variableQtyCode
     */
    public String getVariableQtyCode() {
        return variableQtyCode;
    }

    /**
     * Setter method of "variableQtyCode"
     * 
     * @param variableQtyCode Set in "variableQtyCode".
     */
    public void setVariableQtyCode(String variableQtyCode) {
        this.variableQtyCode = variableQtyCode;
    }

    /**
     * Getter method of "orderLot"
     * 
     * @return the orderLot
     */
    public BigDecimal getOrderLot() {
        return orderLot;
    }

    /**
     * Setter method of "orderLot"
     * 
     * @param orderLot Set in "orderLot".
     */
    public void setOrderLot(BigDecimal orderLot) {
        this.orderLot = orderLot;
    }

    /**
     * Getter method of "rcvLane"
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Setter method of "rcvLane"
     * 
     * @param rcvLane Set in "rcvLane".
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Getter method of "startPeriodDate"
     * 
     * @return the startPeriodDate
     */
    public Date getStartPeriodDate() {
        return startPeriodDate;
    }

    /**
     * Setter method of "startPeriodDate"
     * 
     * @param startPeriodDate Set in "startPeriodDate".
     */
    public void setStartPeriodDate(Date startPeriodDate) {
        this.startPeriodDate = startPeriodDate;
    }

    /**
     * Getter method of "endPeriodDate"
     * 
     * @return the endPeriodDate
     */
    public Date getEndPeriodDate() {
        return endPeriodDate;
    }

    /**
     * Setter method of "endPeriodDate"
     * 
     * @param endPeriodDate Set in "endPeriodDate".
     */
    public void setEndPeriodDate(Date endPeriodDate) {
        this.endPeriodDate = endPeriodDate;
    }

    /**
     * Getter method of "endFirmDate"
     * 
     * @return the endFirmDate
     */
    public Date getEndFirmDate() {
        return endFirmDate;
    }

    /**
     * Setter method of "endFirmDate"
     * 
     * @param endFirmDate Set in "endFirmDate".
     */
    public void setEndFirmDate(Date endFirmDate) {
        this.endFirmDate = endFirmDate;
    }

    /**
     * Getter method of "unitOfMeasure"
     * 
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Setter method of "unitOfMeasure"
     * 
     * @param unitOfMeasure Set in "unitOfMeasure".
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter method of "model"
     * 
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * Setter method of "model"
     * 
     * @param model Set in "model".
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
