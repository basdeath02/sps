/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * @author PC-SAMSUNG
 *
 */
public class SpsMDensoSupplierRelationWithPODNDomain   {
	private static final long serialVersionUID = 1L;
	private String dCd;
	private String sCd;
	private String dPcd;
	private String sPcd;
	private String createDscId;
	private String createDatetime;
	private String lastUpdateDscId;
	private String lastUpdateDatetime;
	private String dnDnFlg;
	private String email;
	private List<SPSTPoHeaderDNDomain> listPO;
	public String getdCd() {
		return dCd;
	}
	public void setdCd(String dCd) {
		this.dCd = dCd;
	}
	public String getsCd() {
		return sCd;
	}
	public void setsCd(String sCd) {
		this.sCd = sCd;
	}
	public String getdPcd() {
		return dPcd;
	}
	public void setdPcd(String dPcd) {
		this.dPcd = dPcd;
	}
	public String getsPcd() {
		return sPcd;
	}
	public void setsPcd(String sPcd) {
		this.sPcd = sPcd;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	public String getCreateDatetime() {
		return createDatetime;
	}
	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	public String getLastUpdateDatetime() {
		return lastUpdateDatetime;
	}
	public void setLastUpdateDatetime(String lastUpdateDatetime) {
		this.lastUpdateDatetime = lastUpdateDatetime;
	}
	public String getDnDnFlg() {
		return dnDnFlg;
	}
	public void setDnDnFlg(String dnDnFlg) {
		this.dnDnFlg = dnDnFlg;
	}
	
	
	public List<SPSTPoHeaderDNDomain> getListPO() {
		return listPO;
	}
	public void setListPO(List<SPSTPoHeaderDNDomain> listPO) {
		this.listPO = listPO;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
