/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTPoCoverPageDetail".<br />
 * Table overview: SPS_T_PO_COVER_PAGE_DETAIL<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageDetailCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * PO_ID
     */
    private BigDecimal poId;

    /**
     * RUNNING_DO_ID
     */
    private BigDecimal runningDoId;

    /**
     * DO_DUE_DATE
     */
    private Date doDueDate;

    /**
     * CIGMA_DO_NO
     */
    private String cigmaDoNo;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * DO_DUE_DATE(condition whether the column value is greater than or equal to the value)
     */
    private Date doDueDateGreaterThanEqual;

    /**
     * DO_DUE_DATE(condition whether the column value is less than or equal to the value)
     */
    private Date doDueDateLessThanEqual;

    /**
     * CIGMA_DO_NO(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaDoNoLikeFront;

    /**
     * CREATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * CREATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * CREATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * LAST_UPDATE_DSC_ID(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * LAST_UPDATE_DATETIME(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTPoCoverPageDetailCriteriaDomain() {
    }

    /**
     * Getter method of "poId".
     * 
     * @return the "poId"
     */
    public BigDecimal getPoId() {
        return poId;
    }

    /**
     * Setter method of "poId".
     * 
     * @param poId Set in "poId".
     */
    public void setPoId(BigDecimal poId) {
        this.poId = poId;
    }

    /**
     * Getter method of "runningDoId".
     * 
     * @return the "runningDoId"
     */
    public BigDecimal getRunningDoId() {
        return runningDoId;
    }

    /**
     * Setter method of "runningDoId".
     * 
     * @param runningDoId Set in "runningDoId".
     */
    public void setRunningDoId(BigDecimal runningDoId) {
        this.runningDoId = runningDoId;
    }

    /**
     * Getter method of "doDueDate".
     * 
     * @return the "doDueDate"
     */
    public Date getDoDueDate() {
        return doDueDate;
    }

    /**
     * Setter method of "doDueDate".
     * 
     * @param doDueDate Set in "doDueDate".
     */
    public void setDoDueDate(Date doDueDate) {
        this.doDueDate = doDueDate;
    }

    /**
     * Getter method of "cigmaDoNo".
     * 
     * @return the "cigmaDoNo"
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo".
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "doDueDateGreaterThanEqual".
     * 
     * @return the "doDueDateGreaterThanEqual"
     */
    public Date getDoDueDateGreaterThanEqual() {
        return doDueDateGreaterThanEqual;
    }

    /**
     * Setter method of "doDueDateGreaterThanEqual".
     * 
     * @param doDueDateGreaterThanEqual Set in "doDueDateGreaterThanEqual".
     */
    public void setDoDueDateGreaterThanEqual(Date doDueDateGreaterThanEqual) {
        this.doDueDateGreaterThanEqual = doDueDateGreaterThanEqual;
    }

    /**
     * Getter method of "doDueDateLessThanEqual".
     * 
     * @return the "doDueDateLessThanEqual"
     */
    public Date getDoDueDateLessThanEqual() {
        return doDueDateLessThanEqual;
    }

    /**
     * Setter method of "doDueDateLessThanEqual".
     * 
     * @param doDueDateLessThanEqual Set in "doDueDateLessThanEqual".
     */
    public void setDoDueDateLessThanEqual(Date doDueDateLessThanEqual) {
        this.doDueDateLessThanEqual = doDueDateLessThanEqual;
    }

    /**
     * Getter method of "cigmaDoNoLikeFront".
     * 
     * @return the "cigmaDoNoLikeFront"
     */
    public String getCigmaDoNoLikeFront() {
        return cigmaDoNoLikeFront;
    }

    /**
     * Setter method of "cigmaDoNoLikeFront".
     * 
     * @param cigmaDoNoLikeFront Set in "cigmaDoNoLikeFront".
     */
    public void setCigmaDoNoLikeFront(String cigmaDoNoLikeFront) {
        this.cigmaDoNoLikeFront = cigmaDoNoLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
