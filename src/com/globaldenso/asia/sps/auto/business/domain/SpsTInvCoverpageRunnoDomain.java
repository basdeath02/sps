/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/18       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTInvCoverpageRunno"<br />
 * Table overview: SPS_T_INV_COVERPAGE_RUNNO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/18 14:44:35<br />
 * 
 * This module generated automatically in 2015/05/18 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvCoverpageRunnoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Year of running number
     */
    private String runningYear;

    /**
     * Month of running number
     */
    private String runningMonth;

    /**
     * date of running number
     */
    private String runningDate;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * Running Number
     */
    private BigDecimal runno;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * Default constructor
     */
    public SpsTInvCoverpageRunnoDomain() {
    }

    /**
     * Getter method of "runningYear"
     * 
     * @return the runningYear
     */
    public String getRunningYear() {
        return runningYear;
    }

    /**
     * Setter method of "runningYear"
     * 
     * @param runningYear Set in "runningYear".
     */
    public void setRunningYear(String runningYear) {
        this.runningYear = runningYear;
    }

    /**
     * Getter method of "runningMonth"
     * 
     * @return the runningMonth
     */
    public String getRunningMonth() {
        return runningMonth;
    }

    /**
     * Setter method of "runningMonth"
     * 
     * @param runningMonth Set in "runningMonth".
     */
    public void setRunningMonth(String runningMonth) {
        this.runningMonth = runningMonth;
    }

    /**
     * Getter method of "runningDate"
     * 
     * @return the runningDate
     */
    public String getRunningDate() {
        return runningDate;
    }

    /**
     * Setter method of "runningDate"
     * 
     * @param runningDate Set in "runningDate".
     */
    public void setRunningDate(String runningDate) {
        this.runningDate = runningDate;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "runno"
     * 
     * @return the runno
     */
    public BigDecimal getRunno() {
        return runno;
    }

    /**
     * Setter method of "runno"
     * 
     * @param runno Set in "runno".
     */
    public void setRunno(BigDecimal runno) {
        this.runno = runno;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

}
