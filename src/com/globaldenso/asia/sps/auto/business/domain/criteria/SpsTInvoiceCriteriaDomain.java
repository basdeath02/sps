/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/07/09       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTInvoice".<br />
 * Table overview: SPS_T_INVOICE<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/07/09 18:24:35<br />
 * 
 * This module generated automatically in 2015/07/09 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running number of Invoice
     */
    private BigDecimal invoiceId;

    /**
     * DENSO Company Name
     */
    private String dCompanyName;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * DENSO TAX Identify
     */
    private String dTaxId;

    /**
     * DENSO Plant Address
     */
    private String dpAddress;

    /**
     * DENSO Currency Code
     */
    private String dCurrencyCd;

    /**
     * Supplier Company Name
     */
    private String sCompanyName;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * Supplier Company Code
     */
    private String sCd;

    /**
     * Supplier TAX Identify
     */
    private String sTaxId;

    /**
     * Supplier Company Address 1
     */
    private String sAddress1;

    /**
     * Supplier Company Address 2
     */
    private String sAddress2;

    /**
     * Supplier Company Address 3
     */
    private String sAddress3;

    /**
     * Supplier Currency Code
     */
    private String sCurrencyCd;

    /**
     * Invoice Number
     */
    private String invoiceNo;

    /**
     * Invoice Issued Date
     */
    private Date invoiceDate;

    /**
     * ISS : Issued, ISC : Issued with CN, CCL : Cancel Invoice, PGL : Post to GL, HOP : Hold payment, PDP : Paid
     */
    private String invoiceStatus;

    /**
     * "1": VAT/GST , "0": None VAT/GST
     */
    private String vatType;

    /**
     * Invoice VAT Rate 
     */
    private BigDecimal vatRate;

    /**
     * Invoice Base Amount
     */
    private BigDecimal sBaseAmount;

    /**
     * Invoice VAT Amount
     */
    private BigDecimal sVatAmount;

    /**
     * Invoice Total Amount
     */
    private BigDecimal sTotalAmount;

    /**
     * Difference Base Amount of DENSO price and Supplier price
     */
    private BigDecimal totalDiffBaseAmount;

    /**
     * Difference Total Amount of DENSO price and Supplier price
     */
    private BigDecimal totalDiffAmount;

    /**
     * Invoice Base Amount
     */
    private BigDecimal totalBaseAmount;

    /**
     * Difference VAT Amount of DENSO price and Supplier price
     */
    private BigDecimal totalDiffVatAmount;

    /**
     * Invoice VAT Amount
     */
    private BigDecimal totalVatAmount;

    /**
     * Invoice Total Amount
     */
    private BigDecimal totalAmount;

    /**
     * Payment Date from JDE
     */
    private Date paymentDate;

    /**
     * "1": Already transfer to JDE , "0": not yet transfer
     */
    private String transferJdeFlag;

    /**
     * Cover Page Document File ID in File Manager
     */
    private String coverPageFileId;

    /**
     * Cover Page No
     */
    private String coverPageNo;

    /**
     * DSC ID of Create User
     */
    private String createDscId;

    /**
     * Datetime when created record
     */
    private Timestamp createDatetime;

    /**
     * DSC ID of Update User
     */
    private String lastUpdateDscId;

    /**
     * Datetime when latest updated record
     */
    private Timestamp lastUpdateDatetime;

    /**
     * DENSO Company Name(condition whether the column value at the beginning is equal to the value)
     */
    private String dCompanyNameLikeFront;

    /**
     * DENSO Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * DENSO TAX Identify(condition whether the column value at the beginning is equal to the value)
     */
    private String dTaxIdLikeFront;

    /**
     * DENSO Plant Address(condition whether the column value at the beginning is equal to the value)
     */
    private String dpAddressLikeFront;

    /**
     * DENSO Currency Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCurrencyCdLikeFront;

    /**
     * Supplier Company Name(condition whether the column value at the beginning is equal to the value)
     */
    private String sCompanyNameLikeFront;

    /**
     * AS/400 Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * Supplier Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier TAX Identify(condition whether the column value at the beginning is equal to the value)
     */
    private String sTaxIdLikeFront;

    /**
     * Supplier Company Address 1(condition whether the column value at the beginning is equal to the value)
     */
    private String sAddress1LikeFront;

    /**
     * Supplier Company Address 2(condition whether the column value at the beginning is equal to the value)
     */
    private String sAddress2LikeFront;

    /**
     * Supplier Company Address 3(condition whether the column value at the beginning is equal to the value)
     */
    private String sAddress3LikeFront;

    /**
     * Supplier Currency Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sCurrencyCdLikeFront;

    /**
     * Invoice Number(condition whether the column value at the beginning is equal to the value)
     */
    private String invoiceNoLikeFront;

    /**
     * Invoice Issued Date(condition whether the column value is greater than or equal to the value)
     */
    private Date invoiceDateGreaterThanEqual;

    /**
     * Invoice Issued Date(condition whether the column value is less than or equal to the value)
     */
    private Date invoiceDateLessThanEqual;

    /**
     * ISS : Issued, ISC : Issued with CN, CCL : Cancel Invoice, PGL : Post to GL, HOP : Hold payment, PDP : Paid(condition whether the column value at the beginning is equal to the value)
     */
    private String invoiceStatusLikeFront;

    /**
     * "1": VAT/GST , "0": None VAT/GST(condition whether the column value at the beginning is equal to the value)
     */
    private String vatTypeLikeFront;

    /**
     * Payment Date from JDE(condition whether the column value is greater than or equal to the value)
     */
    private Date paymentDateGreaterThanEqual;

    /**
     * Payment Date from JDE(condition whether the column value is less than or equal to the value)
     */
    private Date paymentDateLessThanEqual;

    /**
     * "1": Already transfer to JDE , "0": not yet transfer(condition whether the column value at the beginning is equal to the value)
     */
    private String transferJdeFlagLikeFront;

    /**
     * Cover Page Document File ID in File Manager(condition whether the column value at the beginning is equal to the value)
     */
    private String coverPageFileIdLikeFront;

    /**
     * Cover Page No(condition whether the column value at the beginning is equal to the value)
     */
    private String coverPageNoLikeFront;

    /**
     * DSC ID of Create User(condition whether the column value at the beginning is equal to the value)
     */
    private String createDscIdLikeFront;

    /**
     * Datetime when created record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Datetime when created record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * DSC ID of Update User(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateDscIdLikeFront;

    /**
     * Datetime when latest updated record(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Datetime when latest updated record(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTInvoiceCriteriaDomain() {
    }

    /**
     * Getter method of "invoiceId".
     * 
     * @return the "invoiceId"
     */
    public BigDecimal getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method of "invoiceId".
     * 
     * @param invoiceId Set in "invoiceId".
     */
    public void setInvoiceId(BigDecimal invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * Getter method of "dCompanyName".
     * 
     * @return the "dCompanyName"
     */
    public String getDCompanyName() {
        return dCompanyName;
    }

    /**
     * Setter method of "dCompanyName".
     * 
     * @param dCompanyName Set in "dCompanyName".
     */
    public void setDCompanyName(String dCompanyName) {
        this.dCompanyName = dCompanyName;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "dTaxId".
     * 
     * @return the "dTaxId"
     */
    public String getDTaxId() {
        return dTaxId;
    }

    /**
     * Setter method of "dTaxId".
     * 
     * @param dTaxId Set in "dTaxId".
     */
    public void setDTaxId(String dTaxId) {
        this.dTaxId = dTaxId;
    }

    /**
     * Getter method of "dpAddress".
     * 
     * @return the "dpAddress"
     */
    public String getDpAddress() {
        return dpAddress;
    }

    /**
     * Setter method of "dpAddress".
     * 
     * @param dpAddress Set in "dpAddress".
     */
    public void setDpAddress(String dpAddress) {
        this.dpAddress = dpAddress;
    }

    /**
     * Getter method of "dCurrencyCd".
     * 
     * @return the "dCurrencyCd"
     */
    public String getDCurrencyCd() {
        return dCurrencyCd;
    }

    /**
     * Setter method of "dCurrencyCd".
     * 
     * @param dCurrencyCd Set in "dCurrencyCd".
     */
    public void setDCurrencyCd(String dCurrencyCd) {
        this.dCurrencyCd = dCurrencyCd;
    }

    /**
     * Getter method of "sCompanyName".
     * 
     * @return the "sCompanyName"
     */
    public String getSCompanyName() {
        return sCompanyName;
    }

    /**
     * Setter method of "sCompanyName".
     * 
     * @param sCompanyName Set in "sCompanyName".
     */
    public void setSCompanyName(String sCompanyName) {
        this.sCompanyName = sCompanyName;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sTaxId".
     * 
     * @return the "sTaxId"
     */
    public String getSTaxId() {
        return sTaxId;
    }

    /**
     * Setter method of "sTaxId".
     * 
     * @param sTaxId Set in "sTaxId".
     */
    public void setSTaxId(String sTaxId) {
        this.sTaxId = sTaxId;
    }

    /**
     * Getter method of "sAddress1".
     * 
     * @return the "sAddress1"
     */
    public String getSAddress1() {
        return sAddress1;
    }

    /**
     * Setter method of "sAddress1".
     * 
     * @param sAddress1 Set in "sAddress1".
     */
    public void setSAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    /**
     * Getter method of "sAddress2".
     * 
     * @return the "sAddress2"
     */
    public String getSAddress2() {
        return sAddress2;
    }

    /**
     * Setter method of "sAddress2".
     * 
     * @param sAddress2 Set in "sAddress2".
     */
    public void setSAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    /**
     * Getter method of "sAddress3".
     * 
     * @return the "sAddress3"
     */
    public String getSAddress3() {
        return sAddress3;
    }

    /**
     * Setter method of "sAddress3".
     * 
     * @param sAddress3 Set in "sAddress3".
     */
    public void setSAddress3(String sAddress3) {
        this.sAddress3 = sAddress3;
    }

    /**
     * Getter method of "sCurrencyCd".
     * 
     * @return the "sCurrencyCd"
     */
    public String getSCurrencyCd() {
        return sCurrencyCd;
    }

    /**
     * Setter method of "sCurrencyCd".
     * 
     * @param sCurrencyCd Set in "sCurrencyCd".
     */
    public void setSCurrencyCd(String sCurrencyCd) {
        this.sCurrencyCd = sCurrencyCd;
    }

    /**
     * Getter method of "invoiceNo".
     * 
     * @return the "invoiceNo"
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Setter method of "invoiceNo".
     * 
     * @param invoiceNo Set in "invoiceNo".
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * Getter method of "invoiceDate".
     * 
     * @return the "invoiceDate"
     */
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Setter method of "invoiceDate".
     * 
     * @param invoiceDate Set in "invoiceDate".
     */
    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * Getter method of "invoiceStatus".
     * 
     * @return the "invoiceStatus"
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * Setter method of "invoiceStatus".
     * 
     * @param invoiceStatus Set in "invoiceStatus".
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * Getter method of "vatType".
     * 
     * @return the "vatType"
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * Setter method of "vatType".
     * 
     * @param vatType Set in "vatType".
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }

    /**
     * Getter method of "vatRate".
     * 
     * @return the "vatRate"
     */
    public BigDecimal getVatRate() {
        return vatRate;
    }

    /**
     * Setter method of "vatRate".
     * 
     * @param vatRate Set in "vatRate".
     */
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * Getter method of "sBaseAmount".
     * 
     * @return the "sBaseAmount"
     */
    public BigDecimal getSBaseAmount() {
        return sBaseAmount;
    }

    /**
     * Setter method of "sBaseAmount".
     * 
     * @param sBaseAmount Set in "sBaseAmount".
     */
    public void setSBaseAmount(BigDecimal sBaseAmount) {
        this.sBaseAmount = sBaseAmount;
    }

    /**
     * Getter method of "sVatAmount".
     * 
     * @return the "sVatAmount"
     */
    public BigDecimal getSVatAmount() {
        return sVatAmount;
    }

    /**
     * Setter method of "sVatAmount".
     * 
     * @param sVatAmount Set in "sVatAmount".
     */
    public void setSVatAmount(BigDecimal sVatAmount) {
        this.sVatAmount = sVatAmount;
    }

    /**
     * Getter method of "sTotalAmount".
     * 
     * @return the "sTotalAmount"
     */
    public BigDecimal getSTotalAmount() {
        return sTotalAmount;
    }

    /**
     * Setter method of "sTotalAmount".
     * 
     * @param sTotalAmount Set in "sTotalAmount".
     */
    public void setSTotalAmount(BigDecimal sTotalAmount) {
        this.sTotalAmount = sTotalAmount;
    }

    /**
     * Getter method of "totalDiffBaseAmount".
     * 
     * @return the "totalDiffBaseAmount"
     */
    public BigDecimal getTotalDiffBaseAmount() {
        return totalDiffBaseAmount;
    }

    /**
     * Setter method of "totalDiffBaseAmount".
     * 
     * @param totalDiffBaseAmount Set in "totalDiffBaseAmount".
     */
    public void setTotalDiffBaseAmount(BigDecimal totalDiffBaseAmount) {
        this.totalDiffBaseAmount = totalDiffBaseAmount;
    }

    /**
     * Getter method of "totalDiffAmount".
     * 
     * @return the "totalDiffAmount"
     */
    public BigDecimal getTotalDiffAmount() {
        return totalDiffAmount;
    }

    /**
     * Setter method of "totalDiffAmount".
     * 
     * @param totalDiffAmount Set in "totalDiffAmount".
     */
    public void setTotalDiffAmount(BigDecimal totalDiffAmount) {
        this.totalDiffAmount = totalDiffAmount;
    }

    /**
     * Getter method of "totalBaseAmount".
     * 
     * @return the "totalBaseAmount"
     */
    public BigDecimal getTotalBaseAmount() {
        return totalBaseAmount;
    }

    /**
     * Setter method of "totalBaseAmount".
     * 
     * @param totalBaseAmount Set in "totalBaseAmount".
     */
    public void setTotalBaseAmount(BigDecimal totalBaseAmount) {
        this.totalBaseAmount = totalBaseAmount;
    }

    /**
     * Getter method of "totalDiffVatAmount".
     * 
     * @return the "totalDiffVatAmount"
     */
    public BigDecimal getTotalDiffVatAmount() {
        return totalDiffVatAmount;
    }

    /**
     * Setter method of "totalDiffVatAmount".
     * 
     * @param totalDiffVatAmount Set in "totalDiffVatAmount".
     */
    public void setTotalDiffVatAmount(BigDecimal totalDiffVatAmount) {
        this.totalDiffVatAmount = totalDiffVatAmount;
    }

    /**
     * Getter method of "totalVatAmount".
     * 
     * @return the "totalVatAmount"
     */
    public BigDecimal getTotalVatAmount() {
        return totalVatAmount;
    }

    /**
     * Setter method of "totalVatAmount".
     * 
     * @param totalVatAmount Set in "totalVatAmount".
     */
    public void setTotalVatAmount(BigDecimal totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    /**
     * Getter method of "totalAmount".
     * 
     * @return the "totalAmount"
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Setter method of "totalAmount".
     * 
     * @param totalAmount Set in "totalAmount".
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Getter method of "paymentDate".
     * 
     * @return the "paymentDate"
     */
    public Date getPaymentDate() {
        return paymentDate;
    }

    /**
     * Setter method of "paymentDate".
     * 
     * @param paymentDate Set in "paymentDate".
     */
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * Getter method of "transferJdeFlag".
     * 
     * @return the "transferJdeFlag"
     */
    public String getTransferJdeFlag() {
        return transferJdeFlag;
    }

    /**
     * Setter method of "transferJdeFlag".
     * 
     * @param transferJdeFlag Set in "transferJdeFlag".
     */
    public void setTransferJdeFlag(String transferJdeFlag) {
        this.transferJdeFlag = transferJdeFlag;
    }

    /**
     * Getter method of "coverPageFileId".
     * 
     * @return the "coverPageFileId"
     */
    public String getCoverPageFileId() {
        return coverPageFileId;
    }

    /**
     * Setter method of "coverPageFileId".
     * 
     * @param coverPageFileId Set in "coverPageFileId".
     */
    public void setCoverPageFileId(String coverPageFileId) {
        this.coverPageFileId = coverPageFileId;
    }

    /**
     * Getter method of "coverPageNo".
     * 
     * @return the "coverPageNo"
     */
    public String getCoverPageNo() {
        return coverPageNo;
    }

    /**
     * Setter method of "coverPageNo".
     * 
     * @param coverPageNo Set in "coverPageNo".
     */
    public void setCoverPageNo(String coverPageNo) {
        this.coverPageNo = coverPageNo;
    }

    /**
     * Getter method of "createDscId".
     * 
     * @return the "createDscId"
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId".
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId".
     * 
     * @return the "lastUpdateDscId"
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId".
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dCompanyNameLikeFront".
     * 
     * @return the "dCompanyNameLikeFront"
     */
    public String getDCompanyNameLikeFront() {
        return dCompanyNameLikeFront;
    }

    /**
     * Setter method of "dCompanyNameLikeFront".
     * 
     * @param dCompanyNameLikeFront Set in "dCompanyNameLikeFront".
     */
    public void setDCompanyNameLikeFront(String dCompanyNameLikeFront) {
        this.dCompanyNameLikeFront = dCompanyNameLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "dTaxIdLikeFront".
     * 
     * @return the "dTaxIdLikeFront"
     */
    public String getDTaxIdLikeFront() {
        return dTaxIdLikeFront;
    }

    /**
     * Setter method of "dTaxIdLikeFront".
     * 
     * @param dTaxIdLikeFront Set in "dTaxIdLikeFront".
     */
    public void setDTaxIdLikeFront(String dTaxIdLikeFront) {
        this.dTaxIdLikeFront = dTaxIdLikeFront;
    }

    /**
     * Getter method of "dpAddressLikeFront".
     * 
     * @return the "dpAddressLikeFront"
     */
    public String getDpAddressLikeFront() {
        return dpAddressLikeFront;
    }

    /**
     * Setter method of "dpAddressLikeFront".
     * 
     * @param dpAddressLikeFront Set in "dpAddressLikeFront".
     */
    public void setDpAddressLikeFront(String dpAddressLikeFront) {
        this.dpAddressLikeFront = dpAddressLikeFront;
    }

    /**
     * Getter method of "dCurrencyCdLikeFront".
     * 
     * @return the "dCurrencyCdLikeFront"
     */
    public String getDCurrencyCdLikeFront() {
        return dCurrencyCdLikeFront;
    }

    /**
     * Setter method of "dCurrencyCdLikeFront".
     * 
     * @param dCurrencyCdLikeFront Set in "dCurrencyCdLikeFront".
     */
    public void setDCurrencyCdLikeFront(String dCurrencyCdLikeFront) {
        this.dCurrencyCdLikeFront = dCurrencyCdLikeFront;
    }

    /**
     * Getter method of "sCompanyNameLikeFront".
     * 
     * @return the "sCompanyNameLikeFront"
     */
    public String getSCompanyNameLikeFront() {
        return sCompanyNameLikeFront;
    }

    /**
     * Setter method of "sCompanyNameLikeFront".
     * 
     * @param sCompanyNameLikeFront Set in "sCompanyNameLikeFront".
     */
    public void setSCompanyNameLikeFront(String sCompanyNameLikeFront) {
        this.sCompanyNameLikeFront = sCompanyNameLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sTaxIdLikeFront".
     * 
     * @return the "sTaxIdLikeFront"
     */
    public String getSTaxIdLikeFront() {
        return sTaxIdLikeFront;
    }

    /**
     * Setter method of "sTaxIdLikeFront".
     * 
     * @param sTaxIdLikeFront Set in "sTaxIdLikeFront".
     */
    public void setSTaxIdLikeFront(String sTaxIdLikeFront) {
        this.sTaxIdLikeFront = sTaxIdLikeFront;
    }

    /**
     * Getter method of "sAddress1LikeFront".
     * 
     * @return the "sAddress1LikeFront"
     */
    public String getSAddress1LikeFront() {
        return sAddress1LikeFront;
    }

    /**
     * Setter method of "sAddress1LikeFront".
     * 
     * @param sAddress1LikeFront Set in "sAddress1LikeFront".
     */
    public void setSAddress1LikeFront(String sAddress1LikeFront) {
        this.sAddress1LikeFront = sAddress1LikeFront;
    }

    /**
     * Getter method of "sAddress2LikeFront".
     * 
     * @return the "sAddress2LikeFront"
     */
    public String getSAddress2LikeFront() {
        return sAddress2LikeFront;
    }

    /**
     * Setter method of "sAddress2LikeFront".
     * 
     * @param sAddress2LikeFront Set in "sAddress2LikeFront".
     */
    public void setSAddress2LikeFront(String sAddress2LikeFront) {
        this.sAddress2LikeFront = sAddress2LikeFront;
    }

    /**
     * Getter method of "sAddress3LikeFront".
     * 
     * @return the "sAddress3LikeFront"
     */
    public String getSAddress3LikeFront() {
        return sAddress3LikeFront;
    }

    /**
     * Setter method of "sAddress3LikeFront".
     * 
     * @param sAddress3LikeFront Set in "sAddress3LikeFront".
     */
    public void setSAddress3LikeFront(String sAddress3LikeFront) {
        this.sAddress3LikeFront = sAddress3LikeFront;
    }

    /**
     * Getter method of "sCurrencyCdLikeFront".
     * 
     * @return the "sCurrencyCdLikeFront"
     */
    public String getSCurrencyCdLikeFront() {
        return sCurrencyCdLikeFront;
    }

    /**
     * Setter method of "sCurrencyCdLikeFront".
     * 
     * @param sCurrencyCdLikeFront Set in "sCurrencyCdLikeFront".
     */
    public void setSCurrencyCdLikeFront(String sCurrencyCdLikeFront) {
        this.sCurrencyCdLikeFront = sCurrencyCdLikeFront;
    }

    /**
     * Getter method of "invoiceNoLikeFront".
     * 
     * @return the "invoiceNoLikeFront"
     */
    public String getInvoiceNoLikeFront() {
        return invoiceNoLikeFront;
    }

    /**
     * Setter method of "invoiceNoLikeFront".
     * 
     * @param invoiceNoLikeFront Set in "invoiceNoLikeFront".
     */
    public void setInvoiceNoLikeFront(String invoiceNoLikeFront) {
        this.invoiceNoLikeFront = invoiceNoLikeFront;
    }

    /**
     * Getter method of "invoiceDateGreaterThanEqual".
     * 
     * @return the "invoiceDateGreaterThanEqual"
     */
    public Date getInvoiceDateGreaterThanEqual() {
        return invoiceDateGreaterThanEqual;
    }

    /**
     * Setter method of "invoiceDateGreaterThanEqual".
     * 
     * @param invoiceDateGreaterThanEqual Set in "invoiceDateGreaterThanEqual".
     */
    public void setInvoiceDateGreaterThanEqual(Date invoiceDateGreaterThanEqual) {
        this.invoiceDateGreaterThanEqual = invoiceDateGreaterThanEqual;
    }

    /**
     * Getter method of "invoiceDateLessThanEqual".
     * 
     * @return the "invoiceDateLessThanEqual"
     */
    public Date getInvoiceDateLessThanEqual() {
        return invoiceDateLessThanEqual;
    }

    /**
     * Setter method of "invoiceDateLessThanEqual".
     * 
     * @param invoiceDateLessThanEqual Set in "invoiceDateLessThanEqual".
     */
    public void setInvoiceDateLessThanEqual(Date invoiceDateLessThanEqual) {
        this.invoiceDateLessThanEqual = invoiceDateLessThanEqual;
    }

    /**
     * Getter method of "invoiceStatusLikeFront".
     * 
     * @return the "invoiceStatusLikeFront"
     */
    public String getInvoiceStatusLikeFront() {
        return invoiceStatusLikeFront;
    }

    /**
     * Setter method of "invoiceStatusLikeFront".
     * 
     * @param invoiceStatusLikeFront Set in "invoiceStatusLikeFront".
     */
    public void setInvoiceStatusLikeFront(String invoiceStatusLikeFront) {
        this.invoiceStatusLikeFront = invoiceStatusLikeFront;
    }

    /**
     * Getter method of "vatTypeLikeFront".
     * 
     * @return the "vatTypeLikeFront"
     */
    public String getVatTypeLikeFront() {
        return vatTypeLikeFront;
    }

    /**
     * Setter method of "vatTypeLikeFront".
     * 
     * @param vatTypeLikeFront Set in "vatTypeLikeFront".
     */
    public void setVatTypeLikeFront(String vatTypeLikeFront) {
        this.vatTypeLikeFront = vatTypeLikeFront;
    }

    /**
     * Getter method of "paymentDateGreaterThanEqual".
     * 
     * @return the "paymentDateGreaterThanEqual"
     */
    public Date getPaymentDateGreaterThanEqual() {
        return paymentDateGreaterThanEqual;
    }

    /**
     * Setter method of "paymentDateGreaterThanEqual".
     * 
     * @param paymentDateGreaterThanEqual Set in "paymentDateGreaterThanEqual".
     */
    public void setPaymentDateGreaterThanEqual(Date paymentDateGreaterThanEqual) {
        this.paymentDateGreaterThanEqual = paymentDateGreaterThanEqual;
    }

    /**
     * Getter method of "paymentDateLessThanEqual".
     * 
     * @return the "paymentDateLessThanEqual"
     */
    public Date getPaymentDateLessThanEqual() {
        return paymentDateLessThanEqual;
    }

    /**
     * Setter method of "paymentDateLessThanEqual".
     * 
     * @param paymentDateLessThanEqual Set in "paymentDateLessThanEqual".
     */
    public void setPaymentDateLessThanEqual(Date paymentDateLessThanEqual) {
        this.paymentDateLessThanEqual = paymentDateLessThanEqual;
    }

    /**
     * Getter method of "transferJdeFlagLikeFront".
     * 
     * @return the "transferJdeFlagLikeFront"
     */
    public String getTransferJdeFlagLikeFront() {
        return transferJdeFlagLikeFront;
    }

    /**
     * Setter method of "transferJdeFlagLikeFront".
     * 
     * @param transferJdeFlagLikeFront Set in "transferJdeFlagLikeFront".
     */
    public void setTransferJdeFlagLikeFront(String transferJdeFlagLikeFront) {
        this.transferJdeFlagLikeFront = transferJdeFlagLikeFront;
    }

    /**
     * Getter method of "coverPageFileIdLikeFront".
     * 
     * @return the "coverPageFileIdLikeFront"
     */
    public String getCoverPageFileIdLikeFront() {
        return coverPageFileIdLikeFront;
    }

    /**
     * Setter method of "coverPageFileIdLikeFront".
     * 
     * @param coverPageFileIdLikeFront Set in "coverPageFileIdLikeFront".
     */
    public void setCoverPageFileIdLikeFront(String coverPageFileIdLikeFront) {
        this.coverPageFileIdLikeFront = coverPageFileIdLikeFront;
    }

    /**
     * Getter method of "coverPageNoLikeFront".
     * 
     * @return the "coverPageNoLikeFront"
     */
    public String getCoverPageNoLikeFront() {
        return coverPageNoLikeFront;
    }

    /**
     * Setter method of "coverPageNoLikeFront".
     * 
     * @param coverPageNoLikeFront Set in "coverPageNoLikeFront".
     */
    public void setCoverPageNoLikeFront(String coverPageNoLikeFront) {
        this.coverPageNoLikeFront = coverPageNoLikeFront;
    }

    /**
     * Getter method of "createDscIdLikeFront".
     * 
     * @return the "createDscIdLikeFront"
     */
    public String getCreateDscIdLikeFront() {
        return createDscIdLikeFront;
    }

    /**
     * Setter method of "createDscIdLikeFront".
     * 
     * @param createDscIdLikeFront Set in "createDscIdLikeFront".
     */
    public void setCreateDscIdLikeFront(String createDscIdLikeFront) {
        this.createDscIdLikeFront = createDscIdLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateDscIdLikeFront".
     * 
     * @return the "lastUpdateDscIdLikeFront"
     */
    public String getLastUpdateDscIdLikeFront() {
        return lastUpdateDscIdLikeFront;
    }

    /**
     * Setter method of "lastUpdateDscIdLikeFront".
     * 
     * @param lastUpdateDscIdLikeFront Set in "lastUpdateDscIdLikeFront".
     */
    public void setLastUpdateDscIdLikeFront(String lastUpdateDscIdLikeFront) {
        this.lastUpdateDscIdLikeFront = lastUpdateDscIdLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
