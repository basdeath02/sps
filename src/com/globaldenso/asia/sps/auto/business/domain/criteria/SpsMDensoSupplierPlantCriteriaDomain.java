/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsMDensoSupplierPlant".<br />
 * Table overview: SPS_M_DENSO_SUPPLIER_PLANT<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierPlantCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * DENSO company code
     */
    private String dCd;

    /**
     * Supplier company DSC ID
     */
    private String sDscId;

    /**
     * DENSO plant code
     */
    private String dPcd;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /**
     * DENSO part no.
     */
    private String dPn;

    /**
     * Supplier part no.
     */
    private String sPn;

    /**
     * Effective date
     */
    private Timestamp effectDate;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createUser;

    /**
     * Create datetime
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateUser;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;

    /**
     * DENSO company code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * Supplier company DSC ID(condition whether the column value at the beginning is equal to the value)
     */
    private String sDscIdLikeFront;

    /**
     * DENSO plant code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * Supplier plant code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * DENSO part no.(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Supplier part no.(condition whether the column value at the beginning is equal to the value)
     */
    private String sPnLikeFront;

    /**
     * Effective date(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectDateGreaterThanEqual;

    /**
     * Effective date(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectDateLessThanEqual;

    /**
     * Is active(condition whether the column value at the beginning is equal to the value)
     */
    private String isActiveLikeFront;

    /**
     * Create user(condition whether the column value at the beginning is equal to the value)
     */
    private String createUserLikeFront;

    /**
     * Create datetime(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp createDatetimeGreaterThanEqual;

    /**
     * Create datetime(condition whether the column value is less than or equal to the value)
     */
    private Timestamp createDatetimeLessThanEqual;

    /**
     * Last update user(condition whether the column value at the beginning is equal to the value)
     */
    private String lastUpdateUserLikeFront;

    /**
     * Last update datetime(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeGreaterThanEqual;

    /**
     * Last update datetime(condition whether the column value is less than or equal to the value)
     */
    private Timestamp lastUpdateDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsMDensoSupplierPlantCriteriaDomain() {
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sDscId".
     * 
     * @return the "sDscId"
     */
    public String getSDscId() {
        return sDscId;
    }

    /**
     * Setter method of "sDscId".
     * 
     * @param sDscId Set in "sDscId".
     */
    public void setSDscId(String sDscId) {
        this.sDscId = sDscId;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn".
     * 
     * @return the "sPn"
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn".
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "effectDate".
     * 
     * @return the "effectDate"
     */
    public Timestamp getEffectDate() {
        return effectDate;
    }

    /**
     * Setter method of "effectDate".
     * 
     * @param effectDate Set in "effectDate".
     */
    public void setEffectDate(Timestamp effectDate) {
        this.effectDate = effectDate;
    }

    /**
     * Getter method of "isActive".
     * 
     * @return the "isActive"
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive".
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createUser".
     * 
     * @return the "createUser"
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method of "createUser".
     * 
     * @param createUser Set in "createUser".
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * Getter method of "createDatetime".
     * 
     * @return the "createDatetime"
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime".
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateUser".
     * 
     * @return the "lastUpdateUser"
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Setter method of "lastUpdateUser".
     * 
     * @param lastUpdateUser Set in "lastUpdateUser".
     */
    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    /**
     * Getter method of "lastUpdateDatetime".
     * 
     * @return the "lastUpdateDatetime"
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime".
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "sDscIdLikeFront".
     * 
     * @return the "sDscIdLikeFront"
     */
    public String getSDscIdLikeFront() {
        return sDscIdLikeFront;
    }

    /**
     * Setter method of "sDscIdLikeFront".
     * 
     * @param sDscIdLikeFront Set in "sDscIdLikeFront".
     */
    public void setSDscIdLikeFront(String sDscIdLikeFront) {
        this.sDscIdLikeFront = sDscIdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "sPnLikeFront".
     * 
     * @return the "sPnLikeFront"
     */
    public String getSPnLikeFront() {
        return sPnLikeFront;
    }

    /**
     * Setter method of "sPnLikeFront".
     * 
     * @param sPnLikeFront Set in "sPnLikeFront".
     */
    public void setSPnLikeFront(String sPnLikeFront) {
        this.sPnLikeFront = sPnLikeFront;
    }

    /**
     * Getter method of "effectDateGreaterThanEqual".
     * 
     * @return the "effectDateGreaterThanEqual"
     */
    public Timestamp getEffectDateGreaterThanEqual() {
        return effectDateGreaterThanEqual;
    }

    /**
     * Setter method of "effectDateGreaterThanEqual".
     * 
     * @param effectDateGreaterThanEqual Set in "effectDateGreaterThanEqual".
     */
    public void setEffectDateGreaterThanEqual(Timestamp effectDateGreaterThanEqual) {
        this.effectDateGreaterThanEqual = effectDateGreaterThanEqual;
    }

    /**
     * Getter method of "effectDateLessThanEqual".
     * 
     * @return the "effectDateLessThanEqual"
     */
    public Timestamp getEffectDateLessThanEqual() {
        return effectDateLessThanEqual;
    }

    /**
     * Setter method of "effectDateLessThanEqual".
     * 
     * @param effectDateLessThanEqual Set in "effectDateLessThanEqual".
     */
    public void setEffectDateLessThanEqual(Timestamp effectDateLessThanEqual) {
        this.effectDateLessThanEqual = effectDateLessThanEqual;
    }

    /**
     * Getter method of "isActiveLikeFront".
     * 
     * @return the "isActiveLikeFront"
     */
    public String getIsActiveLikeFront() {
        return isActiveLikeFront;
    }

    /**
     * Setter method of "isActiveLikeFront".
     * 
     * @param isActiveLikeFront Set in "isActiveLikeFront".
     */
    public void setIsActiveLikeFront(String isActiveLikeFront) {
        this.isActiveLikeFront = isActiveLikeFront;
    }

    /**
     * Getter method of "createUserLikeFront".
     * 
     * @return the "createUserLikeFront"
     */
    public String getCreateUserLikeFront() {
        return createUserLikeFront;
    }

    /**
     * Setter method of "createUserLikeFront".
     * 
     * @param createUserLikeFront Set in "createUserLikeFront".
     */
    public void setCreateUserLikeFront(String createUserLikeFront) {
        this.createUserLikeFront = createUserLikeFront;
    }

    /**
     * Getter method of "createDatetimeGreaterThanEqual".
     * 
     * @return the "createDatetimeGreaterThanEqual"
     */
    public Timestamp getCreateDatetimeGreaterThanEqual() {
        return createDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "createDatetimeGreaterThanEqual".
     * 
     * @param createDatetimeGreaterThanEqual Set in "createDatetimeGreaterThanEqual".
     */
    public void setCreateDatetimeGreaterThanEqual(Timestamp createDatetimeGreaterThanEqual) {
        this.createDatetimeGreaterThanEqual = createDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "createDatetimeLessThanEqual".
     * 
     * @return the "createDatetimeLessThanEqual"
     */
    public Timestamp getCreateDatetimeLessThanEqual() {
        return createDatetimeLessThanEqual;
    }

    /**
     * Setter method of "createDatetimeLessThanEqual".
     * 
     * @param createDatetimeLessThanEqual Set in "createDatetimeLessThanEqual".
     */
    public void setCreateDatetimeLessThanEqual(Timestamp createDatetimeLessThanEqual) {
        this.createDatetimeLessThanEqual = createDatetimeLessThanEqual;
    }

    /**
     * Getter method of "lastUpdateUserLikeFront".
     * 
     * @return the "lastUpdateUserLikeFront"
     */
    public String getLastUpdateUserLikeFront() {
        return lastUpdateUserLikeFront;
    }

    /**
     * Setter method of "lastUpdateUserLikeFront".
     * 
     * @param lastUpdateUserLikeFront Set in "lastUpdateUserLikeFront".
     */
    public void setLastUpdateUserLikeFront(String lastUpdateUserLikeFront) {
        this.lastUpdateUserLikeFront = lastUpdateUserLikeFront;
    }

    /**
     * Getter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @return the "lastUpdateDatetimeGreaterThanEqual"
     */
    public Timestamp getLastUpdateDatetimeGreaterThanEqual() {
        return lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeGreaterThanEqual".
     * 
     * @param lastUpdateDatetimeGreaterThanEqual Set in "lastUpdateDatetimeGreaterThanEqual".
     */
    public void setLastUpdateDatetimeGreaterThanEqual(Timestamp lastUpdateDatetimeGreaterThanEqual) {
        this.lastUpdateDatetimeGreaterThanEqual = lastUpdateDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @return the "lastUpdateDatetimeLessThanEqual"
     */
    public Timestamp getLastUpdateDatetimeLessThanEqual() {
        return lastUpdateDatetimeLessThanEqual;
    }

    /**
     * Setter method of "lastUpdateDatetimeLessThanEqual".
     * 
     * @param lastUpdateDatetimeLessThanEqual Set in "lastUpdateDatetimeLessThanEqual".
     */
    public void setLastUpdateDatetimeLessThanEqual(Timestamp lastUpdateDatetimeLessThanEqual) {
        this.lastUpdateDatetimeLessThanEqual = lastUpdateDatetimeLessThanEqual;
    }

}
