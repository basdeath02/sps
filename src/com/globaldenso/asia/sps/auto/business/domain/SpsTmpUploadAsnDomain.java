/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTmpUploadAsn"<br />
 * Table overview: SPS_TMP_UPLOAD_ASN<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/17 15:14:35<br />
 * 
 * This module generated automatically in 2015/03/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadAsnDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No of Upload ASN record
     */
    private BigDecimal uploadAsnId;

    /**
     * Login user Session ID
     */
    private String sessionId;

    /**
     * CSV Line No
     */
    private String csvLineNo;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * SPS D/O No
     */
    private String spsDoNo;

    /**
     * Latest Revision of D/O
     */
    private String revision;

    /**
     * Order Method
     */
    private String orderMethod;

    /**
     * Transportation Mode
0 : DENSO Truck
1 : Supplier Truck
     */
    private String tm;

    /**
     * Plan ETA
     */
    private Timestamp planEta;

    /**
     * Actual ETD
     */
    private Timestamp actualEtd;

    /**
     * Trip No
     */
    private String tripNo;

    /**
     * Number of Pallet
     */
    private BigDecimal numberOfPallet;

    /**
     * DENSO Part Number
     */
    private String dPn;

    /**
     * SupplierPart Number
     */
    private String sPn;

    /**
     * Shipping Qty
     */
    private BigDecimal shippingQty;

    /**
     * Unit of Measure
     */
    private String unitOfMeasure;

    /**
     * Receiving Lane
     */
    private String rcvLane;

    /**
     * ASN Number
     */
    private String asnNo;

    /**
     * Error Flag
0 : Not error
1 : Warning
2 : Error
     */
    private String isErrorFlg;

    /**
     * Error Code category
     */
    private String errorCode;

    /**
     * DSC ID of Uploaded data
     */
    private String uploadDscId;

    /**
     * Datetime of Uploaded data
     */
    private Timestamp uploadDatetime;

    /**
     * Default constructor
     */
    public SpsTmpUploadAsnDomain() {
    }

    /**
     * Getter method of "uploadAsnId"
     * 
     * @return the uploadAsnId
     */
    public BigDecimal getUploadAsnId() {
        return uploadAsnId;
    }

    /**
     * Setter method of "uploadAsnId"
     * 
     * @param uploadAsnId Set in "uploadAsnId".
     */
    public void setUploadAsnId(BigDecimal uploadAsnId) {
        this.uploadAsnId = uploadAsnId;
    }

    /**
     * Getter method of "sessionId"
     * 
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Setter method of "sessionId"
     * 
     * @param sessionId Set in "sessionId".
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Getter method of "csvLineNo"
     * 
     * @return the csvLineNo
     */
    public String getCsvLineNo() {
        return csvLineNo;
    }

    /**
     * Setter method of "csvLineNo"
     * 
     * @param csvLineNo Set in "csvLineNo".
     */
    public void setCsvLineNo(String csvLineNo) {
        this.csvLineNo = csvLineNo;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "spsDoNo"
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Setter method of "spsDoNo"
     * 
     * @param spsDoNo Set in "spsDoNo".
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Getter method of "revision"
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Setter method of "revision"
     * 
     * @param revision Set in "revision".
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Getter method of "orderMethod"
     * 
     * @return the orderMethod
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Setter method of "orderMethod"
     * 
     * @param orderMethod Set in "orderMethod".
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Getter method of "tm"
     * 
     * @return the tm
     */
    public String getTm() {
        return tm;
    }

    /**
     * Setter method of "tm"
     * 
     * @param tm Set in "tm".
     */
    public void setTm(String tm) {
        this.tm = tm;
    }

    /**
     * Getter method of "planEta"
     * 
     * @return the planEta
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * Setter method of "planEta"
     * 
     * @param planEta Set in "planEta".
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * Getter method of "actualEtd"
     * 
     * @return the actualEtd
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * Setter method of "actualEtd"
     * 
     * @param actualEtd Set in "actualEtd".
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }

    /**
     * Getter method of "tripNo"
     * 
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo"
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "numberOfPallet"
     * 
     * @return the numberOfPallet
     */
    public BigDecimal getNumberOfPallet() {
        return numberOfPallet;
    }

    /**
     * Setter method of "numberOfPallet"
     * 
     * @param numberOfPallet Set in "numberOfPallet".
     */
    public void setNumberOfPallet(BigDecimal numberOfPallet) {
        this.numberOfPallet = numberOfPallet;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "shippingQty"
     * 
     * @return the shippingQty
     */
    public BigDecimal getShippingQty() {
        return shippingQty;
    }

    /**
     * Setter method of "shippingQty"
     * 
     * @param shippingQty Set in "shippingQty".
     */
    public void setShippingQty(BigDecimal shippingQty) {
        this.shippingQty = shippingQty;
    }

    /**
     * Getter method of "unitOfMeasure"
     * 
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Setter method of "unitOfMeasure"
     * 
     * @param unitOfMeasure Set in "unitOfMeasure".
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter method of "rcvLane"
     * 
     * @return the rcvLane
     */
    public String getRcvLane() {
        return rcvLane;
    }

    /**
     * Setter method of "rcvLane"
     * 
     * @param rcvLane Set in "rcvLane".
     */
    public void setRcvLane(String rcvLane) {
        this.rcvLane = rcvLane;
    }

    /**
     * Getter method of "asnNo"
     * 
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo"
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "isErrorFlg"
     * 
     * @return the isErrorFlg
     */
    public String getIsErrorFlg() {
        return isErrorFlg;
    }

    /**
     * Setter method of "isErrorFlg"
     * 
     * @param isErrorFlg Set in "isErrorFlg".
     */
    public void setIsErrorFlg(String isErrorFlg) {
        this.isErrorFlg = isErrorFlg;
    }

    /**
     * Getter method of "errorCode"
     * 
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Setter method of "errorCode"
     * 
     * @param errorCode Set in "errorCode".
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Getter method of "uploadDscId"
     * 
     * @return the uploadDscId
     */
    public String getUploadDscId() {
        return uploadDscId;
    }

    /**
     * Setter method of "uploadDscId"
     * 
     * @param uploadDscId Set in "uploadDscId".
     */
    public void setUploadDscId(String uploadDscId) {
        this.uploadDscId = uploadDscId;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

}
