/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 2.0.0      2018/01/10       Netband U. Rungsiwut            SPS Phase II
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.sql.Timestamp;
import java.math.BigDecimal;

/**
 * A "Domain" class of "SpsTAsn"<br />
 * Table overview: SPS_T_ASN<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ASN_NO
     */
    private String asnNo;

    /**
     * ASN_STATUS
     */
    private String asnStatus;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * PLAN_ETD
     */
    private Timestamp planEtd;

    /**
     * PLAN_ETA
     */
    private Timestamp planEta;

    /**
     * ACTUAL_ETD
     */
    private Timestamp actualEtd;

    /**
     * ACTUAL_ETA
     */
    private Timestamp actualEta;

    /**
     * TRANSFER_CIGMA_FLG
     */
    private String transferCigmaFlg;

    /**
     * NUMBER_OF_PALLET
     */
    private BigDecimal numberOfPallet;

    /**
     * TRIP_NO
     */
    private String tripNo;

    /**
     * PDF_FILE_ID
     */
    private String pdfFileId;

    /**
     * CREATE_DSC_ID
     */
    private String createDscId;

    /**
     * CREATE_DATETIME
     */
    private Timestamp createDatetime;

    /**
     * LAST_UPDATE_DSC_ID
     */
    private String lastUpdateDscId;

    /**
     * LAST_UPDATE_DATETIME
     */
    private Timestamp lastUpdateDatetime;

    /**
     * CREATED_INVOICE_FLAG
     */
    private String createdInvoiceFlag;

    /**
     * VENDOR_CD
     */
    private String vendorCd; 
    
    /** 
     * The receiveByScan. 
     */
    private String receiveByScan; 

    /**
     * Default constructor
     */
    public SpsTAsnDomain() {
    }

    /**
     * Getter method of "asnNo"
     * 
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Setter method of "asnNo"
     * 
     * @param asnNo Set in "asnNo".
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Getter method of "asnStatus"
     * 
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * Setter method of "asnStatus"
     * 
     * @param asnStatus Set in "asnStatus".
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "planEtd"
     * 
     * @return the planEtd
     */
    public Timestamp getPlanEtd() {
        return planEtd;
    }

    /**
     * Setter method of "planEtd"
     * 
     * @param planEtd Set in "planEtd".
     */
    public void setPlanEtd(Timestamp planEtd) {
        this.planEtd = planEtd;
    }

    /**
     * Getter method of "planEta"
     * 
     * @return the planEta
     */
    public Timestamp getPlanEta() {
        return planEta;
    }

    /**
     * Setter method of "planEta"
     * 
     * @param planEta Set in "planEta".
     */
    public void setPlanEta(Timestamp planEta) {
        this.planEta = planEta;
    }

    /**
     * Getter method of "actualEtd"
     * 
     * @return the actualEtd
     */
    public Timestamp getActualEtd() {
        return actualEtd;
    }

    /**
     * Setter method of "actualEtd"
     * 
     * @param actualEtd Set in "actualEtd".
     */
    public void setActualEtd(Timestamp actualEtd) {
        this.actualEtd = actualEtd;
    }

    /**
     * Getter method of "actualEta"
     * 
     * @return the actualEta
     */
    public Timestamp getActualEta() {
        return actualEta;
    }

    /**
     * Setter method of "actualEta"
     * 
     * @param actualEta Set in "actualEta".
     */
    public void setActualEta(Timestamp actualEta) {
        this.actualEta = actualEta;
    }

    /**
     * Getter method of "transferCigmaFlg"
     * 
     * @return the transferCigmaFlg
     */
    public String getTransferCigmaFlg() {
        return transferCigmaFlg;
    }

    /**
     * Setter method of "transferCigmaFlg"
     * 
     * @param transferCigmaFlg Set in "transferCigmaFlg".
     */
    public void setTransferCigmaFlg(String transferCigmaFlg) {
        this.transferCigmaFlg = transferCigmaFlg;
    }

    /**
     * Getter method of "numberOfPallet"
     * 
     * @return the numberOfPallet
     */
    public BigDecimal getNumberOfPallet() {
        return numberOfPallet;
    }

    /**
     * Setter method of "numberOfPallet"
     * 
     * @param numberOfPallet Set in "numberOfPallet".
     */
    public void setNumberOfPallet(BigDecimal numberOfPallet) {
        this.numberOfPallet = numberOfPallet;
    }

    /**
     * Getter method of "tripNo"
     * 
     * @return the tripNo
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Setter method of "tripNo"
     * 
     * @param tripNo Set in "tripNo".
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    /**
     * Getter method of "pdfFileId"
     * 
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Setter method of "pdfFileId"
     * 
     * @param pdfFileId Set in "pdfFileId".
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * Getter method of "createdInvoiceFlag"
     * 
     * @return the createdInvoiceFlag
     */
    public String getCreatedInvoiceFlag() {
        return createdInvoiceFlag;
    }

    /**
     * Setter method of "createdInvoiceFlag"
     * 
     * @param createdInvoiceFlag Set in "createdInvoiceFlag".
     */
    public void setCreatedInvoiceFlag(String createdInvoiceFlag) {
        this.createdInvoiceFlag = createdInvoiceFlag;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }

}
