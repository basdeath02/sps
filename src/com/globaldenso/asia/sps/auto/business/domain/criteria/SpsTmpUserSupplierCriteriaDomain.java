/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/30       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A search criteria "Domain" class of "SpsTmpUserSupplier".<br />
 * Table overview: SPS_TMP_USER_SUPPLIER<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/30 12:07:21<br />
 * 
 * This module generated automatically in 2015/03/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserSupplierCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * User DSC ID that upload data to system
     */
    private String userDscId;

    /**
     * Source file line number
     */
    private BigDecimal lineNo;

    /**
     * User identify number (import data)
     */
    private String dscId;

    /**
     * Main Desnso company to handle on this user information  (import data)
     */
    private String dOwner;

    /**
     * Supplier company code (import data)
     */
    private String sCd;

    /**
     * Supplier plant code (import data)
     */
    private String sPcd;

    /**
     * Supplier department name (import data)
     */
    private String departmentCd;

    /**
     * User first name (import data)
     */
    private String firstName;

    /**
     * User middle name (import data)
     */
    private String middleName;

    /**
     * User last name (import data)
     */
    private String lastName;

    /**
     * User email addresses if has email address more than 1 separate each of it by comma ","  (import data)
     */
    private String email;

    /**
     * User telephone number (import data)
     */
    private String telephone;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlUrgentOrderFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlSInfoNotfoundFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlAllowReviseFlag;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)
     */
    private String emlCancelInvoiceFlag;

    /**
     * User role assignment value  (import data)
     */
    private String roleCd;

    /**
     * Scope of user right on Supplier plant  (import data)
     */
    private String roleSPcd;

    /**
     * SEQ_NO
     */
    private BigDecimal seqNo;

    /**
     * User role effective start date  (import data)
     */
    private Timestamp effectStart;

    /**
     * User role effective end date  (import data)
     */
    private Timestamp effectEnd;

    /**
     * A : Add, U : Update, D : Delete
     */
    private String informationFlag;

    /**
     * Upload operation timestamp
     */
    private Timestamp uploadDatetime;

    /**
     * User login session code
     */
    private String sessionCd;

    /**
     * A : Add, U : Update, T : Terminate
     */
    private String roleFlag;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete
     */
    private Timestamp userLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_SUPPLIER) in case of Update/Delete
     */
    private Timestamp supplierLastUpdate;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete
     */
    private Timestamp roleLastUpdate;

    /**
     * 0 : not move yet, 1 : move to actual table
     */
    private String isActualRegister;

    /**
     * Move data from temporary table to actual table timestamp
     */
    private Timestamp toActualDatetime;

    /**
     * User DSC ID that upload data to system(condition whether the column value at the beginning is equal to the value)
     */
    private String userDscIdLikeFront;

    /**
     * User identify number (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String dscIdLikeFront;

    /**
     * Main Desnso company to handle on this user information  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String dOwnerLikeFront;

    /**
     * Supplier company code (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier plant code (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * Supplier department name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String departmentCdLikeFront;

    /**
     * User first name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String firstNameLikeFront;

    /**
     * User middle name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String middleNameLikeFront;

    /**
     * User last name (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String lastNameLikeFront;

    /**
     * User email addresses if has email address more than 1 separate each of it by comma ","  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emailLikeFront;

    /**
     * User telephone number (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String telephoneLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlUrgentOrderFlagLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlSInfoNotfoundFlagLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlAllowReviseFlagLikeFront;

    /**
     * 0 : Determined not to receive this email topic. 1 : Determined to get this email topic. (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String emlCancelInvoiceFlagLikeFront;

    /**
     * User role assignment value  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String roleCdLikeFront;

    /**
     * Scope of user right on Supplier plant  (import data)(condition whether the column value at the beginning is equal to the value)
     */
    private String roleSPcdLikeFront;

    /**
     * User role effective start date  (import data)(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectStartGreaterThanEqual;

    /**
     * User role effective start date  (import data)(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectStartLessThanEqual;

    /**
     * User role effective end date  (import data)(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp effectEndGreaterThanEqual;

    /**
     * User role effective end date  (import data)(condition whether the column value is less than or equal to the value)
     */
    private Timestamp effectEndLessThanEqual;

    /**
     * A : Add, U : Update, D : Delete(condition whether the column value at the beginning is equal to the value)
     */
    private String informationFlagLikeFront;

    /**
     * Upload operation timestamp(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp uploadDatetimeGreaterThanEqual;

    /**
     * Upload operation timestamp(condition whether the column value is less than or equal to the value)
     */
    private Timestamp uploadDatetimeLessThanEqual;

    /**
     * User login session code(condition whether the column value at the beginning is equal to the value)
     */
    private String sessionCdLikeFront;

    /**
     * A : Add, U : Update, T : Terminate(condition whether the column value at the beginning is equal to the value)
     */
    private String roleFlagLikeFront;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp userLastUpdateGreaterThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER) in case of Update/Delete(condition whether the column value is less than or equal to the value)
     */
    private Timestamp userLastUpdateLessThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_SUPPLIER) in case of Update/Delete(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp supplierLastUpdateGreaterThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_SUPPLIER) in case of Update/Delete(condition whether the column value is less than or equal to the value)
     */
    private Timestamp supplierLastUpdateLessThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp roleLastUpdateGreaterThanEqual;

    /**
     * Last update date time of this user information from table (SPS_M_USER_ROLE) in case of Update/Delete(condition whether the column value is less than or equal to the value)
     */
    private Timestamp roleLastUpdateLessThanEqual;

    /**
     * 0 : not move yet, 1 : move to actual table(condition whether the column value at the beginning is equal to the value)
     */
    private String isActualRegisterLikeFront;

    /**
     * Move data from temporary table to actual table timestamp(condition whether the column value is greater than or equal to the value)
     */
    private Timestamp toActualDatetimeGreaterThanEqual;

    /**
     * Move data from temporary table to actual table timestamp(condition whether the column value is less than or equal to the value)
     */
    private Timestamp toActualDatetimeLessThanEqual;

    /**
     * Default constructor
     */
    public SpsTmpUserSupplierCriteriaDomain() {
    }

    /**
     * Getter method of "userDscId".
     * 
     * @return the "userDscId"
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId".
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "lineNo".
     * 
     * @return the "lineNo"
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo".
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dscId".
     * 
     * @return the "dscId"
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Setter method of "dscId".
     * 
     * @param dscId Set in "dscId".
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Getter method of "dOwner".
     * 
     * @return the "dOwner"
     */
    public String getDOwner() {
        return dOwner;
    }

    /**
     * Setter method of "dOwner".
     * 
     * @param dOwner Set in "dOwner".
     */
    public void setDOwner(String dOwner) {
        this.dOwner = dOwner;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "departmentCd".
     * 
     * @return the "departmentCd"
     */
    public String getDepartmentCd() {
        return departmentCd;
    }

    /**
     * Setter method of "departmentCd".
     * 
     * @param departmentCd Set in "departmentCd".
     */
    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    /**
     * Getter method of "firstName".
     * 
     * @return the "firstName"
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method of "firstName".
     * 
     * @param firstName Set in "firstName".
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter method of "middleName".
     * 
     * @return the "middleName"
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter method of "middleName".
     * 
     * @param middleName Set in "middleName".
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter method of "lastName".
     * 
     * @return the "lastName"
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method of "lastName".
     * 
     * @param lastName Set in "lastName".
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method of "email".
     * 
     * @return the "email"
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method of "email".
     * 
     * @param email Set in "email".
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method of "telephone".
     * 
     * @return the "telephone"
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone".
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "emlUrgentOrderFlag".
     * 
     * @return the "emlUrgentOrderFlag"
     */
    public String getEmlUrgentOrderFlag() {
        return emlUrgentOrderFlag;
    }

    /**
     * Setter method of "emlUrgentOrderFlag".
     * 
     * @param emlUrgentOrderFlag Set in "emlUrgentOrderFlag".
     */
    public void setEmlUrgentOrderFlag(String emlUrgentOrderFlag) {
        this.emlUrgentOrderFlag = emlUrgentOrderFlag;
    }

    /**
     * Getter method of "emlSInfoNotfoundFlag".
     * 
     * @return the "emlSInfoNotfoundFlag"
     */
    public String getEmlSInfoNotfoundFlag() {
        return emlSInfoNotfoundFlag;
    }

    /**
     * Setter method of "emlSInfoNotfoundFlag".
     * 
     * @param emlSInfoNotfoundFlag Set in "emlSInfoNotfoundFlag".
     */
    public void setEmlSInfoNotfoundFlag(String emlSInfoNotfoundFlag) {
        this.emlSInfoNotfoundFlag = emlSInfoNotfoundFlag;
    }

    /**
     * Getter method of "emlAllowReviseFlag".
     * 
     * @return the "emlAllowReviseFlag"
     */
    public String getEmlAllowReviseFlag() {
        return emlAllowReviseFlag;
    }

    /**
     * Setter method of "emlAllowReviseFlag".
     * 
     * @param emlAllowReviseFlag Set in "emlAllowReviseFlag".
     */
    public void setEmlAllowReviseFlag(String emlAllowReviseFlag) {
        this.emlAllowReviseFlag = emlAllowReviseFlag;
    }

    /**
     * Getter method of "emlCancelInvoiceFlag".
     * 
     * @return the "emlCancelInvoiceFlag"
     */
    public String getEmlCancelInvoiceFlag() {
        return emlCancelInvoiceFlag;
    }

    /**
     * Setter method of "emlCancelInvoiceFlag".
     * 
     * @param emlCancelInvoiceFlag Set in "emlCancelInvoiceFlag".
     */
    public void setEmlCancelInvoiceFlag(String emlCancelInvoiceFlag) {
        this.emlCancelInvoiceFlag = emlCancelInvoiceFlag;
    }

    /**
     * Getter method of "roleCd".
     * 
     * @return the "roleCd"
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Setter method of "roleCd".
     * 
     * @param roleCd Set in "roleCd".
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    /**
     * Getter method of "roleSPcd".
     * 
     * @return the "roleSPcd"
     */
    public String getRoleSPcd() {
        return roleSPcd;
    }

    /**
     * Setter method of "roleSPcd".
     * 
     * @param roleSPcd Set in "roleSPcd".
     */
    public void setRoleSPcd(String roleSPcd) {
        this.roleSPcd = roleSPcd;
    }

    /**
     * Getter method of "seqNo".
     * 
     * @return the "seqNo"
     */
    public BigDecimal getSeqNo() {
        return seqNo;
    }

    /**
     * Setter method of "seqNo".
     * 
     * @param seqNo Set in "seqNo".
     */
    public void setSeqNo(BigDecimal seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * Getter method of "effectStart".
     * 
     * @return the "effectStart"
     */
    public Timestamp getEffectStart() {
        return effectStart;
    }

    /**
     * Setter method of "effectStart".
     * 
     * @param effectStart Set in "effectStart".
     */
    public void setEffectStart(Timestamp effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Getter method of "effectEnd".
     * 
     * @return the "effectEnd"
     */
    public Timestamp getEffectEnd() {
        return effectEnd;
    }

    /**
     * Setter method of "effectEnd".
     * 
     * @param effectEnd Set in "effectEnd".
     */
    public void setEffectEnd(Timestamp effectEnd) {
        this.effectEnd = effectEnd;
    }

    /**
     * Getter method of "informationFlag".
     * 
     * @return the "informationFlag"
     */
    public String getInformationFlag() {
        return informationFlag;
    }

    /**
     * Setter method of "informationFlag".
     * 
     * @param informationFlag Set in "informationFlag".
     */
    public void setInformationFlag(String informationFlag) {
        this.informationFlag = informationFlag;
    }

    /**
     * Getter method of "uploadDatetime".
     * 
     * @return the "uploadDatetime"
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime".
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "sessionCd".
     * 
     * @return the "sessionCd"
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd".
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "roleFlag".
     * 
     * @return the "roleFlag"
     */
    public String getRoleFlag() {
        return roleFlag;
    }

    /**
     * Setter method of "roleFlag".
     * 
     * @param roleFlag Set in "roleFlag".
     */
    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    /**
     * Getter method of "userLastUpdate".
     * 
     * @return the "userLastUpdate"
     */
    public Timestamp getUserLastUpdate() {
        return userLastUpdate;
    }

    /**
     * Setter method of "userLastUpdate".
     * 
     * @param userLastUpdate Set in "userLastUpdate".
     */
    public void setUserLastUpdate(Timestamp userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    /**
     * Getter method of "supplierLastUpdate".
     * 
     * @return the "supplierLastUpdate"
     */
    public Timestamp getSupplierLastUpdate() {
        return supplierLastUpdate;
    }

    /**
     * Setter method of "supplierLastUpdate".
     * 
     * @param supplierLastUpdate Set in "supplierLastUpdate".
     */
    public void setSupplierLastUpdate(Timestamp supplierLastUpdate) {
        this.supplierLastUpdate = supplierLastUpdate;
    }

    /**
     * Getter method of "roleLastUpdate".
     * 
     * @return the "roleLastUpdate"
     */
    public Timestamp getRoleLastUpdate() {
        return roleLastUpdate;
    }

    /**
     * Setter method of "roleLastUpdate".
     * 
     * @param roleLastUpdate Set in "roleLastUpdate".
     */
    public void setRoleLastUpdate(Timestamp roleLastUpdate) {
        this.roleLastUpdate = roleLastUpdate;
    }

    /**
     * Getter method of "isActualRegister".
     * 
     * @return the "isActualRegister"
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister".
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime".
     * 
     * @return the "toActualDatetime"
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime".
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

    /**
     * Getter method of "userDscIdLikeFront".
     * 
     * @return the "userDscIdLikeFront"
     */
    public String getUserDscIdLikeFront() {
        return userDscIdLikeFront;
    }

    /**
     * Setter method of "userDscIdLikeFront".
     * 
     * @param userDscIdLikeFront Set in "userDscIdLikeFront".
     */
    public void setUserDscIdLikeFront(String userDscIdLikeFront) {
        this.userDscIdLikeFront = userDscIdLikeFront;
    }

    /**
     * Getter method of "dscIdLikeFront".
     * 
     * @return the "dscIdLikeFront"
     */
    public String getDscIdLikeFront() {
        return dscIdLikeFront;
    }

    /**
     * Setter method of "dscIdLikeFront".
     * 
     * @param dscIdLikeFront Set in "dscIdLikeFront".
     */
    public void setDscIdLikeFront(String dscIdLikeFront) {
        this.dscIdLikeFront = dscIdLikeFront;
    }

    /**
     * Getter method of "dOwnerLikeFront".
     * 
     * @return the "dOwnerLikeFront"
     */
    public String getDOwnerLikeFront() {
        return dOwnerLikeFront;
    }

    /**
     * Setter method of "dOwnerLikeFront".
     * 
     * @param dOwnerLikeFront Set in "dOwnerLikeFront".
     */
    public void setDOwnerLikeFront(String dOwnerLikeFront) {
        this.dOwnerLikeFront = dOwnerLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "departmentCdLikeFront".
     * 
     * @return the "departmentCdLikeFront"
     */
    public String getDepartmentCdLikeFront() {
        return departmentCdLikeFront;
    }

    /**
     * Setter method of "departmentCdLikeFront".
     * 
     * @param departmentCdLikeFront Set in "departmentCdLikeFront".
     */
    public void setDepartmentCdLikeFront(String departmentCdLikeFront) {
        this.departmentCdLikeFront = departmentCdLikeFront;
    }

    /**
     * Getter method of "firstNameLikeFront".
     * 
     * @return the "firstNameLikeFront"
     */
    public String getFirstNameLikeFront() {
        return firstNameLikeFront;
    }

    /**
     * Setter method of "firstNameLikeFront".
     * 
     * @param firstNameLikeFront Set in "firstNameLikeFront".
     */
    public void setFirstNameLikeFront(String firstNameLikeFront) {
        this.firstNameLikeFront = firstNameLikeFront;
    }

    /**
     * Getter method of "middleNameLikeFront".
     * 
     * @return the "middleNameLikeFront"
     */
    public String getMiddleNameLikeFront() {
        return middleNameLikeFront;
    }

    /**
     * Setter method of "middleNameLikeFront".
     * 
     * @param middleNameLikeFront Set in "middleNameLikeFront".
     */
    public void setMiddleNameLikeFront(String middleNameLikeFront) {
        this.middleNameLikeFront = middleNameLikeFront;
    }

    /**
     * Getter method of "lastNameLikeFront".
     * 
     * @return the "lastNameLikeFront"
     */
    public String getLastNameLikeFront() {
        return lastNameLikeFront;
    }

    /**
     * Setter method of "lastNameLikeFront".
     * 
     * @param lastNameLikeFront Set in "lastNameLikeFront".
     */
    public void setLastNameLikeFront(String lastNameLikeFront) {
        this.lastNameLikeFront = lastNameLikeFront;
    }

    /**
     * Getter method of "emailLikeFront".
     * 
     * @return the "emailLikeFront"
     */
    public String getEmailLikeFront() {
        return emailLikeFront;
    }

    /**
     * Setter method of "emailLikeFront".
     * 
     * @param emailLikeFront Set in "emailLikeFront".
     */
    public void setEmailLikeFront(String emailLikeFront) {
        this.emailLikeFront = emailLikeFront;
    }

    /**
     * Getter method of "telephoneLikeFront".
     * 
     * @return the "telephoneLikeFront"
     */
    public String getTelephoneLikeFront() {
        return telephoneLikeFront;
    }

    /**
     * Setter method of "telephoneLikeFront".
     * 
     * @param telephoneLikeFront Set in "telephoneLikeFront".
     */
    public void setTelephoneLikeFront(String telephoneLikeFront) {
        this.telephoneLikeFront = telephoneLikeFront;
    }

    /**
     * Getter method of "emlUrgentOrderFlagLikeFront".
     * 
     * @return the "emlUrgentOrderFlagLikeFront"
     */
    public String getEmlUrgentOrderFlagLikeFront() {
        return emlUrgentOrderFlagLikeFront;
    }

    /**
     * Setter method of "emlUrgentOrderFlagLikeFront".
     * 
     * @param emlUrgentOrderFlagLikeFront Set in "emlUrgentOrderFlagLikeFront".
     */
    public void setEmlUrgentOrderFlagLikeFront(String emlUrgentOrderFlagLikeFront) {
        this.emlUrgentOrderFlagLikeFront = emlUrgentOrderFlagLikeFront;
    }

    /**
     * Getter method of "emlSInfoNotfoundFlagLikeFront".
     * 
     * @return the "emlSInfoNotfoundFlagLikeFront"
     */
    public String getEmlSInfoNotfoundFlagLikeFront() {
        return emlSInfoNotfoundFlagLikeFront;
    }

    /**
     * Setter method of "emlSInfoNotfoundFlagLikeFront".
     * 
     * @param emlSInfoNotfoundFlagLikeFront Set in "emlSInfoNotfoundFlagLikeFront".
     */
    public void setEmlSInfoNotfoundFlagLikeFront(String emlSInfoNotfoundFlagLikeFront) {
        this.emlSInfoNotfoundFlagLikeFront = emlSInfoNotfoundFlagLikeFront;
    }

    /**
     * Getter method of "emlAllowReviseFlagLikeFront".
     * 
     * @return the "emlAllowReviseFlagLikeFront"
     */
    public String getEmlAllowReviseFlagLikeFront() {
        return emlAllowReviseFlagLikeFront;
    }

    /**
     * Setter method of "emlAllowReviseFlagLikeFront".
     * 
     * @param emlAllowReviseFlagLikeFront Set in "emlAllowReviseFlagLikeFront".
     */
    public void setEmlAllowReviseFlagLikeFront(String emlAllowReviseFlagLikeFront) {
        this.emlAllowReviseFlagLikeFront = emlAllowReviseFlagLikeFront;
    }

    /**
     * Getter method of "emlCancelInvoiceFlagLikeFront".
     * 
     * @return the "emlCancelInvoiceFlagLikeFront"
     */
    public String getEmlCancelInvoiceFlagLikeFront() {
        return emlCancelInvoiceFlagLikeFront;
    }

    /**
     * Setter method of "emlCancelInvoiceFlagLikeFront".
     * 
     * @param emlCancelInvoiceFlagLikeFront Set in "emlCancelInvoiceFlagLikeFront".
     */
    public void setEmlCancelInvoiceFlagLikeFront(String emlCancelInvoiceFlagLikeFront) {
        this.emlCancelInvoiceFlagLikeFront = emlCancelInvoiceFlagLikeFront;
    }

    /**
     * Getter method of "roleCdLikeFront".
     * 
     * @return the "roleCdLikeFront"
     */
    public String getRoleCdLikeFront() {
        return roleCdLikeFront;
    }

    /**
     * Setter method of "roleCdLikeFront".
     * 
     * @param roleCdLikeFront Set in "roleCdLikeFront".
     */
    public void setRoleCdLikeFront(String roleCdLikeFront) {
        this.roleCdLikeFront = roleCdLikeFront;
    }

    /**
     * Getter method of "roleSPcdLikeFront".
     * 
     * @return the "roleSPcdLikeFront"
     */
    public String getRoleSPcdLikeFront() {
        return roleSPcdLikeFront;
    }

    /**
     * Setter method of "roleSPcdLikeFront".
     * 
     * @param roleSPcdLikeFront Set in "roleSPcdLikeFront".
     */
    public void setRoleSPcdLikeFront(String roleSPcdLikeFront) {
        this.roleSPcdLikeFront = roleSPcdLikeFront;
    }

    /**
     * Getter method of "effectStartGreaterThanEqual".
     * 
     * @return the "effectStartGreaterThanEqual"
     */
    public Timestamp getEffectStartGreaterThanEqual() {
        return effectStartGreaterThanEqual;
    }

    /**
     * Setter method of "effectStartGreaterThanEqual".
     * 
     * @param effectStartGreaterThanEqual Set in "effectStartGreaterThanEqual".
     */
    public void setEffectStartGreaterThanEqual(Timestamp effectStartGreaterThanEqual) {
        this.effectStartGreaterThanEqual = effectStartGreaterThanEqual;
    }

    /**
     * Getter method of "effectStartLessThanEqual".
     * 
     * @return the "effectStartLessThanEqual"
     */
    public Timestamp getEffectStartLessThanEqual() {
        return effectStartLessThanEqual;
    }

    /**
     * Setter method of "effectStartLessThanEqual".
     * 
     * @param effectStartLessThanEqual Set in "effectStartLessThanEqual".
     */
    public void setEffectStartLessThanEqual(Timestamp effectStartLessThanEqual) {
        this.effectStartLessThanEqual = effectStartLessThanEqual;
    }

    /**
     * Getter method of "effectEndGreaterThanEqual".
     * 
     * @return the "effectEndGreaterThanEqual"
     */
    public Timestamp getEffectEndGreaterThanEqual() {
        return effectEndGreaterThanEqual;
    }

    /**
     * Setter method of "effectEndGreaterThanEqual".
     * 
     * @param effectEndGreaterThanEqual Set in "effectEndGreaterThanEqual".
     */
    public void setEffectEndGreaterThanEqual(Timestamp effectEndGreaterThanEqual) {
        this.effectEndGreaterThanEqual = effectEndGreaterThanEqual;
    }

    /**
     * Getter method of "effectEndLessThanEqual".
     * 
     * @return the "effectEndLessThanEqual"
     */
    public Timestamp getEffectEndLessThanEqual() {
        return effectEndLessThanEqual;
    }

    /**
     * Setter method of "effectEndLessThanEqual".
     * 
     * @param effectEndLessThanEqual Set in "effectEndLessThanEqual".
     */
    public void setEffectEndLessThanEqual(Timestamp effectEndLessThanEqual) {
        this.effectEndLessThanEqual = effectEndLessThanEqual;
    }

    /**
     * Getter method of "informationFlagLikeFront".
     * 
     * @return the "informationFlagLikeFront"
     */
    public String getInformationFlagLikeFront() {
        return informationFlagLikeFront;
    }

    /**
     * Setter method of "informationFlagLikeFront".
     * 
     * @param informationFlagLikeFront Set in "informationFlagLikeFront".
     */
    public void setInformationFlagLikeFront(String informationFlagLikeFront) {
        this.informationFlagLikeFront = informationFlagLikeFront;
    }

    /**
     * Getter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @return the "uploadDatetimeGreaterThanEqual"
     */
    public Timestamp getUploadDatetimeGreaterThanEqual() {
        return uploadDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeGreaterThanEqual".
     * 
     * @param uploadDatetimeGreaterThanEqual Set in "uploadDatetimeGreaterThanEqual".
     */
    public void setUploadDatetimeGreaterThanEqual(Timestamp uploadDatetimeGreaterThanEqual) {
        this.uploadDatetimeGreaterThanEqual = uploadDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "uploadDatetimeLessThanEqual".
     * 
     * @return the "uploadDatetimeLessThanEqual"
     */
    public Timestamp getUploadDatetimeLessThanEqual() {
        return uploadDatetimeLessThanEqual;
    }

    /**
     * Setter method of "uploadDatetimeLessThanEqual".
     * 
     * @param uploadDatetimeLessThanEqual Set in "uploadDatetimeLessThanEqual".
     */
    public void setUploadDatetimeLessThanEqual(Timestamp uploadDatetimeLessThanEqual) {
        this.uploadDatetimeLessThanEqual = uploadDatetimeLessThanEqual;
    }

    /**
     * Getter method of "sessionCdLikeFront".
     * 
     * @return the "sessionCdLikeFront"
     */
    public String getSessionCdLikeFront() {
        return sessionCdLikeFront;
    }

    /**
     * Setter method of "sessionCdLikeFront".
     * 
     * @param sessionCdLikeFront Set in "sessionCdLikeFront".
     */
    public void setSessionCdLikeFront(String sessionCdLikeFront) {
        this.sessionCdLikeFront = sessionCdLikeFront;
    }

    /**
     * Getter method of "roleFlagLikeFront".
     * 
     * @return the "roleFlagLikeFront"
     */
    public String getRoleFlagLikeFront() {
        return roleFlagLikeFront;
    }

    /**
     * Setter method of "roleFlagLikeFront".
     * 
     * @param roleFlagLikeFront Set in "roleFlagLikeFront".
     */
    public void setRoleFlagLikeFront(String roleFlagLikeFront) {
        this.roleFlagLikeFront = roleFlagLikeFront;
    }

    /**
     * Getter method of "userLastUpdateGreaterThanEqual".
     * 
     * @return the "userLastUpdateGreaterThanEqual"
     */
    public Timestamp getUserLastUpdateGreaterThanEqual() {
        return userLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "userLastUpdateGreaterThanEqual".
     * 
     * @param userLastUpdateGreaterThanEqual Set in "userLastUpdateGreaterThanEqual".
     */
    public void setUserLastUpdateGreaterThanEqual(Timestamp userLastUpdateGreaterThanEqual) {
        this.userLastUpdateGreaterThanEqual = userLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "userLastUpdateLessThanEqual".
     * 
     * @return the "userLastUpdateLessThanEqual"
     */
    public Timestamp getUserLastUpdateLessThanEqual() {
        return userLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "userLastUpdateLessThanEqual".
     * 
     * @param userLastUpdateLessThanEqual Set in "userLastUpdateLessThanEqual".
     */
    public void setUserLastUpdateLessThanEqual(Timestamp userLastUpdateLessThanEqual) {
        this.userLastUpdateLessThanEqual = userLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "supplierLastUpdateGreaterThanEqual".
     * 
     * @return the "supplierLastUpdateGreaterThanEqual"
     */
    public Timestamp getSupplierLastUpdateGreaterThanEqual() {
        return supplierLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "supplierLastUpdateGreaterThanEqual".
     * 
     * @param supplierLastUpdateGreaterThanEqual Set in "supplierLastUpdateGreaterThanEqual".
     */
    public void setSupplierLastUpdateGreaterThanEqual(Timestamp supplierLastUpdateGreaterThanEqual) {
        this.supplierLastUpdateGreaterThanEqual = supplierLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "supplierLastUpdateLessThanEqual".
     * 
     * @return the "supplierLastUpdateLessThanEqual"
     */
    public Timestamp getSupplierLastUpdateLessThanEqual() {
        return supplierLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "supplierLastUpdateLessThanEqual".
     * 
     * @param supplierLastUpdateLessThanEqual Set in "supplierLastUpdateLessThanEqual".
     */
    public void setSupplierLastUpdateLessThanEqual(Timestamp supplierLastUpdateLessThanEqual) {
        this.supplierLastUpdateLessThanEqual = supplierLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "roleLastUpdateGreaterThanEqual".
     * 
     * @return the "roleLastUpdateGreaterThanEqual"
     */
    public Timestamp getRoleLastUpdateGreaterThanEqual() {
        return roleLastUpdateGreaterThanEqual;
    }

    /**
     * Setter method of "roleLastUpdateGreaterThanEqual".
     * 
     * @param roleLastUpdateGreaterThanEqual Set in "roleLastUpdateGreaterThanEqual".
     */
    public void setRoleLastUpdateGreaterThanEqual(Timestamp roleLastUpdateGreaterThanEqual) {
        this.roleLastUpdateGreaterThanEqual = roleLastUpdateGreaterThanEqual;
    }

    /**
     * Getter method of "roleLastUpdateLessThanEqual".
     * 
     * @return the "roleLastUpdateLessThanEqual"
     */
    public Timestamp getRoleLastUpdateLessThanEqual() {
        return roleLastUpdateLessThanEqual;
    }

    /**
     * Setter method of "roleLastUpdateLessThanEqual".
     * 
     * @param roleLastUpdateLessThanEqual Set in "roleLastUpdateLessThanEqual".
     */
    public void setRoleLastUpdateLessThanEqual(Timestamp roleLastUpdateLessThanEqual) {
        this.roleLastUpdateLessThanEqual = roleLastUpdateLessThanEqual;
    }

    /**
     * Getter method of "isActualRegisterLikeFront".
     * 
     * @return the "isActualRegisterLikeFront"
     */
    public String getIsActualRegisterLikeFront() {
        return isActualRegisterLikeFront;
    }

    /**
     * Setter method of "isActualRegisterLikeFront".
     * 
     * @param isActualRegisterLikeFront Set in "isActualRegisterLikeFront".
     */
    public void setIsActualRegisterLikeFront(String isActualRegisterLikeFront) {
        this.isActualRegisterLikeFront = isActualRegisterLikeFront;
    }

    /**
     * Getter method of "toActualDatetimeGreaterThanEqual".
     * 
     * @return the "toActualDatetimeGreaterThanEqual"
     */
    public Timestamp getToActualDatetimeGreaterThanEqual() {
        return toActualDatetimeGreaterThanEqual;
    }

    /**
     * Setter method of "toActualDatetimeGreaterThanEqual".
     * 
     * @param toActualDatetimeGreaterThanEqual Set in "toActualDatetimeGreaterThanEqual".
     */
    public void setToActualDatetimeGreaterThanEqual(Timestamp toActualDatetimeGreaterThanEqual) {
        this.toActualDatetimeGreaterThanEqual = toActualDatetimeGreaterThanEqual;
    }

    /**
     * Getter method of "toActualDatetimeLessThanEqual".
     * 
     * @return the "toActualDatetimeLessThanEqual"
     */
    public Timestamp getToActualDatetimeLessThanEqual() {
        return toActualDatetimeLessThanEqual;
    }

    /**
     * Setter method of "toActualDatetimeLessThanEqual".
     * 
     * @param toActualDatetimeLessThanEqual Set in "toActualDatetimeLessThanEqual".
     */
    public void setToActualDatetimeLessThanEqual(Timestamp toActualDatetimeLessThanEqual) {
        this.toActualDatetimeLessThanEqual = toActualDatetimeLessThanEqual;
    }

}
