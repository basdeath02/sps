/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


public class SPSTAsnDetailDNDomain   {
	private static final long serialVersionUID = 1L;
	private String asnNo;
	private String dCd;
	private String sPn;
	private String dPn;
	private BigDecimal doId;
	private String spsDoNo;
	private String reVision;
	private String cigmaDoNo;
	private BigDecimal shippingQty;
	private BigDecimal shippingRoundQty;
	private BigDecimal qtyBox;
	private BigDecimal shippingBoxQty;
	private String rcvLane;
	private String unitOfMeasure;
	private String changeReasonCd;
	private String reviseShippingQtyFlag;
	private String allowReviseFlag;
	private String createDscId;
	private String createDateTime;
	private String lastUpdateDscId;
	private String lastUpdateDatetime;
	private String kanbanType;
	private String receivedDate;
	private String receivingStatus;
	private String var;
	private String ins;
	private String wh;
	private String dueDate;
	private String processFlag;
	private String databaseName;
	private String sCd;
	private String sPcd;
	private String dPcd;
	private String vendorCd;
	
	private String cigmaCurrDoNo;
	
	public String getAsnNo() {
		return asnNo;
	}
	public void setAsnNo(String asnNo) {
		this.asnNo = asnNo;
	}
	public String getdCd() {
		return dCd;
	}
	public void setdCd(String dCd) {
		this.dCd = dCd;
	}
	public String getsPn() {
		return sPn;
	}
	public void setsPn(String sPn) {
		this.sPn = sPn;
	}
	public String getdPn() {
		return dPn;
	}
	public void setdPn(String dPn) {
		this.dPn = dPn;
	}
	public BigDecimal getDoId() {
		return doId;
	}
	public void setDoId(BigDecimal doId) {
		this.doId = doId;
	}
	
	public String getSpsDoNo() {
		return spsDoNo;
	}
	public void setSpsDoNo(String spsDoNo) {
		this.spsDoNo = spsDoNo;
	}
	public String getReVision() {
		return reVision;
	}
	public void setReVision(String reVision) {
		this.reVision = reVision;
	}
	public String getCigmaDoNo() {
		return cigmaDoNo;
	}
	public void setCigmaDoNo(String cigmaDoNo) {
		this.cigmaDoNo = cigmaDoNo;
	}
	public BigDecimal getShippingQty() {
		return shippingQty;
	}
	public void setShippingQty(BigDecimal shippingQty) {
		this.shippingQty = shippingQty;
	}
	public BigDecimal getShippingRoundQty() {
		return shippingRoundQty;
	}
	public void setShippingRoundQty(BigDecimal shippingRoundQty) {
		this.shippingRoundQty = shippingRoundQty;
	}
	public BigDecimal getQtyBox() {
		return qtyBox;
	}
	public void setQtyBox(BigDecimal qtyBox) {
		this.qtyBox = qtyBox;
	}
	public BigDecimal getShippingBoxQty() {
		return shippingBoxQty;
	}
	public void setShippingBoxQty(BigDecimal shippingBoxQty) {
		this.shippingBoxQty = shippingBoxQty;
	}
	public String getRcvLane() {
		return rcvLane;
	}
	public void setRcvLane(String rcvLane) {
		this.rcvLane = rcvLane;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public String getChangeReasonCd() {
		return changeReasonCd;
	}
	public void setChangeReasonCd(String changeReasonCd) {
		this.changeReasonCd = changeReasonCd;
	}
	public String getReviseShippingQtyFlag() {
		return reviseShippingQtyFlag;
	}
	public void setReviseShippingQtyFlag(String reviseShippingQtyFlag) {
		this.reviseShippingQtyFlag = reviseShippingQtyFlag;
	}
	public String getAllowReviseFlag() {
		return allowReviseFlag;
	}
	public void setAllowReviseFlag(String allowReviseFlag) {
		this.allowReviseFlag = allowReviseFlag;
	}
	public String getCreateDscId() {
		return createDscId;
	}
	public void setCreateDscId(String createDscId) {
		this.createDscId = createDscId;
	}
	public String getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}
	public String getLastUpdateDscId() {
		return lastUpdateDscId;
	}
	public void setLastUpdateDscId(String lastUpdateDscId) {
		this.lastUpdateDscId = lastUpdateDscId;
	}
	public String getLastUpdateDatetime() {
		return lastUpdateDatetime;
	}
	public void setLastUpdateDatetime(String lastUpdateDatetime) {
		this.lastUpdateDatetime = lastUpdateDatetime;
	}
	public String getKanbanType() {
		return kanbanType;
	}
	public void setKanbanType(String kanbanType) {
		this.kanbanType = kanbanType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getReceivingStatus() {
		return receivingStatus;
	}
	public void setReceivingStatus(String receivingStatus) {
		this.receivingStatus = receivingStatus;
	}
	public String getVar() {
		return var;
	}
	public void setVar(String var) {
		this.var = var;
	}
	public String getIns() {
		return ins;
	}
	public void setIns(String ins) {
		this.ins = ins;
	}
	public String getWh() {
		return wh;
	}
	public void setWh(String wh) {
		this.wh = wh;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getsCd() {
		return sCd;
	}
	public void setsCd(String sCd) {
		this.sCd = sCd;
	}
	public String getsPcd() {
		return sPcd;
	}
	public void setsPcd(String sPcd) {
		this.sPcd = sPcd;
	}
	public String getdPcd() {
		return dPcd;
	}
	public void setdPcd(String dPcd) {
		this.dPcd = dPcd;
	}
	public String getVendorCd() {
		return vendorCd;
	}
	public void setVendorCd(String vendorCd) {
		this.vendorCd = vendorCd;
	}
    /**
     * <p>Getter method for cigmaOriginDoNo.</p>
     *
     * @return the cigmaOriginDoNo
     */
    public String getCigmaCurrDoNo() {
        return cigmaCurrDoNo;
    }
    /**
     * <p>Setter method for cigmaOriginDoNo.</p>
     *
     * @param cigmaOriginDoNo Set for cigmaOriginDoNo
     */
    public void setCigmaCurrDoNo(String cigmaCurrDoNo) {
        this.cigmaCurrDoNo = cigmaCurrDoNo;
    }
	
	
}
