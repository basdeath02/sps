/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsMPlantSupplier"<br />
 * Table overview: SPS_M_PLANT_SUPPLIER<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMPlantSupplierDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Supplier plant code
     */
    private String sPcd;

    /**
     * Supplier company DSC ID
     */
    private String sCd;

    /**
     * Supplier plant name
     */
    private String plantName;

    /**
     * Address
     */
    private String address;

    /**
     * Telephone no.
     */
    private String telephone;

    /**
     * Fax no.
     */
    private String fax;

    /**
     * Payment term
     */
    private BigDecimal paymentTerm;

    /**
     * Supplier user DSC ID
     */
    private String contactPerson;

    /**
     * Is active
     */
    private String isActive;

    /**
     * Create user
     */
    private String createDscId;

    /**
     * Create datetime
     */
    private Timestamp createDatetime;

    /**
     * Last update user
     */
    private String lastUpdateDscId;

    /**
     * Last update datetime
     */
    private Timestamp lastUpdateDatetime;

    /**
     * Default constructor
     */
    public SpsMPlantSupplierDomain() {
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "plantName"
     * 
     * @return the plantName
     */
    public String getPlantName() {
        return plantName;
    }

    /**
     * Setter method of "plantName"
     * 
     * @param plantName Set in "plantName".
     */
    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    /**
     * Getter method of "address"
     * 
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter method of "address"
     * 
     * @param address Set in "address".
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Getter method of "telephone"
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Setter method of "telephone"
     * 
     * @param telephone Set in "telephone".
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Getter method of "fax"
     * 
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * Setter method of "fax"
     * 
     * @param fax Set in "fax".
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Getter method of "paymentTerm"
     * 
     * @return the paymentTerm
     */
    public BigDecimal getPaymentTerm() {
        return paymentTerm;
    }

    /**
     * Setter method of "paymentTerm"
     * 
     * @param paymentTerm Set in "paymentTerm".
     */
    public void setPaymentTerm(BigDecimal paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     * Getter method of "contactPerson"
     * 
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Setter method of "contactPerson"
     * 
     * @param contactPerson Set in "contactPerson".
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Getter method of "isActive"
     * 
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Setter method of "isActive"
     * 
     * @param isActive Set in "isActive".
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

}
