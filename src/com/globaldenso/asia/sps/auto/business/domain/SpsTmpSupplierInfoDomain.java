/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A "Domain" class of "SpsTmpSupplierInfo"<br />
 * Table overview: SPS_TMP_SUPPLIER_INFO<br />
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpSupplierInfoDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * USER_DSC_ID
     */
    private String userDscId;

    /**
     * SESSION_CD
     */
    private String sessionCd;

    /**
     * LINE_NO
     */
    private BigDecimal lineNo;

    /**
     * D_CD
     */
    private String dCd;

    /**
     * S_CD
     */
    private String sCd;

    /**
     * D_PCD
     */
    private String dPcd;

    /**
     * S_PCD
     */
    private String sPcd;

    /**
     * D_PN
     */
    private String dPn;

    /**
     * S_PN
     */
    private String sPn;

    /**
     * EFFECT_DATE
     */
    private Timestamp effectDate;

    /**
     * UPLOAD_DATETIME
     */
    private Timestamp uploadDatetime;

    /**
     * RELATION_LAST_UPDATE
     */
    private Timestamp relationLastUpdate;

    /**
     * PART_LAST_UPDATE
     */
    private Timestamp partLastUpdate;

    /**
     * IS_ACTUAL_REGISTER
     */
    private String isActualRegister;

    /**
     * TO_ACTUAL_DATETIME
     */
    private Timestamp toActualDatetime;

    /**
     * FLAG
     */
    private String flag;

    /**
     * VENDOR_CD
     */
    private String vendorCd;

    /**
     * Default constructor
     */
    public SpsTmpSupplierInfoDomain() {
    }

    /**
     * Getter method of "userDscId"
     * 
     * @return the userDscId
     */
    public String getUserDscId() {
        return userDscId;
    }

    /**
     * Setter method of "userDscId"
     * 
     * @param userDscId Set in "userDscId".
     */
    public void setUserDscId(String userDscId) {
        this.userDscId = userDscId;
    }

    /**
     * Getter method of "sessionCd"
     * 
     * @return the sessionCd
     */
    public String getSessionCd() {
        return sessionCd;
    }

    /**
     * Setter method of "sessionCd"
     * 
     * @param sessionCd Set in "sessionCd".
     */
    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    /**
     * Getter method of "lineNo"
     * 
     * @return the lineNo
     */
    public BigDecimal getLineNo() {
        return lineNo;
    }

    /**
     * Setter method of "lineNo"
     * 
     * @param lineNo Set in "lineNo".
     */
    public void setLineNo(BigDecimal lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * Getter method of "dCd"
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd"
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "sCd"
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd"
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "dPcd"
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd"
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "sPcd"
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd"
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dPn"
     * 
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn"
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "sPn"
     * 
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * Setter method of "sPn"
     * 
     * @param sPn Set in "sPn".
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * Getter method of "effectDate"
     * 
     * @return the effectDate
     */
    public Timestamp getEffectDate() {
        return effectDate;
    }

    /**
     * Setter method of "effectDate"
     * 
     * @param effectDate Set in "effectDate".
     */
    public void setEffectDate(Timestamp effectDate) {
        this.effectDate = effectDate;
    }

    /**
     * Getter method of "uploadDatetime"
     * 
     * @return the uploadDatetime
     */
    public Timestamp getUploadDatetime() {
        return uploadDatetime;
    }

    /**
     * Setter method of "uploadDatetime"
     * 
     * @param uploadDatetime Set in "uploadDatetime".
     */
    public void setUploadDatetime(Timestamp uploadDatetime) {
        this.uploadDatetime = uploadDatetime;
    }

    /**
     * Getter method of "relationLastUpdate"
     * 
     * @return the relationLastUpdate
     */
    public Timestamp getRelationLastUpdate() {
        return relationLastUpdate;
    }

    /**
     * Setter method of "relationLastUpdate"
     * 
     * @param relationLastUpdate Set in "relationLastUpdate".
     */
    public void setRelationLastUpdate(Timestamp relationLastUpdate) {
        this.relationLastUpdate = relationLastUpdate;
    }

    /**
     * Getter method of "partLastUpdate"
     * 
     * @return the partLastUpdate
     */
    public Timestamp getPartLastUpdate() {
        return partLastUpdate;
    }

    /**
     * Setter method of "partLastUpdate"
     * 
     * @param partLastUpdate Set in "partLastUpdate".
     */
    public void setPartLastUpdate(Timestamp partLastUpdate) {
        this.partLastUpdate = partLastUpdate;
    }

    /**
     * Getter method of "isActualRegister"
     * 
     * @return the isActualRegister
     */
    public String getIsActualRegister() {
        return isActualRegister;
    }

    /**
     * Setter method of "isActualRegister"
     * 
     * @param isActualRegister Set in "isActualRegister".
     */
    public void setIsActualRegister(String isActualRegister) {
        this.isActualRegister = isActualRegister;
    }

    /**
     * Getter method of "toActualDatetime"
     * 
     * @return the toActualDatetime
     */
    public Timestamp getToActualDatetime() {
        return toActualDatetime;
    }

    /**
     * Setter method of "toActualDatetime"
     * 
     * @param toActualDatetime Set in "toActualDatetime".
     */
    public void setToActualDatetime(Timestamp toActualDatetime) {
        this.toActualDatetime = toActualDatetime;
    }

    /**
     * Getter method of "flag"
     * 
     * @return the flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Setter method of "flag"
     * 
     * @param flag Set in "flag".
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * Getter method of "vendorCd"
     * 
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd"
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

}
