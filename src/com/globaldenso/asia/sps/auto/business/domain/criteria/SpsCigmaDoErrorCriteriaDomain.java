/*
 * Project : SPS
 *
 *
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.domain.criteria;

import com.globaldenso.asia.sps.business.domain.BaseDomain;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * A search criteria "Domain" class of "SpsCigmaDoError".<br />
 * Table overview: SPS_CIGMA_DO_ERROR<br />
 *
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaDoErrorCriteriaDomain extends BaseDomain {

    /**
     * Serial version ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Running No for D/O Error
     */
    private BigDecimal doErrorId;

    /**
     * CIGMA D/O No
     */
    private String cigmaDoNo;

    /**
     * Data Type from CIGMA
     */
    private String dataType;

    /**
     * AS/400 Supplier Code
     */
    private String vendorCd;

    /**
     * D/O Issue Date
     */
    private Date issueDate;

    /**
     * DENSO Part No
     */
    private String dPn;

    /**
     * Current Order Qty
     */
    private BigDecimal currentOrderQty;

    /**
     * Mail Sending Flag
0 : Do not send, 1: Send
     */
    private String mailFlg;

    /**
     * Update by User ID
     */
    private String updateByUserId;

    /**
     * Supplier Code
     */
    private String sCd;

    /**
     * Supplier Plant Code
     */
    private String sPcd;

    /**
     * DENSO Company Code
     */
    private String dCd;

    /**
     * DENSO Plant Code
     */
    private String dPcd;

    /**
     * D/O Delivery Date
     */
    private Date deliveryDate;

    /**
     * D/O Delivery Time
     */
    private String deliveryTime;

    /**
     * Warehouse Prime Receiving
     */
    private String wh;

    /**
     * Dockcode
     */
    private String dockCode;

    /**
     * SPS Error Type Flag
1 : VENDOR_CD not found in SPS system
2 : PARTS_NO not found in SPS system
3 : D/O is already exist in SPS system
4 : P/O number not found in SPS system
5 : Cannot separate D/O in SPS system
6 : D/O is used in ASN creating
     */
    private String errorTypeFlg;

    /**
     * CIGMA D/O No(condition whether the column value at the beginning is equal to the value)
     */
    private String cigmaDoNoLikeFront;

    /**
     * Data Type from CIGMA(condition whether the column value at the beginning is equal to the value)
     */
    private String dataTypeLikeFront;

    /**
     * AS/400 Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String vendorCdLikeFront;

    /**
     * D/O Issue Date(condition whether the column value is greater than or equal to the value)
     */
    private Date issueDateGreaterThanEqual;

    /**
     * D/O Issue Date(condition whether the column value is less than or equal to the value)
     */
    private Date issueDateLessThanEqual;

    /**
     * DENSO Part No(condition whether the column value at the beginning is equal to the value)
     */
    private String dPnLikeFront;

    /**
     * Mail Sending Flag
0 : Do not send, 1: Send(condition whether the column value at the beginning is equal to the value)
     */
    private String mailFlgLikeFront;

    /**
     * Update by User ID(condition whether the column value at the beginning is equal to the value)
     */
    private String updateByUserIdLikeFront;

    /**
     * Supplier Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sCdLikeFront;

    /**
     * Supplier Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String sPcdLikeFront;

    /**
     * DENSO Company Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dCdLikeFront;

    /**
     * DENSO Plant Code(condition whether the column value at the beginning is equal to the value)
     */
    private String dPcdLikeFront;

    /**
     * D/O Delivery Date(condition whether the column value is greater than or equal to the value)
     */
    private Date deliveryDateGreaterThanEqual;

    /**
     * D/O Delivery Date(condition whether the column value is less than or equal to the value)
     */
    private Date deliveryDateLessThanEqual;

    /**
     * D/O Delivery Time(condition whether the column value at the beginning is equal to the value)
     */
    private String deliveryTimeLikeFront;

    /**
     * Warehouse Prime Receiving(condition whether the column value at the beginning is equal to the value)
     */
    private String whLikeFront;

    /**
     * Dockcode(condition whether the column value at the beginning is equal to the value)
     */
    private String dockCodeLikeFront;

    /**
     * SPS Error Type Flag
1 : VENDOR_CD not found in SPS system
2 : PARTS_NO not found in SPS system
3 : D/O is already exist in SPS system
4 : P/O number not found in SPS system
5 : Cannot separate D/O in SPS system
6 : D/O is used in ASN creating(condition whether the column value at the beginning is equal to the value)
     */
    private String errorTypeFlgLikeFront;

    /**
     * Default constructor
     */
    public SpsCigmaDoErrorCriteriaDomain() {
    }

    /**
     * Getter method of "doErrorId".
     * 
     * @return the "doErrorId"
     */
    public BigDecimal getDoErrorId() {
        return doErrorId;
    }

    /**
     * Setter method of "doErrorId".
     * 
     * @param doErrorId Set in "doErrorId".
     */
    public void setDoErrorId(BigDecimal doErrorId) {
        this.doErrorId = doErrorId;
    }

    /**
     * Getter method of "cigmaDoNo".
     * 
     * @return the "cigmaDoNo"
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Setter method of "cigmaDoNo".
     * 
     * @param cigmaDoNo Set in "cigmaDoNo".
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Getter method of "dataType".
     * 
     * @return the "dataType"
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Setter method of "dataType".
     * 
     * @param dataType Set in "dataType".
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * Getter method of "vendorCd".
     * 
     * @return the "vendorCd"
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Setter method of "vendorCd".
     * 
     * @param vendorCd Set in "vendorCd".
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * Getter method of "issueDate".
     * 
     * @return the "issueDate"
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Setter method of "issueDate".
     * 
     * @param issueDate Set in "issueDate".
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Getter method of "dPn".
     * 
     * @return the "dPn"
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * Setter method of "dPn".
     * 
     * @param dPn Set in "dPn".
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * Getter method of "currentOrderQty".
     * 
     * @return the "currentOrderQty"
     */
    public BigDecimal getCurrentOrderQty() {
        return currentOrderQty;
    }

    /**
     * Setter method of "currentOrderQty".
     * 
     * @param currentOrderQty Set in "currentOrderQty".
     */
    public void setCurrentOrderQty(BigDecimal currentOrderQty) {
        this.currentOrderQty = currentOrderQty;
    }

    /**
     * Getter method of "mailFlg".
     * 
     * @return the "mailFlg"
     */
    public String getMailFlg() {
        return mailFlg;
    }

    /**
     * Setter method of "mailFlg".
     * 
     * @param mailFlg Set in "mailFlg".
     */
    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    /**
     * Getter method of "updateByUserId".
     * 
     * @return the "updateByUserId"
     */
    public String getUpdateByUserId() {
        return updateByUserId;
    }

    /**
     * Setter method of "updateByUserId".
     * 
     * @param updateByUserId Set in "updateByUserId".
     */
    public void setUpdateByUserId(String updateByUserId) {
        this.updateByUserId = updateByUserId;
    }

    /**
     * Getter method of "sCd".
     * 
     * @return the "sCd"
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Setter method of "sCd".
     * 
     * @param sCd Set in "sCd".
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Getter method of "sPcd".
     * 
     * @return the "sPcd"
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Setter method of "sPcd".
     * 
     * @param sPcd Set in "sPcd".
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Getter method of "dCd".
     * 
     * @return the "dCd"
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Setter method of "dCd".
     * 
     * @param dCd Set in "dCd".
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Getter method of "dPcd".
     * 
     * @return the "dPcd"
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Setter method of "dPcd".
     * 
     * @param dPcd Set in "dPcd".
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Getter method of "deliveryDate".
     * 
     * @return the "deliveryDate"
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Setter method of "deliveryDate".
     * 
     * @param deliveryDate Set in "deliveryDate".
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * Getter method of "deliveryTime".
     * 
     * @return the "deliveryTime"
     */
    public String getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * Setter method of "deliveryTime".
     * 
     * @param deliveryTime Set in "deliveryTime".
     */
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * Getter method of "wh".
     * 
     * @return the "wh"
     */
    public String getWh() {
        return wh;
    }

    /**
     * Setter method of "wh".
     * 
     * @param wh Set in "wh".
     */
    public void setWh(String wh) {
        this.wh = wh;
    }

    /**
     * Getter method of "dockCode".
     * 
     * @return the "dockCode"
     */
    public String getDockCode() {
        return dockCode;
    }

    /**
     * Setter method of "dockCode".
     * 
     * @param dockCode Set in "dockCode".
     */
    public void setDockCode(String dockCode) {
        this.dockCode = dockCode;
    }

    /**
     * Getter method of "errorTypeFlg".
     * 
     * @return the "errorTypeFlg"
     */
    public String getErrorTypeFlg() {
        return errorTypeFlg;
    }

    /**
     * Setter method of "errorTypeFlg".
     * 
     * @param errorTypeFlg Set in "errorTypeFlg".
     */
    public void setErrorTypeFlg(String errorTypeFlg) {
        this.errorTypeFlg = errorTypeFlg;
    }

    /**
     * Getter method of "cigmaDoNoLikeFront".
     * 
     * @return the "cigmaDoNoLikeFront"
     */
    public String getCigmaDoNoLikeFront() {
        return cigmaDoNoLikeFront;
    }

    /**
     * Setter method of "cigmaDoNoLikeFront".
     * 
     * @param cigmaDoNoLikeFront Set in "cigmaDoNoLikeFront".
     */
    public void setCigmaDoNoLikeFront(String cigmaDoNoLikeFront) {
        this.cigmaDoNoLikeFront = cigmaDoNoLikeFront;
    }

    /**
     * Getter method of "dataTypeLikeFront".
     * 
     * @return the "dataTypeLikeFront"
     */
    public String getDataTypeLikeFront() {
        return dataTypeLikeFront;
    }

    /**
     * Setter method of "dataTypeLikeFront".
     * 
     * @param dataTypeLikeFront Set in "dataTypeLikeFront".
     */
    public void setDataTypeLikeFront(String dataTypeLikeFront) {
        this.dataTypeLikeFront = dataTypeLikeFront;
    }

    /**
     * Getter method of "vendorCdLikeFront".
     * 
     * @return the "vendorCdLikeFront"
     */
    public String getVendorCdLikeFront() {
        return vendorCdLikeFront;
    }

    /**
     * Setter method of "vendorCdLikeFront".
     * 
     * @param vendorCdLikeFront Set in "vendorCdLikeFront".
     */
    public void setVendorCdLikeFront(String vendorCdLikeFront) {
        this.vendorCdLikeFront = vendorCdLikeFront;
    }

    /**
     * Getter method of "issueDateGreaterThanEqual".
     * 
     * @return the "issueDateGreaterThanEqual"
     */
    public Date getIssueDateGreaterThanEqual() {
        return issueDateGreaterThanEqual;
    }

    /**
     * Setter method of "issueDateGreaterThanEqual".
     * 
     * @param issueDateGreaterThanEqual Set in "issueDateGreaterThanEqual".
     */
    public void setIssueDateGreaterThanEqual(Date issueDateGreaterThanEqual) {
        this.issueDateGreaterThanEqual = issueDateGreaterThanEqual;
    }

    /**
     * Getter method of "issueDateLessThanEqual".
     * 
     * @return the "issueDateLessThanEqual"
     */
    public Date getIssueDateLessThanEqual() {
        return issueDateLessThanEqual;
    }

    /**
     * Setter method of "issueDateLessThanEqual".
     * 
     * @param issueDateLessThanEqual Set in "issueDateLessThanEqual".
     */
    public void setIssueDateLessThanEqual(Date issueDateLessThanEqual) {
        this.issueDateLessThanEqual = issueDateLessThanEqual;
    }

    /**
     * Getter method of "dPnLikeFront".
     * 
     * @return the "dPnLikeFront"
     */
    public String getDPnLikeFront() {
        return dPnLikeFront;
    }

    /**
     * Setter method of "dPnLikeFront".
     * 
     * @param dPnLikeFront Set in "dPnLikeFront".
     */
    public void setDPnLikeFront(String dPnLikeFront) {
        this.dPnLikeFront = dPnLikeFront;
    }

    /**
     * Getter method of "mailFlgLikeFront".
     * 
     * @return the "mailFlgLikeFront"
     */
    public String getMailFlgLikeFront() {
        return mailFlgLikeFront;
    }

    /**
     * Setter method of "mailFlgLikeFront".
     * 
     * @param mailFlgLikeFront Set in "mailFlgLikeFront".
     */
    public void setMailFlgLikeFront(String mailFlgLikeFront) {
        this.mailFlgLikeFront = mailFlgLikeFront;
    }

    /**
     * Getter method of "updateByUserIdLikeFront".
     * 
     * @return the "updateByUserIdLikeFront"
     */
    public String getUpdateByUserIdLikeFront() {
        return updateByUserIdLikeFront;
    }

    /**
     * Setter method of "updateByUserIdLikeFront".
     * 
     * @param updateByUserIdLikeFront Set in "updateByUserIdLikeFront".
     */
    public void setUpdateByUserIdLikeFront(String updateByUserIdLikeFront) {
        this.updateByUserIdLikeFront = updateByUserIdLikeFront;
    }

    /**
     * Getter method of "sCdLikeFront".
     * 
     * @return the "sCdLikeFront"
     */
    public String getSCdLikeFront() {
        return sCdLikeFront;
    }

    /**
     * Setter method of "sCdLikeFront".
     * 
     * @param sCdLikeFront Set in "sCdLikeFront".
     */
    public void setSCdLikeFront(String sCdLikeFront) {
        this.sCdLikeFront = sCdLikeFront;
    }

    /**
     * Getter method of "sPcdLikeFront".
     * 
     * @return the "sPcdLikeFront"
     */
    public String getSPcdLikeFront() {
        return sPcdLikeFront;
    }

    /**
     * Setter method of "sPcdLikeFront".
     * 
     * @param sPcdLikeFront Set in "sPcdLikeFront".
     */
    public void setSPcdLikeFront(String sPcdLikeFront) {
        this.sPcdLikeFront = sPcdLikeFront;
    }

    /**
     * Getter method of "dCdLikeFront".
     * 
     * @return the "dCdLikeFront"
     */
    public String getDCdLikeFront() {
        return dCdLikeFront;
    }

    /**
     * Setter method of "dCdLikeFront".
     * 
     * @param dCdLikeFront Set in "dCdLikeFront".
     */
    public void setDCdLikeFront(String dCdLikeFront) {
        this.dCdLikeFront = dCdLikeFront;
    }

    /**
     * Getter method of "dPcdLikeFront".
     * 
     * @return the "dPcdLikeFront"
     */
    public String getDPcdLikeFront() {
        return dPcdLikeFront;
    }

    /**
     * Setter method of "dPcdLikeFront".
     * 
     * @param dPcdLikeFront Set in "dPcdLikeFront".
     */
    public void setDPcdLikeFront(String dPcdLikeFront) {
        this.dPcdLikeFront = dPcdLikeFront;
    }

    /**
     * Getter method of "deliveryDateGreaterThanEqual".
     * 
     * @return the "deliveryDateGreaterThanEqual"
     */
    public Date getDeliveryDateGreaterThanEqual() {
        return deliveryDateGreaterThanEqual;
    }

    /**
     * Setter method of "deliveryDateGreaterThanEqual".
     * 
     * @param deliveryDateGreaterThanEqual Set in "deliveryDateGreaterThanEqual".
     */
    public void setDeliveryDateGreaterThanEqual(Date deliveryDateGreaterThanEqual) {
        this.deliveryDateGreaterThanEqual = deliveryDateGreaterThanEqual;
    }

    /**
     * Getter method of "deliveryDateLessThanEqual".
     * 
     * @return the "deliveryDateLessThanEqual"
     */
    public Date getDeliveryDateLessThanEqual() {
        return deliveryDateLessThanEqual;
    }

    /**
     * Setter method of "deliveryDateLessThanEqual".
     * 
     * @param deliveryDateLessThanEqual Set in "deliveryDateLessThanEqual".
     */
    public void setDeliveryDateLessThanEqual(Date deliveryDateLessThanEqual) {
        this.deliveryDateLessThanEqual = deliveryDateLessThanEqual;
    }

    /**
     * Getter method of "deliveryTimeLikeFront".
     * 
     * @return the "deliveryTimeLikeFront"
     */
    public String getDeliveryTimeLikeFront() {
        return deliveryTimeLikeFront;
    }

    /**
     * Setter method of "deliveryTimeLikeFront".
     * 
     * @param deliveryTimeLikeFront Set in "deliveryTimeLikeFront".
     */
    public void setDeliveryTimeLikeFront(String deliveryTimeLikeFront) {
        this.deliveryTimeLikeFront = deliveryTimeLikeFront;
    }

    /**
     * Getter method of "whLikeFront".
     * 
     * @return the "whLikeFront"
     */
    public String getWhLikeFront() {
        return whLikeFront;
    }

    /**
     * Setter method of "whLikeFront".
     * 
     * @param whLikeFront Set in "whLikeFront".
     */
    public void setWhLikeFront(String whLikeFront) {
        this.whLikeFront = whLikeFront;
    }

    /**
     * Getter method of "dockCodeLikeFront".
     * 
     * @return the "dockCodeLikeFront"
     */
    public String getDockCodeLikeFront() {
        return dockCodeLikeFront;
    }

    /**
     * Setter method of "dockCodeLikeFront".
     * 
     * @param dockCodeLikeFront Set in "dockCodeLikeFront".
     */
    public void setDockCodeLikeFront(String dockCodeLikeFront) {
        this.dockCodeLikeFront = dockCodeLikeFront;
    }

    /**
     * Getter method of "errorTypeFlgLikeFront".
     * 
     * @return the "errorTypeFlgLikeFront"
     */
    public String getErrorTypeFlgLikeFront() {
        return errorTypeFlgLikeFront;
    }

    /**
     * Setter method of "errorTypeFlgLikeFront".
     * 
     * @param errorTypeFlgLikeFront Set in "errorTypeFlgLikeFront".
     */
    public void setErrorTypeFlgLikeFront(String errorTypeFlgLikeFront) {
        this.errorTypeFlgLikeFront = errorTypeFlgLikeFront;
    }

}
