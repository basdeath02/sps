/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/10/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain;


/**
 * A "Service" implementation class of "SpsTmpUploadInvoice"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/10/30 14:41:58<br />
 * 
 * This module generated automatically in 2015/10/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadInvoiceServiceImpl implements SpsTmpUploadInvoiceService {

    /**
     * A "Dao" of "spsTmpUploadInvoice"
     */
    private SpsTmpUploadInvoiceDao spsTmpUploadInvoiceDao;

    /**
     * Default constructor
     */
    public SpsTmpUploadInvoiceServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpUploadInvoice"
     * 
     * @param spsTmpUploadInvoiceDao spsTmpUploadInvoiceDao
     */
    public void setSpsTmpUploadInvoiceDao(SpsTmpUploadInvoiceDao spsTmpUploadInvoiceDao) {
        this.spsTmpUploadInvoiceDao = spsTmpUploadInvoiceDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public SpsTmpUploadInvoiceDomain searchByKey(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public List<SpsTmpUploadInvoiceDomain> searchByCondition(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public List<SpsTmpUploadInvoiceDomain> searchByConditionForPaging(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int searchCount(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUploadInvoiceDomain searchByKeyForChange(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public SpsTmpUploadInvoiceDomain lockByKey(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public SpsTmpUploadInvoiceDomain lockByKeyNoWait(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain)
     */
    public void create(SpsTmpUploadInvoiceDomain domain) throws ApplicationException {
        spsTmpUploadInvoiceDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#create(java.util.List)
     */
    public void create(List<SpsTmpUploadInvoiceDomain> domains) throws ApplicationException {
        for(SpsTmpUploadInvoiceDomain domain : domains) {
            spsTmpUploadInvoiceDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain)
     */
    public int update(SpsTmpUploadInvoiceDomain domain) throws ApplicationException {
        return spsTmpUploadInvoiceDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#update(java.util.List)
     */
    public int update(List<SpsTmpUploadInvoiceDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpUploadInvoiceDomain domain : domains) {
            updateCount += spsTmpUploadInvoiceDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUploadInvoiceDomain domain, SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpUploadInvoiceDomain> domains, List<SpsTmpUploadInvoiceCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpUploadInvoiceDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int delete(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadInvoiceService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadInvoiceDao.deleteByCondition(criteria);
    }

}
