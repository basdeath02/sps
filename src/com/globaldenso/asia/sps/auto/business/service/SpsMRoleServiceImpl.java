/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;


/**
 * A "Service" implementation class of "SpsMRole"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleServiceImpl implements SpsMRoleService {

    /**
     * A "Dao" of "spsMRole"
     */
    private SpsMRoleDao spsMRoleDao;

    /**
     * Default constructor
     */
    public SpsMRoleServiceImpl() {
    }

    /**
     * Set Dao of "spsMRole"
     * 
     * @param spsMRoleDao spsMRoleDao
     */
    public void setSpsMRoleDao(SpsMRoleDao spsMRoleDao) {
        this.spsMRoleDao = spsMRoleDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public SpsMRoleDomain searchByKey(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public List<SpsMRoleDomain> searchByCondition(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public List<SpsMRoleDomain> searchByConditionForPaging(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int searchCount(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleDomain searchByKeyForChange(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public SpsMRoleDomain lockByKey(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public SpsMRoleDomain lockByKeyNoWait(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain)
     */
    public void create(SpsMRoleDomain domain) throws ApplicationException {
        spsMRoleDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#create(java.util.List)
     */
    public void create(List<SpsMRoleDomain> domains) throws ApplicationException {
        for(SpsMRoleDomain domain : domains) {
            spsMRoleDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain)
     */
    public int update(SpsMRoleDomain domain) throws ApplicationException {
        return spsMRoleDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#update(java.util.List)
     */
    public int update(List<SpsMRoleDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMRoleDomain domain : domains) {
            updateCount += spsMRoleDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleDomain domain, SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMRoleDomain> domains, List<SpsMRoleCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMRoleDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int delete(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleDao.deleteByCondition(criteria);
    }

}
