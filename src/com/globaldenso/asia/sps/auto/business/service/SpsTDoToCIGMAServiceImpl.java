/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoToCIGMADao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoToCIGMADao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.service.MailService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.jobcontrol.TransferDoDataToCigmaJob;


/**
 * A "Service" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoToCIGMAServiceImpl implements SpsTDoToCIGMAService {

	private static final Log LOG = LogFactory
			.getLog(SpsTDoToCIGMAService.class);
	/**
     * A "Dao" of "spsTPo"
     */
    private SpsTDoToCIGMADao spsTDoToCIGMADao;


    /**
     * Default constructor
     */
    public SpsTDoToCIGMAServiceImpl() {
    }

    /**
     * Set Dao of "spsTPo"
     * 
     * @param spsTPoDao spsTPoDao
     */
    public void setSpsTDoToCIGMADao(SpsTDoToCIGMADao spsTDoToCIGMADao) {
        this.spsTDoToCIGMADao = spsTDoToCIGMADao;
    }

	public SpsTDoToCIGMADao getSpsTDoToCIGMADao() {
		return spsTDoToCIGMADao;
	}
	
 


	private Locale getDefaultLocale() {
		Properties propApp = Props
				.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
		String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
		String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
		String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
		ContextParams.setDefaultLocale(strDefaultLocale);
		ContextParams.setBaseDirGui(strBaseDirGui);
		ContextParams.setBaseDirMsg(strBaseDirMsg);
		return LocaleUtil.getLocaleFromString(strDefaultLocale);
	}
	
	/**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public List<SpsMDensoDensoRelationWithDODNDomain> searchByCondition(SpsMDensoDensoRelationWithDODNDomain criteria) throws ApplicationException {
    	
    	
        return spsTDoToCIGMADao.searchByCondition(criteria);
    }


    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#UpdatePoTranFlag(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoDensoRelationWithPODNDomain)
     */
    public boolean UpdateDoTranFlag(SPSTDoHeaderDNDomain criteria) throws ApplicationException {
    	Locale locale = this.getDefaultLocale();
    	boolean updateFlag=	spsTDoToCIGMADao.UpdateDoTranFlag(criteria);
    	return updateFlag;
    	
       
    }

    public List<SpsMDensoDensoRelationWithDODNDomain> searchSupplierBySchema(SpsMDensoDensoRelationWithDODNDomain criteria) throws ApplicationException {
        
        
        return spsTDoToCIGMADao.searchSupplierBySchema(criteria);
    }
  

}
