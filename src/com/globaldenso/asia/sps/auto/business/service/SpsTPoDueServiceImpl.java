/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/16       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;


/**
 * A "Service" implementation class of "SpsTPoDue"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/16 14:33:35<br />
 * 
 * This module generated automatically in 2016/02/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDueServiceImpl implements SpsTPoDueService {

    /**
     * A "Dao" of "spsTPoDue"
     */
    private SpsTPoDueDao spsTPoDueDao;

    /**
     * Default constructor
     */
    public SpsTPoDueServiceImpl() {
    }

    /**
     * Set Dao of "spsTPoDue"
     * 
     * @param spsTPoDueDao spsTPoDueDao
     */
    public void setSpsTPoDueDao(SpsTPoDueDao spsTPoDueDao) {
        this.spsTPoDueDao = spsTPoDueDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public SpsTPoDueDomain searchByKey(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public List<SpsTPoDueDomain> searchByCondition(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public List<SpsTPoDueDomain> searchByConditionForPaging(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int searchCount(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    @Deprecated
    public SpsTPoDueDomain searchByKeyForChange(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public SpsTPoDueDomain lockByKey(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public SpsTPoDueDomain lockByKeyNoWait(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain)
     */
    public void create(SpsTPoDueDomain domain) throws ApplicationException {
        spsTPoDueDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#create(java.util.List)
     */
    public void create(List<SpsTPoDueDomain> domains) throws ApplicationException {
        for(SpsTPoDueDomain domain : domains) {
            spsTPoDueDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain)
     */
    public int update(SpsTPoDueDomain domain) throws ApplicationException {
        return spsTPoDueDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#update(java.util.List)
     */
    public int update(List<SpsTPoDueDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTPoDueDomain domain : domains) {
            updateCount += spsTPoDueDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int updateByCondition(SpsTPoDueDomain domain, SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTPoDueDomain> domains, List<SpsTPoDueCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTPoDueDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int delete(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDueService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDueDao.deleteByCondition(criteria);
    }

}
