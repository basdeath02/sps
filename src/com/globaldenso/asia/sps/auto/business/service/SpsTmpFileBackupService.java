/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain;


/**
 * A "Service" interface of "SpsTmpFileBackup"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTmpFileBackupService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public SpsTmpFileBackupDomain searchByKey(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return List of Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpFileBackupDomain> searchByCondition(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return List of Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpFileBackupDomain> searchByConditionForPaging(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTmpFileBackup
     * @return Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTmpFileBackupDomain searchByKeyForChange(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public SpsTmpFileBackupDomain lockByKey(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public SpsTmpFileBackupDomain lockByKeyNoWait(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public void create(SpsTmpFileBackupDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTmpFileBackup"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTmpFileBackupDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTmpFileBackup"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTmpFileBackupDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTmpFileBackup"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTmpFileBackupDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTmpFileBackup"
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTmpFileBackupDomain domain, SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTmpFileBackup"
     * @param criteria List of CriteriaDomain of "spsTmpFileBackup"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTmpFileBackupDomain> domains, List<SpsTmpFileBackupCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpFileBackup"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException;

}
