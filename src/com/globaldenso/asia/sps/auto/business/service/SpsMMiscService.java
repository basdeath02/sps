/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;


/**
 * A "Service" interface of "SpsMMisc"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMMiscService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public SpsMMiscDomain searchByKey(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return List of Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public List<SpsMMiscDomain> searchByCondition(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return List of Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public List<SpsMMiscDomain> searchByConditionForPaging(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMMisc
     * @return Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMMiscDomain searchByKeyForChange(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public SpsMMiscDomain lockByKey(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public SpsMMiscDomain lockByKeyNoWait(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public void create(SpsMMiscDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMMisc"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMMiscDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMMisc"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMMiscDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMMisc"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMMiscDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMMisc"
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMMiscDomain domain, SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMMisc"
     * @param criteria List of CriteriaDomain of "spsMMisc"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMMiscDomain> domains, List<SpsMMiscCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMMisc"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMMiscCriteriaDomain criteria) throws ApplicationException;

}
