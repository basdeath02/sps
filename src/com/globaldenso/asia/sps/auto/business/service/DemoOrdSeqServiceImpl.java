/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.DemoOrdSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "DemoOrdSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class DemoOrdSeqServiceImpl implements DemoOrdSeqService {
 
    /**
     * A "Dao" of "demoOrdSeq"
     */
    private DemoOrdSeqDao demoOrdSeqDao;
 
    /**
     * Default constructor
     */
    public DemoOrdSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "demoOrdSeq"
     * 
     * @param demoOrdSeqDao demoOrdSeqDao
     */
    public void setDemoOrdSeqDao(DemoOrdSeqDao demoOrdSeqDao) {
        this.demoOrdSeqDao = demoOrdSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.DemoOrdSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return demoOrdSeqDao.getNextValue();
    }
 
}
