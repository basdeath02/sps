/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;


/**
 * A "Service" interface of "SpsMRoleType"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMRoleTypeService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public SpsMRoleTypeDomain searchByKey(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return List of Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public List<SpsMRoleTypeDomain> searchByCondition(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return List of Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public List<SpsMRoleTypeDomain> searchByConditionForPaging(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMRoleType
     * @return Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMRoleTypeDomain searchByKeyForChange(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public SpsMRoleTypeDomain lockByKey(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public SpsMRoleTypeDomain lockByKeyNoWait(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public void create(SpsMRoleTypeDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMRoleType"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMRoleTypeDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMRoleType"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMRoleTypeDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMRoleType"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMRoleTypeDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMRoleType"
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMRoleTypeDomain domain, SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMRoleType"
     * @param criteria List of CriteriaDomain of "spsMRoleType"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMRoleTypeDomain> domains, List<SpsMRoleTypeCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMRoleType"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException;

}
