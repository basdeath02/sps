/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;


/**
 * A "Service" interface of "SpsMRole"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMRoleService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    public SpsMRoleDomain searchByKey(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return List of Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    public List<SpsMRoleDomain> searchByCondition(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return List of Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    public List<SpsMRoleDomain> searchByConditionForPaging(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMRole
     * @return Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMRoleDomain searchByKeyForChange(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    public SpsMRoleDomain lockByKey(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    public SpsMRoleDomain lockByKeyNoWait(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMRole"
     * @throws ApplicationException Exception
     */
    public void create(SpsMRoleDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMRole"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMRoleDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMRole"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMRoleDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMRole"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMRoleDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMRole"
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMRoleDomain domain, SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMRole"
     * @param criteria List of CriteriaDomain of "spsMRole"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMRoleDomain> domains, List<SpsMRoleCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMRole"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMRoleCriteriaDomain criteria) throws ApplicationException;

}
