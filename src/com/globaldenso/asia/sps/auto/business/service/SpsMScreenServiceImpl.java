/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain;


/**
 * A "Service" implementation class of "SpsMScreen"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMScreenServiceImpl implements SpsMScreenService {

    /**
     * A "Dao" of "spsMScreen"
     */
    private SpsMScreenDao spsMScreenDao;

    /**
     * Default constructor
     */
    public SpsMScreenServiceImpl() {
    }

    /**
     * Set Dao of "spsMScreen"
     * 
     * @param spsMScreenDao spsMScreenDao
     */
    public void setSpsMScreenDao(SpsMScreenDao spsMScreenDao) {
        this.spsMScreenDao = spsMScreenDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public SpsMScreenDomain searchByKey(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public List<SpsMScreenDomain> searchByCondition(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public List<SpsMScreenDomain> searchByConditionForPaging(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int searchCount(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    @Deprecated
    public SpsMScreenDomain searchByKeyForChange(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public SpsMScreenDomain lockByKey(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public SpsMScreenDomain lockByKeyNoWait(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain)
     */
    public void create(SpsMScreenDomain domain) throws ApplicationException {
        spsMScreenDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#create(java.util.List)
     */
    public void create(List<SpsMScreenDomain> domains) throws ApplicationException {
        for(SpsMScreenDomain domain : domains) {
            spsMScreenDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain)
     */
    public int update(SpsMScreenDomain domain) throws ApplicationException {
        return spsMScreenDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#update(java.util.List)
     */
    public int update(List<SpsMScreenDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMScreenDomain domain : domains) {
            updateCount += spsMScreenDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int updateByCondition(SpsMScreenDomain domain, SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMScreenDomain> domains, List<SpsMScreenCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMScreenDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int delete(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMScreenService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int deleteByCondition(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMScreenDao.deleteByCondition(criteria);
    }

}
