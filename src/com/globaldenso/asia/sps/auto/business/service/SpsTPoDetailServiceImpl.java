/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/31       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;


/**
 * A "Service" implementation class of "SpsTPoDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/31 15:36:38<br />
 * 
 * This module generated automatically in 2015/03/31 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDetailServiceImpl implements SpsTPoDetailService {

    /**
     * A "Dao" of "spsTPoDetail"
     */
    private SpsTPoDetailDao spsTPoDetailDao;

    /**
     * Default constructor
     */
    public SpsTPoDetailServiceImpl() {
    }

    /**
     * Set Dao of "spsTPoDetail"
     * 
     * @param spsTPoDetailDao spsTPoDetailDao
     */
    public void setSpsTPoDetailDao(SpsTPoDetailDao spsTPoDetailDao) {
        this.spsTPoDetailDao = spsTPoDetailDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public SpsTPoDetailDomain searchByKey(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public List<SpsTPoDetailDomain> searchByCondition(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public List<SpsTPoDetailDomain> searchByConditionForPaging(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int searchCount(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTPoDetailDomain searchByKeyForChange(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public SpsTPoDetailDomain lockByKey(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public SpsTPoDetailDomain lockByKeyNoWait(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain)
     */
    public void create(SpsTPoDetailDomain domain) throws ApplicationException {
        spsTPoDetailDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#create(java.util.List)
     */
    public void create(List<SpsTPoDetailDomain> domains) throws ApplicationException {
        for(SpsTPoDetailDomain domain : domains) {
            spsTPoDetailDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain)
     */
    public int update(SpsTPoDetailDomain domain) throws ApplicationException {
        return spsTPoDetailDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#update(java.util.List)
     */
    public int update(List<SpsTPoDetailDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTPoDetailDomain domain : domains) {
            updateCount += spsTPoDetailDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTPoDetailDomain domain, SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTPoDetailDomain> domains, List<SpsTPoDetailCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTPoDetailDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int delete(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDetailDao.deleteByCondition(criteria);
    }

}
