/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/11/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain;


/**
 * A "Service" implementation class of "SpsTPoCoverPage"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/11/28 09:54:32<br />
 * 
 * This module generated automatically in 2014/11/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageServiceImpl implements SpsTPoCoverPageService {

    /**
     * A "Dao" of "spsTPoCoverPage"
     */
    private SpsTPoCoverPageDao spsTPoCoverPageDao;

    /**
     * Default constructor
     */
    public SpsTPoCoverPageServiceImpl() {
    }

    /**
     * Set Dao of "spsTPoCoverPage"
     * 
     * @param spsTPoCoverPageDao spsTPoCoverPageDao
     */
    public void setSpsTPoCoverPageDao(SpsTPoCoverPageDao spsTPoCoverPageDao) {
        this.spsTPoCoverPageDao = spsTPoCoverPageDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public SpsTPoCoverPageDomain searchByKey(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public List<SpsTPoCoverPageDomain> searchByCondition(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public List<SpsTPoCoverPageDomain> searchByConditionForPaging(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int searchCount(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    @Deprecated
    public SpsTPoCoverPageDomain searchByKeyForChange(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public SpsTPoCoverPageDomain lockByKey(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public SpsTPoCoverPageDomain lockByKeyNoWait(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain)
     */
    public void create(SpsTPoCoverPageDomain domain) throws ApplicationException {
        spsTPoCoverPageDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#create(java.util.List)
     */
    public void create(List<SpsTPoCoverPageDomain> domains) throws ApplicationException {
        for(SpsTPoCoverPageDomain domain : domains) {
            spsTPoCoverPageDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain)
     */
    public int update(SpsTPoCoverPageDomain domain) throws ApplicationException {
        return spsTPoCoverPageDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#update(java.util.List)
     */
    public int update(List<SpsTPoCoverPageDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTPoCoverPageDomain domain : domains) {
            updateCount += spsTPoCoverPageDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int updateByCondition(SpsTPoCoverPageDomain domain, SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTPoCoverPageDomain> domains, List<SpsTPoCoverPageCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTPoCoverPageDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int delete(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDao.deleteByCondition(criteria);
    }

}
