/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain;


/**
 * A "Service" implementation class of "SpsMDensoSupplierRelation"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierRelationServiceImpl implements SpsMDensoSupplierRelationService {

    /**
     * A "Dao" of "spsMDensoSupplierRelation"
     */
    private SpsMDensoSupplierRelationDao spsMDensoSupplierRelationDao;

    /**
     * Default constructor
     */
    public SpsMDensoSupplierRelationServiceImpl() {
    }

    /**
     * Set Dao of "spsMDensoSupplierRelation"
     * 
     * @param spsMDensoSupplierRelationDao spsMDensoSupplierRelationDao
     */
    public void setSpsMDensoSupplierRelationDao(SpsMDensoSupplierRelationDao spsMDensoSupplierRelationDao) {
        this.spsMDensoSupplierRelationDao = spsMDensoSupplierRelationDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public SpsMDensoSupplierRelationDomain searchByKey(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public List<SpsMDensoSupplierRelationDomain> searchByCondition(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public List<SpsMDensoSupplierRelationDomain> searchByConditionForPaging(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int searchCount(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    @Deprecated
    public SpsMDensoSupplierRelationDomain searchByKeyForChange(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public SpsMDensoSupplierRelationDomain lockByKey(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public SpsMDensoSupplierRelationDomain lockByKeyNoWait(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain)
     */
    public void create(SpsMDensoSupplierRelationDomain domain) throws ApplicationException {
        spsMDensoSupplierRelationDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#create(java.util.List)
     */
    public void create(List<SpsMDensoSupplierRelationDomain> domains) throws ApplicationException {
        for(SpsMDensoSupplierRelationDomain domain : domains) {
            spsMDensoSupplierRelationDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain)
     */
    public int update(SpsMDensoSupplierRelationDomain domain) throws ApplicationException {
        return spsMDensoSupplierRelationDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#update(java.util.List)
     */
    public int update(List<SpsMDensoSupplierRelationDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMDensoSupplierRelationDomain domain : domains) {
            updateCount += spsMDensoSupplierRelationDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int updateByCondition(SpsMDensoSupplierRelationDomain domain, SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMDensoSupplierRelationDomain> domains, List<SpsMDensoSupplierRelationCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMDensoSupplierRelationDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int delete(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierRelationService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int deleteByCondition(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierRelationDao.deleteByCondition(criteria);
    }

}
