/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;


/**
 * A "Service" implementation class of "SpsTAsn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnServiceImpl implements SpsTAsnService {

    /**
     * A "Dao" of "spsTAsn"
     */
    private SpsTAsnDao spsTAsnDao;

    /**
     * Default constructor
     */
    public SpsTAsnServiceImpl() {
    }

    /**
     * Set Dao of "spsTAsn"
     * 
     * @param spsTAsnDao spsTAsnDao
     */
    public void setSpsTAsnDao(SpsTAsnDao spsTAsnDao) {
        this.spsTAsnDao = spsTAsnDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public SpsTAsnDomain searchByKey(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public List<SpsTAsnDomain> searchByCondition(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public List<SpsTAsnDomain> searchByConditionForPaging(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int searchCount(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    @Deprecated
    public SpsTAsnDomain searchByKeyForChange(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public SpsTAsnDomain lockByKey(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public SpsTAsnDomain lockByKeyNoWait(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain)
     */
    public void create(SpsTAsnDomain domain) throws ApplicationException {
        spsTAsnDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#create(java.util.List)
     */
    public void create(List<SpsTAsnDomain> domains) throws ApplicationException {
        for(SpsTAsnDomain domain : domains) {
            spsTAsnDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain)
     */
    public int update(SpsTAsnDomain domain) throws ApplicationException {
        return spsTAsnDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#update(java.util.List)
     */
    public int update(List<SpsTAsnDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTAsnDomain domain : domains) {
            updateCount += spsTAsnDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int updateByCondition(SpsTAsnDomain domain, SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTAsnDomain> domains, List<SpsTAsnCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTAsnDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int delete(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int deleteByCondition(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDao.deleteByCondition(criteria);
    }

}
