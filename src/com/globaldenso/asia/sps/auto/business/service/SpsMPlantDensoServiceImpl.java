/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;


/**
 * A "Service" implementation class of "SpsMPlantDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMPlantDensoServiceImpl implements SpsMPlantDensoService {

    /**
     * A "Dao" of "spsMPlantDenso"
     */
    private SpsMPlantDensoDao spsMPlantDensoDao;

    /**
     * Default constructor
     */
    public SpsMPlantDensoServiceImpl() {
    }

    /**
     * Set Dao of "spsMPlantDenso"
     * 
     * @param spsMPlantDensoDao spsMPlantDensoDao
     */
    public void setSpsMPlantDensoDao(SpsMPlantDensoDao spsMPlantDensoDao) {
        this.spsMPlantDensoDao = spsMPlantDensoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public SpsMPlantDensoDomain searchByKey(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public List<SpsMPlantDensoDomain> searchByCondition(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public List<SpsMPlantDensoDomain> searchByConditionForPaging(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int searchCount(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    @Deprecated
    public SpsMPlantDensoDomain searchByKeyForChange(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public SpsMPlantDensoDomain lockByKey(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public SpsMPlantDensoDomain lockByKeyNoWait(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain)
     */
    public void create(SpsMPlantDensoDomain domain) throws ApplicationException {
        spsMPlantDensoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#create(java.util.List)
     */
    public void create(List<SpsMPlantDensoDomain> domains) throws ApplicationException {
        for(SpsMPlantDensoDomain domain : domains) {
            spsMPlantDensoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain)
     */
    public int update(SpsMPlantDensoDomain domain) throws ApplicationException {
        return spsMPlantDensoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#update(java.util.List)
     */
    public int update(List<SpsMPlantDensoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMPlantDensoDomain domain : domains) {
            updateCount += spsMPlantDensoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int updateByCondition(SpsMPlantDensoDomain domain, SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMPlantDensoDomain> domains, List<SpsMPlantDensoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMPlantDensoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int delete(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantDensoDao.deleteByCondition(criteria);
    }

}
