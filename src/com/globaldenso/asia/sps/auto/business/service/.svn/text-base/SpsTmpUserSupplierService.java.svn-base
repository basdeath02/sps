/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain;


/**
 * A "Service" interface of "SpsTmpUserSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/30 12:07:21<br />
 * 
 * This module generated automatically in 2015/03/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTmpUserSupplierService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public SpsTmpUserSupplierDomain searchByKey(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return List of Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUserSupplierDomain> searchByCondition(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return List of Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUserSupplierDomain> searchByConditionForPaging(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTmpUserSupplier
     * @return Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTmpUserSupplierDomain searchByKeyForChange(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public SpsTmpUserSupplierDomain lockByKey(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public SpsTmpUserSupplierDomain lockByKeyNoWait(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public void create(SpsTmpUserSupplierDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTmpUserSupplier"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTmpUserSupplierDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTmpUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTmpUserSupplierDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTmpUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTmpUserSupplierDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTmpUserSupplier"
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTmpUserSupplierDomain domain, SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTmpUserSupplier"
     * @param criteria List of CriteriaDomain of "spsTmpUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTmpUserSupplierDomain> domains, List<SpsTmpUserSupplierCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserSupplier"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException;

}
