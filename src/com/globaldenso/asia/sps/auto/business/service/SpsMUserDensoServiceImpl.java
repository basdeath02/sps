/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;


/**
 * A "Service" implementation class of "SpsMUserDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 11:25:05<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserDensoServiceImpl implements SpsMUserDensoService {

    /**
     * A "Dao" of "spsMUserDenso"
     */
    private SpsMUserDensoDao spsMUserDensoDao;

    /**
     * Default constructor
     */
    public SpsMUserDensoServiceImpl() {
    }

    /**
     * Set Dao of "spsMUserDenso"
     * 
     * @param spsMUserDensoDao spsMUserDensoDao
     */
    public void setSpsMUserDensoDao(SpsMUserDensoDao spsMUserDensoDao) {
        this.spsMUserDensoDao = spsMUserDensoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public SpsMUserDensoDomain searchByKey(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public List<SpsMUserDensoDomain> searchByCondition(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public List<SpsMUserDensoDomain> searchByConditionForPaging(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int searchCount(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    @Deprecated
    public SpsMUserDensoDomain searchByKeyForChange(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public SpsMUserDensoDomain lockByKey(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public SpsMUserDensoDomain lockByKeyNoWait(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain)
     */
    public void create(SpsMUserDensoDomain domain) throws ApplicationException {
        spsMUserDensoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#create(java.util.List)
     */
    public void create(List<SpsMUserDensoDomain> domains) throws ApplicationException {
        for(SpsMUserDensoDomain domain : domains) {
            spsMUserDensoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain)
     */
    public int update(SpsMUserDensoDomain domain) throws ApplicationException {
        return spsMUserDensoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#update(java.util.List)
     */
    public int update(List<SpsMUserDensoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMUserDensoDomain domain : domains) {
            updateCount += spsMUserDensoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int updateByCondition(SpsMUserDensoDomain domain, SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMUserDensoDomain> domains, List<SpsMUserDensoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMUserDensoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int delete(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserDensoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDensoDao.deleteByCondition(criteria);
    }

}
