/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain;


/**
 * A "Service" implementation class of "SpsMUploadErrorMsg"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUploadErrorMsgServiceImpl implements SpsMUploadErrorMsgService {

    /**
     * A "Dao" of "spsMUploadErrorMsg"
     */
    private SpsMUploadErrorMsgDao spsMUploadErrorMsgDao;

    /**
     * Default constructor
     */
    public SpsMUploadErrorMsgServiceImpl() {
    }

    /**
     * Set Dao of "spsMUploadErrorMsg"
     * 
     * @param spsMUploadErrorMsgDao spsMUploadErrorMsgDao
     */
    public void setSpsMUploadErrorMsgDao(SpsMUploadErrorMsgDao spsMUploadErrorMsgDao) {
        this.spsMUploadErrorMsgDao = spsMUploadErrorMsgDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public List<SpsMUploadErrorMsgDomain> searchByCondition(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return spsMUploadErrorMsgDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public List<SpsMUploadErrorMsgDomain> searchByConditionForPaging(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return spsMUploadErrorMsgDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public int searchCount(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return spsMUploadErrorMsgDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain)
     */
    public void create(SpsMUploadErrorMsgDomain domain) throws ApplicationException {
        spsMUploadErrorMsgDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#create(java.util.List)
     */
    public void create(List<SpsMUploadErrorMsgDomain> domains) throws ApplicationException {
        for(SpsMUploadErrorMsgDomain domain : domains) {
            spsMUploadErrorMsgDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public int updateByCondition(SpsMUploadErrorMsgDomain domain, SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return spsMUploadErrorMsgDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMUploadErrorMsgDomain> domains, List<SpsMUploadErrorMsgCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMUploadErrorMsgDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUploadErrorMsgService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public int deleteByCondition(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return spsMUploadErrorMsgDao.deleteByCondition(criteria);
    }

}
