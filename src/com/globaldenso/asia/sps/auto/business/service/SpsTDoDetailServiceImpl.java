/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;


/**
 * A "Service" implementation class of "SpsTDoDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoDetailServiceImpl implements SpsTDoDetailService {

    /**
     * A "Dao" of "spsTDoDetail"
     */
    private SpsTDoDetailDao spsTDoDetailDao;

    /**
     * Default constructor
     */
    public SpsTDoDetailServiceImpl() {
    }

    /**
     * Set Dao of "spsTDoDetail"
     * 
     * @param spsTDoDetailDao spsTDoDetailDao
     */
    public void setSpsTDoDetailDao(SpsTDoDetailDao spsTDoDetailDao) {
        this.spsTDoDetailDao = spsTDoDetailDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public SpsTDoDetailDomain searchByKey(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public List<SpsTDoDetailDomain> searchByCondition(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public List<SpsTDoDetailDomain> searchByConditionForPaging(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int searchCount(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTDoDetailDomain searchByKeyForChange(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public SpsTDoDetailDomain lockByKey(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public SpsTDoDetailDomain lockByKeyNoWait(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain)
     */
    public void create(SpsTDoDetailDomain domain) throws ApplicationException {
        spsTDoDetailDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#create(java.util.List)
     */
    public void create(List<SpsTDoDetailDomain> domains) throws ApplicationException {
        for(SpsTDoDetailDomain domain : domains) {
            spsTDoDetailDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain)
     */
    public int update(SpsTDoDetailDomain domain) throws ApplicationException {
        return spsTDoDetailDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#update(java.util.List)
     */
    public int update(List<SpsTDoDetailDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTDoDetailDomain domain : domains) {
            updateCount += spsTDoDetailDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTDoDetailDomain domain, SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTDoDetailDomain> domains, List<SpsTDoDetailCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTDoDetailDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int delete(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDetailDao.deleteByCondition(criteria);
    }

}
