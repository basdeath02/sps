/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/13       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;


/**
 * A "Service" implementation class of "SpsTFileUpload"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/13 11:02:03<br />
 * 
 * This module generated automatically in 2014/10/13 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTFileUploadServiceImpl implements SpsTFileUploadService {

    /**
     * A "Dao" of "spsTFileUpload"
     */
    private SpsTFileUploadDao spsTFileUploadDao;

    /**
     * Default constructor
     */
    public SpsTFileUploadServiceImpl() {
    }

    /**
     * Set Dao of "spsTFileUpload"
     * 
     * @param spsTFileUploadDao spsTFileUploadDao
     */
    public void setSpsTFileUploadDao(SpsTFileUploadDao spsTFileUploadDao) {
        this.spsTFileUploadDao = spsTFileUploadDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public SpsTFileUploadDomain searchByKey(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public List<SpsTFileUploadDomain> searchByCondition(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public List<SpsTFileUploadDomain> searchByConditionForPaging(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int searchCount(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    @Deprecated
    public SpsTFileUploadDomain searchByKeyForChange(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public SpsTFileUploadDomain lockByKey(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public SpsTFileUploadDomain lockByKeyNoWait(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain)
     */
    public void create(SpsTFileUploadDomain domain) throws ApplicationException {
        spsTFileUploadDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#create(java.util.List)
     */
    public void create(List<SpsTFileUploadDomain> domains) throws ApplicationException {
        for(SpsTFileUploadDomain domain : domains) {
            spsTFileUploadDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain)
     */
    public int update(SpsTFileUploadDomain domain) throws ApplicationException {
        return spsTFileUploadDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#update(java.util.List)
     */
    public int update(List<SpsTFileUploadDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTFileUploadDomain domain : domains) {
            updateCount += spsTFileUploadDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int updateByCondition(SpsTFileUploadDomain domain, SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTFileUploadDomain> domains, List<SpsTFileUploadCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTFileUploadDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int delete(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTFileUploadService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int deleteByCondition(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return spsTFileUploadDao.deleteByCondition(criteria);
    }

}
