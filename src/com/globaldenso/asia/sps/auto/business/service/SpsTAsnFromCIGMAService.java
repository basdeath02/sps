/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;


/**
 * A "Service" interface of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTAsnFromCIGMAService {



    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return List of Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchByCondition(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException;
    
    public List<SPSTDoHeaderDNDomain> searchDONotCcl(SPSTDoHeaderDNDomain criteria) throws ApplicationException;
    public List<SPSTDoHeaderDNDomain> searchDOPrevCcl(SPSTDoHeaderDNDomain criteria) throws ApplicationException;
    public List<SPSTDoHeaderDNDomain> searchDO(SPSTDoHeaderDNDomain criteria) throws ApplicationException;
    public List<SPSTDoDetailDNDomain> searchDODetail(SPSTDoDetailDNDomain criteria) throws ApplicationException;
    
    /**
     * Update data and send email return void
     * 
     * @param criteria  List of Domain class of "SpsMDensoSupplierRelationWithPODNDomain"
     * @return Void
     * @throws ApplicationException Exception
     */
    public boolean InsertUpdateASN(SPSTAsnHeaderDNDomain criteria) throws ApplicationException;
    public boolean InsertASNDetail(SPSTAsnDetailDNDomain criteria) throws ApplicationException;
    public boolean UpdateDO(SPSTDoHeaderDNDomain criteria) throws ApplicationException;
    public boolean UpdateDODet(SPSTDoDetailDNDomain criteria) throws ApplicationException;
    public boolean UpdateTranFlag(SPSTAsnHeaderDNDomain criteria) throws ApplicationException;
    public boolean DeleteASN(SPSTAsnHeaderDNDomain criteria) throws ApplicationException;


    public List<DeliveryOrderDetailDomain> searchExistDo(
            DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
            DeliveryOrderInformationDomain deliveryOrderInformationDomain);
    
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
            AcknowledgedDoInformationDomain acknowledgedDOInformationDomain);
    
    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;
    
    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCountPlant(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;
    
    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int SearchCountPrevASN(SPSTAsnHeaderDNDomain  criteria) throws ApplicationException;
    
    
    
    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return List of Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public List<SpsMAs400VendorDomain> searchByConditionAS400(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;
    
    
    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantSupplier"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCountPlantSupplier(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException;
    
    public boolean UpdateDOStatus(SPSTAsnHeaderDNDomain criteria) throws ApplicationException;
    
    
    public List<SpsMUserDomain> searchUserDenso(
    		SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException;
    
    public List<SpsMUserDomain> searchUserSupplier(
        SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException;
    
    public List<SPSTDoDetailDNDomain> searchDODet(SPSTDoDetailDNDomain criteria) throws ApplicationException;
    public List<SPSTDoHeaderDNDomain> searchDODelivery(SPSTDoHeaderDNDomain criteria) throws ApplicationException;
    
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchByConditionGroup(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException;
    
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchSupplierBySchema(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException;
    
    
}
