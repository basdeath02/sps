/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;


/**
 * A "Service" implementation class of "SpsMMenu"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMMenuServiceImpl implements SpsMMenuService {

    /**
     * A "Dao" of "spsMMenu"
     */
    private SpsMMenuDao spsMMenuDao;

    /**
     * Default constructor
     */
    public SpsMMenuServiceImpl() {
    }

    /**
     * Set Dao of "spsMMenu"
     * 
     * @param spsMMenuDao spsMMenuDao
     */
    public void setSpsMMenuDao(SpsMMenuDao spsMMenuDao) {
        this.spsMMenuDao = spsMMenuDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public SpsMMenuDomain searchByKey(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public List<SpsMMenuDomain> searchByCondition(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public List<SpsMMenuDomain> searchByConditionForPaging(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int searchCount(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    @Deprecated
    public SpsMMenuDomain searchByKeyForChange(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public SpsMMenuDomain lockByKey(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public SpsMMenuDomain lockByKeyNoWait(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain)
     */
    public void create(SpsMMenuDomain domain) throws ApplicationException {
        spsMMenuDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#create(java.util.List)
     */
    public void create(List<SpsMMenuDomain> domains) throws ApplicationException {
        for(SpsMMenuDomain domain : domains) {
            spsMMenuDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain)
     */
    public int update(SpsMMenuDomain domain) throws ApplicationException {
        return spsMMenuDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#update(java.util.List)
     */
    public int update(List<SpsMMenuDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMMenuDomain domain : domains) {
            updateCount += spsMMenuDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int updateByCondition(SpsMMenuDomain domain, SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMMenuDomain> domains, List<SpsMMenuCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMMenuDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int delete(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMenuService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int deleteByCondition(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMMenuDao.deleteByCondition(criteria);
    }

}
