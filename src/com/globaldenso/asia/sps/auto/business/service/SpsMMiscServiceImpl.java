/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;


/**
 * A "Service" implementation class of "SpsMMisc"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMMiscServiceImpl implements SpsMMiscService {

    /**
     * A "Dao" of "spsMMisc"
     */
    private SpsMMiscDao spsMMiscDao;

    /**
     * Default constructor
     */
    public SpsMMiscServiceImpl() {
    }

    /**
     * Set Dao of "spsMMisc"
     * 
     * @param spsMMiscDao spsMMiscDao
     */
    public void setSpsMMiscDao(SpsMMiscDao spsMMiscDao) {
        this.spsMMiscDao = spsMMiscDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public SpsMMiscDomain searchByKey(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public List<SpsMMiscDomain> searchByCondition(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public List<SpsMMiscDomain> searchByConditionForPaging(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int searchCount(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    @Deprecated
    public SpsMMiscDomain searchByKeyForChange(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public SpsMMiscDomain lockByKey(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public SpsMMiscDomain lockByKeyNoWait(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain)
     */
    public void create(SpsMMiscDomain domain) throws ApplicationException {
        spsMMiscDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#create(java.util.List)
     */
    public void create(List<SpsMMiscDomain> domains) throws ApplicationException {
        for(SpsMMiscDomain domain : domains) {
            spsMMiscDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain)
     */
    public int update(SpsMMiscDomain domain) throws ApplicationException {
        return spsMMiscDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#update(java.util.List)
     */
    public int update(List<SpsMMiscDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMMiscDomain domain : domains) {
            updateCount += spsMMiscDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int updateByCondition(SpsMMiscDomain domain, SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMMiscDomain> domains, List<SpsMMiscCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMMiscDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int delete(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMMiscService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int deleteByCondition(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return spsMMiscDao.deleteByCondition(criteria);
    }

}
