/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/07/09       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;


/**
 * A "Service" interface of "SpsTInvoice"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/07/09 18:24:35<br />
 * 
 * This module generated automatically in 2015/07/09 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTInvoiceService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public SpsTInvoiceDomain searchByKey(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return List of Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public List<SpsTInvoiceDomain> searchByCondition(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return List of Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public List<SpsTInvoiceDomain> searchByConditionForPaging(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTInvoice
     * @return Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTInvoiceDomain searchByKeyForChange(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public SpsTInvoiceDomain lockByKey(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public SpsTInvoiceDomain lockByKeyNoWait(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public void create(SpsTInvoiceDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTInvoice"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTInvoiceDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTInvoice"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTInvoiceDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTInvoice"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTInvoiceDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTInvoice"
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTInvoiceDomain domain, SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTInvoice"
     * @param criteria List of CriteriaDomain of "spsTInvoice"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTInvoiceDomain> domains, List<SpsTInvoiceCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoice"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException;

}
