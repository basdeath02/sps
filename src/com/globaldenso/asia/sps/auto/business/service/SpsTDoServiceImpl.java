/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/12       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;


/**
 * A "Service" implementation class of "SpsTDo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/12 14:21:17<br />
 * 
 * This module generated automatically in 2015/03/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoServiceImpl implements SpsTDoService {

    /**
     * A "Dao" of "spsTDo"
     */
    private SpsTDoDao spsTDoDao;

    /**
     * Default constructor
     */
    public SpsTDoServiceImpl() {
    }

    /**
     * Set Dao of "spsTDo"
     * 
     * @param spsTDoDao spsTDoDao
     */
    public void setSpsTDoDao(SpsTDoDao spsTDoDao) {
        this.spsTDoDao = spsTDoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public SpsTDoDomain searchByKey(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public List<SpsTDoDomain> searchByCondition(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public List<SpsTDoDomain> searchByConditionForPaging(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int searchCount(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    @Deprecated
    public SpsTDoDomain searchByKeyForChange(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public SpsTDoDomain lockByKey(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public SpsTDoDomain lockByKeyNoWait(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    public void create(SpsTDoDomain domain) throws ApplicationException {
        spsTDoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#create(java.util.List)
     */
    public void create(List<SpsTDoDomain> domains) throws ApplicationException {
        for(SpsTDoDomain domain : domains) {
            spsTDoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    public int update(SpsTDoDomain domain) throws ApplicationException {
        return spsTDoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#update(java.util.List)
     */
    public int update(List<SpsTDoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTDoDomain domain : domains) {
            updateCount += spsTDoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int updateByCondition(SpsTDoDomain domain, SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTDoDomain> domains, List<SpsTDoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTDoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int delete(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int deleteByCondition(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return spsTDoDao.deleteByCondition(criteria);
    }

}
