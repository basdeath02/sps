/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain;


/**
 * A "Service" implementation class of "SpsMCurrency"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCurrencyServiceImpl implements SpsMCurrencyService {

    /**
     * A "Dao" of "spsMCurrency"
     */
    private SpsMCurrencyDao spsMCurrencyDao;

    /**
     * Default constructor
     */
    public SpsMCurrencyServiceImpl() {
    }

    /**
     * Set Dao of "spsMCurrency"
     * 
     * @param spsMCurrencyDao spsMCurrencyDao
     */
    public void setSpsMCurrencyDao(SpsMCurrencyDao spsMCurrencyDao) {
        this.spsMCurrencyDao = spsMCurrencyDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public SpsMCurrencyDomain searchByKey(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public List<SpsMCurrencyDomain> searchByCondition(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public List<SpsMCurrencyDomain> searchByConditionForPaging(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int searchCount(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    @Deprecated
    public SpsMCurrencyDomain searchByKeyForChange(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public SpsMCurrencyDomain lockByKey(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public SpsMCurrencyDomain lockByKeyNoWait(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain)
     */
    public void create(SpsMCurrencyDomain domain) throws ApplicationException {
        spsMCurrencyDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#create(java.util.List)
     */
    public void create(List<SpsMCurrencyDomain> domains) throws ApplicationException {
        for(SpsMCurrencyDomain domain : domains) {
            spsMCurrencyDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain)
     */
    public int update(SpsMCurrencyDomain domain) throws ApplicationException {
        return spsMCurrencyDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#update(java.util.List)
     */
    public int update(List<SpsMCurrencyDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMCurrencyDomain domain : domains) {
            updateCount += spsMCurrencyDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int updateByCondition(SpsMCurrencyDomain domain, SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMCurrencyDomain> domains, List<SpsMCurrencyCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMCurrencyDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int delete(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCurrencyService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int deleteByCondition(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return spsMCurrencyDao.deleteByCondition(criteria);
    }

}
