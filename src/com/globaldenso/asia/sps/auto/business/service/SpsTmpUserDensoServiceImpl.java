/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain;


/**
 * A "Service" implementation class of "SpsTmpUserDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 12:59:09<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserDensoServiceImpl implements SpsTmpUserDensoService {

    /**
     * A "Dao" of "spsTmpUserDenso"
     */
    private SpsTmpUserDensoDao spsTmpUserDensoDao;

    /**
     * Default constructor
     */
    public SpsTmpUserDensoServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpUserDenso"
     * 
     * @param spsTmpUserDensoDao spsTmpUserDensoDao
     */
    public void setSpsTmpUserDensoDao(SpsTmpUserDensoDao spsTmpUserDensoDao) {
        this.spsTmpUserDensoDao = spsTmpUserDensoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public SpsTmpUserDensoDomain searchByKey(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public List<SpsTmpUserDensoDomain> searchByCondition(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public List<SpsTmpUserDensoDomain> searchByConditionForPaging(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int searchCount(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUserDensoDomain searchByKeyForChange(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public SpsTmpUserDensoDomain lockByKey(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public SpsTmpUserDensoDomain lockByKeyNoWait(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain)
     */
    public void create(SpsTmpUserDensoDomain domain) throws ApplicationException {
        spsTmpUserDensoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#create(java.util.List)
     */
    public void create(List<SpsTmpUserDensoDomain> domains) throws ApplicationException {
        for(SpsTmpUserDensoDomain domain : domains) {
            spsTmpUserDensoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain)
     */
    public int update(SpsTmpUserDensoDomain domain) throws ApplicationException {
        return spsTmpUserDensoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#update(java.util.List)
     */
    public int update(List<SpsTmpUserDensoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpUserDensoDomain domain : domains) {
            updateCount += spsTmpUserDensoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUserDensoDomain domain, SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpUserDensoDomain> domains, List<SpsTmpUserDensoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpUserDensoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int delete(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserDensoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserDensoDao.deleteByCondition(criteria);
    }

}
