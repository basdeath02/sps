/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.DemoUsersSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "DemoUsersSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class DemoUsersSeqServiceImpl implements DemoUsersSeqService {
 
    /**
     * A "Dao" of "demoUsersSeq"
     */
    private DemoUsersSeqDao demoUsersSeqDao;
 
    /**
     * Default constructor
     */
    public DemoUsersSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "demoUsersSeq"
     * 
     * @param demoUsersSeqDao demoUsersSeqDao
     */
    public void setDemoUsersSeqDao(DemoUsersSeqDao demoUsersSeqDao) {
        this.demoUsersSeqDao = demoUsersSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.DemoUsersSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return demoUsersSeqDao.getNextValue();
    }
 
}
