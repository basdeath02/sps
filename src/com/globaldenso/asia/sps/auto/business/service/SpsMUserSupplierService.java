/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/27       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;


/**
 * A "Service" interface of "SpsMUserSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/27 17:41:40<br />
 * 
 * This module generated automatically in 2015/03/27 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMUserSupplierService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public SpsMUserSupplierDomain searchByKey(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return List of Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public List<SpsMUserSupplierDomain> searchByCondition(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return List of Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public List<SpsMUserSupplierDomain> searchByConditionForPaging(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMUserSupplier
     * @return Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMUserSupplierDomain searchByKeyForChange(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public SpsMUserSupplierDomain lockByKey(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public SpsMUserSupplierDomain lockByKeyNoWait(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public void create(SpsMUserSupplierDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMUserSupplier"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMUserSupplierDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMUserSupplierDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMUserSupplierDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMUserSupplier"
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMUserSupplierDomain domain, SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMUserSupplier"
     * @param criteria List of CriteriaDomain of "spsMUserSupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMUserSupplierDomain> domains, List<SpsMUserSupplierCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUserSupplier"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException;

}
