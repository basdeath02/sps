/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;


/**
 * A "Service" interface of "SpsMAnnounceMessage"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMAnnounceMessageService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public SpsMAnnounceMessageDomain searchByKey(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return List of Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public List<SpsMAnnounceMessageDomain> searchByCondition(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return List of Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public List<SpsMAnnounceMessageDomain> searchByConditionForPaging(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMAnnounceMessage
     * @return Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMAnnounceMessageDomain searchByKeyForChange(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public SpsMAnnounceMessageDomain lockByKey(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public SpsMAnnounceMessageDomain lockByKeyNoWait(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public void create(SpsMAnnounceMessageDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMAnnounceMessage"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMAnnounceMessageDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMAnnounceMessage"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMAnnounceMessageDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMAnnounceMessage"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMAnnounceMessageDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMAnnounceMessage"
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMAnnounceMessageDomain domain, SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMAnnounceMessage"
     * @param criteria List of CriteriaDomain of "spsMAnnounceMessage"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMAnnounceMessageDomain> domains, List<SpsMAnnounceMessageCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMAnnounceMessage"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException;

}
