/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/04/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;


/**
 * A "Service" implementation class of "SpsTInvoiceDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/04/28 15:59:11<br />
 * 
 * This module generated automatically in 2015/04/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceDetailServiceImpl implements SpsTInvoiceDetailService {

    /**
     * A "Dao" of "spsTInvoiceDetail"
     */
    private SpsTInvoiceDetailDao spsTInvoiceDetailDao;

    /**
     * Default constructor
     */
    public SpsTInvoiceDetailServiceImpl() {
    }

    /**
     * Set Dao of "spsTInvoiceDetail"
     * 
     * @param spsTInvoiceDetailDao spsTInvoiceDetailDao
     */
    public void setSpsTInvoiceDetailDao(SpsTInvoiceDetailDao spsTInvoiceDetailDao) {
        this.spsTInvoiceDetailDao = spsTInvoiceDetailDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public SpsTInvoiceDetailDomain searchByKey(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public List<SpsTInvoiceDetailDomain> searchByCondition(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public List<SpsTInvoiceDetailDomain> searchByConditionForPaging(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int searchCount(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTInvoiceDetailDomain searchByKeyForChange(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public SpsTInvoiceDetailDomain lockByKey(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public SpsTInvoiceDetailDomain lockByKeyNoWait(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain)
     */
    public void create(SpsTInvoiceDetailDomain domain) throws ApplicationException {
        spsTInvoiceDetailDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#create(java.util.List)
     */
    public void create(List<SpsTInvoiceDetailDomain> domains) throws ApplicationException {
        for(SpsTInvoiceDetailDomain domain : domains) {
            spsTInvoiceDetailDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain)
     */
    public int update(SpsTInvoiceDetailDomain domain) throws ApplicationException {
        return spsTInvoiceDetailDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#update(java.util.List)
     */
    public int update(List<SpsTInvoiceDetailDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTInvoiceDetailDomain domain : domains) {
            updateCount += spsTInvoiceDetailDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTInvoiceDetailDomain domain, SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTInvoiceDetailDomain> domains, List<SpsTInvoiceDetailCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTInvoiceDetailDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int delete(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDetailDao.deleteByCondition(criteria);
    }

}
