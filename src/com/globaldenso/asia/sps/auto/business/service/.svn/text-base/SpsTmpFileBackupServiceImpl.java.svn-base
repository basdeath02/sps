/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain;


/**
 * A "Service" implementation class of "SpsTmpFileBackup"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpFileBackupServiceImpl implements SpsTmpFileBackupService {

    /**
     * A "Dao" of "spsTmpFileBackup"
     */
    private SpsTmpFileBackupDao spsTmpFileBackupDao;

    /**
     * Default constructor
     */
    public SpsTmpFileBackupServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpFileBackup"
     * 
     * @param spsTmpFileBackupDao spsTmpFileBackupDao
     */
    public void setSpsTmpFileBackupDao(SpsTmpFileBackupDao spsTmpFileBackupDao) {
        this.spsTmpFileBackupDao = spsTmpFileBackupDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public SpsTmpFileBackupDomain searchByKey(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public List<SpsTmpFileBackupDomain> searchByCondition(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public List<SpsTmpFileBackupDomain> searchByConditionForPaging(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int searchCount(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    @Deprecated
    public SpsTmpFileBackupDomain searchByKeyForChange(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public SpsTmpFileBackupDomain lockByKey(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public SpsTmpFileBackupDomain lockByKeyNoWait(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain)
     */
    public void create(SpsTmpFileBackupDomain domain) throws ApplicationException {
        spsTmpFileBackupDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#create(java.util.List)
     */
    public void create(List<SpsTmpFileBackupDomain> domains) throws ApplicationException {
        for(SpsTmpFileBackupDomain domain : domains) {
            spsTmpFileBackupDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain)
     */
    public int update(SpsTmpFileBackupDomain domain) throws ApplicationException {
        return spsTmpFileBackupDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#update(java.util.List)
     */
    public int update(List<SpsTmpFileBackupDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpFileBackupDomain domain : domains) {
            updateCount += spsTmpFileBackupDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int updateByCondition(SpsTmpFileBackupDomain domain, SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpFileBackupDomain> domains, List<SpsTmpFileBackupCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpFileBackupDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int delete(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpFileBackupService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return spsTmpFileBackupDao.deleteByCondition(criteria);
    }

}
