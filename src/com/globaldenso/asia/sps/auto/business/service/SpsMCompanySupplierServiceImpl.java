/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/29       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;


/**
 * A "Service" implementation class of "SpsMCompanySupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/29 18:51:48<br />
 * 
 * This module generated automatically in 2014/10/29 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanySupplierServiceImpl implements SpsMCompanySupplierService {

    /**
     * A "Dao" of "spsMCompanySupplier"
     */
    private SpsMCompanySupplierDao spsMCompanySupplierDao;

    /**
     * Default constructor
     */
    public SpsMCompanySupplierServiceImpl() {
    }

    /**
     * Set Dao of "spsMCompanySupplier"
     * 
     * @param spsMCompanySupplierDao spsMCompanySupplierDao
     */
    public void setSpsMCompanySupplierDao(SpsMCompanySupplierDao spsMCompanySupplierDao) {
        this.spsMCompanySupplierDao = spsMCompanySupplierDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public SpsMCompanySupplierDomain searchByKey(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public List<SpsMCompanySupplierDomain> searchByCondition(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public List<SpsMCompanySupplierDomain> searchByConditionForPaging(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int searchCount(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    @Deprecated
    public SpsMCompanySupplierDomain searchByKeyForChange(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public SpsMCompanySupplierDomain lockByKey(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public SpsMCompanySupplierDomain lockByKeyNoWait(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain)
     */
    public void create(SpsMCompanySupplierDomain domain) throws ApplicationException {
        spsMCompanySupplierDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#create(java.util.List)
     */
    public void create(List<SpsMCompanySupplierDomain> domains) throws ApplicationException {
        for(SpsMCompanySupplierDomain domain : domains) {
            spsMCompanySupplierDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain)
     */
    public int update(SpsMCompanySupplierDomain domain) throws ApplicationException {
        return spsMCompanySupplierDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#update(java.util.List)
     */
    public int update(List<SpsMCompanySupplierDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMCompanySupplierDomain domain : domains) {
            updateCount += spsMCompanySupplierDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int updateByCondition(SpsMCompanySupplierDomain domain, SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMCompanySupplierDomain> domains, List<SpsMCompanySupplierCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMCompanySupplierDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int delete(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanySupplierService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanySupplierDao.deleteByCondition(criteria);
    }

}
