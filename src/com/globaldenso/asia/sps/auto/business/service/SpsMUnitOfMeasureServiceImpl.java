/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/01/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain;


/**
 * A "Service" implementation class of "SpsMUnitOfMeasure"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/01/05 09:25:07<br />
 * 
 * This module generated automatically in 2016/01/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUnitOfMeasureServiceImpl implements SpsMUnitOfMeasureService {

    /**
     * A "Dao" of "spsMUnitOfMeasure"
     */
    private SpsMUnitOfMeasureDao spsMUnitOfMeasureDao;

    /**
     * Default constructor
     */
    public SpsMUnitOfMeasureServiceImpl() {
    }

    /**
     * Set Dao of "spsMUnitOfMeasure"
     * 
     * @param spsMUnitOfMeasureDao spsMUnitOfMeasureDao
     */
    public void setSpsMUnitOfMeasureDao(SpsMUnitOfMeasureDao spsMUnitOfMeasureDao) {
        this.spsMUnitOfMeasureDao = spsMUnitOfMeasureDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public SpsMUnitOfMeasureDomain searchByKey(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public List<SpsMUnitOfMeasureDomain> searchByCondition(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public List<SpsMUnitOfMeasureDomain> searchByConditionForPaging(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int searchCount(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    @Deprecated
    public SpsMUnitOfMeasureDomain searchByKeyForChange(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public SpsMUnitOfMeasureDomain lockByKey(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public SpsMUnitOfMeasureDomain lockByKeyNoWait(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain)
     */
    public void create(SpsMUnitOfMeasureDomain domain) throws ApplicationException {
        spsMUnitOfMeasureDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#create(java.util.List)
     */
    public void create(List<SpsMUnitOfMeasureDomain> domains) throws ApplicationException {
        for(SpsMUnitOfMeasureDomain domain : domains) {
            spsMUnitOfMeasureDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain)
     */
    public int update(SpsMUnitOfMeasureDomain domain) throws ApplicationException {
        return spsMUnitOfMeasureDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#update(java.util.List)
     */
    public int update(List<SpsMUnitOfMeasureDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMUnitOfMeasureDomain domain : domains) {
            updateCount += spsMUnitOfMeasureDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int updateByCondition(SpsMUnitOfMeasureDomain domain, SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMUnitOfMeasureDomain> domains, List<SpsMUnitOfMeasureCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMUnitOfMeasureDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int delete(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUnitOfMeasureService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int deleteByCondition(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return spsMUnitOfMeasureDao.deleteByCondition(criteria);
    }

}
