/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;


/**
 * A "Service" implementation class of "SpsCigmaDoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaDoErrorServiceImpl implements SpsCigmaDoErrorService {

    /**
     * A "Dao" of "spsCigmaDoError"
     */
    private SpsCigmaDoErrorDao spsCigmaDoErrorDao;

    /**
     * Default constructor
     */
    public SpsCigmaDoErrorServiceImpl() {
    }

    /**
     * Set Dao of "spsCigmaDoError"
     * 
     * @param spsCigmaDoErrorDao spsCigmaDoErrorDao
     */
    public void setSpsCigmaDoErrorDao(SpsCigmaDoErrorDao spsCigmaDoErrorDao) {
        this.spsCigmaDoErrorDao = spsCigmaDoErrorDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public SpsCigmaDoErrorDomain searchByKey(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public List<SpsCigmaDoErrorDomain> searchByCondition(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public List<SpsCigmaDoErrorDomain> searchByConditionForPaging(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaDoErrorDomain searchByKeyForChange(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public SpsCigmaDoErrorDomain lockByKey(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public SpsCigmaDoErrorDomain lockByKeyNoWait(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain)
     */
    public void create(SpsCigmaDoErrorDomain domain) throws ApplicationException {
        spsCigmaDoErrorDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#create(java.util.List)
     */
    public void create(List<SpsCigmaDoErrorDomain> domains) throws ApplicationException {
        for(SpsCigmaDoErrorDomain domain : domains) {
            spsCigmaDoErrorDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain)
     */
    public int update(SpsCigmaDoErrorDomain domain) throws ApplicationException {
        return spsCigmaDoErrorDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#update(java.util.List)
     */
    public int update(List<SpsCigmaDoErrorDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsCigmaDoErrorDomain domain : domains) {
            updateCount += spsCigmaDoErrorDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaDoErrorDomain domain, SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsCigmaDoErrorDomain> domains, List<SpsCigmaDoErrorCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsCigmaDoErrorDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaDoErrorService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaDoErrorDao.deleteByCondition(criteria);
    }

}
