/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;


/**
 * A "Service" interface of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTPoService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public SpsTPoDomain searchByKey(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return List of Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoDomain> searchByCondition(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return List of Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoDomain> searchByConditionForPaging(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTPo
     * @return Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTPoDomain searchByKeyForChange(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public SpsTPoDomain lockByKey(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public SpsTPoDomain lockByKeyNoWait(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public void create(SpsTPoDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTPo"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTPoDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTPo"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTPoDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTPo"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTPoDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTPo"
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTPoDomain domain, SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTPo"
     * @param criteria List of CriteriaDomain of "spsTPo"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTPoDomain> domains, List<SpsTPoCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTPoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTPoCriteriaDomain criteria) throws ApplicationException;

}
