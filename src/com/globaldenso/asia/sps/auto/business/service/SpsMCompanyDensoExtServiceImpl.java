/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/05/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain;


/**
 * A "Service" implementation class of "SpsMCompanyDensoExt"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/05/17 17:15:42<br />
 * 
 * This module generated automatically in 2016/05/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanyDensoExtServiceImpl implements SpsMCompanyDensoExtService {

    /**
     * A "Dao" of "spsMCompanyDensoExt"
     */
    private SpsMCompanyDensoExtDao spsMCompanyDensoExtDao;

    /**
     * Default constructor
     */
    public SpsMCompanyDensoExtServiceImpl() {
    }

    /**
     * Set Dao of "spsMCompanyDensoExt"
     * 
     * @param spsMCompanyDensoExtDao spsMCompanyDensoExtDao
     */
    public void setSpsMCompanyDensoExtDao(SpsMCompanyDensoExtDao spsMCompanyDensoExtDao) {
        this.spsMCompanyDensoExtDao = spsMCompanyDensoExtDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public SpsMCompanyDensoExtDomain searchByKey(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public List<SpsMCompanyDensoExtDomain> searchByCondition(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public List<SpsMCompanyDensoExtDomain> searchByConditionForPaging(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int searchCount(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    @Deprecated
    public SpsMCompanyDensoExtDomain searchByKeyForChange(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public SpsMCompanyDensoExtDomain lockByKey(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public SpsMCompanyDensoExtDomain lockByKeyNoWait(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain)
     */
    public void create(SpsMCompanyDensoExtDomain domain) throws ApplicationException {
        spsMCompanyDensoExtDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#create(java.util.List)
     */
    public void create(List<SpsMCompanyDensoExtDomain> domains) throws ApplicationException {
        for(SpsMCompanyDensoExtDomain domain : domains) {
            spsMCompanyDensoExtDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain)
     */
    public int update(SpsMCompanyDensoExtDomain domain) throws ApplicationException {
        return spsMCompanyDensoExtDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#update(java.util.List)
     */
    public int update(List<SpsMCompanyDensoExtDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMCompanyDensoExtDomain domain : domains) {
            updateCount += spsMCompanyDensoExtDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int updateByCondition(SpsMCompanyDensoExtDomain domain, SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMCompanyDensoExtDomain> domains, List<SpsMCompanyDensoExtCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMCompanyDensoExtDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int delete(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoExtService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int deleteByCondition(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoExtDao.deleteByCondition(criteria);
    }

}
