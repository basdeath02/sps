/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/18       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain;


/**
 * A "Service" implementation class of "SpsTInvCoverpageRunno"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/18 14:44:35<br />
 * 
 * This module generated automatically in 2015/05/18 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvCoverpageRunnoServiceImpl implements SpsTInvCoverpageRunnoService {

    /**
     * A "Dao" of "spsTInvCoverpageRunno"
     */
    private SpsTInvCoverpageRunnoDao spsTInvCoverpageRunnoDao;

    /**
     * Default constructor
     */
    public SpsTInvCoverpageRunnoServiceImpl() {
    }

    /**
     * Set Dao of "spsTInvCoverpageRunno"
     * 
     * @param spsTInvCoverpageRunnoDao spsTInvCoverpageRunnoDao
     */
    public void setSpsTInvCoverpageRunnoDao(SpsTInvCoverpageRunnoDao spsTInvCoverpageRunnoDao) {
        this.spsTInvCoverpageRunnoDao = spsTInvCoverpageRunnoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public SpsTInvCoverpageRunnoDomain searchByKey(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public List<SpsTInvCoverpageRunnoDomain> searchByCondition(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public List<SpsTInvCoverpageRunnoDomain> searchByConditionForPaging(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int searchCount(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    @Deprecated
    public SpsTInvCoverpageRunnoDomain searchByKeyForChange(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public SpsTInvCoverpageRunnoDomain lockByKey(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public SpsTInvCoverpageRunnoDomain lockByKeyNoWait(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain)
     */
    public void create(SpsTInvCoverpageRunnoDomain domain) throws ApplicationException {
        spsTInvCoverpageRunnoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#create(java.util.List)
     */
    public void create(List<SpsTInvCoverpageRunnoDomain> domains) throws ApplicationException {
        for(SpsTInvCoverpageRunnoDomain domain : domains) {
            spsTInvCoverpageRunnoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain)
     */
    public int update(SpsTInvCoverpageRunnoDomain domain) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#update(java.util.List)
     */
    public int update(List<SpsTInvCoverpageRunnoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTInvCoverpageRunnoDomain domain : domains) {
            updateCount += spsTInvCoverpageRunnoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int updateByCondition(SpsTInvCoverpageRunnoDomain domain, SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTInvCoverpageRunnoDomain> domains, List<SpsTInvCoverpageRunnoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTInvCoverpageRunnoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int delete(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int deleteByCondition(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTInvCoverpageRunnoDao.deleteByCondition(criteria);
    }

}
