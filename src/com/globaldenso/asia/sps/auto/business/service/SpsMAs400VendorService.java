/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;


/**
 * A "Service" interface of "SpsMAs400Vendor"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMAs400VendorService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public SpsMAs400VendorDomain searchByKey(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return List of Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public List<SpsMAs400VendorDomain> searchByCondition(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return List of Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public List<SpsMAs400VendorDomain> searchByConditionForPaging(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMAs400Vendor
     * @return Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMAs400VendorDomain searchByKeyForChange(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public SpsMAs400VendorDomain lockByKey(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public SpsMAs400VendorDomain lockByKeyNoWait(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public void create(SpsMAs400VendorDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMAs400Vendor"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMAs400VendorDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMAs400Vendor"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMAs400VendorDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMAs400Vendor"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMAs400VendorDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMAs400Vendor"
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMAs400VendorDomain domain, SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMAs400Vendor"
     * @param criteria List of CriteriaDomain of "spsMAs400Vendor"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMAs400VendorDomain> domains, List<SpsMAs400VendorCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMAs400Vendor"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException;

}
