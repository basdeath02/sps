/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;


/**
 * A "Service" implementation class of "SpsMAnnounceMessage"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAnnounceMessageServiceImpl implements SpsMAnnounceMessageService {

    /**
     * A "Dao" of "spsMAnnounceMessage"
     */
    private SpsMAnnounceMessageDao spsMAnnounceMessageDao;

    /**
     * Default constructor
     */
    public SpsMAnnounceMessageServiceImpl() {
    }

    /**
     * Set Dao of "spsMAnnounceMessage"
     * 
     * @param spsMAnnounceMessageDao spsMAnnounceMessageDao
     */
    public void setSpsMAnnounceMessageDao(SpsMAnnounceMessageDao spsMAnnounceMessageDao) {
        this.spsMAnnounceMessageDao = spsMAnnounceMessageDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public SpsMAnnounceMessageDomain searchByKey(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public List<SpsMAnnounceMessageDomain> searchByCondition(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public List<SpsMAnnounceMessageDomain> searchByConditionForPaging(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int searchCount(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    @Deprecated
    public SpsMAnnounceMessageDomain searchByKeyForChange(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public SpsMAnnounceMessageDomain lockByKey(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public SpsMAnnounceMessageDomain lockByKeyNoWait(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain)
     */
    public void create(SpsMAnnounceMessageDomain domain) throws ApplicationException {
        spsMAnnounceMessageDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#create(java.util.List)
     */
    public void create(List<SpsMAnnounceMessageDomain> domains) throws ApplicationException {
        for(SpsMAnnounceMessageDomain domain : domains) {
            spsMAnnounceMessageDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain)
     */
    public int update(SpsMAnnounceMessageDomain domain) throws ApplicationException {
        return spsMAnnounceMessageDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#update(java.util.List)
     */
    public int update(List<SpsMAnnounceMessageDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMAnnounceMessageDomain domain : domains) {
            updateCount += spsMAnnounceMessageDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int updateByCondition(SpsMAnnounceMessageDomain domain, SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMAnnounceMessageDomain> domains, List<SpsMAnnounceMessageCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMAnnounceMessageDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int delete(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAnnounceMessageService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int deleteByCondition(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return spsMAnnounceMessageDao.deleteByCondition(criteria);
    }

}
