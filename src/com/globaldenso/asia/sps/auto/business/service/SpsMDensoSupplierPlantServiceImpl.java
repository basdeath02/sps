/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain;


/**
 * A "Service" implementation class of "SpsMDensoSupplierPlant"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierPlantServiceImpl implements SpsMDensoSupplierPlantService {

    /**
     * A "Dao" of "spsMDensoSupplierPlant"
     */
    private SpsMDensoSupplierPlantDao spsMDensoSupplierPlantDao;

    /**
     * Default constructor
     */
    public SpsMDensoSupplierPlantServiceImpl() {
    }

    /**
     * Set Dao of "spsMDensoSupplierPlant"
     * 
     * @param spsMDensoSupplierPlantDao spsMDensoSupplierPlantDao
     */
    public void setSpsMDensoSupplierPlantDao(SpsMDensoSupplierPlantDao spsMDensoSupplierPlantDao) {
        this.spsMDensoSupplierPlantDao = spsMDensoSupplierPlantDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public List<SpsMDensoSupplierPlantDomain> searchByCondition(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPlantDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public List<SpsMDensoSupplierPlantDomain> searchByConditionForPaging(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPlantDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public int searchCount(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPlantDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain)
     */
    public void create(SpsMDensoSupplierPlantDomain domain) throws ApplicationException {
        spsMDensoSupplierPlantDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#create(java.util.List)
     */
    public void create(List<SpsMDensoSupplierPlantDomain> domains) throws ApplicationException {
        for(SpsMDensoSupplierPlantDomain domain : domains) {
            spsMDensoSupplierPlantDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public int updateByCondition(SpsMDensoSupplierPlantDomain domain, SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPlantDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMDensoSupplierPlantDomain> domains, List<SpsMDensoSupplierPlantCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMDensoSupplierPlantDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPlantService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public int deleteByCondition(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPlantDao.deleteByCondition(criteria);
    }

}
