/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;


/**
 * A "Service" interface of "SpsMPlantDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMPlantDensoService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public SpsMPlantDensoDomain searchByKey(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return List of Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public List<SpsMPlantDensoDomain> searchByCondition(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return List of Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public List<SpsMPlantDensoDomain> searchByConditionForPaging(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMPlantDenso
     * @return Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMPlantDensoDomain searchByKeyForChange(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public SpsMPlantDensoDomain lockByKey(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public SpsMPlantDensoDomain lockByKeyNoWait(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public void create(SpsMPlantDensoDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMPlantDenso"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMPlantDensoDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMPlantDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMPlantDensoDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMPlantDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMPlantDensoDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMPlantDenso"
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMPlantDensoDomain domain, SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMPlantDenso"
     * @param criteria List of CriteriaDomain of "spsMPlantDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMPlantDensoDomain> domains, List<SpsMPlantDensoCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMPlantDenso"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException;

}
