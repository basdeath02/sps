/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain;


/**
 * A "Service" interface of "SpsMTax"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMTaxService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    public SpsMTaxDomain searchByKey(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return List of Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    public List<SpsMTaxDomain> searchByCondition(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return List of Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    public List<SpsMTaxDomain> searchByConditionForPaging(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMTax
     * @return Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMTaxDomain searchByKeyForChange(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    public SpsMTaxDomain lockByKey(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    public SpsMTaxDomain lockByKeyNoWait(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMTax"
     * @throws ApplicationException Exception
     */
    public void create(SpsMTaxDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMTax"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMTaxDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMTax"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMTaxDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMTax"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMTaxDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMTax"
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMTaxDomain domain, SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMTax"
     * @param criteria List of CriteriaDomain of "spsMTax"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMTaxDomain> domains, List<SpsMTaxCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMTax"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMTaxCriteriaDomain criteria) throws ApplicationException;

}
