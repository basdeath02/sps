/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;


/**
 * A "Service" implementation class of "SpsCigmaChgDoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaChgDoErrorServiceImpl implements SpsCigmaChgDoErrorService {

    /**
     * A "Dao" of "spsCigmaChgDoError"
     */
    private SpsCigmaChgDoErrorDao spsCigmaChgDoErrorDao;

    /**
     * Default constructor
     */
    public SpsCigmaChgDoErrorServiceImpl() {
    }

    /**
     * Set Dao of "spsCigmaChgDoError"
     * 
     * @param spsCigmaChgDoErrorDao spsCigmaChgDoErrorDao
     */
    public void setSpsCigmaChgDoErrorDao(SpsCigmaChgDoErrorDao spsCigmaChgDoErrorDao) {
        this.spsCigmaChgDoErrorDao = spsCigmaChgDoErrorDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public SpsCigmaChgDoErrorDomain searchByKey(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public List<SpsCigmaChgDoErrorDomain> searchByCondition(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public List<SpsCigmaChgDoErrorDomain> searchByConditionForPaging(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaChgDoErrorDomain searchByKeyForChange(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public SpsCigmaChgDoErrorDomain lockByKey(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public SpsCigmaChgDoErrorDomain lockByKeyNoWait(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain)
     */
    public void create(SpsCigmaChgDoErrorDomain domain) throws ApplicationException {
        spsCigmaChgDoErrorDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#create(java.util.List)
     */
    public void create(List<SpsCigmaChgDoErrorDomain> domains) throws ApplicationException {
        for(SpsCigmaChgDoErrorDomain domain : domains) {
            spsCigmaChgDoErrorDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain)
     */
    public int update(SpsCigmaChgDoErrorDomain domain) throws ApplicationException {
        return spsCigmaChgDoErrorDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#update(java.util.List)
     */
    public int update(List<SpsCigmaChgDoErrorDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsCigmaChgDoErrorDomain domain : domains) {
            updateCount += spsCigmaChgDoErrorDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaChgDoErrorDomain domain, SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsCigmaChgDoErrorDomain> domains, List<SpsCigmaChgDoErrorCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsCigmaChgDoErrorDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgDoErrorService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgDoErrorDao.deleteByCondition(criteria);
    }

}
