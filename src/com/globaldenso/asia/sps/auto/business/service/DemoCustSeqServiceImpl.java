/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.DemoCustSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "DemoCustSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class DemoCustSeqServiceImpl implements DemoCustSeqService {
 
    /**
     * A "Dao" of "demoCustSeq"
     */
    private DemoCustSeqDao demoCustSeqDao;
 
    /**
     * Default constructor
     */
    public DemoCustSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "demoCustSeq"
     * 
     * @param demoCustSeqDao demoCustSeqDao
     */
    public void setDemoCustSeqDao(DemoCustSeqDao demoCustSeqDao) {
        this.demoCustSeqDao = demoCustSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.DemoCustSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return demoCustSeqDao.getNextValue();
    }
 
}
