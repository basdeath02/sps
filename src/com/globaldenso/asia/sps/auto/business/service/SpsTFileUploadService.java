/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/13       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;


/**
 * A "Service" interface of "SpsTFileUpload"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/13 11:02:03<br />
 * 
 * This module generated automatically in 2014/10/13 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTFileUploadService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public SpsTFileUploadDomain searchByKey(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return List of Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public List<SpsTFileUploadDomain> searchByCondition(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return List of Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public List<SpsTFileUploadDomain> searchByConditionForPaging(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTFileUpload
     * @return Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTFileUploadDomain searchByKeyForChange(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public SpsTFileUploadDomain lockByKey(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public SpsTFileUploadDomain lockByKeyNoWait(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public void create(SpsTFileUploadDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTFileUpload"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTFileUploadDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTFileUpload"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTFileUploadDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTFileUpload"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTFileUploadDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTFileUpload"
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTFileUploadDomain domain, SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTFileUpload"
     * @param criteria List of CriteriaDomain of "spsTFileUpload"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTFileUploadDomain> domains, List<SpsTFileUploadCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTFileUpload"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException;

}
