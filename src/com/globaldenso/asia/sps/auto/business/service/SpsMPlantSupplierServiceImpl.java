/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;


/**
 * A "Service" implementation class of "SpsMPlantSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMPlantSupplierServiceImpl implements SpsMPlantSupplierService {

    /**
     * A "Dao" of "spsMPlantSupplier"
     */
    private SpsMPlantSupplierDao spsMPlantSupplierDao;

    /**
     * Default constructor
     */
    public SpsMPlantSupplierServiceImpl() {
    }

    /**
     * Set Dao of "spsMPlantSupplier"
     * 
     * @param spsMPlantSupplierDao spsMPlantSupplierDao
     */
    public void setSpsMPlantSupplierDao(SpsMPlantSupplierDao spsMPlantSupplierDao) {
        this.spsMPlantSupplierDao = spsMPlantSupplierDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public SpsMPlantSupplierDomain searchByKey(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public List<SpsMPlantSupplierDomain> searchByCondition(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public List<SpsMPlantSupplierDomain> searchByConditionForPaging(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int searchCount(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    @Deprecated
    public SpsMPlantSupplierDomain searchByKeyForChange(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public SpsMPlantSupplierDomain lockByKey(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public SpsMPlantSupplierDomain lockByKeyNoWait(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain)
     */
    public void create(SpsMPlantSupplierDomain domain) throws ApplicationException {
        spsMPlantSupplierDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#create(java.util.List)
     */
    public void create(List<SpsMPlantSupplierDomain> domains) throws ApplicationException {
        for(SpsMPlantSupplierDomain domain : domains) {
            spsMPlantSupplierDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain)
     */
    public int update(SpsMPlantSupplierDomain domain) throws ApplicationException {
        return spsMPlantSupplierDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#update(java.util.List)
     */
    public int update(List<SpsMPlantSupplierDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMPlantSupplierDomain domain : domains) {
            updateCount += spsMPlantSupplierDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int updateByCondition(SpsMPlantSupplierDomain domain, SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMPlantSupplierDomain> domains, List<SpsMPlantSupplierCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMPlantSupplierDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int delete(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMPlantSupplierDao.deleteByCondition(criteria);
    }

}
