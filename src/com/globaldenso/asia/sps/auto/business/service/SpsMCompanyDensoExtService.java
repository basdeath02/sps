/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/05/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain;


/**
 * A "Service" interface of "SpsMCompanyDensoExt"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/05/17 17:15:42<br />
 * 
 * This module generated automatically in 2016/05/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMCompanyDensoExtService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public SpsMCompanyDensoExtDomain searchByKey(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return List of Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public List<SpsMCompanyDensoExtDomain> searchByCondition(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return List of Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public List<SpsMCompanyDensoExtDomain> searchByConditionForPaging(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMCompanyDensoExt
     * @return Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMCompanyDensoExtDomain searchByKeyForChange(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public SpsMCompanyDensoExtDomain lockByKey(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public SpsMCompanyDensoExtDomain lockByKeyNoWait(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public void create(SpsMCompanyDensoExtDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMCompanyDensoExt"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMCompanyDensoExtDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMCompanyDensoExt"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMCompanyDensoExtDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMCompanyDensoExt"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMCompanyDensoExtDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMCompanyDensoExt"
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMCompanyDensoExtDomain domain, SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMCompanyDensoExt"
     * @param criteria List of CriteriaDomain of "spsMCompanyDensoExt"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMCompanyDensoExtDomain> domains, List<SpsMCompanyDensoExtCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDensoExt"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException;

}
