/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain;


/**
 * A "Service" implementation class of "SpsMAs400Lpar"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400LparServiceImpl implements SpsMAs400LparService {

    /**
     * A "Dao" of "spsMAs400Lpar"
     */
    private SpsMAs400LparDao spsMAs400LparDao;

    /**
     * Default constructor
     */
    public SpsMAs400LparServiceImpl() {
    }

    /**
     * Set Dao of "spsMAs400Lpar"
     * 
     * @param spsMAs400LparDao spsMAs400LparDao
     */
    public void setSpsMAs400LparDao(SpsMAs400LparDao spsMAs400LparDao) {
        this.spsMAs400LparDao = spsMAs400LparDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public SpsMAs400LparDomain searchByKey(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public List<SpsMAs400LparDomain> searchByCondition(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public List<SpsMAs400LparDomain> searchByConditionForPaging(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int searchCount(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    @Deprecated
    public SpsMAs400LparDomain searchByKeyForChange(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public SpsMAs400LparDomain lockByKey(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public SpsMAs400LparDomain lockByKeyNoWait(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain)
     */
    public void create(SpsMAs400LparDomain domain) throws ApplicationException {
        spsMAs400LparDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#create(java.util.List)
     */
    public void create(List<SpsMAs400LparDomain> domains) throws ApplicationException {
        for(SpsMAs400LparDomain domain : domains) {
            spsMAs400LparDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain)
     */
    public int update(SpsMAs400LparDomain domain) throws ApplicationException {
        return spsMAs400LparDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#update(java.util.List)
     */
    public int update(List<SpsMAs400LparDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMAs400LparDomain domain : domains) {
            updateCount += spsMAs400LparDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int updateByCondition(SpsMAs400LparDomain domain, SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMAs400LparDomain> domains, List<SpsMAs400LparCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMAs400LparDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int delete(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400LparService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int deleteByCondition(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400LparDao.deleteByCondition(criteria);
    }

}
