/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;


/**
 * A "Service" interface of "SpsTCnDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/28 10:05:00<br />
 * 
 * This module generated automatically in 2015/01/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTCnDetailService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public SpsTCnDetailDomain searchByKey(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return List of Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTCnDetailDomain> searchByCondition(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return List of Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTCnDetailDomain> searchByConditionForPaging(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTCnDetail
     * @return Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTCnDetailDomain searchByKeyForChange(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public SpsTCnDetailDomain lockByKey(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public SpsTCnDetailDomain lockByKeyNoWait(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public void create(SpsTCnDetailDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTCnDetail"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTCnDetailDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTCnDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTCnDetailDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTCnDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTCnDetailDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTCnDetail"
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTCnDetailDomain domain, SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTCnDetail"
     * @param criteria List of CriteriaDomain of "spsTCnDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTCnDetailDomain> domains, List<SpsTCnDetailCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTCnDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException;

}
