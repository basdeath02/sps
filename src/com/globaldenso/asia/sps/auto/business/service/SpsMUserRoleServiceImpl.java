/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;


/**
 * A "Service" implementation class of "SpsMUserRole"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserRoleServiceImpl implements SpsMUserRoleService {

    /**
     * A "Dao" of "spsMUserRole"
     */
    private SpsMUserRoleDao spsMUserRoleDao;

    /**
     * Default constructor
     */
    public SpsMUserRoleServiceImpl() {
    }

    /**
     * Set Dao of "spsMUserRole"
     * 
     * @param spsMUserRoleDao spsMUserRoleDao
     */
    public void setSpsMUserRoleDao(SpsMUserRoleDao spsMUserRoleDao) {
        this.spsMUserRoleDao = spsMUserRoleDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public List<SpsMUserRoleDomain> searchByCondition(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMUserRoleDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public List<SpsMUserRoleDomain> searchByConditionForPaging(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMUserRoleDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public int searchCount(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMUserRoleDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain)
     */
    public void create(SpsMUserRoleDomain domain) throws ApplicationException {
        spsMUserRoleDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#create(java.util.List)
     */
    public void create(List<SpsMUserRoleDomain> domains) throws ApplicationException {
        for(SpsMUserRoleDomain domain : domains) {
            spsMUserRoleDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public int updateByCondition(SpsMUserRoleDomain domain, SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMUserRoleDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMUserRoleDomain> domains, List<SpsMUserRoleCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMUserRoleDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserRoleService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return spsMUserRoleDao.deleteByCondition(criteria);
    }

}
