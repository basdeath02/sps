/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/04/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain;


/**
 * A "Service" implementation class of "SpsTmpUploadError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/04/07 12:25:27<br />
 * 
 * This module generated automatically in 2559/04/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadErrorServiceImpl implements SpsTmpUploadErrorService {

    /**
     * A "Dao" of "spsTmpUploadError"
     */
    private SpsTmpUploadErrorDao spsTmpUploadErrorDao;

    /**
     * Default constructor
     */
    public SpsTmpUploadErrorServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpUploadError"
     * 
     * @param spsTmpUploadErrorDao spsTmpUploadErrorDao
     */
    public void setSpsTmpUploadErrorDao(SpsTmpUploadErrorDao spsTmpUploadErrorDao) {
        this.spsTmpUploadErrorDao = spsTmpUploadErrorDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public SpsTmpUploadErrorDomain searchByKey(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public List<SpsTmpUploadErrorDomain> searchByCondition(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public List<SpsTmpUploadErrorDomain> searchByConditionForPaging(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int searchCount(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUploadErrorDomain searchByKeyForChange(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public SpsTmpUploadErrorDomain lockByKey(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public SpsTmpUploadErrorDomain lockByKeyNoWait(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain)
     */
    public void create(SpsTmpUploadErrorDomain domain) throws ApplicationException {
        spsTmpUploadErrorDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#create(java.util.List)
     */
    public void create(List<SpsTmpUploadErrorDomain> domains) throws ApplicationException {
        for(SpsTmpUploadErrorDomain domain : domains) {
            spsTmpUploadErrorDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain)
     */
    public int update(SpsTmpUploadErrorDomain domain) throws ApplicationException {
        return spsTmpUploadErrorDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#update(java.util.List)
     */
    public int update(List<SpsTmpUploadErrorDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpUploadErrorDomain domain : domains) {
            updateCount += spsTmpUploadErrorDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUploadErrorDomain domain, SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpUploadErrorDomain> domains, List<SpsTmpUploadErrorCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpUploadErrorDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int delete(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadErrorService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadErrorDao.deleteByCondition(criteria);
    }

}
