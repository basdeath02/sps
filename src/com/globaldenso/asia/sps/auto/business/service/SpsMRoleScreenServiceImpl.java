/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain;


/**
 * A "Service" implementation class of "SpsMRoleScreen"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleScreenServiceImpl implements SpsMRoleScreenService {

    /**
     * A "Dao" of "spsMRoleScreen"
     */
    private SpsMRoleScreenDao spsMRoleScreenDao;

    /**
     * Default constructor
     */
    public SpsMRoleScreenServiceImpl() {
    }

    /**
     * Set Dao of "spsMRoleScreen"
     * 
     * @param spsMRoleScreenDao spsMRoleScreenDao
     */
    public void setSpsMRoleScreenDao(SpsMRoleScreenDao spsMRoleScreenDao) {
        this.spsMRoleScreenDao = spsMRoleScreenDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public SpsMRoleScreenDomain searchByKey(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public List<SpsMRoleScreenDomain> searchByCondition(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public List<SpsMRoleScreenDomain> searchByConditionForPaging(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int searchCount(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleScreenDomain searchByKeyForChange(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public SpsMRoleScreenDomain lockByKey(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public SpsMRoleScreenDomain lockByKeyNoWait(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain)
     */
    public void create(SpsMRoleScreenDomain domain) throws ApplicationException {
        spsMRoleScreenDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#create(java.util.List)
     */
    public void create(List<SpsMRoleScreenDomain> domains) throws ApplicationException {
        for(SpsMRoleScreenDomain domain : domains) {
            spsMRoleScreenDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain)
     */
    public int update(SpsMRoleScreenDomain domain) throws ApplicationException {
        return spsMRoleScreenDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#update(java.util.List)
     */
    public int update(List<SpsMRoleScreenDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMRoleScreenDomain domain : domains) {
            updateCount += spsMRoleScreenDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleScreenDomain domain, SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMRoleScreenDomain> domains, List<SpsMRoleScreenCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMRoleScreenDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int delete(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleScreenService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleScreenDao.deleteByCondition(criteria);
    }

}
