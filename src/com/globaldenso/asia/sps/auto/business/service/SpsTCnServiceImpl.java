/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;


/**
 * A "Service" implementation class of "SpsTCn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTCnServiceImpl implements SpsTCnService {

    /**
     * A "Dao" of "spsTCn"
     */
    private SpsTCnDao spsTCnDao;

    /**
     * Default constructor
     */
    public SpsTCnServiceImpl() {
    }

    /**
     * Set Dao of "spsTCn"
     * 
     * @param spsTCnDao spsTCnDao
     */
    public void setSpsTCnDao(SpsTCnDao spsTCnDao) {
        this.spsTCnDao = spsTCnDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public SpsTCnDomain searchByKey(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public List<SpsTCnDomain> searchByCondition(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public List<SpsTCnDomain> searchByConditionForPaging(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int searchCount(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    @Deprecated
    public SpsTCnDomain searchByKeyForChange(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public SpsTCnDomain lockByKey(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public SpsTCnDomain lockByKeyNoWait(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain)
     */
    public void create(SpsTCnDomain domain) throws ApplicationException {
        spsTCnDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#create(java.util.List)
     */
    public void create(List<SpsTCnDomain> domains) throws ApplicationException {
        for(SpsTCnDomain domain : domains) {
            spsTCnDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain)
     */
    public int update(SpsTCnDomain domain) throws ApplicationException {
        return spsTCnDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#update(java.util.List)
     */
    public int update(List<SpsTCnDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTCnDomain domain : domains) {
            updateCount += spsTCnDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int updateByCondition(SpsTCnDomain domain, SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTCnDomain> domains, List<SpsTCnCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTCnDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int delete(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int deleteByCondition(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDao.deleteByCondition(criteria);
    }

}
