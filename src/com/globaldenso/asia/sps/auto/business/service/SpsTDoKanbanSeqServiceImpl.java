/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain;


/**
 * A "Service" implementation class of "SpsTDoKanbanSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/05 13:51:36<br />
 * 
 * This module generated automatically in 2015/02/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoKanbanSeqServiceImpl implements SpsTDoKanbanSeqService {

    /**
     * A "Dao" of "spsTDoKanbanSeq"
     */
    private SpsTDoKanbanSeqDao spsTDoKanbanSeqDao;

    /**
     * Default constructor
     */
    public SpsTDoKanbanSeqServiceImpl() {
    }

    /**
     * Set Dao of "spsTDoKanbanSeq"
     * 
     * @param spsTDoKanbanSeqDao spsTDoKanbanSeqDao
     */
    public void setSpsTDoKanbanSeqDao(SpsTDoKanbanSeqDao spsTDoKanbanSeqDao) {
        this.spsTDoKanbanSeqDao = spsTDoKanbanSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public SpsTDoKanbanSeqDomain searchByKey(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public List<SpsTDoKanbanSeqDomain> searchByCondition(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public List<SpsTDoKanbanSeqDomain> searchByConditionForPaging(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int searchCount(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    @Deprecated
    public SpsTDoKanbanSeqDomain searchByKeyForChange(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public SpsTDoKanbanSeqDomain lockByKey(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public SpsTDoKanbanSeqDomain lockByKeyNoWait(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain)
     */
    public void create(SpsTDoKanbanSeqDomain domain) throws ApplicationException {
        spsTDoKanbanSeqDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#create(java.util.List)
     */
    public void create(List<SpsTDoKanbanSeqDomain> domains) throws ApplicationException {
        for(SpsTDoKanbanSeqDomain domain : domains) {
            spsTDoKanbanSeqDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain)
     */
    public int update(SpsTDoKanbanSeqDomain domain) throws ApplicationException {
        return spsTDoKanbanSeqDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#update(java.util.List)
     */
    public int update(List<SpsTDoKanbanSeqDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTDoKanbanSeqDomain domain : domains) {
            updateCount += spsTDoKanbanSeqDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int updateByCondition(SpsTDoKanbanSeqDomain domain, SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTDoKanbanSeqDomain> domains, List<SpsTDoKanbanSeqCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTDoKanbanSeqDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int delete(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int deleteByCondition(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return spsTDoKanbanSeqDao.deleteByCondition(criteria);
    }

}
