/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/26       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;


/**
 * A "Service" interface of "SpsMCompanyDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/26 14:21:53<br />
 * 
 * This module generated automatically in 2015/05/26 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMCompanyDensoService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public SpsMCompanyDensoDomain searchByKey(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return List of Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public List<SpsMCompanyDensoDomain> searchByCondition(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return List of Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public List<SpsMCompanyDensoDomain> searchByConditionForPaging(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMCompanyDenso
     * @return Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMCompanyDensoDomain searchByKeyForChange(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public SpsMCompanyDensoDomain lockByKey(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public SpsMCompanyDensoDomain lockByKeyNoWait(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public void create(SpsMCompanyDensoDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMCompanyDenso"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMCompanyDensoDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMCompanyDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMCompanyDensoDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMCompanyDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMCompanyDensoDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMCompanyDenso"
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMCompanyDensoDomain domain, SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMCompanyDenso"
     * @param criteria List of CriteriaDomain of "spsMCompanyDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMCompanyDensoDomain> domains, List<SpsMCompanyDensoCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanyDenso"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException;

}
