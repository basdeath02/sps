/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnFromCIGMADao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoToCIGMADao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.SendEmailDomain;
import com.globaldenso.asia.sps.business.service.MailService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.jobcontrol.TransferPoDataToCigmaJob;


/**
 * A "Service" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnFromCIGMAServiceImpl implements SpsTAsnFromCIGMAService {

	
    /**
     * A "Dao" of "spsTPo"
     */
    private SpsTAsnFromCIGMADao spsTAsnFromCIGMADao;


    /**
     * Default constructor
     */
    public SpsTAsnFromCIGMAServiceImpl() {
    }

    /**
     * Set Dao of "spsTPo"
     * 
     * @param spsTPoDao spsTPoDao
     */
  
 

	
	/** The Constant LOG. */
	private static final Log LOG = LogFactory
			.getLog(SpsTPoToCIGMAService.class);
	public SpsTAsnFromCIGMADao getSpsTAsnFromCIGMADao() {
		return spsTAsnFromCIGMADao;
	}



	public void setSpsTAsnFromCIGMADao(SpsTAsnFromCIGMADao spsTAsnFromCIGMADao) {
		this.spsTAsnFromCIGMADao = spsTAsnFromCIGMADao;
	}



	/**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchByCondition(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException {
    	
    	
        return spsTAsnFromCIGMADao.searchByCondition(criteria);
    }
    
    

    
 public List<SPSTDoHeaderDNDomain> searchDONotCcl(SPSTDoHeaderDNDomain criteria) throws ApplicationException {
    	
    	
        return spsTAsnFromCIGMADao.searchDONotCcl(criteria);
    }

 public List<SPSTDoHeaderDNDomain> searchDOPrevCcl(SPSTDoHeaderDNDomain criteria) throws ApplicationException {
 	
 	
     return spsTAsnFromCIGMADao.searchDOPrevCcl(criteria);
 }
 
 public List<SPSTDoHeaderDNDomain> searchDO(SPSTDoHeaderDNDomain criteria) throws ApplicationException {
 	
 	
     return spsTAsnFromCIGMADao.searchDO(criteria);
 }
 
 public List<SPSTDoDetailDNDomain> searchDODetail(SPSTDoDetailDNDomain criteria) throws ApplicationException {
	 	
	 	
     return spsTAsnFromCIGMADao.searchDODetail(criteria);
 }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#UpdatePoTranFlag(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoDensoRelationWithPODNDomain)
     */
    public boolean InsertUpdateASN(SPSTAsnHeaderDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.InsertUpdateASN(criteria);
        return updateFlag;
    }
    public boolean InsertASNDetail(SPSTAsnDetailDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.InsertASNDetail(criteria);
        return updateFlag;
    }
    
    public boolean UpdateDO(SPSTDoHeaderDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.UpdateDO(criteria);
        return updateFlag;
    }
    public boolean UpdateDODet(SPSTDoDetailDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.UpdateDODet(criteria);
        return updateFlag;
    }
    public boolean UpdateTranFlag(SPSTAsnHeaderDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.UpdateTranFlag(criteria);
        return updateFlag;
    }
    public boolean DeleteASN(SPSTAsnHeaderDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.DeleteASN(criteria);
        return updateFlag;
    }
    
    public List<DeliveryOrderDetailDomain> searchExistDo(
            DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        {
            List<DeliveryOrderDetailDomain> deliveryOrderDetailResultList = null;
            deliveryOrderDetailResultList  = spsTAsnFromCIGMADao.searchExistDo(deliveryOrderInformationDomain);
            return deliveryOrderDetailResultList;
        }
    
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.DeliveryOrderService#searchDeliveryOrderAcknowledgement(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        SpsTPoDomain poDomain = null;
        poDomain = spsTAsnFromCIGMADao.searchDeliveryOrderAcknowledgement(
            deliveryOrderInformationDomain);
        return poDomain;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.eps.business.service.DeliveryOrderService#searchAcknowledgedDO(com.globaldenso.asia.sps.business.domain.AcknowledgedDOInformationDomain)
     */
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDOInformationDomain)
    {
        StringBuffer utcStr = null;
        StringBuffer deliveryDateStr = null;
        StringBuffer shipmentDateStr = null;
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInfoResultList
            = new ArrayList<AcknowledgedDoInformationReturnDomain>();
        
        acknowledgedDOInfoResultList = spsTAsnFromCIGMADao
            .searchAcknowledgedDo(acknowledgedDOInformationDomain);
        if (null != acknowledgedDOInfoResultList
            && Constants.ZERO < acknowledgedDOInfoResultList.size())
        {
            for (AcknowledgedDoInformationReturnDomain acknowledgedDoInfo 
                : acknowledgedDOInfoResultList)
            {
                utcStr = new StringBuffer();
                deliveryDateStr = new StringBuffer();
                shipmentDateStr = new StringBuffer();
                utcStr.append(Constants.SYMBOL_OPEN_BRACKET)
                    .append(Constants.WORD_UTC)
                    .append(acknowledgedDoInfo.getUtc().trim())
                    .append(Constants.SYMBOL_CLOSE_BRACKET);
                deliveryDateStr.append(DateUtil.format(new Date(
                    acknowledgedDoInfo.getDeliveryOrderDomain().getDeliveryDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH)).append(utcStr);
                shipmentDateStr.append(DateUtil.format(new Date(
                    acknowledgedDoInfo.getDeliveryOrderDomain().getShipDatetime().getTime()),
                    DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH)).append(utcStr);
                acknowledgedDoInfo.setDeliveryDateStr(deliveryDateStr.toString());
                acknowledgedDoInfo.setShipmentDateStr(shipmentDateStr.toString());
                if(null != acknowledgedDoInfo.getDeliveryOrderDomain().getDoIssueDate()){
                    acknowledgedDoInfo.setIssuedDateStr(DateUtil.format(new Date(
                        acknowledgedDoInfo.getDeliveryOrderDomain().getDoIssueDate().getTime()),
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
                }else{
                    acknowledgedDoInfo.setIssuedDateStr(Constants.EMPTY_STRING);
                }
            }
        }
        return acknowledgedDOInfoResultList;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int searchCount(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.searchCount(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int searchCountPlant(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.searchCountPlant(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int SearchCountPrevASN(SPSTAsnHeaderDNDomain  criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.SearchCountPrevASN(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public List<SpsMAs400VendorDomain> searchByConditionAS400(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.searchByConditionAS400(criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMPlantSupplierService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int searchCountPlantSupplier(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.searchCountPlantSupplier(criteria);
    }
    /**
	 * Gets the content.
	 * 
	 * @return default locale
	 */
	private Locale getDefaultLocale() {
		Properties propApp = Props
				.getProperties(Constants.COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG);
		String strDefaultLocale = propApp.getProperty(Constants.DEFAULT_LOCALE);
		String strBaseDirGui = propApp.getProperty(Constants.PROP_BASE_DIR_GUI);
		String strBaseDirMsg = propApp.getProperty(Constants.PROP_BASE_DIR_MSG);
		ContextParams.setDefaultLocale(strDefaultLocale);
		ContextParams.setBaseDirGui(strBaseDirGui);
		ContextParams.setBaseDirMsg(strBaseDirMsg);
		return LocaleUtil.getLocaleFromString(strDefaultLocale);
	}
	
    public boolean UpdateDOStatus(SPSTAsnHeaderDNDomain criteria) throws ApplicationException {
    	boolean updateFlag =spsTAsnFromCIGMADao.UpdateDOStatus(criteria);
        return updateFlag;
    }

    public List<SpsMUserDomain> searchUserDenso(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.searchUserDenso(criteria);
    }
    
    public List<SpsMUserDomain> searchUserSupplier(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException {
        return spsTAsnFromCIGMADao.searchUserSupplier(criteria);
    }
    
    public List<SPSTDoDetailDNDomain> searchDODet(SPSTDoDetailDNDomain criteria) throws ApplicationException {
        
        
        return spsTAsnFromCIGMADao.searchDODet(criteria);
    }
    
    public List<SPSTDoHeaderDNDomain> searchDODelivery(SPSTDoHeaderDNDomain criteria) throws ApplicationException {
        
        
        return spsTAsnFromCIGMADao.searchDODelivery(criteria);
    }

    public List<SpsMDensoDensoRelationWithASNDNDomain> searchByConditionGroup(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException {
        
        
        return spsTAsnFromCIGMADao.searchByConditionGroup(criteria);
    }
    
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchSupplierBySchema(SpsMDensoDensoRelationWithASNDNDomain criteria) throws ApplicationException {
        
        
        return spsTAsnFromCIGMADao.searchSupplierBySchema(criteria);
    }
}
