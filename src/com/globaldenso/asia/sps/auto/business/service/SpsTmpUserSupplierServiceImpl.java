/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain;


/**
 * A "Service" implementation class of "SpsTmpUserSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/30 12:07:21<br />
 * 
 * This module generated automatically in 2015/03/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserSupplierServiceImpl implements SpsTmpUserSupplierService {

    /**
     * A "Dao" of "spsTmpUserSupplier"
     */
    private SpsTmpUserSupplierDao spsTmpUserSupplierDao;

    /**
     * Default constructor
     */
    public SpsTmpUserSupplierServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpUserSupplier"
     * 
     * @param spsTmpUserSupplierDao spsTmpUserSupplierDao
     */
    public void setSpsTmpUserSupplierDao(SpsTmpUserSupplierDao spsTmpUserSupplierDao) {
        this.spsTmpUserSupplierDao = spsTmpUserSupplierDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public SpsTmpUserSupplierDomain searchByKey(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public List<SpsTmpUserSupplierDomain> searchByCondition(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public List<SpsTmpUserSupplierDomain> searchByConditionForPaging(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int searchCount(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUserSupplierDomain searchByKeyForChange(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public SpsTmpUserSupplierDomain lockByKey(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public SpsTmpUserSupplierDomain lockByKeyNoWait(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain)
     */
    public void create(SpsTmpUserSupplierDomain domain) throws ApplicationException {
        spsTmpUserSupplierDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#create(java.util.List)
     */
    public void create(List<SpsTmpUserSupplierDomain> domains) throws ApplicationException {
        for(SpsTmpUserSupplierDomain domain : domains) {
            spsTmpUserSupplierDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain)
     */
    public int update(SpsTmpUserSupplierDomain domain) throws ApplicationException {
        return spsTmpUserSupplierDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#update(java.util.List)
     */
    public int update(List<SpsTmpUserSupplierDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpUserSupplierDomain domain : domains) {
            updateCount += spsTmpUserSupplierDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUserSupplierDomain domain, SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpUserSupplierDomain> domains, List<SpsTmpUserSupplierCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpUserSupplierDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int delete(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUserSupplierService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUserSupplierDao.deleteByCondition(criteria);
    }

}
