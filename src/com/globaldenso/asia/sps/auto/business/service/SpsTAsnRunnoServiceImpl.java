/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain;


/**
 * A "Service" implementation class of "SpsTAsnRunno"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnRunnoServiceImpl implements SpsTAsnRunnoService {

    /**
     * A "Dao" of "spsTAsnRunno"
     */
    private SpsTAsnRunnoDao spsTAsnRunnoDao;

    /**
     * Default constructor
     */
    public SpsTAsnRunnoServiceImpl() {
    }

    /**
     * Set Dao of "spsTAsnRunno"
     * 
     * @param spsTAsnRunnoDao spsTAsnRunnoDao
     */
    public void setSpsTAsnRunnoDao(SpsTAsnRunnoDao spsTAsnRunnoDao) {
        this.spsTAsnRunnoDao = spsTAsnRunnoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public SpsTAsnRunnoDomain searchByKey(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public List<SpsTAsnRunnoDomain> searchByCondition(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public List<SpsTAsnRunnoDomain> searchByConditionForPaging(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int searchCount(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    @Deprecated
    public SpsTAsnRunnoDomain searchByKeyForChange(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public SpsTAsnRunnoDomain lockByKey(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public SpsTAsnRunnoDomain lockByKeyNoWait(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain)
     */
    public void create(SpsTAsnRunnoDomain domain) throws ApplicationException {
        spsTAsnRunnoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#create(java.util.List)
     */
    public void create(List<SpsTAsnRunnoDomain> domains) throws ApplicationException {
        for(SpsTAsnRunnoDomain domain : domains) {
            spsTAsnRunnoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain)
     */
    public int update(SpsTAsnRunnoDomain domain) throws ApplicationException {
        return spsTAsnRunnoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#update(java.util.List)
     */
    public int update(List<SpsTAsnRunnoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTAsnRunnoDomain domain : domains) {
            updateCount += spsTAsnRunnoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int updateByCondition(SpsTAsnRunnoDomain domain, SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTAsnRunnoDomain> domains, List<SpsTAsnRunnoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTAsnRunnoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int delete(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int deleteByCondition(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnRunnoDao.deleteByCondition(criteria);
    }

}
