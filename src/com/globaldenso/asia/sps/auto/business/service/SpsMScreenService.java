/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain;


/**
 * A "Service" interface of "SpsMScreen"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMScreenService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public SpsMScreenDomain searchByKey(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return List of Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public List<SpsMScreenDomain> searchByCondition(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return List of Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public List<SpsMScreenDomain> searchByConditionForPaging(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMScreen
     * @return Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMScreenDomain searchByKeyForChange(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public SpsMScreenDomain lockByKey(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public SpsMScreenDomain lockByKeyNoWait(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public void create(SpsMScreenDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMScreen"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMScreenDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMScreen"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMScreenDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsMScreen"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsMScreenDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMScreen"
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMScreenDomain domain, SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMScreen"
     * @param criteria List of CriteriaDomain of "spsMScreen"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMScreenDomain> domains, List<SpsMScreenCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMScreen"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMScreenCriteriaDomain criteria) throws ApplicationException;

}
