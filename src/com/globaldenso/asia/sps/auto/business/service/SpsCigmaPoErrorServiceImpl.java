/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;


/**
 * A "Service" implementation class of "SpsCigmaPoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaPoErrorServiceImpl implements SpsCigmaPoErrorService {

    /**
     * A "Dao" of "spsCigmaPoError"
     */
    private SpsCigmaPoErrorDao spsCigmaPoErrorDao;

    /**
     * Default constructor
     */
    public SpsCigmaPoErrorServiceImpl() {
    }

    /**
     * Set Dao of "spsCigmaPoError"
     * 
     * @param spsCigmaPoErrorDao spsCigmaPoErrorDao
     */
    public void setSpsCigmaPoErrorDao(SpsCigmaPoErrorDao spsCigmaPoErrorDao) {
        this.spsCigmaPoErrorDao = spsCigmaPoErrorDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public SpsCigmaPoErrorDomain searchByKey(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public List<SpsCigmaPoErrorDomain> searchByCondition(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public List<SpsCigmaPoErrorDomain> searchByConditionForPaging(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaPoErrorDomain searchByKeyForChange(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public SpsCigmaPoErrorDomain lockByKey(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public SpsCigmaPoErrorDomain lockByKeyNoWait(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    public void create(SpsCigmaPoErrorDomain domain) throws ApplicationException {
        spsCigmaPoErrorDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#create(java.util.List)
     */
    public void create(List<SpsCigmaPoErrorDomain> domains) throws ApplicationException {
        for(SpsCigmaPoErrorDomain domain : domains) {
            spsCigmaPoErrorDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    public int update(SpsCigmaPoErrorDomain domain) throws ApplicationException {
        return spsCigmaPoErrorDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#update(java.util.List)
     */
    public int update(List<SpsCigmaPoErrorDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsCigmaPoErrorDomain domain : domains) {
            updateCount += spsCigmaPoErrorDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaPoErrorDomain domain, SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsCigmaPoErrorDomain> domains, List<SpsCigmaPoErrorCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsCigmaPoErrorDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaPoErrorService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaPoErrorDao.deleteByCondition(criteria);
    }

}
