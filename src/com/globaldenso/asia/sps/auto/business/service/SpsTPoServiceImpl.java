/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;


/**
 * A "Service" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoServiceImpl implements SpsTPoService {

    /**
     * A "Dao" of "spsTPo"
     */
    private SpsTPoDao spsTPoDao;

    /**
     * Default constructor
     */
    public SpsTPoServiceImpl() {
    }

    /**
     * Set Dao of "spsTPo"
     * 
     * @param spsTPoDao spsTPoDao
     */
    public void setSpsTPoDao(SpsTPoDao spsTPoDao) {
        this.spsTPoDao = spsTPoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public SpsTPoDomain searchByKey(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public List<SpsTPoDomain> searchByCondition(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public List<SpsTPoDomain> searchByConditionForPaging(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int searchCount(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    @Deprecated
    public SpsTPoDomain searchByKeyForChange(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public SpsTPoDomain lockByKey(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public SpsTPoDomain lockByKeyNoWait(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public void create(SpsTPoDomain domain) throws ApplicationException {
        spsTPoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#create(java.util.List)
     */
    public void create(List<SpsTPoDomain> domains) throws ApplicationException {
        for(SpsTPoDomain domain : domains) {
            spsTPoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public int update(SpsTPoDomain domain) throws ApplicationException {
        return spsTPoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#update(java.util.List)
     */
    public int update(List<SpsTPoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTPoDomain domain : domains) {
            updateCount += spsTPoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int updateByCondition(SpsTPoDomain domain, SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTPoDomain> domains, List<SpsTPoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTPoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int delete(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return spsTPoDao.deleteByCondition(criteria);
    }

}
