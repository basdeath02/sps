/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain;


/**
 * A "Service" interface of "SpsTDoKanbanSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/05 13:51:36<br />
 * 
 * This module generated automatically in 2015/02/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTDoKanbanSeqService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public SpsTDoKanbanSeqDomain searchByKey(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return List of Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public List<SpsTDoKanbanSeqDomain> searchByCondition(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return List of Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public List<SpsTDoKanbanSeqDomain> searchByConditionForPaging(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTDoKanbanSeq
     * @return Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTDoKanbanSeqDomain searchByKeyForChange(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public SpsTDoKanbanSeqDomain lockByKey(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public SpsTDoKanbanSeqDomain lockByKeyNoWait(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public void create(SpsTDoKanbanSeqDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTDoKanbanSeq"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTDoKanbanSeqDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTDoKanbanSeq"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTDoKanbanSeqDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTDoKanbanSeq"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTDoKanbanSeqDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTDoKanbanSeq"
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTDoKanbanSeqDomain domain, SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTDoKanbanSeq"
     * @param criteria List of CriteriaDomain of "spsTDoKanbanSeq"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTDoKanbanSeqDomain> domains, List<SpsTDoKanbanSeqCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTDoKanbanSeq"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException;

}
