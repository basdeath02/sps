/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain;


/**
 * A "Service" interface of "SpsTmpUserDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 12:59:09<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTmpUserDensoService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public SpsTmpUserDensoDomain searchByKey(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return List of Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUserDensoDomain> searchByCondition(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return List of Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUserDensoDomain> searchByConditionForPaging(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTmpUserDenso
     * @return Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTmpUserDensoDomain searchByKeyForChange(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public SpsTmpUserDensoDomain lockByKey(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public SpsTmpUserDensoDomain lockByKeyNoWait(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public void create(SpsTmpUserDensoDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTmpUserDenso"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTmpUserDensoDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTmpUserDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTmpUserDensoDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTmpUserDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTmpUserDensoDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTmpUserDenso"
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTmpUserDensoDomain domain, SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTmpUserDenso"
     * @param criteria List of CriteriaDomain of "spsTmpUserDenso"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTmpUserDensoDomain> domains, List<SpsTmpUserDensoCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUserDenso"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException;

}
