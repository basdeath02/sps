/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/27       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;


/**
 * A "Service" implementation class of "SpsMUserSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/27 17:41:40<br />
 * 
 * This module generated automatically in 2015/03/27 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserSupplierServiceImpl implements SpsMUserSupplierService {

    /**
     * A "Dao" of "spsMUserSupplier"
     */
    private SpsMUserSupplierDao spsMUserSupplierDao;

    /**
     * Default constructor
     */
    public SpsMUserSupplierServiceImpl() {
    }

    /**
     * Set Dao of "spsMUserSupplier"
     * 
     * @param spsMUserSupplierDao spsMUserSupplierDao
     */
    public void setSpsMUserSupplierDao(SpsMUserSupplierDao spsMUserSupplierDao) {
        this.spsMUserSupplierDao = spsMUserSupplierDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public SpsMUserSupplierDomain searchByKey(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public List<SpsMUserSupplierDomain> searchByCondition(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public List<SpsMUserSupplierDomain> searchByConditionForPaging(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int searchCount(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    @Deprecated
    public SpsMUserSupplierDomain searchByKeyForChange(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public SpsMUserSupplierDomain lockByKey(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public SpsMUserSupplierDomain lockByKeyNoWait(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain)
     */
    public void create(SpsMUserSupplierDomain domain) throws ApplicationException {
        spsMUserSupplierDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#create(java.util.List)
     */
    public void create(List<SpsMUserSupplierDomain> domains) throws ApplicationException {
        for(SpsMUserSupplierDomain domain : domains) {
            spsMUserSupplierDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain)
     */
    public int update(SpsMUserSupplierDomain domain) throws ApplicationException {
        return spsMUserSupplierDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#update(java.util.List)
     */
    public int update(List<SpsMUserSupplierDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMUserSupplierDomain domain : domains) {
            updateCount += spsMUserSupplierDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int updateByCondition(SpsMUserSupplierDomain domain, SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMUserSupplierDomain> domains, List<SpsMUserSupplierCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMUserSupplierDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int delete(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserSupplierService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return spsMUserSupplierDao.deleteByCondition(criteria);
    }

}
