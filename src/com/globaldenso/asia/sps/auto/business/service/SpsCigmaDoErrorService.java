/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;


/**
 * A "Service" interface of "SpsCigmaDoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsCigmaDoErrorService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public SpsCigmaDoErrorDomain searchByKey(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return List of Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public List<SpsCigmaDoErrorDomain> searchByCondition(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return List of Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public List<SpsCigmaDoErrorDomain> searchByConditionForPaging(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsCigmaDoError
     * @return Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsCigmaDoErrorDomain searchByKeyForChange(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public SpsCigmaDoErrorDomain lockByKey(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public SpsCigmaDoErrorDomain lockByKeyNoWait(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public void create(SpsCigmaDoErrorDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsCigmaDoError"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsCigmaDoErrorDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsCigmaDoError"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsCigmaDoErrorDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsCigmaDoError"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsCigmaDoErrorDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsCigmaDoError"
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsCigmaDoErrorDomain domain, SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsCigmaDoError"
     * @param criteria List of CriteriaDomain of "spsCigmaDoError"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsCigmaDoErrorDomain> domains, List<SpsCigmaDoErrorCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsCigmaDoError"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException;

}
