/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain;


/**
 * A "Service" implementation class of "SpsTmpSupplierInfo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpSupplierInfoServiceImpl implements SpsTmpSupplierInfoService {

    /**
     * A "Dao" of "spsTmpSupplierInfo"
     */
    private SpsTmpSupplierInfoDao spsTmpSupplierInfoDao;

    /**
     * Default constructor
     */
    public SpsTmpSupplierInfoServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpSupplierInfo"
     * 
     * @param spsTmpSupplierInfoDao spsTmpSupplierInfoDao
     */
    public void setSpsTmpSupplierInfoDao(SpsTmpSupplierInfoDao spsTmpSupplierInfoDao) {
        this.spsTmpSupplierInfoDao = spsTmpSupplierInfoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public SpsTmpSupplierInfoDomain searchByKey(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public List<SpsTmpSupplierInfoDomain> searchByCondition(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public List<SpsTmpSupplierInfoDomain> searchByConditionForPaging(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int searchCount(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    @Deprecated
    public SpsTmpSupplierInfoDomain searchByKeyForChange(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public SpsTmpSupplierInfoDomain lockByKey(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public SpsTmpSupplierInfoDomain lockByKeyNoWait(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain)
     */
    public void create(SpsTmpSupplierInfoDomain domain) throws ApplicationException {
        spsTmpSupplierInfoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#create(java.util.List)
     */
    public void create(List<SpsTmpSupplierInfoDomain> domains) throws ApplicationException {
        for(SpsTmpSupplierInfoDomain domain : domains) {
            spsTmpSupplierInfoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain)
     */
    public int update(SpsTmpSupplierInfoDomain domain) throws ApplicationException {
        return spsTmpSupplierInfoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#update(java.util.List)
     */
    public int update(List<SpsTmpSupplierInfoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpSupplierInfoDomain domain : domains) {
            updateCount += spsTmpSupplierInfoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int updateByCondition(SpsTmpSupplierInfoDomain domain, SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpSupplierInfoDomain> domains, List<SpsTmpSupplierInfoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpSupplierInfoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int delete(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpSupplierInfoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return spsTmpSupplierInfoDao.deleteByCondition(criteria);
    }

}
