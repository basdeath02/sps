/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;


/**
 * A "Service" implementation class of "SpsMUser"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserServiceImpl implements SpsMUserService {

    /**
     * A "Dao" of "spsMUser"
     */
    private SpsMUserDao spsMUserDao;

    /**
     * Default constructor
     */
    public SpsMUserServiceImpl() {
    }

    /**
     * Set Dao of "spsMUser"
     * 
     * @param spsMUserDao spsMUserDao
     */
    public void setSpsMUserDao(SpsMUserDao spsMUserDao) {
        this.spsMUserDao = spsMUserDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public SpsMUserDomain searchByKey(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public List<SpsMUserDomain> searchByCondition(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public List<SpsMUserDomain> searchByConditionForPaging(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int searchCount(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    @Deprecated
    public SpsMUserDomain searchByKeyForChange(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public SpsMUserDomain lockByKey(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public SpsMUserDomain lockByKeyNoWait(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain)
     */
    public void create(SpsMUserDomain domain) throws ApplicationException {
        spsMUserDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#create(java.util.List)
     */
    public void create(List<SpsMUserDomain> domains) throws ApplicationException {
        for(SpsMUserDomain domain : domains) {
            spsMUserDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain)
     */
    public int update(SpsMUserDomain domain) throws ApplicationException {
        return spsMUserDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#update(java.util.List)
     */
    public int update(List<SpsMUserDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMUserDomain domain : domains) {
            updateCount += spsMUserDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int updateByCondition(SpsMUserDomain domain, SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMUserDomain> domains, List<SpsMUserCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMUserDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int delete(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMUserService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return spsMUserDao.deleteByCondition(criteria);
    }

}
