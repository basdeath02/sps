/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/07/12       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.DoIdSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "DoIdSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/07/12 15:00:01<br />
 * 
 * This module generated automatically in 2559/07/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class DoIdSeqServiceImpl implements DoIdSeqService {
 
    /**
     * A "Dao" of "doIdSeq"
     */
    private DoIdSeqDao doIdSeqDao;
 
    /**
     * Default constructor
     */
    public DoIdSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "doIdSeq"
     * 
     * @param doIdSeqDao doIdSeqDao
     */
    public void setDoIdSeqDao(DoIdSeqDao doIdSeqDao) {
        this.doIdSeqDao = doIdSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.DoIdSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return doIdSeqDao.getNextValue();
    }
 
}
