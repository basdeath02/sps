/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;


/**
 * A "Service" implementation class of "SpsTCnDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/28 10:05:00<br />
 * 
 * This module generated automatically in 2015/01/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTCnDetailServiceImpl implements SpsTCnDetailService {

    /**
     * A "Dao" of "spsTCnDetail"
     */
    private SpsTCnDetailDao spsTCnDetailDao;

    /**
     * Default constructor
     */
    public SpsTCnDetailServiceImpl() {
    }

    /**
     * Set Dao of "spsTCnDetail"
     * 
     * @param spsTCnDetailDao spsTCnDetailDao
     */
    public void setSpsTCnDetailDao(SpsTCnDetailDao spsTCnDetailDao) {
        this.spsTCnDetailDao = spsTCnDetailDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public SpsTCnDetailDomain searchByKey(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public List<SpsTCnDetailDomain> searchByCondition(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public List<SpsTCnDetailDomain> searchByConditionForPaging(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int searchCount(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTCnDetailDomain searchByKeyForChange(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public SpsTCnDetailDomain lockByKey(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public SpsTCnDetailDomain lockByKeyNoWait(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain)
     */
    public void create(SpsTCnDetailDomain domain) throws ApplicationException {
        spsTCnDetailDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#create(java.util.List)
     */
    public void create(List<SpsTCnDetailDomain> domains) throws ApplicationException {
        for(SpsTCnDetailDomain domain : domains) {
            spsTCnDetailDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain)
     */
    public int update(SpsTCnDetailDomain domain) throws ApplicationException {
        return spsTCnDetailDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#update(java.util.List)
     */
    public int update(List<SpsTCnDetailDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTCnDetailDomain domain : domains) {
            updateCount += spsTCnDetailDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTCnDetailDomain domain, SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTCnDetailDomain> domains, List<SpsTCnDetailCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTCnDetailDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int delete(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTCnDetailDao.deleteByCondition(criteria);
    }

}
