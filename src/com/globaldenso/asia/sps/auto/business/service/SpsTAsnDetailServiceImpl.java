/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;


/**
 * A "Service" implementation class of "SpsTAsnDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnDetailServiceImpl implements SpsTAsnDetailService {

    /**
     * A "Dao" of "spsTAsnDetail"
     */
    private SpsTAsnDetailDao spsTAsnDetailDao;

    /**
     * Default constructor
     */
    public SpsTAsnDetailServiceImpl() {
    }

    /**
     * Set Dao of "spsTAsnDetail"
     * 
     * @param spsTAsnDetailDao spsTAsnDetailDao
     */
    public void setSpsTAsnDetailDao(SpsTAsnDetailDao spsTAsnDetailDao) {
        this.spsTAsnDetailDao = spsTAsnDetailDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public SpsTAsnDetailDomain searchByKey(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public List<SpsTAsnDetailDomain> searchByCondition(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public List<SpsTAsnDetailDomain> searchByConditionForPaging(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int searchCount(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTAsnDetailDomain searchByKeyForChange(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public SpsTAsnDetailDomain lockByKey(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public SpsTAsnDetailDomain lockByKeyNoWait(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain)
     */
    public void create(SpsTAsnDetailDomain domain) throws ApplicationException {
        spsTAsnDetailDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#create(java.util.List)
     */
    public void create(List<SpsTAsnDetailDomain> domains) throws ApplicationException {
        for(SpsTAsnDetailDomain domain : domains) {
            spsTAsnDetailDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain)
     */
    public int update(SpsTAsnDetailDomain domain) throws ApplicationException {
        return spsTAsnDetailDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#update(java.util.List)
     */
    public int update(List<SpsTAsnDetailDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTAsnDetailDomain domain : domains) {
            updateCount += spsTAsnDetailDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTAsnDetailDomain domain, SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTAsnDetailDomain> domains, List<SpsTAsnDetailCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTAsnDetailDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int delete(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTAsnDetailDao.deleteByCondition(criteria);
    }

}
