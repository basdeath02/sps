/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain;


/**
 * A "Service" implementation class of "SpsTmpUploadAsn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/17 15:14:35<br />
 * 
 * This module generated automatically in 2015/03/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadAsnServiceImpl implements SpsTmpUploadAsnService {

    /**
     * A "Dao" of "spsTmpUploadAsn"
     */
    private SpsTmpUploadAsnDao spsTmpUploadAsnDao;

    /**
     * Default constructor
     */
    public SpsTmpUploadAsnServiceImpl() {
    }

    /**
     * Set Dao of "spsTmpUploadAsn"
     * 
     * @param spsTmpUploadAsnDao spsTmpUploadAsnDao
     */
    public void setSpsTmpUploadAsnDao(SpsTmpUploadAsnDao spsTmpUploadAsnDao) {
        this.spsTmpUploadAsnDao = spsTmpUploadAsnDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public SpsTmpUploadAsnDomain searchByKey(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public List<SpsTmpUploadAsnDomain> searchByCondition(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public List<SpsTmpUploadAsnDomain> searchByConditionForPaging(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int searchCount(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUploadAsnDomain searchByKeyForChange(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public SpsTmpUploadAsnDomain lockByKey(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public SpsTmpUploadAsnDomain lockByKeyNoWait(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain)
     */
    public void create(SpsTmpUploadAsnDomain domain) throws ApplicationException {
        spsTmpUploadAsnDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#create(java.util.List)
     */
    public void create(List<SpsTmpUploadAsnDomain> domains) throws ApplicationException {
        for(SpsTmpUploadAsnDomain domain : domains) {
            spsTmpUploadAsnDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain)
     */
    public int update(SpsTmpUploadAsnDomain domain) throws ApplicationException {
        return spsTmpUploadAsnDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#update(java.util.List)
     */
    public int update(List<SpsTmpUploadAsnDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTmpUploadAsnDomain domain : domains) {
            updateCount += spsTmpUploadAsnDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUploadAsnDomain domain, SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTmpUploadAsnDomain> domains, List<SpsTmpUploadAsnCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTmpUploadAsnDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int delete(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTmpUploadAsnService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return spsTmpUploadAsnDao.deleteByCondition(criteria);
    }

}
