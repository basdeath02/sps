/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.UploadAsnIdSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "UploadAsnIdSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class UploadAsnIdSeqServiceImpl implements UploadAsnIdSeqService {
 
    /**
     * A "Dao" of "uploadAsnIdSeq"
     */
    private UploadAsnIdSeqDao uploadAsnIdSeqDao;
 
    /**
     * Default constructor
     */
    public UploadAsnIdSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "uploadAsnIdSeq"
     * 
     * @param uploadAsnIdSeqDao uploadAsnIdSeqDao
     */
    public void setUploadAsnIdSeqDao(UploadAsnIdSeqDao uploadAsnIdSeqDao) {
        this.uploadAsnIdSeqDao = uploadAsnIdSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.UploadAsnIdSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return uploadAsnIdSeqDao.getNextValue();
    }
 
}
