/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain;


/**
 * A "Service" interface of "SpsTmpUploadAsn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/17 15:14:35<br />
 * 
 * This module generated automatically in 2015/03/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTmpUploadAsnService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public SpsTmpUploadAsnDomain searchByKey(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return List of Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUploadAsnDomain> searchByCondition(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return List of Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUploadAsnDomain> searchByConditionForPaging(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTmpUploadAsn
     * @return Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTmpUploadAsnDomain searchByKeyForChange(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public SpsTmpUploadAsnDomain lockByKey(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public SpsTmpUploadAsnDomain lockByKeyNoWait(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public void create(SpsTmpUploadAsnDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTmpUploadAsn"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTmpUploadAsnDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTmpUploadAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTmpUploadAsnDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTmpUploadAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTmpUploadAsnDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTmpUploadAsn"
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTmpUploadAsnDomain domain, SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTmpUploadAsn"
     * @param criteria List of CriteriaDomain of "spsTmpUploadAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTmpUploadAsnDomain> domains, List<SpsTmpUploadAsnCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadAsn"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException;

}
