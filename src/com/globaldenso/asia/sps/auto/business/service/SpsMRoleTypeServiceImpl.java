/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;


/**
 * A "Service" implementation class of "SpsMRoleType"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleTypeServiceImpl implements SpsMRoleTypeService {

    /**
     * A "Dao" of "spsMRoleType"
     */
    private SpsMRoleTypeDao spsMRoleTypeDao;

    /**
     * Default constructor
     */
    public SpsMRoleTypeServiceImpl() {
    }

    /**
     * Set Dao of "spsMRoleType"
     * 
     * @param spsMRoleTypeDao spsMRoleTypeDao
     */
    public void setSpsMRoleTypeDao(SpsMRoleTypeDao spsMRoleTypeDao) {
        this.spsMRoleTypeDao = spsMRoleTypeDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public SpsMRoleTypeDomain searchByKey(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public List<SpsMRoleTypeDomain> searchByCondition(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public List<SpsMRoleTypeDomain> searchByConditionForPaging(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int searchCount(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleTypeDomain searchByKeyForChange(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public SpsMRoleTypeDomain lockByKey(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public SpsMRoleTypeDomain lockByKeyNoWait(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain)
     */
    public void create(SpsMRoleTypeDomain domain) throws ApplicationException {
        spsMRoleTypeDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#create(java.util.List)
     */
    public void create(List<SpsMRoleTypeDomain> domains) throws ApplicationException {
        for(SpsMRoleTypeDomain domain : domains) {
            spsMRoleTypeDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain)
     */
    public int update(SpsMRoleTypeDomain domain) throws ApplicationException {
        return spsMRoleTypeDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#update(java.util.List)
     */
    public int update(List<SpsMRoleTypeDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMRoleTypeDomain domain : domains) {
            updateCount += spsMRoleTypeDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleTypeDomain domain, SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMRoleTypeDomain> domains, List<SpsMRoleTypeCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMRoleTypeDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int delete(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleTypeService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleTypeDao.deleteByCondition(criteria);
    }

}
