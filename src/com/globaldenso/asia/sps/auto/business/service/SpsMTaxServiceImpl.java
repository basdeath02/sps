/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain;


/**
 * A "Service" implementation class of "SpsMTax"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMTaxServiceImpl implements SpsMTaxService {

    /**
     * A "Dao" of "spsMTax"
     */
    private SpsMTaxDao spsMTaxDao;

    /**
     * Default constructor
     */
    public SpsMTaxServiceImpl() {
    }

    /**
     * Set Dao of "spsMTax"
     * 
     * @param spsMTaxDao spsMTaxDao
     */
    public void setSpsMTaxDao(SpsMTaxDao spsMTaxDao) {
        this.spsMTaxDao = spsMTaxDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public SpsMTaxDomain searchByKey(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public List<SpsMTaxDomain> searchByCondition(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public List<SpsMTaxDomain> searchByConditionForPaging(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int searchCount(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    @Deprecated
    public SpsMTaxDomain searchByKeyForChange(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public SpsMTaxDomain lockByKey(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public SpsMTaxDomain lockByKeyNoWait(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain)
     */
    public void create(SpsMTaxDomain domain) throws ApplicationException {
        spsMTaxDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#create(java.util.List)
     */
    public void create(List<SpsMTaxDomain> domains) throws ApplicationException {
        for(SpsMTaxDomain domain : domains) {
            spsMTaxDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain)
     */
    public int update(SpsMTaxDomain domain) throws ApplicationException {
        return spsMTaxDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#update(java.util.List)
     */
    public int update(List<SpsMTaxDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMTaxDomain domain : domains) {
            updateCount += spsMTaxDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int updateByCondition(SpsMTaxDomain domain, SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMTaxDomain> domains, List<SpsMTaxCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMTaxDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int delete(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMTaxService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int deleteByCondition(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return spsMTaxDao.deleteByCondition(criteria);
    }

}
