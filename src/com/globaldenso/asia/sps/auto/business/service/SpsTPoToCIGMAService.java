/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;


/**
 * A "Service" interface of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTPoToCIGMAService {



    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPo"
     * @return List of Domain class of "spsTPo"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoDensoRelationWithPODNDomain> searchByCondition(SpsMDensoDensoRelationWithPODNDomain criteria) throws ApplicationException;
    
    
    /**
     * Update data and send email return void
     * 
     * @param criteria  List of Domain class of "SpsMDensoSupplierRelationWithPODNDomain"
     * @return Void
     * @throws ApplicationException Exception
     */
    public boolean UpdatePoTranFlag(SPSTPoHeaderDNDomain criteria) throws ApplicationException;


}
