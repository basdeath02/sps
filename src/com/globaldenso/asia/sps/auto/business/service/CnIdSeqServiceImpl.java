/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.CnIdSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "CnIdSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class CnIdSeqServiceImpl implements CnIdSeqService {
 
    /**
     * A "Dao" of "cnIdSeq"
     */
    private CnIdSeqDao cnIdSeqDao;
 
    /**
     * Default constructor
     */
    public CnIdSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "cnIdSeq"
     * 
     * @param cnIdSeqDao cnIdSeqDao
     */
    public void setCnIdSeqDao(CnIdSeqDao cnIdSeqDao) {
        this.cnIdSeqDao = cnIdSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.CnIdSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return cnIdSeqDao.getNextValue();
    }
 
}
