/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;


/**
 * A "Service" implementation class of "SpsMDensoSupplierParts"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierPartsServiceImpl implements SpsMDensoSupplierPartsService {

    /**
     * A "Dao" of "spsMDensoSupplierParts"
     */
    private SpsMDensoSupplierPartsDao spsMDensoSupplierPartsDao;

    /**
     * Default constructor
     */
    public SpsMDensoSupplierPartsServiceImpl() {
    }

    /**
     * Set Dao of "spsMDensoSupplierParts"
     * 
     * @param spsMDensoSupplierPartsDao spsMDensoSupplierPartsDao
     */
    public void setSpsMDensoSupplierPartsDao(SpsMDensoSupplierPartsDao spsMDensoSupplierPartsDao) {
        this.spsMDensoSupplierPartsDao = spsMDensoSupplierPartsDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public SpsMDensoSupplierPartsDomain searchByKey(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public List<SpsMDensoSupplierPartsDomain> searchByCondition(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public List<SpsMDensoSupplierPartsDomain> searchByConditionForPaging(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int searchCount(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    @Deprecated
    public SpsMDensoSupplierPartsDomain searchByKeyForChange(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public SpsMDensoSupplierPartsDomain lockByKey(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public SpsMDensoSupplierPartsDomain lockByKeyNoWait(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain)
     */
    public void create(SpsMDensoSupplierPartsDomain domain) throws ApplicationException {
        spsMDensoSupplierPartsDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#create(java.util.List)
     */
    public void create(List<SpsMDensoSupplierPartsDomain> domains) throws ApplicationException {
        for(SpsMDensoSupplierPartsDomain domain : domains) {
            spsMDensoSupplierPartsDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain)
     */
    public int update(SpsMDensoSupplierPartsDomain domain) throws ApplicationException {
        return spsMDensoSupplierPartsDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#update(java.util.List)
     */
    public int update(List<SpsMDensoSupplierPartsDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMDensoSupplierPartsDomain domain : domains) {
            updateCount += spsMDensoSupplierPartsDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int updateByCondition(SpsMDensoSupplierPartsDomain domain, SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMDensoSupplierPartsDomain> domains, List<SpsMDensoSupplierPartsCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMDensoSupplierPartsDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int delete(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMDensoSupplierPartsService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int deleteByCondition(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return spsMDensoSupplierPartsDao.deleteByCondition(criteria);
    }

}
