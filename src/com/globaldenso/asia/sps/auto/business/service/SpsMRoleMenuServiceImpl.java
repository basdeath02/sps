/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain;


/**
 * A "Service" implementation class of "SpsMRoleMenu"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleMenuServiceImpl implements SpsMRoleMenuService {

    /**
     * A "Dao" of "spsMRoleMenu"
     */
    private SpsMRoleMenuDao spsMRoleMenuDao;

    /**
     * Default constructor
     */
    public SpsMRoleMenuServiceImpl() {
    }

    /**
     * Set Dao of "spsMRoleMenu"
     * 
     * @param spsMRoleMenuDao spsMRoleMenuDao
     */
    public void setSpsMRoleMenuDao(SpsMRoleMenuDao spsMRoleMenuDao) {
        this.spsMRoleMenuDao = spsMRoleMenuDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public SpsMRoleMenuDomain searchByKey(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public List<SpsMRoleMenuDomain> searchByCondition(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public List<SpsMRoleMenuDomain> searchByConditionForPaging(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int searchCount(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleMenuDomain searchByKeyForChange(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public SpsMRoleMenuDomain lockByKey(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public SpsMRoleMenuDomain lockByKeyNoWait(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain)
     */
    public void create(SpsMRoleMenuDomain domain) throws ApplicationException {
        spsMRoleMenuDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#create(java.util.List)
     */
    public void create(List<SpsMRoleMenuDomain> domains) throws ApplicationException {
        for(SpsMRoleMenuDomain domain : domains) {
            spsMRoleMenuDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain)
     */
    public int update(SpsMRoleMenuDomain domain) throws ApplicationException {
        return spsMRoleMenuDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#update(java.util.List)
     */
    public int update(List<SpsMRoleMenuDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMRoleMenuDomain domain : domains) {
            updateCount += spsMRoleMenuDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleMenuDomain domain, SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMRoleMenuDomain> domains, List<SpsMRoleMenuCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMRoleMenuDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int delete(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMRoleMenuService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return spsMRoleMenuDao.deleteByCondition(criteria);
    }

}
