/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.DemoOrderItemsSeqDao;
import java.math.BigDecimal;

/**
 * A "Service" implementation class of "DemoOrderItemsSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class DemoOrderItemsSeqServiceImpl implements DemoOrderItemsSeqService {
 
    /**
     * A "Dao" of "demoOrderItemsSeq"
     */
    private DemoOrderItemsSeqDao demoOrderItemsSeqDao;
 
    /**
     * Default constructor
     */
    public DemoOrderItemsSeqServiceImpl() {
    }

    /**
     * Setter method of the Dao of "demoOrderItemsSeq"
     * 
     * @param demoOrderItemsSeqDao demoOrderItemsSeqDao
     */
    public void setDemoOrderItemsSeqDao(DemoOrderItemsSeqDao demoOrderItemsSeqDao) {
        this.demoOrderItemsSeqDao = demoOrderItemsSeqDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.DemoOrderItemsSeqService#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return demoOrderItemsSeqDao.getNextValue();
    }
 
}
