/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;


/**
 * A "Service" interface of "SpsTAsn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTAsnService {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public SpsTAsnDomain searchByKey(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return List of Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public List<SpsTAsnDomain> searchByCondition(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return List of Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public List<SpsTAsnDomain> searchByConditionForPaging(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTAsn
     * @return Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTAsnDomain searchByKeyForChange(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public SpsTAsnDomain lockByKey(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public SpsTAsnDomain lockByKeyNoWait(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public void create(SpsTAsnDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsTAsn"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsTAsnDomain> domains) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTAsnDomain domain) throws ApplicationException;

    /**
     * Update some data according to the main key as the condition.
     * 
     * @param domains List of Domain of "spsTAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(List<SpsTAsnDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTAsn"
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTAsnDomain domain, SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsTAsn"
     * @param criteria List of CriteriaDomain of "spsTAsn"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsTAsnDomain> domains, List<SpsTAsnCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTAsn"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTAsnCriteriaDomain criteria) throws ApplicationException;

}
