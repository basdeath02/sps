/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;


/**
 * A "Service" implementation class of "SpsCigmaChgPoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaChgPoErrorServiceImpl implements SpsCigmaChgPoErrorService {

    /**
     * A "Dao" of "spsCigmaChgPoError"
     */
    private SpsCigmaChgPoErrorDao spsCigmaChgPoErrorDao;

    /**
     * Default constructor
     */
    public SpsCigmaChgPoErrorServiceImpl() {
    }

    /**
     * Set Dao of "spsCigmaChgPoError"
     * 
     * @param spsCigmaChgPoErrorDao spsCigmaChgPoErrorDao
     */
    public void setSpsCigmaChgPoErrorDao(SpsCigmaChgPoErrorDao spsCigmaChgPoErrorDao) {
        this.spsCigmaChgPoErrorDao = spsCigmaChgPoErrorDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public SpsCigmaChgPoErrorDomain searchByKey(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public List<SpsCigmaChgPoErrorDomain> searchByCondition(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public List<SpsCigmaChgPoErrorDomain> searchByConditionForPaging(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaChgPoErrorDomain searchByKeyForChange(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public SpsCigmaChgPoErrorDomain lockByKey(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public SpsCigmaChgPoErrorDomain lockByKeyNoWait(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain)
     */
    public void create(SpsCigmaChgPoErrorDomain domain) throws ApplicationException {
        spsCigmaChgPoErrorDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#create(java.util.List)
     */
    public void create(List<SpsCigmaChgPoErrorDomain> domains) throws ApplicationException {
        for(SpsCigmaChgPoErrorDomain domain : domains) {
            spsCigmaChgPoErrorDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain)
     */
    public int update(SpsCigmaChgPoErrorDomain domain) throws ApplicationException {
        return spsCigmaChgPoErrorDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#update(java.util.List)
     */
    public int update(List<SpsCigmaChgPoErrorDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsCigmaChgPoErrorDomain domain : domains) {
            updateCount += spsCigmaChgPoErrorDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaChgPoErrorDomain domain, SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsCigmaChgPoErrorDomain> domains, List<SpsCigmaChgPoErrorCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsCigmaChgPoErrorDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsCigmaChgPoErrorService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return spsCigmaChgPoErrorDao.deleteByCondition(criteria);
    }

}
