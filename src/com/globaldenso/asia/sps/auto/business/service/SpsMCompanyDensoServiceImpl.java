/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/26       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;


/**
 * A "Service" implementation class of "SpsMCompanyDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/26 14:21:53<br />
 * 
 * This module generated automatically in 2015/05/26 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanyDensoServiceImpl implements SpsMCompanyDensoService {

    /**
     * A "Dao" of "spsMCompanyDenso"
     */
    private SpsMCompanyDensoDao spsMCompanyDensoDao;

    /**
     * Default constructor
     */
    public SpsMCompanyDensoServiceImpl() {
    }

    /**
     * Set Dao of "spsMCompanyDenso"
     * 
     * @param spsMCompanyDensoDao spsMCompanyDensoDao
     */
    public void setSpsMCompanyDensoDao(SpsMCompanyDensoDao spsMCompanyDensoDao) {
        this.spsMCompanyDensoDao = spsMCompanyDensoDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public SpsMCompanyDensoDomain searchByKey(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public List<SpsMCompanyDensoDomain> searchByCondition(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public List<SpsMCompanyDensoDomain> searchByConditionForPaging(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int searchCount(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    @Deprecated
    public SpsMCompanyDensoDomain searchByKeyForChange(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public SpsMCompanyDensoDomain lockByKey(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public SpsMCompanyDensoDomain lockByKeyNoWait(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public void create(SpsMCompanyDensoDomain domain) throws ApplicationException {
        spsMCompanyDensoDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#create(java.util.List)
     */
    public void create(List<SpsMCompanyDensoDomain> domains) throws ApplicationException {
        for(SpsMCompanyDensoDomain domain : domains) {
            spsMCompanyDensoDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public int update(SpsMCompanyDensoDomain domain) throws ApplicationException {
        return spsMCompanyDensoDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#update(java.util.List)
     */
    public int update(List<SpsMCompanyDensoDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMCompanyDensoDomain domain : domains) {
            updateCount += spsMCompanyDensoDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int updateByCondition(SpsMCompanyDensoDomain domain, SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMCompanyDensoDomain> domains, List<SpsMCompanyDensoCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMCompanyDensoDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int delete(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMCompanyDensoService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return spsMCompanyDensoDao.deleteByCondition(criteria);
    }

}
