/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain;


/**
 * A "Service" implementation class of "SpsTPoCoverPageDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageDetailServiceImpl implements SpsTPoCoverPageDetailService {

    /**
     * A "Dao" of "spsTPoCoverPageDetail"
     */
    private SpsTPoCoverPageDetailDao spsTPoCoverPageDetailDao;

    /**
     * Default constructor
     */
    public SpsTPoCoverPageDetailServiceImpl() {
    }

    /**
     * Set Dao of "spsTPoCoverPageDetail"
     * 
     * @param spsTPoCoverPageDetailDao spsTPoCoverPageDetailDao
     */
    public void setSpsTPoCoverPageDetailDao(SpsTPoCoverPageDetailDao spsTPoCoverPageDetailDao) {
        this.spsTPoCoverPageDetailDao = spsTPoCoverPageDetailDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public SpsTPoCoverPageDetailDomain searchByKey(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public List<SpsTPoCoverPageDetailDomain> searchByCondition(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public List<SpsTPoCoverPageDetailDomain> searchByConditionForPaging(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int searchCount(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTPoCoverPageDetailDomain searchByKeyForChange(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public SpsTPoCoverPageDetailDomain lockByKey(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public SpsTPoCoverPageDetailDomain lockByKeyNoWait(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain)
     */
    public void create(SpsTPoCoverPageDetailDomain domain) throws ApplicationException {
        spsTPoCoverPageDetailDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#create(java.util.List)
     */
    public void create(List<SpsTPoCoverPageDetailDomain> domains) throws ApplicationException {
        for(SpsTPoCoverPageDetailDomain domain : domains) {
            spsTPoCoverPageDetailDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain)
     */
    public int update(SpsTPoCoverPageDetailDomain domain) throws ApplicationException {
        return spsTPoCoverPageDetailDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#update(java.util.List)
     */
    public int update(List<SpsTPoCoverPageDetailDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTPoCoverPageDetailDomain domain : domains) {
            updateCount += spsTPoCoverPageDetailDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTPoCoverPageDetailDomain domain, SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTPoCoverPageDetailDomain> domains, List<SpsTPoCoverPageDetailCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTPoCoverPageDetailDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int delete(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return spsTPoCoverPageDetailDao.deleteByCondition(criteria);
    }

}
