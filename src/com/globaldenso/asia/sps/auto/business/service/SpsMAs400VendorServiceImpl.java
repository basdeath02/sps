/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;


/**
 * A "Service" implementation class of "SpsMAs400Vendor"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400VendorServiceImpl implements SpsMAs400VendorService {

    /**
     * A "Dao" of "spsMAs400Vendor"
     */
    private SpsMAs400VendorDao spsMAs400VendorDao;

    /**
     * Default constructor
     */
    public SpsMAs400VendorServiceImpl() {
    }

    /**
     * Set Dao of "spsMAs400Vendor"
     * 
     * @param spsMAs400VendorDao spsMAs400VendorDao
     */
    public void setSpsMAs400VendorDao(SpsMAs400VendorDao spsMAs400VendorDao) {
        this.spsMAs400VendorDao = spsMAs400VendorDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public SpsMAs400VendorDomain searchByKey(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public List<SpsMAs400VendorDomain> searchByCondition(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public List<SpsMAs400VendorDomain> searchByConditionForPaging(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int searchCount(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    @Deprecated
    public SpsMAs400VendorDomain searchByKeyForChange(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public SpsMAs400VendorDomain lockByKey(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public SpsMAs400VendorDomain lockByKeyNoWait(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain)
     */
    public void create(SpsMAs400VendorDomain domain) throws ApplicationException {
        spsMAs400VendorDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#create(java.util.List)
     */
    public void create(List<SpsMAs400VendorDomain> domains) throws ApplicationException {
        for(SpsMAs400VendorDomain domain : domains) {
            spsMAs400VendorDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain)
     */
    public int update(SpsMAs400VendorDomain domain) throws ApplicationException {
        return spsMAs400VendorDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#update(java.util.List)
     */
    public int update(List<SpsMAs400VendorDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMAs400VendorDomain domain : domains) {
            updateCount += spsMAs400VendorDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int updateByCondition(SpsMAs400VendorDomain domain, SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMAs400VendorDomain> domains, List<SpsMAs400VendorCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMAs400VendorDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int delete(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400VendorService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int deleteByCondition(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400VendorDao.deleteByCondition(criteria);
    }

}
