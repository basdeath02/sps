/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/07/09       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;


/**
 * A "Service" implementation class of "SpsTInvoice"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/07/09 18:24:35<br />
 * 
 * This module generated automatically in 2015/07/09 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceServiceImpl implements SpsTInvoiceService {

    /**
     * A "Dao" of "spsTInvoice"
     */
    private SpsTInvoiceDao spsTInvoiceDao;

    /**
     * Default constructor
     */
    public SpsTInvoiceServiceImpl() {
    }

    /**
     * Set Dao of "spsTInvoice"
     * 
     * @param spsTInvoiceDao spsTInvoiceDao
     */
    public void setSpsTInvoiceDao(SpsTInvoiceDao spsTInvoiceDao) {
        this.spsTInvoiceDao = spsTInvoiceDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public SpsTInvoiceDomain searchByKey(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public List<SpsTInvoiceDomain> searchByCondition(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public List<SpsTInvoiceDomain> searchByConditionForPaging(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int searchCount(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    @Deprecated
    public SpsTInvoiceDomain searchByKeyForChange(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public SpsTInvoiceDomain lockByKey(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public SpsTInvoiceDomain lockByKeyNoWait(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#create(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain)
     */
    public void create(SpsTInvoiceDomain domain) throws ApplicationException {
        spsTInvoiceDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#create(java.util.List)
     */
    public void create(List<SpsTInvoiceDomain> domains) throws ApplicationException {
        for(SpsTInvoiceDomain domain : domains) {
            spsTInvoiceDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#update(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain)
     */
    public int update(SpsTInvoiceDomain domain) throws ApplicationException {
        return spsTInvoiceDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#update(java.util.List)
     */
    public int update(List<SpsTInvoiceDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsTInvoiceDomain domain : domains) {
            updateCount += spsTInvoiceDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int updateByCondition(SpsTInvoiceDomain domain, SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsTInvoiceDomain> domains, List<SpsTInvoiceCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsTInvoiceDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int delete(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int deleteByCondition(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return spsTInvoiceDao.deleteByCondition(criteria);
    }

}
