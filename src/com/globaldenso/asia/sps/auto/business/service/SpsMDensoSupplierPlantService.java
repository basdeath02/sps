/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain;


/**
 * A "Service" interface of "SpsMDensoSupplierPlant"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMDensoSupplierPlantService {

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierPlant"
     * @return List of Domain class of "spsMDensoSupplierPlant"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoSupplierPlantDomain> searchByCondition(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierPlant"
     * @return List of Domain class of "spsMDensoSupplierPlant"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoSupplierPlantDomain> searchByConditionForPaging(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierPlant"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMDensoSupplierPlant"
     * @throws ApplicationException Exception
     */
    public void create(SpsMDensoSupplierPlantDomain domain) throws ApplicationException;

    /**
     * Register some data.
     * 
     * @param domains List of Domain of "spsMDensoSupplierPlant"
     * @throws ApplicationException Exception
     */
    public void create(List<SpsMDensoSupplierPlantDomain> domains) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMDensoSupplierPlant"
     * @param criteria CriteriaDomain class of "spsMDensoSupplierPlant"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMDensoSupplierPlantDomain domain, SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException;

    /**
     * Update some data based on a certain condition.
     * 
     * @param domains List of Domain of "spsMDensoSupplierPlant"
     * @param criteria List of CriteriaDomain of "spsMDensoSupplierPlant"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(List<SpsMDensoSupplierPlantDomain> domains, List<SpsMDensoSupplierPlantCriteriaDomain> criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierPlant"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException;

}
