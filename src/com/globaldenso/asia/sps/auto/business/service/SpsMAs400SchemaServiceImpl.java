/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/12/16       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.service;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain;


/**
 * A "Service" implementation class of "SpsMAs400Schema"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/12/16 12:06:44<br />
 * 
 * This module generated automatically in 2015/12/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400SchemaServiceImpl implements SpsMAs400SchemaService {

    /**
     * A "Dao" of "spsMAs400Schema"
     */
    private SpsMAs400SchemaDao spsMAs400SchemaDao;

    /**
     * Default constructor
     */
    public SpsMAs400SchemaServiceImpl() {
    }

    /**
     * Set Dao of "spsMAs400Schema"
     * 
     * @param spsMAs400SchemaDao spsMAs400SchemaDao
     */
    public void setSpsMAs400SchemaDao(SpsMAs400SchemaDao spsMAs400SchemaDao) {
        this.spsMAs400SchemaDao = spsMAs400SchemaDao;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public SpsMAs400SchemaDomain searchByKey(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.searchByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public List<SpsMAs400SchemaDomain> searchByCondition(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.searchByCondition(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public List<SpsMAs400SchemaDomain> searchByConditionForPaging(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.searchByConditionForPaging(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int searchCount(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.searchCount(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    @Deprecated
    public SpsMAs400SchemaDomain searchByKeyForChange(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.searchByKeyForChange(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public SpsMAs400SchemaDomain lockByKey(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.lockByKey(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public SpsMAs400SchemaDomain lockByKeyNoWait(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.lockByKeyNoWait(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain)
     */
    public void create(SpsMAs400SchemaDomain domain) throws ApplicationException {
        spsMAs400SchemaDao.create(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#create(java.util.List)
     */
    public void create(List<SpsMAs400SchemaDomain> domains) throws ApplicationException {
        for(SpsMAs400SchemaDomain domain : domains) {
            spsMAs400SchemaDao.create(domain);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain)
     */
    public int update(SpsMAs400SchemaDomain domain) throws ApplicationException {
        return spsMAs400SchemaDao.update(domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#update(java.util.List)
     */
    public int update(List<SpsMAs400SchemaDomain> domains) throws ApplicationException {
        int updateCount = 0;
        for(SpsMAs400SchemaDomain domain : domains) {
            updateCount += spsMAs400SchemaDao.update(domain);
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int updateByCondition(SpsMAs400SchemaDomain domain, SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.updateByCondition(domain, criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#updateByCondition(java.util.List, java.util.List)
     */
    public int updateByCondition(List<SpsMAs400SchemaDomain> domains, List<SpsMAs400SchemaCriteriaDomain> criterions) throws ApplicationException {
        int updateCount = 0;
        for(int i = 0; i < domains.size(); i++) {
            updateCount += spsMAs400SchemaDao.updateByCondition(domains.get(i), criterions.get(i));
        }
        return updateCount;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int delete(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.delete(criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.service.SpsMAs400SchemaService#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int deleteByCondition(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return spsMAs400SchemaDao.deleteByCondition(criteria);
    }

}
