/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/07/12       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;
 
 
/**
 * A "Dao" interface of "SpsMDensoSupplierParts"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/07/12 15:00:01<br />
 * 
 * This module generated automatically in 2559/07/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMDensoSupplierPartsDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    public SpsMDensoSupplierPartsDomain searchByKey(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return List of Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoSupplierPartsDomain> searchByCondition(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return List of Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoSupplierPartsDomain> searchByConditionForPaging(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMDensoSupplierParts
     * @return Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMDensoSupplierPartsDomain searchByKeyForChange(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    public SpsMDensoSupplierPartsDomain lockByKey(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    public SpsMDensoSupplierPartsDomain lockByKeyNoWait(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMDensoSupplierParts"
     * @throws ApplicationException Exception
     */
    public void create(SpsMDensoSupplierPartsDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMDensoSupplierParts"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMDensoSupplierPartsDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMDensoSupplierParts"
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMDensoSupplierPartsDomain domain, SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierParts"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException;

}
