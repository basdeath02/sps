/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/29       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;
 
 
/**
 * A "Dao" interface of "SpsMCompanySupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/29 18:51:48<br />
 * 
 * This module generated automatically in 2014/10/29 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMCompanySupplierDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    public SpsMCompanySupplierDomain searchByKey(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return List of Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    public List<SpsMCompanySupplierDomain> searchByCondition(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return List of Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    public List<SpsMCompanySupplierDomain> searchByConditionForPaging(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMCompanySupplier
     * @return Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMCompanySupplierDomain searchByKeyForChange(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    public SpsMCompanySupplierDomain lockByKey(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    public SpsMCompanySupplierDomain lockByKeyNoWait(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMCompanySupplier"
     * @throws ApplicationException Exception
     */
    public void create(SpsMCompanySupplierDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMCompanySupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMCompanySupplierDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMCompanySupplier"
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMCompanySupplierDomain domain, SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMCompanySupplier"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException;

}
