/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/04/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;
 
 
/**
 * A "Dao" interface of "SpsTInvoiceDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/04/28 15:59:11<br />
 * 
 * This module generated automatically in 2015/04/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTInvoiceDetailDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    public SpsTInvoiceDetailDomain searchByKey(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return List of Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTInvoiceDetailDomain> searchByCondition(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return List of Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTInvoiceDetailDomain> searchByConditionForPaging(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTInvoiceDetail
     * @return Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTInvoiceDetailDomain searchByKeyForChange(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    public SpsTInvoiceDetailDomain lockByKey(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    public SpsTInvoiceDetailDomain lockByKeyNoWait(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTInvoiceDetail"
     * @throws ApplicationException Exception
     */
    public void create(SpsTInvoiceDetailDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTInvoiceDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTInvoiceDetailDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTInvoiceDetail"
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTInvoiceDetailDomain domain, SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTInvoiceDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException;

}
