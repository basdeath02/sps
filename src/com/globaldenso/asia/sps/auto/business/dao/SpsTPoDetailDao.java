/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/31       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;
 
 
/**
 * A "Dao" interface of "SpsTPoDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/31 15:36:38<br />
 * 
 * This module generated automatically in 2015/03/31 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTPoDetailDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    public SpsTPoDetailDomain searchByKey(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return List of Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoDetailDomain> searchByCondition(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return List of Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoDetailDomain> searchByConditionForPaging(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTPoDetail
     * @return Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTPoDetailDomain searchByKeyForChange(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    public SpsTPoDetailDomain lockByKey(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    public SpsTPoDetailDomain lockByKeyNoWait(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTPoDetail"
     * @throws ApplicationException Exception
     */
    public void create(SpsTPoDetailDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTPoDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTPoDetailDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTPoDetail"
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTPoDetailDomain domain, SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPoDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException;

}
