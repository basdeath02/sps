/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/07/12       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import java.math.BigDecimal;


/**
 * A "Dao" interface of "DoIdSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/07/12 15:00:01<br />
 * 
 * This module generated automatically in 2559/07/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface DoIdSeqDao {

    /**
     * Get the number of next sequence of "doIdSeq".
     *
     * @return the number of next sequence of "doIdSeq"
     * @throws ApplicationException Exception
     */
    public BigDecimal getNextValue() throws ApplicationException;

}
