/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/11/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain;
 
 
/**
 * A "Dao" interface of "SpsTPoCoverPage"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/11/28 09:54:32<br />
 * 
 * This module generated automatically in 2014/11/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTPoCoverPageDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    public SpsTPoCoverPageDomain searchByKey(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return List of Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoCoverPageDomain> searchByCondition(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return List of Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoCoverPageDomain> searchByConditionForPaging(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTPoCoverPage
     * @return Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTPoCoverPageDomain searchByKeyForChange(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    public SpsTPoCoverPageDomain lockByKey(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    public SpsTPoCoverPageDomain lockByKeyNoWait(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTPoCoverPage"
     * @throws ApplicationException Exception
     */
    public void create(SpsTPoCoverPageDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTPoCoverPage"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTPoCoverPageDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTPoCoverPage"
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTPoCoverPageDomain domain, SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPage"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException;

}
