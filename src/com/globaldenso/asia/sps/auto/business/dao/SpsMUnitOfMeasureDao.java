/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/01/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain;
 
 
/**
 * A "Dao" interface of "SpsMUnitOfMeasure"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/01/05 09:25:07<br />
 * 
 * This module generated automatically in 2016/01/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMUnitOfMeasureDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    public SpsMUnitOfMeasureDomain searchByKey(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return List of Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    public List<SpsMUnitOfMeasureDomain> searchByCondition(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return List of Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    public List<SpsMUnitOfMeasureDomain> searchByConditionForPaging(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMUnitOfMeasure
     * @return Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMUnitOfMeasureDomain searchByKeyForChange(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    public SpsMUnitOfMeasureDomain lockByKey(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    public SpsMUnitOfMeasureDomain lockByKeyNoWait(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMUnitOfMeasure"
     * @throws ApplicationException Exception
     */
    public void create(SpsMUnitOfMeasureDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMUnitOfMeasure"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMUnitOfMeasureDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMUnitOfMeasure"
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMUnitOfMeasureDomain domain, SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUnitOfMeasure"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException;

}
