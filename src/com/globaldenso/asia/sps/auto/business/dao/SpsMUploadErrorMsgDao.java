/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain;
 
 
/**
 * A "Dao" interface of "SpsMUploadErrorMsg"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMUploadErrorMsgDao {

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUploadErrorMsg"
     * @return List of Domain class of "spsMUploadErrorMsg"
     * @throws ApplicationException Exception
     */
    public List<SpsMUploadErrorMsgDomain> searchByCondition(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUploadErrorMsg"
     * @return List of Domain class of "spsMUploadErrorMsg"
     * @throws ApplicationException Exception
     */
    public List<SpsMUploadErrorMsgDomain> searchByConditionForPaging(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMUploadErrorMsg"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMUploadErrorMsg"
     * @throws ApplicationException Exception
     */
    public void create(SpsMUploadErrorMsgDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMUploadErrorMsg"
     * @param criteria CriteriaDomain class of "spsMUploadErrorMsg"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMUploadErrorMsgDomain domain, SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUploadErrorMsg"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException;

}
