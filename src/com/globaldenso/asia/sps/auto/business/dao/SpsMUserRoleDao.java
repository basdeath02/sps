/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;
 
 
/**
 * A "Dao" interface of "SpsMUserRole"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMUserRoleDao {

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUserRole"
     * @return List of Domain class of "spsMUserRole"
     * @throws ApplicationException Exception
     */
    public List<SpsMUserRoleDomain> searchByCondition(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUserRole"
     * @return List of Domain class of "spsMUserRole"
     * @throws ApplicationException Exception
     */
    public List<SpsMUserRoleDomain> searchByConditionForPaging(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMUserRole"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMUserRole"
     * @throws ApplicationException Exception
     */
    public void create(SpsMUserRoleDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMUserRole"
     * @param criteria CriteriaDomain class of "spsMUserRole"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMUserRoleDomain domain, SpsMUserRoleCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUserRole"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException;

}
