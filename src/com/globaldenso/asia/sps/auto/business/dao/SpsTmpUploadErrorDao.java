/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/04/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain;
 
 
/**
 * A "Dao" interface of "SpsTmpUploadError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/04/07 12:25:27<br />
 * 
 * This module generated automatically in 2559/04/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTmpUploadErrorDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    public SpsTmpUploadErrorDomain searchByKey(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return List of Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUploadErrorDomain> searchByCondition(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return List of Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    public List<SpsTmpUploadErrorDomain> searchByConditionForPaging(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTmpUploadError
     * @return Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTmpUploadErrorDomain searchByKeyForChange(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    public SpsTmpUploadErrorDomain lockByKey(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    public SpsTmpUploadErrorDomain lockByKeyNoWait(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTmpUploadError"
     * @throws ApplicationException Exception
     */
    public void create(SpsTmpUploadErrorDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTmpUploadError"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTmpUploadErrorDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTmpUploadError"
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTmpUploadErrorDomain domain, SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTmpUploadError"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException;

}
