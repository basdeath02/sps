/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
 
 
/**
 * A "Dao" interface of "SpsTDoDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTDoDetailDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    public SpsTDoDetailDomain searchByKey(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return List of Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTDoDetailDomain> searchByCondition(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return List of Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTDoDetailDomain> searchByConditionForPaging(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTDoDetail
     * @return Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTDoDetailDomain searchByKeyForChange(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    public SpsTDoDetailDomain lockByKey(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    public SpsTDoDetailDomain lockByKeyNoWait(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTDoDetail"
     * @throws ApplicationException Exception
     */
    public void create(SpsTDoDetailDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTDoDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTDoDetailDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTDoDetail"
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTDoDetailDomain domain, SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTDoDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException;

}
