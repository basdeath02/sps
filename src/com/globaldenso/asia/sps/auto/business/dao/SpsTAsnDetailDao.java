/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
 
 
/**
 * A "Dao" interface of "SpsTAsnDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTAsnDetailDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    public SpsTAsnDetailDomain searchByKey(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return List of Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTAsnDetailDomain> searchByCondition(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return List of Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTAsnDetailDomain> searchByConditionForPaging(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTAsnDetail
     * @return Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTAsnDetailDomain searchByKeyForChange(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    public SpsTAsnDetailDomain lockByKey(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    public SpsTAsnDetailDomain lockByKeyNoWait(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTAsnDetail"
     * @throws ApplicationException Exception
     */
    public void create(SpsTAsnDetailDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTAsnDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTAsnDetailDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTAsnDetail"
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTAsnDetailDomain domain, SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTAsnDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException;

}
