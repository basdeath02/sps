/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
 
 
/**
 * A "Dao" interface of "SpsMUser"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMUserDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    public SpsMUserDomain searchByKey(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return List of Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    public List<SpsMUserDomain> searchByCondition(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return List of Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    public List<SpsMUserDomain> searchByConditionForPaging(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMUser
     * @return Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMUserDomain searchByKeyForChange(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    public SpsMUserDomain lockByKey(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    public SpsMUserDomain lockByKeyNoWait(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMUser"
     * @throws ApplicationException Exception
     */
    public void create(SpsMUserDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMUser"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMUserDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMUser"
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMUserDomain domain, SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMUserCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMUser"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMUserCriteriaDomain criteria) throws ApplicationException;

}
