/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain;
 
 
/**
 * A "Dao" interface of "SpsMDensoSupplierRelation"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsMDensoSupplierRelationDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    public SpsMDensoSupplierRelationDomain searchByKey(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return List of Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoSupplierRelationDomain> searchByCondition(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return List of Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    public List<SpsMDensoSupplierRelationDomain> searchByConditionForPaging(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsMDensoSupplierRelation
     * @return Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsMDensoSupplierRelationDomain searchByKeyForChange(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    public SpsMDensoSupplierRelationDomain lockByKey(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    public SpsMDensoSupplierRelationDomain lockByKeyNoWait(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsMDensoSupplierRelation"
     * @throws ApplicationException Exception
     */
    public void create(SpsMDensoSupplierRelationDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsMDensoSupplierRelation"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsMDensoSupplierRelationDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsMDensoSupplierRelation"
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsMDensoSupplierRelationDomain domain, SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsMDensoSupplierRelation"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException;

}
