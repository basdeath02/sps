/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.business.dao;

import java.util.List;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain;
 
 
/**
 * A "Dao" interface of "SpsTPoCoverPageDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public interface SpsTPoCoverPageDetailDao {

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    public SpsTPoCoverPageDetailDomain searchByKey(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return List of Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoCoverPageDetailDomain> searchByCondition(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * <<for paging process>>
     * Search table data based on a certain condition and return the search result in a list form.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return List of Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    public List<SpsTPoCoverPageDetailDomain> searchByConditionForPaging(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search table data based on a certain condition and return the number of search results.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return int The number of search results
     * @throws ApplicationException Exception
     */
    public int searchCount(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * Use this SQL when obtaining a record before update processing.
     * 
     * Basically, it is the same function as "searchByKey".
     * </pre>
     * 
     * @param criteria CriteriaDomain class of spsTPoCoverPageDetail
     * @return Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    @Deprecated
    public SpsTPoCoverPageDetailDomain searchByKeyForChange(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Obtain the lock according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, wait until the lock can be obtained.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    public SpsTPoCoverPageDetailDomain lockByKey(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Search data according to the main key as the condition and return the search result.
     * 
     * <pre>
     * If the lock can not be obtained, an exception occurs.
     * </pre>
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    public SpsTPoCoverPageDetailDomain lockByKeyNoWait(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Register data.
     * 
     * @param domain Domain class of "spsTPoCoverPageDetail"
     * @throws ApplicationException Exception
     */
    public void create(SpsTPoCoverPageDetailDomain domain) throws ApplicationException;

    /**
     * Update data according to the main key as the condition.
     * 
     * @param domain Domain class of "spsTPoCoverPageDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int update(SpsTPoCoverPageDetailDomain domain) throws ApplicationException;

    /**
     * Update data based on a certain condition.
     * 
     * @param domain Domain class of "spsTPoCoverPageDetail"
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return The number of updated data
     * @throws ApplicationException Exception
     */
    public int updateByCondition(SpsTPoCoverPageDetailDomain domain, SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete data physically according to the main key as the condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int delete(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

    /**
     * Delete a data physically based on a certain condition.
     * 
     * @param criteria CriteriaDomain class of "spsTPoCoverPageDetail"
     * @return The number of deleted data
     * @throws ApplicationException Exception
     */
    public int deleteByCondition(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException;

}
