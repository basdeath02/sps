/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;


/**
 * A "Dao" implementation class of "SpsTAsn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnDaoImpl extends SqlMapClientDaoSupport implements SpsTAsnDao {

    /**
     * Default constructor
     */
    public SpsTAsnDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public SpsTAsnDomain searchByKey(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsn.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTAsnDomain> searchByCondition(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTAsnDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsn.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTAsnDomain> searchByConditionForPaging(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTAsnDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsn.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int searchCount(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsn.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    @Deprecated
    public SpsTAsnDomain searchByKeyForChange(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsn.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public SpsTAsnDomain lockByKey(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsn.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public SpsTAsnDomain lockByKeyNoWait(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTAsn.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain)
     */
    public void create(SpsTAsnDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTAsn.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain)
     */
    public int update(SpsTAsnDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTAsn.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int updateByCondition(SpsTAsnDomain domain, SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTAsn.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int delete(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTAsn.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnCriteriaDomain)
     */
    public int deleteByCondition(SpsTAsnCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTAsn.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
