/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain;


/**
 * A "Dao" implementation class of "SpsMRoleScreen"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleScreenDaoImpl extends SqlMapClientDaoSupport implements SpsMRoleScreenDao {

    /**
     * Default constructor
     */
    public SpsMRoleScreenDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public SpsMRoleScreenDomain searchByKey(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleScreenDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleScreen.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleScreenDomain> searchByCondition(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleScreenDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRoleScreen.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleScreenDomain> searchByConditionForPaging(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleScreenDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRoleScreen.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int searchCount(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleScreen.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleScreenDomain searchByKeyForChange(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleScreenDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleScreen.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public SpsMRoleScreenDomain lockByKey(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleScreenDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleScreen.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public SpsMRoleScreenDomain lockByKeyNoWait(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleScreenDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMRoleScreen.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain)
     */
    public void create(SpsMRoleScreenDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMRoleScreen.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain)
     */
    public int update(SpsMRoleScreenDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMRoleScreen.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleScreenDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleScreenDomain domain, SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMRoleScreen.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int delete(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRoleScreen.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleScreenDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleScreenCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleScreenCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRoleScreen.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
