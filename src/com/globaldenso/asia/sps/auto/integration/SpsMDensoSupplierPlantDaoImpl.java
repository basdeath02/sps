/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain;


/**
 * A "Dao" implementation class of "SpsMDensoSupplierPlant"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierPlantDaoImpl extends SqlMapClientDaoSupport implements SpsMDensoSupplierPlantDao {

    /**
     * Default constructor
     */
    public SpsMDensoSupplierPlantDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMDensoSupplierPlantDomain> searchByCondition(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMDensoSupplierPlantDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMDensoSupplierPlant.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMDensoSupplierPlantDomain> searchByConditionForPaging(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMDensoSupplierPlantDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMDensoSupplierPlant.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public int searchCount(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierPlant.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain)
     */
    public void create(SpsMDensoSupplierPlantDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMDensoSupplierPlant.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPlantDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public int updateByCondition(SpsMDensoSupplierPlantDomain domain, SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMDensoSupplierPlant.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPlantDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPlantCriteriaDomain)
     */
    public int deleteByCondition(SpsMDensoSupplierPlantCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMDensoSupplierPlant.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
