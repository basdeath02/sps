/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/13       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;


/**
 * A "Dao" implementation class of "SpsTFileUpload"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/13 11:02:03<br />
 * 
 * This module generated automatically in 2014/10/13 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTFileUploadDaoImpl extends SqlMapClientDaoSupport implements SpsTFileUploadDao {

    /**
     * Default constructor
     */
    public SpsTFileUploadDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public SpsTFileUploadDomain searchByKey(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return (SpsTFileUploadDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTFileUpload.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTFileUploadDomain> searchByCondition(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTFileUploadDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTFileUpload.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTFileUploadDomain> searchByConditionForPaging(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTFileUploadDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTFileUpload.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int searchCount(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTFileUpload.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    @Deprecated
    public SpsTFileUploadDomain searchByKeyForChange(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return (SpsTFileUploadDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTFileUpload.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public SpsTFileUploadDomain lockByKey(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return (SpsTFileUploadDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTFileUpload.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public SpsTFileUploadDomain lockByKeyNoWait(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return (SpsTFileUploadDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTFileUpload.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain)
     */
    public void create(SpsTFileUploadDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTFileUpload.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain)
     */
    public int update(SpsTFileUploadDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTFileUpload.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int updateByCondition(SpsTFileUploadDomain domain, SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTFileUpload.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int delete(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTFileUpload.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTFileUploadDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTFileUploadCriteriaDomain)
     */
    public int deleteByCondition(SpsTFileUploadCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTFileUpload.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
