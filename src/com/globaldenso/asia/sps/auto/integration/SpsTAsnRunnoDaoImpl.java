/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain;


/**
 * A "Dao" implementation class of "SpsTAsnRunno"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnRunnoDaoImpl extends SqlMapClientDaoSupport implements SpsTAsnRunnoDao {

    /**
     * Default constructor
     */
    public SpsTAsnRunnoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public SpsTAsnRunnoDomain searchByKey(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnRunnoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnRunno.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTAsnRunnoDomain> searchByCondition(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTAsnRunnoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsnRunno.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTAsnRunnoDomain> searchByConditionForPaging(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTAsnRunnoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsnRunno.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int searchCount(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnRunno.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    @Deprecated
    public SpsTAsnRunnoDomain searchByKeyForChange(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnRunnoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnRunno.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public SpsTAsnRunnoDomain lockByKey(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnRunnoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnRunno.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public SpsTAsnRunnoDomain lockByKeyNoWait(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnRunnoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTAsnRunno.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain)
     */
    public void create(SpsTAsnRunnoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTAsnRunno.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain)
     */
    public int update(SpsTAsnRunnoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTAsnRunno.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnRunnoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int updateByCondition(SpsTAsnRunnoDomain domain, SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTAsnRunno.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int delete(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTAsnRunno.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnRunnoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnRunnoCriteriaDomain)
     */
    public int deleteByCondition(SpsTAsnRunnoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTAsnRunno.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
