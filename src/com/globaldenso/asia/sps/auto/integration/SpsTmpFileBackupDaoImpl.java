/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain;


/**
 * A "Dao" implementation class of "SpsTmpFileBackup"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpFileBackupDaoImpl extends SqlMapClientDaoSupport implements SpsTmpFileBackupDao {

    /**
     * Default constructor
     */
    public SpsTmpFileBackupDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public SpsTmpFileBackupDomain searchByKey(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpFileBackupDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpFileBackup.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpFileBackupDomain> searchByCondition(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpFileBackupDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpFileBackup.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpFileBackupDomain> searchByConditionForPaging(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpFileBackupDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpFileBackup.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int searchCount(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpFileBackup.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    @Deprecated
    public SpsTmpFileBackupDomain searchByKeyForChange(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpFileBackupDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpFileBackup.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public SpsTmpFileBackupDomain lockByKey(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpFileBackupDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpFileBackup.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public SpsTmpFileBackupDomain lockByKeyNoWait(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpFileBackupDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpFileBackup.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain)
     */
    public void create(SpsTmpFileBackupDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpFileBackup.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain)
     */
    public int update(SpsTmpFileBackupDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpFileBackup.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpFileBackupDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int updateByCondition(SpsTmpFileBackupDomain domain, SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpFileBackup.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int delete(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpFileBackup.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpFileBackupDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpFileBackupCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpFileBackupCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpFileBackup.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
