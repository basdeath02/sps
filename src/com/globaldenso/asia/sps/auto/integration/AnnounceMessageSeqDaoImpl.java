/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import com.globaldenso.asia.sps.auto.business.dao.AnnounceMessageSeqDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import java.math.BigDecimal;


/**
 * A "Dao" implementation class of "AnnounceMessageSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class AnnounceMessageSeqDaoImpl extends SqlMapClientDaoSupport implements AnnounceMessageSeqDao {

    /**
     * Default constructor
     */
    public AnnounceMessageSeqDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.AnnounceMessageSeqDao#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return (BigDecimal)getSqlMapClientTemplate().queryForObject("AnnounceMessageSeq.GetNextValue");
    }

}
