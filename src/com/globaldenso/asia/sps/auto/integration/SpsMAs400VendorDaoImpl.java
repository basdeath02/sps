/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;


/**
 * A "Dao" implementation class of "SpsMAs400Vendor"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400VendorDaoImpl extends SqlMapClientDaoSupport implements SpsMAs400VendorDao {

    /**
     * Default constructor
     */
    public SpsMAs400VendorDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public SpsMAs400VendorDomain searchByKey(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400VendorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Vendor.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400VendorDomain> searchByCondition(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400VendorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAs400Vendor.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400VendorDomain> searchByConditionForPaging(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400VendorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAs400Vendor.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int searchCount(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Vendor.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    @Deprecated
    public SpsMAs400VendorDomain searchByKeyForChange(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400VendorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Vendor.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public SpsMAs400VendorDomain lockByKey(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400VendorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Vendor.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public SpsMAs400VendorDomain lockByKeyNoWait(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400VendorDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMAs400Vendor.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain)
     */
    public void create(SpsMAs400VendorDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMAs400Vendor.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain)
     */
    public int update(SpsMAs400VendorDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMAs400Vendor.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int updateByCondition(SpsMAs400VendorDomain domain, SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMAs400Vendor.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int delete(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAs400Vendor.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    public int deleteByCondition(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAs400Vendor.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
