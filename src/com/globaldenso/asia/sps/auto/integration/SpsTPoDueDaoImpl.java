/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/16       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;


/**
 * A "Dao" implementation class of "SpsTPoDue"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/16 14:33:35<br />
 * 
 * This module generated automatically in 2016/02/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDueDaoImpl extends SqlMapClientDaoSupport implements SpsTPoDueDao {

    /**
     * Default constructor
     */
    public SpsTPoDueDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public SpsTPoDueDomain searchByKey(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDueDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDue.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoDueDomain> searchByCondition(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoDueDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoDue.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoDueDomain> searchByConditionForPaging(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoDueDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoDue.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int searchCount(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDue.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    @Deprecated
    public SpsTPoDueDomain searchByKeyForChange(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDueDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDue.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public SpsTPoDueDomain lockByKey(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDueDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDue.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public SpsTPoDueDomain lockByKeyNoWait(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDueDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTPoDue.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain)
     */
    public void create(SpsTPoDueDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTPoDue.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain)
     */
    public int update(SpsTPoDueDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTPoDue.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int updateByCondition(SpsTPoDueDomain domain, SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTPoDue.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int delete(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoDue.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDueDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDueCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoDueCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoDue.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
