/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain;


/**
 * A "Dao" implementation class of "SpsTPoCoverPageDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageDetailDaoImpl extends SqlMapClientDaoSupport implements SpsTPoCoverPageDetailDao {

    /**
     * Default constructor
     */
    public SpsTPoCoverPageDetailDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public SpsTPoCoverPageDetailDomain searchByKey(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPageDetail.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoCoverPageDetailDomain> searchByCondition(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoCoverPageDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoCoverPageDetail.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoCoverPageDetailDomain> searchByConditionForPaging(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoCoverPageDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoCoverPageDetail.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int searchCount(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPageDetail.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTPoCoverPageDetailDomain searchByKeyForChange(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPageDetail.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public SpsTPoCoverPageDetailDomain lockByKey(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPageDetail.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public SpsTPoCoverPageDetailDomain lockByKeyNoWait(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDetailDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTPoCoverPageDetail.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain)
     */
    public void create(SpsTPoCoverPageDetailDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTPoCoverPageDetail.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain)
     */
    public int update(SpsTPoCoverPageDetailDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTPoCoverPageDetail.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTPoCoverPageDetailDomain domain, SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTPoCoverPageDetail.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int delete(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoCoverPageDetail.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDetailDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoCoverPageDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoCoverPageDetail.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
