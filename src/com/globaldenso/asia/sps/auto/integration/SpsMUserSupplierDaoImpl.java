/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/27       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;


/**
 * A "Dao" implementation class of "SpsMUserSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/27 17:41:40<br />
 * 
 * This module generated automatically in 2015/03/27 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserSupplierDaoImpl extends SqlMapClientDaoSupport implements SpsMUserSupplierDao {

    /**
     * Default constructor
     */
    public SpsMUserSupplierDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public SpsMUserSupplierDomain searchByKey(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserSupplier.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserSupplierDomain> searchByCondition(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserSupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUserSupplier.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserSupplierDomain> searchByConditionForPaging(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserSupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUserSupplier.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int searchCount(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserSupplier.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    @Deprecated
    public SpsMUserSupplierDomain searchByKeyForChange(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserSupplier.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public SpsMUserSupplierDomain lockByKey(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserSupplier.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public SpsMUserSupplierDomain lockByKeyNoWait(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserSupplierDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMUserSupplier.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain)
     */
    public void create(SpsMUserSupplierDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMUserSupplier.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain)
     */
    public int update(SpsMUserSupplierDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMUserSupplier.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int updateByCondition(SpsMUserSupplierDomain domain, SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMUserSupplier.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int delete(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUserSupplier.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserSupplierDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserSupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUserSupplier.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
