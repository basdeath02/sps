/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain;


/**
 * A "Dao" implementation class of "SpsMDensoSupplierRelation"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierRelationDaoImpl extends SqlMapClientDaoSupport implements SpsMDensoSupplierRelationDao {

    /**
     * Default constructor
     */
    public SpsMDensoSupplierRelationDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public SpsMDensoSupplierRelationDomain searchByKey(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierRelationDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierRelation.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMDensoSupplierRelationDomain> searchByCondition(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMDensoSupplierRelationDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMDensoSupplierRelation.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMDensoSupplierRelationDomain> searchByConditionForPaging(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMDensoSupplierRelationDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMDensoSupplierRelation.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int searchCount(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierRelation.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    @Deprecated
    public SpsMDensoSupplierRelationDomain searchByKeyForChange(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierRelationDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierRelation.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public SpsMDensoSupplierRelationDomain lockByKey(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierRelationDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierRelation.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public SpsMDensoSupplierRelationDomain lockByKeyNoWait(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierRelationDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMDensoSupplierRelation.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain)
     */
    public void create(SpsMDensoSupplierRelationDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMDensoSupplierRelation.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain)
     */
    public int update(SpsMDensoSupplierRelationDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMDensoSupplierRelation.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierRelationDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int updateByCondition(SpsMDensoSupplierRelationDomain domain, SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMDensoSupplierRelation.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int delete(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMDensoSupplierRelation.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierRelationDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierRelationCriteriaDomain)
     */
    public int deleteByCondition(SpsMDensoSupplierRelationCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMDensoSupplierRelation.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
