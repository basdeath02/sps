/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/31       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain;


/**
 * A "Dao" implementation class of "SpsTPoDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/31 15:36:38<br />
 * 
 * This module generated automatically in 2015/03/31 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDetailDaoImpl extends SqlMapClientDaoSupport implements SpsTPoDetailDao {

    /**
     * Default constructor
     */
    public SpsTPoDetailDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public SpsTPoDetailDomain searchByKey(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDetail.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoDetailDomain> searchByCondition(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoDetail.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoDetailDomain> searchByConditionForPaging(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoDetail.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int searchCount(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDetail.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTPoDetailDomain searchByKeyForChange(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDetail.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public SpsTPoDetailDomain lockByKey(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoDetail.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public SpsTPoDetailDomain lockByKeyNoWait(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDetailDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTPoDetail.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain)
     */
    public void create(SpsTPoDetailDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTPoDetail.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain)
     */
    public int update(SpsTPoDetailDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTPoDetail.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTPoDetailDomain domain, SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTPoDetail.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int delete(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoDetail.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDetailDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoDetail.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
