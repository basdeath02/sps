/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/12/16       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain;


/**
 * A "Dao" implementation class of "SpsMAs400Schema"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/12/16 12:06:44<br />
 * 
 * This module generated automatically in 2015/12/16 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400SchemaDaoImpl extends SqlMapClientDaoSupport implements SpsMAs400SchemaDao {

    /**
     * Default constructor
     */
    public SpsMAs400SchemaDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public SpsMAs400SchemaDomain searchByKey(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400SchemaDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Schema.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400SchemaDomain> searchByCondition(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400SchemaDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAs400Schema.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400SchemaDomain> searchByConditionForPaging(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400SchemaDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAs400Schema.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int searchCount(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Schema.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    @Deprecated
    public SpsMAs400SchemaDomain searchByKeyForChange(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400SchemaDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Schema.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public SpsMAs400SchemaDomain lockByKey(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400SchemaDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Schema.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public SpsMAs400SchemaDomain lockByKeyNoWait(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400SchemaDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMAs400Schema.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain)
     */
    public void create(SpsMAs400SchemaDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMAs400Schema.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain)
     */
    public int update(SpsMAs400SchemaDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMAs400Schema.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400SchemaDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int updateByCondition(SpsMAs400SchemaDomain domain, SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMAs400Schema.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int delete(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAs400Schema.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400SchemaDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400SchemaCriteriaDomain)
     */
    public int deleteByCondition(SpsMAs400SchemaCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAs400Schema.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
