/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;


/**
 * A "Dao" implementation class of "SpsMMenu"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMMenuDaoImpl extends SqlMapClientDaoSupport implements SpsMMenuDao {

    /**
     * Default constructor
     */
    public SpsMMenuDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public SpsMMenuDomain searchByKey(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMenuDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMMenu.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMMenuDomain> searchByCondition(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMMenuDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMMenu.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMMenuDomain> searchByConditionForPaging(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMMenuDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMMenu.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int searchCount(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMMenu.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    @Deprecated
    public SpsMMenuDomain searchByKeyForChange(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMenuDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMMenu.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public SpsMMenuDomain lockByKey(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMenuDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMMenu.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public SpsMMenuDomain lockByKeyNoWait(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMenuDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMMenu.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain)
     */
    public void create(SpsMMenuDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMMenu.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain)
     */
    public int update(SpsMMenuDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMMenu.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int updateByCondition(SpsMMenuDomain domain, SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMMenu.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int delete(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMMenu.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMenuDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMenuCriteriaDomain)
     */
    public int deleteByCondition(SpsMMenuCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMMenu.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
