/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;


/**
 * A "Dao" implementation class of "SpsMPlantSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMPlantSupplierDaoImpl extends SqlMapClientDaoSupport implements SpsMPlantSupplierDao {

    /**
     * Default constructor
     */
    public SpsMPlantSupplierDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public SpsMPlantSupplierDomain searchByKey(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantSupplier.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMPlantSupplierDomain> searchByCondition(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMPlantSupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMPlantSupplier.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMPlantSupplierDomain> searchByConditionForPaging(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMPlantSupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMPlantSupplier.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int searchCount(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantSupplier.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    @Deprecated
    public SpsMPlantSupplierDomain searchByKeyForChange(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantSupplier.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public SpsMPlantSupplierDomain lockByKey(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantSupplier.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public SpsMPlantSupplierDomain lockByKeyNoWait(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantSupplierDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMPlantSupplier.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain)
     */
    public void create(SpsMPlantSupplierDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMPlantSupplier.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain)
     */
    public int update(SpsMPlantSupplierDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMPlantSupplier.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int updateByCondition(SpsMPlantSupplierDomain domain, SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMPlantSupplier.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int delete(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMPlantSupplier.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMPlantSupplier.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
