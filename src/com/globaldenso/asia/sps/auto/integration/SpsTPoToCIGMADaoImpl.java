/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoToCIGMADao;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoDueDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * A "Dao" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table
 * definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoToCIGMADaoImpl extends SqlMapClientDaoSupport implements
		SpsTPoToCIGMADao {

	/**
	 * Default constructor
	 */
	public SpsTPoToCIGMADaoImpl() {
	}



	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
	 */
	@SuppressWarnings("unchecked")
	public List<SpsMDensoDensoRelationWithPODNDomain> searchByCondition(
			SpsMDensoDensoRelationWithPODNDomain criteria)
			throws ApplicationException {

		List<SpsMDensoDensoRelationWithPODNDomain> result = new ArrayList<SpsMDensoDensoRelationWithPODNDomain>();

		//result = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplier", criteria);
		result = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplierGroup", criteria);
		if (result != null) {
			if (result.size() > 0) {
				for (int i = 0; i < result.size(); i++) {
					List<SPSTPoHeaderDNDomain> listPO = new ArrayList<SPSTPoHeaderDNDomain>();
					listPO = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchPO", result.get(i));
					
					if (listPO != null) {
						if (listPO.size() > 0) {
							for (int j = 0; j < listPO.size(); j++) {
							  //check is in DN-Dn Relate
                                String msgError = "";
                                
                                SpsMDensoDensoRelationWithPODNDomain dataCom = new SpsMDensoDensoRelationWithPODNDomain();
                                dataCom.setSchemaCd(result.get(i).getSchemaCd());
                                dataCom.setdCd(listPO.get(j).getdCD());
                                dataCom.setOther(criteria.getdCd());
                                
                                List<SpsMDensoDensoRelationWithPODNDomain> selectListCom = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplierBySchema", dataCom);
                                if(selectListCom==null||selectListCom.size()==0){
                                    //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
                                    msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M01;
                                }else{
                                    dataCom.setdPcd(listPO.get(j).getdPCD());
                                    selectListCom = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplierBySchema", dataCom);
                                    if(selectListCom==null||selectListCom.size()==0){
                                        //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                                        msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M02;
                                    }else{
                                        dataCom.setsCd(listPO.get(j).getsCD());
                                        selectListCom = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplierBySchema", dataCom);
                                        if(selectListCom==null||selectListCom.size()==0){
                                            msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M03;
                                        }else{
                                            dataCom.setsPcd(listPO.get(j).getsPCD());
                                            selectListCom = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplierBySchema", dataCom);
                                            if(selectListCom==null||selectListCom.size()==0){
                                                msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M04;
                                            }
                                        }
                                    }
                                }
                                if(!msgError.equals("")){
                                    listPO.get(j).setMsgError(msgError);
                                    listPO.get(j).setDnDnTrnFlg("9");
                                }else{
                                    
                                    SpsMDensoDensoRelationWithPODNDomain data = new SpsMDensoDensoRelationWithPODNDomain();
                                    data.setSchemaCd(result.get(i).getSchemaCd());
                                    data.setdCd(listPO.get(j).getdCD());
                                    data.setdPcd(listPO.get(j).getdPCD());
                                    data.setsCd(listPO.get(j).getsCD());
                                    data.setsPcd(listPO.get(j).getsPCD());
                                    data.setVendorCd(listPO.get(j).getVendorCD());
                                    data.setOther(criteria.getdCd());
                                    
                                    List<SpsMDensoDensoRelationWithPODNDomain> selectList = getSqlMapClientTemplate().queryForList("SpsTPoToCIGMA.SearchSupplierBySchema", data);
                                    
                                    if(selectList.size()>0){
                                        listPO.get(j).setCusNo(selectList.get(0).getCusNo());
                                        listPO.get(j).setShipNo(selectList.get(0).getShipNo());
                                        //listPO.get(j).setVendorCD(selectList.get(0).getVendorCd());
                                    }
                                    
                                    List<SPSTPoDetailDNDomain> listPODet = new ArrayList<SPSTPoDetailDNDomain>();
                                    List<SPSTPoDueDNDomain> listPODue = new ArrayList<SPSTPoDueDNDomain>();

                                    listPODet = getSqlMapClientTemplate()
                                            .queryForList(
                                                    "SpsTPoToCIGMA.SearchPODet",
                                                    listPO.get(j));

                                    listPODue = getSqlMapClientTemplate()
                                            .queryForList(
                                                    "SpsTPoToCIGMA.SearchPODue",
                                                    listPO.get(j));

                                    if (listPODet != null) {
                                        if (listPODet.size() > 0) {
                                            listPO.get(j).setListPoDet(listPODet);
                                        }
                                    }
                                    
                                    if (listPODue != null) {
                                        if (listPODue.size() > 0) {
                                            listPO.get(j).setListPoDue(listPODue);
                                        }
                                    }
                                }
                                
							}
							result.get(i).setListPO(listPO);

						}
					}
				}
			}
		}

		return result;
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
	 */
	public boolean UpdatePoTranFlag(SPSTPoHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		int updateSuccess =getSqlMapClientTemplate().update("SpsTPoToCIGMA.UpdatePOFlag", criteria);
		if(updateSuccess >0){
			
			result =true;
		}
		return result;
	}

}
