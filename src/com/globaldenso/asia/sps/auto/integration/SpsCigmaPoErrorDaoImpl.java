/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;


/**
 * A "Dao" implementation class of "SpsCigmaPoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaPoErrorDaoImpl extends SqlMapClientDaoSupport implements SpsCigmaPoErrorDao {

    /**
     * Default constructor
     */
    public SpsCigmaPoErrorDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public SpsCigmaPoErrorDomain searchByKey(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaPoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaPoError.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaPoErrorDomain> searchByCondition(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaPoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaPoError.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaPoErrorDomain> searchByConditionForPaging(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaPoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaPoError.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaPoError.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaPoErrorDomain searchByKeyForChange(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaPoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaPoError.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public SpsCigmaPoErrorDomain lockByKey(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaPoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaPoError.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public SpsCigmaPoErrorDomain lockByKeyNoWait(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaPoErrorDomain)getSqlMapClientTemplate()
                .queryForObject("SpsCigmaPoError.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    public void create(SpsCigmaPoErrorDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsCigmaPoError.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    public int update(SpsCigmaPoErrorDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsCigmaPoError.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaPoErrorDomain domain, SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsCigmaPoError.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaPoError.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaPoErrorDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaPoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaPoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaPoError.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
