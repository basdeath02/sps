/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain;


/**
 * A "Dao" implementation class of "SpsMRoleMenu"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleMenuDaoImpl extends SqlMapClientDaoSupport implements SpsMRoleMenuDao {

    /**
     * Default constructor
     */
    public SpsMRoleMenuDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public SpsMRoleMenuDomain searchByKey(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleMenuDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleMenu.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleMenuDomain> searchByCondition(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleMenuDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRoleMenu.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleMenuDomain> searchByConditionForPaging(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleMenuDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRoleMenu.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int searchCount(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleMenu.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleMenuDomain searchByKeyForChange(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleMenuDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleMenu.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public SpsMRoleMenuDomain lockByKey(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleMenuDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleMenu.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public SpsMRoleMenuDomain lockByKeyNoWait(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleMenuDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMRoleMenu.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain)
     */
    public void create(SpsMRoleMenuDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMRoleMenu.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain)
     */
    public int update(SpsMRoleMenuDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMRoleMenu.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleMenuDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleMenuDomain domain, SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMRoleMenu.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int delete(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRoleMenu.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleMenuDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleMenuCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleMenuCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRoleMenu.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
