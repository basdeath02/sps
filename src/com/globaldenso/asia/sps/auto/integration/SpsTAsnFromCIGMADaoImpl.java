/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnFromCIGMADao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao;
import com.globaldenso.asia.sps.auto.business.dao.SpsTPoToCIGMADao;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoDueDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;
import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * A "Dao" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table
 * definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnFromCIGMADaoImpl extends SqlMapClientDaoSupport implements
SpsTAsnFromCIGMADao {

	/**
	 * Default constructor
	 */
	public SpsTAsnFromCIGMADaoImpl() {
	}



	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
	 */
	@SuppressWarnings("unchecked")
	public List<SpsMDensoDensoRelationWithASNDNDomain> searchByCondition(
			SpsMDensoDensoRelationWithASNDNDomain criteria)
			throws ApplicationException {

		List<SpsMDensoDensoRelationWithASNDNDomain> result = new ArrayList<SpsMDensoDensoRelationWithASNDNDomain>();

		result = getSqlMapClientTemplate().queryForList(
				"SpsTAsnFromCIGMA.SearchSupplier", criteria);

		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SPSTDoHeaderDNDomain> searchDONotCcl(
			SPSTDoHeaderDNDomain criteria)
			throws ApplicationException {

		List<SPSTDoHeaderDNDomain> result = new ArrayList<SPSTDoHeaderDNDomain>();

		result = getSqlMapClientTemplate().queryForList(
				"SpsTAsnFromCIGMA.SearchDONotCcl", criteria);

		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<SPSTDoHeaderDNDomain> searchDOPrevCcl(
			SPSTDoHeaderDNDomain criteria)
			throws ApplicationException {

		List<SPSTDoHeaderDNDomain> result = new ArrayList<SPSTDoHeaderDNDomain>();

		result = getSqlMapClientTemplate().queryForList(
				"SpsTAsnFromCIGMA.SearchDOPrevCcl", criteria);

		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SPSTDoHeaderDNDomain> searchDO(
			SPSTDoHeaderDNDomain criteria)
			throws ApplicationException {

		List<SPSTDoHeaderDNDomain> result = new ArrayList<SPSTDoHeaderDNDomain>();

		result = getSqlMapClientTemplate().queryForList(
				"SpsTAsnFromCIGMA.SearchDO", criteria);

		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<SPSTDoDetailDNDomain> searchDODetail(
			SPSTDoDetailDNDomain criteria)
			throws ApplicationException {

		List<SPSTDoDetailDNDomain> result = new ArrayList<SPSTDoDetailDNDomain>();

		result = getSqlMapClientTemplate().queryForList(
				"SpsTAsnFromCIGMA.SearchDODetail", criteria);

		return result;
	}
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
	 */
	public boolean InsertUpdateASN(SPSTAsnHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;		
		if(criteria.getAsnStatus().equals("CCL")){
			int updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateAsnStatus", criteria);
			if(updateSuccess >0){
				
				result =true;
			}
		}else{
		int rowInsert =0;
    	 SqlMapClient client = getSqlMapClientTemplate().getSqlMapClient();
    	 try {

    	            client.startTransaction();
    	            client.getCurrentConnection().setAutoCommit(false);
    	            String actualEtaTime="000000";
    	            if(criteria.getActualEtaTime() != null){
    	            	actualEtaTime =criteria.getActualEtaTime();
    	            }
    	            criteria.setActualEta(criteria.getActualEta()+actualEtaTime);
    	            String actualEtdTime="000000";
    	            if(criteria.getActualEtdTime() != null){
    	            	actualEtdTime =criteria.getActualEtdTime();
    	            }
    	            criteria.setActualEtd(criteria.getActualEtd()+actualEtdTime);
    	            
    	            String planEtaTime="000000";
    	            if(criteria.getPlanEtaTime() != null){
    	            	planEtaTime =criteria.getPlanEtaTime();
    	            }
    	            //criteria.setPlanEta(criteria.getPlanEta()+planEtaTime);
    	            criteria.setPlanEta(criteria.getActualEta());
    	            
    	            String planEtdTime="000000";
    	            if(criteria.getPlanEtdTime() != null){
    	            	planEtdTime =criteria.getPlanEtdTime();
    	            }
    	            criteria.setPlanEtd(criteria.getPlanEtd()+planEtdTime);
    	            
    	       	 rowInsert =client.update("SpsTAsnFromCIGMA.InsertASN", criteria);
    	       	 int check =0;
    	       	 rowInsert=0;
    	   			if(criteria.getListAsnDet()!= null){
    	   				if(criteria.getListAsnDet().size()>0){
    	   					for(int k=0;k<criteria.getListAsnDet().size();k++){
    	   						
    	   						rowInsert = client.update("SpsTAsnFromCIGMA.InsertASNDetail", criteria.getListAsnDet().get(k));
    	   						if(rowInsert<0){
    	   							check++;
    	   						}
    	   					}
    	   				}
    	   			}
    	   			
    	   			if(check==0){
    	   				rowInsert =1;
    	   			result =true;
    	   			 client.getCurrentConnection().commit();
    	   			}else{
    	   				rowInsert =0;
    	   				result =false;
    	   			 client.getCurrentConnection().rollback();
    	   			}
    	        
    	           

    	        
    	    }
    	   catch(Exception ex){
    		   try{
    			   client.getCurrentConnection().rollback();
   	            }catch(Exception e){
   	            	e.printStackTrace();
   	            }
    		   ex.printStackTrace();
    		   throw new ApplicationException(ex.getMessage());
    	   }
    	   finally {
    		   
    		   try{
    			   client.endTransaction();
   	            }catch(Exception e){
   	            	e.printStackTrace();
   	            }
    	    }
    	
		}
		return result;
	}
	public boolean InsertASNDetail(SPSTAsnDetailDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		int updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.InsertASNDetail", criteria);
		if(updateSuccess >0){
			
			result =true;
		}
		return result;
	}
	public boolean UpdateDO(SPSTDoHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		int updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateDO", criteria);
		if(updateSuccess >0){
			
			result =true;
		}
		return result;
	}
	public boolean UpdateDODet(SPSTDoDetailDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		int updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateDODet", criteria);
		if(updateSuccess >0){
			
			result =true;
		}
		return result;
	}
	
	public boolean UpdateTranFlag(SPSTAsnHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		int updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateTrnFlag", criteria);
		if(updateSuccess >0){
			
			result =true;
		}
		return result;
	}
	
	public boolean DeleteASN(SPSTAsnHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;

		int updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.DeleteAsn", criteria);
		if(updateSuccess >0){
			
			updateSuccess =getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.DeleteAsnDetail", criteria);
			if(updateSuccess >0){
				
				result =true;
			}
		}
		
		return result;
	}
	
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderDetailDomain> searchExistDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
        
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailResultList = null;
        deliveryOrderDetailResultList = getSqlMapClientTemplate().queryForList("SpsTAsnFromCIGMA.SearchExistDo",deliveryOrderInformationDomain);

        return deliveryOrderDetailResultList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDeliveryOrderAcknowledgement(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        SpsTPoDomain poDomain = null;
        poDomain = (SpsTPoDomain)getSqlMapClientTemplate().queryForObject(
            "SpsTAsnFromCIGMA.SearchDeliveryOrderAcknowledgement",
            deliveryOrderInformationDomain);
        return poDomain;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchAcknowledgedDO(com.globaldenso.AcknowledgedDOInformationDomain.business.domain.AcknowledgedDOInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDoInformationDomain)
    {
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInfoResultList = 
            new ArrayList<AcknowledgedDoInformationReturnDomain>();
        acknowledgedDOInfoResultList = (List<AcknowledgedDoInformationReturnDomain>)
            getSqlMapClientTemplate().queryForList("SpsTAsnFromCIGMA.SearchAcknowledgedDo",
                acknowledgedDoInformationDomain);
        return acknowledgedDOInfoResultList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int searchCount(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnFromCIGMA.SearchCount", criteria);
        return cnt;
    }


    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int searchCountPlant(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnFromCIGMA.SearchCountPlant", criteria);
        return cnt;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int SearchCountPrevASN(SPSTAsnHeaderDNDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnFromCIGMA.SearchCountPrevASN", criteria);
        return cnt;
    }
    
    

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400VendorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400VendorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400VendorDomain> searchByConditionAS400(SpsMAs400VendorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400VendorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsnFromCIGMA.SearchByConditionAS400", criteria);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantSupplierDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantSupplierCriteriaDomain)
     */
    public int searchCountPlantSupplier(SpsMPlantSupplierCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnFromCIGMA.SearchCountPlantSupplier", criteria);
        return cnt;
    }
    
	public boolean UpdateDOStatus(SPSTAsnHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		if(criteria != null){
			if(criteria.getListAsnDet() != null){
				if(criteria.getListAsnDet().size() >0){
					for(int i=0;i<criteria.getListAsnDet().size();i++){
						SPSTDoDetailDNDomain data = new SPSTDoDetailDNDomain();
						data.setDoId(criteria.getListAsnDet().get(i).getDoId());
						data.setsPn(criteria.getListAsnDet().get(i).getsPn());
					
						// 27/2/2020 : Fix cancel when do_id has cancel more than one time
						SPSTAsnDetailDNDomain asnDetData = criteria.getListAsnDet().get(i);
					
						if(criteria.getAsnStatus().equals("ISS")) {
						    getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateDODetCPS", data);
	                        getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateDODetPTS", data);
	                        
	                        
	                        SPSTDoHeaderDNDomain dataHead = new SPSTDoHeaderDNDomain();
	                        dataHead.setDoId(criteria.getListAsnDet().get(i).getDoId());
	                        getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateDOCPS", dataHead);
	                        getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateDOPTS", dataHead);
						} else if (criteria.getAsnStatus().equals("CCL")) {
						    // 5/2/2020 update do status for cancel asn
						    
						    // 27/2/2020 Fix not update status when do_id is in N asn (CCL), add where ASN_No
						    getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateCancelDODetPTS", asnDetData); 
                            getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateCancelDODetISS", asnDetData);
                            // 27/2/2020 End Fix not update status when do_id is in N asn (CCL), add where ASN_No
                            
                            SPSTDoHeaderDNDomain dataHead = new SPSTDoHeaderDNDomain();
                            dataHead.setDoId(criteria.getListAsnDet().get(i).getDoId());
                            getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateCancelDOPTS", dataHead);
                            getSqlMapClientTemplate().update("SpsTAsnFromCIGMA.UpdateCancelDOISS", dataHead);
						}
					}
				}
			}
			
		}
		result =true;
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SpsMUserDomain> searchUserDenso(
			SpsMDensoDensoRelationWithASNDNDomain criteria)
			throws ApplicationException {

		List<SpsMUserDomain> result = new ArrayList<SpsMUserDomain>();

		result = getSqlMapClientTemplate().queryForList(
				"SpsTAsnFromCIGMA.SearchUserDenso", criteria);

		return result;
	}
	
	@SuppressWarnings("unchecked")
    public List<SpsMUserDomain> searchUserSupplier(
            SpsMDensoDensoRelationWithASNDNDomain criteria)
            throws ApplicationException {

        List<SpsMUserDomain> result = new ArrayList<SpsMUserDomain>();

        result = getSqlMapClientTemplate().queryForList(
                "SpsTAsnFromCIGMA.SearchUserSupplier", criteria);

        return result;
    }
	
	@SuppressWarnings("unchecked")
    public List<SPSTDoDetailDNDomain> searchDODet(
            SPSTDoDetailDNDomain criteria)
            throws ApplicationException {

        List<SPSTDoDetailDNDomain> result = new ArrayList<SPSTDoDetailDNDomain>();

        result = getSqlMapClientTemplate().queryForList(
                "SpsTAsnFromCIGMA.SearchDODet", criteria);

        return result;
    }
	
	@SuppressWarnings("unchecked")
    public List<SPSTDoHeaderDNDomain> searchDODelivery(SPSTDoHeaderDNDomain criteria)throws ApplicationException {

        List<SPSTDoHeaderDNDomain> result = new ArrayList<SPSTDoHeaderDNDomain>();

        result = getSqlMapClientTemplate().queryForList("SpsTAsnFromCIGMA.SearchDODeliveryDate", criteria);

        return result;
    }
	
	@SuppressWarnings("unchecked")
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchByConditionGroup(
            SpsMDensoDensoRelationWithASNDNDomain criteria)
            throws ApplicationException {

        List<SpsMDensoDensoRelationWithASNDNDomain> result = new ArrayList<SpsMDensoDensoRelationWithASNDNDomain>();

        result = getSqlMapClientTemplate().queryForList("SpsTAsnFromCIGMA.SearchSupplierGroup", criteria);

        return result;
    }
	
	@SuppressWarnings("unchecked")
    public List<SpsMDensoDensoRelationWithASNDNDomain> searchSupplierBySchema(
            SpsMDensoDensoRelationWithASNDNDomain criteria)
            throws ApplicationException {

        List<SpsMDensoDensoRelationWithASNDNDomain> result = new ArrayList<SpsMDensoDensoRelationWithASNDNDomain>();

        result = getSqlMapClientTemplate().queryForList(
                "SpsTAsnFromCIGMA.SearchSupplierBySchema", criteria);

        return result;
    }
}
