/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/01/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain;


/**
 * A "Dao" implementation class of "SpsMUnitOfMeasure"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/01/05 09:25:07<br />
 * 
 * This module generated automatically in 2016/01/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUnitOfMeasureDaoImpl extends SqlMapClientDaoSupport implements SpsMUnitOfMeasureDao {

    /**
     * Default constructor
     */
    public SpsMUnitOfMeasureDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public SpsMUnitOfMeasureDomain searchByKey(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUnitOfMeasureDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUnitOfMeasure.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUnitOfMeasureDomain> searchByCondition(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUnitOfMeasureDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUnitOfMeasure.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUnitOfMeasureDomain> searchByConditionForPaging(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUnitOfMeasureDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUnitOfMeasure.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int searchCount(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMUnitOfMeasure.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    @Deprecated
    public SpsMUnitOfMeasureDomain searchByKeyForChange(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUnitOfMeasureDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUnitOfMeasure.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public SpsMUnitOfMeasureDomain lockByKey(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUnitOfMeasureDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUnitOfMeasure.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public SpsMUnitOfMeasureDomain lockByKeyNoWait(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUnitOfMeasureDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMUnitOfMeasure.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain)
     */
    public void create(SpsMUnitOfMeasureDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMUnitOfMeasure.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain)
     */
    public int update(SpsMUnitOfMeasureDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMUnitOfMeasure.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUnitOfMeasureDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int updateByCondition(SpsMUnitOfMeasureDomain domain, SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMUnitOfMeasure.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int delete(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUnitOfMeasure.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUnitOfMeasureDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUnitOfMeasureCriteriaDomain)
     */
    public int deleteByCondition(SpsMUnitOfMeasureCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUnitOfMeasure.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
