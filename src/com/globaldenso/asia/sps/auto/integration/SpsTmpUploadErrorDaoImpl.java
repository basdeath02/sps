/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2559/04/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain;


/**
 * A "Dao" implementation class of "SpsTmpUploadError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2559/04/07 12:25:27<br />
 * 
 * This module generated automatically in 2559/04/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadErrorDaoImpl extends SqlMapClientDaoSupport implements SpsTmpUploadErrorDao {

    /**
     * Default constructor
     */
    public SpsTmpUploadErrorDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public SpsTmpUploadErrorDomain searchByKey(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadError.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUploadErrorDomain> searchByCondition(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUploadErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUploadError.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUploadErrorDomain> searchByConditionForPaging(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUploadErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUploadError.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int searchCount(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadError.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUploadErrorDomain searchByKeyForChange(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadError.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public SpsTmpUploadErrorDomain lockByKey(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadError.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public SpsTmpUploadErrorDomain lockByKeyNoWait(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadErrorDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpUploadError.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain)
     */
    public void create(SpsTmpUploadErrorDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpUploadError.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain)
     */
    public int update(SpsTmpUploadErrorDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpUploadError.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUploadErrorDomain domain, SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpUploadError.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int delete(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUploadError.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadErrorDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUploadErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUploadError.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
