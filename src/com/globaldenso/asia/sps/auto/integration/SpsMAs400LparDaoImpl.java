/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain;


/**
 * A "Dao" implementation class of "SpsMAs400Lpar"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAs400LparDaoImpl extends SqlMapClientDaoSupport implements SpsMAs400LparDao {

    /**
     * Default constructor
     */
    public SpsMAs400LparDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public SpsMAs400LparDomain searchByKey(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400LparDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Lpar.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400LparDomain> searchByCondition(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400LparDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAs400Lpar.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAs400LparDomain> searchByConditionForPaging(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAs400LparDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAs400Lpar.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int searchCount(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Lpar.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    @Deprecated
    public SpsMAs400LparDomain searchByKeyForChange(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400LparDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Lpar.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public SpsMAs400LparDomain lockByKey(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400LparDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAs400Lpar.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public SpsMAs400LparDomain lockByKeyNoWait(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAs400LparDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMAs400Lpar.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain)
     */
    public void create(SpsMAs400LparDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMAs400Lpar.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain)
     */
    public int update(SpsMAs400LparDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMAs400Lpar.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAs400LparDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int updateByCondition(SpsMAs400LparDomain domain, SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMAs400Lpar.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int delete(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAs400Lpar.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAs400LparDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAs400LparCriteriaDomain)
     */
    public int deleteByCondition(SpsMAs400LparCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAs400Lpar.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
