/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/12       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;


/**
 * A "Dao" implementation class of "SpsTDo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/12 14:21:17<br />
 * 
 * This module generated automatically in 2015/03/12 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoDaoImpl extends SqlMapClientDaoSupport implements SpsTDoDao {

    /**
     * Default constructor
     */
    public SpsTDoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public SpsTDoDomain searchByKey(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDo.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTDoDomain> searchByCondition(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTDoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTDo.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTDoDomain> searchByConditionForPaging(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTDoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTDo.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int searchCount(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTDo.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    @Deprecated
    public SpsTDoDomain searchByKeyForChange(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDo.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public SpsTDoDomain lockByKey(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDo.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public SpsTDoDomain lockByKeyNoWait(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTDo.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    public void create(SpsTDoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTDo.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    public int update(SpsTDoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTDo.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int updateByCondition(SpsTDoDomain domain, SpsTDoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTDo.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int delete(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTDo.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
     */
    public int deleteByCondition(SpsTDoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTDo.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
