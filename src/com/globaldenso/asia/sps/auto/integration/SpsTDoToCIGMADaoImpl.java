/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTDoToCIGMADao;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoDetailDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoKanbanSeqDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoDueDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;


/**
 * A "Dao" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoToCIGMADaoImpl extends SqlMapClientDaoSupport implements SpsTDoToCIGMADao {

    /**
     * Default constructor
     */
    public SpsTDoToCIGMADaoImpl() {
    }

	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
	 */
	@SuppressWarnings("unchecked")
	public List<SpsMDensoDensoRelationWithDODNDomain> searchByCondition(
			SpsMDensoDensoRelationWithDODNDomain criteria)
			throws ApplicationException {

		List<SpsMDensoDensoRelationWithDODNDomain> result = new ArrayList<SpsMDensoDensoRelationWithDODNDomain>();

		//result = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplier", criteria);
		result = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierGroup", criteria);
		if (result != null) {
			if (result.size() > 0) {
				for (int i = 0; i < result.size(); i++) {
					List<SPSTDoHeaderDNDomain> listDO = new ArrayList<SPSTDoHeaderDNDomain>();
					listDO = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchDO", result.get(i));

					if (listDO != null) {
						if (listDO.size() > 0) {
						    for (int j = 0; j < listDO.size(); j++) {
						        //check is in DN-DN Relate
						        String msgError = "";
	                            SpsMDensoDensoRelationWithDODNDomain dataCom = new SpsMDensoDensoRelationWithDODNDomain();
	                            dataCom.setSchemaCd(result.get(i).getSchemaCd());
	                            dataCom.setdCd(listDO.get(j).getdCd());
	                            dataCom.setOther(criteria.getdCd());
	                            
	                            List<SpsMDensoDensoRelationWithDODNDomain> selectListCom = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", dataCom);
                                if(selectListCom==null||selectListCom.size()==0){
                                    //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
                                    msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M01;
                                }else{
                                    dataCom.setdPcd(listDO.get(j).getdPcd());
                                    selectListCom = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", dataCom);
                                    if(selectListCom==null||selectListCom.size()==0){
                                        //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                                        msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M02;
                                    }else{
                                        dataCom.setsCd(listDO.get(j).getsCd());
                                        selectListCom = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", dataCom);
                                        if(selectListCom==null||selectListCom.size()==0){
                                            //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
                                            msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M03;
                                        }else{
                                            dataCom.setsPcd(listDO.get(j).getsPcd());
                                            selectListCom = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", dataCom);
                                            if(selectListCom==null||selectListCom.size()==0){
                                                //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                                                msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M05;
                                            }
                                        }
                                    }
                                }
	                            /*
                                SpsMDensoDensoRelationWithDODNDomain dataSup = new SpsMDensoDensoRelationWithDODNDomain();
                                dataSup.setSchemaCd(result.get(i).getSchemaCd());
                                dataSup.setsCd(listDO.get(j).getsCd());
                                
                                List<SpsMDensoDensoRelationWithDODNDomain> selectListSup = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", dataSup);
                                if(selectListSup==null||selectListSup.size()==0){
                                    //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M01);
                                    msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M03;
                                }else{
                                    dataSup.setsPcd(listDO.get(j).getsPcd());
                                    selectListSup = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", dataSup);
                                    if(selectListSup==null||selectListSup.size()==0){
                                        //errorList.add(SupplierPortalConstant.UPLOAD_ERROR_CODE_M02);
                                        msgError = msgError+" "+SupplierPortalConstant.UPLOAD_ERROR_CODE_M05;
                                    }
                                }
                                */
                                if(!msgError.equals("")){
                                    listDO.get(j).setMsgError(msgError);
                                    listDO.get(j).setDnDnTrnFlg("9");
                                }else{
                                    SpsMDensoDensoRelationWithDODNDomain data = new SpsMDensoDensoRelationWithDODNDomain();
                                    data.setSchemaCd(result.get(i).getSchemaCd());
                                    data.setdCd(listDO.get(j).getdCd());
                                    data.setsCd(listDO.get(j).getsCd());
                                    data.setdPcd(listDO.get(j).getdPcd());
                                    data.setsPcd(listDO.get(j).getsPcd());
                                    data.setVendorCd(listDO.get(j).getVendorCd());
                                    data.setOther(criteria.getdCd());
                                    
                                    List<SpsMDensoDensoRelationWithDODNDomain> selectList = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", data);
                                    
                                    if(selectList.size()>0){
                                        listDO.get(j).setCusNo(selectList.get(0).getCusNo());
                                        listDO.get(j).setShipNo(selectList.get(0).getShipNo());
                                    }
                                    
                                    List<SPSTDoDetailDNDomain> listDODet = new ArrayList<SPSTDoDetailDNDomain>();
                                    List<SPSTDoKanbanSeqDNDomain> listDOKanban = new ArrayList<SPSTDoKanbanSeqDNDomain>();
                                    listDODet = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchDODet",listDO.get(j));

                                    listDOKanban = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchDOKanban",listDO.get(j));
                                    if (listDODet != null) {
                                        if (listDODet.size() > 0) {
                                            listDO.get(j).setListDoDet(listDODet);
                                        }
                                    }
                                    
                                    if (listDOKanban != null) {
                                        if (listDOKanban.size() > 0) {
                                            listDO.get(j).setListDoKanban(listDOKanban);
                                        }
                                    }
                                }
							}
							result.get(i).setListDO(listDO);
						}
					}
				}
			}
		}

		return result;
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoCriteriaDomain)
	 */
	public boolean UpdateDoTranFlag(SPSTDoHeaderDNDomain criteria)
			throws ApplicationException {
		boolean result =false;
		int updateSuccess =getSqlMapClientTemplate().update("SpsTDoToCIGMA.UpdateDOFlag", criteria);
		if(updateSuccess >0){
			result =true;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
    public List<SpsMDensoDensoRelationWithDODNDomain> searchSupplierBySchema(SpsMDensoDensoRelationWithDODNDomain criteria)throws ApplicationException {

        List<SpsMDensoDensoRelationWithDODNDomain> result = new ArrayList<SpsMDensoDensoRelationWithDODNDomain>();

        result = getSqlMapClientTemplate().queryForList("SpsTDoToCIGMA.SearchSupplierBySchema", criteria);

        return result;
    }
  

}
