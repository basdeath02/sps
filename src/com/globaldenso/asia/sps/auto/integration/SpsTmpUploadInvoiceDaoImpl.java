/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/10/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain;


/**
 * A "Dao" implementation class of "SpsTmpUploadInvoice"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/10/30 14:41:58<br />
 * 
 * This module generated automatically in 2015/10/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadInvoiceDaoImpl extends SqlMapClientDaoSupport implements SpsTmpUploadInvoiceDao {

    /**
     * Default constructor
     */
    public SpsTmpUploadInvoiceDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public SpsTmpUploadInvoiceDomain searchByKey(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadInvoiceDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadInvoice.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUploadInvoiceDomain> searchByCondition(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUploadInvoiceDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUploadInvoice.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUploadInvoiceDomain> searchByConditionForPaging(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUploadInvoiceDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUploadInvoice.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int searchCount(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadInvoice.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUploadInvoiceDomain searchByKeyForChange(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadInvoiceDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadInvoice.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public SpsTmpUploadInvoiceDomain lockByKey(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadInvoiceDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadInvoice.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public SpsTmpUploadInvoiceDomain lockByKeyNoWait(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadInvoiceDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpUploadInvoice.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain)
     */
    public void create(SpsTmpUploadInvoiceDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpUploadInvoice.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain)
     */
    public int update(SpsTmpUploadInvoiceDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpUploadInvoice.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadInvoiceDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUploadInvoiceDomain domain, SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpUploadInvoice.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int delete(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUploadInvoice.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadInvoiceDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadInvoiceCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUploadInvoiceCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUploadInvoice.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
