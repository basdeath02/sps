/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/30       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain;


/**
 * A "Dao" implementation class of "SpsTmpUserSupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/30 12:07:21<br />
 * 
 * This module generated automatically in 2015/03/30 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserSupplierDaoImpl extends SqlMapClientDaoSupport implements SpsTmpUserSupplierDao {

    /**
     * Default constructor
     */
    public SpsTmpUserSupplierDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public SpsTmpUserSupplierDomain searchByKey(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserSupplier.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUserSupplierDomain> searchByCondition(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUserSupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUserSupplier.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUserSupplierDomain> searchByConditionForPaging(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUserSupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUserSupplier.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int searchCount(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserSupplier.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUserSupplierDomain searchByKeyForChange(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserSupplier.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public SpsTmpUserSupplierDomain lockByKey(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserSupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserSupplier.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public SpsTmpUserSupplierDomain lockByKeyNoWait(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserSupplierDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpUserSupplier.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain)
     */
    public void create(SpsTmpUserSupplierDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpUserSupplier.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain)
     */
    public int update(SpsTmpUserSupplierDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpUserSupplier.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserSupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUserSupplierDomain domain, SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpUserSupplier.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int delete(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUserSupplier.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserSupplierDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserSupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUserSupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUserSupplier.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
