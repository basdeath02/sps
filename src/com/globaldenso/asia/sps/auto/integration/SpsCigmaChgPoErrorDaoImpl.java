/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;


/**
 * A "Dao" implementation class of "SpsCigmaChgPoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaChgPoErrorDaoImpl extends SqlMapClientDaoSupport implements SpsCigmaChgPoErrorDao {

    /**
     * Default constructor
     */
    public SpsCigmaChgPoErrorDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public SpsCigmaChgPoErrorDomain searchByKey(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgPoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgPoError.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaChgPoErrorDomain> searchByCondition(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaChgPoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaChgPoError.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaChgPoErrorDomain> searchByConditionForPaging(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaChgPoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaChgPoError.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgPoError.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaChgPoErrorDomain searchByKeyForChange(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgPoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgPoError.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public SpsCigmaChgPoErrorDomain lockByKey(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgPoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgPoError.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public SpsCigmaChgPoErrorDomain lockByKeyNoWait(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgPoErrorDomain)getSqlMapClientTemplate()
                .queryForObject("SpsCigmaChgPoError.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain)
     */
    public void create(SpsCigmaChgPoErrorDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsCigmaChgPoError.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain)
     */
    public int update(SpsCigmaChgPoErrorDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsCigmaChgPoError.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaChgPoErrorDomain domain, SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsCigmaChgPoError.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaChgPoError.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgPoErrorDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgPoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaChgPoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaChgPoError.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
