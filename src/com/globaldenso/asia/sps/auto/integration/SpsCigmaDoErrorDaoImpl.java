/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/21       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;


/**
 * A "Dao" implementation class of "SpsCigmaDoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/21 15:45:27<br />
 * 
 * This module generated automatically in 2015/01/21 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaDoErrorDaoImpl extends SqlMapClientDaoSupport implements SpsCigmaDoErrorDao {

    /**
     * Default constructor
     */
    public SpsCigmaDoErrorDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public SpsCigmaDoErrorDomain searchByKey(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaDoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaDoError.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaDoErrorDomain> searchByCondition(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaDoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaDoError.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaDoErrorDomain> searchByConditionForPaging(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaDoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaDoError.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaDoError.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaDoErrorDomain searchByKeyForChange(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaDoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaDoError.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public SpsCigmaDoErrorDomain lockByKey(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaDoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaDoError.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public SpsCigmaDoErrorDomain lockByKeyNoWait(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaDoErrorDomain)getSqlMapClientTemplate()
                .queryForObject("SpsCigmaDoError.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain)
     */
    public void create(SpsCigmaDoErrorDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsCigmaDoError.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain)
     */
    public int update(SpsCigmaDoErrorDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsCigmaDoError.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaDoErrorDomain domain, SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsCigmaDoError.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaDoError.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaDoErrorDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaDoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaDoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaDoError.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
