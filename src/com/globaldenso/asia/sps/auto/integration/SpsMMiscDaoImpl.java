/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;


/**
 * A "Dao" implementation class of "SpsMMisc"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMMiscDaoImpl extends SqlMapClientDaoSupport implements SpsMMiscDao {

    /**
     * Default constructor
     */
    public SpsMMiscDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public SpsMMiscDomain searchByKey(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMiscDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMMisc.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMMiscDomain> searchByCondition(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMMiscDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMMisc.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMMiscDomain> searchByConditionForPaging(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMMiscDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMMisc.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int searchCount(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMMisc.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    @Deprecated
    public SpsMMiscDomain searchByKeyForChange(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMiscDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMMisc.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public SpsMMiscDomain lockByKey(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMiscDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMMisc.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public SpsMMiscDomain lockByKeyNoWait(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return (SpsMMiscDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMMisc.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain)
     */
    public void create(SpsMMiscDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMMisc.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain)
     */
    public int update(SpsMMiscDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMMisc.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int updateByCondition(SpsMMiscDomain domain, SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMMisc.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int delete(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMMisc.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMMiscDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMMiscCriteriaDomain)
     */
    public int deleteByCondition(SpsMMiscCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMMisc.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
