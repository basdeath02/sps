/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;


/**
 * A "Dao" implementation class of "SpsMAnnounceMessage"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMAnnounceMessageDaoImpl extends SqlMapClientDaoSupport implements SpsMAnnounceMessageDao {

    /**
     * Default constructor
     */
    public SpsMAnnounceMessageDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public SpsMAnnounceMessageDomain searchByKey(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAnnounceMessageDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAnnounceMessage.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAnnounceMessageDomain> searchByCondition(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAnnounceMessageDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAnnounceMessage.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMAnnounceMessageDomain> searchByConditionForPaging(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMAnnounceMessageDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMAnnounceMessage.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int searchCount(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMAnnounceMessage.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    @Deprecated
    public SpsMAnnounceMessageDomain searchByKeyForChange(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAnnounceMessageDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAnnounceMessage.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public SpsMAnnounceMessageDomain lockByKey(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAnnounceMessageDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMAnnounceMessage.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public SpsMAnnounceMessageDomain lockByKeyNoWait(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return (SpsMAnnounceMessageDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMAnnounceMessage.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain)
     */
    public void create(SpsMAnnounceMessageDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMAnnounceMessage.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain)
     */
    public int update(SpsMAnnounceMessageDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMAnnounceMessage.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int updateByCondition(SpsMAnnounceMessageDomain domain, SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMAnnounceMessage.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int delete(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAnnounceMessage.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMAnnounceMessageDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMAnnounceMessageCriteriaDomain)
     */
    public int deleteByCondition(SpsMAnnounceMessageCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMAnnounceMessage.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
