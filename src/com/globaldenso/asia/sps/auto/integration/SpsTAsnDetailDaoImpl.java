/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;


/**
 * A "Dao" implementation class of "SpsTAsnDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTAsnDetailDaoImpl extends SqlMapClientDaoSupport implements SpsTAsnDetailDao {

    /**
     * Default constructor
     */
    public SpsTAsnDetailDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public SpsTAsnDetailDomain searchByKey(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnDetail.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTAsnDetailDomain> searchByCondition(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTAsnDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsnDetail.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTAsnDetailDomain> searchByConditionForPaging(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTAsnDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTAsnDetail.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int searchCount(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnDetail.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTAsnDetailDomain searchByKeyForChange(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnDetail.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public SpsTAsnDetailDomain lockByKey(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTAsnDetail.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public SpsTAsnDetailDomain lockByKeyNoWait(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTAsnDetailDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTAsnDetail.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain)
     */
    public void create(SpsTAsnDetailDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTAsnDetail.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain)
     */
    public int update(SpsTAsnDetailDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTAsnDetail.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTAsnDetailDomain domain, SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTAsnDetail.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int delete(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTAsnDetail.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTAsnDetailDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTAsnDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTAsnDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTAsnDetail.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
