/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain;


/**
 * A "Dao" implementation class of "SpsMDensoSupplierParts"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMDensoSupplierPartsDaoImpl extends SqlMapClientDaoSupport implements SpsMDensoSupplierPartsDao {

    /**
     * Default constructor
     */
    public SpsMDensoSupplierPartsDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public SpsMDensoSupplierPartsDomain searchByKey(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierPartsDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierParts.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMDensoSupplierPartsDomain> searchByCondition(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMDensoSupplierPartsDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMDensoSupplierParts.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMDensoSupplierPartsDomain> searchByConditionForPaging(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMDensoSupplierPartsDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMDensoSupplierParts.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int searchCount(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierParts.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    @Deprecated
    public SpsMDensoSupplierPartsDomain searchByKeyForChange(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierPartsDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierParts.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public SpsMDensoSupplierPartsDomain lockByKey(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierPartsDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMDensoSupplierParts.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public SpsMDensoSupplierPartsDomain lockByKeyNoWait(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return (SpsMDensoSupplierPartsDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMDensoSupplierParts.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain)
     */
    public void create(SpsMDensoSupplierPartsDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMDensoSupplierParts.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain)
     */
    public int update(SpsMDensoSupplierPartsDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMDensoSupplierParts.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMDensoSupplierPartsDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int updateByCondition(SpsMDensoSupplierPartsDomain domain, SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMDensoSupplierParts.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int delete(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMDensoSupplierParts.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMDensoSupplierPartsDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMDensoSupplierPartsCriteriaDomain)
     */
    public int deleteByCondition(SpsMDensoSupplierPartsCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMDensoSupplierParts.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
