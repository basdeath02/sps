/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/05       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain;


/**
 * A "Dao" implementation class of "SpsTDoKanbanSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/05 13:51:36<br />
 * 
 * This module generated automatically in 2015/02/05 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoKanbanSeqDaoImpl extends SqlMapClientDaoSupport implements SpsTDoKanbanSeqDao {

    /**
     * Default constructor
     */
    public SpsTDoKanbanSeqDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public SpsTDoKanbanSeqDomain searchByKey(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoKanbanSeqDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoKanbanSeq.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTDoKanbanSeqDomain> searchByCondition(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTDoKanbanSeqDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTDoKanbanSeq.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTDoKanbanSeqDomain> searchByConditionForPaging(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTDoKanbanSeqDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTDoKanbanSeq.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int searchCount(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoKanbanSeq.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    @Deprecated
    public SpsTDoKanbanSeqDomain searchByKeyForChange(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoKanbanSeqDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoKanbanSeq.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public SpsTDoKanbanSeqDomain lockByKey(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoKanbanSeqDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoKanbanSeq.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public SpsTDoKanbanSeqDomain lockByKeyNoWait(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoKanbanSeqDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTDoKanbanSeq.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain)
     */
    public void create(SpsTDoKanbanSeqDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTDoKanbanSeq.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain)
     */
    public int update(SpsTDoKanbanSeqDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTDoKanbanSeq.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTDoKanbanSeqDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int updateByCondition(SpsTDoKanbanSeqDomain domain, SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTDoKanbanSeq.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int delete(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTDoKanbanSeq.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoKanbanSeqDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoKanbanSeqCriteriaDomain)
     */
    public int deleteByCondition(SpsTDoKanbanSeqCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTDoKanbanSeq.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
