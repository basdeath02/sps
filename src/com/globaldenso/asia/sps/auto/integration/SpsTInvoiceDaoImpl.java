/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/07/09       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;


/**
 * A "Dao" implementation class of "SpsTInvoice"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/07/09 18:24:35<br />
 * 
 * This module generated automatically in 2015/07/09 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceDaoImpl extends SqlMapClientDaoSupport implements SpsTInvoiceDao {

    /**
     * Default constructor
     */
    public SpsTInvoiceDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public SpsTInvoiceDomain searchByKey(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoice.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTInvoiceDomain> searchByCondition(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTInvoiceDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTInvoice.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTInvoiceDomain> searchByConditionForPaging(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTInvoiceDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTInvoice.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int searchCount(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoice.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    @Deprecated
    public SpsTInvoiceDomain searchByKeyForChange(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoice.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public SpsTInvoiceDomain lockByKey(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoice.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public SpsTInvoiceDomain lockByKeyNoWait(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTInvoice.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain)
     */
    public void create(SpsTInvoiceDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTInvoice.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain)
     */
    public int update(SpsTInvoiceDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTInvoice.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int updateByCondition(SpsTInvoiceDomain domain, SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTInvoice.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int delete(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTInvoice.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain)
     */
    public int deleteByCondition(SpsTInvoiceCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTInvoice.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
