/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/03/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain;


/**
 * A "Dao" implementation class of "SpsTmpUploadAsn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/03/17 15:14:35<br />
 * 
 * This module generated automatically in 2015/03/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUploadAsnDaoImpl extends SqlMapClientDaoSupport implements SpsTmpUploadAsnDao {

    /**
     * Default constructor
     */
    public SpsTmpUploadAsnDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public SpsTmpUploadAsnDomain searchByKey(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadAsnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadAsn.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUploadAsnDomain> searchByCondition(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUploadAsnDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUploadAsn.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUploadAsnDomain> searchByConditionForPaging(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUploadAsnDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUploadAsn.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int searchCount(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadAsn.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUploadAsnDomain searchByKeyForChange(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadAsnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadAsn.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public SpsTmpUploadAsnDomain lockByKey(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadAsnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUploadAsn.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public SpsTmpUploadAsnDomain lockByKeyNoWait(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUploadAsnDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpUploadAsn.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain)
     */
    public void create(SpsTmpUploadAsnDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpUploadAsn.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain)
     */
    public int update(SpsTmpUploadAsnDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpUploadAsn.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUploadAsnDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUploadAsnDomain domain, SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpUploadAsn.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int delete(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUploadAsn.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUploadAsnDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUploadAsnCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUploadAsnCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUploadAsn.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
