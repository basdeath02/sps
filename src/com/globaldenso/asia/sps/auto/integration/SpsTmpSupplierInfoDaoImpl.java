/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain;


/**
 * A "Dao" implementation class of "SpsTmpSupplierInfo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpSupplierInfoDaoImpl extends SqlMapClientDaoSupport implements SpsTmpSupplierInfoDao {

    /**
     * Default constructor
     */
    public SpsTmpSupplierInfoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public SpsTmpSupplierInfoDomain searchByKey(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpSupplierInfoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpSupplierInfo.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpSupplierInfoDomain> searchByCondition(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpSupplierInfoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpSupplierInfo.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpSupplierInfoDomain> searchByConditionForPaging(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpSupplierInfoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpSupplierInfo.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int searchCount(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpSupplierInfo.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    @Deprecated
    public SpsTmpSupplierInfoDomain searchByKeyForChange(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpSupplierInfoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpSupplierInfo.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public SpsTmpSupplierInfoDomain lockByKey(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpSupplierInfoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpSupplierInfo.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public SpsTmpSupplierInfoDomain lockByKeyNoWait(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpSupplierInfoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpSupplierInfo.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain)
     */
    public void create(SpsTmpSupplierInfoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpSupplierInfo.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain)
     */
    public int update(SpsTmpSupplierInfoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpSupplierInfo.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpSupplierInfoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int updateByCondition(SpsTmpSupplierInfoDomain domain, SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpSupplierInfo.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int delete(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpSupplierInfo.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpSupplierInfoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpSupplierInfoCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpSupplierInfoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpSupplierInfo.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
