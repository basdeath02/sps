/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/05/17       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain;


/**
 * A "Dao" implementation class of "SpsMCompanyDensoExt"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/05/17 17:15:42<br />
 * 
 * This module generated automatically in 2016/05/17 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanyDensoExtDaoImpl extends SqlMapClientDaoSupport implements SpsMCompanyDensoExtDao {

    /**
     * Default constructor
     */
    public SpsMCompanyDensoExtDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public SpsMCompanyDensoExtDomain searchByKey(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoExtDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDensoExt.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCompanyDensoExtDomain> searchByCondition(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCompanyDensoExtDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCompanyDensoExt.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCompanyDensoExtDomain> searchByConditionForPaging(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCompanyDensoExtDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCompanyDensoExt.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int searchCount(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDensoExt.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    @Deprecated
    public SpsMCompanyDensoExtDomain searchByKeyForChange(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoExtDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDensoExt.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public SpsMCompanyDensoExtDomain lockByKey(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoExtDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDensoExt.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public SpsMCompanyDensoExtDomain lockByKeyNoWait(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoExtDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMCompanyDensoExt.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain)
     */
    public void create(SpsMCompanyDensoExtDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMCompanyDensoExt.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain)
     */
    public int update(SpsMCompanyDensoExtDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMCompanyDensoExt.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoExtDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int updateByCondition(SpsMCompanyDensoExtDomain domain, SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMCompanyDensoExt.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int delete(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCompanyDensoExt.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoExtDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoExtCriteriaDomain)
     */
    public int deleteByCondition(SpsMCompanyDensoExtCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCompanyDensoExt.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
