/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;


/**
 * A "Dao" implementation class of "SpsMRole"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleDaoImpl extends SqlMapClientDaoSupport implements SpsMRoleDao {

    /**
     * Default constructor
     */
    public SpsMRoleDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public SpsMRoleDomain searchByKey(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRole.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleDomain> searchByCondition(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRole.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleDomain> searchByConditionForPaging(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRole.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int searchCount(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMRole.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleDomain searchByKeyForChange(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRole.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public SpsMRoleDomain lockByKey(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRole.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public SpsMRoleDomain lockByKeyNoWait(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMRole.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain)
     */
    public void create(SpsMRoleDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMRole.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain)
     */
    public int update(SpsMRoleDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMRole.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleDomain domain, SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMRole.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int delete(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRole.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRole.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
