/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/04/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain;


/**
 * A "Dao" implementation class of "SpsTInvoiceDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/04/28 15:59:11<br />
 * 
 * This module generated automatically in 2015/04/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvoiceDetailDaoImpl extends SqlMapClientDaoSupport implements SpsTInvoiceDetailDao {

    /**
     * Default constructor
     */
    public SpsTInvoiceDetailDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public SpsTInvoiceDetailDomain searchByKey(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoiceDetail.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTInvoiceDetailDomain> searchByCondition(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTInvoiceDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTInvoiceDetail.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTInvoiceDetailDomain> searchByConditionForPaging(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTInvoiceDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTInvoiceDetail.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int searchCount(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoiceDetail.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTInvoiceDetailDomain searchByKeyForChange(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoiceDetail.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public SpsTInvoiceDetailDomain lockByKey(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvoiceDetail.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public SpsTInvoiceDetailDomain lockByKeyNoWait(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvoiceDetailDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTInvoiceDetail.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain)
     */
    public void create(SpsTInvoiceDetailDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTInvoiceDetail.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain)
     */
    public int update(SpsTInvoiceDetailDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTInvoiceDetail.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTInvoiceDetailDomain domain, SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTInvoiceDetail.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int delete(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTInvoiceDetail.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvoiceDetailDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTInvoiceDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTInvoiceDetail.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
