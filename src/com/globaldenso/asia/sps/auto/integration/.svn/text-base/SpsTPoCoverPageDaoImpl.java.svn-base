/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/11/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain;


/**
 * A "Dao" implementation class of "SpsTPoCoverPage"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/11/28 09:54:32<br />
 * 
 * This module generated automatically in 2014/11/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoCoverPageDaoImpl extends SqlMapClientDaoSupport implements SpsTPoCoverPageDao {

    /**
     * Default constructor
     */
    public SpsTPoCoverPageDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public SpsTPoCoverPageDomain searchByKey(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPage.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoCoverPageDomain> searchByCondition(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoCoverPageDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoCoverPage.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoCoverPageDomain> searchByConditionForPaging(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoCoverPageDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPoCoverPage.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int searchCount(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPage.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    @Deprecated
    public SpsTPoCoverPageDomain searchByKeyForChange(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPage.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public SpsTPoCoverPageDomain lockByKey(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPoCoverPage.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public SpsTPoCoverPageDomain lockByKeyNoWait(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoCoverPageDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTPoCoverPage.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain)
     */
    public void create(SpsTPoCoverPageDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTPoCoverPage.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain)
     */
    public int update(SpsTPoCoverPageDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTPoCoverPage.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoCoverPageDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int updateByCondition(SpsTPoCoverPageDomain domain, SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTPoCoverPage.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int delete(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoCoverPage.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoCoverPageDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCoverPageCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoCoverPageCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPoCoverPage.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
