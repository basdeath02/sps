/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;


/**
 * A "Dao" implementation class of "SpsMUser"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserDaoImpl extends SqlMapClientDaoSupport implements SpsMUserDao {

    /**
     * Default constructor
     */
    public SpsMUserDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public SpsMUserDomain searchByKey(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUser.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserDomain> searchByCondition(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUser.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserDomain> searchByConditionForPaging(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUser.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int searchCount(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMUser.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    @Deprecated
    public SpsMUserDomain searchByKeyForChange(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUser.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public SpsMUserDomain lockByKey(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUser.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public SpsMUserDomain lockByKeyNoWait(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMUser.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain)
     */
    public void create(SpsMUserDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMUser.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain)
     */
    public int update(SpsMUserDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMUser.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int updateByCondition(SpsMUserDomain domain, SpsMUserCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMUser.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int delete(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUser.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUser.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
