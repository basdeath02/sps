/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;


/**
 * A "Dao" implementation class of "SpsCigmaChgDoError"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsCigmaChgDoErrorDaoImpl extends SqlMapClientDaoSupport implements SpsCigmaChgDoErrorDao {

    /**
     * Default constructor
     */
    public SpsCigmaChgDoErrorDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public SpsCigmaChgDoErrorDomain searchByKey(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgDoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgDoError.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaChgDoErrorDomain> searchByCondition(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaChgDoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaChgDoError.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsCigmaChgDoErrorDomain> searchByConditionForPaging(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsCigmaChgDoErrorDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsCigmaChgDoError.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int searchCount(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgDoError.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    @Deprecated
    public SpsCigmaChgDoErrorDomain searchByKeyForChange(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgDoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgDoError.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public SpsCigmaChgDoErrorDomain lockByKey(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgDoErrorDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsCigmaChgDoError.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public SpsCigmaChgDoErrorDomain lockByKeyNoWait(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return (SpsCigmaChgDoErrorDomain)getSqlMapClientTemplate()
                .queryForObject("SpsCigmaChgDoError.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain)
     */
    public void create(SpsCigmaChgDoErrorDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsCigmaChgDoError.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain)
     */
    public int update(SpsCigmaChgDoErrorDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsCigmaChgDoError.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int updateByCondition(SpsCigmaChgDoErrorDomain domain, SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsCigmaChgDoError.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int delete(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaChgDoError.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsCigmaChgDoErrorDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsCigmaChgDoErrorCriteriaDomain)
     */
    public int deleteByCondition(SpsCigmaChgDoErrorCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsCigmaChgDoError.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
