/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;


/**
 * A "Dao" implementation class of "SpsMUserDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 11:25:05<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserDensoDaoImpl extends SqlMapClientDaoSupport implements SpsMUserDensoDao {

    /**
     * Default constructor
     */
    public SpsMUserDensoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public SpsMUserDensoDomain searchByKey(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserDenso.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserDensoDomain> searchByCondition(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUserDenso.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserDensoDomain> searchByConditionForPaging(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUserDenso.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int searchCount(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserDenso.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    @Deprecated
    public SpsMUserDensoDomain searchByKeyForChange(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserDenso.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public SpsMUserDensoDomain lockByKey(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserDenso.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public SpsMUserDensoDomain lockByKeyNoWait(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMUserDensoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMUserDenso.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain)
     */
    public void create(SpsMUserDensoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMUserDenso.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain)
     */
    public int update(SpsMUserDensoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMUserDenso.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int updateByCondition(SpsMUserDensoDomain domain, SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMUserDenso.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int delete(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUserDenso.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserDensoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUserDenso.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
