/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/26       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;


/**
 * A "Dao" implementation class of "SpsMCompanyDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/26 14:21:53<br />
 * 
 * This module generated automatically in 2015/05/26 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanyDensoDaoImpl extends SqlMapClientDaoSupport implements SpsMCompanyDensoDao {

    /**
     * Default constructor
     */
    public SpsMCompanyDensoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public SpsMCompanyDensoDomain searchByKey(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDenso.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCompanyDensoDomain> searchByCondition(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCompanyDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCompanyDenso.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCompanyDensoDomain> searchByConditionForPaging(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCompanyDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCompanyDenso.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int searchCount(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDenso.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    @Deprecated
    public SpsMCompanyDensoDomain searchByKeyForChange(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDenso.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public SpsMCompanyDensoDomain lockByKey(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanyDenso.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public SpsMCompanyDensoDomain lockByKeyNoWait(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanyDensoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMCompanyDenso.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public void create(SpsMCompanyDensoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMCompanyDenso.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    public int update(SpsMCompanyDensoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMCompanyDenso.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int updateByCondition(SpsMCompanyDensoDomain domain, SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMCompanyDenso.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int delete(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCompanyDenso.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanyDensoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanyDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsMCompanyDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCompanyDenso.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
