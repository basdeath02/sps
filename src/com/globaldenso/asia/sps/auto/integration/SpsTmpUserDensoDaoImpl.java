/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2016/02/10       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain;


/**
 * A "Dao" implementation class of "SpsTmpUserDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2016/02/10 12:59:09<br />
 * 
 * This module generated automatically in 2016/02/10 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTmpUserDensoDaoImpl extends SqlMapClientDaoSupport implements SpsTmpUserDensoDao {

    /**
     * Default constructor
     */
    public SpsTmpUserDensoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public SpsTmpUserDensoDomain searchByKey(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserDenso.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUserDensoDomain> searchByCondition(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUserDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUserDenso.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTmpUserDensoDomain> searchByConditionForPaging(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTmpUserDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTmpUserDenso.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int searchCount(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserDenso.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    @Deprecated
    public SpsTmpUserDensoDomain searchByKeyForChange(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserDenso.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public SpsTmpUserDensoDomain lockByKey(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTmpUserDenso.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public SpsTmpUserDensoDomain lockByKeyNoWait(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTmpUserDensoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTmpUserDenso.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain)
     */
    public void create(SpsTmpUserDensoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTmpUserDenso.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain)
     */
    public int update(SpsTmpUserDensoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTmpUserDenso.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTmpUserDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int updateByCondition(SpsTmpUserDensoDomain domain, SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTmpUserDenso.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int delete(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUserDenso.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTmpUserDensoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTmpUserDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsTmpUserDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTmpUserDenso.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
