/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/09/23       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import com.globaldenso.asia.sps.auto.business.dao.PoIdSeqDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import java.math.BigDecimal;


/**
 * A "Dao" implementation class of "PoIdSeq"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/09/23 16:04:50<br />
 * 
 * This module generated automatically in 2014/09/23 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class PoIdSeqDaoImpl extends SqlMapClientDaoSupport implements PoIdSeqDao {

    /**
     * Default constructor
     */
    public PoIdSeqDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.PoIdSeqDao#getNextValue()
     */
    public BigDecimal getNextValue() throws ApplicationException {
        return (BigDecimal)getSqlMapClientTemplate().queryForObject("PoIdSeq.GetNextValue");
    }

}
