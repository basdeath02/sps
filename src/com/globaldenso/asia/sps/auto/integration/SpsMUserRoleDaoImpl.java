/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain;


/**
 * A "Dao" implementation class of "SpsMUserRole"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUserRoleDaoImpl extends SqlMapClientDaoSupport implements SpsMUserRoleDao {

    /**
     * Default constructor
     */
    public SpsMUserRoleDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserRoleDomain> searchByCondition(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserRoleDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUserRole.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUserRoleDomain> searchByConditionForPaging(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUserRoleDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUserRole.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public int searchCount(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMUserRole.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain)
     */
    public void create(SpsMUserRoleDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMUserRole.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUserRoleDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public int updateByCondition(SpsMUserRoleDomain domain, SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMUserRole.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUserRoleDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain)
     */
    public int deleteByCondition(SpsMUserRoleCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUserRole.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
