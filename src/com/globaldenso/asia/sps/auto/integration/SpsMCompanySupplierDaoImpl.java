/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/29       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain;


/**
 * A "Dao" implementation class of "SpsMCompanySupplier"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/29 18:51:48<br />
 * 
 * This module generated automatically in 2014/10/29 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCompanySupplierDaoImpl extends SqlMapClientDaoSupport implements SpsMCompanySupplierDao {

    /**
     * Default constructor
     */
    public SpsMCompanySupplierDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public SpsMCompanySupplierDomain searchByKey(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanySupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanySupplier.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCompanySupplierDomain> searchByCondition(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCompanySupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCompanySupplier.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCompanySupplierDomain> searchByConditionForPaging(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCompanySupplierDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCompanySupplier.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int searchCount(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanySupplier.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    @Deprecated
    public SpsMCompanySupplierDomain searchByKeyForChange(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanySupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanySupplier.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public SpsMCompanySupplierDomain lockByKey(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanySupplierDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCompanySupplier.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public SpsMCompanySupplierDomain lockByKeyNoWait(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCompanySupplierDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMCompanySupplier.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain)
     */
    public void create(SpsMCompanySupplierDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMCompanySupplier.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain)
     */
    public int update(SpsMCompanySupplierDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMCompanySupplier.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCompanySupplierDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int updateByCondition(SpsMCompanySupplierDomain domain, SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMCompanySupplier.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int delete(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCompanySupplier.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCompanySupplierDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCompanySupplierCriteriaDomain)
     */
    public int deleteByCondition(SpsMCompanySupplierCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCompanySupplier.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
