/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;


/**
 * A "Dao" implementation class of "SpsMPlantDenso"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMPlantDensoDaoImpl extends SqlMapClientDaoSupport implements SpsMPlantDensoDao {

    /**
     * Default constructor
     */
    public SpsMPlantDensoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public SpsMPlantDensoDomain searchByKey(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantDenso.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMPlantDensoDomain> searchByCondition(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMPlantDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMPlantDenso.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMPlantDensoDomain> searchByConditionForPaging(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMPlantDensoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMPlantDenso.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int searchCount(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantDenso.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    @Deprecated
    public SpsMPlantDensoDomain searchByKeyForChange(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantDenso.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public SpsMPlantDensoDomain lockByKey(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantDensoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMPlantDenso.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public SpsMPlantDensoDomain lockByKeyNoWait(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return (SpsMPlantDensoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMPlantDenso.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain)
     */
    public void create(SpsMPlantDensoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMPlantDenso.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain)
     */
    public int update(SpsMPlantDensoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMPlantDenso.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int updateByCondition(SpsMPlantDensoDomain domain, SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMPlantDenso.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int delete(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMPlantDenso.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMPlantDensoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMPlantDensoCriteriaDomain)
     */
    public int deleteByCondition(SpsMPlantDensoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMPlantDenso.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
