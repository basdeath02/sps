/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;


/**
 * A "Dao" implementation class of "SpsTCn"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTCnDaoImpl extends SqlMapClientDaoSupport implements SpsTCnDao {

    /**
     * Default constructor
     */
    public SpsTCnDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public SpsTCnDomain searchByKey(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTCn.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTCnDomain> searchByCondition(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTCnDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTCn.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTCnDomain> searchByConditionForPaging(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTCnDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTCn.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int searchCount(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTCn.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    @Deprecated
    public SpsTCnDomain searchByKeyForChange(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTCn.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public SpsTCnDomain lockByKey(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTCn.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public SpsTCnDomain lockByKeyNoWait(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTCn.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain)
     */
    public void create(SpsTCnDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTCn.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain)
     */
    public int update(SpsTCnDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTCn.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int updateByCondition(SpsTCnDomain domain, SpsTCnCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTCn.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int delete(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTCn.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain)
     */
    public int deleteByCondition(SpsTCnCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTCn.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
