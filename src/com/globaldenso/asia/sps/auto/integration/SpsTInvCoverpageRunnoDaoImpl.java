/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/05/18       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain;


/**
 * A "Dao" implementation class of "SpsTInvCoverpageRunno"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/05/18 14:44:35<br />
 * 
 * This module generated automatically in 2015/05/18 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTInvCoverpageRunnoDaoImpl extends SqlMapClientDaoSupport implements SpsTInvCoverpageRunnoDao {

    /**
     * Default constructor
     */
    public SpsTInvCoverpageRunnoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public SpsTInvCoverpageRunnoDomain searchByKey(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvCoverpageRunnoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvCoverpageRunno.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTInvCoverpageRunnoDomain> searchByCondition(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTInvCoverpageRunnoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTInvCoverpageRunno.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTInvCoverpageRunnoDomain> searchByConditionForPaging(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTInvCoverpageRunnoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTInvCoverpageRunno.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int searchCount(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvCoverpageRunno.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    @Deprecated
    public SpsTInvCoverpageRunnoDomain searchByKeyForChange(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvCoverpageRunnoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvCoverpageRunno.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public SpsTInvCoverpageRunnoDomain lockByKey(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvCoverpageRunnoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTInvCoverpageRunno.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public SpsTInvCoverpageRunnoDomain lockByKeyNoWait(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTInvCoverpageRunnoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTInvCoverpageRunno.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain)
     */
    public void create(SpsTInvCoverpageRunnoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTInvCoverpageRunno.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain)
     */
    public int update(SpsTInvCoverpageRunnoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTInvCoverpageRunno.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTInvCoverpageRunnoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int updateByCondition(SpsTInvCoverpageRunnoDomain domain, SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTInvCoverpageRunno.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int delete(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTInvCoverpageRunno.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTInvCoverpageRunnoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvCoverpageRunnoCriteriaDomain)
     */
    public int deleteByCondition(SpsTInvCoverpageRunnoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTInvCoverpageRunno.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
