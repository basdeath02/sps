/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain;


/**
 * A "Dao" implementation class of "SpsMCurrency"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMCurrencyDaoImpl extends SqlMapClientDaoSupport implements SpsMCurrencyDao {

    /**
     * Default constructor
     */
    public SpsMCurrencyDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public SpsMCurrencyDomain searchByKey(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCurrencyDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCurrency.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCurrencyDomain> searchByCondition(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCurrencyDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCurrency.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMCurrencyDomain> searchByConditionForPaging(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMCurrencyDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMCurrency.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int searchCount(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMCurrency.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    @Deprecated
    public SpsMCurrencyDomain searchByKeyForChange(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCurrencyDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCurrency.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public SpsMCurrencyDomain lockByKey(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCurrencyDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMCurrency.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public SpsMCurrencyDomain lockByKeyNoWait(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return (SpsMCurrencyDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMCurrency.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain)
     */
    public void create(SpsMCurrencyDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMCurrency.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain)
     */
    public int update(SpsMCurrencyDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMCurrency.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMCurrencyDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int updateByCondition(SpsMCurrencyDomain domain, SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMCurrency.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int delete(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCurrency.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMCurrencyDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMCurrencyCriteriaDomain)
     */
    public int deleteByCondition(SpsMCurrencyCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMCurrency.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
