/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/19       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;


/**
 * A "Dao" implementation class of "SpsTPo"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/19 09:39:10<br />
 * 
 * This module generated automatically in 2015/02/19 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTPoDaoImpl extends SqlMapClientDaoSupport implements SpsTPoDao {

    /**
     * Default constructor
     */
    public SpsTPoDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public SpsTPoDomain searchByKey(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPo.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoDomain> searchByCondition(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPo.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTPoDomain> searchByConditionForPaging(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTPoDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTPo.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int searchCount(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTPo.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    @Deprecated
    public SpsTPoDomain searchByKeyForChange(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPo.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public SpsTPoDomain lockByKey(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTPo.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public SpsTPoDomain lockByKeyNoWait(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return (SpsTPoDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTPo.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public void create(SpsTPoDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTPo.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public int update(SpsTPoDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTPo.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int updateByCondition(SpsTPoDomain domain, SpsTPoCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTPo.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int delete(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPo.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTPoDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTPoCriteriaDomain)
     */
    public int deleteByCondition(SpsTPoCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTPo.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
