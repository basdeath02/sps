/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain;


/**
 * A "Dao" implementation class of "SpsMScreen"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMScreenDaoImpl extends SqlMapClientDaoSupport implements SpsMScreenDao {

    /**
     * Default constructor
     */
    public SpsMScreenDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public SpsMScreenDomain searchByKey(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMScreenDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMScreen.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMScreenDomain> searchByCondition(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMScreenDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMScreen.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMScreenDomain> searchByConditionForPaging(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMScreenDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMScreen.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int searchCount(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMScreen.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    @Deprecated
    public SpsMScreenDomain searchByKeyForChange(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMScreenDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMScreen.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public SpsMScreenDomain lockByKey(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMScreenDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMScreen.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public SpsMScreenDomain lockByKeyNoWait(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return (SpsMScreenDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMScreen.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain)
     */
    public void create(SpsMScreenDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMScreen.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain)
     */
    public int update(SpsMScreenDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMScreen.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMScreenDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int updateByCondition(SpsMScreenDomain domain, SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMScreen.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int delete(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMScreen.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMScreenDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMScreenCriteriaDomain)
     */
    public int deleteByCondition(SpsMScreenCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMScreen.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
