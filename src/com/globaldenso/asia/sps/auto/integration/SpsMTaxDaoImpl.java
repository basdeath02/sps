/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain;


/**
 * A "Dao" implementation class of "SpsMTax"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMTaxDaoImpl extends SqlMapClientDaoSupport implements SpsMTaxDao {

    /**
     * Default constructor
     */
    public SpsMTaxDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public SpsMTaxDomain searchByKey(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return (SpsMTaxDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMTax.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMTaxDomain> searchByCondition(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMTaxDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMTax.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMTaxDomain> searchByConditionForPaging(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMTaxDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMTax.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int searchCount(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMTax.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    @Deprecated
    public SpsMTaxDomain searchByKeyForChange(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return (SpsMTaxDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMTax.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public SpsMTaxDomain lockByKey(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return (SpsMTaxDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMTax.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public SpsMTaxDomain lockByKeyNoWait(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return (SpsMTaxDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMTax.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain)
     */
    public void create(SpsMTaxDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMTax.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain)
     */
    public int update(SpsMTaxDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMTax.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMTaxDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int updateByCondition(SpsMTaxDomain domain, SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMTax.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int delete(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMTax.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMTaxDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMTaxCriteriaDomain)
     */
    public int deleteByCondition(SpsMTaxCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMTax.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
