/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;


/**
 * A "Dao" implementation class of "SpsMRoleType"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMRoleTypeDaoImpl extends SqlMapClientDaoSupport implements SpsMRoleTypeDao {

    /**
     * Default constructor
     */
    public SpsMRoleTypeDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public SpsMRoleTypeDomain searchByKey(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleTypeDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleType.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleTypeDomain> searchByCondition(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleTypeDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRoleType.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMRoleTypeDomain> searchByConditionForPaging(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMRoleTypeDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMRoleType.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int searchCount(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleType.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    @Deprecated
    public SpsMRoleTypeDomain searchByKeyForChange(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleTypeDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleType.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public SpsMRoleTypeDomain lockByKey(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleTypeDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsMRoleType.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public SpsMRoleTypeDomain lockByKeyNoWait(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return (SpsMRoleTypeDomain)getSqlMapClientTemplate()
                .queryForObject("SpsMRoleType.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain)
     */
    public void create(SpsMRoleTypeDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMRoleType.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain)
     */
    public int update(SpsMRoleTypeDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsMRoleType.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int updateByCondition(SpsMRoleTypeDomain domain, SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMRoleType.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int delete(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRoleType.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMRoleTypeDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMRoleTypeCriteriaDomain)
     */
    public int deleteByCondition(SpsMRoleTypeCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMRoleType.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
