/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/01/28       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain;


/**
 * A "Dao" implementation class of "SpsTCnDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/01/28 10:05:00<br />
 * 
 * This module generated automatically in 2015/01/28 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTCnDetailDaoImpl extends SqlMapClientDaoSupport implements SpsTCnDetailDao {

    /**
     * Default constructor
     */
    public SpsTCnDetailDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public SpsTCnDetailDomain searchByKey(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTCnDetail.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTCnDetailDomain> searchByCondition(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTCnDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTCnDetail.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTCnDetailDomain> searchByConditionForPaging(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTCnDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTCnDetail.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int searchCount(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTCnDetail.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTCnDetailDomain searchByKeyForChange(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTCnDetail.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public SpsTCnDetailDomain lockByKey(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTCnDetail.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public SpsTCnDetailDomain lockByKeyNoWait(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTCnDetailDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTCnDetail.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain)
     */
    public void create(SpsTCnDetailDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTCnDetail.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain)
     */
    public int update(SpsTCnDetailDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTCnDetail.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTCnDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTCnDetailDomain domain, SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTCnDetail.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int delete(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTCnDetail.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTCnDetailDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTCnDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTCnDetail.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
