/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2015/02/04       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;


/**
 * A "Dao" implementation class of "SpsTDoDetail"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2015/02/04 14:30:39<br />
 * 
 * This module generated automatically in 2015/02/04 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsTDoDetailDaoImpl extends SqlMapClientDaoSupport implements SpsTDoDetailDao {

    /**
     * Default constructor
     */
    public SpsTDoDetailDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#searchByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public SpsTDoDetailDomain searchByKey(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoDetail.SearchByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTDoDetailDomain> searchByCondition(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTDoDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTDoDetail.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsTDoDetailDomain> searchByConditionForPaging(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsTDoDetailDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsTDoDetail.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int searchCount(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoDetail.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#searchByKeyForChange(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    @Deprecated
    public SpsTDoDetailDomain searchByKeyForChange(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoDetail.SearchByKeyForChange", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#lockByKey(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public SpsTDoDetailDomain lockByKey(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDetailDomain)getSqlMapClientTemplate()
                    .queryForObject("SpsTDoDetail.LockByKey", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#lockByKeyNoWait(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public SpsTDoDetailDomain lockByKeyNoWait(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return (SpsTDoDetailDomain)getSqlMapClientTemplate()
                .queryForObject("SpsTDoDetail.LockByKeyNoWait", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain)
     */
    public void create(SpsTDoDetailDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsTDoDetail.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#update(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain)
     */
    public int update(SpsTDoDetailDomain domain) throws ApplicationException {
        return getSqlMapClientTemplate()
                .update("SpsTDoDetail.Update", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int updateByCondition(SpsTDoDetailDomain domain, SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsTDoDetail.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#delete(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int delete(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTDoDetail.Delete", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsTDoDetailDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTDoDetailCriteriaDomain)
     */
    public int deleteByCondition(SpsTDoDetailCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsTDoDetail.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
