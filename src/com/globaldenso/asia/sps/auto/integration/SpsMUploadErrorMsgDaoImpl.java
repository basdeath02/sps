/*
 * Project : SPS
 * 
 * 
 * Version.   Date of update   Person in charge of update   Details of update
 * 1.0.0      2014/10/07       CSI                             New
 * 
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
 */
package com.globaldenso.asia.sps.auto.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain;


/**
 * A "Dao" implementation class of "SpsMUploadErrorMsg"
 * 
 * <br />
 * SACT Version: 1.1.1<br />
 * Date of generation: 2014/10/07 11:18:40<br />
 * 
 * This module generated automatically in 2014/10/07 according to the table definition
 * 
 * @author $Author$ (Auto)
 * @version $Revision$
 */
public class SpsMUploadErrorMsgDaoImpl extends SqlMapClientDaoSupport implements SpsMUploadErrorMsgDao {

    /**
     * Default constructor
     */
    public SpsMUploadErrorMsgDaoImpl() {
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao#searchByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUploadErrorMsgDomain> searchByCondition(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUploadErrorMsgDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUploadErrorMsg.SearchByCondition", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao#searchByConditionForPaging(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    @SuppressWarnings("unchecked")
    public List<SpsMUploadErrorMsgDomain> searchByConditionForPaging(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return (List<SpsMUploadErrorMsgDomain>)getSqlMapClientTemplate()
                    .queryForList("SpsMUploadErrorMsg.SearchByConditionForPaging", criteria);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao#searchCount(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public int searchCount(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        int cnt = (Integer)getSqlMapClientTemplate()
                    .queryForObject("SpsMUploadErrorMsg.SearchCount", criteria);
        return cnt;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao#create(com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain)
     */
    public void create(SpsMUploadErrorMsgDomain domain) throws ApplicationException {
        getSqlMapClientTemplate()
                .insert("SpsMUploadErrorMsg.Create", domain);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao#updateByCondition(com.globaldenso.asia.sps.auto.business.domain.SpsMUploadErrorMsgDomain, com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public int updateByCondition(SpsMUploadErrorMsgDomain domain, SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        Map<String, Object> updateByConditionMap = getUpdateByConditionMap(domain, criteria);
        return getSqlMapClientTemplate()
                .update("SpsMUploadErrorMsg.UpdateByCondition", updateByConditionMap);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.auto.business.dao.SpsMUploadErrorMsgDao#deleteByCondition(com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUploadErrorMsgCriteriaDomain)
     */
    public int deleteByCondition(SpsMUploadErrorMsgCriteriaDomain criteria) throws ApplicationException {
        return getSqlMapClientTemplate()
                .delete("SpsMUploadErrorMsg.DeleteByCondition", criteria);
    }

    /**
     * Generate the conditional update parameters in a form of "Map".
     * 
     * @param domain Domain to be updated
     * @param criteria Domain of update condition
     * @return "Map" of the conditional update parameters
     */
    private Map<String, Object> getUpdateByConditionMap(Object domain, Object criteria) {
        
        Map<String, Object> updateByConditionMap = new HashMap<String, Object>();
        
        // Set an update item in a "Map".
        updateByConditionMap.put("condition", criteria);
        // Set an update condition in a "Map".
        updateByConditionMap.put("domain", domain);
        
        return updateByConditionMap;
    }

}
