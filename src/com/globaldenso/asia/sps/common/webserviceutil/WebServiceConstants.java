/*
 * ModifyDate Development company     Describe 
 * 2014/03/13 CSI Akat                Create
 * 2015/12/02 CSI Akat                [IN037]
 * 2017/10/12 Netband Rungsiwut       SPS Phase II
 * 2018/12/20 CTC P.Pawan             [IN1744]Extract Lib name to database.
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.webserviceutil;

/**
 * Constants for Web Service
 * 
 * @author CSI Akat
 * @version 1.0.0
 * */
public class WebServiceConstants {

    /**
     * WebService Timestamp型の入力フォーマット
     */
    public static final String WS_TIMESTAMP_FORMAT = "yyyy-MM-dd hh:mm:dd.SSS";
    
    /** The Resource name ASN. */
    public static final String ASN_RESOURCE = "asn";
    
    /** The Resource name ASN Batch. */
    public static final String ASN_BATCH_RESOURCE = "asnBatch";
    
    /** The Resource Path name for Invoice. */
    public static final String INVOICE_RESOURCE = "invoice";
    
    /** The Resource Path name for Purchase Price Master. */
    public static final String PURCHASE_PRICE_MASTER_RESOURCE = "purchasePriceMaster";

    /** The Resource Path name for AP Interface. */
    public static final String APINTERFACE_RESOURCE = "apInterface";
    
    /** The Resource Path name for CIGMA P/O Resource. */
    public static final String CIGMA_PO_RESOURCE = "cigmaPo";
    
    /** The Resource Path name for CIGMA P/O Resource. */
    public static final String CIGMA_PO_DN_RESOURCE = "cigmaPoDN";
    
    /** The Resource Path name for CIGMA D/O Resource. */
    public static final String CIGMA_DO_DN_RESOURCE = "cigmaDoDN";
    
    /** The Resource Path name for CIGMA Change P/O Resource. */
    public static final String CIGMA_CHG_PO_RESOURCE = "cigmaChgPo";
    
    /** The Resource Path name for CIGMA P/O Cover Page Resource. */
    public static final String CIGMA_PO_COVER_PAGE_RESOURCE = "cigmaPoCoverPage";

    // [IN056-2] Add new resource
    /** The Resource Path name for CIGMA Purchase Order Vendor Code. */
    public static final String CIGMA_PO_VENDOR_CODE_RESOURCE = "cigmaPoVendorCode";
    
    /** The Resource Path name for CIGMA D/O Resource. */
    public static final String CIGMA_DO_RESOURCE = "cigmaDo";
    
    /** The Resource Path name for CIGMA Change D/O Resource. */
    public static final String CIGMA_CHG_DO_RESOURCE = "cigmaChgDo";
    
    /** The Resource Path name for CIGMA Change D/O Resource. */
    public static final String CIGMA_KB_RESOURCE = "cigmaKb";
    
    /** The parameter name LPar Name. */
    public static final String PARAM_LPAR_NAME = "lparName";
    
    /** The parameter name Schema Name. */
    public static final String PARAM_SCHEMA_NAME = "schemaName";
    
    /** The parameter name Schema Name Other 1. */
    public static final String PARAM_SCHEMA_NAME_OTHER_1 = "schemaNameOther1";

    /** The parameter name Schema Name for JDE. */
    public static final String PARAM_SCHEMA_NAME_JDE = "schemaNameJde";
    
    /** The parameter name Schema Name Share. */
    public static final String PARAM_SCHEMA_NAME_SHARE = "schemaNameShare";
    
    /** The parameter name Schema Name Replicate. */
    public static final String PARAM_SCHEMA_NAME_REPLICATE = "schemaNameReplicate";

    /** The parameter name Connection Number. */
    public static final String PARAM_CONNECTION_NO = "connectionNo";
    
    /** The parameter name DENSO Code. */
    public static final String PARAM_DENSO_CODE = "densoCode";
    
    /** The parameter name PO Status. */
    public static final String PARAM_PO_STATUS = "poStatus";

    /** The parameter name Order Type */
    public static final String PARAM_ORDER_TYPE = "orderType";

    /** The parameter name Supplier Plant Code. */
    public static final String PARAM_SUPPLIER_PLANT_CODE = "supplierPlantCode";

    /** The parameter name DENSO Plant Code. */
    public static final String PARAM_DENSO_PLANT_CODE = "densoPlantCode";

    /** The parameter name PO No. */
    public static final String PARAM_PO_NO = "poNo";

    /** The parameter name Issue Date From. */
    public static final String PARAM_ISSUE_DATE_FROM = "issueDateFrom";

    /** The parameter name Issue Date To. */
    public static final String PARAM_ISSUE_DATE_TO = "issueDateTo";
    
    /** The parameter name Due Date From. */
    public static final String PARAM_DUE_DATE_FROM = "dueDateFrom";

    /** The parameter name Due Date To. */
    public static final String PARAM_DUE_DATE_TO = "dueDateTo";
    
    /** The parameter name Row Number From. */
    public static final String PARAM_ROW_NUM_FROM = "rowNumFrom";

    /** The parameter name Row Number To. */
    public static final String PARAM_ROW_NUM_TO = "rowNumTo";
    
    //For batch search send ASN to CIGMA.
    /** The parameter name Supplier Code Header. */
    public static final String PARAM_SUPPLIER_CODE_HEADER = "supplierCodeHeader";
    
    /** The parameter name ASN No Header. */
    public static final String PARAM_ASN_NO_HEADER = "asnNoHeader";
    
    /** The parameter name Receiving Status Header. */
    public static final String PARAM_RECEIVING_STATUS_HEADER = "receivingStatusHeader";
    
    /** The parameter name Plant Denso Code Header. */
    public static final String PARAM_DPCD_HEADER = "dPcdHeader";
    
    /** The parameter name Actual Etd Date. */
    public static final String PARAM_ACTUAL_ETD_DATE = "actualEtdDate";
    
    /** The parameter name Actual Etd Time. */
    public static final String PARAM_ACTUAL_ETD_TIME = "actualEtdTime";
    
    /** The parameter name Plan Eta Date. */
    public static final String PARAM_PLAN_ETA_DATE = "planEtaDate";
    
    /** The parameter name  Plan Eta Time. */
    public static final String PARAM_PLAN_ETA_TIME = "planEtaTime";
    
    /** The parameter name Create Date Header. */
    public static final String PARAM_CREATE_DATE_HEADER = "createDateHeader";
    
    /** The parameter name Create Time Header. */
    public static final String PARAM_CREATE_TIME_HEADER = "createTimeHeader";
    
    /** The parameter name Update By Header. */
    public static final String PARAM_UPDATE_BY_HEADER = "updateByHeader";
    
    /** The parameter name Update Date Header. */
    public static final String PARAM_UPDATE_DATE_HEADER = "updateDateHeader";
    
    /** The parameter name Update Time Header. */
    public static final String PARAM_UPDATE_TIME_HEADER = "updateTimeHeader";
    
    /** The parameter name Process Flag. */
    public static final String PARAM_PROCESS_FLAG = "processFlag";
    
    /** The parameter name Sps Asn Status. */
    public static final String PARAM_SPS_ASN_STATUS = "spsAsnStatus";
    
    /** The parameter name Number of Pallet. */
    public static final String PARAM_NUMBER_OF_PALLET = "numberOfPallet";
    
    /** The parameter name Trip No. */
    public static final String PARAM_TRIP_NO = "tripNo";
    
    /** The parameter name Supplier Code detail. */
    public static final String PARAM_SUPPLIER_CODE_DETAIL = "supplierCodeDetail";
    
    /** The parameter name Plant Denso Code Detail. */
    public static final String PARAM_DPCD_DETAIL = "dPcdDetail";
    
    /** The parameter name ASN No Detail. */
    public static final String PARAM_ASN_NO_DETAIL = "asnNoDetail";
    
    /** The parameter name Cigma Current DO No. */
    public static final String PARAM_CIGMA_CURRENT_DO_NO = "cigmaCurrentDoNo";
    
    /** The parameter name Cigma Original DO No. */
    public static final String PARAM_CIGMA_ORIGINAL_DO_NO = "cigmaOriginalDoNo";
    
    /** The parameter name Receiving Status Detail. */
    public static final String PARAM_RECEIVING_STATUS_DETAIL = "receivingStatusDetail";
    
    /** The parameter name Part Number. */
    public static final String PARAM_PART_NUMBER = "partNumber";
    
    /** The parameter name SPS Do No. */
    public static final String PARAM_SPS_DO_NO = "spsDoNo";
    
    /** The parameter name Shipping Qty. */
    public static final String PARAM_SHIPPING_QTY = "shippingQty";
    
    /** The parameter name Receiving Qty. */
    public static final String PARAM_RECEIVED_QTY = "receivedQty";
    
    /** The parameter name Remaining Qty. */
    public static final String PARAM_REMAINING_QTY = "remainingQty";
    
    /** The parameter name Var. */
    public static final String PARAM_VAR = "var";
    
    /** The parameter name Ins. */
    public static final String PARAM_INS = "ins";
    
    /** The parameter name Wh. */
    public static final String PARAM_WH = "wh";
    
    /** The parameter name Due Date. */
    public static final String PARAM_DUE_DATE = "dueDate";
    
    /** The parameter name Delivery Date. */
    public static final String PARAM_DELIVERY_DATE = "deliveryDate";
    
    /** The parameter name Rcv Lane. */
    public static final String PARAM_RCV_LANE = "rcvLane";
    
    /** The parameter name Received Date. */
    public static final String PARAM_RECEIVED_DATE = "receivedDate";
    
    /** The parameter name Create Date Detail. */
    public static final String PARAM_CREATE_DATE_DETAIL = "createDateDetail";
    
    /** The parameter name Create Time Detail. */
    public static final String PARAM_CREATE_TIME_DETAIL = "createTimeDetail";
    
    /** The parameter name Update By Detail. */
    public static final String PARAM_UPDATE_BY_DETAIL = "updateByDetail";
    
    /** The parameter name Update Date Detail. */
    public static final String PARAM_UPDATE_DATE_DETAIL = "updateDateDetail";
    
    /** The parameter name Update Time Detail. */
    public static final String PARAM_UPDATE_TIME_DETAIL = "updateTimeDetail";
    
    /** The parameter name Qty Box. */
    public static final String PARAM_QTY_BOX = "qtyBox";
    
    /** The parameter name Unit Of Measure. */
    public static final String PARAM_UNIT_OF_MEASURE = "unitOfMeasure";
    
    /** The parameter name Change Reason Cd. */
    public static final String PARAM_CHANGE_REASON_CD = "changeReasonCd";
    
    /** The parameter name Data Scope Date. */
    public static final String PARAM_DATA_SCOPE_DATE = "dataScopeDate";
    
    /** The parameter name Last Update Date. */
    public static final String PARAM_LAST_UPDATE = "lastUpdate";
    
    /** The parameter name Last Update Time. */
    public static final String PARAM_LAST_UPDATE_TIME = "lastUpdateTime";
    
    /** The parameter name Pn Last Update Date. */
    public static final String PARAM_PN_LAST_UPDATE = "pnLastUpdate";
    
    /** The parameter name Pn Last Update Time. */
    public static final String PARAM_PN_LAST_UPDATE_TIME = "pnLastUpdateTime";
    
    /** The parameter name Pn Process Flag. */
    public static final String PARAM_PN_PROCESS_FLAG = "pnProcessFlag"; 
    
    /** The parameter name ASN No. */
    public static final String PARAM_ASN_NO = "asnNo";
    
    /** The parameter name ASN Status. */
    public static final String PARAM_ASN_STATUS = "asnStatus";
    
    /** The parameter name ASN Status. */
    public static final String PARAM_INVOICE_ID = "invoiceId";
    
    /** The parameter name ASN Status. */
    public static final String PARAM_ASN_NO_IN = "asnNoIn";
    
    /** The parameter name DENSO Code. */
    public static final String PARAM_D_CODE = "dCode";
    
    /** The parameter name CIGMA Supplier Code. */
    public static final String PARAM_CIGMA_SUPPLIER_CODE = "cigmaSupplierCode";
    
    /** The parameter name Header Invoice Number. */
    public static final String PARAM_HEADER_INVOICE_NO = "headerInvoiceNo";
    
    /** The parameter name Header Reference Number. */
    public static final String PARAM_HEADER_REFERENCE_NO = "headerReferenceNo";
    
    /** The parameter name Header Currency Code. */
    public static final String PARAM_HEADER_CURRENCY_CODE = "headerCurrencyCode";
    
    /** The parameter name Header Base Amount. */
    public static final String PARAM_HEADER_BASE_AMOUNT = "headerBaseAmount";
    
    /** The parameter name Header VAT GST Rate. */
    public static final String PARAM_HEADER_VAT_GST_RATE = "headerVatGstRate";
    
    /** The parameter name Header VAT GST Amount. */
    public static final String PARAM_HEADER_VAT_GST_AMOUNT = "headerVatGstAmount";
    
    /** The parameter name Header Total Amount. */
    public static final String PARAM_HEADER_TOTAL_AMOUNT = "headerTotalAmount";
    
    /** The parameter name Header Invoice Date. */
    public static final String PARAM_HEADER_INVOICE_DATE = "headerInvoiceDate";
    
    /** The parameter name Header Update Flag. */
    public static final String PARAM_HEADER_UPDATE_FLAG = "headerUpdateFlag";
    
    /** The parameter name Detail Invoice Number. */
    public static final String PARAM_DETAIL_INVOICE_NO = "detailInvoiceNo";
    
    /** The parameter name Detail ASN Number. */
    public static final String PARAM_DETAIL_ASN_NO = "detailAsnNo";
    
    /** The parameter name Detail CIGMA DO Number. */
    public static final String PARAM_DETAIL_CIGMA_DO_NO = "detailCigmaDoNo";
    
    /** The parameter name Detail DENSO Part Number. */
    public static final String PARAM_DETAIL_D_PART_NO = "detailDPartNo";
    
    /** The parameter name Detail Shipping Quantity. */
    public static final String PARAM_DETAIL_SHIPPING_QTY = "detailShippingQty";
    
    /** The parameter name Detail DENSO Unit Price. */
    public static final String PARAM_DETAIL_D_UNIT_PRICE = "detailDUnitPrice";
    
    /** The parameter name Detail Supplier Unit Price. */
    public static final String PARAM_DETAIL_S_UNIT_PRICE = "detailSUnitPrice";
    
    /** The parameter name Detail Transaction Date. */
    public static final String PARAM_DETAIL_TRANSACTION_DATE = "detailTransactionDate";
    
    /** The parameter name Detail Update Flag. */
    public static final String PARAM_DETAIL_UPDATE_FLAG = "detailUpdateFlag";
    
    /** The parameter name Transaction User. */
    public static final String PARAM_TRANSACT_USER = "transactUser";
    
    /** The parameter name Transaction Date. */
    public static final String PARAM_TRANSACT_DATE = "transactDate";
    
    /** The parameter name Transaction Time. */
    public static final String PARAM_TRANSACT_TIME = "transactTime";
    
    /** The parameter name Vendor Code detail. */
    public static final String PARAM_VENDOR_CODE = "vendorCd";
    
    /** The parameter name Supplier Code detail. */
    public static final String PARAM_EFFECTIVE_DATE = "effectiveDate";
    
    /** The parameter name DENSO part Code. */
    public static final String PARAM_DENSO_PART_CODE = "dPn";
    
    /** The parameter name SPS Flag. */
    public static final String PARAM_SPS_FLAG = "spsFlag";
    
    /** The parameter name UPDATE_USER_ID. */
    public static final String PARAM_UPDATE_USER_ID = "updateByUserId";
    
    /** The parameter name UPDATE_DATE. */
    public static final String PARAM_UPDATE_DATE = "updateDate";
    
    /** The parameter name UPDATE_TIME. */
    public static final String PARAM_UPDATE_TIME = "updateTime";
    
    /** The parameter name SCD. */
    public static final String PARAM_SCD = "sCd";
    
    /** The parameter name DPN. */
    public static final String PARAM_DPN = "dPn";
    
    /** The parameter name DO_SCD. */
    public static final String PARAM_DO_SCD = "doSCd";
    
    /** The parameter name DO_DPN. */
    public static final String PARAM_DO_DPN = "doDPn";
    
    /** The parameter name CREATE_DATE. */
    public static final String PARAM_CREATE_DATE = "createDate";
    
    /** The parameter name CREATE_TIME. */
    public static final String PARAM_CREATE_TIME = "createTime";
    
    /** The parameter name CIGMA_DO NO. */
    public static final String PARAM_CIGMA_DO_NO = "doNumber";
    
    /** The parameter name CIGMA_CHG_DO NO. */
    public static final String PARAM_CIGMA_CHG_DO_NO = "currentDelivery";
    
    /** The parameter name CIGMA_PO NO. */
    public static final String PARAM_CIGMA_PO_NO = "poNumber";
    
    /** The parameter name CIGMA_CHG_PO NO. */
    public static final String PARAM_CIGMA_CHG_PO_NO = "poNumber";
    
    /** The parameter name PARAM_REPORT_TYPE. */
    public static final String PARAM_REPORT_TYPE = "reportType";
    
    /** The parameter name PARAM_SKIP. */
    public static final String PARAM_SKIP = "skip";
    
    /** The parameter name PARAM_MAX_RECORD. */
    public static final String PARAM_MAX_RECORD = "maxRecord";

    /** The parameter name PARAM_MAX_RECORD. */
    public static final String PARAM_KANBAN_SEQ_FLG = "kanbanSeqFlg";
    
    /** The max ASN_NO length for search ASN Receiving Status. */
    public static final int MAX_ASN_NO_LENGTH = 7000;
    
    // [IN037] : select or update data by AS400 member
    /** The parameter name PARAM_MEMBER_NAME. */
    public static final String PARAM_MEMBER_NAME = "memberName";
    
    // Additional from SPS phase II
    /** The constant TYPE_CHRSET_UTF8. */
    public static final String TYPE_CHRSET_UTF8 = ";charset=utf-8";

    /** The Resource Path name for receive resource. */
    public static final String SPS_RECEIVE = "spsReceive";

    /** The Resource Path name for receive upgrade resource. */
    public static final String SPS_UPGRADE = "spsUpgrade";
    
    /** The Resource Path name for receive resource. */
    public static final String CIGMA_RECEIVE = "cigmaReceive";
    
    /** The parameter name PARAM_SUPPLIER_CODE. */
    public static final String PARAM_SUPPLIER_CODE = "supplierCode";
    
    /** The parameter name PARAM_DENSO_COMPANY_CODE. */
    public static final String PARAM_DENSO_COMPANY_CODE = "densoCompanyCode";
    
    /** The parameter name PARAM_KANBAN_QTY. */
    public static final String PARAM_KANBAN_QTY = "kanbanQty";
    
    /** The parameter name PARAM_KANBAN_TAG_SEQ. */
    public static final String PARAM_KANBAN_TAG_SEQ = "kanbanTagSeq";
    
    /** The parameter name PARAM_KANBAN_TAG_SEQ. */
    public static final String PARAM_KANBAN_TYPE = "kanbanType";
    
    /** The parameter name PARAM_RECEIVE_BY_SCAN. */
    public static final String PARAM_RECEIVE_BY_SCAN = "receiveByScan";
    
    /** The parameter name PARAM_KANBAN_TAG_SEQ. */
    public static final String PARAM_TM = "tm";
    
    /** The parameter name PARAM_VER_NO. */
    public static final String PARAM_VER_NO = "verNo";
    
    /** The parameter name PARAM_SYSTEM_ID. */
    public static final String PARAM_SYSTEM_ID = "systemId";
    
    /** The parameter name PARAM_TAG_SCAN_DATETIME. */
    public static final String PARAM_TAG_SCAN_DATETIME = "tagScanDateTime";
    
    /** The parameter name PARAM_RECEIVE_DATE. */
    public static final String PARAM_RECEIVE_DATE = "receiveDate";
    
    /** The parameter name PARAM_DSC_ID. */
    public static final String PARAM_DSC_ID = "dscId";
    
    /** The parameter name PARAM_DEVICE_ID. */
    public static final String PARAM_DEVICE_ID = "deviceId";
    
    /** The parameter name PARAM_ASN_SCAN_DATETIME. */
    public static final String PARAM_ASN_SCAN_DATETIME = "asnScanDateTime";
    
    /** The parameter name PARAM_ALLOCATE_QTY. */
    public static final String PARAM_ALLOCATE_QTY = "allocateQty";
    
    /** The parameter name PARAM_SERVER_NAME. */
    public static final String PARAM_SERVER_NAME = "serverName";
    
    /** The parameter name PARAM_RECEIVE_BATCH_NO. */
    public static final String PARAM_RECEIVE_BATCH_NO = "receiveBatchNo";
    
    /** The parameter name PARAM_TEXT_SEQ_NO. */
    public static final String PARAM_TEXT_SEQ_NO = "textSeqNo";
    
    /** The parameter name PARAM_NUM_OF_TEXT. */
    public static final String PARAM_NUM_OF_TEXT = "numOfText";
    
    /** The parameter name PARAM_RECEIVE_TIME. */
    public static final String PARAM_RECEIVE_TIME = "receiveTime";
    
    /** The parameter name PARAM_PROCESS_STATUS. */
    public static final String PARAM_PROCESS_STATUS = "processStatus";
    
    /** The parameter name PARAM_PROCESS_DATE. */
    public static final String PARAM_PROCESS_DATE = "processDate";
    
    /** The parameter name PARAM_PROCESS_TIME. */
    public static final String PARAM_PROCESS_TIME = "processTime";
    
    /** The parameter name PARAM_PROCESS_TIME. */
    public static final String PARAM_RECEIVE_DATA_MSG = "receiveDataMsg";
    
    /** The parameter name PARAM_AS400_ADDRESS. */
    public static final String PARAM_AS400_ADDRESS = "as400Address";
    
    /** The parameter name PARAM_AS400_ADDRESS. */
    public static final String PARAM_AS400_USERNAME = "as400Username";
    
    /** The parameter name PARAM_AS400_ADDRESS. */
    public static final String PARAM_AS400_PASSWORD = "as400Password";
    
    
    /** The parameter name PO_DATA_DN */
    public static final String PARAM_PO_DATA_DN = "poDataDN";
    
    /** The parameter name DO_DATA_DN */
    public static final String PARAM_DO_DATA_DN = "doDataDN";
    
    /** The parameter name Schema CD. */
    public static final String PARAM_SCHEMA_CD = "schemaCd";
    
    /** The parameter name DO_DATA_DN */
    public static final String PARAM_ASN_DATA_DN = "asnDataDN";
    
    public static final String CIGMA_ASN_DN_RESOURCE = "cigmaAsnDN";
    /**
     * The Default Constructor
     * */
    public WebServiceConstants() {
    }
    
}
