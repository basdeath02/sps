/*
 * ModifyDate Development company     Describe 
 * 2014/06/30  CSI Phakaporn           Create
 * 2015/08/04 CSI Akat                [IN009]
 * 2015/12/02 CSI Akat                [IN037]
 * 2016/02/04 CSI Akat                [IN056]
 * 2017/08/21 Netband U. Rungsiwut    SPS Phase II
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.webserviceutil;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globaldenso.ai.library.webservicecallrest.WebServiceCallerRest;
import com.globaldenso.ai.library.webservicecallrest.domain.WebServiceCallerRestDomain;
import com.globaldenso.ai.library.webservicecallrest.exception.WebServiceCallerRestException;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.RequestResponseDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.RequestResponsePODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTAsnHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTDoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SPSTPoHeaderDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithASNDNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithDODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMDensoDensoRelationWithPODNDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDetailDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.CnDetailInformationDomain;
import com.globaldenso.asia.sps.business.domain.CnInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceDetailDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaAsnDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaChangeDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaChangePoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaOneWayKanbanInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoCoverPageInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPurchasePriceMasterDomain;
import com.globaldenso.asia.sps.business.domain.PseudoUpdatedInvoiceStatusDomain;
import com.globaldenso.asia.sps.business.domain.SendAsnDataToCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangePoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoCoverPageDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.UpdateInvoiceInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.NumberUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.sun.jersey.api.client.GenericType;

import static com.globaldenso.asia.sps.common.constant.Constants.TWO;
import static com.globaldenso.asia.sps.common.constant.Constants.FOUR;

/**
 * Common Web Service util.
 * 
 * @author CSI Phakaporn
 * @version 1.0.0
 * */
public class CommonWebServiceUtil {

	/**
	 * Log from apache commen.
	 * */
	private static Log log = LogFactory.getLog(CommonWebServiceUtil.class);

	/**
	 * The default constructor.
	 * */
	public CommonWebServiceUtil() {
	}

	/**
	 * ASN Batch Resource Transact Send ASN.
	 * 
	 * @param sendAsnDataToCigmaDomain
	 *            the send ASN data to Cigma domain
	 * @return String number of row count
	 * @throws WebServiceCallerRestException
	 *             WebServiceCallerRestException
	 */
	public static String asnBatchResourceTransactSendAsn(
			SendAsnDataToCigmaDomain sendAsnDataToCigmaDomain)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// As400ServerConnectionInformationDomain as400Server
		// = sendAsnDataToCigmaDomain.getAs400ServerInformation();
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigmaReplicate());
		// domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
		// as400Server.getSpsMCompanyDensoDomain().getDCd());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());
		// StringBuffer url = new StringBuffer();
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.ASN_BATCH_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asnBatch");
		// domain.setUrl(url.toString());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());

		As400ServerConnectionInformationDomain as400Server = sendAsnDataToCigmaDomain
				.getAs400ServerInformation();
		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());

		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.ASN_BATCH_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asnBatch");
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());
		// End : [IN037] separate REST between Order and ASN/Invoice

		AsnInfoDomain asnInformation = sendAsnDataToCigmaDomain
				.getAsnInformationDomain();
		CommonWebServiceUtil.setParamMapAsnInfoDetail(domain, asnInformation,
				sendAsnDataToCigmaDomain.getLastUpdateDscId());

		String webServiceResult = null;
		webServiceResult = WebServiceCallerRest.put(domain,
				new GenericType<String>() {
				});
		return webServiceResult;
	}

	/**
	 * ASN Resource Create ASN.
	 * 
	 * @param asnList
	 *            the list of asnMaintenanceReturn domain
	 * @param as400Server
	 *            the as400ServerConnectionInformation domain
	 * @param updateDscId
	 *            the update dsc Id
	 * @param caseType
	 *            the case type
	 * @return String number of row count
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	public static String asnResourceCreateAsn(
			List<AsnMaintenanceReturnDomain> asnList,
			As400ServerConnectionInformationDomain as400Server,
			String updateDscId, String caseType) throws ApplicationException {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigmaReplicate());
		// domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
		// as400Server.getSpsMCompanyDensoDomain().getDCd());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());

		// StringBuffer url = new StringBuffer();
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.ASN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asn");
		// domain.setUrl(url.toString());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());

		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());

		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.ASN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asn");
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());
		// End : [IN037] separate REST between Order and ASN/Invoice

		CommonWebServiceUtil.setParamMapAsnInformationForAsnResource(domain,
				asnList, updateDscId, caseType);

		String webServiceResult = null;
		try {
			webServiceResult = WebServiceCallerRest.post(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return webServiceResult;
	}

	/**
	 * ASN Resource Update ASN.
	 * 
	 * @param asnList
	 *            the list of asnMaintenanceReturn domain
	 * @param as400Server
	 *            the as400ServerConnectionInformation domain
	 * @param updateDscId
	 *            the update dsc Id
	 * @param caseType
	 *            the case type
	 * @return String number of row count
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	public static String asnResourceUpdateAsn(
			List<AsnMaintenanceReturnDomain> asnList,
			As400ServerConnectionInformationDomain as400Server,
			String updateDscId, String caseType) throws ApplicationException {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigmaReplicate());
		// domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
		// as400Server.getSpsMCompanyDensoDomain().getDCd());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());

		// StringBuffer url = new StringBuffer();
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.ASN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asn");
		// domain.setUrl(url.toString());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());

		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());

		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.ASN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asn");
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());
		// End : [IN037] separate REST between Order and ASN/Invoice

		CommonWebServiceUtil.setParamMapAsnInformationForAsnResource(domain,
				asnList, updateDscId, caseType);

		String webServiceResult = null;
		try {
			webServiceResult = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return webServiceResult;
	}

	/**
	 * Get Purchase Order from Web Service server.
	 * 
	 * @return list of pseudo PO Domain
	 * @param asnNo
	 *            the string
	 * @param asnStatus
	 *            the string
	 * @param as400Server
	 *            the as400 server information
	 * @param locale
	 *            for get Error Message
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	public static List<PseudoCigmaAsnDomain> asnResourceSearchAsnReceiving(
			String asnNo, String asnStatus,
			As400ServerConnectionInformationDomain as400Server, Locale locale)
			throws ApplicationException {
		if (WebServiceConstants.MAX_ASN_NO_LENGTH < asnNo.length()) {
			MessageUtil.throwsApplicationMessage(locale,
					SupplierPortalConstant.ERROR_CD_SP_E6_0056, null);
		}

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigmaReplicate());
		// domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
		// as400Server.getSpsMCompanyDensoDomain().getDCd());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());
		// domain.setParamMap(WebServiceConstants.PARAM_ASN_NO,
		// StringUtil.nullToEmpty(asnNo));
		// domain.setParamMap(WebServiceConstants.PARAM_ASN_STATUS,
		// StringUtil.nullToEmpty(asnStatus));

		// StringBuffer url = new StringBuffer();
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.ASN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asn");
		// domain.setUrl(url.toString());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());

		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());
		domain.setParamMap(WebServiceConstants.PARAM_ASN_NO,
				StringUtil.nullToEmpty(asnNo));
		domain.setParamMap(WebServiceConstants.PARAM_ASN_STATUS,
				StringUtil.nullToEmpty(asnStatus));

		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.ASN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/asn");
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());
		// End : [IN037] separate REST between Order and ASN/Invoice

		List<PseudoCigmaAsnDomain> webServiceResult = null;
		try {
			webServiceResult = WebServiceCallerRest.get(domain,
					new GenericType<List<PseudoCigmaAsnDomain>>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return webServiceResult;
	}

	/**
	 * Get Purchase Order from Web Service server.
	 * 
	 * @return list of pseudo PO Domain
	 * @param vendorCd
	 *            the string
	 * @param dPn
	 *            the string
	 * @param effectiveDate
	 *            the string
	 * @param as400Server
	 *            the as400 server information
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	public static List<PseudoCigmaPurchasePriceMasterDomain> purchasePriceMasterResourceSearchUnitPrice(
			String vendorCd, String dPn, String effectiveDate,
			As400ServerConnectionInformationDomain as400Server)
			throws ApplicationException {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigmaReplicate());
		// domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
		// as400Server.getSpsMCompanyDensoDomain().getDCd());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());
		// StringBuffer url = new StringBuffer();
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.PURCHASE_PRICE_MASTER_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/purchasePriceMaster");
		// domain.setUrl(url.toString());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());

		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getOrderLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain().getOrdSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getOrdConnectionNo().toString());
		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.PURCHASE_PRICE_MASTER_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/purchasePriceMaster");
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());
		// Start : [IN037] separate REST between Order and ASN/Invoice

		domain.setParamMap(WebServiceConstants.PARAM_VENDOR_CODE,
				StringUtil.nullToEmpty(vendorCd));
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_PART_CODE,
				StringUtil.nullToEmpty(dPn));
		domain.setParamMap(WebServiceConstants.PARAM_EFFECTIVE_DATE,
				StringUtil.nullToEmpty(effectiveDate));

		List<PseudoCigmaPurchasePriceMasterDomain> webServiceResult = null;
		try {
			webServiceResult = WebServiceCallerRest
					.get(domain,
							new GenericType<List<PseudoCigmaPurchasePriceMasterDomain>>() {
							});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return webServiceResult;
	}

	/**
	 * Call InvoiceInformationResource by WebService REST to search Invoice
	 * Status update.
	 * 
	 * @param updateInvoiceInformationDomain
	 *            Update Invoice Status Domain
	 * @return List of Pseudo Updated Invoice Status Domain
	 * @throws ApplicationException
	 *             ApplicationException
	 * */
	public static List<PseudoUpdatedInvoiceStatusDomain> searchInvoiceStatusUpdate(
			UpdateInvoiceInformationDomain updateInvoiceInformationDomain)
			throws ApplicationException {
		List<PseudoUpdatedInvoiceStatusDomain> updateInvoiceList = null;
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// StringBuffer url = new StringBuffer();
		// url.append(updateInvoiceInformationDomain.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.INVOICE_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/invoice");
		// domain.setUrl(url.toString());
		// domain.setUserName(
		// updateInvoiceInformationDomain.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(
		// updateInvoiceInformationDomain.getSpsMAs400SchemaDomain().getRestPassword());

		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// updateInvoiceInformationDomain.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_JDE,
		// updateInvoiceInformationDomain.getSpsMAs400SchemaDomain().getSchemaNameJde());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// updateInvoiceInformationDomain.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// updateInvoiceInformationDomain.getSpsMAs400SchemaDomain().getConnectionNo().toString());
		// domain.setParamMap(WebServiceConstants.PARAM_DATA_SCOPE_DATE,
		// updateInvoiceInformationDomain.getDataScopeDate());

		StringBuffer url = new StringBuffer();
		url.append(updateInvoiceInformationDomain.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.INVOICE_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/invoice");
		domain.setUrl(url.toString());
		domain.setUserName(updateInvoiceInformationDomain
				.getSpsMAs400SchemaDomain().getAsnInvRestUserName());
		domain.setPassword(updateInvoiceInformationDomain
				.getSpsMAs400SchemaDomain().getAsnInvRestPassword());
		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
				updateInvoiceInformationDomain.getSpsMAs400LparDomain()
						.getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_JDE,
				updateInvoiceInformationDomain.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaJde());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				updateInvoiceInformationDomain.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
				updateInvoiceInformationDomain.getSpsMAs400SchemaDomain()
						.getAsnInvConnectionNo().toString());
		domain.setParamMap(WebServiceConstants.PARAM_DATA_SCOPE_DATE,
				updateInvoiceInformationDomain.getDataScopeDate());
		// End : [IN037] separate REST between Order and ASN/Invoice

		try {
			updateInvoiceList = WebServiceCallerRest.get(domain,
					new GenericType<List<PseudoUpdatedInvoiceStatusDomain>>() {
					});
		} catch (WebServiceCallerRestException e) {
			if (Constants.ZERO == CommonWebServiceUtil.checkException(e,
					domain, updateInvoiceInformationDomain.getLocale())) {
				return null;
			}
		} catch (Exception e) {
			MessageUtil.throwsApplicationMessageHandledException(
					updateInvoiceInformationDomain.getLocale(),
					SupplierPortalConstant.ERROR_CD_SP_90_0002, new String[] {
							e.getClass().getName(), e.getMessage() });
		}

		return updateInvoiceList;
	}

	/**
	 * Transaction transfer invoice/CN data to JDE.
	 * 
	 * @param invoiceId
	 *            current transaction invoice ID
	 * @param asnNoIn
	 *            ASN Number to get (many ASN Number comma separate).
	 * @param batchJobId
	 *            Job ID from AIJM
	 * @param systemDatetime
	 *            update datetime for batch running current round
	 * @param as400Server
	 *            AS400 server information
	 * @param invoiceInformationDomain
	 *            invoice to transfer
	 * @param cnList
	 *            list of CN to transfer
	 * @return record Effect
	 * @throws ApplicationException
	 *             an ApplicationException
	 * */
	public static Integer apInterfaceResourceTransactTransferInvoice(
			BigDecimal invoiceId, String asnNoIn, String batchJobId,
			Timestamp systemDatetime,
			As400ServerConnectionInformationDomain as400Server,
			InvoiceInformationDomain invoiceInformationDomain,
			List<CnInformationDomain> cnList) throws ApplicationException {
		String result = Constants.STR_ZERO;
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		StringBuffer url = new StringBuffer();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		// url.append(WebServiceConstants.APINTERFACE_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/apInterface");
		// domain.setUrl(url.toString());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());
		url.append(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.APINTERFACE_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/apInterface");
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());
		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());
		// End : [IN037] separate REST between Order and ASN/Invoice

		try {
			transformParameterToAiwsApInterface(invoiceId, asnNoIn, batchJobId,
					systemDatetime, invoiceInformationDomain, cnList, domain);
		} catch (Exception e) {
			MessageUtil.getErrorMessageForBatch(
					invoiceInformationDomain.getLocale(),
					SupplierPortalConstant.ERROR_CD_SP_90_0002, new String[] {
							e.getClass().getName(), e.getMessage() }, log, TWO,
					true);
		}

		try {
			result = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			if (Constants.ZERO == CommonWebServiceUtil.checkException(e,
					domain, invoiceInformationDomain.getLocale())) {
				return Constants.ZERO;
			}
		} catch (Exception e) {
			MessageUtil.getErrorMessageForBatch(
					invoiceInformationDomain.getLocale(),
					SupplierPortalConstant.ERROR_CD_SP_90_0002, new String[] {
							e.getClass().getName(), e.getMessage() }, log, TWO,
					true);
		}

		if (null == result) {
			result = Constants.STR_ZERO;
		}
		return Integer.parseInt(result);
	}

	// /**
	// *
	// * <p>Search As400 Status.</p>
	// *
	// * @param as400Server As400ServerConnectionInformationDomain
	// * @return String status
	// * @throws Exception Exception
	// */
	// public static String searchAs400Status(
	// As400ServerConnectionInformationDomain as400Server) throws Exception{
	// WebServiceCallerRestDomain domain = initialDomain(as400Server);
	// StringBuffer url = new StringBuffer();
	// // url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
	// // url.append(WebServiceConstants.CIGMA_PO_RESOURCE);
	// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaPo");
	// domain.setUrl(url.toString());
	// String result =
	// WebServiceCallerRest.post(domain,
	// new GenericType<String>(){});
	// return result;
	// }

	/**
	 * Search Delivery Order.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * 
	 * @return the list of pseudo cigma do information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaDoInformationDomain> searchCigmaDeliveryOrder(
			As400ServerConnectionInformationDomain as400Server, String flag)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);
		domain.setParamMap(WebServiceConstants.PARAM_KANBAN_SEQ_FLG,
				as400Server.getSpsMCompanyDensoDomain().getReportKanbanSeqFlg());

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_DO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaDo");
		domain.setUrl(url.toString());
		List<PseudoCigmaDoInformationDomain> resultList = WebServiceCallerRest
				.get(domain,
						new GenericType<List<PseudoCigmaDoInformationDomain>>() {
						});

		for (PseudoCigmaDoInformationDomain pseudo : resultList) {
			if (null != pseudo.getPseudoPlantCode()) {
				pseudo.setPseudoPlantCode(pseudo.getPseudoPlantCode().trim());
			}
			if (null != pseudo.getPseudoPoNumber()) {
				pseudo.setPseudoPoNumber(pseudo.getPseudoPoNumber().trim());
			}
			if (null != pseudo.getPseudoPartNumber()) {
				pseudo.setPseudoPartNumber(pseudo.getPseudoPartNumber().trim());
			}
			if (null != pseudo.getPseudoAddTruckRouteFlag()) {
				pseudo.setPseudoAddTruckRouteFlag(pseudo
						.getPseudoAddTruckRouteFlag().trim());
			}
			if (null != pseudo.getPseudoUpdateByUserId()) {
				pseudo.setPseudoUpdateByUserId(pseudo.getPseudoUpdateByUserId()
						.trim());
			}
			if (null != pseudo.getPseudoDataType()) {
				pseudo.setPseudoDataType(pseudo.getPseudoDataType().trim());
			}
			if (null != pseudo.getPseudoDoNumber()) {
				pseudo.setPseudoDoNumber(pseudo.getPseudoDoNumber().trim());
			}
			if (null != pseudo.getPseudoHeaderVendorCd()) {
				pseudo.setPseudoHeaderVendorCd(pseudo.getPseudoHeaderVendorCd()
						.trim());
			}
			if (null != pseudo.getPseudoDoPartNumber()) {
				pseudo.setPseudoDoPartNumber(pseudo.getPseudoDoPartNumber()
						.trim());
			}
			if (null != pseudo.getPseudoScanReceiveFlag()) {
				pseudo.setPseudoScanReceiveFlag(pseudo
						.getPseudoScanReceiveFlag().trim());
			}
			if (null != pseudo.getPseudoKanbanType()) {
				pseudo.setPseudoKanbanType(pseudo.getPseudoKanbanType().trim());
			}
			if (null != pseudo.getPseudoRemark1()) {
				pseudo.setPseudoRemark1(pseudo.getPseudoRemark1().trim());
			}
			if (null != pseudo.getPseudoRemark2()) {
				pseudo.setPseudoRemark2(pseudo.getPseudoRemark2().trim());
			}
			if (null != pseudo.getPseudoRemark3()) {
				pseudo.setPseudoRemark3(pseudo.getPseudoRemark3().trim());
			}
			if (null != pseudo.getPseudoDCustomerPartNo()) {
				pseudo.setPseudoDCustomerPartNo(pseudo
						.getPseudoDCustomerPartNo().trim());
			}
			if (null != pseudo.getPseudoSProcessCode()) {
				pseudo.setPseudoSProcessCode(pseudo.getPseudoSProcessCode()
						.trim());
			}
			if (null != pseudo.getPseudoTagOutput()) {
				pseudo.setPseudoTagOutput(pseudo.getPseudoTagOutput().trim());
			}
		}
		return resultList;
	}

	/**
	 * Update Delivery Order.
	 * 
	 * @param as400Server
	 *            the config server.
	 * @param transferList
	 *            the transfer do data from cigma domain
	 * @param spsFlag
	 *            spsFlag
	 * @param jobId
	 *            jobId
	 * @return part no cannot update
	 * @throws ApplicationException
	 *             the ApplicationException
	 * */
	public static String updateCigmaDeliveryOrder(
			As400ServerConnectionInformationDomain as400Server,
			List<TransferDoDataFromCigmaDomain> transferList, String spsFlag,
			String jobId) throws ApplicationException {
		if (null == transferList || Constants.ZERO == transferList.size()) {
			return Constants.EMPTY_STRING;
		}
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_DO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaDo");
		domain.setUrl(url.toString());

		Timestamp current = new Timestamp(System.currentTimeMillis());
		String curDate = DateUtil.format(current, DateUtil.PATTERN_YYYYMMDD);
		String curTime = DateUtil.format(current, DateUtil.PATTERN_HHMMSS);

		List<String> dpnList = new ArrayList<String>();
		List<String> delList = new ArrayList<String>();
		List<String> ctdList = new ArrayList<String>();
		List<String> cttList = new ArrayList<String>();
		for (TransferDoDataFromCigmaDomain transfer : transferList) {
			for (TransferDoDetailDataFromCigmaDomain detail : transfer
					.getCigmaDoDetail()) {
				// for(TransferDoKanbanlDataFromCigmaDomain kanban :
				// detail.getCigmaDoKanbanSeq()){
				dpnList.add(detail.getPartNo());
				delList.add(transfer.getDeliveryDate());
				ctdList.add(detail.getUpdateDate());
				cttList.add(detail.getUpdateTime());
				// }
			}
		}
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, spsFlag);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_USER_ID, jobId);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE, curDate);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME, curTime);
		domain.setParamMap(WebServiceConstants.PARAM_DO_SCD,
				transferList.get(Constants.ZERO).getVendorCd());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_DO_NO, transferList
				.get(Constants.ZERO).getCigmaDoNo());

		domain.setParamMap(WebServiceConstants.PARAM_DO_DPN,
				StringUtil.convertListToStringCommaSeperate(dpnList));
		domain.setParamMap(WebServiceConstants.PARAM_DELIVERY_DATE,
				StringUtil.convertListToStringCommaSeperate(delList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE,
				StringUtil.convertListToStringCommaSeperate(ctdList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME,
				StringUtil.convertListToStringCommaSeperate(cttList));

		String result = null;
		try {
			result = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}

	/**
	 * Search Change Delivery Order.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * @return the list of pseudo cigma change do information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaChangeDoInformationDomain> searchCigmaChangeDeliveryOrder(
			As400ServerConnectionInformationDomain as400Server, String flag)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);
		domain.setParamMap(WebServiceConstants.PARAM_KANBAN_SEQ_FLG,
				as400Server.getSpsMCompanyDensoDomain().getReportKanbanSeqFlg());

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_CHG_DO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaChgDo");
		domain.setUrl(url.toString());
		List<PseudoCigmaChangeDoInformationDomain> resultList = WebServiceCallerRest
				.get(domain,
						new GenericType<List<PseudoCigmaChangeDoInformationDomain>>() {
						});
		for (PseudoCigmaChangeDoInformationDomain pseudo : resultList) {
			if (null != pseudo.getPseudoPlantCode()) {
				pseudo.setPseudoPlantCode(pseudo.getPseudoPlantCode().trim());
			}
			if (null != pseudo.getPseudoDoPartNumber()) {
				pseudo.setPseudoDoPartNumber(pseudo.getPseudoDoPartNumber()
						.trim());
			}
			if (null != pseudo.getPseudoPartNumber()) {
				pseudo.setPseudoPartNumber(pseudo.getPseudoPartNumber().trim());
			}
			if (null != pseudo.getPseudoUpdateByUserId()) {
				pseudo.setPseudoUpdateByUserId(pseudo.getPseudoUpdateByUserId()
						.trim());
			}
			if (null != pseudo.getPseudoDataType()) {
				pseudo.setPseudoDataType(pseudo.getPseudoDataType().trim());
			}
			if (null != pseudo.getPseudoHeaderVendorCd()) {
				pseudo.setPseudoHeaderVendorCd(pseudo.getPseudoHeaderVendorCd()
						.trim());
			}
			if (null != pseudo.getPseudoPartNumber()) {
				pseudo.setPseudoPartNumber(pseudo.getPseudoPartNumber().trim());
			}
			if (null != pseudo.getPseudoKanbanVendorCd()) {
				pseudo.setPseudoKanbanVendorCd(pseudo.getPseudoKanbanVendorCd()
						.trim());
			}
			if (null != pseudo.getPseudoPlantCd()) {
				pseudo.setPseudoPlantCd(pseudo.getPseudoPlantCd().trim());
			}
			if (null != pseudo.getPseudoOriginalDelivery()) {
				pseudo.setPseudoOriginalDelivery(pseudo
						.getPseudoOriginalDelivery().trim());
			}
			if (null != pseudo.getPseudoPrevDelivery()) {
				pseudo.setPseudoPrevDelivery(pseudo.getPseudoPrevDelivery()
						.trim());
			}
			if (null != pseudo.getPseudoWarehouseForPrimeReceiving()) {
				pseudo.setPseudoWarehouseForPrimeReceiving(pseudo
						.getPseudoWarehouseForPrimeReceiving().trim());
			}
			if (null != pseudo.getPseudoScanReceiveFlag()) {
				pseudo.setPseudoScanReceiveFlag(pseudo
						.getPseudoScanReceiveFlag().trim());
			}
			if (null != pseudo.getPseudoKanbanType()) {
				pseudo.setPseudoKanbanType(pseudo.getPseudoKanbanType().trim());
			}
			if (null != pseudo.getPseudoRemark1()) {
				pseudo.setPseudoRemark1(pseudo.getPseudoRemark1().trim());
			}
			if (null != pseudo.getPseudoRemark2()) {
				pseudo.setPseudoRemark2(pseudo.getPseudoRemark2().trim());
			}
			if (null != pseudo.getPseudoRemark3()) {
				pseudo.setPseudoRemark3(pseudo.getPseudoRemark3().trim());
			}
			if (null != pseudo.getPseudoDCustomerPartNo()) {
				pseudo.setPseudoDCustomerPartNo(pseudo
						.getPseudoDCustomerPartNo().trim());
			}
			if (null != pseudo.getPseudoSProcessCode()) {
				pseudo.setPseudoSProcessCode(pseudo.getPseudoSProcessCode()
						.trim());
			}
			if (null != pseudo.getPseudoTagOutput()) {
				pseudo.setPseudoTagOutput(pseudo.getPseudoTagOutput().trim());
			}

		}
		return resultList;
	}

	/**
	 * Update Change Delivery Order.
	 * 
	 * @param as400Server
	 *            As400ServerConnectionInformationDomain
	 * @param transferList
	 *            the transfer change do data from cigma domain
	 * @param spsFlag
	 *            spsFlag
	 * @param jobId
	 *            jobId
	 * @return the number of updated record
	 * @throws ApplicationException
	 *             the ApplicationException
	 * */
	public static String updateCigmaChangeDeliveryOrder(
			As400ServerConnectionInformationDomain as400Server,
			List<TransferChangeDoDataFromCigmaDomain> transferList,
			String spsFlag, String jobId) throws ApplicationException {
		if (null == transferList || Constants.ZERO == transferList.size()) {
			return Constants.EMPTY_STRING;
		}
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_CHG_DO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaChgDo");
		domain.setUrl(url.toString());

		Timestamp current = new Timestamp(System.currentTimeMillis());
		String curDate = DateUtil.format(current, DateUtil.PATTERN_YYYYMMDD);
		String curTime = DateUtil.format(current, DateUtil.PATTERN_HHMMSS);

		List<String> dpnList = new ArrayList<String>();
		List<String> delList = new ArrayList<String>();
		List<String> ctdList = new ArrayList<String>();
		List<String> cttList = new ArrayList<String>();
		for (TransferChangeDoDataFromCigmaDomain transfer : transferList) {
			for (TransferChangeDoDetailDataFromCigmaDomain detail : transfer
					.getCigmaDoDetail()) {
				// for(TransferDoKanbanlDataFromCigmaDomain kanban :
				// detail.getCigmaDoKanbanSeq()){
				dpnList.add(detail.getPartNo());
				delList.add(transfer.getDeliveryDate());
				ctdList.add(detail.getUpdateDate());
				cttList.add(detail.getUpdateTime());
				// }
			}
		}

		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, spsFlag);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_USER_ID, jobId);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE, curDate);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME, curTime);
		domain.setParamMap(WebServiceConstants.PARAM_DO_SCD,
				transferList.get(Constants.ZERO).getVendorCd());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_CHG_DO_NO,
				transferList.get(Constants.ZERO).getCurrentDelivery());

		domain.setParamMap(WebServiceConstants.PARAM_DO_DPN,
				StringUtil.convertListToStringCommaSeperate(dpnList));
		domain.setParamMap(WebServiceConstants.PARAM_DELIVERY_DATE,
				StringUtil.convertListToStringCommaSeperate(delList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE,
				StringUtil.convertListToStringCommaSeperate(ctdList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME,
				StringUtil.convertListToStringCommaSeperate(cttList));

		String result = null;
		try {
			result = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}

	// [IN056-2] Search CIGMA Vendor Code
	/**
	 * Search CIGMA Vendor Code
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * @return the list of pseudo cigma po cover information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaPoCoverPageInformationDomain> searchCigmaVendorCode(
			As400ServerConnectionInformationDomain as400Server, String flag)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_PO_VENDOR_CODE_RESOURCE);
		domain.setUrl(url.toString());
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);

		List<PseudoCigmaPoCoverPageInformationDomain> resultList = WebServiceCallerRest
				.get(domain,
						new GenericType<List<PseudoCigmaPoCoverPageInformationDomain>>() {
						});
		for (PseudoCigmaPoCoverPageInformationDomain pseudo : resultList) {
			if (null != pseudo.getPseudoVendorCd()) {
				pseudo.setPseudoVendorCd(pseudo.getPseudoVendorCd().trim());
			}
			if (null != pseudo.getPseudoPoNo()) {
				pseudo.setPseudoPoNo(pseudo.getPseudoPoNo().trim());
			}
			if (null != pseudo.getPseudoDataType()) {
				pseudo.setPseudoDataType(pseudo.getPseudoDataType().trim());
			}
			if (null != pseudo.getPseudoDPcd()) {
				pseudo.setPseudoDPcd(pseudo.getPseudoDPcd().trim());
			}
		}
		return resultList;
	}

	/**
	 * Search Purchase Order.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * @param vendorCd
	 *            CIGMA Vendor Code
	 * @return the list of pseudo cigma po information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaPoInformationDomain> searchCigmaPurchaseOrder(
			As400ServerConnectionInformationDomain as400Server, String flag

			// [IN056] : Add condition to get P/O by Vendor
			, String vendorCd

	) throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_PO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaPo");
		domain.setUrl(url.toString());
		List<PseudoCigmaPoInformationDomain> resultList = new ArrayList<PseudoCigmaPoInformationDomain>();

		domain.setParamMap(WebServiceConstants.PARAM_MAX_RECORD,
				String.valueOf(Constants.TEN_THOUSANDS));
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);
		domain.setParamMap(WebServiceConstants.PARAM_SKIP, Constants.STR_ZERO);

		// [IN056] : Add condition to get P/O by Vendor
		domain.setParamMap(WebServiceConstants.PARAM_VENDOR_CODE, vendorCd);

		resultList = WebServiceCallerRest.get(domain,
				new GenericType<List<PseudoCigmaPoInformationDomain>>() {
				});

		// List<PseudoCigmaPoInformationDomain> bufferList = null;
		// Integer iSkip = new Integer( Constants.ZERO );
		// domain.setParamMap(
		// WebServiceConstants.PARAM_MAX_RECORD, String.valueOf(
		// Constants.TEN_THOUSANDS ));
		// domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);
		// do{
		// domain.setParamMap(WebServiceConstants.PARAM_SKIP, iSkip.toString());
		//
		// bufferList
		// = WebServiceCallerRest.get(domain,
		// new GenericType<List<PseudoCigmaPoInformationDomain>>(){});
		// if( null != bufferList ){
		// resultList.addAll( bufferList );
		// }
		// iSkip += Constants.TEN_THOUSANDS;
		// }while( null != bufferList && Constants.TEN_THOUSANDS ==
		// bufferList.size() );
		// SortUtil.sort(resultList, SortUtil.COMPARE_PSEUDO_CIGMA_PO);
		for (PseudoCigmaPoInformationDomain pseudo : resultList) {
			if (null != pseudo.getPseudoVendorCd()) {
				pseudo.setPseudoVendorCd(pseudo.getPseudoVendorCd().trim());
			}
			if (null != pseudo.getPseudoDPcd()) {
				pseudo.setPseudoDPcd(pseudo.getPseudoDPcd().trim());
			}
			if (null != pseudo.getPseudoPoNumber()) {
				pseudo.setPseudoPoNumber(pseudo.getPseudoPoNumber().trim());
			}
			if (null != pseudo.getPseudoPartNumber()) {
				pseudo.setPseudoPartNumber(pseudo.getPseudoPartNumber().trim());
			}
			if (null != pseudo.getPseudoAddTruckRouteFlag()) {
				pseudo.setPseudoAddTruckRouteFlag(pseudo
						.getPseudoAddTruckRouteFlag().trim());
			}
			if (null != pseudo.getPseudoUpdateByUserId()) {
				pseudo.setPseudoUpdateByUserId(pseudo.getPseudoUpdateByUserId()
						.trim());
			}
			if (null != pseudo.getPseudoDataType()) {
				pseudo.setPseudoDataType(pseudo.getPseudoDataType().trim());
			}
			if (null != pseudo.getPseudoOrderType()) {
				pseudo.setPseudoOrderType(pseudo.getPseudoOrderType().trim());
			}
			if (null != pseudo.getPseudoVariableQtyCode()) {
				pseudo.setPseudoVariableQtyCode(pseudo
						.getPseudoVariableQtyCode().trim());
			}
			if (null != pseudo.getPseudoReportType()) {
				pseudo.setPseudoReportType(pseudo.getPseudoReportType().trim());
			}
		}
		if (Constants.ZERO < resultList.size()) {

			String strOrderType = Constants.STR_C;
			PseudoCigmaPoInformationDomain po = resultList.get(Constants.ZERO);
			String strId = StringUtil.appendsString(po.getPseudoPoNumber(),
					po.getPseudoDataType(), po.getPseudoDPcd(),
					po.getPseudoPartNumber());
			for (PseudoCigmaPoInformationDomain pseudo : resultList) {
				if (!StringUtil.appendsString(pseudo.getPseudoPoNumber(),
						pseudo.getPseudoDataType(), pseudo.getPseudoDPcd(),
						pseudo.getPseudoPartNumber()).equals(strId)) {
					strOrderType = Constants.STR_C;
					strId = StringUtil.appendsString(
							pseudo.getPseudoPoNumber(),
							pseudo.getPseudoDataType(), pseudo.getPseudoDPcd(),
							pseudo.getPseudoPartNumber());
				}

				if (Constants.EMPTY_STRING.equals(pseudo.getPseudoOrderType())) {
					pseudo.setPseudoOrderType(strOrderType);
				} else {
					strOrderType = pseudo.getPseudoOrderType();
				}
			}
		}
		return resultList;
	}

	/**
	 * Search Purchase Order Cover Page.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * @param vendorCd
	 *            CIGMA Vendor Code
	 * @return the list of pseudo cigma po cover information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaPoCoverPageInformationDomain> searchCigmaPurchaseOrderCoverPage(
			As400ServerConnectionInformationDomain as400Server, String flag
			// [IN056] : Add condition to get P/O by Vendor
			, String vendorCd) throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_PO_COVER_PAGE_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaPoCoverPage");
		domain.setUrl(url.toString());

		// [IN056] : Add condition to get P/O by Vendor
		domain.setParamMap(WebServiceConstants.PARAM_VENDOR_CODE, vendorCd);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);

		List<PseudoCigmaPoCoverPageInformationDomain> resultList = WebServiceCallerRest
				.get(domain,
						new GenericType<List<PseudoCigmaPoCoverPageInformationDomain>>() {
						});
		for (PseudoCigmaPoCoverPageInformationDomain pseudo : resultList) {
			if (null != pseudo.getPseudoVendorCd()) {
				pseudo.setPseudoVendorCd(pseudo.getPseudoVendorCd().trim());
			}
			if (null != pseudo.getPseudoPoNo()) {
				pseudo.setPseudoPoNo(pseudo.getPseudoPoNo().trim());
			}
			if (null != pseudo.getPseudoDataType()) {
				pseudo.setPseudoDataType(pseudo.getPseudoDataType().trim());
			}
			if (null != pseudo.getPseudoDPcd()) {
				pseudo.setPseudoDPcd(pseudo.getPseudoDPcd().trim());
			}
		}
		return resultList;
	}

	/**
	 * Update Purchase Order.
	 * 
	 * @param as400Server
	 *            As400ServerConnectionInformationDomain
	 * @param transferList
	 *            the transfer po data from cigma domain
	 * @param cover
	 *            the transfer po cover page data from cigma domain
	 * @param spsFlag
	 *            spsFlag
	 * @param jobId
	 *            jobId
	 * @return the number of updated record
	 * @throws ApplicationException
	 *             the ApplicationException
	 * */
	public static String updateCigmaPurchaseOrder(
			As400ServerConnectionInformationDomain as400Server,
			List<TransferPoDataFromCigmaDomain> transferList,
			TransferPoCoverPageDataFromCigmaDomain cover, String spsFlag,
			String jobId) throws ApplicationException {
		if (null == transferList || Constants.ZERO == transferList.size()) {
			return Constants.EMPTY_STRING;
		}
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_PO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaPo");
		domain.setUrl(url.toString());

		Timestamp current = new Timestamp(System.currentTimeMillis());
		String curDate = DateUtil.format(current, DateUtil.PATTERN_YYYYMMDD);
		String curTime = DateUtil.format(current, DateUtil.PATTERN_HHMMSS);

		List<String> dpnList = new ArrayList<String>();
		List<String> dueList = new ArrayList<String>();
		List<String> ctdList = new ArrayList<String>();
		List<String> cttList = new ArrayList<String>();
		List<String> rtyList = new ArrayList<String>();
		for (TransferPoDataFromCigmaDomain transfer : transferList) {
			for (TransferPoDetailDataFromCigmaDomain detail : transfer
					.getCigmaPoDetailList()) {
				for (TransferPoDueDataFromCigmaDomain due : detail
						.getCigmaPoDueList()) {
					dpnList.add(detail.getDPn());
					dueList.add(DateUtil.format(due.getEtd(),
							DateUtil.PATTERN_YYYYMMDD));
					ctdList.add(due.getUpdateDate());
					cttList.add(due.getUpdateTime());
					rtyList.add(due.getReportType());
				}
			}
		}

		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, spsFlag);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_USER_ID, jobId);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE, curDate);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME, curTime);
		domain.setParamMap(WebServiceConstants.PARAM_SCD,
				transferList.get(Constants.ZERO).getVendorCd());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_PO_NO, transferList
				.get(Constants.ZERO).getCigmaPoNo());

		domain.setParamMap(WebServiceConstants.PARAM_DPN,
				StringUtil.convertListToStringCommaSeperate(dpnList));
		domain.setParamMap(WebServiceConstants.PARAM_DUE_DATE,
				StringUtil.convertListToStringCommaSeperate(dueList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE,
				StringUtil.convertListToStringCommaSeperate(ctdList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME,
				StringUtil.convertListToStringCommaSeperate(cttList));
		domain.setParamMap(WebServiceConstants.PARAM_REPORT_TYPE,
				StringUtil.convertListToStringCommaSeperate(rtyList));
		String result = null;
		try {
			result = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}

	/**
	 * Search Change Purchase Order.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * @return the list of pseudo cigma change po information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaChangePoInformationDomain> searchCigmaChangePurchaseOrder(
			As400ServerConnectionInformationDomain as400Server, String flag)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_CHG_PO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaChgPo");
		domain.setUrl(url.toString());
		List<PseudoCigmaChangePoInformationDomain> resultList = WebServiceCallerRest
				.get(domain,
						new GenericType<List<PseudoCigmaChangePoInformationDomain>>() {
						});
		for (PseudoCigmaChangePoInformationDomain pseudo : resultList) {
			if (null != pseudo.getPseudoVendorCd()) {
				pseudo.setPseudoVendorCd(pseudo.getPseudoVendorCd().trim());
			}
			if (null != pseudo.getPseudoPoNumber()) {
				pseudo.setPseudoPoNumber(pseudo.getPseudoPoNumber().trim());
			}
			if (null != pseudo.getPseudoPartNumber()) {
				pseudo.setPseudoPartNumber(pseudo.getPseudoPartNumber().trim());
			}
			if (null != pseudo.getPseudoDataType()) {
				pseudo.setPseudoDataType(pseudo.getPseudoDataType().trim());
			}
			if (null != pseudo.getPseudoReportType()) {
				pseudo.setPseudoReportType(pseudo.getPseudoReportType().trim());
			}
			if (null != pseudo.getPseudoOrderType()) {
				pseudo.setPseudoOrderType(pseudo.getPseudoOrderType().trim());
			}
			if (null != pseudo.getPseudoAddTruckRouteFlag()) {
				pseudo.setPseudoAddTruckRouteFlag(pseudo
						.getPseudoAddTruckRouteFlag().trim());
			}
		}
		return resultList;
	}

	/**
	 * Update Change Purchase Order.
	 * 
	 * @param as400Server
	 *            As400ServerConnectionInformationDomain
	 * @param transferList
	 *            the transfer change po data from cigma domain
	 * @param spsFlag
	 *            spsFlag
	 * @param jobId
	 *            jobId
	 * @return the number of updated record
	 * @throws ApplicationException
	 *             the ApplicationExceptions
	 * */
	public static String updateCigmaChangePurchaseOrder(
			As400ServerConnectionInformationDomain as400Server,
			List<TransferChangePoDataFromCigmaDomain> transferList,
			String spsFlag, String jobId) throws ApplicationException {

		if (null == transferList || Constants.ZERO == transferList.size()) {
			return Constants.EMPTY_STRING;
		}

		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		StringBuffer url = new StringBuffer();

		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());

		url.append(WebServiceConstants.CIGMA_CHG_PO_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaChgPo");
		domain.setUrl(url.toString());

		Timestamp current = new Timestamp(System.currentTimeMillis());
		String curDate = DateUtil.format(current, DateUtil.PATTERN_YYYYMMDD);
		String curTime = DateUtil.format(current, DateUtil.PATTERN_HHMMSS);

		List<String> dpnList = new ArrayList<String>();
		List<String> dueList = new ArrayList<String>();
		List<String> ctdList = new ArrayList<String>();
		List<String> cttList = new ArrayList<String>();
		for (TransferChangePoDataFromCigmaDomain transfer : transferList) {
			for (TransferChangePoDetailDataFromCigmaDomain detail : transfer
					.getCigmaPoDetailList()) {
				for (TransferChangePoDueDataFromCigmaDomain due : detail
						.getCigmaPoDueList()) {
					dpnList.add(detail.getDPn());
					dueList.add(DateUtil.format(due.getDueDate(),
							DateUtil.PATTERN_YYYYMMDD));
					ctdList.add(due.getUpdateDate());
					cttList.add(due.getUpdateTime());
				}
			}
		}

		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, spsFlag);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_USER_ID, jobId);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE, curDate);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME, curTime);
		domain.setParamMap(WebServiceConstants.PARAM_SCD,
				transferList.get(Constants.ZERO).getVendorCd());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_CHG_PO_NO,
				transferList.get(Constants.ZERO).getCigmaPoNo());

		domain.setParamMap(WebServiceConstants.PARAM_DPN,
				StringUtil.convertListToStringCommaSeperate(dpnList));
		domain.setParamMap(WebServiceConstants.PARAM_DUE_DATE,
				StringUtil.convertListToStringCommaSeperate(dueList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE,
				StringUtil.convertListToStringCommaSeperate(ctdList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME,
				StringUtil.convertListToStringCommaSeperate(cttList));

		String result = null;
		try {
			result = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}

	/**
	 * Set param map from asn info detail to web service parameter domain.
	 * 
	 * @param domain
	 *            the web service parameter domain
	 * @param updateDscId
	 *            the update dsc id (Job Id)
	 * @param asnInformationDomain
	 *            the asn Info domain
	 * */
	private static void setParamMapAsnInfoDetail(
			WebServiceCallerRestDomain domain,
			AsnInfoDomain asnInformationDomain, String updateDscId) {
		// Header
		String supplierCodeHeaderStr = Constants.EMPTY_STRING;
		String asnNoHeaderStr = Constants.EMPTY_STRING;
		String dPcdHeaderStr = Constants.EMPTY_STRING;
		String rcvStatusHeaderStr = Constants.EMPTY_STRING;
		String createDateHeaderStr = Constants.EMPTY_STRING;
		String createTimeHeaderStr = Constants.EMPTY_STRING;
		String updateByHeaderStr = Constants.EMPTY_STRING;
		String updateDateHeaderStr = Constants.EMPTY_STRING;
		String updateTimeHeaderStr = Constants.EMPTY_STRING;
		String planEtaDateStr = Constants.EMPTY_STRING;
		String planEtaTimeStr = Constants.EMPTY_STRING;
		String actualEtdDateStr = Constants.EMPTY_STRING;
		String actualEtdTimeStr = Constants.EMPTY_STRING;
		String spsAsnStatusStr = Constants.EMPTY_STRING;
		String noOfPalletStr = Constants.EMPTY_STRING;
		String tripNoStr = Constants.EMPTY_STRING;
		String processFlagStr = Constants.EMPTY_STRING;

		// Detail
		List<String> supplierCodeDetailList = new ArrayList<String>();
		List<String> asnNoDetailList = new ArrayList<String>();
		List<String> dPcdDetailList = new ArrayList<String>();
		List<String> cigmaCurrentDoNoList = new ArrayList<String>();
		List<String> cigmaOriginalDoNoList = new ArrayList<String>();
		List<String> rcvStatusDetailList = new ArrayList<String>();
		List<String> dPnList = new ArrayList<String>();
		List<String> spsDoNoList = new ArrayList<String>();
		List<String> shippingQtyList = new ArrayList<String>();
		List<String> receivedQtyList = new ArrayList<String>();
		List<String> remainingQtyList = new ArrayList<String>();
		List<String> varList = new ArrayList<String>();
		List<String> insList = new ArrayList<String>();
		List<String> whList = new ArrayList<String>();
		List<String> dueDateList = new ArrayList<String>();
		List<String> rcvLaneList = new ArrayList<String>();
		List<String> receivedDateList = new ArrayList<String>();
		List<String> createDateDetailList = new ArrayList<String>();
		List<String> createTimeDetailList = new ArrayList<String>();
		List<String> updateByDetailList = new ArrayList<String>();
		List<String> updateDateDetailList = new ArrayList<String>();
		List<String> updateTimeDetailList = new ArrayList<String>();
		List<String> qtyBoxList = new ArrayList<String>();
		List<String> unitOfMeasureList = new ArrayList<String>();
		List<String> changeReasonCdList = new ArrayList<String>();
		List<String> pnProcessFlagList = new ArrayList<String>();

		supplierCodeHeaderStr = asnInformationDomain.getSpsTAsnDomain()
				.getVendorCd();
		asnNoHeaderStr = asnInformationDomain.getSpsTAsnDomain().getAsnNo();
		dPcdHeaderStr = asnInformationDomain.getSpsTAsnDomain().getDPcd();
		rcvStatusHeaderStr = Constants.EMPTY_STRING;

		if (!StringUtil.checkNullOrEmpty(asnInformationDomain
				.getSpsTAsnDomain().getActualEtd())) {
			actualEtdDateStr = DateUtil.format(new Date(asnInformationDomain
					.getSpsTAsnDomain().getActualEtd().getTime()),
					DateUtil.PATTERN_YYYYMMDD);
			actualEtdTimeStr = DateUtil.format(new Date(asnInformationDomain
					.getSpsTAsnDomain().getActualEtd().getTime()),
					DateUtil.PATTERN_HHMM);
		}
		if (!StringUtil.checkNullOrEmpty(asnInformationDomain
				.getSpsTAsnDomain().getPlanEta())) {
			planEtaDateStr = DateUtil.format(new Date(asnInformationDomain
					.getSpsTAsnDomain().getPlanEta().getTime()),
					DateUtil.PATTERN_YYYYMMDD);
			planEtaTimeStr = DateUtil.format(new Date(asnInformationDomain
					.getSpsTAsnDomain().getPlanEta().getTime()),
					DateUtil.PATTERN_HHMM);
		}

		createDateHeaderStr = DateUtil.format(new Date(asnInformationDomain
				.getSpsTAsnDomain().getCreateDatetime().getTime()),
				DateUtil.PATTERN_YYYYMMDD);
		createTimeHeaderStr = DateUtil.format(new Date(asnInformationDomain
				.getSpsTAsnDomain().getCreateDatetime().getTime()),
				DateUtil.PATTERN_HHMMSS);

		updateDateHeaderStr = DateUtil.format(new Date(asnInformationDomain
				.getSpsTAsnDomain().getLastUpdateDatetime().getTime()),
				DateUtil.PATTERN_YYYYMMDD);
		updateTimeHeaderStr = DateUtil.format(new Date(asnInformationDomain
				.getSpsTAsnDomain().getLastUpdateDatetime().getTime()),
				DateUtil.PATTERN_HHMMSS);
		updateByHeaderStr = updateDscId;

		spsAsnStatusStr = asnInformationDomain.getSpsTAsnDomain()
				.getAsnStatus();
		noOfPalletStr = StringUtil.nullToEmpty(asnInformationDomain
				.getSpsTAsnDomain().getNumberOfPallet());
		tripNoStr = StringUtil.nullToEmpty(asnInformationDomain
				.getSpsTAsnDomain().getTripNo());

		for (AsnInfoDetailDomain asnDetail : asnInformationDomain
				.getAsnInfoDetailList()) {
			String deliveryDate = Constants.EMPTY_STRING;
			String createDate = Constants.EMPTY_STRING;
			String createTime = Constants.EMPTY_STRING;
			String updateDate = Constants.EMPTY_STRING;
			String updateTime = Constants.EMPTY_STRING;

			supplierCodeDetailList.add(asnInformationDomain.getSpsTAsnDomain()
					.getVendorCd());
			asnNoDetailList.add(asnInformationDomain.getSpsTAsnDomain()
					.getAsnNo());
			dPcdDetailList.add(asnInformationDomain.getSpsTAsnDomain()
					.getDPcd());
			if (StringUtil.checkNullOrEmpty(asnDetail.getSpsTDoDetailDomain())) {
				cigmaCurrentDoNoList.add(asnDetail.getSpsTAsnDetailDomain()
						.getCigmaDoNo());
			} else {
				cigmaCurrentDoNoList.add(asnDetail.getSpsTDoDetailDomain()
						.getChgCigmaDoNo());
			}
			cigmaOriginalDoNoList.add(asnDetail.getSpsTAsnDetailDomain()
					.getCigmaDoNo());
			dPnList.add(asnDetail.getSpsTAsnDetailDomain().getDPn());
			spsDoNoList.add(asnDetail.getSpsTAsnDetailDomain().getSpsDoNo());
			shippingQtyList.add(asnDetail.getSpsTAsnDetailDomain()
					.getShippingQty().toString());
			receivedQtyList.add(Constants.STR_ZERO);
			remainingQtyList.add(Constants.STR_ZERO);

			if (!StringUtil.checkNullOrEmpty(asnDetail.getSpsTDoDomain()
					.getDeliveryDatetime())) {
				deliveryDate = DateUtil.format(new Date(asnDetail
						.getSpsTDoDomain().getDeliveryDatetime().getTime()),
						DateUtil.PATTERN_YYYYMMDD);
			}
			dueDateList.add(deliveryDate);
			rcvLaneList.add(asnDetail.getSpsTAsnDetailDomain().getRcvLane()
					.toString());
			receivedDateList.add(Constants.EMPTY_STRING);

			createDate = DateUtil.format(new Date(asnDetail
					.getSpsTAsnDetailDomain().getCreateDatetime().getTime()),
					DateUtil.PATTERN_YYYYMMDD);
			createTime = DateUtil.format(new Date(asnDetail
					.getSpsTAsnDetailDomain().getCreateDatetime().getTime()),
					DateUtil.PATTERN_HHMMSS);
			createDateDetailList.add(createDate);
			createTimeDetailList.add(createTime);

			updateDate = DateUtil.format(
					new Date(asnDetail.getSpsTAsnDetailDomain()
							.getLastUpdateDatetime().getTime()),
					DateUtil.PATTERN_YYYYMMDD);
			updateTime = DateUtil.format(
					new Date(asnDetail.getSpsTAsnDetailDomain()
							.getLastUpdateDatetime().getTime()),
					DateUtil.PATTERN_HHMMSS);
			updateDateDetailList.add(updateDate);
			updateTimeDetailList.add(updateTime);
			updateByDetailList.add(updateDscId);
			pnProcessFlagList.add(Constants.EMPTY_STRING);

			qtyBoxList.add(asnDetail.getSpsTAsnDetailDomain().getQtyBox()
					.toString());
			unitOfMeasureList.add(asnDetail.getSpsTAsnDetailDomain()
					.getUnitOfMeasure().toString());
			changeReasonCdList.add(StringUtil.nullToEmpty(asnDetail
					.getSpsTAsnDetailDomain().getChangeReasonCd()));
		}

		domain.setParamMap(WebServiceConstants.PARAM_SUPPLIER_CODE_HEADER,
				supplierCodeHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_HEADER,
				asnNoHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_DPCD_HEADER, dPcdHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_RECEIVING_STATUS_HEADER,
				rcvStatusHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_PLAN_ETA_DATE,
				planEtaDateStr);
		domain.setParamMap(WebServiceConstants.PARAM_PLAN_ETA_TIME,
				planEtaTimeStr);
		domain.setParamMap(WebServiceConstants.PARAM_ACTUAL_ETD_DATE,
				actualEtdDateStr);
		domain.setParamMap(WebServiceConstants.PARAM_ACTUAL_ETD_TIME,
				actualEtdTimeStr);
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE_HEADER,
				createDateHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME_HEADER,
				createTimeHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_BY_HEADER,
				updateByHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE_HEADER,
				updateDateHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME_HEADER,
				updateTimeHeaderStr);
		domain.setParamMap(WebServiceConstants.PARAM_PROCESS_FLAG,
				processFlagStr);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_ASN_STATUS,
				spsAsnStatusStr);
		domain.setParamMap(WebServiceConstants.PARAM_NUMBER_OF_PALLET,
				noOfPalletStr);
		domain.setParamMap(WebServiceConstants.PARAM_TRIP_NO, tripNoStr);

		domain.setParamMap(
				WebServiceConstants.PARAM_SUPPLIER_CODE_DETAIL,
				StringUtil
						.convertListToStringCommaSeperate(supplierCodeDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_DETAIL,
				StringUtil.convertListToStringCommaSeperate(asnNoDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_DPCD_DETAIL,
				StringUtil.convertListToStringCommaSeperate(dPcdDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_CURRENT_DO_NO,
				StringUtil
						.convertListToStringCommaSeperate(cigmaCurrentDoNoList));
		domain.setParamMap(
				WebServiceConstants.PARAM_CIGMA_ORIGINAL_DO_NO,
				StringUtil
						.convertListToStringCommaSeperate(cigmaOriginalDoNoList));
		domain.setParamMap(WebServiceConstants.PARAM_RECEIVING_STATUS_DETAIL,
				StringUtil
						.convertListToStringCommaSeperate(rcvStatusDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_PART_NUMBER,
				StringUtil.convertListToStringCommaSeperate(dPnList));
		domain.setParamMap(WebServiceConstants.PARAM_SPS_DO_NO,
				StringUtil.convertListToStringCommaSeperate(spsDoNoList));
		domain.setParamMap(WebServiceConstants.PARAM_SHIPPING_QTY,
				StringUtil.convertListToStringCommaSeperate(shippingQtyList));
		domain.setParamMap(WebServiceConstants.PARAM_RECEIVED_QTY,
				StringUtil.convertListToStringCommaSeperate(receivedQtyList));
		domain.setParamMap(WebServiceConstants.PARAM_REMAINING_QTY,
				StringUtil.convertListToStringCommaSeperate(remainingQtyList));
		domain.setParamMap(WebServiceConstants.PARAM_VAR,
				StringUtil.convertListToStringCommaSeperate(varList));
		domain.setParamMap(WebServiceConstants.PARAM_INS,
				StringUtil.convertListToStringCommaSeperate(insList));
		domain.setParamMap(WebServiceConstants.PARAM_WH,
				StringUtil.convertListToStringCommaSeperate(whList));
		domain.setParamMap(WebServiceConstants.PARAM_DUE_DATE,
				StringUtil.convertListToStringCommaSeperate(dueDateList));
		domain.setParamMap(WebServiceConstants.PARAM_RCV_LANE,
				StringUtil.convertListToStringCommaSeperate(rcvLaneList));
		domain.setParamMap(WebServiceConstants.PARAM_RECEIVED_DATE,
				StringUtil.convertListToStringCommaSeperate(receivedDateList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE_DETAIL,
				StringUtil
						.convertListToStringCommaSeperate(createDateDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME_DETAIL,
				StringUtil
						.convertListToStringCommaSeperate(createTimeDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_BY_DETAIL,
				StringUtil.convertListToStringCommaSeperate(updateByDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE_DETAIL,
				StringUtil
						.convertListToStringCommaSeperate(updateDateDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME_DETAIL,
				StringUtil
						.convertListToStringCommaSeperate(updateTimeDetailList));
		domain.setParamMap(WebServiceConstants.PARAM_QTY_BOX,
				StringUtil.convertListToStringCommaSeperate(qtyBoxList));
		domain.setParamMap(WebServiceConstants.PARAM_UNIT_OF_MEASURE,
				StringUtil.convertListToStringCommaSeperate(unitOfMeasureList));
		domain.setParamMap(WebServiceConstants.PARAM_CHANGE_REASON_CD,
				StringUtil.convertListToStringCommaSeperate(changeReasonCdList));
		domain.setParamMap(WebServiceConstants.PARAM_PN_PROCESS_FLAG,
				StringUtil.convertListToStringCommaSeperate(pnProcessFlagList));
	}

	/**
	 * Set param map from asn maintenance return domain to web service parameter
	 * domain.
	 * 
	 * @param domain
	 *            the web service parameter domain
	 * @param updateDscId
	 *            the update dsc id
	 * @param asnList
	 *            the liset of asn maintenance return domain
	 * @param caseType
	 *            the case of ASN Maintenance
	 * */
	private static void setParamMapAsnInformationForAsnResource(
			WebServiceCallerRestDomain domain,
			List<AsnMaintenanceReturnDomain> asnList, String updateDscId,
			String caseType) {
		// Header
		String supplierCodeHeader = Constants.EMPTY_STRING;
		String asnNoHeader = Constants.EMPTY_STRING;
		String dPcdHeader = Constants.EMPTY_STRING;
		String rcvStatusHeader = Constants.EMPTY_STRING;
		String actualEtdDateStr = Constants.EMPTY_STRING;
		String actualEtdTimeStr = Constants.EMPTY_STRING;
		String planEtaDateStr = Constants.EMPTY_STRING;
		String planEtaTimeStr = Constants.EMPTY_STRING;
		String createDateHeader = Constants.EMPTY_STRING;
		String createTimeHeader = Constants.EMPTY_STRING;
		String updateByHeader = Constants.EMPTY_STRING;
		String updateDateHeader = Constants.EMPTY_STRING;
		String updateTimeHeader = Constants.EMPTY_STRING;
		String lastUpdateDate = Constants.EMPTY_STRING;
		String lastUpdateTime = Constants.EMPTY_STRING;
		String processFlag = Constants.EMPTY_STRING;
		String asnStatusCriteria = Constants.EMPTY_STRING;
		/* Start add attribute for ASN header */
		String receiveByScan = Constants.EMPTY_STRING;
		String tm = Constants.EMPTY_STRING;
		/* End add attribute for ASN header */

		// Detail
		List<String> supplierCodeDetailList = new ArrayList<String>();
		List<String> asnNoDetailList = new ArrayList<String>();
		List<String> dPcdDetailList = new ArrayList<String>();
		List<String> cigmaCurrentDoNoList = new ArrayList<String>();
		List<String> cigmaOriginalDoNoList = new ArrayList<String>();
		List<String> rcvStatusDetailList = new ArrayList<String>();
		List<String> dPnList = new ArrayList<String>();
		List<String> spsDoNoList = new ArrayList<String>();
		List<String> shippingQtyList = new ArrayList<String>();
		List<String> receivedQtyList = new ArrayList<String>();
		List<String> remainingQtyList = new ArrayList<String>();
		List<String> varList = new ArrayList<String>();
		List<String> insList = new ArrayList<String>();
		List<String> whList = new ArrayList<String>();
		List<String> dueDateList = new ArrayList<String>();
		List<String> rcvLaneList = new ArrayList<String>();
		List<String> receivedDateList = new ArrayList<String>();
		List<String> createDateDetailList = new ArrayList<String>();
		List<String> createTimeDetailList = new ArrayList<String>();
		List<String> updateByDetailList = new ArrayList<String>();
		List<String> updateDateDetailList = new ArrayList<String>();
		List<String> updateTimeDetailList = new ArrayList<String>();
		List<String> pnLastUpdateDateList = new ArrayList<String>();
		List<String> pnLastUpdateTimeList = new ArrayList<String>();
		List<String> pnProcessFlagList = new ArrayList<String>();
		/* Start add attribute for ASN detail */
		List<String> kanbanType = new ArrayList<String>();
		/* End add attribute for ASN detail */

		AsnMaintenanceReturnDomain asnHeader = asnList.get(Constants.ZERO);

		supplierCodeHeader = asnHeader.getAsnDomain().getVendorCd();
		asnNoHeader = asnHeader.getAsnDomain().getAsnNo();
		dPcdHeader = asnHeader.getAsnDomain().getDPcd();
		updateByHeader = updateDscId;
		updateDateHeader = DateUtil.format(new Date(asnHeader.getAsnDomain()
				.getLastUpdateDatetime().getTime()), DateUtil.PATTERN_YYYYMMDD);
		updateTimeHeader = DateUtil.format(new Date(asnHeader.getAsnDomain()
				.getLastUpdateDatetime().getTime()), DateUtil.PATTERN_HHMMSS);

		if (Constants.STR_CREATE.equals(caseType)) {
			if (!StringUtil.checkNullOrEmpty(asnHeader.getAsnDomain()
					.getActualEtd())) {
				actualEtdDateStr = DateUtil.format(new Date(asnHeader
						.getAsnDomain().getActualEtd().getTime()),
						DateUtil.PATTERN_YYYYMMDD);
				actualEtdTimeStr = DateUtil.format(new Date(asnHeader
						.getAsnDomain().getActualEtd().getTime()),
						DateUtil.PATTERN_HHMM);
			}
			if (!StringUtil.checkNullOrEmpty(asnHeader.getAsnDomain()
					.getPlanEta())) {
				planEtaDateStr = DateUtil.format(new Date(asnHeader
						.getAsnDomain().getPlanEta().getTime()),
						DateUtil.PATTERN_YYYYMMDD);
				planEtaTimeStr = DateUtil.format(new Date(asnHeader
						.getAsnDomain().getPlanEta().getTime()),
						DateUtil.PATTERN_HHMM);
			}

			rcvStatusHeader = Constants.ASN_STATUS_N;
			createDateHeader = DateUtil.format(new Date(asnHeader
					.getAsnDomain().getCreateDatetime().getTime()),
					DateUtil.PATTERN_YYYYMMDD);
			createTimeHeader = DateUtil.format(new Date(asnHeader
					.getAsnDomain().getCreateDatetime().getTime()),
					DateUtil.PATTERN_HHMMSS);

			/* Start add the value to attribute */
			receiveByScan = asnHeader.getGroupDoDomain().getReceiveByScan();
			tm = asnHeader.getGroupDoDomain().getTm();
			/* End add the value to attribute */

			domain.setParamMap(WebServiceConstants.PARAM_SUPPLIER_CODE_HEADER,
					supplierCodeHeader);
			domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_HEADER,
					asnNoHeader);
			domain.setParamMap(WebServiceConstants.PARAM_DPCD_HEADER,
					dPcdHeader);
			domain.setParamMap(
					WebServiceConstants.PARAM_RECEIVING_STATUS_HEADER,
					rcvStatusHeader);
			domain.setParamMap(WebServiceConstants.PARAM_PLAN_ETA_DATE,
					planEtaDateStr);
			domain.setParamMap(WebServiceConstants.PARAM_PLAN_ETA_TIME,
					planEtaTimeStr);
			domain.setParamMap(WebServiceConstants.PARAM_ACTUAL_ETD_DATE,
					actualEtdDateStr);
			domain.setParamMap(WebServiceConstants.PARAM_ACTUAL_ETD_TIME,
					actualEtdTimeStr);
			domain.setParamMap(WebServiceConstants.PARAM_CREATE_DATE_HEADER,
					createDateHeader);
			domain.setParamMap(WebServiceConstants.PARAM_CREATE_TIME_HEADER,
					createTimeHeader);
			domain.setParamMap(WebServiceConstants.PARAM_UPDATE_BY_HEADER,
					updateByHeader);
			domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE_HEADER,
					updateDateHeader);
			domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME_HEADER,
					updateTimeHeader);
			domain.setParamMap(WebServiceConstants.PARAM_PROCESS_FLAG,
					processFlag);
			/* Start add the attribute to parameter of web services */
			domain.setParamMap(WebServiceConstants.PARAM_RECEIVE_BY_SCAN,
					receiveByScan);
			domain.setParamMap(WebServiceConstants.PARAM_TM, tm);
			/* End add the attribute to parameter of web services */

			String createDate = Constants.EMPTY_STRING;
			String createTime = Constants.EMPTY_STRING;
			String updateDate = Constants.EMPTY_STRING;
			String updateTime = Constants.EMPTY_STRING;
			String deliveryDate = Constants.EMPTY_STRING;

			for (AsnMaintenanceReturnDomain asnDetail : asnList) {
				supplierCodeDetailList.add(asnDetail.getAsnDomain()
						.getVendorCd());
				asnNoDetailList.add(asnDetail.getAsnDomain().getAsnNo());
				dPcdDetailList.add(asnDetail.getAsnDomain().getDPcd());
				if (StringUtil.checkNullOrEmpty(asnDetail
						.getGroupDoDetailDomain().getChgCigmaDoNo())) {
					cigmaCurrentDoNoList.add(asnDetail.getAsnDetailDomain()
							.getCigmaDoNo());
				} else {
					cigmaCurrentDoNoList.add(asnDetail.getGroupDoDetailDomain()
							.getChgCigmaDoNo());
				}
				cigmaOriginalDoNoList.add(asnDetail.getAsnDetailDomain()
						.getCigmaDoNo());
				rcvStatusDetailList.add(Constants.ASN_STATUS_N);
				dPnList.add(asnDetail.getAsnDetailDomain().getDPn());
				rcvLaneList.add(asnDetail.getAsnDetailDomain().getRcvLane());
				receivedDateList.add(Constants.EMPTY_STRING);
				spsDoNoList.add(asnDetail.getAsnDetailDomain().getSpsDoNo());
				/*
				 * if(Constants.DEFAULT_REVISION.equals(asnDetail.getAsnDetailDomain
				 * ().getRevision())) {
				 * spsDoNoList.add(asnDetail.getAsnDetailDomain().getSpsDoNo());
				 * }else{ StringBuffer spsDoNo = new StringBuffer();
				 * spsDoNo.append(asnDetail.getAsnDetailDomain().getSpsDoNo());
				 * spsDoNo.append(asnDetail.getAsnDetailDomain().getRevision());
				 * spsDoNoList.add(spsDoNo.toString()); }
				 */
				shippingQtyList.add(asnDetail.getAsnDetailDomain()
						.getShippingQty().toString());
				receivedQtyList.add(Constants.STR_ZERO);
				remainingQtyList.add(asnDetail.getAsnDetailDomain()
						.getShippingQty().toString());
				varList.add(Constants.EMPTY_STRING);
				insList.add(Constants.EMPTY_STRING);
				whList.add(Constants.EMPTY_STRING);
				if (!StringUtil.checkNullOrEmpty(asnDetail.getGroupDoDomain()
						.getDeliveryDatetime())) {
					deliveryDate = DateUtil.format(
							new Date(asnDetail.getGroupDoDomain()
									.getDeliveryDatetime().getTime()),
							DateUtil.PATTERN_YYYYMMDD);
					dueDateList.add(deliveryDate);
				} else {
					dueDateList.add(Constants.EMPTY_STRING);
				}

				createDate = DateUtil.format(
						new Date(asnDetail.getAsnDetailDomain()
								.getLastUpdateDatetime().getTime()),
						DateUtil.PATTERN_YYYYMMDD);
				createTime = DateUtil.format(
						new Date(asnDetail.getAsnDetailDomain()
								.getLastUpdateDatetime().getTime()),
						DateUtil.PATTERN_HHMMSS);
				updateDate = DateUtil.format(
						new Date(asnDetail.getAsnDetailDomain()
								.getLastUpdateDatetime().getTime()),
						DateUtil.PATTERN_YYYYMMDD);
				updateTime = DateUtil.format(
						new Date(asnDetail.getAsnDetailDomain()
								.getLastUpdateDatetime().getTime()),
						DateUtil.PATTERN_HHMMSS);

				createDateDetailList.add(createDate);
				createTimeDetailList.add(createTime);
				updateByDetailList.add(updateDscId);
				updateDateDetailList.add(updateDate);
				updateTimeDetailList.add(updateTime);
				/* Start add the value to attribute */
				kanbanType.add(asnDetail.getGroupDoDetailDomain()
						.getKanbanType());
				/* End add the value to attribute */

				pnProcessFlagList.add(Constants.EMPTY_STRING);
			}

			domain.setParamMap(
					WebServiceConstants.PARAM_SUPPLIER_CODE_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(supplierCodeDetailList));
			domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(asnNoDetailList));
			domain.setParamMap(WebServiceConstants.PARAM_DPCD_DETAIL,
					StringUtil.convertListToStringCommaSeperate(dPcdDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_CIGMA_CURRENT_DO_NO,
					StringUtil
							.convertListToStringCommaSeperate(cigmaCurrentDoNoList));
			domain.setParamMap(
					WebServiceConstants.PARAM_CIGMA_ORIGINAL_DO_NO,
					StringUtil
							.convertListToStringCommaSeperate(cigmaOriginalDoNoList));
			domain.setParamMap(
					WebServiceConstants.PARAM_RECEIVING_STATUS_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(rcvStatusDetailList));
			domain.setParamMap(WebServiceConstants.PARAM_SPS_DO_NO,
					StringUtil.convertListToStringCommaSeperate(spsDoNoList));
			domain.setParamMap(WebServiceConstants.PARAM_SHIPPING_QTY,
					StringUtil
							.convertListToStringCommaSeperate(shippingQtyList));
			domain.setParamMap(WebServiceConstants.PARAM_RECEIVED_QTY,
					StringUtil
							.convertListToStringCommaSeperate(receivedQtyList));
			domain.setParamMap(WebServiceConstants.PARAM_PART_NUMBER,
					StringUtil.convertListToStringCommaSeperate(dPnList));
			domain.setParamMap(WebServiceConstants.PARAM_REMAINING_QTY,
					StringUtil
							.convertListToStringCommaSeperate(remainingQtyList));
			domain.setParamMap(WebServiceConstants.PARAM_VAR,
					StringUtil.convertListToStringCommaSeperate(varList));
			domain.setParamMap(WebServiceConstants.PARAM_INS,
					StringUtil.convertListToStringCommaSeperate(insList));
			domain.setParamMap(WebServiceConstants.PARAM_WH,
					StringUtil.convertListToStringCommaSeperate(whList));
			domain.setParamMap(WebServiceConstants.PARAM_DUE_DATE,
					StringUtil.convertListToStringCommaSeperate(dueDateList));
			domain.setParamMap(WebServiceConstants.PARAM_RCV_LANE,
					StringUtil.convertListToStringCommaSeperate(rcvLaneList));
			domain.setParamMap(WebServiceConstants.PARAM_RECEIVED_DATE,
					StringUtil
							.convertListToStringCommaSeperate(receivedDateList));
			domain.setParamMap(
					WebServiceConstants.PARAM_CREATE_DATE_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(createDateDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_CREATE_TIME_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(createTimeDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_UPDATE_BY_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(updateByDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_UPDATE_DATE_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(updateDateDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_UPDATE_TIME_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(updateTimeDetailList));
			/* Start add the attribute to parameter of web services */
			domain.setParamMap(WebServiceConstants.PARAM_KANBAN_TYPE,
					StringUtil.convertListToStringCommaSeperate(kanbanType));
			/* End add the attribute to parameter of web services */
			domain.setParamMap(
					WebServiceConstants.PARAM_PN_PROCESS_FLAG,
					StringUtil
							.convertListToStringCommaSeperate(pnProcessFlagList));

		} else { // update
			if (Constants.STR_UPDATE.equals(caseType)) {
				rcvStatusHeader = Constants.EMPTY_STRING;
				asnStatusCriteria = Constants.EMPTY_STRING;
			} else {
				rcvStatusHeader = Constants.ASN_STATUS_D;
				asnStatusCriteria = Constants.ASN_STATUS_N;
			}
			lastUpdateDate = asnHeader.getCigmaAsnDomain()
					.getPseudoLastUpdate();
			lastUpdateTime = asnHeader.getCigmaAsnDomain()
					.getPseudoLastUpdateTime();

			for (AsnMaintenanceReturnDomain asnDetail : asnList) {
				supplierCodeDetailList.add(asnDetail.getAsnDomain()
						.getVendorCd());
				asnNoDetailList.add(asnDetail.getAsnDomain().getAsnNo());
				dPcdDetailList.add(asnDetail.getAsnDomain().getDPcd());
				if (StringUtil.checkNullOrEmpty(asnDetail
						.getGroupDoDetailDomain().getChgCigmaDoNo())) {
					cigmaCurrentDoNoList.add(asnDetail.getAsnDetailDomain()
							.getCigmaDoNo());
				} else {
					cigmaCurrentDoNoList.add(asnDetail.getGroupDoDetailDomain()
							.getChgCigmaDoNo());
				}
				dPnList.add(asnDetail.getAsnDetailDomain().getDPn());
				rcvLaneList.add(asnDetail.getAsnDetailDomain().getRcvLane());

				if (Constants.STR_UPDATE.equals(caseType)) {
					shippingQtyList.add(asnDetail.getAsnDetailDomain()
							.getShippingQty().toString());
					remainingQtyList.add(Constants.STR_ZERO);
					if (Constants.STR_UPDATE.equals(asnDetail
							.getReviseAsnDetailStatus())) {
						rcvStatusDetailList.add(Constants.EMPTY_STRING);
					} else {
						rcvStatusDetailList.add(Constants.ASN_STATUS_D);
					}
				} else {
					shippingQtyList.add(Constants.EMPTY_STRING);
					remainingQtyList.add(Constants.EMPTY_STRING);
					rcvStatusDetailList.add(Constants.ASN_STATUS_D);
				}

				updateByDetailList.add(updateDscId);
				// AKAT : 20150625 : for update date and time in UD@25PR same as
				// UD@24PR
				// updateDateDetailList.add(DateUtil.format(
				// new
				// Date(asnDetail.getAsnDetailDomain().getLastUpdateDatetime().getTime()),
				// DateUtil.YYYYMMDD));
				// updateTimeDetailList.add(DateUtil.format(
				// new
				// Date(asnDetail.getAsnDetailDomain().getLastUpdateDatetime().getTime()),
				// DateUtil.HHMMSS));
				updateDateDetailList.add(updateDateHeader);
				updateTimeDetailList.add(updateTimeHeader);

				pnLastUpdateDateList.add(asnDetail.getCigmaAsnDomain()
						.getPseudoPnLastUpdate());
				pnLastUpdateTimeList.add(asnDetail.getCigmaAsnDomain()
						.getPseudoPnLastUpdateTime());
				pnProcessFlagList.add(Constants.EMPTY_STRING);
			}

			// set to domain
			domain.setParamMap(WebServiceConstants.PARAM_LAST_UPDATE,
					lastUpdateDate);
			domain.setParamMap(WebServiceConstants.PARAM_LAST_UPDATE_TIME,
					lastUpdateTime);
			domain.setParamMap(WebServiceConstants.PARAM_SUPPLIER_CODE_HEADER,
					supplierCodeHeader);
			domain.setParamMap(
					WebServiceConstants.PARAM_RECEIVING_STATUS_HEADER,
					rcvStatusHeader);
			domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_HEADER,
					asnNoHeader);
			domain.setParamMap(WebServiceConstants.PARAM_DPCD_HEADER,
					dPcdHeader);
			domain.setParamMap(WebServiceConstants.PARAM_UPDATE_BY_HEADER,
					updateByHeader);
			domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE_HEADER,
					updateDateHeader);
			domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME_HEADER,
					updateTimeHeader);
			domain.setParamMap(WebServiceConstants.PARAM_PROCESS_FLAG,
					processFlag);
			domain.setParamMap(WebServiceConstants.PARAM_SPS_ASN_STATUS,
					asnStatusCriteria);

			domain.setParamMap(
					WebServiceConstants.PARAM_SUPPLIER_CODE_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(supplierCodeDetailList));
			domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(asnNoDetailList));
			domain.setParamMap(WebServiceConstants.PARAM_DPCD_DETAIL,
					StringUtil.convertListToStringCommaSeperate(dPcdDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_CIGMA_CURRENT_DO_NO,
					StringUtil
							.convertListToStringCommaSeperate(cigmaCurrentDoNoList));
			domain.setParamMap(WebServiceConstants.PARAM_RCV_LANE,
					StringUtil.convertListToStringCommaSeperate(rcvLaneList));
			domain.setParamMap(WebServiceConstants.PARAM_PART_NUMBER,
					StringUtil.convertListToStringCommaSeperate(dPnList));
			domain.setParamMap(WebServiceConstants.PARAM_SHIPPING_QTY,
					StringUtil
							.convertListToStringCommaSeperate(shippingQtyList));
			domain.setParamMap(WebServiceConstants.PARAM_REMAINING_QTY,
					StringUtil
							.convertListToStringCommaSeperate(remainingQtyList));
			domain.setParamMap(
					WebServiceConstants.PARAM_RECEIVING_STATUS_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(rcvStatusDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_UPDATE_BY_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(updateByDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_UPDATE_DATE_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(updateDateDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_UPDATE_TIME_DETAIL,
					StringUtil
							.convertListToStringCommaSeperate(updateTimeDetailList));
			domain.setParamMap(
					WebServiceConstants.PARAM_PN_LAST_UPDATE,
					StringUtil
							.convertListToStringCommaSeperate(pnLastUpdateDateList));
			domain.setParamMap(
					WebServiceConstants.PARAM_PN_LAST_UPDATE_TIME,
					StringUtil
							.convertListToStringCommaSeperate(pnLastUpdateTimeList));
			domain.setParamMap(
					WebServiceConstants.PARAM_PN_PROCESS_FLAG,
					StringUtil
							.convertListToStringCommaSeperate(pnProcessFlagList));
		}
	}

	/**
	 * Check exception thrown from web service and decide what to return or
	 * throw.
	 * 
	 * @param webServiceRestException
	 *            Exception thrown from web service
	 * @param domain
	 *            web service parameter domain
	 * @param locale
	 *            for get message
	 * @return return flag
	 * @throws ApplicationException
	 *             ApplicationException
	 * */
	private static int checkException(
			WebServiceCallerRestException webServiceRestException,
			WebServiceCallerRestDomain domain, Locale locale)
			throws ApplicationException {
		if (WebServiceCallerRestException.ERR_EMPTY_RETURN
				.equals(webServiceRestException.getErrorCode())) {
			return Constants.ZERO;
		}

		ApplicationException applicationException = null;

		if (WebServiceCallerRestException.ERR_SERVER_NOT_FOUND
				.equals(webServiceRestException.getErrorCode())
				|| WebServiceCallerRestException.ERR_SERVICE_NOT_FOUND
						.equals(webServiceRestException.getErrorCode())
				|| WebServiceCallerRestException.ERR_SERVICE_UNAVAILABLE
						.equals(webServiceRestException.getErrorCode())) {
			String message = MessageUtil.getApplicationMessageHandledException(
					locale, Constants.MSG_CODE_WEB_SERVICE_NOT_AVAILABLE, null);
			applicationException = new ApplicationException(message,
					webServiceRestException);
		} else {
			applicationException = new ApplicationException(
					webServiceRestException.getMessage());
		}

		throw applicationException;
	}

	/**
	 * Transaction transfer invoice/CN data to JDE.
	 * 
	 * @param invoiceId
	 *            current transaction invoice ID
	 * @param asnNoIn
	 *            ASN Number to get (many ASN Number comma separate).
	 * @param batchJobId
	 *            Job ID from AIJM
	 * @param systemDatetime
	 *            update datetime for batch running current round
	 * @param invoiceInformationDomain
	 *            invoice to transfer
	 * @param cnList
	 *            list of CN to transfer
	 * @param domain
	 *            Web Service Parameter to set
	 * */
	private static void transformParameterToAiwsApInterface(
			BigDecimal invoiceId, String asnNoIn, String batchJobId,
			Timestamp systemDatetime,
			InvoiceInformationDomain invoiceInformationDomain,
			List<CnInformationDomain> cnList, WebServiceCallerRestDomain domain) {
		String transactDate = DateUtil.format(systemDatetime,
				DateUtil.PATTERN_YYYYMMDD_HHMMSS);
		DecimalFormat fourDecimal = new DecimalFormat(
				Constants.AS400_FOUR_DECIMAL_NUMBER_FORMAT);
		DecimalFormat twoDecimal = new DecimalFormat(
				Constants.AS400_TWO_DECIMAL_NUMBER_FORMAT);

		SpsTInvoiceDomain sactInvoice = invoiceInformationDomain
				.getSpsTInvoiceDomain();

		List<String> headerInvoiceNoList = new ArrayList<String>();
		List<String> headerReferenceNoList = new ArrayList<String>();
		List<String> headerCurrencyCodeList = new ArrayList<String>();
		List<String> headerBaseAmountList = new ArrayList<String>();
		List<String> headerVatGstRateList = new ArrayList<String>();
		List<String> headerVatGstAmountList = new ArrayList<String>();
		List<String> headerTotalAmountList = new ArrayList<String>();
		List<String> headerInvoiceDateList = new ArrayList<String>();
		List<String> headerUpdateFlagList = new ArrayList<String>();
		List<String> detailInvoiceNoList = new ArrayList<String>();
		List<String> detailAsnNoList = new ArrayList<String>();
		List<String> detailCigmaDoNoList = new ArrayList<String>();
		List<String> detailDPartNoList = new ArrayList<String>();
		List<String> detailShippingQtyList = new ArrayList<String>();
		List<String> detailDUnitPriceList = new ArrayList<String>();
		List<String> detailSUnitPriceList = new ArrayList<String>();
		List<String> detailTransactionDateList = new ArrayList<String>();
		List<String> detailUpdateFlagList = new ArrayList<String>();

		headerInvoiceNoList.add(sactInvoice.getInvoiceNo());
		headerReferenceNoList.add(Constants.EMPTY_STRING);
		headerCurrencyCodeList.add(sactInvoice.getSCurrencyCd());

		// [IN009] Rounding Mode use HALF_UP
		// headerBaseAmountList.add(fourDecimal.format(sactInvoice.getSBaseAmount()));
		// headerVatGstRateList.add(twoDecimal.format(sactInvoice.getVatRate()));
		// headerVatGstAmountList.add(fourDecimal.format(sactInvoice.getSVatAmount()));
		// headerTotalAmountList.add(fourDecimal.format(sactInvoice.getSTotalAmount()));
		headerBaseAmountList.add(NumberUtil.formatRoundingHalfUp(fourDecimal,
				sactInvoice.getSBaseAmount(), FOUR));
		headerVatGstRateList.add(NumberUtil.formatRoundingHalfUp(twoDecimal,
				sactInvoice.getVatRate(), TWO));
		headerVatGstAmountList.add(NumberUtil.formatRoundingHalfUp(fourDecimal,
				sactInvoice.getSVatAmount(), FOUR));
		headerTotalAmountList.add(NumberUtil.formatRoundingHalfUp(fourDecimal,
				sactInvoice.getSTotalAmount(), FOUR));

		headerInvoiceDateList.add(DateUtil.format(sactInvoice.getInvoiceDate(),
				DateUtil.PATTERN_YYYYMMDD));
		headerUpdateFlagList.add(Constants.EMPTY_STRING);

		for (InvoiceDetailDomain invoiceDetail : invoiceInformationDomain
				.getInvoiceDetailDomainList()) {
			detailInvoiceNoList.add(sactInvoice.getInvoiceNo());
			detailAsnNoList.add(invoiceDetail.getSpsTInvoiceDetailDomain()
					.getAsnNo());
			detailCigmaDoNoList.add(invoiceDetail.getSpsTAsnDetailDomain()
					.getCigmaDoNo());
			detailDPartNoList.add(invoiceDetail.getSpsTInvoiceDetailDomain()
					.getDPn());

			// [IN009] Rounding Mode use HALF_UP
			// detailShippingQtyList.add(twoDecimal.format(
			// invoiceDetail.getSpsTInvoiceDetailDomain().getShippingQty()));
			// detailDUnitPriceList.add(fourDecimal.format(
			// invoiceDetail.getSpsTInvoiceDetailDomain().getDUnitPrice()));
			// detailSUnitPriceList.add(fourDecimal.format(
			// invoiceDetail.getSpsTInvoiceDetailDomain().getSUnitPrice()));
			detailShippingQtyList.add(NumberUtil.formatRoundingHalfUp(
					twoDecimal, invoiceDetail.getSpsTInvoiceDetailDomain()
							.getShippingQty(), TWO));
			detailDUnitPriceList.add(NumberUtil.formatRoundingHalfUp(
					fourDecimal, invoiceDetail.getSpsTInvoiceDetailDomain()
							.getDUnitPrice(), FOUR));
			detailSUnitPriceList.add(NumberUtil.formatRoundingHalfUp(
					fourDecimal, invoiceDetail.getSpsTInvoiceDetailDomain()
							.getSUnitPrice(), FOUR));

			detailTransactionDateList.add(DateUtil.format(invoiceDetail
					.getSpsTAsnDetailDomain().getCreateDatetime(),
					DateUtil.PATTERN_YYYYMMDD));
			detailUpdateFlagList.add(Constants.EMPTY_STRING);
		}

		if (Constants.ZERO != cnList.size()) {
			CnInformationDomain cn = cnList.get(Constants.ZERO);

			headerInvoiceNoList.add(cn.getSpsTCnDomain().getCnNo());
			headerReferenceNoList.add(cn.getSpsTInvoiceDomain().getInvoiceNo());
			headerCurrencyCodeList.add(cn.getSpsTInvoiceDomain()
					.getSCurrencyCd());

			// [IN009] Rounding Mode use HALF_UP
			// headerBaseAmountList.add(fourDecimal.format(cn.getSpsTCnDomain().getBaseAmount()));
			// headerVatGstRateList.add(twoDecimal.format(cn.getSpsTInvoiceDomain().getVatRate()));
			// headerVatGstAmountList.add(fourDecimal.format(cn.getSpsTCnDomain().getVatAmount()));
			// headerTotalAmountList.add(fourDecimal.format(cn.getSpsTCnDomain().getTotalAmount()));
			headerBaseAmountList.add(NumberUtil.formatRoundingHalfUp(
					fourDecimal, cn.getSpsTCnDomain().getBaseAmount(), FOUR));
			headerVatGstRateList.add(NumberUtil.formatRoundingHalfUp(
					twoDecimal, cn.getSpsTInvoiceDomain().getVatRate(), TWO));
			headerVatGstAmountList.add(NumberUtil.formatRoundingHalfUp(
					fourDecimal, cn.getSpsTCnDomain().getVatAmount(), FOUR));
			headerTotalAmountList.add(NumberUtil.formatRoundingHalfUp(
					fourDecimal, cn.getSpsTCnDomain().getTotalAmount(), FOUR));

			headerInvoiceDateList.add(DateUtil.format(cn.getSpsTCnDomain()
					.getCnDate(), DateUtil.PATTERN_YYYYMMDD));
			headerUpdateFlagList.add(Constants.EMPTY_STRING);

			for (CnDetailInformationDomain cnDetail : cn
					.getCnDetailInformationList()) {
				detailInvoiceNoList.add(cn.getSpsTCnDomain().getCnNo());
				detailAsnNoList
						.add(cnDetail.getSpsTCnDetailDomain().getAsnNo());
				detailCigmaDoNoList.add(cnDetail.getSpsTAsnDetailDomain()
						.getCigmaDoNo());
				detailDPartNoList
						.add(cnDetail.getSpsTCnDetailDomain().getDPn());

				// [IN009] Rounding Mode use HALF_UP
				// detailShippingQtyList.add(twoDecimal.format(
				// cnDetail.getSpsTAsnDetailDomain().getShippingQty()));
				// detailDUnitPriceList.add(fourDecimal.format(
				// cnDetail.getSpsTCnDetailDomain().getDPriceUnit()));
				// detailSUnitPriceList.add(fourDecimal.format(
				// cnDetail.getSpsTCnDetailDomain().getSPriceUnit()));
				detailShippingQtyList.add(NumberUtil.formatRoundingHalfUp(
						twoDecimal, cnDetail.getSpsTAsnDetailDomain()
								.getShippingQty(), TWO));
				detailDUnitPriceList.add(NumberUtil.formatRoundingHalfUp(
						fourDecimal, cnDetail.getSpsTCnDetailDomain()
								.getDPriceUnit(), FOUR));
				detailSUnitPriceList.add(NumberUtil.formatRoundingHalfUp(
						fourDecimal, cnDetail.getSpsTCnDetailDomain()
								.getSPriceUnit(), FOUR));

				detailTransactionDateList.add(DateUtil.format(cnDetail
						.getSpsTAsnDetailDomain().getCreateDatetime(),
						DateUtil.PATTERN_YYYYMMDD));
				detailUpdateFlagList.add(Constants.EMPTY_STRING);
			}
		}

		domain.setParamMap(WebServiceConstants.PARAM_INVOICE_ID,
				invoiceId.toString());
		domain.setParamMap(WebServiceConstants.PARAM_ASN_NO_IN, asnNoIn);
		domain.setParamMap(WebServiceConstants.PARAM_D_CODE,
				invoiceInformationDomain.getSpsTInvoiceDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_SUPPLIER_CODE,
				invoiceInformationDomain.getSpsMAs400VendorDomain()
						.getVendorCd());

		domain.setParamMap(WebServiceConstants.PARAM_HEADER_INVOICE_NO,
				StringUtil
						.convertListToStringCommaSeperate(headerInvoiceNoList));
		domain.setParamMap(
				WebServiceConstants.PARAM_HEADER_REFERENCE_NO,
				StringUtil
						.convertListToStringCommaSeperate(headerReferenceNoList));
		domain.setParamMap(
				WebServiceConstants.PARAM_HEADER_CURRENCY_CODE,
				StringUtil
						.convertListToStringCommaSeperate(headerCurrencyCodeList));
		domain.setParamMap(WebServiceConstants.PARAM_HEADER_BASE_AMOUNT,
				StringUtil
						.convertListToStringCommaSeperate(headerBaseAmountList));
		domain.setParamMap(WebServiceConstants.PARAM_HEADER_VAT_GST_RATE,
				StringUtil
						.convertListToStringCommaSeperate(headerVatGstRateList));
		domain.setParamMap(
				WebServiceConstants.PARAM_HEADER_VAT_GST_AMOUNT,
				StringUtil
						.convertListToStringCommaSeperate(headerVatGstAmountList));
		domain.setParamMap(
				WebServiceConstants.PARAM_HEADER_TOTAL_AMOUNT,
				StringUtil
						.convertListToStringCommaSeperate(headerTotalAmountList));
		domain.setParamMap(
				WebServiceConstants.PARAM_HEADER_INVOICE_DATE,
				StringUtil
						.convertListToStringCommaSeperate(headerInvoiceDateList));
		domain.setParamMap(WebServiceConstants.PARAM_HEADER_UPDATE_FLAG,
				StringUtil
						.convertListToStringCommaSeperate(headerUpdateFlagList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_INVOICE_NO,
				StringUtil
						.convertListToStringCommaSeperate(detailInvoiceNoList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_ASN_NO,
				StringUtil.convertListToStringCommaSeperate(detailAsnNoList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_CIGMA_DO_NO,
				StringUtil
						.convertListToStringCommaSeperate(detailCigmaDoNoList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_D_PART_NO,
				StringUtil.convertListToStringCommaSeperate(detailDPartNoList));
		domain.setParamMap(
				WebServiceConstants.PARAM_DETAIL_SHIPPING_QTY,
				StringUtil
						.convertListToStringCommaSeperate(detailShippingQtyList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_D_UNIT_PRICE,
				StringUtil
						.convertListToStringCommaSeperate(detailDUnitPriceList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_S_UNIT_PRICE,
				StringUtil
						.convertListToStringCommaSeperate(detailSUnitPriceList));
		domain.setParamMap(
				WebServiceConstants.PARAM_DETAIL_TRANSACTION_DATE,
				StringUtil
						.convertListToStringCommaSeperate(detailTransactionDateList));
		domain.setParamMap(WebServiceConstants.PARAM_DETAIL_UPDATE_FLAG,
				StringUtil
						.convertListToStringCommaSeperate(detailUpdateFlagList));

		domain.setParamMap(WebServiceConstants.PARAM_TRANSACT_USER, batchJobId);
		domain.setParamMap(WebServiceConstants.PARAM_TRANSACT_DATE,
				transactDate.substring(Constants.ZERO, Constants.EIGHT));
		domain.setParamMap(WebServiceConstants.PARAM_TRANSACT_TIME,
				transactDate.substring(Constants.EIGHT, Constants.FOURTEEN));
	}

	/**
	 * 
	 * <p>
	 * Fill config server to argument.
	 * </p>
	 * 
	 * @return WebServiceCallerRestDomain domain
	 * @param as400Server
	 *            config
	 * 
	 */
	private static WebServiceCallerRestDomain initialDomain(
			As400ServerConnectionInformationDomain as400Server) {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		// Start : [IN037] separate REST between Order and ASN/Invoice
		// domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME,
		// as400Server.getSpsMAs400LparDomain().getLparName());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigma());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameShare());
		// domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
		// as400Server.getSpsMAs400SchemaDomain().getSchemaNameCigmaReplicate());
		// domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE,
		// as400Server.getSpsMCompanyDensoDomain().getDCd());
		// domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO,
		// as400Server.getSpsMAs400SchemaDomain().getConnectionNo().toString());
		// [IN037] specified member name to read data
		// domain.setParamMap(WebServiceConstants.PARAM_MEMBER_NAME,
		// as400Server.getSpsMAs400SchemaDomain().getMemberName());
		// domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getRestUserName());
		// domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getRestPassword());

		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getOrderLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain().getOrdSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain().getOrdSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getOrdConnectionNo().toString());
		domain.setParamMap(WebServiceConstants.PARAM_MEMBER_NAME, as400Server
				.getSpsMAs400SchemaDomain().getOrdMemberName());

		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());
		// End : [IN037] separate REST between Order and ASN/Invoice
		return domain;
	}

	/**
	 * search Cigma Delivery Order One Way Kanban.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * @param originalCigmaDoNumber
	 *            originalCigmaDoNumber
	 * @param vendorCode
	 *            vendorCode
	 * 
	 * @return the list of pseudo cigma do information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaOneWayKanbanInformationDomain> searchCigmaDeliveryOrderOneWayKanban(
			As400ServerConnectionInformationDomain as400Server, String flag,
			String originalCigmaDoNumber, String vendorCode)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_ORIGINAL_DO_NO,
				originalCigmaDoNumber);
		domain.setParamMap(WebServiceConstants.PARAM_VENDOR_CODE, vendorCode);
		domain.setParamMap(WebServiceConstants.PARAM_KANBAN_SEQ_FLG,
				as400Server.getSpsMCompanyDensoDomain().getReportKanbanSeqFlg());

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_KB_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaKb");
		domain.setUrl(url.toString());
		List<PseudoCigmaOneWayKanbanInformationDomain> resultList = WebServiceCallerRest
				.get(domain,
						new GenericType<List<PseudoCigmaOneWayKanbanInformationDomain>>() {
						});

		for (PseudoCigmaOneWayKanbanInformationDomain pseudo : resultList) {
			if (pseudo.getPseudoCigmaDoNo() != null) {
				pseudo.setPseudoCigmaDoNo(pseudo.getPseudoCigmaDoNo().trim());
			}
			if (pseudo.getPseudosCd() != null) {
				pseudo.setPseudosCd(pseudo.getPseudosCd().trim());
			}
			if (pseudo.getPseudoDeliveryDate() != null) {
				pseudo.setPseudoDeliveryDate(pseudo.getPseudoDeliveryDate()
						.trim());
			}
			if (pseudo.getPseudoDnItemNumber() != null) {
				pseudo.setPseudoDnItemNumber(pseudo.getPseudoDnItemNumber()
						.trim());
			}
			if (pseudo.getPseudoTagSeqNumber() != null) {
				pseudo.setPseudoTagSeqNumber(pseudo.getPseudoTagSeqNumber()
						.trim());
			}
			if (pseudo.getPseudoKanbanType() != null) {
				pseudo.setPseudoKanbanType(pseudo.getPseudoKanbanType().trim());
			}
			if (pseudo.getPseudoCapacity() != null) {
				pseudo.setPseudoCapacity(pseudo.getPseudoCapacity().trim());
			}
			if (pseudo.getPseudoPartialFlag() != null) {
				pseudo.setPseudoPartialFlag(pseudo.getPseudoPartialFlag()
						.trim());
			}
		}
		return resultList;
	}

	/**
	 * Search Vendor Code and Original CIGMA D/O Number.
	 * 
	 * @param as400Server
	 *            AS400 server information
	 * @param flag
	 *            flag
	 * 
	 * @return the list of pseudo cigma do information domain
	 * @throws WebServiceCallerRestException
	 *             the WebServiceCallerRestException
	 * */
	public static List<PseudoCigmaOneWayKanbanInformationDomain> searchCigmaDeliveryOrderOnewayKanbanTagSeparateByVendorCode(
			As400ServerConnectionInformationDomain as400Server, String flag)
			throws WebServiceCallerRestException {
		WebServiceCallerRestDomain domain = initialDomain(as400Server);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, flag);
		domain.setParamMap(WebServiceConstants.PARAM_KANBAN_SEQ_FLG,
				as400Server.getSpsMCompanyDensoDomain().getReportKanbanSeqFlg());

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_KB_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaKb");
		domain.setUrl(url.toString());
		List<PseudoCigmaOneWayKanbanInformationDomain> resultList = WebServiceCallerRest
				.post(domain,
						new GenericType<List<PseudoCigmaOneWayKanbanInformationDomain>>() {
						});

		for (PseudoCigmaOneWayKanbanInformationDomain pseudo : resultList) {
			if (pseudo.getPseudoCigmaDoNo() != null) {
				pseudo.setPseudoCigmaDoNo(pseudo.getPseudoCigmaDoNo().trim());
			}
			if (pseudo.getPseudosCd() != null) {
				pseudo.setPseudosCd(pseudo.getPseudosCd().trim());
			}
		}
		return resultList;
	}

	/**
	 * ASN Resource Update ASN.
	 * 
	 * @param as400Server
	 *            the as400ServerConnectionInformation domain
	 * @param currentCigmaDoNo
	 *            currentCigmaDoNo
	 * @param sCd
	 *            sCd
	 * @param kanbanTagFlag
	 *            kanbanTagFlag
	 * @param jobId
	 *            jobId
	 * @return String number of row count
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	public static String kanbanResourceUpdateKanbanTag(
			As400ServerConnectionInformationDomain as400Server,
			String currentCigmaDoNo, String sCd, String kanbanTagFlag,
			String jobId) throws ApplicationException {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();
		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_CURRENT_DO_NO,
				currentCigmaDoNo);
//		domain.setParamMap(WebServiceConstants.PARAM_SUPPLIER_CODE, sCd);
		domain.setParamMap(WebServiceConstants.PARAM_SPS_FLAG, kanbanTagFlag);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_USER_ID, jobId);
		Timestamp current = new Timestamp(System.currentTimeMillis());
		String curDate = DateUtil.format(current, DateUtil.PATTERN_YYYYMMDD);
		String curTime = DateUtil.format(current, DateUtil.PATTERN_HHMMSS);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_DATE, curDate);
		domain.setParamMap(WebServiceConstants.PARAM_UPDATE_TIME, curTime);

		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_KB_RESOURCE);
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());
		String webServiceResult = null;
		try {
			webServiceResult = WebServiceCallerRest.put(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return webServiceResult;
	}

	/**
	 * Get Purchase Order from Web Service server.
	 * 
	 * @return list of pseudo PO Domain
	 * @param cigmaDoNumber
	 *            the string
	 * @param shipmentTotal
	 *            the string
	 * @param as400Server
	 *            the as400 server information
	 * @param locale
	 *            for get Error Message
	 * @throws ApplicationException
	 *             ApplicationException
	 */
	public static String cigmaDoResourceSearchDoReceiving(String cigmaDoNumber,
			BigDecimal shipmentTotal,
			As400ServerConnectionInformationDomain as400Server, Locale locale)
			throws ApplicationException {
		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server
				.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,
				as400Server.getSpsMAs400SchemaDomain()
						.getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server
				.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server
				.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());
		domain.setParamMap(WebServiceConstants.PARAM_CIGMA_DO_NO, cigmaDoNumber);
		domain.setParamMap(WebServiceConstants.PARAM_SHIPPING_QTY,
				shipmentTotal.toString());

		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestIpAddress());
		url.append(WebServiceConstants.CIGMA_DO_RESOURCE);
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());
		// End : [IN037] separate REST between Order and ASN/Invoice

		String webServiceResult = null;
		try {
			webServiceResult = WebServiceCallerRest.post(domain,
					new GenericType<String>() {
					});
		} catch (WebServiceCallerRestException e) {
			throw new ApplicationException(e.getMessage());
		}
		return webServiceResult;
	}

	public static SPSTPoHeaderDNDomain insertPO(
			As400ServerConnectionInformationDomain as400Server, String flag,
			SPSTPoHeaderDNDomain request)
			throws WebServiceCallerRestException, ApplicationException {

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		SPSTPoHeaderDNDomain result = new SPSTPoHeaderDNDomain();
		String serialized = "";
		ObjectMapper oMapper = new ObjectMapper();
		try {
			serialized = new ObjectMapper().writeValueAsString(request);
			serialized=serialized.replaceAll("%", "%25");
			serialized=serialized.replaceAll("\\+", "%2B");
			serialized=serialized.replaceAll("/", "%2F");
			serialized=serialized.replaceAll("\\?", "%3F");
			serialized=serialized.replaceAll("#", "%23");
			serialized=serialized.replaceAll("&", "%26");
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		domain.setParamMap(WebServiceConstants.PARAM_PO_DATA_DN, serialized);

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_PO_DN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaKb");
		//String aa = "http://localhost:6080/CIGMA_REST/rest/cigmaPoDN";
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());

		String resData = WebServiceCallerRest.put(domain,
				new GenericType<String>() {
				});

		try {
			result = new ObjectMapper().readValue(resData,SPSTPoHeaderDNDomain.class);

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}
	
	public static SPSTDoHeaderDNDomain insertDO(
			As400ServerConnectionInformationDomain as400Server, String flag,
			SPSTDoHeaderDNDomain request)
			throws WebServiceCallerRestException, ApplicationException {

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		SPSTDoHeaderDNDomain result = new SPSTDoHeaderDNDomain();
		String serialized = "";
		ObjectMapper oMapper = new ObjectMapper();
		try {
			serialized = new ObjectMapper().writeValueAsString(request);
			
			serialized=serialized.replaceAll("%", "%25");
			serialized=serialized.replaceAll("\\+", "%2B");
			serialized=serialized.replaceAll("/", "%2F");
			serialized=serialized.replaceAll("\\?", "%3F");
			serialized=serialized.replaceAll("#", "%23");
			serialized=serialized.replaceAll("&", "%26");

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		domain.setParamMap(WebServiceConstants.PARAM_DO_DATA_DN, serialized);

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_DO_DN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaKb");
		//String aa = "http://localhost:6080/CIGMA_REST/rest/cigmaDoDN";
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());

		String resData = WebServiceCallerRest.put(domain,
				new GenericType<String>() {
				});

		try {
			result = new ObjectMapper().readValue(resData,SPSTDoHeaderDNDomain.class);

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}
	
	public static String callCommandPO(
			As400ServerConnectionInformationDomain as400Server,
			String schemaCd)
			throws WebServiceCallerRestException, ApplicationException {

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		String result = "";
		
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server.getSpsMAs400SchemaDomain().getSchemaNameOther1());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_CD, schemaCd);
		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_PO_DN_RESOURCE);
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());

		result = WebServiceCallerRest.post(domain,
				new GenericType<String>() {
				});

		
		return result;
	}
	public static String callCommandDO(
			As400ServerConnectionInformationDomain as400Server,
			String schemaCd)
			throws WebServiceCallerRestException, ApplicationException {

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		String result = "";
		
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server.getSpsMAs400SchemaDomain().getSchemaNameOther1());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_CD, schemaCd);
		StringBuffer url = new StringBuffer();
		url.append(as400Server.getSpsMAs400SchemaDomain().getOrdRestIpAddress());
		url.append(WebServiceConstants.CIGMA_DO_DN_RESOURCE);
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getOrdRestPassword());

		result = WebServiceCallerRest.post(domain,
				new GenericType<String>() {
				});

		
		return result;
	}
	
	
	public static SpsMDensoDensoRelationWithASNDNDomain searchAsnData(
			As400ServerConnectionInformationDomain as400Server, String flag,
			SpsMDensoDensoRelationWithASNDNDomain request)
			throws WebServiceCallerRestException, ApplicationException {

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		SpsMDensoDensoRelationWithASNDNDomain result = new SpsMDensoDensoRelationWithASNDNDomain();
		String serialized = "";
		ObjectMapper oMapper = new ObjectMapper();
		try {
			serialized = new ObjectMapper().writeValueAsString(request);
			
			serialized=serialized.replaceAll("%", "%25");
			serialized=serialized.replaceAll("\\+", "%2B");
			serialized=serialized.replaceAll("/", "%2F");
			serialized=serialized.replaceAll("\\?", "%3F");
			serialized=serialized.replaceAll("#", "%23");
			serialized=serialized.replaceAll("&", "%26");

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		domain.setParamMap(WebServiceConstants.PARAM_ASN_DATA_DN, serialized);

		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getAsnInvRestIpAddress());
		url.append(WebServiceConstants.CIGMA_ASN_DN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaKb");
		//String aa = "http://localhost:6080/CIGMA_REST/rest/cigmaDoDN";
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain()
				.getAsnInvRestPassword());

		String resData = WebServiceCallerRest.put(domain,
				new GenericType<String>() {
				});

		try {
			result = new ObjectMapper().readValue(resData,SpsMDensoDensoRelationWithASNDNDomain.class);

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}
	
	
	public static SPSTAsnHeaderDNDomain insertUpdateUD2425Data(
			As400ServerConnectionInformationDomain as400Server, String flag,
			SPSTAsnHeaderDNDomain request,String dCD)
			throws WebServiceCallerRestException, ApplicationException {

		WebServiceCallerRestDomain domain = new WebServiceCallerRestDomain();

		SPSTAsnHeaderDNDomain result = new SPSTAsnHeaderDNDomain();
		String serialized = "";
		ObjectMapper oMapper = new ObjectMapper();
		try {
			if(request.getMode().equals("insert")){
				request.setDatabaseName(dCD.trim());
			}
			serialized = new ObjectMapper().writeValueAsString(request);
			
			serialized=serialized.replaceAll("%", "%25");
			serialized=serialized.replaceAll("\\+", "%2B");
			serialized=serialized.replaceAll("/", "%2F");
			serialized=serialized.replaceAll("\\?", "%3F");
			serialized=serialized.replaceAll("#", "%23");
			serialized=serialized.replaceAll("&", "%26");

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		domain.setParamMap(WebServiceConstants.PARAM_ASN_DATA_DN, serialized);
		//send server share and server replicate for insert data if server status <> 1
		
		domain.setParamMap(WebServiceConstants.PARAM_LPAR_NAME, as400Server.getAsnInvoiceLpar().getLparName());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME, as400Server.getSpsMAs400SchemaDomain().getAsnInvSchemaCigma());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_SHARE,as400Server.getSpsMAs400SchemaDomain().getAsnInvSchemaNameShare());
		domain.setParamMap(WebServiceConstants.PARAM_SCHEMA_NAME_REPLICATE,as400Server.getSpsMAs400SchemaDomain().getAsnInvSchemaCigmaReplicate());
		domain.setParamMap(WebServiceConstants.PARAM_DENSO_CODE, as400Server.getSpsMCompanyDensoDomain().getDCd());
		domain.setParamMap(WebServiceConstants.PARAM_CONNECTION_NO, as400Server.getSpsMAs400SchemaDomain().getAsnInvConnectionNo().toString());
        
		StringBuffer url = new StringBuffer();
		// [IN037] separate REST between Order and ASN/Invoice
		// url.append(as400Server.getSpsMAs400SchemaDomain().getRestIpAddress());
		url.append(as400Server.getSpsMAs400SchemaDomain().getAsnInvRestIpAddress());
		url.append(WebServiceConstants.CIGMA_ASN_DN_RESOURCE);
		// url.append("http://localhost:8086/CIGMA_REST/rest/cigmaKb");
		//String aa = "http://localhost:6080/CIGMA_REST/rest/cigmaDoDN";
		domain.setUrl(url.toString());
		domain.setUserName(as400Server.getSpsMAs400SchemaDomain().getAsnInvRestUserName());
		domain.setPassword(as400Server.getSpsMAs400SchemaDomain().getAsnInvRestPassword());

		String resData = WebServiceCallerRest.post(domain,
				new GenericType<String>() {
				});

		try {
			result = new ObjectMapper().readValue(resData,SPSTAsnHeaderDNDomain.class);

		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage());
		}
		return result;
	}
	
}
