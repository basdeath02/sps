/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.fw;

import java.util.List;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * This class is used for writing JSON string.
 * When our transaction execution is performed successfully,
 *
 * Property named jsonResult.
 * we will set SUCCESS to property named "result", in case of failure we will set
 * FAILURE.
 *
 * Property named jsonList.
 * It is used for storage list of object to be written.
 *
 * Property named jsonParams
 * Its type is Map, and used for storage key and value in case we want to return
 * unique name parameter to client.
 * @author CSI
 */
public class JsonResult {
    
    /**
     * SUCCESS is used when the result of transaction execution is success.
     */
    public static final String SUCCESS = "success";
    /**
     * FAILURE is used when the result of transaction execution is failure.
     */
    public static final String FAILURE = "failure";

    /** The json result. */
    private String jsonResult = null;
    
    /** The json list1. */
    private List<Object> jsonList1 = null;
    
    /** The json list2. */
    private List<Object> jsonList2 = null;
    
    /** The json list3. */
    private List<Object> jsonList3 = null;
    
    /** The json list4. */
    private List<Object> jsonList4 = null;
    
    /** The json list5. */
    private List<Object> jsonList5 = null;
    
    /** The json list6. */
    private List<Object> jsonList6 = null;
    
    /** The json list7. */
    private List<Object> jsonList7 = null;

    /** The json list8. */
    private List<Object> jsonList8 = null;

    /** The json list9. */
    private List<Object> jsonList9 = null;

    /** The json list10. */
    private List<Object> jsonList10 = null;
    
    /** The json list11. */
    private List<Object> jsonList11 = null;

    /** The json list12. */
    private List<Object> jsonList12 = null;
    
    /** The json list13. */
    private List<Object> jsonList13 = null;
    
    /** The json list14. */
    private List<Object> jsonList14 = null;
    
    /** The json list15. */
    private List<Object> jsonList15 = null;
    
    /** The json list16. */
    private List<Object> jsonList16 = null;
    
    /** The json list17. */
    private List<Object> jsonList17 = null;

    /** The json list18. */
    private List<Object> jsonList18 = null;
    

    /**
     * Instantiates a new json result.
     */
    public JsonResult(){
        super();
    }
    
    /**
     * Instantiates a new json result.
     * 
     * @param jsonList1 the json list1
     */
    public JsonResult(List<Object> jsonList1){
        this.jsonList1 = jsonList1;
    }

    /**
     * Instantiates a new json result.
     * 
     * @param jsonList1 the json list1
     * @param jsonList2 the json list2
     */
    public JsonResult(List<Object> jsonList1, List<Object> jsonList2) {
        this.jsonList1 = jsonList1;
        this.jsonList2 = jsonList2;
    }
    
    /**
     * Instantiates a new json result.
     * 
     * @param jsonList1 the json list1
     * @param jsonList2 the json list2
     * @param jsonList3 the json list3
     */
    public JsonResult(List<Object> jsonList1, List<Object> jsonList2, List<Object> jsonList3){
        this.jsonList1 = jsonList1;
        this.jsonList2 = jsonList2;
        this.jsonList3 = jsonList3;
    }
    
    /**
     * Instantiates a new json result.
     * 
     * @param jsonList1 the json list1
     * @param jsonList2 the json list2
     * @param jsonList3 the json list3
     * @param jsonList4 the json list4
     * @param jsonList5 the json list5
     * @param jsonList6 the json list6
     * @param jsonList7 the json list7
     * @param jsonList8 the json list8
     * @param jsonList9 the json list9
     * @param jsonList10 the json list10
     */
    public JsonResult(List<Object> jsonList1, List<Object> jsonList2, List<Object> jsonList3, 
        List<Object> jsonList4, List<Object> jsonList5, List<Object> jsonList6, 
        List<Object> jsonList7, List<Object> jsonList8, List<Object> jsonList9, 
        List<Object> jsonList10){
        this.jsonList1 = jsonList1;
        this.jsonList2 = jsonList2;
        this.jsonList3 = jsonList3;
        this.jsonList4 = jsonList4;
        this.jsonList5 = jsonList5;
        this.jsonList6 = jsonList6;
        this.jsonList7 = jsonList7;
        this.jsonList8 = jsonList8;
        this.jsonList9 = jsonList9;
        this.jsonList10 = jsonList10;
    }

    /**
     * Instantiates a new json result.
     * 
     * @param jsonList1 the json list1
     * @param jsonList2 the json list2
     * @param jsonList3 the json list3
     * @param jsonList4 the json list4
     * @param jsonList5 the json list5
     * @param jsonList6 the json list6
     * @param jsonList7 the json list7
     * @param jsonList8 the json list8
     * @param jsonArrayOfList array of List support 10 items.
     */
    public JsonResult(List<Object> jsonList1, List<Object> jsonList2, List<Object> jsonList3, 
        List<Object> jsonList4, List<Object> jsonList5, List<Object> jsonList6,
        List<Object> jsonList7, List<Object> jsonList8, List<Object>[] jsonArrayOfList)
    {
        this.jsonList1 = jsonList1;
        this.jsonList2 = jsonList2;
        this.jsonList3 = jsonList3;
        this.jsonList4 = jsonList4;
        this.jsonList5 = jsonList5;
        this.jsonList6 = jsonList6;
        this.jsonList7 = jsonList7;
        this.jsonList8 = jsonList8;
        
        if (null != jsonArrayOfList) {
            if (Constants.ZERO < jsonArrayOfList.length) {
                this.jsonList9 = jsonArrayOfList[Constants.ZERO];
            }
            if (Constants.ONE < jsonArrayOfList.length) {
                this.jsonList10 = jsonArrayOfList[Constants.ONE];
            }
            if (Constants.TWO < jsonArrayOfList.length) {
                this.jsonList11 = jsonArrayOfList[Constants.TWO];
            }
            if (Constants.THREE < jsonArrayOfList.length) {
                this.jsonList12 = jsonArrayOfList[Constants.THREE];
            }
            if (Constants.FOUR < jsonArrayOfList.length) {
                this.jsonList13 = jsonArrayOfList[Constants.FOUR];
            }
            if (Constants.FIVE < jsonArrayOfList.length) {
                this.jsonList14 = jsonArrayOfList[Constants.FIVE];
            }
            if (Constants.SIX < jsonArrayOfList.length) {
                this.jsonList15 = jsonArrayOfList[Constants.SIX];
            }
            if (Constants.SEVEN < jsonArrayOfList.length) {
                this.jsonList16 = jsonArrayOfList[Constants.SEVEN];
            }
            if (Constants.EIGHT < jsonArrayOfList.length) {
                this.jsonList17 = jsonArrayOfList[Constants.EIGHT];
            }
            if (Constants.NINE < jsonArrayOfList.length) {
                this.jsonList18 = jsonArrayOfList[Constants.NINE];
            }
        }
    }
    
    /**
     * Gets the json result.
     * 
     * @return failure or success based on a specified key.
     */
    public String getJsonResult() {
        return jsonResult;
    }
    
    /**
     * To use key in this class for setting result in case failure or success.
     * 
     * @param jsonResult the new json result
     */
    public void setJsonResult(String jsonResult) {
        this.jsonResult = jsonResult;
    }
    
    /**
     * Gets the json list1.
     * 
     * @return the jsonList1
     */
    public List<Object> getJsonList1() {
        return jsonList1;
    }

    /**
     * Sets the json list1.
     * 
     * @param jsonList1 the jsonList1 to set
     */
    public void setJsonList1(List<Object> jsonList1) {
        this.jsonList1 = jsonList1;
    }

    /**
     * Gets the json list2.
     * 
     * @return the jsonList2
     */
    public List<Object> getJsonList2() {
        return jsonList2;
    }

    /**
     * Sets the json list2.
     * 
     * @param jsonList2 the jsonList2 to set
     */
    public void setJsonList2(List<Object> jsonList2) {
        this.jsonList2 = jsonList2;
    }

    /**
     * Gets the json list3.
     * 
     * @return the json list3
     */
    public List<Object> getJsonList3() {
        return jsonList3;
    }

    /**
     * Sets the json list3.
     * 
     * @param jsonList3 the new json list3
     */
    public void setJsonList3(List<Object> jsonList3) {
        this.jsonList3 = jsonList3;
    }

    /**
     * Gets the json list4.
     * 
     * @return the json list4
     */
    public List<Object> getJsonList4() {
        return jsonList4;
    }

    /**
     * Sets the json list4.
     * 
     * @param jsonList4 the new json list4
     */
    public void setJsonList4(List<Object> jsonList4) {
        this.jsonList4 = jsonList4;
    }

    /**
     * Gets the json list5.
     * 
     * @return the json list5
     */
    public List<Object> getJsonList5() {
        return jsonList5;
    }

    /**
     * Sets the json list5.
     * 
     * @param jsonList5 the new json list5
     */
    public void setJsonList5(List<Object> jsonList5) {
        this.jsonList5 = jsonList5;
    }

    /**
     * Gets the json list6.
     * 
     * @return the json list6
     */
    public List<Object> getJsonList6() {
        return jsonList6;
    }

    /**
     * Sets the json list6.
     * 
     * @param jsonList6 the new json list6
     */
    public void setJsonList6(List<Object> jsonList6) {
        this.jsonList6 = jsonList6;
    }

    /**
     * Gets the json list7.
     * 
     * @return the json list7
     */
    public List<Object> getJsonList7() {
        return jsonList7;
    }

    /**
     * Sets the json list7.
     * 
     * @param jsonList7 the new json list7
     */
    public void setJsonList7(List<Object> jsonList7) {
        this.jsonList7 = jsonList7;
    }
    
    /**
     * Gets the json list8.
     * 
     * @return the json list8
     */
    public List<Object> getJsonList8() {
        return jsonList8;
    }

    /**
     * Sets the json list8.
     * 
     * @param jsonList8 the new json list8
     */
    public void setJsonList8(List<Object> jsonList8) {
        this.jsonList8 = jsonList8;
    }    
    
    /**
     * Gets the json list9.
     * 
     * @return the json list9
     */
    public List<Object> getJsonList9() {
        return jsonList9;
    }

    /**
     * Sets the json list9.
     * 
     * @param jsonList9 the new json list9
     */
    public void setJsonList9(List<Object> jsonList9) {
        this.jsonList9 = jsonList9;
    }

    /**
     * <p>Get the json list10.</p>
     *
     * @return the json list10
     */
    public List<Object> getJsonList10() {
        return jsonList10;
    }

    /**
     * <p>Set the json list10.</p>
     *
     * @param jsonList10 json list10 to set
     */
    public void setJsonList10(List<Object> jsonList10) {
        this.jsonList10 = jsonList10;
    }

    /**
     * <p>Get the json list11.</p>
     *
     * @return the json list11
     */
    public List<Object> getJsonList11() {
        return jsonList11;
    }

    /**
     * <p>Set the json list11.</p>
     *
     * @param jsonList11 json list11 to set
     */
    public void setJsonList11(List<Object> jsonList11) {
        this.jsonList11 = jsonList11;
    }

    /**
     * <p>Get the json list12.</p>
     *
     * @return the json list12
     */
    public List<Object> getJsonList12() {
        return jsonList12;
    }

    /**
     * <p>Set the json list12.</p>
     *
     * @param jsonList12 json list12 to set
     */
    public void setJsonList12(List<Object> jsonList12) {
        this.jsonList12 = jsonList12;
    }

    /**
     * <p>Get the json list13.</p>
     *
     * @return the json list13
     */
    public List<Object> getJsonList13() {
        return jsonList13;
    }

    /**
     * <p>Set the json list13.</p>
     *
     * @param jsonList13 json list13 to set
     */
    public void setJsonList13(List<Object> jsonList13) {
        this.jsonList13 = jsonList13;
    }
    
    /**
     * <p>Get the json list14.</p>
     *
     * @return the json list14
     */
    public List<Object> getJsonList14() {
        return jsonList14;
    }

    /**
     * <p>Set the json list14.</p>
     *
     * @param jsonList14 json list14 to set
     */
    public void setJsonList14(List<Object> jsonList14) {
        this.jsonList14 = jsonList14;
    }

    /**
     * <p>Get the json list15.</p>
     *
     * @return the json list15
     */
    public List<Object> getJsonList15() {
        return jsonList15;
    }

    /**
     * <p>Set the json list15.</p>
     *
     * @param jsonList15 json list15 to set
     */
    public void setJsonList15(List<Object> jsonList15) {
        this.jsonList15 = jsonList15;
    }

    /**
     * <p>Get the json list16.</p>
     *
     * @return the json list16
     */
    public List<Object> getJsonList16() {
        return jsonList16;
    }

    /**
     * <p>Set the json list16.</p>
     *
     * @param jsonList16 json list16 to set
     */
    public void setJsonList16(List<Object> jsonList16) {
        this.jsonList16 = jsonList16;
    }
    
    /**
     * <p>Get the json list17.</p>
     *
     * @return the json list17
     */
    public List<Object> getJsonList17() {
        return jsonList17;
    }

    /**
     * <p>Set the json list17.</p>
     *
     * @param jsonList17 json list17 to set
     */
    public void setJsonList17(List<Object> jsonList17) {
        this.jsonList17 = jsonList17;
    }
    
    /**
     * <p>Get the json list18.</p>
     *
     * @return the json list18
     */
    public List<Object> getJsonList18() {
        return jsonList18;
    }

    /**
     * <p>Set the json list18.</p>
     *
     * @param jsonList18 json list18 to set
     */
    public void setJsonList18(List<Object> jsonList18) {
        this.jsonList18 = jsonList18;
    }
}