/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 2016/03/31 CSI Akat           [IN063]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.fw;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * For map data file in ApplicationConfig.properties to use in system.
 * Such as START_BUDGET_PERIOD, BASE_DIR_MSG etc...
 * The Class ContextParams.
 * @author CSI
 */
public class ContextParams {

    /** The log. */
    private static final Log LOG = LogFactory.getLog(ContextParams.class);
    
    /** The base dir application conf. */
    private static String baseDirApplicationConf;

    /** The base dir gui. */
    private static String baseDirGui;
    
    /** The base email. */
    private static String baseEmail;
    
    /** The base dir report gui. */
    private static String baseDirReportGui;

    /** The default locale. */
    private static String defaultLocale;

    /** The base dir msg. */
    private static String baseDirMsg;
    
    /** The email smtp. */
    private static String emailSmtp;
    
    /** The default e-mail from. */
    private static String defaultEmailFrom;

    // Web Service Part ===========================================================================
    
    /** The default row per page. */
    private static int defaultRowPerPage;
    
    /** The legend file id. */
    private static String legendFileId;
    
    /** The max csv file size. */
    private static int maxCsvFileSize;
    
    /** The Jasper Report file path. */
    private static String jasperFilePath;
    
    /** The Default CIGMA P/O No. */
    private static String defaultCigmaPoNo;

    // [IN063] for upload digital signature
    /** Maximum image for digital signature file size. */
    private static int maxSignatureFileSize;

    // Purge process send e-mail to person in charge
    /**Purge process send e-mail to person in charge. */
    private static String purgeEmailSendTo;

    /**
     * Instantiates a new context params.
     */
    public ContextParams(){
        super();
    }

    /**
     * Gets the base dir application conf.
     * 
     * @return the base dir application conf
     */
    public static String getBaseDirApplicationConf() {
        LOG.debug("BASE DIR APP CONF: " + baseDirApplicationConf);
        return baseDirApplicationConf;
    }

    /**
     * Sets the base dir application conf.
     * 
     * @param baseDirApplicationConf the new base dir application conf
     */
    public static void setBaseDirApplicationConf(String baseDirApplicationConf) {
        ContextParams.baseDirApplicationConf = baseDirApplicationConf;
    }

    /**
     * Gets the base dir gui.
     * 
     * @return the base dir gui
     */
    public static String getBaseDirGui() {
        LOG.debug("BASE DIR GUI: " + baseDirGui);
        return baseDirGui;
    }

    /**
     * Sets the base dir gui.
     * 
     * @param baseDirGui the new base dir gui
     */
    public static void setBaseDirGui(String baseDirGui) {
        ContextParams.baseDirGui = baseDirGui;
    }

    /**
     * Gets the default locale.
     * 
     * @return the default locale
     */
    public static String getDefaultLocale() {
        LOG.debug("DEFAULT LOCALE: " + defaultLocale);
        return defaultLocale;
    }

    /**
     * Sets the default locale.
     * 
     * @param defaultLocale the new default locale
     */
    public static void setDefaultLocale(String defaultLocale) {
        ContextParams.defaultLocale = defaultLocale;
    }

    /**
     * Gets the base dir msg.
     * 
     * @return the base dir msg
     */
    public static String getBaseDirMsg() {
        LOG.debug("BASE DIR MST: " + baseDirMsg);
        return baseDirMsg;
    }

    /**
     * Sets the base dir msg.
     * 
     * @param baseDirMsg the new base dir msg
     */
    public static void setBaseDirMsg(String baseDirMsg) {
        ContextParams.baseDirMsg = baseDirMsg;
    }

    /**
     * Gets the base dir rport gui.
     * 
     * @return the base dir rport gui
     */
    public static String getBaseDirReportGui() {
        return baseDirReportGui;
    }

    /**
     * Sets the base dir report gui.
     * 
     * @param baseDirReportGui the new base dir report gui
     */
    public static void setBaseDirReportGui(String baseDirReportGui) {
        ContextParams.baseDirReportGui = baseDirReportGui;
    }

    /**
     * Gets the base email.
     * 
     * @return the base email
     */
    public static String getBaseEmail() {
        return baseEmail;
    }

    /**
     * Sets the base email.
     * 
     * @param baseEmail the new base email
     */
    public static void setBaseEmail(String baseEmail) {
        ContextParams.baseEmail = baseEmail;
    }

    /**
     * Gets the default row per page.
     * 
     * @return the default row per page
     */
    public static int getDefaultRowPerPage() {
        return defaultRowPerPage;
    }

    /**
     * Sets the default row per page.
     * 
     * @param defaultRowPerPage the new default row per page
     */
    public static void setDefaultRowPerPage(int defaultRowPerPage) {
        ContextParams.defaultRowPerPage = defaultRowPerPage;
    }

    /**
     * <p>Gets the email smtp.</p>
     *
     * @return the email smtp
     */
    public static String getEmailSmtp() {
        return emailSmtp;
    }

    /**
     * Sets the email smtp.
     * 
     * @param emailSmtp the new email smtp
     */
    public static void setEmailSmtp(String emailSmtp) {
        ContextParams.emailSmtp = emailSmtp;
    }
    
    /**
     * <p>Gets the default email from.</p>
     *
     * @return the default email from
     */
    public static String getDefaultEmailFrom() {
        return defaultEmailFrom;
    }

    /**
     * Sets the default email from.
     * 
     * @param defaultEmailFrom the new default email from
     */
    public static void setDefaultEmailFrom(String defaultEmailFrom) {
        ContextParams.defaultEmailFrom = defaultEmailFrom;
    }

    /**
     * <p>Gets the legend file id.</p>
     *
     * @return the legend file id
     */
    public static String getLegendFileId() {
        return legendFileId;
    }

    /**
     * Sets the legend file id.
     * 
     * @param legendFileId the new legend file id
     */
    public static void setLegendFileId(String legendFileId) {
        ContextParams.legendFileId = legendFileId;
    }

    /**
     * <p>Gets the max csv file size.</p>
     *
     * @return the max csv file size
     */
    public static int getMaxCsvFileSize() {
        return maxCsvFileSize;
    }

    /**
     * Sets the max csv file size.
     * 
     * @param maxCsvFileSize the new max csv file size
     */
    public static void setMaxCsvFileSize(int maxCsvFileSize) {
        ContextParams.maxCsvFileSize = maxCsvFileSize;
    }

    /**
     * <p>Getter method for jasperFilePath.</p>
     *
     * @return the jasperFilePath
     */
    public static String getJasperFilePath() {
        return jasperFilePath;
    }

    /**
     * <p>Setter method for jasperFilePath.</p>
     *
     * @param jasperFilePath Set for jasperFilePath
     */
    public static void setJasperFilePath(String jasperFilePath) {
        ContextParams.jasperFilePath = jasperFilePath;
    }

    /**
     * <p>Getter method for defaultCigmaPoNo.</p>
     *
     * @return the defaultCigmaPoNo
     */
    public static String getDefaultCigmaPoNo() {
        return defaultCigmaPoNo;
    }

    /**
     * <p>Setter method for defaultCigmaPoNo.</p>
     *
     * @param defaultCigmaPoNo Set for defaultCigmaPoNo
     */
    public static void setDefaultCigmaPoNo(String defaultCigmaPoNo) {
        ContextParams.defaultCigmaPoNo = defaultCigmaPoNo;
    }

    /**
     * <p>Getter method for purgeEmailSendTo.</p>
     *
     * @return the purgeEmailSendTo
     */
    public static String getPurgeEmailSendTo() {
        return purgeEmailSendTo;
    }

    /**
     * <p>Setter method for purgeEmailSendTo.</p>
     *
     * @param purgeEmailSendTo Set for purgeEmailSendTo
     */
    public static void setPurgeEmailSendTo(String purgeEmailSendTo) {
        ContextParams.purgeEmailSendTo = purgeEmailSendTo;
    }

    /**
     * <p>Getter method for maxSignatureFileSize.</p>
     *
     * @return the maxSignatureFileSize
     */
    public static int getMaxSignatureFileSize() {
        return maxSignatureFileSize;
    }

    /**
     * <p>Setter method for maxSignatureFileSize.</p>
     *
     * @param maxSignatureFileSize Set for maxSignatureFileSize
     */
    public static void setMaxSignatureFileSize(int maxSignatureFileSize) {
        ContextParams.maxSignatureFileSize = maxSignatureFileSize;
    }

}