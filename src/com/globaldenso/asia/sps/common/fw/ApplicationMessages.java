/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.fw;

import java.util.ArrayList;
import java.util.List;

/**
 * ApplicationMessages is used for collect ApplicationMessage object.
 * @author CSI
 *
 */
public class ApplicationMessages {

    /**
    * Flag indicates that the result should be displayed in form of pop up message.
    */
    public static final String DISPLAY_ALERT = "alert";
    /**
    * Flag indicates that the result should be displayed in form of pop up message.
    */
    public static final String DISPLAY_TEXTBOX = "textbox";
    
    /** Flag check case message for message type. */
    public static final String MESSAGE_SUCCESS = "success";
    
    /** The Constant MESSAGE_FAILURE. */
    public static final String MESSAGE_FAILURE = "failure";
    
    /** The Constant MESSAGE_FAILURE. */
    public static final String MESSAGE_FAILURE_VIEW = "failure_view";
    /**
    * Flag check case message for message type.
    */
    public static final String MESSAGE_PARTIAL_ERROR_CONCURRENT = "partialerrorconcurrent";
    /**
    * Flag check case message for message type.
    */
    public static final String MESSAGE_FULL_ERROR_CONCURRENT = "fullerrorconcurrent";
    /**
    * Flag check case message for message type.
    */
    public static final String MESSAGE_ERROR = "error";
    
    /** The display type. */
    private String displayType = null;
    
    /** The message type. */
    private String messageType = null;
    
    /** The message result. */
    private String messageResult = null;
    
    /** The application messages. */
    private List<ApplicationMessage> applicationMessages;
 
    /**
    * Instantiates a new application messages.
    */
    public ApplicationMessages() {
        applicationMessages = new ArrayList<ApplicationMessage>();
    }

    /**
    * Instantiates a new application messages.
    * 
    * @param applicationMessage the application message
    */
    public ApplicationMessages(ApplicationMessage applicationMessage) {
        if (null == applicationMessages) {
            this.applicationMessages = new ArrayList<ApplicationMessage>();
        }
        this.applicationMessages.add(applicationMessage);
    }

    /**
    * Gets the application messages.
    * 
    * @return the application messages
    */
    public List<ApplicationMessage> getApplicationMessages() {
        return this.applicationMessages;
    }

    /**
    * Adds the.
    * 
    * @param applicationMessage the application message
    */
    public void add(ApplicationMessage applicationMessage) {

        this.applicationMessages.add(applicationMessage);
    }

    /**
    * Gets the display type.
    * 
    * @return the display type
    */
    public String getDisplayType() {
        return this.displayType;
    }

    /**
    * Sets the display type.
    * 
    * @param displayType the new display type
    */
    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    /**
    * Gets the message type.
    * 
    * @return the message type
    */
    public String getMessageType() {
        return messageType;
    }

    /**
    * Sets the message type.
    * 
    * @param messageType the new message type
    */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
    * Gets the message result.
    * 
    * @return the message result
    */
    public String getMessageResult() {
        return messageResult;
    }

    /**
    * Sets the message result.
    * 
    * @param messageResult the new message result
    */
    public void setMessageResult(String messageResult) {
        this.messageResult = messageResult;
    }
}
