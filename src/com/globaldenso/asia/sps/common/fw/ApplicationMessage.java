/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.fw;

/**
 * ApplicationMessage is used when the system return message to inform user from server.
 * @author CSI
 */
public class ApplicationMessage implements java.io.Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7815725587953617923L;
    
    /** The key. */
    private String key;
    
    /** The params. */
    private Object[] params;

    /**
     * Instantiates a new application message.
     */
    public ApplicationMessage() {
    }

    /**
     * Instantiates a new application message.
     * 
     * @param key the key
     */
    public ApplicationMessage(String key) {
        this.key = key;
    }

    /**
     * Instantiates a new application message.
     * 
     * @param key the key
     * @param params the params
     */
    public ApplicationMessage(String key, String[] params) {
        this.key = key;
        this.params = params;
    }

    /**
     * Gets the key.
     * 
     * @return the key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Sets the key.
     * 
     * @param key the new key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets the params.
     * 
     * @return the params
     */
    public Object[] getParams() {
        return this.params;
    }

    /**
     * Sets the params.
     * 
     * @param params the new params
     */
    public void setParams(String[] params) {
        this.params = params;
    }
}
