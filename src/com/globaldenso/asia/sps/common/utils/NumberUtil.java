package com.globaldenso.asia.sps.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * DateUtil.class is used as date time converter utility class
 * @author CSI
 */
public class NumberUtil {
    
    /** The revision format. */
    public static final DecimalFormat REVISION_FORMAT = 
        new DecimalFormat(Constants.DEFAULT_REVISION);
    
    /** The integer zero. */
    public static final int INT_ZERO = Constants.ZERO;
    
    /** The zero. */
    public static final BigDecimal ZERO = new BigDecimal(Constants.ZERO);
    
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public NumberUtil() {
        super();
    }
    
    /**
     * 
     * <p>Convert String to Bigdecimal.</p>
     *
     * @param num input
     * @return BigDecimal
     */
    public static BigDecimal toBigDecimal(String num){
        try {
            return new BigDecimal(num);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 
     * <p>Convert String to Bigdecimal.</p>
     *
     * @param num input
     * @param scale the int
     * @return BigDecimal
     */
    public static BigDecimal toBigDecimal(String num, int scale){
        return toBigDecimal(num, scale, RoundingMode.HALF_EVEN);
    }
    
    /**
     * 
     * <p>Convert String to Bigdecimal.</p>
     *
     * @param num input
     * @param scale the int
     * @param mode the RoundingMode
     * @return BigDecimal
     */
    public static BigDecimal toBigDecimal(String num, int scale, RoundingMode mode){
        try {
            BigDecimal result =  new BigDecimal(num);
            result.setScale(scale, mode);
            return result;
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 
     * <p>Convert String to big decimal default zero.</p>
     *
     * @param num input
     * @return BigDecimal
     */
    public static BigDecimal toBigDecimalDefaultZero(String num){
        try {
            return new BigDecimal(num);
        } catch (Exception e) {
            return ZERO;
        }
    }
    
    /**
     * 
     * <p>Convert BigDecimal to integer default zero.</p>
     *
     * @param num input
     * @return BigDecimal
     */
    public static Integer toIntegerDefaultZero(BigDecimal num){
        try {
            return num.intValue();
        } catch (Exception e) {
            return INT_ZERO;
        }
    }
    
    /**
     * 
     * <p>Convert String to Bigdecimal.</p>
     *
     * @param num input
     * @return BigDecimal
     */
    public static BigDecimal toBigDecimal(int num){
        try {
            return new BigDecimal(num);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 
     * <p>increase nuber to format.</p>
     *
     * @param num input
     * @param format NumberFormat
     * @return String
     */
    public static String increaseNumber(String num, DecimalFormat format) {
        try {
            return format.format(format.parse(num).intValue() + 1);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 
     * <p>Pad Zero character.</p>
     *
     * @param value value
     * @param amount amountNumber
     * @return String format
     */
    public static String padZero(int value, int amount){
        return String.format( 
            StringUtil.appendsString( 
                Constants.SYMBOL_PERCENT, 
                Constants.STR_ZERO, 
                String.valueOf(amount),
                Constants.STR_D.toLowerCase()), value);
    }
    
    // [IN009] Rounding Mode use HALF_UP
    /**
     * Format number using RoundingMode : HALF_UP
     * @param formatter : formatter
     * @param target : big decimal to format
     * @param scale : scale for format
     * @return String formatted number.
     * */
    public static String formatRoundingHalfUp(DecimalFormat formatter, BigDecimal target, int scale)
    {
        return formatter.format(target.setScale(scale, RoundingMode.HALF_UP));
    }
    /**
     * Format number using RoundingMode : HALF_UP
     * @param formatter : formatter
     * @param target : string to format
     * @param scale : scale for format
     * @return String formatted number.
     * */
    public static String formatRoundingHalfUp(DecimalFormat formatter, String target, int scale)
    {
        String result = Constants.EMPTY_STRING;
        BigDecimal tempBd = new BigDecimal(target);
        tempBd = tempBd.setScale(scale, RoundingMode.HALF_UP);
        result = formatter.format(tempBd);
        return result;
    }
}
