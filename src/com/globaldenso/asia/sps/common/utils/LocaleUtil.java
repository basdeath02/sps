/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.Cookie;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * Utility class for Locale.
 * @author CSI
 */
public class LocaleUtil {

    /**
     * Instantiates a new locale util.
     */
    public LocaleUtil(){
        super();
    }
    
    /**
    * getLocaleFromString performs converting input string to Locale object.
    * @param locale - a String contains locale information in form of language_Country. ex: en_US, ja_JP.
    * @return Locale.
    */
    public static Locale getLocaleFromString(String locale) {
        if (!StringUtil.checkNullOrEmpty(locale)) {
            String[] langCountry = locale.split("_");
            return new Locale(langCountry[0], langCountry[1]);
        } else {
            return null;
        }
    }

    /**
    * getListLocaleFromString performs converting input String to list of locale.
    * @param locales - a String contains Locale information. Ex. "en_US,ja_JP,th_TH".
    * @return List of Locale that has been converted from input string.
    */
    public static List<Locale> getListLocaleFromString(String locales) {
        if (!StringUtil.checkNullOrEmpty(locales)) {
            String[] arrLocale = locales.split(",");
            List<Locale> listLocale = new ArrayList<Locale>();
            for (String locale : arrLocale) {
                String[] langCountry = locale.split("_");
                listLocale.add(new Locale(langCountry[0], langCountry[1]));
            }
            return listLocale;
        } else {
            return null;
        }
    }

    /**
    * getLocaleFromCookie performs getting Locale from cookie.
    * It compares cookie's key with Constants.SESSION_LOCALE_CODE.
    * If cookie's key match with constant key, it will convert string to Locale object.
    * @param cookies - an array of Cookie object.
    * @return Locale object.
    */
    public static Locale getLocaleFromCookie(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (Constants.SESSION_LOCALE_CODE.equals(cookie.getName())) {
                String cookieLocale = cookie.getValue();
                return getLocaleFromString(cookieLocale);
            }
        }
        return null;
    }
}
