package com.globaldenso.asia.sps.common.utils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.business.domain.AsnDetailInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnReportDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PseudoCigmaPoInformationDomain;
import com.globaldenso.asia.sps.business.domain.TransferChangeDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.business.domain.TransferDoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDetailDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoDueDataFromCigmaDomain;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

import static com.globaldenso.asia.sps.common.constant.Constants.ZERO;

/**
 * 
 * <p>Sort Object Utility.</p>
 *
 * @author CSI
 */
public final class SortUtil {

    /** Constant Comparator SpsCigmaPoErrorDomain */
    public static final Comparator<SpsCigmaPoErrorDomain> COMPARE_CIGMA_PO_ERROR = 
        new Comparator<SpsCigmaPoErrorDomain>() {
            public int compare(SpsCigmaPoErrorDomain cigmaPo1,
                SpsCigmaPoErrorDomain cigmaPo2) {
                // Compare Cigma PO No.
                if( null == cigmaPo1.getSCd() 
                    || null == cigmaPo2.getSCd() ){
                    return ZERO;
                }
                int iScd = cigmaPo1.getSCd()
                    .compareTo(cigmaPo2.getSCd());
                if( ZERO != iScd ){
                    return iScd;
                }
                
                // Compare Cigma PO No.
                if( null == cigmaPo1.getCigmaPoNo() 
                    || null == cigmaPo2.getCigmaPoNo() ){
                    return ZERO;
                }
                int iCigmaPoNo = cigmaPo1.getCigmaPoNo()
                    .compareTo(cigmaPo2.getCigmaPoNo());
                if( ZERO != iCigmaPoNo ){
                    return iCigmaPoNo;
                }
                // Compare Data Type.
                if( null == cigmaPo1.getDataType() 
                    || null == cigmaPo2.getDataType() ){
                    return ZERO;
                }
                int iDataType = cigmaPo1.getDataType()
                    .compareTo(cigmaPo2.getDataType());
                if( ZERO != iDataType ){
                    return iDataType;
                }
                
                // Compare Data Type.
                if( null == cigmaPo1.getDPn() 
                    || null == cigmaPo2.getDPn() ){
                    return ZERO;
                }
                int iDpn = cigmaPo1.getDPn()
                    .compareTo(cigmaPo2.getDPn());
                if( ZERO != iDpn ){
                    return iDpn;
                }
                
                // Compare Data Type.
                if( null == cigmaPo1.getEtd() 
                    || null == cigmaPo2.getEtd() ){
                    return ZERO;
                }
                int iEtd = cigmaPo1.getEtd()
                    .compareTo(cigmaPo2.getEtd());
                return iEtd;
            }
        };
        
    /** Constant Comparator SpsCigmaChgPoErrorDomain */
    public static final Comparator<SpsCigmaChgPoErrorDomain> COMPARE_CIGMA_CHG_PO_ERROR = 
        new Comparator<SpsCigmaChgPoErrorDomain>() {
            public int compare(SpsCigmaChgPoErrorDomain cigmaPo1,
                SpsCigmaChgPoErrorDomain cigmaPo2) {
                // Compare Cigma PO No.
                if( null == cigmaPo1.getSCd() 
                    || null == cigmaPo2.getSCd() ){
                    return ZERO;
                }
                int iScd = cigmaPo1.getSCd()
                    .compareTo(cigmaPo2.getSCd());
                if( ZERO != iScd ){
                    return iScd;
                }
                
                // Compare Cigma PO No.
                if( null == cigmaPo1.getCigmaPoNo() 
                    || null == cigmaPo2.getCigmaPoNo() ){
                    return ZERO;
                }
                int iCigmaPoNo = cigmaPo1.getCigmaPoNo()
                    .compareTo(cigmaPo2.getCigmaPoNo());
                if( ZERO != iCigmaPoNo ){
                    return iCigmaPoNo;
                }
                // Compare Data Type.
                if( null == cigmaPo1.getDataType() 
                    || null == cigmaPo2.getDataType() ){
                    return ZERO;
                }
                int iDataType = cigmaPo1.getDataType()
                    .compareTo(cigmaPo2.getDataType());
//                if( ZERO != iDataType ){
                return iDataType;
//                }
                /*
                // Compare Data Type.
                if( null == cigmaPo1.getEtd() 
                    || null == cigmaPo2.getEtd() ){
                    return ZERO;
                }
                int iEtd = cigmaPo1.getEtd()
                    .compareTo(cigmaPo2.getEtd());
                return iEtd;*/
            }
        };
        
    /** Constant Comparator SpsCigmaDoErrorDomain */
    public static final Comparator<SpsCigmaDoErrorDomain> COMPARE_CIGMA_DO_ERROR = 
        new Comparator<SpsCigmaDoErrorDomain>() {
            public int compare(SpsCigmaDoErrorDomain cigmaDo1,
                SpsCigmaDoErrorDomain cigmaDo2) {
                // Compare Cigma PO No.
                if( null == cigmaDo1.getSCd() 
                    || null == cigmaDo2.getSCd() ){
                    return ZERO;
                }
                int iScd = cigmaDo1.getSCd()
                    .compareTo(cigmaDo2.getSCd());
                if( ZERO != iScd ){
                    return iScd;
                }
                
                // Compare Cigma PO No.
                if( null == cigmaDo1.getCigmaDoNo() 
                    || null == cigmaDo2.getCigmaDoNo() ){
                    return ZERO;
                }
                int iCigmaDoNo = cigmaDo1.getCigmaDoNo()
                    .compareTo(cigmaDo2.getCigmaDoNo());
                if( ZERO != iCigmaDoNo ){
                    return iCigmaDoNo;
                }
                // Compare Data Type.
                if( null == cigmaDo1.getDataType() 
                    || null == cigmaDo2.getDataType() ){
                    return ZERO;
                }
                int iDataType = cigmaDo1.getDataType()
                    .compareTo(cigmaDo2.getDataType());
                if( ZERO != iDataType ){
                    return iDataType;
                }
                
                // Compare Spcd.
                if( null == cigmaDo1.getSPcd() 
                    || null == cigmaDo2.getSPcd() ){
                    return ZERO;
                }
                int iSpcd = cigmaDo1.getSPcd()
                    .compareTo(cigmaDo2.getSPcd());
                if( ZERO != iDataType ){
                    return iSpcd;
                }
                
                // Compare Dpn.
                if( null == cigmaDo1.getDPn() 
                    || null == cigmaDo2.getDPn() ){
                    return ZERO;
                }
                int iDpn = cigmaDo1.getDPn()
                    .compareTo(cigmaDo2.getDPn());
                if( ZERO != iDpn ){
                    return iDpn;
                }
                
                // Compare Data Type.
                if( null == cigmaDo1.getDeliveryDate()
                    || null == cigmaDo2.getDeliveryDate() ){
                    return ZERO;
                }
                int iDeliveryDate = cigmaDo1.getDeliveryDate()
                    .compareTo(cigmaDo2.getDeliveryDate());
                return iDeliveryDate;
            }
        };
        
    /** Constant Comparator SpsCigmaChgDoErrorDomain */
    public static final Comparator<SpsCigmaChgDoErrorDomain> COMPARE_CIGMA_CHG_DO_ERROR = 
        new Comparator<SpsCigmaChgDoErrorDomain>() {
            public int compare(SpsCigmaChgDoErrorDomain cigmaPo1,
                SpsCigmaChgDoErrorDomain cigmaPo2) {
                
                // Compare Cigma PO No.
                if( null == cigmaPo1.getErrorTypeFlg() 
                    || null == cigmaPo2.getErrorTypeFlg() ){
                    return ZERO;
                }
                int iErrorTyeFlag = cigmaPo1.getErrorTypeFlg()
                    .compareTo(cigmaPo2.getErrorTypeFlg());
                if( ZERO != iErrorTyeFlag ){
                    return iErrorTyeFlag;
                }
                
                // Compare Cigma PO No.
                if( null == cigmaPo1.getSCd() 
                    || null == cigmaPo2.getSCd() ){
                    return ZERO;
                }
                int iScd = cigmaPo1.getSCd()
                    .compareTo(cigmaPo2.getSCd());
                if( ZERO != iScd ){
                    return iScd;
                }
                
                // Compare Cigma PO No.
                if( null == cigmaPo1.getCigmaDoNo() 
                    || null == cigmaPo2.getCigmaDoNo() ){
                    return ZERO;
                }
                int iCigmaPoNo = cigmaPo1.getCigmaDoNo()
                    .compareTo(cigmaPo2.getCigmaDoNo());
                if( ZERO != iCigmaPoNo ){
                    return iCigmaPoNo;
                }
                // Compare Data Type.
                if( null == cigmaPo1.getDataType() 
                    || null == cigmaPo2.getDataType() ){
                    return ZERO;
                }
                int iDataType = cigmaPo1.getDataType()
                    .compareTo(cigmaPo2.getDataType());
                if( ZERO != iDataType ){
                    return iDataType;
                }
                
                // Compare Spcd.
                if( null == cigmaPo1.getSPcd() 
                    || null == cigmaPo2.getSPcd() ){
                    return ZERO;
                }
                int iSpcd = cigmaPo1.getSPcd()
                    .compareTo(cigmaPo2.getSPcd());
                if( ZERO != iDataType ){
                    return iSpcd;
                }
                
                // Compare Dpn.
                if( null == cigmaPo1.getDPn() 
                    || null == cigmaPo2.getDPn() ){
                    return ZERO;
                }
                int iDpn = cigmaPo1.getDPn()
                    .compareTo(cigmaPo2.getDPn());
                if( ZERO != iDpn ){
                    return iDpn;
                }
                
                // Compare Data Type.
                if( null == cigmaPo1.getDeliveryDate()
                    || null == cigmaPo2.getDeliveryDate() ){
                    return ZERO;
                }
                int iDeliveryDate = cigmaPo1.getDeliveryDate()
                    .compareTo(cigmaPo2.getDeliveryDate());
                return iDeliveryDate;
            }
        };
    
    /** Constant Comparator TransferPoDataFromCigmaDomain */
    public static final Comparator<PseudoCigmaPoInformationDomain> COMPARE_PSEUDO_CIGMA_PO = 
        new Comparator<PseudoCigmaPoInformationDomain>() {
            public int compare(PseudoCigmaPoInformationDomain cigmaPo1,
                PseudoCigmaPoInformationDomain cigmaPo2) {
                
                // Compare Cigma PO No.
                if( null == cigmaPo1.getPseudoPoNumber() 
                    || null == cigmaPo2.getPseudoPoNumber() ){
                    return ZERO;
                }
                int iPoNo = cigmaPo1.getPseudoPoNumber()
                    .compareTo(cigmaPo2.getPseudoPoNumber());
                if( ZERO != iPoNo ){
                    return iPoNo;
                }
                
                // Compare Part No.
                if( null == cigmaPo1.getPseudoPartNumber() 
                    || null == cigmaPo2.getPseudoPartNumber() ) {
                    return ZERO;
                }
                int iPartNo = cigmaPo1.getPseudoPartNumber()
                    .compareTo(cigmaPo2.getPseudoPartNumber());
                if( ZERO != iPartNo ){
                    return iPartNo;
                }

                // Compare Data Type.
                if( null == cigmaPo1.getPseudoDataType() 
                    || null == cigmaPo2.getPseudoDataType() ) {
                    return ZERO;
                }
                int iDataType = cigmaPo1.getPseudoDataType().compareTo(
                    cigmaPo2.getPseudoDataType());
                if( ZERO != iDataType ){
                    return iDataType;
                }
                
                // Compare Order Type
                if( null == cigmaPo1.getPseudoOrderType() 
                    || null == cigmaPo2.getPseudoOrderType() ) {
                    return ZERO;
                }
                int iOrderType = cigmaPo1.getPseudoOrderType().compareTo(
                    cigmaPo2.getPseudoOrderType());
                return iOrderType;
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
    
    /** Constant Comparator TransferPoDataFromCigmaDomain */
    public static final Comparator<TransferPoDataFromCigmaDomain> COMPARE_CIGMA_PO = 
        new Comparator<TransferPoDataFromCigmaDomain>() {
            public int compare(TransferPoDataFromCigmaDomain cigmaPo1,
                TransferPoDataFromCigmaDomain cigmaPo2) {
                
                // Compare SPS Supplier Code.
                if( null == cigmaPo1.getSCd() 
                    || null == cigmaPo2.getSCd() ){
                    return ZERO;
                }
                int iSupplier = cigmaPo1.getSCd()
                    .compareTo(cigmaPo2.getSCd());
                if( ZERO != iSupplier ){
                    return iSupplier;
                }
                
                // Compare Cigma P/O.
                if( null == cigmaPo1.getCigmaPoNo() || null == cigmaPo2.getCigmaPoNo() ) {
                    return ZERO;
                }
                int iCigmaDo = cigmaPo1.getCigmaPoNo().compareTo(cigmaPo2.getCigmaPoNo());
                if( ZERO != iCigmaDo ){
                    return iCigmaDo;
                }

                // Compare SPS Supplier Plant Code.
                if( null == cigmaPo1.getSPcd() 
                    || null == cigmaPo2.getSPcd() ) {
                    return ZERO;
                }
                int iPlantCode = cigmaPo1.getSPcd().compareTo(
                    cigmaPo2.getSPcd());
                if( ZERO != iPlantCode ){
                    return iPlantCode;
                }
                
                // Compare Data Type.
                if( null == cigmaPo1.getPoType() 
                    || null == cigmaPo2.getPoType() ) {
                    return ZERO;
                }
                int iDatType = cigmaPo1.getPoType().compareTo(
                    cigmaPo2.getPoType());
                return iDatType;
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
        
    /** Constant Comparator TransferPoDataFromCigmaDomain */
    public static final Comparator<TransferPoDetailDataFromCigmaDomain> COMPARE_CIGMA_PO_DETAIL = 
        new Comparator<TransferPoDetailDataFromCigmaDomain>() {
            public int compare(TransferPoDetailDataFromCigmaDomain cigmaPo1,
                TransferPoDetailDataFromCigmaDomain cigmaPo2) {
                // Compare DPn.
                if( null == cigmaPo1.getDPn() || null == cigmaPo2.getDPn() ) {
                    return ZERO;
                }
                int iDpn = cigmaPo1.getDPn().compareTo(cigmaPo2.getDPn());
                if( ZERO != iDpn ){
                    return iDpn;
                }
                // Compare SPn.
                if( null == cigmaPo1.getSPn() || null == cigmaPo2.getSPn() ) {
                    return ZERO;
                }
                int iSpn = cigmaPo1.getSPn().compareTo(cigmaPo2.getSPn());
                return iSpn;
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
        
    /** Constant Comparator TransferPoDueDataFromCigmaDomain */
    public static final Comparator<TransferPoDueDataFromCigmaDomain> COMPARE_CIGMA_PO_DUE = 
        new Comparator<TransferPoDueDataFromCigmaDomain>() {
            public int compare(TransferPoDueDataFromCigmaDomain cigmaPo1,
                TransferPoDueDataFromCigmaDomain cigmaPo2) {
                // Compare DPn.
                if( null == cigmaPo1.getEtd() || null == cigmaPo2.getEtd() ) {
                    return ZERO;
                }
                int iDpn = DateUtil.format(cigmaPo1.getEtd(), DateUtil.PATTERN_YYYYMMDD) 
                    .compareTo(DateUtil.format(cigmaPo2.getEtd(), DateUtil.PATTERN_YYYYMMDD));
                return iDpn;
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
    
    /** Constant Comparator TransferDoDataFromCigmaDomain */
    public static final Comparator<TransferDoDataFromCigmaDomain> COMPARE_CIGMA_DO = 
        new Comparator<TransferDoDataFromCigmaDomain>() {
            public int compare(TransferDoDataFromCigmaDomain cigmaDo1,
                TransferDoDataFromCigmaDomain cigmaDo2) {
                // Compare Cigma D/O.
                if( null == cigmaDo1.getCigmaDoNo() || null == cigmaDo2.getCigmaDoNo() ) {
                    return ZERO;
                }
                int iCigmaDo = cigmaDo1.getCigmaDoNo().compareTo(cigmaDo2.getCigmaDoNo());
                if( ZERO != iCigmaDo ){
                    return iCigmaDo;
                }
                // Compare SPS Supplier Code.
                if( null == cigmaDo1.getSCd() 
                    || null == cigmaDo2.getSCd() ){
                    return ZERO;
                }
                int iSupplier = cigmaDo1.getSCd()
                    .compareTo(cigmaDo2.getSCd());
                if( ZERO != iSupplier ){
                    return iSupplier;
                }
                // Compare SPS Supplier Plant Code.
                if( null == cigmaDo1.getSPcd() 
                    || null == cigmaDo2.getSPcd() ) {
                    return ZERO;
                }
                int iPlantCode = cigmaDo1.getSPcd().compareTo(
                    cigmaDo2.getSPcd());
                if( ZERO != iPlantCode ){
                    return iPlantCode;
                }
                
                // Compare Data Type.
                if( null == cigmaDo1.getDataType() 
                    || null == cigmaDo2.getDataType() ) {
                    return ZERO;
                }
                int iDatType = cigmaDo1.getDataType().compareTo(
                    cigmaDo2.getDataType());
                return iDatType;
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
    
    /** Constant Comparator TransferDoDataFromCigmaDomain */
    public static final Comparator<TransferChangeDoDataFromCigmaDomain> COMPARE_CIGMA_CHANGE_DO = 
        new Comparator<TransferChangeDoDataFromCigmaDomain>() {
            public int compare(TransferChangeDoDataFromCigmaDomain cigmaDo1,
                TransferChangeDoDataFromCigmaDomain cigmaDo2) {
                // Compare Cigma D/O.
                if( null == cigmaDo1.getCurrentDelivery() 
                    || null == cigmaDo2.getCurrentDelivery() ) {
                    return ZERO;
                }
                int iCigmaDo = cigmaDo1.getCurrentDelivery()
                    .compareTo(cigmaDo2.getCurrentDelivery());
                if( ZERO != iCigmaDo ){
                    return iCigmaDo;
                }
                // Compare SPS Supplier Code.
                if( null == cigmaDo1.getVendorCd() 
                    || null == cigmaDo2.getVendorCd() ){
                    return ZERO;
                }
                int iSupplier = cigmaDo1.getVendorCd()
                    .compareTo(cigmaDo2.getVendorCd());
                if( ZERO != iSupplier ){
                    return iSupplier;
                }
                // Compare SPS Supplier Plant Code.
                if( null == cigmaDo1.getSPcd() 
                    || null == cigmaDo2.getSPcd() ) {
                    return ZERO;
                }
                int iPlantCode = cigmaDo1.getSPcd().compareTo(
                    cigmaDo2.getSPcd());
                if( ZERO != iPlantCode ){
                    return iPlantCode;
                }
                
                // Compare Data Type.
                if( null == cigmaDo1.getDataType() 
                    || null == cigmaDo2.getDataType() ) {
                    return ZERO;
                }
                int iDatType = cigmaDo1.getDataType().compareTo(
                    cigmaDo2.getDataType());
                return iDatType;
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
    /** Constant Comparator TransferDoDataFromCigmaDomain */
    public static final Comparator<SpsCigmaChgDoErrorDomain> COMPARE_SPS_CHG_DO_ERROR = 
        new Comparator<SpsCigmaChgDoErrorDomain>() {
            public int compare(SpsCigmaChgDoErrorDomain error1,
                SpsCigmaChgDoErrorDomain error2) {
                // Compare ErrorTypeFlg.
                if( null == error1.getErrorTypeFlg() 
                    || null == error2.getErrorTypeFlg() ) {
                    return ZERO;
                }
                int iErrorFlag = error1.getErrorTypeFlg()
                    .compareTo(error2.getErrorTypeFlg());
                if( ZERO != iErrorFlag ){
                    return iErrorFlag;
                }
                
                if( Constants.STR_ONE.equals(error1.getErrorTypeFlg()) ){
                    // Compare Denso Code.
                    if( null == error1.getDCd() 
                        || null == error2.getDCd() ) {
                        return ZERO;
                    }
                    int iDcd = error1.getDCd()
                        .compareTo(error2.getDCd());
                    return iDcd;
                }else{
                    // Compare Supplier Code.
                    if( null == error1.getSCd() 
                        || null == error2.getSCd() ) {
                        return ZERO;
                    }
                    int iScd = error1.getSCd()
                        .compareTo(error2.getSCd());
                    return iScd;
                }
            } // end compare method.
        }; // end Comparator TransferDoDataFromCigmaDomain
        
        
    /** Constant Comparator SpsCigmaPoErrorDomain */
    public static final Comparator<BackOrderInformationDomain> COMPARE_BACK_ORDER_INFORMATION = 
        new Comparator<BackOrderInformationDomain>() {
            public int compare(BackOrderInformationDomain backOrder1,
                BackOrderInformationDomain backOrder2) {
                // Compare Cigma PO No.
                if( null == backOrder1 
                    || null == backOrder1.getSpsTDoDomain().getSpsDoNo()
                    || null == backOrder2
                    || null == backOrder2.getSpsTDoDomain().getSpsDoNo()){
                    return ZERO;
                }
                int iSpsDoNo = backOrder1.getSpsTDoDomain().getSpsDoNo()
                    .compareTo(backOrder2.getSpsTDoDomain().getSpsDoNo());
                if( ZERO != iSpsDoNo ){
                    return iSpsDoNo;
                }
                
                // Compare Data Type.
                if( null == backOrder1 
                    || null == backOrder1.getSpsTDoDetailDomain().getSPn()
                    || null == backOrder2
                    || null == backOrder2.getSpsTDoDetailDomain().getSPn()){
                    return ZERO;
                }
                int iSPn = backOrder1.getSpsTDoDetailDomain().getSPn()
                    .compareTo(backOrder2.getSpsTDoDetailDomain().getSPn());
                return iSPn;
            }
        };
        
    /** Constant Comparator AsnDomain. */
    public static final Comparator<AsnDomain> COMPARE_ASN_DETAIL_INFORMATION = 
        new Comparator<AsnDomain>() {
            public int compare(AsnDomain asnDomain1, AsnDomain asnDomain2) {
                //Compare ASN No.
                if( null == asnDomain1.getAsnNo() || null == asnDomain2.getAsnNo()){
                    return ZERO;
                }
                
                int result = asnDomain1.getAsnNo().compareTo(asnDomain2.getAsnNo());
                if(ZERO != result){
                    return result;
                }
                result = asnDomain1.getDPn().compareTo(asnDomain2.getDPn());
                return result;
            }
        };
    
    /** Constant Comparator AsnDetailInformationReturnDomain. */
    public static final Comparator<AsnDetailInformationReturnDomain> COMPARE_ASN_DETAIL_INFO =
        new Comparator<AsnDetailInformationReturnDomain>() {
            public int compare(AsnDetailInformationReturnDomain asnDetail1,
                AsnDetailInformationReturnDomain asnDetail2){
                //Compare ASN No. order by descending
                if(null == asnDetail1.getAsnDomain().getAsnNo()
                    || null == asnDetail2.getAsnDomain().getAsnNo()){
                    return ZERO;
                }
                int result = asnDetail1.getAsnDomain().getAsnNo().compareTo(
                    asnDetail2.getAsnDomain().getAsnNo());
                return result * Constants.MINUS_ONE;
            }
        };
    
    /** Constant Comparator AsnReportDomain. */
    public static final Comparator<AsnReportDomain> COMPARE_ASN_REPORT =
        new Comparator<AsnReportDomain>() {
            public int compare(AsnReportDomain asn1, AsnReportDomain asn2) {
                int result = ZERO;
                
                // Compare RCV_LANE
                boolean rcv1Blank = Strings.judgeBlank(asn1.getRcvLane());
                boolean rcv2Blank = Strings.judgeBlank(asn2.getRcvLane());
                if (!rcv1Blank && rcv2Blank) {
                    return Constants.ONE;
                }
                if (rcv1Blank && !rcv2Blank) {
                    return Constants.MINUS_ONE;
                }
                if (!rcv1Blank) {
                    result = asn1.getRcvLane().compareTo(asn2.getRcvLane());
                }
                if (ZERO != result) {
                    return result;
                }
                
                // Compare D_PN
                boolean dpn1Blank = Strings.judgeBlank(asn1.getDPn());
                boolean dpn2Blank = Strings.judgeBlank(asn2.getDPn());
                if (!dpn1Blank && dpn2Blank) {
                    return Constants.ONE;
                }
                if (dpn1Blank && !dpn2Blank) {
                    return Constants.MINUS_ONE;
                }
                if (!dpn1Blank) {
                    result = asn1.getDPn().compareTo(asn2.getDPn());
                }
                if (ZERO != result) {
                    return result;
                }
                
                // Compare S_PN
                boolean spn1Blank = Strings.judgeBlank(asn1.getSPn());
                boolean spn2Blank = Strings.judgeBlank(asn2.getSPn());
                if (!spn1Blank && spn2Blank) {
                    return Constants.ONE;
                }
                if (spn1Blank && !spn2Blank) {
                    return Constants.MINUS_ONE;
                }
                if (!spn1Blank) {
                    result = asn1.getSPn().compareTo(asn2.getSPn());
                }
                if (ZERO != result) {
                    return result;
                }
                // Compare CTRL_NO
                boolean ctrlNo1Blank = Strings.judgeBlank(asn1.getCtrlNo());
                boolean ctrlNo2Blank = Strings.judgeBlank(asn2.getCtrlNo());
                if (!ctrlNo1Blank && ctrlNo2Blank) {
                    return Constants.ONE;
                }
                if (ctrlNo1Blank && !ctrlNo2Blank) {
                    return Constants.MINUS_ONE;
                }
                if (!ctrlNo1Blank) {
                    result = asn1.getCtrlNo().compareTo(asn2.getCtrlNo());
                }
                if (ZERO != result) {
                    return result;
                }
                
                // Compare CTRL_NO
                boolean spsDoNo1Blank = Strings.judgeBlank(asn1.getSpsDoNo());
                boolean spsDoNo2Blank = Strings.judgeBlank(asn2.getSpsDoNo());
                if (!spsDoNo1Blank && spsDoNo2Blank) {
                    return Constants.ONE;
                }
                if (spsDoNo1Blank && !spsDoNo2Blank) {
                    return Constants.MINUS_ONE;
                }
                if (!spsDoNo1Blank) {
                    result = asn1.getSpsDoNo().compareTo(asn2.getSpsDoNo());
                }
                
                return result;
            }
        };
        
    /** Comparator for sort TransferPoErrorEmailDomain. */
    public static final Comparator<TransferPoErrorEmailDomain> COMPARE_TRANSFER_PO_ERROR_EMAIL = 
        new Comparator<TransferPoErrorEmailDomain>() {
            public int compare(TransferPoErrorEmailDomain error1
                , TransferPoErrorEmailDomain error2)
            {
                int result = ZERO;
                
                // Compare S_CD
                if (null == error1.getCigmaPoError().getSCd()
                    || null == error2.getCigmaPoError().getSCd())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaPoError().getSCd().compareTo(
                    error2.getCigmaPoError().getSCd());
                if (ZERO != result) {
                    return result;
                }
                
                // Compare ERROR_TYPE_FLG
                if (null == error1.getCigmaPoError().getErrorTypeFlg()
                    || null == error2.getCigmaPoError().getErrorTypeFlg())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaPoError().getErrorTypeFlg().compareTo(
                    error2.getCigmaPoError().getErrorTypeFlg());
                if (ZERO != result) {
                    return result;
                }
                
                // Compare CIGMA_PO_NO
                if (null == error1.getCigmaPoError().getCigmaPoNo()
                    || null == error2.getCigmaPoError().getCigmaPoNo())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaPoError().getCigmaPoNo().compareTo(
                    error2.getCigmaPoError().getCigmaPoNo());
                if (ZERO != result) {
                    return result;
                }

                // Compare D_PCD
                if (null == error1.getCigmaPoError().getDPcd()
                    || null == error2.getCigmaPoError().getDPcd())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaPoError().getDPcd().compareTo(
                    error2.getCigmaPoError().getDPcd());
                if (ZERO != result) {
                    return result;
                }
                
                // Compare D_PN
                if (null == error1.getCigmaPoError().getDPn()
                    || null == error2.getCigmaPoError().getDPn())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaPoError().getDPn().compareTo(
                    error2.getCigmaPoError().getDPn());
                
                return result;
            }
        };

    /** Comparator for sort TransferPoErrorEmailDomain. */
    public static final Comparator<TransferChgPoErrorEmailDomain>
    COMPARE_TRANSFER_CHG_PO_ERROR_EMAIL =
        new Comparator<TransferChgPoErrorEmailDomain>() {
            public int compare(TransferChgPoErrorEmailDomain error1
                , TransferChgPoErrorEmailDomain error2) 
            {
                int result = ZERO;
                
                // Compare S_CD
                if (null == error1.getCigmaChgPoError().getSCd()
                    || null == error2.getCigmaChgPoError().getSCd())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaChgPoError().getSCd().compareTo(
                    error2.getCigmaChgPoError().getSCd());
                if (ZERO != result) {
                    return result;
                }

                // Compare ERROR_TYPE_FLG
                if (null == error1.getCigmaChgPoError().getErrorTypeFlg()
                    || null == error2.getCigmaChgPoError().getErrorTypeFlg())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaChgPoError().getErrorTypeFlg().compareTo(
                    error2.getCigmaChgPoError().getErrorTypeFlg());
                if (ZERO != result) {
                    return result;
                }
                
                // Compare CIGMA_PO_NO
                if (null == error1.getCigmaChgPoError().getCigmaPoNo()
                    || null == error2.getCigmaChgPoError().getCigmaPoNo())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaChgPoError().getCigmaPoNo().compareTo(
                    error2.getCigmaChgPoError().getCigmaPoNo());
                if (ZERO != result) {
                    return result;
                }
                
                // Compare D_PCD
                if (null == error1.getCigmaChgPoError().getDPcd()
                    || null == error2.getCigmaChgPoError().getDPcd())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaChgPoError().getDPcd().compareTo(
                    error2.getCigmaChgPoError().getDPcd());
                if (ZERO != result) {
                    return result;
                }
                
                // Compare D_PN
                if (null == error1.getCigmaChgPoError().getDPn()
                    || null == error2.getCigmaChgPoError().getDPn())
                {
                    return ZERO;
                }
                
                result = error1.getCigmaChgPoError().getDPn().compareTo(
                    error2.getCigmaChgPoError().getDPn());
                
                return result;
            }
        };
        
    /** Comparator for sort AsnMaintenanceReturnDomain. */
    public static final Comparator<AsnMaintenanceReturnDomain> COMPARE_ASN_MAINTENANCE =
        new Comparator<AsnMaintenanceReturnDomain>() {
            public int compare(AsnMaintenanceReturnDomain asn1, AsnMaintenanceReturnDomain asn2) {
                int result = ZERO;
                
                //Compare Rcv Lane
                if(null == asn1.getAsnDetailDomain().getRcvLane()
                    || null == asn2.getAsnDetailDomain().getRcvLane()){
                    return ZERO;
                }
                
                result = asn1.getAsnDetailDomain().getRcvLane().compareTo(
                    asn2.getAsnDetailDomain().getRcvLane());
                if (ZERO != result) {
                    return result;
                }
                
                //Compare DENSO Part No.
                if(null == asn1.getDPn() || null == asn2.getDPn()){
                    return ZERO;
                }
                
                result = asn1.getDPn().compareTo(asn2.getDPn());
                if (ZERO != result) {
                    return result;
                }
                
                //Compare SPS DO No.
                if(null == asn1.getAsnDetailDomain().getSpsDoNo()
                    || null == asn2.getAsnDetailDomain().getSpsDoNo()){
                    return ZERO;
                }
                
                result = asn1.getAsnDetailDomain().getSpsDoNo().compareTo(
                    asn2.getAsnDetailDomain().getSpsDoNo());
                return result;
            }
        };
        
    /** Comparator for sort AsnInformationDomain. */
    public static final Comparator<AsnInformationDomain>
    COMPARE_CREATE_INVOICE_BY_MANUAL = new Comparator<AsnInformationDomain>() {
            public int compare(AsnInformationDomain asn1, AsnInformationDomain asn2) {
                int result = ZERO;
                
                //Compare Plan Eta
                if(null == asn1.getSpsTAsnDomain().getPlanEta()
                    || null == asn2.getSpsTAsnDomain().getPlanEta()){
                    return ZERO;
                }
                
                result = asn1.getSpsTAsnDomain().getPlanEta().compareTo(
                    asn2.getSpsTAsnDomain().getPlanEta());
                if(ZERO != result){
                    return result;
                }
                
                //Compare ASN Status
                if(null == asn1.getSpsTAsnDomain().getAsnStatus()
                    || null == asn2.getSpsTAsnDomain().getAsnStatus()){
                    return ZERO;
                }
                
                result = asn1.getSpsTAsnDomain().getAsnStatus().compareTo(
                    asn2.getSpsTAsnDomain().getAsnStatus());
                if(ZERO != result){
                    return result * Constants.MINUS_ONE;
                }
                
                //Compare ASN No.
                if(null == asn1.getSpsTAsnDomain().getAsnNo()
                    || null == asn2.getSpsTAsnDomain().getAsnNo()){
                    return ZERO;
                }
                result = asn1.getSpsTAsnDomain().getAsnNo().compareTo(
                    asn2.getSpsTAsnDomain().getAsnNo());
                return result * Constants.MINUS_ONE;
            }
        };
        
    /**
     * 
     * <p>the default constructor.</p>
     *
     */
    public SortUtil() {
    }
        
    /**
     * 
     * <p>Sort Object List</p>
     *
     * @param list Object
     * @param comparator Comparator
     * @return List
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, 
        Constants.SUPPRESS_WARNINGS_UNCHECKED})
    public static List sort(List list, Comparator comparator){
        Object [] alData = new Object[list.size()];
        alData = list.toArray(alData);
        Arrays.sort(alData, comparator);
        list.clear();
        for(Object obj : alData){
            list.add(obj);
        }
        return list;
    }
    
}
