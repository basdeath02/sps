/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 2015/08/05 CSI Akat           [IN009]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.globaldenso.asia.sps.common.constant.Constants;


/**
 * This class is used for enable modification String process easier to manage.
 * @author CSI
 */

public class StringUtil {
    
    /** The dfnumber. */
    private static DecimalFormat dfnumber = new DecimalFormat("#,##0");

    /** The dfcurrency. */
    private static DecimalFormat dfcurrency = new DecimalFormat("#,##0.00");
    
    /**
     * Instantiates a new string util.
     */
    public StringUtil(){
        super();
    }
    
    /**
     * Checks if is numeric.
     * 
     * @param str the str
     * @return true, if is numeric
     */
    public static boolean isNumeric(String str){
        return str.matches("-?\\d+(\\.\\d+)?");
    }
    
    // [IN059] check positive numeric
    /**
     * Checks if is numeric.
     * 
     * @param str the str
     * @return true, if is positive numeric
     */
    public static boolean isPositiveNumeric(String str){
        return str.matches("^\\d+(\\.\\d+)?");
    }

    // [IN059] check negative numeric
    /**
     * Checks if is numeric.
     * 
     * @param str the str
     * @return true, if is negative numeric
     */
    public static boolean isNegativeNumeric(String str){
        return str.matches("-{1}\\d+(\\.\\d+)?");
    }
    
    /**
     * Checks if is positive integer.
     * 
     * @param str the str
     * @return true, if is positive integer
     */
    public static boolean isPositiveInteger(String str){
        return str.matches("^\\d+$");
    }

    
    /**
    * Check null or empty.
    * 
    * @param strParam the str param
    * @return true, if successful
    */
    public static boolean checkNullOrEmpty(Object strParam) {
        if(null == strParam || strParam.toString().length() == Constants.ZERO
            || "null".equals(strParam.toString())){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * <p>Check null or empty.</p>
     *
     * @param tokens array string
     * @return true, if successful
     */
    public static boolean checkStringsNullOrEmpty( Object ... tokens ){
        for(Object token : tokens){
            if(null == token || token.toString().length() == Constants.ZERO
                || "null".equals(token.toString())){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Delete comma.
     * 
     * @param strInput the str input
     * @return the string
     */
    public static String deleteComma(String strInput) {
        if (checkNullOrEmpty(strInput)) {
            return strInput;
        }
        return strInput.replaceAll(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
    }

    /**
     * nullToEmpty performs changing value of objInput from null to empty.
     * 
     * @param objInput the obj input
     * @return the string
     * in case objInput is null object, empty string is return,
     *         otherwise objInput.toString() is return.
     */
    public static String nullToEmpty(Object objInput) {
        if( null == objInput || Constants.NULL_STRING.equals(objInput)){
            return Constants.EMPTY_STRING;
        }else{
            return objInput.toString();
        }
    }

    /**
     * nullToNoneBreakingSpace performs changing value of objInput from null to &nbsp;.
     * 
     * @param objInput the obj input
     * @return the string
     * in case objInput is null object, &nbsp; is return,
     *         otherwise objInput.toString() is return.
     */
    public static String nullToNoneBreakingSpace(Object objInput) {
        if( null == objInput || Constants.NULL_STRING.equals(objInput)){
            return Constants.SYMBOL_NBSP;
        }else{
            return objInput.toString();
        }
    }
    
    /**
     * To string without whitespace.
     * Performs changing value of strParam from null to empty and also trim data.
     * 
     * @param strParam the object
     * @return toStringWithoutWhitespace(" Test ") return ("Test").
     */
    public static String checkNullToEmpty(Object strParam) {
        if(null == strParam || strParam.toString().length() == Constants.ZERO
            || "null".equals(strParam.toString())){
            return Constants.EMPTY_STRING;
        }else{
            return strParam.toString().trim();
        }
    }
    
    /**
     * To string.
     * 
     * @param object the object
     * @param defaultValue the default value
     * @return the string
     */
    public static String toString(Object object, String defaultValue) {
        if (null == object){
            return defaultValue;
        }
        return String.valueOf(object);
    }
    
    /**
     * To number format.
     * Converting plain input String to "#,##0" format.
     * @param strInput the str input
     * @return return formatted String
     * @sample toNumberFormat("1234") return "1,234".
     */
    public static String toNumberFormat(String strInput) {
        if (checkNullOrEmpty(strInput)) {
            return Constants.EMPTY_STRING;
        }
        // [IN009] Rounding Mode use HALF_UP
        //return dfnumber.format(Double.parseDouble(strInput));
        BigDecimal temp = new BigDecimal(strInput);
        temp = temp.setScale(Constants.ZERO, RoundingMode.HALF_UP);
        return dfnumber.format(temp);
    }
    
    /**
     * Converting input String to "#,##0.00" format.
     * @param strInput - plain String
     * @return Formatted string in "#,##0.00" format.
     * @sample toCurrencyFormat("1234") return "1,234.00"
     */
    public static String toCurrencyFormat(String strInput) {
        if (checkNullOrEmpty(strInput)) {
            return Constants.EMPTY_STRING;
        }
        return dfcurrency.format(Double.parseDouble(strInput));
    }
    
    /**
     * Escape character.
     * Replace escape character with '[' and ']' at first and last of string.
     * @param strInput the str input
     * @return the string
     */
    public static String escapeCharacter(String strInput) {
        String result = strInput.replaceAll("%", "[%]").replaceAll("'", "''");
        return result;
    }
    
    /**
     * Append String separate by , .<br />
     * Example List of "x", "y" to String "x,y"
     * @param valueList list of value to append
     * @return String already append
     * */
    public static String convertListToStringCommaSeperate(List<String> valueList){
        StringBuffer result = new StringBuffer(Constants.EMPTY_STRING);
        for (String value : valueList) {
            result.append(value).append(Constants.SYMBOL_COMMA);
        }
        if(Constants.ZERO < result.length()){
            result.setLength(result.length() - Constants.ONE);
        }
        return result.toString();
    }
    
    /**
     * Append String surround with ' and separate by , .<br />
     * Example List of "x", "y" to String "'x','y'"
     * @param valueList list of value to append
     * @return String already append
     * */
    public static String convertListToVarcharCommaSeperate(List<String> valueList) {
        StringBuffer result = new StringBuffer();
        for (String value : valueList) {
            result.append(Constants.SYMBOL_SINGLE_QUOTE).append(value)
                .append(Constants.SYMBOL_SINGLE_QUOTE).append(Constants.SYMBOL_COMMA);
        }
        if(Constants.ZERO < result.length()){
            result.setLength(result.length() - Constants.ONE);
        }
        
        return result.toString();
    }
    
    /**
     * Append String surround with ' and separate by , .<br />
     * Example List of "x", "y" to String "'x','y'"
     * @param valueList list of value to append
     * @return String already append
     * */
    public static String convertListToVarcharCommaSeperate(Set<String> valueList) {
        StringBuffer result = new StringBuffer();
        for (String value : valueList) {
            result.append(Constants.SYMBOL_SINGLE_QUOTE).append(value)
                .append(Constants.SYMBOL_SINGLE_QUOTE).append(Constants.SYMBOL_COMMA);
        }
        if(Constants.ZERO < result.length()){
            result.setLength(result.length() - Constants.ONE);
        }
        
        return result.toString();
    }
    
    /**
     * Append String surround with ' and separate by , .<br />
     * Example List of "x", "y" to String "'x','y'"
     * @param valueList list of value to append
     * @return String already append
     * */
    public static String convertListToVarcharCommaSeperate(String...valueList) {
        StringBuffer result = new StringBuffer();
        for (String value : valueList) {
            result.append(Constants.SYMBOL_SINGLE_QUOTE).append(value)
                .append(Constants.SYMBOL_SINGLE_QUOTE).append(Constants.SYMBOL_COMMA);
        }
        if(Constants.ZERO < result.length()){
            result.setLength(result.length() - Constants.ONE);
        }
        
        return result.toString();
    }
    
    /**
     * Append String surround with ' and separate by , .<br />
     * Example List of "x", "y" to String "'x','y'"
     * @param valueList list of value to append
     * @param length length
     * @return String already append
     * */
    public static String convertListToCharCommaSeperate(List<String> valueList, int length) {
        StringBuffer sbFormat = new StringBuffer();
        sbFormat.append("%-");
        sbFormat.append(length);
        sbFormat.append(Constants.STR_S);
        StringBuffer result = new StringBuffer();
        for (String value : valueList) {
            result.append(Constants.SYMBOL_SINGLE_QUOTE)
                .append( String.format(sbFormat.toString(), value.toString()) )
                .append(Constants.SYMBOL_SINGLE_QUOTE).append(Constants.SYMBOL_COMMA);
        }
        if(Constants.ZERO < result.length()){
            result.setLength(result.length() - Constants.ONE);
        }
        
        return result.toString();
    }
    /**
     * 
     * <p>.</p>
     *
     * @param value String
     * @param length length Right
     * @return String
     */
    public static String rightPad(String value, int length){
        StringBuffer sbFormat = new StringBuffer();
        sbFormat.append("%-");
        sbFormat.append(length);
        sbFormat.append(Constants.STR_S);
        return String.format( sbFormat.toString() , value.trim());
    }
    
    
    /**
     * Checking number format.
     * 
     * @param strNumber the string
     * @param precision the total number of digits
     * @param scale the number of digits to the right of the decimal point
     * @return true, if number format is correct
     */
    public static boolean isNumberFormat(String strNumber, int precision, int scale){
        
        try {
            int indexOfDot = strNumber.indexOf(Constants.SYMBOL_DOT);
            int numBeforeDot = strNumber.substring(Constants.ZERO, indexOfDot).length();
            int numAfterDot = strNumber.substring(indexOfDot + 1, strNumber.length()).length();
            if((precision - scale) < numBeforeDot || scale < numAfterDot){
                return false;
            }else{
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Checking number format.
     * 
     * @param line the string
     * @param max the integer
     * @return list of split CSV data
     */
    public static List<String> splitCsvData(String line, int max){
        List<String> resultList = new ArrayList<String>();
        String[] resultArrList = line.split(Constants.SYMBOL_COMMA);
        
        for(int i = Constants.ZERO; i < max; i++){
            if (resultArrList.length <= i) {
                resultList.add(Constants.EMPTY_STRING);
                continue;
            }
            
            if(null == resultArrList[i] || resultArrList[i].length() == Constants.ZERO
                || Constants.STR_NULL.equals(resultArrList[i]))
            {
                resultList.add(Constants.EMPTY_STRING);
            }else{
                resultList.add(resultArrList[i].trim());
            }
        }
        return resultList;
    }
    
    /**
     * 
     * <p>Appens String method.</p>
     *
     * @param tokens array string
     * @return String value
     */
    public static String appendsString( String ... tokens ){
        StringBuffer sbResult = new StringBuffer();
        for(String token : tokens){
            sbResult.append( token );
        }
        return sbResult.toString();
    }
    
    /**
     * 
     * <p>Appens String method.</p>
     *
     * @param tokens array string
     * @return String value
     */
    public static String appendsStringDefaultEmptyString( String ... tokens ){
        StringBuffer sbResult = new StringBuffer();
        for(String token : tokens){
            if(checkNullOrEmpty(token)){
                sbResult.append( Constants.EMPTY_STRING );
            }else{
                sbResult.append( token );
            }
        }
        return sbResult.toString();
    }
    
    /**
     * 
     * <p>Set BigDecimal Scale To String.</p>
     *
     * @param value the BigDecimal
     * @param scale the integer
     * @return String value
     */
    public static String setBigDecimalScaleToString(BigDecimal value , int scale){
        
        String result = null;
        result = value.setScale(scale, BigDecimal.ROUND_DOWN).toString();
        return result;
    }
    
    /**
     * 
     * <p>Add space.</p>
     *
     * @param value the String
     * @param max the integer
     * @return String value
     */
    public static String addSpace(String value , int max){
        
        for(int i = value.length(); i < max; i++){
            value += Constants.SYMBOL_SPACE;
        }
        return value;
    }
    
    /**
     * 
     * <p>Add space.</p>
     *
     * @param value the String
     * @return String value
     */
    public static String removeComma(String value){
        
        if(null == value){
            return Constants.EMPTY_STRING;
        }else{
            return value.replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING);
        }
    }
}