/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * Utility class for message.
 * @author CSI
 */
public class MessageUtil {

    /** The log. */
    private static final Log LOG = LogFactory.getLog(MessageUtil.class);

    /**
     * Instantiates a new message util.
     */
    public MessageUtil(){
        super();
    }
    
    /**
     * getMessage performs getting message based on a specified file path and key.
     * 
     * @param properties - properties file path. Ex. com.globaldenso.asia.sps.conf
     * @param key - Key of message in .properties file.
     * @return Value of a specified key.
     * @throws Exception the exception
     */
    public static String getMessage(String properties, String key) throws Exception {
        String message = null;
        message = getMessage(properties, null, key, null);
        return message;
    }

    /**
     * getMessage performs getting message based on a specified input.
     * 
     * @param properties - properties file path. Ex. com.globaldenso.asia.sps.conf
     * @param locale - Locale object that used for getting message.
     * @param key - Key of message in .properties file.
     * @return Value of a specified key.
     * @throws Exception the exception
     */
    public static String getMessage(String properties, Locale locale, String key) throws Exception {
        String message = null;
        message = getMessage(properties, locale, key, null);
        return message;
    }

    /**
     * getMessageResources performs getting message from a specified properties file.
     * It gets message based on the specified key and replace each parameter in message 
     * by using value from params.
     * 
     * @param properties Properties file name. Ex. com.globaldenso.asia.sps.conf
     * @param locale Locale
     * @param key Key of message in properties file.
     * @param params the params
     * @return Complete message.
     * @throws Exception : Any error, exception will be thrown.
     * @sample In msg_*_*.properties we have key and value as below;
     * ERRCOM001 = Item {0} : Mandatory Error. Please fill in the field.
     * 
     * For getting complete message we use below command.
     * String[] params = new String[]{"user name"};
     * String message = MessageUtil.getMessageResource("ERRCOM001",params);
     * 
     * Return result will be
     * "Item user name : Mandatory Error. Please fill in the field."
     */
    public static String getMessage(String properties, Locale locale, String key, String[] params) 
        throws Exception {
        String message = Constants.EMPTY_STRING;
        StringBuffer messageBuilder = new StringBuffer();
        try {
            ResourceBundle resource;
            if (null == locale) {
                // Message: properties file ['properties']
                messageBuilder.append(Constants.WORD_MESSAGE).append(Constants.SYMBOL_COLON)
                    .append(Constants.SYMBOL_SPACE).append(Constants.WORDS_PROPERTY_FILE)
                    .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                    .append(properties).append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET);
                LOG.debug(messageBuilder.toString());
                resource = PropertyResourceBundle.getBundle(properties);
            } else {
                // Message: properties file ['properties'] Locale ['country'/'language']
                messageBuilder.append(Constants.WORD_MESSAGE).append(Constants.SYMBOL_COLON)
                    .append(Constants.SYMBOL_SPACE).append(Constants.WORDS_PROPERTY_FILE)
                    .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                    .append(properties).append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET)
                    .append(Constants.SYMBOL_SPACE).append(Constants.WORD_LOCALE)
                    .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                    .append(locale.getCountry()).append(Constants.SYMBOL_SLASH)
                    .append(locale.getLanguage()).append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET);
                LOG.debug(messageBuilder.toString());
                resource = PropertyResourceBundle.getBundle(properties, locale);
            }
            message = resource.getString(key);
            messageBuilder.setLength(Constants.ZERO);
            // Key: ['key'] Message: ['message']
            messageBuilder.append(Constants.WORD_KEY).append(Constants.SYMBOL_COLON)
                .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                .append(key).append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET)
                .append(Constants.WORD_MESSAGE).append(Constants.SYMBOL_COLON)
                .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                .append(message).append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET);
            LOG.debug(messageBuilder.toString());
        } catch (Exception e) {
            throw e;
        }
        /** Replace parameter. */
        if (!StringUtil.checkNullOrEmpty(params) && Constants.ZERO < params.length) {
            int lengthParam = params.length;
            for (int index = Constants.ZERO; index < lengthParam; index++) {
                messageBuilder.setLength(Constants.ZERO);
                if(null != params[index]){
                    messageBuilder.append(Constants.SYMBOL_OPEN_BRACES).append(index)
                        .append(Constants.SYMBOL_CLOSE_BRACES);
                    message = message.replace(messageBuilder.toString(), params[index]);
                }else{
                    /** When found null value, exist loop. */
                    break;
                }
            }
        }
        messageBuilder.setLength(Constants.ZERO);
        messageBuilder.append(Constants.WORDS_COMPLETE_MESSAGE).append(Constants.SYMBOL_COLON)
            .append(Constants.SYMBOL_SPACE).append(message);
        LOG.debug(messageBuilder.toString());
        return message;
    }

    /**
     * getLabel is used for getting message from the key that starting with
     * "label." + argument key in a specified properties file name.
     * 
     * @param locale the locale
     * @param key a String contain key name in the specified properties file
     * name.
     * @return String: message that corresponding to the specified key.
     * @throws Exception the exception
     */
    public static String getLabel(Locale locale, String key) throws Exception {
        String message = null;
        message = getMessage(ContextParams.getBaseDirGui(), locale, 
            Constants.MESSAGE_UTIL_PREFIX_LABEL + key);
        return message;
    }
    
    /**
     * getMasterColumnLabel is used for getting message from the key that starting with
     * "mst.column." + argument key in a specified properties file name.
     * 
     * @param locale the locale
     * @param key a String contain key name in the specified properties file
     * name.
     * @return String: message that corresponding to the specified key.
     * @throws Exception the exception
     */
    public static String getMasterColumnLabel(Locale locale, String key) throws Exception {
        String message = null;
        StringBuffer messageBuilder = new StringBuffer();
        messageBuilder.append(Constants.MESSAGE_UTIL_PREFIX_MASTER_COLUMN).append(key);
        message = getMessage(ContextParams.getBaseDirGui(), locale, messageBuilder.toString());
        return message;
    }
    
    
    /**
     * Gets the report label.
     * 
     * @param locale the locale
     * @param key the key
     * @return the report label
     */
    public static String getReportLabel(Locale locale, String key) {
        String message = null;
        StringBuffer messageBuilder = new StringBuffer();
        messageBuilder.append(Constants.MESSAGE_UTIL_PREFIX_LABEL).append(key);
        try {
            message = getMessage(ContextParams.getBaseDirReportGui(), locale,
                messageBuilder.toString());
        } catch (Exception e) {
            message = key;
        }
        return message;
    }
    
    /**
     * Gets the email label.
     * 
     * @param locale the locale
     * @param key the key
     * @return the email label
     * @throws Exception the exception
     */
    public static String getEmailLabel(Locale locale, String key) throws Exception {
        String message = null;
        message = getMessage(ContextParams.getBaseEmail(), locale, key);
        return message;
    }
    
    /**
     * Gets the email label.
     * 
     * @param locale the locale
     * @param key the key
     * @param params the params
     * @return the email label
     * @throws Exception the exception
     */
    public static String getEmailLabel(Locale locale, String key, String[] params) 
        throws Exception
    {
        String message = null;
        message = getMessage(ContextParams.getBaseEmail(), locale, key, params);
        return message;
    }
    
    /**
     * getLabelFormat performs getting message based on the specified key and
     * locale.
     * 
     * @param key key in properties file to be obtained message.
     * @param locale Locale
     * @return String: message in properties file that corresponding to the
     * specified arguments.
     * @throws Exception the exception
     */
    public static String getFormat(String key, Locale locale) throws Exception {
        String message = null;
        StringBuffer messageBuilder = new StringBuffer();
        messageBuilder.append(Constants.MESSAGE_UTIL_PREFIS_LABEL_FORMAT).append(key);
        message = getMessage(ContextParams.getBaseDirGui(), locale, messageBuilder.toString());
        return message;
    }

    /**
     * getMessageResource perform get message from "MessageResource" file.
     * 
     * @param locale the locale
     * @param key : Key in properties file that used for getting message.
     * @param params : String[] contains parameter in message.
     * @return value of a specified key.
     * @throws Exception the exception
     */
    public static String getApplicationMessage(Locale locale, String key, String[] params) 
        throws Exception
    {
        String message = null;
        message = getMessage(ContextParams.getBaseDirMsg(), locale, key, params);
        return message;
    }

    /**
     * getMessageResource perform get message from "MessageResource" file.
     * 
     * @param locale Locale
     * @param key Key in properties file that used for getting message.
     * @return Complete string.
     * @throws Exception the exception
     */
    public static String getApplicationMessage(Locale locale, String key) throws Exception {
        String message = null;
        message = getApplicationMessage(locale, key, null);
        return message;
    }
    
    /**
     * getMessageResource perform get message from "MessageResource" file.
     * 
     * @param locale the locale
     * @param key Key in properties file that used for getting message.
     * @param params String[] contains parameter in message.
     * @return value of a specified key.
     * @throws Exception the exception
     */
    public static String getApplicationMessage(String locale, String key, String[] params) 
        throws Exception
    {
        String message = null;
        Locale objLocale = null;
        if(null != locale){
            String[] locales = locale.split(Constants.SYMBOL_UNDER_SCORE);
            objLocale = new Locale(locales[0], locales[1]);
        }else{
            objLocale = LocaleUtility.langToLocale(ContextParams.getDefaultLocale());
        }
        message = getMessage(ContextParams.getBaseDirMsg(), objLocale , key, params);
        return message;
    }
    
    /**
     * Get label by label key and send as parameter to get message.
     * 
     * @param locale the locale
     * @param messageKey the key for get message
     * @param labelKey key for get label
     * @throws ApplicationException the application exception
     */
    public static void throwsApplicationMessageWithLabel(Locale locale, String messageKey,
        String labelKey) throws ApplicationException
    {
        String label = null;
        String message = null;
        label = getLabelHandledException(locale, labelKey);
        message = getApplicationMessageHandledException(locale, messageKey,
            new String[] {label});
        throw new ApplicationException(message);
    }
    
    /**
     * Throwing Application Exception. 
     * If message is not find, this method will throw default message.
     * 
     * @param locale the locale
     * @param messageKey the key for get message
     * @throws ApplicationException the application exception
     */
    public static void throwsApplicationMessageHandledException(Locale locale, String messageKey) 
        throws ApplicationException
    {
        String message = null;
        message = getApplicationMessageHandledException(locale, messageKey);
        throw new ApplicationException(message);
    }
    
    /**
     * Throwing Application Exception. 
     * If message is not find, this method will throw default message.
     * 
     * @param locale the locale
     * @param messageKey the key for get message
     * @param params String[] contains parameter in message.
     * @throws ApplicationException the application exception
     */
    public static void throwsApplicationMessageHandledException(Locale locale, String messageKey, 
        String[] params) throws ApplicationException
    {
        String message = null;
        message = getApplicationMessageHandledException(locale, messageKey, params);
        throw new ApplicationException(message);
    }
    
    /**
     * throwsApplicationMessage handle message.
     * 
     * @param locale the locale
     * @param primaryMessage the primary message
     * @param secondaryMessage the secondary message use when system can't get the primary message.
     * @throws ApplicationException the application exception
     */
    public static void throwsApplicationMessage(Locale locale, String primaryMessage,
        String secondaryMessage) throws ApplicationException
    {
        throwsApplicationMessage(locale, primaryMessage, secondaryMessage, null);
    }
    
    /**
     * throwsApplicationMessage handle message.
     * 
     * @param locale the locale
     * @param primaryMessage the primary message
     * @param secondaryMessage the secondary message use when system can't get the primary message.
     * @param params String[] contains parameter in message.
     * @throws ApplicationException 
     * @throws ApplicationException the application exception
     */
    public static void throwsApplicationMessage(Locale locale, String primaryMessage,
        String secondaryMessage, String[] params) throws ApplicationException 
    {
        String message = null;
        try{
            message = getApplicationMessage(locale, primaryMessage, params);
        }
        catch(Exception ex){
            if (null == secondaryMessage) {
                StringBuffer messageBuffer = new StringBuffer();
                messageBuffer.append(Constants.MESSAGE_UTIL_CANNOT_GET_PROPERTY)
                    .append(primaryMessage);
                message = messageBuffer.toString();
            } else {
                message = secondaryMessage;
            }
        }
        throw new ApplicationException(message);
    }
    
    /**
     * throwsApplicationMessage handle message.
     * 
     * @param locale the locale
     * @param primaryMessage the primary message
     * @param secondaryMessage the secondary message use when system can't get the primary message.
     * @return string
     */
    public static String getErrorMessage(Locale locale, String primaryMessage,
        String secondaryMessage){
        
        String message = null;
        try{
            message = getApplicationMessage(locale, primaryMessage);
        }
        catch(Exception ex){
            message = secondaryMessage;
        }
        return message;
    }
    
    /**
     * throwsApplicationMessage handle message.
     * 
     * @param locale the locale
     * @param primaryMessage the primary message
     * @param secondaryMessage the secondary message use when system can't get the primary message.
     * @param params String[] contains parameter in message.
     * @return string
     */
    public static String getErrorMessage(Locale locale, String primaryMessage,
        String secondaryMessage, String[] params)  {
        
        String message = null;
        try{
            message = getApplicationMessage(locale, primaryMessage, params);
        }
        catch(Exception ex){
            message = secondaryMessage;
        }
        return message;
    }

    /**
     * 
     * <p>Write Log method.</p>
     *
     * @param log the log
     * @param logLevel - 0 is info, 1 is warning, 2 is error
     * @param message the message
     */
    public static void writeLog(Log log, int logLevel, String message){
        if (Constants.ZERO == logLevel) {
            log.info(message);
        } else if (Constants.ONE == logLevel) {
            log.warn(message);
        } else if (Constants.TWO == logLevel) {
            log.error(message);
        }
    }
    
    /** 
     * Get error message for batch processing.
     *  
     * @param locale the locale
     * @param keyString - Key of message in .properties file.
     * @param params the string[] contains parameter in message.
     * @param log the log
     * @param logLevel - 0 is info, 1 is warning, 2 is error
     * @param throwsFlag the boolean
     * @throws ApplicationException ApplicationException
     */
    public static void getErrorMessageForBatch(Locale locale, String keyString,
        String[] params, Log log, int logLevel, boolean throwsFlag) throws ApplicationException
    {
        String message = null;
        try{
            if (null == params) {
                message = getMessage(ContextParams.getBaseDirMsg(), locale, keyString);
            } else {
                message = getMessage(ContextParams.getBaseDirMsg(), locale, keyString, params);
            }
        }catch(Exception e){
            StringBuffer messageBuffer = new StringBuffer();
            messageBuffer.append(Constants.MESSAGE_UTIL_CANNOT_GET_PROPERTY)
                .append(keyString);
            message = messageBuffer.toString();
        }
        
        writeLog(log, logLevel, message);
        
        if(throwsFlag){
            throw new ApplicationException(message);
        }
    }

    /**
    * Call getLabelHandledException(Locale, String) with parameter (locale, labelKey) and then call
    * getErrorMessageForBatch(locale, keyString, new String[]{label}, log, throwsFlag).
    * 
    * @param locale the locale
    * @param keyString - Key of message in .properties file.
    * @param labelKey the label key of message in properties file.
    * @param params the params
    * @param log the log
    * @param logLevel - 0 is info, 1 is warning, 2 is error
    * @param throwsFlag the boolean
    * @throws ApplicationException ApplicationException
    */
    public static void getErrorMessageForBatchWithLabel(Locale locale, String keyString,
        String labelKey, String[] params, Log log, int logLevel, boolean throwsFlag)
        throws ApplicationException
    {
        String label = null;
        if (null != params) {
            label = getLabelHandledException(locale, labelKey, params);
        } else {
            label = getLabelHandledException(locale, labelKey);
        }
        MessageUtil.getErrorMessageForBatch(locale, keyString, new String[] {label}, log, logLevel,
            throwsFlag);
    }
    
    /**
     * Call getApplicationMessageHandledException(Locale, String, String[]) with parameter
     * (locale, key, null)
     * 
     * @param locale the locale
     * @param key : Key in properties file that used for getting message.
     * @return value of a specified key.
     */
    public static String getApplicationMessageHandledException(Locale locale, String key) {
        String message = null;
        message = getApplicationMessageHandledException(locale, key, null);
        return message;
    }
    
    /**
     * Call getMessageHandledException(String, Locale, String, String[]) with parameter
     * (ContextParams.getBaseDirMsg(), locale, key, params)
     * 
     * @param locale the locale
     * @param key : Key in properties file that used for getting message.
     * @param params : String[] contains parameter in message.
     * @return value of a specified key.
     */
    public static String getApplicationMessageHandledException(Locale locale, String key,
        String[] params)
    {
        String message = null;
        message = getMessageHandledException(ContextParams.getBaseDirMsg(), locale, key, params);
        return message;
    }
    
    /**
     * Call getLabelHandledException(Locale, String) with parameter (locale, labelKey) and then call
     * getApplicationMessageHandledException(locale, messageKey new String[]{label}).
     * 
     * @param locale the locale
     * @param messageKey the key for get message
     * @param labelKey key for get label
     * @return value of a specified key.
     */
    public static String getApplicationMessageWithLabel(Locale locale, String messageKey,
        String labelKey)
    {
        String label = null;
        String message = null;
        label = getLabelHandledException(locale, labelKey);
        message = getApplicationMessageHandledException(locale, messageKey,
            new String[] {label});
        return message;
    }
    
    /**
     * Call getMessageHandledException(String, Locale, String, String[]) with parameter
     * (ContextParams.getBaseDirGui(), locale, Constants.MESSAGE_UTIL_PREFIX_LABEL + key, null)
     * @param locale Locale
     * @param key Key of message in properties file.
     * @return Complete message.
     * */
    public static String getLabelHandledException(Locale locale, String key) {
        String message = null;
        StringBuffer messageBuffer = new StringBuffer();
        messageBuffer.append(Constants.MESSAGE_UTIL_PREFIX_LABEL)
            .append(key);
        message = getMessageHandledException(
            ContextParams.getBaseDirGui(), locale, messageBuffer.toString(), null);
        return message;
    }
    
    /**
     * Call getMessageHandledException(String, Locale, String, String[]) with parameter
     * (ContextParams.getBaseDirGui(), locale, Constants.MESSAGE_UTIL_PREFIX_LABEL + key, null)
     * @param locale Locale
     * @param key Key of message in properties file.
     * @param params the params
     * @return Complete message.
     * */
    public static String getLabelHandledException(Locale locale, String key, String[] params) {
        String message = null;
        StringBuffer messageBuffer = new StringBuffer();
        messageBuffer.append(Constants.MESSAGE_UTIL_PREFIX_LABEL)
            .append(key);
        message = getMessageHandledException(
            ContextParams.getBaseDirGui(), locale, messageBuffer.toString(), params);
        return message;
    }
    
    /**
     * Call getMessage(String, Locale, String, String[]) and handle exception.
     * @param properties Properties file name. Ex. com.globaldenso.asia.sps.conf
     * @param locale Locale
     * @param key Key of message in properties file.
     * @param params the params
     * @return Complete message.
     * */
    public static String getMessageHandledException(String properties, Locale locale,
        String key, String[] params)
    {
        String message = null;
    
        try {
            message = getMessage(properties, locale, key, params);
        } catch (Exception exception) {
            message = Constants.MESSAGE_UTIL_CANNOT_GET_PROPERTY + key;
        }
    
        return message;
    }

    /**
     * Call getMessage(String, Locale, String, String[]) with parameter
     * (ContextParams.getBaseEmail(), locale, key, params) and handle exception.* 
     * @param locale the locale
     * @param key the key
     * @param params the params
     * @return the email label
     */
    public static String getEmailLabelHandledException(Locale locale, String key, String[] params) {
        String message = null;
        try {
            message = getMessage(ContextParams.getBaseEmail(), locale, key, params);
        } catch (Exception exception) {
            message = Constants.MESSAGE_UTIL_CANNOT_GET_PROPERTY + key;
        }
        
        return message;
    }

}