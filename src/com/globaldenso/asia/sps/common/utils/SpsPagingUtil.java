/*
 * ModifyDate Development company     Describe 
 * 2014/07/22 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import com.globaldenso.asia.sps.business.domain.BaseDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.presentation.form.CoreActionForm;

/**
 * The class SPSPagingUtil. Paging utility implement by SPS project.
 * @author CSI
 * */
public class SpsPagingUtil {

    /** The default constructor. */
    public SpsPagingUtil() {
    }
    
    /**
     * Calculate first row number and last row number.
     * [This method can call from Business Layer.]
     * @param domain : domain to calculate
     * @param max : record count
     * */
    public static void calcPaging(BaseDomain domain, int max) {
        domain.setTotalCount(max);
        
        double totalPage = Math.ceil(max / (double)domain.getMaxRowPerPage());
        if(totalPage < domain.getPageNumber()){
            domain.setPageNumber((int)totalPage);
        }
        
        // Row number to = (pageNumber * rowPerPage)
        domain.setRowNumTo(domain.getPageNumber() * domain.getMaxRowPerPage());
        
        // Row number from = (pageNumber * rowPerPage) - rowPerPage + 1
        // Row number from = rowNumberTo - rowPerPage + 1
        domain.setRowNumFrom(domain.getRowNumTo() - domain.getMaxRowPerPage() + Constants.ONE);
        
        if (max < domain.getRowNumTo()) {
            domain.setRowNumTo(max);
        }
    }

    /**
     * Calculate first row number and last row number.
     * [This method call from Presentation Layer only.]
     * @param domain : domain that already calcPaging
     * @param form : core action form
     * */
    public static void setFormForPaging(BaseDomain domain, CoreActionForm form) {
        if (Constants.PAGE_SEARCH.equals(form.getFirstPages())) {
            form.setCount(domain.getMaxRowPerPage());
            form.setFrom(domain.getRowNumFrom());
            form.setTo(domain.getRowNumTo());
            form.setMax(domain.getTotalCount());
            form.setPageNo(Constants.ONE);
        }
        if (Constants.PAGE_CLICK.equals(form.getPages())) {
            form.setCount(domain.getMaxRowPerPage());
            form.setMax(domain.getTotalCount());
            form.setPageNo(domain.getPageNumber());
            form.setFrom(domain.getRowNumFrom());
            form.setTo(domain.getRowNumTo());
            form.setPages(Constants.EMPTY_STRING);
        }
    }
}
