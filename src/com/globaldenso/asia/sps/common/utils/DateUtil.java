/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.sun.org.apache.xml.internal.utils.LocaleUtility;

/**
 * DateUtil.class is used as date time converter utility class
 * @author CSI
 */
public class DateUtil {

    /** Pattern 'HH:mm'. */
    public static final int PATTERN_HHMM_COLON = 0;

    /** Pattern 'HHmmss'. */
    public static final int PATTERN_HHMMSS = 1;
    
    /** Pattern 'yyMMdd'. */
    public static final int PATTERN_YYMMDD = 2;
    
    /** Pattern 'dd/MM/yy'. */
    public static final int PATTERN_DDMMYY_SLASH = 3;

    /** Pattern 'M/yy'. */
    public static final int PATTERN_MYY_SLASH = 4;
    
    /** Pattern 'yyyy/MM/dd'. */
    public static final int PATTERN_YYYYMMDD_SLASH = 5;

    /** Pattern 'yyyy-MM-dd'. */
    public static final int PATTERN_YYYYMMDD_DASH = 6;
    
    /** Pattern 'yyyyMMdd'. */
    public static final int PATTERN_YYYYMMDD = 7;

    /** Pattern 'yyyy/MM/dd HH:mm'. */
    public static final int PATTERN_YYYYMMDD_HHMM_SLASH = 8;

    /** Pattern 'yyyyMMddHHmmss'. */
    public static final int PATTERN_YYYYMMDD_HHMMSS = 9;
    
    /** Pattern 'yyyy-MM-dd HH:mm:ss.SSS'. */
    public static final int PATTERN_YYYYMMDD_HHMMSSSSS_DASH = 10;
    
    /** Pattern 'HHmm'. */
    public static final int PATTERN_HHMM = 11;
    
    /** Pattern 'd/MM/yy'. */
    public static final int PATTERN_DMMYY_SLASH = 12;
    
    /** Pattern 'yyyy/MM'. */
    public static final int PATTERN_YYYYMM_SLASH = 13;
    
    /** Pattern 'yyyyMMddHHmm'. */
    public static final int PATTERN_YYYYMMDD_HHMM = 14;

    /** String pattern 'HH:mm'. */
    private static final String STR_PATTERN_HHMM_COLON = "HH:mm";
    
    /** String pattern 'HHmm'. */
    private static final String STR_PATTERN_HHMM = "HHmm";

    /** String pattern 'HHmmss'. */
    private static final String STR_PATTERN_HHMMSS = "HHmmss";
    
    /** String pattern 'yyMMdd'. */
    private static final String STR_PATTERN_YYMMDD = "yyMMdd";
    
    /** String pattern 'dd/MM/yy'. */
    private static final String STR_PATTERN_DDMMYY_SLASH = "dd/MM/yy";

    /** String pattern 'M/yy'. */
    private static final String STR_PATTERN_MYY_SLASH = "M/yy";
    
    /** String pattern 'yyyy/MM/dd'. */
    private static final String STR_PATTERN_YYYYMMDD_SLASH = "yyyy/MM/dd";

    /** String pattern 'yyyy-MM-dd'. */
    private static final String STR_PATTERN_YYYYMMDD_DASH = "yyyy-MM-dd";
    
    /** String pattern 'yyyyMMdd'. */
    private static final String STR_PATTERN_YYYYMMDD = "yyyyMMdd";

    /** String pattern 'yyyy/MM/dd HH:mm'. */
    private static final String STR_PATTERN_YYYYMMDD_HHMM_SLASH = "yyyy/MM/dd HH:mm";

    /** String pattern 'yyyyMMddHHmmss'. */
    private static final String STR_PATTERN_YYYYMMDD_HHMMSS = "yyyyMMddHHmmss";
    
    /** String pattern 'yyyy-MM-dd HH:mm:ss.SSS'. */
    private static final String STR_PATTERN_YYYYMMDD_HHMMSSSSS_DASH = "yyyy-MM-dd HH:mm:ss.SSS";
    
    /** String pattern 'd/MM/yy'. */
    private static final String STR_PATTERN_DMMYY_SLASH = "d/MM/yy";
    
    /** String pattern 'yyyy/MM'. */
    private static final String STR_PATTERN_YYYYMM_SLASH = "yyyy/MM";
    
    /** String pattern 'yyyyMMddHHmm'. */
    private static final String STR_PATTERN_YYYYMMDD_HHMM = "yyyyMMddHHmm";
    
    /** The log. */
    private static final Log LOG = LogFactory.getLog(DateUtil.class);

    /**
     * Instantiates a new date util.
     */
    public DateUtil(){
        super();
    }
    
    /**
     * Gets the date.
     * 
     * @param locale the locale
     * @return the date
     */
    public static Date getDate(Locale locale) {
        Calendar c = Calendar.getInstance(locale);
        return c.getTime();
    }
    
    /**
     * Gets the date time.
     * 
     * @return the date time
     */
    public static long getDateTime() {
        long dateTime = getDate(Locale.US).getTime();
        return dateTime;
    }

    /**
     * Get default locale from ContextParams.getDefaultLocale() then convert
     * String to Locale. Default locale will be set when application is started.
     * 
     * @return java.util.Locale
     */
    public static Locale getDefaultLocale() {
        return LocaleUtility.langToLocale(ContextParams.getDefaultLocale());
    }

    /**
     * Checks if is valid date.
     * 
     * @param inDate the in date
     * @param pattern the for format
     * @return true, if is valid date
     */
    public static boolean isValidDate(String inDate, int pattern) {
        if (null == inDate){
            return false;
        }
        
        SimpleDateFormat formatter = selectDateFormat(pattern);
        
        //set the format to use as a constructor argument
        if (inDate.trim().length() != formatter.toPattern().length()){
            return false;
        }
        formatter.setLenient(false);
        try {
            //parse the inDate parameter
            formatter.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
    
    /**
     * Gets the year.
     * Substring date
     * CreateDate = 2012/06/09 13:58:05 
     * result is 2012
     * @param createDate the create date
     * @return the year
     */
    public static String getYear(Timestamp createDate){
        if(null != createDate){
            return createDate.toString().substring(0, 4);
        }
        return Constants.EMPTY_STRING; 
    }
    
    /**
     * Gets the month.
     * Substring date
     * CreateDate = 2012/06/09 13:58:05 
     * result is 06
     * @param createDate the create date
     * @return the month
     */
    public static String getMonth(Timestamp createDate){
        if(null != createDate){
            return createDate.toString().substring(5, 7);
        }
        return Constants.EMPTY_STRING; 
    }
    
    /**
     * Compare date.
     * 
     * @param fromDate the from date
     * @param toDate the to date
     * @return the int
     */
    public static int compareDate(String fromDate, String toDate) {
        try {
            
            SimpleDateFormat formatter = new SimpleDateFormat(STR_PATTERN_YYYYMMDD_SLASH);
            
            Date date1 = formatter.parse(fromDate);
            Date date2 = formatter.parse(toDate);
    
            if(date1.compareTo(date2) < Constants.ZERO){
                // return -1 if date1 is less than date2
                return Constants.MINUS_ONE;
            }else if(Constants.ZERO < date1.compareTo(date2)){
                // return 1 if date1 is greater than date2
                return Constants.ONE;
            }else{
                // return 0 if date1 is equal to date2
                return Constants.ZERO;
            }
        } catch (ParseException pe) {
            return Constants.MINUS_TWO;
        }
    }
    
    /**
     * 
     * <p>Add Day.</p>
     *
     * @param date Date
     * @param days Integer
     * @return Date
     */
    public static java.sql.Date addDays(java.sql.Date date, int days){
        Calendar c = Calendar.getInstance();
        c.setTime( date );
        c.add(Calendar.DAY_OF_MONTH, days);
        return new java.sql.Date(c.getTime().getTime());
    }

    /**
     * Parse string date and time to java.sql.Timestamp.
     * 
     * @param dateTime - a String contains date and time
     * @param pattern - for select pattern
     * @return Timestamp object.
     * */
    public static Timestamp parseToTimestamp(String dateTime, int pattern) {
        return parseToTimestamp(dateTime, pattern, null, false);
    }
    
    /**
     * Parse string date and time to java.sql.Timestamp.
     * When cannot parse data return default return
     * 
     * @param dateTime - a String contains date and time
     * @param pattern - for select pattern
     * @param returnVal - default value
     * @return Timestamp object.
     * */
    public static Timestamp parseToTimestamp(String dateTime, int pattern, Timestamp returnVal) {
        return parseToTimestamp(dateTime, pattern, returnVal, false);
    }
    
    /**
     * Parse string date and time to java.sql.Timestamp.
     * When cannot parse data return default return
     * 
     * @param dateTime - a String contains date and time
     * @param pattern - for select pattern
     * @param returnVal - default value
     * @param bWrite - write log
     * @return Timestamp object.
     * */
    public static Timestamp parseToTimestamp(String dateTime, int pattern, Timestamp returnVal,
        boolean bWrite) {
        try {

            SimpleDateFormat formatter = selectDateFormat(pattern);
            if( null == dateTime || Constants.EMPTY_STRING.equals(dateTime) ){
                return returnVal;
            }else if(Constants.STR_ZERO.equals(dateTime)) {
                return returnVal;
            }else {
                return new Timestamp(formatter.parse(dateTime).getTime());
            }
        } catch (ParseException e) {
            if( bWrite ){
                LOG.error("Class: " + e.getClass() + " " + e.getMessage());
            }
        }
        return returnVal;
    }
    
    /**
     * Parse string date to java.sql.Date.
     * When cannot parse data return default return
     * @param date - a String contains date
     * @param pattern - for select pattern
     * @return converted Date
     * */
    public static java.sql.Date parseToSqlDate(String date, int pattern) {
        return parseToSqlDate(date, pattern, null, false);
    }
    
    /**
     * Parse string date to java.sql.Date.
     * When cannot parse data return default return
     * @param date - a String contains date
     * @param pattern - for select pattern
     * @param returnVal - default value
     * @return converted Date
     * */
    public static java.sql.Date parseToSqlDate(String date, int pattern, java.sql.Date returnVal) {
        return parseToSqlDate(date, pattern, returnVal, false);
    }
    
    /**
     * Parse string date to java.sql.Date.
     * @param date - a String contains date
     * @param pattern - for select pattern
     * @param returnVal - default value
     * @param bWrite - write log
     * @return converted Date
     * */
    public static java.sql.Date parseToSqlDate(String date, int pattern, java.sql.Date returnVal,
        boolean bWrite) {
        try {
            SimpleDateFormat formatter = selectDateFormat(pattern);
            
            if( null == date || Constants.EMPTY_STRING.equals(date) ){
                return returnVal;
            }else if(Constants.STR_ZERO.equals(date)) {
                return returnVal;
            }else {
                return new java.sql.Date(formatter.parse(date).getTime());
            }
        } catch (ParseException e) {
            if( bWrite ){
                LOG.error("Class: " + e.getClass() + " " + e.getMessage());
            }
        }
        return returnVal;
    }
    
    /**
     * Parse string date to java.util.Date.
     * @param date - a String contains date
     * @param pattern - for select pattern
     * @return converted Date
     * */
    public static Date parseToUtilDate(String date, int pattern) {
        return parseToUtilDate(date, pattern, null, false);
    }
    
    /**
     * Parse string date to java.util.Date.
     * @param date - a String contains date
     * @param pattern - for select pattern
     * @param returnVal - default value
     * @return converted Date
     * */
    public static Date parseToUtilDate(String date, int pattern, Date returnVal) {
        return parseToUtilDate(date, pattern, returnVal, false);
    }
    
    /**
     * Parse string date to java.util.Date.
     * @param date - a String contains date
     * @param pattern - for select pattern
     * @param returnVal - default value
     * @param bWrite - write log
     * @return converted Date
     * */
    public static Date parseToUtilDate(String date, int pattern, Date returnVal,
        boolean bWrite) {
        if (StringUtil.checkNullOrEmpty(date)){
            return returnVal;
        }
        try {
            SimpleDateFormat formatter = selectDateFormat(pattern);
            
            if( null == date || Constants.EMPTY_STRING.equals(date) ){
                return returnVal;
            }else if(Constants.STR_ZERO.equals(date)) {
                return returnVal;
            }else {
                return formatter.parse(date);
            }
        } catch (Exception e) {
            if( bWrite ){
                LOG.error("Class: " + e.getClass() + " " + e.getMessage());
            }
        }
        return returnVal;
    }
    
    /**
     * 
     * <p>Format Date to String.</p>
     *
     * @param date - java.util.Date to format to String
     * @param pattern - for select pattern
     * @return String value
     */
    public static String format(Date date, int pattern) {
        try {
            SimpleDateFormat formatter = selectDateFormat(pattern);
            
            return formatter.format(date);
        } catch (Exception e) {
//            LOG.error("Class: " + e.getClass() + " " + e.getMessage());
        }
        return null;
    }
    
    /* ========================================================================================== */
    /* JULIAN calendar util */
    /* ========================================================================================== */
    
    /**
     * Convert String Gregorian date to String Julian Date.
     * @param gregorianDate string Gregorian date in format yyyy/MM/dd
     * @return Julian date in format xyyddd
     * */
    public static String convertStringGregorianDateToJulianDate(String gregorianDate) {
        String result = null;
        Calendar calendar = GregorianCalendar.getInstance();
        int intJulianDate = 0;
        
        try {
            if ((null != gregorianDate) && !Constants.EMPTY_STRING.equals(gregorianDate)) {
                SimpleDateFormat formatter = new SimpleDateFormat(STR_PATTERN_YYYYMMDD_SLASH);
                calendar.setTimeInMillis(formatter.parse(gregorianDate).getTime());
            }
            int year = calendar.get(Calendar.YEAR);
            if (Constants.TWO_THOUSANDS < year) {
                intJulianDate += Constants.ONE_HUNDRED_THOUSAND;
            }
            intJulianDate += (year % Constants.ONE_HUNDRED) * Constants.ONE_THOUSAND;
            intJulianDate += calendar.get(Calendar.DAY_OF_YEAR);
            result = String.valueOf(intJulianDate);
        } catch (ParseException e) {
            return null;
        }
        
        return result;
    }

    /**
     * Convert java.sql.Date Gregorian date to String Julian Date.
     * @param gregorianDate java.sql.Date Gregorian date
     * @return Julian date in format xyyddd
     * */
    public static String convertDateGregorianDateToJulianDate(java.sql.Date gregorianDate) {
        String result = null;
        Calendar calendar = GregorianCalendar.getInstance();
        int intJulianDate = 0;
        
        calendar.setTimeInMillis(gregorianDate.getTime());
        int year = calendar.get(Calendar.YEAR);
        // 100000 or 000000
        if (Constants.TWO_THOUSANDS < year) {
            intJulianDate += Constants.ONE_HUNDRED_THOUSAND;
        }
        
        // 1yy000 or 0yy000
        intJulianDate += (year % Constants.ONE_HUNDRED) * Constants.ONE_THOUSAND;
        
        // 1yyddd or 0yyddd
        intJulianDate += calendar.get(Calendar.DAY_OF_YEAR);
        result = String.valueOf(intJulianDate);
        
        return result;
    }

    /**
     * Convert String Julian Date to java.sql.Date in Gregorian system.
     * @param julianDate String Julian date in format xyyddd
     * @return java.sql.Date in Gregorian system
     * */
    public static java.sql.Date convertStringJulianDateToDateGregorianDate(String julianDate) {
        java.sql.Date result = null;
        Calendar calendar = GregorianCalendar.getInstance();
        
        try {
            int intJulian = Integer.parseInt(julianDate);
            // 1900
            int year = Constants.ONE_THOUSAND_AND_NINE_HUNDRED;
            if (Constants.ONE_HUNDRED_THOUSAND < intJulian) {
                // 2000
                year += Constants.ONE_HUNDRED;
                
                // from 1yyddd to yyddd
                intJulian -= Constants.ONE_HUNDRED_THOUSAND;
            }
            // 1900 or 2000 + yy
            year += intJulian / Constants.ONE_THOUSAND;
            
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.DAY_OF_YEAR, intJulian % Constants.ONE_THOUSAND);
            
            result = new java.sql.Date(calendar.getTimeInMillis());
        } catch (Exception ex) {
            Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
            if (null == locale) {
                locale = LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
            }
            
            LOG.error(MessageUtil.getApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E5_0028, 
                new String[] {ex.getClass().getName(), ex.getMessage()}));
            
            return null;
        }
        
        return result;
    }
    
    /**
     * Create SimpleDateFormat for specified pattern.
     * @param pattern - pattern of simple date format.
     * @return SimpleDateFormat that match to pattern.
     * */
    private static SimpleDateFormat selectDateFormat(int pattern) {
        if (PATTERN_HHMM_COLON == pattern) {
            return new SimpleDateFormat(STR_PATTERN_HHMM_COLON);
        } else if (PATTERN_HHMMSS == pattern) {
            return new SimpleDateFormat(STR_PATTERN_HHMMSS);
        } else if (PATTERN_YYMMDD == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYMMDD);
        } else if (PATTERN_DDMMYY_SLASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_DDMMYY_SLASH);
        } else if (PATTERN_MYY_SLASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_MYY_SLASH);
        } else if (PATTERN_YYYYMMDD_SLASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD_SLASH);
        } else if (PATTERN_YYYYMMDD_DASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD_DASH);
        } else if (PATTERN_YYYYMMDD == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD);
        } else if (PATTERN_YYYYMMDD_HHMM_SLASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD_HHMM_SLASH);
        } else if (PATTERN_YYYYMMDD_HHMMSS == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD_HHMMSS);
        } else if (PATTERN_YYYYMMDD_HHMMSSSSS_DASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD_HHMMSSSSS_DASH);
        } else if (PATTERN_HHMM == pattern) {
            return new SimpleDateFormat(STR_PATTERN_HHMM);
        } else if (PATTERN_DMMYY_SLASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_DMMYY_SLASH);
        } else if (PATTERN_YYYYMM_SLASH == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMM_SLASH);
        } else if (PATTERN_YYYYMMDD_HHMM == pattern) {
            return new SimpleDateFormat(STR_PATTERN_YYYYMMDD_HHMM);
        }
        
        return null;
    }
    
}
