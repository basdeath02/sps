/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;

import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.common.core.util.Props;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;

/**
 * The Class FileUtil.
 * @author CSI
 */
public class FileUtil {

    /** The constant B (from BOM). */
    private static final byte BYTE_B = (byte)0xEF;

    /** The constant O (from BOM). */
    private static final byte BYTE_O = (byte)0xBB;

    /** The constant M (from BOM). */
    private static final byte BYTE_M = (byte)0xBF;
    
    /** The constant BOM_SIZE. */
    private static final int BOM_SIZE = 3;
    
    /**
     * Instantiates a new file util.
     */
    public FileUtil(){
        super();
    }
    
    /**
     * Gets the type of file.
     * Ex. fileNameWithExt = pr.txt
     * result = txt
     * @param fileNameWithExt the file name with ext
     * @return the type of file
     */
    public static String getTypeOfFile(String fileNameWithExt){
        String extension = "";
        if(!Strings.judgeBlank(fileNameWithExt)) {
            extension = FilenameUtils.getExtension(fileNameWithExt);
        }
        return extension;
    }
    
    /**
     * Gets the file name without extension.
     * 
     * @param fileNameWithExt the file name with ext
     * @return the file name without extension
     */
    public static String getFileNameWithOutExt(String fileNameWithExt){
        String fileNameWithOutExt = FilenameUtils.removeExtension(fileNameWithExt);
        return fileNameWithOutExt;
    }
    
    /**
     * Generate file name.
     * 
     * @param fileNameWithExt the file name with ext
     * @param running for add running number to file name
     * @param id and id for document
     * @param docType PR or PO
     * Ex. 
     * fileNameWithExt = prNo.txt
     * result = PR01-20120723_111213_01.txt
     * @return the string
     */
    public static String generateFileName(String fileNameWithExt,
        int running, int id, String docType)
    {
        String generateFileName = "";
        Date date = new Date();
        String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
        String currentTime = new SimpleDateFormat("HHmmss").format(date);
        String runningNumber = String.format("%02d", running);
        generateFileName = 
            docType + id + "-" + currentDate + "_" + currentTime + "_"
            + runningNumber + "." + getTypeOfFile(fileNameWithExt);
        return generateFileName;
    }
    
    /**
     * Gets the upload full path.
     * 
     * @param filePath the file path
     * @param fileName the file name
     * @return the upload full path
     */
    public static String getUploadFullPath(String filePath, String fileName){
        Properties prop = Props.getProperties("fileManagerfs");
        String extension = prop.getProperty("rootPath") + File.separator 
            + filePath + File.separator + fileName;
        return extension;
    }
    
    /**
     * Delete file.
     * 
     * @param file the file
     * @return true, if successful
     * @throws FileNotFoundException the file not found exception
     */
    public static boolean deleteFile(File file) throws FileNotFoundException {
        boolean result = false;
        if(!file.exists()){
            throw new FileNotFoundException("File not found : " + file);
        }else if(!file.delete()){
            throw new IllegalArgumentException("Can not delete file : " + file);
        }else{
            result = true;
        }
        return result;
    }
    
    /**
     * Check utf8 bom and skip
     * <ul>
     * <li>Check the utf8 file If file has BOM, the system will skip.</li>
     * </ul>
     * @param inputStream the inputStream
     * @return the inputStream
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static InputStream checkUtf8BOMAndSkip(InputStream inputStream) throws IOException {
        PushbackInputStream pushBackIS = new PushbackInputStream(
            new BufferedInputStream(inputStream), FileUtil.BOM_SIZE);
        byte[] bom = new byte[FileUtil.BOM_SIZE];
        if (Constants.CANNOT_READ_FILE != pushBackIS.read(bom)) {
            if (!(FileUtil.BYTE_B == bom[Constants.ZERO] && FileUtil.BYTE_O == bom[Constants.ONE] 
                && FileUtil.BYTE_M == bom[Constants.TWO])) {
                pushBackIS.unread(bom);
            }
        }
        return pushBackIS;
    }
    
    /**
     * Read InputStream and write to OutputStream.
     * @param output an OutputStream
     * @param input an InputStream
     * @throws IOException in case cannot read input or write output
     * */
    public static void writeInputStreamToOutputStream(InputStream input, OutputStream output)
        throws IOException
    {
        byte [] buffer = new byte[SupplierPortalConstant.LENGTH_STREAM_BUFFER];
        int readSize = Constants.MINUS_ONE;

        while( (readSize = input.read(buffer, Constants.ZERO, Constants.READ_BUFFER_SIZE))
            != Constants.MINUS_ONE )
        {
            output.write(buffer, Constants.ZERO, readSize);
        }
        input.close();
        output.flush();
    }
    
}