/*
 * ModifyDate Development company     Describe 
 * 2014/1016   CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.utils;

import org.apache.commons.logging.Log;

import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.conf.ApplicationConfig;

/**
 * Utility class load web context property for batch.
 * @author CSI
 * */
public class LoadContextPropertyForBatch {

    /** Base directory for Application Config. */
    private static final String BASE_DIR_APP_CONF =
        "com.globaldenso.asia.sps.conf.ApplicationConfig";
    
    /** Default Locale. */
    private static final String DEFAULT_LOCALE = "en_US";
    
    
    /**
     * Default constructor.
     * */
    public LoadContextPropertyForBatch() {
        // default
    }
    
    /**
     * Load Context Listener.
     * @param LOG - logger.
     * */
    public static void loadContextListener(Log LOG) {
        try {
            ContextParams.setBaseDirApplicationConf(BASE_DIR_APP_CONF);
            
            ContextParams.setDefaultLocale(DEFAULT_LOCALE);
            
            String baseDirGui = MessageUtil.getMessage(BASE_DIR_APP_CONF, 
                ApplicationConfig.KEY.BASE_DIR_GUI.name());
            ContextParams.setBaseDirGui(baseDirGui);
            LOG.debug(baseDirGui);
            
            String baseDirReportGui = MessageUtil.getMessage(BASE_DIR_APP_CONF, 
                ApplicationConfig.KEY.BASE_DIR_REPORT_GUI.name());
            ContextParams.setBaseDirReportGui(baseDirReportGui);
            LOG.debug(baseDirReportGui);
            
            String baseEmail = MessageUtil.getMessage(BASE_DIR_APP_CONF, 
                ApplicationConfig.KEY.BASE_DIR_EMAIL.name());
            ContextParams.setBaseEmail(baseEmail);
            LOG.debug(baseEmail);

            String baseDirMsg = MessageUtil.getMessage(BASE_DIR_APP_CONF, 
                ApplicationConfig.KEY.BASE_DIR_MSG.name());
            ContextParams.setBaseDirMsg(baseDirMsg);
            LOG.debug(baseDirMsg);

            String legendFileId = MessageUtil.getMessage(BASE_DIR_APP_CONF, 
                ApplicationConfig.KEY.LEGEND_FILE_ID.name());
            ContextParams.setLegendFileId(legendFileId);
            LOG.debug(legendFileId);
            
            String maxCsvFileSize = MessageUtil.getMessage(BASE_DIR_APP_CONF,
                ApplicationConfig.KEY.MAX_CSV_FILE_SIZE.name());
            ContextParams.setMaxCsvFileSize(Integer.valueOf(maxCsvFileSize));
            LOG.debug(maxCsvFileSize);
            
            String jasperFilePath = MessageUtil.getMessage(BASE_DIR_APP_CONF,
                ApplicationConfig.KEY.JASPER_FILE_PATH.name());
            LOG.debug(jasperFilePath);
            ContextParams.setJasperFilePath(jasperFilePath);
            
            String emailSmtp = MessageUtil.getMessage(BASE_DIR_APP_CONF,
                ApplicationConfig.KEY.EMAIL_SMTP.name());
            ContextParams.setEmailSmtp(emailSmtp);
            LOG.debug(emailSmtp);
            
            String defaultEmailFrom = MessageUtil.getMessage(BASE_DIR_APP_CONF,
                ApplicationConfig.KEY.DEFAULT_EMAIL_FROM.name());
            ContextParams.setDefaultEmailFrom(defaultEmailFrom);
            LOG.debug(defaultEmailFrom);
            
            String defaultCigmaPoNo = MessageUtil.getMessage(BASE_DIR_APP_CONF,
                ApplicationConfig.KEY.DEFAULT_CIGMA_PO_NO.name());
            ContextParams.setDefaultCigmaPoNo(defaultCigmaPoNo);
            LOG.debug("Default Cigma P/O No: " + defaultCigmaPoNo);
            
        } catch (Exception e) {
            LOG.debug(e.toString());
        }
    }
}
