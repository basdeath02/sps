/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 2015/10/06 CSI Akat                [IN016]
 * 2015/10/14 CSI Akat                [IN026]
 * 2015/10/27 CSI Akat                [IN036]
 * 2015/11/16 CSI Akat                [IN025]
 * 2015/12/24 CSI Akat                [IN043]
 * 2016/01/06 CSI Akat                [IN039]
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/02/16 CSI Akat                [IN054]
 * 2016/03/16 CSI Akat                [IN068]
 * 2016/03/31 CSI Akat                [IN063]
 * 2016/04/18 CSI Akat                [IN070]
 * 2016/04/29 CSI Akat                [IN071]
 * 2016/04/29 Netband Napol           [IN072]
 * 2015/08/22 Netband U.Rungsiwut     SPS phase II
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class SupplierPortalConstant Constants.
 * @author CSI
 */
public class SupplierPortalConstant {

    /** The Constant USER_TYPE_DENSO. */
    public static final String USER_TYPE_DENSO = "DENSO";
    
    /** The Constant USER_TYPE_SUPPLIER. */
    public static final String USER_TYPE_SUPPLIER = "Supplier";
    
    /** The Constant COMPANY_DENSO_TYPE_THA. */
    public static final String COMPANY_DENSO_TYPE_THA = "THA";
    
    /** The Constant COMPANY_DENSO_TYPE_MAL. */
    public static final String COMPANY_DENSO_TYPE_MAL = "MAL";
    
    /** The Constant COMPANY_DENSO_TYPE_IND. */
    public static final String COMPANY_DENSO_TYPE_IND = "IND";
    
    /** The constant for Invoice Status Hold Payment in CIGMA side. */
    public static final String INVOICE_STATUS_HOLD_PAYMENT_CIGMA = "H";
    
    /** The constant for Invoice Status Paid in CIGMA side. */
    public static final String INVOICE_STATUS_PAID_CIGMA = "P";
    
    /** The constants for parameter date not specified date. */
    public static final String PARAM_DATE_NOT_SPECIFIED = "9999/99/99";
    
    /** The max part for generate QR Code. */
    public static final int MAX_PART_FOR_QR = 30;
    
    /** The max CIGMA DO Number for generate QR Code. */
    public static final int MAX_DO_NO_FOR_QR = 10;

    /** The max CIGMA DO Number for generate QR Code. */
    public static final int MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN = 30;

    /** The max CIGMA DO Number for generate QR Code. */
    public static final int MAX_RECORD_FOR_RECEIVE_BY_SCAN = 4 * MAX_PARTS_PER_QR_WHEN_RECEIVE_BY_SCAN;

    /** The max CIGMA DO Number for generate QR Code. */
    public static final String STR_MAX_RECORD_FOR_RECEIVE_BY_SCAN = "STR_MAX_RECORD_FOR_RECEIVE_BY_SCAN";
    
    /** The Constant DEFAULT_INV_COVERPAGE_DIFF_DPCD. */
    public static final String DEFAULT_INV_COVERPAGE_DIFF_DPCD = "CO";
    
    /* Misc Type ================================================================================ */
    
    /** The Constant LIMIT_BATCH_RECORD_TYPE. */
    public static final String LIMIT_BATCH_RECORD_TYPE = "BatchRecordLimit";
    
    /** The Constant LIMIT_MAX_REC_ORD_E_CODE. */
    public static final String LIMIT_MAX_REC_ORD_E_CODE = "MaxRecOrdE";
    
    /** The Constant LIMIT_MAX_REC_ORD_C_CODE. */
    public static final String LIMIT_MAX_REC_ORD_C_CODE = "MaxRecOrdC";
    
    /** The Constant MISC_TYPE_INVOICE_RCV_STATUS. */
    public static final String MISC_TYPE_INVOICE_RCV_STATUS = "InvoiceRCVStatus";
    
    /** The Constant MISC_TYPE_INVOICE_STATUS. */
    public static final String MISC_TYPE_INVOICE_STATUS = "InvoiceStatus";
    
    /** The Constant MISC_CODE_WINV001_RLM. */
    public static final String MISC_CODE_WINV001_RLM = "WINV001RLm";
    
    /** The Constant MISC_CODE_WINV001_PLM. */
    public static final String MISC_CODE_WINV001_PLM = "WINV001PLm";
    
    /** The Constant MISC_CODE_WINV002_RLM. */
    public static final String MISC_CODE_WINV002_RLM = "WINV002RLm";
    
    /** The Constant MISC_CODE_WINV002_PLM. */
    public static final String MISC_CODE_WINV002_PLM = "WINV002PLm";
    
    /** The Constant MISC_CODE_WINV003_RLM. */
    public static final String MISC_CODE_WINV003_RLM = "WINV003RLm";
    
    /** The Constant MISC_CODE_WINV003_PLM. */
    public static final String MISC_CODE_WINV003_PLM = "WINV003PLm";
    
    /** The Constant MISC_CODE_WINV004_RLM. */
    public static final String MISC_CODE_WINV004_RLM = "WINV004RLm";
    
    /** The Constant MISC_CODE_WSHP007_RLM. */
    public static final String MISC_CODE_WSHP007_RLM = "WSHP007RLm";
    
    /** The Constant MISC_CODE_WSHP007_PLM. */
    public static final String MISC_CODE_WSHP007_PLM = "WSHP007PLm";
    
    /** The Constant MISC_CODE_WINV005_RLM. */
    public static final String MISC_CODE_WINV005_RLM = "WINV005RLm";
    
    /** The Constant MISC_CODE_WINV005_PLM. */
    public static final String MISC_CODE_WINV005_PLM = "WINV005PLm";
    
    /** The Constant MISC_CODE_WINV007_RLM. */
    public static final String MISC_CODE_WINV007_RLM = "WINV007RLm";
    
    /** The Constant MISC_CODE_WINV007_PLM. */
    public static final String MISC_CODE_WINV007_PLM = "WINV007PLm";
    
    /** The Constant BORD0003_MISC_TYPE. */
    public static final String BORD0003_MISC_TYPE = "BatchRecLm";
    
    /** The Constant BORD0003_MISC_CODE. */
    public static final String BORD0003_MISC_CODE = "MaxRecAsnS";

    /** The Constant BORD0006_MISC_TYPE. */
    public static final String BORD0006_MISC_TYPE = "BatchRecLm";
    
    /** The Constant BORD0006_MISC_CODE. */
    public static final String BORD0006_MISC_CODE = "MaxRecOrdE";

    /** The Constant MISC_TYPE_PENDING_REASON_CB. */
    public static final String MISC_TYPE_PENDING_REASON_CB = "PendRSNCB";
    
    /** The Constant MISC_TYPE_SHIP_STS_CB. */
    public static final String MISC_TYPE_SHIP_STS_CB = "ShipStsCB";
    
    /** The Constant MISC_TYPE_TRANS_CB. */
    public static final String MISC_TYPE_TRANS_CB = "TRANsCB";
    
    /** The Constant MISC_TYPE_TRANS_CB. */
    public static final String MISC_TYPE_REVISION_CB = "RevisionCB";
    
    /** The Constant MISC_TYPE_ORDER_MTH_CB. */
    public static final String MISC_TYPE_ORDER_MTH_CB = "OrderMthCB";
    
    /** The Constant MISC_TYPE_CHANGE_REASON_CB. */
    public static final String MISC_TYPE_CHANGE_REASON_CB = "ChangeReasonCB";
    
    /** The Constant MISC_TYPE_SCR_REC_LM. */
    public static final String MISC_TYPE_SCR_REC_LM = "ScrnRecLm";
    
    /** The Constant MISC_TYPE_SCR_REC_LM_PG. */
    public static final String MISC_TYPE_SCR_REC_LM_PG = "ScrnRecLmPg";
    
    /** The Constant MISC_TYPE_SCR_REC_LM_CSV. */
    public static final String MISC_TYPE_SCR_REC_LM_CSV = "ScrnRecLmCsv";
    
    // [IN054] Add new Misc Type for new combobox
    /** The Constant MISC_TYPE_ACCEPT_REJECT_CB. */
    public static final String MISC_TYPE_ACCEPT_REJECT_CB = "AcceptRejectCB";

    // [SPS Phase II] Add new Misc Type for new combobox receiveByScan
    /** The Constant MISC_TYPE_RECEIVE_BY_SCAN. */
    public static final String MISC_TYPE_RECEIVE_BY_SCAN = "receiveByScan";
    
    // [SPS Phase II] Add new Misc Type for new combobox ltFlag
    /** The Constant MISC_TYPE_RECEIVE_BY_SCAN. */
    public static final String MISC_TYPE_LT_FLAG = "ltFlag";
    
    /** The Constant MISC_CODE_WSHP001_RLM. */
    public static final String MISC_CODE_WSHP001_RLM = "WSHP001RLm";
    
    /** The Constant MISC_CODE_WSHP008_RLM. */
    public static final String MISC_CODE_WSHP008_RLM = "WSHP008RLm";
    
    /** The Constant MISC_CODE_WSHP008_PLM. */
    public static final String MISC_CODE_WSHP008_PLM = "WSHP008PLm";
    
    /** The Constant MISC_CODE_WSHP001_RLM. */
    public static final String MISC_CODE_WSHP009_RLM = "WSHP009RLm";
    
    /** The Constant MISC_CODE_WSHP001_PLM. */
    public static final String MISC_CODE_WSHP001_PLM = "WSHP001PLm";
    
    /** The Constant MISC_CODE_WSHP009_PLM. */
    public static final String MISC_CODE_WSHP009_PLM = "WSHP009PLm";
    
    /** The Constant MISC_CODE_WADM001_PLM. */
    public static final String MISC_CODE_WADM001_PLM = "WADM001PLm";
    
    /** The Constant MISC_CODE_WADM001_RLM. */
    public static final String MISC_CODE_WADM001_RLM = "WADM001RLm";
    
    /** The Constant MISC_CODE_WADM003_PLM. */
    public static final String MISC_CODE_WADM003_PLM = "WADM003PLm";
    
    /** The Constant MISC_CODE_WADM003_RLM. */
    public static final String MISC_CODE_WADM003_RLM = "WADM003RLm";
    
    /** The Constant MISC_CODE_WADM004_PLM. */
    public static final String MISC_CODE_WADM004_PLM = "WADM004PLm";
    
    /** The Constant MISC_CODE_WADM004_RLM. */
    public static final String MISC_CODE_WADM004_RLM = "WADM004RLm";
    
    /** The Constant MISC_CODE_WADM005_PLM. */
    public static final String MISC_CODE_WADM005_PLM = "WADM005PLm";
    
    /** The Constant MISC_CODE_WADM005_RLM. */
    public static final String MISC_CODE_WADM005_RLM = "WADM005RLm";
    
    /** The Constant MISC_CODE_WADM007_PLM. */
    public static final String MISC_CODE_WADM007_PLM = "WADM007PLm";
    
    /** The Constant MISC_CODE_WADM007_RLM. */
    public static final String MISC_CODE_WADM007_RLM = "WADM007RLm";
    
    /** The Constant MISC_CODE_WADM007_PLM. */
    public static final String MISC_CODE_WADM008_PLM = "WADM008PLm";
    
    /** The Constant MISC_CODE_WADM007_RLM. */
    public static final String MISC_CODE_WADM008_RLM = "WADM008RLm";
    
    /** The Constant MISC_CODE_WORD001_PLM. */
    public static final String MISC_CODE_WORD001_PLM = "WORD001PLm";
    
    /** The Constant MISC_CODE_WORD001_RLM. */
    public static final String MISC_CODE_WORD001_RLM = "WORD001RLm";
    
    /** The Constant MISC_CODE_WSHP003_RLM. */
    public static final String MISC_CODE_WSHP003_RLM = "WSHP003RLm";
    
    /** The Constant MISC_CODE_WSHP004_RLM. */
    public static final String MISC_CODE_WSHP004_RLM = "WSHP004RLm";
    
    /** The Constant MISC_CODE_WSHP006_RLM. */
    public static final String MISC_CODE_WSHP006_PLM = "WSHP006PLm";
    
    /** The Constant MISC_CODE_WSHP003_PLM. */
    public static final String MISC_CODE_WSHP003_PLM = "WSHP003PLm";
    
    /** The Constant MISC_CODE_ADD_DTE. */
    public static final String MISC_CODE_ADD_DTE = "ADD_DTE"; 

    /** The Constant MISC_TYPE_ASN_STATUS. */
    public static final String MISC_TYPE_ASN_STATUS = "ASNStatus";
    
    /** The Constant MISC_TYPE_VAL_UPLOAD. */
    public static final String MISC_TYPE_VAL_UPLOAD = "ADM_UPLD";
    
    /**Start: Miscellaneous Type for Purchase Order **/

    /** The PO Status. */
    public static final String MISC_TYPE_PO_STATUS = "POStatusCB";
    
    /** The PO Type. */
    public static final String MISC_TYPE_PO_TYPE = "POTypeCB";
    
    /** The Period Type. */
    public static final String MISC_TYPE_PERIOD_TYPE = "PeriodTyCB";
    
    /** The View PDF. */
    public static final String MISC_TYPE_VIEW_PDF = "POVwPDFCB";
    
    // [IN063] Misc Type for Digital Signature Type
    /** The Digital Signature Type Combo box. */
    public static final String MISC_TYPE_SIGNATURE_TYPE = "SignatureTypeCB";
    
    /** The Period Type Firm. */
    public static final String FIRM = "Firm";
    
    /** The Period Type Forecast. */
    public static final String FORECAST = "Forecast";
    
    /** The PO Status ISSUED. */
    public static final String PO_STATUS_ISSUED = "ISS";
    
    /** The FAC. */
    public static final String FAC = "FAC";
    
    /** The BATCH ID BORD001. */
    public static final String BORD001 = "BORD001";
    
    /** The PO Status ACKNOWLEDGE. */
    public static final String PO_STATUS_ACKNOWLEDGE = "ACK";

    /** The PO Status PENDING. */
    public static final String PO_STATUS_PENDING = "PED";
    
    // [IN054] add new P/O Status RPL (DENSO Reply)
    /** The PO Status DENSO Reply. */
    public static final String PO_STATUS_REPLY = "RPL";

    /** The Constant MISC_CODE_WORD002_RLM. */
    public static final String MISC_CODE_WORD002_RLM = "WORD002RLm";

    /** The Constant MISC_CODE_WORD002_PLM. */
    public static final String MISC_CODE_WORD002_PLM = "WORD002PLm";

    /** The Constant MISC_CODE_WORD003_PLM. */
    public static final String MISC_CODE_WORD003_PLM = "WORD003PLm";

    /** The Constant MISC_CODE_WORD006_PLM. */
    public static final String MISC_CODE_WORD006_PLM = "WORD006PLm";
    
    /** The Constant MISC_CODE_WORD006_RLM. */
    public static final String MISC_CODE_WORD006_RLM = "WORD006RLm";
    
    /** The Constant MISC_CODE_WORD007_RLM. */
    public static final String MISC_CODE_WORD007_RLM = "WORD007RLm";
    
    /** The Constant MISC_CODE_WORD007_PLM. */
    public static final String MISC_CODE_WORD007_PLM = "WORD007PLm";
    
    /**End: Miscellaneous Type for Purchase Order **/
    /* End Misc Type ============================================================================ */
    
    /* SCREEN ID ================================================================================ */
    /** The constant SCREEN_ID_WCOM001. */
    public static final String SCREEN_ID_WCOM001 = "WCOM001";
    
    /** The constant SCREEN_ID_WCOM002. */
    public static final String SCREEN_ID_WCOM002 = "WCOM002";
    
    /** The constant SCREEN_ID_WCOM003. */
    public static final String SCREEN_ID_WCOM003 = "WCOM003";
    
    /** The constant SCREEN_ID_WORD001. */
    public static final String SCREEN_ID_WORD001 = "WORD001";
    
    /** The constant SCREEN_ID_WORD002. */
    public static final String SCREEN_ID_WORD002 = "WORD002";
    
    /** The constant SCREEN_ID_WORD003. */
    public static final String SCREEN_ID_WORD003 = "WORD003";
    
    /** The constant SCREEN_ID_WORD004. */
    public static final String SCREEN_ID_WORD004 = "WORD004";
    
    /** The constant SCREEN_ID_WORD005. */
    public static final String SCREEN_ID_WORD005 = "WORD005";
    
    /** The constant SCREEN_ID_WORD006. */
    public static final String SCREEN_ID_WORD006 = "WORD006";
    
    /** The constant SCREEN_ID_WORD007. */
    public static final String SCREEN_ID_WORD007 = "WORD007";
    
    /** The constant SCREEN_ID_WSHP001. */
    public static final String SCREEN_ID_WSHP001 = "WSHP001";
    
    /** The constant SCREEN_ID_WSHP003. */
    public static final String SCREEN_ID_WSHP003 = "WSHP003";
    
    /** The constant SCREEN_ID_WSHP004. */
    public static final String SCREEN_ID_WSHP004 = "WSHP004";
    
    /** The constant SCREEN_ID_WSHP006. */
    public static final String SCREEN_ID_WSHP006 = "WSHP006";
    
    /** The constant SCREEN_ID_WSHP007. */
    public static final String SCREEN_ID_WSHP007 = "WSHP007";
    
    /** The constant SCREEN_ID_WSHP008. */
    public static final String SCREEN_ID_WSHP008 = "WSHP008";
    
    /** The constant SCREEN_ID_WSHP009. */
    public static final String SCREEN_ID_WSHP009 = "WSHP009";
    
    /** The constant SCREEN_ID_WINV001. */
    public static final String SCREEN_ID_WINV001 = "WINV001";
    
    /** The constant SCREEN_ID_WINV002. */
    public static final String SCREEN_ID_WINV002 = "WINV002";
    
    /** The constant SCREEN_ID_WINV003. */
    public static final String SCREEN_ID_WINV003 = "WINV003";
    
    /** The constant SCREEN_ID_WINV004. */
    public static final String SCREEN_ID_WINV004 = "WINV004";
    
    /** The constant SCREEN_ID_WINV005. */
    public static final String SCREEN_ID_WINV005 = "WINV005";
    
    /** The constant SCREEN_ID_WINV006. */
    public static final String SCREEN_ID_WINV006 = "WINV006";
    
    /** The constant SCREEN_ID_WINV007. */
    public static final String SCREEN_ID_WINV007 = "WINV007";
    
    /** The constant SCREEN_ID_WADM001 */
    public static final String SCREEN_ID_WADM001 = "WADM001";
    
    /** The constant SCREEN_ID_WADM002*/
    public static final String SCREEN_ID_WADM002 = "WADM002";
    
    /** The constant SCREEN_ID_WADM003 */
    public static final String SCREEN_ID_WADM003 = "WADM003";
    
    /** The constant SCREEN_ID_WADM004 */
    public static final String SCREEN_ID_WADM004 = "WADM004";
    
    /** The constant SCREEN_ID_WADM005 */
    public static final String SCREEN_ID_WADM005 = "WADM005";
    
    /** The constant SCREEN_ID_WADM006 */
    public static final String SCREEN_ID_WADM006 = "WADM006";
    
    /** The constant SCREEN_ID_WADM007 */
    public static final String SCREEN_ID_WADM007 = "WADM007";
    
    /** The constant SCREEN_ID_WADM008 */
    public static final String SCREEN_ID_WADM008 = "WADM008";

    // [IN063] New Screen
    /** The constant SCREEN_ID_WADM009 */
    public static final String SCREEN_ID_WADM009 = "WADM009";
    
    /* End SCREEN ID ============================================================================ */
    
    /* MESSAGE ID =============================================================================== */

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0004 = "SP-80-0004";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0009 = "SP-80-0009";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0010 = "SP-80-0010";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0012 = "SP-80-0012";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0013 = "SP-80-0013";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0014 = "SP-80-0014";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0015 = "SP-80-0015";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0016 = "SP-80-0016";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_80_0017 = "SP-80-0017";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_90_0001 = "SP-90-0001";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_90_0002 = "SP-90-0002";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_90_0003 = "SP-90-0003";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_90_0004 = "SP-90-0004";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I5_0001 = "SP-I5-0001";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I5_0002 = "SP-I5-0002";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I5_0003 = "SP-I5-0003";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I5_0005 = "SP-I5-0005";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I5_0006 = "SP-I5-0006";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0001 = "SP-I6-0001";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0002 = "SP-I6-0002";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0003 = "SP-I6-0003";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0004 = "SP-I6-0004";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0005 = "SP-I6-0005";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0006 = "SP-I6-0006";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0007 = "SP-I6-0007";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0008 = "SP-I6-0008";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0010 = "SP-I6-0010";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0012 = "SP-I6-0012";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0013 = "SP-I6-0013";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0014 = "SP-I6-0014";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0015 = "SP-I6-0015";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0016 = "SP-I6-0016";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0017 = "SP-I6-0017";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0018 = "SP-I6-0018";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0019 = "SP-I6-0019";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0020 = "SP-I6-0020";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0021 = "SP-I6-0021";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0022 = "SP-I6-0022";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0023 = "SP-I6-0023";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0024 = "SP-I6-0024";

    // [IN054] New message for DENSO Reply Process Finish
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_I6_0025 = "SP-I6-0025";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0001 = "SP-E5-0001";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0002 = "SP-E5-0002";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0003 = "SP-E5-0003";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0004 = "SP-E5-0004";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0005 = "SP-E5-0005";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0006 = "SP-E5-0006";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0007 = "SP-E5-0007";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0008 = "SP-E5-0008";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0009 = "SP-E5-0009";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0010 = "SP-E5-0010";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0011 = "SP-E5-0011";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0012 = "SP-E5-0012";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0018 = "SP-E5-0018";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0023 = "SP-E5-0023";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0025 = "SP-E5-0025";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0026 = "SP-E5-0026";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0028 = "SP-E5-0028";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0029 = "SP-E5-0029";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0030 = "SP-E5-0030";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0031 = "SP-E5-0031";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0032 = "SP-E5-0032";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0033 = "SP-E5-0033";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0034 = "SP-E5-0034";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0035 = "SP-E5-0035";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0036 = "SP-E5-0036";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0037 = "SP-E5-0037";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0038 = "SP-E5-0038";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0039 = "SP-E5-0039";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0040 = "SP-E5-0040";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0041 = "SP-E5-0041";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0042 = "SP-E5-0042";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0043 = "SP-E5-0043";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0044 = "SP-E5-0044";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0045 = "SP-E5-0045";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0046 = "SP-E5-0046";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0047 = "SP-E5-0047";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0048 = "SP-E5-0048";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0049 = "SP-E5-0049";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E5_0050 = "SP-E5-0050";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0001 = "SP-E6-0001";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0002 = "SP-E6-0002";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0003 = "SP-E6-0003";
    // [IN071] Add constants for error message
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0004 = "SP-E6-0004";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0006 = "SP-E6-0006";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0007 = "SP-E6-0007";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0008 = "SP-E6-0008";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0009 = "SP-E6-0009";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0010 = "SP-E6-0010";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0012 = "SP-E6-0012";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0013 = "SP-E6-0013";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0014 = "SP-E6-0014";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0015 = "SP-E6-0015";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0016 = "SP-E6-0016";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0017 = "SP-E6-0017";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0018 = "SP-E6-0018";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0019 = "SP-E6-0019";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0020 = "SP-E6-0020";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0021 = "SP-E6-0021";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0022 = "SP-E6-0022";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0023 = "SP-E6-0023";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0024 = "SP-E6-0024";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0025 = "SP-E6-0025";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0026 = "SP-E6-0026";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0027 = "SP-E6-0027";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0029 = "SP-E6-0029";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0031 = "SP-E6-0031";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0032 = "SP-E6-0032";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0033 = "SP-E6-0033";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0034 = "SP-E6-0034";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0035 = "SP-E6-0035";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0036 = "SP-E6-0036";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0038 = "SP-E6-0038";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0039 = "SP-E6-0039";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0040 = "SP-E6-0040";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0041 = "SP-E6-0041";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0042 = "SP-E6-0042";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0043 = "SP-E6-0043";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0044 = "SP-E6-0044";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0045 = "SP-E6-0045";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0046 = "SP-E6-0046";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0047 = "SP-E6-0047";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0049 = "SP-E6-0049";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0050 = "SP-E6-0050";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0051 = "SP-E6-0051";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0054 = "SP-E6-0054";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0055 = "SP-E6-0055";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0056 = "SP-E6-0056";
    
    // [IN054] Add new error message
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0057 = "SP-E6-0057";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0058 = "SP-E6-0058";
    
    // [IN063] Duplica Digital Signature.
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0059 = "SP-E6-0059";
    /** Constants for Error Message code SPS Phase II */
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0060 = "SP-E6-0060";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0061 = "SP-E6-0061";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0062 = "SP-E6-0062";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0063 = "SP-E6-0063";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0064 = "SP-E6-0064";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0065 = "SP-E6-0065";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0066 = "SP-E6-0066";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0067 = "SP-E6-0067";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0068 = "SP-E6-0068";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0069 = "SP-E6-0069";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0070 = "SP-E6-0070";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0071 = "SP-E6-0071";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0072 = "SP-E6-0072";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0073 = "SP-E6-0073";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0074 = "SP-E6-0074";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0075 = "SP-E6-0075";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0076 = "SP-E6-0076";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0077 = "SP-E6-0077";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0078 = "SP-E6-0078";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0079 = "SP-E6-0079";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0080 = "SP-E6-0080";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0081 = "SP-E6-0081";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0082 = "SP-E6-0082";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0083 = "SP-E6-0083";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E6_0084 = "SP-E6-0084";
    public static final String ERROR_CD_SP_E6_0085 = "SP-E6-0085";
    
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0003 = "SP-E7-0003";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0004 = "SP-E7-0004";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0006 = "SP-E7-0006";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0007 = "SP-E7-0007";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0008 = "SP-E7-0008";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0009 = "SP-E7-0009";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0010 = "SP-E7-0010";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0011 = "SP-E7-0011";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0012 = "SP-E7-0012";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0013 = "SP-E7-0013";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0014 = "SP-E7-0014";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0015 = "SP-E7-0015";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0017 = "SP-E7-0017";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0020 = "SP-E7-0020";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0023 = "SP-E7-0023";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0024 = "SP-E7-0024";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0027 = "SP-E7-0027";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0028 = "SP-E7-0028";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0029 = "SP-E7-0029";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0030 = "SP-E7-0030";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0031 = "SP-E7-0031";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0032 = "SP-E7-0032";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0033 = "SP-E7-0033";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0034 = "SP-E7-0034";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0036 = "SP-E7-0036";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0037 = "SP-E7-0037";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0038 = "SP-E7-0038";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0039 = "SP-E7-0039";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0040 = "SP-E7-0040";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0041 = "SP-E7-0041";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0042 = "SP-E7-0042";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0043 = "SP-E7-0043";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0044 = "SP-E7-0044";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0045 = "SP-E7-0045";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0046 = "SP-E7-0046";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0047 = "SP-E7-0047";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0048 = "SP-E7-0048";
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0049 = "SP-E7-0049";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E7_0050 = "SP-E7-0050";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E9_0003 = "SP-E9-0003";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_E9_0004 = "SP-E9-0004";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W2_0001 = "SP-W2-0001";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W2_0004 = "SP-W2-0004";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W2_0007 = "SP-W2-0007";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0001 = "SP-W6-0001";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0002 = "SP-W6-0002";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0003 = "SP-W6-0003";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0004 = "SP-W6-0004";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0006 = "SP-W6-0006";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0008 = "SP-W6-0008";

    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0011 = "SP-W6-0011";
    
    /** Constants for Error Message code */
    public static final String ERROR_CD_SP_W6_0012 = "SP-W6-0012";
    
    /* End MESSAGE ID =========================================================================== */
    
    /* LABEL KEY ================================================================================ */
    /** The constant LBL_ASN_DATA. */
    public static final String LBL_ASN_DATA = "ASN_DATA";
    
    /** The constant LBL_ASN_STATUS. */
    public static final String LBL_ASN_STATUS = "ASN_STATUS";
    
    /** The constant LBL_ASN_RCV_STATUS. */
    public static final String LBL_ASN_RCV_STATUS = "ASN_RCV_STATUS";
    
    /** The constant LBL_ACTUAL_ETD. */
    public static final String LBL_ACTUAL_ETD = "ACTUAL_ETD";
    
    /** The constant LBL_ASN_NO_NOT_SAME. */
    public static final String LBL_ASN_NO_NOT_SAME = "ASN_NO_NOT_SAME";
    
    /** The constant LBL_ASN_RECEIVED_QTY. */
    public static final String LBL_ASN_RECEIVED_QTY = "ASN_RECEIVED_QTY";
    
    /** The constant LBL_ACTUAL_ETD_DATE. */
    public static final String LBL_ACTUAL_ETD_DATE = "ACTUAL_ETD_DATE";
    
    /** The constant LBL_ACTUAL_ETD_TIME. */
    public static final String LBL_ACTUAL_ETD_TIME = "ACTUAL_ETD_TIME";

    // [IN054] new Label constants for new column
    /** The constant LBL_REASON. */
    public static final String LBL_ACCEPT_REJECT = "ACCEPT_REJECT";
    
    /** The constant LBL_AS400_VENDOR. */
    public static final String LBL_AS400_VENDOR = "AS400_VENDOR";
    
    /** The constant LBL_ALLOW_REVISE. */
    public static final String LBL_ALLOW_REVISE = "ALLOW_REVISE";
    
    /** The constant LBL_CIGMA_DO_NO. */
    public static final String LBL_CIGMA_DO_NO = "CIGMA_DO#";
    
    /** The constant LBL_CANCEL. */
    public static final String LBL_CANCEL = "CANCEL";
    
    /** The constant LBL_CANCEL_ASN. */
    public static final String LBL_CANCEL_ASN = "CANCEL_ASN";
    
    /** The constant LBL_CREATE_ASN. */
    public static final String LBL_CREATE_ASN = "CREATE_ASN";

    /** The constant LBL_CANCEL_INVOICE. */
    public static final String LBL_CANCEL_INVOICE = "CANCEL_INVOICE";
    
    /** The constant LBL_COULD_YOU_PRINT_KANBAN_TAG_FIRST. */
    public static final String LBL_COULD_YOU_PRINT_KANBAN_TAG_FIRST = "COULD_YOU_PRINT_KANBAN_TAG_FIRST";
    
    /** The constant LBL_CSV. */
    public static final String LBL_CSV = "CSV";
    
    /** The constant LBL_CSV_LINE_NO. */
    public static final String LBL_CSV_LINE_NO = "CSV_LINE_NO";
    
    /** The constant LBL_CURRENT_SPS_DO_NO. */
    public static final String LBL_CURRENT_SPS_DO_NO = "CURRENT_SPS_DO_NO";
    
    /** The constant LBL_DENSO_COMPANY_CODE. */
    public static final String LBL_DENSO_COMPANY_CODE = "DENSO_COMPANY_CODE";
    
    /** The constant LBL_DENSO_PLANT_CODE. */
    public static final String LBL_DENSO_PLANT_CODE = "DENSO_PLANT_CODE";
    
    /** The constant LBL_DENSO_AND_SUPPLIER_RELATION. */
    public static final String LBL_DENSO_AND_SUPPLIER_RELATION = "DENSO_AND_SUPPLIER_RELATION";
    
    /** The constant LBL_DENSO_PART_NO. */
    public static final String LBL_DENSO_PART_NO = "DENSO_PART_NO";
    
    /** The constant LBL_D_OWNER. */
    public static final String LBL_D_OWNER = "DENSO_OWNER";

    /** The constant LBL_DEL. */
    public static final String LBL_DEL = "DEL";
    
    /** The constant LBL_DELIVERY_DATE. */
    public static final String LBL_DELIVERY_DATE = "DELIVERY_DATE";
    
    /** The constant LBL_DELIVERY_TIME. */
    public static final String LBL_DELIVERY_TIME = "DELIVERY_TIME";
    
    /** The constant LBL_DELIVERY_TIME_FROM. */
    public static final String LBL_DELIVERY_TIME_FROM = "DELIVERY_TIME_FROM";
    
    /** The constant LBL_DELIVERY_TIME_TO. */
    public static final String LBL_DELIVERY_TIME_TO = "DELIVERY_TIME_TO";
    
    /** The constant LBL_DELIVERY_DATE_FROM. */
    public static final String LBL_DELIVERY_DATE_FROM = "DELIVERY_DATE_FROM";

    /** The constant LBL_DELIVERY_DATE_TO. */
    public static final String LBL_DELIVERY_DATE_TO = "DELIVERY_DATE_TO";

    /** The constant LBL_D_CD_NOT_SAME. */
    public static final String LBL_D_CD_NOT_SAME = "D_CD_NOT_SAME";
    
    /** The constant LBL_D_PCD_NOT_SAME. */
    public static final String LBL_D_PCD_NOT_SAME = "D_PCD_NOT_SAME";
    
    /** The constant LBL_D_PCD_NOT_SAME. */
    public static final String LBL_DENSO_USER = "DENSO_USER"; 
    
    /** The constant LBL_DO_SHIP_STATUS. */
    public static final String LBL_DO_SHIP_STATUS = "DO_SHIP_STATUS";
    
    /** The constant LBL_D_PCD_NOT_SAME. */
    public static final String LBL_EML_URGENT_ORDER = "EML_URGENT_ORDER";
    
    /** The constant LBL_EFFECT_START. */
    public static final String LBL_EFFECT_START = "EFFECT_START";
    
    /** The constant LBL_EFFECT_END. */
    public static final String LBL_EFFECT_END = "EFFECT_END";
    
    /** The constant LBL_EML_S_INFO_NOTFOUND. */
    public static final String LBL_EML_S_INFO_NOTFOUND = "EML_S_INFO_NOTFOUND";
    
    /** The constant LBL_EML_CREATE_CANCEL_ASN. */
    public static final String LBL_EML_CREATE_CANCEL_ASN = "EML_CREATE_CANCEL_ASN";
    
    /** The constant LBL_EML_REVISE_SHIPPING_QTY. */
    public static final String LBL_EML_REVISE_SHIPPING_QTY  = "EML_REVISE_SHIPPING_QTY";
    
    /** The constant LBL_EML_CREATE_INV_WITH_CN. */
    public static final String LBL_EML_CREATE_INV_WITH_CN = "EML_CREATE_INV_WITH_CN";
    
    /** The constant LBL_EML_ABNORMAL_TRANSFER_1. */
    public static final String LBL_EML_ABNORMAL_TRANSFER_1 = "EML_ABNORMAL_TRANSFER_1";
    
    /** The constant LBL_EML_ABNORMAL_TRANSFER_2. */
    public static final String LBL_EML_ABNORMAL_TRANSFER_2 = "EML_ABNORMAL_TRANSFER_2";
    
    /** The constant LBL_EML_ALLOW_REVISE. */
    public static final String LBL_EML_ALLOW_REVISE = "EML_ALLOW_REVISE";
    
    /** The constant LBL_EML_CANCEL_INVOICE. */
    public static final String LBL_EML_CANCEL_INVOICE = "EML_CANCEL_INVOICE";
    
    /** The constant LBL_ERROR. */
    public static final String LBL_ERROR = "ERROR";
    
    /** The constant LBL_ERROR_MESSAGE. */
    public static final String LBL_ERROR_MESSAGE = "ERROR_MESSAGE";

    /** The constant LBL_FILE_NAME. */
    public static final int LENGTH_STREAM_BUFFER = 4096;
    
    /** The constant LBL_REVISE_SHIPPING_FLAG. */
    public static final String LBL_REVISE_SHIPPING_QTY_FLAG = "REVISE_SHIPPING_QTY_FLAG";
    
    /** The constant LBL_FILE_NAME. */
    public static final String LBL_FILE_NAME = "FILE_NAME";
    
    /** The constant LBL_GENERATE_DATE. */
    public static final String LBL_GENERATE_DATE = "GENERATE_DATE";
    
    /** The constant LBL_INNER_TEXT_FROM_EXCEPTION. */
    public static final String LBL_INNER_TEXT_FROM_EXCEPTION = "INNER_TEXT_FROM_EXCEPTION";
    
    /** The constant LBL_INVOICE_NO. */
    public static final String LBL_INVOICE_NO = "INVOICE_NO";

    /** The constant LBL_ISSUED_DATE. */
    public static final String LBL_ISSUED_DATE = "ISSUED_DATE";

    /** The constant LBL_ISSUE_DATE_FROM. */
    public static final String LBL_ISSUE_DATE_FROM = "ISSUE_DATE_FROM";
    
    /** The constant LBL_ISSUE_DATE_TO. */
    public static final String LBL_ISSUE_DATE_TO = "ISSUE_DATE_TO";
    
    /** The constant LBL_INFO_FLAG. */
    public static final String LBL_INFO_FLAG = "INFO_FLAG";
    
    /** The constant LBL_ITEM_DESC. */
    public static final String LBL_ITEM_DESC = "ITEM_DESC"; 
    
    /** The constant LBL_NUMBER_PALLET. */
    public static final String LBL_NUMBER_PALLET = "NUMBER_PALLET";
    
    /** The constant LBL_ORDER_METHOD. */
    public static final String LBL_ORDER_METHOD = "ORDER_METHOD";
    
    /** The constant LBL_PERIOD_TYPE. */
    public static final String LBL_PERIOD_TYPE = "PERIOD_TYPE";
    
    /** The constant LBL_PN_RCV_STATUS. */
    public static final String LBL_PN_RCV_STATUS = "PN_RCV_STATUS";
    
    /** The constant LBL_PN_SHIP_STATUS. */
    public static final String LBL_PN_SHIP_STATUS = "PN_SHIP_STATUS";
    
    /** The constant LBL_PLAN_ETA. */
    public static final String LBL_PLAN_ETA = "PLAN_ETA";
    
    /** The constant LBL_PLAN_ETA_FROM. */
    public static final String LBL_PLAN_ETA_FROM = "PLAN_ETA_FROM";
    
    /** The constant LBL_PLAN_ETA_TO. */
    public static final String LBL_PLAN_ETA_TO = "PLAN_ETA_TO";
    
    /** The constant LBL_PLAN_ETA_DATE. */
    public static final String LBL_PLAN_ETA_DATE = "PLAN_ETA_DATE";
    
    /** The constant LBL_PLAN_ETA_TIME. */
    public static final String LBL_PLAN_ETA_TIME = "PLAN_ETA_TIME";
    
    /** The constant LBL_PLAN_ETA_TIME_FROM. */
    public static final String LBL_PLAN_ETA_TIME_FROM = "PLAN_ETA_TIME_FROM";

    /** The constant LBL_PLAN_ETA_TIME_TO. */
    public static final String LBL_PLAN_ETA_TIME_TO = "PLAN_ETA_TIME_TO";

    /** The constant LBL_PO_STATUS. */
    public static final String LBL_PO_STATUS = "PO_STATUS";

    /** The constant LBL_PO_TYPE. */
    public static final String LBL_PO_TYPE = "PO_TYPE";

    /** The constant LBL_PROPOSED_DATE. */
    public static final String LBL_PROPOSED_DATE = "PROPOSED_DATE";
    
    /** The constant LBL_RECEIVE_BY_SCAN_NOT_SAME. */
    public static final String LBL_RECEIVE_BY_SCAN_NOT_SAME = "RECEIVE_BY_SCAN_NOT_SAME";
    
    /** The constant LBL_REGISTER_INVOICE. */
    public static final String LBL_REGISTER_INVOICE = "REGISTER_INVOICE";

    /** The constant LBL_REGISTER_INVOICE_W_CN. */
    public static final String LBL_REGISTER_INVOICE_W_CN = "REGISTER_INVOICE_W_CN";
    
    /** The constant LBL_ROUTE. */
    public static final String LBL_ROUTE = "ROUTE";
    
    /** The constant LBL_ROLE_CD. */
    public static final String LBL_ROLE_CD = "ROLE_CD";
    
    /** The constant LBL_ROLE_FLAG. */
    public static final String LBL_ROLE_FLAG = "ROLE_FLAG";
    
    /** The constant LBL_ROLE_S_P. */
    public static final String LBL_ROLE_S_P = "ROLE_S_P";
    
    /** The constant LBL_ROLE_D_CD. */
    public static final String LBL_ROLE_D_CD = "ROLE_D_CD";
    
    /** The constant LBL_ROLE_D_PCD. */
    public static final String LBL_ROLE_D_PCD = "ROLE_D_PCD";
    
    /** The constant LBL_RUN_NO. */
    public static final String LBL_RUN_NO = "RUN_NO";
    
    /** The constant LBL_SHIPMENT_DATE_FROM. */
    public static final String LBL_SHIPMENT_DATE_FROM = "SHIPMENT_DATE_FROM";
    
    /** The constant LBL_SHIPMENT_DATE_TO. */
    public static final String LBL_SHIPMENT_DATE_TO = "SHIPMENT_DATE_TO";
    
    /** The constant LBL_SHIPMENT_STATUS. */
    public static final String LBL_SHIPMENT_STATUS = "SHIPMENT_STATUS";

    // Start : [IN063] for Digital Signature.
    /** The constants. LBL_SIGNATURE_TYPE. */
    public static final String LBL_SIGNATURE_TYPE = "SIGNATURE_TYPE";

    /** The constants. LBL_ALLOW_IMAGE_EXTENSION. */
    public static final String LBL_ALLOW_IMAGE_EXTENSION = "ALLOW_IMAGE_EXTENSION";

    /** The constants. LBL_OWNER_NAME. */
    public static final String LBL_OWNER_NAME = "OWNER_NAME";
    // End : [IN063] for Digital Signature.
    
    /** The constant LBL_SPS_DO_NO. */
    public static final String LBL_SPS_DO_NO = "SPS_DO_NO";
    
    /** The constant LBL_SUPPLIER_COMPANY_CODE. */
    public static final String LBL_SUPPLIER_COMPANY_CODE = "SUPPLIER_COMPANY_CODE";
    
    /** The constant LBL_SUPPLIER_COMPANY_NAME. */
    public static final String LBL_SUPPLIER_COMPANY_NAME = "SUPPLIER_COMPANY_NAME";
    
    /** The constant LBL_SUPPLIER_PLANT_CODE. */
    public static final String LBL_SUPPLIER_PLANT_CODE = "SUPPLIER_PLANT_CODE";
    
    /** The constant LBL_SUPPLIER_USER. */
    public static final String LBL_SUPPLIER_USER = "SUPPLIER_USER";
    
    /** The constant LBL_SUPPLIER_USER. */
    public static final String LBL_SUPPLIER_INFO = "SUPPLIER_INFORMATION";
    
    /** The constant LBL_SUPPLIER_INVOICE_AMOUNT. */
    public static final String LBL_SUPPLIER_INVOICE_AMOUNT = "SUPPLIER_INVOICE_AMOUNT";
    
    /** The constant LBL_SUPPLIER_UNIT_PRICE. */
    public static final String LBL_SUPPLIER_UNIT_PRICE = "SUPPLIER_UNIT_PRICE";
    
    /** The constant LBL_S_PCD_NOT_SAME. */
    public static final String LBL_S_PCD_NOT_SAME = "S_PCD_NOT_SAME";
    
    /** The constant LBL_S_CD_NOT_SAME. */
    public static final String LBL_S_CD_NOT_SAME = "S_CD_NOT_SAME";
    
    /** The constant LBL_S_TAX_ID_NOT_SAME. */
    public static final String LBL_S_TAX_ID_NOT_SAME  = "S_TAX_ID_NOT_SAME";
    
    /** The constant LBL_INVOICE_NO_NOT_SAME. */
    public static final String LBL_INVOICE_NO_NOT_SAME  = "INVOICE_NO_NOT_SAME";
    
    /** The constant LBL_INVOICE_DATE_NOT_SAMEE. */
    public static final String LBL_INVOICE_DATE_NOT_SAME  = "INVOICE_DATE_NOT_SAME";
    
    /** The constant LBL_SHIPPING_QTY_NOT_EQUAL. */
    public static final String LBL_SHIPPING_QTY_NOT_EQUAL  = "SHIPPING_QTY_NOT_EQUAL";
    
    /** The constant LBL_TM. */
    public static final String LBL_TM = "TM";

    /** The constant LBL_TRANS_NOT_SAME. */
    public static final String LBL_TRANS_NOT_SAME = "TRANS_NOT_SAME";
    
    /** The constant LBL_UPDATE_ALLOW_REVISE_FLAG. */
    public static final String LBL_UPDATE_ALLOW_REVISE_FLAG = "UPDATE_ALLOW_REVISE_FLAG";
    
    /** The constant LBL_UPLOAD_CANNOT_INSERT_IN_TMP_TABLE. */
    public static final String LBL_UPLOAD_CANNOT_INSERT_IN_TMP_TABLE
        = "UPLOAD_CANNOT_INSERT_IN_TMP_TABLE";
    
    /** The constant LBL_UPLOAD_SAME_SPSDONO_DPN. */
    public static final String LBL_UPLOAD_SAME_SPSDONO_DPN = "UPLOAD_SAME_SPSDONO_DPN";
    
    /** The constant LBL_UPLOAD_FILE. */
    public static final String LBL_UPLOAD_FILE = "UPLOAD_FILE";
    
    /** The constant LBL_VIEW_PDF. */
    public static final String LBL_VIEW_PDF = "VIEW_PDF";
    
    /** The constant LBL_ORDER_METHOD_NOT_SAME. */
    public static final String LBL_ORDER_METHOD_NOT_SAME = "ORDER_METHOD_NOT_SAME";
    
    /** The constant LBL_ORDER_METHOD_NOT_SAME_WARNING. */
    public static final String LBL_ORDER_METHOD_NOT_SAME_WARNING = "ORDER_METHOD_NOT_SAME_WARNING";
    
    /** The constant LBL_SHIP_STATUS_INVALID. */
    public static final String LBL_SHIP_STATUS_INVALID = "SHIP_STATUS_INVALID";
    
    /** The constant LBL_ALL_CRITERIA. */
    public static final String LBL_ALL_CRITERIA = "ALL_CRITERIA";
    
    /** The constant LBL_ASN_NO. */
    public static final String LBL_ASN_NO = "ASN_NO";
    
    /** The constant LBL_PLEASE_SELECT_CRITERIA. */
    public static final String LBL_PLEASE_SELECT_CRITERIA = "PLEASE_SELECT_CRITERIA";
    
    /** The constant LBL_DSC_ID. */
    public static final String LBL_DSC_ID = "DSC_ID";
    
    /** The constant LBL_D_PART_NO. */
    public static final String LBL_D_PART_NO = "D_PART_NO";
    
    /** The constant LBL_FIRST_NAME. */
    public static final String LBL_FIRST_NAME = "FIRST_NAME";
    
    /** The constant LBL_MIDDLE_NAME. */
    public static final String LBL_MIDDLE_NAME = "MIDDLE_NAME";
    
    /** The constant LBL_LAST_NAME. */
    public static final String LBL_LAST_NAME = "LAST_NAME";
    
    /** The constant LBL_EMAIL. */
    public static final String LBL_EMAIL = "EMAIL";
    
    /** The constant LBL_TELEPHONE. */
    public static final String LBL_TELEPHONE = "TELEPHONE";
    
    /** The constant LBL_REGISTER_DATE_FROM. */
    public static final String LBL_REGISTER_DATE_FROM = "REGISTER_DATE_FROM";
    
    /** The constant LBL_REGISTER_DATE_TO. */
    public static final String LBL_REGISTER_DATE_TO = "REGISTER_DATE_TO";
    
    /** The constant LBL_REGISTER_DENSO_SUPPLIER_RELATE. */
    public static final String LBL_REGISTER_DENSO_SUPPLIER_RELATE =
        "REGISTER_DENSO_SUPPLIER_RELATE";
    
    /** The constant LBL_REGISTER_DENSO_SUPPLIER_PART. */
    public static final String LBL_REGISTER_DENSO_SUPPLIER_PART =
        "REGISTER_DENSO_SUPPLIER_PART";
    
    /** The constant LBL_REGISTER_USER_INFO. */
    public static final String LBL_REGISTER_USER_INFO = "REGISTER_USER_INFO";
    
    /** The constant LBL_REGISTER_SUPPLIER_USER_INFO. */
    public static final String LBL_REGISTER_SUPPLIER_USER_INFO = "REGISTER_SUPPLIER_USER_INFO";
    
    /** The constant LBL_REGISTER_DENSO_USER_INFO. */
    public static final String LBL_REGISTER_DENSO_USER_INFO = "REGISTER_DENSO_USER_INFO";
    
    /** The constant LBL_REGISTER_USER_ROLE. */
    public static final String LBL_REGISTER_USER_ROLE = "REGISTER_USER_ROLE";
    
    /** The constant LBL_DELETE. */
    public static final String LBL_DELETE = "DELETE";
    
    /** The constant LBL_DELETE_SELECTED_ITEMS. */
    public static final String LBL_DELETE_SELECTED_ITEMS = "DELETE_SELECTED_ITEMS";
    
    /** The constant DELETE_USER_INFO. */
    public static final String LBL_DELETE_USER_INFO = "DELETE_USER_INFO";
    
    /** The constant DELETE_USER_INFO. */
    public static final String LBL_DELETE_USER_ROLE = "DELETE_USER_ROLE";
    
    /** The constant LBL_DEPARTMENT_NAME. */
    public static final String LBL_DEPARTMENT_NAME = "DEPARTMENT_NAME";
    
    /** The constant LBL_USER. */
    public static final String LBL_USER = "USER";
    
    /** The constant LBL_S_PART_NO. */
    public static final String LBL_S_PART_NO = "S_PART_NO";
    
    /** The constant LBL_SUPPLIER_COMPANY. */
    public static final String LBL_SUPPLIER_COMPANY = "SUPPLIER_COMPANY";
    
    /** The constant LBL_SUPPLIER_PLANT. */
    public static final String LBL_SUPPLIER_PLANT = "SUPPLIER_PLANT";
    
    /** The constant LBL_UPDATE_ASN. */
    public static final String LBL_UPDATE_ASN = "UPDATE_ASN";
    
    // [IN071] 
    /** The constant LBL_UPDATE_DO. */
    public static final String LBL_UPDATE_DO = "UPDATE_DO";
    
    /** The constant LBL_UPLOAD_DATE_FORM. */
    public static final String LBL_UPLOAD_DATE_FROM = "UPLOAD_DATE_FROM";
    
    /** The constant LBL_UPLOAD_DATE_TO. */
    public static final String LBL_UPLOAD_DATE_TO = "UPLOAD_DATE_TO";
    
    /** The constant LBL_UPLOAD_RESULT. */
    public static final String LBL_UPLOAD_RESULT = "UPLOAD_RESULT";
    
    /** The constant LBL_UPDATE_DENSO_USER_INFO. */
    public static final String LBL_UPDATE_DENSO_USER_INFO = "UPDATE_DENSO_USER_INFO";
    
    /** The constant LBL_UPDATE_SUPPLIER_USER. */
    public static final String LBL_UPDATE_USER_INFO = "UPDATE_USER_INFO";
    
    /** The constant LBL_UPDATE_SUPPLIER_USER_INFO. */
    public static final String LBL_UPDATE_SUPPLIER_USER_INFO = "UPDATE_SUPPLIER_USER_INFO";
    
    /** The constant LBL_UPDATE_MOVE_TO_ACTUAL_FLAG. */
    public static final String LBL_UPDATE_MOVE_TO_ACTUAL_FLAG = "UPDATE_MOVE_TO_ACTUAL_FLAG";
    
    /** The constant LBL_UPDATE_DENSO_SUPPLIER_RELATE. */
    public static final String LBL_UPDATE_DENSO_SUPPLIER_RELATE = "UPDATE_DENSO_SUPPLIER_RELATE";
    
    /** The constant LBL_UPDATE_DENSO_SUPPLIER_PART. */
    public static final String LBL_UPDATE_DENSO_SUPPLIER_PART = "UPDATE_DENSO_SUPPLIER_PART";
    
    /** The constant LBL_DELETE_DENSO_SUPPLIER_PART. */
    public static final String LBL_DELETE_DENSO_SUPPLIER_PART = "DELETE_DENSO_SUPPLIER_PART";
    
    /** The constant LBL_DENSO_OWNER_COMPANY. */
    public static final String LBL_DENSO_OWNER_COMPANY = "DENSO_OWNER_COMPANY";
    
    /** The constant LBL_DENSO_INVOICE_AMOUNT. */
    public static final String LBL_DENSO_INVOICE_AMOUNT = "DENSO_INVOICE_AMOUNT";
    
    /** The constant LBL_DENSO_UNIT_PRICE. */
    public static final String LBL_DENSO_UNIT_PRICE = "DENSO_UNIT_PRICE";

    /** The constant LBL_SPS_PO_NO. */
    public static final String LBL_SPS_PO_NO = "SPS_PO_NO";

    /** The constant LBL_CIGMA_PO_NO. */
    public static final String LBL_CIGMA_PO_NO = "CIGMA_PO_NO";

    /** The constant LBL_S_CD. */
    public static final String LBL_S_CD = "S_CD";

    /** The constant LBL_S_PCD. */
    public static final String LBL_S_PCD = "S_PCD";

    /** The constant LBL_D_CD. */
    public static final String LBL_D_CD = "D_CD";

    /** The constant LBL_D_PCD. */
    public static final String LBL_D_PCD = "D_PCD";
    
    /** The constant LBL_ACTUAL_ETD_DATE_FROM. */
    public static final String LBL_ACTUAL_ETD_DATE_FROM = "ACTUAL_ETD_FROM";
    
    /** The constant LBL_ACTUAL_ETD_DATE_TO. */
    public static final String LBL_ACTUAL_ETD_DATE_TO = "ACTUAL_ETD_TO";
    
    /** The constant LBL_ACTUAL_ETA_DATE_FROM. */
    public static final String LBL_ACTUAL_ETA_DATE_FROM = "ACTUAL_ETA_FROM";
    
    /** The constant LBL_ACTUAL_ETA_DATE_TO. */
    public static final String LBL_ACTUAL_ETA_DATE_TO = "ACTUAL_ETA_TO";
    
    /** The constant LBL_INVOICE_DATE_FROM. */
    public static final String LBL_INVOICE_DATE_FROM = "INVOICE_DATE_FROM";
    
    /** The constant LBL_INVOICE_DATE_TO. */
    public static final String LBL_INVOICE_DATE_TO = "INVOICE_DATE_TO";
    
    /** The constant LBL_TEMPORARY_MODE. */
    public static final String LBL_TEMPORARY_MODE = "TEMPORARY_MODE";
    
    /** The constant LBL_TEMP_PRICE. */
    public static final String LBL_TEMP_PRICE = "TEMP_PRICE";
    
    /** The constant LBL_REASON. */
    public static final String LBL_REASON = "REASON";
    
    /** The constant LBL_SUPPLIER_PART_NO. */
    public static final String LBL_SUPPLIER_PART_NO = "SUPPLIER_PART_NO";
    
    /** The constant LBL_SHIPPING_QTY. */
    public static final String LBL_SHIPPING_QTY = "SHIPPING_QTY";
    
    // [IN039] : Add label for Shipping Box Qty
    /** The constant LBL_SHIPPING_BOX_QTY. */
    public static final String LBL_SHIPPING_BOX_QTY = "SHIPPING_BOX_QTY";
    
    /** The constant LBL_RECEIVED_QTY. */
    public static final String LBL_RECEIVED_QTY = "RECEIVED_QTY";
    
    /** The constant LBL_ACTUAL_ETA. */
    public static final String LBL_ACTUAL_ETA = "ACTUAL_ETA";
    
    /** The constant LBL_SHIPQTY_GREATER_REMAINQTY. */
    public static final String LBL_SHIPQTY_GREATER_REMAINQTY = "SHIPQTY_GREATER_REMAINQTY";
    
    /** The constant LBL_TRIP_NO. */
    public static final String LBL_TRIP_NO = "TRIP_NO";
    
    /** The constant LBL_CHANGE_REASON. */
    public static final String LBL_CHANGE_REASON = "CHANGE_REASON";
    
    /** The constant LBL_USER_ROLE. */
    public static final String LBL_USER_ROLE = "USER_ROLE";
    
    /** The constant LBL_PO_NO. */
    public static final String LBL_PO_NO = "PO_NO";
    
    /** The constant LBL_ROLE. */
    public static final String LBL_ROLE = "ROLE";
    
    /** The constant LBL_UPDATE_USER_ROLE_ACTIVE_STATUS. */
    public static final String LBL_UPDATE_USER_ROLE_ACTIVE_STATUS = "UPDATE_USER_ROLE_ACTIVE_STATUS";
    
    /** The constant LBL_ASSIGN_NEW_USER_ROLE. */
    public static final String LBL_ASSIGN_NEW_USER_ROLE = "ASSIGN_NEW_USER_ROLE";
    
    /** The constant LBL_EFFECTIVE_START. */
    public static final String LBL_EFFECTIVE_START = "EFFECTIVE_START";
    
    /** The constant LBL_EFFECTIVE_END. */
    public static final String LBL_EFFECTIVE_END = "EFFECTIVE_END";
    
    /** The constant LBL_EMPLOYEE_CODE. */
    public static final String LBL_EMPLOYEE_CODE = "EMPLOYEE_CODE";
    
    /** The constant LBL_UPDATE_USER_ROLE. */
    public static final String LBL_UPDATE_USER_ROLE = "UPDATE_USER_ROLE";

    /** The constant LBL_NOTIFICATION_PENDING_PO. */
    public static final String LBL_NOTIFICATION_PENDING_PO = "NOTIFICATION_PENDING_PO";
    
    /** The constant LBL_SESSION_ID. */
    public static final String LBL_SESSION_ID = "SESSION_ID";
    
    /** The constant LBL_SUPPLIER_CODE. */
    public static final String LBL_SUPPLIER_CODE  = "SUPPLIER_CODE";
    
    /** The constant LBL_SUPPLIER_TAX_ID. */
    public static final String LBL_SUPPLIER_TAX_ID  = "SUPPLIER_TAX_ID";
    
    /** The constant LBL_DENSO_CODE. */
    public static final String LBL_DENSO_CODE  = "DENSO_CODE";
    
    /** The constant LBL_DENSO_COMPANY. */
    public static final String LBL_DENSO_COMPANY  = "DENSO_COMPANY";
    
    /** The constant LBL_INVOICE_ID. */
    public static final String LBL_INVOICE_ID  = "INVOICE_ID";
    
    /** The constant LBL_INVOICE_DATE. */
    public static final String LBL_INVOICE_DATE  = "INVOICE_DATE";
    
    /** The constant LBL_CN_NO. */
    public static final String LBL_CN_NO  = "CN_NO";
    
    /** The constant LBL_CN_DATE. */
    public static final String LBL_CN_DATE  = "CN_DATE";
    
    /** The constant LBL_INVOICE_BASE_AMOUNT. */
    public static final String LBL_INVOICE_BASE_AMOUNT  = "INVOICE_BASE_AMOUNT";
    
    /** The constant LBL_INVOICE_BASE_AMOUNT_INVOICE_VAT_GST_AMOUNT. */
    public static final String LBL_INVOICE_BASE_AMOUNT_INVOICE_VAT_GST_AMOUNT
        = "INVOICE_BASE_AMOUNT_INVOICE_VAT_GST_AMOUNT";
    
    /** The constant LBL_INVOICE_VAT_GST_AMOUNT. */
    public static final String LBL_INVOICE_VAT_GST_AMOUNT  = "INVOICE_VAT_GST_AMOUNT";
    
    /** The constant LBL_VAT_GST_RAET. */
    public static final String LBL_VAT_GST_RAET  = "VAT_GST_RAET";
    
    /** The constant LBL_VAT. */
    public static final String LBL_VAT  = "VAT";
    
    /** The constant LBL_CREDIT_NOTE_BASE_AMOUNT. */
    public static final String LBL_CREDIT_NOTE_BASE_AMOUNT  = "CREDIT_NOTE_BASE_AMOUNT";
    
    /** The constant LBL_CREDIT_NOTE_BASE_AMOUNT_CREDIT_NOTE_VAT_GST_AMOUNT. */
    public static final String LBL_CREDIT_NOTE_BASE_AMOUNT_CREDIT_NOTE_VAT_GST_AMOUNT
        = "CREDIT_NOTE_BASE_AMOUNT_CREDIT_NOTE_VAT_GST_AMOUNT";
    
    /** The constant LBL_CN_AMOUNT_NEGATIVE_VALUE. */
    public static final String LBL_CN_AMOUNT_NEGATIVE_VALUE = "CN_AMOUNT_NEGATIVE_VALUE";
    
    /** The constant LBL_PARTS_IN_INVOICE_NOT_SAME_UNIT_PRICE. */
    public static final String LBL_PARTS_IN_INVOICE_NOT_SAME_UNIT_PRICE
        = "PARTS_IN_INVOICE_NOT_SAME_UNIT_PRICE";
    
    /** The constant LBL_CREDIT_NOTE_VAT_GST_AMOUNT. */
    public static final String LBL_CREDIT_NOTE_VAT_GST_AMOUNT = "CREDIT_NOTE_VAT_GST_AMOUNT";
    
    /** The constant LBL_CREDIT_NOTE_TOTAL_AMOUNT. */
    public static final String LBL_CREDIT_NOTE_TOTAL_AMOUNT = "CREDIT_NOTE_TOTAL_AMOUNT";
    
    /** The constant LBL_CREDIT_NOTE_NO. */
    public static final String LBL_CREDIT_NOTE_NO = "CREDIT_NOTE_NO";
    
    /** The constant LBL_CREDIT_NOTE_DATE. */
    public static final String LBL_CREDIT_NOTE_DATE  = "CREDIT_NOTE_DATE";
    
    /** The constant LBL_CREDIT_NOTE_VAT_AMOUNT. */
    public static final String LBL_CREDIT_NOTE_VAT_AMOUNT  = "CREDIT_NOTE_VAT_AMOUNT";
    
    /** The constant LBL_CREDIT_NOTE_AMOUNT. */
    public static final String LBL_CREDIT_NOTE_AMOUNT  = "CREDIT_NOTE_AMOUNT";
    
    /** The constant LBL_COVER_PAGE_NO. */
    public static final String LBL_COVER_PAGE_NO = "COVER_PAGE_NO";
    
    /** The constant LBL_TOTAL_BASE_AMOUNT. */
    public static final String LBL_TOTAL_BASE_AMOUNT = "TOTAL_BASE_AMOUNT";
    
    /** The constant LBL_TOTAL_VAT_GST_AMOUNT. */
    public static final String LBL_TOTAL_VAT_GST_AMOUNT = "TOTAL_VAT_GST_AMOUNT";
    
    /** The constant LBL_INVOICE_TOTAL_AMOUNT. */
    public static final String LBL_INVOICE_TOTAL_AMOUNT = "INVOICE_TOTAL_AMOUNT";
    
    /** The constant LBL_TOTAL_AMOUNT. */
    public static final String LBL_TOTAL_AMOUNT = "TOTAL_AMOUNT";
    
    /** The constant LBL_TOTAL_DIFFERENCE_VAT_GST_AMOUNT. */
    public static final String LBL_TOTAL_DIFFERENCE_VAT_GST_AMOUNT 
        = "TOTAL_DIFFERENCE_VAT_GST_AMOUNT";
    
    /** The constant LBL_TOTAL_DIFFERENCE_BASE_AMOUNT. */
    public static final String LBL_TOTAL_DIFFERENCE_BASE_AMOUNT = "TOTAL_DIFFERENCE_BASE_AMOUNT";
    
    /** The constant LBL_TOTAL_DIFFERENCE_AMOUNT. */
    public static final String LBL_TOTAL_DIFFERENCE_AMOUNT = "TOTAL_DIFFERENCE_AMOUNT";
    
    /** The constant LBL_TOTAL_BASE_AMOUNT_TOTAL_DIFFERENCE_BASE_AMOUNT. */
    public static final String LBL_TOTAL_BASE_AMOUNT_TOTAL_DIFFERENCE_BASE_AMOUNT
        = "TOTAL_BASE_AMOUNT_TOTAL_DIFFERENCE_BASE_AMOUNT";
    
    /** The constant LBL_TOTAL_VAT_GST_AMOUNT_TOTAL_DIFFERENCE_VAT_GST_AMOUNT. */
    public static final String LBL_TOTAL_VAT_GST_AMOUNT_TOTAL_DIFFERENCE_VAT_GST_AMOUNT 
        = "TOTAL_VAT_GST_AMOUNT_TOTAL_DIFFERENCE_VAT_GST_AMOUNT";
    
    /** The constant LBL_TOTAL_AMOUNT_TOTAL_DIFFERENCE_AMOUNT. */
    public static final String LBL_TOTAL_AMOUNT_TOTAL_DIFFERENCE_AMOUNT 
        = "TOTAL_AMOUNT_TOTAL_DIFFERENCE_AMOUNT";
    
    /** The constant LBL_CORRECT_RECORD. */
    public static final String LBL_CORRECT_RECORD = "CORRECT_RECORD"; 
    
    /** The constant LBL_ERROR_RECORD. */
    public static final String LBL_ERROR_RECORD = "ERROR_RECORD";
    
    /** The constant LBL_WARNING_RECORD. */
    public static final String LBL_WARNING_RECORD = "WARNING_RECORD";
    
    /** The constant LBL_TOTAL_RECORD. */
    public static final String LBL_TOTAL_RECORD = "TOTAL_RECORD";
    
    /** The constant LBL_RECEIVING_LANE. */
    public static final String LBL_RECEIVING_LANE = "RECEIVING_LANE";
    
    /** The constant LBL_REVISION_OF_DO. */
    public static final String LBL_REVISION_OF_DO = "REVISION_OF_DO";
    
    /** The constant LBL_S_UM. */
    public static final String LBL_S_UM  = "S_UM";
    
    /** The constant LBL_SEND_ASN_DATA_TO_CIGMA. */
    public static final String LBL_SEND_ASN_DATA_TO_CIGMA = "SEND_ASN_DATA_TO_CIGMA";
    
    /** The constant LBL_DATA_TYPE. */
    public static final String LBL_DATA_TYPE = "DATA_TYPE";
    
    /** The constant LBL_ISSUE_DATE. */
    public static final String LBL_ISSUE_DATE = "ISSUE_DATE";
    
    /** The constant LBL_REPORT_TYPE. */
    public static final String LBL_REPORT_TYPE = "REPORT_TYPE";
    
    /** The constant LBL_REGISTER_DATE. */
    public static final String LBL_REGISTER_DATE = "REGISTER_DATE";
    
    /** The constant LBL_EFFECTIVE_DATE. */
    public static final String LBL_EFFECTIVE_DATE = "EFFECTIVE_DATE";
    
    /** The constant LBL_CURRENT_SPS_PO_NO. */
    public static final String LBL_CURRENT_SPS_PO_NO = "CURRENT_SPS_PO_NO";
    
    /** The constant LBL_RELEASE_NO. */
    public static final String LBL_RELEASE_NO = "RELEASE_NO";
    
    /** The constant LBL_D_PN. */
    public static final String LBL_D_PN = "D_PN";
    
    /** The constant LBL_S_PN. */
    public static final String LBL_S_PN = "S_PN";
    
    /** The constant LBL_S_P. */
    public static final String LBL_S_P = "S_P";
    
    /** The constant LBL_DUE_DATE. */
    public static final String LBL_DUE_DATE = "DUE_DATE";
    
    /** The constant LBL_ETD. */
    public static final String LBL_ETD = "ETD";
    
    /** The constant LBL_ORDER_QTY. */
    public static final String LBL_ORDER_QTY = "ORDER_QTY";
    
    /** The constant LBL_ORDER_LOT. */
    public static final String LBL_ORDER_LOT = "ORDER_LOT";
    
    /** The constant LBL_UNIT_OF_MEASURE. */
    public static final String LBL_UNIT_OF_MEASURE = "UNIT_OF_MEASURE";
    
    /** The constant LBL_UNIT_PRICE. */
    public static final String LBL_UNIT_PRICE = "UNIT_PRICE";
    
    /** The constant LBL_S_PRICE_UNIT. */
    public static final String LBL_S_PRICE_UNIT = "S_PRICE_UNIT";
    
    /** The constant LBL_D_PRICE_UNIT. */
    public static final String LBL_D_PRICE_UNIT = "D_PRICE_UNIT";
    
    /** The constant LBL_DIFFERENCE_PRICE_UNIT. */
    public static final String LBL_DIFFERENCE_PRICE_UNIT = "DIFFERENCE_PRICE_UNIT";
    
    /** The constant LBL_CURRENCY_CODE. */
    public static final String LBL_CURRENCY_CODE = "CURRENCY_CODE";
    
    /** The constant LBL_MODEL. */
    public static final String LBL_MODEL = "MODEL";
    
    /** The constant LBL_PLANNER_CODE. */
    public static final String LBL_PLANNER_CODE = "PLANNER_CODE";
    
    /** The constant LBL_PHASE_CODE. */
    public static final String LBL_PHASE_CODE = "PHASE_CODE";
    
    /** The constant LBL_WH_PRIME_RECEIVING. */
    public static final String LBL_WH_PRIME_RECEIVING = "WH_PRIME_RECEIVING";
    
    /** The constant LBL_RECEIVING_DOCK. */
    public static final String LBL_RECEIVING_DOCK = "RECEIVING_DOCK";
    
    /** The constant LBL_RECEIVED_DATE. */
    public static final String LBL_RECEIVED_DATE = "RECEIVED_DATE";
    
    /** The constant LBL_PN_STATUS. */
    public static final String LBL_PN_STATUS = "PN_STATUS";
    
    /** The constant LBL_PREVIOUS_SPS_PO_No. */
    public static final String LBL_PREVIOUS_SPS_PO_NO = "PREVIOUS_SPS_PO_NO";
    
    /** The constant LBL_ORIGINAL_SPS_PO_NO. */
    public static final String LBL_ORIGINAL_SPS_PO_NO = "ORIGINAL_SPS_PO_NO";
    
    /** The constant LBL_PREVIOUS_QTY. */
    public static final String LBL_PREVIOUS_QTY = "PREVIOUS_QTY";
    
    /** The constant LBL_DIFFERENCE_QTY. */
    public static final String LBL_DIFFERENCE_QTY = "DIFFERENCE_QTY";
    
    /** The constant LBL_DIFFERENT_QTY. */
    public static final String LBL_DIFFERENT_QTY = "DIFFERENT_QTY";
    
    /** The constant LBL_RSN. */
    public static final String LBL_RSN = "RSN";
    
    /** The constant LBL_REGISTER. */
    public static final String LBL_REGISTER = "REGISTER";
    
    /** The constant LBL_DENSO_TAX_ID. */
    public static final String LBL_DENSO_TAX_ID = "DENSO_TAX_ID";
    
    /** The constant LBL_SUPPLIER_VAT_TYPE. */
    public static final String LBL_SUPPLIER_VAT_TYPE = "SUPPLIER_VAT_TYPE";
    
    /** The constant LBL_SUPPLIER_VAT_RATE. */
    public static final String LBL_SUPPLIER_VAT_RATE = "SUPPLIER_VAT_RATE";
    
    /** The constant LBL_SUPPLIER_CURRENCY. */
    public static final String LBL_SUPPLIER_CURRENCY = "SUPPLIER_CURRENCY";
    
    /** The constant LBL_SUPPLIER_BASE_AMOUNT. */
    public static final String LBL_SUPPLIER_BASE_AMOUNT = "SUPPLIER_BASE_AMOUNT";
    
    /** The constant LBL_SUPPLIER_VAT_AMOUNT. */
    public static final String LBL_SUPPLIER_VAT_AMOUNT = "SUPPLIER_VAT_AMOUNT";
    
    /** The constant LBL_SUPPLIER_TOTAL_AMOUNT. */
    public static final String LBL_SUPPLIER_TOTAL_AMOUNT = "SUPPLIER_TOTAL_AMOUNT";
    
    /** The constant LBL_SUPPLIER_CN_BASE_AMOUNT. */
    public static final String LBL_SUPPLIER_CN_BASE_AMOUNT = "SUPPLIER_CN_BASE_AMOUNT";
    
    /** The constant LBL_SUPPLIER_CN_VAT_AMOUNT. */
    public static final String LBL_SUPPLIER_CN_VAT_AMOUNT = "SUPPLIER_CN_VAT_AMOUNT";
    
    /** The constant LBL_SUPPLIER_CN_TOTAL_AMOUNT. */
    public static final String LBL_SUPPLIER_CN_TOTAL_AMOUNT = "SUPPLIER_CN_TOTAL_AMOUNT";
    
    /** The constant LBL_DIFFERENCE_TOTAL_BASE_AMOUNT. */
    public static final String LBL_DIFFERENCE_TOTAL_BASE_AMOUNT = "DIFFERENCE_TOTAL_BASE_AMOUNT";
    
    /** The constant LBL_DIFFERENCE_TOTAL_VAT_AMOUNT. */
    public static final String LBL_DIFFERENCE_TOTAL_VAT_AMOUNT = "DIFFERENCE_TOTAL_VAT_AMOUNT";
    
    /** The constant LBL_DIFFERENCE_TOTAL_AMOUNT. */
    public static final String LBL_DIFFERENCE_TOTAL_AMOUNT = "DIFFERENCE_TOTAL_AMOUNT";
    
    /** The constant LBL_DIFFERENCE_VAT_GST_AMOUNT. */
    public static final String LBL_DIFFERENCE_VAT_GST_AMOUNT = "DIFFERENCE_VAT_GST_AMOUNT";
    
    /** The constant LBL_VAT_GST_AMOUNT. */
    public static final String LBL_VAT_GST_AMOUNT = "VAT_GST_AMOUNT";
    
    /** The constant LBL_DENSO_PRICE_UNIT. */
    public static final String LBL_DENSO_PRICE_UNIT = "DENSO_PRICE_UNIT";
    
    /** The constant LBL_SUPPLIER_PRICE_UNIT. */
    public static final String LBL_SUPPLIER_PRICE_UNIT = "SUPPLIER_PRICE_UNIT";
    
    /** The constant LBL_DENSO_BASE_AMOUNT. */
    public static final String LBL_DENSO_BASE_AMOUNT = "DENSO_BASE_AMOUNT";
    
    /** The constant LBL_DENSO_VAT_AMOUNT. */
    public static final String LBL_DENSO_VAT_AMOUNT = "DENSO_VAT_AMOUNT";
    
    /** The constant LBL_DENSO_TOTAL_AMOUNT. */
    public static final String LBL_DENSO_TOTAL_AMOUNT = "DENSO_TOTAL_AMOUNT";
    
    /** The constant LBL_DIFFERENCE_BASE_AMOUNT. */
    public static final String LBL_DIFFERENCE_BASE_AMOUNT = "DIFFERENCE_BASE_AMOUNT";
    
    /** The constant LBL_DIFFERENCE_BASE_AMOUNT_BY_PN. */
    public static final String LBL_DIFFERENCE_BASE_AMOUNT_BY_PN = "DIFFERENCE_BASE_AMOUNT_BY_PN";

    /** The constant LBL_DO_NO. */
    public static final String LBL_DO_NO = "D/O_NO";
    
    /** The constant LBL_VENDOR_CD. */
    public static final String LBL_VENDOR_CD = "VENDOR_CD";
    
    /** The constant LBL_RSHP001_REPORT_NAME. */
    public static final String LBL_RSHP001_REPORT_NAME = "RSHP001_REPORT_NAME";

    /** The constant LBL_RSHP001_ASN_NO. */
    public static final String LBL_RSHP001_ASN_NO = "RSHP001_ASN_NO";

    /** The constant LBL_RSHP001_NO. */
    public static final String LBL_RSHP001_NO = "RSHP001_NO";

    /** The constant LBL_RSHP001_CODE. */
    public static final String LBL_RSHP001_CODE = "RSHP001_CODE";

    /** The constant LBL_RSHP001_ROUTE. */
    public static final String LBL_RSHP001_ROUTE = "RSHP001_ROUTE";

    /** The constant LBL_RSHP001_DEL. */
    public static final String LBL_RSHP001_DEL = "RSHP001_DEL";

    /** The constant LBL_RSHP001_SUPPLIER. */
    public static final String LBL_RSHP001_SUPPLIER = "RSHP001_SUPPLIER";

    /** The constant LBL_RSHP001_SUP_NAME. */
    public static final String LBL_RSHP001_SUP_NAME = "RSHP001_SUP_NAME";

    /** The constant LBL_RSHP001_SUP_LOC. */
    public static final String LBL_RSHP001_SUP_LOC = "RSHP001_SUP_LOC";

    /** The constant LBL_RSHP001_SUP_PLANT. */
    public static final String LBL_RSHP001_SUP_PLANT = "RSHP001_SUP_PLANT";

    /** The constant LBL_RSHP001_PAGE. */
    public static final String LBL_RSHP001_PAGE = "RSHP001_PAGE";

    /** The constant LBL_RSHP001_G_PAGE. */
    public static final String LBL_RSHP001_G_PAGE = "RSHP001_G_PAGE";

    /** The constant LBL_RSHP001_A_ETD. */
    public static final String LBL_RSHP001_A_ETD = "RSHP001_A_ETD";

    /** The constant LBL_RSHP001_SCHEDULE. */
    public static final String LBL_RSHP001_SCHEDULE = "RSHP001_SCHEDULE";

    /** The constant LBL_RSHP001_TIME. */
    public static final String LBL_RSHP001_TIME = "RSHP001_TIME";

    /** The constant LBL_RSHP001_P_ETA. */
    public static final String LBL_RSHP001_P_ETA = "RSHP001_P_ETA";

    /** The constant LBL_RSHP001_PLANT. */
    public static final String LBL_RSHP001_PLANT = "RSHP001_PLANT";

    /** The constant LBL_RSHP001_WARE. */
    public static final String LBL_RSHP001_WARE = "RSHP001_WARE";

    /** The constant LBL_RSHP001_HOUSE. */
    public static final String LBL_RSHP001_HOUSE = "RSHP001_HOUSE";

    /** The constant LBL_RSHP001_DOCK. */
    public static final String LBL_RSHP001_DOCK = "RSHP001_DOCK";

    /** The constant LBL_RSHP001_RCV. */
    public static final String LBL_RSHP001_RCV = "RSHP001_RCV";

    /** The constant LBL_RSHP001_LANE. */
    public static final String LBL_RSHP001_LANE = "RSHP001_LANE";

    /** The constant LBL_RSHP001_TRANS. */
    public static final String LBL_RSHP001_TRANS = "RSHP001_TRANS";

    /** The constant LBL_RSHP001_CYCLE. */
    public static final String LBL_RSHP001_CYCLE = "RSHP001_CYCLE";

    /** The constant LBL_RSHP001_TRIP_NO. */
    public static final String LBL_RSHP001_TRIP_NO = "RSHP001_TRIP_NO";

    /** The constant LBL_RSHP001_ISSUE. */
    public static final String LBL_RSHP001_ISSUE = "RSHP001_ISSUE";

    /** The constant LBL_RSHP001_DATE. */
    public static final String LBL_RSHP001_DATE = "RSHP001_DATE";

    /** The constant LBL_RSHP001_LAST. */
    public static final String LBL_RSHP001_LAST = "RSHP001_LAST";

    /** The constant LBL_RSHP001_MODIFIED. */
    public static final String LBL_RSHP001_MODIFIED = "RSHP001_MODIFIED";

    /** The constant LBL_RSHP001_CTRL. */
    public static final String LBL_RSHP001_CTRL = "RSHP001_CTRL";

    /** The constant LBL_RSHP001_F. */
    public static final String LBL_RSHP001_F = "RSHP001_F";

    /** The constant LBL_RSHP001_D_PART_NUMBER. */
    public static final String LBL_RSHP001_D_PART_NUMBER = "RSHP001_D_PART_NUMBER";

    /** The constant LBL_RSHP001_S_PART_NUMBER. */
    public static final String LBL_RSHP001_S_PART_NUMBER = "RSHP001_S_PART_NUMBER";

    /** The constant LBL_RSHP001_DESCRIPTION. */
    public static final String LBL_RSHP001_DESCRIPTION = "RSHP001_DESCRIPTION";

    /** The constant LBL_RSHP001_UM. */
    public static final String LBL_RSHP001_UM = "RSHP001_UM";

    /** The constant LBL_RSHP001_ORDER. */
    public static final String LBL_RSHP001_ORDER = "RSHP001_ORDER";

    /** The constant LBL_RSHP001_METHOD. */
    public static final String LBL_RSHP001_METHOD = "RSHP001_METHOD";

    /** The constant LBL_RSHP001_QUANTITY. */
    public static final String LBL_RSHP001_QUANTITY = "RSHP001_QUANTITY";

    /** The constant LBL_RSHP001_BOX. */
    public static final String LBL_RSHP001_BOX = "RSHP001_BOX";

    /** The constant LBL_RSHP001_NO_OF. */
    public static final String LBL_RSHP001_NO_OF = "RSHP001_NO_OF";

    /** The constant LBL_RSHP001_CURRENT. */
    public static final String LBL_RSHP001_CURRENT = "RSHP001_CURRENT";

    /** The constant LBL_RSHP001_QTY. */
    public static final String LBL_RSHP001_QTY = "RSHP001_QTY";

    /** The constant LBL_RSHP001_SPS_DO_NO. */
    public static final String LBL_RSHP001_SPS_DO_NO = "RSHP001_SPS_DO_NO";

    /** The constant LBL_RSHP001_CIGMA_DO_NO. */
    public static final String LBL_RSHP001_CIGMA_DO_NO = "RSHP001_CIGMA_DO_NO";

    /** The constant LBL_RSHP001_DRIVER. */
    public static final String LBL_RSHP001_DRIVER = "RSHP001_DRIVER";

    /** The constant LBL_RSHP001_RECV. */
    public static final String LBL_RSHP001_RECV = "RSHP001_RECV";

    /** The constant LBL_RSHP001_CHECK. */
    public static final String LBL_RSHP001_CHECK = "RSHP001_CHECK";

    /** The constant LBL_RSHP001_TOTAL_NO_OF. */
    public static final String LBL_RSHP001_TOTAL_NO_OF = "RSHP001_TOTAL_NO_OF";

    /** The constant LBL_RSHP001_FOR. */
    public static final String LBL_RSHP001_FOR = "RSHP001_FOR";

    /** The constant LBL_RSHP001_BOXES. */
    public static final String LBL_RSHP001_BOXES = "RSHP001_BOXES";

    /** The constant LBL_RSHP001_PALLET. */
    public static final String LBL_RSHP001_PALLET = "RSHP001_PALLET";

    /** The constant LBL_RSHP001_SUP_DEL. */
    public static final String LBL_RSHP001_SUP_DEL = "RSHP001_SUP_DEL";

    /** The constant LBL_RSHP001_DENSO_REC. */
    public static final String LBL_RSHP001_DENSO_REC = "RSHP001_DENSO_REC";

    /** The constant LBL_RSHP001_QR_REMARK_TYPE2. */
    public static final String LBL_RSHP001_QR_REMARK_TYPE2 = "RSHP001_QR_REMARK_TYPE2";

    /** The constant LBL_RSHP001_QR_REMARK_TYPE3. */
    public static final String LBL_RSHP001_QR_REMARK_TYPE3 = "RSHP001_QR_REMARK_TYPE3";

    /** The constant LBL_RSHP001_SHIPPING. */
    public static final String LBL_RSHP001_SHIPPING = "RSHP001_SHIPPING";

    /** The constant LBL_RSHP001_RSN. */
    public static final String LBL_RSHP001_RSN = "RSHP001_RSN";

    /** The constant LBL_TRANSPORTATION_MODE. */
    public static final String LBL_TRANSPORTATION_MODE = "TRANSPORTATION_MODE";
    
    /** The constant LBL_ALL_NOT_INPUT_MANDATORY. */
    public static final String LBL_ALL_NOT_INPUT_MANDATORY = "ALL_NOT_INPUT_MANDATORY";
    
    /** The constant LBL_REGISTER_INVOICE_DATA. */
    public static final String LBL_REGISTER_INVOICE_DATA = "REGISTER_INVOICE_DATA";
    
    /** The constant LBL_REGISTER_CREDIT_NOTE_DATA. */
    public static final String LBL_REGISTER_CREDIT_NOTE_DATA = "REGISTER_CREDIT_NOTE_DATA";
    
    /** The constant LBL_UPDATE_INVOICE_INFORMATION. */
    public static final String LBL_UPDATE_INVOICE_INFORMATION = "UPDATE_INVOICE_INFORMATION";
    
    /** The constant LBL_UPDATE_CN_INFORMATION. */
    public static final String LBL_UPDATE_CN_INFORMATION  = "UPDATE_CN_INFORMATION";
    
    /** The constant LBL_UPDATE_INVOICE_STATUS_FROM_JDE. */
    public static final String LBL_UPDATE_INVOICE_STATUS_FROM_JDE 
        = "UPDATE_INVOICE_STATUS_FROM_JDE";
    
    /** The constant LBL_UPDATE_PAYMENT_STATUS_FROM_JDE. */
    public static final String LBL_UPDATE_PAYMENT_STATUS_FROM_JDE
        = "UPDATE_PAYMENT_STATUS_FROM_JDE";
    
    /** The constant LBL_UPDATE_COVER_PAGE_RUNNING_NO. */
    public static final String LBL_UPDATE_COVER_PAGE_RUNNING_NO = "UPDATE_COVER_PAGE_RUNNING_NO";
    
    /** The constant LBL_CREATE_INVOICE_COVERPAGE. */
    public static final String LBL_CREATE_INVOICE_COVERPAGE = "CREATE_INVOICE_COVERPAGE";
    
    /** The constant LBL_CREATE_ASN_NO_SHIPPING_QTY. */
    public static final String LBL_CREATE_ASN_NO_SHIPPING_QTY = "CREATE_ASN_NO_SHIPPING_QTY";
    
    /** The constant LBL_ASN_DETAIL_PURGING_DATA. */
    public static final String LBL_ASN_DETAIL_PURGING_DATA = "ASN_DETAIL_PURGING_DATA";
    
    /** The constant LBL_ASN_HEADER_PURGING_DATA. */
    public static final String LBL_ASN_HEADER_PURGING_DATA = "ASN_HEADER_PURGING_DATA";
    
    /** The constant LBL_DELIVERY_KANBAN_SEQUENCE_PURGING_DATA. */
    public static final String LBL_DELIVERY_KANBAN_SEQUENCE_PURGING_DATA
        = "DELIVERY_KANBAN_SEQUENCE_PURGING_DATA";
    
    /** The constant LBL_DELIVERY_ORDER_DETAIL_PURGING_DATA. */
    public static final String LBL_DELIVERY_ORDER_DETAIL_PURGING_DATA
        = "DELIVERY_ORDER_DETAIL_PURGING_DATA";
    
    /** The constant LBL_DELIVERY_ORDER_HEADER_PURGING_DATA. */
    public static final String LBL_DELIVERY_ORDER_HEADER_PURGING_DATA
        = "DELIVERY_ORDER_HEADER_PURGING_DATA";
    
    /** The constant LBL_PURCHASE_ORDER_COVER_PAGE_DETAIL_PURGING_DATA. */
    public static final String LBL_PURCHASE_ORDER_COVER_PAGE_DETAIL_PURGING_DATA
        = "PURCHASE_ORDER_COVER_PAGE_DETAIL_PURGING_DATA";
    
    /** The constant LBL_PURCHASE_ORDER_COVER_PAGE_HEADER_PURGING_DATA. */
    public static final String LBL_PURCHASE_ORDER_COVER_PAGE_HEADER_PURGING_DATA
        = "PURCHASE_ORDER_COVER_PAGE_HEADER_PURGING_DATA";
    
    /** The constant LBL_PURCHASE_ORDER_DETAIL_PURGING_DATA. */
    public static final String LBL_PURCHASE_ORDER_DETAIL_PURGING_DATA
        = "PURCHASE_ORDER_DETAIL_PURGING_DATA";
    
    /** The constant LBL_PURCHASE_ORDER_DUE_PURGING_DATA. */
    public static final String LBL_PURCHASE_ORDER_DUE_PURGING_DATA
        = "PURCHASE_ORDER_DUE_PURGING_DATA";
    
    /** The constant LBL_PURCHASE_ORDER_PURGING_DATA. */
    public static final String LBL_PURCHASE_ORDER_PURGING_DATA
        = "PURCHASE_ORDER_PURGING_DATA";
    
    /** The constant LBL_INVOICE. */
    public static final String LBL_INVOICE = "INVOICE";
    
    /** The constant LBL_INVOICE. */
    public static final String LBL_CN_DETAIL_PURGING_DATA = "CN_DETAIL_PURGING_DATA";
    
    /** The constant LBL_INVOICE. */
    public static final String LBL_CN_HEADER_PAGE_PURGING_DATA = "CN_HEADER_PAGE_PURGING_DATA";
    
    /** The constant LBL_INVOICE. */
    public static final String LBL_INVOICE_COVER_PAGE_PURGING_DATA = "INVOICE_COVER_PAGE_PURGING_DATA";
    
    /** The constant LBL_INVOICE. */
    public static final String LBL_INVOICE_DETAIL_PURGING_DATA = "INVOICE_DETAIL_PURGING_DATA";
    
    /** The constant LBL_INVOICE. */
    public static final String LBL_INVOICE_PURGING_DATA = "INVOICE_PURGING_DATA";
    
    /** The constant LBL_INVOICE_COVER_PAGE_PURGING_RUNNING_NO. */
    public static final String LBL_INVOICE_COVER_PAGE_PURGING_RUNNING_NO = "INVOICE_COVER_PAGE_PURGING_RUNNING_NO";
    
    /** The constant LBL_PURGE_DATA_PROCESS. */
    public static final String LBL_PURGE_DATA_PROCESS = "PURGE_DATA_PROCESS";
    
    /** The constant LBL_PURGE_DATA_PROCESS. */
    public static final String LBL_SOME_DENSO_COMPANY = "SOME_DENSO_COMPANY";
    
    /** The constant LBL_SHIP_DATE_FORM. */
    public static final String LBL_SHIP_DATE_FORM = "SHIP_DATE_FORM";
    
    /** The constant LBL_SHIP_DATE_TO. */
    public static final String LBL_SHIP_DATE_TO = "SHIP_DATE_TO";
    
    /** The constant LBL_ROUTE_NO. */
    public static final String LBL_ROUTE_NO = "ROUTE_NO";
    
    /** The constant LBL_REV. */
    public static final String LBL_REV = "REV";
    
    /** The constant LBL_REVISION. */
    public static final String LBL_REVISION = "REVISION";
    
    /** The constant LBL_SHIP_DATE. */
    public static final String LBL_SHIP_DATE = "SHIP_DATE";
    
    /** The constant LBL_SHIP_DATE. */
    public static final String LBL_SHIP_TIME = "SHIP_TIME";
    
    /** The constant LBL_ORIGINAL_SPS_D/O. */
    public static final String LBL_ORIGINAL_SPS_DO = "ORIGINAL_SPS_DO_NO";
    
    /** The constant LBL_PREVIOUS_SPS_D/O. */
    public static final String LBL_PREVIOUS_SPS_DO = "PREVIOUS_SPS_DO_NO";
    
    /** The constant LBL_CURRENT_SPS_DO. */
    public static final String LBL_CURRENT_SPS_DO = "CURRENT_SPS_DO"; 
    
    /** The constant LBL_CURRENT_CIGMA_DO. */
    public static final String LBL_CURRENT_CIGMA_DO = "CURRENT_CIGMA_DO";
    
    /** The constant LBL_CURRENT_CIGMA_DO_NO. */
    public static final String LBL_CURRENT_CIGMA_DO_NO = "CURRENT_CIGMA_DO_NO";
    
    /** The constant LBL_CURRENT_CIGMA_PO_NO. */
    public static final String LBL_CURRENT_CIGMA_PO_NO = "CURRENT_CIGMA_PO_NO";
    
    /** The constant LBL_QTY_BOX. */
    public static final String LBL_QTY_BOX = "QTY_BOX";
    
    /** The constant LBL_NO_OF_BOX. */
    public static final String LBL_NO_OF_BOX = "NO_OF_BOX";
    
    /** The constant LBL_NUMBER_OF_PALLET. */
    public static final String LBL_NUMBER_OF_PALLET = "NUMBER_OF_PALLET";
    
    /** The constant LBL_D_U/M. */
    public static final String LBL_D_UM = "D_U/M";
    
    /** The constant LBL_CONTROL_NO. */
    public static final String LBL_CONTROL_NO = "CONTROL_NO";
    
    /** The constant LBL_WH. */
    public static final String LBL_WH = "WH";
    
    /** The constant LBL_DOCKCODE. */
    public static final String LBL_DOCKCODE = "DOCKCODE";
    
    /** The constant LBL_CTRL_NO. */
    public static final String LBL_CTRL_NO = "CTRL_NO";
    
    /** The constant LBL_CYCLE. */
    public static final String LBL_CYCLE = "CYCLE";
    
    /** The constant LBL_KANBAN_SEQ. */
    public static final String LBL_KANBAN_SEQ = "KANBAN_SEQ";
    
    /** The constant LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE. */
    public static final String LBL_PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE = "PURCHASE_ORDER_FIRM_WAIT_TO_ACKNOWLEDGE";
    
    /** The constant LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE. */
    public static final String LBL_PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE = "PURCHASE_ORDER_FORCAST_WAIT_TO_ACKNOWLEDGE";
    
    /** The constant LBL_URGENT_ORDER. */
    public static final String LBL_URGENT_ORDER = "URGENT_ORDER";
    
    // [IN072] Change caption for Tasklist Accept/Reject P/O
    
    /** The constant LBL_PURCHASE_ORDER_FORCAST_ACCEPT. */
    public static final String LBL_PURCHASE_ORDER_FORCAST_ACCEPT = "PURCHASE_ORDER_FORCAST_ACCEPT";
    
    /** The constant LBL_PURCHASE_ORDER_FORCAST_REJECT. */
    public static final String LBL_PURCHASE_ORDER_FORCAST_REJECT = "PURCHASE_ORDER_FORCAST_REJECT";
    
    /** The constant LBL_PURCHASE_ORDER_FORCAST_PENDING_WAIT. */
    public static final String LBL_PURCHASE_ORDER_FORCAST_PENDING_WAIT 
        = "PURCHASE_ORDER_FORCAST_PENDING_WAIT";

    // [IN054] Change caption for email flag Urgent Order
    /** The constant LBL_URGENT_ORDER_DENSO_REPLY. */
    public static final String LBL_URGENT_ORDER_DENSO_REPLY = "URGENT_ORDER_DENSO_REPLY";
    
    /** The constant LBL_DATA_ALREADY_USED. */
    public static final String LBL_DATA_ALREADY_USED = "DATA_ALREADY_USED";
    
    /** The constant LBL_SCHEDULE_TRANSFER_INVOICE. */
    public static final String LBL_SCHEDULE_TRANSFER_INVOICE = "SCHEDULE_TRANSFER_INVOICE";
    
    /** The constant LBL_ASN_RECEIVING_NOT_EXIST. */
    public static final String LBL_ASN_RECEIVING_NOT_EXIST = "ASN_RECEIVING_NOT_EXIST";
    
    /** The constant LBL_GROUPING_INVOICE. */
    public static final String LBL_GROUPING_INVOICE = "GROUPING_INVOICE";
    
    /** The constant LBL_TO_SAME_DENSO_CODE. */
    public static final String LBL_TO_SAME_DENSO_CODE = "TO_SAME_DENSO_CODE";
    
    /** The constant LBL_TO_SAME_DENSO_PLANT_CODE. */
    public static final String LBL_TO_SAME_DENSO_PLANT_CODE = "TO_SAME_DENSO_PLANT_CODE";
    
    /** The constant LBL_TO_SAME_SUPPLIER_TAX_ID. */
    public static final String LBL_TO_SAME_SUPPLIER_TAX_ID = "TO_SAME_SUPPLIER_TAX_ID";
    
    /** The constant LBL_TO_SAME_PLANETA_DATE. */
    public static final String LBL_TO_SAME_PLANETA_DATE = "TO_SAME_PLANETA_DATE";
    
    /** The constant LBL_TO_SAME_PLANETA_MONTH. */
    public static final String LBL_TO_SAME_PLANETA_MONTH = "TO_SAME_PLANETA_MONTH";
    
    /** The constant LBL_TO_SAME_PLANETA_WEEK. */
    public static final String LBL_TO_SAME_PLANETA_WEEK = "TO_SAME_PLANETA_WEEK";
    
    /** The constant LBL_PLANETA_DATE. */
    public static final String LBL_PLANETA_DATE = "PLANETA_DATE";
    
    /** The constant LBL_PLANETA_MONTH. */
    public static final String LBL_PLANETA_MONTH = "PLANETA_MONTH";
    
    /** The constant LBL_PLANETA_WEEK. */
    public static final String LBL_PLANETA_WEEK = "PLANETA_WEEK";
    
    /** The constant LBL_LATEST_DO_REVISION. */
    public static final String LBL_LATEST_DO_REVISION = "LATEST_DO_REVISION";
    
    /** The constant LBL_UM. */
    public static final String LBL_UM = "UM";
    
    /** The constant LBL_TOTAL_SHIPPING_QTY. */
    public static final String LBL_TOTAL_SHIPPING_QTY = "TOTAL_SHIPPING_QTY";
    
    /** The constant LBL_TOTAL_RECEIVED_QTY. */
    public static final String LBL_TOTAL_RECEIVED_QTY = "TOTAL_RECEIVED_QTY";
    
    /** The constant LBL_TOTAL_IN_TRANSIT_QTY. */
    public static final String LBL_TOTAL_IN_TRANSIT_QTY = "TOTAL_IN_TRANSIT_QTY";
    
    /** The constant LBL_TOTAL_BACK_ORDER_QTY. */
    public static final String LBL_TOTAL_BACK_ORDER_QTY = "TOTAL_BACK_ORDER_QTY";
    
    /** The constant LBL_CORRECT. */
    public static final String LBL_CORRECT = "CORRECT";
    
    /** The constant LBL_WARNING. */
    public static final String LBL_WARNING = "WARNING";
    
    /** The constant LBL_ACTUAL_DEPARTURE. */
    public static final String LBL_ACTUAL_DEPARTURE = "ACTUAL_DEPARTURE";
    
    /** The constant LBL_SUPPLIER_UOM. */
    public static final String LBL_SUPPLIER_UOM = "SUPPLIER_UOM";
    
    /** The constant LBL_NOT_SAME_D_PN. */
    public static final String LBL_NOT_SAME_D_PN = "NOT_SAME_D_PN";
    
    /** The constant LBL_INVOICE_RCV_STATUS. */
    public static final String LBL_INVOICE_RCV_STATUS = "INVOICE_RCV_STATUS";
    
    /** The constant LBL_INVOICE_STATUS. */
    public static final String LBL_INVOICE_STATUS = "INVOICE_STATUS";
    
    /** The constant LBL_CN_DATE_FROM. */
    public static final String LBL_CN_DATE_FROM = "CN_DATE_FROM";
    
    /** The constant LBL_CN_DATE_TO. */
    public static final String LBL_CN_DATE_TO = "CN_DATE_TO";
    
    /** The constant LBL_CURRENT_USER. */
    public static final String LBL_CURRENT_USER = "CURRENT_USER";
    
    /** The constant LBL_DATE. */
    public static final String LBL_DATE = "DATE";
    
    /** The constant LBL_MONTH. */
    public static final String LBL_MONTH = "MONTH";
    
    /** The constant LBL_WEEK. */
    public static final String LBL_WEEK = "WEEK";
    
    /** The constant LBL_PLAN_ETA_DATE_FROM. */
    public static final String LBL_PLAN_ETA_DATE_FROM = "PLAN_ETA_DATE_FROM";
    
    /** The constant LBL_PLAN_ETA_DATE_TO. */
    public static final String LBL_PLAN_ETA_DATE_TO = "PLAN_ETA_DATE_TO";
    
    /** The constant LBL_CANCEL_INVOICE_LOWER. */
    public static final String LBL_CANCEL_INVOICE_LOWER = "CANCEL_INVOICE_LOWER";
    
    /** The constant LBL_BASE_AMOUNT. */
    public static final String LBL_BASE_AMOUNT = "BASE_AMOUNT";
    
    /** The constant LBL_BASE_AMOUNT. */
    public static final String LBL_VAT_AMOUNT = "VAT_AMOUNT";
    
    /** The constant LBL_PAYMENT_DATE. */
    public static final String LBL_PAYMENT_DATE = "PAYMENT_DATE";
    
    /** The constant LBL_PO_FORCE_ACKNOWLEDGE. */
    public static final String LBL_PO_FORCE_ACKNOWLEDGE = "PO_FORCE_ACKNOWLEDGE";
    
    /** The constant LBL_DENSO_CURRENCY. */
    public static final String LBL_DENSO_CURRENCY = "DENSO_CURRENCY";
    
    /** The constant LBL_VAT_RATE. */
    public static final String LBL_VAT_RATE = "VAT_RATE";
    
    /** The constant LBL_VAT_TYPE. */
    public static final String LBL_VAT_TYPE = "VAT_TYPE";
    
    /** The constant LBL_ASN_DATE. */
    public static final String LBL_ASN_DATE = "ASN_DATE";
    
    /** The constant LBL_DENSO_UNIT_OF_MEASURE. */
    public static final String LBL_DENSO_UNIT_OF_MEASURE = "DENSO_UNIT_OF_MEASURE";
    
    /** The constant LBL_DENSO_AMOUNT_BY_PN. */
    public static final String LBL_DENSO_BASE_AMOUNT_BY_PN = "DENSO_BASE_AMOUNT_BY_PN";
    
    /** The constant LBL_SUPPLIER_BASE_AMOUNT_BY_PN. */
    public static final String LBL_SUPPLIER_BASE_AMOUNT_BY_PN = "SUPPLIER_BASE_AMOUNT_BY_PN";
    
    /** The constant LBL_COMPANY_CODE_LIST. */
    public static final String LBL_COMPANY_CODE_LIST = "COMPANY_CODE_LIST";
    
    /** The constant LBL_PARRENT_JOB_ID. */
    public static final String LBL_PARRENT_JOB_ID = "PARRENT_JOB_ID";
    
    /** The constant LBL_START_DENSO_COMPANY_CODE. */
    public static final String LBL_START_DENSO_COMPANY_CODE = "START_DENSO_COMPANY_CODE";
    
    /** The constant LBL_PURCHASE_ORDER. */
    public static final String LBL_PURCHASE_ORDER = "PURCHASE_ORDER";
    
    /** The constant LBL_PO. */
    public static final String LBL_PO = "P/O";
    
    /** The constant LBL_DO. */
    public static final String LBL_DO = "D/O";
    
    /** The constant LBL_ASN. */
    public static final String LBL_ASN = "ASN";
    
    /** The constant LBL_DELIVERY_ORDER. */
    public static final String LBL_DELIVERY_ORDER = "DELIVERY_ORDER";
    
    /** The constant LBL_ORDER. */
    public static final String LBL_ORDER = "ORDER";
    
    /** The constant LBL_CN. */
    public static final String LBL_CN = "CN";
    
    /** The constant LBL_START_PURGING_ORDER_BY_PO_NUMBER. */
    public static final String LBL_START_PURGING_ORDER_BY_PO_NUMBER 
        = "START_PURGING_ORDER_BY_PO_NUMBER";
    
    /** The constant LBL_LANGUAGE. */
    public static final String LBL_LANGUAGE = "LANGUAGE";
    
    /** The constant LBL_SELECT_ENGLISH. */
    public static final String LBL_SELECT_ENGLISH = "SELECT_ENGLISH";
    
    /** The constant LBL_SELECT_THAI. */
    public static final String LBL_SELECT_THAI = "SELECT_THAI";
    
    /** The constant LBL_SELECT_JAPANESE. */
    public static final String LBL_SELECT_JAPANESE = "SELECT_JAPANESE";
    
    /** The constant LBL_D_P. */
    public static final String LBL_D_P = "D_P";
    
    /** The constant LBL_UNIT_PRICE_OF. */
    public static final String LBL_UNIT_PRICE_OF = "UNIT_PRICE_OF";

    /** The constant LBL_SEND_INVOICE. */
    public static final String LBL_SEND_INVOICE = "SEND_INVOICE";

    /** The constant LBL_SEND_INVOICE. */
    public static final String LBL_CALCULATE_CN_WARNING = "CALCULATE_CN_WARNING";
    
    /** The constant LBL_UPDATE_CREATED_INVOICE_FLAG. */
    public static final String LBL_UPDATE_CREATED_INVOICE_FLAG = "UPDATE_CREATED_INVOICE_FLAG";
    
    /** The constant LBL_CREATE_INVOICE_DUPLICATE_INVOICE_NO. */
    public static final String LBL_CREATE_INVOICE_DUPLICATE_INVOICE_NO 
        = "CREATE_INVOICE_DUPLICATE_INVOICE_NO";
    
    /** The constant LBL_CREATE_INVOICE_DUPLICATE_CN_NO. */
    public static final String LBL_CREATE_INVOICE_DUPLICATE_CN_NO 
        = "CREATE_INVOICE_DUPLICATE_CN_NO";

    /** The constant LBL_INPUT_COMMA_IN_INVOICE_NO. */
    public static final String LBL_INPUT_COMMA_IN_INVOICE_NO = "INPUT_COMMA_IN_INVOICE_NO";

    /** The constant LBL_INPUT_COMMA_IN_CN_NO. */
    public static final String LBL_INPUT_COMMA_IN_CN_NO = "INPUT_COMMA_IN_CN_NO";
    
    /** The constant LBL_NO_PRICE_DIFFERENCEN. */
    public static final String LBL_NO_PRICE_DIFFERENCEN = "NO_PRICE_DIFFERENCEN";
    
    /** The constant LBL_UPDATE_FILE_ID. */
    public static final String LBL_UPDATE_FILE_ID = "UPDATE_FILE_ID";
    
    /** The constant LBL_BOTH_DELIVERY_TIME_FROM_AND_TO. */
    public static final String LBL_BOTH_DELIVERY_TIME_FROM_AND_TO
        = "BOTH_DELIVERY_TIME_FROM_AND_TO";
    
    /** The constant LBL_BOTH_PLAN_ETA_TIME_FROM_AND_TO. */
    public static final String LBL_BOTH_PLAN_ETA_TIME_FROM_AND_TO
        = "BOTH_PLAN_ETA_TIME_FROM_AND_TO";
    
    /** The constant LBL_BECAUSE_ALREADY_TRANSFERED_OR_CANCELLED. */
    public static final String LBL_BECAUSE_ALREADY_TRANSFERED_OR_CANCELLED
        = "BECAUSE_ALREADY_TRANSFERED_OR_CANCELLED";

    // [IN054] Add new label for error message
    /** The constant LBL_DENSO_REPLY_PENDING_PO. */
    public static final String LBL_DENSO_REPLY_PENDING_PO = "DENSO_REPLY_PENDING_PO";

    /** Add new label for SPS phase II. */
    /** The constant LBL_RECEIVE_BY_SCAN. */
    public static final String LBL_RECEIVE_BY_SCAN = "SCAN_FLAG";
    /** Add new label for SPS phase II. */
    /** The constant LBL_RECEIVE_BY_SCAN. */
    public static final String LBL_LT_FLAG = "LT_FLAG";
    
    /** The constant LBL_RECEIVE_BY_SCAN. */
    public static final String LBL_KANBAN_TYPE = "KANBAN_TYPE";
    
    /** The constant LBL_RECEIVE_BY_SCAN. */
    public static final String LBL_WH_PRIME_RECEIVING_NOT_SAME = "WH_PRIME_RECEIVING_NOT_SAME";
    
    /** The constant LBL_FIXED_ORDER. */
    public static final String LBL_FIXED_ORDER = "FIXED_ORDER";
    
    /** The constant LBL_FIXED_ORDER. */
    public static final String LBL_KANBAN_ORDER = "KANBAN_ORDER";
    
    /* End LABEL KEY ============================================================================ */

    /* REPORT PARAMETER NAME ==================================================================== */

    /** The constant RPT_PRM_REPORT_NAME. */
    public static final String RPT_PRM_REPORT_NAME = "REPORT_NAME";

    /** The constant RPT_PRM_ASN_NO. */
    public static final String RPT_PRM_ASN_NO = "ASN_NO";

    /** The constant RPT_PRM_NO. */
    public static final String RPT_PRM_NO = "NO";

    /** The constant RPT_PRM_CODE. */
    public static final String RPT_PRM_CODE = "CODE";

    /** The constant RPT_PRM_ROUTE. */
    public static final String RPT_PRM_ROUTE = "ROUTE";

    /** The constant RPT_PRM_DEL. */
    public static final String RPT_PRM_DEL = "DEL";

    /** The constant RPT_PRM_SUPPLIER. */
    public static final String RPT_PRM_SUPPLIER = "SUPPLIER";

    /** The constant RPT_PRM_SUP_NAME. */
    public static final String RPT_PRM_SUP_NAME = "SUP_NAME";

    /** The constant RPT_PRM_SUP_LOC. */
    public static final String RPT_PRM_SUP_LOC = "SUP_LOC";

    /** The constant RPT_PRM_SUP_PLANT. */
    public static final String RPT_PRM_SUP_PLANT = "SUP_PLANT";

    /** The constant RPT_PRM_PAGE. */
    public static final String RPT_PRM_PAGE = "PAGE";

    /** The constant RPT_PRM_G_PAGE. */
    public static final String RPT_PRM_G_PAGE = "G_PAGE";

    /** The constant RPT_PRM_A_ETD. */
    public static final String RPT_PRM_A_ETD = "A_ETD";

    /** The constant RPT_PRM_SCHEDULE. */
    public static final String RPT_PRM_SCHEDULE = "SCHEDULE";

    /** The constant RPT_PRM_TIME. */
    public static final String RPT_PRM_TIME = "TIME";

    /** The constant RPT_PRM_P_ETA. */
    public static final String RPT_PRM_P_ETA = "P_ETA";

    /** The constant RPT_PRM_PLANT. */
    public static final String RPT_PRM_PLANT = "PLANT";

    /** The constant RPT_PRM_WARE. */
    public static final String RPT_PRM_WARE = "WARE";

    /** The constant RPT_PRM_HOUSE. */
    public static final String RPT_PRM_HOUSE = "HOUSE";

    /** The constant RPT_PRM_DOCK. */
    public static final String RPT_PRM_DOCK = "DOCK";

    /** The constant RPT_PRM_RCV. */
    public static final String RPT_PRM_RCV = "RCV";

    /** The constant RPT_PRM_LANE. */
    public static final String RPT_PRM_LANE = "LANE";

    /** The constant RPT_PRM_TRANS. */
    public static final String RPT_PRM_TRANS = "TRANS";

    /** The constant RPT_PRM_CYCLE. */
    public static final String RPT_PRM_CYCLE = "CYCLE";

    /** The constant RPT_PRM_TRIP_NO. */
    public static final String RPT_PRM_TRIP_NO = "TRIP_NO";

    /** The constant RPT_PRM_ISSUE. */
    public static final String RPT_PRM_ISSUE = "ISSUE";

    /** The constant RPT_PRM_DATE. */
    public static final String RPT_PRM_DATE = "DATE";

    /** The constant RPT_PRM_LAST. */
    public static final String RPT_PRM_LAST = "LAST";

    /** The constant RPT_PRM_MODIFIED. */
    public static final String RPT_PRM_MODIFIED = "MODIFIED";

    /** The constant RPT_PRM_CTRL. */
    public static final String RPT_PRM_CTRL = "CTRL";

    /** The constant RPT_PRM_F. */
    public static final String RPT_PRM_F = "F";

    /** The constant RPT_PRM_D_PART_NUMBER. */
    public static final String RPT_PRM_D_PART_NUMBER = "D_PART_NUMBER";

    /** The constant RPT_PRM_S_PART_NUMBER. */
    public static final String RPT_PRM_S_PART_NUMBER = "S_PART_NUMBER";

    /** The constant RPT_PRM_DESCRIPTION. */
    public static final String RPT_PRM_DESCRIPTION = "DESCRIPTION";

    /** The constant RPT_PRM_UM. */
    public static final String RPT_PRM_UM = "UM";

    /** The constant RPT_PRM_ORDER. */
    public static final String RPT_PRM_ORDER = "ORDER";

    /** The constant RPT_PRM_METHOD. */
    public static final String RPT_PRM_METHOD = "METHOD";

    /** The constant RPT_PRM_QUANTITY. */
    public static final String RPT_PRM_QUANTITY = "QUANTITY";

    /** The constant RPT_PRM_BOX. */
    public static final String RPT_PRM_BOX = "BOX";

    /** The constant RPT_PRM_NO_OF. */
    public static final String RPT_PRM_NO_OF = "NO_OF";

    /** The constant RPT_PRM_CURRENT. */
    public static final String RPT_PRM_CURRENT = "CURRENT";

    /** The constant RPT_PRM_QTY. */
    public static final String RPT_PRM_QTY = "QTY";

    /** The constant RPT_PRM_SPS_DO_NO. */
    public static final String RPT_PRM_SPS_DO_NO = "SPS_DO_NO";

    /** The constant RPT_PRM_CIGMA_DO_NO. */
    public static final String RPT_PRM_CIGMA_DO_NO = "CIGMA_DO_NO";

    /** The constant RPT_PRM_DRIVER. */
    public static final String RPT_PRM_DRIVER = "DRIVER";

    /** The constant RPT_PRM_RECV. */
    public static final String RPT_PRM_RECV = "RECV";

    /** The constant RPT_PRM_CHECK. */
    public static final String RPT_PRM_CHECK = "CHECK";

    /** The constant RPT_PRM_TOTAL_NO_OF. */
    public static final String RPT_PRM_TOTAL_NO_OF = "TOTAL_NO_OF";

    /** The constant RPT_PRM_FOR. */
    public static final String RPT_PRM_FOR = "FOR";

    /** The constant RPT_PRM_BOXES. */
    public static final String RPT_PRM_BOXES = "BOXES";

    /** The constant RPT_PRM_PALLET. */
    public static final String RPT_PRM_PALLET = "PALLET";

    /** The constant RPT_PRM_SUP_DEL. */
    public static final String RPT_PRM_SUP_DEL = "SUP_DEL";

    /** The constant RPT_PRM_DENSO_REC. */
    public static final String RPT_PRM_DENSO_REC = "DENSO_REC";

    /** The constant RPT_PRM_SHIPPING. */
    public static final String RPT_PRM_SHIPPING = "SHIPPING";

    /** The constant RPT_PRM_RSN. */
    public static final String RPT_PRM_RSN = "RSN";

    /** The constant RPT_PRM_RSN_CAPTION_1. */
    public static final String RPT_PRM_RSN_CAPTION_1 = "RSN_CAPTION_1";

    /** The constant RPT_PRM_RSN_CAPTION_2. */
    public static final String RPT_PRM_RSN_CAPTION_2 = "RSN_CAPTION_2";

    /** The constant RPT_PRM_DENSO_COMPANY. */
    public static final String RPT_PRM_DENSO_COMPANY = "DENSO_COMPANY";

    /** The constant RPT_PRM_TOTAL_RECORD. */
    public static final String RPT_PRM_TOTAL_RECORD = "TOTAL_RECORD";

    /** The constant ORDER_METHOD. */
    public static final String RPT_PRM_ORDER_METHOD = "ORDER_METHOD";
    
    /** The constant RORD001_COL_QTY. */
    public static final String RORD001_COL_QTY = "RORD001_COL_QTY";

    /** The constant RORD001_DOCK. */
    public static final String RORD001_DOCK = "RORD001_DOCK";

    /** The constant RORD001_ISSUED. */
    public static final String RORD001_ISSUED = "RORD001_ISSUED";

    /** The constant RORD001_FROM. */
    public static final String RORD001_FROM = "RORD001_FROM";

    /** The constant RORD001_CIGMA_PO_NO. */
    public static final String RORD001_CIGMA_PO_NO = "RORD001_CIGMA_PO_NO";

    /** The constant RORD001_FRI. */
    public static final String RORD001_FRI = "RORD001_FRI";

    /** The constant RORD001_TITLE. */
    public static final String RORD001_TITLE = "RORD001_TITLE";

    /** The constant RORD001_VARIABLE. */
    public static final String RORD001_VARIABLE = "RORD001_VARIABLE";

    /** The constant RORD001_WEN. */
    public static final String RORD001_WEN = "RORD001_WEN";

    /** The constant RORD001_TRANS. */
    public static final String RORD001_TRANS = "RORD001_TRANS";

    /** The constant RORD001_PLANNER. */
    public static final String RORD001_PLANNER = "RORD001_PLANNER";

    /** The constant RORD001_DWM. */
    public static final String RORD001_DWM = "RORD001_DWM";

    /** The constant RORD001_COL_MONTH. */
    public static final String RORD001_COL_MONTH = "RORD001_COL_MONTH";

    /** The constant RORD001_TUE. */
    public static final String RORD001_TUE = "RORD001_TUE";

    /** The constant RORD001_MON. */
    public static final String RORD001_MON = "RORD001_MON";

    /** The constant RORD001_SUP_PLANT. */
    public static final String RORD001_SUP_PLANT = "RORD001_SUP_PLANT";

    /** The constant RORD001_COL_DATE. */
    public static final String RORD001_COL_DATE = "RORD001_COL_DATE";

    /** The constant RORD001_SAT. */
    public static final String RORD001_SAT = "RORD001_SAT";

    /** The constant RORD001_QTY_BOX. */
    public static final String RORD001_QTY_BOX = "RORD001_QTY_BOX";

    /** The constant RORD001_CD. */
    public static final String RORD001_CD = "RORD001_CD";

    /** The constant RORD001_ORDER_METHOD. */
    public static final String RORD001_ORDER_METHOD = "RORD001_ORDER_METHOD";

    /** The constant RORD001_TOTAL. */
    public static final String RORD001_TOTAL = "RORD001_TOTAL";

    /** The constant RORD001_UNIT. */
    public static final String RORD001_UNIT = "RORD001_UNIT";

    /** The constant RORD001_SUN. */
    public static final String RORD001_SUN = "RORD001_SUN";

    /** The constant RORD001_UP. */
    public static final String RORD001_UP = "RORD001_UP";

    /** The constant RORD001_ORDER_LOT. */
    public static final String RORD001_ORDER_LOT = "RORD001_ORDER_LOT";

    /** The constant RORD001_DESCRIPTION. */
    public static final String RORD001_DESCRIPTION = "RORD001_DESCRIPTION";

    /** The constant RORD001_TO. */
    public static final String RORD001_TO = "RORD001_TO";

    /** The constant RORD001_DATE. */
    public static final String RORD001_DATE = "RORD001_DATE";

    /** The constant RORD001_SUPPLIER. */
    public static final String RORD001_SUPPLIER = "RORD001_SUPPLIER";

    /** The constant RORD001_RELEASE_NO. */
    public static final String RORD001_RELEASE_NO = "RORD001_RELEASE_NO";

    /** The constant RORD001_D_PART. */
    public static final String RORD001_D_PART = "RORD001_D_PART";

    /** The constant RORD001_DENSO. */
    public static final String RORD001_DENSO = "RORD001_DENSO";

    /** The constant RORD001_SPS_PO_NO. */
    public static final String RORD001_SPS_PO_NO = "RORD001_SPS_PO_NO";

    /** The constant RORD001_PIO. */
    public static final String RORD001_PIO = "RORD001_PIO";

    /** The constant RORD001_THU. */
    public static final String RORD001_THU = "RORD001_THU";
    
    /** The constant RORD002_TRANS. */
    public static final String RORD002_TRANS = "RORD002_TRANS";

    /** The constant RORD002_TO. */
    public static final String RORD002_TO = "RORD002_TO";

    /** The constant RORD002_D_PART_NUMBER. */
    public static final String RORD002_D_PART_NUMBER = "RORD002_D_PART_NUMBER";

    /** The constant RORD002_SUP__PLANT. */
    public static final String RORD002_SUP__PLANT = "RORD002_SUP__PLANT";

    /** The constant RORD002_DESCRIPTION. */
    public static final String RORD002_DESCRIPTION = "RORD002_DESCRIPTION";

    /** The constant RORD002_SUP_PLANT. */
    public static final String RORD002_SUP_PLANT = "RORD002_SUP_PLANT";

    /** The constant RORD002_PAGE. */
    public static final String RORD002_PAGE = "RORD002_PAGE";

    /** The constant RORD002_CURRENT_SPS_PO_NO. */
    public static final String RORD002_CURRENT_SPS_PO_NO = "RORD002_CURRENT_SPS_PO_NO";

    /** The constant RORD002_REASON_FOR_USE. */
    public static final String RORD002_REASON_FOR_USE = "RORD002_REASON_FOR_USE";

    /** The constant RORD002_ETD. */
    public static final String RORD002_ETD = "RORD002_ETD";

    /** The constant RORD002_DIFF. */
    public static final String RORD002_DIFF = "RORD002_DIFF";

    /** The constant RORD002_RELEASE_NO. */
    public static final String RORD002_RELEASE_NO = "RORD002_RELEASE_NO";

    /** The constant RORD002_DWM. */
    public static final String RORD002_DWM = "RORD002_DWM";

    /** The constant RORD002_ISSUED. */
    public static final String RORD002_ISSUED = "RORD002_ISSUED";

    /** The constant RORD002_SUPPLIER. */
    public static final String RORD002_SUPPLIER = "RORD002_SUPPLIER";

    /** The constant RORD002_SUP_CODE. */
    public static final String RORD002_SUP_CODE = "RORD002_SUP_CODE";

    /** The constant RORD002_IS_6. */
    public static final String RORD002_IS_6 = "RORD002_IS_6";

    /** The constant RORD002_IS_5. */
    public static final String RORD002_IS_5 = "RORD002_IS_5";

    /** The constant RORD002_IS_4. */
    public static final String RORD002_IS_4 = "RORD002_IS_4";

    /** The constant RORD002_IS_3. */
    public static final String RORD002_IS_3 = "RORD002_IS_3";

    /** The constant RORD002_IS_2. */
    public static final String RORD002_IS_2 = "RORD002_IS_2";

    /** The constant RORD002_IS_1. */
    public static final String RORD002_IS_1 = "RORD002_IS_1";

    /** The constant RORD002_MODEL. */
    public static final String RORD002_MODEL = "RORD002_MODEL";

    /** The constant RORD002_FAX_NO. */
    public static final String RORD002_FAX_NO = "RORD002_FAX_NO";

    /** The constant RORD002_CURRENT. */
    public static final String RORD002_CURRENT = "RORD002_CURRENT";

    /** The constant RORD002_CD. */
    public static final String RORD002_CD = "RORD002_CD";

    /** The constant RORD002_DESC. */
    public static final String RORD002_DESC = "RORD002_DESC";

    /** The constant RORD002_PREVIOUS. */
    public static final String RORD002_PREVIOUS = "RORD002_PREVIOUS";

    /** The constant RORD002_LOCATION. */
    public static final String RORD002_LOCATION = "RORD002_LOCATION";

    /** The constant RORD002_PLANNER. */
    public static final String RORD002_PLANNER = "RORD002_PLANNER";

    /** The constant RORD002_DEL_SEQ. */
    public static final String RORD002_DEL_SEQ = "RORD002_DEL_SEQ";

    /** The constant RORD002_TITLE. */
    public static final String RORD002_TITLE = "RORD002_TITLE";

    /** The constant RORD002_WH. */
    public static final String RORD002_WH = "RORD002_WH";

    /** The constant RORD002_FROM. */
    public static final String RORD002_FROM = "RORD002_FROM";

    /** The constant RORD002_UM. */
    public static final String RORD002_UM = "RORD002_UM";

    /** The constant RORD002_ORDER_TYPE. */
    public static final String RORD002_ORDER_TYPE = "RORD002_ORDER_TYPE";

    /** The constant RORD002_RSN. */
    public static final String RORD002_RSN = "RORD002_RSN";
    
    /** The constant RORD003_REPORT_NAME. */
    public static final String RORD003_REPORT_NAME = "RORD003_REPORT_NAME";
    
    /** The constant RORD003_ROUTE_NO. */
    public static final String RORD003_ROUTE_NO = "RORD003_ROUTE_NO";
    
    /** The constant RORD003_DEL_NO. */
    public static final String RORD003_DEL_NO = "RORD003_DEL_NO";
    
    /** The constant RORD003_SUPPLIER_CODE. */
    public static final String RORD003_SUPPLIER_CODE = "RORD003_SUPPLIER_CODE";
    
    /** The constant RORD003_SUP_NAME. */
    public static final String RORD003_SUP_NAME = "RORD003_SUP_NAME";
    
    /** The constant RORD003_SUP_LOC. */
    public static final String RORD003_SUP_LOC = "RORD003_SUP_LOC";
    
    /** The constant RORD003_SUP_PLANT. */
    public static final String RORD003_SUP_PLANT = "RORD003_SUP_PLANT";
    
    /** The constant RORD003_ISSUE_DATE. */
    public static final String RORD003_ISSUE_DATE = "RORD003_ISSUE_DATE";
    
    /** The constant RORD003_PAGE. */
    public static final String RORD003_PAGE = "RORD003_PAGE";
    
    /** The constant RORD003_CURRENT_SPS_DO_NO. */
    public static final String RORD003_CURRENT_SPS_DO_NO = "RORD003_CURRENT_SPS_DO_NO";
    
    /** The constant RORD003_ORIGINAL_SPS_DO_NO. */
    public static final String RORD003_ORIGINAL_SPS_DO_NO = "RORD003_ORIGINAL_SPS_DO_NO";
    
    /** The constant RORD003_ETD_SCHEDULE. */
    public static final String RORD003_ETD_SCHEDULE = "RORD003_ETD_SCHEDULE";
    
    /** The constant RORD003_SHIP_DATE. */
    public static final String RORD003_SHIP_DATE = "RORD003_SHIP_DATE";
    
    /** The constant RORD003_TIME. */
    public static final String RORD003_TIME = "RORD003_TIME";
    
    /** The constant RORD003_ETA_SCHEDULE. */
    public static final String RORD003_ETA_SCHEDULE = "RORD003_ETA_SCHEDULE";
    
    /** The constant RORD003_DEL_DATE. */
    public static final String RORD003_DEL_DATE = "RORD003_DEL_DATE";
    
    /** The constant RORD003_PLANT_CODE. */
    public static final String RORD003_PLANT_CODE = "RORD003_PLANT_CODE";
    
    /** The constant RORD003_WARE_HOUSE. */
    public static final String RORD003_WARE_HOUSE = "RORD003_WARE_HOUSE";
    
    /** The constant RORD003_DOCK_CODE. */
    public static final String RORD003_DOCK_CODE = "RORD003_DOCK_CODE";
    
    /** The constant RORD003_RCV_LANE. */
    public static final String RORD003_RCV_LANE = "RORD003_RCV_LANE";
    
    /** The constant RORD003_TRANS. */
    public static final String RORD003_TRANS = "RORD003_TRANS";
    
    /** The constant RORD003_CTRL_NO. */
    public static final String RORD003_CTRL_NO = "RORD003_CTRL_NO";
    
    /** The constant RORD003_D_PART_NUMBER. */
    public static final String RORD003_D_PART_NUMBER = "RORD003_D_PART_NUMBER";
    
    /** The constant RORD003_S_PART_NUMBER. */
    public static final String RORD003_S_PART_NUMBER = "RORD003_S_PART_NUMBER";
    
    /** The constant RORD003_DESCRIPTION. */
    public static final String RORD003_DESCRIPTION = "RORD003_DESCRIPTION";
    
    /** The constant RORD003_U_M. */
    public static final String RORD003_U_M = "RORD003_U_M";
    
    /** The constant RORD003_QUANTITY_BOX. */
    public static final String RORD003_QUANTITY_BOX = "RORD003_QUANTITY_BOX";
    
    /** The constant RORD003_NO_OF_BOX. */
    public static final String RORD003_NO_OF_BOX = "RORD003_NO_OF_BOX";
    
    /** The constant RORD003_PREVIOUS_QTY. */
    public static final String RORD003_PREVIOUS_QTY = "RORD003_PREVIOUS_QTY";
    
    /** The constant RORD003_CURRENT_QTY. */
    public static final String RORD003_CURRENT_QTY = "RORD003_CURRENT_QTY";
    
    /** The constant RORD003_DIFF_QTY. */
    public static final String RORD003_DIFF_QTY = "RORD003_DIFF_QTY";
    
    /** The constant RORD003_RSN. */
    public static final String RORD003_RSN = "RORD003_RSN";
    
    /** The constant RORD003_CURRENT_CIGMA_DO_No. */
    public static final String RORD003_CURRENT_CIGMA_DO_No = "RORD003_CURRENT_CIGMA_DO_No";
    
    /** The constant RORD003_THIS. */
    public static final String RORD003_THIS = "RORD003_THIS";
    
    /** The constant RORD003_AT. */
    public static final String RORD003_AT = "RORD003_AT";
    
    /** The constant RORD003_COMMENT. */
    public static final String RORD003_COMMENT = "RORD003_COMMENT";
    
    /** The constant RORD003_BACK_ORDER. */
    public static final String RORD003_BACK_ORDER = "RORD003_BACK_ORDER";
    
    /** The constant RORD003_WRONG_ORDER. */
    public static final String RORD003_WRONG_ORDER = "RORD003_WRONG_ORDER";
    
    /** The constant RORD003_PROD_SCHEDULE_CHANGE. */
    public static final String RORD003_PROD_SCHEDULE_CHANGE = "RORD003_PROD_SCHEDULE_CHANGE";
    
    /** The constant RORD003_CUST_URGENT_ORDER. */
    public static final String RORD003_CUST_URGENT_ORDER = "RORD003_CUST_URGENT_ORDER";
    
    /** The constant RORD003_QUALITY_PROBLEM. */
    public static final String RORD003_QUALITY_PROBLEM = "RORD003_QUALITY_PROBLEM";
    
    /** The constant RORD003_OTHER. */
    public static final String RORD003_OTHER = "RORD003_OTHER";
    
    /** The constant RORD003_SUPPLIER. */
    public static final String RORD003_SUPPLIER = "RORD003_SUPPLIER";
    
    /** The constant RORD003_SIGNATURE. */
    public static final String RORD003_SIGNATURE = "RORD003_SIGNATURE";
    
    /** The constant RORD004_SIGNATURE. */
    public static final String RORD004_SIGNATURE = "RORD004_SIGNATURE";

    /** The constant RORD004_RELEASE_NO. */
    public static final String RORD004_RELEASE_NO = "RORD004_RELEASE_NO";

    /** The constant RORD004_COVER_PAGE. */
    public static final String RORD004_COVER_PAGE = "RORD004_COVER_PAGE";

    /** The constant RORD004_PRICE_TERM. */
    public static final String RORD004_PRICE_TERM = "RORD004_PRICE_TERM";

    /** The constant RORD004_SPS_PO_NO. */
    public static final String RORD004_SPS_PO_NO = "RORD004_SPS_PO_NO";

    /** The constant RORD004_NAME. */
    public static final String RORD004_NAME = "RORD004_NAME";

    /** The constant RORD004_TO. */
    public static final String RORD004_TO = "RORD004_TO";

    /** The constant RORD004_TITLE. */
    public static final String RORD004_TITLE = "RORD004_TITLE";

    /** The constant RORD004_FAX_NO. */
    public static final String RORD004_FAX_NO = "RORD004_FAX_NO";

    /** The constant RORD004_DATE. */
    public static final String RORD004_DATE = "RORD004_DATE";

    /** The constant RORD004_PAYMENT_TERM. */
    public static final String RORD004_PAYMENT_TERM = "RORD004_PAYMENT_TERM";

    /** The constant RORD004_LABEL9. */
    public static final String RORD004_LABEL9 = "RORD004_LABEL9";

    /** The constant RORD004_LABEL8. */
    public static final String RORD004_LABEL8 = "RORD004_LABEL8";

    /** The constant RORD004_LABEL7. */
    public static final String RORD004_LABEL7 = "RORD004_LABEL7";

    /** The constant RORD004_LABEL6. */
    public static final String RORD004_LABEL6 = "RORD004_LABEL6";

    /** The constant RORD004_LABEL5. */
    public static final String RORD004_LABEL5 = "RORD004_LABEL5";

    /** The constant RORD004_LABEL4. */
    public static final String RORD004_LABEL4 = "RORD004_LABEL4";

    /** The constant RORD004_LABEL3. */
    public static final String RORD004_LABEL3 = "RORD004_LABEL3";

    /** The constant RORD004_LABEL2. */
    public static final String RORD004_LABEL2 = "RORD004_LABEL2";

    /** The constant RORD004_LABEL1. */
    public static final String RORD004_LABEL1 = "RORD004_LABEL1";

    /** The constant RORD004_PIC. */
    public static final String RORD004_PIC = "RORD004_PIC";

    /** The constant RORD004_ISSUED. */
    public static final String RORD004_ISSUED = "RORD004_ISSUED";

    /** The constant RORD004_PAGE. */
    public static final String RORD004_PAGE = "RORD004_PAGE";

    /** The constant RORD004_CIGMA_PO_NO. */
    public static final String RORD004_CIGMA_PO_NO = "RORD004_CIGMA_PO_NO";

    /** The constant RORD004_SHIP_VIA. */
    public static final String RORD004_SHIP_VIA = "RORD004_SHIP_VIA";

    /** The constant RORD004_ACKNOWLEDGEMENT. */
    public static final String RORD004_ACKNOWLEDGEMENT = "RORD004_ACKNOWLEDGEMENT";

    /** The constant RORD004_NON-EDI. */
    public static final String RORD004_NON_EDI = "RORD004_NON_EDI";

    /** The constant RPT_PRM_QR_CODE. */
    public static final String RPT_PRM_QR_CODE = "QR_CODE";

    /** The constant RPT_PRM_QR_REMARK. */
    public static final String RPT_PRM_QR_REMARK = "QR_REMARK";
    
    /** The constant RORD005_REPORT_NAME. */
    public static final String RORD005_REPORT_NAME = "RORD005_REPORT_NAME";
    
    /** The constant RORD005_CYCLE. */
    public static final String RORD005_CYCLE = "RORD005_CYCLE";
    
    /** The constant RORD005_KANBAN_SEQ_NO. */
    public static final String RORD005_KANBAN_SEQ_NO = "RORD005_KANBAN_SEQ_NO";

    /* END REPORT PARAMETER NAME ================================================================ */
    
    /* Start Email Key ========================================================================== */

    /** The constant EMAIL_PENDING_PO_SENDER_NAME. */
    public static final String EMAIL_PENDING_PO_SENDER_NAME = "PENDING_PO_SENDER_NAME";

    /** The constant EMAIL_PENDING_PO_SUBJECT. */
    public static final String EMAIL_PENDING_PO_SUBJECT = "PENDING_PO_SUBJECT";

    /** The constant EMAIL_PENDING_PO_HEADER_TEXT_LINE1. */
    public static final String EMAIL_PENDING_PO_HEADER_TEXT_LINE1 = "PENDING_PO_HEADER_TEXT_LINE1";

    /** The constant EMAIL_PENDING_PO_HEADER_TEXT_LINE2. */
    public static final String EMAIL_PENDING_PO_HEADER_TEXT_LINE2 = "PENDING_PO_HEADER_TEXT_LINE2";

    /** The constant EMAIL_PENDING_PO_HEADER_TEXT_LINE3. */
    public static final String EMAIL_PENDING_PO_HEADER_TEXT_LINE3 = "PENDING_PO_HEADER_TEXT_LINE3";

    /** The constant EMAIL_PENDING_PO_HEADER_TEXT_LINE4. */
    public static final String EMAIL_PENDING_PO_HEADER_TEXT_LINE4 = "PENDING_PO_HEADER_TEXT_LINE4";

    /** The constant EMAIL_PENDING_PO_HEADER_TABLE_LINE1. */
    public static final String EMAIL_PENDING_PO_HEADER_TABLE_LINE1
        = "PENDING_PO_HEADER_TABLE_LINE1";

    /** The constant EMAIL_PENDING_PO_HEADER_TABLE_LINE2. */
    public static final String EMAIL_PENDING_PO_HEADER_TABLE_LINE2
        = "PENDING_PO_HEADER_TABLE_LINE2";

    /** The constant EMAIL_PENDING_PO_HEADER_TABLE_LINE3. */
    public static final String EMAIL_PENDING_PO_HEADER_TABLE_LINE3
        = "PENDING_PO_HEADER_TABLE_LINE3";

    /** The constant EMAIL_PENDING_PO_DETAIL_TABLE_LINE1. */
    public static final String EMAIL_PENDING_PO_DETAIL_TABLE_LINE1 = "PENDING_PO_DETAIL_TABLE_LINE1";
    
    /** The constant EMAIL_PENDING_PO_DETAIL_TABLE_LINE2. */
    public static final String EMAIL_PENDING_PO_DETAIL_TABLE_LINE2 = "PENDING_PO_DETAIL_TABLE_LINE2";

    /** The constant EMAIL_PENDING_PO_FOOTER_LINE1. */
    public static final String EMAIL_PENDING_PO_FOOTER_LINE1 = "PENDING_PO_FOOTER_LINE1";

    /** The constant EMAIL_PENDING_PO_FOOTER_LINE2. */
    public static final String EMAIL_PENDING_PO_FOOTER_LINE2 = "PENDING_PO_FOOTER_LINE2";
    
    
    // Start : [IN054] For Reply Pending P/O email
    /** The constant EMAIL_REPLY_PO_SENDER_NAME. */
    public static final String EMAIL_REPLY_PO_SENDER_NAME = "REPLY_PO_SENDER_NAME";

    /** The constant EMAIL_REPLY_PO_SUBJECT. */
    public static final String EMAIL_REPLY_PO_SUBJECT = "REPLY_PO_SUBJECT";

    /** The constant EMAIL_REPLY_PO_HEADER_TEXT_LINE1. */
    public static final String EMAIL_REPLY_PO_HEADER_TEXT_LINE1 = "REPLY_PO_HEADER_TEXT_LINE1";

    /** The constant EMAIL_REPLY_PO_HEADER_TEXT_LINE2. */
    public static final String EMAIL_REPLY_PO_HEADER_TEXT_LINE2 = "REPLY_PO_HEADER_TEXT_LINE2";

    /** The constant EMAIL_REPLY_PO_HEADER_TEXT_LINE3. */
    public static final String EMAIL_REPLY_PO_HEADER_TEXT_LINE3 = "REPLY_PO_HEADER_TEXT_LINE3";

    /** The constant EMAIL_REPLY_PO_HEADER_TEXT_LINE4. */
    public static final String EMAIL_REPLY_PO_HEADER_TEXT_LINE4 = "REPLY_PO_HEADER_TEXT_LINE4";

    /** The constant EMAIL_REPLY_PO_HEADER_TABLE_LINE1. */
    public static final String EMAIL_REPLY_PO_HEADER_TABLE_LINE1
        = "REPLY_PO_HEADER_TABLE_LINE1";

    /** The constant EMAIL_REPLY_PO_HEADER_TABLE_LINE2. */
    public static final String EMAIL_REPLY_PO_HEADER_TABLE_LINE2
        = "REPLY_PO_HEADER_TABLE_LINE2";

    /** The constant EMAIL_REPLY_PO_HEADER_TABLE_LINE3. */
    public static final String EMAIL_REPLY_PO_HEADER_TABLE_LINE3
        = "REPLY_PO_HEADER_TABLE_LINE3";

    /** The constant EMAIL_REPLY_PO_DETAIL_TABLE_LINE1. */
    public static final String EMAIL_REPLY_PO_DETAIL_TABLE_LINE1 = "REPLY_PO_DETAIL_TABLE_LINE1";
    
    /** The constant EMAIL_REPLY_PO_DETAIL_TABLE_LINE2. */
    public static final String EMAIL_REPLY_PO_DETAIL_TABLE_LINE2 = "REPLY_PO_DETAIL_TABLE_LINE2";

    /** The constant EMAIL_REPLY_PO_FOOTER_LINE1. */
    public static final String EMAIL_REPLY_PO_FOOTER_LINE1 = "REPLY_PO_FOOTER_LINE1";

    /** The constant EMAIL_REPLY_PO_FOOTER_LINE2. */
    public static final String EMAIL_REPLY_PO_FOOTER_LINE2 = "REPLY_PO_FOOTER_LINE2";
    // End : [IN054] For Reply Pending P/O email

    /** The constant EMAIL_ASN_MAINTENANCE_SENDER_NAME. */
    public static final String EMAIL_ASN_MAINTENANCE_SENDER_NAME = "ASN_MAINTENANCE_SENDER_NAME";
    
    /** The constant EMAIL_ASN_MAINTENANCE_SUBJECT. */
    public static final String EMAIL_ASN_MAINTENANCE_SUBJECT_CREATE
        = "ASN_MAINTENANCE_SUBJECT_CREATE";
    
    /** The constant EMAIL_ASN_MAINTENANCE_SUBJECT_UPDATE. */
    public static final String EMAIL_ASN_MAINTENANCE_SUBJECT_UPDATE
        = "ASN_MAINTENANCE_SUBJECT_UPDATE";
    
    /** The constant EMAIL_ASN_MAINTENANCE_SUBJECT_CANCEL. */
    public static final String EMAIL_ASN_MAINTENANCE_SUBJECT_CANCEL
        = "ASN_MAINTENANCE_SUBJECT_CANCEL";
    
    /** The constant EMAIL_ASN_MAINTENANCE_SUBJECT_ALLOW. */
    public static final String EMAIL_ASN_MAINTENANCE_SUBJECT_ALLOW
        = "ASN_MAINTENANCE_SUBJECT_ALLOW";
    
    /** The constant EMAIL_ASN_MAINTENANCE_SUBJECT_DISALLOW. */
    public static final String EMAIL_ASN_MAINTENANCE_SUBJECT_DISALLOW
        = "ASN_MAINTENANCE_SUBJECT_DISALLOW";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE1. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE1
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE1";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CREATE. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CREATE
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE2_CREATE";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_UPDATE. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_UPDATE
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE2_UPDATE";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CANCEL. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_CANCEL
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE2_CANCEL";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_ALLOW. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_ALLOW
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE2_ALLOW";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_DISALLOW. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE2_DISALLOW
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE2_DISALLOW";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE3. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE3
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE3";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE4. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TEXT_LINE4
        = "ASN_MAINTENANCE_HEADER_TEXT_LINE4";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE1. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE1
        = "ASN_MAINTENANCE_HEADER_TABLE_LINE1";
    
    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE2. */
    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE2
        = "ASN_MAINTENANCE_HEADER_TABLE_LINE2";
    
//    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE3. */
//    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE3 = "ASN_MAINTENANCE_HEADER_TABLE_LINE3";
//    
//    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE4. */
//    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE4 = "ASN_MAINTENANCE_HEADER_TABLE_LINE4";
//    
//    /** The constant EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE5. */
//    public static final String EMAIL_ASN_MAINTENANCE_HEADER_TABLE_LINE5 = "ASN_MAINTENANCE_HEADER_TABLE_LINE5";
    
    /** The constant EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE1. */
    public static final String EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE1
        = "ASN_MAINTENANCE_DETAIL_TABLE_LINE1";
    
    /** The constant EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE2. */
    public static final String EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE2
        = "ASN_MAINTENANCE_DETAIL_TABLE_LINE2";
    
    /** The constant EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE3. */
    public static final String EMAIL_ASN_MAINTENANCE_DETAIL_TABLE_LINE3
        = "ASN_MAINTENANCE_DETAIL_TABLE_LINE3";
    
    /** The constant EMAIL_ASN_MAINTENANCE_REMARK. */
    public static final String EMAIL_ASN_MAINTENANCE_REMARK
        = "ASN_MAINTENANCE_REMARK";
    
    /** The constant EMAIL_ASN_MAINTENANCE_REMARK_ALLOW. */
    public static final String EMAIL_ASN_MAINTENANCE_REMARK_ALLOW
        = "ASN_MAINTENANCE_REMARK_ALLOW";
    
    /** The constant EMAIL_ASN_MAINTENANCE_REMARK_DISALLOW. */
    public static final String EMAIL_ASN_MAINTENANCE_REMARK_DISALLOW
        = "ASN_MAINTENANCE_REMARK_DISALLOW";
    
    /** The constant EMAIL_ASN_MAINTENANCE_FOOTER_LINE1. */
    public static final String EMAIL_ASN_MAINTENANCE_FOOTER_LINE1 = "ASN_MAINTENANCE_FOOTER_LINE1";
    
    /** The constant EMAIL_ASN_MAINTENANCE_FOOTER_LINE2. */
    public static final String EMAIL_ASN_MAINTENANCE_FOOTER_LINE2 = "ASN_MAINTENANCE_FOOTER_LINE2";
    
    /* End Email Key ============================================================================ */
    
    /* Replace ================================================================================== */

    /** The constant REPLACE_SUPPLIER_CODE. */
    public static final String REPLACE_SUPPLIER_CODE = "#supplierCode#";

    /** The constant REPLACE_SUPPLIER_PLANT_CODE. */
    public static final String REPLACE_SUPPLIER_PLANT_CODE = "#supplierPlantCode#";

    /** The constant REPLACE_DENSO_CODE. */
    public static final String REPLACE_DENSO_CODE = "#densoCode#";

    /** The constant REPLACE_DENSO_PLANT_CODE. */
    public static final String REPLACE_DENSO_PLANT_CODE = "#densoPlantCode#";
    
    /** The constant REPLACE_DOCKCODE. */
    public static final String REPLACE_DOCKCODE = "#dockCode#";

    /** The constant REPLACE_ISSUE_DATE. */
    public static final String REPLACE_ISSUE_DATE = "#issueDate#";
    
    /** The constant REPLACE_CIGMADONO. */
    public static final String REPLACE_CIGMADONO = "#cigmaDoNo#";
    
    /** The constant REPLACE_CIGMAPONO. */
    public static final String REPLACE_CIGMAPONO = "#cigmaPONo#";

    /** The constant REPLACE_SPSPONO. */
    public static final String REPLACE_SPSPONO = "#spsPONo#";

    /** The constant REPLACE_DPARTNO. */
    public static final String REPLACE_DPARTNO = "#dPartNo#";

    /** The constant REPLACE_SPARTNO. */
    public static final String REPLACE_SPARTNO = "#sPartNo#";
//   
    /** The constant REPLACE_DUEDATE. */
    public static final String REPLACE_DUEDATE = "#dueDate#";

    /** The constant REPLACE_ORDERQTY. */
    public static final String REPLACE_ORDERQTY = "#orderQty#";

    /** The constant REPLACE_SPSPROPOSED_DUDATE. */
    public static final String REPLACE_SPSPROPOSED_DUDATE = "#spsProposedDueDate#";

    /** The constant REPLACE_SPSPROPOSED_QTY. */
    public static final String REPLACE_SPSPROPOSED_QTY = "#spsProposedQty#";

    /** The constant REPLACE_SPSPENDING_REASON. */
    public static final String REPLACE_SPSPENDING_REASON = "#spsPendingReason#";
    
    /** The constant REPLACE_UNITOFMEASURE. */
    public static final String REPLACE_UNITOFMEASURE = "#unitOfMeasure#";
    
    /** The constant REPLACE_USERNAME. */
    public static final String REPLACE_USERNAME = "#userName#";

    /** The constant REPLACE_MODEL. */
    public static final String REPLACE_MODEL = "#model#";

    /** The constant REPLACE_PLANNERCODE. */
    public static final String REPLACE_PLANNERCODE = "#plannerCode#";
    
    /** The constant REPLACE_ASNNO. */
    public static final String REPLACE_ASNNO = "#asnNo#";
    
    /** The constant REPLACE_ACTUAL_ETD. */
    public static final String REPLACE_ACTUAL_ETD = "#actualEtd#";
    
    /** The constant REPLACE_PLAN_ETA. */
    public static final String REPLACE_PLAN_ETA = "#planEta#";
    
    /** The constant REPLACE_NOOFPALLET. */
    public static final String REPLACE_NOOFPALLET = "#noOfPallet#";
    
    /** The constant REPLACE_TRIPNO. */
    public static final String REPLACE_TRIPNO = "#tripNo#";
    
    /** The constant REPLACE_ROWNO. */
    public static final String REPLACE_ROWNO = "#rowNo#";
    
    /** The constant REPLACE_RCVLANE. */
    public static final String REPLACE_RCVLANE = "#rcvLane#";
    
    /** The constant REPLACE_SPSDONO. */
    public static final String REPLACE_SPSDONO = "#spsDONo#";
    
    /** The constant REPLACE_REVISION. */
    public static final String REPLACE_REVISION = "#revision#";
    
    /** The constant REPLACE_RSN. */
    public static final String REPLACE_RSN = "#rsn#";
    
    /** The constant REPLACE_CURRENT_QTY. */
    public static final String REPLACE_CURRENT_QTY = "#currentQty#";
    
    /** The constant REPLACE_DIFFERENCE_QTY. */
    public static final String REPLACE_DIFFERENCE_QTY = "#differenceQty#";
    
    /** The constant REPLACE_SHIPPING_QTY. */
    public static final String REPLACE_SHIPPING_QTY = "#shippingQty#";
    
    /** The constant REPLACE_PREVIOUS_QTY. */
    public static final String REPLACE_PREVIOUS_QTY = "#previousQty#";
    
    /** The constant REPLACE_UM. */
    public static final String REPLACE_UM = "#unitOfMeasure#";
    
    /** The constant REPLACE_QTYBOX. */
    public static final String REPLACE_QTYBOX = "#qtyBox#";
    
    /** The constant REPLACE_SHIPPINGBOX_QTY. */
    public static final String REPLACE_SHIPPINGBOX_QTY = "#shippingBoxQty#";
    
    /** The constant REPLACE_CHANGE_REASON. */
    public static final String REPLACE_CHANGE_REASON = "#changeReason#";
    
    /** The constant REPLACE_SCD. */
    public static final String REPLACE_SCD = "#supplierCode#";
    
    /** The constant REPLACE_SPCD. */
    public static final String REPLACE_SPCD = "#supplierPlantCode#";
    
    /** The constant REPLACE_DCD. */
    public static final String REPLACE_DCD = "#densoCode#";
    
    /** The constant REPLACE_DPCD. */
    public static final String REPLACE_DPCD = "#densoPlantCode#";
    
    /** The constant REPLACE_LAST_MODIFIED. */
    public static final String REPLACE_LAST_MODIFIED = "#lastModified#";
    
    /** The constant REPLACE_WH. */
    public static final String REPLACE_WH = "#wh#";

    /** The constant REPLACE_LAST_MODIFIED. */
    public static final String REPLACE_CHG_CIGMA_DO_NO = "#chgCigmaDoNo#";
    
    public static final String REPLACE_PN_SHIPMENT_STATUS = "#pnShipmentStatus#";
    
    /** The constant CN_DN_DOWNLOADING_SQL. */
    public static final String CN_DN_DOWNLOADING_SQL = "CREATE_DATETIME desc";

    /** The constant REPLACE_COVER_PAGE_NO. */
    public static final String REPLACE_COVER_PAGE_NO = "#coverPageNo#";
    
    /** The constant REPLACE_INVOICE_NO. */
    public static final String REPLACE_INVOICE_NO = "#invoiceNo#";
    
    /** The constant REPLACE_INVOICE_DATE. */
    public static final String REPLACE_INVOICE_DATE = "#invoiceDate#";
    
    /** The constant REPLACE_TOTAL_INVOICE_AMOUNT. */
    public static final String REPLACE_TOTAL_INVOICE_AMOUNT = "#totalInvoiceAmount#";
    
    /** The constant REPLACE_CN_NO. */
    public static final String REPLACE_CN_NO = "#cnNo#";
    
    /** The constant REPLACE_CN_DATE. */
    public static final String REPLACE_CN_DATE = "#cnDate#";
    
    /** The constant REPLACE_TOTAL_CN_AMOUNT. */
    public static final String REPLACE_TOTAL_CN_AMOUNT = "#totalCnAmount#";
    
    /** The constant REPLACE_FIRST_NAME. */
    public static final String REPLACE_FIRST_NAME = "#firstName#";
    
    /** The constant REPLACE_MIDDLE_NAME. */
    public static final String REPLACE_MIDDLE_NAME = "#middleName#";
    
    /** The constant REPLACE_LAST_NAME. */
    public static final String REPLACE_LAST_NAME = "#lastName#";
    
    /** The constant REPLACE_GOOD_RCV_STATUS. */
    public static final String REPLACE_GOOD_RCV_STATUS = "#goodRcvStatus#";
    
    /** The constant REPLACE_D_UNIT_PRICE. */
    public static final String REPLACE_D_UNIT_PRICE = "#dUnitPrice#";
    
    /** The constant REPLACE_S_UNIT_PRICE. */
    public static final String REPLACE_S_UNIT_PRICE = "#sUnitPrice#";
    
    /** The constant REPLACE_TEMP_PRICE. */
    public static final String REPLACE_TEMP_PRICE = "#tempPrice#";
    
    /** The constant REPLACE_D_UNIT_OF_MEASURE. */
    public static final String REPLACE_D_UNIT_OF_MEASURE = "#dUnitOfMeasure#";
    
    /** The constant REPLACE_DIFF_BASE_AMT_BY_PART. */
    public static final String REPLACE_DIFF_BASE_AMT_BY_PART = "#diffBaseAmtByPart#";
    
    /** The constant REPLACE_ROW_COLOR. */
    public static final String REPLACE_ROW_COLOR = "#rowColor#";

    /** The constant REPLACE_TRANSFER_PROCESS. */
    public static final String REPLACE_TRANSFER_PROCESS = "#transferProcess#";

    // Start : [IN054] new column email
    /** The constant REPLACE_DENSO_REPLY. */
    public static final String REPLACE_DENSO_REPLY = "#densoReply#";

    /** The constant REPLACE_DENSO_REPLY. */
    public static final String REPLACE_REPLY_COLOR = "#replyColor#";
    // End : [IN054] new column email
    
    /* End Replace ============================================================================== */

    /* LENGTH =================================================================================== */
    
    /** The constants LENGTH_ASN_NO. */
    public static final int LENGTH_ASN_NO = 16;
    
    /** The constants LENGTH_CIGMA_PO_NO. */
    public static final int LENGTH_CIGMA_PO_NO = 10;

    /** The constants LENGTH_CIGMA_DO_NO. */
    public static final int LENGTH_CIGMA_DO_NO = 10;
  
    /** The constants LENGTH_DENSO_CODE. */
    public static final int LENGTH_DENSO_CODE = 5;
  
    /** The constants LENGTH_DENSO_CODE. */
    public static final int LENGTH_DENSO_COMPANY_CODE = 4;
    
    /** The constants LENGTH_DENSO_PART_NO. */
    public static final int LENGTH_DENSO_PART_NO = 15;
    
    /** The constants LENGTH_DENSO_PLANT_CODE. */
    public static final int LENGTH_DENSO_PLANT_CODE = 2;
    
    /** The constants LENGTH_RCV_LANE. */
    public static final int LENGTH_RCV_LANE = 13;
    
    /** The constants LENGTH_RCV_LANE. */
    public static final int LENGTH_RCV_LANE_COUNT = 3;
    
    /** The constants LENGTH_REVISION. */
    public static final int LENGTH_REVISION = 2;
    
    /** The constants LENGTH_SUPPLIER_CODE. */
    public static final int LENGTH_SUPPLIER_CODE = 6;
    
    /** The constants LENGTH_SUPPLIER_PART_NO. */
    public static final int LENGTH_SUPPLIER_PART_NO = 20;
    
    /** The constants LENGTH_SUPPLIER_PLANT_CODE. */
    public static final int LENGTH_SUPPLIER_PLANT_CODE = 1;
    
    /** The constants LENGTH_SPS_DO_NO. */
    public static final int LENGTH_SPS_DO_NO = 11;
    
    /** The constants LENGTH_SPS_PO_NO. */
    public static final int LENGTH_SPS_PO_NO = 13;
    
    /** The constants LENGTH_TRIP_NO. */
    public static final int LENGTH_TRIP_NO = 3;
    
    /** The constants LENGTH_UM. */
    public static final int LENGTH_UM = 2;
    
    /** The constants LENGTH_VENDOR_CD. */
    public static final int LENGTH_VENDOR_CD = 6;

    /** The constants LENGTH_CIGMA_VENDOR_CD. */
    public static final int LENGTH_CIGMA_VENDOR_CD = 6;

    /** The constants LENGTH_PLANT_CODE. */
    public static final int LENGTH_PLANT_CODE = 2;

    /** The constants LENGTH_ETD. */
    public static final int LENGTH_ETD = 8;

    /** The constants LENGTH_ISSUE_DATE. */
    public static final int LENGTH_ISSUE_DATE = 8;

    /** The constants LENGTH_DPN. */
    public static final int LENGTH_DPN = 15;

    /** The constants LENGTH_QTY. */
    public static final int LENGTH_QTY = 13;

    /** The constants LENGTH_QTY_FLOATING. */
    public static final int LENGTH_QTY_FLOATING = 2;

    /** The constants LENGTH_TM. */
    public static final int LENGTH_TM = 1;

    /** The constants LENGTH_TM. */
    public static final int LENGTH_TOTAL_QR_CODE_ASN = 1;

    /** The constants LENGTH_TM. */
    public static final int LENGTH_ORDER_QR_CODE_ASN = 1;

    /** The constants LENGTH_WAREHOUSE. */
    public static final int LENGTH_WAREHOUSE = 1;

    /** The constants LENGTH_KANBAN_TYPE. */
    public static final int LENGTH_KANBAN_TYPE = 2;

    /** The constants LENGTH_KANBAN_TYPE. */
    public static final int LENGTH_SHIPPING_QTY = 9;

    /** The constants LENGTH_KANBAN_TYPE. */
    public static final int LENGTH_IS_PARTIAL = 1;

    /** The constants LENGTH_TAG_SEQ. */
    public static final int LENGTH_TAG_SEQ = 7;

    /** The constants LENGTH_TAG_SEQ. */
    public static final int LENGTH_ALLOCATE_QTY = 9;

    /** The constants LENGTH_TAG_SEQ. */
    public static final int LENGTH_APPEND_RECEIVE_DATA_MSG = 81;
    
    /* End LENGTH =============================================================================== */

    /* URL ====================================================================================== */

    /** The constants URL_WORD002_ACTION. */
    public static final String URL_WORD002_ACTION = "/PurchaseOrderInformationAction.do?";
    
    /** The constants URL_WORD003_ACTION. */
    public static final String URL_WORD003_ACTION = "/PurchaseOrderAcknowledgementAction.do?";

    /** The constants URL_WORD004_ACTION. */
    public static final String URL_WORD004_ACTION = "/SupplierPromisedDueSubmittionAction.do?";
    
    /** The constants URL_WORD005_ACTION. */
    public static final String URL_WORD005_ACTION = "/PurchaseOrderNotificationAction.do?";
    
    /** The constants URL_WSHP001_ACTION. */
    public static final String URL_WSHP001_ACTION = "/AcknowledgedDoInformationAction.do?";
    
    /** The constants URL_WSHP003_ACTION. */
    public static final String URL_WSHP003_ACTION = "/InquiryDoDetailAction.do?";
    
    /** The constants URL_WSHP004_ACTION. */
    public static final String URL_WSHP004_ACTION = "/AsnMaintenanceAction.do?";
    
    /** The constants URL_WSHP006_ACTION. */
    public static final String URL_WSHP006_ACTION = "/AsnUploadingAction.do?";
    
    /** The constants URL_WSHP007_ACTION. */
    public static final String URL_WSHP007_ACTION = "/BackOrderInformationAction.do?";
    
    /** The constants URL_WSHP008_ACTION. */
    public static final String URL_WSHP008_ACTION = "/AsnDetailInformationAction.do?";
    
    /** The constants URL_WSHP009_ACTION. */
    public static final String URL_WSHP009_ACTION = "/AsnDetailProgressAction.do?";
    
    /** The constants URL_WINV001_ACTION. */
    public static final String URL_WINV001_ACTION = "/InvoiceInformationAction.do?";
    
    /** The constants URL_WINV002_ACTION. */
    public static final String URL_WINV002_ACTION = "/AsnInformationAction.do?";
    
    /** The constants URL_WINV003_ACTION. */
    public static final String URL_WINV003_ACTION = "/InvoiceInformationUploadingAction.do?";
    
    /** The constants URL_WINV004_ACTION. */
    public static final String URL_WINV004_ACTION = "/InvoiceMaintenanceAction.do?";
    
    /** The constants URL_WINV005_ACTION. */
    public static final String URL_WINV005_ACTION = "/PriceDifferenceInformationAction.do?";
    
    /** The constants URL_WINV006_ACTION. */
    public static final String URL_WINV006_ACTION = "/CnDnUploadingAction.do?";
    
    /** The constants URL_WADM001_ACTION. */
    public static final String URL_WADM001_ACTION = "/SupplierUserInformationAction.do?";
    
    /** The constants URL_WADM002_ACTION. */
    public static final String URL_WADM002_ACTION = "/SupplierUserRegistrationAction.do?";
    
    /** The constants URL_WADM004_ACTION. */
    public static final String URL_WADM004_ACTION = "/SupplierUserRoleAssignmentAction.do?";
    
    /** The constants URL_WADM005_ACTION. */
    public static final String URL_WADM005_ACTION = "/DensoUserInformationAction.do?";
    
    /** The constants URL_WADM006_ACTION. */
    public static final String URL_WADM006_ACTION = "/DensoUserRegistrationAction.do?";
    
    /** The constants URL_WADM008_ACTION. */
    public static final String URL_WADM008_ACTION = "/DensoUserRoleAssignmentAction.do?";
    
    /** The constants URL_PARAM_METHOD. */
    public static final String URL_PARAM_METHOD = "method=";
    
    /** The constants URL_PARAM_MODE_EDIT. */
    public static final String URL_PARAM_MODE_EDIT = "&mode=Edit";
    
    /** The constants URL_PARAM_MODE_EDIT. */
    public static final String URL_PARAM_RETURN_MODE = "&returnMode=";
    
    /** The constants URL_PARAM_ACTUAL_ETD_FROM. */
    public static final String URL_PARAM_ACTUAL_ETD_FROM = "&actualEtdFrom=";
    
    /** The constants URL_PARAM_ACTUAL_ETD_TO. */
    public static final String URL_PARAM_ACTUAL_ETD_TO = "&actualEtdTo=";
    
    /** The constants URL_PARAM_ASNNO. */
    public static final String URL_PARAM_ASNNO = "&asnNo=";
    
    /** The constants URL_PARAM_DEL. */
    public static final String URL_PARAM_DEL = "&del=";
    
    /** The constants URL_PARAM_DCD. */
    public static final String URL_PARAM_DCD = "&DCd=";
    
    /** The constants URL_PARAM_DPCD. */
    public static final String URL_PARAM_DPCD = "&DPcd=";
    
    /** The constants URL_PARAM_DSCID. */
    public static final String URL_PARAM_DSCID = "&dscId=";
    
    /** The constants URL_PARAM_DOID. */
    public static final String URL_PARAM_DOID = "&doId=";
    
    /** The constants URL_PARAM_FIRSTPAGES. */
    public static final String URL_PARAM_FIRSTPAGES = "&firstPages=";
    
    /** The constants URL_PARAM_FIRSTNAME. */
    public static final String URL_PARAM_FIRSTNAME = "&firstName=";
    
    /** The constants URL_PARAM_MIDDLENAME. */
    public static final String URL_PARAM_MIDDLENAME = "&middleName=";
    
    /** The constants URL_PARAM_LASTNAME. */
    public static final String URL_PARAM_LASTNAME = "&lastName=";
    
    /** The constants URL_PARAM_REGISTER_DATE_FROM. */
    public static final String URL_PARAM_REGISTER_DATE_FROM = "&registerDateFrom=";
    
    /** The constants URL_PARAM_REGISTER_DATE_TO. */
    public static final String URL_PARAM_REGISTER_DATE_TO = "&registerDateTo=";
    
    /** The constants URL_PARAM_POID. */
    public static final String URL_PARAM_POID = "&poId=";
    
    /** The constants URL_PARAM_PERIODTYPE. */
    public static final String URL_PARAM_PERIODTYPE = "&periodType=";
    
    /** The constants URL_PARAM_PERIODTYPENAME. */
    public static final String URL_PARAM_PERIODTYPENAME = "&periodTypeName=";
    
    /** The constants URL_PARAM_POTYPE. */
    public static final String URL_PARAM_POTYPE = "&poType=";
    
    /** The constants URL_PARAM_PREV_SCREENID. */
    public static final String URL_PARAM_PREV_SCREENID = "&prevScreenId=";
    
    /** The constants URL_PARAM_REVISION. */
    public static final String URL_PARAM_REVISION = "&revision=";
    
    /** The constants URL_PARAM_ROUTENO. */
    public static final String URL_PARAM_ROUTENO = "&routeNo=";
    
    /** The constants URL_PARAM_SCREENID. */
    public static final String URL_PARAM_SCREENID = "&screenId=";
    
    /** The constants URL_PARAM_SESSIONID. */
    public static final String URL_PARAM_SESSIONID = "&sessionId=";
    
    /** The constants URL_PARAM_SPSDONO. */
    public static final String URL_PARAM_SPSDONO = "&spsDoNo=";
    
    /** The constants URL_PARAM_SPSPONO. */
    public static final String URL_PARAM_SPSPONO = "&spsPoNo=";
    
    /** The constants URL_PARAM_SCD. */
    public static final String URL_PARAM_SCD = "&SCd=";
    
    /** The constants URL_PARAM_SPCD. */
    public static final String URL_PARAM_SPCD = "&SPcd=";
    
    /** The constants URL_PARAM_SUPPLIER_PLANT_TAX_ID. */
    public static final String URL_PARAM_SUPPLIER_PLANT_TAX_ID = "&supplierPlantTaxId=";
    
    /** The constants URL_PARAM_TEMPORARY_MODE. */
    public static final String URL_PARAM_TEMPORARY_MODE = "&temporaryMode=";
    
    /** The constants URL_PARAM_VENDOR_CD. */
    public static final String URL_PARAM_VENDOR_CD = "&vendorCd=";
    
    /** The constants URL_PARAM_POTYPE. */
    public static final String URL_X_WWW_FORM_URLENCODED = "x-www-form-urlencoded=";
    
    /** The constants URL_PARAM_ISRETURN. */
    public static final String URL_PARAM_ISRETURN = "&isReturn=";
    
    /** The constants URL_PARAM_METHOD. */
    public static final String URL_PARAM_MODE = "&mode=";
    
    /** The constants URL_PARAM_ACTIONMODE. */
    public static final String URL_PARAM_ACTIONMODE = "&actionMode=";
    
    /** The constants URL_PARAM_SPARTNO. */
    public static final String URL_PARAM_SPARTNO = "&SPn=";
    
    /** The constants URL_PARAM_DPARTNO. */
    public static final String URL_PARAM_DPARTNO = "&DPn=";
    
    /** The constants URL_PARAM_INVOICE_ID. */
    public static final String URL_PARAM_INVOICE_ID = "&invoiceId=";
    
    /** The constants URL_PARAM_INVOICE_DATE. */
    public static final String URL_PARAM_INVOICE_DATE = "&invoiceDate=";
    
    /** The constants URL_PARAM_INVOICE_DATE_TO. */
    public static final String URL_PARAM_INVOICE_DATE_TO = "&invoiceDateTo=";
    
    /** The constants URL_PARAM_INVOICE_RCV_STATUS. */
    public static final String URL_PARAM_INVOICE_RCV_STATUS = "&invoiceRCVStatus=";
    
    /** The constants URL_PARAM_INVOICE_NO. */
    public static final String URL_PARAM_INVOICE_NO = "&invoiceNo=";
    
    /** The constants URL_PARAM_CN_NO. */
    public static final String URL_PARAM_CN_NO = "&cnNo=";
    
    /** The constants URL_PARAM_CN_DATE. */
    public static final String URL_PARAM_CN_DATE = "&cnDate=";
    
    /** The constants URL_PARAM_WARNING_MESSAGE. */
    public static final String URL_PARAM_WARNING_MESSAGE = "&warningMessage=";
    
    /** The constants URL_PARAM_COMPANY_NAME. */
    public static final String URL_PARAM_COMPANY_NAME = "&companyName=";
    
    /** The Parameter name for Application Message messageType. */
    public static final String URL_PRM_APPLICATION_MESSAGE_TYPE
        = "&applicationMessageItem[0].messageType";
    
    /** The Parameter name for Application Message message. */
    public static final String URL_PRM_APPLICATION_MESSAGE_MESSAGE
        = "&applicationMessageItem[0].message";
    
    /** The constants URL_LOGIN */
    public static final String URL_LOGIN = "/index.html";
    
    /* End URL ================================================================================== */

    /* ACTION METHOD MAP ======================================================================== */

    /** The constants ACTION_RESET. */
    public static final String ACTION_RESET = "action.RESET";

    /** The constants ACTION_SEARCH. */
    public static final String ACTION_SEARCH = "action.SEARCH";
    
    /** The constants ACTION_EXPORT. */
    public static final String ACTION_EXPORT = "action.EXPORT";

    /** The constants ACTION_ACKNOWLEDGE. */
    public static final String ACTION_ACKNOWLEDGE = "action.ACKNOWLEDGE";
    
    /** The constants ACTION_DELETE. */
    public static final String ACTION_DELETE = "action.DELETE";
    
    /** The constants ACTION_DOWNLOAD. */
    public static final String ACTION_DOWNLOAD = "action.DOWNLOAD";
    
    /** The constants ACTION_LINK. */
    public static final String ACTION_LINK = "action.LINK";
    
    /** The constants ACTION_LINK_ASNNO. */
    public static final String ACTION_LINK_ASNNO = "action.LINK_ASNNO";
    
    /** The constants ACTION_LINK_DSC_ID. */
    public static final String ACTION_LINK_DSC_ID = "action.LINK_DSC_ID";
    
    /** The constants ACTION_LINK_ROLE. */
    public static final String ACTION_LINK_ROLE = "action.LINK_ROLE";
    
    /** The constants ACTION_LINK_SPSDONO. */
    public static final String ACTION_LINK_SPSDONO = "action.LINK_SPSDONO";
    
    /** The constants ACTION_LOGIN. */
    public static final String ACTION_LOGIN = "action.LOGIN";
    
    /** The constants ACTION_LOGOUT. */
    public static final String ACTION_LOGOUT = "action.LOGOUT";
    
    /** The constants ACTION_CHANGE_LOCALE. */
    public static final String ACTION_CHANGE_LOCALE = "action.CHANGE_LOCALE";
    
    /** The constants ACTION_SAVE_AND_SEND. */
    public static final String ACTION_SAVE_AND_SEND = "action.SAVE_AND_SEND";
    
    /** The constants ACTION_CANCEL_AND_SEND. */
    public static final String ACTION_CANCEL_AND_SEND = "action.CANCEL_AND_SEND";
    
    /** The constants ACTION_DOWNLOAD_ONE_WAY_KANBAN_PDF. */
    public static final String ACTION_DOWNLOAD_ONE_WAY_KANBAN_PDF = "action.DOWNLOAD_ONE_WAY_KANBAN_PDF"; 
    
    /** The constants ACTION_DOWNLOAD_PDF. */
    public static final String ACTION_DOWNLOAD_PDF = "action.DOWNLOAD_PDF"; 
    
    /** The constants ACTION_DOWNLOAD_PDF_ORIGINAL. */
    public static final String ACTION_DOWNLOAD_PDF_ORIGINAL = "action.DOWNLOAD_PDF_ORIGINAL";

    /** The constants ACTION_DOWNLOAD_PDF_CHANGE. */
    public static final String ACTION_DOWNLOAD_PDF_CHANGE = "action.DOWNLOAD_PDF_CHANGE";
    
    /** The constants ACTION_DOWNLOAD_PDF_PREVIEW. */
    public static final String ACTION_DOWNLOAD_PDF_PREVIEW = "action.DOWNLOAD_PDF_PREVIEW";

    /** The constants ACTION_DOWNLOAD_LEGEND_FILE. */
    public static final String ACTION_DOWNLOAD_LEGEND_FILE = "action.DOWNLOAD_LEGEND_FILE";
    
    /** The constants ACTION_RETURN. */
    public static final String ACTION_RETURN = "action.RETURN";
    
    /** The constants ACTION_UPDATE. */
    public static final String ACTION_UPDATE = "action.UPDATE";
    
    /** The constants ACTION_REGISTER. */
    public static final String ACTION_REGISTER = "action.REGISTER";
    
    /** The constants ACTION_MORE. */
    public static final String ACTION_MORE = "action.MORE";
    
    /** The constants ACTION_GROUP_ASN. */
    public static final String ACTION_GROUP_ASN = "action.GROUP_ASN";
    
    /** The constants ACTION_ADD_USER. */
    public static final String ACTION_ADD_USER = "action.ADD_USER";
    
    /** The constants ACTION_UPDATE_USER. */
    public static final String ACTION_UPDATE_USER = "action.UPDATE_USER";

    /** The constants ACTION_PREVIEW. */
    public static final String ACTION_PREVIEW = "action.PREVIEW";
    
    /** The constants ACTION_PREVIEWPENDING. */
    public static final String ACTION_PREVIEWPENDING = "action.PREVIEWPENDING";
    
    /** The constants ACTION_UPLOAD. */
    public static final String ACTION_UPLOAD = "action.UPLOAD";
    
    /** The constants ACTION_GROUP. */
    public static final String ACTION_GROUP = "action.GROUP";
    
    /** The constants ACTION_FORWARD_PAGE. */
    public static final String ACTION_FORWARD_PAGE = "action.FORWARD_PAGE";
    
    /** The constants ACTION_CANCEL. */
    public static final String ACTION_CANCEL = "action.CANCEL";
    
    /** The constants ACTION_ALLOW_REVISE. */
    public static final String ACTION_ALLOW_REVISE = "action.ALLOW_REVISE";
    
    /** The constants ACTION_GLOBAL_DO_SELECT_SCD. */
    public static final String ACTION_GLOBAL_DO_SELECT_SCD = "global.action.link.doSelectSCD";

    /** The constants ACTION_GLOBAL_DO_SELECT_DCD. */
    public static final String ACTION_GLOBAL_DO_SELECT_DCD = "global.action.link.doSelectDCD";

    /** The constants ACTION_GLOBAL_DO_SELECT_SPCD. */
    public static final String ACTION_GLOBAL_DO_SELECT_SPCD = "global.action.link.doSelectSPCD";

    /** The constants ACTION_GLOBAL_DO_SELECT_DPCD. */
    public static final String ACTION_GLOBAL_DO_SELECT_DPCD = "global.action.link.doSelectDPCD";

    /** The constants ACTION_GLOBAL_DO_INITIAL. */
    public static final String ACTION_GLOBAL_DO_INITIAL = "global.action.link.doInitial";
    
    /** The constants ACTION_GLOBAL_DO_INITIAL_RETURN. */
    public static final String ACTION_GLOBAL_DO_INITIAL_RETURN = "global.action.link.doInitialReturn";
    
    /** The constants ACTION_GLOBAL_DO_SEARCH. */
    public static final String ACTION_GLOBAL_DO_SEARCH = "global.action.link.doSearch";
    
    /** The constants ACTION_GLOBAL_DO_RETURN. */
    public static final String ACTION_GLOBAL_DO_RETURN = "global.action.link.doReturn"; 
    
    /** The constants ACTION_GLOBAL_DO_INITIAL_WITH_CRITERIA. */
    public static final String ACTION_GLOBAL_DO_INITIAL_WITH_CRITERIA
        = "global.action.link.doInitialWithCriteria";
    
    /** The constants ACTION_GLOBAL_DO_OPEN_SPECIFIED_PAGE. */
    public static final String ACTION_GLOBAL_DO_OPEN_SPECIFIED_PAGE = "global.action.link.doOpenSpecifiedPage";
    
    /** The constants ACTION_GLOBAL_DO_PAGING. */
    public static final String ACTION_GLOBAL_DO_PAGING = "global.action.link.doPaging";
    
    /** The constants ACTION_GLOBAL_DO_INITIAL_POPUP. */
    public static final String ACTION_GLOBAL_DO_INITIAL_POPUP = "global.action.link.doInitialPopup";

    /** The constants ACTION_GLOBAL_LINK_SPSPONO. */
    public static final String ACTION_GLOBAL_LINK_SPSPONO = "global.action.link.spsPoNo";

    /** The constants ACTION_GLOBAL_LINK_ACTIONMETHOD. */
    public static final String ACTION_GLOBAL_LINK_ACTIONMODE = "global.action.link.actionMode";
    
    /** The constants ACTION_GLOBAL_LINK_DO_LOGOUT. */
    public static final String ACTION_GLOBAL_LINK_DO_LOGOUT = "global.action.link.doLogout";
    
    /** The constants ACTION_GLOBAL_LINK_INITIAL_CONTACT_US. */
    public static final String ACTION_GLOBAL_LINK_INITIAL_CONTACT_US 
        = "global.action.link.doInitialContactUs";
    
    /** The constants ACTION_GLOBAL_DO_INITIAL_HELP. */
    public static final String ACTION_GLOBAL_DO_INITIAL_HELP = "global.action.link.doInitialHelp";
    
    // Start : [IN054] Add new action DENSO Reply and Save
    /** The constants ACTION_DENSO_REPLY. */
    public static final String ACTION_DENSO_REPLY = "action.DENSO_REPLY";

    /** The constants ACTION_DO_SAVE. */
    public static final String ACTION_DO_SAVE = "action.DO_SAVE";
    // End : [IN054] Add new action DENSO Reply and Save
    
    /** The constants METHOD_DO_INITIAL. */
    public static final String METHOD_DO_INITIAL = "doInitial";
    
    /** The constants METHOD_DO_INITIAL_RETURN. */
    public static final String METHOD_DO_INITIAL_RETURN = "doInitialReturn";
    
    /** The constants METHOD_DO_INITIAL_BY_RETURN. */
    public static final String METHOD_DO_INITIAL_BY_RETURN = "doInitialByReturn";
    
    /** The constants METHOD_DO_INITIAL_CONTACT_US. */
    public static final String METHOD_DO_INITIAL_CONTACT_US = "doInitialContactUs";
    
    /** The constants METHOD_DO_INITIAL_HELP. */
    public static final String METHOD_DO_INITIAL_HELP = "doInitialHelp";
    
    /** The constants METHOD_DO_INITIAL_WITH_CRITERIA. */
    public static final String METHOD_DO_INITIAL_WITH_CRITERIA = "doInitialWithCriteria";
    
    /** The constants METHOD_DO_INITIAL_POPUP. */
    public static final String METHOD_DO_INITIAL_POPUP = "doInitialPopup";
    
    /** The constants METHOD_DO_SEARCH. */
    public static final String METHOD_DO_SEARCH = "doSearch";

    /** The constants METHOD_SEARCH. */
    public static final String METHOD_SEARCH = "Search";
    
    /** The constants METHDO_DO_DOWNLOAD. */
    public static final String METHOD_DO_DELETE = "doDelete";

    /** The constants METHDO_DO_DOWNLOAD. */
    public static final String METHOD_DO_DOWNLOAD = "doDownload";
    
    /** The constants METHOD_DO_LINK_DSC_ID. */
    public static final String METHOD_DO_LINK_DSC_ID = "doLinkDscId";
    
    /** The constants METHOD_DO_LINK_ROLE. */
    public static final String METHOD_DO_LINK_ROLE = "doLinkRole";
    
    /** The constants METHOD_DO_LOGIN. */
    public static final String METHOD_DO_LOGIN = "doLogin";
    
    /** The constants METHOD_DO_LOGOUT. */
    public static final String METHOD_DO_LOGOUT = "doLogout";
    
    /** The constants METHOD_DO_CHANGE_LOCALE. */
    public static final String METHOD_DO_CHANGE_LOCALE = "doChangeLocale";

    /** The constants METHOD_DO_OPEN_WORD003. */
    public static final String METHOD_DO_OPEN_WORD003 = "doOpenWord003";
    
    /** The constants METHOD_DO_OPEN_WORD004. */
    public static final String METHOD_DO_OPEN_WORD004 = "doOpenWord004";
    
    /** The constants METHOD_DO_ACKNOWLEDGE. */
    public static final String METHOD_DO_ACKNOWLEDGE = "doAcknowledge";
    
    /** The constants METHOD_DO_DOWNLOAD_ONE_WAY_KANBAN_PDF. */
    public static final String METHOD_DO_DOWNLOAD_ONE_WAY_KANBAN_PDF = "doDownloadOneWayKanbanPdf";
    
    /** The constants METHOD_DO_DOWNLOAD_PDF. */
    public static final String METHOD_DO_DOWNLOAD_PDF = "doDownloadPdf";
    
    /** The constants METHOD_DO_DOWNLOAD_PDF_PREVIEW. */
    public static final String METHOD_DO_DOWNLOAD_PDF_PREVIEW = "doDownloadPdfPreview";
    
    /** The constants METHOD_DO_DOWNLOAD_PDF_ORIGINAL. */
    public static final String METHOD_DO_DOWNLOAD_PDF_ORIGINAL = "doDownloadPdfOriginal";

    /** The constants METHOD_DO_DOWNLOAD_PDF_CHANGE. */
    public static final String METHOD_DO_DOWNLOAD_PDF_CHANGE = "doDownloadPdfChange";

    /** The constants METHOD_DO_DOWNLOAD_LEGEND_FILE. */
    public static final String METHOD_DO_DOWNLOAD_LEGEND_FILE = "doDownloadLegendFile";

    /** The constants METHOD_DO_DO_CHANGE_SCD. */
    public static final String METHOD_DO_DO_SELECT_SCD = "doSelectSCd";

    /** The constants METHOD_DO_DO_CHANGE_DCD. */
    public static final String METHOD_DO_DO_SELECT_DCD = "doSelectDCd";

    /** The constants METHOD_DO_SELECT_SPCD. */
    public static final String METHOD_DO_SELECT_SPCD = "doSelectSPcd";

    /** The constants METHOD_DO_SELECT_DPCD. */
    public static final String METHOD_DO_SELECT_DPCD = "doSelectDPcd";
    
    /** The constants METHOD_DO_RESET. */
    public static final String METHOD_DO_RESET = "doReset";
    
    /** The constants METHOD_DO_EXPORT. */
    public static final String METHOD_DO_EXPORT = "doExport";
    
    /** The constants METHOD_DO_RETURN. */
    public static final String METHOD_DO_RETURN = "doReturn";
    
    /** The constants METHOD_DO_REGISTER. */
    public static final String METHOD_DO_REGISTER = "doRegister";
    
    /** The constants METHOD_DO_UPDATE. */
    public static final String METHOD_DO_UPDATE = "doUpdate";
    
    /** The constants METHOD_DO_UPLOAD. */
    public static final String METHOD_DO_UPLOAD = "doUpload";
    
    /** The constants METHOD_DO_SAVE_AND_SEND. */
    public static final String METHOD_DO_SAVE_AND_SEND = "doSaveAndSend";
    
    /** The constants METHOD_DO_CANCEL_AND_SEND. */
    public static final String METHOD_DO_CANCEL_AND_SEND = "doCancelAndSend";
    
    /** The constants METHOD_DO_CLINK_LINK_ASNNO. */
    public static final String METHOD_DO_CLINK_LINK_ASNNO = "doClickLinkAsnNo";
    
    /** The constants METHOD_DO_CLICK_LINK_MORE. */
    public static final String METHOD_DO_CLICK_LINK_MORE = "doClickLinkMore";
    
    /** The constants METHOD_DO_CLINK_LINK_SPSDONO. */
    public static final String METHOD_DO_CLINK_LINK_SPSDONO = "doClickLinkSpsDoNo";
    
    /** The constants METHOD_DO_CLINK_LINK_SHIP_NOTICE. */
    public static final String METHOD_DO_CLINK_LINK_SHIP_NOTICE = "doClickLinkShipNotice";

    /** The constants METHOD_DO_PREVIEW. */
    public static final String METHOD_DO_PREVIEW = "doPreview";
    
    /** The constants METHOD_DO_PREVIEWPENDING. */
    public static final String METHOD_DO_PREVIEWPENDING = "doPreviewPending";
    
    /** The constants METHOD_DO_PAGING. */
    public static final String METHOD_DO_PAGING = "doPaging";
    
    /** The constants METHOD_DO_OPEN_SPECIFIED_PAGE. */
    public static final String METHOD_DO_OPEN_SPECIFIED_PAGE = "doOpenSpecifiedPage";
    
    /** The constants METHOD_DO_GROUP_ASN. */
    public static final String METHOD_DO_GROUP_ASN = "doGroupAsn";
    
    /** The constants METHOD_DO_GROUP. */
    public static final String METHOD_DO_GROUP = "doGroup";
    
    /** The constants METHOD_DO_FORWARD_PAGE. */
    public static final String METHOD_DO_FORWARD_PAGE = "doForwardPage";
    
    /** The constants METHOD_DO_CANCEL. */
    public static final String METHOD_DO_CANCEL = "doCancel";
    
    /** The constants METHOD_CANCEL. */
    public static final String METHOD_CANCEL = "Cancel";
    
    /** The constants METHOD_DO_ALLOW_REVISE. */
    public static final String METHOD_DO_ALLOW_REVISE  = "doAllowReviseQty";
    
    // Start : [IN054] Add new action DENSO Reply and Save
    /** The constants METHOD_DO_DENSO_REPLY. */
    public static final String METHOD_DO_DENSO_REPLY  = "doDensoReply";

    /** The constants METHOD_DO_SAVE. */
    public static final String METHOD_DO_SAVE  = "doSave";
    // End : [IN054] Add new action DENSO Reply and Save
    
    /* End ACTION METHOD MAP ==================================================================== */
    
    /* Session Key ============================================================================== */

    /** The Constant SESSION_USER_INFO. */
    public static final String SESSION_USER_LOGIN = "sessionUserLogin";

    /** The Constant SESSION_WORD002_FORM. */
    public static final String SESSION_WORD002_FORM = "sessionWORD002Form";

    /** The Constant SESSION_WORD003_FORM. */
    public static final String SESSION_WORD003_FORM = "sessionWORD003Form";
    
    /** The Constant SESSION_WSHP001_FORM. */
    public static final String SESSION_WSHP001_FORM = "sessionWSHP001Form";
    
    /** The Constant SESSION_WSHP003_FORM. */
    public static final String SESSION_WSHP003_FORM = "sessionWSHP003Form";
    
    /** The Constant SESSION_WSHP006_FORM. */
    public static final String SESSION_WSHP006_FORM = "sessionWSHP006Form";
    
    /** The Constant SESSION_WSHP007_FORM. */
    public static final String SESSION_WSHP007_FORM = "sessionWSHP007Form";
    
    /** The Constant SESSION_WSHP008_FORM. */
    public static final String SESSION_WSHP008_FORM = "sessionWSHP008Form";
    
    /** The Constant SESSION_WSHP009_FORM. */
    public static final String SESSION_WSHP009_FORM = "sessionWSHP009Form";
    
    /** The Constant SESSION_WINV001_FORM. */
    public static final String SESSION_WINV001_FORM = "sessionWINV001Form";
    
    /** The Constant SESSION_WINV002_FORM. */
    public static final String SESSION_WINV002_FORM = "sessionWINV002Form";
    
    /** The Constant SESSION_WINV003_FORM. */
    public static final String SESSION_WINV003_FORM = "sessionWINV003Form";
    
    /** The Constant SESSION_WINV005_FORM. */
    public static final String SESSION_WINV005_FORM = "sessionWINV005Form";
    
    /** The Constant SESSION_WADM001_FORM. */
    public static final String SESSION_WADM001_FORM = "sessionWADM001Form";
    
    /** The Constant SESSION_WADM005_FORM. */
    public static final String SESSION_WADM005_FORM = "sessionWADM005Form";
    
    /** The Constant SESSION_ASNMAINTENANCE_RESULT. */
    public static final String SESSION_ASNMAINTENANCE_RESULT = "sessionAsnMaintenanceResult";
    
    /* End Session Key ========================================================================== */
    
    /** The constant GROUP_ASN_WARNING_TYPE. */
    public static final String GROUP_ASN_WARNING_TYPE = "0";
    
    /** The constant GROUP_ASN_ERROR_TYPE. */
    public static final String GROUP_ASN_ERROR_TYPE = "1";
    
    /** The constant GROUP_INVOICE_WARNING_TYPE. */
    public static final String GROUP_INVOICE_WARNING_TYPE = "0";
    
    /** The constant GROUP_INVOICE_ERROR_TYPE. */
    public static final String GROUP_INVOICE_ERROR_TYPE = "1";
    
    /** The constant GROUP_INVOICE_DATE_TYPE. */
    public static final String GROUP_INVOICE_DATE_TYPE = "D";
    
    /** The constant GROUP_INVOICE_MONTH_TYPE. */
    public static final String GROUP_INVOICE_MONTH_TYPE = "M";
    
    /** The constant GROUP_INVOICE_YEAR_TYPE. */
    public static final String GROUP_INVOICE_WEEK_TYPE = "W";
    
    /** The constant GROUP_INVOICE_NOT_ALLOW_DIFF_DPCD. */
    public static final String GROUP_INVOICE_NOT_ALLOW_DIFF_DPCD = "0";
    
    /** The constant OK_INVOICE_RCV_STATUS. */
    public static final String OK_INVOICE_RCV_STATUS = "OK";
    
    /** The constant NO_INVOICE_RCV_STATUS. */
    public static final String NO_INVOICE_RCV_STATUS = "NO";
    
    /** The constant INV_DOCUMENT_TYPE. */
    public static final String INV_DOCUMENT_TYPE = "INV";
    
    /** The constant INV_DOCUMENT_TYPE. */
    public static final String ASN_DOCUMENT_TYPE = "ASN";
    
    /** The constant INV_DOCUMENT_TYPE. */
    public static final String CN_DOCUMENT_TYPE = "CN";
    
    /* Start: Upload Error Code ========================================================================== */
    /** The constant UPLOAD_ERROR_CODE_ERR001. */
    public static final String UPLOAD_ERROR_CODE_ERR001 = "VLD001";
    
    /** The constant UPLOAD_ERROR_CODE_ERR002. */
    public static final String UPLOAD_ERROR_CODE_ERR002 = "VLD002";
    
    /** The constant UPLOAD_ERROR_CODE_ERR003. */
    public static final String UPLOAD_ERROR_CODE_ERR003 = "VLD003";
    
    /** The constant UPLOAD_ERROR_CODE_ERR004. */
    public static final String UPLOAD_ERROR_CODE_ERR004 = "VLD004";
    
    /** The constant UPLOAD_ERROR_CODE_ERR005. */
    public static final String UPLOAD_ERROR_CODE_ERR005 = "VLD005";
    
    /** The constant UPLOAD_ERROR_CODE_ERR006. */
    public static final String UPLOAD_ERROR_CODE_ERR006 = "VLD006";
    
    /** The constant UPLOAD_ERROR_CODE_ERR007. */
    public static final String UPLOAD_ERROR_CODE_ERR007 = "VLD007";
    
    /** The constant UPLOAD_ERROR_CODE_ERR008. */
    public static final String UPLOAD_ERROR_CODE_ERR008 = "VLD008";
    
    /** The constant UPLOAD_ERROR_CODE_ERR009. */
    public static final String UPLOAD_ERROR_CODE_ERR009 = "VLD009";
    
    /** The constant UPLOAD_ERROR_CODE_ERR010. */
    public static final String UPLOAD_ERROR_CODE_ERR010 = "VLD010";
    
    /** The constant UPLOAD_ERROR_CODE_ERR011. */
    public static final String UPLOAD_ERROR_CODE_ERR011 = "VLD011";
    
    /** The constant UPLOAD_ERROR_CODE_ERR012. */
    public static final String UPLOAD_ERROR_CODE_ERR012 = "VLD012";
    
    /** The constant UPLOAD_ERROR_CODE_ERR013. */
    public static final String UPLOAD_ERROR_CODE_ERR013 = "VLD013";
    
    /** The constant UPLOAD_ERROR_CODE_ERR014. */
    public static final String UPLOAD_ERROR_CODE_ERR014 = "VLD014";
    
    /** The constant UPLOAD_ERROR_CODE_ERR015. */
    public static final String UPLOAD_ERROR_CODE_ERR015 = "VLD015";
    
    /** The constant UPLOAD_ERROR_CODE_ERR016. */
    public static final String UPLOAD_ERROR_CODE_ERR016 = "VLD016";
    
    /** The constant UPLOAD_ERROR_CODE_ERR017. */
    public static final String UPLOAD_ERROR_CODE_ERR017 = "VLD017";
    
    /** The constant UPLOAD_ERROR_CODE_ERR018. */
    public static final String UPLOAD_ERROR_CODE_ERR018 = "VLD018";
    
    /** The constant UPLOAD_ERROR_CODE_ERR019. */
    public static final String UPLOAD_ERROR_CODE_ERR019 = "VLD019";
    
    /** The constant UPLOAD_ERROR_CODE_ERR020. */
    public static final String UPLOAD_ERROR_CODE_ERR020 = "VLD020";
    
    /** The constant UPLOAD_ERROR_CODE_ERR021. */
    public static final String UPLOAD_ERROR_CODE_ERR021 = "VLD021";
    
    /** The constant UPLOAD_ERROR_CODE_ERR022. */
    public static final String UPLOAD_ERROR_CODE_ERR022 = "VLD022";
    
    /** The constant UPLOAD_ERROR_CODE_ERR023. */
    public static final String UPLOAD_ERROR_CODE_ERR023 = "VLD023";
    
    /** The constant UPLOAD_ERROR_CODE_ERR024. */
    public static final String UPLOAD_ERROR_CODE_ERR024 = "VLD024";
    
    /** The constant UPLOAD_ERROR_CODE_ERR025. */
    public static final String UPLOAD_ERROR_CODE_ERR025 = "VLD025";
    
    /** The constant UPLOAD_ERROR_CODE_ERR026. */
    public static final String UPLOAD_ERROR_CODE_ERR026 = "VLD026";
    
    /** The constant UPLOAD_ERROR_CODE_ERR027. */
    public static final String UPLOAD_ERROR_CODE_ERR027 = "VLD027";
    
    /** The constant UPLOAD_ERROR_CODE_ERR028. */
    public static final String UPLOAD_ERROR_CODE_ERR028 = "VLD028";
    
    /** The constant UPLOAD_ERROR_CODE_ERR029. */
    public static final String UPLOAD_ERROR_CODE_ERR029 = "VLD029";
    
    /** The constant UPLOAD_ERROR_CODE_ERR030. */
    public static final String UPLOAD_ERROR_CODE_ERR030 = "VLD030";
    
    /** The constant UPLOAD_ERROR_CODE_ERR031. */
    public static final String UPLOAD_ERROR_CODE_ERR031 = "VLD031";
    
    /** The constant UPLOAD_ERROR_CODE_ERR032. */
    public static final String UPLOAD_ERROR_CODE_ERR032 = "VLD032";
    
    /** The constant UPLOAD_ERROR_CODE_ERR033. */
    public static final String UPLOAD_ERROR_CODE_ERR033 = "VLD033";
    
    /** The constant UPLOAD_ERROR_CODE_ERR034. */
    public static final String UPLOAD_ERROR_CODE_ERR034 = "VLD034";
    
    /** The constant UPLOAD_ERROR_CODE_ERR035. */
    public static final String UPLOAD_ERROR_CODE_ERR035 = "VLD035";
    
    /** The constant UPLOAD_ERROR_CODE_ERR036. */
    public static final String UPLOAD_ERROR_CODE_ERR036 = "VLD036";
    
    /** The constant UPLOAD_ERROR_CODE_ERR037. */
    public static final String UPLOAD_ERROR_CODE_ERR037 = "VLD037";
    
    /** The constant UPLOAD_ERROR_CODE_ERR038. */
    public static final String UPLOAD_ERROR_CODE_ERR038 = "VLD038";
    
    /** The constant UPLOAD_ERROR_CODE_ERR039. */
    public static final String UPLOAD_ERROR_CODE_ERR039 = "VLD039";
    
    /** The constant UPLOAD_ERROR_CODE_ERR040. */
    public static final String UPLOAD_ERROR_CODE_ERR040 = "VLD040";
    
    /** The constant UPLOAD_ERROR_CODE_ERR041. */
    public static final String UPLOAD_ERROR_CODE_ERR041 = "VLD041";
    
    /** The constant UPLOAD_ERROR_CODE_ERR042. */
    public static final String UPLOAD_ERROR_CODE_ERR042 = "VLD042";
    
    /** The constant UPLOAD_ERROR_CODE_ERR043. */
    public static final String UPLOAD_ERROR_CODE_ERR043 = "VLD043";
    
    /** The constant UPLOAD_ERROR_CODE_ERR044. */
    public static final String UPLOAD_ERROR_CODE_ERR044 = "VLD044";
    
    /** The constant UPLOAD_ERROR_CODE_ERR045. */
    public static final String UPLOAD_ERROR_CODE_ERR045 = "VLD045";
    
    /** The constant UPLOAD_ERROR_CODE_ERR046. */
    public static final String UPLOAD_ERROR_CODE_ERR046 = "VLD046";
    
    /** The constant UPLOAD_ERROR_CODE_ERR047. */
    public static final String UPLOAD_ERROR_CODE_ERR047 = "VLD047";
    
    /** The constant UPLOAD_ERROR_CODE_ERR048. */
    public static final String UPLOAD_ERROR_CODE_ERR048 = "VLD048";
    
    /** The constant UPLOAD_ERROR_CODE_ERR049. */
    public static final String UPLOAD_ERROR_CODE_ERR049 = "VLD049";
    
    /** The constant UPLOAD_ERROR_CODE_ERR050. */
    public static final String UPLOAD_ERROR_CODE_ERR050 = "VLD050";
    
    /** The constant UPLOAD_ERROR_CODE_ERR051. */
    public static final String UPLOAD_ERROR_CODE_ERR051 = "VLD051";
    
    /** The constant UPLOAD_ERROR_CODE_ERR052. */
    public static final String UPLOAD_ERROR_CODE_ERR052 = "VLD052";
    
    /** The constant UPLOAD_ERROR_CODE_ERR053. */
    public static final String UPLOAD_ERROR_CODE_ERR053 = "VLD053";
    
    /** The constant UPLOAD_ERROR_CODE_ERR054. */
    public static final String UPLOAD_ERROR_CODE_ERR054 = "VLD054";
    
    /** The constant UPLOAD_ERROR_CODE_ERR055. */
    public static final String UPLOAD_ERROR_CODE_ERR055 = "VLD055";
    
    /** The constant UPLOAD_ERROR_CODE_ERR056. */
    public static final String UPLOAD_ERROR_CODE_ERR056 = "VLD056";
    
    /** The constant UPLOAD_ERROR_CODE_ERR057. */
    public static final String UPLOAD_ERROR_CODE_ERR057 = "VLD057";
    
    /** The constant UPLOAD_ERROR_CODE_ERR058. */
    public static final String UPLOAD_ERROR_CODE_ERR058 = "VLD058";
    
    /** The constant UPLOAD_ERROR_CODE_ERR059. */
    public static final String UPLOAD_ERROR_CODE_ERR059 = "VLD059";
    
    /** The constant UPLOAD_ERROR_CODE_ERR060. */
    public static final String UPLOAD_ERROR_CODE_ERR060 = "VLD060";
    
    /** The constant UPLOAD_ERROR_CODE_ERR061. */
    public static final String UPLOAD_ERROR_CODE_ERR061 = "VLD061";
    
    /** The constant UPLOAD_ERROR_CODE_ERR062. */
    public static final String UPLOAD_ERROR_CODE_ERR062 = "VLD062";
    
    /** The constant UPLOAD_ERROR_CODE_ERR063. */
    public static final String UPLOAD_ERROR_CODE_ERR063 = "VLD063";
    
    /** The constant UPLOAD_ERROR_CODE_ERR064. */
    public static final String UPLOAD_ERROR_CODE_ERR064 = "VLD064";
    
    /** The constant UPLOAD_ERROR_CODE_ERR065. */
    public static final String UPLOAD_ERROR_CODE_ERR065 = "VLD065";
    
    /** The constant UPLOAD_ERROR_CODE_ERR066. */
    public static final String UPLOAD_ERROR_CODE_ERR066 = "VLD066";
    
    /** The constant UPLOAD_ERROR_CODE_ERR067. */
    public static final String UPLOAD_ERROR_CODE_ERR067 = "VLD067";
    
    /** The constant UPLOAD_ERROR_CODE_ERR068. */
    public static final String UPLOAD_ERROR_CODE_ERR068 = "VLD068";
    
    /** The constant UPLOAD_ERROR_CODE_ERR069. */
    public static final String UPLOAD_ERROR_CODE_ERR069 = "VLD069";
    
    /** The constant UPLOAD_ERROR_CODE_ERR070. */
    public static final String UPLOAD_ERROR_CODE_ERR070 = "VLD070";
    
    /** The constant UPLOAD_ERROR_CODE_ERR071. */
    public static final String UPLOAD_ERROR_CODE_ERR071 = "VLD071";
    
    /** The constant UPLOAD_ERROR_CODE_ERR072. */
    public static final String UPLOAD_ERROR_CODE_ERR072 = "VLD072";
    
    /** The constant UPLOAD_ERROR_CODE_ERR073. */
    public static final String UPLOAD_ERROR_CODE_ERR073 = "VLD073";
    
    /** The constant UPLOAD_ERROR_CODE_ERR074. */
    public static final String UPLOAD_ERROR_CODE_ERR074 = "VLD074";
    
    /** The constant UPLOAD_ERROR_CODE_ERR075. */
    public static final String UPLOAD_ERROR_CODE_ERR075 = "VLD075";
    
    /** The constant UPLOAD_ERROR_CODE_ERR076. */
    public static final String UPLOAD_ERROR_CODE_ERR076 = "VLD076";
    
    /** The constant UPLOAD_ERROR_CODE_ERR077. */
    public static final String UPLOAD_ERROR_CODE_ERR077 = "VLD077";
    
    /** The constant UPLOAD_ERROR_CODE_ERR078. */
    public static final String UPLOAD_ERROR_CODE_ERR078 = "VLD078";
    
    /** The constant UPLOAD_ERROR_CODE_ERR079. */
    public static final String UPLOAD_ERROR_CODE_ERR079 = "VLD079";
    
    /** The constant UPLOAD_ERROR_CODE_ERR080. */
    public static final String UPLOAD_ERROR_CODE_ERR080 = "VLD080";
    
    /** The constant UPLOAD_ERROR_CODE_ERR081. */
    public static final String UPLOAD_ERROR_CODE_ERR081 = "VLD081";
    
    /** The constant UPLOAD_ERROR_CODE_ERR082. */
    public static final String UPLOAD_ERROR_CODE_ERR082 = "VLD082";
    
    /** The constant UPLOAD_ERROR_CODE_ERR083. */
    public static final String UPLOAD_ERROR_CODE_ERR083 = "VLD083";
    
    /** The constant UPLOAD_ERROR_CODE_ERR084. */
    public static final String UPLOAD_ERROR_CODE_ERR084 = "VLD084";
    
    /** The constant UPLOAD_ERROR_CODE_ERR085. */
    public static final String UPLOAD_ERROR_CODE_ERR085 = "VLD085";
    
    /** The constant UPLOAD_ERROR_CODE_ERR086. */
    public static final String UPLOAD_ERROR_CODE_ERR086 = "VLD086";
    
    /** The constant UPLOAD_ERROR_CODE_ERR087. */
    public static final String UPLOAD_ERROR_CODE_ERR087 = "VLD087";
    
    /** The constant UPLOAD_ERROR_CODE_ERR088. */
    public static final String UPLOAD_ERROR_CODE_ERR088 = "VLD088";
    
    /** The constant UPLOAD_ERROR_CODE_ERR089. */
    public static final String UPLOAD_ERROR_CODE_ERR089 = "VLD089";
    
    /** The constant UPLOAD_ERROR_CODE_ERR090. */
    public static final String UPLOAD_ERROR_CODE_ERR090 = "VLD090";
    
    /** The constant UPLOAD_ERROR_CODE_ERR091. */
    public static final String UPLOAD_ERROR_CODE_ERR091 = "VLD091";
    
    /** The constant UPLOAD_ERROR_CODE_ERR092. */
    public static final String UPLOAD_ERROR_CODE_ERR092 = "VLD092";
    
    /** The constant UPLOAD_ERROR_CODE_ERR093. */
    public static final String UPLOAD_ERROR_CODE_ERR093 = "VLD093";
    
    /** The constant UPLOAD_ERROR_CODE_ERR094. */
    public static final String UPLOAD_ERROR_CODE_ERR094 = "VLD094";
    
    /** The constant UPLOAD_ERROR_CODE_ERR095. */
    public static final String UPLOAD_ERROR_CODE_ERR095 = "VLD095";
    
    /** The constant UPLOAD_ERROR_CODE_ERR096. */
    public static final String UPLOAD_ERROR_CODE_ERR096 = "VLD096";
    
    /** The constant UPLOAD_ERROR_CODE_ERR097. */
    public static final String UPLOAD_ERROR_CODE_ERR097 = "VLD097";
    
    /** The constant UPLOAD_ERROR_CODE_ERR098. */
    public static final String UPLOAD_ERROR_CODE_ERR098 = "VLD098";
    
    /** The constant UPLOAD_ERROR_CODE_ERR099. */
    public static final String UPLOAD_ERROR_CODE_ERR099 = "VLD099";
    
    /** The constant UPLOAD_ERROR_CODE_ERR100. */
    public static final String UPLOAD_ERROR_CODE_ERR100 = "VLD100";
    
    /** The constant UPLOAD_ERROR_CODE_ERR101. */
    public static final String UPLOAD_ERROR_CODE_ERR101 = "VLD101";

    // [IN026] Effective start date overlap with current order
    /** The constant UPLOAD_ERROR_CODE_ERR102. */
    public static final String UPLOAD_ERROR_CODE_ERR102 = "VLD102";
    
    // [IN043] Validate Effective date more than P/O's End Firm Date
    /** The constant UPLOAD_ERROR_CODE_ERR103. */
    public static final String UPLOAD_ERROR_CODE_ERR103 = "VLD103";
    
    // Start : [IN055] Check End Forecast when delete Supplier Information
    /** The constant UPLOAD_ERROR_CODE_ERR104. */
    public static final String UPLOAD_ERROR_CODE_ERR104 = "VLD104";    
    
    /** The constant UPLOAD_ERROR_CODE_ERR105. */
    public static final String UPLOAD_ERROR_CODE_ERR105 = "VLD105";
    // End : [IN055] Check End Forecast when delete Supplier Information
    
    // Start : [IN068] Check effective date with table CIGMA ERROR
    /** The constant UPLOAD_ERROR_CODE_ERR106. */
    public static final String UPLOAD_ERROR_CODE_ERR106 = "VLD106";
    
    /** The constant UPLOAD_ERROR_CODE_ERR107. */
    public static final String UPLOAD_ERROR_CODE_ERR107 = "VLD107";
    
    /** The constant UPLOAD_ERROR_CODE_ERR108. */
    public static final String UPLOAD_ERROR_CODE_ERR108 = "VLD108";
    // End : [IN068] Check effective date with table CIGMA ERROR
    
    /** The constant UPLOAD_ERROR_CODE_A01. */
    public static final String UPLOAD_ERROR_CODE_A01 = "A01";
    
    /** The constant UPLOAD_ERROR_CODE_A02. */
    public static final String UPLOAD_ERROR_CODE_A02 = "A02";
    
    /** The constant UPLOAD_ERROR_CODE_A03. */
    public static final String UPLOAD_ERROR_CODE_A03 = "A03";
    
    /** The constant UPLOAD_ERROR_CODE_A04. */
    public static final String UPLOAD_ERROR_CODE_A04 = "A04";
    
    /** The constant UPLOAD_ERROR_CODE_A05. */
    public static final String UPLOAD_ERROR_CODE_A05 = "A05";
    
    /** The constant UPLOAD_ERROR_CODE_D05. */
    public static final String UPLOAD_ERROR_CODE_D05 = "D05";
    
    /** The constant UPLOAD_ERROR_CODE_D06. */
    public static final String UPLOAD_ERROR_CODE_D06 = "D06";
    
    /** The constant UPLOAD_ERROR_CODE_D14. */
    public static final String UPLOAD_ERROR_CODE_D14 = "D14";
    
    /** The constant UPLOAD_ERROR_CODE_F01. */
    public static final String UPLOAD_ERROR_CODE_F01 = "F01";
    
    /** The constant UPLOAD_ERROR_CODE_F02. */
    public static final String UPLOAD_ERROR_CODE_F02 = "F02";
    
    /** The constant UPLOAD_ERROR_CODE_F03. */
    public static final String UPLOAD_ERROR_CODE_F03 = "F03";
    
    /** The constant UPLOAD_ERROR_CODE_F04. */
    public static final String UPLOAD_ERROR_CODE_F04 = "F04";
    
    /** The constant UPLOAD_ERROR_CODE_F05. */
    public static final String UPLOAD_ERROR_CODE_F05 = "F05";
    
    /** The constant UPLOAD_ERROR_CODE_F06. */
    public static final String UPLOAD_ERROR_CODE_F06 = "F06";
    
    /** The constant UPLOAD_ERROR_CODE_F07. */
    public static final String UPLOAD_ERROR_CODE_F07 = "F07";
    
    /** The constant UPLOAD_ERROR_CODE_F08. */
    public static final String UPLOAD_ERROR_CODE_F08 = "F08";
    
    /** The constant UPLOAD_ERROR_CODE_F09. */
    public static final String UPLOAD_ERROR_CODE_F09 = "F09";
    
    /** The constant UPLOAD_ERROR_CODE_F10. */
    public static final String UPLOAD_ERROR_CODE_F10 = "F10";
    
    /** The constant UPLOAD_ERROR_CODE_F11. */
    public static final String UPLOAD_ERROR_CODE_F11 = "F11";
    
    /** The constant UPLOAD_ERROR_CODE_F12. */
    public static final String UPLOAD_ERROR_CODE_F12 = "F12";
    
    /** The constant UPLOAD_ERROR_CODE_F13. */
    public static final String UPLOAD_ERROR_CODE_F13 = "F13";
    
    /** The constant UPLOAD_ERROR_CODE_F14. */
    public static final String UPLOAD_ERROR_CODE_F14 = "F14";
    
    /** The constant UPLOAD_ERROR_CODE_F15. */
    public static final String UPLOAD_ERROR_CODE_F15 = "F15";
    
    /** The constant UPLOAD_ERROR_CODE_F16. */
    public static final String UPLOAD_ERROR_CODE_F16 = "F16";
    
    /** The constant UPLOAD_ERROR_CODE_F17. */
    public static final String UPLOAD_ERROR_CODE_F17 = "F17";
    
    /** The constant UPLOAD_ERROR_CODE_F18. */
    public static final String UPLOAD_ERROR_CODE_F18 = "F18";
    
    /** The constant UPLOAD_ERROR_CODE_F19. */
    public static final String UPLOAD_ERROR_CODE_F19 = "F19";
    
    /** The constant UPLOAD_ERROR_CODE_F20. */
    public static final String UPLOAD_ERROR_CODE_F20 = "F20";
    
    /** The constant UPLOAD_ERROR_CODE_F21. */
    public static final String UPLOAD_ERROR_CODE_F21 = "F21";
    
    /** The constant UPLOAD_ERROR_CODE_F22. */
    public static final String UPLOAD_ERROR_CODE_F22 = "F22";
    
    /** The constant UPLOAD_ERROR_CODE_F23. */
    public static final String UPLOAD_ERROR_CODE_F23 = "F23";
    
    /** The constant UPLOAD_ERROR_CODE_F24. */
    public static final String UPLOAD_ERROR_CODE_F24 = "F24";
    
    /** The constant UPLOAD_ERROR_CODE_F25. */
    public static final String UPLOAD_ERROR_CODE_F25 = "F25";
    
    /** The constant UPLOAD_ERROR_CODE_F26. */
    public static final String UPLOAD_ERROR_CODE_F26 = "F26";
    
    /** The constant UPLOAD_ERROR_CODE_F27. */
    public static final String UPLOAD_ERROR_CODE_F27 = "F27";
    
    // [IN036]
    /** The constant UPLOAD_ERROR_CODE_F28. */
    public static final String UPLOAD_ERROR_CODE_F28 = "F28";

    /** The constant UPLOAD_ERROR_CODE_F29. */
    public static final String UPLOAD_ERROR_CODE_F29 = "F29";
    // [IN036]
    
    /** The constant UPLOAD_ERROR_CODE_M01. */
    public static final String UPLOAD_ERROR_CODE_M01 = "M01";
    
    /** The constant UPLOAD_ERROR_CODE_M02. */
    public static final String UPLOAD_ERROR_CODE_M02 = "M02";
    
    /** The constant UPLOAD_ERROR_CODE_M03. */
    public static final String UPLOAD_ERROR_CODE_M03 = "M03";
    
    /** The constant UPLOAD_ERROR_CODE_M04. */
    public static final String UPLOAD_ERROR_CODE_M04 = "M04";
    
    /** The constant UPLOAD_ERROR_CODE_M05. */
    public static final String UPLOAD_ERROR_CODE_M05 = "M05";
    
    /** The constant UPLOAD_ERROR_CODE_M10. */
    public static final String UPLOAD_ERROR_CODE_M10 = "M10";
    
    /** The constant UPLOAD_ERROR_CODE_M27. */
    public static final String UPLOAD_ERROR_CODE_M27 = "M27";

    // [IN036]
    /** The constant UPLOAD_ERROR_CODE_M29. */
    public static final String UPLOAD_ERROR_CODE_M29 = "M29";

    /** The constant UPLOAD_ERROR_CODE_T05. */
    public static final String UPLOAD_ERROR_CODE_T05 = "T05";
    
    /** The constant UPLOAD_ERROR_CODE_T01. */
    public static final String UPLOAD_ERROR_CODE_T01 = "T01";
    /** The constant UPLOAD_ERROR_CODE_T02. */
    public static final String UPLOAD_ERROR_CODE_T02 = "T02";
    /** The constant UPLOAD_ERROR_CODE_T03. */
    public static final String UPLOAD_ERROR_CODE_T03 = "T03";
    /** The constant UPLOAD_ERROR_CODE_T04. */
    public static final String UPLOAD_ERROR_CODE_T04 = "T04";
    
    /** The constant UPLOAD_ERROR_CODE_T12. */
    public static final String UPLOAD_ERROR_CODE_T12 = "T12";
    
    /** The constant UPLOAD_ERROR_CODE_T13. */
    public static final String UPLOAD_ERROR_CODE_T13 = "T13";
    
    /** The constant UPLOAD_ERROR_CODE_T14. */
    public static final String UPLOAD_ERROR_CODE_T14 = "T14";
    
    /** The constant UPLOAD_ERROR_CODE_T15. */
    public static final String UPLOAD_ERROR_CODE_T15 = "T15";
    
    /** The constant UPLOAD_ERROR_CODE_T16. */
    public static final String UPLOAD_ERROR_CODE_T16 = "T16";
    
    /** The constant UPLOAD_ERROR_CODE_T17. */
    public static final String UPLOAD_ERROR_CODE_T17 = "T17";
    
    /** The constant UPLOAD_ERROR_CODE_T18. */
    public static final String UPLOAD_ERROR_CODE_T18 = "T18";
    
    /** The constant UPLOAD_ERROR_CODE_T19. */
    public static final String UPLOAD_ERROR_CODE_T19 = "T19";
    
    /** The constant UPLOAD_ERROR_CODE_T20. */
    public static final String UPLOAD_ERROR_CODE_T20 = "T20";
    
    /** The constant UPLOAD_ERROR_CODE_T21. */
    public static final String UPLOAD_ERROR_CODE_T21 = "T21";
    
    /** The constant UPLOAD_ERROR_CODE_T22. */
    public static final String UPLOAD_ERROR_CODE_T22 = "T22";
    
    /** The constant UPLOAD_ERROR_CODE_T23. */
    public static final String UPLOAD_ERROR_CODE_T23 = "T23";
    
    /** The constant UPLOAD_ERROR_CODE_T24. */
    public static final String UPLOAD_ERROR_CODE_T24 = "T24";
    
    /** The constant UPLOAD_ERROR_CODE_T25. */
    public static final String UPLOAD_ERROR_CODE_T25 = "T25";
    
    /** The constant UPLOAD_ERROR_CODE_T26. */
    public static final String UPLOAD_ERROR_CODE_T26 = "T26";
    
    /** The constant UPLOAD_ERROR_CODE_T27. */
    public static final String UPLOAD_ERROR_CODE_T27 = "T27";
    
    // [IN036]
    /** The constant UPLOAD_ERROR_CODE_T28. */
    public static final String UPLOAD_ERROR_CODE_T28 = "T28";

    /** The constant UPLOAD_ERROR_CODE_T29. */
    public static final String UPLOAD_ERROR_CODE_T29 = "T29";
    // [IN036]
    
    
    /* End: Upload Error Code ========================================================================== */
    
    /* Start: Report ========================================================================== */
    
    /** The constant BASE_PATH. */
    public static final String BASE_PATH = "C:\\Eclipse_AIJ3\\workspace\\SPS\\report\\";
    
    /** The constant INVOICE_COVER_PAGE_PATH. */
    public static final String INVOICE_COVER_PAGE_PATH = "InvoiceCoverPageReport.jasper";

    /** The ASN Report Jasper file name. */
    public static final String ASN_REPORT_JASPER_FILE_NAME = "AdvancedShipNotice.jasper";

    /** The One way Kanban tag Report Jasper file name. */
    public static final String ONE_WAY_KANBAN_TAG_REPORT_JASPER_FILE_NAME = "OneWayKanbanTag.jasper";

    /** The One way Kanban tag Report Jasper file name. */
    public static final String ONE_WAY_KANBAN_INNER_TAG_REPORT_JASPER_FILE_NAME = "OneWayKanbanInnerTag.jasper";
    
    /** The constant INVOICE_COVER_PAGE. */
    public static final String INVOICE_COVER_PAGE = "InvoiceCoverPage";
    
    /** The constant ADVANCE_SHIP_NOTICE. */
    public static final String ADVANCE_SHIP_NOTICE = "AdvanceShipNotice";
    
    /** The constant ADVANCE_SHIP_NOTICE. */
    public static final String ONE_WAY_KANBAN_TAG = "OneWayKanbanTag";
    
    /** The constant PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER = "PurchaseOrder";
    
    /** The constant CHANGE_PURCHASE_ORDER. */
    public static final String CHANGE_PURCHASE_ORDER = "ChangeMaterialRelease";
    
    /** The constant DELIVERY_ORDER. */
    public static final String DELIVERY_ORDER = "DeliveryOrder";
    
    /** The constant KANBAN. */
    public static final String KANBAN = "Kanban";
    
    /** The constant DELIVERY_ORDER_REPORT. */
    public static final String DELIVERY_ORDER_REPORT = "DeliveryOrderReport.jasper";

    /** The constant KANBAN_DELOVERY_ORDER_REPORT. */
    public static final String KANBAN_DELOVERY_ORDER_REPORT = "KanbanDeliveryOrderReport.jasper";

    /** The constant KANBAN_DELOVERY_ORDER_REPORT. */
    public static final String KANBAN_DELOVERY_ORDER_REPORT_NO_SEQ = "KanbanDeliveryOrderReport_NoSeq.jasper";
    
    /** The constant CHANGE_MATERIAL_RELEASE_REPORT. */
    public static final String CHANGE_MATERIAL_RELEASE_REPORT 
        = "ChangeMaterialReleaseReport.jasper";
    
    /** The constant PO_COVER_PAGE_REPORT. */
    public static final String PO_COVER_PAGE_REPORT = "PurchaseOrderCoverPageReport.jasper";
    
    /** The constant PURCHASE_ORDER_REPORT. */
    public static final String PURCHASE_ORDER_REPORT = "PurchaseOrderReport.jasper";
    
    /** The constant NO_PURCHASE_ORDER_REPORT. */
    public static final String NO_PURCHASE_ORDER_REPORT = "NoPurchaseOrderReport.jasper";
    
    /** The constant PO_TERMS_AND_CONDITION_REPORT. */
    public static final String PO_TERMS_AND_CONDITION_REPORT = "PoTermsAndConditionReport_";
    
    /* End: Report ========================================================================== */
    
    /** The constant JOB_ID_BORD003. */
    public static final String JOB_ID_BORD003 = "BORD003";
    
    /** The constant PO_ORDER_TYPE. */
    public static final Map<String, String> PO_ORDER_TYPE = new HashMap<String, String>();
    
    /** The constant PO_PERIOD_TYPE. */
    public static final Map<String, String> PO_PERIOD_TYPE = new HashMap<String, String>();
    
    /** The constant PO_DWM. */
    public static final Map<String, String> PO_DWM = new HashMap<String, String>();
    
    /** PN_STATUS_MAP */
    public static final Map<String, String> PN_STATUS_MAP = new HashMap<String, String>();
    /** ASN_STATUS_MAP */
    public static final Map<String, String> ASN_STATUS_MAP = new HashMap<String, String>();

    // [IN054] DENSO Reply map
    /** PO_REPLY_TYPE */
    public static final Map<String, String> PO_REPLY_TYPE = new HashMap<String, String>();
    
    /** The constant PO_DWM_D. */
    public static final String PO_DWM_D = "0";
    
    /** The constant PO_DWM_W. */
    public static final String PO_DWM_W = "1";
    
    /** The constant PO_DWM_M. */
    public static final String PO_DWM_M = "2";
    
    /** The constant PO_TOTAL_PAGE. */
    public static final String PO_TOTAL_PAGE = "TotalPage";
    
    /** The constant PO_REPORT_DAY_REGION. */
    public static final String PO_REPORT_DAY_REGION = "1";
    
    /** The constant PO_REPORT_WEEK_REGION. */
    public static final String PO_REPORT_WEEK_REGION = "2";
    
    /** The constant PO_REPORT_MONTH_REGION. */
    public static final String PO_REPORT_MONTH_REGION = "3";
    
    /**
     * 
     */
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_SUBJECT = "CIGMA_PO_ERROR_SUBJECT";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_SENDER_NAME = "CIGMA_PO_ERROR_SENDER_NAME";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER1 = "CIGMA_PO_ERROR_HEADER1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER2 = "CIGMA_PO_ERROR_HEADER2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER3 = "CIGMA_PO_ERROR_HEADER3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER_TABLE1 = "CIGMA_PO_ERROR_HEADER_TABLE1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER_TABLE2 = "CIGMA_PO_ERROR_HEADER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER_TABLE3 = "CIGMA_PO_ERROR_HEADER_TABLE3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_HEADER_TABLE4 = "CIGMA_PO_ERROR_HEADER_TABLE4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_DETAIL = "CIGMA_PO_ERROR_DETAIL";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_PO_ERROR_FOOTER = "CIGMA_PO_ERROR_FOOTER";
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_SUBJECT = "CIGMA_CHG_PO_1_ERROR_SUBJECT";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_SENDER_NAME = "CIGMA_CHG_PO_1_ERROR_SENDER_NAME";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER1 = "CIGMA_CHG_PO_1_ERROR_HEADER1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER2 = "CIGMA_CHG_PO_1_ERROR_HEADER2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER3 = "CIGMA_CHG_PO_1_ERROR_HEADER3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE1 = "CIGMA_CHG_PO_1_ERROR_HEADER_TABLE1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE2 = "CIGMA_CHG_PO_1_ERROR_HEADER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE3 = "CIGMA_CHG_PO_1_ERROR_HEADER_TABLE3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_HEADER_TABLE4 = "CIGMA_CHG_PO_1_ERROR_HEADER_TABLE4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_DETAIL = "CIGMA_CHG_PO_1_ERROR_DETAIL";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_PO_1_ERROR_FOOTER = "CIGMA_CHG_PO_1_ERROR_FOOTER";
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_SUBJECT = "CIGMA_DO_ERROR_SUBJECT";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_SENDER_NAME = "CIGMA_DO_ERROR_SENDER_NAME";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER1 = "CIGMA_DO_ERROR_HEADER1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER2 = "CIGMA_DO_ERROR_HEADER2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER3 = "CIGMA_DO_ERROR_HEADER3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_TABLE1 = "CIGMA_DO_ERROR_HEADER_TABLE1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_TABLE2 = "CIGMA_DO_ERROR_HEADER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_TABLE3 = "CIGMA_DO_ERROR_HEADER_TABLE3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_TABLE4 = "CIGMA_DO_ERROR_HEADER_TABLE4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_DETAIL = "CIGMA_DO_ERROR_DETAIL";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_FOOTER = "CIGMA_DO_ERROR_FOOTER";
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_SUBJECT = "CIGMA_CHG_DO_1_ERROR_SUBJECT";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_SENDER_NAME = "CIGMA_CHG_DO_1_ERROR_SENDER_NAME";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER1 = "CIGMA_CHG_DO_1_ERROR_HEADER1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER2 = "CIGMA_CHG_DO_1_ERROR_HEADER2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER3 = "CIGMA_CHG_DO_1_ERROR_HEADER3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE1 = "CIGMA_CHG_DO_1_ERROR_HEADER_TABLE1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE2 = "CIGMA_CHG_DO_1_ERROR_HEADER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE3 = "CIGMA_CHG_DO_1_ERROR_HEADER_TABLE3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_HEADER_TABLE4 = "CIGMA_CHG_DO_1_ERROR_HEADER_TABLE4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_DETAIL = "CIGMA_CHG_DO_1_ERROR_DETAIL";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_1_ERROR_FOOTER = "CIGMA_CHG_DO_1_ERROR_FOOTER";
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_SUBJECT = "CIGMA_CHG_DO_2_ERROR_SUBJECT";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_SENDER_NAME = "CIGMA_CHG_DO_2_ERROR_SENDER_NAME";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER1 = "CIGMA_CHG_DO_2_ERROR_HEADER1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER2 = "CIGMA_CHG_DO_2_ERROR_HEADER2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER3 = "CIGMA_CHG_DO_2_ERROR_HEADER3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE1 = "CIGMA_CHG_DO_2_ERROR_HEADER_TABLE1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE2 = "CIGMA_CHG_DO_2_ERROR_HEADER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3 = "CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE4 = "CIGMA_CHG_DO_2_ERROR_HEADER_TABLE4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_DETAIL = "CIGMA_CHG_DO_2_ERROR_DETAIL";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_FOOTER = "CIGMA_CHG_DO_2_ERROR_FOOTER";
    
    // [IN025] change email content
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER2_PONOTFOUND = "CIGMA_DO_ERROR_HEADER2_PONOTFOUND";
    
    // Start : [IN055] Change header column case Part not found
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_TABLE3_PRT_NOT_FOUND = "CIGMA_DO_ERROR_HEADER_TABLE3_PRT_NOT_FOUND";

    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3_PRT_NOT_FOUND = "CIGMA_CHG_DO_2_ERROR_HEADER_TABLE3_PRT_NOT_FOUND";
    // End : [IN055] Change header column case Part not found
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER2_DODUP = "CIGMA_DO_ERROR_HEADER2_DODUP";

    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_2_ERROR_HEADER2_PREREVERROR = "CIGMA_CHG_DO_2_ERROR_HEADER2_PREREVERROR";
    
    // End : [IN025] change email content
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_SUBJECT_BACKORDER = "CIGMA_DO_ERROR_SUBJECT_BACKORDER";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER = "CIGMA_DO_ERROR_HEADER_BACKORDER";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_REMARK_BACKORDER = "CIGMA_DO_ERROR_REMARK_BACKORDER";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE = "CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE2 = "CIGMA_DO_ERROR_HEADER_BACKORDER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_ERROR_DETAIL_BACKORDER = "CIGMA_DO_ERROR_DETAIL_BACKORDER";
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_ERROR_SUBJECT_BACKORDER = "CIGMA_CHG_DO_ERROR_SUBJECT_BACKORDER";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_CHG_DO_ERROR_HEADER_BACKORDER = "CIGMA_CHG_DO_ERROR_HEADER_BACKORDER";
    
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_SUBJECT = "CIGMA_DO_NOTI_BACKORDER_SUBJECT";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_SENDER_NAME = "CIGMA_DO_NOTI_BACKORDER_SENDER_NAME";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER1 = "CIGMA_DO_NOTI_BACKORDER_HEADER1";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER2 = "CIGMA_DO_NOTI_BACKORDER_HEADER2";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER3 = "CIGMA_DO_NOTI_BACKORDER_HEADER3";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER4 = "CIGMA_DO_NOTI_BACKORDER_HEADER4";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE1 = "CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE1";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE2 = "CIGMA_DO_NOTI_BACKORDER_HEADER_TABLE2";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_DETAIL = "CIGMA_DO_NOTI_BACKORDER_DETAIL";
    public static final String MAIL_CIGMA_DO_NOTI_BACKORDER_FOOTER = "CIGMA_DO_NOTI_BACKORDER_FOOTER";
    public static final String MAIL_CIGMA_CHG_DO_NOTI_BACKORDER_SUBJECT = "CIGMA_CHG_DO_NOTI_BACKORDER_SUBJECT";
    
    // End : 21082021 change email content Backorder
    
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_SUBJECT = "CIGMA_DO_URGENT_SUBJECT";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_SENDER_NAME = "CIGMA_DO_URGENT_SENDER_NAME";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER1 = "CIGMA_DO_URGENT_HEADER1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER2 = "CIGMA_DO_URGENT_HEADER2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER3 = "CIGMA_DO_URGENT_HEADER3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER4 = "CIGMA_DO_URGENT_HEADER4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER_TABLE1 = "CIGMA_DO_URGENT_HEADER_TABLE1";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER_TABLE2 = "CIGMA_DO_URGENT_HEADER_TABLE2";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER_TABLE3 = "CIGMA_DO_URGENT_HEADER_TABLE3";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_HEADER_TABLE4 = "CIGMA_DO_URGENT_HEADER_TABLE4";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_DETAIL = "CIGMA_DO_URGENT_DETAIL";
    /** Constant Mail ID */
    public static final String MAIL_CIGMA_DO_URGENT_FOOTER = "CIGMA_DO_URGENT_FOOTER";

    /** Constant Mail ID */
    public static final String MAIL_DO_NOTIFICATION_SUBJECT = "DO_NOTIFICATION_SUBJECT";

    /** Constant Mail ID */
    public static final String MAIL_KANBAN_NOTIFICATION_SUBJECT = "KANBAN_NOTIFICATION_SUBJECT";

    /** Constant Mail ID */
    public static final String MAIL_DO_NOTIFICATION_CONTENT_HEADER = "DO_NOTIFICATION_CONTENT_HEADER";

    /** Constant Mail ID */
    public static final String MAIL_DO_NOTIFICATION_CONTENT_DETAIL_HEADER = "DO_NOTIFICATION_CONTENT_DETAIL_HEADER";

    /** Constant Mail ID */
    public static final String MAIL_DO_NOTIFICATION_CONTENT_DETAIL = "DO_NOTIFICATION_CONTENT_DETAIL";

    /** Constant Mail ID */
    public static final String MAIL_DO_NOTIFICATION_CONTENT_FOOTER = "DO_NOTIFICATION_CONTENT_FOOTER";

    /** Constant Mail ID */
    public static final String PURGE_NOTIFICATION_SUBJECT = "PURGE_NOTIFICATION_SUBJECT";

    /** Constant Mail ID */
    public static final String PURGE_NOTIFICATION_CONTENT_HEADER = "PURGE_NOTIFICATION_CONTENT_HEADER";

    /** Constant Mail ID */
    public static final String PURGE_NOTIFICATION_CONTENT_COMPLETE = "PURGE_NOTIFICATION_CONTENT_COMPLETE";

    /** Constant Mail ID */
    public static final String PURGE_NOTIFICATION_CONTENT_INCOMPLETE = "PURGE_NOTIFICATION_CONTENT_INCOMPLETE";

    /** Constant Mail ID */
    public static final String PURGE_NOTIFICATION_CONTENT_FOOTER = "PURGE_NOTIFICATION_CONTENT_FOOTER";
    
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_SUBJECT_INVOICE_CN = "INVOICE_SUBJECT_INVOICE_CN";                                                                   
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TEXT_LINE1 = "INVOICE_HEADER_TEXT_LINE1";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TEXT_LINE2_CN = "INVOICE_HEADER_TEXT_LINE2_CN";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TEXT_LINE3 = "INVOICE_HEADER_TEXT_LINE3";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TEXT_LINE4 = "INVOICE_HEADER_TEXT_LINE4";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TEXT_LINE5 = "INVOICE_HEADER_TEXT_LINE5";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TABLE_LINE1 = "INVOICE_HEADER_TABLE_LINE1";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TABLE_LINE2 = "INVOICE_HEADER_TABLE_LINE2";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_DETAIL_TABLE_LINE1 = "INVOICE_DETAIL_TABLE_LINE1";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_DETAIL_TABLE_LINE3 = "INVOICE_DETAIL_TABLE_LINE3";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_FOOTER_LINE1 = "INVOICE_FOOTER_LINE1";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_FOOTER_LINE2 = "INVOICE_FOOTER_LINE2";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_SUBJECT_INVOICE_CANCEL = "INVOICE_SUBJECT_INVOICE_CANCEL";
    /** Constant Mail ID */
    public static final String MAIL_INVOICE_HEADER_TEXT_LINE2_CANCEL = "INVOICE_HEADER_TEXT_LINE2_CANCEL";

    /** Constant MESSAGE_CHECKING_DO */
    public static final String MESSAGE_CHECKING_DO = "Checkin D/O is in created ASN";
    
    /** Constant MESSAGE_UPDATE_CURRENT_DO */
    public static final String MESSAGE_UPDATE_CURRENT_DO = "Update current revision of D/O";
    
    /** Constant MESSAGE_SEARCH_CHANGE_MATERIAL_RELEASE_PO */
    public static final String MESSAGE_SEARCH_CHANGE_MATERIAL_RELEASE_PO 
        = "Search Change Material Release Report";
    
    /** Constant MESSAGE_UPDATE_PURCHASE_ORDER_DETAIL */
    public static final String MESSAGE_UPDATE_PURCHASE_ORDER_DETAIL
        = "Update Purchase Order Detail";
    
    /** Constant MESSAGE_UPDATE_PURCHASE_ORDER_DUE */
    public static final String MESSAGE_UPDATE_PURCHASE_ORDER_DUE
        = "Update Purchase Order Due";
    /** Constant MESSAGE_SEARCH_EXISTS_PURCHASE_ORDER */
    public static final String MESSAGE_SEARCH_EXISTS_PURCHASE_ORDER
        = "Search Exists Purchase Order";
    
    /** Constant MESSAGE_SEARCH_EXISTS_DELIVERY_ORDER */
    public static final String MESSAGE_SEARCH_EXISTS_DELIVERY_ORDER
        = "Search exist Delivery Order";
    
    /** Constant MESSAGE_CREATE_DELIVERY_ORDER_IN_NEW_REVISION */
    public static final String MESSAGE_CREATE_DELIVERY_ORDER_IN_NEW_REVISION
        = "Create Delivery Order in new revision";
    
    /** Constant MESSAGE_COPY_DELIVERY_ORDER_DETAIL_TO_CREATE_NEW_REVISION */
    public static final String MESSAGE_COPY_DELIVERY_ORDER_DETAIL_TO_CREATE_NEW_REVISION
        = "Copy Delivery Order Detail to create new revision";
    
    /** Constant MESSAGE_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION */
    public static final String MESSAGE_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION
        = "Search D/O Detail to create new revision";
    
    /** Constant MESSAGE_NO_URGENT_ORDER */
    public static final String MESSAGE_NO_URGENT_ORDER 
        = "No urgent order in this batch running period";
    
    /** Constant MESSAGE_CREATE_PO_COVER_PAGE_DETAIL */
    public static final String MESSAGE_CREATE_PO_COVER_PAGE_DETAIL 
        = "Create P/O Cover page Detail";
    
    /** Constant MESSAGE_CREATE_PURCHASE_ORDER */
    public static final String MESSAGE_CREATE_PURCHASE_ORDER 
        = "Create Purchase Order";
    
    /** Constant MESSAGE_CREATE_PURCHASE_ORDER_DETAIL */
    public static final String MESSAGE_CREATE_PURCHASE_ORDER_DETAIL 
        = "Create Purchase Order Detail";
    
    /** Constant MESSAGE_CREATE_DELIVERY_ORDER */
    public static final String MESSAGE_CREATE_DELIVERY_ORDER 
        = "Create Delivery Order";
    
    /** Constant MESSAGE_CREATE_DELIVERY_ORDER_DETAIL */
    public static final String MESSAGE_CREATE_DELIVERY_ORDER_DETAIL 
        = "Create Delivery Order Detail";
    
    /** Constant MESSAGE_CREATE_KANBAN_SEQUENCE */
    public static final String MESSAGE_CREATE_KANBAN_SEQUENCE 
        = "Create KANBAN Sequence";
    
    /** Constant MESSAGE_PO */
    public static final String MESSAGE_PO = "P/O";
    
    /** Constant MESSAGE_SPS_CIGMA_CHG_PO_ERROR */
    public static final String SPS_CIGMA_CHG_PO_ERROR = "SPS_CIGMA_CHG_PO_ERROR";
    
    /** Constant MESSAGE_SPS_CIGMA_PO_ERROR */
    public static final String SPS_CIGMA_PO_ERROR = "SPS_CIGMA_PO_ERROR";
    
    /** Constant SPS_PO_NO_START */
    public static final String SPS_PO_NO_START = "01";

    /** Constant MAIL_VENDOR_CD_REPLACEMENT */
    public static final String MAIL_VENDOR_CD_REPLACEMENT = "#vendorCd#";
    
    /** Constant MAIL_SCD_REPLACEMENT */
    public static final String MAIL_SCD_REPLACEMENT = "#sCd#";
    
    /** Constant MAIL_DCD_REPLACEMENT */
    public static final String MAIL_DCD_REPLACEMENT = "#dCd#";
    
    /** Constant MAIL_DPCD_REPLACEMENT */
    public static final String MAIL_DPCD_REPLACEMENT = "#dPcd#";
    
    /** Constant MAIL_SPCD_REPLACEMENT */
    public static final String MAIL_SPCD_REPLACEMENT = "#sPcd#";
    

    /** Constant MAIL_CIGMA_DO_NO_REPLACEMENT */
    public static final String MAIL_CIGMA_DO_NO_REPLACEMENT = "#cigmaDoNo#";
    
    /** Constant MAIL_ISSUE_DATE_REPLACEMENT */
    public static final String MAIL_ISSUE_DATE_REPLACEMENT = "#issueDate#";
    
    /** Constant MAIL_ASN_NO_LIST_REPLACEMENT */
    public static final String MAIL_ASN_NO_LIST_REPLACEMENT = "#asnNoList#";
    
    /** Constant MAIL_DELIVERY_DATE_REPLACEMENT */
    public static final String MAIL_DELIVERY_DATE_REPLACEMENT = "#deliveryDate#";
    
    /** Constant MAIL_DELIVERY_DATE_REPLACEMENT */
    public static final String MAIL_SHIPMENT_DATE_REPLACEMENT = "#shipDate#";
    
    /** Constant MAIL_DELIVERY_DATE_TIME_REPLACEMENT */
    public static final String MAIL_DELIVERY_DATE_TIME_REPLACEMENT = "#deliveryDatetime#";
    
    /** Constant MAIL_DELIVERY_TIME_REPLACEMENT */
    public static final String MAIL_DELIVERY_TIME_REPLACEMENT = "#deliveryTime#";
    
    /** Constant MAIL_DO_ISSUE_DATE */
    public static final String MAIL_DO_ISSUE_DATE = "#doIssueDate#";
    
    /** Constant MAIL_SPS_DO_NO_REPLACEMENT */
    public static final String MAIL_SPS_DO_NO_REPLACEMENT = "#spsDoNo#";
    
    /** Constant MAIL_REV_NO_REPLACEMENT */
    public static final String MAIL_REV_NO_REPLACEMENT = "#revision#";

    /** Constant MAIL_TRUCK_ROUTE_REPLACEMENT */
    public static final String MAIL_TRUCK_ROUTE_REPLACEMENT = "#truckRoute#";
    
    /** Constant MAIL_TRUCK_SEQ_REPLACEMENT */
    public static final String MAIL_TRUCK_SEQ_REPLACEMENT = "#truckSeq#";
    
    /** Constant MAIL_ORDER_TYPE_REPLACEMENT */
    public static final String MAIL_ORDER_TYPE_REPLACEMENT = "#orderType#";
    
    /** Constant MAIL_ORDER_TYPE_REPLACEMENT */
    public static final String MAIL_SHIPMENT_STATUS_REPLACEMENT = "#shipmentStatus#";
    
    /** Constant TABLE_CIGMA_PO_NAME */
    public static final String TABLE_CIGMA_PO_NAME = "SPS_CIGMA_PO_ERROR";
    
    /** Constant TABLE_CIGMA_CHG_PO_NAME */
    public static final String TABLE_CIGMA_CHG_PO_NAME = "SPS_CIGMA_CHG_PO_ERROR";
    
    /** Constant TABLE_CIGMA_DO_NAME */
    public static final String TABLE_CIGMA_DO_NAME = "SPS_CIGMA_DO_ERROR";
    
    /** Constant TABLE_CIGMA_CHG_DO_NAME */
    public static final String TABLE_CIGMA_CHG_DO_NAME = "SPS_CIGMA_CHG_DO_ERROR";
    
    /** Constant TRANSFER_DO_STEP_PROCESS_NAME */
    public static final String TRANSFER_DO_STEP_PROCESS_NAME = "TRANSFER_DO_TRANSFER_DO_PROCNAME";
    /** Constant TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER */
    public static final String TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER
        = "TRANSFER_DO_CREATE_DELIVERY_ORDER";
    /** Constant TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER_DETAIL */
    public static final String TRANSFER_DO_STEP_CREATE_DELIVERY_ORDER_DETAIL
        = "TRANSFER_DO_CREATE_DELIVERY_ORDER_DETAIL";
    /** Constant TRANSFER_DO_STEP_CREATE_KANBAN_SEQUENCE */
    public static final String TRANSFER_DO_STEP_CREATE_KANBAN_SEQUENCE
        = "TRANSFER_DO_CREATE_KANBAN_SEQUENCE";
    /** Constant TRANSFER_DO_STEP_SEARCH_DELIVERY_ORDER_REPORT */
    public static final String TRANSFER_DO_STEP_SEARCH_DELIVERY_ORDER_REPORT
        = "TRANSFER_DO_SEARCH_DELIVERY_ORDER_REPORT";
    /** Constant TRANSFER_DO_STEP_SEARCH_KANBAN_DELIVERY_ORDER_REPORT */
    public static final String TRANSFER_DO_STEP_SEARCH_KANBAN_DELIVERY_ORDER_REPORT
        = "TRANSFER_DO_SEARCH_KANBAN_DELIVERY_ORDER_REPORT";
    /** Constant TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID */
    public static final String TRANSFER_DO_STEP_UPDATE_PDF_FIELD_ID
        = "TRANSFER_DO_UPDATE_PDF_FIELD_ID";
    /** Constant TRANSFER_DO_STEP_SEARCH_EXISTS_DELIVERY_ORDER */
    public static final String TRANSFER_DO_STEP_SEARCH_EXISTS_DELIVERY_ORDER
        = "TRANSFER_DO_SEARCH_EXISTS_DELIVERY_ORDER";
    /** Constant TRANSFER_DO_STEP_CHECK_DO_IN_CREATE_ASN */
    public static final String TRANSFER_DO_STEP_CHECK_DO_IN_CREATE_ASN
        = "TRANSFER_DO_CHECK_DO_IN_CREATE_ASN";
    /** Constant TRANSFER_DO_STEP_UPDATE_CURRENT_REVISION_DO */
    public static final String TRANSFER_DO_STEP_UPDATE_CURRENT_REVISION_DO
        = "TRANSFER_DO_UPDATE_CURRENT_REVISION_DO";
    /** Constant TRANSFER_DO_STEP_CHECK_PREVIOUS_REV_ERROR */
    public static final String TRANSFER_DO_STEP_CHECK_PREVIOUS_REV_ERROR
        = "TRANSFER_DO_CHECK_PREVIOUS_REV_ERROR";
    /** Constant TRANSFER_DO_STEP_SEARCH_KANBAN_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_SEARCH_KANBAN_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_SEARCH_KANBAN_TO_CREATE_NEW_REVISION";
    /** Constant TRANSFER_DO_STEP_CREATE_KANBAN_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_CREATE_KANBAN_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_CREATE_KANBAN_TO_CREATE_NEW_REVISION";
    /** Constant TRANSFER_DO_STEP_COPY_KANBAN_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_COPY_KANBAN_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_COPY_KANBAN_TO_CREATE_NEW_REVISION";
    
    /** Constant TRANSFER_DO_STEP_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_SEARCH_DO_DETAIL_TO_CREATE_NEW_REVISION";
    /** Constant TRANSFER_DO_STEP_CREATE_DO_DETAIL_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_CREATE_DO_DETAIL_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_CREATE_DO_DETAIL_TO_CREATE_NEW_REVISION";
    /** Constant TRANSFER_DO_STEP_COPY_DO_DETAIL_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_COPY_DO_DETAIL_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_COPY_DO_DETAIL_TO_CREATE_NEW_REVISION";
    
    /** Constant TRANSFER_DO_STEP_CREATE_DO_IN_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_CREATE_DO_IN_NEW_REVISION
        = "TRANSFER_DO_CREATE_DO_IN_NEW_REVISION";
    /** Constant TRANSFER_DO_STEP_SEARCH_DO_TO_CREATE_NEW_REVISION */
    public static final String TRANSFER_DO_STEP_SEARCH_DO_TO_CREATE_NEW_REVISION
        = "TRANSFER_DO_SEARCH_DO_TO_CREATE_NEW_REVISION";
    
    // [IN012] Transfer D/O new Step
    /** Constant TRANSFER_DO_STEP_UPDATE_CHG_DO_STATUS */
    public static final String TRANSFER_DO_STEP_UPDATE_CHG_DO_STATUS
        = "TRANSFER_DO_UPDATE_CHANGE_DO_STATUS";
    
    /** Constant TRANSFER_PO_STEP_PROCESS_NAME */
    public static final String TRANSFER_PO_STEP_PROCESS_NAME = "TRANSFER_PO_TRANSFER_PO_PROCNAME";
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_PO
        = "TRANSFER_PO_STEP_CREATE_PO"; // Create Purchase Order
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_PO_DETAIL
        = "TRANSFER_PO_STEP_CREATE_PO_DETAIL"; // Create Purchase Order Detail
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_PO_DUE
        = "TRANSFER_PO_STEP_CREATE_PO_DUE"; // Create Purchase Order Due
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE
        = "TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE"; // Create P/O Cover Page
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE_DETAIL
        = "TRANSFER_PO_STEP_CREATE_PO_COVER_PAGE_DETAIL"; // Create P/O Cover Page Detail
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT
        = "TRANSFER_PO_STEP_SEARCH_PO_COVER_PAGE_REPORT"; // Search P/O Cover Page Report
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_SEARCH_PO_REPORT
        = "TRANSFER_PO_STEP_SEARCH_PO_REPORT"; // Search Purchase Order Report
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID
        = "TRANSFER_PO_STEP_UPDATE_PDF_ORG_FIELD_ID"; // Update PDF Original File ID
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID
        = "TRANSFER_PO_STEP_UPDATE_PDF_CHG_FIELD_ID"; // Update PDF Change File ID
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_SEARCH_EXIST_PO
        = "TRANSFER_PO_STEP_SEARCH_EXIST_PO"; // Search Exist Purchase Order
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_UPDATE_PO_DUE
        = "TRANSFER_PO_STEP_UPDATE_PO_DUE"; // Update Purchase Order Due
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_UPDATE_PO_DETAIL
        = "TRANSFER_PO_STEP_UPDATE_PO_DETAIL"; // Update Purchase Order Detail
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_UPDATE_PO
        = "TRANSFER_PO_STEP_UPDATE_PO"; // Update Purchase Order
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_SEARCH_CHG_MAT_REPORT
        = "TRANSFER_PO_STEP_SEARCH_CHG_MAT_REPORT"; // Search Change Material Report
    /** Constant Transfer P/O Step */
    public static final String TRANSFER_PO_STEP_SEARCH_EXISTS_FOR_FIRM
        = "TRANSFER_PO_STEP_SEARCH_EXISTS_FOR_FIRM"; // Search Exists Purchaser Order for Firm Period
    /** Constant Transfer Change P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_CHG_PO
        = "TRANSFER_PO_STEP_CREATE_CHG_PO"; // Create Purchase Order
    /** Constant Transfer Change P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_CHG_PO_DETAIL
        = "TRANSFER_PO_STEP_CREATE_CHG_PO_DETAIL"; // Create Purchase Order Detail
    /** Constant Transfer Change P/O Step */
    public static final String TRANSFER_PO_STEP_CREATE_CHG_PO_DUE
        = "TRANSFER_PO_STEP_CREATE_CHG_PO_DUE"; // Create Purchase Order Due
    
    /** Constant SPS_SYSTEM_NAME */
    public static final String SPS_SYSTEM_NAME = "SPS_SYSTEM";
    
    /** Constant INPUT_CNDATE_LESS_THAN_INVDATE */
    public static final String LBL_INPUT_CNDATE_LESS_THAN_INVDATE
        = "INPUT_CNDATE_LESS_THAN_INVDATE";
    
    /** Constant ERROR_TYPE_VENDOR_NOT_FOUND */
    public static final String ERROR_TYPE_VENDOR_NOT_FOUND = "1";

    /** Constant ERROR_TYPE_PARTS_NO_NOT_FOUND */
    public static final String ERROR_TYPE_PARTS_NO_NOT_FOUND = "2";

    /** Constant ERROR_TYPE_PARTS_NO_NOT_FOUND */
    public static final String ERROR_TYPE_ORDER_DATE_LESS_THAN_EFFECTIVE = "3";

    /** Constant ERROR_TYPE_PO_NOT_FOUND */
    public static final String ERROR_TYPE_PO_NOT_FOUND = "4";

    /** Constant ERROR_TYPE_DO_DUP */
    public static final String ERROR_TYPE_DO_DUP = "5";

    /** Constant ERROR_TYPE_DO_ALREADY_CREATE_ASN */
    public static final String ERROR_TYPE_DO_ALREADY_CREATE_ASN = "6";

    /** Constant ERROR_TYPE_DO_ORIGINAL_NOT_FOUND */
    public static final String ERROR_TYPE_DO_ORIGINAL_NOT_FOUND = "7";

    /** Constant ERROR_TYPE_PREVIOUS_REV_ERROR */
    public static final String ERROR_TYPE_PREVIOUS_REV_ERROR = "8";

    /** Constant ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND */
    public static final String ERROR_TYPE_BACK_ORDER_VENDOR_NOT_FOUND = "9";
    
    /** Constant ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND */
    public static final String ERROR_TYPE_BACK_ORDER_PARTS_NO_NOT_FOUND = "10";
    
    /** Constant ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST */
    public static final String ERROR_TYPE_BACK_ORDER_DO_NOT_EXIST = "11";
    
    /** Constant ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN */
    public static final String ERROR_TYPE_BACK_ORDER_DO_QTY_NOT_MATCH_ASN = "12";
    
    /** Constant ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST */
    public static final String ERROR_TYPE_BACK_ORDER_CHANGE_DO_NOT_EXIST = "13";
    
    /** Constant ERROR_TYPE_BACK_ORDER_CAHNGE_DO_QTY_NOT_MATCH_ASN */
    public static final String ERROR_TYPE_BACK_ORDER_CHANGE_DO_QTY_NOT_MATCH_ASN = "14";

    /** Constant MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_VENDOR_NOTFOUND
        = "CIGMA_ERROR_HEADER_VENDOR_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_PARTS_NOTFOUND
        = "CIGMA_ERROR_HEADER_PARTS_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_DO_DUPLICATE */
    public static final String MAIL_CIGMA_ERROR_HEADER_DO_DUPLICATE
        = "CIGMA_ERROR_HEADER_DO_DUPLICATE";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_DO_DUPLICATE */
    public static final String MAIL_CIGMA_ERROR_HEADER_PO_NOTFOUND
        = "CIGMA_ERROR_HEADER_PO_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_DO_ORIGINAL_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_DO_ORIGINAL_NOTFOUND
        = "CIGMA_ERROR_HEADER_DO_ORIGINAL_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_DO_ALREADY_CREATE_ASN */
    public static final String MAIL_CIGMA_ERROR_HEADER_DO_ALREADY_CREATE_ASN
        = "CIGMA_ERROR_HEADER_DO_ALREADY_CREATE_ASN";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE */
    public static final String MAIL_CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE
        = "CIGMA_ERROR_HEADER_ORDER_DATE_LESS_THAN_EFFECTIVE";
    /** Constant MAIL_CIGMA_ERROR_HEADER_PREVIOUS_REV_ERROR */
    public static final String MAIL_CIGMA_ERROR_HEADER_PREVIOUS_REV_ERRO
        = "CIGMA_ERROR_HEADER_PREVIOUS_REV_ERROR";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND
        = "CIGMA_ERROR_HEADER_BACKORDER_VENDOR_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND
        = "CIGMA_ERROR_HEADER_BACKORDER_PART_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTFOUND
        = "CIGMA_ERROR_HEADER_BACKORDER_DO_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTMATCH_ASN */
    public static final String MAIL_CIGMA_ERROR_HEADER_BACKORDER_DO_NOTMATCH_ASN
        = "CIGMA_ERROR_HEADER_BACKORDER_DO_NOTMATCH_ASN";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND */
    public static final String MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND
        = "CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTFOUND";
    
    /** Constant MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN */
    public static final String MAIL_CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN
        = "CIGMA_ERROR_HEADER_BACKORDER_CHANGE_DO_NOTMATCH_ASN";
    
    // [IN070] new label for transfer error email
    /** Constant MAIL_CIGMA_ERROR_PERSON_IN_CHARGE */
    public static final String MAIL_CIGMA_ERROR_PERSON_IN_CHARGE = "CIGMA_ERROR_PERSON_IN_CHARGE";
    
    /** Constant DO_DATA_TYPE_MAP */
    //public static final Map<String, String> DO_DATA_TYPE_MAP
    //    = new HashMap<String, String>(4);
    
    /** Constant PO_DATA_TYPE_MAP */
    //public static final Map<String, String> PO_DATA_TYPE_MAP
    //    = new HashMap<String, String>(4);

    /** Constant SORT_PART_MASTER */
    public static final String SORT_PART_MASTER =  " EFFECT_DATE desc ";

    /** Constant SORT_PART_MASTER */
    public static final String SORT_PART_MASTER_ASC =  " EFFECT_DATE ";

    // Start : [IN068] Add Issue Date to sorting condition
    //public static final String SORT_ERROR_FOR_SEND_TO_SUPPLIER 
    //    = " ERROR.S_CD, ERROR.ERROR_TYPE_FLG, ERROR.D_CD, ERROR.D_PCD"
    //        + ", ERROR.CIGMA_DO_NO, ERROR.D_PN ";
    //public static final String SORT_ERROR_FOR_SEND_TO_DENSO
    //    = " ERROR.D_CD, ERROR.D_PCD, ERROR.ERROR_TYPE_FLG, ERROR.S_CD"
    //        + ", ERROR.CIGMA_DO_NO, ERROR.D_PN ";

    /** Constant SORT_ERROR_FOR_SEND_TO_SUPPLIER */
    public static final String SORT_ERROR_FOR_SEND_TO_SUPPLIER 
        = " ERROR.S_CD, ERROR.ERROR_TYPE_FLG, ERROR.D_CD, ERROR.D_PCD"
            + ", ERROR.ISSUE_DATE, ERROR.CIGMA_DO_NO, ERROR.D_PN ";
    
    /** Constant SORT_ERROR_FOR_SEND_TO_DENSO */
    public static final String SORT_ERROR_FOR_SEND_TO_DENSO
        = " ERROR.D_CD, ERROR.D_PCD, ERROR.ERROR_TYPE_FLG, ERROR.S_CD"
            + ", ERROR.ISSUE_DATE, ERROR.CIGMA_DO_NO, ERROR.D_PN ";
    // End : [IN068] Add Issue Date to sorting condition
    
    // [IN016] Error Email sent by plant (if possible)
    ///** Constant SORT_ERROR_FOR_SEND_TO_SUPPLIER */
    //public static final String SORT_ERROR_PO_FOR_SEND_TO_SUPPLIER 
    //    = " ERROR.S_CD, ERROR.ERROR_TYPE_FLG, ERROR.D_CD, ERROR.D_PCD"
    //        + ", ERROR.CIGMA_PO_NO, ERROR.D_PN ";
    /** Constant SORT_ERROR_FOR_SEND_TO_SUPPLIER */
    public static final String SORT_ERROR_PO_FOR_SEND_TO_SUPPLIER 
        = " ERROR.S_CD, EFFECT.S_PCD, ERROR.ERROR_TYPE_FLG, ERROR.D_CD, ERROR.D_PCD"
            + ", ERROR.CIGMA_PO_NO, ERROR.D_PN ";
    
    /** Constant SORT_ERROR_FOR_SEND_TO_DENSO */
    public static final String SORT_ERROR_PO_FOR_SEND_TO_DENSO
        = " ERROR.D_CD, ERROR.D_PCD, ERROR.ERROR_TYPE_FLG, ERROR.S_CD"
            + ", ERROR.CIGMA_PO_NO, ERROR.D_PN ";
    
    /** The Constant PREFERRED_ORDER_ANNOUNCE_MESSAGE. */
    public static final String PREFERRED_ORDER_ANNOUNCE_MESSAGE = "EFFECT_START desc";

    /** The Constant ERROR_TYPE_FLG_FOR_SEND_TO_SUPPLIER. */
    public static final String ERROR_TYPE_FLG_FOR_SEND_TO_SUPPLIER = " '2', '3' ";

    // [IN068] swap flag value
    //public static final String ERROR_TYPE_FLG_FOR_SEND_TO_DENSO = " '4', '5', '7', '8' ";
    //public static final String ERROR_TYPE_FLG_FOR_SEND_TO_ADMIN = " '2', '3', '6' ";
    /** The Constant ERROR_TYPE_FLG_FOR_SEND_TO_DENSO. */
    public static final String ERROR_TYPE_FLG_FOR_SEND_TO_DENSO = " '2', '3', '6' ";
    /** The Constant ERROR_TYPE_FLG_FOR_SEND_TO_ADMIN. */
    public static final String ERROR_TYPE_FLG_FOR_SEND_TO_ADMIN = " '4', '5', '7', '8' ";
    public static final String ERROR_TYPE_FLG_DOBACKORDER_FOR_SEND_TO_ADMIN = " '9', '10', '11', '12', '13', '14' ";

    static{
        PO_ORDER_TYPE.put("0", "FIX");
        PO_ORDER_TYPE.put("1", "KB");
        
        PO_PERIOD_TYPE.put("0", "C");
        PO_PERIOD_TYPE.put("1", "D");
        
        PO_DWM.put("0", "D");
        PO_DWM.put("1", "W");
        PO_DWM.put("2", "M");
        
        ASN_STATUS_MAP.put("N", "ISS");
        ASN_STATUS_MAP.put("C", "CCL");
        ASN_STATUS_MAP.put("P", "PTR");
        ASN_STATUS_MAP.put("R", "RCP");
                                
        PN_STATUS_MAP.put("N", "NEW");
        PN_STATUS_MAP.put("C", "CCL");
        PN_STATUS_MAP.put("P", "PTR");
        PN_STATUS_MAP.put("R", "RCP");
        
        //DO_DATA_TYPE_MAP.put("DCD", "DDO");
        //DO_DATA_TYPE_MAP.put("KCD", "KDO");
        
        //PO_DATA_TYPE_MAP.put("DCP", "DPO");
        //PO_DATA_TYPE_MAP.put("ICP", "IPO");
        //PO_DATA_TYPE_MAP.put("KCP", "KPO");
        //PO_DATA_TYPE_MAP.put("IKC", "IKP");
        

        // [IN054] Set DENSO Reply map
        PO_REPLY_TYPE.put("0", "Reject");
        PO_REPLY_TYPE.put("1", "Accept");
    }
    /** Additional constants for SPS Phase II. */
    /** The Constant QR_CODE_ASN_SYSTEM_ID. */
    public static final String QR_CODE_ASN_SYSTEM_ID = "SPS";
    
    /** The Constant QR_CODE_ASN_SYSTEM_ID. */
    public static final String QR_CODE_ASN_TYPE = "02";
    
    /** The Constant SPS_KANBAN_FLAG_TRANSFER. */
    public static final String SPS_KANBAN_FLAG_TRANSFER = Constants.STR_ONE;
    
    /** The Constant SPS_KANBAN_FLAG_TRANSFERED. */
    public static final String SPS_KANBAN_FLAG_TRANSFERED = Constants.STR_TWO;

    /** The Constant SPS_SPLIT_QR_CONTENT_SIGN. */
    public static final String SPS_SPLIT_QR_CONTENT_SIGN = "#";
    
    /** Average of waiting time per 1 record */
    public static final Integer AVERAGE_PER_RECORD = 1000;
    
    /**
     * Instantiates a new constants.
     */
    public SupplierPortalConstant() {
        super();
    }

    /**
     * For use PO_STATUS_ISSUED in JSP.
     * @return PO_STATUS_ISSUED
     * */
    public String getPO_STATUS_ISSUED() {
        return PO_STATUS_ISSUED;
    }
    
    /** Constant TRANSFER_ASN_STEP_PROCESS_NAME */
    public static final String TRANSFER_ASN_STEP_PROCESS_NAME = "TRANSFER_ASN_PROCNAME";
}
