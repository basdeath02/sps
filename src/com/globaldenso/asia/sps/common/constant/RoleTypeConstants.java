/*
 * ModifyDate Developmentcompany Describe 
 * 2014/12/03 CSI Akat           Create
 * 2016/02/17 CSI Akat           [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.constant;

/**
 * The Class RoleTypeConstants.
 * @author CSI
 */
public class RoleTypeConstants {
    
    /** Constants for Button/Link name WADM001_bSearch */
    public static final String WADM001_BSEARCH = "WADM001_bSearch";

    /** Constants for Button/Link name WADM001_bReset */
    public static final String WADM001_BRESET = "WADM001_bReset";

    /** Constants for Button/Link name WADM001_bDelete */
    public static final String WADM001_BDELETE = "WADM001_bDelete";

    /** Constants for Button/Link name WADM001_bDownload */
    public static final String WADM001_BDOWNLOAD = "WADM001_bDownload";

    /** Constants for Button/Link name WADM001_lDSCId */
    public static final String WADM001_LDSCID = "WADM001_lDSCId";

    /** Constants for Button/Link name WADM001_lAssignRole */
    public static final String WADM001_LASSIGNROLE = "WADM001_lAssignRole";

    /** Constants for Button/Link name WADM002_bRegister */
    public static final String WADM002_BREGISTER = "WADM002_bRegister";

    /** Constants for Button/Link name WADM002_bReset */
    public static final String WADM002_BRESET = "WADM002_bReset";

    /** Constants for Button/Link name WADM002_bReturn */
    public static final String WADM002_BRETURN = "WADM002_bReturn";

    /** Constants for Button/Link name WADM003_bBrowse */
    public static final String WADM003_BBROWSE = "WADM003_bBrowse";

    /** Constants for Button/Link name WADM003_bUpload */
    public static final String WADM003_BUPLOAD = "WADM003_bUpload";

    /** Constants for Button/Link name WADM003_bReset */
    public static final String WADM003_BRESET = "WADM003_bReset";

    /** Constants for Button/Link name WADM003_bRegister */
    public static final String WADM003_BREGISTER = "WADM003_bRegister";

    /** Constants for Button/Link name WADM004_bAdd */
    public static final String WADM004_BADD = "WADM004_bAdd";

    /** Constants for Button/Link name WADM004_bDelete */
    public static final String WADM004_BDELETE = "WADM004_bDelete";

    /** Constants for Button/Link name WADM004_bReturn */
    public static final String WADM004_BRETURN = "WADM004_bReturn";

    /** Constants for Button/Link name WADM004_pOK */
    public static final String WADM004_POK = "WADM004_pOK";

    /** Constants for Button/Link name WADM004_pCancel */
    public static final String WADM004_PCANCEL = "WADM004_pCancel";

    /** Constants for Button/Link name WADM004_lUpdate */
    public static final String WADM004_LUPDATE = "WADM004_lUpdate";

    /** Constants for Button/Link name WADM005_bSearch */
    public static final String WADM005_BSEARCH = "WADM005_bSearch";

    /** Constants for Button/Link name WADM005_bReset */
    public static final String WADM005_BRESET = "WADM005_bReset";

    /** Constants for Button/Link name WADM005_bDownload */
    public static final String WADM005_BDOWNLOAD = "WADM005_bDownload";
    
    /** Constants for Button/Link name WADM005_bDelete */
    public static final String WADM005_BDELETE = "WADM005_bDelete";

    /** Constants for Button/Link name WADM005_lDSCId */
    public static final String WADM005_LDSCID = "WADM005_lDSCId";

    /** Constants for Button/Link name WADM005_lAssignRole */
    public static final String WADM005_LASSIGNROLE = "WADM005_lAssignRole";
    
    /** Constants for Button/Link name WADM006_bReturn */
    public static final String WADM006_BRETURN = "WADM006_bReturn";

    /** Constants for Button/Link name WADM006_bRegister */
    public static final String WADM006_BREGISTER = "WADM006_bRegister";
    
    /** Constants for Button/Link name WADM006_bReset */
    public static final String WADM006_BRESET = "WADM006_bReset";

    /** Constants for Button/Link name WADM007_bBrowse */
    public static final String WADM007_BBROWSE = "WADM007_bBrowse";

    /** Constants for Button/Link name WADM007_bUpload */
    public static final String WADM007_BUPLOAD = "WADM007_bUpload";

    /** Constants for Button/Link name WADM007_bReset */
    public static final String WADM007_BRESET = "WADM007_bReset";

    /** Constants for Button/Link name WADM007_bRegister */
    public static final String WADM007_BREGISTER = "WADM007_bRegister";

    /** Constants for Button/Link name WADM008_bAdd */
    public static final String WADM008_BADD = "WADM008_bAdd";

    /** Constants for Button/Link name WADM008_bDelete */
    public static final String WADM008_BDELETE = "WADM008_bDelete";

    /** Constants for Button/Link name WADM008_bReturn */
    public static final String WADM008_BRETURN = "WADM008_bReturn";

    /** Constants for Button/Link name WADM008_pOK */
    public static final String WADM008_POK = "WADM008_pOK";

    /** Constants for Button/Link name WADM008_pCancel */
    public static final String WADM008_PCANCEL = "WADM008_pCancel";

    /** Constants for Button/Link name WADM008_lUpdate */
    public static final String WADM008_LUPDATE = "WADM008_lUpdate";

    // Start : [IN063] New screen
    /** Constants for Button/Link name WADM009_BRESET */
    public static final String WADM009_BRESET = "WADM009_bReset";

    /** Constants for Button/Link name WADM009_BSEARCH */
    public static final String WADM009_BSEARCH = "WADM009_bSearch";

    /** Constants for Button/Link name WADM009_LDOWNLOAD */
    public static final String WADM009_LDOWNLOAD = "WADM009_lDownload";
    
    /** Constants for Button/Link name WADM009_LDELETE */
    public static final String WADM009_LDELETE = "WADM009_lDelete";
    
    /** Constants for Button/Link name WADM009_BREGISTER */
    public static final String WADM009_BREGISTER = "WADM009_bRegister";

    /** Constants for Button/Link name WADM009_BBROWSE */
    public static final String WADM009_BBROWSE = "WADM009_bBrowse";
    // End : [IN063] New screen
    
    /** Constants for Button/Link name WINV001_bSearch */
    public static final String WINV001_BSEARCH = "WINV001_bSearch";

    /** Constants for Button/Link name WINV001_bReset */
    public static final String WINV001_BRESET = "WINV001_bReset";

    /** Constants for Button/Link name WINV001_bDownload */
    public static final String WINV001_BDOWNLOAD = "WINV001_bDownload";

    /** Constants for Button/Link name WINV001_lInvoiceNo */
    public static final String WINV001_LINVOICENO = "WINV001_lInvoiceNo";

    /** Constants for Button/Link name WINV001_lCancelInvoice */
    public static final String WINV001_LCANCELINVOICE = "WINV001_lCancelInvoice";

    /** Constants for Button/Link name WINV001_lCoverPage */
    public static final String WINV001_LCOVERPAGE = "WINV001_lCoverPage";

    /** Constants for Button/Link name WINV002_bSearch */
    public static final String WINV002_BSEARCH = "WINV002_bSearch";

    /** Constants for Button/Link name WINV002_bReset */
    public static final String WINV002_BRESET = "WINV002_bReset";

    /** Constants for Button/Link name WINV002_bGroupInvoice */
    public static final String WINV002_BGROUPINVOICE = "WINV002_bGroupInvoice";

    /** Constants for Button/Link name WINV003_bBrowse */
    public static final String WINV003_BBROWSE = "WINV003_bBrowse";

    /** Constants for Button/Link name WINV003_bUpload */
    public static final String WINV003_BUPLOAD = "WINV003_bUpload";

    /** Constants for Button/Link name WINV003_bReset */
    public static final String WINV003_BRESET = "WINV003_bReset";

    /** Constants for Button/Link name WINV003_bDownload */
    public static final String WINV003_BDOWNLOAD = "WINV003_bDownload";

    /** Constants for Button/Link name WINV003_bGroupInvoice */
    public static final String WINV003_BGROUPINVOICE = "WINV003_bGroupInvoice";

    /** Constants for Button/Link name WINV004_bPreviewCoverPage */
    public static final String WINV004_BPREVIEWCOVERPAGE = "WINV004_bPreviewCoverPage";

    /** Constants for Button/Link name WINV004_bDownload */
    public static final String WINV004_BDOWNLOAD = "WINV004_bDownload";

    /** Constants for Button/Link name WINV004_bRegister */
    public static final String WINV004_BREGISTER = "WINV004_bRegister";

    /** Constants for Button/Link name WINV004_bReturn */
    public static final String WINV004_BRETURN = "WINV004_bReturn";
    
    /** Constants for Button/Link name WINV004_bDownloadPDF */
    public static final String WINV004_BDOWNLOADPDF = "WINV004_bDownloadPDF";

    /** Constants for Button/Link name WINV004_bCalculateCN */
    public static final String WINV004_BCALCULATECN = "WINV004_bCalculateCN";
    
    /** Constants for Button/Link name WINV005_bSearch */
    public static final String WINV005_BSEARCH = "WINV005_bSearch";

    /** Constants for Button/Link name WINV005_bReset */
    public static final String WINV005_BRESET = "WINV005_bReset";

    /** Constants for Button/Link name WINV005_bDownload */
    public static final String WINV005_BDOWNLOAD = "WINV005_bDownload";

    /** Constants for Button/Link name WINV005_lInvoiceNo */
    public static final String WINV005_LINVOICENO = "WINV005_lInvoiceNo";

    /** Constants for Button/Link name WINV006_bBrowse */
    public static final String WINV006_BBROWSE = "WINV006_bBrowse";

    /** Constants for Button/Link name WINV006_bUpload */
    public static final String WINV006_BUPLOAD = "WINV006_bUpload";

    /** Constants for Button/Link name WINV006_bReset */
    public static final String WINV006_BRESET = "WINV006_bReset";

    /** Constants for Button/Link name WINV007_bSearch */
    public static final String WINV007_BSEARCH = "WINV007_bSearch";

    /** Constants for Button/Link name WINV007_bReset */
    public static final String WINV007_BRESET = "WINV007_bReset";

    /** Constants for Button/Link name WINV007_lDownload */
    public static final String WINV007_LDOWNLOAD = "WINV007_lDownload";

    /** Constants for Button/Link name WORD001_bBrowse */
    public static final String WORD001_BBROWSE = "WORD001_bBrowse";

    /** Constants for Button/Link name WORD001_bUpload */
    public static final String WORD001_BUPLOAD = "WORD001_bUpload";

    /** Constants for Button/Link name WORD001_bExport */
    public static final String WORD001_BEXPORT = "WORD001_bExport";

    /** Constants for Button/Link name WORD001_bReset */
    public static final String WORD001_BRESET = "WORD001_bReset";

    /** Constants for Button/Link name WORD001_bRegister */
    public static final String WORD001_BREGISTER = "WORD001_bRegister";

    /** Constants for Button/Link name WORD002_bSearch */
    public static final String WORD002_BSEARCH = "WORD002_bSearch";

    /** Constants for Button/Link name WORD002_bReset */
    public static final String WORD002_BRESET = "WORD002_bReset";

    /** Constants for Button/Link name WORD002_bDownload */
    public static final String WORD002_BDOWNLOAD = "WORD002_bDownload";

    /** Constants for Button/Link name WORD002_bAcknowledge */
    public static final String WORD002_BACKNOWLEDGE = "WORD002_bAcknowledge";

    /** Constants for Button/Link name WORD002_lSPSPONo */
    public static final String WORD002_LSPSPONO = "WORD002_lSPSPONo";

    /** Constants for Button/Link name WORD002_lPDFOriginal */
    public static final String WORD002_LPDFORIGINAL = "WORD002_lPDFOriginal";

    /** Constants for Button/Link name WORD002_lPDFChange */
    public static final String WORD002_LPDFCHANGE = "WORD002_lPDFChange";

    /** Constants for Button/Link name WORD003_bPreviewPending */
    public static final String WORD003_BPREVIEWPENDING = "WORD003_bPreviewPending";

    /** Constants for Button/Link name WORD003_bSaveSend */
    public static final String WORD003_BSAVESEND = "WORD003_bSaveSend";

    /** Constants for Button/Link name WORD003_bReturn */
    public static final String WORD003_BRETURN = "WORD003_bReturn";

    /** Constants for Button/Link name WORD003_lEditView */
    public static final String WORD003_LEDITVIEW = "WORD003_lEditView";
    
    // [IN054] add new button
    /** Constants for Button/Link name WORD003_bDensoReply */
    public static final String WORD003_BDENSOREPLY = "WORD003_bDensoReply";
    
    /** Constants for Button/Link name WORD004_bRegister */
    public static final String WORD004_BREGISTER = "WORD004_bRegister";

    /** Constants for Button/Link name WORD004_bReturn */
    public static final String WORD004_BRETURN = "WORD004_bReturn";
    
    // [IN054] add new button
    /** Constants for Button/Link name WORD003_bDensoReply */
    public static final String WORD004_BSAVE = "WORD004_bSave";
    
    /** Constants for Button/Link name WORD005_bReturn */
    public static final String WORD005_BRETURN = "WORD005_bReturn";

    /** Constants for Button/Link name WORD006_bSearch */
    public static final String WORD006_BSEARCH = "WORD006_bSearch";

    /** Constants for Button/Link name WORD006_bReset */
    public static final String WORD006_BRESET = "WORD006_bReset";

    /** Constants for Button/Link name WORD006_bDownload */
    public static final String WORD006_BDOWNLOAD = "WORD006_bDownload";

    /** Constants for Button/Link name WORD006_lViewPDF */
    public static final String WORD006_LVIEWPDF = "WORD006_lViewPDF";

    /** Constants for Button/Link name WORD007_bSearch */
    public static final String WORD007_BSEARCH = "WORD007_bSearch";

    /** Constants for Button/Link name WORD007_bReset */
    public static final String WORD007_BRESET = "WORD007_bReset";

    /** Constants for Button/Link name WORD007_bDownload */
    public static final String WORD007_BDOWNLOAD = "WORD007_bDownload";

    /** Constants for Button/Link name WORD007_lViewPDF */
    public static final String WORD007_LVIEWPDF = "WORD007_lViewPDF";

    /** Constants for Button/Link name WSHP001_bSearch */
    public static final String WSHP001_BSEARCH = "WSHP001_bSearch";

    /** Constants for Button/Link name WSHP001_bReset */
    public static final String WSHP001_BRESET = "WSHP001_bReset";

    /** Constants for Button/Link name WSHP001_bGroupASN */
    public static final String WSHP001_BGROUPASN = "WSHP001_bGroupASN";

    /** Constants for Button/Link name WSHP001_lShipNotice */
    public static final String WSHP001_LSHIPNOTICE = "WSHP001_lShipNotice";

    /** Constants for Button/Link name WSHP001_lViewPDF */
    public static final String WSHP001_LVIEWPDF = "WSHP001_lViewPDF";

    /** Constants for Button/Link name WSHP003_bReturn */
    public static final String WSHP003_BRETURN = "WSHP003_bReturn";

    /** Constants for Button/Link name WSHP003_lASNNo */
    public static final String WSHP003_LASNNO = "WSHP003_lASNNo";

    /** Constants for Button/Link name WSHP004_bReviseShippingQTY */
    public static final String WSHP004_BREVISESHIPPINGQTY = "WSHP004_bReviseShippingQTY";

    /** Constants for Button/Link name WSHP004_bSaveSend */
    public static final String WSHP004_BSAVESEND = "WSHP004_bSaveSend";

    /** Constants for Button/Link name WSHP004_bViewPDF */
    public static final String WSHP004_BVIEWPDF = "WSHP004_bViewPDF";

    /** Constants for Button/Link name WSHP004_bDownloadPDF */
    public static final String WSHP004_BDOWNLOADPDF = "WSHP004_bDownloadPDF";

    /** Constants for Button/Link name WSHP004_bReturn */
    public static final String WSHP004_BRETURN = "WSHP004_bReturn";

    /** Constants for Button/Link name WSHP004_bCancelASNSend */
    public static final String WSHP004_BCANCELASNSEND = "WSHP004_bCancelASNSend";
    
    /** Constants for Button/Link name WSHP004_bAllowReviseQty */
    public static final String WSHP004_BALLOWREVISEQTY = "WSHP004_bAllowReviseQty";

    /** Constants for Button/Link name WSHP006_bBrowse */
    public static final String WSHP006_BBROWSE = "WSHP006_bBrowse";

    /** Constants for Button/Link name WSHP006_bUpload */
    public static final String WSHP006_BUPLOAD = "WSHP006_bUpload";

    /** Constants for Button/Link name WSHP006_bReset */
    public static final String WSHP006_BRESET = "WSHP006_bReset";

    /** Constants for Button/Link name WSHP006_bDownload */
    public static final String WSHP006_BDOWNLOAD = "WSHP006_bDownload";

    /** Constants for Button/Link name WSHP006_bRegister */
    public static final String WSHP006_BREGISTER = "WSHP006_bRegister";

    /** Constants for Button/Link name WSHP007_bSearch */
    public static final String WSHP007_BSEARCH = "WSHP007_bSearch";

    /** Constants for Button/Link name WSHP007_bReset */
    public static final String WSHP007_BRESET = "WSHP007_bReset";

    /** Constants for Button/Link name WSHP007_bDownload */
    public static final String WSHP007_BDOWNLOAD = "WSHP007_bDownload";

    /** Constants for Button/Link name WSHP007_lSPSDONo */
    public static final String WSHP007_LSPSDONO = "WSHP007_lSPSDONo";

    /** Constants for Button/Link name WSHP008_bSearch */
    public static final String WSHP008_BSEARCH = "WSHP008_bSearch";

    /** Constants for Button/Link name WSHP008_bReset */
    public static final String WSHP008_BRESET = "WSHP008_bReset";

    /** Constants for Button/Link name WSHP008_bDownload */
    public static final String WSHP008_BDOWNLOAD = "WSHP008_bDownload";

    /** Constants for Button/Link name WSHP008_lASNNo */
    public static final String WSHP008_LASNNO = "WSHP008_lASNNo";

    /** Constants for Button/Link name WSHP008_lSPSDONo */
    public static final String WSHP008_LSPSDONO = "WSHP008_lSPSDONo";

    /** Constants for Button/Link name WSHP008_lASNPDF */
    public static final String WSHP008_LASNPDF = "WSHP008_lASNPDF";
    
    /** Constants for Button/Link name WSHP009_lASNNo */
    public static final String WSHP009_LASNNO = "WSHP009_lASNNo";

    /** Constants for Button/Link name WSHP009_bSearch */
    public static final String WSHP009_BSEARCH = "WSHP009_bSearch";

    /** Constants for Button/Link name WSHP009_bReset */
    public static final String WSHP009_BRESET = "WSHP009_bReset";

    /** Constants for Button/Link name WSHP009_bDownload */
    public static final String WSHP009_BDOWNLOAD = "WSHP009_bDownload";

    /** Constants for Button/Link name WSHP009_lInvoiceNo */
    public static final String WSHP009_LINVOICENO = "WSHP009_lInvoiceNo";

    /** The default constructor. */
    public RoleTypeConstants() {
        super();
    }

}
