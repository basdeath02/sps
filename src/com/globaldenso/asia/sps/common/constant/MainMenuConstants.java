/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.constant;

/**
 * The Class Constants.
 * @author CSI
 */
public class MainMenuConstants {  

    /* MENU ID ================================================================================== */
    /** The constant MENU_ID_HM. */
    public static final String MENU_ID_HM = "HM";
    
    /** The constant MENU_ID_OR. */
    public static final String MENU_ID_OR = "OR";
    
    /** The constant MENU_ID_SH. */
    public static final String MENU_ID_SH = "SH";
    
    /** The constant MENU_ID_IV. */
    public static final String MENU_ID_IV = "IV";
    
    /** The constant MENU_ID_AD. */
    public static final String MENU_ID_AD = "AD";
    
    /** The constant MENU_ID_CT. */
    public static final String MENU_ID_CT = "CT";
    
    /** The constant MENU_ID_HP. */
    public static final String MENU_ID_HP = "HP";
    
    /** The constant MENU_ID_LO. */
    public static final String MENU_ID_LO = "LO";
    
    /** The constant MENU_ID_HM_MN. */
    public static final String MENU_ID_HM_MN = "HM_MN";
    
    /** The constant MENU_ID_OR_SU. */
    public static final String MENU_ID_OR_SU = "OR_SU";
    
    /** The constant MENU_ID_OR_PO. */
    public static final String MENU_ID_OR_PO = "OR_PO";
    
    /** The constant MENU_ID_OR_DO. */
    public static final String MENU_ID_OR_DO = "OR_DO";
    
    /** The constant MENU_ID_OR_KO. */
    public static final String MENU_ID_OR_KO = "OR_KO";
    
    /** The constant MENU_ID_SH_AC. */
    public static final String MENU_ID_SH_AC = "SH_AC";
    
    /** The constant MENU_ID_SH_AU. */
    public static final String MENU_ID_SH_AU = "SH_AU";
    
    /** The constant MENU_ID_SH_AD. */
    public static final String MENU_ID_SH_AD = "SH_AD";
    
    /** The constant MENU_ID_SH_AP. */
    public static final String MENU_ID_SH_AP = "SH_AP";
    
    /** The constant MENU_ID_SH_BO. */
    public static final String MENU_ID_SH_BO = "SH_BO";
    
    /** The constant MENU_ID_IV_AS. */
    public static final String MENU_ID_IV_AS = "IV_AS";
    
    /** The constant MENU_ID_IV_IU. */
    public static final String MENU_ID_IV_IU = "IV_IU";
    
    /** The constant MENU_ID_IV_II. */
    public static final String MENU_ID_IV_II = "IV_II";
    
    /** The constant MENU_ID_IV_DP. */
    public static final String MENU_ID_IV_DP = "IV_DP";
    
    /** The constant MENU_ID_IV_UF. */
    public static final String MENU_ID_IV_UF = "IV_UF";
    
    /** The constant MENU_ID_IV_DF. */
    public static final String MENU_ID_IV_DF = "IV_DF";
    
    /** The constant MENU_ID_AD_SU. */
    public static final String MENU_ID_AD_SU = "AD_SU";
    
    /** The constant MENU_ID_AD_DU. */
    public static final String MENU_ID_AD_DU = "AD_DU";
    
    // Start : [IN063] add new menu
    /** The constant MENU_ID_AD_DG. */
    public static final String MENU_ID_AD_DG = "AD_DG";
    
    /** The constant MENU_ID_AD_DG_DU. */
    public static final String MENU_ID_AD_DG_DU = "AD_DG_DU";
    // End : [IN063] add new menu
    
    
    /** The constant MENU_ID_AD_SU_SI. */
    public static final String MENU_ID_AD_SU_SI = "AD_SU_SI";
    
    /** The constant MENU_ID_AD_SU_SR. */
    public static final String MENU_ID_AD_SU_SR = "AD_SU_SR";
    
    /** The constant MENU_ID_AD_SU_SU. */
    public static final String MENU_ID_AD_SU_SU = "AD_SU_SU";
    
    /** The constant MENU_ID_AD_DU_DI. */
    public static final String MENU_ID_AD_DU_DI = "AD_DU_DI";
    
    /** The constant MENU_ID_AD_DU_DR. */
    public static final String MENU_ID_AD_DU_DR = "AD_DU_DR";
    
    /** The constant MENU_ID_AD_DU_DU. */
    public static final String MENU_ID_AD_DU_DU = "AD_DU_DU";
    
    
    /**
     * Instantiates a new constants.
     */
    public MainMenuConstants(){
        super();
    }
    
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_HM() {
        return MENU_ID_HM;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_OR() {
        return MENU_ID_OR;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_SH() {
        return MENU_ID_SH;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV() {
        return MENU_ID_IV;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD() {
        return MENU_ID_AD;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_CT() {
        return MENU_ID_CT;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_HP() {
        return MENU_ID_HP;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_LO() {
        return MENU_ID_LO;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_HM_MN() {
        return MENU_ID_HM_MN;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_OR_SU() {
        return MENU_ID_OR_SU;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_OR_PO() {
        return MENU_ID_OR_PO;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_OR_DO() {
        return MENU_ID_OR_DO;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_OR_KO() {
        return MENU_ID_OR_KO;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_SH_AC() {
        return MENU_ID_SH_AC;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_SH_AU() {
        return MENU_ID_SH_AU;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_SH_AD() {
        return MENU_ID_SH_AD;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_SH_AP() {
        return MENU_ID_SH_AP;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_SH_BO() {
        return MENU_ID_SH_BO;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV_AS() {
        return MENU_ID_IV_AS;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV_IU() {
        return MENU_ID_IV_IU;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV_II() {
        return MENU_ID_IV_II;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV_DP() {
        return MENU_ID_IV_DP;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV_UF() {
        return MENU_ID_IV_UF;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_IV_DF() {
        return MENU_ID_IV_DF;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_SU() {
        return MENU_ID_AD_SU;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_DU() {
        return MENU_ID_AD_DU;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_SU_SI() {
        return MENU_ID_AD_SU_SI;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_SU_SR() {
        return MENU_ID_AD_SU_SR;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_SU_SU() {
        return MENU_ID_AD_SU_SU;
    }
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_DU_DI() {
        return MENU_ID_AD_DU_DI;
    }
    
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_DU_DR() {
        return MENU_ID_AD_DU_DR;
    }
    
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_DU_DU() {
        return MENU_ID_AD_DU_DU;
    }
    
    // Start : [IN063] add new menu
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_DG() {
        return MENU_ID_AD_DG;
    }
    
    /**
     * <p>Getter method for menu Id.</p>
     *
     * @return the menu Id.
     */
    public String getMENU_ID_AD_DG_DU() {
        return MENU_ID_AD_DG_DU;
    }
    // End : [IN063] add new menu
    
}