/*
 * ModifyDate Development company     Describe 
 * 2014/09/19 CSI Phakaporn           Create
 * 2015/08/24 CSI Akat                [IN012]
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/02/24 CSI Akat                [IN054]
 * 2015/08/22 Netband U.Rungsiwut     SPS phase II
 * 
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.common.constant;

/**
 * The Class SQL Map Constants.
 * @author CSI
 */
public class SqlMapConstants {
   
    /* ASN Dao =========================================================== */
    /** The Constant ASN_SEARCH_SENDING_ASN. */
    public static final String ASN_SEARCH_SENDING_ASN 
        = "Asn.SearchSendingAsn";
    
    /** The Constant ASN_SEARCH_ASN_DETAIL. */
    public static final String ASN_SEARCH_ASN_DETAIL
        = "Asn.SearchAsnDetail";
    
    /** The Constant ASN_SEARCH_ASN_INFORMATION. */
    public static final String ASN_SEARCH_ASN_INFORMATION
        = "Asn.SearchAsnInformation";
    
    /** The Constant ASN_SEARCH_COUNT_ASN_INFORMATION. */
    public static final String ASN_SEARCH_COUNT_ASN_INFORMATION
        = "Asn.SearchCountAsnInformation";
    
    /** The Constant ASN_SEARCH_EXIST_ASN. */
    public static final String ASN_SEARCH_EXIST_ASN
        = "Asn.SearchExistAsn";
    
    /** The Constant ASN_SEARCH_ASN_REPORT. */
    public static final String ASN_SEARCH_ASN_REPORT
        = "Asn.SearchAsnReport";
    
    /** The Constant ASN_SEARCH_ASN_FOR_GROUP_INVOICE. */
    public static final String ASN_SEARCH_ASN_FOR_GROUP_INVOICE
        = "Asn.SearchAsnForGroupInvoice";
    
    /** The Constant ASN_SEARCH_PURGING_ASN_ORDER. */
    public static final String ASN_SEARCH_PURGING_ASN_ORDER
        = "Asn.SearchPurgingAsnOrder";
    

    // [IN012] For check delete P/O, D/O
    /** The Constant ASN_SEARCH_COUNT_NOT_PURGING_ASN_ORDER. */
    public static final String ASN_SEARCH_COUNT_NOT_PURGING_ASN_ORDER
        = "Asn.SearchCountNotPurgingAsn";
    
    /** The Constant ASN_SEARCH_SUM_SHIPPING_QTY. */
    public static final String ASN_SEARCH_SUM_SHIPPING_QTY = "Asn.SearchSumShippingQty";

    /** The Constant ASN_SEARCH_SUM_SHIPPING_QTY_BY_PART. */
    public static final String ASN_SEARCH_SUM_SHIPPING_QTY_BY_PART
        = "Asn.SearchSumShippingQtyByPart";        
        
    /** The Constant ASN_DELETE_SPS_T_ASN_DETAIL. */
    public static final String ASN_DELETE_SPS_T_ASN_DETAIL
        = "Asn.DeleteSpsTAsnDetail";
    
    /** The Constant ASN_DELETE_SPS_T_ASN. */
    public static final String ASN_DELETE_SPS_T_ASN
        = "Asn.DeleteSpsTAsn";
    
    /** The Constant ASN_DELETE_SPS_T_BHT_TRANSMIT_LOG. */
    public static final String ASN_DELETE_SPS_T_BHT_TRANSMIT_LOG
        = "Asn.DeleteSpsTBhtTransmitLog";
    
    /* Common Dao ============================================================================= */
    /** The Constant COMMON_SEARCH_SYSDATE. */
    public static final String COMMON_SEARCH_SYSDATE
        = "Common.SearchSysDate";
    
    /** The Constant COMMON_SEARCH_SYSDATE_BY_CONDITION. */
    public static final String COMMON_SEARCH_SYSDATE_BY_CONDITION
        = "Common.SearchSysDateByCondition";
    
    /* Company Denso Dao ====================================================================== */
    /** The Constant COMPANY_DENSO_SEARCH_COMPANY_DENSO_DETAIL. */
    public static final String COMPANY_DENSO_SEARCH_COMPANY_DENSO_DETAIL 
        = "CompanyDenso.SearchCompanyDensoDetail";
    
    /** The Constant COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_ROLE. */
    public static final String COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_ROLE 
        = "CompanyDenso.SearchCompanyDensoByRole";
    
    /** The Constant COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_RELATION. */
    public static final String COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_RELATION 
        = "CompanyDenso.SearchCompanyDensoByRelation";
    
    /** The Constant COMPANY_DENSO_SEARCH_AS400_SERVER_CONNECTION. */
    public static final String COMPANY_DENSO_SEARCH_AS400_SERVER_CONNECTION
        = "CompanyDenso.SearchAs400ServerConnection";
    
    /** The Constant COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_CODE_LIST. */
    public static final String COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_CODE_LIST
        = "CompanyDenso.SearchCompanyDensoByCodeList";
    
    /** The Constant COMPANY_DENSO_SEARCH_EXIST_COMPANY_DENSO_BY_ROLE. */
    public static final String COMPANY_DENSO_SEARCH_EXIST_COMPANY_DENSO_BY_ROLE
        = "CompanyDenso.SearchExistCompanyDensoByRole";
    
    /** The Constant COMPANY_DENSO_SEARCH_EXIST_COMPANY_DENSO_BY_RELATION. */
    public static final String COMPANY_DENSO_SEARCH_EXIST_COMPANY_DENSO_BY_RELATION 
        = "CompanyDenso.SearchExistCompanyDensoByRelation";
    
    /* Company Supplier Dao ===================================================================== */
    /** The Constant COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_ROLE. */
    public static final String COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_ROLE 
        = "CompanySupplier.SearchCompanySupplierByRole";
    
    /** The Constant COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_RELATION. */
    public static final String COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_RELATION
        = "CompanySupplier.SearchCompanySupplierByRelation";
    
    /** The Constant COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_NAME_BY_RELATION. */
    public static final String COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_NAME_BY_RELATION
        = "CompanySupplier.SearchCompanySupplierNameByRelation";
    
    /** The Constant COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_CODE. */
    public static final String COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_CODE
        = "CompanySupplier.SearchCompanySupplierByCode";
    
    /** The Constant COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_INFORMATION. */
    public static final String COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_INFORMATION
        = "CompanySupplier.SearchExistCompanySupplierInformation";
    
    /** The Constant COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_BY_RELATION. */
    public static final String COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_BY_RELATION
        = "CompanySupplier.SearchExistCompanySupplierByRelation";
    
    /** The Constant COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_BY_ROLE. */
    public static final String COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_BY_ROLE
        = "CompanySupplier.SearchExistCompanySupplierByRole";
    
    /* Denso Supplier Part Dao ===================================================================== */
    /** The Constant DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART. */
    public static final String DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART
        = "DensoSupplierPart.SearchDensoSupplierPart";
    
    /** The Constant DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART_BY_ROLE. */
    public static final String DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART_BY_ROLE
        = "DensoSupplierPart.SearchDensoSupplierPartByRole";
    
    /** The Constant DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART_BY_RELATION. */
    public static final String DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART_BY_RELATION
        = "DensoSupplierPart.SearchDensoSupplierPartByRelation";

    /* Denso Supplier Relation Dao ===================================================================== */
    /** The Constant DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART. */
    public static final String DENSO_SUPPLIER_RELATION_SEARCH_DENSO_SUPPLIER_RELATION_BY_ROLE_DENSO
        = "DensoSupplierRelation.SearchRelationByRoleDenso";
    
    /** The Constant DENSO_SUPPLIER_RELATION_SEARCH_DENSO_SUPPLIER_RELATION_BY_ROLE_SUPPLIER. */
    public static final String 
    DENSO_SUPPLIER_RELATION_SEARCH_DENSO_SUPPLIER_RELATION_BY_ROLE_SUPPLIER
        = "DensoSupplierRelation.SearchRelationByRoleSupplier";
    
    /* Delivery Order Dao ===================================================================== */
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_ACKNOWLEDGED_DO. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_ACKNOWLEDGED_DO
        = "DeliveryOrder.SearchCountAcknowledgedDo";
    
    /** The Constant DELIVERY_ORDER_SEARCH_ACKNOWLEDGED_DO. */
    public static final String DELIVERY_ORDER_SEARCH_ACKNOWLEDGED_DO
        = "DeliveryOrder.SearchAcknowledgedDo";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_DO_DETAIL. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_DO_DETAIL
        = "DeliveryOrder.SearchCountDoDetail";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DO_DETAIL. */
    public static final String DELIVERY_ORDER_SEARCH_DO_DETAIL = "DeliveryOrder.SearchDoDetail";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DO_GROUP_ASN. */
    public static final String DELIVERY_ORDER_SEARCH_DO_GROUP_ASN
        = "DeliveryOrder.SearchDoGroupAsn";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COMPLETE_SHIP_DO. */
    public static final String DELIVERY_ORDER_SEARCH_COMPLETE_SHIP_DO
        = "DeliveryOrder.SearchCompleteShipDo";
    
    // [IN012] for DeliveryOrder.SearchCountDoDetailGroupByStatus
    /** The Constant DELIVERY_ORDER_SEARCH_DISTINCT_PN_STATUS. */
    public static final String DELIVERY_ORDER_SEARCH_DISTINCT_PN_STATUS
        = "DeliveryOrder.SearchDistinctPnStatus";
    
    /** The Constant DELIVERY_ORDER_SEARCH_EXIST_DO. */
    public static final String DELIVERY_ORDER_SEARCH_EXIST_DO = "DeliveryOrder.SearchExistDo";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_BACK_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_BACK_ORDER
        = "DeliveryOrder.SearchCountBackOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_BACK_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_BACK_ORDER
        = "DeliveryOrder.SearchBackOrder";
    
    /** The Constant DELIVERY_ORDER_CREATE_DELIVERY_ORDER. */
    public static final String DELIVERY_ORDER_CREATE_DELIVERY_ORDER
        = "DeliveryOrder.CreateDeliveryOrder";
    
    /** The Constant DELIVERY_ORDER_CREATE_KANBAN_TAG. */
    public static final String DELIVERY_ORDER_CREATE_KANBAN_TAG
        = "DeliveryOrder.CreateKanbanTag";
    
    /** The Constant DELIVERY_ORDER_UPDATE_ONE_WAY_KANBAN_TAG_FLAG. */
    public static final String DELIVERY_ORDER_UPDATE_ONE_WAY_KANBAN_TAG_FLAG
        = "DeliveryOrder.updateOneWayKanbanTagFlag";
    
    /** The Constant DELIVERY_ORDER_SEARCH_BATCH_NO. */
    public static final String DELIVERY_ORDER_SEARCH_BATCH_NO
        = "DeliveryOrder.searchBatchNo";
    
    /** The Constant DELIVERY_ORDER_CREATE_TRANSMIT_LOG. */
    public static final String DELIVERY_ORDER_CREATE_TRANSMIT_LOG
        = "DeliveryOrder.createTransmitLog";
    
    /** The Constant DELIVERY_ORDER_SEARCH_EXIST_DELIVERY_ORDER. */
    public static final String  DELIVERY_ORDER_SEARCH_EXIST_DELIVERY_ORDER
        = "DeliveryOrder.SearchExistDeliveryOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_URGENT_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_URGENT_ORDER
        = "DeliveryOrder.SearchUrgentOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DO_CREATED_ASN. */
    public static final String DELIVERY_ORDER_SEARCH_DO_CREATED_ASN
        = "DeliveryOrder.SearchDoCreatedAsn";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_DELIVERY_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_DELIVERY_ORDER
        = "DeliveryOrder.SearchCountDeliveryOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_DELIVERY_ORDER
        = "DeliveryOrder.SearchDeliveryOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_BACK_ORDER_HEADER. */
    public static final String DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_BACK_ORDER_HEADER
        = "DeliveryOrder.SearchDeliveryOrderBackOrderHeader";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_BACK_ORDER_DETAIL. */
    public static final String DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_BACK_ORDER_DETAIL
        = "DeliveryOrder.SearchDeliveryOrderBackOrderDetail";
    
    /** The Constant DELIVERY_ORDER_UPDATE_BACK_ORDER_DETAIL. */
    public static final String DELIVERY_ORDER_UPDATE_BACK_ORDER_DETAIL
        = "DeliveryOrder.UpdateBackOrderDetail";
    
    /** The Constant DELIVERY_ORDER_UPDATE_BACK_ORDER_HEADER. */
    public static final String DELIVERY_ORDER_UPDATE_BACK_ORDER_HEADER
        = "DeliveryOrder.UpdateBackOrderHeader";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_KANBAN_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_KANBAN_ORDER
        = "DeliveryOrder.SearchCountKanbanOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_KANBAN_ORDER. */
    public static final String DELIVERY_ORDER_SEARCH_KANBAN_ORDER
        = "DeliveryOrder.SearchKanbanOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_KANBAN_ORDER_HEADER. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_KANBAN_ORDER_HEADER
        = "DeliveryOrder.SearchCountKanbanOrderHeader";
    
    /** The Constant DELIVERY_ORDER_SEARCH_KANBAN_ORDER_HEADER. */
    public static final String DELIVERY_ORDER_SEARCH_KANBAN_ORDER_HEADER
        = "DeliveryOrder.SearchKanbanOrderHeader";
    
    /** The Constant DELIVERY_ORDER_SEARCH_COUNT_DELIVERY_ORDER_HEADER. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_DELIVERY_ORDER_HEADER
        = "DeliveryOrder.SearchCountDeliveryOrderHeader";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_HEADER. */
    public static final String DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_HEADER
        = "DeliveryOrder.SearchDeliveryOrderHeader";
    
    /** The Constant DELIVERY_ORDER_SEARCH_URGENT_ORDER_FOR_SUPPLIER_USER. */
    public static final String DELIVERY_ORDER_SEARCH_URGENT_ORDER_FOR_SUPPLIER_USER
        = "DeliveryOrder.SearchUrgentOrderForSupplierUser";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_REPORT. */
    public static final String DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_REPORT
        = "DeliveryOrder.SearchDeliveryOrderReport";
    
    /** The Constant DELIVERY_ORDER_SEARCH_KANBAN_DELOVERY_ORDER_REPORT. */
    public static final String DELIVERY_ORDER_SEARCH_KANBAN_DELOVERY_ORDER_REPORT
        = "DeliveryOrder.SearchKanbanDeliveryOrderReport";
    
    /** The Constant DELIVERY_ORDER_SEARCH_PURGING_DO. */
    public static final String DELIVERY_ORDER_SEARCH_PURGING_DO
        = "DeliveryOrder.SearchPurgingDeliveryOrder";
    
    // [IN012] for check delete P/O
    /** The Constant DELIVERY_ORDER_SEARCH_PURGING_DO. */
    public static final String DELIVERY_ORDER_SEARCH_COUNT_NOT_PURGING_DO
        = "DeliveryOrder.SearchCountNotPurgingDeliveryOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_PURGING_URGENT_DO. */
    public static final String DELIVERY_ORDER_SEARCH_PURGING_URGENT_DO
        = "DeliveryOrder.SearchPurgingUrgentDeliveryOrder";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_ACKNOWLEDGEMENT. */
    public static final String DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_ACKNOWLEDGEMENT
        = "DeliveryOrder.SearchDeliveryOrderAcknowledgement";
    
    /** The Constant DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_ACKNOWLEDGEMENT. */
    public static final String UPDATE_DATE_TIME_KANBAN_PDF
        = "DeliveryOrder.updateDateTimeKanbanPdf";

    /** The Constant DELIVERY_ORDER_SEARCH_ONE_WAY_KANBAN_TAG_REPORT. */
    public static final String DELIVERY_ORDER_SEARCH_ONE_WAY_KANBAN_TAG_REPORT
        = "DeliveryOrder.searchOneWayKanbanTagReport";

    /** The Constant DELIVERY_ORDER_SEARCH_NOTIFY_DELIVERY_ORDER_TO_SUPPLIER. */
    public static final String DELIVERY_ORDER_SEARCH_NOTIFY_DELIVERY_ORDER_TO_SUPPLIER
        = "DeliveryOrder.searchNotifyDeliveryOrderToSupplier";

    /** The Constant DELIVERY_ORDER_SEARCH_IS_EXIST_TRANSMIT_LOG. */
    public static final String DELIVERY_ORDER_SEARCH_IS_EXIST_TRANSMIT_LOG
        = "DeliveryOrder.searchIsExistTransmitLog";

    /** The Constant DELIVERY_ORDER_DELETE_SPS_T_DO_KANBAN_SEQ. */
    public static final String DELIVERY_ORDER_DELETE_SPS_T_DO_KANBAN_SEQ
        = "DeliveryOrder.deleteSpsTDoKanbanSeq";

    /** The Constant DELIVERY_ORDER_DELETE_SPS_T_SPS_T_KANBAN_TAG. */
    public static final String DELIVERY_ORDER_DELETE_SPS_T_SPS_T_KANBAN_TAG
        = "DeliveryOrder.deleteSpsTKanbanTag";

    /** The Constant DELIVERY_ORDER_DELETE_SPS_T_DO_DETAIL. */
    public static final String DELIVERY_ORDER_DELETE_SPS_T_DO_DETAIL
        = "DeliveryOrder.deleteSpsTDoDetail";

    /** The Constant DELIVERY_ORDER_DELETE_SPS_T_DO. */
    public static final String DELIVERY_ORDER_DELETE_SPS_T_DO
        = "DeliveryOrder.deleteSpsTDo";

    /** The Constant DELIVERY_ORDER_DELETE_SPS_T_BHT_TRANSMIT_LOG_BY_PURGE. */
    public static final String DELIVERY_ORDER_DELETE_SPS_T_BHT_TRANSMIT_LOG_BY_PURGE
        = "DeliveryOrder.deleteSpsTBhtTransmitLogByPurge";

    /** The Constant DELIVERY_ORDER_DELETE_SPS_T_DO_BY_PURGE. */
    public static final String DELIVERY_ORDER_DELETE_SPS_T_DO_BY_PURGE
        = "DeliveryOrder.deleteSpsTDoByPurge";
    /* Menu Dao ============================================================================= */
    /** The Constant MENU_SEARCH_MENU_BY_USER_PERMISSION. */
    public static final String MENU_SEARCH_MENU_BY_USER_PERMISSION
        = "Menu.SearchMenuByUserPermission";
    
    /* Miscellaneous Dao ===================================================================== */
    /** The Constant MISCELLANEOUS_SEARCH_MISC_VALUE. */
    public static final String MISCELLANEOUS_SEARCH_MISC_VALUE
        = "Miscellaneous.SearchMiscValue";
    
    /** The Constant MISCELLANEOUS_SEARCH_MISC. */
    public static final String MISCELLANEOUS_SEARCH_MISC
        = "Miscellaneous.SearchMisc";
    
    /* Plant Denso Dao ===================================================================== */
    /** The Constant PLANT_DENSO_SEARCH_PLANT_DENSO_BY_ROLE. */
    public static final String PLANT_DENSO_SEARCH_PLANT_DENSO_BY_ROLE
        = "PlantDenso.SearchPlantDensoByRole";
    
    /** The Constant PLANT_DENSO_SEARCH_PLANT_DENSO_BY_RELATION. */
    public static final String PLANT_DENSO_SEARCH_PLANT_DENSO_BY_RELATION
        = "PlantDenso.SearchPlantDensoByRelation";
    
    /** The Constant PLANT_DENSO_SEARCH_PLANT_DENSO_BY_ROLE. */
    public static final String PLANT_DENSO_SEARCH_PLANT_DENSO_INFORMATION_BY_ROLE
        = "PlantDenso.SearchPlantDensoInformationByRole";
    
    /** The Constant PLANT_DENSO_SEARCH_PLANT_DENSO_BY_RELATION. */
    public static final String PLANT_DENSO_SEARCH_PLANT_DENSO_INFORMATION_BY_RELATION
        = "PlantDenso.SearchPlantDensoInformationByRelation";
    
    /** The Constant PLANT_DENSO_SEARCH_EXIST_PLANT_DENSO_BY_RELATION. */
    public static final String PLANT_DENSO_SEARCH_EXIST_PLANT_DENSO_BY_RELATION
        = "PlantDenso.SearchExistPlantDensoByRelation";
    
    /** The Constant PLANT_DENSO_SEARCH_EXIST_PLANT_DENSO_BY_ROLE. */
    public static final String PLANT_DENSO_SEARCH_EXIST_PLANT_DENSO_BY_ROLE
        = "PlantDenso.SearchExistPlantDensoByRole";
    
    /* Plant Supplier Dao ===================================================================== */
    /** The Constant PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_ROLE. */
    public static final String PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_ROLE
        = "PlantSupplier.SearchPlantSupplierByRole";
    
    /** The Constant PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_RELATION. */
    public static final String PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_RELATION
        = "PlantSupplier.SearchPlantSupplierByRelation";
    
    /** The Constant PLANT_DENSO_SEARCH_PLANT_DENSO_BY_ROLE. */
    public static final String PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_INFORMATION_BY_ROLE
        = "PlantSupplier.SearchPlantSupplierInformationByRole";
    
    /** The Constant PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_RELATION. */
    public static final String PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_INFORMATION_BY_RELATION
        = "PlantSupplier.SearchPlantSupplierInformationByRelation";
    
    /** The Constant PLANT_SUPPLIER_SEARCH_EXIST_PLANT_SUPPLIER_BY_RELATION. */
    public static final String PLANT_SUPPLIER_SEARCH_EXIST_PLANT_SUPPLIER_BY_RELATION
        = "PlantSupplier.SearchExistPlantSupplierByRelation";
    
    /** The Constant PLANT_SUPPLIER_SEARCH_EXIST_PLANT_SUPPLIER_BY_ROLE. */
    public static final String PLANT_SUPPLIER_SEARCH_EXIST_PLANT_SUPPLIER_BY_ROLE
        = "PlantSupplier.SearchExistPlantSupplierByRole";
    
    /* Purchase Order Dao ===================================================================== */
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FIRM_WAIT_TO_ACK_FOR_S_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FIRM_WAIT_TO_ACK_FOR_S_USER
        = "PurchaseOrder.SearchPoFirmWaitToAckForSUser";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FORCAST_WAIT_TO_ACK_FOR_S_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FORCAST_WAIT_TO_ACK_FOR_S_USER
        = "PurchaseOrder.SearchPoForcastWaitToAckForSUser";
    //[IN072]
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FORCAST_ACCEPT_FOR_S_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FORCAST_ACCEPT_FOR_S_USER
        = "PurchaseOrder.SearchPoForcastDensoAcceptForSUser";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FORCAST_REJECT_FOR_S_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FORCAST_REJECT_FOR_S_USER
        = "PurchaseOrder.SearchPoForcastDensoRejectForSUser";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FORCAST_PENDING_WAIT_D_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FORCAST_PENDING_WAIT_D_USER
        = "PurchaseOrder.SearchPoForcastDensoPendingForDUser";
    
  // End [IN072]
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FIRM_WAIT_TO_ACK_FOR_D_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FIRM_WAIT_TO_ACK_FOR_D_USER
        = "PurchaseOrder.SearchPoFirmWaitToAckForDUser";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PO_FORCAST_WAIT_TO_ACK_FOR_D_USER. */
    public static final String PURCHASE_ORDER_SEARCH_PO_FORCAST_WAIT_TO_ACK_FOR_D_USER
        = "PurchaseOrder.SearchPoForcastWaitToAckForDUser";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_REPORT. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_REPORT
        = "PurchaseOrder.SearchPurchaseOrderReport";
    
    /** The Constant PURCHASE_ORDER_SEARCH_CHANGE_MATERIAL_RELEASE_REPORT. */
    public static final String PURCHASE_ORDER_SEARCH_CHANGE_MATERIAL_RELEASE_REPORT
        = "PurchaseOrder.SearchChangeMaterialReleaseReport";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER
        = "PurchaseOrder.SearchPurchaseOrder";
    
    /** The Constant PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER
        = "PurchaseOrder.SearchCountPurchaseOrder";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_DETAIL. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_DETAIL
        = "PurchaseOrder.SearchPurchaseOrderDetail";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_NOTIFICATION. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_NOTIFICATION
        = "PurchaseOrder.SearchPurchaseOrderNotification";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_DUE. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_DUE
        = "PurchaseOrder.SearchPurchaseOrderDue";

    /** The Constant PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_DETAIL. */
    public static final String PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_DETAIL
        = "PurchaseOrder.SearchCountPurchaseOrderDetail";

    /** The Constant PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_DETAIL_PENDING. */
    public static final String PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_DETAIL_PENDING
        = "PurchaseOrder.SearchCountPurchaseOrderDetailPending";
    
    /** The Constant PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_HEADER. */
    public static final String PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_HEADER
        = "PurchaseOrder.SearchCountPurchaseOrderHeader";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_HEADER. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_HEADER
        = "PurchaseOrder.SearchPurchaseOrderHeader";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURGING_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_SEARCH_PURGING_PURCHASE_ORDER
        = "PurchaseOrder.SearchPurgingPurchaseOrder";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_BY_DO. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_BY_DO
        = "PurchaseOrder.SearchPurchaseOrderByDo";
    
    /** The Constant PURCHASE_ORDER_CREATE_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_CREATE_PURCHASE_ORDER
        = "PurchaseOrder.CreatePurchaseOrder";
    
    /** The Constant PURCHASE_ORDER_SEARCH_EXIST_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_SEARCH_EXIST_PURCHASE_ORDER
        = "PurchaseOrder.SearchExistPurchaseOrder";

    /** The Constant PURCHASE_ORDER_SEARCH_CHANGE_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_SEARCH_CHANGE_PURCHASE_ORDER
        = "PurchaseOrder.SearchChangePurchaseOrder";
    
    /** The Constant PURCHASE_ORDER_SEARCH_CHANGE_COUNT_PURCHASE_ORDER. */
    public static final String PURCHASE_ORDER_SEARCH_CHANGE_COUNT_PURCHASE_ORDER
        = "PurchaseOrder.SearchCountChangePurchaseOrder";
    
    /** The Constant PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_KANBAN_REPORT. */
    public static final String PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_KANBAN_REPORT
        = "PurchaseOrder.SearchPurchaseOrderKanbanReport";
    
    // [IN055] Validate Supplier Information before delete
    /** The Constant PURCHASE_ORDER_SEARCH_COUNT_EFFECT_PARTS. */
    public static final String PURCHASE_ORDER_SEARCH_COUNT_EFFECT_PARTS
        = "PurchaseOrder.SearchCountEffectParts";
    
    // [IN054] Update all Supplier Promised Due that not reply to Reject
    /** The Constant PURCHASE_ORDER_SEARCH_COUNT_EFFECT_PARTS. */
    public static final String PURCHASE_ORDER_UPDATE_REJECT_NOT_REPLY_DUE
        = "PurchaseOrder.UpdateRejectNotReplyDue";
    
    /** The Constant PURCHASE_ORDER_DELETE_SPS_T_PO_COVER_PAGE_DETAIL. */
    public static final String PURCHASE_ORDER_DELETE_SPS_T_PO_COVER_PAGE_DETAIL
        = "PurchaseOrder.DeleteSpsTPoCoverPageDetail";
    
    /** The Constant PURCHASE_ORDER_DELETE_SPS_T_PO_COVER_PAGE. */
    public static final String PURCHASE_ORDER_DELETE_SPS_T_PO_COVER_PAGE
        = "PurchaseOrder.DeleteSpsTPoCoverPage";
    
    /** The Constant PURCHASE_ORDER_DELETE_SPS_T_PO_DUE. */
    public static final String PURCHASE_ORDER_DELETE_SPS_T_PO_DUE
        = "PurchaseOrder.DeleteSpsTPoDue";
    
    /** The Constant PURCHASE_ORDER_DELETE_SPS_T_PO_DETAIL. */
    public static final String PURCHASE_ORDER_DELETE_SPS_T_PO_DETAIL
        = "PurchaseOrder.DeleteSpsTPoDetail";
    
    /** The Constant PURCHASE_ORDER_DELETE_SPS_T_PO. */
    public static final String PURCHASE_ORDER_DELETE_SPS_T_PO
        = "PurchaseOrder.DeleteSpsTPo";
    
    /** The Constant PURCHASE_ORDER_DELETE_SPS_T_PO_BY_PURGE. */
    public static final String PURCHASE_ORDER_DELETE_SPS_T_PO_BY_PURGE
        = "PurchaseOrder.DeleteSpsTPoByPurge";
    
    /* Purchase Order Cover Page Dao ========================================================== */
    /** The Constant PURCHASE_ORDER_COVER_PAGE_SEARCH_PURCHASE_ORDER_REPORT. */
    public static final String PURCHASE_ORDER_COVER_PAGE_SEARCH_PURCHASE_ORDER_REPORT
        = "PurchaseOrderCoverPage.SearchPurchaseOrderReport";
    
    /* Record Limit Dao ======================================================================= */
    /** The Constant RECORD_LIMIT_SEARCH_RECORD_LIMIT. */
    public static final String RECORD_LIMIT_SEARCH_RECORD_LIMIT
        = "RecordLimit.SearchRecordLimit";
    
    /** The Constant RECORD_LIMIT_SEARCH_RECORD_LIMIT_PER_PAGE. */
    public static final String RECORD_LIMIT_SEARCH_RECORD_LIMIT_PER_PAGE
        = "RecordLimit.SearchRecordLimitPerPage";
    
    /* Screen Dao ============================================================================= */
    /** The Constant SCREEN_SEARCH_SCREEN_BY_USER_PERMISSION. */
    public static final String SCREEN_SEARCH_SCREEN_BY_USER_PERMISSION
        = "Screen.SearchScreenByUserPermission";
    
    /* Tmp Upload Invoice Dao ================================================================== */
    /** The Constant TMP_UPLOAD_INVOICE_SEARCH_TMP_UPLOAD_INVOICE. */
    public static final String TMP_UPLOAD_INVOICE_SEARCH_TMP_UPLOAD_INVOICE
        = "TmpUploadInvoice.SearchTmpUploadInvoice";
    
    /* User Dao =============================================================================== */
    /** The Constant USER_DEPARTMENT_NAME. */
    public static final String USER_SEARCH_DEPARTMENT_NAME = "User.SearchDepartmentName";
    
    /** The Constant USER_SEARCH_EMAIL_USER_DENSO. */
    public static final String USER_SEARCH_EMAIL_USER_DENSO = "User.SearchEmailUserDenso";
    
    /* User Denso Dao =========================================================================== */
    /** The Constant USER_DENSO_SEARCH_COUNT_USER_DENSO. */
    public static final String USER_DENSO_SEARCH_COUNT_USER_DENSO = "UserDenso.SearchCountUserDenso";
    
    /** The Constant USER_DENSO_SEARCH_USER_DENSO. */
    public static final String USER_DENSO_SEARCH_USER_DENSO = "UserDenso.SearchUserDenso";
    
    /** The Constant USER_DENSO_SEARCH_COUNT_USER_DENSO_INCLUDE_ROLE. */
    public static final String USER_DENSO_SEARCH_COUNT_USER_DENSO_INCLUDE_ROLE 
        = "UserDenso.SearchCountUserDensoIncludeRole";
    
    /** The Constant USER_DENSO_SEARCH_USER_DENSO_INCLUDE_ROLE. */
    public static final String USER_DENSO_SEARCH_USER_DENSO_INCLUDE_ROLE 
        = "UserDenso.SearchUserDensoIncludeRole";
    
    /* User Role Dao =========================================================================== */
    /** The Constant USER_DENSO_SEARCH_USER_DENSO_INCLUDE_ROLE. */
    public static final String USER_ROLE_SEARCH_USER_ROLE_BY_DSCID
        = "UserRole.SearchUserRoleByDscId";
    
    /** The Constant USER_ROLE_DELETE_USER_ROLE_BY_DSCID. */
    public static final String USER_ROLE_DELETE_USER_ROLE_BY_DSCID
        = "UserRole.DeleteUserRoleByDscId";

    /** The Constant USER_ROLE_SEARCH_USER_ROLE_TYPE. */
    public static final String USER_ROLE_SEARCH_USER_ROLE_TYPE
        = "UserRole.SearchUserRoleType";
    
    /* User Supplier Dao =========================================================================== */
    /** The Constant USER_SUPPLIER_SEARCH_COUNT_USER_SUPPLIER. */
    public static final String USER_SUPPLIER_SEARCH_COUNT_USER_SUPPLIER
        = "UserSupplier.SearchCountUserSupplier";
    
    /** The Constant USER_SUPPLIER_SEARCH_USER_SUPPLIER. */
    public static final String USER_SUPPLIER_SEARCH_USER_SUPPLIER
        = "UserSupplier.SearchUserSupplier";
    
    /** The Constant USER_SUPPLIER_SEARCH_USER_EMAIL_BY_SUPPLIER_CODE. */
    public static final String USER_SUPPLIER_SEARCH_USER_EMAIL_BY_SUPPLIER_CODE
        = "UserSupplier.SearchUserEmailBySupplierCode";
    
    /** The Constant USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_NOT_FOUND_FLAG. */
    public static final String USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_NOT_FOUND_FLAG
        = "UserSupplier.SearchUserSupplierEmailByNotFoundFlag";
    
    /** The Constant USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_URGENT_FLAG. */
    public static final String USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_URGENT_FLAG
        = "UserSupplier.SearchUserSupplierEmailByUrgentFlag";
    
    /** The Constant USER_SUPPLIER_SEARCH_USER_SUPPLIER_INCLUDE_ROLE. */
    public static final String USER_SUPPLIER_SEARCH_USER_SUPPLIER_INCLUDE_ROLE
        = "UserSupplier.SearchUserSupplierIncludeRole";
    
    /** The Constant USER_SUPPLIER_SEARCH_EMAIL_USER_SUPPLIER. */
    public static final String USER_SUPPLIER_SEARCH_EMAIL_USER_SUPPLIER
        = "UserSupplier.SearchEmailUserSupplier";
    
    /** The Constant USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_URGENT_FLAG_FOR_BACKORDER. */
    public static final String USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_URGENT_FLAG_FOR_BACKORDER
        = "UserSupplier.SearchUserSupplierEmailByUrgentFlagForBackorder";
    
    /* Cn Dao ================================================================================= */
    /** The Constant CN_SEARCH_CN. */
    public static final String CN_SEARCH_CN 
        = "Cn.SearchCn";
    
    /** The Constant CN_SEARCH_COUNT_CN. */
    public static final String CN_SEARCH_COUNT_CN 
        = "Cn.SearchCountCn";
    
    /** The Constant CN_CREATE_CN. */
    public static final String CN_CREATE_CN 
        = "Cn.CreateCn";
    
    /** The Constant CN_SEARCH_CN_COVER_PAGE. */
    public static final String CN_SEARCH_CN_COVER_PAGE 
        = "Cn.SearchCnCoverPage";
    
    /** The Constant CN_SEARCH_CN_FOR_TRANSFER_TO_JDE. */
    public static final String CN_SEARCH_CN_FOR_TRANSFER_TO_JDE 
        = "Cn.SearchCnForTransferToJde";
   
    /* Tmp Supplier Info Dao ================================================================== */
    /** The Constant TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_ONE_RECORD. */
    public static final String TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_ONE_RECORD 
        = "TmpSupplierInfo.SearchTmpUploadSupplierInfoOneRecord";
    
    /** The Constant TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_COMPANY_NAME. */
    public static final String TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_COMPANY_NAME
        = "TmpSupplierInfo.SearchTmpUploadSupplierInfoCompanyName";
    
    /** The Constant TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_PLANT. */
    public static final String TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_PLANT 
        = "TmpSupplierInfo.SearchTmpUploadSupplierInfoPlant";
    
    /* Tmp Upload Error Dao =================================================================== */
    /** The Constant TMP_UPLOAD_ERROR_SEARCH_COUNT_WARNING. */
    public static final String TMP_UPLOAD_ERROR_SEARCH_COUNT_WARNING 
        = "TmpUploadError.SearchCountWarning";
    
    /** The Constant TMP_UPLOAD_ERROR_SEARCH_COUNT_IN_CORRECT. */
    public static final String TMP_UPLOAD_ERROR_SEARCH_COUNT_IN_CORRECT 
        = "TmpUploadError.SearchCountIncorrect";
    
    /** The Constant TMP_UPLOAD_ERROR_SEARCH_GROUP_OF_VALIDATION_ERROR. */
    public static final String TMP_UPLOAD_ERROR_SEARCH_GROUP_OF_VALIDATION_ERROR 
        = "TmpUploadError.SearchGroupOfValidationError";
    
    /** The Constant TMP_UPLOAD_ERROR_SEARCH_COUNT_GROUP_OF_VALIDATION_ERROR. */
    public static final String TMP_UPLOAD_ERROR_SEARCH_COUNT_GROUP_OF_VALIDATION_ERROR
        = "TmpUploadError.SearchCountGroupOfValidationError";
    
    /* Tmp Upload User Denso Dao ============================================================== */
    /** The Constant TMP_UPLOAD_USER_DENSO_SERACH_TMP_UPLOAD_DENSO_ONE_RECORD. */
    public static final String TMP_UPLOAD_USER_DENSO_SERACH_TMP_UPLOAD_DENSO_ONE_RECORD 
        = "TmpUploadUserDenso.SearchTmpUploadDensoOneRecord";
    
    /* Tmp Upload User Supplier Dao =========================================================== */
    /** The Constant TMP_UPLOAD_USER_SUPPLIER_SERACH_TMP_UPLOAD_SUPPLIER_ONE_RECORD. */
    public static final String TMP_UPLOAD_USER_SUPPLIER_SERACH_TMP_UPLOAD_SUPPLIER_ONE_RECORD 
        = "TmpUploadUserSupplier.SearchTmpUploadSupplierOneRecord";
    
    /* Upload Asn Dao =========================================================== */
    /** The Constant UPLOAD_ASN_SEARCH_TMP_UPLOAD_ASN. */
    public static final String UPLOAD_ASN_SEARCH_TMP_UPLOAD_ASN
        = "UploadAsn.SearchTmpUploadAsn";
    
    /* Invoice Dao =========================================================== */
    /** The Constant INVOICE_SEARCH_INVOICE_INFORMATION. */
    public static final String INVOICE_SEARCH_INVOICE_INFORMATION
        = "Invoice.SearchInvoiceInformation";
    
    /** The Constant INVOICE_SEARCH_COUNT_INVOICE_INFORMATION. */
    public static final String INVOICE_SEARCH_COUNT_INVOICE_INFORMATION
        = "Invoice.SearchCountInvoiceInformation";
    
    /** The Constant INVOICE_CREATE_INVOICE. */
    public static final String INVOICE_CREATE_INVOICE = "Invoice.CreateInvoice";
    
    /** The Constant INVOICE_SEARCH_INVOICE_COVER_PAGE. */
    public static final String INVOICE_SEARCH_INVOICE_COVER_PAGE = "Invoice.SearchInvoiceCoverPage";
    
    /** The Constant INVOICE_SEARCH_INVOICE_BY_ASN. */
    public static final String INVOICE_SEARCH_INVOICE_BY_ASN = "Invoice.SearchInvoiceByAsn";
    
    /** The Constant INVOICE_SEARCH_PURGING_INVOICE. */
    public static final String INVOICE_SEARCH_PURGING_INVOICE = "Invoice.SearchPurgingInvoice";
    
    /** The Constant INVOICE_SEARCH_PURGING_INVOICE. */
    public static final String INVOICE_SEARCH_INVOICE_FOR_TRANSFER_TO_JDE 
        = "Invoice.SearchInvoiceForTransferToJde";
    
    /** The Constant INVOICE_DELETE_SPS_T_CN_DETAIL. */
    public static final String INVOICE_DELETE_SPS_T_CN_DETAIL 
        = "Invoice.DeleteSpsTCnDetail";
    
    /** The Constant INVOICE_DELETE_SPS_T_CN. */
    public static final String INVOICE_DELETE_SPS_T_CN 
        = "Invoice.DeleteSpsTCn";
    
    /** The Constant INVOICE_DELETE_SPS_T_INVOICE_DETAIL. */
    public static final String INVOICE_DELETE_SPS_T_INVOICE_DETAIL 
        = "Invoice.DeleteSpsTInvoiceDetail";
    
    /** The Constant INVOICE_SPS_T_INV_COVERPAGE_RUNNO. */
    public static final String INVOICE_SPS_T_INV_COVERPAGE_RUNNO 
        = "Invoice.DeleteSpsTInvCoverpageRunno";
    
    /** The Constant INVOICE_DELETE_SPS_T_INVOICE. */
    public static final String INVOICE_DELETE_SPS_T_INVOICE 
        = "Invoice.DeleteSpsTInvoice";
    
    /** The Constant INVOICE_DELETE_SPS_T_INVOICE_BY_PURGE. */
    public static final String INVOICE_DELETE_SPS_T_INVOICE_BY_PURGE 
        = "Invoice.DeleteSpsTInvoiceByPurge";
    
    /* File Upload Dao =========================================================== */
    /** The Constant FILE_UPLOAD_SEARCH_BY_CONDITION_FOR_PAGING. */
    public static final String FILE_UPLOAD_SEARCH_BY_CONDITION_FOR_PAGING
        = "FileUpload.SearchByConditionForPaging";
    
    /** The Constant FILE_UPLOAD_SEARCH_COUNT. */
    public static final String FILE_UPLOAD_SEARCH_COUNT
        = "FileUpload.SearchCount";
    
    /* Cigma P/O Error ============================================================ */

    /** The Constant CIGMA_PO_ERROR_SEARCH_TRANSFER_PO_ERROR. */
    public static final String CIGMA_PO_ERROR_SEARCH_TRANSFER_PO_ERROR 
        = "CigmaPoError.SearchTransferPoError";

    /** The Constant CIGMA_CHG_PO_ERROR_SEARCH_TRANSFER_CHG_PO_ERROR. */
    public static final String CIGMA_CHG_PO_ERROR_SEARCH_TRANSFER_CHG_PO_ERROR 
        = "CigmaChgPoError.SearchTransferChgPoError";

    // [IN068] add method for search minimum issue date
    /** The Constant CIGMA_PO_ERROR_SEARCH_MIN_ISSUE_DATE. */
    public static final String CIGMA_PO_ERROR_SEARCH_MIN_ISSUE_DATE
        = "CigmaPoError.SearchMinIssueDate";

    /* Cigma D/O Error ============================================================ */

    /** The Constant CIGMA_DO_ERROR_SEARCH_TRANSFER_DO_ERROR. */
    public static final String CIGMA_DO_ERROR_SEARCH_TRANSFER_DO_ERROR 
        = "CigmaDoError.SearchTransferDoError";

    /** The Constant CIGMA_CHG_DO_ERROR_SEARCH_TRANSFER_CHG_DO_ERROR. */
    public static final String CIGMA_CHG_DO_ERROR_SEARCH_TRANSFER_CHG_DO_ERROR 
        = "CigmaChgDoError.SearchTransferChgDoError";
    
    /** The Constant for SPS Phase II */
    /** The Constant for UPGRADE_BHT_SEARCH_BHT_UPGRADE */
    public static final String UPGRADE_BHT_SEARCH_BHT_UPGRADE 
        = "UpgradeBht.SearchBhtUpgrade";
    
    /**
     * Instantiates a new SQL Map constants.
     */
    public SqlMapConstants(){
        super();
    }
    
    /** The Constant DELIVERY_ORDER_SEARCH_MAIL_BACKORDER. */
    public static final String DELIVERY_ORDER_SEARCH_MAIL_BACKORDER = "DeliveryOrder.SearchMailBackOrder";
}