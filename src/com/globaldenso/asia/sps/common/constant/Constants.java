/*
 * ModifyDate Developmentcompany    Describe 
 * 2014/03/03 CSI Akat              Create
 * 2015/08/24 CSI Akat              [IN012]
 * 2016/01/06 CSI Akat              [IN039]
 * 2016/02/16 CSI Akat              [IN054]
 * 2016/03/01 CSI Akat              [IN059]
 * 2016/03/16 CSI Akat              [IN068]
 * 2016/04/20 CSI Akat              [IN070]
 * 2018/01/10 Netband U. Rungsiwut  SPS Phase II
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.common.constant;

import java.math.BigDecimal;

/**
 * The Class Constants.
 * @author CSI
 */
public class Constants {  

    /** The constant DEFAULT_RUNNO_DATE. */
    public static final String DEFAULT_RUNNO_DATE = "20140101";
    
    /** The constant ZERO_MILLISECOND_TIME. */
    public static final String ZERO_MILLISECOND_TIME = "00:00:00.000";
    
    /** The Constant EMPTY_STRING. */
    public static final String EMPTY_STRING = "";
    
    /** The Constant NULL_STRING. */
    public static final String NULL_STRING = "null";

    /** The Constant COLOR_WHITE. */
    public static final String COLOR_WHITE = "#FFFFFF";

    /** The Constant COLOR_YELLOW. */
    public static final String COLOR_YELLOW = "#FFFFE0";
    
    // Start : [IN054] Add new color for DENSO Reply Email;
    /** */
    /** The Constant COLOR_BLACK. */
    public static final String COLOR_BLACK = "#000000";

    /** The Constant COLOR_RED. */
    public static final String COLOR_RED = "#FF0000";
    // End : [IN054] Add new color for DENSO Reply Email;
    
    /** The default number format for SPS system. */
    public static final String DEFAULT_NUMBER_FORMAT = "#,##0.00";
    
    /** The constant DecimalFormat for SPS system. */ 
    public static final String DECIMAL_FORMAT = "#0.00";

    /** The no decimal point number format for SPS system. */
    public static final String NO_DECIMAL_NUMBER_FORMAT = "#,##0";

    /** The six digit for decimal point number format for SPS system. */
    public static final String SIX_DECIMAL_NUMBER_FORMAT = "#,###.000000";

    /** The format number for transfer to AS400 with two decimal point. */
    public static final String AS400_TWO_DECIMAL_NUMBER_FORMAT = "#.00";

    /** The format number for transfer to AS400 with four decimal point. */
    public static final String AS400_FOUR_DECIMAL_NUMBER_FORMAT = "#.0000";
    
    /** The constants for read input stream buffer size (4096). */
    public static final int READ_BUFFER_SIZE = 4096;
    
    /** The constant for QR Code size. */
    public static final int QR_CODE_SIZE = 50;
    
    /** The constant default locale for batch. */
    public static final String BATCH_DEFAULT_LOCALE = "en-US";
    
    /** The constant running no format for ASN No. */
    public static final String RUNNING_NO_FORMAT = "%02d";
    
    /** The constant format four digit. */
    public static final String FORMAT_FOUR_DIGIT = "%04d";
    
    /** The constant running no format for Company Denso Code. */
    public static final String DCD_FORMAT = "%-5s";
    
    /** The constant running no format for Company Supplier Code. */
    public static final String SCD_FORMAT = "%-6s";

    /** Real URL of SPS web. */
    public static final String SPS_URL = "https://sps.asia.gscm.globaldenso.com/siteminderagent/forms_ja-JP/login-asps.fcc?TYPE=33554433&REALMOID=06-000e6dc0-24fb-143f-aaa4-16e20a05b004&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=-SM-RgGt4Sdz5Dq%2bXBqjwzSwxG2nX2hItsTUI8KwanRq3Du0drZl2UFRsHFe4ue%2f3v%2f6&TARGET=-SM-https%3a%2f%2fsps%2easia%2egscm%2eglobaldenso%2ecom%2f";
//    public static final String SPS_URL = "http://10.74.201.182:8091/SPS/";

    /** Real Logout URL of SPS web. */
    public static final String SPS_LOGOUT_URL = "https://sps.asia.gscm.globaldenso.com/siteminderagent/forms_ja-JP/logout-asps.html";
//    public static final String SPS_LOGOUT_URL = "http://10.74.201.182:8091/SPS/";
    
    // [IN068] to check current local time
    /** Difference time between AP Server local time and GMT time (Singapore GMT+8). */
    public static final int DIFFERENCE_AP_SERVER_LOCAL_TIME_WITH_GMT = -8;
    
    /* Number =================================================================================== */
    
    /** The Constant MINUS_THREE. */
    public static final int MINUS_THREE = -3;
    
    /** The Constant MINUS_TWO. */
    public static final int MINUS_TWO = -2;

    /** The Constant MINUS_ONE. */
    public static final int MINUS_ONE = -1;
    
    /** The constant MINUS_SEVEN. */
    public static final int MINUS_SEVEN = -7;

    // [IN059] to calculate VAT Rate
    // Akat K. : Change Invoice input allow input different from calculate from 0.01 to 0.0
    /** The constant ZERO_POINT_ONE. */
    public static final BigDecimal ZERO_POINT_ZERO_ONE = new BigDecimal(0.01);
    
    /** The constant INVOICE_ALLOW_DIFFERENT. */
    public static final BigDecimal INVOICE_ALLOW_DIFFERENT = new BigDecimal(0.0);

    /** The constant BIG_DECIMAL_ZERO. */
    public static final BigDecimal BIG_DECIMAL_ZERO = new BigDecimal(0);
    
    /** The constant BIG_DECIMAL_ONE. */
    public static final BigDecimal BIG_DECIMAL_ONE = new BigDecimal(1);
    
    /** The constant ZERO. */
    public static final int ZERO = 0;
    
    /** The constant ONE. */
    public static final int ONE = 1;
    
    /** The constant TWO. */
    public static final int TWO = 2;

    /** The constant THREE. */
    public static final int THREE = 3;

    /** The constant FOUR. */
    public static final int FOUR = 4;

    /** The constant FIVE. */
    public static final int FIVE = 5;

    /** The constant SIX. */
    public static final int SIX = 6;

    /** The constant SEVEN. */
    public static final int SEVEN = 7;

    /** The constant EIGHT. */
    public static final int EIGHT = 8;

    /** The constant NINE. */
    public static final int NINE = 9;
    
    /** The constant TEN. */
    public static final int TEN = 10;
    
    /** The constant ELEVEN. */
    public static final int ELEVEN = 11;
    
    /** The constant TWELVE. */
    public static final int TWELVE = 12;
    
    /** The constant THIRTEEN. */
    public static final int THIRTEEN = 13;
    
    /** The constant FOURTEEN. */
    public static final int FOURTEEN = 14;
    
    /** The constant FIFTEEN. */
    public static final int FIFTEEN = 15;
    
    /** The constant SIXTEEN. */
    public static final int SIXTEEN = 16;
    
    /** The constant SEVENTEEN. */
    public static final int SEVENTEEN = 17;
    
    /** The constant EIGHTEEN. */
    public static final int EIGHTEEN = 18;
    
    /** The constant NINETEEN. */
    public static final int NINETEEN = 19;
    
    /** The constant TWENTY. */
    public static final int TWENTY = 20;
    
    /** The constant TWENTY_ONE. */
    public static final int TWENTY_ONE = 21;
    
    /** The constant TWENTY_TWO. */
    public static final int TWENTY_TWO = 22;
    
    /** The constant TWENTY_THREE. */
    public static final int TWENTY_THREE = 23;
    
    /** The constant TWENTY_FOUR. */
    public static final int TWENTY_FOUR = 24;
    
    /** The constant TWENTY_FIVE. */
    public static final int TWENTY_FIVE = 25;
    
    /** The constant TWENTY_SIX. */
    public static final int TWENTY_SIX = 26;
    
    /** The constant TWENTY_SEVEN. */
    public static final int TWENTY_SEVEN = 27;
    
    /** The constant TWENTY_EIGHT. */
    public static final int TWENTY_EIGHT = 28;
    
    /** The constant TWENTY_NINE. */
    public static final int TWENTY_NINE = 29;
    
    /** The constant THIRTY. */
    public static final int THIRTY = 30;
    
    /** The constant THIRTY_ONE. */
    public static final int THIRTY_ONE = 31;
    
    /** The constant THIRTY_TWO. */
    public static final int THIRTY_TWO = 32;
    
    /** The constant THIRTY_THREE. */
    public static final int THIRTY_THREE = 33;
    
    /** The constant THIRTY_FOUR. */
    public static final int THIRTY_FOUR = 34;
    
    /** The constant THIRTY_FIVE. */
    public static final int THIRTY_FIVE = 35;
    
    /** The constant THIRTY_SIX. */
    public static final int THIRTY_SIX = 36;
    
    /** The constant THIRTY_SEVEN. */
    public static final int THIRTY_SEVEN = 37;
    
    /** The constant THIRTY_EIGHT. */
    public static final int THIRTY_EIGHT = 38;
    
    /** The constant THIRTY_NINE. */
    public static final int THIRTY_NINE = 39;
    
    /** The constant FORTY. */
    public static final int FORTY = 40;
    
    /** The constant FORTY_ONE. */
    public static final int FORTY_ONE = 41;
    
    /** The constant FORTY_TWO. */
    public static final int FORTY_TWO = 42;
    
    /** The constant FORTY_THREE. */
    public static final int FORTY_THREE = 43;
    
    /** The constant FORTY_FOUR. */
    public static final int FORTY_FOUR = 44;
    
    /** The constant FORTY_FIVE. */
    public static final int FORTY_FIVE = 45;
    
    /** The constant FIFTY_FOUR. */
    public static final int FIFTY_FOUR = 54;
    
    // [IN068] to calculate current local time for DENSO company
    /** The constants SIXTY. */
    public static final int SIXTY = 60;
    
    /** The constants NINETY. */
    public static final int NINETY = 90;
    
    /** The constant ONE_HUNDRED. */
    public static final int ONE_HUNDRED = 100;
    
    /** The constant ONE_THOUSAND. */
    public static final int ONE_THOUSAND = 1000;
    
    /** The constant ONE_THOUSAND_AND_NINE_HUNDRED. */
    public static final int ONE_THOUSAND_AND_NINE_HUNDRED = 1900;
    
    /** The constant TWO_THOUSANDS. */
    public static final int TWO_THOUSANDS = 2000;
    
    /** The constant TEN_THOUSANDS. */
    public static final int TEN_THOUSANDS = 10000;
    
    /** The constant ONE_HUNDRED. */
    public static final int ONE_HUNDRED_THOUSAND = 100000;
    
    /** The Constant STR_MINUS_TWO. */
    public static final String STR_MINUS_TWO = "-2";
    
    /** The constant STR_ZERO. */
    public static final String STR_ZERO = "0";
    
    // [IN059] for validate input zero
    /** The constant STR_ZERO_TWO_DIGIT. */
    public static final String STR_ZERO_TWO_DIGIT = "0.00";
    
    /** The constant STR_ONE. */
    public static final String STR_ONE = "1";
    
    /** The constant STR_TWO. */
    public static final String STR_TWO = "2";
    
    /** The constant STR_THREE. */
    public static final String STR_THREE = "3";
    
    /** The constant STR_FOUR. */
    public static final String STR_FOUR = "4";
    
    /** The constant STR_FIVE. */
    public static final String STR_FIVE = "5";
    
    /** The constant STR_SIX. */
    public static final String STR_SIX = "6";
    
    /** The constant STR_SEVEN. */
    public static final String STR_SEVEN = "7";
    
    /** The constant STR_EIGHT. */
    public static final String STR_EIGHT = "8";
    
    /** The constant STR_NINE. */
    public static final String STR_NINE = "9";
    
    /** The constant STR_TEN. */
    public static final String STR_TEN = "10";
    
    /** The constant STR_ELEVEN. */
    public static final String STR_ELEVEN = "11";
    
    /** The constant STR_TWELVE. */
    public static final String STR_TWELVE = "12";
    
    /** The constant STR_THIRTEEN. */
    public static final String STR_THIRTEEN = "13";
    
    /** The constant STR_SIXTEEN. */
    public static final String STR_SIXTEEN = "16";
    
    /** The constant STR_NINETY_NINE. */
    public static final String STR_NINETY_NINE = "99";
    
    // [IN068] To check input date is first date
    /** Constants first date of month. */
    public static final String STR_FIRST_DATE = "01";

    /** The constant STR_S. */
    public static final String STR_S = "S";

    /** The constant STR_KB. */
    public static final String STR_KB = "KB";
    
    /* END Number =============================================================================== */
    
    /** The Constant RESULT_MESSAGE. */
    public static final String RESULT_MESSAGE = "resultMessage";
    
    /** The Constant ERROR_MESSAGE. */
    public static final String ERROR_MESSAGE = "ErrorMessages";
    
    /** The message type success. */
    public static final String MESSAGE_SUCCESS = "success";
    
    /** The message type failure. */
    public static final String MESSAGE_FAILURE = "failure";
    
    /** The message type warning. */
    public static final String MESSAGE_WARNING = "warning";
    
    /** The constant ERROR_CD_ORA_00001 error message in Oracle. */
    public static final String ERROR_CD_ORA_00001 = "ORA-00001";
    
    /**
    * SESSION_LOCALE_CODE is a key to store Locale code in session.
    */
    public static final String SESSION_LOCALE_CODE = "sessionLocaleCode";
    
    /** The first page number. */
    public static final String PAGE_FIRST = "1";
    
    /** The Constant PAGE_TYPE_POPUP. */
    public static final String PAGE_TYPE_POPUP = "POPUP";
    
    /** The Constant ACTION_FWD_SUCCESS_POPUP. */
    public static final String ACTION_FWD_SUCCESS_POPUP = "successPopup";
    
    /** The action forward Initial. */
    public static final String ACTION_FWD_INITIAL = "initial";
    
    /** The action forward Initial Help. */
    public static final String ACTION_FWD_INITIAL_HELP = "initialHelp";
    
    /** The action forward success. */
    public static final String ACTION_FWD_SUCCESS = "success";
    
    /** The action forward failure. */
    public static final String ACTION_FWD_FAILURE = "failure";
    
    /** The action forward logout. */
    public static final String ACTION_FWD_LOGOUT = "logout";
    
    /** The action forward contact us. */
    public static final String ACTION_FWD_SUCCESS_CONTACT_US = "successContactUs";
    
    /** The active flag. */
    public static final String IS_ACTIVE = "1";
    
    /** The active flag. */
    public static final String IS_NOT_ACTIVE = "0";
    
    /**
     * Message Code Web Service Not Available.
     */
    public static final String MSG_CODE_WEB_SERVICE_NOT_AVAILABLE = "01-80-0001";
    
    /**Start: Miscellaneous Type for Purchase Order **/
    
    /** The PO Status. */
    public static final String MISC_TYPE_PO_STATUS = "POStatusCB";
    
    /** The PO Type. */
    public static final String MISC_TYPE_PO_TYPE = "POTypeCB";
    
    /** The Period Type. */
    public static final String MISC_TYPE_PERIOD_TYPE = "PeriodTyCB";
    
    /** The View PDF. */
    public static final String MISC_TYPE_VIEW_PDF = "POVwPDFCB";
    
    /** The Period Type Firm. */
    public static final String FIRM = "Firm";
    
    /** The Period Type Forecast. */
    public static final String FORECAST = "Forecast";
    
    /** The PO Status ISSUED. */
    public static final String ISS_TYPE = "ISS";
    
    /** D/O Data Type. */
    public static final String DDO_TYPE = "DDO";
    
    /** Change D/O Data Type. */
    //public static final String DCD_TYPE = "DCD";
    
    /** Kanban Data Type. */
    public static final String KDO_TYPE = "KDO";
    
    /** DPO Data Type. */
    public static final String DPO_TYPE = "DPO";
    
    /** P/O Data Type. */
    public static final String IPO_TYPE = "IPO";
    
    // [IN043] : Add constants for PO Type
    /** P/O Type Import KANBAN. */
    public static final String IKO_TYPE = "IKO";
    
    /** P/O Data Kanban Type. */
    //public static final String KPO_TYPE = "KPO";
    public static final String DKO_TYPE = "DKO";
    
    /** P/O Data Import Kanban Type. */
    //public static final String IKP_TYPE = "IKP";
    
    /** Change P/O Data Type. */
    //public static final String DCP_TYPE = "DCP";
    
    /** Change P/O Kanban Data Type. */
    //public static final String KCP_TYPE = "KCP";
    
    /** Change P/O Data Type. */
    //public static final String ICP_TYPE = "ICP";
    
    /** D/O */
    public static final String DO = "D/O";
    
    /** P/O */
    public static final String PO = "P/O";
    
    /** CHG D/O */
    public static final String CHG_DO = "CHG D/O";
    
    /** CHG P/O */
    public static final String CHG_PO = "CHG P/O";
    
    /** The PO Status ACKNOWLEDGE. */
    public static final String PO_STATUS_ACKNOWLEDGE = "ACK";
    
    /** The PO Status FORCE ACKNOWLEDGE. */
    public static final String PO_STATUS_FORCE_ACKNOWLEDGE = "FAC";
    
    /**End: Miscellaneous Type for Purchase Order **/
    
    /** The constant REGX_DATEFORMAT. */
    public static final String REGX_DATEFORMAT 
        = "((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])";
    
    /** The constant REGX_DATEFORMAT. */
    public static final String REGX_NUMBER_FORMAT = "[0-9]+";
    
    /** The constant REGX_DECIMAL_NUMBER_FORMAT. */
    public static final String REGX_DECIMAL_NUMBER_FORMAT = "[0-9]+[.][0-9]+";
    
    /** The constant REGX_DECIMAL_NUMBER_FORMAT. */
    public static final String REGX_DECIMAL_NUMBER_FORMAT_NEG = "[-]?[0-9]+[.][0-9]+";
    
    /** The constant REGX_DATEFORMAT. */
    public static final String REGX_CHARACTER_FORMAT = "[A-Z]";
    
    /** The constant REGX_TIME24HOURS_FORMAT. */
    public static final String REGX_TIME24HOURS_FORMAT = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
    
    /** The constant REGX_EMAIL_FORMAT. */
    public static final String REGX_EMAIL_FORMAT =
        "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z-]+(\\.[A-Za-z-]+)*(\\.[A-Za-z-]+)";
    
    /** The constant REGX_LETTER_FORMAT. */
    public static final String REGX_LETTER_FORMAT = "^[a-zA-Z]+$";
    
    /** The constant REGX_ASN_NO_MONTH_FORMAT. */
    public static final String REGX_ASN_NO_MONTH_FORMAT = "[1-9X-Z]";
    
    /** The constant REGX_ASN_NO_DATE_FORMAT. */
    public static final String REGX_ASN_NO_DATE_FORMAT = "^(([0]?[1-9])|([1-2][0-9])|(3[01]))";

    /** The constant REGX_PIPE. */
    public static final String REGX_PIPE = "[|]";
    
    // [IN068] for remove '+' from UTC
    /** The regular expression for plus sign. */
    public static final String REGX_PLUS = "[+]";

    // [IN063] for remove '/' from Date
    /** The regular expression for slash sign. */
    public static final String REGX_SLASH = "[/]";
    
    /** The constant FORMAT_HH_MM. */
    public static final String FORMAT_HH_MM = "hh:mm";
    
    /** The Constant REPORT_CSV_FORMAT. */
    public static final String REPORT_CSV_FORMAT  = "csv";
    
    /** The Constant REPORT_PDF_FORMAT. */
    public static final String REPORT_PDF_FORMAT = "pdf";
    
    /** The Constant REPORT_JASPER_FORMAT. */
    public static final String REPORT_JASPER_FORMAT = "jasper";
    
    // Start : [IN063] for check upload image extension
    /** GIF file extension. */
    public static final String EXTENSION_GIF = ".gif";

    /** PNG file extension. */
    public static final String EXTENSION_PNG = ".png";

    /** JPG file extension. */
    public static final String EXTENSION_JPG = ".jpg";

    /** JPEG file extension. */
    public static final String EXTENSION_JPEG = ".jpeg";

    /** BMP file extension. */
    public static final String EXTENSION_BMP = ".bmp";
    // End : [IN063] for check upload image extension
    
    /** The Constant MODE_INITIAL. */
    public static final String MODE_INITIAL = "initial";
    
    /** The Constant MODE_SAVE. */
    public static final String MODE_SAVE = "save";
    
    /** The Constant MODE_CANCEL. */
    public static final String MODE_CANCEL = "cancel";
    
    /** The Constant MODE_EDIT. */
    public static final String MODE_EDIT = "Edit";
    
    /** The Constant MODE_UPDATE. */
    public static final String MODE_UPDATE = "Update";
    
    /** The Constant MODE_DELETE. */
    public static final String MODE_DELETE = "Delete";
    
    /** The Constant MODE_REGISTER. */
    public static final String MODE_REGISTER = "Register";
    
    /** The Constant MODE_VIEW. */
    public static final String MODE_VIEW = "View";
    
    /** The Constant MODE_UPLOAD. */
    public static final String MODE_UPLOAD = "Upload";
    
    // [IN054] add new mode for screen Supplier Promised Due Submission
    /** The Constant MODE_REPLY. */
    public static final String MODE_REPLY = "Reply";
    
    /** The Constant SAVE_LIMIT_TERM. */
    public static final Number SAVE_LIMIT_TERM  = 1;
    
    /** The Constant INVOICE_ADMIN_ROLE_TYPE_CD. */
    public static final String INVOICE_ADMIN_ROLE_TYPE_CD  = "01";
    
    /** The Constant INVOICE_COVER_PAGE_ROLE_TYPE_CD. */
    public static final String INVOICE_COVER_PAGE_ROLE_TYPE_CD  = "02";
    
    /** The Constant ASN_DETAIL_INFORMATION_LIST_FILE_NAME. */
    public static final String ASN_DETAIL_INFORMATION_LIST_FILE_NAME = "ASNDetailInformationList";
    
    /** The Constant ASN_UPLOAD_RESULT_LIST. */
    public static final String ASN_UPLOAD_RESULT_LIST_FILE_NAME = "UploadASNResultList";
    
    /** The Constant SUPPLIER_INFO_LIST_FILE_NAME. */
    public static final String SUPPLIER_INFO_LIST_FILE_NAME = "SupplierInformationList";
    
    /** The Constant SUPPLIER_USER_INFO_LIST_FILE_NAME. */
    public static final String SUPPLIER_USER_INFO_LIST_FILE_NAME = "SupplierUserInformationList";
    
    /** The Constant DENSO_USER_INFO_LIST_FILE_NAME. */
    public static final String DENSO_USER_INFO_LIST_FILE_NAME = "DensoUserInformationList";
    
    /** The Constant PURCHASE_ORDER_INFO_LIST_FILE_NAME. */
    public static final String PURCHASE_ORDER_INFO_LIST_FILE_NAME = "PurhcaseOrderList";
    
    /** The Constant PURCHASE_ORDER_INFO_LIST_FILE_NAME. */
    public static final String ASN_PROGRESS_INFORMATION_INFO_LIST_FILE_NAME = "ASNProgressInformationList";
    
    /** The Constant INVOICE_LIST_INFORMATION. */
    public static final String INVOICE_LIST_INFORMATION = "InvoiceInformationList";
    
    /** The Constant UPLOAD_INVOICE_RESULT_LIST. */
    public static final String UPLOAD_INVOICE_RESULT_LIST = "UploadInvoiceResultList";
    
    /** The Constant PRICE_DIFFERENCE_INFORMATION_LIST. */
    public static final String PRICE_DIFFERENCE_INFORMATION_LIST = "PriceDifferenceInformationList";
    
    /** The Constant INVOICE_DETAIL_INFORMATION. */
    public static final String INVOICE_DETAIL_INFORMATION = "InvoiceDetailInformation";
    
    /** The Constant BACK_ORDER_INFORMATION_LIST. */
    public static final String BACK_ORDER_INFORMATION_LIST = "BackOrderList";
    
    /** The Constant KANBAN ORDER INFORMATION LIST. */
    public static final String KANBAN_ORDER_INFORMATION_LIST = "KanbanDeliveryOrderList";
    
    /** The Constant DELIVERY ORDER INFORMATION LIST. */
    public static final String DELIVERY_ORDER_INFORMATION_LIST = "DeliveryOrderList";
    
    /** The Constant ASN_SELECT_MAP. */
    public static final String ASN_SELECT_MAP = "asnSelectedMap";
    
    /** The constant CANNOT_READ_FILE. */
    public static final int CANNOT_READ_FILE = -1;
    
    /** The Constant MAX_ANNOUNCE_MESSAGE_LENGTH. */
    public static final int MAX_ANNOUNCE_MESSAGE_LENGTH = 100;
    
    /** The Constant MAX_TEXTBOX_LENGTH. */
    public static final int MAX_TEXTBOX_LENGTH = 300;
    
    /** The Constant MAX_ASN_NO_LENGTH. */
    public static final int MAX_ASN_NO_LENGTH = 16;
    
    /** The Constant MAX_SUPPLIER_PLANT_TAX_ID_LENGTH. */
    public static final int MAX_SUPPLIER_PLANT_TAX_ID_LENGTH = 20;
    
    /** The Constant MAX_DSC_ID_LENGTH. */
    public static final int MAX_DSC_ID_LENGTH = 20;
    
    /** The Constant MAX_FIRST_NAME_LENGTH. */
    public static final int MAX_FIRST_NAME_LENGTH = 20;
    
    /** The Constant MAX_MIDDLE_NAME_LENGTH. */
    public static final int MAX_MIDDLE_NAME_LENGTH = 20;
    
    /** The Constant MAX_LAST_NAME_LENGTH. */
    public static final int MAX_LAST_NAME_LENGTH = 30;
    
    /** The Constant MAX_EMAIL_LENGTH. */
    public static final int MAX_EMAIL_LENGTH = 50;
    
    /** The Constant MAX_EMAIL_LENGTH. */
    public static final int MAX_INVOICE_NO_LENGTH = 25;
    
    /** The Constant MAX_EMAIL_LENGTH. */
    public static final int MAX_D_PART_NO_LENGTH = 15;
    
    /** The Constant MAX_EMAIL_LENGTH. */
    public static final int MAX_S_PART_NO_LENGTH = 20;
    
    /** The Constant MAX_TELEPHONE_LENGTH. */
    public static final int MAX_TELEPHONE_LENGTH = 23;
    
    /** The Constant MAX_CN_NO_LENGTH. */
    public static final int MAX_CN_NO_LENGTH = 25;
    
    /** The Constant MAX_INVOICE_LENGTH. */
    public static final int MAX_INVOICE_LENGTH = 25;
    
    /** The Constant MAX_DEPARTMENT_LENGTH. */
    public static final int MAX_DEPARTMENT_LENGTH = 50;
    
    /** The Constant MAX_SPS_DO_NO. */
    public static final int MAX_SPS_DO_NO = 11;
    
    /** The Constant MAX_EMPLOYEE_CD_LENGTH. */
    public static final int MAX_EMPLOYEE_CD_LENGTH = 10;
    
    /** The Constant THREE_HUNDREDS. */
    public static final int THREE_HUNDREDS = 300;
    
    /** The Constant FIVE_HUNDREDTH. */
    public static final int FIVE_HUNDREDTH = 500;
    
    /** The Constant TRANSFER_CIGMA_FLAG_YES. */
    public static final String TRANSFER_CIGMA_FLAG_YES = "1";
    
    /** The Constant TRANSFER_CIGMA_FLAG_NO. */
    public static final String TRANSFER_CIGMA_FLAG_NO = "0";
    
    /** The constant HTTP_HEADER_KEY_UID. */
    public static final String HTTP_HEADER_KEY_UID = "UID";
    
    /** The constant RANK_FIRST_RANK. */
    public static final String RANK_FIRST_RANK = "1";
    
    /** The constant RANK_SECOND_RANK. */
    public static final String RANK_SECOND_RANK = "2";
    
    /** The constant SUPPRESS WARNINGS UNCHECKED */
    public static final String SUPPRESS_WARNINGS_UNCHECKED = "unchecked";
    
    /** The constant SUPPRESS_WARNINGS_RAWTYPES */
    public static final String SUPPRESS_WARNINGS_RAWTYPES = "rawtypes";
    
    /* Message Util ============================================================================= */
    
    /** The constant MESSAGE_UTIL_ERROR_CANNOT_GET_PROPERTY. */
    public static final String MESSAGE_UTIL_CANNOT_GET_PROPERTY = "Cannot get property for key : ";
    
    /** The constant MESSAGE_UTIL_PREFIX_LABEL. */
    public static final String MESSAGE_UTIL_PREFIX_LABEL = "label.";
    
    /** The constant MESSAGE_UTIL_PREFIX_MASTER_COLUMN. */
    public static final String MESSAGE_UTIL_PREFIX_MASTER_COLUMN = "mst.column.";
    
    /** The constant MESSAGE_UTIL_PREFIS_LABEL_FORMAT. */
    public static final String MESSAGE_UTIL_PREFIS_LABEL_FORMAT = "label.format.";
    
    /** The constant DENSO_AND_SUPPLIER_RELATION. */
    public static final String DENSO_AND_SUPPLIER_RELATION = "DENSO_AND_SUPPLIER_RELATION";

    /* End Message Util ========================================================================= */
    
    /* Symbol =================================================================================== */
    
    /** The constant SYMBOL_OPEN_SQUARE_BRACKET. */
    public static final String SYMBOL_OPEN_SQUARE_BRACKET = "[";

    /** The constant SYMBOL_CLOSE_SQUARE_BRACKET. */
    public static final String SYMBOL_CLOSE_SQUARE_BRACKET = "]";
    
    /** The constant SYMBOL_OPEN_BRACKET. */
    public static final String SYMBOL_OPEN_BRACKET = "(";
    
    /** The constant SYMBOL_CLOSE_BRACKET. */
    public static final String SYMBOL_CLOSE_BRACKET = ")";
    
    /** The constant SYMBOL_OPEN_BRACES. */
    public static final String SYMBOL_OPEN_BRACES = "{";

    /** The constant SYMBOL_CLOSE_BRACES. */
    public static final String SYMBOL_CLOSE_BRACES = "}";

    /** The constant SYMBOL_EQUAL. */
    public static final String SYMBOL_EQUAL = "=";
    
    /** The constant SYMBOL_COLON. */
    public static final String SYMBOL_COLON = ":";

    /** The constant SYMBOL_COMMA. */
    public static final String SYMBOL_COMMA = ",";
    
    /** The constant SYMBOL_COMMA_CHAR. */
    public static final Character SYMBOL_COMMA_CHAR = ',';

    /** The constant SYMBOL_SEMI_COLON. */
    public static final String SYMBOL_SEMI_COLON = ";";

    /** The constant SYMBOL_SLASH. */
    public static final String SYMBOL_SLASH = "/";

    /** The constant SYMBOL_BACK_SLASH. */
    public static final String SYMBOL_BACK_SLASH = "\\";

    /** The constant SYMBOL_SPACE. */
    public static final String SYMBOL_SPACE = " ";
    
    /** The constant SYMBOL_UNDER_SCORE. */
    public static final String SYMBOL_UNDER_SCORE = "_";
    
    /** The constant SYMBOL_DOT. */
    public static final String SYMBOL_DOT = ".";

    /** The constant SYMBOL_AT. */
    public static final String SYMBOL_AT = "@";

    /** The constant SYMBOL_AMPERSAND. */
    public static final String SYMBOL_AMPERSAND = "&";
    
    /** The constant SYMBOL_PERCENT. */
    public static final String SYMBOL_PERCENT = "%";
    
    /** The constant SYMBOL_DOUBLE_QUOTE. */
    public static final String SYMBOL_DOUBLE_QUOTE = "\"";

    /** The constant SYMBOL_SINGLE_QUOTE. */
    public static final String SYMBOL_SINGLE_QUOTE = "'";

    /** The constant SYMBOL_CR_LF. */
    public static final String SYMBOL_CR_LF = "\r\n";

    /** The constant SYMBOL_STAR. */
    public static final String SYMBOL_STAR = "*";
    
    /** The constant SYMBOL_SUBTRACT. */
    public static final String SYMBOL_SUBTRACT = "-";
    
    /** The constant SYMBOL_PLUS. */
    public static final String SYMBOL_PLUS = "+";

    /** The constant SYMBOL_PIPE. */
    public static final String SYMBOL_PIPE = "|";

    /** The constant SYMBOL_QUESTION_MARK. */
    public static final String SYMBOL_QUESTION_MARK = "?";
    
    /** The constant SYMBOL_NEWLINE. */
    public static final String SYMBOL_NEWLINE = "<br />";
    
    /** The constant SYMBOL_END_TABLE. */
    public static final String SYMBOL_END_TABLE = "</table>";
    
    /** The constant SYMBOL_START_PARAGRAPH. */
    public static final String SYMBOL_START_PARAGRAPH = "<P>";

    // [IN070] for end remark
    /** The constant SYMBOL_END_PARAGRAPH. */
    public static final String SYMBOL_END_PARAGRAPH = "</ P>";
    
    /** The constant SYMBOL_NEWLINE_REPLACMENT. */
    public static final String SYMBOL_NEWLINE_REPLACMENT = "<br&nbsp;/>";
    
    /** The constant SYMBOL_NBSP. */
    public static final String SYMBOL_NBSP = "&nbsp;";
    
    // Start : [IN068] for change color in email
    /** The constants for HTML tag span with red color */
    public static final String SYMBOL_SPAN_RED = "<span style='color:red'>";

    /** The constants for close HTML tag span */
    public static final String SYMBOL_END_SPAN = "</span>";
    // End : [IN068] for change color in email
    
    /* End Symbol =============================================================================== */
    
    /* Word ===================================================================================== */

    /** The constant WORD_CHANGE. */
    public static final String WORD_CHANGE = "Change";

    /** The constant WORD_ORIGINAL. */
    public static final String WORD_ORIGINAL = "Original";
    
    /** The constant WORD_MSIE. */
    public static final String WORD_MSIE = "MSIE";
   
    /** The constant WORD_MESSAGE. */
    public static final String WORD_MESSAGE = "Message";

    /** The constant WORD_KEY. */
    public static final String WORD_KEY = "Key";

    /** The constant WORD_IGNORE. */
    public static final String WORD_IGNORE = "ignore";

    /** The constant WORD_ENCODING. */
    public static final String WORD_ENCODING = "encoding";
    
    /** The constant WORD_LOCALE. */
    public static final String WORD_LOCALE = "Locale";

    /** The constant WORD_PRIVATE. */
    public static final String WORD_PRIVATE = "private";
    
    /** The constant WORD_UTC. */
    public static final String WORD_UTC = "UTC";

    /** The constant WORD_ERROR. */
    public static final String WORD_ERROR = "Error";
    
    /** The constant WORD_YES. */
    public static final String WORD_YES = "yes";
    
    /** The constant WORD_AND. */
    public static final String WORD_AND = "and";

    /** The constant WORD_FIXED. */
    public static final String WORD_FIXED = "Fixed";

    /** The constant WORD_FIXED_UPPER. */
    public static final String WORD_FIXED_UPPER = "FIXED";

    /** The constant WORD_KANBAN. */
    public static final String WORD_KANBAN = "Kanban";

    /** The constant WORD_KANBAN_UPPER. */
    public static final String WORD_KANBAN_UPPER = "KANBAN";

    /** The constant WORD_ORDER. */
    public static final String WORD_ORDER = "Order";

    /** The constant WORDS_PROPERTY_FILE. */
    public static final String WORDS_PROPERTY_FILE = "Properties file";

    /** The constant WORDS_COMPLETE_MESSAGE. */
    public static final String WORDS_COMPLETE_MESSAGE = "Complete message";
    
    /* End Word ================================================================================= */

    /* HTTP HEADER ============================================================================== */

    /** The constant HTTP_HEADER_CONTENT_DISPOSITION. */
    public static final String HTTP_HEADER_CONTENT_DISPOSITION = "Content-disposition";

    /** The constant HTTP_HEADER_ATTACHMENT_FILENAME. */
    public static final String HTTP_HEADER_ATTACHMENT_FILENAME = "attachment; filename=";

    /** The constant HTTP_HEADER_CONTENTTYPE_APPLICATION. */
    public static final String HTTP_HEADER_CONTENTTYPE_APPLICATION = "application/";

    /** The constant HTTP_HEADER_JSON. */
    public static final String HTTP_HEADER_JSON = "json";
    
    /** The constants HTTP_HEADER_CHARSET. */
    public static final String HTTP_HEADER_CHARSET = "charset";
    
    /** The constant HTTP_HEADER_ENCODING_TIS_620. */
    public static final String HTTP_HEADER_ENCODING_TIS_620 = "TIS-620";

    /** The constant HTTP_HEADER_CACHE_CONTROL. */
    public static final String HTTP_HEADER_CACHE_CONTROL = "Cache-Control";
    
    /** The constant HTTP_HEADER_NO_CACHE. */
    public static final String HTTP_HEADER_NO_CACHE = "no-cache";

    /** The constant HTTP_HEADER_PRAGMA. */
    public static final String HTTP_HEADER_PRAGMA = "Pragma";

    /** The constant HTTP_HEADER_EXPIRES. */
    public static final String HTTP_HEADER_EXPIRES = "Expires";
    
    /** The constant HTTP_HEADER_USER_AGENT. */
    public static final String HTTP_HEADER_USER_AGENT = "User-Agent";
    
    /** The constant HTTP_HEADER_SHIFT_JIS. */
    public static final String HTTP_HEADER_SHIFT_JIS = "Shift_JIS";
    
    /** The constant HTTP_HEADER_UTF_8. */
    public static final String HTTP_HEADER_UTF_8 = "UTF-8";
   
    

    /* END HTTP HEADER ========================================================================== */
    
    /** The constant MAX_SPS_DO_NO_LENGTH. */
    public static final int MAX_SPS_DO_NO_LENGTH = 11;
    
    /** The constant MAX_CIGMA_DO_NO_LENGTH. */
    public static final int MAX_CIGMA_DO_NO_LENGTH = 10;
    
    /** The constant MAX_ROUTE_LENGTH. */
    public static final int MAX_ROUTE_LENGTH = 6;
    
    /** The constant MAX_DEL_LENGTH. */
    public static final int MAX_DEL_LENGTH = 3;
    
    /** The constant SHIP_NOTICE_FLAG_YES. */
    public static final String SHIP_NOTICE_FLAG_YES = "1";
    
    /** The constant SHIP_NOTICE_FLAG_NO. */
    public static final String SHIP_NOTICE_FLAG_NO = "0";
    
    /** The constant MAP_KEY_SELECTED_DO. */
    public static final String MAP_KEY_SELECTED_DO = "selectedDoMap";
    
    /** The constant MAP_KEY_SELECTED_INV. */
    public static final String MAP_KEY_SELECTED_INV = "selectedInvMap";
    
    /** The constant MAP_KEY_SELECTED_DSC_ID. */
    public static final String MAP_KEY_SELECTED_DSC_ID = "selectedDscIdMap";
    
    /** The constant MAP_KEY_SELECTED_DENSO_DSC_ID. */
    public static final String MAP_KEY_SELECTED_DENSO_DSC_ID = "selectedDensoDscIdMap";
    
    /** The constant MAP_KEY_SELECTED_ROLE_CODE. */
    public static final String MAP_KEY_SELECTED_ROLE_CODE = "selectedRoleCodeMap";
    
    /** The constant MAP_KEY_SELECTED_DENSO_ROLE_CODE. */
    public static final String MAP_KEY_SELECTED_DENSO_ROLE_CODE = "selectedDensoRoleCodeMap";
    
    /** The constant PAGE_SEARCH. */
    public static final String PAGE_SEARCH = "search";
    
    /** The constant PAGE_CLICK. */
    public static final String PAGE_CLICK = "click";
    
    /** The constant SHIPMENT_STATUS_ISS. */
    public static final String SHIPMENT_STATUS_ISS = "ISS";
    
    /** The constant SHIPMENT_STATUS_CPS. */
    public static final String SHIPMENT_STATUS_CPS = "CPS";
    
    /** The constant SHIPMENT_STATUS_PTS. */
    public static final String SHIPMENT_STATUS_PTS = "PTS";
    
    // [IN012] new shipment status
    /** The constant SHIPMENT_STATUS_CCL. */
    public static final String SHIPMENT_STATUS_CCL = "CCL";
    
    /** The constant ASN_STATUS_N. */
    public static final String ASN_STATUS_N = "N";
    
    /** The constant ASN_STATUS_D. */
    public static final String ASN_STATUS_D = "D";
    
    /** The constant ASN_STATUS_P. */
    public static final String ASN_STATUS_P = "P";
    
    /** The constant ASN_STATUS_R. */
    public static final String ASN_STATUS_R = "R";
    
    /** The constant ASN_STATUS_ISS. */
    public static final String ASN_STATUS_ISS = "ISS";
   
    /** The constant ASN_STATUS_CCL. */
    public static final String ASN_STATUS_CCL = "CCL";
    
    /** The constant ASN_STATUS_PTR. */
    public static final String ASN_STATUS_PTR = "PTR";
    
    /** The constant ASN_STATUS_RCP. */
    public static final String ASN_STATUS_RCP = "RCP";
    
    /** The constant ASN_STATUS_NEW. */
    public static final String ASN_STATUS_NEW = "NEW";
    
    /** The constant INVOICE_STATUS_ISS. */
    public static final String INVOICE_STATUS_ISS = "ISS";
    
    /** The constant INVOICE_STATUS_ISC. */
    public static final String INVOICE_STATUS_ISC = "ISC";
    
    /** The constant INVOICE_STATUS_DCL. */
    public static final String INVOICE_STATUS_DCL = "DCL";
    
    /** The constant INVOICE_STATUS_RAP. */
    public static final String INVOICE_STATUS_RAP = "RAP";
    
    /** The constant INVOICE_STATUS_DHP. */
    public static final String INVOICE_STATUS_DHP = "DHP";
    
    /** The constant INVOICE_STATUS_PAY. */
    public static final String INVOICE_STATUS_PAY = "PAY";
    
    /** The constant ISS_STATUS. */
    public static final String ISS_STATUS = "ISS";
    
    /** The constant CCL_STATUS. */
    public static final String CCL_STATUS = "CCL";
    
    /** The constant ERROR_FLAG_CORRECT. */
    public static final String ERROR_FLAG_CORRECT = "0";
    
    /** The constant ERROR_FLAG_WARNING. */
    public static final String ERROR_FLAG_WARNING = "1";
    
    /** The constant ERROR_FLAG_ERROR. */
    public static final String ERROR_FLAG_ERROR = "2";
    
    /** The constant DISPLAY_DECIMAL_FLAG_YES. */
    public static final String DISPLAY_DECIMAL_FLAG_YES = "1";
    
    /** The constant DISPLAY_DECIMAL_FLAG_NO. */
    public static final String DISPLAY_DECIMAL_FLAG_NO = "0";
    
    /** The constant DEFAULT_REVISION. */
    public static final String DEFAULT_REVISION = "00";
    
    /** The constant DEFAULT_RUINNING_NO. */
    public static final String DEFAULT_RUINNING_NO = "0001";
    
    /** The constant MESSAGE_TYPE_WARNING. */
    public static final String MESSAGE_TYPE_WARNING = "W";
    
    /** The constant MESSAGE_TYPE_ERROR. */
    public static final String MESSAGE_TYPE_ERROR = "E";
    
    /** The constant MISC_CODE_ALL. */
    public static final String MISC_CODE_ALL = "ALL";
    
    /** The constant STR_NULL. */
    public static final String STR_NULL = "NULL";
    
    /** The constant STR_A. */
    public static final String STR_A = "A";
    
    /** The constant STR_C. */
    public static final String STR_C = "C";
    
    /** The constant STR_C_STAR. */
    public static final String STR_C_STAR = "C*";
    
    /** The constant STR_K. */
    public static final String STR_K = "K";
    
    /** The constant STR_D. */
    public static final String STR_D = "D";
    
    /** The constant STR_F. */
    public static final String STR_F = "F";
    
    /** The constant STR_F_LOWERCASE. */
    public static final String STR_F_LOWERCASE = "f";
    
    /** The constant STR_M. */
    public static final String STR_M = "M";
    
    /** The constant STR_N. */
    public static final String STR_N = "N";
    
    /** The constant STR_P. */
    public static final String STR_P = "P";
    
    /** The constant STR_T. */
    public static final String STR_T = "T";
    
    /** The constant STR_U. */
    public static final String STR_U = "U";
    
    // [IN059] Add new constant for "V"
    /** The constant STR_V. */
    public static final String STR_V = "V";
    
    /** The constant STR_W. */
    public static final String STR_W = "W";
    
    /** The constant STR_X. */
    public static final String STR_X = "X";
    
    /** The constant STR_Y. */
    public static final String STR_Y = "Y";
    
    /** The constant STR_Z. */
    public static final String STR_Z = "Z";
    
    /** The Constant STR_CREATE. */
    public static final String STR_CREATE = "Create";
    
    /** The Constant STR_COLON. */
    public static final String STR_COLON = ":";
    
    /** The Constant STR_UPDATE. */
    public static final String STR_UPDATE = "Update";
    
    /** The Constant STR_UPLOAD. */
    public static final String STR_UPLOAD = "Upload";
    
    /** The Constant STR_CANCEL. */
    public static final String STR_CANCEL = "Cancel";
    
    /** The Constant STR_RETURN. */
    public static final String STR_RETURN = "Return";
    
    /** The Constant STR_DELETE. */
    public static final String STR_DELETE = "Delete";
    
    /** The constant STR_DSCID. */
    public static final String STR_DSCID = "DSC ID";
    
    /** The constant STR_SUP_COMPANY_CODE. */
    public static final String STR_SUP_COMPANY_CODE = "Supplier Company Code";
    
    /** The constant STR_SUP_PLANT_CODE. */
    public static final String STR_SUP_PLANT_CODE = "Supplier Plant Code";
    
    /** The constant STR_DENSO_OWNER. */
    public static final String STR_DENSO_OWNER = "DENSO Owner";
    
    /** The constant STR_TELEPHONE. */
    public static final String STR_FIRS_NAME = "First Name";
    
    /** The constant STR_TELEPHONE. */
    public static final String STR_MIDDLE_NAME = "Middle Name";
    
    /** The constant STR_TELEPHONE. */
    public static final String STR_LAST_NAME = "Last Name";
    
    /** The constant STR_DEPARTMENT. */
    public static final String STR_DEPARTMENT = "Department";

    /** The constant STR_EMAIL. */
    public static final String STR_EMAIL = "Email";
    
    /** The constant STR_TELEPHONE. */
    public static final String STR_TELEPHONE = "Telephone";
    
    /** The constant DENSO_MALAYSIA. */
    public static final String DENSO_MALAYSIA = "DNMY";
    
    /** The constant DENSO_INDONESIA. */
    public static final String DENSO_INDONESIA = "DNIA";
    
    /** The constant Header Asn No. */
    public static final String STR_ASN_NO = "ASN_NO";
    
    /** The constant Header Supplier Code. */
    public static final String STR_S_CD = "S_CD";
    
    /** The constant Header Supplier Plant Code. */
    public static final String STR_S_PCD = "S_PCD";
    
    /** The constant Header Denso Code. */
    public static final String STR_D_CD = "D_CD";
    
    /** The constant Header Denso Plant Code. */
    public static final String STR_D_PCD = "D_PCD";
    
    /** The constant Header Denso PN. */
    public static final String STR_D_PN = "D_PN";
    
    /** The constant Header P/N No. */
    public static final String STR_S_PN = "S_PN";
    
    /** The constant Header Shipping Quantity. */
    public static final String STR_SHIPPNG_QTY = "SHIPPNG_QTY";
    
    /** The constant Header Receiving Quantity. */
    public static final String STR_RECEIVING_QTY = "RECEIVING_QTY";
    
    /** The constant Header Actual ETA. */
    public static final String STR_ACTUAL_ETA = "ACTUAL_ETA";
    
    /** The constant Header Asn PN Receive Status */
    public static final String STR_PN_RCV_STATUS = "PN_RCV_STATUS";
    
    /** The constant Header Asn Receive Status. */
    public static final String STR_ASN_RCV_STATUS = "ASN_RCV_STATUS";
    
    /** The constant Header Invoice No. */
    public static final String STR_INVOICE_NO = "INVOICE_NO";
    
    /** The constant Header Invoice Date. */
    public static final String STR_INVOICE_DATE = "INVOICE_DATE";
    
    /** The constant Header Generate Date. */
    public static final String STR_GENERATE_DATE = "GENERATE_DATE";
    
    /** The constant STR_NG. */
    public static final String STR_NG = "NG";
    
    /** The constant STR_OK. */
    public static final String STR_OK = "OK";
    
    /** The constant BACK_SLASH_BACK_SLASH. */
    public static final String BACK_SLASH_BACK_SLASH = "\\";
    
    /** The constant EXPIRE_MESSAGE. */
    public static final String EXPIRE_MESSAGE = "EXPIRE_MSG";
    
    /** The constant APPLICATION_CONTEXT_AIJB_JOB_XML. */
    public static final String APPLICATION_CONTEXT_AIJB_JOB_XML = "applicationContext-aijb-job.xml";
    
    /** The constant APPLICATION_CONTEXT_AIJB_AOP_XML. */
    public static final String APPLICATION_CONTEXT_AIJB_AOP_XML = "applicationContext-aijb-aop.xml";
    
    /** The constant APPLICATION_CONTEXT_AIJB_JDBC_XML. */
    public static final String APPLICATION_CONTEXT_AIJB_JDBC_XML 
        = "applicationContext-aijb-jdbc.xml";
    
    /** The constant SPS_M_COMPANY_DENSO_SERVICE. */
    public static final String SPS_M_COMPANY_DENSO_SERVICE = "spsMCompanyDensoService";
    
    /** The constant SPS_M_ANNOUNCE_MESSAGE_SERVICE. */
    public static final String SPS_M_ANNOUNCE_MESSAGE_SERVICE = "spsMAnnounceMessageService";
    
    /** The constant SPS_SYSTEM. */
    public static final String SPS_SYSTEM = "SPS_SYSTEM";
    
    /** The constant SPS_TMP_FILE_BACKUP_SERVICE. */
    public static final String SPS_TMP_FILE_BACKUP_SERVICE = "spsTmpFileBackupService";
    
    /** The constant COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG. */
    public static final String COM_GLOBALDENSO_SPS_CONF_APPLICATION_CONFIG 
        = "com.globaldenso.asia.sps.conf.ApplicationConfig";
    
    /** The constant DEFAULT_LOCALE. */
    public static final String DEFAULT_LOCALE = "DEFAULT_LOCALE";
    
    /** The constant PROP_BASE_DIR_GUI. */
    public static final String PROP_BASE_DIR_GUI = "BASE_DIR_GUI";
    
    /** The constant PROP_BASE_DIR_MSG. */
    public static final String PROP_BASE_DIR_MSG = "BASE_DIR_MSG";
    
    /** The constant UNUSED. */
    public static final String UNUSED = "unused";
    
    /** The constant UPDATE_USER_INFORMATION. */
    public static final String UPDATE_USER_INFORMATION = "update user information";
    
    /** The constant ERROR. */
    public static final String ERROR = "Error";
    
    /** The constant INNER_TEXT_FROM_EXCEPTION. */
    public static final String INNER_TEXT_FROM_EXCEPTION = "Inner Text from Exception";
    
    /** The constant PO_FORCE_ACKNOWLEDGE. */
    public static final String PO_FORCE_ACKNOWLEDGE = "P/O Force Acknowledge";
    
    /** The constant MAIL_FORMAT_TEXT_HMTL. */
    public static final String MAIL_FORMAT_TEXT_HMTL = "text/html";
    
    /** The constant MAIL_FORMAT_PLAIN_TEXT. */
    public static final String MAIL_FORMAT_PLAIN_TEXT = "plain/text";
    
    /** The constant MAIL_FORMAT_MULTIPART_MIXED. */
    public static final String MAIL_FORMAT_MULTIPART_MIXED = "multipart/mixed";
    
    /** The constant DAY_MILLIS. */
    public static final long DAY_MILLIS = 86400000l;
    
    /** The constant FORMAT_DIGIT_SHIPPING_QTY. */
    public static final String FORMAT_DIGIT_SHIPPING_QTY = "9,999,999.99";
    
    /** The constant FORMAT_DIGIT_NUMBER_OF_PALLET. */
    public static final String FORMAT_DIGIT_NUMBER_OF_PALLET = "99,999,999,999.99";
    
    /** The constant UNDEFINED_FOR_SUPPLIER. */
    public static final String UNDEFINED_FOR_SUPPLIER = "-";

    // [IN039] : Add format digit for Shipping Box Qty
    /** The constant FORMAT_DIGIT_SHIPPING_BOX_QTY. */
    public static final String FORMAT_DIGIT_SHIPPING_BOX_QTY = "99,999,999,999";
    
    /* Language ========================================================================== */
    
    /** The constant LANGUAGE_EN. */
    public static final String LANGUAGE_EN = "en_US";
    
    /** The constant EN. */
    public static final String EN = "en";
    
    /** The constant LANGUAGE_TH. */
    public static final String LANGUAGE_TH = "th_TH";
    
    /** The constant TH. */
    public static final String TH = "th";
    
    /** The constant LANGUAGE_JA. */
    public static final String LANGUAGE_JA = "ja_JP";
    
    /** The constant JA. */
    public static final String JA = "ja";
    
    /** The constant MEGABYTES. */
    public static final String MEGABYTES = "MB";
    
    /** The constant REGX_TIME24HOURS_FORMAT. */
    public static final String REGX_TIME24HOURSML_FORMAT = "([01]?[0-9]|2[0-3])[0-5][0-9][0-5][0-9]";
    /**
     * Instantiates a new constants.
     */
    public Constants(){
        super();
    }
    
    /**
     * <p>Getter method for message type success.</p>
     *
     * @return the message type success
     */
    public String getMESSAGE_SUCCESS() {
        return MESSAGE_SUCCESS;
    }

    /**
     * <p>Getter method for message type failure.</p>
     *
     * @return the message type failure
     */
    public String getMESSAGE_FAILURE() {
        return MESSAGE_FAILURE;
    }

    /**
     * <p>Getter method for message type warning.</p>
     *
     * @return the message type warning
     */
    public String getMESSAGE_WARNING() {
        return MESSAGE_WARNING;
    }
    /**
     * <p>Getter method for mode edit.</p>
     *
     * @return the mode edit.
     */
    public String getMODE_EDIT() {
        return MODE_EDIT;
    }
    
    /**
     * <p>Getter method for mode register.</p>
     *
     * @return the mode register.
     */
    public String getMODE_REGISTER() {
        return MODE_REGISTER;
    }
    

    /**
     * <p>Getter method for Misc Code ALL.</p>
     *
     * @return the Misc Code ALL.
     */
    public String getMISC_CODE_ALL() {
        return MISC_CODE_ALL;
    }
    
    /**
     * <p>Getter method for Message type warning.</p>
     *
     * @return the Message type warning.
     */
    public String getMESSAGE_TYPE_WARNING() {
        return MESSAGE_TYPE_WARNING;
    }
    
    /**
     * <p>Getter method for Message type Error.</p>
     *
     * @return the Message type Error.
     */
    public String getMESSAGE_TYPE_ERROR() {
        return MESSAGE_TYPE_ERROR;
    }
    
    /**
     * <p>Getter method for Message mode view.</p>
     *
     * @return the Message mode view.
     */
    public String getMODE_VIEW() {
        return MODE_VIEW;
    }
    
    /**
     * <p>Getter method for Message mode initial.</p>
     *
     * @return the Message mode initial.
     */
    public String getMODE_INITIAL() {
        return MODE_INITIAL;
    }
    
    /**
     * Getter method for MODE_UPLOAD.
     * @return the constant MODE_UPLOAD
     * */
    public String getMODE_UPLOAD() {
        return MODE_UPLOAD;
    }
    
    /**
     * Getter method for INVOICE_STATUS_ISS.
     * @return the constant INVOICE_STATUS_ISS
     * */
    public String getINVOICE_STATUS_ISS() {
        return INVOICE_STATUS_ISS;
    }
    
    /**
     * Getter method for INVOICE_STATUS_ISC.
     * @return the constant INVOICE_STATUS_ISC
     * */
    public String getINVOICE_STATUS_ISC() {
        return INVOICE_STATUS_ISC;
    }
    
    /**
     * Getter method for INVOICE_STATUS_DCL.
     * @return the constant INVOICE_STATUS_DCL
     * */
    public String getINVOICE_STATUS_DCL() {
        return INVOICE_STATUS_DCL;
    }
    
    /**
     * Getter method for UNDEFINED_FOR_SUPPLIER.
     * @return the constant UNDEFINED_FOR_SUPPLIER
     * */
    public String getUNDEFINED_FOR_SUPPLIER() {
        return UNDEFINED_FOR_SUPPLIER;
    }
    
    // [IN054] add new mode for screen Supplier Promised Due Submission
    /**
     * Getter method for MODE_REPLY.
     * @return the constant MODE_REPLY
     * */
    public String getMODE_REPLY() {
        return MODE_REPLY;
    }
    
}