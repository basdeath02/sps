/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 2016/03/31 CSI Akat           [IN063]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.conf;

/**
 * The Class ApplicationConfig.
 * @author CSI
 */
public class ApplicationConfig {

    /**
     * Instantiates a new application config.
     */
    public ApplicationConfig(){
        super();
    }
    
    /**
    * Keys in ApplicationConfig.properties file.
    */
    public enum KEY {
        
        /** The BASE_DIR_DEFAULT_THEME. */
        BASE_DIR_DEFAULT_THEME,
        
        /** The BASE_DIR_GUI. */
        BASE_DIR_GUI,
        
        /** The BASE_DIR_REPORT_GUI. */
        BASE_DIR_REPORT_GUI,
        
        /** The BASE_DIR_EMAIL. */
        BASE_DIR_EMAIL,
        
        /** The BASE_DIR_MSG. */
        BASE_DIR_MSG,
        
        /** The DEFAULT_LOCALE. */
        DEFAULT_LOCALE,
        
        /** The LEGEND_FILE_ID. */
        LEGEND_FILE_ID,
        
        /** The MAX_CSV_FILE_SIZE. */
        MAX_CSV_FILE_SIZE,
        
        /** Jasper Report file path. */ 
        JASPER_FILE_PATH,
        
        /** Email Smtp. */ 
        EMAIL_SMTP,
        
        /** Default Email From. */ 
        DEFAULT_EMAIL_FROM,
        
        /** Default CIGMA P/O No. */ 
        DEFAULT_CIGMA_PO_NO

        // [IN063] for upload digital signature
        /** Maximum image for digital signature file size. */
        , MAX_SIGNATURE_FILE_SIZE

        // Purge process send e-mail to person in charge
        /** Purge process send e-mail to person in charge. */
        , PURGE_EMAIL_SEND_TO
    }
}