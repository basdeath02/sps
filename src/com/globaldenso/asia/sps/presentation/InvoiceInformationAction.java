/*
 * ModifyDate Developmentcompany        Describe 
 * 2014/06/24 CSI Karnrawee             Create
 * 2018/04/11 Netband U.Rungsiwut       Generate invoice PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.InvoiceInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Winv001Form;

/**
 * The Class InvoiceInformationAction.
 * @author CSI
 */
public class InvoiceInformationAction extends CoreAction {

    /** The invoice information facade service. */
    private InvoiceInformationFacadeService invoiceInformationFacadeService = null;
    
    /** The default constructor. */
    public InvoiceInformationAction() {
        super();
    }

    /**
     * Set the invoice information facade service.
     * 
     * @param invoiceInformationFacadeService the invoice information facade service to set
     */
    public void setInvoiceInformationFacadeService(
        InvoiceInformationFacadeService invoiceInformationFacadeService) {
        this.invoiceInformationFacadeService = invoiceInformationFacadeService;
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceInformationReturnDomain result  = null;
        try {
            form.setUserLogin(super.setUserLogin());
            form.setSessionId(request.getSession().getId());
            this.initialCombobox(form, true, false);
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV001_BDOWNLOAD)) 
                {
                    this.invoiceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV001_BDOWNLOAD, locale);
                }
                form.setCannotDownloadMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotDownloadMessage( ae.getMessage() );
            }
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV001_BRESET)) 
                {
                    this.invoiceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV001_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }
            form.setCountCancelInvoice(Constants.STR_ZERO);
            form.setStrConfirm(MessageUtil.getApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_W6_0001 , 
                new String[]{MessageUtil.getLabelHandledException(locale, 
                    SupplierPortalConstant.LBL_CANCEL_INVOICE_LOWER)}));
        } catch (ApplicationException applicationException) {
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, applicationException.getMessage()));
            this.setMessageOnScreen(form, result);
        } catch (Exception e) {
            result = new InvoiceInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
            this.setMessageOnScreen(form, result);
        } finally{
            this.setMessageOnScreen(form, result);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String fristPages = form.getFirstPages();
        String pages = form.getPages();
        
        InvoiceInformationReturnDomain result  = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        if (Constants.PAGE_SEARCH.equals(fristPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV001_BSEARCH)) {
                    this.invoiceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV001_BSEARCH, locale);
                }
                if(Constants.PAGE_SEARCH.equals(fristPages)){
                    DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_INV,
                        new HashMap<String, AsnInformationDomain>());
                    form.setCountCancelInvoice(String.valueOf(Constants.ZERO));
                } else {
                    //Keep selected Invoice when page search or click
                    this.doSetSelectedInvoiceId(form);
                }
                
                form.setSessionId(request.getSession().getId());
                
                result = this.search(form, true);
                if(null == result.getErrorMessageList() 
                    || result.getErrorMessageList().size() <= Constants.ZERO) {
                    form.setInvoiceInformationForDisplayList(
                        result.getInvoiceInformationDomainList());
                    form.setMessageType(Constants.MESSAGE_SUCCESS);
                    
                    Map<String, InvoiceInformationDomain> selectedInvMap = 
                        (HashMap<String, InvoiceInformationDomain>)
                        DensoContext.get().getGeneralArea(Constants.MAP_KEY_SELECTED_INV);
                    for(InvoiceInformationDomain item
                        : result.getInvoiceInformationDomainList()){
                        String invoiceId = String.valueOf(
                            item.getSpsTInvoiceDomain().getInvoiceId());
                        if(selectedInvMap.containsKey(invoiceId)){
                            item.setInvoiceIdSelected(invoiceId);
                        }
                    }
                }
            } catch (ApplicationException ae) {
                result = new InvoiceInformationReturnDomain();
                result.getErrorMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            } catch (Exception e) {
                result = new InvoiceInformationReturnDomain();
                result.setLocale(locale);
                this.setMessageForException(result, e);
            } finally {
                try{
                    this.setMessageOnScreen(form, result);
                    this.initialCombobox(form, true, true);
                    if (!Strings.judgeBlank(form.getDCd())) {
                        List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                        form.setPlantDensoList(plantDensoList);
                    }
                }catch (ApplicationException ae) {
                    form.getApplicationMessageList().add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, ae.getMessage()));
                }
            }
        }
    
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download.
     * <p>Download data by criteria from screen.</p>
     * <ul>
     * <li>In case click download button, system search data and count by criteria.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceInformationReturnDomain result  = null;
        ServletOutputStream outputStream = null;
        
        try {
            UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            
            InvoiceInformationDomain invoiceInformationDomain = this.setCriteria(form);
            result = this.invoiceInformationFacadeService.searchInvoiceInformationCsv(
                invoiceInformationDomain);
            
            byte[] output = result.getCsvResult().toString().getBytes();
            super.setHttpHeaderForCsv(response, result.getFileName());
            response.setContentLength(output.length);
            outputStream = response.getOutputStream();
            outputStream.write(output, 0, output.length);
            outputStream.flush();
            
            return null;
        } catch (ApplicationException ae) {
            
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
        } finally {
            try{
                this.setMessageOnScreen(form, result);
                this.initialCombobox(form, true, true);
                if (!Strings.judgeBlank(form.getDCd())){
                    List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                    form.setPlantDensoList(plantDensoList);
                }
            }catch (ApplicationException ae) {
                form.getApplicationMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            }
            if(null != outputStream) {
                outputStream.close();
            }
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do cancel.
     * <p>Setting invoice status to be cancel status by click Cancel link from screen.</p>
     * <ul>
     * <li>In case click Cancel link, system set invoice status to be cancel status and search data 
     * by criteria if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doCancel(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceInformationReturnDomain result  = null;
        
        List<InvoiceInformationDomain> invoiceInformationList
            = new ArrayList<InvoiceInformationDomain>();
        InvoiceInformationReturnDomain invoiceInformationDomain
            = new InvoiceInformationReturnDomain();
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setUserLogin(super.setUserLogin());
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV001_LCANCELINVOICE)) 
            {
                this.invoiceInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV001_LCANCELINVOICE, locale);
            }
            
            this.doSetSelectedInvoiceId(form);
            Map<String, InvoiceInformationDomain> selectedInvMap
                = (HashMap<String, InvoiceInformationDomain>)DensoContext.get().getGeneralArea(
                    Constants.MAP_KEY_SELECTED_INV);
            
            if(null != selectedInvMap && !selectedInvMap.isEmpty()){
                
                Iterator<String> iterator = selectedInvMap.keySet().iterator();
                while(iterator.hasNext()){
                    String keyStr = (String)iterator.next();
                    invoiceInformationList.add(selectedInvMap.get(keyStr));
                }
                
                if(Constants.ZERO < invoiceInformationList.size()){
                    invoiceInformationDomain.setInvoiceInformationDomainList(
                        invoiceInformationList);
                    invoiceInformationDomain.setDscId(userLoginDomain.getDscId());
                    invoiceInformationDomain.setLocale(locale);
                    
                    boolean isSuccess = this.invoiceInformationFacadeService.transactCancelInvoice(
                        invoiceInformationDomain);
                    if(isSuccess){
                        boolean isSuccessSendMail
                            = this.invoiceInformationFacadeService.searchSendingEmail(
                                invoiceInformationDomain);
                        
                        String message = null;
                        if (isSuccessSendMail) {
                            message = MessageUtil.getApplicationMessageWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_I6_0022,
                                SupplierPortalConstant.LBL_CANCEL_INVOICE);
                        } else {
                            StringBuffer paremeter = new StringBuffer();
                            paremeter.append(MessageUtil.getLabelHandledException(
                                locale, SupplierPortalConstant.LBL_INVOICE_NO));
                            paremeter.append(Constants.SYMBOL_SPACE);
                            paremeter.append(invoiceInformationDomain.getInvNoErrorResult());
                            message = MessageUtil.getApplicationMessageHandledException(locale,
                                SupplierPortalConstant.ERROR_CD_SP_I6_0024,
                                new String[]{MessageUtil.getLabelHandledException(locale,
                                    SupplierPortalConstant.LBL_CANCEL_INVOICE),
                                    paremeter.toString()});
                        }
                        
                        form.setPages(Constants.PAGE_SEARCH);
                        result = this.searchData(form, userLoginDomain);
                        result.getSuccessMessageList().add( new ApplicationMessageDomain(
                            Constants.MESSAGE_SUCCESS, message));
                        
                        //Clear selected Invoice
                        DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_INV, 
                            new HashMap<String, InvoiceInformationDomain>());
                        form.setCountCancelInvoice(Constants.STR_ZERO);
                    }else{
                        applicationMessageList.addAll(
                            invoiceInformationDomain.getErrorMessageList());
                    }
                }
                
//                InvoiceInformationDomain invoiceInformationDomain 
//                    = new InvoiceInformationDomain();
//                index = Integer.parseInt(form.getStatusIndex());
//                invoiceInformationDomain.setLocale(locale);
//                invoiceInformationDomain.getSpsTInvoiceDomain().setInvoiceId(form
//                    .getInvoiceInformationForDisplayList().get(index).getSpsTInvoiceDomain()
//                        .getInvoiceId());
//                invoiceInformationDomain.setInvoiceRcvStatus(
//                    form.getInvoiceInformationForDisplayList().get(index).getInvoiceRcvStatus());
//                invoiceInformationDomain.getSpsTInvoiceDomain().setInvoiceStatus(form
//                    .getInvoiceInformationForDisplayList().get(index).getSpsTInvoiceDomain()
//                        .getInvoiceStatus());
//                invoiceInformationDomain.getSpsTInvoiceDomain().setLastUpdateDatetime(form
//                    .getInvoiceInformationForDisplayList().get(index).getSpsTInvoiceDomain()
//                        .getLastUpdateDatetime());
//                invoiceInformationDomain.getSpsTInvoiceDomain().setLastUpdateDscId(
//                    userLoginDomain.getDscId());
            }
        }catch (ApplicationException ae) {
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        }catch(Exception ex){
            result = new InvoiceInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, ex);
        }finally {
            if(Constants.ZERO < applicationMessageList.size()){
                form.setApplicationMessageList(applicationMessageList);
            }
            try{
                this.setMessageOnScreen(form, result);
                this.initialCombobox(form, true, true);
                if (!Strings.judgeBlank(form.getDCd())){
                    List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                    form.setPlantDensoList(plantDensoList);
                }
            }catch (ApplicationException ae) {
                form.getApplicationMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do forward page.
     * <p>Forwarding to target page.</p>
     * <ul>
     * <li>In case click Invoice no link, system forwarding to target page and send invoice no 
     * for initial screen.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doForwardPage(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain sessionUserLoginDomain = (UserLoginDomain)DensoContext.get().
            getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        int statusIndex = Integer.parseInt(form.getStatusIndex());
        
        StringBuffer forward = null;
        InvoiceInformationReturnDomain result  = null;
        
        try{
            if (!sessionUserLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV001_LINVOICENO)) 
            {
                this.invoiceInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV001_LINVOICENO, locale);
            }
            
            form.setUserLogin(super.setUserLogin());
            
            Winv001Form sessionForm = new Winv001Form();
            this.setCriteriaOnScreen(form, sessionForm);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WINV001_FORM,
                sessionForm);
            
            forward = new StringBuffer();
            forward.append(SupplierPortalConstant.URL_WINV004_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID)
                .append(sessionUserLoginDomain.getDscId());
            forward.append(SupplierPortalConstant.URL_PARAM_INVOICE_ID)
                .append(form.getInvoiceInformationForDisplayList()
                    .get(statusIndex).getSpsTInvoiceDomain().getInvoiceId());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                .append(SupplierPortalConstant.SCREEN_ID_WINV001);
            
            return new ActionForward(forward.toString());
        }catch (ApplicationException ae) {
            result = new InvoiceInformationReturnDomain();
            result = this.searchData(form, sessionUserLoginDomain);
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        }catch(Exception ex){
            result = new InvoiceInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, ex);
        }finally {
            try{
                this.setMessageOnScreen(form, result);
                this.initialCombobox(form, true, true);
                if (!Strings.judgeBlank(form.getDCd())){
                    List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                    form.setPlantDensoList(plantDensoList);
                }
            }catch (ApplicationException ae) {
                form.getApplicationMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            }
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do return.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Winv001Form sessionForm = (Winv001Form)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WINV001_FORM);
        
        InvoiceInformationReturnDomain result  = null;
        
        try {
            form.setUserLogin(super.setUserLogin());
            form.setSessionId(request.getSession().getId());
            
            this.setCriteriaOnScreen(sessionForm, form);
            result = this.search(form, true);
            form.setInvoiceInformationForDisplayList(result.getInvoiceInformationDomainList());
            
            //Clear Selected CheckBox
            DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_INV, 
                new HashMap<String, InvoiceInformationDomain>());
            form.setCountCancelInvoice(String.valueOf(Constants.ZERO));
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV001_BDOWNLOAD)) 
                {
                    this.invoiceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV001_BDOWNLOAD, locale);
                }
                form.setCannotDownloadMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotDownloadMessage( ae.getMessage() );
            }
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV001_BRESET)) 
                {
                    this.invoiceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV001_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }
            
            form.setStrConfirm(MessageUtil.getApplicationMessage(locale, 
                SupplierPortalConstant.ERROR_CD_SP_W6_0001 , 
                new String[]{MessageUtil.getLabelHandledException(locale, 
                    SupplierPortalConstant.LBL_CANCEL_INVOICE_LOWER)}));
            
        } catch (ApplicationException ae) {
            
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        } catch (Exception e) {
            
            result = new InvoiceInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        } finally{
            
            try{
                this.setMessageOnScreen(form, result);
                this.initialCombobox(form, true, true);
                if (!Strings.judgeBlank(form.getDCd())){
                    List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                    form.setPlantDensoList(plantDensoList);
                }
            }catch (ApplicationException ae) {
                form.getApplicationMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            }
        }
        
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do download file management.
     * <p>Download file by click File link from screen.</p>
     * <ul>
     * <li>In case click File link, system search file and show Download dialog box.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadPdf(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceInformationReturnDomain result  = null;
        OutputStream outputStream = null;
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV001_LCOVERPAGE)) 
            {
                form.setFirstPages(null);
                form.setPages(Constants.PAGE_CLICK);
                result = this.search(form, true);
                form.setInvoiceInformationForDisplayList(result.getInvoiceInformationDomainList());
                this.invoiceInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV001_LCOVERPAGE, locale);
            }
            
            InvoiceInformationDomain invoiceInformationDomain = new InvoiceInformationDomain();
            invoiceInformationDomain.setInvoiceIdSelected(form.getCoverPageFileId());
            invoiceInformationDomain.setLocale(locale);
            
            FileManagementDomain fileManagementDomain  = this.invoiceInformationFacadeService.searchGenerateInvoice(
                invoiceInformationDomain);

            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());
//            super.setHttpHeaderForFileManagement(response, request, fileName);
//            outputStream = response.getOutputStream();
//            this.invoiceInformationFacadeService.searchInvoiceCoverPage(invoiceInformationDomain, 
//                outputStream);
//            outputStream.flush();
            form.setMessageType(Constants.MESSAGE_SUCCESS);
            return null;
        } catch (IOException ie) {
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0009)));
        }catch (ApplicationException ae) {
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
            this.initialCombobox(form, false, false);
//            this.setMessageOnScreen(form, result);
        } catch (Exception e) {
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
            form.setMax(Constants.ZERO);
//            this.setMessageOnScreen(form, result);
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
            this.setMessageOnScreen(form, result);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download legend file.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadLegendFile (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceInformationDomain invoiceInformationDomain = null;
        InvoiceInformationReturnDomain result = null;
        String fileId = null;
        String pdfFileName = null;
        OutputStream outputStream = null;
        
        try{
            UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            
            fileId = ContextParams.getLegendFileId();
            invoiceInformationDomain = new InvoiceInformationDomain();
            invoiceInformationDomain.setFileId(fileId);
            invoiceInformationDomain.setLocale(locale);
            
            FileManagementDomain fileManagementDomain =
                this.invoiceInformationFacadeService.searchFileName(invoiceInformationDomain);
            pdfFileName = fileManagementDomain.getFileName();
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            outputStream = response.getOutputStream();
            this.invoiceInformationFacadeService.searchLegendInfo(
                invoiceInformationDomain, outputStream);
            outputStream.flush();
            form.setMessageType(Constants.MESSAGE_SUCCESS);
            return null;
        }catch (IOException ie) {
            
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getLabelHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_80_0009)));
        }catch (ApplicationException ae) {
            
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
            this.initialCombobox(form, false, false);
            this.setMessageOnScreen(form, result);
        } catch (Exception e) {
            
            result = new InvoiceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
            this.setMessageOnScreen(form, result);
        }finally{
            
            if(null != outputStream){
                outputStream.close();
            }
            this.setMessageOnScreen(form, result);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * <p>Denso company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when company combo box change load new denso plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantDensoDomain> plantDensoList = null;
        try{
            
            plantDensoList = this.getPlantDenso(form);
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        }catch(ApplicationException ae){
            
            printJson(response, null, ae);
        }catch(Exception e){
            
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        
        return null;
    }
    
    /**
     * <p>Denso plant code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when plant combo box change load new denso company code
     * for relate plant selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv001Form form = (Winv001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        DataScopeControlDomain dataScopeControlDomain = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.setLocale(locale);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        if(Constants.MISC_CODE_ALL.equals(form.getDPcd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(
                Constants.EMPTY_STRING);
        }else{
            plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(form.getDPcd());
        }
        
        List<CompanyDensoDomain> companyDensoList = null;
        try{
            companyDensoList = this.invoiceInformationFacadeService.searchSelectedPlantDenso(
                plantDensoWithScopeDomain);
            
            printJson(response, new JsonResult((List<Object>)(Object)companyDensoList), null);
        }catch(ApplicationException ae){
            printJson(response, null, ae);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH, 
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_CANCEL, 
            SupplierPortalConstant.METHOD_DO_CANCEL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN, 
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_FORWARD_PAGE, 
            SupplierPortalConstant.METHOD_DO_FORWARD_PAGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_LEGEND_FILE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        return keyMethodMap;
    }
    
    /**
     * Search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * </ul>
     * @param form the WINV001 form
     * @param isSearch the search flag 
     * @return list of invoice information domain
     * @throws ApplicationException the application exception
     */
    private InvoiceInformationReturnDomain search(Winv001Form form, boolean isSearch) 
        throws ApplicationException{
        
        InvoiceInformationReturnDomain result = null;
        List<InvoiceInformationDomain> invoiceInformationDomainList = null;
        int rowNumFrom = Constants.ZERO;
        int rowNumTo = Constants.ZERO;
        
        InvoiceInformationDomain invoiceInformationDomain = this.setCriteria(form);
        result = this.invoiceInformationFacadeService.searchInvoiceInformation(
            invoiceInformationDomain);
        if(null == result.getErrorMessageList() 
            || result.getErrorMessageList().size() <= Constants.ZERO){
            
            SpsPagingUtil.setFormForPaging(invoiceInformationDomain, form);
            rowNumTo = invoiceInformationDomain.getRowNumTo();
            
            invoiceInformationDomainList = new ArrayList<InvoiceInformationDomain>();
            for(rowNumFrom = invoiceInformationDomain.getRowNumFrom() - Constants.ONE; 
                rowNumFrom < rowNumTo; rowNumFrom++){
                
                invoiceInformationDomainList.add(result.getInvoiceInformationDomainList()
                    .get(rowNumFrom));
            }
            result.setInvoiceInformationDomainList(invoiceInformationDomainList);
        }
        
        return result;
    }
    
    /**
     * setCriteria.
     * <p>Setting criteria for search.</p>
     * <ul>
     * </ul>
     * @param form the WINV001 form
     * @return Invoice information domain
     */
    private InvoiceInformationDomain setCriteria(Winv001Form form) {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceInformationDomain invoiceInformationDomain = new InvoiceInformationDomain();
        
        if(!Strings.judgeBlank(form.getInvoiceDateFrom())){
            invoiceInformationDomain.setInvoiceDateFrom(form.getInvoiceDateFrom());
        }
        if(!Strings.judgeBlank(form.getInvoiceDateTo())){
            invoiceInformationDomain.setInvoiceDateTo(form.getInvoiceDateTo());
        }
        if(!Strings.judgeBlank(form.getCnDateFrom())){
            invoiceInformationDomain.setCnDateFrom(form.getCnDateFrom());
        }
        if(!Strings.judgeBlank(form.getCnDateTo())){
            invoiceInformationDomain.setCnDateTo(form.getCnDateTo());
        }
        if(!Strings.judgeBlank(form.getInvoiceRcvStatus())){
            invoiceInformationDomain.setInvoiceRcvStatus(form.getInvoiceRcvStatus());
        }
        if(!Strings.judgeBlank(form.getVendorCd())){
            invoiceInformationDomain.setVendorCd(form.getVendorCd());
        }
        if(!Strings.judgeBlank(form.getDCd())){
            invoiceInformationDomain.setDCd(form.getDCd());
        }
        if(!Strings.judgeBlank(form.getDPcd())){
            invoiceInformationDomain.setDPcd(form.getDPcd());
        }
        if(!Strings.judgeBlank(form.getInvoiceStatus())){
            invoiceInformationDomain.getSpsTInvoiceDomain().setInvoiceStatus(
                form.getInvoiceStatus());
        }
        if(!Strings.judgeBlank(form.getInvoiceNo())){
            invoiceInformationDomain.getSpsTInvoiceDomain().setInvoiceNo(
                form.getInvoiceNo().trim().toUpperCase());
        }
        if(!Strings.judgeBlank(form.getCnNo())){
            invoiceInformationDomain.getSpsTCnDomain().setCnNo(
                form.getCnNo().trim().toUpperCase());
        }
        
        invoiceInformationDomain.setSupplierAuthenList(form.getSupplierAuthenList());
        invoiceInformationDomain.setDensoAuthenList(form.getDensoAuthenList());
        
        if(Constants.PAGE_SEARCH.equals(form.getFirstPages())){
            invoiceInformationDomain.setPageNumber(Constants.ONE);
        }
        else{
            invoiceInformationDomain.setPageNumber(form.getPageNo());
        }
        invoiceInformationDomain.setLocale(locale);
        
        return invoiceInformationDomain;
    }
    
    /**
     * Get plant denso.
     * <p>Get Plant Denso Data for initial combobox.</p>
     * 
     * @param form the Winv001Form form
     * @return the RoleScreenDomain
     * @throws ApplicationException ApplicationException
     */
    private List<PlantDensoDomain> getPlantDenso(Winv001Form form)throws ApplicationException {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        DataScopeControlDomain dataScopeControlDomain = null;
        List<PlantDensoDomain> plantDensoList = null;
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.setLocale(locale);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        if(Constants.MISC_CODE_ALL.equals(form.getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                Constants.EMPTY_STRING);
        }else{
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
        }
        
        try{
            plantDensoList = this.invoiceInformationFacadeService.searchSelectedCompanyDenso(
                plantDensoWithScopeDomain);
        }catch(ApplicationException ae){
            throw ae;
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        return plantDensoList;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation(){
        
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for Invoice Information Screen.
     * <p>Get roles from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain getRoleScreen(UserLoginDomain userLoginDomain) {
        
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WINV001.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * initialCombobox.
     * <p>Initial combo box from screen.</p>
     * 
     * @param form the WINV002 form
     * @param isInitial the boolean
     * @param isSearch the boolean
     * @throws ApplicationException the application exception
     * @throws Exception the exception
     */
    private void initialCombobox(Winv001Form form, boolean isInitial, boolean isSearch) 
        throws ApplicationException, Exception {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceInformationDomain invoiceInformationDomain = new InvoiceInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        InvoiceInformationReturnDomain invoiceInformationReturnDomain = null;
        
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        try{
            if(isInitial){
                invoiceInformationReturnDomain = this.invoiceInformationFacadeService.searchInitial(
                    dataScopeControlDomain, invoiceInformationDomain);
                if(!isSearch){
                    form.resetForm();
                    Calendar firstDay = Calendar.getInstance();
                    firstDay.set(Calendar.DAY_OF_MONTH, Constants.ONE);
                    form.setInvoiceDateFrom(DateUtil.format(firstDay.getTime(), 
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
                    Calendar lastDay = Calendar.getInstance();
                    lastDay.set(Calendar.DAY_OF_MONTH, 
                        lastDay.getActualMaximum(Calendar.DAY_OF_MONTH));
                    form.setInvoiceDateTo(DateUtil.format(lastDay.getTime(), 
                        DateUtil.PATTERN_YYYYMMDD_SLASH));
                }
                form.setCompanySupplierList(
                    invoiceInformationReturnDomain.getCompanySupplierList());
                form.setCompanyDensoList(invoiceInformationReturnDomain.getCompanyDensoList());
            }else{
                invoiceInformationDomain.setDCd(form.getDCd());
                invoiceInformationDomain.setVendorCd(form.getVendorCd());
                invoiceInformationReturnDomain = this.invoiceInformationFacadeService.searchInitial(
                    dataScopeControlDomain, invoiceInformationDomain);
            }
            
            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV001));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV001));
            
            form.setPlantDensoList(invoiceInformationReturnDomain.getPlantDensoList());
            form.setInvoiceStatusList(invoiceInformationReturnDomain.getInvoiceStatusList());
            form.setInvoiceRcvStatusList(invoiceInformationReturnDomain.getInvoiceRcvStatusList());
        }catch(ApplicationException ae){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
    }
    
    /**
     * setCriteriaOnScreen.
     * <p>Setting data between action form and  session form.</p>
     * 
     * @param sourceForm the Winv001Form
     * @param targetForm the Winv001Form
     */
    private void setCriteriaOnScreen(Winv001Form sourceForm, Winv001Form targetForm){
        
        if(!Strings.judgeBlank(sourceForm.getInvoiceDateFrom())){
            targetForm.setInvoiceDateFrom(sourceForm.getInvoiceDateFrom());
        }
        if(!Strings.judgeBlank(sourceForm.getInvoiceDateTo())){
            targetForm.setInvoiceDateTo(sourceForm.getInvoiceDateTo());
        }
        if(!Strings.judgeBlank(sourceForm.getCnDateFrom())){
            targetForm.setCnDateFrom(sourceForm.getCnDateFrom());
        }
        if(!Strings.judgeBlank(sourceForm.getCnDateTo())){
            targetForm.setCnDateTo(sourceForm.getCnDateTo());
        }
        if(!Strings.judgeBlank(sourceForm.getInvoiceRcvStatus())){
            targetForm.setInvoiceRcvStatus(sourceForm.getInvoiceRcvStatus());
        }
        if(!Strings.judgeBlank(sourceForm.getVendorCd())){
            targetForm.setVendorCd(sourceForm.getVendorCd());
        }
        if(!Strings.judgeBlank(sourceForm.getDCd())){
            targetForm.setDCd(sourceForm.getDCd());
        }
        if(!Strings.judgeBlank(sourceForm.getDPcd())){
            targetForm.setDPcd(sourceForm.getDPcd());
        }
        if(!Strings.judgeBlank(sourceForm.getInvoiceStatus())){
            targetForm.setInvoiceStatus(sourceForm.getInvoiceStatus());
        }
        if(!Strings.judgeBlank(sourceForm.getInvoiceNo())){
            targetForm.setInvoiceNo(sourceForm.getInvoiceNo());
        }
        
        targetForm.setSupplierAuthenList(sourceForm.getSupplierAuthenList());
        targetForm.setDensoAuthenList(sourceForm.getDensoAuthenList());
        
        targetForm.setPageNo(Constants.ONE);
        targetForm.setPages(Constants.PAGE_CLICK);
    }
    
    /**
     * searchData.
     * <p>Search data for show on screen</p>
     * 
     * @param form the WINV001 form
     * @param userLoginDomain the User Login Domain
     * @return the Invoice Information Return Domain
     * @throws ApplicationException the ApplicationException
     */
    private InvoiceInformationReturnDomain searchData(Winv001Form form,
        UserLoginDomain userLoginDomain) throws ApplicationException {
        
        InvoiceInformationReturnDomain result;
        result = this.search(form, true);
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        userLoginDomain = this.getCurrentUserLoginInformation();
        form.setInvoiceInformationForDisplayList(
            result.getInvoiceInformationDomainList());
        return result;
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param result the ASN Information Return Domain
     * @param e the Exception
     */
    private void setMessageForException(InvoiceInformationReturnDomain result, Exception e){
        try {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002, 
                    new String[]{e.getClass().toString(), e.getMessage()})));
        } catch (Exception exception) {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()})));
        }
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param e the Exception
     * @return the String
     */
    private ApplicationMessageDomain getMessageForException(Exception e){
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        ApplicationMessageDomain applicationMessage = null;
        
        try {
            applicationMessage = new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()}));
        } catch (Exception exception) {
            applicationMessage = new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()}));
        }
        return applicationMessage;
    }
    
    /**
     * setMessageOnScreen.
     * <p>Setting message for appearance on screen.</p>
     * 
     * @param form the WINV001 form
     * @param result the ASN Information Return Domain
     */
    private void setMessageOnScreen(Winv001Form form, InvoiceInformationReturnDomain result) {
        
        if(null != result){
            List<ApplicationMessageDomain> applicationMessageList;
            if(null != result.getErrorMessageList() 
                && Constants.ZERO < result.getErrorMessageList().size()){
                applicationMessageList = new ArrayList<ApplicationMessageDomain>();
                applicationMessageList.addAll(result.getErrorMessageList());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    applicationMessageList.addAll(result.getWarningMessageList());
                }
                
                form.setApplicationMessageList(applicationMessageList);
            }else if(null != result.getSuccessMessageList() 
                && Constants.ZERO < result.getSuccessMessageList().size()){
                
                form.setApplicationMessageList(result.getSuccessMessageList());
            }
        }
    }
    
    /**
     * Do set selected invoice id
     * @param form the winv001 form
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    private void doSetSelectedInvoiceId(Winv001Form form) throws Exception
    {
        List<InvoiceInformationDomain> itemList = form.getInvoiceInformationForDisplayList();
        Map<String, InvoiceInformationDomain> selectedInvMap 
            = (HashMap<String, InvoiceInformationDomain>)
                DensoContext.get().getGeneralArea(Constants.MAP_KEY_SELECTED_INV);
        for(InvoiceInformationDomain item : itemList)
        {
            String invoiceId = String.valueOf(item.getSpsTInvoiceDomain().getInvoiceId());
            if(!StringUtil.checkNullOrEmpty(item.getInvoiceIdSelected())){
                if(!selectedInvMap.containsKey(invoiceId)){
                    selectedInvMap.put(invoiceId, item);
                }
            }else{
                if(selectedInvMap.containsKey(invoiceId)){
                    selectedInvMap.remove(invoiceId);
                }
            }
        }
    }
}
