/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/28 CSI Parichat       Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.InquiryDoDetailFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp001Form;
import com.globaldenso.asia.sps.presentation.form.Wshp003Form;
import com.globaldenso.asia.sps.presentation.form.Wshp007Form;
import com.globaldenso.asia.sps.presentation.form.Wshp008Form;

/**
 * The Class WSHP001Action.
 * @author CSI
 */
public class InquiryDoDetailAction extends CoreAction {
    
    /** The inquiry do detail facade service. */
    private InquiryDoDetailFacadeService inquiryDoDetailFacadeService;

    /** The default constructor. */
    public InquiryDoDetailAction() {
        super();
    }

    /**
     * Set the inquiry do detail facade service.
     * 
     * @param inquiryDoDetailFacadeService the inquiry do detail facade service to set
     */
    public void setInquiryDoDetailFacadeService(
        InquiryDoDetailFacadeService inquiryDoDetailFacadeService) {
        this.inquiryDoDetailFacadeService = inquiryDoDetailFacadeService;
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        Wshp003Form form = (Wshp003Form)actionForm;
        InquiryDoDetailDomain inquiryDoDetailDomain = null;
        List<ApplicationMessageDomain> appMsgList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPage = form.getFirstPages();
        String page = form.getPages();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        if(SupplierPortalConstant.SCREEN_ID_WSHP001.equals(form.getScreenId())){
            Wshp001Form sessionForm = (Wshp001Form)DensoContext.get().getGeneralArea(
                SupplierPortalConstant.SESSION_WSHP001_FORM);
            form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
            form.setDensoAuthenList(sessionForm.getDensoAuthenList());
        }else if(SupplierPortalConstant.SCREEN_ID_WSHP007.equals(form.getScreenId())){
            Wshp007Form sessionForm = (Wshp007Form)DensoContext.get().getGeneralArea(
                SupplierPortalConstant.SESSION_WSHP007_FORM);
            form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
            form.setDensoAuthenList(sessionForm.getDensoAuthenList());
        }else if(SupplierPortalConstant.SCREEN_ID_WSHP008.equals(form.getScreenId())){
            Wshp008Form sessionForm = (Wshp008Form)DensoContext.get().getGeneralArea(
                SupplierPortalConstant.SESSION_WSHP008_FORM);
            form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
            form.setDensoAuthenList(sessionForm.getDensoAuthenList());
        }
        
        if(StringUtil.checkNullOrEmpty(form.getMode())){
            form.setFirstPages(Constants.PAGE_SEARCH);
            firstPage = Constants.PAGE_SEARCH;
        }
        try{
            if(Constants.PAGE_SEARCH.equals(firstPage) || Constants.PAGE_CLICK.equals(page)){
                inquiryDoDetailDomain = new InquiryDoDetailDomain();
                
                if(Constants.PAGE_SEARCH.equals(firstPage)){
                    inquiryDoDetailDomain.setPageNumber(Constants.ONE);
                }else{
                    inquiryDoDetailDomain.setPageNumber(form.getPageNo());
                }
                inquiryDoDetailDomain.setLocale(locale);
                inquiryDoDetailDomain.setDoId(form.getDoId());
                inquiryDoDetailDomain.setSupplierAuthenList(form.getSupplierAuthenList());
                inquiryDoDetailDomain.setDensoAuthenList(form.getDensoAuthenList());
                inquiryDoDetailFacadeService.searchInitial(inquiryDoDetailDomain);
                if(Constants.ZERO < inquiryDoDetailDomain.getInquiryDoDetailReturnList().size()){
                    form.setInquiryDoDetailList(
                        inquiryDoDetailDomain.getInquiryDoDetailReturnList());
                    form.setDisplayResultFlag(String.valueOf(Constants.ZERO));
                    form.setMode(Constants.MODE_INITIAL);
                    SpsPagingUtil.setFormForPaging(inquiryDoDetailDomain, form);
                }else{
                    form.setDisplayResultFlag(String.valueOf(Constants.ONE));
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E6_0001, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
            }
        }catch(ApplicationException e){
            appMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            appMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(Constants.ZERO < appMsgList.size()){
                form.setApplicationMessageList(appMsgList);
                form.setDisplayResultFlag(String.valueOf(Constants.ONE));
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do click link more.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp003Form form = (Wshp003Form)actionForm;
        OutputStream outputStream = null;
        InquiryDoDetailDomain inquiryDoDetailDomain = new InquiryDoDetailDomain();
        List<ApplicationMessageDomain> appMsgList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            String legendFileId = ContextParams.getLegendFileId();
            inquiryDoDetailDomain.setPdfFileId(legendFileId);
            
            FileManagementDomain fileManagementDomain 
                = inquiryDoDetailFacadeService.searchFileName(inquiryDoDetailDomain);
            String pdfFileName = fileManagementDomain.getFileName();
            
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            outputStream = response.getOutputStream();
            inquiryDoDetailFacadeService.searchLegendInfo(inquiryDoDetailDomain, outputStream);
            outputStream.flush();
            return null;
        }catch(ApplicationException e){
            appMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            appMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.getApplicationMessageList().addAll(appMsgList);
            if(null != outputStream){
                outputStream.close();
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Return.
     * <p>Return to previous screen.</p>
     * <ul>
     * <li>In case click 'Return' button, the system will return to parent screen.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp003Form form = (Wshp003Form)actionForm;
        StringBuffer forward = new StringBuffer();
        
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP003_BRETURN)){
                this.inquiryDoDetailFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP003_BRETURN, locale);
            }
            if(SupplierPortalConstant.SCREEN_ID_WSHP001.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP001_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_INITIAL_RETURN);
                forward.append(SupplierPortalConstant.URL_PARAM_MODE);
                forward.append(Constants.STR_RETURN);
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP008.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP008_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA);
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP007.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP007_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_RETURN);
            }
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
        form.setApplicationMessageList(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap()
    {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE, 
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        return keyMethodMap;
    }
    

    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return userLoginDomain the user login domain
     */
    private UserLoginDomain getUserLoginInformation()
    {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
}
