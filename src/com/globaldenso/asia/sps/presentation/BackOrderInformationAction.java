/*
 * ModifyDate Developmentcompany Describe 
 * 2014/08/25 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.BackOrderInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp007Form;

/**
 * The Class BackOrderInformationAction.
 * @author CSI
 */
public class BackOrderInformationAction extends CoreAction {

    /** The back order information facade service. */
    private BackOrderInformationFacadeService backOrderInformationFacadeService = null;
    
    
    /** The default constructor. */
    public BackOrderInformationAction() {
        super();
    }

    /**
     * Set the back order information facade service.
     * 
     * @param backOrderInformationFacadeService the back order information facade service to set
     */
    public void setBackOrderInformationFacadeService(
        BackOrderInformationFacadeService backOrderInformationFacadeService) {
        this.backOrderInformationFacadeService = backOrderInformationFacadeService;
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        BackOrderInformationReturnDomain result  = null;
        try{
            form.setUserLogin(super.setUserLogin());
            this.initialCombobox(form);
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP007_BRESET)){
                    this.backOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP007_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP007_BDOWNLOAD)){
                    this.backOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP007_BDOWNLOAD, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotDownloadMessage(ex.getMessage());
            }
        }catch(ApplicationException applicationException){
            result = new BackOrderInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, applicationException.getMessage()));
        }catch(Exception e){
            result = new BackOrderInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }finally{
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        
        BackOrderInformationReturnDomain result  = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP007_BSEARCH)){
                    this.backOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP007_BSEARCH, locale);
                }
                
                result = this.searchBackOrder(form);
                if(null == result.getErrorMessageList() 
                    || result.getErrorMessageList().size() <= Constants.ZERO){
                    form.setBackOrderInformationList(result.getBackOrderInformationList());
                }
            } catch (ApplicationException ae) {
                result = new BackOrderInformationReturnDomain();
                result.getErrorMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            } catch (Exception e) {
                result = new BackOrderInformationReturnDomain();
                result.setLocale(locale);
                this.setMessageForException(result, e);
            } finally {
                this.setMessageOnScreen(form, result);
                this.setPlantCombobox(form);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do return.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Wshp007Form sessionForm = (Wshp007Form)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WSHP007_FORM);
        
        BackOrderInformationReturnDomain result  = null;
        try {
            this.initialCombobox(form);
            this.setPlantCombobox(form);
            form.setSessionId(request.getSession().getId());
            form.setUserLogin(super.setUserLogin());
            form.setCannotResetMessage(sessionForm.getCannotResetMessage());
            form.setCannotDownloadMessage(sessionForm.getCannotDownloadMessage());
            this.setCriteriaOnScreen(sessionForm, form);
            result = this.searchBackOrder(form);
            form.setBackOrderInformationList(result.getBackOrderInformationList());
        } catch (ApplicationException ae) {
            result = new BackOrderInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            result = new BackOrderInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        } finally{
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do download.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        BackOrderInformationReturnDomain result = null;
        ServletOutputStream outputStream = null;
        
        try {
            form.setUserLogin(super.setUserLogin());
            
            BackOrderInformationDomain backOrderInformationCriteria = this.setCriteria(form);
            result = this.backOrderInformationFacadeService
                .searchBackOrderInformationCsv(backOrderInformationCriteria);
            
            if(null == result.getErrorMessageList() || Constants.ZERO 
                == result.getErrorMessageList().size()){
            
                byte[] output = result.getCsvResult().toString().getBytes();
                super.setHttpHeaderForCsv(response, result.getFileName());
                response.setContentLength(output.length);
                outputStream = response.getOutputStream();
                outputStream.write(output, 0, output.length);
                outputStream.flush();
            
            }
        } catch (ApplicationException ae) {
            
            result = new BackOrderInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            
            result = new BackOrderInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        } finally {
            if(null != outputStream) {
                outputStream.close();
            }
        }
        if(null != result.getErrorMessageList() 
            && Constants.ZERO < result.getErrorMessageList().size()){
            this.setMessageOnScreen(form, result);
            this.setPlantCombobox(form);
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }else{
            return null;
        }
    }
    
    /**
     * Do forward page.
     * <p>Forwarding to target page.</p>
     * <ul>
     * <li>In case click Invoice no link, system forwarding to target page and send invoice no 
     * for initial screen.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doForwardPage(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wshp007Form form = (Wshp007Form)actionForm;
        StringBuffer forward = new StringBuffer();
        BackOrderInformationReturnDomain result  = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        Wshp007Form sessionForm = new Wshp007Form();
        this.setCriteriaOnScreen(form, sessionForm);
        DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP007_FORM,
            sessionForm);
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(this.setUserLogin());
        
        int statusIndex = Integer.parseInt(form.getStatusIndex());
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP007_LSPSDONO)){
                this.backOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP007_LSPSDONO, locale);
            }
            forward.append(SupplierPortalConstant.URL_WSHP003_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID)
                .append(userLoginDomain.getDscId());
            forward.append(SupplierPortalConstant.URL_PARAM_DOID)
                .append(form.getBackOrderInformationList()
                    .get(statusIndex).getSpsTDoDomain().getDoId());
            forward.append(SupplierPortalConstant.URL_PARAM_SPSDONO)
                .append(form.getBackOrderInformationList()
                    .get(statusIndex).getSpsTDoDomain().getSpsDoNo());
            forward.append(SupplierPortalConstant.URL_PARAM_REVISION)
                .append(form.getBackOrderInformationList()
                    .get(statusIndex).getSpsTDoDomain().getRevision());
            forward.append(SupplierPortalConstant.URL_PARAM_ROUTENO)
                .append(form.getBackOrderInformationList()
                    .get(statusIndex).getSpsTDoDomain().getTruckRoute());
            forward.append(SupplierPortalConstant.URL_PARAM_DEL)
                .append(form.getBackOrderInformationList()
                    .get(statusIndex).getSpsTDoDomain().getTruckSeq());
            forward.append(SupplierPortalConstant.URL_PARAM_DCD)
                .append(form.getBackOrderInformationList()
                    .get(statusIndex).getSpsTDoDomain().getDCd());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                .append(SupplierPortalConstant.SCREEN_ID_WSHP007);
            return new ActionForward(forward.toString());
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.setFirstPages(null);
        form.setPages(Constants.PAGE_CLICK);
        result = this.searchBackOrder(form);
        if(null == result.getErrorMessageList() 
            || result.getErrorMessageList().size() <= Constants.ZERO){
            form.setBackOrderInformationList(result.getBackOrderInformationList());
        }
        this.setMessageOnScreen(form, result);
        this.setPlantCombobox(form);
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do select Dcd
     * <p>Denso company code combo box will be changed in criteria area of screen.</p>
     * <ul>
     * <li>System will initial denso plant code combo box for related denso company code
     * when denso company code combo box is changed.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp007Form form = (Wshp007Form)actionForm;
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        try{
            UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLogin);
            dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setUserType(userLogin.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
            if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                plantDensoWithScope.getPlantDensoDomain().setDCd(form.getDCd());
            }
            plantDensoWithScope.setLocale(locale);
            plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = this.backOrderInformationFacadeService.searchSelectedCompanyDenso(
                plantDensoWithScope);
            
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    
    /**
     * Do select Scd
     * <p>Supplier company code combo box will be changed in criteria area of screen.</p>
     * <ul>
     * <li>System will initial supplier plant code combo box for related supplier company code
     * when supplier company code combo box is changed.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp007Form form = (Wshp007Form)actionForm;
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantSupplierDomain> plantSupplierList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        try{
            UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLogin);
            dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setUserType(userLogin.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if(!Constants.MISC_CODE_ALL.equals(form.getVendorCd())){
                plantSupplier.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControlDomain);
            plantSupplierList = this.backOrderInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplier);
            
            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        }catch(ApplicationException ae){
            printJson(response, null, ae);
        }catch(Exception e){
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**
     * Search company DENSO Code by selected plant DENSO Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        try{
            UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLogin);
            dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLogin.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setDataScopeControlDomain(dataScopeControlDomain);
            plantDenso.setLocale(locale);
            companyList = this.backOrderInformationFacadeService
                .searchSelectedPlantDenso(plantDenso);
            
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        try{
            UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLogin);
            dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLogin.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControlDomain);
            companyList = this.backOrderInformationFacadeService
                .searchSelectedPlantSupplier(plantSupplier);
            
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**
     * Do download file management.
     * <p>Download file by click File link from screen.</p>
     * <ul>
     * <li>In case click File link, system search file and show Download dialog box.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadLegendFile(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wshp007Form form = (Wshp007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        OutputStream output = null;
        String message = null;
        BackOrderInformationReturnDomain result = null;
        BackOrderInformationDomain backOrderInformation = null;
        String pdfFileName = null;
        String fileId = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        
        try{
            fileId = ContextParams.getLegendFileId();
            backOrderInformation = new BackOrderInformationDomain();
            backOrderInformation.setFileId(fileId);
            backOrderInformation.setLocale(locale);
            
            FileManagementDomain fileManagementDomain 
                = this.backOrderInformationFacadeService.searchFileName(backOrderInformation);
            pdfFileName = fileManagementDomain.getFileName();
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            output = response.getOutputStream();
            this.backOrderInformationFacadeService.searchLegendInfo(backOrderInformation, output);
            output.flush();
            form.setMessageType(Constants.MESSAGE_SUCCESS);
            
        } catch (IOException ie) {
            
            result = new BackOrderInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getLabelHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_80_0009)));
        }catch (ApplicationException ae) {
            
            result = new BackOrderInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        }catch (Exception e) {
            
            result = new BackOrderInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
            
            message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        }finally {
            if(null != output) {
                output.close();
            }
            form.setApplicationMessage(message);
            form.setApplicationMessageList(errorMessageList);
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH, 
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_FORWARD_PAGE, 
            SupplierPortalConstant.METHOD_DO_FORWARD_PAGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_LEGEND_FILE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);
        return keyMethodMap;
    }
    
    /**
     * initialCombobox.
     * <p>Initial combo box from screen.</p>
     * 
     * @param form the WSHP007 form
     * @throws ApplicationException the application exception
     */
    private void initialCombobox(Wshp007Form form) throws ApplicationException
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        dataScopeControlDomain.setLocale(locale);
       
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        BackOrderInformationReturnDomain backOrderInformationReturn 
            = this.backOrderInformationFacadeService.searchInitial(dataScopeControlDomain);
        
        form.resetForm();
        form.setCompanySupplierList(backOrderInformationReturn.getCompanySupplierList());
        form.setCompanyDensoList(backOrderInformationReturn.getCompanyDensoList());
        form.setPlantSupplierList(backOrderInformationReturn.getPlantSupplierList());
        form.setPlantDensoList(backOrderInformationReturn.getPlantDensoList());
        
        form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
            backOrderInformationReturn.getDensoSupplierRelationList(),
            SupplierPortalConstant.SCREEN_ID_WSHP007));
        form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
            backOrderInformationReturn.getDensoSupplierRelationList(),
            SupplierPortalConstant.SCREEN_ID_WSHP007));
        form.setTransList(backOrderInformationReturn.getTransportModeCbList());
    }
    
    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the Wshp001Form
     * */
    private void setPlantCombobox(Wshp007Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        DataScopeControlDomain dataScopeControlDomain = null;
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLogin);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        if (!Strings.judgeBlank(form.getVendorCd())){
            List<PlantSupplierDomain> plantSupplierList = null;
            try{
                PlantSupplierWithScopeDomain plantSupplierWithScope
                    = new PlantSupplierWithScopeDomain();
                
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplierWithScope.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
                }
                plantSupplierWithScope.setLocale(locale);
                plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
                plantSupplierList = this.backOrderInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplierWithScope);
                form.setPlantSupplierList(plantSupplierList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd())){
            List<PlantDensoDomain> plantDensoList = null;
            try{
                PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
                if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                    plantDensoWithScope.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDensoWithScope.setLocale(locale);
                plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
                plantDensoList = this.backOrderInformationFacadeService
                    .searchSelectedCompanyDenso(plantDensoWithScope);
                form.setPlantDensoList(plantDensoList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
    }
    
    /**
     * Method to get current user login information detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation(){
        
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * </ul>
     * @param form the Back Order form
     * @return list of Back Order Information Return Domain
     * @throws Exception 
     */
    private BackOrderInformationReturnDomain searchBackOrder(Wshp007Form form) throws Exception
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        BackOrderInformationReturnDomain result = null;
        List<BackOrderInformationDomain> backOrderInformationList = null;
        int rowNumFrom = Constants.ZERO;
        int rowNumTo = Constants.ZERO;
        
        BackOrderInformationDomain backOrderInformationCriteria = this.setCriteria(form);
        if(Constants.PAGE_SEARCH.equals(form.getFirstPages())){
            backOrderInformationCriteria.setPageNumber(Constants.ONE);
        }
        else{
            backOrderInformationCriteria.setPageNumber(form.getPageNo());
        }
        
        backOrderInformationCriteria.setLocale(locale);
        result = this.backOrderInformationFacadeService.searchBackOrderInformation(
            backOrderInformationCriteria);
        if(null == result.getErrorMessageList() 
            || result.getErrorMessageList().size() <= Constants.ZERO){
            
            SpsPagingUtil.setFormForPaging(backOrderInformationCriteria, form);
            rowNumTo = backOrderInformationCriteria.getRowNumTo();
            
            backOrderInformationList = new ArrayList<BackOrderInformationDomain>();
            for(rowNumFrom = backOrderInformationCriteria.getRowNumFrom() - 1; 
                rowNumFrom < rowNumTo ; rowNumFrom++){
                backOrderInformationList.add(result.getBackOrderInformationList().get(rowNumFrom));
            }
            result.setBackOrderInformationList(backOrderInformationList);
        }
        return result;
    }
    
    /**
     * setCriteria.
     * <p>Setting criteria for search data.</p>
     * 
     * @param form the Wshp007Form form
     * @return backOrderInformationCriteria the Back Order Information Domain
     */
    private BackOrderInformationDomain setCriteria(Wshp007Form form) {
        
        BackOrderInformationDomain backOrderInformationCriteria = new BackOrderInformationDomain();
        if(!Strings.judgeBlank(form.getDeliveryDateFrom())){
            backOrderInformationCriteria.setDeliveryDateFrom(form.getDeliveryDateFrom());
        }
        if(!Strings.judgeBlank(form.getDeliveryDateTo())){
            backOrderInformationCriteria.setDeliveryDateTo(form.getDeliveryDateTo());
        }
        if(!Strings.judgeBlank(form.getVendorCd())){
            backOrderInformationCriteria.setVendorCd(form.getVendorCd());
        }
        if(!Strings.judgeBlank(form.getSPcd())){
            backOrderInformationCriteria.setSPcd(form.getSPcd());
        }
        if(!Strings.judgeBlank(form.getDCd())){
            backOrderInformationCriteria.setDCd(form.getDCd());
        }
        if(!Strings.judgeBlank(form.getDPcd())){
            backOrderInformationCriteria.setDPcd(form.getDPcd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getSpsDoNo().trim())){
            backOrderInformationCriteria.setSpsDoNo(form.getSpsDoNo().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getAsnNo().trim())){
            backOrderInformationCriteria.setAsnNo(form.getAsnNo().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getSPn().trim())){
            backOrderInformationCriteria.setSPn(form.getSPn().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDPn().trim())){
            backOrderInformationCriteria.setDPn(form.getDPn().toUpperCase().trim());
        }
        if(!Strings.judgeBlank(form.getTransportModeCb())){
            backOrderInformationCriteria.setTransportModeCb(form.getTransportModeCb());
        }
        backOrderInformationCriteria.setSupplierAuthenList(form.getSupplierAuthenList());
        backOrderInformationCriteria.setDensoAuthenList(form.getDensoAuthenList());
        return backOrderInformationCriteria;
    }
    
    /**
     * setCriteria.
     * <p>Setting criteria for search data.</p>
     * 
     * @param sourceForm the Wshp007Form
     * @param targetForm the Wshp007Form
     */
    private void setCriteriaOnScreen(Wshp007Form sourceForm, Wshp007Form targetForm) {
        
        if(!Strings.judgeBlank(sourceForm.getDeliveryDateFrom())){
            targetForm.setDeliveryDateFrom(sourceForm.getDeliveryDateFrom());
        }
        if(!Strings.judgeBlank(sourceForm.getDeliveryDateTo())){
            targetForm.setDeliveryDateTo(sourceForm.getDeliveryDateTo());
        }
        if(!Strings.judgeBlank(sourceForm.getVendorCd())){
            targetForm.setVendorCd(sourceForm.getVendorCd());
        }
        if(!Strings.judgeBlank(sourceForm.getSPcd())){
            targetForm.setSPcd(sourceForm.getSPcd());
        }
        if(!Strings.judgeBlank(sourceForm.getDCd())){
            targetForm.setDCd(sourceForm.getDCd());
        }
        if(!Strings.judgeBlank(sourceForm.getDPcd())){
            targetForm.setDPcd(sourceForm.getDPcd());
        }
        if(!Strings.judgeBlank(sourceForm.getSpsDoNo())){
            targetForm.setSpsDoNo(sourceForm.getSpsDoNo());
        }
        if(!Strings.judgeBlank(sourceForm.getAsnNo())){
            targetForm.setAsnNo(sourceForm.getAsnNo());
        }
        if(!Strings.judgeBlank(sourceForm.getSPn())){
            targetForm.setSPn(sourceForm.getSPn());
        }
        if(!Strings.judgeBlank(sourceForm.getDPn())){
            targetForm.setDPn(sourceForm.getDPn());
        }
        targetForm.setSupplierAuthenList(sourceForm.getSupplierAuthenList());
        targetForm.setDensoAuthenList(sourceForm.getDensoAuthenList());
        targetForm.setCannotResetMessage(sourceForm.getCannotResetMessage());
        targetForm.setCannotDownloadMessage(sourceForm.getCannotDownloadMessage());
        targetForm.setFirstPages(Constants.PAGE_SEARCH);
    }
    
    /**
     * Get roles for Back order Information Screen.
     * <p>Get roles from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain getRoleScreen(UserLoginDomain userLoginDomain) {
        
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WSHP007.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param result the Back Order Information Return Domain
     * @param e the Exception
     */
    private void setMessageForException(BackOrderInformationReturnDomain result, Exception e){
        try {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002, 
                    new String[]{e.getClass().toString(), e.getMessage()})));
        } catch (Exception exception) {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()})));
        }
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param e the Exception
     * @return the String
     */
    private ApplicationMessageDomain getMessageForException(Exception e)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        ApplicationMessageDomain applicationMessage = null;
        
        try{
            applicationMessage = new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()}));
        }catch(Exception exception){
            applicationMessage = new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()}));
        }
        return applicationMessage;
    }
    
    /**
     * setMessageOnScreen.
     * <p>Setting message for appearance on screen.</p>
     * 
     * @param form the WSHP007 form
     * @param result the Back Order Information Return Domain
     */
    private void setMessageOnScreen(Wshp007Form form, BackOrderInformationReturnDomain result)
    {
        if(null != result){
            List<ApplicationMessageDomain> applicationMessageList;
            if(null != result.getErrorMessageList() 
                && Constants.ZERO < result.getErrorMessageList().size()){
                applicationMessageList = new ArrayList<ApplicationMessageDomain>();
                applicationMessageList.addAll(result.getErrorMessageList());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    applicationMessageList.addAll(result.getWarningMessageList());
                }
                form.setApplicationMessageList(applicationMessageList);
            }
        }
    }
}
