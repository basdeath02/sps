/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Word002Form;
import com.globaldenso.asia.sps.business.service.PurchaseOrderInformationFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;

/**
 * The Class PurchaseOrderInformationAction.
 * <p>Receive value from WORD002_PurchaseOrderInformation.jsp for initial value and send to 
 * purchaseOrderInformationFacadeService for search</p>
 * <ul>
 * <li>Method search    : doInitial</li>
 * <li>Method search    : doInitialWithSearch</li>
 * <li>Method search    : doSearch</li>
 * <li>Method update    : doAcknowledge</li>
 * <li>Method search    : doDownload</li>
 * <li>Method search    : doDownloadPdfOriginal</li>
 * <li>Method search    : doDownloadPdfChange</li>
 * <li>Method search    : doSelectSCd</li>
 * <li>Method search    : doSelectDCd</li>
 * <li>Method search    : doClickLinkMore</li>
 * <li>Method search    : doOpenWORD003</li>
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderInformationAction extends CoreAction {
    
    /** The Purchase Order Information facade service. */
    private PurchaseOrderInformationFacadeService purchaseOrderInformationFacadeService;
    
    /**
     * Instantiates a new PurchaseOrderInformationAction.
     */
    public PurchaseOrderInformationAction(){
        super();
    }
    
    /**
     * Sets the Purchase Order Information facade service.
     * 
     * @param purchaseOrderInformationFacadeService the new Purchase Order Information facade service
     */
    public void setPurchaseOrderInformationFacadeService(
        PurchaseOrderInformationFacadeService purchaseOrderInformationFacadeService) {
        this.purchaseOrderInformationFacadeService = purchaseOrderInformationFacadeService;
    }
    
    /**
     * Do initial.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>In case click menu Purchase Orders, Initial criteria data for search when load page.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        form.resetForm();
        PurchaseOrderInformationDomain purchaseOrderInformationDomain = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            DataScopeControlDomain dataScopeControl
                = this.getDataScopeControlForUserLogin(userLogin);
            dataScopeControl.setLocale(locale);

            // 2. Call Façade Service PurchaseOrderInformationFacadeService initial()
            purchaseOrderInformationDomain = this.purchaseOrderInformationFacadeService
                .searchInitial(dataScopeControl);
            if(purchaseOrderInformationDomain != null){
                // 3. Set value of domain to ActionForm
                this.setMasterForm(form, purchaseOrderInformationDomain);
            }
            
            try {
                if (!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD002_BRESET)) 
                {
                    this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD002_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }
            
            try {
                if (!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD002_BDOWNLOAD)) 
                {
                    this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD002_BDOWNLOAD, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotDownloadCsvMessage(applicationException.getMessage());
            }
            
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0001
                , new String[]{e.getClass().toString(), e.getMessage()}));
        } finally {
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do initial.
     * <p>Initial data and load previous criteria from DensoContext and search.</p>
     * <ul>
     * <li>In case WORD003-[Return] or [Save&Send] , WORD002-[Acknowledge]</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialWithCriteria(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        form.resetForm();
        PurchaseOrderInformationDomain purchaseOrderInformationDomain = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            DataScopeControlDomain dataScopeControl
                = this.getDataScopeControlForUserLogin(userLogin);
            dataScopeControl.setLocale(locale);

            try {
                if (!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD002_BRESET)) 
                {
                    this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD002_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }
            
            try {
                if (!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD002_BDOWNLOAD)) 
                {
                    this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD002_BDOWNLOAD, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotDownloadCsvMessage(applicationException.getMessage());
            }
            
            // 2. Call Façade Service PurchaseOrderInformationFacadeService initial()
            purchaseOrderInformationDomain = purchaseOrderInformationFacadeService
                .searchInitial(dataScopeControl);
            if(purchaseOrderInformationDomain != null){
                // 3. Set value of domain to ActionForm
                this.setMasterForm(form, purchaseOrderInformationDomain);
                Word002Form sessionForm = (Word002Form)DensoContext.get().getGeneralArea(
                    SupplierPortalConstant.SESSION_WORD002_FORM);
                form.setVendorCd(sessionForm.getVendorCd());
                form.setSPcd(sessionForm.getSPcd());
                form.setDCd(sessionForm.getDCd());
                form.setDPcd(sessionForm.getDPcd());
                form.setIssuedDateFrom(sessionForm.getIssuedDateFrom());
                form.setIssuedDateTo(sessionForm.getIssuedDateTo());
                form.setPoStatus(sessionForm.getPoStatus());
                form.setPoType(sessionForm.getPoType());
                form.setPeriodType(sessionForm.getPeriodType());
                form.setDataType(sessionForm.getDataType());
                form.setViewPdf(sessionForm.getViewPdf());
                form.setSpsPoNo(sessionForm.getSpsPoNo());
                form.setCigmaPoNo(sessionForm.getCigmaPoNo());
                form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
                form.setDensoAuthenList(sessionForm.getDensoAuthenList());
                
                DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WORD002_FORM);
                
                // 4. Call Private Action Method for search Purchase Order information
                form.setFrom(sessionForm.getFrom());
                form.setTo(sessionForm.getTo());
                form.setMax(sessionForm.getMax());
                form.setPages(null);
                form.setCount(sessionForm.getCount());
                form.setPageNo(sessionForm.getPageNo());
                form.setFirstCount(Constants.FIVE);
                form.setFirstPages(Constants.PAGE_SEARCH);
                PurchaseOrderInformationDomain criteria = new PurchaseOrderInformationDomain();
                this.setParameterCriteria(criteria, form);
                applicationMessageSet.addAll(
                    this.searchPurchaseOrderInformation(criteria, form, locale));
            }
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0001
                , new String[]{e.getClass().toString(), e.getMessage()}));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PurchaseOrderInformationDomain criteria = new PurchaseOrderInformationDomain();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        
        if (Constants.PAGE_SEARCH.equals(form.getFirstPages())
            || Constants.PAGE_CLICK.equals(form.getPages()))
        {
            try {
                if (!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD002_BSEARCH)) 
                {
                    this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD002_BSEARCH, locale);
                }
                
                // 1. Call Private Action Method for search Purchase Order information
                this.setParameterCriteria(criteria, form);
                applicationMessageSet.addAll(
                    this.searchPurchaseOrderInformation(criteria, form, locale));
            } catch (ApplicationException applicationException) {
                applicationMessageSet.add(applicationException.getMessage());
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            } finally {
                applicationMessageSet.addAll(this.setPlantCombobox(form));
                for (String message : applicationMessageSet) {
                    form.getApplicationMessageList().add(
                        new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
                }
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Acknowledge.
     * <p>update P/O status to 'ACK' by criteria from screen.</p>
     * <ul>
     * <li>In case click Acknowledge button, system update data and count by criteria 
     * if count result size is 0 return message to screen "Update process failed".</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doAcknowledge(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        String updateDscId = userLogin.getSpsMUserDomain().getDscId();
        List<PurchaseOrderInformationDomain> poInfoList = form.getPoList(); 
        
        PurchaseOrderInformationDomain criteria = new PurchaseOrderInformationDomain();
        this.setParameterCriteria(criteria, form);
        criteria.setPoList(poInfoList);
        criteria.setLocale(locale);
        criteria.setLastUpdateDscId(updateDscId);
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD002_BACKNOWLEDGE)) 
            {
                this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD002_BACKNOWLEDGE, locale);
            }
            
            // 1. Call Façade Service PurchaseOrderInformationFacadeService updateAcknowledgePO()
            Integer updateCount = purchaseOrderInformationFacadeService.updateAcknowledgementPo(
                criteria);
            if (Constants.ZERO < updateCount) {
                form.getApplicationMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_SUCCESS, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_I6_0002)));
                
                // 2. Call Private Action Method for search Purchase Order information
                form.setPages(Constants.PAGE_CLICK);
                List<PurchaseOrderInformationDomain> clearList
                    = new ArrayList<PurchaseOrderInformationDomain>();
                form.setPoList(clearList);
                form.setMax(Constants.ZERO);
                this.setParameterCriteria(criteria, form);
                applicationMessageSet.addAll(
                    this.searchPurchaseOrderInformation(criteria, form, locale));
            }else{
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0003));
            }
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0001
                , new String[]{e.getClass().toString(), e.getMessage()}));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));

            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Export CSV.
     * 
     * @param actionMapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping actionMapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        PurchaseOrderInformationDomain purchaseOrderInformationDomain
            = new PurchaseOrderInformationDomain();
        this.setParameterCriteria(purchaseOrderInformationDomain, form);
        CommonDomain commonDomain = new CommonDomain();
        purchaseOrderInformationDomain.setLocale(locale);
        commonDomain.setLocale(locale);
        String fileName = null;
        ServletOutputStream outputStream = null;
        try{
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD002_BDOWNLOAD)) 
            {
                this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD002_BDOWNLOAD, locale);
            }
            
            PurchaseOrderInformationResultDomain result = purchaseOrderInformationFacadeService
                .searchPurchaseOrderInformationCsv(
                purchaseOrderInformationDomain, commonDomain);
            if (null != result.getApplicationMessageSet()
                && Constants.ZERO < result.getApplicationMessageSet().size())
            {
                applicationMessageSet.addAll(result.getApplicationMessageSet());
            } else {
                Calendar cal = Calendar.getInstance();
                fileName = StringUtil.appendsString(
                    Constants.PURCHASE_ORDER_INFO_LIST_FILE_NAME,
                    Constants.SYMBOL_UNDER_SCORE,
                    DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                byte[] output = result.getCsvBufffer().toString().getBytes();
                super.setHttpHeaderForCsv(response, fileName);
                response.setContentLength(output.length);
                outputStream = response.getOutputStream();
                outputStream.write(output, Constants.ZERO, output.length);
                outputStream.flush();
                return null;
            }
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());    
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
            if (null != outputStream) {
                outputStream.close();
            }
        }
        
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download PDF Original.
     * <p>Download PDF Original from File Management Service</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadPdfOriginal(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PurchaseOrderInformationDomain criteria = new PurchaseOrderInformationDomain();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();

        OutputStream output = null;
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD002_LPDFORIGINAL)) 
            {
                this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD002_LPDFORIGINAL, locale);
            }
            
            /* 1. Call Façade Service PurchaseOrderInformationFacadeService searchFileName()
             * to get PDF file name
             * */
            criteria.setFileIdSelected(form.getFileIdSelected());
            
            criteria.setLocale(locale);
            FileManagementDomain file
                = this.purchaseOrderInformationFacadeService.generateOriginalPo(criteria);
            
            /* 2. Call Façade Service PurchaseOrderInformationFacadeService
             * searchPurchaseOrderReport() to get PDF file
             * */
            super.setHttpHeaderForFileManagement(response, request, file.getFileName());
            output = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(file.getFileData(), response.getOutputStream());
//            this.purchaseOrderInformationFacadeService.searchPurchaseOrderReport(criteria, output);
            return null;
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
            if (null != output) {
                output.close();
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download PDF Change.
     * <p>Download PDF Change from File Management Service</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadPdfChange(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PurchaseOrderInformationDomain criteria = new PurchaseOrderInformationDomain();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();

        OutputStream output = null;
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD002_LPDFCHANGE)) 
            {
                this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD002_LPDFCHANGE, locale);
            }
            
            /* 1. Call Façade Service PurchaseOrderInformationFacadeService searchFileName()
             * to get PDF file name
             * */
            criteria.setFileIdSelected(form.getFileIdSelected());
            criteria.setLocale(locale);
            FileManagementDomain file
                = this.purchaseOrderInformationFacadeService.generateChangePo(criteria);
            
            /* 2. Call Façade Service PurchaseOrderInformationFacadeService
             * searchChangeMaterialRelease() to get PDF file
             * */
            super.setHttpHeaderForFileManagement(response, request, file.getFileName());
            output = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(file.getFileData(), response.getOutputStream());
//            this.purchaseOrderInformationFacadeService.searchChangeMaterialRelease(criteria,
//                output);
            return null;
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
            if (null != output) {
                output.close();
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Search Supplier Plant Code by selected Supplier Company Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<PlantSupplierDomain> plantList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * searchPlantBySelectSupplierCompany() to get the list of supplier plant code by
             * supplier company code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                plantSupplier.getPlantSupplierDomain().setVendorCd(
                    form.getVendorCd());
            }
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            plantList = this.purchaseOrderInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplier);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)plantList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * searchCompanyBySelectSupplierPlant() to get the list of supplier company code by
             * supplier plant code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            companyList = this.purchaseOrderInformationFacadeService
                .searchSelectedPlantSupplier(plantSupplier);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * Search DENSO Plant Code by selected DENSO Company Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<PlantDensoDomain> plantList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * changeSelectSupplierCompany() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                plantDenso.getPlantDensoDomain().setDCd(form.getDCd());
            }
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            plantList = this.purchaseOrderInformationFacadeService.searchSelectedCompanyDenso(
                plantDenso);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)plantList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * Search DENSO company Code by selected DENSO plant Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * changeSelectSupplierCompany() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            companyList = this.purchaseOrderInformationFacadeService
                .searchSelectedPlantDenso(plantDenso);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * Do Download PDF Change.
     * <p>Download PDF Change from File Management Service</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PurchaseOrderInformationDomain criteria = new PurchaseOrderInformationDomain();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();

        OutputStream output = null;
        try {
            /* 1. Call Façade Service PurchaseOrderInformationFacadeService searchFileName()
             * to get PDF file name
             * */
            criteria.setFileIdSelected(ContextParams.getLegendFileId());
            criteria.setLocale(locale);
            FileManagementDomain file
                = this.purchaseOrderInformationFacadeService.searchFileName(criteria);
            
            /* 2. Call Façade Service PurchaseOrderInformationFacadeService searchLegendInfo()
             * to get PDF file
             * */
            super.setHttpHeaderForFileManagement(response, request, file.getFileName());
            output = response.getOutputStream();
            this.purchaseOrderInformationFacadeService.searchLegendInfo(criteria, output);
            return null;
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
            if (null != output) {
                output.close();
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do open WORD003.
     * <p>Forward to screen WORD003: Purchase Order Acknowledgement.
     * After click SPS P/O No.</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doOpenWord003(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        StringBuffer forward = new StringBuffer();
        Word002Form form = (Word002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        Set<String> applicationMessageSet = new LinkedHashSet<String>();

        DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WORD002_FORM, form);
        
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD002_LSPSPONO)) 
            {
                this.purchaseOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD002_LSPSPONO, locale);
            }
            
            /*
             * If WORD002Form. poInfoList. periodTypeName == "Forecast"
             * && WORD002Form. poInfoList. orderMethod == "1"   Then (this condition may not have to
             *     check)
             * */
            String periodType = Constants.EMPTY_STRING;
            if (SupplierPortalConstant.FORECAST.equals(form.getPeriodTypeName())) {
                periodType = Constants.STR_ONE;
            } else {
                periodType = form.getClickedPeriodType();
            }
            
            /* Require parameter to open screen WORD003: Purchase Order Acknowledgement.
             * - DSC ID
             * - poId
             * - SPS P/O No.
             * - periodType
             * - periodTypeName
             * - poType
             */
            forward.append(SupplierPortalConstant.URL_WORD003_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                .append(SupplierPortalConstant.URL_PARAM_DSCID).append(userLogin.getDscId())
                .append(SupplierPortalConstant.URL_PARAM_POID).append(form.getPoId())
                .append(SupplierPortalConstant.URL_PARAM_SPSPONO).append(form.getClickedSpsPoNo())
                .append(SupplierPortalConstant.URL_PARAM_PERIODTYPE).append(periodType)
                .append(SupplierPortalConstant.URL_PARAM_PERIODTYPENAME)
                .append(form.getPeriodTypeName())
                .append(SupplierPortalConstant.URL_PARAM_POTYPE).append(form.getClickedPoType())
                .append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES).append(Constants.PAGE_SEARCH);
            return new ActionForward(forward.toString(), true);
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009));
        } finally {
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET
            , SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH
            , SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_EXPORT
            , SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_LINK_SPSPONO
            , SupplierPortalConstant.METHOD_DO_OPEN_WORD003);
        keyMethodMap.put(SupplierPortalConstant.ACTION_ACKNOWLEDGE
            , SupplierPortalConstant.METHOD_DO_ACKNOWLEDGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF_ORIGINAL
            , SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF_ORIGINAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF_CHANGE
            , SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF_CHANGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE
            , SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD
            , SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD
            , SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL
            , SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_WITH_CRITERIA
            , SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA);
        
        return keyMethodMap;
    }

    /**
     * Sets the criteria to search.
     * <p>Set criteria form to domain for condition of query Purchase order Information.</p>
     * 
     * @param purchaseOrderInformationDomain Purchase Order Information Domain
     * @param form the Word002Form
     */
    private void setParameterCriteria(PurchaseOrderInformationDomain purchaseOrderInformationDomain
        , Word002Form form)
    {
        if (Constants.PAGE_SEARCH.equals(form.getFirstPages())) {
            form.setPageNo(Constants.ONE);
        }
        purchaseOrderInformationDomain.getSpsTPoDomain().setVendorCd(form.getVendorCd());
        purchaseOrderInformationDomain.getSpsTPoDomain().setSPcd(form.getSPcd());
        purchaseOrderInformationDomain.getSpsTPoDomain().setDCd(form.getDCd());
        purchaseOrderInformationDomain.getSpsTPoDomain().setDPcd(form.getDPcd());
        purchaseOrderInformationDomain.setIssuedDateFrom(this.emptyToNull(
            form.getIssuedDateFrom()));
        purchaseOrderInformationDomain.setIssuedDateTo(this.emptyToNull(form.getIssuedDateTo()));
        purchaseOrderInformationDomain.getSpsTPoDomain().setPoStatus(this.emptyToNull(
            form.getPoStatus()));
        purchaseOrderInformationDomain.getSpsTPoDomain().setPoType(this.emptyToNull(
            form.getPoType()));
        purchaseOrderInformationDomain.getSpsTPoDomain().setPeriodType(this.emptyToNull(
            form.getPeriodType()));
//        purchaseOrderInformationDomain.setViewPdf(this.emptyToNull(form.getViewPdf()));
        purchaseOrderInformationDomain.setDataType(this.emptyToNull(form.getDataType()));
        purchaseOrderInformationDomain.setViewPdf(form.getViewPdf());
        purchaseOrderInformationDomain.getSpsTPoDomain().setSpsPoNo(this.emptyToNull(
            form.getSpsPoNo()));
        purchaseOrderInformationDomain.getSpsTPoDomain().setCigmaPoNo(this.emptyToNull(
            form.getCigmaPoNo()));
        purchaseOrderInformationDomain.setSupplierAuthenList(form.getSupplierAuthenList());
        purchaseOrderInformationDomain.setDensoAuthenList(form.getDensoAuthenList());
    }

    /**
     * Sets the data to combo box.
     * <p>Set Purchase order Information Domain to WORD002 Form for initial master combo box.</p>
     * 
     * @param form the Word002Form
     * @param purchaseOrderInformationDomain the Purchase order Information Domain.
     */
    private void setMasterForm(Word002Form form,
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        form.setPoStatusList(purchaseOrderInformationDomain.getPoStatusList());
        form.setPoTypeList(purchaseOrderInformationDomain.getPoTypeList());
        form.setPeriodTypeList(purchaseOrderInformationDomain.getPeriodTypeList());
        form.setViewPdfList(purchaseOrderInformationDomain.getViewPdfList());
        form.setCompanyDensoList(purchaseOrderInformationDomain.getCompanyDensoList());
        form.setCompanySupplierList(purchaseOrderInformationDomain.getCompanySupplierList());
        form.setIssuedDateFrom(purchaseOrderInformationDomain.getIssuedDateFrom());
        form.setIssuedDateTo(purchaseOrderInformationDomain.getIssuedDateTo());
        form.setPlantDensoList(purchaseOrderInformationDomain.getPlantDensoList());
        form.setPlantSupplierList(purchaseOrderInformationDomain.getPlantSupplierList());
        
        // PO Status default value is 'ISS'
        form.setPoStatus(Constants.ISS_STATUS);
        
        form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
            purchaseOrderInformationDomain.getDensoSupplierRelationList(), 
            SupplierPortalConstant.SCREEN_ID_WORD002));
        form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
            purchaseOrderInformationDomain.getDensoSupplierRelationList(), 
            SupplierPortalConstant.SCREEN_ID_WORD002));
    }
    
    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the Word002Form
     * @return set of application message
     * */
    private Set<String> setPlantCombobox(Word002Form form) {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);
        
        if (!Strings.judgeBlank(form.getVendorCd())) {
            List<PlantSupplierDomain> plantList = null;
            
            try{
                PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplier.getPlantSupplierDomain().setVendorCd(
                        form.getVendorCd());
                }
                plantSupplier.setDataScopeControlDomain(dataScopeControl);
                plantList = this.purchaseOrderInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplier);
                
                form.setPlantSupplierList(plantList);
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd())) {
            List<PlantDensoDomain> plantList = null;
            
            try{
                PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                    plantDenso.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDenso.setDataScopeControlDomain(dataScopeControl);
                plantList = this.purchaseOrderInformationFacadeService
                    .searchSelectedCompanyDenso(plantDenso);
                form.setPlantDensoList(plantList);
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            }
        }
        
        return applicationMessageSet;
    }
    
    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getUserLoginFromDensoContext() {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * Get Data Scope Control for current user login.
     * @param userLogin User Login Information
     * @return Data Scope Control
     * */
    private DataScopeControlDomain getDataScopeControlForUserLogin(UserLoginDomain userLogin) {
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        dataScopeControl.setUserType(userLogin.getUserType());
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WORD002.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd()))
            {
                dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
            }
        }
        dataScopeControl.setDensoSupplierRelationDomainList(null);
        return dataScopeControl;
    }
    
    /**
     * Search Purchase Order information.
     * @param criteria the search criteria
     * @param form ActionForm
     * @param locale Locale for get message
     * @return set of application message
     * */
    private Set<String> searchPurchaseOrderInformation(PurchaseOrderInformationDomain criteria,
        Word002Form form, Locale locale)
    {
        criteria.setLocale(locale);

        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        
        criteria.setPageNumber(form.getPageNo());
        
        try {
            PurchaseOrderInformationResultDomain result = purchaseOrderInformationFacadeService
                .searchPurchaseOrderInformation(criteria);
            SpsPagingUtil.setFormForPaging(criteria, form);

            if (null != result.getApplicationMessageSet()
                && Constants.ZERO < result.getApplicationMessageSet().size())
            {
                applicationMessageSet.addAll(result.getApplicationMessageSet());
            } else {
                // Set value of domain to ActionForm
                form.setPoList(result.getPurchaseOrderInformationDomainList());
            }
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0001,
                new String[] {e.getClass().toString(), e.getMessage()}));
        }
        return applicationMessageSet;
    }

    /**
     * If input string is empty or has only space, return null.<br />
     * If not, return trimmed string.
     * @param input - input string
     * @return trimmed input string or null
     * @
     * */
    private String emptyToNull(String input) {
        if (null == input) {
            return null;
        }
        if (Constants.EMPTY_STRING.equals(input.trim())) {
            return null;
        }
        return input.toUpperCase().trim();
    }
}