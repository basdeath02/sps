/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.presentation.form.Wcom001Form;

/**
 * The Class LoginAction.
 * @author CSI
 */
public class LoginAction extends CoreAction {
    
    /** The Constant LOG. */
    //private static final Log LOG = LogFactory.getLog(WCOM001Action.class);

    /**
     * Instantiates a new WCOM001Action.
     */
    public LoginAction(){
        super();
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param form the form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do home.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doHome(ActionMapping actionMapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Initial.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doLogin(ActionMapping actionMapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wcom001Form form = (Wcom001Form)actionForm;
        
        // Login
        StringBuilder forward = new StringBuilder();
        forward.append("/MainScreenAction.do?method=doInitial&dscId=" + form.getUserName());
//        forward.append("/AsnUploadingAction.do?method=doInitial");
            
        return new ActionForward(forward.toString());
        //String forward = Constants.ACTION_FWD_SUCCESS;
        //return actionMapping.findForward(forward);
    }
    
    /**
     * Do change locale.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doChangeLocale(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String targetLocale = getParameter(request, "targetLocale");
        //HttpSession session = request.getSession();
        if(!Strings.judgeBlank(targetLocale)){
            //session.setAttribute(Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(targetLocale));
            DensoContext.get().putGeneralArea(
                Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(targetLocale));
        }else{
            //session.setAttribute(Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(
            //    ContextParams.getDefaultLocale()));
            DensoContext.get().putGeneralArea(Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(
                ContextParams.getDefaultLocale()));
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do logout.
     * 
     * @param mapping the mapping
     * @param form the form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doLogout(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        //SessionInvalidateUtil.invalidateSession(request, response);
        
        
        // Akat K. : Test file manager stream
        //return mapping.findForward("logout");
        StringBuilder forward = new StringBuilder();
        forward.append("/WADM999Action.do?method=doInitial");
        return new ActionForward(forward.toString());
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_LOGIN,
            SupplierPortalConstant.METHOD_DO_LOGIN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LOGOUT,
            SupplierPortalConstant.METHOD_DO_LOGOUT);
        keyMethodMap.put(SupplierPortalConstant.ACTION_CHANGE_LOCALE,
            SupplierPortalConstant.METHOD_DO_CHANGE_LOCALE);
        
        return keyMethodMap;
    }
    
}