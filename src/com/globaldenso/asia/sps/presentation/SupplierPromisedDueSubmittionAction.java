/*
 * ModifyDate Development company     Describe 
 * 2014/08/14 CSI Akat                Create
 * 2016/03/18 CSI Akat                [FIX] avoid special character in URL
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDueDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;
import com.globaldenso.asia.sps.business.domain.SupplierPromisedDueSubmittionDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.SupplierPromisedDueSubmittionFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.presentation.form.Word003Form;
import com.globaldenso.asia.sps.presentation.form.Word004Form;

/**
 * Action for screen WORD004 Supplier Promised Due Submittion.
 * <p>After supplier click link Pending from Purchase Order Acknowledgement screen. Supplier decide
 * which Due Date, Due Qty cannot fulfill and then input New Due.</p>
 * 
 * <ul>
 * <li>Method search    : doInitial</li>
 * <li>Method search    : doRegister</li>
 * <li>Method search    : doReturn</li>
 * <li>Method search    : doClickLinkMore</li>
 * </ul>
 * 
 * @author CSI
 * */
public class SupplierPromisedDueSubmittionAction extends CoreAction {

    /** XML attribute name 'id'. */
    private static final String ATTR_NAME_ID = "id";
    
    /** HTML element id 'chkPoDue'. */
    private static final String ID_CHK_PO_DUE = "chkPoDue";

    /** JQuery selector for not select any element. */
    private static final String SELECTOR_NONE = "[id='none']";
    
    /** The facade for Supplier Promised Due Submittion screen. */
    private SupplierPromisedDueSubmittionFacadeService supplierPromisedDueSubmittionFacadeService;
    
    /** The default constructor. */
    public SupplierPromisedDueSubmittionAction() {
        super();
    }

    /**
     * <p>Setter method for supplierPromisedDueSubmittionFacadeService.</p>
     *
     * @param supplierPromisedDueSubmittionFacadeService Set for supplierPromisedDueSubmittionFacadeService
     */
    public void setSupplierPromisedDueSubmittionFacadeService(
        SupplierPromisedDueSubmittionFacadeService supplierPromisedDueSubmittionFacadeService)
    {
        this.supplierPromisedDueSubmittionFacadeService
            = supplierPromisedDueSubmittionFacadeService;
    }

    /**
     * Do initial.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>In case click Edit/View link in Purchase Order Acknowledgement screen.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word004Form form = (Word004Form)actionForm;
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        form.resetForm();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain
            = new SupplierPromisedDueSubmittionDomain();
        
        /* Start : [FIX] avoid special character in URL, receive parameter from WORD003 by 
         * DensoContext general area
         * */
        Word003Form word003Form = (Word003Form)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WORD003_FORM);
        form.setPoId(word003Form.getPoId());
        form.setSPn(word003Form.getSelectedSPn());
        form.setDPn(word003Form.getSelectedDPn());
        form.setPeriodType(word003Form.getPeriodType());
        form.setPeriodTypeName(word003Form.getPeriodTypeName());
        form.setPoType(word003Form.getPoType());
        form.setActionMode(word003Form.getSelectedActionMode());
        form.setSpsPoNo(word003Form.getSpsPoNo());
        // End : [FIX] avoid special character in URL
        
        try{
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            // 2. Call Façade Service SupplierPromisedDueSubmittionFacadeService initial()
            supplierPromisedDueSubmittionDomain.setLocale(locale);
            supplierPromisedDueSubmittionDomain.setDscId(userLogin.getDscId());
            supplierPromisedDueSubmittionDomain.setPoId(form.getPoId());
            supplierPromisedDueSubmittionDomain.setSPn(form.getSPn());
            supplierPromisedDueSubmittionDomain.setDPn(form.getDPn());

            // [IN054] Check Action Mode when get data.
            supplierPromisedDueSubmittionDomain.setActionMode(form.getActionMode());
            
            supplierPromisedDueSubmittionDomain
                = this.supplierPromisedDueSubmittionFacadeService.searchInitial(
                    supplierPromisedDueSubmittionDomain);
            
            // 3. Set value of domain to ActionForm
            form.setPendingReasonCodeList(
                supplierPromisedDueSubmittionDomain.getPendingReasonCodeList());
            form.setPurchaseOrderDueList(
                supplierPromisedDueSubmittionDomain.getPurchaseOrderDueList());
            form.setMax(supplierPromisedDueSubmittionDomain.getPendingReasonCodeList().size());
            
            // [IN054] Add new Misc Type for new combo box
            form.setAcceptRejectCodeList(
                supplierPromisedDueSubmittionDomain.getAcceptRejectCodeList());
            
            this.setDataPoDueForScreen(form.getPurchaseOrderDueList());
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }
        
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Register new propose due date/quantity.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        Word004Form form = (Word004Form)actionForm;
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain
            = new SupplierPromisedDueSubmittionDomain();
        
        try{
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            StringBuffer idBuffer = new StringBuffer();
            List<PurchaseOrderDueDomain> changeDueList = new ArrayList<PurchaseOrderDueDomain>();
            int poDueCount = Constants.ZERO;
            for (PurchaseOrderDueDomain poDue : form.getPurchaseOrderDueList()) {
                if (Constants.STR_ONE.equals(poDue.getChangePoDueFlag())) {
                    changeDueList.add(poDue);
                    
                    // [id='chkPoDue']
                    idBuffer.append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                        .append(ATTR_NAME_ID)
                        .append(Constants.SYMBOL_EQUAL)
                        .append(Constants.SYMBOL_SINGLE_QUOTE)
                        .append(ID_CHK_PO_DUE)
                        .append(poDueCount)
                        .append(Constants.SYMBOL_SINGLE_QUOTE)
                        .append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET)
                        .append(Constants.SYMBOL_COMMA);
                }
                
                poDueCount++;
            }
            
            if (Constants.ZERO == idBuffer.length()) {
                form.setEnableCheckPoDueId(SELECTOR_NONE);
            } else {
                idBuffer.setLength(idBuffer.length() - Constants.ONE);
                form.setEnableCheckPoDueId(idBuffer.toString());
            }
            
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD004_BREGISTER)) 
            {
                this.supplierPromisedDueSubmittionFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD004_BREGISTER, locale);
            }
            
            if (Constants.ZERO == changeDueList.size()) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0010)));
                this.setDataPoDueForScreen(form.getPurchaseOrderDueList());
                return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
            }
            
            //1. Call Façade Service SupplierPromisedDueSubmittionFacadeService transactNewDue()
            supplierPromisedDueSubmittionDomain.setLocale(locale);
            supplierPromisedDueSubmittionDomain.setDscId(userLogin.getDscId());
            supplierPromisedDueSubmittionDomain.setPoId(form.getPoId());
            supplierPromisedDueSubmittionDomain.setSPn(form.getSPn());
            supplierPromisedDueSubmittionDomain.setDPn(form.getDPn());
            supplierPromisedDueSubmittionDomain.setPurchaseOrderDueList(changeDueList);

            List<ApplicationMessageDomain> applicationMessageResultList
                = this.supplierPromisedDueSubmittionFacadeService.transactNewDue(
                    supplierPromisedDueSubmittionDomain);
            
            
            if (null != applicationMessageResultList
                && Constants.ZERO != applicationMessageResultList.size())
            {
                applicationMessageList.addAll(applicationMessageResultList);
                this.setDataPoDueForScreen(form.getPurchaseOrderDueList());
                return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
            }
            
            String message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0008);
            
            // [IN054] Call private method to generate URL
            String forwardUrl = this.generateActionForwardText(userLogin, form, message);
            
            return new ActionForward(forwardUrl, true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Save DENSO Reply result.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSave(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        Word004Form form = (Word004Form)actionForm;
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        SupplierPromisedDueSubmittionDomain supplierPromisedDueSubmittionDomain
            = new SupplierPromisedDueSubmittionDomain();
        
        try{
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            List<PurchaseOrderDueDomain> changeDueList = new ArrayList<PurchaseOrderDueDomain>();
            for (PurchaseOrderDueDomain poDue : form.getPurchaseOrderDueList()) {
                if (Constants.STR_ONE.equals(poDue.getSpsTPoDueDomain().getMarkPendingFlg())) {
                    changeDueList.add(poDue);
                }
            }
            
            form.setEnableCheckPoDueId(SELECTOR_NONE);
            
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD004_BSAVE)) 
            {
                this.supplierPromisedDueSubmittionFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD004_BSAVE, locale);
            }
            
            if (Constants.ZERO == changeDueList.size()) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E7_0010)));
                this.setDataPoDueForScreen(form.getPurchaseOrderDueList());
                return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
            }
            
            //1. Call Façade Service SupplierPromisedDueSubmittionFacadeService transactNewDue()
            supplierPromisedDueSubmittionDomain.setLocale(locale);
            supplierPromisedDueSubmittionDomain.setDscId(userLogin.getDscId());
            supplierPromisedDueSubmittionDomain.setPoId(form.getPoId());
            supplierPromisedDueSubmittionDomain.setSPn(form.getSPn());
            supplierPromisedDueSubmittionDomain.setDPn(form.getDPn());
            supplierPromisedDueSubmittionDomain.setPurchaseOrderDueList(changeDueList);

            List<ApplicationMessageDomain> applicationMessageResultList 
                = this.supplierPromisedDueSubmittionFacadeService.transactReplyDue(
                    supplierPromisedDueSubmittionDomain);

            if (null != applicationMessageResultList
                && Constants.ZERO != applicationMessageResultList.size())
            {
                applicationMessageList.addAll(applicationMessageResultList);
                this.setDataPoDueForScreen(form.getPurchaseOrderDueList());
                return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
            }
            
            String message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0008);

            String forwardUrl = this.generateActionForwardText(userLogin, form, message);
            
            return new ActionForward(forwardUrl, true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Return to screen Purchase Order Acknowledgement.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word004Form form = (Word004Form)actionForm;
        StringBuffer forward = new StringBuffer();
        
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        try{
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            StringBuffer idBuffer = new StringBuffer();
            List<PurchaseOrderDueDomain> changeDueList = new ArrayList<PurchaseOrderDueDomain>();
            int poDueCount = Constants.ZERO;
            for (PurchaseOrderDueDomain poDue : form.getPurchaseOrderDueList()) {
                if (Constants.STR_ONE.equals(poDue.getChangePoDueFlag())) {
                    changeDueList.add(poDue);
                    
                    // [id='chkPoDue']
                    idBuffer.append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
                        .append(ATTR_NAME_ID)
                        .append(Constants.SYMBOL_EQUAL)
                        .append(Constants.SYMBOL_SINGLE_QUOTE)
                        .append(ID_CHK_PO_DUE)
                        .append(poDueCount)
                        .append(Constants.SYMBOL_SINGLE_QUOTE)
                        .append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET)
                        .append(Constants.SYMBOL_COMMA);
                }
                poDueCount++;
            }
            
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD004_BRETURN)) 
            {
                this.supplierPromisedDueSubmittionFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD004_BRETURN, locale);
            }
            
            /* Require parameter to open screen WORD003: Purchase Order Acknowledgement.
             * - DSC ID
             * - poId
             * - SPS P/O No.
             * - periodType
             * - periodTypeName
             * - poType
             */
            forward.append(SupplierPortalConstant.URL_WORD003_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                .append(SupplierPortalConstant.URL_PARAM_DSCID).append(userLogin.getDscId())
                .append(SupplierPortalConstant.URL_PARAM_POID).append(form.getPoId())
                .append(SupplierPortalConstant.URL_PARAM_SPSPONO).append(form.getSpsPoNo())
                .append(SupplierPortalConstant.URL_PARAM_PERIODTYPE).append(form.getPeriodType())
                .append(SupplierPortalConstant.URL_PARAM_PERIODTYPENAME)
                .append(form.getPeriodTypeName())
                .append(SupplierPortalConstant.URL_PARAM_POTYPE).append(form.getPoType())
                .append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES).append(Constants.PAGE_SEARCH);
            return new ActionForward(forward.toString(), true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download PDF Change.
     * <p>Download PDF Change from File Management Service</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word004Form form = (Word004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        SupplierPromisedDueSubmittionDomain criteria = new SupplierPromisedDueSubmittionDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();

        OutputStream output = null;
        try {
            /* 1. Call Façade Service PurchaseOrderInformationFacadeService searchFileName()
             * to get PDF file name
             * */
            criteria.setFileId(ContextParams.getLegendFileId());
            criteria.setLocale(locale);
            FileManagementDomain fileManage
                = this.supplierPromisedDueSubmittionFacadeService.searchFileName(criteria);
            
            /* 2. Call Façade Service PurchaseOrderInformationFacadeService searchLegendInfo()
             * to get PDF file
             * */
            super.setHttpHeaderForFileManagement(response, request, fileManage.getFileName());
            output = response.getOutputStream();
            this.supplierPromisedDueSubmittionFacadeService.searchLegendInfo(criteria, output);
            return null;
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            applicationMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
            if (null != output) {
                output.close();
            }
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        // doInitial
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL
            , SupplierPortalConstant.METHOD_DO_INITIAL);
        
        // doRegister
        keyMethodMap.put(SupplierPortalConstant.ACTION_REGISTER
            , SupplierPortalConstant.METHOD_DO_REGISTER);
        
        // doReturn
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN
            , SupplierPortalConstant.METHOD_DO_RETURN);
        
        // [IN054] Add new action for new button
        keyMethodMap.put(SupplierPortalConstant.ACTION_DO_SAVE
            , SupplierPortalConstant.METHOD_DO_SAVE);
        
        // doClickLinkMore
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE
            , SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        
        return keyMethodMap;
    }
    
    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getUserLoginFromDensoContext() {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * Set format data to show in screen.
     * @param result - result list to set.
     * */
    private void setDataPoDueForScreen(List<PurchaseOrderDueDomain> result) {
        SpsTPoDueDomain spsTPoDue = null;
        DecimalFormat decimalFormat = new DecimalFormat(Constants.DEFAULT_NUMBER_FORMAT);
        for (PurchaseOrderDueDomain poDueResult : result) {
            spsTPoDue = poDueResult.getSpsTPoDueDomain();
            if (null != spsTPoDue) {
                if (null != spsTPoDue.getSpsProposedQty()
                    && Strings.judgeBlank(poDueResult.getStringProposedQty()))
                {
                    poDueResult.setStringProposedQty(decimalFormat.format(
                        spsTPoDue.getSpsProposedQty()));
                }
                
                if (null != spsTPoDue.getSpsProposedDueDate()
                    && Strings.judgeBlank(poDueResult.getStringProposedDate()))
                {
                    poDueResult.setStringProposedDate(DateUtil.format(
                        spsTPoDue.getSpsProposedDueDate(), DateUtil.PATTERN_YYYYMMDD_SLASH));
                }
            }
        }
        
    }
    
    // [IN054] Add new common method
    /**
     * Create Action Forward URL information to return to screen WORD003
     * @param userLogin : User Login Domain
     * @param form : Screen Action Form
     * @param message : message to show in message area
     * @return Action Forward URL information
     * */
    private String generateActionForwardText(UserLoginDomain userLogin, Word004Form form,
        String message)
    {
        StringBuffer forward = new StringBuffer();
        
        /* Return to screen WORD003: Purchase Order Acknowledgement.
         */
        forward.append(SupplierPortalConstant.URL_WORD003_ACTION)
            .append(SupplierPortalConstant.URL_PARAM_METHOD)
            .append(SupplierPortalConstant.METHOD_DO_INITIAL)
            .append(SupplierPortalConstant.URL_PARAM_DSCID).append(userLogin.getDscId())
            .append(SupplierPortalConstant.URL_PARAM_POID).append(form.getPoId())
            .append(SupplierPortalConstant.URL_PARAM_SPSPONO).append(form.getSpsPoNo())
            .append(SupplierPortalConstant.URL_PARAM_PERIODTYPE)
            .append(form.getPeriodType())
            .append(SupplierPortalConstant.URL_PARAM_PERIODTYPENAME)
            .append(form.getPeriodTypeName())
            .append(SupplierPortalConstant.URL_PARAM_POTYPE).append(form.getPoType())
            
            // param name
            .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_TYPE)
            .append(Constants.SYMBOL_EQUAL)
            // value
            .append(Constants.MESSAGE_SUCCESS)
            
            // param name
            .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_MESSAGE)
            .append(Constants.SYMBOL_EQUAL)
            // value
            .append(message)
            
            .append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES)
            .append(Constants.PAGE_SEARCH);
        
        return forward.toString();
    }
    
}
