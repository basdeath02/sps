/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/21 CSI Arnon       Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.AsnProgressInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp009Form;

/**
 * The Class WSHP009Action.
 * @author CSI
 */
public class AsnProgressInformationAction extends CoreAction {

    /** The Asn Progress Information facade service. */
    private AsnProgressInformationFacadeService asnProgressInformationFacadeService;
    
    /** The default constructor. */
    public AsnProgressInformationAction() {
        super();
    }

    /**
     * Set the acknowledged do information facade service.
     * 
     * @param asnProgressInformationFacadeService the acknowledged do information facade service to set
     */
    public void setAsnProgressInformationFacadeService(
        AsnProgressInformationFacadeService asnProgressInformationFacadeService) {
        this.asnProgressInformationFacadeService = asnProgressInformationFacadeService;
    }

    /**
     * Do return from Invoice Information.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        return doInitial(mapping, actionForm, request, response);
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp009Form form = (Wshp009Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        AsnProgressInformationDomain asnProgressInformation = null;
        
        try{
            UserLoginDomain userLogin = this.getUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            
            asnProgressInformation = new AsnProgressInformationDomain();
            asnProgressInformation.setLocale(locale);
            
            if(StringUtil.checkNullOrEmpty(form.getMode())){
                form.resetForm();
                this.setComboboxInitial(form, asnProgressInformation);
                this.setInitialCriteria(form);
            }else{
                this.doInitialForOtherPages(form, asnProgressInformation);
                this.setComboboxInitial(form, asnProgressInformation);
                this.setPlantCombobox(form);
            }
            
            try{
                if(!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP009_BRESET)){
                    this.asnProgressInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP009_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            
            try{
                if(!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP009_BDOWNLOAD)){
                    this.asnProgressInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP009_BDOWNLOAD, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotDownloadMessage(ex.getMessage());
            }
            
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            String message = MessageUtil.getApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_90_0001,
                new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
        }finally{
            form.getApplicationMessageList().addAll(errorMessageList);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        // Convert ActionForm to Wshp009Form.
        Wshp009Form form = (Wshp009Form)actionForm;
        // Handle Paging.
        String firstPage = form.getFirstPages();
        String page = form.getPages();
        // Initial Criteria and Result.
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        List<AsnProgressInformationReturnDomain> asnProgressInformationList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnProgressInformationDomain criteria = new AsnProgressInformationDomain();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        if(Constants.PAGE_SEARCH.equals(firstPage) || Constants.PAGE_CLICK.equals(page)){
            try {
                if(!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP009_BSEARCH)){
                    this.asnProgressInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP009_BSEARCH, locale);
                }
                
                criteria.setLocale(locale);
                asnProgressInformationList
                    = this.searchAsnProgressInformation(form, criteria, false);
                if(null != asnProgressInformationList 
                    && Constants.ZERO < asnProgressInformationList.size())
                {
                    form.setAsnProgressList(asnProgressInformationList);
                    SpsPagingUtil.setFormForPaging(criteria, form);
                }else{
                    errorMessageList = criteria.getErrorMessageList();
                }
            }catch(ApplicationException e){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    e.getMessage()));
            }catch(Exception e){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.setApplicationMessageList(errorMessageList);
                this.setPlantCombobox(form);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download.
     * 
     * <p>
     * download data that meet criteria values.
     * </p>
     * 
     * <li>In case click download button, system will load data that meet to criteria 
     * and export all data to CSV file.</li>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception{
        Wshp009Form form = (Wshp009Form)actionForm;
        List<AsnProgressInformationReturnDomain> resultList = 
            new ArrayList<AsnProgressInformationReturnDomain>();
        String fileName = null;
        ServletOutputStream outputStream = null;
        
        // Initial Criteria and Result.
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnProgressInformationDomain criteria = new AsnProgressInformationDomain();
        
        try{
            resultList = searchAsnProgressInformation(form, criteria, true);
            String resultStr = asnProgressInformationFacadeService.convertToCsvData(
                resultList, locale);
            if(!StringUtil.checkNullOrEmpty(resultStr)){
                Calendar cal = Calendar.getInstance();
                fileName = StringUtil.appendsString(
                    Constants.ASN_PROGRESS_INFORMATION_INFO_LIST_FILE_NAME,
                    Constants.SYMBOL_UNDER_SCORE,
                    DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                
                byte[] output = resultStr.toString().getBytes();
                setHttpHeaderForCsv(response, fileName);
                outputStream = response.getOutputStream();
                outputStream.write(output, Constants.ZERO, output.length);
                outputStream.flush();
                return null;
            }else{
                errorMessageList = criteria.getErrorMessageList();
            }
        }catch(ApplicationException applicationException){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
            this.setPlantCombobox(form);
            form.setApplicationMessageList(errorMessageList);
        }
//        this.setInitialCriteria(form);
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**Do Click Link ASN No.
     * <p>In case of clicking 'ASN No.' link, the system will link to WSHP004 screen.
     * </p>
     * 
     * @param mapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doClickLinkAsnNo(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp009Form form = (Wshp009Form)actionForm;
        StringBuffer forward = new StringBuffer();
        AsnProgressInformationDomain criteria = new AsnProgressInformationDomain();
        List<AsnProgressInformationReturnDomain> asnProgressInformationList = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP009_LASNNO))
            {
                this.setPlantCombobox(form);
                form.setFirstPages(null);
                form.setPages(Constants.PAGE_CLICK);
                criteria.setLocale(locale);
                asnProgressInformationList = this.searchAsnProgressInformation(
                    form, criteria, false);
                if(null != asnProgressInformationList 
                    && Constants.ZERO < asnProgressInformationList.size()){
                    form.setAsnProgressList(asnProgressInformationList);
                    SpsPagingUtil.setFormForPaging(criteria, form);
                }else{
                    errorMessageList = criteria.getErrorMessageList();
                }
                this.asnProgressInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP009_LASNNO, locale);
            }
            
            forward.append(SupplierPortalConstant.URL_WSHP004_ACTION);
            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                .append(SupplierPortalConstant.SCREEN_ID_WSHP009);
            forward.append(SupplierPortalConstant.URL_PARAM_ASNNO).append(form.getSelectedAsnNo());
            forward.append(SupplierPortalConstant.URL_PARAM_DCD).append(form.getSelectedDCd());
            forward.append(SupplierPortalConstant.URL_PARAM_TEMPORARY_MODE)
                .append(Constants.STR_ZERO);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP009_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download legend file.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp009Form form = (Wshp009Form)actionForm;
        OutputStream outputStream = null;
        AsnProgressInformationDomain asnProgressInformation = 
            new AsnProgressInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            String legendFileId = ContextParams.getLegendFileId();
            asnProgressInformation.setFileId(legendFileId);
            FileManagementDomain fileManagementDomain = 
                asnProgressInformationFacadeService.searchFileName(asnProgressInformation);
            String pdfFileName = fileManagementDomain.getFileName();
            
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            outputStream = response.getOutputStream();
            asnProgressInformationFacadeService.searchLegendInfo(
                asnProgressInformation, outputStream);
            outputStream.flush();
            return null;
        }catch(ApplicationException e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            this.setPlantCombobox(form);
            if(null != outputStream){
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);
        }
        this.setInitialCriteria(form);
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do view ship notice.
     * <p>View ship notice.</p>
     * <ul>
     * <li>In case click link on search result, the system group selected D/O from checkbox.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doForwardPage(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception{
        Wshp009Form form = (Wshp009Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<AsnProgressInformationReturnDomain> asnProgressInformationList = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        AsnProgressInformationDomain criteria = new AsnProgressInformationDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            if(!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP009_LINVOICENO)){
                this.asnProgressInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP009_LINVOICENO, locale);
            }
            forward.append(SupplierPortalConstant.URL_WINV004_ACTION);
            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_INVOICE_ID);
            forward.append(form.getInvoiceIdCriteria());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID);
            forward.append(SupplierPortalConstant.SCREEN_ID_WSHP009);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP009_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        this.setPlantCombobox(form);
        form.setFirstPages(null);
        form.setPages(Constants.PAGE_CLICK);
        criteria.setLocale(locale);
        asnProgressInformationList = this.searchAsnProgressInformation(form, criteria, false);
        if(null != asnProgressInformationList 
            && Constants.ZERO < asnProgressInformationList.size()){
            form.setAsnProgressList(asnProgressInformationList);
            SpsPagingUtil.setFormForPaging(criteria, form);
        }else{
            errorMessageList = criteria.getErrorMessageList();
        }
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * <p>Denso company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when company combo box change load new denso plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp009Form form = (Wshp009Form)actionForm;
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        try{
            UserLoginDomain userLogin = this.getUserLoginInformation();
            DataScopeControlDomain dataScopeControlDomain
                = this.getDataScopeControlForUserLogin(userLogin);
            
            PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
            if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                plantDensoWithScope.getPlantDensoDomain().setDCd(form.getDCd());
            }
            plantDensoWithScope.setLocale(locale);
            plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = asnProgressInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScope);
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * <p>Supplier company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when supplier company code combo box change load new supplier plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp009Form form = (Wshp009Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<PlantSupplierDomain> plantSupplierList = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControlDomain
                = this.getDataScopeControlForUserLogin(userLogin);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if(!Constants.MISC_CODE_ALL.equals(form.getVendorCd())){
                plantSupplier.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControlDomain);
            plantSupplierList = asnProgressInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplier);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Search company DENSO Code by selected plant DENSO Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp009Form form = (Wshp009Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControlDomain
                = this.getDataScopeControlForUserLogin(userLogin);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            /* 3. Call Facade Service AsnProgressInformationFacadeService
             * searchSelectedPlantDenso() to get the list of company denso code by 
             * denso plant code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setLocale(locale);
            plantDenso.setDataScopeControlDomain(dataScopeControlDomain);
            companyList = this.asnProgressInformationFacadeService.searchSelectedPlantDenso(
                plantDenso);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
    * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
    * 
    * @param actionMapping the action mapping
    * @param actionForm the action form
    * @param request the request
    * @param response the response
    * @return the action forward
    * @throws Exception the exception
    */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp009Form form = (Wshp009Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companySupplierList = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginInformation();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControlDomain
                = this.getDataScopeControlForUserLogin(userLogin);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            /* 3. Call Facade Service AsnProgressInformationFacadeService
             * searchSelectedPlantDenso() to get the list of company supplier code by 
             * supplier plant code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControlDomain);
            
            companySupplierList = this.asnProgressInformationFacadeService
                .searchSelectedPlantSupplier(plantSupplier);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companySupplierList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_PAGING, 
            SupplierPortalConstant.METHOD_DO_PAGING);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET, 
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH, 
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_FORWARD_PAGE, 
            SupplierPortalConstant.METHOD_DO_FORWARD_PAGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE, 
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL, 
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SEARCH, 
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN, 
            SupplierPortalConstant.METHOD_DO_INITIAL_BY_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD, 
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD, 
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN, 
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LINK_ASNNO, 
            SupplierPortalConstant.METHOD_DO_CLINK_LINK_ASNNO);
        return keyMethodMap;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return userLoginDomain the user login domain 
     */
    private UserLoginDomain getUserLoginInformation()
    {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * <p>Initial value to combo box when on load page</p>
     * <ul>
     * <li>if action is initial new all list and set to combo box result area.</li>
     * <li>else user select data set data to combo box result area.</li>
     * </ul>
     * 
     * @param form the form
     * @param asnProgressInformation the asnProgressInformation domain
     * @throws Exception 
     */
    private void setComboboxInitial(Wshp009Form form, AsnProgressInformationDomain 
        asnProgressInformation) throws Exception
    {
        //initial screen
        UserLoginDomain userLoginDomain = this.getUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        asnProgressInformation.setDataScopeControlDomain(dataScopeControlDomain);
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        asnProgressInformationFacadeService.searchInitial(asnProgressInformation);
        
        form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
            asnProgressInformation.getDataScopeControlDomain()
                .getDensoSupplierRelationDomainList(), SupplierPortalConstant.SCREEN_ID_WSHP009));
        form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
            asnProgressInformation.getDataScopeControlDomain()
                .getDensoSupplierRelationDomainList(), SupplierPortalConstant.SCREEN_ID_WSHP009));
        
        form.setCompanySupplierList(asnProgressInformation.getCompanySupplierList());
        form.setCompanyDensoList(asnProgressInformation.getCompanyDensoList());
        form.setPlantSupplierList(asnProgressInformation.getPlantSupplierList());
        form.setPlantDensoList(asnProgressInformation.getPlantDensoList());
        form.setAsnStatusList(asnProgressInformation.getAsnStatusList());
        form.setInvoiceStatusList(asnProgressInformation.getInvoiceStatusList());
    }
    
    /**
     * <p>Do initial return</p>
     * <ul>
     * <li>Initial form when return from other screens.</li>
     * </ul>
     * 
     * @param form the form
     * @param asnProgressInformation the asnProgressInformation domain
     * @throws ApplicationException ApplicationException
     */
    private void doInitialForOtherPages(Wshp009Form form,
        AsnProgressInformationDomain asnProgressInformation) throws ApplicationException
    {
        List<AsnProgressInformationReturnDomain> asnProgressInformationList = null;
        boolean isReturnAction = false;
        if(null != DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WSHP009_FORM)){
            Wshp009Form sessionForm = (Wshp009Form)DensoContext.get().getGeneralArea(
                SupplierPortalConstant.SESSION_WSHP009_FORM);
            form.setActualEtaFrom(sessionForm.getActualEtaFrom());
            form.setActualEtaTo(sessionForm.getActualEtaTo());
            form.setActualEtdFrom(sessionForm.getActualEtdFrom());
            form.setActualEtdTo(sessionForm.getActualEtdTo());
            form.setInvoiceDateFrom(sessionForm.getInvoiceDateFrom());
            form.setInvoiceDateTo(sessionForm.getInvoiceDateTo());
            form.setVendorCd(sessionForm.getVendorCd());
            form.setSPcd(sessionForm.getSPcd());
            form.setDCd(sessionForm.getDCd());
            form.setDPcd(sessionForm.getDPcd());
            form.setAsnStatus(sessionForm.getAsnStatus());
            form.setAsnNo(sessionForm.getAsnNo());
            form.setInvoiceNo(sessionForm.getInvoiceNo());
            form.setInvoiceStatus(sessionForm.getInvoiceStatus());
            form.setSPn(sessionForm.getSPn());
            form.setDPn(sessionForm.getDPn());
            form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
            form.setDensoAuthenList(sessionForm.getDensoAuthenList());
            
            form.setFrom(sessionForm.getFrom());
            form.setTo(sessionForm.getTo());
            form.setMax(sessionForm.getMax());
            form.setPages(null);
            form.setCount(sessionForm.getCount());
            form.setPageNo(sessionForm.getPageNo());
            form.setFirstCount(Constants.FIVE);
            DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WSHP009_FORM);
            isReturnAction = true;
        }else{
            form.setAsnStatus(Constants.MISC_CODE_ALL);
        }
        form.setFirstPages(Constants.PAGE_SEARCH);
        asnProgressInformationList =  this.searchAsnProgressInformation(
            form, asnProgressInformation, false);
        if(null != asnProgressInformationList 
            && Constants.ZERO < asnProgressInformationList.size()){
            form.setAsnProgressList(asnProgressInformationList);
            SpsPagingUtil.setFormForPaging(asnProgressInformation, form);
        }else{
            if(Constants.ZERO < asnProgressInformation.getErrorMessageList().size()){
                form.getApplicationMessageList().addAll(
                    asnProgressInformation.getErrorMessageList());
            }
        }
        if(!isReturnAction){
            form.setAsnStatus(Constants.EMPTY_STRING);
            form.setActualEtaFrom(Constants.EMPTY_STRING);
            form.setActualEtaTo(Constants.EMPTY_STRING);
            form.setInvoiceDateFrom(Constants.EMPTY_STRING);
            form.setInvoiceDateTo(Constants.EMPTY_STRING);
            form.setInvoiceNo(Constants.EMPTY_STRING);
            form.setInvoiceStatus(Constants.EMPTY_STRING);
            form.setSPn(Constants.EMPTY_STRING);
            form.setDPn(Constants.EMPTY_STRING);
        }
    }
    
    /**
     * <p>Search acknowledged do information</p>
     * <ul>
     * <li>Search acknowledged do information.</li>
     * </ul>
     * 
     * @param form the form
     * @param criteria the asnProgressInformation domain
     * @param isDownload flag.
     * @return the list of acknowledged do information
     * @throws ApplicationException Exception
     */
    private List<AsnProgressInformationReturnDomain> searchAsnProgressInformation (
        Wshp009Form form, AsnProgressInformationDomain criteria, boolean isDownload)
        throws ApplicationException
    {
        List<AsnProgressInformationReturnDomain> asnProgressInformationList = null;
        String firstPage = form.getFirstPages();
        
        if(Constants.PAGE_SEARCH.equals(firstPage)){
            criteria.setPageNumber(Constants.ONE);
        }else{
            criteria.setPageNumber(form.getPageNo());
        }
        
        if(!StringUtil.checkNullOrEmpty(form.getActualEtaFrom())){
            criteria.setActualEtaFrom(form.getActualEtaFrom());
        }
        if(!StringUtil.checkNullOrEmpty(form.getActualEtaTo())){
            criteria.setActualEtaTo(form.getActualEtaTo());
        }
        if(!StringUtil.checkNullOrEmpty(form.getActualEtdFrom())){
            criteria.setActualEtdFrom(form.getActualEtdFrom());
        }
        if(!StringUtil.checkNullOrEmpty(form.getActualEtdTo())){
            criteria.setActualEtdTo(form.getActualEtdTo());
        }
        if(!StringUtil.checkNullOrEmpty(form.getInvoiceDateFrom())){
            criteria.setInvoiceDateFrom(form.getInvoiceDateFrom());
        }
        if(!StringUtil.checkNullOrEmpty(form.getInvoiceDateTo())){
            criteria.setInvoiceDateTo(form.getInvoiceDateTo());
        }
        if(!StringUtil.checkNullOrEmpty(form.getVendorCd())){
            criteria.setVendorCd(form.getVendorCd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getSPcd())){
            criteria.setSPcd(form.getSPcd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDCd())){
            criteria.setDCd(form.getDCd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDPcd())){
            criteria.setDPcd(form.getDPcd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getAsnNo())){
            criteria.setAsnNo(form.getAsnNo().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getAsnStatus())){
            criteria.setAsnStatus(form.getAsnStatus());
        }
        if(!StringUtil.checkNullOrEmpty(form.getInvoiceNo())){
            criteria.setInvoiceNo(form.getInvoiceNo().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getInvoiceStatus())){
            criteria.setInvoiceStatus(form.getInvoiceStatus());
        }
        if(!StringUtil.checkNullOrEmpty(form.getSPn())){
            criteria.setSPn(form.getSPn().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDPn())){
            criteria.setDPn(form.getDPn().toUpperCase().trim());
        }
        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        criteria.setDensoAuthenList(form.getDensoAuthenList());
        
        if(!isDownload){
            asnProgressInformationList = asnProgressInformationFacadeService
                .searchAsnProgressInformation(criteria);
        }else{
            asnProgressInformationList = asnProgressInformationFacadeService
                .searchAsnProgressInformationCsv(criteria);
        }
        return asnProgressInformationList;
    }
    
    /**
     * Get Data Scope Control for current user login.
     * @param userLogin User Login Information
     * @return Data Scope Control
     * */
    private DataScopeControlDomain getDataScopeControlForUserLogin(UserLoginDomain userLogin)
    {
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        dataScopeControl.setUserType(userLogin.getUserType());
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList())
        {
            if (SupplierPortalConstant.SCREEN_ID_WSHP009.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
            }
        }
        dataScopeControl.setDensoSupplierRelationDomainList(null);
        return dataScopeControl;
    }
    
    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the Wshp009Form
     * */
    private void setPlantCombobox(Wshp009Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        
        if (!Strings.judgeBlank(form.getVendorCd())){
            List<PlantSupplierDomain> plantSupplierList = null;
            try{
                PlantSupplierWithScopeDomain plantSupplierWithScope
                    = new PlantSupplierWithScopeDomain();
                
                if(!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplierWithScope.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
                }
                plantSupplierWithScope.setLocale(locale);
                plantSupplierWithScope.setDataScopeControlDomain(dataScopeControl);
                plantSupplierList = asnProgressInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplierWithScope);
                form.setPlantSupplierList(plantSupplierList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd())){
            List<PlantDensoDomain> plantDensoList = null;
            try{
                PlantDensoWithScopeDomain plantDensoWithScope
                    = new PlantDensoWithScopeDomain();
                if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                    plantDensoWithScope.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDensoWithScope.setLocale(locale);
                plantDensoWithScope.setDataScopeControlDomain(dataScopeControl);
                plantDensoList = this.asnProgressInformationFacadeService
                    .searchSelectedCompanyDenso(plantDensoWithScope);
                form.setPlantDensoList(plantDensoList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
    }
    
    /**
     * Sets Initial data for search criteria.
     * @param form the Wshp009Form
     * */
    private void setInitialCriteria(Wshp009Form form){
        form.setAsnProgressList(null);
        String currentDateTimeStr = DateUtil.format(
            Calendar.getInstance().getTime(), DateUtil.PATTERN_YYYYMMDD_SLASH);
        form.setActualEtaFrom(Constants.EMPTY_STRING);
        form.setActualEtaTo(Constants.EMPTY_STRING);
        form.setInvoiceDateFrom(Constants.EMPTY_STRING);
        form.setInvoiceDateTo(Constants.EMPTY_STRING);
        form.setVendorCd(Constants.EMPTY_STRING);
        form.setSPcd(Constants.EMPTY_STRING);
        form.setDCd(Constants.EMPTY_STRING);
        form.setDPcd(Constants.EMPTY_STRING);
        form.setAsnStatus(Constants.EMPTY_STRING);
        form.setAsnNo(Constants.EMPTY_STRING);
        form.setInvoiceNo(Constants.EMPTY_STRING);
        form.setInvoiceStatus(Constants.EMPTY_STRING);
        form.setSPn(Constants.EMPTY_STRING);
        form.setDPn(Constants.EMPTY_STRING);
        form.setActualEtdFrom(currentDateTimeStr);
        form.setActualEtdTo(currentDateTimeStr);
    }
}
