/*
 * ModifyDate Development company     Describe 
 * 2014/07/29 CSI Chatchai            Create
 * 2015/12/25 CSI Akat                [ADDITIONAL]
 * 2017/08/22 Netband U.Rungsiwut     SPS phase II
 * 2018/04/11 Netband U.Rungsiwut     Generate D/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.business.service.DeliveryOrderInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Word006Form;

/**
 * The Class DeliveryOrderInformationAction.
 * 
 * @author CSI
 */
public class DeliveryOrderInformationAction extends CoreAction {

    /**
     * <p>
     * Call Facade Service DeliveryOrderInformation.
     * </p>
     */
    private DeliveryOrderInformationFacadeService deliveryOrderInformationFacadeService;
    
    /** The default Constructor */
    public DeliveryOrderInformationAction() {

    }

    /**
     * <p>
     * Setter method for deliveryOrderInformationFacadeService.
     * </p>
     * 
     * @param deliveryOrderInformationFacadeService Set for
     *            deliveryOrderInformationFacadeService
     */
    public void setDeliveryOrderInformationFacadeService(
        DeliveryOrderInformationFacadeService deliveryOrderInformationFacadeService) {
        this.deliveryOrderInformationFacadeService = deliveryOrderInformationFacadeService;
    }

    /**
     * Do initial.
     * <p>
     * Initial data when load page.
     * </p>
     * <ul>
     * <li>Show user information.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word006Form form = (Word006Form)actionForm;
        form.resetForm();

        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        /** Call private action method to get current user login and roles from DENSO-context. */
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();

        DeliveryOrderInformationDomain deliveryOrderInformationDomain = 
            new DeliveryOrderInformationDomain();
        
        form.setUserLogin(setUserLogin());
        
        /** Call Façade Service DeliveryOrderInformationFacadeService searchInitial(). */
        try {
            deliveryOrderInformationDomain = deliveryOrderInformationFacadeService
                .searchInitial(dataScopeControlDomain);

            /** Set return value of previous doInitial to ActionForm */
            Date date = new Date(DateUtil.getDateTime());
            Date addissueDateFrom = DateUtil.addDays(date, Constants.MINUS_ONE);
            
            String delissueDateFromFormat = DateUtil.format(addissueDateFrom, 
                DateUtil.PATTERN_YYYYMMDD_SLASH);
            String issueDateFromFormat = DateUtil.format(date, 
                DateUtil.PATTERN_YYYYMMDD_SLASH);

            form.setIssueDateFrom(delissueDateFromFormat);
            form.setIssueDateTo(issueDateFromFormat);
            form.setDeliveryDateFrom(null);
            form.setDeliveryDateTo(null);
            form.setDeliveryTimeFrom(null);
            form.setDeliveryTimeTo(null);
            form.setShipDateFrom(null);
            form.setShipDateTo(null);
            form.setRouteNo(null);
            form.setDelNo(null);
            form.setSpsDoNo(null);
            form.setCigmaDoNo(null);
            
            // [ADDITIONAL] : Mr. Sittichai request to set default Revision combobox to 'Latest'
            form.setRevision(Constants.STR_ONE);

            form.setRevisionList(deliveryOrderInformationDomain.getMiscRevision());
            form.setShipmentStatusList(deliveryOrderInformationDomain.getMiscShipmentStatusCb());
            form.setTransportModeList(deliveryOrderInformationDomain.getTransportModeCb());
            form.setCompanySupplierList(deliveryOrderInformationDomain.getCompanySupplierList());
            form.setPlantSupplierList(deliveryOrderInformationDomain.getPlantSupplierList());
            form.setCompanyDensoList(deliveryOrderInformationDomain.getCompanyDensoList());
            form.setPlantDensoList(deliveryOrderInformationDomain.getPlantDensoList());

            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                deliveryOrderInformationDomain.getDensoSupplierRelationList()
                , SupplierPortalConstant.SCREEN_ID_WORD006));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                deliveryOrderInformationDomain.getDensoSupplierRelationList()
                , SupplierPortalConstant.SCREEN_ID_WORD006));
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD006_BRESET)) 
                {
                    this.deliveryOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD006_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }

            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD006_BDOWNLOAD)) 
                {
                    this.deliveryOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD006_BDOWNLOAD, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotDownloadCsvMessage(applicationException.getMessage());
            }
            
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Do search.
     * <p>
     * Search data.
     * </p>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word006Form form = (Word006Form)actionForm;
        DeliveryOrderInformationDomain criteria = new DeliveryOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        /** Call private action method to get current user login and roles from DENSO-context. */
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);
        
        form.setUserLogin(setUserLogin());
        
        try {
            form.setMode(SupplierPortalConstant.METHOD_DO_SEARCH);
        
            String firstPages = form.getFirstPages();
            String pages = form.getPages();

            /** Call Façade Service DeliveryOrderInformationFacadeService searchDeliveryOrderInformation(). */
            if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD006_BSEARCH)) 
                {
                    this.deliveryOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD006_BSEARCH, locale);
                }
                
                setParameterCriteria(form, firstPages, criteria);
                criteria.setLocale(locale);

                DeliveryOrderInformationResultDomain deliveryOrderInformationResultDomain = 
                    new DeliveryOrderInformationResultDomain();
                deliveryOrderInformationResultDomain = deliveryOrderInformationFacadeService
                    .searchDeliveryOrderInformation(criteria);
                if (null != deliveryOrderInformationResultDomain
                    .getErrorMessageList()) {
                    errorMessageList = deliveryOrderInformationResultDomain
                        .getErrorMessageList();
                } else {
                    /** Set return value of previous doSearch to WORD006Form */
                    form.setDoInfoList(deliveryOrderInformationResultDomain
                        .getDeliveryOrderDetailDomain());
                    SpsPagingUtil.setFormForPaging(criteria, form);
                }
            }
        
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * <p>
     * Select SCd.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return the ActionForward
     * @throws Exception the Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        /** Call private action method to get current user login information and roles from DENSO-context. */
        Word006Form form = (Word006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);

        /** Call Façade Service DeliveryOrderInformationFacadeService changeSelectSupplierCompany() to get the list of supplier plant code by supplier company code. */
        PlantSupplierWithScopeDomain plantSupplierWithScope = 
            new PlantSupplierWithScopeDomain();
        
        if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
            plantSupplierWithScope.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
        }
        plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScope.setLocale(locale);

        List<PlantSupplierDomain> plantSupplierDomain = new ArrayList<PlantSupplierDomain>();
        
        form.setUserLogin(setUserLogin());

        try {
            plantSupplierDomain = deliveryOrderInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScope);
            
            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierDomain), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        
        return null;
    }

    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word006Form form = (Word006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getLoginUser();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * searchCompanyBySelectSupplierPlant() to get the list of supplier company code by
             * supplier plant code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            companyList = this.deliveryOrderInformationFacadeService.searchSelectedPlantSupplier(
                plantSupplier);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * <p>
     * Select DCd.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        /** Call private action method to get current user login information and roles from DENSO-context. */
        Word006Form form = (Word006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);

        /** Call Façade Service DeliveryOrderInformationFacadeService changeSelectDENSOCompany() to get the list of DENSO plant code by DENSO company code. */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
        }
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantDensoWithScopeDomain.setLocale(locale);

        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
        form.setUserLogin(setUserLogin());

        try {
            plantDensoDomain = deliveryOrderInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)plantDensoDomain), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Search DENSO company Code by selected DENSO plant Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word006Form form = (Word006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getLoginUser();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * changeSelectSupplierCompany() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            companyList = this.deliveryOrderInformationFacadeService.searchSelectedPlantDenso(
                plantDenso);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * <p>
     * Download CSV.
     * </p>
     * 
     * @param actionMapping the action Mapping
     * @param actionForm the action Form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the Exception
     */
    public ActionForward doDownload(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        
        Word006Form form = (Word006Form)actionForm;
        DeliveryOrderInformationDomain criteria = new DeliveryOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        List<UserRoleDomain> roleUserList = null;
        for (RoleScreenDomain roleScreen : userLoginDomain
            .getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WORD006.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        dataScopeControlDomain.setLocale(locale);
        
        form.setUserLogin(setUserLogin());
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());
        
        try {
            form.setMode(SupplierPortalConstant.METHOD_DO_SEARCH);
            
            String firstPages = form.getFirstPages();
            String pages = form.getPages();
            
            
            /** Call Façade Service DeliveryOrderInformationFacadeService searchDeliveryOrderInformationCSV() to get data and create file. */
            if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
                setParameterCriteria(form, firstPages, criteria);
                criteria.setLocale(locale);
                
                DeliveryOrderInformationResultDomain deliveryOrderInformationResultDomain = 
                    new DeliveryOrderInformationResultDomain();
                deliveryOrderInformationResultDomain = deliveryOrderInformationFacadeService
                    .searchDeliveryOrderInformationCsv(criteria);
                if (null != deliveryOrderInformationResultDomain.getErrorMessageList()) {
                    errorMessageList = deliveryOrderInformationResultDomain.getErrorMessageList();
                } else {
                    
                    /** Get created CSV file from DeliveryOrderInformationFacadeService. */
                    form.setDoInfoList(
                        deliveryOrderInformationResultDomain.getDeliveryOrderDetailDomain());
                    
                    String fileName = null;
                    StringBuffer resultStr = deliveryOrderInformationResultDomain.getResultString();
                    if (null != resultStr) {
                        Calendar cal = Calendar.getInstance();
                        fileName = StringUtil.appendsString(
                            Constants.DELIVERY_ORDER_INFORMATION_LIST,
                            Constants.SYMBOL_UNDER_SCORE,
                            DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                        
                        byte[] output = resultStr.toString().getBytes();
                        setHttpHeaderForCsv(response, fileName);
                        outputStream = response.getOutputStream();
                        outputStream.write(output, Constants.ZERO,
                            output.length);
                        outputStream.flush();
                        
                    } else {
                        errorMessageList = deliveryOrderInformationResultDomain
                            .getErrorMessageList();
                    }
                }
            }
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0012);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                message));
        } finally {
            if(null != outputStream){
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }

    /**
     * <p>
     * Download PDF.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doDownloadPdf(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word006Form form = (Word006Form)actionForm;
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = 
            new DeliveryOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        FileManagementDomain fileManagementDomain = null;
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());

        try {
            UserLoginDomain userLoginDomain = this.getLoginUser();
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD006_LVIEWPDF)) 
            {
                this.deliveryOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD006_LVIEWPDF, locale);
            }
            
            deliveryOrderInformationDomain.setPdfFileId(form.getPdfFileId());
            deliveryOrderInformationDomain.setLocale(locale);

            /** Call Façade Service DeliveryOrderInformationFacadeService searchDeliveryOrderReport() to get PDF file. */
            fileManagementDomain = deliveryOrderInformationFacadeService
                .searchGenerateDo(deliveryOrderInformationDomain);
            
            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());

//            fileManagementDomain = deliveryOrderInformationFacadeService.searchDeliveryOrderReport(
//                deliveryOrderInformationDomain, response.getOutputStream());
            form.setMessageType(Constants.MESSAGE_SUCCESS);
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }

    /**
     * <p>
     * Download Kanban PDF.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doDownloadOneWayKanbanPdf(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word006Form form = (Word006Form)actionForm;
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = 
            new DeliveryOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());

        try {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_W6_0012);
            new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message);
            UserLoginDomain userLoginDomain = this.getLoginUser();
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD006_LVIEWPDF)) 
            {
                this.deliveryOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD006_LVIEWPDF, locale);
            }
            
            /** Call Façade Service DeliveryOrderInformationFacadeService searchFileName() to get PDF file name */
            deliveryOrderInformationDomain.setDoId(form.getDoId());
            deliveryOrderInformationDomain.setLocale(locale);
            
            InputStream input = deliveryOrderInformationFacadeService.createOneWayKanbanTagReport(deliveryOrderInformationDomain);
            
            if(!StringUtil.checkNullOrEmpty(input)){
                if(form.getPdfType().equals(Constants.STR_KB)){
                    if(form.getPdfFileKbTagPrintDate().equals(Constants.STR_N)){
                        deliveryOrderInformationFacadeService
                        .updateDateTimeKanbanPdf(deliveryOrderInformationDomain);
                    }
                }
                StringBuffer fileName = new StringBuffer();
                fileName.append(SupplierPortalConstant.ONE_WAY_KANBAN_TAG);
                fileName.append(deliveryOrderInformationDomain.getDoId());
                fileName.append(Constants.SYMBOL_DOT);
                fileName.append(Constants.REPORT_PDF_FORMAT);
                
                this.setHttpHeaderForFileManagement(response, request, fileName.toString());
                FileUtil.writeInputStreamToOutputStream(input, response.getOutputStream());
                return null;
            }
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0017);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }
    /**
     * <p>
     * Click link more.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word006Form form = (Word006Form)actionForm;
        DeliveryOrderInformationDomain deliveryOrderInformationDomain = 
            new DeliveryOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        FileManagementDomain fileManagementDomain = null;
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());
        
        /** Call Façade Service DeliveryOrderInformationFacadeService searchFileName() to get PDF file name. */
        try {
            String legendFileId = ContextParams.getLegendFileId();
            deliveryOrderInformationDomain.setPdfFileId(legendFileId);
            deliveryOrderInformationDomain.setLocale(locale);
            
            fileManagementDomain = deliveryOrderInformationFacadeService
                .searchFileName(deliveryOrderInformationDomain);
            String pdfFileName = fileManagementDomain.getFileName();
            
            outputStream = response.getOutputStream();

            super
                .setHttpHeaderForFileManagement(response, request, pdfFileName);

            /** Call Façade Service DeliveryOrderInformationFacadeService searchLegendInfo() to get PDF file. */
            fileManagementDomain = deliveryOrderInformationFacadeService.searchLegendInfo(
                deliveryOrderInformationDomain, outputStream);
            form.setMessageType(Constants.MESSAGE_SUCCESS);
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.apache.struts.actions.LookupDispatchAction#getKeyMethodMap()
     */
    @Override
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE,
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_ONE_WAY_KANBAN_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_ONE_WAY_KANBAN_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        
        return keyMethodMap;
    }

    /**
     * <p>
     * Set parameter criteria.
     * </p>
     * 
     * @param form form
     * @param firstPages firstPages
     * @param criteria criteria
     */
    private void setParameterCriteria(Word006Form form, String firstPages,
        DeliveryOrderInformationDomain criteria) {

        if (!StringUtil.checkNullOrEmpty(form.getDeliveryDateFrom())) {
            criteria.setDeliveryDateFrom(form.getDeliveryDateFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDeliveryDateTo())) {
            criteria.setDeliveryDateTo(form.getDeliveryDateTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDeliveryTimeFrom())) {
            criteria.setDeliveryTimeFrom(form.getDeliveryTimeFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDeliveryTimeTo())) {
            criteria.setDeliveryTimeTo(form.getDeliveryTimeTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSCd())) {
            criteria.setSCd(form.getSCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getVendorCd())) {
            criteria.setVendorCd(form.getVendorCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSPcd())) {
            criteria.setSPcd(form.getSPcd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDCd())) {
            criteria.setDCd(form.getDCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDPcd())) {
            criteria.setDPcd(form.getDPcd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getShipmentStatus())) {
            criteria.setShipmentStatus(form.getShipmentStatus());
        }
        if (!StringUtil.checkNullOrEmpty(form.getTransportMode())) {
            criteria.setTransportMode(form.getTransportMode());
        }
        if (!StringUtil.checkNullOrEmpty(form.getRouteNo())) {
            criteria.setRouteNo(form.getRouteNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDelNo())) {
            criteria.setDel(form.getDelNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getShipDateFrom())) {
            criteria.setShipDateFrom(form.getShipDateFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getShipDateTo())) {
            criteria.setShipDateTo(form.getShipDateTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getIssueDateFrom())) {
            criteria.setIssueDateFrom(form.getIssueDateFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getIssueDateTo())) {
            criteria.setIssueDateTo(form.getIssueDateTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getRevision())) {
            criteria.setRevision(form.getRevision());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSpsDoNo())) {
            criteria.setSpsDoNo(form.getSpsDoNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getCigmaDoNo())) {
            criteria.setCigmaDoNo(form.getCigmaDoNo().toUpperCase().trim());
        }
        if (Constants.PAGE_SEARCH.equals(firstPages)) {
            criteria.setPageNumber(Constants.ONE);
        } else {
            criteria.setPageNumber(form.getPageNo());
        }
        
        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        criteria.setDensoAuthenList(form.getDensoAuthenList());
        
    }

    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the Word006Form
     * @return set of application message
     * */
    private Set<String> setPlantCombobox(Word006Form form) {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getLoginUser();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);
        
        if (!Strings.judgeBlank(form.getVendorCd())) {
            List<PlantSupplierDomain> plantList = null;
            
            try{
                PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplier.getPlantSupplierDomain().setVendorCd(
                        form.getVendorCd());
                }
                plantSupplier.setDataScopeControlDomain(dataScopeControl);
                plantList = this.deliveryOrderInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplier);
                
                form.setPlantSupplierList(plantList);
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd())) {
            List<PlantDensoDomain> plantList = null;
            
            try{
                PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                    plantDenso.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDenso.setDataScopeControlDomain(dataScopeControl);
                plantList = this.deliveryOrderInformationFacadeService
                    .searchSelectedCompanyDenso(plantDenso);
                form.setPlantDensoList(plantList);
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            }
        }
        
        return applicationMessageSet;
    }
    
    /**
     * Get Data Scope Control for current user login.
     * @param userLogin User Login Information
     * @return Data Scope Control
     * */
    private DataScopeControlDomain getDataScopeControlForUserLogin(UserLoginDomain userLogin) {
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        dataScopeControl.setUserType(userLogin.getUserType());
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WORD006.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd()))
            {
                dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
            }
        }
        dataScopeControl.setDensoSupplierRelationDomainList(null);
        return dataScopeControl;
    }
    
    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getLoginUser() {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
}