/*
 * ModifyDate Developmentcompany Describe 
 * 2014/08/08 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationUploadingDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.InvoiceInformationUploadingFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.presentation.form.Winv003Form;

/**
 * The Class InvoiceInformationUploadingAction.
 * @author CSI
 */
public class InvoiceInformationUploadingAction extends CoreAction {
    
    /** The invoice information uploading facade service. */
    private InvoiceInformationUploadingFacadeService 
    invoiceInformationUploadingFacadeService = null;
    
    /** The default constructor. */
    public InvoiceInformationUploadingAction() {
        super();
    }
    
    /**
     * Set the invoice information uploading facade service.
     * 
     * @param invoiceInformationUploadingFacadeService the invoice information uploading facade service to set
     */
    public void setInvoiceInformationUploadingFacadeService(
        InvoiceInformationUploadingFacadeService invoiceInformationUploadingFacadeService) {
        this.invoiceInformationUploadingFacadeService = invoiceInformationUploadingFacadeService;
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv003Form form = (Winv003Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceInformationUploadingDomain result  = null;
        try {
            UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            form.resetForm();
            form.setSessionId(request.getSession().getId());
            form.setUserLogin(super.setUserLogin());
            
            InvoiceInformationUploadingDomain invoiceInformationUploadingDomain 
                = new InvoiceInformationUploadingDomain();
            invoiceInformationUploadingDomain.setSessionId(form.getSessionId());
            invoiceInformationUploadingDomain.setDscId(userLoginDomain.getDscId());
            this.invoiceInformationUploadingFacadeService.deleteTmpUploadInvoice(
                invoiceInformationUploadingDomain);
            
            this.invoiceInformationUploadingFacadeService.deleteTmpUploadInvoice(
                null);
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV003_BDOWNLOAD)) 
                {
                    this.invoiceInformationUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV003_BDOWNLOAD, locale);
                }
                form.setCannotDownloadMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotDownloadMessage( ae.getMessage() );
            }
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV003_BRESET)) 
                {
                    this.invoiceInformationUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV003_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }
        }catch (Exception e) {
            
            result = new InvoiceInformationUploadingDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
            this.setMessageOnScreen(form, result);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do return.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv003Form form = (Winv003Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Winv003Form sessionForm = (Winv003Form)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WINV003_FORM);
        
        InvoiceInformationUploadingDomain result  = null;
        try {
            this.setCriteriaOnScreen(sessionForm, form);
            UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
            form.setUserLogin(super.setUserLogin());
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            
            InvoiceInformationUploadingDomain invoiceInformationUploadingDomain 
                = new InvoiceInformationUploadingDomain();
            invoiceInformationUploadingDomain.setSessionId(sessionForm.getSessionId());
            invoiceInformationUploadingDomain.setDscId(sessionForm.getDscId());
            invoiceInformationUploadingDomain.setPageNumber(form.getPageNo());
            result = this.invoiceInformationUploadingFacadeService.searchByReturn(
                invoiceInformationUploadingDomain);
            
            form.setTotalRecord(result.getTotalRecord());
            form.setCorrectRecord(result.getCorrectRecord());
            form.setWarningRecord(result.getWarningRecord());
            form.setErrorRecord(result.getErrorRecord());
            form.setInvoiceNo(result.getInvoiceNo());
            form.setVendorCd(result.getVendorCd());
            form.setSupplierTaxId(result.getSupplierTaxId());
            form.setDCd(result.getDCd());
            form.setDPcd(result.getDPcd());
            form.setCannotResetMessage(sessionForm.getCannotResetMessage());
            form.setCannotDownloadMessage(sessionForm.getCannotDownloadMessage());
            SpsPagingUtil.setFormForPaging(invoiceInformationUploadingDomain, form);
            form.setTmpUploadInvoiceResultList(result.getTmpUploadInvoiceResultList());
        }catch (ApplicationException ae) {
            result = new InvoiceInformationUploadingDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        }catch (Exception e) {
            result = new InvoiceInformationUploadingDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }finally{
            this.setMessageOnScreen(form, result);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do upload, save file using file manager stream.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doUpload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv003Form form = (Winv003Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain 
            = new InvoiceInformationUploadingDomain();
        
        InvoiceInformationUploadingDomain result  = null;
        int maxCSVFileSize = Constants.ZERO;
        try {
            UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            String sessionId = request.getSession().getId();
            
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV003_BUPLOAD)){
                this.invoiceInformationUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV003_BUPLOAD, locale);
            }
            
            if(Constants.PAGE_SEARCH.equals(form.getFirstPages())){
                InvoiceInformationUploadingDomain criteriaForClearTemp 
                    = new InvoiceInformationUploadingDomain();
                criteriaForClearTemp.setSessionId(sessionId);
                criteriaForClearTemp.setDscId(userLoginDomain.getDscId());
                this.invoiceInformationUploadingFacadeService.deleteTmpUploadInvoice(
                    criteriaForClearTemp);
                
                if(Constants.ZERO == form.getFileData().getFileSize()){
                    MessageUtil.throwsApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0014);
                }
                
                maxCSVFileSize = ContextParams.getMaxCsvFileSize();
                if(maxCSVFileSize < form.getFileData().getFileSize()){
                    MessageUtil.throwsApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0015,
                        new String[]{String.valueOf(maxCSVFileSize)});
                }
                
                if(!Constants.REPORT_CSV_FORMAT.equals(
                    FileUtil.getTypeOfFile(form.getFileData().getFileName()).toLowerCase())){
                    MessageUtil.throwsApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0013, 
                        new String[]{Constants.REPORT_CSV_FORMAT});
                }
                
                FileManagementDomain fileManagementDomain = new FileManagementDomain();
                fileManagementDomain.setFileData(form.getFileData().getInputStream());
                fileManagementDomain.setFileName(form.getFileData().getFileName());
                
                RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
                DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
                dataScopeControlDomain.setLocale(locale);
                dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
                dataScopeControlDomain.setUserRoleDomainList(
                    roleScreenDomain.getUserRoleDomainList());
                dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
                this.invoiceInformationUploadingFacadeService.searchDensoSupplierRelation(
                    dataScopeControlDomain);

                invoiceInformationUploadingDomain.setFileManagementDomain(fileManagementDomain);
                invoiceInformationUploadingDomain.setSessionId(sessionId);
                invoiceInformationUploadingDomain.setDscId(userLoginDomain.getDscId());
                invoiceInformationUploadingDomain.setPageNumber(Constants.ONE);
                
                
                invoiceInformationUploadingDomain.setSupplierAuthenList(
                    super.doCreateSupplierPlantAuthenList(
                        dataScopeControlDomain.getDensoSupplierRelationDomainList(),
                        SupplierPortalConstant.SCREEN_ID_WINV002));
                invoiceInformationUploadingDomain.setDensoAuthenList(
                    super.doCreateDensoPlantAuthenList(
                        dataScopeControlDomain.getDensoSupplierRelationDomainList(),
                        SupplierPortalConstant.SCREEN_ID_WINV002));
                
                result = this.invoiceInformationUploadingFacadeService.transactInvoice(
                    invoiceInformationUploadingDomain, dataScopeControlDomain);
            }else{
                invoiceInformationUploadingDomain.setSessionId(sessionId);
                invoiceInformationUploadingDomain.setDscId(userLoginDomain.getDscId());
                invoiceInformationUploadingDomain.setPageNumber(form.getPageNo());
                result = this.invoiceInformationUploadingFacadeService.searchByReturn(
                    invoiceInformationUploadingDomain);
            }
            
            if(null == result){
                result = new InvoiceInformationUploadingDomain();
                result.setErrorMessageList(invoiceInformationUploadingDomain.getErrorMessageList());
            }else{
                if(null == result.getErrorMessageList() 
                    ||  result.getErrorMessageList().size() <= Constants.ZERO){
                    form.setTotalRecord(result.getTotalRecord());
                    form.setCorrectRecord(result.getCorrectRecord());
                    form.setWarningRecord(result.getWarningRecord());
                    form.setErrorRecord(result.getErrorRecord());
                    form.setInvoiceNo(result.getInvoiceNo());
                    form.setVendorCd(result.getVendorCd());
                    form.setSupplierTaxId(result.getSupplierTaxId());
                    form.setDCd(result.getDCd());
                    form.setDPcd(result.getDPcd());
                    SpsPagingUtil.setFormForPaging(invoiceInformationUploadingDomain, form);
                    form.setTmpUploadInvoiceResultList(result.getTmpUploadInvoiceResultList());
                    
                    result.getSuccessMessageList().add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, 
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_I6_0010)));
                }
            }
        }catch(ApplicationException ae){
            result = new InvoiceInformationUploadingDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        }catch(Exception e){
            result = new InvoiceInformationUploadingDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }finally{
            this.setMessageOnScreen(form, result);
            if(Constants.PAGE_SEARCH.equals(form.getFirstPages())){
                form.setFileName(form.getFileData().getFileName());
                form.getFileData().destroy();
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download.
     * <p>Download data by criteria from screen.</p>
     * <ul>
     * <li>In case click download button, system search data and count by criteria.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv003Form form = (Winv003Form)actionForm;
        InvoiceInformationUploadingDomain result  = null;
        ServletOutputStream outputStream = null;
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain 
            = new InvoiceInformationUploadingDomain();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try {
            invoiceInformationUploadingDomain.setSessionId(request.getSession().getId());
            invoiceInformationUploadingDomain.setDscId(userLoginDomain.getDscId());
            invoiceInformationUploadingDomain.setLocale(locale);
            
            result = this.invoiceInformationUploadingFacadeService.searchTmpUploadInvoiceCsv(
                invoiceInformationUploadingDomain);
            
            byte[] output = result.getCsvResult().toString().getBytes();
            setHttpHeaderForCsv(response, result.getFileName());
            response.setContentLength(output.length);
            outputStream = response.getOutputStream();
            outputStream.write(output, 0, output.length);
            outputStream.flush();
            return null;
        } catch (ApplicationException ae) {
            result = new InvoiceInformationUploadingDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        }catch (Exception e) {
            result = new InvoiceInformationUploadingDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
        } finally {
            if(null != outputStream) {
                outputStream.close();
            }
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doGroup(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv003Form form = (Winv003Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceInformationUploadingDomain invoiceInformationUploadingDomain 
            = new InvoiceInformationUploadingDomain();
        StringBuffer forward = new StringBuffer();
        InvoiceInformationUploadingDomain result = null;
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try {
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV003_BGROUPINVOICE)){
                form.setPages(Constants.PAGE_CLICK);
                String sessionId = request.getSession().getId();
                invoiceInformationUploadingDomain.setSessionId(sessionId);
                invoiceInformationUploadingDomain.setDscId(userLoginDomain.getDscId());
                invoiceInformationUploadingDomain.setPageNumber(form.getPageNo());
                result = this.invoiceInformationUploadingFacadeService.searchByReturn(
                    invoiceInformationUploadingDomain);
                
                if(null == result.getErrorMessageList() 
                    ||  result.getErrorMessageList().isEmpty()){
                    form.setTotalRecord(result.getTotalRecord());
                    form.setCorrectRecord(result.getCorrectRecord());
                    form.setWarningRecord(result.getWarningRecord());
                    form.setErrorRecord(result.getErrorRecord());
                    form.setInvoiceNo(result.getInvoiceNo());
                    form.setVendorCd(result.getVendorCd());
                    form.setSupplierTaxId(result.getSupplierTaxId());
                    form.setDCd(result.getDCd());
                    form.setDPcd(result.getDPcd());
                    SpsPagingUtil.setFormForPaging(invoiceInformationUploadingDomain, form);
                    form.setTmpUploadInvoiceResultList(result.getTmpUploadInvoiceResultList());
                }
                
                this.invoiceInformationUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV003_BGROUPINVOICE, locale);
            }
            
            String sessionId = request.getSession().getId();
            String dscId = userLoginDomain.getDscId();
            
            invoiceInformationUploadingDomain.setDCd(form.getDCd());
            invoiceInformationUploadingDomain.setSessionId(sessionId);
            invoiceInformationUploadingDomain.setDscId(dscId);
            invoiceInformationUploadingDomain.setLocale(locale);
            result = this.invoiceInformationUploadingFacadeService.searchValidateGroupInvoice(
                invoiceInformationUploadingDomain);
            
            if(null == result.getErrorMessageList() 
                || result.getErrorMessageList().size() <= Constants.ZERO){
                
                UserLoginDomain sessionUserLoginDomain = (UserLoginDomain)DensoContext.get().
                    getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
                
                form.setSessionId(sessionId);
                form.setDscId(dscId);
                
                Winv003Form sessionForm = new Winv003Form();
                this.setCriteriaOnScreen(form, sessionForm);
                DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WINV003_FORM,
                    sessionForm);
                
                forward.append(SupplierPortalConstant.URL_WINV004_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_SESSIONID)
                    .append(request.getSession().getId());
                forward.append(SupplierPortalConstant.URL_PARAM_DSCID)
                    .append(sessionUserLoginDomain.getDscId());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    forward.append(SupplierPortalConstant.URL_PARAM_WARNING_MESSAGE)
                        .append(result.getWarningMessageList().get(Constants.ZERO).getMessage());
                }
                forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                    .append(SupplierPortalConstant.SCREEN_ID_WINV003);
            }else{
                form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                
                invoiceInformationUploadingDomain.setSessionId(sessionId);
                invoiceInformationUploadingDomain.setDscId(dscId);
                invoiceInformationUploadingDomain.setPageNumber(form.getPageNo());
                InvoiceInformationUploadingDomain searchResult 
                    = this.invoiceInformationUploadingFacadeService.searchByReturn(
                        invoiceInformationUploadingDomain);
                form.setTotalRecord(searchResult.getTotalRecord());
                form.setCorrectRecord(searchResult.getCorrectRecord());
                form.setWarningRecord(searchResult.getWarningRecord());
                form.setErrorRecord(searchResult.getErrorRecord());
                form.setInvoiceNo(searchResult.getInvoiceNo());
                form.setVendorCd(searchResult.getVendorCd());
                form.setSupplierTaxId(searchResult.getSupplierTaxId());
                form.setDCd(searchResult.getDCd());
                form.setDPcd(searchResult.getDPcd());
                SpsPagingUtil.setFormForPaging(invoiceInformationUploadingDomain, form);
                form.setTmpUploadInvoiceResultList(searchResult.getTmpUploadInvoiceResultList());
            }
        } catch (ApplicationException ae) {
            result = new InvoiceInformationUploadingDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        } catch (Exception e) {
            result = new InvoiceInformationUploadingDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }
        
        if(Constants.ZERO == forward.length()){
            this.setMessageOnScreen(form, result);
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }else{
            return new ActionForward(forward.toString());
        }
    }
    
    /**
     * setCriteriaOnScreen.
     * <p>Setting data between action form and  session form.</p>
     * 
     * @param sourceForm the Winv003Form
     * @param targetForm the Winv003Form
     */
    private void setCriteriaOnScreen(Winv003Form sourceForm, Winv003Form targetForm) {
        if(!Strings.judgeBlank(sourceForm.getSessionId())){
            targetForm.setSessionId(sourceForm.getSessionId());
        }
        if(!Strings.judgeBlank(sourceForm.getDscId())){
            targetForm.setDscId(sourceForm.getDscId());
        }
        if(!Strings.judgeBlank(sourceForm.getFileName())){
            targetForm.setFileName(sourceForm.getFileName());
        }
        
        targetForm.setCannotResetMessage(sourceForm.getCannotResetMessage());
        targetForm.setCannotDownloadMessage(sourceForm.getCannotDownloadMessage());
        targetForm.setPageNo(Constants.ONE);
        targetForm.setPages(Constants.PAGE_CLICK);
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_UPLOAD, 
            SupplierPortalConstant.METHOD_DO_UPLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GROUP,
            SupplierPortalConstant.METHOD_DO_GROUP);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN, 
            SupplierPortalConstant.METHOD_DO_RETURN);
        return keyMethodMap;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation(){
        
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for ASN Information Screen.
     * <p>Get roles from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain getRoleScreen(UserLoginDomain userLoginDomain) {
        
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WINV002.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param result the ASN Information Return Domain
     * @param e the Exception
     */
    private void setMessageForException(InvoiceInformationUploadingDomain result, Exception e){
        try {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002, 
                    new String[]{e.getClass().toString(), e.getMessage()})));
        } catch (Exception exception) {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()})));
        }
    }
    
    /**
     * setMessageOnScreen.
     * <p>Setting message for appearance on screen.</p>
     * 
     * @param form the WINV003 form
     * @param result the Invoice Information Uploading Domain
     */
    private void setMessageOnScreen(Winv003Form form, InvoiceInformationUploadingDomain result){
        
        if(null != result){
            List<ApplicationMessageDomain> applicationMessageList;
            if(null != result.getErrorMessageList() 
                && Constants.ZERO < result.getErrorMessageList().size()){
                applicationMessageList = new ArrayList<ApplicationMessageDomain>();
                applicationMessageList.addAll(result.getErrorMessageList());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    applicationMessageList.addAll(result.getWarningMessageList());
                }
                form.setApplicationMessageList(applicationMessageList);
            }else if(null != result.getSuccessMessageList() 
                && Constants.ZERO < result.getSuccessMessageList().size()){
                
                form.setApplicationMessageList(result.getSuccessMessageList());
            }
        }
        
    }
}
