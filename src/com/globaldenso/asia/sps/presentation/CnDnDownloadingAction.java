/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/17 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.IOException;
import java.io.OutputStream;
//import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadResultDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.business.service.CnDnDownloadingFacadeService;
import com.globaldenso.asia.sps.business.service.DensoSupplierRelationService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.presentation.form.Winv007Form;

/**
 * The Class CNDNDownloadingAction.
 * 
 * @author CSI
 */
public class CnDnDownloadingAction extends CoreAction {

    /** The CNDN downloading facade service. */
    private CnDnDownloadingFacadeService cndnDownloadingFacadeService = null;

    /** The densoSupplierRelation Service. */
    private DensoSupplierRelationService densoSupplierRelationService;

    /** The default constructor. */
    public CnDnDownloadingAction() {
        super();
    }

    /**
     * Set the CNDN downloading facade service.
     * 
     * @param cndnDownloadingFacadeService the CNDN downloading facade service
     *            to set
     */
    public void setCndnDownloadingFacadeService(
        CnDnDownloadingFacadeService cndnDownloadingFacadeService) {
        this.cndnDownloadingFacadeService = cndnDownloadingFacadeService;
    }

    /**
     * <p>
     * Setter method for densoSupplierRelationService.
     * </p>
     * 
     * @param densoSupplierRelationService Set for densoSupplierRelationService
     */
    public void setDensoSupplierRelationService(
        DensoSupplierRelationService densoSupplierRelationService) {
        this.densoSupplierRelationService = densoSupplierRelationService;
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        Winv007Form form = (Winv007Form)actionForm;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.resetForm();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();

        List<UserRoleDomain> roleUserList = null;
        for (RoleScreenDomain roleScreen : userLoginDomain
            .getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }

        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        plantSupplierWithScopeDomain
            .setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain
            .setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierDomain.setVendorCd(Constants.EMPTY_STRING);

        try {
            FileUploadResultDomain fileUploadResultDomainList = cndnDownloadingFacadeService
                .searchInitial(dataScopeControlDomain);

            Calendar firstDateOfCurrentMonth = Calendar.getInstance();
            firstDateOfCurrentMonth.set(Calendar.DAY_OF_MONTH, Constants.ONE);

            Calendar lastDateOfCurrentMonth = Calendar.getInstance();
            lastDateOfCurrentMonth.set(Calendar.DAY_OF_MONTH,
                lastDateOfCurrentMonth.getActualMaximum(Calendar.DAY_OF_MONTH));

            form.setUpdateDateFrom(DateUtil.format(
                firstDateOfCurrentMonth.getTime(),
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            form.setUpdateDateTo(DateUtil.format(
                lastDateOfCurrentMonth.getTime(),
                DateUtil.PATTERN_YYYYMMDD_SLASH));

            form.setFileNameCriteria(null);
            form.setSCdList(fileUploadResultDomainList.getCompanySupplierList());
            form.setCompanyDensoList(fileUploadResultDomainList.getCompanyDensoList());
            form.setSPcdList(fileUploadResultDomainList.getPlantSupplierList());
            form.setDPcdList(fileUploadResultDomainList.getPlantDensoList());
            form.setUserLogin(setUserLogin());

            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV007_LDOWNLOAD)) {
                    this.cndnDownloadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV007_LDOWNLOAD, locale);
                }
                form.setCannotDownloadMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotDownloadMessage( ae.getMessage() );
            }
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV007_BRESET)) 
                {
                    this.cndnDownloadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV007_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }
            
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }

        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Do select SCd
     * <ul>
     * <li>System will initial supplier plant code combo box for related
     * supplier company code when supplier company code combo box is changed.</li>
     * </ul>
     * 
     * @param mapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv007Form form = (Winv007Form)actionForm;
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        List<UserRoleDomain> roleUserList = null;
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        plantSupplierDomain.setVendorCd(form.getVendorCd());
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);

        try {
            List<SpsMMiscDomain> plantSupplierList = this.cndnDownloadingFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Do select SPcd
     * <ul>
     * <li>System will initial supplier company code combo box for related
     * supplier plant code when supplier plant code combo box is changed.</li>
     * </ul>
     * 
     * @param mapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv007Form form = (Winv007Form)actionForm;
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        List<UserRoleDomain> roleUserList = null;
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        plantSupplierDomain.setSPcd(form.getSPcd());
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);

        try {
            List<CompanySupplierDomain> companySupplierList = this.cndnDownloadingFacadeService
                .searchSelectedPlantSupplier(plantSupplierWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)companySupplierList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * <p>
     * Select DCd.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        /**
         * Call private action method to get current user login information and
         * roles from DENSO-context.
         */
        Winv007Form form = (Winv007Form)actionForm;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        List<UserRoleDomain> roleUserList = new ArrayList<UserRoleDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);

        /** Get roles for denso user. */
        for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /**
         * Call Façade Service DeliveryOrderInformationFacadeService
         * changeSelectDENSOCompany() to get the list of DENSO plant code by
         * DENSO company code.
         */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantDensoWithScopeDomain.setLocale(locale);

        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();

        try {
            plantDensoDomain = cndnDownloadingFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)plantDensoDomain), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * <p>
     * Select DPcd
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        /**
         * Call private action method to get current user login information and
         * roles from DENSO-context.
         */
        Winv007Form form = (Winv007Form)actionForm;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        List<UserRoleDomain> roleUserList = new ArrayList<UserRoleDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);

        /** Get roles for denso user. */
        for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /**
         * Call Façade Service DeliveryOrderInformationFacadeService
         * changeSelectDENSOCompany() to get the list of DENSO plant code by
         * DENSO company code.
         */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(form.getDPcd());
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantDensoWithScopeDomain.setLocale(locale);

        try {
            List<CompanyDensoDomain> companyDensoList = cndnDownloadingFacadeService
                .searchSelectedPlantDenso(plantDensoWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)companyDensoList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }
    /**
     * Do search.
     * <p>
     * Search data by criteria from screen.
     * </p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        Winv007Form form = (Winv007Form)actionForm;
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        FileUploadCriteriaDomain fileUploadCriteriaDomain = new FileUploadCriteriaDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        String message = null;
        List<UserRoleDomain> roleUserList = null;
        form.setUserLogin(setUserLogin());

        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);

        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }

        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        try {
            UserLoginDomain userLogin1 = this
                .getCurrentUserLoginInformation();
            List<UserRoleDomain> roleUserList1 = null;
            PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
            DataScopeControlDomain dataScopeControlDomain1 = new DataScopeControlDomain();
            PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
                new PlantSupplierWithScopeDomain();

            for (RoleScreenDomain roleScreen : userLogin1.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(
                    roleScreen.getSpsMScreenDomain().getScreenCd())) {
                    roleUserList1 = roleScreen.getUserRoleDomainList();
                }
            }
            
            if (Constants.MISC_CODE_ALL.endsWith(form.getVendorCd())) {
                plantSupplierDomain.setVendorCd(Constants.EMPTY_STRING);
            } else {
                plantSupplierDomain.setVendorCd(form.getVendorCd());
            }
            dataScopeControlDomain1.setUserType(userLogin1.getUserType());
            dataScopeControlDomain1.setUserRoleDomainList(roleUserList1);
            dataScopeControlDomain1
                .setDensoSupplierRelationDomainList(null);

            plantSupplierWithScopeDomain
                .setPlantSupplierDomain(plantSupplierDomain);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain1);

            FileUploadResultDomain fileUploadResultDomainList = cndnDownloadingFacadeService
                .searchInitial(dataScopeControlDomain);

            form.setSCdList(fileUploadResultDomainList
                .getCompanySupplierList());
            form.setCompanyDensoList(fileUploadResultDomainList
                .getCompanyDensoList());

            List<SpsMMiscDomain> plantSupplierList = this.cndnDownloadingFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setSPcdList(plantSupplierList);

            PlantDensoWithScopeDomain plantDensoWithScopeDomain = 
                new PlantDensoWithScopeDomain();

            if (Constants.MISC_CODE_ALL.endsWith(form.getDCd())) {
                plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(Constants.EMPTY_STRING);
            } else {
                plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
            }

            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setUserType(userLogin1.getUserType());
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setUserRoleDomainList(roleUserList);
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setDensoSupplierRelationDomainList(null);
            plantDensoWithScopeDomain.setLocale(locale);

            List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();

            form.setUserLogin(setUserLogin());
            plantDensoDomain = cndnDownloadingFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);

            form.setDPcdList(plantDensoDomain);
            
            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                fileUploadResultDomainList.getDensoSupplierRelationList()
                , SupplierPortalConstant.SCREEN_ID_WINV007));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                fileUploadResultDomainList.getDensoSupplierRelationList()
                , SupplierPortalConstant.SCREEN_ID_WINV007));
            
            if (Constants.PAGE_SEARCH.equals(firstPages)
                || Constants.PAGE_CLICK.equals(pages)) {
                
                fileUploadCriteriaDomain.setLocale(locale);
                if (!Strings.judgeBlank(form.getUpdateDateFrom())) {
                    fileUploadCriteriaDomain.setUpdateDateFrom(form
                        .getUpdateDateFrom());
                }
                if (!Strings.judgeBlank(form.getUpdateDateTo())) {
                    fileUploadCriteriaDomain.setUpdateDateTo(form
                        .getUpdateDateTo());
                }
                if (!Strings.judgeBlank(form.getVendorCd())) {
                    fileUploadCriteriaDomain.setVendorCd(form.getVendorCd());
                }
                if (!Strings.judgeBlank(form.getSPcd())) {
                    fileUploadCriteriaDomain.setSPcd(form.getSPcd());
                }
                if (!Strings.judgeBlank(form.getFileNameCriteria().trim())) {
                    fileUploadCriteriaDomain.setFileName(
                        form.getFileNameCriteria().toUpperCase().trim());
                }
                if (!Strings.judgeBlank(form.getDCd())) {
                    fileUploadCriteriaDomain.setDCd(form.getDCd());
                }
                if (!Strings.judgeBlank(form.getDPcd())) {
                    fileUploadCriteriaDomain.setDPcd(form.getDPcd());
                }
                if (Constants.PAGE_SEARCH.equals(form.getFirstPages())) {
                    fileUploadCriteriaDomain.setPageNumber(Constants.ONE);
                } else {
                    fileUploadCriteriaDomain.setPageNumber(form.getPageNo());
                }
                
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV007_BSEARCH)) 
                {
                    this.cndnDownloadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV007_BSEARCH, locale);
                }
                
                FileUploadResultDomain result = cndnDownloadingFacadeService
                    .searchFileDownloadInformation(fileUploadCriteriaDomain);

                if (null != result.getErrorMessageList()) {
                    errorMessageList = result.getErrorMessageList();

                } else {
                    SpsPagingUtil.setFormForPaging(fileUploadCriteriaDomain, form);
                    form.setFileUploadForDisplayList(result.getFileUploadForDisplayDomainList());
                    form.setMessageType(Constants.MESSAGE_SUCCESS);
                }
            }
            
        } catch (ApplicationException applicationException) {
            message = applicationException.getMessage();
            form.setMessageType(Constants.MESSAGE_FAILURE);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } catch (Exception exception) {
            message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{exception.getClass().toString(), exception.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
            form.setMessageType(Constants.MESSAGE_FAILURE);

        } finally {
            form.setApplicationMessageList(errorMessageList);
        }

        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do download, export file using file manager stream.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv007Form form = (Winv007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        OutputStream output = null;
        String message = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();

        try {
            form.setUserLogin(super.setUserLogin());
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV007_LDOWNLOAD)) 
            {
                this.cndnDownloadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV007_LDOWNLOAD, locale);
            }
            
            FileUploadDomain fileUploadDomain = new FileUploadDomain();
            fileUploadDomain.setLocale(locale);
            fileUploadDomain.setFileId(form.getFileId());
            form.setUserLogin(setUserLogin());

            FileManagementDomain fileManagementDomain = new FileManagementDomain();

            fileManagementDomain = cndnDownloadingFacadeService
                .searchFileName(fileUploadDomain);
            String fileName = fileManagementDomain.getFileName();

            super.setHttpHeaderForFileManagement(response, request, fileName);
            output = response.getOutputStream();
            cndnDownloadingFacadeService.searchFileDownloadCsv(
                fileUploadDomain, output);
            output.flush();

            form.setMessageType(Constants.MESSAGE_SUCCESS);
            form.setUserLogin(setUserLogin());
        } catch (IOException ie) {
            try {
                message = MessageUtil.getApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_80_0009);
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            } catch (Exception e) {
                message = MessageUtil.getApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001);
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            }
            form.setMessageType(Constants.MESSAGE_FAILURE);
        } catch (ApplicationException ae) {
            message = ae.getMessage();
            form.setMessageType(Constants.MESSAGE_FAILURE);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } catch (Exception ex) {
            message = MessageUtil.getApplicationMessage(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            form.setMessageType(Constants.MESSAGE_FAILURE);
        } finally {
            if (null != output) {
                output.close();
            }
            form.setApplicationMessageList(errorMessageList);
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReset(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        Winv007Form form = (Winv007Form)actionForm;
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV007.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try {
            form.resetForm();
            form.setUserLogin(super.setUserLogin());
            PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
            PlantSupplierWithScopeDomain plantSupplierWithScopeDomain
                = new PlantSupplierWithScopeDomain();
            
            if (Constants.MISC_CODE_ALL.endsWith(form.getVendorCd())) {
                plantSupplierDomain.setVendorCd(Constants.EMPTY_STRING);
            } else {
                plantSupplierDomain.setVendorCd(form.getVendorCd());
            }
            
            plantSupplierWithScopeDomain
                .setPlantSupplierDomain(plantSupplierDomain);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);
            
            FileUploadResultDomain fileUploadResultDomainList = cndnDownloadingFacadeService
                .searchInitial(dataScopeControlDomain);
            
            form.setSCdList(fileUploadResultDomainList
                .getCompanySupplierList());
            form.setCompanyDensoList(fileUploadResultDomainList
                .getCompanyDensoList());
            
            List<SpsMMiscDomain> plantSupplierList = this.cndnDownloadingFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setSPcdList(plantSupplierList);
            
            PlantDensoWithScopeDomain plantDensoWithScopeDomain = 
                new PlantDensoWithScopeDomain();
            
            if (Constants.MISC_CODE_ALL.endsWith(form.getDCd())) {
                plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                    Constants.EMPTY_STRING);
            } else {
                plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                    form.getDCd());
            }
            
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setUserType(userLogin.getUserType());
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setUserRoleDomainList(roleUserList);
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setDensoSupplierRelationDomainList(null);
            plantDensoWithScopeDomain.setLocale(locale);
            
            List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
            
            form.setUserLogin(setUserLogin());
            plantDensoDomain = cndnDownloadingFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
            form.setDPcdList(plantDensoDomain);
            
            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                fileUploadResultDomainList.getDensoSupplierRelationList()
                , SupplierPortalConstant.SCREEN_ID_WINV007));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                fileUploadResultDomainList.getDensoSupplierRelationList()
                , SupplierPortalConstant.SCREEN_ID_WINV007));
            
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(Constants.ZERO < errorMessageList.size()){
                form.setApplicationMessageList(errorMessageList);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * <p>
     * Get key method map
     * <p>
     * 
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_RESET);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        
        return keyMethodMap;
    }

    /**
     * Method to get current user login information detail from DENSO context.
     * <p>
     * Set UserLoginDomain from current user login detail.
     * </p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation() {

        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }

}
