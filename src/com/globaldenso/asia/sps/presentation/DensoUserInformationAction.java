/*
 * ModifyDate Development company     Describe 
 * 2014/06/03 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wadm005Form;
import com.globaldenso.asia.sps.business.service.DensoUserInformationFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;

/**
 * The Class DensoUserInformationAction.
 * <p>
 * Receive value from WADM005_DensoUserInformationAction.jsp for initial value and send to 
 * densoUserInformationFacadeService
 * for search
 * </p>
 * <ul>
 * <li>Method initial  : doInitial</li>
 * <li>Method initial  : doInitialByReturn</li>
 * <li>Method search   : doSearch</li>
 * <li>Method delete   : doDelete</li>
 * <li>Method download : doDownload</li>
 * <li>Method hyper link : doDscIdLink</li>
 * <li>Method hyper link : doRoleLink</li>
 * </ul>
 * 
 * @author CSI
 */
public class DensoUserInformationAction extends CoreAction {

    /** The DENSO User facade service. */
    private DensoUserInformationFacadeService densoUserInformationFacadeService;
    
    /**
     * Instantiates a new DensoUserInformationAction.
     */
    public DensoUserInformationAction() {
        super();
    }
    
    /**
     * Sets the DENSO User Facade Information Service.
     * 
     * @param densoUserInformationFacadeService the DENSO User Facade Information Service.
     */
    public void setDensoUserInformationFacadeService(
        DensoUserInformationFacadeService densoUserInformationFacadeService) {
        this.densoUserInformationFacadeService = densoUserInformationFacadeService;
    }
    
    /**
     * Do initial.
     * <p>
     * Initial data when load page.
     * </p>
     * <ul>
     * <li>In case click MainMenu (DENSO UserInformation) link, Reset button</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request  the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        DensoUserInformationDomain result = new DensoUserInformationDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Call Facade Service for initial*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        try{
            result = densoUserInformationFacadeService.searchInitial(dataScopeControlDomain);
            /*Set value from domain to ActionForm*/
            form.resetForm();
            form.setCompanyDensoList(result.getCompanyDensoList());
            form.setPlantDensoList(result.getPlantDensoList());
            /*Set Authentication list*/
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                result.getDataScopeControlDomain().getDensoSupplierRelationDomainList(),
                SupplierPortalConstant.SCREEN_ID_WADM005));
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM005_BRESET)){
                    this.densoUserInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM005_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM005_BDOWNLOAD)){
                    this.densoUserInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM005_BDOWNLOAD, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotDownloadMessage(ex.getMessage());
            }
            
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do initial by Return.
     * <p>
     * Initial data when Return from "WADM006 - DENSO User Registration" or "WADM008 - 
     * DENSO User Role Assignment".
     * </p>
     * <ul>
     * <li>In case click return button from "WADM006 - DENSO User Registration" or "WADM008 - 
     * DENSO User Role Assignment".then load page by previous criteria.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialByReturn(ActionMapping actionMapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Wadm005Form form = (Wadm005Form) actionForm;
        Wadm005Form sessionForm = (Wadm005Form)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_WADM005_FORM);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        List<ApplicationMessageDomain> applicationMessageList =
            new ArrayList<ApplicationMessageDomain>();
        DensoUserInformationDomain criteria = new DensoUserInformationDomain();
        DensoUserInformationDomain result = new DensoUserInformationDomain();
        String firstname = null;
        String middleName = null;
        String lastName = null;
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        if(Constants.MISC_CODE_ALL.equals(sessionForm.getDCd())){
            criteria.setDCd(null);
        }else{
            criteria.setDCd(sessionForm.getDCd());
        }
        if(Constants.MISC_CODE_ALL.equals(sessionForm.getDPcd())){
            criteria.setDPcd(null);
        }else{
            criteria.setDPcd(sessionForm.getDPcd());
        }
        if(!StringUtil.checkNullOrEmpty(sessionForm.getFirstName())){
            firstname = sessionForm.getFirstName().toUpperCase();
        }
        if(!StringUtil.checkNullOrEmpty(sessionForm.getMiddleName())){
            middleName = sessionForm.getMiddleName().toUpperCase();
        }
        if(!StringUtil.checkNullOrEmpty(sessionForm.getLastName())){
            lastName = sessionForm.getLastName().toUpperCase();
        }
        String dscId = sessionForm.getDscId().toUpperCase();
        String employeeCode = sessionForm.getEmployeeCode().toUpperCase();
        
        criteria.setDscId(dscId);
        criteria.setEmployeeCode(employeeCode);
        criteria.setFirstName(firstname);
        criteria.setMiddleName(middleName);
        criteria.setLastName(lastName);
        criteria.setRegisterDateFrom(sessionForm.getRegisterDateFrom());
        criteria.setRegisterDateTo(sessionForm.getRegisterDateTo());
        criteria.setIsActive(Constants.IS_ACTIVE);
        criteria.setDensoAuthenList(sessionForm.getDensoAuthenList());
        
        /*Set value from domain to criteria Form*/
        form.setDscId(sessionForm.getDscId());
        form.setEmployeeCode(sessionForm.getEmployeeCode());
        form.setDCd(sessionForm.getDCd());
        form.setDPcd(sessionForm.getDPcd());
        form.setFirstName(sessionForm.getFirstName());
        form.setMiddleName(sessionForm.getMiddleName());
        form.setLastName(sessionForm.getLastName());
        form.setRegisterDateFrom(sessionForm.getRegisterDateFrom());
        form.setRegisterDateTo(sessionForm.getRegisterDateTo());
        form.setUserLogin(sessionForm.getUserLogin());
        form.setMenuCodelist(sessionForm.getMenuCodelist());
        form.setCannotResetMessage(sessionForm.getCannotResetMessage());
        form.setCannotDownloadMessage(sessionForm.getCannotDownloadMessage());
        form.setDensoAuthenList(sessionForm.getDensoAuthenList());
        
        /*Call Facade Service for initial*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        //Clear session
        DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WADM005_FORM);
        form.setCountDelete(Constants.STR_ZERO);
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }
                result = densoUserInformationFacadeService.searchInitialWithCriteria(criteria);
                form.setCompanyDensoList(result.getCompanyDensoList());
                SpsPagingUtil.setFormForPaging(result, form);
                form.setUserDensoInfoList(result.getUserDensoInfoList());
            }catch(ApplicationException applicationException){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                this.setPlantCombobox(form);
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Search DENSO Plant Code by selected DENSO Company Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantDensoDomain> plantDensoList =  new ArrayList<PlantDensoDomain>();
        DensoUserInformationDomain criteria = new DensoUserInformationDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = this.setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try{
            if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                criteria.setDCd(form.getDCd());
            }
            criteria.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = densoUserInformationFacadeService.searchSelectedCompanyDenso(criteria);
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001)));
            printJson(response, null, e);
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**
     * Search company DENSO Code by selected plant DENSO Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.setUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLogin);
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try{
            /* 3. Call Facade Service AcknowledgedDoInformationFacadeService
             * searchSelectedPlantDenso() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())
                && !Constants.STR_NINETY_NINE.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setLocale(locale);
            plantDenso.setDataScopeControlDomain(dataScopeControlDomain);
            companyList = this.densoUserInformationFacadeService.searchSelectedPlantDenso(
                plantDenso);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Do search.
     * <p>
     * Search data by criteria from screen.
     * </p>
     * <ul>
     * <li>In case click search button, search the DENSO user information 
     * that meet the criteria values.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request  the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSearch(ActionMapping actionMapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        DensoUserInformationDomain result = new DensoUserInformationDomain();
        DensoUserInformationDomain densoUserInfoDomain = new DensoUserInformationDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
//        /*Get roles for supplier user*/
//        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        this.setParameterCriteria(form, densoUserInfoDomain);
        densoUserInfoDomain.setLocale(locale);
        densoUserInfoDomain.setIsActive(Constants.IS_ACTIVE);
        
//        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
//        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
//        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
//        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
//        densoUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM005_BSEARCH)){
                this.densoUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM005_BSEARCH, locale);
            }
            
            if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    densoUserInfoDomain.setPageNumber(Constants.ONE);
                }else{
                    densoUserInfoDomain.setPageNumber(form.getPageNo());
                }
                result = densoUserInformationFacadeService.searchUserDenso(densoUserInfoDomain);
                List<DensoUserInformationDomain> densoUserList = result.getUserDensoInfoList();
                if(null != densoUserList){
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID,
                            new HashMap<String, DensoUserInformationDomain>());
                        form.setCountDelete(String.valueOf(Constants.ZERO));
                    }else{
                        this.doSetSelectedDscId(form);
                        Map<String, DensoUserInformationDomain> selectedDscIdMap = 
                            (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                                .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
                        for(String itemKey : selectedDscIdMap.keySet()){
                            for(DensoUserInformationDomain item : densoUserList){
                                if(itemKey.equals(item.getDscId())){
                                    item.setDscIdSelected(itemKey);
                                }
                            }
                        }
                    }
                    SpsPagingUtil.setFormForPaging(result, form);
                    form.setUserDensoInfoList(densoUserList); 
                }else{
                    errorMessageList = result.getErrorMessageList();
                }
            }
        }catch (ApplicationException ex) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    ex.getMessage()));
        } catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally {
            this.setPlantCombobox(form);
            form.setApplicationMessageList(errorMessageList);
        }
//        try{
//            /*initial screen*/
//            densoUserInfoDomain = densoUserInformationFacadeService.searchInitial(
//                dataScopeControlDomain);
//            form.setCompanyDensoList(densoUserInfoDomain.getCompanyDensoList());
//        }catch (ApplicationException applicationException) {
//            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                applicationException.getMessage()));
//            form.setApplicationMessageList(errorMessageList);
//        }catch (Exception e) {
//            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do delete DENSO user.
     * 
     * <p>
     * set DENSO user(s) to inactive user.
     * </p>
     * 
     * <li>In case click delete button to set the target user(s)
     * that selected in the search result to inactive.</li>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doDelete(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        String firstPages = form.getFirstPages();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        DensoUserInformationDomain result = new DensoUserInformationDomain();
        DensoUserInformationDomain criteria = new DensoUserInformationDomain();
        List<DensoUserInformationDomain> densoUserSelectedList = new 
            ArrayList<DensoUserInformationDomain>();
        List<DensoUserInformationDomain> densoUserList = 
            new ArrayList<DensoUserInformationDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Set value to argument Domain.*/
        this.setParameterCriteria(form, criteria);
        criteria.setLocale(locale);
        criteria.setUpdateUser(userLoginDomain.getDscId());
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM005_BDELETE)){
                //Search DENSO User data when has an error.
                form.setPages(Constants.PAGE_CLICK);
                
                /*Set value to argument Domain.*/
                criteria.setIsActive(Constants.IS_ACTIVE);
                criteria.setPageNumber(form.getPageNo());
                result = densoUserInformationFacadeService.searchUserDenso(criteria);
                densoUserList = result.getUserDensoInfoList();
                if(null != densoUserList){
                    this.doSetSelectedDscId(form);
                    Map<String, DensoUserInformationDomain> selectedDscIdMap = 
                        (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                            .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
                    for(String itemKey : selectedDscIdMap.keySet()){
                        for(DensoUserInformationDomain item : densoUserList){
                            if(itemKey.equals(item.getDscId())){
                                item.setDscIdSelected(itemKey);
                            }
                        }
                    }
                    SpsPagingUtil.setFormForPaging(result, form);
                }
                this.densoUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM005_BDELETE, locale);
            }
            
            this.doSetSelectedDscId(form);
            Map<String, DensoUserInformationDomain> selectedDscIdMap = 
                (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                    .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
            
            if(null != selectedDscIdMap && !selectedDscIdMap.isEmpty()){
                Iterator<String> iterator = selectedDscIdMap.keySet().iterator();
                while(iterator.hasNext()){
                    String keyStr = (String)iterator.next();
                    densoUserSelectedList.add(selectedDscIdMap.get(keyStr));
                }
                if(Constants.ZERO < densoUserSelectedList.size()){
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        criteria.setPageNumber(Constants.ONE);
                    }else{
                        form.setPages(Constants.PAGE_CLICK);
                        criteria.setPageNumber(form.getPageNo());
                    }
                    criteria.setUserDensoInfoList(densoUserSelectedList);
                    result = densoUserInformationFacadeService.deleteUserDenso(criteria);
                    densoUserList = result.getUserDensoInfoList();
                    
                    SpsPagingUtil.setFormForPaging(result, form);
                    form.setCountDelete(Constants.STR_ZERO);
                    
                    /*initial screen*/
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0003);
                    errorMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, message));
                    form.setMethod(SupplierPortalConstant.METHOD_DO_SEARCH);
                    DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID,
                        new HashMap<String, DensoUserInformationDomain>());
                }
            }
        } catch (ApplicationException applicationException) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        } catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            this.setPlantCombobox(form);
            if(Constants.ZERO == result.getTotalCount()){
                form.setMax(Constants.ZERO);
                form.setUserDensoInfoList(null);
            }else{
                form.setUserDensoInfoList(densoUserList);
            }
            form.setApplicationMessageList(errorMessageList);
        }
//        try{
//            /*initial screen*/
//            criteria = densoUserInformationFacadeService.searchInitial(dataScopeControlDomain);
//            form.setCompanyDensoList(criteria.getCompanyDensoList());
//        } catch (ApplicationException applicationException) {
//            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                applicationException.getMessage()));
//            form.setApplicationMessageList(errorMessageList);
//        }catch (Exception e) {
//            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download.
     * 
     * <p>
     * download data that meet criteria values.
     * </p>
     * 
     * <li>In case click download button, system will load data that meet to criteria 
     * and export all data to CSV file.</li>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        DensoUserInformationDomain result = new DensoUserInformationDomain();
        DensoUserInformationDomain criteria = new DensoUserInformationDomain();
        Calendar cal = Calendar.getInstance();
        ServletOutputStream outputStream = null;
        boolean downloadCompleted = false;
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Set value to argument Domain.*/
        this.setParameterCriteria(form, criteria);
        criteria.setLocale(locale);
        
        try{
            result = densoUserInformationFacadeService.searchUserDensoCsv(criteria);
            StringBuffer resultStr = result.getResultString();
            if(null != resultStr){
                String fileName = StringUtil.appendsString(
                    Constants.DENSO_USER_INFO_LIST_FILE_NAME, Constants.SYMBOL_UNDER_SCORE,
                    DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                
                byte[] output = resultStr.toString().getBytes();
                setHttpHeaderForCsv(response, fileName);
                outputStream = response.getOutputStream();
                outputStream.write(output, Constants.ZERO, output.length);
                outputStream.flush();
                downloadCompleted = true;
                return null;
            }else{
                errorMessageList = result.getErrorMessageList();
            }
        }catch(ApplicationException applicationException){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0012);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
            if(!downloadCompleted){
                //Search DENSO User data when has an error.
                form.setPages(Constants.PAGE_CLICK);
                /*Set value to argument Domain.*/
                criteria.setIsActive(Constants.IS_ACTIVE);
                criteria.setPageNumber(form.getPageNo());
                result = densoUserInformationFacadeService.searchUserDenso(criteria);
                List<DensoUserInformationDomain> densoUserList = result.getUserDensoInfoList();
                if(null != densoUserList){
                    this.doSetSelectedDscId(form);
                    Map<String, DensoUserInformationDomain> selectedDscIdMap = 
                        (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                            .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
                    for(String itemKey : selectedDscIdMap.keySet()){
                        for(DensoUserInformationDomain item : densoUserList){
                            if(itemKey.equals(item.getDscId())){
                                item.setDscIdSelected(itemKey);
                            }
                        }
                    }
                    SpsPagingUtil.setFormForPaging(result, form);
                }
                this.setPlantCombobox(form);
                form.getApplicationMessageList().addAll(errorMessageList);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do DSC ID hyper link.
     * <p>DSC Id hyper link to open WADM006 screen for edit DENSO user detail.</p>
     * 
     *  <li>In case click DSC ID hyper link, system will load data that meet to DSC ID 
     *  for initial data to open WADM006: DENSO User Registration.</li>
     * 
     * @param mapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doLinkDscId(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm005Form form = (Wadm005Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        DensoUserInformationDomain densoUserInfoDomain = new DensoUserInformationDomain();
        
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM005_LDSCID)){
                this.densoUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM005_LDSCID, locale);
            }
            
            /* Parameter to WADM005
             * - dscId
             * - densoCompanyCode
             * - firstName
             * - middleName
             * - lastName
             * - registerDateFrom
             * - registerDateTo
             * - dscIdSelected
             * */
            forward.append(SupplierPortalConstant.URL_WADM006_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA)
                .append(SupplierPortalConstant.URL_PARAM_MODE_EDIT);
//            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
//            forward.append(form.getDscId());
//            forward.append(SupplierPortalConstant.URL_PARAM_DENSO_COMPANY_CODE);
//            forward.append(form.getDCd());
//            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
//            forward.append(form.getFirstName());
//            forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
//            forward.append(form.getMiddleName());
//            forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
//            forward.append(form.getLastName());
//            forward.append(SupplierPortalConstant.URL_PARAM_REGISTER_DATE_FROM);
//            forward.append(form.getRegisterDateFrom());
//            forward.append(SupplierPortalConstant.URL_PARAM_REGISTER_DATE_TO);
//            forward.append(form.getRegisterDateTo());
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
            forward.append(form.getDscIdSelected());
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WADM005_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
        //Search DENSO User data when has an error.
        this.setPlantCombobox(form);
        form.setPages(Constants.PAGE_CLICK);
        
        /*Set value to argument Domain.*/
        this.setParameterCriteria(form, densoUserInfoDomain);
        densoUserInfoDomain.setLocale(locale);
        densoUserInfoDomain.setIsActive(Constants.IS_ACTIVE);
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        densoUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        densoUserInfoDomain.setPageNumber(form.getPageNo());
        DensoUserInformationDomain result
            = densoUserInformationFacadeService.searchUserDenso(densoUserInfoDomain);
        List<DensoUserInformationDomain> densoUserList = result.getUserDensoInfoList();
        if(null != densoUserList){
            this.doSetSelectedDscId(form);
            Map<String, DensoUserInformationDomain> selectedDscIdMap = 
                (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                    .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
            for(String itemKey : selectedDscIdMap.keySet()){
                for(DensoUserInformationDomain item : densoUserList){
                    if(itemKey.equals(item.getDscId())){
                        item.setDscIdSelected(itemKey);
                    }
                }
            }
            SpsPagingUtil.setFormForPaging(result, form);
            form.setUserDensoInfoList(densoUserList); 
        }
        form.getApplicationMessageList().addAll(applicationMessageList);
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do Role hyper link.
     * 
     * <p>Role hyper link to open WADM008 screen for edit DENSO user detail.</p>
     * 
     *  <li>In case click Role hyper link, system will load data that meet to DSC ID 
     *  for initial data to open WADM008 : DENSO User Role Assignment.</li>
     * 
     * @param mapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doLinkRole(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wadm005Form form = (Wadm005Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        DensoUserInformationDomain densoUserInfoDomain = new DensoUserInformationDomain();
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM005_LASSIGNROLE)){
                this.densoUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM005_LASSIGNROLE, locale);
            }
            /* Parameter to WADM008
             * - dscId
             * - densoCodeSelected
             * - densoPlantSelected
             * - firstName
             * - middleName
             * - lastName
             * */
            forward.append(SupplierPortalConstant.URL_WADM008_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
            forward.append(form.getDscIdSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_DCD);
            forward.append(form.getDCdSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_DPCD);
            forward.append(form.getDPcdSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
            forward.append(form.getFirstNameSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
            forward.append(form.getMiddleNameSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
            forward.append(form.getLastNameSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
            forward.append(Constants.PAGE_SEARCH);
            
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WADM005_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
        //Search DENSO User data when has an error.
        this.setPlantCombobox(form);
        form.setPages(Constants.PAGE_CLICK);
        
        /*Set value to argument Domain.*/
        this.setParameterCriteria(form, densoUserInfoDomain);
        densoUserInfoDomain.setLocale(locale);
        densoUserInfoDomain.setIsActive(Constants.IS_ACTIVE);
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        densoUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        densoUserInfoDomain.setPageNumber(form.getPageNo());
        DensoUserInformationDomain result
            = densoUserInformationFacadeService.searchUserDenso(densoUserInfoDomain);
        List<DensoUserInformationDomain> densoUserList = result.getUserDensoInfoList();
        if(null != densoUserList){
            this.doSetSelectedDscId(form);
            Map<String, DensoUserInformationDomain> selectedDscIdMap = 
                (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                    .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
            for(String itemKey : selectedDscIdMap.keySet()){
                for(DensoUserInformationDomain item : densoUserList){
                    if(itemKey.equals(item.getDscId())){
                        item.setDscIdSelected(itemKey);
                    }
                }
            }
            SpsPagingUtil.setFormForPaging(result, form);
            form.setUserDensoInfoList(densoUserList); 
        }
        form.getApplicationMessageList().addAll(applicationMessageList);
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DELETE,
            SupplierPortalConstant.METHOD_DO_DELETE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LINK_ROLE,
            SupplierPortalConstant.METHOD_DO_LINK_ROLE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LINK_DSC_ID, 
            SupplierPortalConstant.METHOD_DO_LINK_DSC_ID);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN,
            SupplierPortalConstant.METHOD_DO_INITIAL_BY_RETURN);
        return keyMethodMap;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }

    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WADM005.equals(roleScreen.getSpsMScreenDomain()
                .getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * Do set selected DSC ID
     * @param form the form
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    private void doSetSelectedDscId(Wadm005Form form) throws Exception{
        List<DensoUserInformationDomain> itemList = form.getUserDensoInfoList();
        Map<String, DensoUserInformationDomain> selectedDensoDscIdMap 
            = (HashMap<String, DensoUserInformationDomain>)DensoContext.get()
                .getGeneralArea(Constants.MAP_KEY_SELECTED_DENSO_DSC_ID);
        for(DensoUserInformationDomain item : itemList){
            if(!StringUtil.checkNullOrEmpty(item.getDscIdSelected())){
                if(!selectedDensoDscIdMap.containsKey(item.getDscId())){
                    selectedDensoDscIdMap.put(String.valueOf(item.getDscId()), item);
                }
            }else{
                if(selectedDensoDscIdMap.containsKey(item.getDscId())){
                    selectedDensoDscIdMap.remove(item.getDscId());
                }
            }
        }
        
        if(null != selectedDensoDscIdMap && !selectedDensoDscIdMap.isEmpty()){
            int countDel = Constants.ZERO;
            Iterator<String> itr = selectedDensoDscIdMap.keySet().iterator();
            while(itr.hasNext()){
                itr.next();
                countDel += Constants.ONE;
            }
            form.setCountDelete(String.valueOf(countDel));
        }else{
            form.setCountDelete(String.valueOf(Constants.ZERO));
        }
    }
    
    /**
     * Set value inside Supplier Plant combo box.
     * @param form the Wadm001Form
     * */
    private void setPlantCombobox(Wadm005Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.setUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantDensoDomain> plantDensoList =  new ArrayList<PlantDensoDomain>();
        DensoUserInformationDomain criteria = new DensoUserInformationDomain();
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try{
            if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                criteria.setDCd(form.getDCd());
            }
            criteria.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = densoUserInformationFacadeService.searchSelectedCompanyDenso(criteria);
            form.setPlantDensoList(plantDensoList);
        }catch(ApplicationException e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
    }

    /**
     * Set value from Wadm005Form to parameter criteria.
     * @param form the Wadm005Form
     * @param criteria the supplier user information domain
     * */
    private void setParameterCriteria(Wadm005Form form, DensoUserInformationDomain criteria){
        if(!StringUtil.checkNullOrEmpty(form.getDscId().trim())){
            criteria.setDscId(form.getDscId().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getEmployeeCode().trim())){
            criteria.setEmployeeCode(form.getEmployeeCode().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getFirstName().trim())){
            criteria.setFirstName(form.getFirstName().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getMiddleName().trim())){
            criteria.setMiddleName(form.getMiddleName().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getLastName().trim())){
            criteria.setLastName(form.getLastName().toUpperCase().trim());
        }
        
        criteria.setDCd(form.getDCd());
        criteria.setDPcd(form.getDPcd());
        criteria.setRegisterDateFrom(form.getRegisterDateFrom());
        criteria.setRegisterDateTo(form.getRegisterDateTo());
        criteria.setDensoAuthenList(form.getDensoAuthenList());
    }
}