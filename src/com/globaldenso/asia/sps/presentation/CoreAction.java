/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.actions.LookupDispatchAction;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ApplicationMessages;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

import flexjson.JSONSerializer;

/**
 * The Class CoreAction.
 * @author CSI
 */
public abstract class CoreAction extends LookupDispatchAction {

    /** The Constant for file pattern class. */
    private static final String FILE_PATTERN_CLASS = "*.class";
    
    /** The Constant JSON_PARAM. */
    private static final String JSON_PARAM = "jsonParams";

    /** The Constant JSON_LIST1. */
    private static final String JSON_LIST1 = "jsonList1";

    /** The Constant JSON_LIST2. */
    private static final String JSON_LIST2 = "jsonList2";

    /** The Constant JSON_LIST3. */
    private static final String JSON_LIST3 = "jsonList3";

    /** The Constant JSON_LIST4. */
    private static final String JSON_LIST4 = "jsonList4";

    /** The Constant JSON_LIST5. */
    private static final String JSON_LIST5 = "jsonList5";

    /** The Constant JSON_LIST6. */
    private static final String JSON_LIST6 = "jsonList6";

    /** The Constant JSON_LIST7. */
    private static final String JSON_LIST7 = "jsonList7";

    /** The Constant JSON_LIST8. */
    private static final String JSON_LIST8 = "jsonList8";

    /** The Constant JSON_LIST9. */
    private static final String JSON_LIST9 = "jsonList9";

    /** The Constant JSON_LIST10. */
    private static final String JSON_LIST10 = "jsonList10";

    /** The Constant JSON_LIST11. */
    private static final String JSON_LIST11 = "jsonList11";

    /** The Constant JSON_LIST12. */
    private static final String JSON_LIST12 = "jsonList12";

    /** The Constant JSON_LIST13. */
    private static final String JSON_LIST13 = "jsonList13";

    /** The Constant JSON_LIST14. */
    private static final String JSON_LIST14 = "jsonList14";

    /** The Constant JSON_LIST15. */
    private static final String JSON_LIST15 = "jsonList15";

    /** The Constant JSON_LIST16. */
    private static final String JSON_LIST16 = "jsonList16";

    /** The log. */
    private transient Log LOG = LogFactory.getLog(CoreAction.class);
    
    
    
    /**
    * Sets the attribute.
    * 
    * @param request the request
    * @param attributeName the attribute name
    * @param valueObj the value obj
    */
    protected void setAttribute(HttpServletRequest request, String attributeName, Object valueObj) {
        request.setAttribute(attributeName, valueObj);
    }

    /**
    * setResultMessage is used for setting result message of transaction
    * operation.
    * 
    * @param request the request
    * @param message the message
    */
    protected void setResultMessage(HttpServletRequest request, String message) {
        request.setAttribute(Constants.RESULT_MESSAGE, message);
    }
    
    /**
     * setResultMessage is used for setting result message of transaction
     * operation.
     * 
     * @param request the request
     * @param msgs the msgs
     */
    protected void setResultMessage(HttpServletRequest request, ApplicationMessages msgs) {
        request.setAttribute(Constants.RESULT_MESSAGE, msgs);
    }

    /**
    * getParameter is used for getting parameter from request.
    * 
    * @param request : an instance of HttpServletRequest.
    * @param parameterName the parameter name
    * @return the parameter
    * @return
    */
    protected String getParameter(HttpServletRequest request, String parameterName) {
        return StringUtil.nullToEmpty(request.getParameter(parameterName));
    }

    /**
    * printJSON is used for printing JsonResult in form of JSON string.
    * 
    * @param response the response
    * @param jsonResult the json result
    * @param ex the json error
    */
    protected void printJson(HttpServletResponse response, 
        JsonResult jsonResult,
        Exception ex) {

        StringBuffer contentTypeBuffer = new StringBuffer();
        // application/json; charset=utf-8
        contentTypeBuffer.append(Constants.HTTP_HEADER_CONTENTTYPE_APPLICATION)
            .append(Constants.HTTP_HEADER_JSON).append(Constants.SYMBOL_SEMI_COLON)
            .append(Constants.SYMBOL_SPACE).append(Constants.HTTP_HEADER_CHARSET)
            .append(Constants.SYMBOL_EQUAL).append(Constants.HTTP_HEADER_UTF_8);
        
        response.setContentType(contentTypeBuffer.toString());
        response.setHeader(Constants.HTTP_HEADER_CACHE_CONTROL, Constants.HTTP_HEADER_NO_CACHE);

        String strResult = null;
        JSONSerializer serializer = new JSONSerializer().exclude(FILE_PATTERN_CLASS);
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        if (null == jsonResult) {
            jsonResult = new JsonResult();
            jsonResult.setJsonResult(ex.getMessage());
        }
        serializer.include(JSON_PARAM);
        serializer.include(JSON_LIST1);
        serializer.include(JSON_LIST2);
        serializer.include(JSON_LIST3);
        serializer.include(JSON_LIST4);
        serializer.include(JSON_LIST5);
        serializer.include(JSON_LIST6);
        serializer.include(JSON_LIST7);
        serializer.include(JSON_LIST8);
        serializer.include(JSON_LIST9);
        serializer.include(JSON_LIST10);
        serializer.include(JSON_LIST11);
        serializer.include(JSON_LIST12);
        serializer.include(JSON_LIST13);
        serializer.include(JSON_LIST14);
        serializer.include(JSON_LIST15);
        serializer.include(JSON_LIST16);

        strResult = serializer.serialize(jsonResult);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.print(strResult);
            if (null == locale) {
                locale = LocaleUtil.getLocaleFromString(ContextParams.getDefaultLocale());
            }
        } catch (Exception e) {
            LOG.error(e);
            LOG.error(MessageUtil.getApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_E5_0030, new String[] {strResult}));
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    /**
     * Create Supplier Plant Authentication list (pear between S_CD and S_PCD).
     * @param densoSupplierRelationList list of DENSO Supplier Relation.
     * @param screenId current screen ID
     * @return List of SpsMPlantSupplierDomain
     * */
    protected List<SpsMPlantSupplierDomain> doCreateSupplierPlantAuthenList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList, String screenId)
    {
        List<SpsMPlantSupplierDomain> result = new ArrayList<SpsMPlantSupplierDomain>();
        SpsMPlantSupplierDomain plant = null;
        String supplierCode = null;

        UserLoginDomain userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        
        Set<String> companyPlantSet = new HashSet<String>();
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(userLogin.getUserType())) {
            for (DensoSupplierRelationDomain relation : densoSupplierRelationList) {
                supplierCode = relation.getSCd();
                if (!Strings.judgeBlank(supplierCode)) {
                    String companyPlant = StringUtil.appendsString(
                        supplierCode, Constants.SYMBOL_PIPE, relation.getSPcd());
                    companyPlantSet.add(companyPlant);
                }
            }
            
            for (String addedPlant : companyPlantSet) {
                String[] splitted = addedPlant.split(Constants.REGX_PIPE);
                plant = new SpsMPlantSupplierDomain();
                plant.setSCd(splitted[Constants.ZERO]);
                plant.setSPcd(splitted[Constants.ONE]);
                result.add(plant);
            }
        }

        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(userLogin.getUserType())) {
            for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
                if (screenId.equals(roleScreen.getSpsMScreenDomain().getScreenCd())) {
                    for (UserRoleDomain userRole : roleScreen.getUserRoleDomainList()) {
                        supplierCode = userRole.getSCd();
                        if (!Strings.judgeBlank(supplierCode)) {
                            plant = new SpsMPlantSupplierDomain();
                            plant.setSCd(supplierCode);
                            plant.setSPcd(userRole.getSPcd());
                            result.add(plant);
                        }
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Create DENSO Plant Authentication list (pear between D_CD and D_PCD).
     * @param densoSupplierRelationList list of DENSO Supplier Relation.
     * @param screenId current screen ID
     * @return List of SpsMPlantDensoDomain
     * */
    protected List<SpsMPlantDensoDomain> doCreateDensoPlantAuthenList(
        List<DensoSupplierRelationDomain> densoSupplierRelationList, String screenId)
    {
        List<SpsMPlantDensoDomain> result = new ArrayList<SpsMPlantDensoDomain>();
        String densoCode = null;
        SpsMPlantDensoDomain plant = new SpsMPlantDensoDomain(); 

        UserLoginDomain userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        
        Set<String> companyPlantSet = new HashSet<String>();
        if (SupplierPortalConstant.USER_TYPE_SUPPLIER.equals(userLogin.getUserType())) {
            for (DensoSupplierRelationDomain relation : densoSupplierRelationList) {
                densoCode = relation.getDCd();
                if (!Strings.judgeBlank(densoCode)) {
                    String companyPlant = StringUtil.appendsString(
                        densoCode, Constants.SYMBOL_PIPE, relation.getDPcd());
                    companyPlantSet.add(companyPlant);
                }
            }
            for (String addedPlant : companyPlantSet) {
                String[] splitted = addedPlant.split(Constants.REGX_PIPE);
                plant = new SpsMPlantDensoDomain();
                plant.setDCd(splitted[Constants.ZERO]);
                plant.setDPcd(splitted[Constants.ONE]);
                result.add(plant);
            }
        }
        
        if (SupplierPortalConstant.USER_TYPE_DENSO.equals(userLogin.getUserType())) {
            for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
                if (screenId.equals(roleScreen.getSpsMScreenDomain().getScreenCd())) {
                    for (UserRoleDomain userRole : roleScreen.getUserRoleDomainList()) {
                        densoCode = userRole.getDCd();
                        if (!Strings.judgeBlank(densoCode)) {
                            plant = new SpsMPlantDensoDomain();
                            plant.setDCd(densoCode);
                            plant.setDPcd(userRole.getDPcd());
                            result.add(plant);
                        }
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Set HTTP Header for client download CSV file.
     * @param response HTTP Servlet Response
     * @param fileName file name (not include '.csv')
     * */
    protected void setHttpHeaderForCsv(HttpServletResponse response, String fileName) {
        StringBuffer httpStringBuffer = new StringBuffer();
        // "attachment; filename='fileName'.'Constants.REPORT_CSV_FORMAT'
        httpStringBuffer.append(Constants.HTTP_HEADER_ATTACHMENT_FILENAME).append(fileName)
            .append(Constants.SYMBOL_DOT).append(Constants.REPORT_CSV_FORMAT);
        response.setHeader(Constants.HTTP_HEADER_CONTENT_DISPOSITION, 
            httpStringBuffer.toString());
        
        httpStringBuffer.setLength(Constants.ZERO);
        httpStringBuffer.append(Constants.HTTP_HEADER_CONTENTTYPE_APPLICATION)
            .append(Constants.REPORT_CSV_FORMAT);
        response.setContentType(httpStringBuffer.toString());
        
        response.setCharacterEncoding(Constants.HTTP_HEADER_ENCODING_TIS_620);
        //HTTP 1.1
        response.setHeader(Constants.HTTP_HEADER_CACHE_CONTROL, Constants.WORD_PRIVATE);
        //HTTP 1.0
        response.setHeader(Constants.HTTP_HEADER_PRAGMA, Constants.WORD_PRIVATE);
        //prevents caching
        response.setDateHeader(Constants.HTTP_HEADER_EXPIRES, Constants.ZERO);
    }

    /**
     * Set HTTP Header for client download from FileManagementService.
     * @param response HTTP Servlet Response
     * @param request HTTP Servlet Request
     * @param fileName file name
     * @throws UnsupportedEncodingException 
     * */
    protected void setHttpHeaderForFileManagement(HttpServletResponse response,
        HttpServletRequest request, String fileName) throws UnsupportedEncodingException
    {
        StringBuffer application = new StringBuffer();
        StringBuffer attachment = new StringBuffer();
        
        application.append(Constants.HTTP_HEADER_CONTENTTYPE_APPLICATION);
        application.append(SupplierPortalConstant.URL_X_WWW_FORM_URLENCODED);
        
        response.setContentType(application.toString());
        if(request.getHeader(Constants.HTTP_HEADER_USER_AGENT).indexOf(Constants.WORD_MSIE)
            == Constants.CANNOT_READ_FILE)
        {
            attachment.append(Constants.HTTP_HEADER_ATTACHMENT_FILENAME);
            attachment.append(URLEncoder.encode(fileName, Constants.HTTP_HEADER_SHIFT_JIS));
            
            response.setHeader(Constants.HTTP_HEADER_CONTENT_DISPOSITION, attachment.toString());
        } else {
            attachment.append(Constants.HTTP_HEADER_ATTACHMENT_FILENAME);
            attachment.append(URLEncoder.encode(fileName, Constants.HTTP_HEADER_UTF_8));
            
            response.setHeader(Constants.HTTP_HEADER_CONTENT_DISPOSITION, attachment.toString() );
        }
        
        //HTTP 1.1
        response.setHeader(Constants.HTTP_HEADER_CACHE_CONTROL, Constants.WORD_PRIVATE);
        //HTTP 1.0
        response.setHeader(Constants.HTTP_HEADER_PRAGMA, Constants.WORD_PRIVATE);
        //prevents caching
        response.setDateHeader(Constants.HTTP_HEADER_EXPIRES, Constants.ZERO);
    }
    
    /**
     * 
     * <p>Method for set user login information from session.</p>
     *
     * @return String the user login information.
     */
    protected String setUserLogin(){
        StringBuffer userLoginBuffer = new  StringBuffer();
        String companyName = new String();
        String userLogin = new String();
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        
        String firstName = userLoginDomain.getSpsMUserDomain().getFirstName();
        /*String middleName = userLoginDomain.getSpsMUserDomain().getMiddleName();*/
        String LastName = userLoginDomain.getSpsMUserDomain().getLastName();
        
        if(userLoginDomain.getUserType().equals(SupplierPortalConstant.USER_TYPE_SUPPLIER)){
            
            if(null != userLoginDomain.getUserSupplierDetailDomain()
                .getSpsMCompanySupplierDomain()){
                companyName = userLoginDomain.getUserSupplierDetailDomain()
                    .getSpsMCompanySupplierDomain().getCompanyName();
            } else {
                companyName = Constants.EMPTY_STRING;
            }
            
        }else if(userLoginDomain.getUserType().equals(SupplierPortalConstant.USER_TYPE_DENSO)){
            
            if(null != userLoginDomain.getUserDensoDetailDomain()
                .getSpsMCompanyDensoDomain()){
                companyName = userLoginDomain.getUserDensoDetailDomain()
                    .getSpsMCompanyDensoDomain().getCompanyName();
            } else {
                companyName = Constants.EMPTY_STRING;
            }
            
        }
        
        userLogin = userLoginBuffer.append(firstName).append(Constants.SYMBOL_SPACE)
            .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_SPACE).append(LastName)
            .append(Constants.SYMBOL_SPACE).append(Constants.SYMBOL_OPEN_SQUARE_BRACKET)
            .append(companyName).append(Constants.SYMBOL_CLOSE_SQUARE_BRACKET).toString();
        
        return userLogin;
    }
}