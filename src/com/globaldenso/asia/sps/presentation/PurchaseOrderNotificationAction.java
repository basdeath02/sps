/*
 * ModifyDate Development company     Describe 
 * 2014/08/18 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.PurchaseOrderNotificationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.presentation.form.Word005Form;

/**
 * The Class PurchaseOrderNotificationAction.
 * <p>Display list of pending reason and detail for P/O after click Preview Pending button from
 * screen Purchase Order Acknowledgement</p>
 * <ul>
 * <li>Method initial   : doInitial</li>
 * <li>Method initial   : doReturn</li>
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderNotificationAction extends CoreAction {

    /** The Purchase Order Notification Facade Service. */
    private PurchaseOrderNotificationFacadeService purchaseOrderNotificationFacadeService;
    
    /** The Default constructor. */
    public PurchaseOrderNotificationAction() {
        super();
    }
    
    /**
     * <p>Setter method for purchaseOrderNotificationFacadeService.</p>
     *
     * @param purchaseOrderNotificationFacadeService Set for purchaseOrderNotificationFacadeService
     */
    public void setPurchaseOrderNotificationFacadeService(
        PurchaseOrderNotificationFacadeService purchaseOrderNotificationFacadeService) {
        this.purchaseOrderNotificationFacadeService = purchaseOrderNotificationFacadeService;
    }

    /**
     * Do initial.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>In case click Preview Pending button in Purchase Order Acknowledgement screen.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word005Form form = (Word005Form)actionForm;
        form.setUserLogin(super.setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        form.resetForm();
        
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain
            = new PurchaseOrderNotificationDomain();
        
        try{
            // 2. Call Façade Service PurchaseOrderNotificationFacadeService initial()
            purchaseOrderNotificationDomain.setLocale(locale);
            if (!Strings.judgeBlank(form.getPoId())) {
                purchaseOrderNotificationDomain.getSpsTPo().setPoId(
                    new BigDecimal(form.getPoId()));
            }
            List<PurchaseOrderNotificationDomain> notificationList
                = this.purchaseOrderNotificationFacadeService.searchInitial(
                    purchaseOrderNotificationDomain);
            
            // 3. Set value of domain to ActionForm
            form.setPurchaseOrderNotificationList(notificationList);
            form.setMax(notificationList.size());
            
            form.setReturnFlag(Constants.STR_ONE);
            try {
                UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
                if (!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD005_BRETURN)) 
                {
                    this.purchaseOrderNotificationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD005_BRETURN, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setReturnFlag(Constants.STR_ZERO);
                form.setCannotReturnMessage(applicationException.getMessage());
            }
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        // doInitial
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL
            , SupplierPortalConstant.METHOD_DO_INITIAL);
        
        // Akat Remark : may be not use.
        // doReturn
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN
            , SupplierPortalConstant.METHOD_DO_RETURN);
        return keyMethodMap;
    }

    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getUserLoginFromDensoContext() {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
}
