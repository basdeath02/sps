/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/09 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.AsnInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.presentation.form.Winv002Form;

/**
 * The Class ASNInformationAction.
 * @author CSI
 */
public class AsnInformationAction extends CoreAction {

    /** The ASN Information Facade Service. */
    private AsnInformationFacadeService asnInformationFacadeService = null;
    
    
    /** The default constructor. */
    public AsnInformationAction() {
        super();
    }

    /**
     * Set the ASN Information Facade Service.
     * 
     * @param asnInformationFacadeService the ASN Information Facade Service to set
     */
    public void setAsnInformationFacadeService(
        AsnInformationFacadeService asnInformationFacadeService) {
        this.asnInformationFacadeService = asnInformationFacadeService;
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        AsnInformationReturnDomain result  = null;
        try {
            
            form.setSessionId(request.getSession().getId());
            form.setUserLogin(super.setUserLogin());
            
            form.setCountGroupAsn(Constants.STR_ZERO);
            this.initialCombobox(form, true, false);
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV002_BGROUPINVOICE)){
                    this.asnInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV002_BGROUPINVOICE, locale);
                }
                form.setCannotGroupMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotGroupMessage( ae.getMessage() );
            }
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV002_BRESET)){
                    this.asnInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV002_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }
            
        } catch (ApplicationException applicationException) {
            result = new AsnInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, applicationException.getMessage()));
        } catch (Exception e) {
            result = new AsnInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        } finally{
            this.setMessageOnScreen(form, result, false);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Do return.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Winv002Form sessionForm = (Winv002Form)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WINV002_FORM);
        
        AsnInformationReturnDomain result  = null;
        
        try {
            
            form.setSessionId(request.getSession().getId());
            form.setUserLogin(super.setUserLogin());
            
            this.setCriteriaOnScreen(sessionForm, form);
            result = this.search(form);
            form.setAsnInformationForDisplayList(result.getAsnInformationDomainList());
            
            Map<String, AsnInformationDomain> asnSelectedMap = 
                (HashMap<String, AsnInformationDomain>)DensoContext.get()
                    .getGeneralArea(Constants.ASN_SELECT_MAP);
            
            for(String itemAsnSelected : asnSelectedMap.keySet()){
                for(AsnInformationDomain item : result.getAsnInformationDomainList()){
                    if(itemAsnSelected.equals(item.getSpsTAsnDomain().getAsnNo())){
                        item.setAsnSelected(itemAsnSelected);
                    }
                }
            }
            
            form.setCountGroupAsn(String.valueOf(asnSelectedMap.size()));
        } catch (ApplicationException ae) {
            
            result = new AsnInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        } catch (Exception e) {
            
            result = new AsnInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        } finally{
            try{
                this.setMessageOnScreen(form, result, true);
                this.initialCombobox(form, false, true);
                if (!Strings.judgeBlank(form.getVendorCd())){
                    List<PlantSupplierDomain> plantSupplierList = this.getPlantSupplier(form);
                    form.setPlantSupplierList(plantSupplierList);
                }
                if (!Strings.judgeBlank(form.getDCd())){
                    List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                    form.setPlantDensoList(plantDensoList);
                }
            }catch (ApplicationException ae) {
                form.getApplicationMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        
        AsnInformationReturnDomain result  = null;
        UserLoginDomain userLoginDomain = null;
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            
            try {
                form.setSessionId(request.getSession().getId());
                form.setUserLogin(super.setUserLogin());
                
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV002_BSEARCH)) 
                {
                    this.asnInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV002_BSEARCH, locale);
                }
                
                result = this.search(form);
                if(null == result.getErrorMessageList() 
                    || result.getErrorMessageList().size() <= Constants.ZERO){
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        DensoContext.get().putGeneralArea(Constants.ASN_SELECT_MAP,
                            new HashMap<String, AsnInformationDomain>());
                        form.setCountGroupAsn(String.valueOf(Constants.ZERO));
                    }
                    else{
                        this.setAsnSelected(form);
                        Map<String, AsnInformationDomain> asnSelectedMap = 
                            (HashMap<String, AsnInformationDomain>)DensoContext.get()
                                .getGeneralArea(Constants.ASN_SELECT_MAP);
                        
                        for(String itemAsnSelected : asnSelectedMap.keySet()){
                            for(AsnInformationDomain item : result.getAsnInformationDomainList()){
                                if(itemAsnSelected.equals(item.getSpsTAsnDomain().getAsnNo())){
                                    item.setAsnSelected(itemAsnSelected);
                                }
                            }
                        }
                    }
                    userLoginDomain = this.getCurrentUserLoginInformation();
                    form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                    form.setAsnInformationForDisplayList(result.getAsnInformationDomainList());
                    form.setMessageType(Constants.MESSAGE_SUCCESS);
                }
            } catch (ApplicationException ae) {
                
                result = new AsnInformationReturnDomain();
                result.getErrorMessageList().add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, ae.getMessage()));
            } catch (Exception e) {
                
                result = new AsnInformationReturnDomain();
                result.setLocale(locale);
                this.setMessageForException(result, e);
            } finally {
                
                try{
                    this.setMessageOnScreen(form, result, true);
                    this.initialCombobox(form, false, true);
                    if (!Strings.judgeBlank(form.getVendorCd())){
                        List<PlantSupplierDomain> plantSupplierList = this.getPlantSupplier(form);
                        form.setPlantSupplierList(plantSupplierList);
                    }
                    if (!Strings.judgeBlank(form.getDCd())){
                        List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                        form.setPlantDensoList(plantDensoList);
                    }
                }catch (ApplicationException ae) {
                    form.getApplicationMessageList().add(new ApplicationMessageDomain(
                        Constants.MESSAGE_FAILURE, ae.getMessage()));
                }
            }
        }
        
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doGroup(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        Map<String, AsnInformationDomain> asnSelectedMap = null;
        AsnInformationReturnDomain result = null;
        StringBuffer forward = null;
        
        try {
            form.setSessionId(request.getSession().getId());
            form.setUserLogin(super.setUserLogin());
            
            this.setAsnSelected(form);
            asnSelectedMap = (HashMap<String, AsnInformationDomain>)DensoContext.get()
                .getGeneralArea(Constants.ASN_SELECT_MAP);
            result = this.asnInformationFacadeService.validateGroupInvoice(asnSelectedMap, locale);
            
            if(Constants.ZERO == result.getErrorMessageList().size()){
                
                UserLoginDomain sessionUserLoginDomain = (UserLoginDomain)DensoContext.get().
                    getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
                
                Winv002Form sessionForm = new Winv002Form();
                this.setCriteriaOnScreen(form, sessionForm);
                DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WINV002_FORM,
                    sessionForm);
                
                forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_WINV004_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_DSCID)
                    .append(sessionUserLoginDomain.getDscId());
                forward.append(SupplierPortalConstant.URL_PARAM_VENDOR_CD)
                    .append(result.getVendorCd());
                forward.append(SupplierPortalConstant.URL_PARAM_SUPPLIER_PLANT_TAX_ID)
                    .append(result.getSupplierPlantTaxId());
                forward.append(SupplierPortalConstant.URL_PARAM_DCD)
                    .append(result.getDCd());
                forward.append(SupplierPortalConstant.URL_PARAM_DPCD)
                    .append(result.getDPcd());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    forward.append(SupplierPortalConstant.URL_PARAM_WARNING_MESSAGE)
                        .append(result.getWarningMessageList().get(Constants.ZERO).getMessage());
                }
                forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                    .append(SupplierPortalConstant.SCREEN_ID_WINV002);
            }
            else{
                AsnInformationReturnDomain asnInformationResult = this.search(form);
                
                for(String itemAsnSelected : asnSelectedMap.keySet()){
                    for(AsnInformationDomain item 
                        : asnInformationResult.getAsnInformationDomainList()){
                        if(itemAsnSelected.equals(item.getSpsTAsnDomain().getAsnNo())){
                            item.setAsnSelected(itemAsnSelected);
                        }
                    }
                }
                
                form.setAsnInformationForDisplayList(
                    asnInformationResult.getAsnInformationDomainList());
                form.setCountGroupAsn(String.valueOf(asnSelectedMap.size()));
            }
        } catch (ApplicationException ae) {
            
            result = new AsnInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        } catch (Exception e) {
            
            form.setMax(Constants.ZERO);
            result = new AsnInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }
        
        if(null == forward){
            this.setMessageOnScreen(form, result, false);
            this.initialCombobox(form, false, true);
            if (!Strings.judgeBlank(form.getVendorCd())){
                List<PlantSupplierDomain> plantSupplierList = this.getPlantSupplier(form);
                form.setPlantSupplierList(plantSupplierList);
            }
            if (!Strings.judgeBlank(form.getDCd())){
                List<PlantDensoDomain> plantDensoList = this.getPlantDenso(form);
                form.setPlantDensoList(plantDensoList);
            }
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }else{
            return new ActionForward(forward.toString());
        }
    }
    
    /**
     * Do download legend file.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadLegendFile (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        AsnInformationDomain asnInformationDomain = null;
        AsnInformationReturnDomain result = null;
        String fileId = null;
        String pdfFileName = null;
        OutputStream outputStream = null;
        UserLoginDomain userLoginDomain = null;
        
        try{
            
            form.setSessionId(request.getSession().getId());
            form.setUserLogin(super.setUserLogin());
            
            userLoginDomain = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            
            fileId = ContextParams.getLegendFileId();
            asnInformationDomain = new AsnInformationDomain();
            asnInformationDomain.setFileId(fileId);
            FileManagementDomain fileManagementDomain =
                this.asnInformationFacadeService.searchFileName(asnInformationDomain);
            pdfFileName = fileManagementDomain.getFileName();
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            outputStream = response.getOutputStream();
            this.asnInformationFacadeService.searchLegendInfo(asnInformationDomain, outputStream);
            outputStream.flush();
            form.setMessageType(Constants.MESSAGE_SUCCESS);
            return null;
        }catch(ApplicationException ae){
            
            result = new AsnInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        }catch(Exception e){
            
            result = new AsnInformationReturnDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }finally{
            
            if(null != outputStream){
                outputStream.close();
            }
            this.setMessageOnScreen(form, result, false);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * <p>Denso company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when company combo box change load new denso plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantDensoDomain> plantDensoList = null;
        try{
            
            plantDensoList = this.getPlantDenso(form);
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        }catch(ApplicationException ae){
            
            printJson(response, null, ae);
        }catch(Exception e){
            
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        
        return null;
    }

    /**
     * <p>Denso plant code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when plant combo box change load new denso company code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        DataScopeControlDomain dataScopeControlDomain = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.setLocale(locale);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        if(Constants.MISC_CODE_ALL.equals(form.getDPcd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(
                Constants.EMPTY_STRING);
        }else{
            plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(form.getDPcd());
        }

        List<CompanyDensoDomain> companyDensoList = null;
        try {
            companyDensoList = this.asnInformationFacadeService.searchSelectedPlantDenso(
                plantDensoWithScopeDomain);
            printJson(response, new JsonResult((List<Object>)(Object)companyDensoList), null);
        } catch(ApplicationException ae) {
            printJson(response, null, ae);
        } catch(Exception e) {
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * <p>Supplier company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when supplier company code combo box change load new supplier plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantSupplierDomain> plantSupplierList = null;
        
        try{
            
            plantSupplierList = this.getPlantSupplier(form);
            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        }catch(ApplicationException ae){
            
            printJson(response, null, ae);
        }catch(Exception e){
            
            applicationMessageList.add(this.getMessageForException(e));
        }finally{
            
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        
        return null;
    }

    /**
     * <p>Supplier company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when supplier company code combo box change load new supplier plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv002Form form = (Winv002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        DataScopeControlDomain dataScopeControlDomain = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain 
            = new PlantSupplierWithScopeDomain();
        plantSupplierWithScopeDomain.setLocale(locale);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        if(Constants.MISC_CODE_ALL.equals(form.getSPcd())){
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setSPcd(
                Constants.EMPTY_STRING);
        }else{
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setSPcd(form.getSPcd());
        }
        
        List<CompanySupplierDomain> companySupplierList = null;
        try{
            companySupplierList = this.asnInformationFacadeService.searchSelectedPlantSupplier(
                plantSupplierWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)companySupplierList), null);
        }catch(ApplicationException ae){
            printJson(response, null, ae);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH, 
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GROUP,
            SupplierPortalConstant.METHOD_DO_GROUP);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN, 
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_LEGEND_FILE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        
        return keyMethodMap;
    }
    
    /**
     * Search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * </ul>
     * @param form the WINV002 form
     * @return list of invoice information domain
     * @throws ApplicationException 
     */
    private AsnInformationReturnDomain search(Winv002Form form) throws ApplicationException{
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnInformationDomain asnInformationDomain = new AsnInformationDomain();
        
        AsnInformationReturnDomain result = null;
        List<AsnInformationDomain> asnInformationDomainList = null;
        int rowNumFrom = Constants.ZERO;
        int rowNumTo = Constants.ZERO;
        
        asnInformationDomain.setLocale(locale);
        if(!Strings.judgeBlank(form.getVendorCd())){
            asnInformationDomain.getSpsTAsnDomain().setVendorCd(form.getVendorCd());
        }
        if(!Strings.judgeBlank(form.getSPcd())){
            asnInformationDomain.getSpsTAsnDomain().setSPcd(form.getSPcd());
        }
        if(!Strings.judgeBlank(form.getDCd())){
            asnInformationDomain.getSpsTAsnDomain().setDCd(form.getDCd());
        }
        if(!Strings.judgeBlank(form.getDPcd())){
            asnInformationDomain.getSpsTAsnDomain().setDPcd(form.getDPcd());
        }
        if(!Strings.judgeBlank(form.getPlanEtaFrom())){
            asnInformationDomain.setPlanEtaDateFrom(form.getPlanEtaFrom());
        }
        if(!Strings.judgeBlank(form.getPlanEtaTo())){
            asnInformationDomain.setPlanEtaDateTo(form.getPlanEtaTo());
        }
        if(!Strings.judgeBlank(form.getPlanEtaTimeFrom())){
            asnInformationDomain.setPlanEtaTimeFrom(form.getPlanEtaTimeFrom());
        }
        if(!Strings.judgeBlank(form.getPlanEtaTimeTo())){
            asnInformationDomain.setPlanEtaTimeTo(form.getPlanEtaTimeTo());
        }
        if(!Strings.judgeBlank(form.getAsnStatus())){
            asnInformationDomain.getSpsTAsnDomain().setAsnStatus(form.getAsnStatus());
        }
        if(!Strings.judgeBlank(form.getAsnNo())){
            asnInformationDomain.getSpsTAsnDomain().setAsnNo(form.getAsnNo().toUpperCase().trim());
        }
        if(!Strings.judgeBlank(form.getActualEtdFrom())){
            asnInformationDomain.setActualEtdDateFrom(form.getActualEtdFrom());
        }
        if(!Strings.judgeBlank(form.getActualEtdTo())){
            asnInformationDomain.setActualEtdDateTo(form.getActualEtdTo());
        }
        if(!Strings.judgeBlank(form.getSupplierPlantTaxId())){
            asnInformationDomain.setSTaxId(form.getSupplierPlantTaxId().toUpperCase().trim());
        }
        
        asnInformationDomain.setSupplierAuthenList(form.getSupplierAuthenList());
        asnInformationDomain.setDensoAuthenList(form.getDensoAuthenList());
        
        if(Constants.PAGE_SEARCH.equals(form.getFirstPages())){
            asnInformationDomain.setPageNumber(Constants.ONE);
        }
        else{
            asnInformationDomain.setPageNumber(form.getPageNo());
        }
        
        result = this.asnInformationFacadeService.searchAsnInformation(asnInformationDomain);
        if(null == result.getErrorMessageList() 
            || result.getErrorMessageList().size() <= Constants.ZERO){
            
            SpsPagingUtil.setFormForPaging(asnInformationDomain, form);
            rowNumTo = asnInformationDomain.getRowNumTo();
            
            asnInformationDomainList = new ArrayList<AsnInformationDomain>();
            for(rowNumFrom = asnInformationDomain.getRowNumFrom() - Constants.ONE; 
                rowNumFrom < rowNumTo ; rowNumFrom++){
                
                asnInformationDomainList.add(result.getAsnInformationDomainList().get(rowNumFrom));
            }
            result.setAsnInformationDomainList(asnInformationDomainList);
        }
        
        return result;
    }
    
    /**
     * Get plant denso.
     * <p>Get Plant Denso Data for initial combobox.</p>
     * 
     * @param form the Winv002Form form
     * @return the RoleScreenDomain
     * @throws ApplicationException ApplicationException
     */
    private List<PlantDensoDomain> getPlantDenso(Winv002Form form)throws ApplicationException {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        DataScopeControlDomain dataScopeControlDomain = null;
        List<PlantDensoDomain> plantDensoList = null;
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.setLocale(locale);
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        if(Constants.MISC_CODE_ALL.equals(form.getDCd())){
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(
                Constants.EMPTY_STRING);
        }else{
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
        }
        
        try{
            plantDensoList = this.asnInformationFacadeService.searchSelectedCompanyDenso(
                plantDensoWithScopeDomain);
        }catch(ApplicationException ae){
            throw ae;
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        return plantDensoList;
    }
    
    /**
     * Get plant supplier.
     * <p>Get plant supplier data for initial combobox.</p>
     * 
     * @param form the Winv002Form form
     * @return the RoleScreenDomain
     * @throws ApplicationException ApplicationException
     */
    private List<PlantSupplierDomain> getPlantSupplier(Winv002Form form) 
        throws ApplicationException {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        DataScopeControlDomain dataScopeControlDomain = null;
        List<PlantSupplierDomain> plantSupplierList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain 
            = new PlantSupplierWithScopeDomain();
        plantSupplierWithScopeDomain.setLocale(locale);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        if(Constants.MISC_CODE_ALL.equals(form.getVendorCd())){
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                Constants.EMPTY_STRING);
        }else{
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
        }
        
        try{
            plantSupplierList = this.asnInformationFacadeService.searchSelectedCompanySupplier(
                plantSupplierWithScopeDomain);
        }catch(ApplicationException ae){
            throw ae;
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
        return plantSupplierList;
    }
    
    /**
     * Method to get current user login information detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation(){
        
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for ASN Information Screen.
     * <p>Get roles from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain getRoleScreen(UserLoginDomain userLoginDomain) {
        
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WINV002.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * Search.
     * <p>Search data by criteria from screen.</p>
     * 
     * @param form the WINV001 form
     * @throws ApplicationException the application exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    private void setAsnSelected(Winv002Form form) throws ApplicationException{
        
        List<AsnInformationDomain> result = form.getAsnInformationForDisplayList();
        Map<String, AsnInformationDomain> asnSelectedMap = (HashMap<String, AsnInformationDomain>)
            DensoContext.get().getGeneralArea(Constants.ASN_SELECT_MAP);
        
        for(AsnInformationDomain item : result){
            if(!Strings.judgeBlank(item.getAsnSelected())){
                if(!asnSelectedMap.containsKey(item.getSpsTAsnDomain().getAsnNo())){
                    asnSelectedMap.put(item.getSpsTAsnDomain().getAsnNo(), item);
                }
            }
            else{
                if(asnSelectedMap.containsKey(item.getSpsTAsnDomain().getAsnNo())){
                    asnSelectedMap.remove(item.getSpsTAsnDomain().getAsnNo());
                }
            }
        }
    }
    
    /**
     * initialCombobox.
     * <p>Initial combo box from screen.</p>
     * 
     * @param form the WINV002 form
     * @param isInitial the boolean
     * @param isReturn the boolean
     * @throws ApplicationException the application exception
     */
    private void initialCombobox(Winv002Form form, boolean isInitial, boolean isReturn) 
        throws ApplicationException {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        RoleScreenDomain roleScreenDomain = this.getRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        dataScopeControlDomain.setLocale(locale);
        
        AsnInformationReturnDomain asnInformationReturnDomain = null;
       
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        try{
            if(isInitial){
                asnInformationReturnDomain = this.asnInformationFacadeService.searchInitial(
                    dataScopeControlDomain, new AsnInformationDomain());
                
                if(!isReturn){
                    form.resetForm();
                }
                form.setCompanySupplierList(asnInformationReturnDomain.getCompanySupplierList());
                form.setCompanyDensoList(asnInformationReturnDomain.getCompanyDensoList());
            }
            else{
                AsnInformationDomain asnInformationDomain = new AsnInformationDomain();
                asnInformationDomain.getSpsTAsnDomain().setDCd(form.getDCd());
                asnInformationDomain.getSpsTAsnDomain().setVendorCd(form.getVendorCd());
                
                asnInformationReturnDomain = this.asnInformationFacadeService.searchInitial(
                    dataScopeControlDomain, asnInformationDomain);
                
                // [FIX] D.CD and S.CD combo box has no item when return
                form.setCompanySupplierList(asnInformationReturnDomain.getCompanySupplierList());
                form.setCompanyDensoList(asnInformationReturnDomain.getCompanyDensoList());
            }

            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV002));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                dataScopeControlDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV002));
            
            form.setPlantSupplierList(asnInformationReturnDomain.getPlantSupplierList());
            form.setPlantDensoList(asnInformationReturnDomain.getPlantDensoList());
            form.setAsnStatusList(asnInformationReturnDomain.getAsnStatusList());
            
        }catch(ApplicationException ae){
            throw ae;
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
    }
    
    /**
     * setCriteriaOnScreen.
     * <p>Setting data between action form and  session form.</p>
     * 
     * @param sourceForm the Winv002Form
     * @param targetForm the Winv002Form
     */
    private void setCriteriaOnScreen(Winv002Form sourceForm, Winv002Form targetForm) {
        
        if(!Strings.judgeBlank(sourceForm.getVendorCd())){
            targetForm.setVendorCd(sourceForm.getVendorCd());
        }
        if(!Strings.judgeBlank(sourceForm.getSPcd())){
            targetForm.setSPcd(sourceForm.getSPcd());
        }
        if(!Strings.judgeBlank(sourceForm.getDCd())){
            targetForm.setDCd(sourceForm.getDCd());
        }
        if(!Strings.judgeBlank(sourceForm.getDPcd())){
            targetForm.setDPcd(sourceForm.getDPcd());
        }
        if(!Strings.judgeBlank(sourceForm.getPlanEtaFrom())){
            targetForm.setPlanEtaFrom(sourceForm.getPlanEtaFrom());
        }
        if(!Strings.judgeBlank(sourceForm.getPlanEtaTo())){
            targetForm.setPlanEtaTo(sourceForm.getPlanEtaTo());
        }
        if(!Strings.judgeBlank(sourceForm.getPlanEtaTimeFrom())){
            targetForm.setPlanEtaTimeFrom(sourceForm.getPlanEtaTimeFrom());
        }
        if(!Strings.judgeBlank(sourceForm.getPlanEtaTimeTo())){
            targetForm.setPlanEtaTimeTo(sourceForm.getPlanEtaTimeTo());
        }
        if(!Strings.judgeBlank(sourceForm.getAsnStatus())){
            targetForm.setAsnStatus(sourceForm.getAsnStatus());
        }
        if(!Strings.judgeBlank(sourceForm.getAsnNo())){
            targetForm.setAsnNo(sourceForm.getAsnNo());
        }
        if(!Strings.judgeBlank(sourceForm.getActualEtdFrom())){
            targetForm.setActualEtdFrom(sourceForm.getActualEtdFrom());
        }
        if(!Strings.judgeBlank(sourceForm.getActualEtdTo())){
            targetForm.setActualEtdTo(sourceForm.getActualEtdTo());
        }
        if(!Strings.judgeBlank(sourceForm.getSupplierPlantTaxId())){
            targetForm.setSupplierPlantTaxId(sourceForm.getSupplierPlantTaxId());
        }
        
        targetForm.setSupplierAuthenList(sourceForm.getSupplierAuthenList());
        targetForm.setDensoAuthenList(sourceForm.getDensoAuthenList());
        
        targetForm.setPageNo(Constants.ONE);
        targetForm.setPages(Constants.PAGE_CLICK);
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param result the ASN Information Return Domain
     * @param e the Exception
     */
    private void setMessageForException(AsnInformationReturnDomain result, Exception e){
        try {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(result.getLocale(),
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()})));
        } catch (Exception exception) {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()})));
        }
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param e the Exception
     * @return the String
     */
    private ApplicationMessageDomain getMessageForException(Exception e){
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        ApplicationMessageDomain applicationMessage = null;
        
        try {
            applicationMessage = new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()}));
        } catch (Exception exception) {
            applicationMessage = new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()}));
        }
        return applicationMessage;
    }
    
    /**
     * setMessageOnScreen.
     * <p>Setting message for appearance on screen.</p>
     * 
     * @param form the WINV002 form
     * @param result the ASN Information Return Domain
     * @param searchFlag the boolean
     */
    private void setMessageOnScreen(Winv002Form form, AsnInformationReturnDomain result,
        boolean searchFlag) {
        
        if(null != result){
            List<ApplicationMessageDomain> applicationMessageList 
                = new ArrayList<ApplicationMessageDomain>();
            
            if(null != result.getErrorMessageList() 
                && Constants.ZERO < result.getErrorMessageList().size()){
                applicationMessageList.addAll(result.getErrorMessageList());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    applicationMessageList.addAll(result.getWarningMessageList());
                }
                form.setApplicationMessageList(applicationMessageList);
            }else if(searchFlag && null != result.getWarningMessageList()){
                applicationMessageList.addAll(result.getWarningMessageList());
                form.setApplicationMessageList(applicationMessageList);
            }
        }
    }
}
