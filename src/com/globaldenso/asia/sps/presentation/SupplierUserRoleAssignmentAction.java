/*
 * ModifyDate Development company     Describe 
 * 2014/05/19 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wadm004Form;
import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.business.service.SupplierUserRoleAssignmentFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserRoleAssignmentDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;

/**
 * The Class SupplierUserRoleAssignmentAction.
 * <p>
 * Receive value from WADM004_SupplierUserRoleAssignment.jsp for initial value and send
 * to supplierUserRoleAssignmentFacadeServiceImpl
 * for search
 * </p>
 * <ul>
 * <li>Method initial : doInitial</li>
 * <li>Method initial : doInitialPopup</li>
 * <li>Method create : doRegister</li>
 * <li>Method update : doUpdate</li>
 * <li>Method return : doReturn</li>
 * <li>Method update : doDelete</li>
 * </ul>
 * 
 * @author CSI
 */
public class SupplierUserRoleAssignmentAction extends CoreAction {

    /** The Supplier User Facade service. */
    private SupplierUserRoleAssignmentFacadeService supplierUserRoleAssignmentFacadeService;
    
    
    /**
     * Instantiates a new SupplierUserRoleAssignmentAction.
     */
    public SupplierUserRoleAssignmentAction() {
        super();
    }
    
    /**
     * Sets the supplier User Role Facade Service.
     * 
     * @param supplierUserRoleAssignmentFacadeService the supplier User Role Facade Service.
     */
    public void setSupplierUserRoleAssignmentFacadeService(
            SupplierUserRoleAssignmentFacadeService supplierUserRoleAssignmentFacadeService) {
        this.supplierUserRoleAssignmentFacadeService = supplierUserRoleAssignmentFacadeService;
    }

    /**
     * Do initial.
     * <p>
     * Forward from WADM001 : Supplier User Information / WADM002 : Supplier User Registration.
     * </p>
     * <ul>
     * <li>Initial data of supplier user role for update and create supplier user role.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm004Form form = (Wadm004Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        String message = new String();
        StringBuffer name = new StringBuffer();
        StringBuffer sCdBuffer = new StringBuffer();
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        String userName = new String();
        String supplierCodeName = new String();
        SupplierUserRoleAssignmentDomain criteria = new SupplierUserRoleAssignmentDomain();
        SupplierUserRoleAssignmentDomain result = new SupplierUserRoleAssignmentDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        
        userName = name.append(form.getFirstName().toUpperCase()).append(Constants.SYMBOL_SPACE)
            .append(form.getMiddleName().toUpperCase()).append(Constants.SYMBOL_SPACE)
            .append(form.getLastName().toUpperCase()).toString();
        supplierCodeName = sCdBuffer.append(Constants.SYMBOL_OPEN_BRACKET)
            .append(form.getSCd().trim()).append(Constants.SYMBOL_CLOSE_BRACKET)
            .append(Constants.SYMBOL_SPACE).toString();
        form.setSupplierUserName(userName);
        form.setSupplierCodeName(supplierCodeName);
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Call Facade Service for initial*/
        criteria.getUserSupplierDetailDomain().getUserRoleDetailDomain().setDscId(form.getDscId());
        criteria.setSCd(form.getSCd());
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        criteria.setMode(form.getReturnMode());
        
        /*Function for Paging*/
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }  
                result = supplierUserRoleAssignmentFacadeService.searchInitial(criteria);
                if(null != result.getErrorMessageList()){
                    form.resetForm();
                    applicationMessageList = result.getErrorMessageList();
                }else{
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_ROLE_CODE,
                            new HashMap<String, UserRoleDetailDomain>());
                        form.setCountDelete(String.valueOf(Constants.ZERO));
                    }else{
                        this.doSetSelectedRoleCode(form);
                        Map<String, UserRoleDetailDomain> selectedRoleCodeMap =
                            (HashMap<String, UserRoleDetailDomain>)DensoContext.get()
                                .getGeneralArea(Constants.MAP_KEY_SELECTED_ROLE_CODE);
                        for(String itemKey : selectedRoleCodeMap.keySet()){
                            for(UserRoleDetailDomain item : result.getUserSupplierDetailDomain()
                                .getUserRoleDetailList()){
                                if(itemKey.equals(item.getSeqNo().toString())){
                                    item.setRoleCdSelected(itemKey);
                                }
                            }
                        }
                    }
                    if(!Constants.MODE_REGISTER.equals(form.getReturnMode())){
                        form.setUserRoleDetailList(result.getUserSupplierDetailDomain()
                            .getUserRoleDetailList());
                        SpsPagingUtil.setFormForPaging(result.getUserSupplierDetailDomain()
                            .getUserRoleDetailDomain(), form);
                        //Set update success message
                        if(Constants.MODE_EDIT.equals(form.getReturnMode())){
                            message = MessageUtil.getApplicationMessage(locale, 
                                SupplierPortalConstant.ERROR_CD_SP_I6_0002);
                            applicationMessageList.add(new ApplicationMessageDomain(
                                Constants.MESSAGE_SUCCESS, message));
                        }
                    }else{
                      //Set Register success message
                        message = MessageUtil.getApplicationMessage(locale, SupplierPortalConstant.
                            ERROR_CD_SP_I6_0001);
                        applicationMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_SUCCESS, message));
                    }
                    form.setIsDCompany(result.getIsDCompany());
                    if(!result.getIsDCompany()){
                        message = MessageUtil.getApplicationMessage(locale, SupplierPortalConstant.
                            ERROR_CD_SP_E7_0036);
                        applicationMessageList.add(new ApplicationMessageDomain(
                            Constants.MESSAGE_FAILURE, message));
                    }
                }
                
                try {
                    if (!userLoginDomain.getButtonLinkNameList().contains(
                        RoleTypeConstants.WADM004_LUPDATE)) 
                    {
                        this.supplierUserRoleAssignmentFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WADM004_LUPDATE, locale);
                    }
                } catch (ApplicationException applicationException) {
                    form.setCannotUpdateMessage(applicationException.getMessage());
                }
                
                try {
                    if (!userLoginDomain.getButtonLinkNameList().contains(
                        RoleTypeConstants.WADM004_BADD)) 
                    {
                        this.supplierUserRoleAssignmentFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WADM004_BADD, locale);
                    }
                } catch (ApplicationException applicationException) {
                    form.setCannotAddMessage(applicationException.getMessage());
                }
                
            }catch(ApplicationException applicationException){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Delete.
     * <p>
     * update isActive flag is false in database.
     * </p>
     * <ul>
     * <li>In case click Delete button, system will update data.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doDelete(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm004Form form = (Wadm004Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message =  new String();
        String firstPages = form.getFirstPages();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        List<UserRoleDetailDomain> supplierUserRoleSelectedList = new 
            ArrayList<UserRoleDetailDomain>();
        SupplierUserRoleAssignmentDomain criteria = new SupplierUserRoleAssignmentDomain();
        SupplierUserRoleAssignmentDomain result = new SupplierUserRoleAssignmentDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Call Facade Service for initial*/
        criteria.getUserSupplierDetailDomain().getUserRoleDetailDomain().setDscId(form.getDscId());
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        criteria.setUpdateUser(userLoginDomain.getDscId());
        criteria.setSCd(form.getSCd());
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM004_BDELETE)) 
            {
                this.supplierUserRoleAssignmentFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM004_BDELETE, locale);
            }
            
            this.doSetSelectedRoleCode(form);
            Map<String, UserRoleDetailDomain> selectedRoleCodeMap = 
                (HashMap<String, UserRoleDetailDomain>)DensoContext.get()
                    .getGeneralArea(Constants.MAP_KEY_SELECTED_ROLE_CODE);
            
            if(null != selectedRoleCodeMap && !selectedRoleCodeMap.isEmpty()){
                Iterator<String> iterator = selectedRoleCodeMap.keySet().iterator();
                while(iterator.hasNext()){
                    String keyStr = (String)iterator.next();
                    supplierUserRoleSelectedList.add(selectedRoleCodeMap.get(keyStr));
                }
                if(Constants.ZERO < supplierUserRoleSelectedList.size()){
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        criteria.setPageNumber(Constants.ONE);
                    }else{
                        criteria.setPageNumber(form.getPageNo());
                    }
                    criteria.getUserSupplierDetailDomain()
                        .setUserRoleDetailList(supplierUserRoleSelectedList);
                    supplierUserRoleAssignmentFacadeService.deleteSupplierUserRole(criteria);
                    
                    message = MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0019, new String[] {
                            Constants.MODE_DELETE});
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, message));
                    form.setMethod(SupplierPortalConstant.METHOD_DO_SEARCH);
                    form.setCountDelete(Constants.IS_NOT_ACTIVE);
                    DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_ROLE_CODE,
                        new HashMap<String, UserRoleDetailDomain>());
                }
            }
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
            form.setIsDCompany(true);
            form.setMethod(SupplierPortalConstant.METHOD_DO_INITIAL);
        }
        try{
            /*set initial screen*/
            if(null == firstPages){
                firstPages = Constants.PAGE_SEARCH;
                form.setFirstPages(firstPages);
            }
            if(Constants.PAGE_SEARCH.equals(firstPages)){
                criteria.setPageNumber(Constants.ONE);
            }else{
                criteria.setPageNumber(form.getPageNo());
            }
            result = supplierUserRoleAssignmentFacadeService.searchInitial(criteria);
            SpsPagingUtil.setFormForPaging(result.getUserSupplierDetailDomain()
                .getUserRoleDetailDomain(), form);
            form.setUserRoleDetailList(result.getUserSupplierDetailDomain()
                .getUserRoleDetailList());
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            form.setUserRoleDetailList(null);
            form.setApplicationMessageList(applicationMessageList);
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Initial Pop up.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialPopup(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm004Form form = (Wadm004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        form.setPageType(Constants.PAGE_TYPE_POPUP);
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        SupplierUserRoleAssignmentDomain result = new SupplierUserRoleAssignmentDomain();
        SupplierUserRoleAssignmentDomain criteria = new SupplierUserRoleAssignmentDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        PlantSupplierWithScopeDomain plantSupplierWithScope = new PlantSupplierWithScopeDomain();
        plantSupplierWithScope.getPlantSupplierDomain().setSCd(form.getSCd());
        criteria.setPlantSupplierWithScopeDomain(plantSupplierWithScope);
        try{
            /*Call Facade Service  get all master data for combo box on pop up screen.*/
            result = supplierUserRoleAssignmentFacadeService.searchInitialPopup(criteria);
            if(Constants.MODE_REGISTER.equals(form.getMode())){
                form.setEffectEnd(Constants.EMPTY_STRING);
                form.setEffectStart(Constants.EMPTY_STRING);
                form.setPlantSupplierList(null);
                form.setRoleList(null);
                setMasterCombo(result, form);
            }else if(Constants.MODE_EDIT.equals(form.getMode())){
                setMasterCombo(result, form);
            }
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM004_POK)) 
                {
                    this.supplierUserRoleAssignmentFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM004_POK, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotOkMessage(applicationException.getMessage());
            }
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM004_PCANCEL)) 
                {
                    this.supplierUserRoleAssignmentFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM004_PCANCEL, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotCancelMessage(applicationException.getMessage());
            }
            
        }catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
//            this.setMasterCombo(result, form);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS_POPUP);
    }
    
    /**
     * Do Add User role.
     * <p>
     * insert data from screen into database.
     * </p>
     * <ul>
     * <li>In case click OK button, system will insert data.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Wadm004Form form = (Wadm004Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        String pages = form.getPages();
        String firstPages = form.getFirstPages();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        SupplierUserRoleAssignmentDomain criteria = new SupplierUserRoleAssignmentDomain();
        SupplierUserRoleAssignmentDomain result = new SupplierUserRoleAssignmentDomain();
        
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        /*Set value to argument Domain.*/
        UserRoleDetailDomain detailDomain = new UserRoleDetailDomain();
        detailDomain.setDscId(form.getDscId());
        detailDomain.setRoleCd(form.getRoleCode());
        detailDomain.setSCd(form.getSCd());
        detailDomain.setSPcd(form.getSPcd());
        detailDomain.setEffectStartScreen(form.getEffectStart());
        detailDomain.setEffectEndScreen(form.getEffectEnd());
        detailDomain.setCreateDscId(userDscId);
        detailDomain.setLastUpdateDscId(userDscId);
        criteria.setSCd(form.getSCd());
        criteria.getUserSupplierDetailDomain().setUserRoleDetailDomain(detailDomain);
        criteria.setEffectEndScreen(form.getEffectEnd());
        criteria.setEffectStartScreen(form.getEffectStart());
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try{
                result = supplierUserRoleAssignmentFacadeService.createUserRole(criteria);
                if(null != result.getErrorMessageList()){
                    applicationMessageList = result.getErrorMessageList();
                }else{
                    /*set message for register success.*/
                    message = MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0019, new String[] {
                            Constants.MODE_REGISTER});
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, message));
                }
            }catch (ApplicationException applicationException) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.setApplicationMessageList(applicationMessageList);
                form.setIsDCompany(true);
                form.setMethod(SupplierPortalConstant.METHOD_DO_INITIAL);
            }
            /*set initial screen*/
            try{
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }
                result = supplierUserRoleAssignmentFacadeService.searchInitial(criteria);
                SpsPagingUtil.setFormForPaging(result.getUserSupplierDetailDomain()
                    .getUserRoleDetailDomain(), form);
                form.setUserRoleDetailList(result.getUserSupplierDetailDomain()
                    .getUserRoleDetailList());
            }catch (ApplicationException applicationException) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
                form.setApplicationMessageList(applicationMessageList);
            }catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);

    }

    /**
     * Do update User role.
     * <p>update data from pop up screen into database.</p>
     * <ul>
     * <li>In case click OK button, system will update and validate data.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doUpdate(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Wadm004Form form = (Wadm004Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        String pages = form.getPages();
        String firstPages = form.getFirstPages();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        SupplierUserRoleAssignmentDomain criteria = new SupplierUserRoleAssignmentDomain();
        SupplierUserRoleAssignmentDomain result = new SupplierUserRoleAssignmentDomain();
        
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        
        /*Set value to argument Domain.*/
        UserRoleDetailDomain detailDomain = new UserRoleDetailDomain();
        detailDomain.setSeqNo(BigDecimal.valueOf(Integer.parseInt(form.getSeqNo())));
        detailDomain.setDscId(form.getDscId());
        detailDomain.setRoleCd(form.getRoleCode());
        detailDomain.setSCd(form.getSCd());
        detailDomain.setSPcd(form.getSPcd());
        detailDomain.setEffectStartScreen(form.getEffectStart());
        detailDomain.setEffectEndScreen(form.getEffectEnd());
        detailDomain.setLastUpdateDscId(userDscId);
        detailDomain.setLastUpdateDatetimeScreen(form.getLastUpdateDatetimeScreen());
        criteria.getUserSupplierDetailDomain().setUserRoleDetailDomain(detailDomain);
        criteria.setEffectEndScreen(form.getEffectEnd());
        criteria.setEffectStartScreen(form.getEffectStart());
        criteria.setRoleCode(form.getRoleNameSelected());
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try{
                result = supplierUserRoleAssignmentFacadeService.updateSupplierUserRole(criteria);
                if(null != result.getErrorMessageList()){
                    applicationMessageList = result.getErrorMessageList();
                }else{
                    /*set message for update success.*/
                    message = MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0019, new String[] {
                            Constants.MODE_UPDATE});
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, message));
                }
            }catch (ApplicationException applicationException) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.setApplicationMessageList(applicationMessageList);
                form.setIsDCompany(true);
                form.setMethod(SupplierPortalConstant.METHOD_DO_INITIAL);
            }
        }
        /*set initial screen*/
        try{
            if(Constants.PAGE_SEARCH.equals(firstPages)){
                criteria.setPageNumber(Constants.ONE);
            }else{
                criteria.setPageNumber(form.getPageNo());
            }
            criteria.setSCd(form.getSCd());
            result = supplierUserRoleAssignmentFacadeService.searchInitial(criteria);
            SpsPagingUtil.setFormForPaging(result.getUserSupplierDetailDomain()
                .getUserRoleDetailDomain(), form);
            form.setUserRoleDetailList(result.getUserSupplierDetailDomain()
                .getUserRoleDetailList());
        }catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            form.setApplicationMessageList(applicationMessageList);
        }catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do return to WADM001.
     * <p>Open WADM001_SupplierUserInformation</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm004Form form = (Wadm004Form) actionForm;
        StringBuilder forward = new StringBuilder();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM004_BRETURN)) 
            {
                this.supplierUserRoleAssignmentFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM004_BRETURN, locale);
            }
            
            if(Constants.MODE_REGISTER.equals(form.getReturnMode())){
                forward.append(SupplierPortalConstant.URL_WADM002_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
            }else{
                forward.append(SupplierPortalConstant.URL_WADM001_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_RETURN);
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
                forward.append(Constants.PAGE_SEARCH);
            }
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(SupplierPortalConstant.ACTION_DELETE, SupplierPortalConstant.METHOD_DO_DELETE);
        map.put(SupplierPortalConstant.ACTION_ADD_USER, SupplierPortalConstant.METHOD_DO_REGISTER);
        map.put(SupplierPortalConstant.ACTION_UPDATE_USER, SupplierPortalConstant.METHOD_DO_UPDATE);
        map.put(SupplierPortalConstant.ACTION_RETURN, SupplierPortalConstant.METHOD_DO_RETURN);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL, 
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_POPUP,
            SupplierPortalConstant.METHOD_DO_INITIAL_POPUP);
        return map;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant
                .SCREEN_ID_WADM004.equals(roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * Do set selected DSC ID
     * @param form the form
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    private void doSetSelectedRoleCode(Wadm004Form form) throws Exception{
        List<UserRoleDetailDomain> itemList = form.getUserRoleDetailList();
        Map<String, UserRoleDetailDomain> selectedRoleCodeMap 
            = (HashMap<String, UserRoleDetailDomain>)DensoContext.get()
                .getGeneralArea(Constants.MAP_KEY_SELECTED_ROLE_CODE);
        for(UserRoleDetailDomain item : itemList){
            if(!StringUtil.checkNullOrEmpty(item.getRoleCdSelected())){
                if(!selectedRoleCodeMap.containsKey(item.getSeqNo().toString())){
                    selectedRoleCodeMap.put(String.valueOf(item.getSeqNo()), item);
                }
            }else{
                if(selectedRoleCodeMap.containsKey(item.getSeqNo().toString())){
                    selectedRoleCodeMap.remove(item.getSeqNo().toString());
                }
            }
        }
        
        if(null != selectedRoleCodeMap && !selectedRoleCodeMap.isEmpty()){
            int countDel = Constants.ZERO;
            Iterator<String> itr = selectedRoleCodeMap.keySet().iterator();
            while(itr.hasNext()){
                itr.next();
                countDel += Constants.ONE;
            }
            form.setCountDelete(String.valueOf(countDel));
        }else{
            form.setCountDelete(String.valueOf(Constants.ZERO));
        }
    }
    /**
     * set combo box.
     * <p>Set all master data for combo box on Pop-up screen.</p>
     * 
     * @param domain that keep data all combo box.
     * @param form that Wadm004Form.
     */
    private void setMasterCombo(SupplierUserRoleAssignmentDomain domain, 
        Wadm004Form form) {
        List<PlantSupplierDomain> plantList = domain.getPlantSupplierList();
        List<SpsMRoleDomain> roleList = domain.getUserSupplierDetailDomain().getSpsMRoleList();
        if(null != plantList){
            form.setPlantSupplierList(plantList);
        }
        if(null != roleList){
            form.setRoleList(roleList);
        }
    }
}