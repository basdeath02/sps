/*
 * ModifyDate Developmentcompany    Describe 
 * 2014/06/17 CSI Karnrawee         Create
 * 2016/03/08 CSI Akat              [IN059]
 * 2018/04/18 Netband U.Rungsiwut   Generate invoice PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.InvoiceMaintenanceFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Winv004Form;

/**
 * The Class InvoiceMaintenanceAction.
 * @author CSI
 */
public class InvoiceMaintenanceAction extends CoreAction {

    /** The invoice maintenance facade service. */
    private InvoiceMaintenanceFacadeService invoiceMaintenanceFacadeService = null;
    
    
    /** The default constructor. */
    public InvoiceMaintenanceAction() {
        super();
    }

    /**
     * Set the invoice maintenance facade service.
     * 
     * @param invoiceMaintenanceFacadeService the invoice maintenance facade service to set
     */
    public void setInvoiceMaintenanceFacadeService(
        InvoiceMaintenanceFacadeService invoiceMaintenanceFacadeService) {
        this.invoiceMaintenanceFacadeService = invoiceMaintenanceFacadeService;
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv004Form form = (Winv004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String screenId = form.getScreenId();
        
        InvoiceMaintenanceDomain result = null;
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setStrConfirm(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_W6_0001 , 
            new String[]{MessageUtil.getLabelHandledException(locale, 
                SupplierPortalConstant.LBL_SEND_INVOICE)}));
        form.setStrConfirmReturn(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_W6_0008));

        form.setStrConfirmCalculate(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_W6_0001 , 
            new String[]{MessageUtil.getLabelHandledException(locale, 
                SupplierPortalConstant.LBL_CALCULATE_CN_WARNING)}));
        
        form.setErrorSupplierInvoiceAmount(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E7_0038 , 
            new String[]{
                MessageUtil.getLabelHandledException(locale, 
                    SupplierPortalConstant.LBL_SUPPLIER_INVOICE_AMOUNT),
                MessageUtil.getLabelHandledException(locale, 
                    SupplierPortalConstant.LBL_DENSO_INVOICE_AMOUNT)
            }));

        form.setErrorSupplierUnitPrice(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E7_0038 , 
            new String[]{
                MessageUtil.getLabelHandledException(locale, 
                    SupplierPortalConstant.LBL_SUPPLIER_UNIT_PRICE),
                MessageUtil.getLabelHandledException(locale, 
                    SupplierPortalConstant.LBL_DENSO_UNIT_PRICE)
            }));
        form.setErrorSupplierBaseNull(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E7_0007 , 
            new String[]{ MessageUtil.getLabelHandledException(locale,
                SupplierPortalConstant.LBL_SUPPLIER_BASE_AMOUNT)
            }));
        
        form.setErrorSupplierVatNull(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E7_0007 , 
            new String[]{ MessageUtil.getLabelHandledException(locale,
                SupplierPortalConstant.LBL_SUPPLIER_VAT_AMOUNT)
            }));
        form.setErrorSupplierTotalNull(MessageUtil.getApplicationMessage(locale, 
            SupplierPortalConstant.ERROR_CD_SP_E7_0007 , 
            new String[]{ MessageUtil.getLabelHandledException(locale,
                SupplierPortalConstant.LBL_SUPPLIER_TOTAL_AMOUNT)
            }));
        
        try {
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV004_BDOWNLOAD)){
                this.invoiceMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV004_BDOWNLOAD, locale);
            }
            form.setCannotDownloadMessage(Constants.EMPTY_STRING);
        } catch (ApplicationException ae) {
            form.setCannotDownloadMessage(ae.getMessage());
        }

        try {
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV004_BCALCULATECN)){
                this.invoiceMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV004_BCALCULATECN, locale);
            }
            form.setCannotCalculateCnMessage(Constants.EMPTY_STRING);
        } catch (ApplicationException ae) {
            form.setCannotCalculateCnMessage(ae.getMessage());
        }
        
        try {
            if(SupplierPortalConstant.SCREEN_ID_WINV002.equals(screenId)){
                Map<String, AsnInformationDomain> asnSelectedMap = 
                    (HashMap<String, AsnInformationDomain>)DensoContext.get()
                        .getGeneralArea(Constants.ASN_SELECT_MAP);
                
                InvoiceMaintenanceDomain invoiceMaintenanceDomain = new InvoiceMaintenanceDomain();
                invoiceMaintenanceDomain.setVendorCd(form.getVendorCd());
                invoiceMaintenanceDomain.setSupplierPlantTaxId(form.getSupplierPlantTaxId());
                invoiceMaintenanceDomain.setDCd(form.getDCd());
                invoiceMaintenanceDomain.setDPcd(form.getDPcd());
                invoiceMaintenanceDomain.setAsnSelectedMap(asnSelectedMap);
                invoiceMaintenanceDomain.setWarningMessage(form.getWarningMessage());
                invoiceMaintenanceDomain.setLocale(locale);
                result = this.invoiceMaintenanceFacadeService
                    .searchInitialGroupInvoiceByManual(invoiceMaintenanceDomain);
                form.setDiffDPcdFlag(result.getDiffDPcdFlag());
                this.setDataOnScreen(form, result);
                this.setMessageOnScreen(form, result);
                form.setMode(Constants.MODE_REGISTER);
            }else if(SupplierPortalConstant.SCREEN_ID_WINV003.equals(screenId)){
                
                InvoiceMaintenanceDomain invoiceMaintenanceDomain = new InvoiceMaintenanceDomain();
                invoiceMaintenanceDomain.setSessionId(form.getSessionId());
                invoiceMaintenanceDomain.setDscId(form.getDscId());
                invoiceMaintenanceDomain.setWarningMessage(form.getWarningMessage());
                invoiceMaintenanceDomain.setScreenId(screenId);
                invoiceMaintenanceDomain.setLocale(locale);
                result = this.invoiceMaintenanceFacadeService
                    .searchInitialGroupInvoiceByCsv(invoiceMaintenanceDomain);
                form.setDiffDPcdFlag(result.getDiffDPcdFlag());
                this.setDataOnScreen(form, result);
                this.setMessageOnScreen(form, result);
                form.setMode(Constants.MODE_UPLOAD);
            }else{
                
                InvoiceMaintenanceDomain invoiceMaintenanceDomain = new InvoiceMaintenanceDomain();
                invoiceMaintenanceDomain.setInvoiceId(form.getInvoiceId());
                invoiceMaintenanceDomain.setLocale(locale);
                result = this.invoiceMaintenanceFacadeService
                    .searchInitialViewInvoiceMaintenance(invoiceMaintenanceDomain);
                //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --start--
                result.setInvoiceId(form.getInvoiceId());
                //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --end--
                form.setMode(Constants.MODE_VIEW);
                this.setDataOnScreen(form, result);
                this.setMessageOnScreen(form, result);
            }
        } catch (ApplicationException ae) {
            
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
            
            form.setDCd(null);
            form.setDPcd(null);
            form.setVendorCd(null);
            form.setMode(Constants.MODE_INITIAL);
        } catch (Exception e) {
            
            result = new InvoiceMaintenanceDomain();
            result.setLocale(locale);
            form.setDCd(null);
            form.setDPcd(null);
            form.setVendorCd(null);
            this.setMessageForException(result, e);
            
        } finally{
            
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do register.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doRegister(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Winv004Form form = (Winv004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceMaintenanceDomain result = null;
        
        form.setUserLogin(super.setUserLogin());
        InvoiceMaintenanceDomain invoiceMaintenanceDomain = this.setCriteriaFromForm(form);
        invoiceMaintenanceDomain.setDscId(form.getDscId());
        try {
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV004_BREGISTER)) 
            {
                this.invoiceMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV004_BREGISTER, locale);
            }
            
            //form.setMode(Constants.MODE_REGISTER);
            
            result = this.invoiceMaintenanceFacadeService
                .transactRegisterInvoice(invoiceMaintenanceDomain);
            
            if(Constants.ZERO == result.getErrorMessageList().size()){
                form.setCoverPageFileId(result.getFileId());
                form.setMode(Constants.MODE_VIEW);
                if (Constants.ZERO == result.getSuccessMessageList().size()) {
                    result.getSuccessMessageList().add(
                        new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                            MessageUtil.getApplicationMessageWithLabel(locale,
                                SupplierPortalConstant.ERROR_CD_SP_I6_0022,
                                SupplierPortalConstant.LBL_REGISTER_INVOICE)));
                }
                if(SupplierPortalConstant.SCREEN_ID_WINV002.equals(form.getScreenId())
                    && Constants.ZERO < result.getAsnNoRegister().size()){
                    Map<String, AsnInformationDomain> asnSelectedMap 
                        = (HashMap<String, AsnInformationDomain>)
                        DensoContext.get().getGeneralArea(Constants.ASN_SELECT_MAP);
                    for(String asnNo : result.getAsnNoRegister()){
                        asnSelectedMap.remove(asnNo);
                    }
                }
                
                if(SupplierPortalConstant.SCREEN_ID_WINV003.equals(form.getScreenId())){
                    InvoiceMaintenanceDomain invoiceMaintenanceCriteriaForDelete 
                        = new InvoiceMaintenanceDomain();
                    invoiceMaintenanceDomain.setSessionId(form.getSessionId());
                    invoiceMaintenanceDomain.setDscId(form.getDscId());
                    this.invoiceMaintenanceFacadeService.deleteTmpUploadInvoice(
                        invoiceMaintenanceCriteriaForDelete);
                }
            }
        } catch (ApplicationException ae) {
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            result = new InvoiceMaintenanceDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }finally{
          //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --start--
            if(!StringUtil.checkNullOrEmpty(result.getInvoiceId())){
                form.setInvoiceId(result.getInvoiceId());
            }
          //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --end--
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do preview.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doPreview(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv004Form form = (Winv004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        InvoiceMaintenanceDomain result = null;
        
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV004_BPREVIEWCOVERPAGE)){
                this.invoiceMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV004_BPREVIEWCOVERPAGE, locale);
            }
            
            InvoiceMaintenanceDomain invoiceMaintenanceDomain = this.setCriteriaFromForm(form);
            result = this.invoiceMaintenanceFacadeService
                .searchPreviewCoverPage(invoiceMaintenanceDomain);
            
            if(null == result.getErrorMessageList() 
                || result.getErrorMessageList().size() <= Constants.ZERO){
                
                this.setHttpHeaderForFileManagement(response, request, result.getFileName());
                FileUtil.writeInputStreamToOutputStream(result.getInvoiceCoverPage(),
                    response.getOutputStream());
                return null;
            }else{
                this.setMessageOnScreen(form, result);
                //form.setMode(Constants.MODE_REGISTER);
            }
        }catch(ApplicationException ae){
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        }catch(Exception e){
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
        }finally{
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download pdf.
     * <p>Download Cover Page Report file.</p>
     * <ul>
     * <li>In case click 'Download PDF' button, system will download Cover Page Report file.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadPdf(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Winv004Form form = (Winv004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceMaintenanceDomain criteria = new InvoiceMaintenanceDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        ServletOutputStream outputStream = null;
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WINV004_BDOWNLOADPDF)){
                this.invoiceMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV004_BDOWNLOADPDF, locale);
            }
            //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --start--
            criteria.setInvoiceId(form.getInvoiceId());
            //[IN1898] Invoice cover page shows wrong in case invoice number are duplicated. --end--
            criteria.setInvoiceNo(form.getInvoiceNo());
            criteria.setDCd(form.getDCd());
            criteria.setDPcd(form.getDPcd());
            criteria.setVendorCd(form.getVendorCd());
            criteria.setDecimalDisp(form.getDecimalDisp());
            criteria.setLocale(locale);
            
            /** to get PDF information. */
            FileManagementDomain fileManagementDomain = invoiceMaintenanceFacadeService.searchGenerateInvoice(criteria);
            
            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());
            
//            FileManagementDomain fileMangement
//                = this.invoiceMaintenanceFacadeService.searchFileName(criteria);
            
//            super.setHttpHeaderForFileManagement(response, request, fileMangement.getFileName());
//            this.invoiceMaintenanceFacadeService.searchDownloadCoverPageReport(criteria,
//                response.getOutputStream());
            return null;
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(Exception e) {
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(applicationMessageList);
        }
        form.setApplicationMessageList(applicationMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Winv004Form form = (Winv004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceMaintenanceDomain result = null;
        ServletOutputStream outputStream = null;
        
        try {
            form.setUserLogin(super.setUserLogin());
            InvoiceMaintenanceDomain invoiceMaintenanceDomain = this.setCriteriaFromForm(form);
            result = this.invoiceMaintenanceFacadeService.searchInvoiceMaintenanceCsv(
                invoiceMaintenanceDomain);
            
            byte[] output = result.getCsvResult().toString().getBytes();
            setHttpHeaderForCsv(response, result.getFileName());
            response.setContentLength(output.length);
            outputStream = response.getOutputStream();
            outputStream.write(output, 0, output.length);
            outputStream.flush();
            
            return null;
        } catch (ApplicationException ae) {
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
        } finally {
            if(null != outputStream) {
                outputStream.close();
            }
            this.setMessageOnScreen(form, result);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do preview.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv004Form form = (Winv004Form)actionForm;
        String screenId = form.getScreenId();
        StringBuffer forward = new StringBuffer();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceMaintenanceDomain result = null;
        try {
            form.setUserLogin(super.setUserLogin());
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV004_BRETURN)) 
            {
                this.invoiceMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV004_BRETURN, locale);
            }
        } catch (ApplicationException ae) {
            
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
            this.setMessageOnScreen(form, result);
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }
        
        if(SupplierPortalConstant.SCREEN_ID_WINV002.equals(screenId)){
            
            forward.append(SupplierPortalConstant.URL_WINV002_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_RETURN);
        }else if(SupplierPortalConstant.SCREEN_ID_WINV003.equals(screenId)){
            
            forward.append(SupplierPortalConstant.URL_WINV003_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_RETURN);
        }else if(SupplierPortalConstant.SCREEN_ID_WINV001.equals(screenId)){
            
            forward.append(SupplierPortalConstant.URL_WINV001_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_RETURN);
        }else if(SupplierPortalConstant.SCREEN_ID_WINV005.equals(screenId)){
            
            forward.append(SupplierPortalConstant.URL_WINV005_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_RETURN);
        }else if(SupplierPortalConstant.SCREEN_ID_WSHP009.equals(screenId)){
            
            forward.append(SupplierPortalConstant.URL_WSHP009_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                .append(SupplierPortalConstant.URL_PARAM_MODE)
                .append(Constants.MODE_INITIAL);
        }
        
        return new ActionForward(forward.toString(), true);
    }
    
    /**
     * Do download legend file.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadLegendFile (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv004Form form = (Winv004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        InvoiceMaintenanceDomain invoiceMaintenance = null;
        InvoiceMaintenanceDomain result = null;
        String fileId = null;
        String pdfFileName = null;
        OutputStream outputStream = null;
        
        try{
            form.setUserLogin(super.setUserLogin());
            
            fileId = ContextParams.getLegendFileId();
            invoiceMaintenance = new InvoiceMaintenanceDomain();
            invoiceMaintenance.setFileId(fileId);
            invoiceMaintenance.setLocale(locale);
            FileManagementDomain fileManagementDomain 
                = this.invoiceMaintenanceFacadeService.searchFileName(invoiceMaintenance);
            
            pdfFileName = fileManagementDomain.getFileName();
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            outputStream = response.getOutputStream();
            this.invoiceMaintenanceFacadeService.searchLegendInfo(invoiceMaintenance, outputStream);
            outputStream.flush();
            form.setMessageType(Constants.MESSAGE_SUCCESS);
        }catch(ApplicationException ae){
            result = new InvoiceMaintenanceDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ae.getMessage()));
        }catch(Exception e){
            result = new InvoiceMaintenanceDomain();
            result.setLocale(locale);
            this.setMessageForException(result, e);
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
            this.setMessageOnScreen(form, result);
        }
        return null;
    }
    
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_PREVIEW,
            SupplierPortalConstant.METHOD_DO_PREVIEW);
        keyMethodMap.put(SupplierPortalConstant.ACTION_REGISTER,
            SupplierPortalConstant.METHOD_DO_REGISTER);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_LEGEND_FILE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF);
        return keyMethodMap;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation(){
        
        UserLoginDomain sessionUserLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        
        List<RoleScreenDomain> roleScreenList = null;
        RoleScreenDomain roleScreenDomain = null;
        
        userLoginDomain.setDscId(sessionUserLoginDomain.getDscId());
        userLoginDomain.setUserType(sessionUserLoginDomain.getUserType());
        userLoginDomain.setSpsMUserDomain(sessionUserLoginDomain.getSpsMUserDomain());
        
        userLoginDomain.getUserDensoDetailDomain().setSpsMUserDensoDomain(sessionUserLoginDomain
            .getUserDensoDetailDomain().getSpsMUserDensoDomain());
        userLoginDomain.getUserDensoDetailDomain().setSpsMCompanyDensoDomain(sessionUserLoginDomain
            .getUserDensoDetailDomain().getSpsMCompanyDensoDomain());
        userLoginDomain.getUserDensoDetailDomain().setSpsMPlantDensoDomain(sessionUserLoginDomain
            .getUserDensoDetailDomain().getSpsMPlantDensoDomain());
        
        userLoginDomain.getUserSupplierDetailDomain().setSpsMUserSupplierDomain(
            sessionUserLoginDomain.getUserSupplierDetailDomain().getSpsMUserSupplierDomain());
        userLoginDomain.getUserSupplierDetailDomain().setSpsMCompanySupplierDomain(
            sessionUserLoginDomain.getUserSupplierDetailDomain().getSpsMCompanySupplierDomain());
        userLoginDomain.getUserSupplierDetailDomain().setSpsMPlantSupplierDomain(
            sessionUserLoginDomain.getUserSupplierDetailDomain().getSpsMPlantSupplierDomain());
        
        roleScreenList = new ArrayList<RoleScreenDomain>();
        for(RoleScreenDomain roleScreen : sessionUserLoginDomain.getRoleScreenDomainList()){
            if(null != roleScreen.getSpsMScreenDomain() && Constants.ZERO < roleScreen
                .getUserRoleDomainList().size()){
                roleScreenDomain = new RoleScreenDomain();
                roleScreenDomain.setSpsMScreenDomain(roleScreen.getSpsMScreenDomain());
                roleScreenDomain.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
                roleScreenList.add(roleScreenDomain);   
            }
        }
        userLoginDomain.setRoleScreenDomainList(roleScreenList);
        
        userLoginDomain.setSpsMMenuDomainlist(sessionUserLoginDomain.getSpsMMenuDomainlist());
        
        userLoginDomain.setButtonLinkNameList( sessionUserLoginDomain.getButtonLinkNameList() );
        
        return userLoginDomain;
    }
    
    /**
     * setDataOnScreen.
     * <p>Setting data for appearance on screen.</p>
     * 
     * @param form the WINV004 form
     * @return the Invoice Maintenance Domain
     */
    private InvoiceMaintenanceDomain setCriteriaFromForm(Winv004Form form) {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        InvoiceMaintenanceDomain invoiceMaintenanceDomain = new InvoiceMaintenanceDomain();
        
        invoiceMaintenanceDomain.setDensoName(form.getDensoName());
        invoiceMaintenanceDomain.setDCd(form.getDCd());
        invoiceMaintenanceDomain.setDPcd(form.getDPcd());
        invoiceMaintenanceDomain.setDensoTaxId(form.getDensoTaxId());
        invoiceMaintenanceDomain.setDensoAddress(form.getDensoAddress());
        invoiceMaintenanceDomain.setDensoCurrency(form.getDensoCurrency());
        invoiceMaintenanceDomain.setSupplierName(form.getSupplierName());
        invoiceMaintenanceDomain.setSCd(form.getSCd());
        invoiceMaintenanceDomain.setVendorCd(form.getVendorCd());
        invoiceMaintenanceDomain.setSupplierTaxId(form.getSupplierTaxId());
        invoiceMaintenanceDomain.setSupplierAddress(form.getSupplierAddress());
        invoiceMaintenanceDomain.setSupplierAddress1(form.getSupplierAddress1());
        invoiceMaintenanceDomain.setSupplierAddress2(form.getSupplierAddress2());
        invoiceMaintenanceDomain.setSupplierAddress3(form.getSupplierAddress3());
        invoiceMaintenanceDomain.setSupplierCurrency(form.getSupplierCurrency());
        invoiceMaintenanceDomain.setInvoiceNo(form.getInvoiceNo().toUpperCase().trim());
        invoiceMaintenanceDomain.setInvoiceDate(form.getInvoiceDate());
        invoiceMaintenanceDomain.setVatType(form.getVatType());
        invoiceMaintenanceDomain.setVatRate(form.getVatRate());
        invoiceMaintenanceDomain.setDiffDPcdFlag(form.getDiffDPcdFlag());
        if(!StringUtil.checkNullOrEmpty(form.getSupplierBaseAmount())){
            invoiceMaintenanceDomain.setSupplierBaseAmount(form.getSupplierBaseAmount()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        }else{
            invoiceMaintenanceDomain.setSupplierBaseAmount(null);
        }
        if(!StringUtil.checkNullOrEmpty(form.getSupplierVatAmount())){
            invoiceMaintenanceDomain.setSupplierVatAmount(form.getSupplierVatAmount()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        }else{
            invoiceMaintenanceDomain.setSupplierVatAmount(null);
        }
        if(!StringUtil.checkNullOrEmpty(form.getSupplierTotalAmount())){
            invoiceMaintenanceDomain.setSupplierTotalAmount(form.getSupplierTotalAmount()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        }else{
            invoiceMaintenanceDomain.setSupplierTotalAmount(null);
        }
        invoiceMaintenanceDomain.setCnNo(form.getCnNo().toUpperCase().trim());
        invoiceMaintenanceDomain.setCnDate(form.getCnDate());
        if(!StringUtil.checkNullOrEmpty(form.getCnBaseAmount())){
            invoiceMaintenanceDomain.setCnBaseAmount(form.getCnBaseAmount()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        }else{
            invoiceMaintenanceDomain.setCnBaseAmount(null);
        }
        if(!StringUtil.checkNullOrEmpty(form.getCnVatAmount())){
            invoiceMaintenanceDomain.setCnVatAmount(form.getCnVatAmount()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        }else{
            invoiceMaintenanceDomain.setCnVatAmount(null);
        }
        if(!StringUtil.checkNullOrEmpty(form.getCnTotalAmount())){
            invoiceMaintenanceDomain.setCnTotalAmount(form.getCnTotalAmount()
                .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        }else{
            invoiceMaintenanceDomain.setCnTotalAmount(null);
        }
        invoiceMaintenanceDomain.setTotalDiffBaseAmount(form.getTotalDiffBaseAmount()
            .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        invoiceMaintenanceDomain.setTotalDiffVatAmount(form.getTotalDiffVatAmount()
            .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        invoiceMaintenanceDomain.setTotalDiffAmount(form.getTotalDiffAmount()
            .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        invoiceMaintenanceDomain.setTotalBaseAmount(form.getTotalBaseAmount()
            .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        invoiceMaintenanceDomain.setTotalVatAmount(form.getTotalVatAmount()
            .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        invoiceMaintenanceDomain.setTotalAmount(form.getTotalAmount()
            .replace(Constants.SYMBOL_COMMA, Constants.EMPTY_STRING));
        invoiceMaintenanceDomain.setAsnDetailList(form.getAsnDetailList());
        invoiceMaintenanceDomain.setSessionId(form.getSessionId());
        invoiceMaintenanceDomain.setDscId(form.getDscId());
        invoiceMaintenanceDomain.setDecimalDisp(form.getDecimalDisp());
        invoiceMaintenanceDomain.setLocale(locale);
        
        return invoiceMaintenanceDomain;
    }
    
    /**
     * setDataOnScreen.
     * <p>Setting data for appearance on screen.</p>
     * 
     * @param form the WINV004 form
     * @param result the Invoice Maintenance Domain
     */
    private void setDataOnScreen(Winv004Form form, InvoiceMaintenanceDomain result) {
        form.setDensoName(result.getDensoName());
        form.setDCd(result.getDCd());
        form.setDPcd(result.getDPcd());
        form.setDensoTaxId(result.getDensoTaxId());
        form.setDensoAddress(result.getDensoAddress());
        form.setDensoCurrency(result.getDensoCurrency());
        form.setSupplierName(result.getSupplierName());
        form.setSCd(result.getSCd());
        form.setVendorCd(result.getVendorCd());
        form.setSupplierTaxId(result.getSupplierTaxId());
        form.setSupplierAddress(result.getSupplierAddress());
        form.setSupplierAddress1(result.getSupplierAddress1());
        form.setSupplierAddress2(result.getSupplierAddress2());
        form.setSupplierAddress3(result.getSupplierAddress3());
        form.setSupplierCurrency(result.getSupplierCurrency());
        form.setInvoiceNo(result.getInvoiceNo());
        form.setInvoiceDate(result.getInvoiceDate());
        form.setVatType(result.getVatType());
        form.setVatRate(result.getVatRate());
        // [IN059] To keep default VAT Rate
        form.setDefaultVatRate(result.getVatRate());
        
        form.setSupplierBaseAmount(result.getSupplierBaseAmount());
        form.setSupplierVatAmount(result.getSupplierVatAmount());
        form.setSupplierTotalAmount(result.getSupplierTotalAmount());
        form.setCnNo(result.getCnNo());
        form.setCnDate(result.getCnDate());
        form.setCnBaseAmount(result.getCnBaseAmount());
        form.setCnVatAmount(result.getCnVatAmount());
        form.setCnTotalAmount(result.getCnTotalAmount());
        form.setTotalDiffBaseAmount(result.getTotalDiffBaseAmount());
        form.setTotalDiffVatAmount(result.getTotalDiffVatAmount());
        form.setTotalDiffAmount(result.getTotalDiffAmount());
        form.setTotalBaseAmount(result.getTotalBaseAmount());
        form.setTotalVatAmount(result.getTotalVatAmount());
        form.setTotalAmount(result.getTotalAmount());
        form.setDecimalDisp(result.getDecimalDisp());
        form.setCoverPageFileId(result.getFileId());
        form.setAsnDetailList(result.getAsnDetailList());
    }
    
    /**
     * setMessageForException.
     * <p>Setting message for exception on screen.</p>
     * 
     * @param result the Invoice Maintenance Return Domain
     * @param e the Exception
     */
    private void setMessageForException(InvoiceMaintenanceDomain result, Exception e){
        try {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0002, 
                    new String[]{e.getClass().toString(), e.getMessage()})));
        } catch (Exception exception) {
            result.getErrorMessageList().add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(result.getLocale(), 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[]{e.getClass().toString(), e.getMessage()})));
        }
    }
    
    /**
     * setMessageOnScreen.
     * <p>Setting message for appearance on screen.</p>
     * 
     * @param form the WINV004 form
     * @param result the Invoice Maintenance Domain
     */
    private void setMessageOnScreen(Winv004Form form, InvoiceMaintenanceDomain result){
        
        if(null != result){
            List<ApplicationMessageDomain> applicationMessageList;
            if(null != result.getErrorMessageList() 
                && Constants.ZERO < result.getErrorMessageList().size()){
                applicationMessageList = new ArrayList<ApplicationMessageDomain>();
                applicationMessageList.addAll(result.getErrorMessageList());
                if(null != result.getWarningMessageList() 
                    && Constants.ZERO < result.getWarningMessageList().size()){
                    applicationMessageList.addAll(result.getWarningMessageList());
                }
                form.setApplicationMessageList(applicationMessageList);
            }else if(null != result.getWarningMessageList() 
                && Constants.ZERO < result.getWarningMessageList().size()){
                form.setApplicationMessageList(result.getWarningMessageList());
            }else if(null != result.getSuccessMessageList() 
                && Constants.ZERO < result.getSuccessMessageList().size()){
                
                form.setApplicationMessageList(result.getSuccessMessageList());
            }
        }
        
    }
}
