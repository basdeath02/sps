/*
 * ModifyDate Development company     Describe 
 * 2014/04/23 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.presentation.form.Wadm001Form;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.service.SupplierUserInformationFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;

/**
 * The Class SupplierUserInformationAction.
 * <p>
 * Receive value from WADM001_SupplierUserInformation.jsp for initial value and send to 
 * supplierUserFacadeServiceImpl.
 * for search, update and create CSV string for download file.
 * </p>
 * <ul>
 * <li>Method initial    : doInitial</li>
 * <li>Method initial    : doInitialByReturn</li>
 * <li>Method search     : doSearch</li>
 * <li>Method delete     : doDeleteSupplierUser</li>
 * <li>Method download   : doDownload</li>
 * <li>Method hyper link : doDscIdLink</li>
 * <li>Method hyper link : doRoleLink</li>
 * </ul>
 * 
 * @author CSI
 */
public class SupplierUserInformationAction extends CoreAction {

    /** The supplier User facade service. */
    private SupplierUserInformationFacadeService supplierUserInformationFacadeService;

    /**
     * Instantiates a new SupplierUserInformationAction.
     */
    public SupplierUserInformationAction() {
        super();
    }
    
    /**
     * Sets the supplier User Facade Service.
     * 
     * @param supplierUserInformationFacadeService the supplier User Facade Service.
     */
    public void setSupplierUserInformationFacadeService(
        SupplierUserInformationFacadeService supplierUserInformationFacadeService) {
        this.supplierUserInformationFacadeService = supplierUserInformationFacadeService;
    }

    /**
     * Do initial.
     * <p>
     * Initial data when load page.
     * </p>
     * <ul>
     * <li>In case click menu Supplier User Information, Initial criteria data for search
     * when load page.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping,
            ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Wadm001Form form = (Wadm001Form) actionForm;
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        SupplierUserInformationDomain result = new SupplierUserInformationDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        try{
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM001_BRESET))
                {
                    this.supplierUserInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM001_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM001_BDOWNLOAD))
                {
                    this.supplierUserInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM001_BDOWNLOAD, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotDownloadCsvMessage(applicationException.getMessage());
            }
            
            /*Call Facade Service for search DensoSupplierRelation*/
            dataScopeControlDomain = supplierUserInformationFacadeService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            
            /*Call Facade Service for initial*/
            result = supplierUserInformationFacadeService.searchInitial(dataScopeControlDomain);
            
            /*Set value from domain to ActionForm*/
            form.resetForm();
            form.setCompanySupplierList(result.getCompanySupplierList());
            form.setPlantSupplierList(result.getPlantSupplierList());
            
            /*Set Authentication list*/
            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                result.getDataScopeControlDomain().getDensoSupplierRelationDomainList(),
                SupplierPortalConstant.SCREEN_ID_WADM001));
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do initial by Return.
     * <p>
     * Initial data when Return from "WADM002 - Supplier User Registration" 
     * or "WADM004 - Supplier User Role Assignment".
     * </p>
     * <ul>
     * <li>In case click return button from "WADM002 - Supplier User Registration" 
     * or "WADM004 - Supplier User Role Assignment".then load page by previous criteria.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialByReturn(ActionMapping actionMapping,
            ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Wadm001Form form = (Wadm001Form) actionForm;
        Wadm001Form sessionForm = (Wadm001Form)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_WADM001_FORM);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        String firstname = null;
        String middleName = null;
        String lastName = null;
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        SupplierUserInformationDomain supplierUserInfoDomain = new SupplierUserInformationDomain();
        supplierUserInfoDomain.setDscId(sessionForm.getDscId().toUpperCase());
        if(Constants.MISC_CODE_ALL.equals(sessionForm.getSCd())){
            supplierUserInfoDomain.setSCd(null);
        }else{
            supplierUserInfoDomain.setSCd(sessionForm.getSCd());
        }
        if(Constants.MISC_CODE_ALL.equals(sessionForm.getSPcd())){
            supplierUserInfoDomain.setSPcd(null);
        }else{
            supplierUserInfoDomain.setSPcd(sessionForm.getSPcd());
        }
        if(!StringUtil.checkNullOrEmpty(sessionForm.getFirstName())){
            firstname = sessionForm.getFirstName().toUpperCase();
        }
        if(!StringUtil.checkNullOrEmpty(sessionForm.getMiddleName())){
            middleName = sessionForm.getMiddleName().toUpperCase();
        }
        if(!StringUtil.checkNullOrEmpty(sessionForm.getLastName())){
            lastName = sessionForm.getLastName().toUpperCase();
        }
        
        supplierUserInfoDomain.setFirstName(firstname);
        supplierUserInfoDomain.setMiddleName(middleName);
        supplierUserInfoDomain.setLastName(lastName);
        supplierUserInfoDomain.setRegisterDateFrom(sessionForm.getRegisterDateFrom());
        supplierUserInfoDomain.setRegisterDateTo(sessionForm.getRegisterDateTo());
        supplierUserInfoDomain.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
        supplierUserInfoDomain.setIsActive(Constants.IS_ACTIVE);
        
        /*Set value from domain to criteria Form*/
        form.setDscId(sessionForm.getDscId());
        form.setSCd(sessionForm.getSCd());
        form.setSPcd(sessionForm.getSPcd());
        form.setFirstName(sessionForm.getFirstName());
        form.setMiddleName(sessionForm.getMiddleName());
        form.setLastName(sessionForm.getLastName());
        form.setRegisterDateFrom(sessionForm.getRegisterDateFrom());
        form.setRegisterDateTo(sessionForm.getRegisterDateTo());
        form.setUserLogin(sessionForm.getUserLogin());
        form.setMenuCodelist(sessionForm.getMenuCodelist());
        form.setCountDelete(sessionForm.getCountDelete());
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        supplierUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        //Clear session
        DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WADM001_FORM);
        form.setCountDelete(Constants.STR_ZERO);
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                try {
                    if (!userLoginDomain.getButtonLinkNameList().contains(
                        RoleTypeConstants.WADM001_BRESET)) 
                    {
                        this.supplierUserInformationFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WADM001_BRESET, locale);
                    }
                } catch (ApplicationException applicationException) {
                    form.setCannotResetMessage(applicationException.getMessage());
                }
                
                try {
                    if (!userLoginDomain.getButtonLinkNameList().contains(
                        RoleTypeConstants.WADM001_BDOWNLOAD)) 
                    {
                        this.supplierUserInformationFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WADM001_BDOWNLOAD, locale);
                    }
                } catch (ApplicationException applicationException) {
                    form.setCannotDownloadCsvMessage(applicationException.getMessage());
                }
                
                /*Call Facade Service for search DensoSupplierRelation*/
                dataScopeControlDomain = supplierUserInformationFacadeService
                    .searchDensoSupplierRelation(dataScopeControlDomain);
                supplierUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
                
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    supplierUserInfoDomain.setPageNumber(Constants.ONE);
                }else{
                    supplierUserInfoDomain.setPageNumber(form.getPageNo());
                }
                /*Call Facade Service for  initial screen after return from WADM002 and WADM004*/
                supplierUserInfoDomain = supplierUserInformationFacadeService
                    .searchInitialWithCriteria(supplierUserInfoDomain);
                
                /*Set value of domain to ActionForm*/
                form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                    dataScopeControlDomain.getDensoSupplierRelationDomainList(),
                    SupplierPortalConstant.SCREEN_ID_WADM001));
                form.setCompanySupplierList(supplierUserInfoDomain.getCompanySupplierList());
                form.setPlantSupplierList(supplierUserInfoDomain.getPlantSupplierList());
                SpsPagingUtil.setFormForPaging(supplierUserInfoDomain, form);
                form.setUserSupplierInfoList(supplierUserInfoDomain.getUserSupplierInfoList());
            }
            catch(ApplicationException applicationException){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                this.setPlantCombobox(form);
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Supplier Company Changed.
     * <p>
     * set data from the supplier company code combo box change supplier company code value.
     * </p>
     * <ul>
     * <li>In case change supplier company code value.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Wadm001Form form = (Wadm001Form) actionForm;
        List<PlantSupplierDomain> plantSupplierResultList = new ArrayList<PlantSupplierDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = 
            plantSupplierWithScopeDomain.getPlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = 
            plantSupplierWithScopeDomain.getDataScopeControlDomain();
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);
        try{
            /*Set value to null when select all.*/
            if (!Constants.MISC_CODE_ALL.equals(form.getSCd())) {
                plantSupplierWithScopeDomain.getPlantSupplierDomain().setSCd(form.getSCd());
            }
            /*Call Facade Service for search DensoSupplierRelation*/
            dataScopeControlDomain = supplierUserInformationFacadeService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            
            /*Call Facade Service to get the list of supplier plant code by supplier company code*/
            plantSupplierResultList = supplierUserInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            
            /*Set value of domain to ActionForm*/
            form.setPlantSupplierList(plantSupplierResultList);
            printJson(response,
                new JsonResult((List<Object>)(Object)plantSupplierResultList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm001Form form = (Wadm001Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        
        try{
            UserLoginDomain userLogin = this.setUserLoginInformation();
            RoleScreenDomain roleScreenDomain = setRoleScreen(userLogin);
            
            CompanySupplierWithScopeDomain companySupplierWithScope
                = new CompanySupplierWithScopeDomain();
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLogin.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            companySupplierWithScope.setLocale(locale);
            companySupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())
                && !Constants.UNDEFINED_FOR_SUPPLIER.equals(form.getSPcd())) {
                companySupplierWithScope.getCompanySupplierDomain().setSPcd(form.getSPcd());
            }
            
            companyList = this.supplierUserInformationFacadeService
                .searchSelectedPlantSupplier(companySupplierWithScope);
            
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, search the supplier user information 
     * that meet the criteria values.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * 
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSearch(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm001Form form = (Wadm001Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain = 
            new SupplierUserInformationReturnDomain();
        SupplierUserInformationReturnDomain criteria = new SupplierUserInformationReturnDomain();
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        SupplierUserInformationDomain supplierUserInfoDomain = new SupplierUserInformationDomain();
        this.setParameterCriteria(form, supplierUserInfoDomain);
        supplierUserInfoDomain.setLocale(locale);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        supplierUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        
        criteria.setSupplierUserInfomationDomain(supplierUserInfoDomain);
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM001_BSEARCH)) 
                {
                    this.supplierUserInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM001_BSEARCH, locale);
                }
                
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    supplierUserInfoDomain.setPageNumber(Constants.ONE);
                }else{
                    supplierUserInfoDomain.setPageNumber(form.getPageNo());
                }
                /*Call Facade Service search the supplier user information by criteria values.*/
                supplierUserInformationReturnDomain = supplierUserInformationFacadeService
                    .searchUserSupplier(criteria);
                List<SupplierUserInformationDomain> supplierUserList = 
                    supplierUserInformationReturnDomain.getSupplierUserInfoDomainList();
                SupplierUserInformationDomain result = supplierUserInformationReturnDomain
                    .getSupplierUserInfomationDomain();
                if(null != supplierUserList){
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_DSC_ID,
                            new HashMap<String, SupplierUserInformationDomain>());
                        form.setCountDelete(Constants.STR_ZERO);
                    }else{
                        this.doSetSelectedDscId(form);
                        Map<String, SupplierUserInformationDomain> selectedDoMap = 
                            (HashMap<String, SupplierUserInformationDomain>)DensoContext.get()
                                .getGeneralArea(Constants.MAP_KEY_SELECTED_DSC_ID);
                        for(String itemKey : selectedDoMap.keySet()){
                            for(SupplierUserInformationDomain item : supplierUserList){
                                if(itemKey.equals(item.getDscId())){
                                    item.setDscIdSelected(itemKey);
                                }
                            }
                        }
                    }
                    SpsPagingUtil.setFormForPaging(result, form);
                    /*Set value of domain to ActionForm*/
                    form.setUserSupplierInfoList(supplierUserList); 
                }else{
                    errorMessageList = supplierUserInformationReturnDomain.getErrorMessageList();
                }
            } catch (ApplicationException applicationException) {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        applicationException.getMessage()));
            } catch (Exception e) {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally {
                this.setPlantCombobox(form);
                form.setApplicationMessageList(errorMessageList);
            }
            /*try{
                initial screen
                Call Facade Service for search DensoSupplierRelation
                dataScopeControlDomain = supplierUserInformationFacadeService
                    .searchDensoSupplierRelation(dataScopeControlDomain);
                
                supplierUserInfoDomain = supplierUserInformationFacadeService
                    .searchInitial(dataScopeControlDomain);
                form.setCompanySupplierList(supplierUserInfoDomain.getCompanySupplierList());
            } catch (ApplicationException applicationException) {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
                form.setApplicationMessageList(errorMessageList);
            } catch (Exception e) {
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }*/
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do delete supplier user.
     * 
     * <p>
     * set supplier user(s) to inactive user.
     * </p>
     * 
     * <li>In case click delete button to set the target user(s)
     * that selected in the search result to inactive.</li>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doDelete(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wadm001Form form = (Wadm001Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        String firstPages = form.getFirstPages();
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain = 
            new SupplierUserInformationReturnDomain();
        List<SupplierUserInformationDomain> supplierUserSelectedList = new 
            ArrayList<SupplierUserInformationDomain>();
        SupplierUserInformationDomain result = new SupplierUserInformationDomain();
        List<SupplierUserInformationDomain> supplierUserList = 
            new ArrayList<SupplierUserInformationDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        SupplierUserInformationDomain supplierUserInfoDomain = new SupplierUserInformationDomain();
        this.setParameterCriteria(form, supplierUserInfoDomain);
        supplierUserInfoDomain.setLocale(locale);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        supplierUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        supplierUserInfoDomain.setUpdateUser(userLoginDomain.getDscId());
        
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM001_BDELETE)) 
            {
                supplierUserList = form.getUserSupplierInfoList();
                result.setTotalCount(form.getMax());
                this.supplierUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM001_BDELETE, locale);
            }
            
            this.doSetSelectedDscId(form);
            Map<String, SupplierUserInformationDomain> selectedDscIdMap = 
                (HashMap<String, SupplierUserInformationDomain>)DensoContext.get()
                    .getGeneralArea(Constants.MAP_KEY_SELECTED_DSC_ID);
            
            if(null != selectedDscIdMap && !selectedDscIdMap.isEmpty()){
                Iterator<String> iterator = selectedDscIdMap.keySet().iterator();
                while(iterator.hasNext()){
                    String keyStr = (String)iterator.next();
                    supplierUserSelectedList.add(selectedDscIdMap.get(keyStr));
                }
                if(Constants.ZERO < supplierUserSelectedList.size()){
                    if(Constants.PAGE_SEARCH.equals(firstPages)){
                        supplierUserInfoDomain.setPageNumber(Constants.ONE);
                    }else{
                        form.setPages(Constants.PAGE_CLICK);
                        supplierUserInfoDomain.setPageNumber(form.getPageNo());
                    }
                    supplierUserInfoDomain.setUserSupplierInfoList(supplierUserSelectedList);
                    
                    /*Call Facade Service to set the list of supplier user to inactive.*/
                    supplierUserInformationReturnDomain = supplierUserInformationFacadeService
                        .deleteUserSupplier(supplierUserInfoDomain);
                    supplierUserList = 
                        supplierUserInformationReturnDomain.getSupplierUserInfoDomainList();
                    result = supplierUserInformationReturnDomain.getSupplierUserInfomationDomain();
                    
                    SpsPagingUtil.setFormForPaging(result, form);
                    form.setCountDelete(Constants.IS_NOT_ACTIVE);
                    
                    message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0003);
                    errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                        message));
                    form.setMethod(SupplierPortalConstant.METHOD_DO_SEARCH);
                    DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_DSC_ID,
                        new HashMap<String, SupplierUserInformationDomain>());
                }
            }
        } catch (ApplicationException applicationException) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        } catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally {
            this.setPlantCombobox(form);
            if(Constants.ZERO == result.getTotalCount()){
                form.setMax(Constants.ZERO);
                form.setUserSupplierInfoList(null);
            }else{
                form.setUserSupplierInfoList(supplierUserList);
            }
            form.setApplicationMessageList(errorMessageList);
        }
        /*try{
            initial screen
            dataScopeControlDomain = supplierUserInformationFacadeService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            
            supplierUserInfoDomain = supplierUserInformationFacadeService
                .searchInitial(dataScopeControlDomain);
            form.setCompanySupplierList(supplierUserInfoDomain.getCompanySupplierList());
        } catch (ApplicationException applicationException) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            form.setApplicationMessageList(errorMessageList);
        } catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }*/
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download.
     * 
     * <p>
     * download data that meet criteria values.
     * </p>
     * 
     * <li>In case click download button, system will load data that meet to criteria 
     * and export all data to CSV file.</li>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception{
        
        Wadm001Form form = (Wadm001Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        SupplierUserInformationReturnDomain supplierUserInformationReturnDomain = 
            new SupplierUserInformationReturnDomain();
        Calendar cal = Calendar.getInstance();
        String fileName = StringUtil.appendsString(
            Constants.SUPPLIER_USER_INFO_LIST_FILE_NAME,
            Constants.SYMBOL_UNDER_SCORE,
            DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
        ServletOutputStream outputStream = null;
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        SupplierUserInformationDomain supplierUserInfoDomain = new SupplierUserInformationDomain();
        this.setParameterCriteria(form, supplierUserInfoDomain);
        supplierUserInfoDomain.setLocale(locale);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        supplierUserInfoDomain.setDataScopeControlDomain(dataScopeControlDomain);
        try{
            /*Call Facade Service to  export the supplier user information to the CSV file.*/
            supplierUserInformationReturnDomain = supplierUserInformationFacadeService
                .searchUserSupplierCsv(supplierUserInfoDomain);
            StringBuffer resultStr = supplierUserInformationReturnDomain.getResultString();
            if(null != resultStr ){
                byte[] output = resultStr.toString().getBytes();
                setHttpHeaderForCsv(response, fileName);
                outputStream = response.getOutputStream();
                /*Get created CSV file.*/
                outputStream.write(output, Constants.ZERO, output.length);
                outputStream.flush();
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_I6_0005);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                    message));
                return null;
            }else{
                errorMessageList = supplierUserInformationReturnDomain.getErrorMessageList();
            }
            
        }catch (ApplicationException applicationException) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch (Exception e) {
            message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0012);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                message));
        }finally{
            this.setPlantCombobox(form);
            if(null != outputStream){
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);
        }
        /*try{
            initial screen
            dataScopeControlDomain = supplierUserInformationFacadeService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            
            supplierUserInfoDomain = supplierUserInformationFacadeService
                .searchInitial(dataScopeControlDomain);
            form.setCompanySupplierList(supplierUserInfoDomain.getCompanySupplierList());
        } catch (ApplicationException applicationException) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            form.setApplicationMessageList(errorMessageList);
        }catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }*/
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }


    /**
     * Do DSC ID hyper link.
     * <p>DSC Id hyper link to open WADM002 screen for edit supplier user detail.</p>
     * 
     *  <li>In case click DSC ID hyper link, system will load data that meet to DSC ID 
     *  for initial data to open WADM002: Supplier User Registration.</li>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doLinkDscId(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm001Form form = (Wadm001Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        boolean isSuccess = false;
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM001_LDSCID)) 
            {
                this.supplierUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM001_LDSCID, locale);
            }
            
            /* Parameter to WADM002
             * - dscId
             * - supplierCompanyCode
             * - firstName
             * - middleName
             * - lastName
             * - registerDateFrom
             * - registerDateTo
             * - dscIdSelected
             * */
            forward.append(SupplierPortalConstant.URL_WADM002_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA)
                .append(SupplierPortalConstant.URL_PARAM_MODE_EDIT);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
            forward.append(form.getDscIdSelected());
//            forward.append(SupplierPortalConstant.URL_PARAM_SUPPLIER_COMPANY_CODE);
//            forward.append(form.getSCd());
//            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
//            forward.append(form.getFirstName());
//            forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
//            forward.append(form.getMiddleName());
//            forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
//            forward.append(form.getLastName());
//            forward.append(SupplierPortalConstant.URL_PARAM_REGISTER_DATE_FROM);
//            forward.append(form.getRegisterDateFrom());
//            forward.append(SupplierPortalConstant.URL_PARAM_REGISTER_DATE_TO);
//            forward.append(form.getRegisterDateTo());
            
            isSuccess = true;
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WADM001_FORM, form);
            return new ActionForward(forward.toString(), true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isSuccess){
                this.setPlantCombobox(form);
            }
            form.setApplicationMessageList(applicationMessageList);
        }
        /*try{
            RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            SupplierUserInformationDomain result = new SupplierUserInformationDomain();
            
            initial screen
            Call Facade Service for search DensoSupplierRelation
            dataScopeControlDomain = supplierUserInformationFacadeService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            
            result = supplierUserInformationFacadeService.searchInitial(dataScopeControlDomain);
            form.setCompanySupplierList(result.getCompanySupplierList());
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            form.setApplicationMessageList(applicationMessageList);
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }*/
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Role hyper link.
     * 
     * <p>Role hyper link to open WADM004 screen for edit supplier user detail.</p>
     * 
     *  <li>In case click Role hyper link, system will load data that meet to DSC ID 
     *  for initial data to open WADM004: Supplier User Role Assignment.</li>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doLinkRole(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm001Form form = (Wadm001Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        boolean isSuccess = false;
        
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM001_LASSIGNROLE)) 
            {
                this.supplierUserInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM001_LASSIGNROLE, locale);
            }
            
            /* Parameter to WADM004
             * - dscId
             * - supplierCompanyCode
             * - plantSupplier
             * - firstName
             * - middleName
             * - lastName
             * - CompanyName
             * */
            forward.append(SupplierPortalConstant.URL_WADM004_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
            forward.append(form.getDscIdSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_SCD);
            forward.append(form.getSCdSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_SPCD);
            forward.append(form.getSPcdSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
            forward.append(form.getFirstNameSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
            forward.append(form.getMiddleNameSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
            forward.append(form.getLastNameSelected());
            forward.append(SupplierPortalConstant.URL_PARAM_COMPANY_NAME);
            forward.append(form.getCompanyName());
            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
            forward.append(Constants.PAGE_SEARCH);
            
            isSuccess = true;
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WADM001_FORM, form);
            return new ActionForward(forward.toString(), true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isSuccess){
                this.setPlantCombobox(form);
            }
            form.setApplicationMessageList(applicationMessageList);
        }
        /*try{
            RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            SupplierUserInformationDomain result = new SupplierUserInformationDomain();
            
            initial screen
            Call Facade Service for search DensoSupplierRelation
            dataScopeControlDomain = supplierUserInformationFacadeService
                .searchDensoSupplierRelation(dataScopeControlDomain);
            
            result = supplierUserInformationFacadeService
                .searchInitial(dataScopeControlDomain);
            form.setCompanySupplierList(result.getCompanySupplierList());
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            form.setApplicationMessageList(applicationMessageList);
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }*/
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

     /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(SupplierPortalConstant.ACTION_RESET, SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_SEARCH, SupplierPortalConstant.METHOD_DO_SEARCH);
        map.put(SupplierPortalConstant.ACTION_DELETE, SupplierPortalConstant.METHOD_DO_DELETE);
        map.put(SupplierPortalConstant.ACTION_DOWNLOAD, SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        map.put(SupplierPortalConstant.ACTION_LINK_ROLE, 
            SupplierPortalConstant.METHOD_DO_LINK_ROLE);
        map.put(SupplierPortalConstant.ACTION_LINK_DSC_ID, 
            SupplierPortalConstant.METHOD_DO_LINK_DSC_ID);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN,
            SupplierPortalConstant.METHOD_DO_INITIAL_BY_RETURN);
        return map;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WADM001.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * Do set selected DSC ID
     * @param form the form
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    private void doSetSelectedDscId(Wadm001Form form) throws Exception{
        List<SupplierUserInformationDomain> itemList = form.getUserSupplierInfoList();
        Map<String, SupplierUserInformationDomain> selectedDscIdMap 
            = (HashMap<String, SupplierUserInformationDomain>)DensoContext.get()
                .getGeneralArea(Constants.MAP_KEY_SELECTED_DSC_ID);
        for(SupplierUserInformationDomain item : itemList){
            if(!StringUtil.checkNullOrEmpty(item.getDscIdSelected())){
                if(!selectedDscIdMap.containsKey(item.getDscId())){
                    selectedDscIdMap.put(item.getDscId(), item);
                }
            }else{
                if(selectedDscIdMap.containsKey(item.getDscId())){
                    selectedDscIdMap.remove(item.getDscId());
                }
            }
        }
        
        if(null != selectedDscIdMap && !selectedDscIdMap.isEmpty()){
            int countDel = Constants.ZERO;
            Iterator<String> itr = selectedDscIdMap.keySet().iterator();
            while(itr.hasNext()){
                itr.next();
                countDel += Constants.ONE;
            }
            form.setCountDelete(String.valueOf(countDel));
        }else{
            form.setCountDelete(String.valueOf(Constants.ZERO));
        }
    }
    
    /**
     * Set value inside Supplier Plant combo box.
     * @param form the Wadm001Form
     * */
    private void setPlantCombobox(Wadm001Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.setUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = 
            plantSupplierWithScopeDomain.getPlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = 
            plantSupplierWithScopeDomain.getDataScopeControlDomain();
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);
        
        if (!Strings.judgeBlank(form.getSCd())){
            List<PlantSupplierDomain> plantSupplierDomainList = null;
            try{
                PlantSupplierWithScopeDomain plantSupplierWithScope
                    = new PlantSupplierWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getSCd())) {
                    plantSupplierWithScope.getPlantSupplierDomain().setSCd(form.getSCd());
                }
                
                /*Call Facade Service for search DensoSupplierRelation*/
                dataScopeControlDomain = supplierUserInformationFacadeService
                    .searchDensoSupplierRelation(dataScopeControlDomain);
                plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
                
                plantSupplierWithScope.setLocale(locale);
                plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
                plantSupplierDomainList = supplierUserInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplierWithScope);
                form.setPlantSupplierList(plantSupplierDomainList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.setApplicationMessageList(applicationMessageList);
            }
        }
    }
    
    /**
     * Set value from Wadm001Form to parameter criteria.
     * @param form the Wadm001Form
     * @param criteria the supplier user information domain
     * */
    private void setParameterCriteria(Wadm001Form form, SupplierUserInformationDomain criteria){
        if(!StringUtil.checkNullOrEmpty(form.getDscId().trim())){
            criteria.setDscId(form.getDscId().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getFirstName().trim())){
            criteria.setFirstName(form.getFirstName().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getMiddleName().trim())){
            criteria.setMiddleName(form.getMiddleName().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getLastName().trim())){
            criteria.setLastName(form.getLastName().toUpperCase().trim());
        }
        
        criteria.setSCd(form.getSCd());
        criteria.setSPcd(form.getSPcd());
        criteria.setRegisterDateFrom(form.getRegisterDateFrom());
        criteria.setRegisterDateTo(form.getRegisterDateTo());
        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
    }
}