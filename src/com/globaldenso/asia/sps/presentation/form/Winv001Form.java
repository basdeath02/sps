/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/24 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WINV001Form.
 * @author CSI
 */
public class Winv001Form extends CoreActionForm {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 8720900529081611615L;
    
    /** The mode. */
    private String mode;
    
    /** The invoice date from. */
    private String invoiceDateFrom;
    
    /** The invoice date to. */
    private String invoiceDateTo;
    
    /** The cn date from. */
    private String cnDateFrom;
    
    /** The cn date to. */
    private String cnDateTo;
    
    /** The invoice rcv Status. */
    private String invoiceRcvStatus;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The denso code. */
    private String dCd;
    
    /** The supplier plant code. */
    private String dPcd;
    
    /** The invoice status.*/
    private String invoiceStatus;
    
    /** The invoice no.*/
    private String invoiceNo;
    
    /** The cn no.*/
    private String cnNo;
    
    /** The session Id. */
    private String sessionId;
    
    /** The list of invoice information domain.*/
    private List<InvoiceInformationDomain> invoiceInformationForDisplayList;
    
    /** The cover page file ID. */
    private String coverPageFileId;
    
    /** The status index.*/
    private String statusIndex;
    
    /** The string confirm*/
    private String strConfirm;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The supplier code list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The denso code list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The company denso plant list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The invoice status list. */
    private List<MiscellaneousDomain> invoiceStatusList;
    
    /** The invoice rcv status list. */
    private List<MiscellaneousDomain> invoiceRcvStatusList;
    
    /** The Message when cannot click reset. */
    private String cannotResetMessage;
    
    /** The Message when cannot click download. */
    private String cannotDownloadMessage;
    
    /** The count selected cancel invoice. */
    private String countCancelInvoice;
    
    /** The default constructor. */
    public Winv001Form() {
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        cnDateFrom = Constants.EMPTY_STRING;
        cnDateTo = Constants.EMPTY_STRING;
        invoiceRcvStatus = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        invoiceStatus = Constants.EMPTY_STRING;
        invoiceNo = Constants.EMPTY_STRING;
        cnNo = Constants.EMPTY_STRING;
        statusIndex = Constants.EMPTY_STRING;
        strConfirm = Constants.EMPTY_STRING;
        invoiceInformationForDisplayList = new ArrayList<InvoiceInformationDomain>();
        companySupplierList = new ArrayList<CompanySupplierDomain>();
        companyDensoList = new ArrayList<CompanyDensoDomain>();
        invoiceStatusList = new ArrayList<MiscellaneousDomain>();
        invoiceRcvStatusList = new ArrayList<MiscellaneousDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * <p>Setter method for invoice information.</p>
     * 
     * @param items the new invoice information
     */
    public void setInvoiceInformation(InvoiceInformationDomain items) {
        this.invoiceInformationForDisplayList.add(items);
    }
    
    /**
     * Gets the invoice information item.
     * 
     * @param index the index
     * @return the invoice information item.
     */
    public InvoiceInformationDomain getInvoiceInformation(int index){
        if (invoiceInformationForDisplayList == null) {
            invoiceInformationForDisplayList = new ArrayList<InvoiceInformationDomain>();
        }
        while (invoiceInformationForDisplayList.size() <= index) {
            invoiceInformationForDisplayList.add(new InvoiceInformationDomain());
        }
        return invoiceInformationForDisplayList.get(index);
    }
    
    /**
     * <p>Setter method for supplier code.</p>
     * 
     * @param items the new company supplier domain
     */
    public void setCompanySupplierCode(CompanySupplierDomain items) {
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the supplier code.
     * 
     * @param index the index
     * @return the company supplier domain.
     */
    public CompanySupplierDomain getCompanySupplierCode(int index){
        if(companySupplierList == null){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * <p>Setter method for denso code.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoCode(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the denso code.
     * 
     * @param index the index
     * @return the company denso domain.
     */
    public CompanyDensoDomain getCompanyDensoCode(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * <p>Setter method for invoice status.</p>
     * 
     * @param items the new misc domain
     */
    public void setInvoiceStatus(MiscellaneousDomain items) {
        this.invoiceStatusList.add(items);
    }
    
    /**
     * Gets the invoice status.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public MiscellaneousDomain getInvoiceStatus(int index){
        if(invoiceStatusList == null){
            invoiceStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(invoiceStatusList.size() <= index){
            invoiceStatusList.add(new MiscellaneousDomain());
        }
        return invoiceStatusList.get(index);
    }
    
    /**
     * <p>Setter method for invoice rcv status.</p>
     * 
     * @param items the new misc domain
     */
    public void setInvoiceRcvStatus(MiscellaneousDomain items) {
        this.invoiceRcvStatusList.add(items);
    }
    
    /**
     * Gets the invoice rcv status.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public MiscellaneousDomain getInvoiceRcvStatus(int index){
        if(invoiceRcvStatusList == null){
            invoiceRcvStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(invoiceRcvStatusList.size() <= index){
            invoiceRcvStatusList.add(new MiscellaneousDomain());
        }
        return invoiceRcvStatusList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    
    /**
     * <p>Getter method for invoice date from.</p>
     *
     * @return the invoice date from
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }
    
    /**
     * <p>Setter method for invoice date from.</p>
     *
     * @param invoiceDateFrom Set for invoice date to
     */
    public void setInvoiceDateFrom(String invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }
    
    /**
     * <p>Getter method for invoice date to.</p>
     *
     * @return the invoice date to
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }

    /**
     * <p>Setter method for invoice date to.</p>
     *
     * @param invoiceDateTo Set for invoice date to
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }
    
    /**
     * <p>Getter method for cn date from.</p>
     *
     * @return the cn date from
     */
    public String getCnDateFrom() {
        return cnDateFrom;
    }
    
    /**
     * <p>Setter method for cn date from.</p>
     *
     * @param cnDateFrom Set for cn date to
     */
    public void setCnDateFrom(String cnDateFrom) {
        this.cnDateFrom = cnDateFrom;
    }
    
    /**
     * <p>Getter method for cn date to.</p>
     *
     * @return the cn date to
     */
    public String getCnDateTo() {
        return cnDateTo;
    }

    /**
     * <p>Setter method for cn date to.</p>
     *
     * @param cnDateTo Set for cn date to
     */
    public void setCnDateTo(String cnDateTo) {
        this.cnDateTo = cnDateTo;
    }
    
    /**
     * <p>Getter method for invoice rcv status.</p>
     *
     * @return the invoice rcv status
     */
    public String getInvoiceRcvStatus() {
        return invoiceRcvStatus;
    }

    /**
     * <p>Setter method for invoice rcv status.</p>
     *
     * @param invoiceRcvStatus Set for invoice rcv status
     */
    public void setInvoiceRcvStatus(String invoiceRcvStatus) {
        this.invoiceRcvStatus = invoiceRcvStatus;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * <p>Setter method for denso code.</p>
     *
     * @param dCd Set for denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * <p>Getter method for invoice status.</p>
     *
     * @return the invoice status
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }
    /**
     * <p>Setter method for invoice status.</p>
     *
     * @param invoiceStatus Set for invoice status
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }
    
    /**
     * <p>Getter method for invoice no.</p>
     *
     * @return the invoice no
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }
    /**
     * <p>Setter method for invoice no.</p>
     *
     * @param invoiceNo Set for invoice no
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
    
    /**
     * <p>Getter method for cn no.</p>
     *
     * @return the cn no
     */
    public String getCnNo() {
        return cnNo;
    }
    
    /**
     * <p>Setter method for cn no.</p>
     *
     * @param cnNo Set for cn noe
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }
    
    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for denso plan code.</p>
     *
     * @param dPcd Set for denso plan code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for invoice information list.</p>
     *
     * @return the invoice information list
     */
    public List<InvoiceInformationDomain> getInvoiceInformationForDisplayList() {
        return invoiceInformationForDisplayList;
    }
    
    /**
     * <p>Setter method for invoice information for display list.</p>
     *
     * @param invoiceInformationForDisplayList Set for invoice information for display list
     */
    public void setInvoiceInformationForDisplayList(
        List<InvoiceInformationDomain> invoiceInformationForDisplayList) {
        this.invoiceInformationForDisplayList = invoiceInformationForDisplayList;
    }
    
    /**
     * Gets the status index.
     * 
     * @return the status index.
     */
    public String getStatusIndex() {
        return statusIndex;
    }
    
    /**
     * Sets the status index.
     * 
     * @param statusIndex the status index.
     */  
    public void setStatusIndex(String statusIndex) {
        this.statusIndex = statusIndex;
    }
    
    /**
     * Gets the string confirm.
     * 
     * @return the string confirm.
     */
    public String getStrConfirm() {
        return strConfirm;
    }
    
    /**
     * Sets the string confirm.
     * 
     * @param strConfirm the string confirm.
     */  
    public void setStrConfirm(String strConfirm) {
        this.strConfirm = strConfirm;
    }
    
    /**
     * Gets the supplier code list.
     * 
     * @return the supplier code list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the supplier code list.
     * 
     * @param companySupplierList the supplier code list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the denso code list.
     * 
     * @return the denso code list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso code list.
     * 
     * @param companyDensoList the denso code list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso plant list.
     * 
     * @return the company denso plant list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the company denso plant list.
     * 
     * @param plantDensoList the company denso plant list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    
    /**
     * Gets the invoice status list.
     * 
     * @return the invoice status list.
     */
    public List<MiscellaneousDomain> getInvoiceStatusList() {
        return invoiceStatusList;
    }

    /**
     * Sets the invoice status list.
     * 
     * @param invoiceStatusList the invoice status list.
     */
    public void setInvoiceStatusList(List<MiscellaneousDomain> invoiceStatusList) {
        this.invoiceStatusList = invoiceStatusList;
    }
    
    /**
     * Gets the invoice rcv status list.
     * 
     * @return the invoice rcv status list.
     */
    public List<MiscellaneousDomain> getInvoiceRcvStatusList() {
        return invoiceRcvStatusList;
    }

    /**
     * Sets the invoice rcv status list.
     * 
     * @param invoiceRcvStatusList the invoice rcv status list.
     */
    public void setInvoiceRcvStatusList(List<MiscellaneousDomain> invoiceRcvStatusList) {
        this.invoiceRcvStatusList = invoiceRcvStatusList;
    }
    
    /**
     * Gets the cover page file ID.
     * 
     * @return the cover page file ID.
     */
    public String getCoverPageFileId() {
        return coverPageFileId;
    }
    
    /**
     * Sets the cover page file ID.
     * 
     * @param coverPageFileId the cover page file ID.
     */  
    public void setCoverPageFileId(String coverPageFileId) {
        this.coverPageFileId = coverPageFileId;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadMessage.</p>
     *
     * @return the cannotDownloadMessage
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * <p>Setter method for cannotDownloadMessage.</p>
     *
     * @param cannotDownloadMessage Set for cannotDownloadMessage
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * <p>Getter method for countCancelInvoice.</p>
     *
     * @return the countCancelInvoice
     */
    public String getCountCancelInvoice() {
        return countCancelInvoice;
    }

    /**
     * <p>Setter method for countCancelInvoice.</p>
     *
     * @param countCancelInvoice Set for countCancelInvoice
     */
    public void setCountCancelInvoice(String countCancelInvoice) {
        this.countCancelInvoice = countCancelInvoice;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
}
