/*
 * ModifyDate Development company   Describe 
 * 2014/07/28 CSI Parichat          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WSHP004Form.
 * @author CSI
 */

public class Wshp004Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8067930666382747237L;
    
    /** The mode. */
    private String mode;
    
    /** The screen id. */
    private String screenId;
    
    /** The previous screen id. */
    private String prevScreenId;
    
    /** The temporary mode. */
    private String temporaryMode;
    
    /** The dsc id. */
    private String dscId;
    
    /** The asn no. */
    private String asnNo;
    
    /** The dCd. */
    private String dCd;
    
    /** The asn rcv status. */
    private String asnRcvStatus;
    
    /** The inv status. */
    private String invStatus;
    
    /** The last modified. */
    private String lastModified;
    
    /** The actual etd date. */
    private String actualEtdDate;
    
    /** The actual etd time. */
    private String actualEtdTime;
    
    /** The plan eta date. */
    private String planEtaDate;
    
    /** The plan eta time. */
    private String planEtaTime;
    
    /** The no of pallet. */
    private String noOfPallet;
    
    /** The trip no. */
    private String tripNo;
    
    /** The utc. */
    private String utc;
    
    /** The success flag. */
    private String successFlag;
    
    /** The error message for operating revise shipping qty. */
    private String operateReviseQtyMsg;
    
    //For screen Wshp003
    /** The do id. */
    private String doId;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The revision. */
    private String revision;
    
    /** The route no. */
    private String routeNo;
    
    /** The del. */
    private String del;
    
    //For screen Wshp006
    /** The session id. */
    private String sessionId;
    
    /** The allow revise qty flag. */
    private String allowReviseQtyFlag;
    
    /** The skip validate lot size flag, 0: not skip, 1: skip. */
    private String skipValidateLotSizeFlag;
    
    /** The unmatched lot size flag, 0: matched, 1: unmatched. */
    private String unmatchedLotSizeFlag;
    
    /** The continue operation, 0: not continue, 1: continue. */
    private String continueOperation;
    
    /** The do group asn list. */
    private List<AsnMaintenanceReturnDomain> doGroupAsnList;
    
    /** The chg reason list. */
    private List<SpsMMiscDomain> chgReasonList;    
    
    /** The receiveByScan. */
    private String receiveByScan;
    
    /** The default Constructor */
    public Wshp004Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        asnNo = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        asnRcvStatus = Constants.EMPTY_STRING;
        invStatus = Constants.EMPTY_STRING;
        lastModified = Constants.EMPTY_STRING;
        actualEtdDate = Constants.EMPTY_STRING;
        actualEtdTime = Constants.EMPTY_STRING;
        planEtaDate = Constants.EMPTY_STRING;
        planEtaTime = Constants.EMPTY_STRING;
        noOfPallet = Constants.EMPTY_STRING;
        tripNo = Constants.EMPTY_STRING;
        utc = Constants.EMPTY_STRING;
        chgReasonList = new ArrayList<SpsMMiscDomain>();
        doGroupAsnList = new ArrayList<AsnMaintenanceReturnDomain>();
    }
    
    /**
     * <p>Setter method for do group asn.</p>
     * 
     * @param items the new do group asn
     */
    public void setAsnMaintenanceReturn(AsnMaintenanceReturnDomain items){
        this.doGroupAsnList.add(items);
    }
    
    /**
     * Gets the do group asn.
     * 
     * @param index the index
     * @return the asn maintenance return domain.
     */
    public AsnMaintenanceReturnDomain getAsnMaintenanceReturn(int index){
        if(doGroupAsnList == null){
            doGroupAsnList = new ArrayList<AsnMaintenanceReturnDomain>();
        }
        while(doGroupAsnList.size() <= index){
            doGroupAsnList.add(new AsnMaintenanceReturnDomain());
        }
        return doGroupAsnList.get(index);
    }
    
    /**
     * <p>Setter method for change reason combobox.</p>
     * 
     * @param items the new misc domain
     */
    public void setChangeReasonCb(SpsMMiscDomain items) {
        this.chgReasonList.add(items);
    }
    
    /**
     * Gets the change reason combobox.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public SpsMMiscDomain getChangeReasonCb(int index){
        if(chgReasonList == null){
            chgReasonList = new ArrayList<SpsMMiscDomain>();
        }
        while(chgReasonList.size() <= index){
            chgReasonList.add(new SpsMMiscDomain());
        }
        return chgReasonList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the screen id.
     * 
     * @return the screen id
     */
    public String getScreenId() {
        return screenId;
    }

    /**
     * Sets the prev screen id.
     * 
     * @param screenId the prev screen id
     */
    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }
    
    /**
     * Gets the prev screen id.
     * 
     * @return the prev screen id
     */
    public String getPrevScreenId() {
        return prevScreenId;
    }

    /**
     * Sets the screen id.
     * 
     * @param prevScreenId the screen id
     */
    public void setPrevScreenId(String prevScreenId) {
        this.prevScreenId = prevScreenId;
    }

    /**
     * Gets the dsc id.
     * 
     * @return the dsc id
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Sets the dsc id.
     * 
     * @param dscId the dsc id
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Gets the asn no.
     * 
     * @return the asn no.
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Sets the asn no.
     * 
     * @param asnNo the asn no.
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }
    
    /**
     * Gets the dCd.
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the dCd.
     * 
     * @param dCd the dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the temporary mode.
     * 
     * @return the temporary mode.
     */
    public String getTemporaryMode() {
        return temporaryMode;
    }

    /**
     * Sets the temporary mode.
     * 
     * @param temporaryMode the temporary mode.
     */
    public void setTemporaryMode(String temporaryMode) {
        this.temporaryMode = temporaryMode;
    }

    /**
     * Gets the asn rcv status.
     * 
     * @return the asn rcv status.
     */
    public String getAsnRcvStatus() {
        return asnRcvStatus;
    }

    /**
     * Sets the asn rcv status.
     * 
     * @param asnRcvStatus the asn rcv status.
     */
    public void setAsnRcvStatus(String asnRcvStatus) {
        this.asnRcvStatus = asnRcvStatus;
    }

    /**
     * Gets the inv status.
     * 
     * @return the inv status.
     */
    public String getInvStatus() {
        return invStatus;
    }

    /**
     * Sets the inv status.
     * 
     * @param invStatus the inv status.
     */
    public void setInvStatus(String invStatus) {
        this.invStatus = invStatus;
    }

    /**
     * Gets the last modified.
     * 
     * @return the last modified.
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * Sets the last modified.
     * 
     * @param lastModified the last modified.
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
    
    /**
     * Gets the actual etd date.
     * 
     * @return the actual etd date.
     */
    public String getActualEtdDate() {
        return actualEtdDate;
    }

    /**
     * Sets the actual etd date.
     * 
     * @param actualEtdDate the actual etd date.
     */
    public void setActualEtdDate(String actualEtdDate) {
        this.actualEtdDate = actualEtdDate;
    }

    /**
     * Gets the actual etd time.
     * 
     * @return the actual etd time.
     */
    public String getActualEtdTime() {
        return actualEtdTime;
    }

    /**
     * Sets the actual etd time.
     * 
     * @param actualEtdTime the actual etd time.
     */
    public void setActualEtdTime(String actualEtdTime) {
        this.actualEtdTime = actualEtdTime;
    }

    /**
     * Gets the plan etd date.
     * 
     * @return the plan etd date.
     */
    public String getPlanEtaDate() {
        return planEtaDate;
    }

    /**
     * Sets the plan etd date.
     * 
     * @param planEtaDate the plan etd date.
     */
    public void setPlanEtaDate(String planEtaDate) {
        this.planEtaDate = planEtaDate;
    }

    /**
     * Gets the plan etd time.
     * 
     * @return the plan etd time.
     */
    public String getPlanEtaTime() {
        return planEtaTime;
    }

    /**
     * Sets the plan etd time.
     * 
     * @param planEtaTime the plan etd time.
     */
    public void setPlanEtaTime(String planEtaTime) {
        this.planEtaTime = planEtaTime;
    }

    /**
     * Gets the no of pallet.
     * 
     * @return the no of pallet.
     */
    public String getNoOfPallet() {
        return noOfPallet;
    }

    /**
     * Sets the no of pallet.
     * 
     * @param noOfPallet the no of pallet.
     */
    public void setNoOfPallet(String noOfPallet) {
        this.noOfPallet = noOfPallet;
    }

    /**
     * Gets the trip no.
     * 
     * @return the trip no.
     */
    public String getTripNo() {
        return tripNo;
    }

    /**
     * Sets the trip no.
     * 
     * @param tripNo the trip no.
     */
    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }
    
    /**
     * Gets the utc.
     * 
     * @return the do utc.
     */
    public String getUtc() {
        return utc;
    }

    /**
     * Sets the utc.
     * 
     * @param utc the utc.
     */
    public void setUtc(String utc) {
        this.utc = utc;
    }

    /**
     * Gets the do group asn list.
     * 
     * @return the do group asn list.
     */
    public List<AsnMaintenanceReturnDomain> getDoGroupAsnList() {
        return doGroupAsnList;
    }

    /**
     * Sets the do group asn list.
     * 
     * @param doGroupAsnList the do group asn list.
     */
    public void setDoGroupAsnList(List<AsnMaintenanceReturnDomain> doGroupAsnList) {
        this.doGroupAsnList = doGroupAsnList;
    }

    /**
     * Gets the chg reason list.
     * 
     * @return the chg reason list.
     */
    public List<SpsMMiscDomain> getChgReasonList() {
        return chgReasonList;
    }

    /**
     * Sets the chg reason list.
     * 
     * @param chgReasonList the chg reason list.
     */
    public void setChgReasonList(List<SpsMMiscDomain> chgReasonList) {
        this.chgReasonList = chgReasonList;
    }

    /**
     * Gets the do id.
     * 
     * @return the do id.
     */
    public String getDoId() {
        return doId;
    }

    /**
     * Sets the do id.
     * 
     * @param doId the do id.
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * Gets the sps do no.
     * 
     * @return the sps do no.
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Sets the sps do no.
     * 
     * @param spsDoNo the sps do no.
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Gets the revision.
     * 
     * @return the revision.
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Sets the revision.
     * 
     * @param revision the revision.
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Gets the route no.
     * 
     * @return the route no.
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * Sets the route no.
     * 
     * @param routeNo the route no.
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * Gets the del.
     * 
     * @return the del.
     */
    public String getDel() {
        return del;
    }

    /**
     * Sets the del.
     * 
     * @param del the del.
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * Gets the session id.
     * 
     * @return the session id.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the session id.
     * 
     * @param sessionId the session id.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Gets the success flag.
     * 
     * @return the success flag
     */
    public String getSuccessFlag() {
        return successFlag;
    }

    /**
     * Sets the success flag.
     * 
     * @param successFlag the success flag
     */
    public void setSuccessFlag(String successFlag) {
        this.successFlag = successFlag;
    }
    
    /**
     * Gets the operate revise qty msg.
     * 
     * @return the operate revise qty msg
     */
    public String getOperateReviseQtyMsg() {
        return operateReviseQtyMsg;
    }

    /**
     * Sets the operate revise qty msg.
     * 
     * @param operateReviseQtyMsg the operate revise qty msg
     */
    public void setOperateReviseQtyMsg(String operateReviseQtyMsg) {
        this.operateReviseQtyMsg = operateReviseQtyMsg;
    }
    
    /**
     * <p>Getter method for allowReviseQtyFlag.</p>
     *
     * @return the allowReviseQtyFlag
     */
    public String getAllowReviseQtyFlag() {
        return allowReviseQtyFlag;
    }
   
    /**
     * Sets the allowReviseQtyFlag.
     * 
     * @param allowReviseQtyFlag the allowReviseQtyFlag
     */
    public void setAllowReviseQtyFlag(String allowReviseQtyFlag) {
        this.allowReviseQtyFlag = allowReviseQtyFlag;
    }
    
    /**
     * Gets the skipValidateLotSizeFlag.
     * 
     * @return the skipValidateLotSizeFlag
     */
    public String getSkipValidateLotSizeFlag() {
        return skipValidateLotSizeFlag;
    }

    /**
     * Sets the skipValidateLotSizeFlag.
     * 
     * @param skipValidateLotSizeFlag the skipValidateLotSizeFlag
     */
    public void setSkipValidateLotSizeFlag(String skipValidateLotSizeFlag) {
        this.skipValidateLotSizeFlag = skipValidateLotSizeFlag;
    }

    /**
     * Gets the unmatchedLotSizeFlag.
     * 
     * @return the unmatchedLotSizeFlag
     */
    public String getUnmatchedLotSizeFlag() {
        return unmatchedLotSizeFlag;
    }

    /**
     * Sets the unmatchedLotSizeFlag.
     * 
     * @param unmatchedLotSizeFlag the unmatchedLotSizeFlag
     */
    public void setUnmatchedLotSizeFlag(String unmatchedLotSizeFlag) {
        this.unmatchedLotSizeFlag = unmatchedLotSizeFlag;
    }

    /**
     * Gets the continueOperation.
     * 
     * @return the continueOperation
     */
    public String getContinueOperation() {
        return continueOperation;
    }

    /**
     * Sets the continueOperation.
     * 
     * @param continueOperation the continueOperation
     */
    public void setContinueOperation(String continueOperation) {
        this.continueOperation = continueOperation;
    }
    /**
     * Gets the receiveByScan.
     * 
     * @return the receiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * Sets the receiveByScan.
     * 
     * @param receiveByScan the receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }
    
}