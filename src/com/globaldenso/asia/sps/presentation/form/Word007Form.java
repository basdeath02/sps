package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * <p>
 * Word007 Form.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class Word007Form extends CoreActionForm {

    /**
     * <p>
     * The Serial Version.
     * </p>
     */
    private static final long serialVersionUID = -1562577968523866086L;

    /** The doId. */
    private String doId;

    /** The Supplier Code. */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;

    /** The Supplier Plant Code. */
    private String sPcd;

    /** The DENSO Code. */
    private String dCd;

    /** The DENSO Plant Code. */
    private String dPcd;

    /** The Constant issue Date From. */
    private String issueDateFrom;

    /** The Constant issue Date To. */
    private String issueDateTo;

    /** The Constant delivery Date From. */
    private String deliveryDateFrom;

    /** The Constant delivery Date To. */
    private String deliveryDateTo;

    /** The delivery time from. */
    private String deliveryTimeFrom;
    
    /** The delivery time to */
    private String deliveryTimeTo;
    
    /** The Constant ship Date From. */
    private String shipDateFrom;

    /** The Constant ship Date To. */
    private String shipDateTo;

    /** The Constant revision. */
    private String revision;

    /** The Constant shipment Status. */
    private String shipmentStatus;

    /** The Constant transport Mode. */
    private String transportMode;

    /** The Constant route No. */
    private String routeNo;

    /** The Constant del. */
    private String del;

    /** The Constant sps DO No. */
    private String spsDoNo;

    /** The Constant cigma DO No. */
    private String cigmaDoNo;

    /** The revision List. */
    private List<MiscellaneousDomain> revisionList;

    /** The shipment Status List. */
    private List<MiscellaneousDomain> shipmentStatusList;

    /** The transportMode List. */
    private List<MiscellaneousDomain> transportModeList;

    /** The do Info List. */
    private List<DeliveryOrderDetailDomain> doInfoList;

    /** The List of Company Supplier. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The List of Plant Supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of Plant DENSO. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of Company DENSO. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The mode. */
    private String mode;

    /** The pdf File Id. */
    private String pdfFileId;
    
    /** The pdf File Type Id. */
    private String pdfType;
    
    /** The pdf File Type Id. */
    private String pdfFileKbTagPrintDate;
    
    /** Error message if user cannot click Reset button. */
    private String cannotResetMessage;
    
    /** Error message if user cannot click Download CSV. */
    private String cannotDownloadCsvMessage;
    
    /** The Kanban Information. */
    private List<KanbanOrderInformationResultDomain> kanbanInformation;

    /**
     * <p>
     * The constructor.
     * </p>
     */
    public Word007Form() {
        super();
        this.companySupplierList = new ArrayList<CompanySupplierDomain>();
        this.companyDensoList = new ArrayList<CompanyDensoDomain>();
        this.plantDensoList = new ArrayList<PlantDensoDomain>();
        this.plantSupplierList = new ArrayList<PlantSupplierDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.presentation.form.CoreActionForm#resetForm()
     */
    public void resetForm() {
        sCd = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        issueDateFrom = Constants.EMPTY_STRING;
        issueDateTo = Constants.EMPTY_STRING;
        deliveryDateFrom = Constants.EMPTY_STRING;
        deliveryDateTo = Constants.EMPTY_STRING;
        shipDateFrom = Constants.EMPTY_STRING;
        shipDateTo = Constants.EMPTY_STRING;
        revision = Constants.EMPTY_STRING;
        shipmentStatus = Constants.EMPTY_STRING;
        transportMode = Constants.EMPTY_STRING;
        routeNo = Constants.EMPTY_STRING;
        del = Constants.EMPTY_STRING;
        spsDoNo = Constants.EMPTY_STRING;
        cigmaDoNo = Constants.EMPTY_STRING;
        pdfFileId = Constants.EMPTY_STRING;
        pdfType = Constants.EMPTY_STRING;
        pdfFileKbTagPrintDate = Constants.EMPTY_STRING;
        revisionList = null;
        shipmentStatusList = null;
        transportModeList = null;
        doInfoList = null;
        companySupplierList = null;
        companyDensoList = null;
    }

    /**
     * <p>
     * Getter method for sCd.
     * </p>
     * 
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>
     * Setter method for sCd.
     * </p>
     * 
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>
     * Getter method for sPcd.
     * </p>
     * 
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>
     * Setter method for sPcd.
     * </p>
     * 
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>
     * Getter method for dCd.
     * </p>
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>
     * Setter method for dCd.
     * </p>
     * 
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>
     * Getter method for dPcd.
     * </p>
     * 
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>
     * Setter method for dPcd.
     * </p>
     * 
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>
     * Getter method for issueDateFrom.
     * </p>
     * 
     * @return the issueDateFrom
     */
    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    /**
     * <p>
     * Setter method for issueDateFrom.
     * </p>
     * 
     * @param issueDateFrom Set for issueDateFrom
     */
    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    /**
     * <p>
     * Getter method for issueDateTo.
     * </p>
     * 
     * @return the issueDateTo
     */
    public String getIssueDateTo() {
        return issueDateTo;
    }

    /**
     * <p>
     * Setter method for issueDateTo.
     * </p>
     * 
     * @param issueDateTo Set for issueDateTo
     */
    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    /**
     * <p>
     * Getter method for deliveryDateFrom.
     * </p>
     * 
     * @return the deliveryDateFrom
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * <p>
     * Setter method for deliveryDateFrom.
     * </p>
     * 
     * @param deliveryDateFrom Set for deliveryDateFrom
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * <p>
     * Getter method for deliveryDateTo.
     * </p>
     * 
     * @return the deliveryDateTo
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * <p>
     * Setter method for deliveryDateTo.
     * </p>
     * 
     * @param deliveryDateTo Set for deliveryDateTo
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }

    /**
     * <p>
     * Getter method for shipDateFrom.
     * </p>
     * 
     * @return the shipDateFrom
     */
    public String getShipDateFrom() {
        return shipDateFrom;
    }

    /**
     * <p>
     * Setter method for shipDateFrom.
     * </p>
     * 
     * @param shipDateFrom Set for shipDateFrom
     */
    public void setShipDateFrom(String shipDateFrom) {
        this.shipDateFrom = shipDateFrom;
    }

    /**
     * <p>
     * Getter method for shipDateTo.
     * </p>
     * 
     * @return the shipDateTo
     */
    public String getShipDateTo() {
        return shipDateTo;
    }

    /**
     * <p>
     * Setter method for shipDateTo.
     * </p>
     * 
     * @param shipDateTo Set for shipDateTo
     */
    public void setShipDateTo(String shipDateTo) {
        this.shipDateTo = shipDateTo;
    }

    /**
     * <p>
     * Getter method for revision.
     * </p>
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * <p>
     * Setter method for revision.
     * </p>
     * 
     * @param revision Set for revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * <p>
     * Getter method for shipmentStatus.
     * </p>
     * 
     * @return the shipmentStatus
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * <p>
     * Setter method for shipmentStatus.
     * </p>
     * 
     * @param shipmentStatus Set for shipmentStatus
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * <p>
     * Getter method for transportMode.
     * </p>
     * 
     * @return the transportMode
     */
    public String getTransportMode() {
        return transportMode;
    }

    /**
     * <p>
     * Setter method for transportMode.
     * </p>
     * 
     * @param transportMode Set for transportMode
     */
    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    /**
     * <p>
     * Getter method for routeNo.
     * </p>
     * 
     * @return the routeNo
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * <p>
     * Setter method for routeNo.
     * </p>
     * 
     * @param routeNo Set for routeNo
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * <p>
     * Getter method for del.
     * </p>
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * <p>
     * Setter method for del.
     * </p>
     * 
     * @param del Set for del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * <p>
     * Getter method for spsDoNo.
     * </p>
     * 
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>
     * Setter method for spsDoNo.
     * </p>
     * 
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>
     * Getter method for cigmaDoNo.
     * </p>
     * 
     * @return the cigmaDoNo
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * <p>
     * Setter method for cigmaDoNo.
     * </p>
     * 
     * @param cigmaDoNo Set for cigmaDoNo
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * <p>
     * Getter method for revisionList.
     * </p>
     * 
     * @return the revisionList
     */
    public List<MiscellaneousDomain> getRevisionList() {
        return revisionList;
    }

    /**
     * <p>
     * Setter method for revisionList.
     * </p>
     * 
     * @param revisionList Set for revisionList
     */
    public void setRevisionList(List<MiscellaneousDomain> revisionList) {
        this.revisionList = revisionList;
    }

    /**
     * <p>
     * Getter method for shipmentStatusList.
     * </p>
     * 
     * @return the shipmentStatusList
     */
    public List<MiscellaneousDomain> getShipmentStatusList() {
        return shipmentStatusList;
    }

    /**
     * <p>
     * Setter method for shipmentStatusList.
     * </p>
     * 
     * @param shipmentStatusList Set for shipmentStatusList
     */
    public void setShipmentStatusList(List<MiscellaneousDomain> shipmentStatusList) {
        this.shipmentStatusList = shipmentStatusList;
    }

    /**
     * <p>
     * Getter method for transportModeList.
     * </p>
     * 
     * @return the transportModeList
     */
    public List<MiscellaneousDomain> getTransportModeList() {
        return transportModeList;
    }

    /**
     * <p>
     * Setter method for transportModeList.
     * </p>
     * 
     * @param transportModeList Set for transportModeList
     */
    public void setTransportModeList(List<MiscellaneousDomain> transportModeList) {
        this.transportModeList = transportModeList;
    }

    /**
     * <p>
     * Getter method for doInfoList.
     * </p>
     * 
     * @return the doInfoList
     */
    public List<DeliveryOrderDetailDomain> getDoInfoList() {
        return doInfoList;
    }

    /**
     * <p>
     * Setter method for doInfoList.
     * </p>
     * 
     * @param doInfoList Set for doInfoList
     */
    public void setDoInfoList(List<DeliveryOrderDetailDomain> doInfoList) {
        this.doInfoList = doInfoList;
    }

    /**
     * <p>
     * Getter method for mode.
     * </p>
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * <p>
     * Setter method for mode.
     * </p>
     * 
     * @param mode Set for mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * <p>
     * Getter method for kanbanInformation.
     * </p>
     * 
     * @return the kanbanInformation
     */
    public List<KanbanOrderInformationResultDomain> getKanbanInformation() {
        return kanbanInformation;
    }

    /**
     * <p>
     * Setter method for kanbanInformation.
     * </p>
     * 
     * @param kanbanInformation Set for kanbanInformation
     */
    public void setKanbanInformation(
        List<KanbanOrderInformationResultDomain> kanbanInformation) {
        this.kanbanInformation = kanbanInformation;
    }

    /**
     * <p>
     * Getter method for pdfFileId.
     * </p>
     * 
     * @return the pdfFileId
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * <p>
     * Setter method for pdfFileId.
     * </p>
     * 
     * @param pdfFileId Set for pdfFileId
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }
    /**
     * <p>
     * Getter method for pdfType.
     * </p>
     * 
     * @return the pdfType
     */
    public String getPdfType() {
        return pdfType;
    }
    
    /**
     * <p>
     * Setter method for pdfFileId.
     * </p>
     * 
     * @param pdfType Set for pdfType
     */
    public void setPdfType(String pdfType) {
        this.pdfType = pdfType;
    }

    /**
     * <p>
     * Getter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @return the pdfFileKbTagPrintDate
     */
    public String getPdfFileKbTagPrintDate() {
        return pdfFileKbTagPrintDate;
    }
    
    /**
     * <p>
     * Setter method for pdfFileKbTagPrintDate.
     * </p>
     * 
     * @param pdfFileKbTagPrintDate Set for pdfFileKbTagPrintDate
     */
    public void setPdfFileKbTagPrintDate(String pdfFileKbTagPrintDate) {
        this.pdfFileKbTagPrintDate = pdfFileKbTagPrintDate;
    }  
    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadCsvMessage.</p>
     *
     * @return the cannotDownloadCsvMessage
     */
    public String getCannotDownloadCsvMessage() {
        return cannotDownloadCsvMessage;
    }

    /**
     * <p>Setter method for cannotDownloadCsvMessage.</p>
     *
     * @param cannotDownloadCsvMessage Set for cannotDownloadCsvMessage
     */
    public void setCannotDownloadCsvMessage(String cannotDownloadCsvMessage) {
        this.cannotDownloadCsvMessage = cannotDownloadCsvMessage;
    }

    /**
     * <p>Getter method for deliveryTimeFrom.</p>
     *
     * @return the deliveryTimeFrom
     */
    public String getDeliveryTimeFrom() {
        return deliveryTimeFrom;
    }

    /**
     * <p>Setter method for deliveryTimeFrom.</p>
     *
     * @param deliveryTimeFrom Set for deliveryTimeFrom
     */
    public void setDeliveryTimeFrom(String deliveryTimeFrom) {
        this.deliveryTimeFrom = deliveryTimeFrom;
    }

    /**
     * <p>Getter method for deliveryTimeTo.</p>
     *
     * @return the deliveryTimeTo
     */
    public String getDeliveryTimeTo() {
        return deliveryTimeTo;
    }

    /**
     * <p>Setter method for deliveryTimeTo.</p>
     *
     * @param deliveryTimeTo Set for deliveryTimeTo
     */
    public void setDeliveryTimeTo(String deliveryTimeTo) {
        this.deliveryTimeTo = deliveryTimeTo;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }

    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Sets the Revision
     * 
     * @param revision the new Revision.
     */
    public void setRevisionItem(MiscellaneousDomain revision) {
        this.revisionList.add(revision);
    }
    
    /**
     * Gets the Revision.
     * 
     * @param index the index
     * @return the Revision.
     */
    public MiscellaneousDomain getRevisionItem(int index) {
        if (revisionList == null) {
            revisionList = new ArrayList<MiscellaneousDomain>();
        }
        while (revisionList.size() <= index) {
            revisionList.add(new MiscellaneousDomain());
        }
        return revisionList.get(index);
    }

    /**
     * Sets the Shipment Status
     * 
     * @param shipmentStatus the new Shipment Status.
     */
    public void setShipmentStatusItem(MiscellaneousDomain shipmentStatus) {
        this.shipmentStatusList.add(shipmentStatus);
    }
    
    /**
     * Gets the Shipment Status.
     * 
     * @param index the index
     * @return the Shipment Status.
     */
    public MiscellaneousDomain getShipmentStatusItem(int index) {
        if (shipmentStatusList == null) {
            shipmentStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while (shipmentStatusList.size() <= index) {
            shipmentStatusList.add(new MiscellaneousDomain());
        }
        return shipmentStatusList.get(index);
    }

    /**
     * Sets the Transport Mode
     * 
     * @param transportMode the new Transport Mode.
     */
    public void setTransportModeItem(MiscellaneousDomain transportMode) {
        this.transportModeList.add(transportMode);
    }
    
    /**
     * Gets the Transport Mode.
     * 
     * @param index the index
     * @return the Transport Mode.
     */
    public MiscellaneousDomain getTransportModeItem(int index) {
        if (transportModeList == null) {
            transportModeList = new ArrayList<MiscellaneousDomain>();
        }
        while (transportModeList.size() <= index) {
            transportModeList.add(new MiscellaneousDomain());
        }
        return transportModeList.get(index);
    }

    /**
     * Sets the D/O Information
     * 
     * @param doInfo the new D/O Information.
     */
    public void setDoInfoItem(DeliveryOrderDetailDomain doInfo) {
        this.doInfoList.add(doInfo);
    }
    
    /**
     * Gets the D/O Information.
     * 
     * @param index the index
     * @return the D/O Information.
     */
    public DeliveryOrderDetailDomain getDoInfoItem(int index) {
        if (doInfoList == null) {
            doInfoList = new ArrayList<DeliveryOrderDetailDomain>();
        }
        while (doInfoList.size() <= index) {
            doInfoList.add(new DeliveryOrderDetailDomain());
        }
        return doInfoList.get(index);
    }

    /**
     * Sets the Company Supplier
     * 
     * @param companySupplier the new Company Supplier.
     */
    public void setCompanySupplierItem(CompanySupplierDomain companySupplier) {
        this.companySupplierList.add(companySupplier);
    }
    
    /**
     * Gets the Company Supplier.
     * 
     * @param index the index
     * @return the Company Supplier.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index) {
        if (companySupplierList == null) {
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while (companySupplierList.size() <= index) {
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }

    /**
     * Sets the Plant Supplier
     * 
     * @param plantSupplier the new Plant Supplier.
     */
    public void setPlantSupplierItem(PlantSupplierDomain plantSupplier) {
        this.plantSupplierList.add(plantSupplier);
    }
    
    /**
     * Gets the Plant Supplier.
     * 
     * @param index the index
     * @return the Plant Supplier.
     */
    public PlantSupplierDomain getPlantSupplierItem(int index) {
        if (plantSupplierList == null) {
            plantSupplierList = new ArrayList<PlantSupplierDomain>();
        }
        while (plantSupplierList.size() <= index) {
            plantSupplierList.add(new PlantSupplierDomain());
        }
        return plantSupplierList.get(index);
    }

    /**
     * Sets the Company DENSO
     * 
     * @param companyDenso the new Company DENSO.
     */
    public void setCompanyDensoItem(CompanyDensoDomain companyDenso) {
        this.companyDensoList.add(companyDenso);
    }
    
    /**
     * Gets the Company DENSO.
     * 
     * @param index the index
     * @return the Company DENSO.
     */
    public CompanyDensoDomain getCompanyDensoItem(int index) {
        if (companyDensoList == null) {
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while (companyDensoList.size() <= index) {
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }

    /**
     * Sets the Plant DENSO
     * 
     * @param plantDenso the new Plant DENSO.
     */
    public void setPlantDensoItem(PlantDensoDomain plantDenso) {
        this.plantDensoList.add(plantDenso);
    }
    
    /**
     * Gets the Plant DENSO.
     * 
     * @param index the index
     * @return the Plant DENSO.
     */
    public PlantDensoDomain getPlantDensoItem(int index) {
        if (plantDensoList == null) {
            plantDensoList = new ArrayList<PlantDensoDomain>();
        }
        while (plantDensoList.size() <= index) {
            plantDensoList.add(new PlantDensoDomain());
        }
        return plantDensoList.get(index);
    }
    /**
     * Gets the doId.
     * 
     * @param doId
     * @return the doId.
     */

    public String getDoId() {
        return doId;
    }

    /**
     * Sets the doId
     * 
     * @param doId the doId.
     */

    public void setDoId(String doId) {
        this.doId = doId;
    }

}
