/*
 * ModifyDate Development company     Describe 
 * 2014/06/17 CSI Phakaporn           Create
 * 2016/02/10 CSI Akat                [IN049]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.common.constant.Constants;


/**
 * The Class WADM006Form.
 * @author CSI
 */
public class Wadm006Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The Company DENSO Code. */
    private String dCd;
   
    /** The Plant DENSO. */
    private String dPcd;
    
    /** The first name. */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The Employee code. */
    private String employeeCode;
    
    /** The department. */
    private String departmentName; 
    
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The Mode. */
    private String mode;
    
    /** The Mode. */
    private Boolean isReset = false;
    
    /** The Email to Create/Cancel ASN flag. */
    private String emlCreateCancelAsnFlag;
    
    /** The Email to Revise ASN flag. */
    private String emlReviseAsnFlag;
    
    /** The Email to Create Invoice flag. */
    private String emlCreateInvoiceFlag;
    
    // Start : [IN049] Change Column Name
    /** The Email to Abnormal Transfer Data 1 flag. */
    //private String emlAbnormalTransferData1Flag;
    /** The Email to Abnormal Transfer Data 2 flag. */
    //private String emlAbnormalTransferData2Flag;
    /** Email to Pending P/O | D/O already create ASN | Supplier Info Not Found. */
    private String emlPedpoDoalcasnUnmatpnFlg;
    
    /** Email to abnormal data transfer. */
    private String emlAbnormalTransferFlg;
    // End : [IN049] Change Column Name
    
    /** The List of Company DENSO domain. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of Plant DENSO domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of Supplier User domain. */
    private List<SpsMUserDomain> departmentList;
    
    /** The max of record department list. */
    private int maxDepartment;
    
    /** The Date time update. */
    private String updateDatetime;
    
    /** The Date time update. */
    private String updateDatetimeDenso;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The default Constructor */
    public Wadm006Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        email = Constants.EMPTY_STRING;
        dscId = Constants.EMPTY_STRING;
        telephone = Constants.EMPTY_STRING;
        firstName = Constants.EMPTY_STRING;
        middleName = Constants.EMPTY_STRING;
        lastName = Constants.EMPTY_STRING;
        departmentName = Constants.EMPTY_STRING;
        emlCreateCancelAsnFlag = Constants.STR_ZERO;
        emlReviseAsnFlag = Constants.STR_ZERO;
        emlCreateInvoiceFlag = Constants.STR_ZERO;
        // Start : [IN049] Change Column Name
        //emlAbnormalTransferData1Flag = Constants.STR_ZERO;
        //emlAbnormalTransferData2Flag = Constants.STR_ZERO;
        emlPedpoDoalcasnUnmatpnFlg = Constants.STR_ZERO;
        emlAbnormalTransferFlg = Constants.STR_ZERO;
        // End : [IN049] Change Column Name
        
        employeeCode = Constants.EMPTY_STRING;
    }
    
    /**
     * <p>Setter method for Company Denso Item.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoItem(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the Company Denso Item.
     * 
     * @param index the index
     * @return the company denso domain.
     */
    public CompanyDensoDomain getCompanyDensoItem(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * Sets the Department.
     * 
     * @param department the new Department.
     */
    public void setDepartmentItem(SpsMUserDomain department) {
        this.departmentList.add(department);
    }
    
    /**
     * Gets the Department.
     * 
     * @param index the index
     * @return the Department.
     */
    public SpsMUserDomain getDepartmentItem(int index) {
        if (departmentList == null) {
            departmentList = new ArrayList<SpsMUserDomain>();
        }
        while (departmentList.size() <= index) {
            departmentList.add(new SpsMUserDomain());
        }
        return departmentList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode(Register & Edit).
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode(Register & Edit).
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the Reset flag.
     * 
     * @return the Reset flag.
     */
    public Boolean getIsReset() {
        return isReset;
    }

    /**
     * Sets the Reset flag.
     * 
     * @param isReset the Reset flag.
     */
    public void setIsReset(Boolean isReset) {
        this.isReset = isReset;
    }

    /**
     * Gets the Email to Create Cancel ASN flag.
     * 
     * @return the emlCreateCancelAsnFlag
     */
    public String getEmlCreateCancelAsnFlag() {
        return emlCreateCancelAsnFlag;
    }

    /**
     * Sets the Email to Create Cancel ASN flag.
     * 
     * @param emlCreateCancelAsnFlag the Email to Create Cancel ASN flag
     */
    public void setEmlCreateCancelAsnFlag(String emlCreateCancelAsnFlag) {
        this.emlCreateCancelAsnFlag = emlCreateCancelAsnFlag;
    }

    /**
     * Gets the Email to Revise ASN flag.
     * 
     * @return the emlReviseAsnFlag
     */
    public String getEmlReviseAsnFlag() {
        return emlReviseAsnFlag;
    }

    /**
     * Sets the Email to Revise ASN flag.
     * 
     * @param emlReviseAsnFlag the Email to Revise ASN flag
     */
    public void setEmlReviseAsnFlag(String emlReviseAsnFlag) {
        this.emlReviseAsnFlag = emlReviseAsnFlag;
    }

    /**
     * Gets the Email to Create Invoice flag.
     * 
     * @return the emlCreateInvoiceFlag
     */
    public String getEmlCreateInvoiceFlag() {
        return emlCreateInvoiceFlag;
    }

    /**
     * Sets the Email to Create Invoice flag.
     * 
     * @param emlCreateInvoiceFlag the Email to Create Invoice flag
     */
    public void setEmlCreateInvoiceFlag(String emlCreateInvoiceFlag) {
        this.emlCreateInvoiceFlag = emlCreateInvoiceFlag;
    }

    // Start : [IN049] Change Column Name
    /**
     * Gets the Email to Abnormal Transfer Data 1 flag.
     * 
     * @return the emlAbnormalTransferData1Flag
     */
    //public String getEmlAbnormalTransferData1Flag() {
    //    return emlAbnormalTransferData1Flag;
    //}
    /**
     * Sets the Email to Abnormal Transfer Data 1 flag.
     * 
     * @param emlAbnormalTransferData1Flag the Email to Abnormal Transfer Data 1 flag
     */
    //public void setEmlAbnormalTransferData1Flag(String emlAbnormalTransferData1Flag) {
    //    this.emlAbnormalTransferData1Flag = emlAbnormalTransferData1Flag;
    //}
    /**
     * Gets the Email to Abnormal Transfer Data 2 flag.
     * 
     * @return the emlAbnormalTransferData1Flag
     */
    //public String getEmlAbnormalTransferData2Flag() {
    //    return emlAbnormalTransferData2Flag;
    //}
    /**
     * Sets the Email to Abnormal Transfer Data 2 flag.
     * 
     * @param emlAbnormalTransferData2Flag the Email to Abnormal Transfer Data 2 flag
     */
    //public void setEmlAbnormalTransferData2Flag(String emlAbnormalTransferData2Flag) {
    //    this.emlAbnormalTransferData2Flag = emlAbnormalTransferData2Flag;
    //}
    
    /**
     * <p>Getter method for emlPedpoDoalcasnUnmatpnFlg.</p>
     *
     * @return the emlPedpoDoalcasnUnmatpnFlg
     */
    public String getEmlPedpoDoalcasnUnmatpnFlg() {
        return emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * <p>Setter method for emlPedpoDoalcasnUnmatpnFlg.</p>
     *
     * @param emlPedpoDoalcasnUnmatpnFlg Set for emlPedpoDoalcasnUnmatpnFlg
     */
    public void setEmlPedpoDoalcasnUnmatpnFlg(String emlPedpoDoalcasnUnmatpnFlg) {
        this.emlPedpoDoalcasnUnmatpnFlg = emlPedpoDoalcasnUnmatpnFlg;
    }

    /**
     * <p>Getter method for emlAbnormalTransferFlg.</p>
     *
     * @return the emlAbnormalTransferFlg
     */
    public String getEmlAbnormalTransferFlg() {
        return emlAbnormalTransferFlg;
    }

    /**
     * <p>Setter method for emlAbnormalTransferFlg.</p>
     *
     * @param emlAbnormalTransferFlg Set for emlAbnormalTransferFlg
     */
    public void setEmlAbnormalTransferFlg(String emlAbnormalTransferFlg) {
        this.emlAbnormalTransferFlg = emlAbnormalTransferFlg;
    }
    // End : [IN049] Change Column Name

    /**
     * Gets the employee Code.
     * 
     * @return the employee Code.
     */
    public String getEmployeeCode() {
        return employeeCode;
    }

    /**
     * Sets the employee Code.
     * 
     * @param employeeCode the employee Code.
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    
    /**
     * Gets the department name.
     * 
     * @return the department name.
     */
    public String getDepartmentName() {
        return departmentName;
    }
    /**
     * Sets the department name.
     * 
     * @param departmentName the department name.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    /**
     * Gets the Company DENSO Code.
     * 
     * @return the Company DENSO Code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Sets the Company DENSO Code.
     * 
     * @param dCd the Company DENSO Code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    /**
     * Gets the Plant DENSO code.
     * 
     * @return the Plant DENSO code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the Plant DENSO code.
     * 
     * @param dPcd the Plant DENSO code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * Gets the list of Company DENSO
     * 
     * @return the list of Company DENSO
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }
    /**
     * Sets the list of Company DENSO
     * 
     * @param companyDensoList the list of Company DENSO
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    /**
     * Gets the list of plant DENSO
     * 
     * @return the list of plant DENSO
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }
    /**
     * Sets the list of plant DENSO
     * 
     * @param plantDensoList the list of plant DENSO
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    
    /**
     * Gets the list of department.
     * 
     * @return the list of department.
     */
    public List<SpsMUserDomain> getDepartmentList() {
        return departmentList;
    }
    
    /**
     * Sets the list of department.
     * 
     * @param departmentList the list of department.
     */
    public void setDepartmentList(List<SpsMUserDomain> departmentList) {
        this.departmentList = departmentList;
    }
    
    /**
     * Gets the max of record department list.
     * 
     * @return the max of record department list.
     */
    public int getMaxDepartment() {
        return maxDepartment;
    }
    /**
     * Sets the max of record department list.
     * 
     * @param maxDepartment the max of record department list.
     */
    public void setMaxDepartment(int maxDepartment) {
        this.maxDepartment = maxDepartment;
    }
    
    /**
     * Gets the updateDatetime.
     * 
     * @return the updateDatetime.
     */
    public String getUpdateDatetime() {
        return updateDatetime;
    }
    /**
     * Sets the updateDatetime.
     * 
     * @param updateDatetime the updateDatetime.
     */
    public void setUpdateDatetime(String updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
    /**
     * Gets the updateDatetimeDenso.
     * 
     * @return the updateDatetimeDenso.
     */
    public String getUpdateDatetimeDenso() {
        return updateDatetimeDenso;
    }
    /**
     * Sets the updateDatetimeDenso.
     * 
     * @param updateDatetimeDenso the updateDatetimeDenso.
     */
    public void setUpdateDatetimeDenso(String updateDatetimeDenso) {
        this.updateDatetimeDenso = updateDatetimeDenso;
    }
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

}