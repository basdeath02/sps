/*
 * ModifyDate Development company     Describe 
 * 2014/04/23 CSI Phakaporn           Create
 * 
 * (c) Copyright DENSO All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WADM001Form.
 * @author CSI
 */
public class Wadm001Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID Selected. */
    private String dscIdSelected;
    
    /** The Company Supplier Code. */
    private String sCd;
    
    /** The Plant Supplier. */
    private String sPcd;
    
    /** The first name */
    private String firstName;
    
    /** The Middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The register date. */
    private String registerDate;
    
    /** The department. */
    private String department; 
    
    /** The user name. */
    private String userName; 
    
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The mode. */
    private String mode; 
    
    /** The count of delete. */
    private String countDelete;
    
    /** The company supplier code criteria. */
    private String sCdCriteria;
    
    /** The return flag. */
    private Boolean isReturn = false;
    
    /** The Plant Supplier Selected. */
    private String sPcdSelected;
    
    /** The Company Supplier Code Selected. */
    private String sCdSelected;
    
    /** The First Name Selected. */
    private String firstNameSelected;
    
    /** The Middle Name Selected. */
    private String middleNameSelected;
    
    /** The last Name Selected. */
    private String lastNameSelected;
    
    /** The Company Name. */
    private String companyName;
    
    /** Error message if user cannot click Reset button. */
    private String cannotResetMessage;
    
    /** Error message if user cannot click Download CSV. */
    private String cannotDownloadCsvMessage;
    
    /** The List of Supplier User Info domain. */
    private List<SupplierUserInformationDomain> userSupplierInfoList;
    
    /** The List of Company Supplier domain. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The List of plant supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The default Constructor */
    public Wadm001Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        sCd = Constants.EMPTY_STRING;
        telephone = Constants.EMPTY_STRING;
        email = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        userName = Constants.EMPTY_STRING;
        department = Constants.EMPTY_STRING;
        dscId = Constants.EMPTY_STRING;
        registerDate = Constants.EMPTY_STRING;
        registerDateFrom = Constants.EMPTY_STRING;
        registerDateTo = Constants.EMPTY_STRING;
        firstName = Constants.EMPTY_STRING;
        middleName = Constants.EMPTY_STRING;
        lastName = Constants.EMPTY_STRING;
        isReturn = false;
        countDelete = Constants.STR_ZERO;
        userSupplierInfoList = new ArrayList<SupplierUserInformationDomain>();
    }
    
    /**
     * Sets the supplier item.
     * 
     * @param items the new supplier item
     */
    public void setUserSupplierInfo(SupplierUserInformationDomain items) {
        this.userSupplierInfoList.add(items);
    }
    
    /**
     * Gets the supplier item.
     * 
     * @param index the index
     * @return the supplier item.
     */
    public SupplierUserInformationDomain getUserSupplierInfo(int index){
        if (userSupplierInfoList == null) {
            userSupplierInfoList = new ArrayList<SupplierUserInformationDomain>();
        }
        while (userSupplierInfoList.size() <= index) {
            userSupplierInfoList.add(new SupplierUserInformationDomain());
        }
        return userSupplierInfoList.get(index);
    }
    
    /**
     * Sets the company supplier item.
     * 
     * @param items the new company supplier domain
     */
    public void setCompanySupplierItem(CompanySupplierDomain items){
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the company supplier item.
     * 
     * @param index the index
     * @return the company supplier domain.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index){
        if(companySupplierList == null){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the supplier plant code
     * 
     * @return the supplier plant code
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }

    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }

    /**
     * Gets the register Date.
     * 
     * @return the register Date.
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * Sets the register Date.
     * 
     * @param registerDate the register Date.
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * Gets the list of user supplier information detail.
     * 
     * @return the list of user supplier information detail.
     */
    public List<SupplierUserInformationDomain> getUserSupplierInfoList() {
        return userSupplierInfoList;
    }

    /**
     * Sets the list of user supplier information detail.
     * 
     * @param userSupplierInfoList the list of user supplier information detail.
     */
    public void setUserSupplierInfoList(List<SupplierUserInformationDomain> userSupplierInfoList) {
        this.userSupplierInfoList = userSupplierInfoList;
    }

    /**
     * Gets the Department.
     * 
     * @return the Department.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the Department.
     * 
     * @param department the Department.
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * Gets the user name.
     * 
     * @return the user name.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     * 
     * @param userName the user name.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Gets the DSCID that Selected.
     * 
     * @return the DSCID Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }

    /**
     * Sets the DSCID that Selected.
     * 
     * @param dscIdSelected the DSCID that Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode(Register & Edit).
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode(Register & Edit).
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the Return flag.
     * 
     * @return the isReturn flag.
     */
    public Boolean getIsReturn() {
        return isReturn;
    }

    /**
     * Sets the isReturn.
     * 
     * @param isReturn the Return flag.
     */
    public void setIsReturn(Boolean isReturn) {
        this.isReturn = isReturn;
    }

    /**
     * Gets the middle name.
     * 
     * @return the middle name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the middle name.
     * 
     * @param middleName the middle name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the Company Supplier Code.
     * 
     * @return the Company Supplier Code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Sets the Company Supplier Code.
     * 
     * @param sCd the Company Supplier Code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Gets the Plant Supplier Code.
     * 
     * @return the Plant Supplier Code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the Plant Supplier Code.
     * 
     * @param sPcd the Plant Supplier Code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * Gets the list of company supplier.
     * 
     * @return the list of company supplier.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }
    /**
     * Sets the list of company supplier.
     * 
     * @param companySupplierList the list of company supplier.
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }
    /**
     * Gets the list of plant supplier.
     * 
     * @return the list of plant supplier.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the list of plant supplier.
     * 
     * @param plantSupplierList the list of plant supplier.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * Gets the count of delete.
     * 
     * @return the count of delete.
     */
    public String getCountDelete() {
        return countDelete;
    }

    /**
     * Sets the count of delete.
     * 
     * @param countDelete the count of delete.
     */
    public void setCountDelete(String countDelete) {
        this.countDelete = countDelete;
    }
    /**
     * Gets the company supplier code criteria.
     * 
     * @return the company supplier code criteria.
     */
    public String getSCdCriteria() {
        return sCdCriteria;
    }

    /**
     * Sets the company supplier code criteria.
     * 
     * @param sCdCriteria the company supplier code criteria.
     */
    public void setSCdCriteria(String sCdCriteria) {
        this.sCdCriteria = sCdCriteria;
    }
    
    /**
     * Gets the Plant Supplier Selected.
     * 
     * @return the Plant Supplier Selected.
     */
    public String getSPcdSelected() {
        return sPcdSelected;
    }

    /**
     * Sets the Plant Supplier Selected.
     * 
     * @param sPcdSelected the Plant Supplier Selected.
     */
    public void setSPcdSelected(String sPcdSelected) {
        this.sPcdSelected = sPcdSelected;
    }
    
    /**
     * Gets the company supplier code selected.
     * 
     * @return the company supplier code selected.
     */
    public String getSCdSelected() {
        return sCdSelected;
    }
    /**
     * Sets the company supplier code selected.
     * 
     * @param sCdSelected the company supplier code selected.
     */
    public void setSCdSelected(String sCdSelected) {
        this.sCdSelected = sCdSelected;
    }
    /**
     * Gets the first name selected.
     * 
     * @return the first name selected.
     */
    public String getFirstNameSelected() {
        return firstNameSelected;
    }
    /**
     * Sets the first name selected.
     * 
     * @param firstNameSelected the first name selected.
     */
    public void setFirstNameSelected(String firstNameSelected) {
        this.firstNameSelected = firstNameSelected;
    }
    /**
     * Gets the middle name selected.
     * 
     * @return the middle name selected.
     */
    public String getMiddleNameSelected() {
        return middleNameSelected;
    }
    /**
     * Sets the middle name selected.
     * 
     * @param middleNameSelected the middle name selected.
     */
    public void setMiddleNameSelected(String middleNameSelected) {
        this.middleNameSelected = middleNameSelected;
    }
    /**
     * Gets the last name selected.
     * 
     * @return the last name selected.
     */
    public String getLastNameSelected() {
        return lastNameSelected;
    }
    /**
     * Sets the last name selected.
     * 
     * @param lastNameSelected the last name selected.
     */
    public void setLastNameSelected(String lastNameSelected) {
        this.lastNameSelected = lastNameSelected;
    }
    /**
     * Gets the company name.
     * 
     * @return the company name.
     */
    public String getCompanyName() {
        return companyName;
    }
    /**
     * Sets the company name.
     * 
     * @param companyName the company name.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadCsvMessage.</p>
     *
     * @return the cannotDownloadCsvMessage
     */
    public String getCannotDownloadCsvMessage() {
        return cannotDownloadCsvMessage;
    }

    /**
     * <p>Setter method for cannotDownloadCsvMessage.</p>
     *
     * @param cannotDownloadCsvMessage Set for cannotDownloadCsvMessage
     */
    public void setCannotDownloadCsvMessage(String cannotDownloadCsvMessage) {
        this.cannotDownloadCsvMessage = cannotDownloadCsvMessage;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

}