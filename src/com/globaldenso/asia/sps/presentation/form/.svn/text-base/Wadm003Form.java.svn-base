/*
 * ModifyDate Development company     Describe 
 * 2014/06/17 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.TmpUploadErrorDomain;
import com.globaldenso.asia.sps.common.constant.Constants;


/**
 * The Class WADM003Form.
 * @author CSI
 */
public class Wadm003Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The file ID. */
    private String fileId;
    
    /** The file name. */
    private String fileName;
    
    /** The file data. */
    private FormFile fileData;
    
    /** The total error Record. */
    private String totalRecord;
    
    /** The correct Record. */
    private String correctRecord;
    
    /** The warning Record. */
    private String warningRecord;
    
    /** The incorrect Record. */
    private String incorrectRecord;
    
    /** The isUpload Flag. */
    private Boolean isUpload = false;
    
    /** The List of Temporary Upload Error domain. */
    private List<TmpUploadErrorDomain> tmpUploadErrorList;
    
    /** The List of Group Upload Error domain. */
    private List<GroupUploadErrorDomain> uploadErrorDetailDomain;

    /** Error message if user cannot click Reset button. */
    private String cannotResetMessage;
    
    /** The default Constructor */
    public Wadm003Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        totalRecord = Constants.STR_ZERO;
        correctRecord = Constants.STR_ZERO;
        warningRecord = Constants.STR_ZERO;
        incorrectRecord = Constants.STR_ZERO;
        fileName = Constants.EMPTY_STRING;
        fileData = null;
        uploadErrorDetailDomain = new ArrayList<GroupUploadErrorDomain>();
    }

    /**
     * <p>Setter method for file ID.</p>
     *
     * @param fileId Set for file ID
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * <p>Getter method for file ID.</p>
     *
     * @return the file ID.
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Getter method for file data.</p>
     *
     * @return the file data.
     */
    public FormFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter method for file data.</p>
     *
     * @param fileData Set for file data.
     */
    public void setFileData(FormFile fileData) {
        this.fileData = fileData;
    }

    /**
     * Gets the total error Record.
     * 
     * @return the total error Record.
     */
    public String getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the total error Record.
     * 
     * @param totalRecord the total error Record.
     */
    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    /**
     * Gets the correct Record.
     * 
     * @return the correct Record.
     */
    public String getCorrectRecord() {
        return correctRecord;
    }

    /**
     * Sets the correct Record.
     * 
     * @param correctRecord the correct Record.
     */
    public void setCorrectRecord(String correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * Gets the warning record.
     * 
     * @return the warning record.
     */
    public String getWarningRecord() {
        return warningRecord;
    }

    /**
     * Sets the warning record.
     * 
     * @param warningRecord the warning record.
     */
    public void setWarningRecord(String warningRecord) {
        this.warningRecord = warningRecord;
    }

    /**
     * Gets the Incorrect record.
     * 
     * @return the Incorrect record.
     */
    public String getIncorrectRecord() {
        return incorrectRecord;
    }

    /**
     * Sets the Incorrect record.
     * 
     * @param incorrectRecord the Incorrect record.
     */
    public void setIncorrectRecord(String incorrectRecord) {
        this.incorrectRecord = incorrectRecord;
    }

    /**
     * Gets the Upload flag.
     * 
     * @return the Upload flag.
     */
    public Boolean getIsUpload() {
        return isUpload;
    }

    /**
     * Sets the Upload flag.
     * 
     * @param isUpload the Upload flag.
     */
    public void setIsUpload(Boolean isUpload) {
        this.isUpload = isUpload;
    }
    
    /**
     * Gets the Temporary of Upload Error List.
     * 
     * @return the Temporary of Upload Error List.
     */
    public List<TmpUploadErrorDomain> getTmpUploadErrorList() {
        return tmpUploadErrorList;
    }

    /**
     * Sets the Temporary of Upload Error List.
     * 
     * @param tmpUploadErrorList the Temporary of Upload Error List.
     */
    public void setTmpUploadErrorList(List<TmpUploadErrorDomain> tmpUploadErrorList) {
        this.tmpUploadErrorList = tmpUploadErrorList;
    }
    /**
     * Gets the list of group error domain.
     * 
     * @return the list of group error domain.
     */
    public List<GroupUploadErrorDomain> getUploadErrorDetailDomain() {
        return uploadErrorDetailDomain;
    }

    /**
     * Sets the list of group error domain.
     * 
     * @param uploadErrorDetailDomain the list of group error domain.
     */
    public void setUploadErrorDetailDomain(
        List<GroupUploadErrorDomain> uploadErrorDetailDomain) {
        this.uploadErrorDetailDomain = uploadErrorDetailDomain;
    }
    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }
    
    /**
     * Sets the Upload Error.
     * 
     * @param uploadError the new Upload Error.
     */
    public void setUploaderror(GroupUploadErrorDomain uploadError) {
        this.uploadErrorDetailDomain.add(uploadError);
    }
    
    /**
     * Gets the Upload Error.
     * 
     * @param index the index
     * @return the Upload Error.
     */
    public GroupUploadErrorDomain getUploaderror(int index) {
        if (uploadErrorDetailDomain == null) {
            uploadErrorDetailDomain = new ArrayList<GroupUploadErrorDomain>();
        }
        while (uploadErrorDetailDomain.size() <= index) {
            uploadErrorDetailDomain.add(new GroupUploadErrorDomain());
        }
        return uploadErrorDetailDomain.get(index);
    }
    
}