/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/24 CSI Karnrawee      Create
 * 2016/03/08 CSI Akat           [IN059]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;

/**
 * The Class Winv004Form.
 * @author CSI
 */
public class Winv004Form extends CoreActionForm {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 8720900529081611615L;
    
    /** The mode. */
    private String mode;
    
    /** The invoice date. */
    private String invoiceDateFrom;
    
    /** The invoice date. */
    private String invoiceDateTo;
    
    /** The update date. */
    private String cnDateFrom;
    
    /** The update date. */
    private String cnDateTo;
    
    /** The supplier code. */
    private String sCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The invoice status.*/
    private String invoiceStatus;
    
    /** The invoice no.*/
    private String invoiceNoCriteria;
    
    /** The list of invoice information domain.*/
    private List<InvoiceInformationDomain> invoiceInformationForDisplayList;
    
    /** The DSC ID.*/
    private String dscId;
    
    /** The supplier plant tax ID. */
    private String supplierPlantTaxId;
    
    /** The warning message. */
    private String warningMessage;
    
    /** The screen ID. */
    private String screenId;
    
    /** The invoice ID. */
    private String invoiceId;
    
    /** The invoice date. */
    private String invoiceDate;
    
    /** The invoice RCV status. */
    private String invoiceRCVStatus;
    
    /** The invoice no. */
    private String invoiceNo;
    
    /** The session Id. */
    private String sessionId;
    
    /** The denso name. */
    private String densoName;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The denso tax id. */
    private String densoTaxId;
    
    /** The denso address. */
    private String densoAddress;
    
    /** The denso currency. */
    private String densoCurrency;
    
    /** The supplier name. */
    private String supplierName;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier tax id. */
    private String supplierTaxId;
    
    /** The supplier address. */
    private String supplierAddress;
    
    /** The supplier address1. */
    private String supplierAddress1;
    
    /** The supplier address2. */
    private String supplierAddress2;
    
    /** The supplier address3. */
    private String supplierAddress3;
    
    /** The supplier currency. */
    private String supplierCurrency;
    
    /** The decimal disp. */
    private String decimalDisp;
    
    /** The vat rate */
    private String vatRate;
    
    // [IN059] To keep default VAT Rate
    /** Default VAT Rate. */
    private String defaultVatRate;
    
    /** The vat type */
    private String vatType;
    
    /** The vat rate */
    private String supplierBaseAmount;
    
    /** The vat rate */
    private String supplierVatAmount;
    
    /** The vat rate */
    private String supplierTotalAmount;
    
    /** The vat rate */
    private String cnNo;
    
    /** The vat rate */
    private String cnDate;
    
    /** The vat rate */
    private String cnBaseAmount;
    
    /** The vat rate */
    private String cnVatAmount;
    
    /** The vat rate */
    private String cnTotalAmount;
    
    /** The total diff base amount */
    private String totalDiffBaseAmount;
    
    /** The total diff vat amount */
    private String totalDiffVatAmount;
    
    /** The total diff amount */
    private String totalDiffAmount;
    
    /** The total total base amount */
    private String totalBaseAmount;
    
    /** The total total vat amount */
    private String totalVatAmount;
    
    /** The total total amount */
    private String totalAmount;
    
    /** The string confirm*/
    private String strConfirm;
    
    /** The string confirm return*/
    private String strConfirmReturn;
    
    /** The confirmation message for Calculate button. */
    private String strConfirmCalculate;
    
    /** The error message for inputed supplier invoice amount. */
    private String errorSupplierInvoiceAmount;
    
    /** The error message for inputed supplier unit price. */
    private String errorSupplierUnitPrice;

    /** The error message for Supplier Base Amount is null or empty. */
    private String errorSupplierBaseNull;

    /** The error message for Supplier Vat Amount is null or empty. */
    private String errorSupplierVatNull;

    /** The error message for Supplier Total Amount is null or empty. */
    private String errorSupplierTotalNull;
    
    /** The cover page file id. */
    private String coverPageFileId;
    
    /** The ASN detail list. */
    private List<AsnDomain> asnDetailList;
    
    /** The Message when cannot click download. */
    private String cannotDownloadMessage;

    /** The Message when cannot click Calculate CN. */
    private String cannotCalculateCnMessage;
    
    /** The difference dPcd flag. */
    private String diffDPcdFlag;
    
    /** The default constructor. */
    public Winv004Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        this.asnDetailList = new ArrayList<AsnDomain>();
    }
    
    /**
     * <p>Setter method for asn detail.</p>
     * 
     * @param items the new asn detail
     */
    public void setAsnDetail(AsnDomain items) {
        this.asnDetailList.add(items);
    }
    
    /**
     * Gets the asn detail item.
     * 
     * @param index the index
     * @return the asn detail item.
     */
    public AsnDomain getAsnDetail(int index){
        if (asnDetailList == null) {
            asnDetailList = new ArrayList<AsnDomain>();
        }
        while (asnDetailList.size() <= index) {
            asnDetailList.add(new AsnDomain());
        }
        return asnDetailList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    /**
     * <p>Getter method for invoice date from.</p>
     *
     * @return the invoice date from
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }
    
    /**
     * <p>Setter method for invoice date from.</p>
     *
     * @param invoiceDateFrom Set for invoice date to
     */
    public void setInvoiceDateFrom(String invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }
    
    /**
     * <p>Getter method for invoice date to.</p>
     *
     * @return the invoice date to
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }

    /**
     * <p>Setter method for invoice date to.</p>
     *
     * @param invoiceDateTo Set for invoice date to
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }
    
    /**
     * <p>Getter method for cn date to.</p>
     *
     * @return the cn date to
     */
    public String getCnDateTo() {
        return cnDateTo;
    }

    /**
     * <p>Setter method for cn date to.</p>
     *
     * @param cnDateTo Set for cn date to
     */
    public void setCnDateTo(String cnDateTo) {
        this.cnDateTo = cnDateTo;
    }
    
    /**
     * <p>Getter method for invoice status.</p>
     *
     * @return the invoice status
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }
    /**
     * <p>Setter method for invoice status.</p>
     *
     * @param invoiceStatus Set for invoice status
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }
    
    /**
     * <p>Getter method for invoice no criteria.</p>
     *
     * @return the invoice no criteria
     */
    public String getInvoiceNoCriteria() {
        return invoiceNoCriteria;
    }
    /**
     * <p>Setter method for invoice no criteria.</p>
     *
     * @param invoiceNoCriteria Set for invoice no criteria
     */
    public void setInvoiceNoCriteria(String invoiceNoCriteria) {
        this.invoiceNoCriteria = invoiceNoCriteria;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for invoice information list.</p>
     *
     * @return the invoice information list
     */
    public List<InvoiceInformationDomain> getInvoiceInformationForDisplayList() {
        return invoiceInformationForDisplayList;
    }
    
    /**
     * <p>Setter method for invoice information for display list.</p>
     *
     * @param invoiceInformationForDisplayList Set for invoice information for display list
     */
    public void setInvoiceInformationForDisplayList(List<InvoiceInformationDomain> invoiceInformationForDisplayList) {
        this.invoiceInformationForDisplayList = invoiceInformationForDisplayList;
    }
    
    /**
     * <p>Getter method for user DSC ID.</p>
     *
     * @return the user DSC ID
     */
    public String getDscId() {
        return dscId;
    }
    
    /**
     * <p>Setter method for user DSC ID.</p>
     *
     * @param dscId Set for user DSC ID
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    
    /**
     * <p>Getter method for supplier plant tax ID.</p>
     *
     * @return the supplier plant tax ID
     */
    public String getSupplierPlantTaxId() {
        return supplierPlantTaxId;
    }
    
    /**
     * <p>Setter method for supplier plant tax ID.</p>
     *
     * @param supplierPlantTaxId Set for supplier plant tax ID
     */
    public void setSupplierPlantTaxId(String supplierPlantTaxId) {
        this.supplierPlantTaxId = supplierPlantTaxId;
    }
    
    /**
     * <p>Getter method for warning message.</p>
     *
     * @return the warning message
     */
    public String getWarningMessage() {
        return warningMessage;
    }
    
    /**
     * <p>Setter method for warning message.</p>
     *
     * @param warningMessage Set for warning message
     */
    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }
    
    /**
     * <p>Getter method for screen ID.</p>
     *
     * @return the screen ID
     */
    public String getScreenId() {
        return screenId;
    }
    
    /**
     * <p>Setter method for screen ID.</p>
     *
     * @param screenId Set for screen ID
     */
    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }

    /**
     * <p>Getter method for invoiceId.</p>
     *
     * @return the invoiceId
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * <p>Setter method for invoiceId.</p>
     *
     * @param invoiceId Set for invoiceId
     */
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * <p>Getter method for invoiceDate.</p>
     *
     * @return the invoiceDate
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * <p>Setter method for invoiceDate.</p>
     *
     * @param invoiceDate Set for invoiceDate
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * <p>Getter method for cnDate.</p>
     *
     * @return the cnDate
     */
    public String getCnDate() {
        return cnDate;
    }

    /**
     * <p>Setter method for cnDate.</p>
     *
     * @param cnDate Set for cnDate
     */
    public void setCnDate(String cnDate) {
        this.cnDate = cnDate;
    }

    /**
     * <p>Getter method for invoiceRCVStatus.</p>
     *
     * @return the invoiceRCVStatus
     */
    public String getInvoiceRCVStatus() {
        return invoiceRCVStatus;
    }

    /**
     * <p>Setter method for invoiceRCVStatus.</p>
     *
     * @param invoiceRCVStatus Set for invoiceRCVStatus
     */
    public void setInvoiceRCVStatus(String invoiceRCVStatus) {
        this.invoiceRCVStatus = invoiceRCVStatus;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
    
    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    

    /**
     * <p>Getter method for densoName.</p>
     *
     * @return the densoName
     */
    public String getDensoName() {
        return densoName;
    }

    /**
     * <p>Setter method for densoName.</p>
     *
     * @param densoName Set for densoName
     */
    public void setDensoName(String densoName) {
        this.densoName = densoName;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for densoTaxId.</p>
     *
     * @return the densoTaxId
     */
    public String getDensoTaxId() {
        return densoTaxId;
    }

    /**
     * <p>Setter method for densoTaxId.</p>
     *
     * @param densoTaxId Set for densoTaxId
     */
    public void setDensoTaxId(String densoTaxId) {
        this.densoTaxId = densoTaxId;
    }

    /**
     * <p>Getter method for densoAddress.</p>
     *
     * @return the densoAddress
     */
    public String getDensoAddress() {
        return densoAddress;
    }

    /**
     * <p>Setter method for densoAddress.</p>
     *
     * @param densoAddress Set for densoAddress
     */
    public void setDensoAddress(String densoAddress) {
        this.densoAddress = densoAddress;
    }

    /**
     * <p>Getter method for densoCurrency.</p>
     *
     * @return the densoCurrency
     */
    public String getDensoCurrency() {
        return densoCurrency;
    }

    /**
     * <p>Setter method for densoCurrency.</p>
     *
     * @param densoCurrency Set for densoCurrency
     */
    public void setDensoCurrency(String densoCurrency) {
        this.densoCurrency = densoCurrency;
    }

    /**
     * <p>Getter method for supplierName.</p>
     *
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * <p>Setter method for supplierName.</p>
     *
     * @param supplierName Set for supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }

    /**
     * <p>Getter method for supplierAddress.</p>
     *
     * @return the supplierAddress
     */
    public String getSupplierAddress() {
        return supplierAddress;
    }

    /**
     * <p>Setter method for supplierAddress.</p>
     *
     * @param supplierAddress Set for supplierAddress
     */
    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    /**
     * <p>Getter method for supplierAddress1.</p>
     *
     * @return the supplierAddress1
     */
    public String getSupplierAddress1() {
        return supplierAddress1;
    }

    /**
     * <p>Setter method for supplierAddress1.</p>
     *
     * @param supplierAddress1 Set for supplierAddress1
     */
    public void setSupplierAddress1(String supplierAddress1) {
        this.supplierAddress1 = supplierAddress1;
    }

    /**
     * <p>Getter method for supplierAddress2.</p>
     *
     * @return the supplierAddress2
     */
    public String getSupplierAddress2() {
        return supplierAddress2;
    }

    /**
     * <p>Setter method for supplierAddress2.</p>
     *
     * @param supplierAddress2 Set for supplierAddress2
     */
    public void setSupplierAddress2(String supplierAddress2) {
        this.supplierAddress2 = supplierAddress2;
    }

    /**
     * <p>Getter method for supplierAddress3.</p>
     *
     * @return the supplierAddress3
     */
    public String getSupplierAddress3() {
        return supplierAddress3;
    }

    /**
     * <p>Setter method for supplierAddress3.</p>
     *
     * @param supplierAddress3 Set for supplierAddress3
     */
    public void setSupplierAddress3(String supplierAddress3) {
        this.supplierAddress3 = supplierAddress3;
    }

    /**
     * <p>Getter method for supplierCurrency.</p>
     *
     * @return the supplierCurrency
     */
    public String getSupplierCurrency() {
        return supplierCurrency;
    }

    /**
     * <p>Setter method for supplierCurrency.</p>
     *
     * @param supplierCurrency Set for supplierCurrency
     */
    public void setSupplierCurrency(String supplierCurrency) {
        this.supplierCurrency = supplierCurrency;
    }

    /**
     * <p>Getter method for decimalDisp.</p>
     *
     * @return the decimalDisp
     */
    public String getDecimalDisp() {
        return decimalDisp;
    }

    /**
     * <p>Setter method for decimalDisp.</p>
     *
     * @param decimalDisp Set for decimalDisp
     */
    public void setDecimalDisp(String decimalDisp) {
        this.decimalDisp = decimalDisp;
    }

    /**
     * <p>Getter method for vatType.</p>
     *
     * @return the vatType
     */
    public String getVatType() {
        return vatType;
    }

    /**
     * <p>Setter method for vatType.</p>
     *
     * @param vatType Set for vatType
     */
    public void setVatType(String vatType) {
        this.vatType = vatType;
    }
    
    /**
     * <p>Getter method for supplierBaseAmount.</p>
     *
     * @return the supplierBaseAmount
     */
    public String getSupplierBaseAmount() {
        return supplierBaseAmount;
    }

    /**
     * <p>Setter method for supplierBaseAmount.</p>
     *
     * @param supplierBaseAmount Set for supplierBaseAmount
     */
    public void setSupplierBaseAmount(String supplierBaseAmount) {
        this.supplierBaseAmount = supplierBaseAmount;
    }

    /**
     * <p>Getter method for supplierVatAmount.</p>
     *
     * @return the supplierVatAmount
     */
    public String getSupplierVatAmount() {
        return supplierVatAmount;
    }

    /**
     * <p>Setter method for supplierVatAmount.</p>
     *
     * @param supplierVatAmount Set for supplierVatAmount
     */
    public void setSupplierVatAmount(String supplierVatAmount) {
        this.supplierVatAmount = supplierVatAmount;
    }

    /**
     * <p>Getter method for supplierTotalAmount.</p>
     *
     * @return the supplierTotalAmount
     */
    public String getSupplierTotalAmount() {
        return supplierTotalAmount;
    }

    /**
     * <p>Setter method for supplierTotalAmount.</p>
     *
     * @param supplierTotalAmount Set for supplierTotalAmount
     */
    public void setSupplierTotalAmount(String supplierTotalAmount) {
        this.supplierTotalAmount = supplierTotalAmount;
    }

    /**
     * <p>Getter method for cn no.</p>
     *
     * @return the cn no
     */
    public String getCnNo() {
        return cnNo;
    }
    
    /**
     * <p>Setter method for cn no.</p>
     *
     * @param cnNo Set for cn noe
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }
    
    
    /**
     * <p>Getter method for cn date from.</p>
     *
     * @return the cn date from
     */
    public String getCnDateFrom() {
        return cnDateFrom;
    }
    
    /**
     * <p>Setter method for cn date from.</p>
     *
     * @param cnDateFrom Set for cn date to
     */
    public void setCnDateFrom(String cnDateFrom) {
        this.cnDateFrom = cnDateFrom;
    }
    
    /**
     * <p>Getter method for cnBaseAmount.</p>
     *
     * @return the cnBaseAmount
     */
    public String getCnBaseAmount() {
        return cnBaseAmount;
    }

    /**
     * <p>Setter method for cnBaseAmount.</p>
     *
     * @param cnBaseAmount Set for cnBaseAmount
     */
    public void setCnBaseAmount(String cnBaseAmount) {
        this.cnBaseAmount = cnBaseAmount;
    }

    /**
     * <p>Getter method for cnVatAmount.</p>
     *
     * @return the cnVatAmount
     */
    public String getCnVatAmount() {
        return cnVatAmount;
    }

    /**
     * <p>Setter method for cnVatAmount.</p>
     *
     * @param cnVatAmount Set for cnVatAmount
     */
    public void setCnVatAmount(String cnVatAmount) {
        this.cnVatAmount = cnVatAmount;
    }

    /**
     * <p>Getter method for cnTotalalAmount.</p>
     *
     * @return the cnTotalalAmount
     */
    public String getCnTotalAmount() {
        return cnTotalAmount;
    }

    /**
     * <p>Setter method for cnTotalalAmount.</p>
     *
     * @param cnTotalAmount Set for cnTotalAmount
     */
    public void setCnTotalAmount(String cnTotalAmount) {
        this.cnTotalAmount = cnTotalAmount;
    }

    /**
     * <p>Getter method for vatRate.</p>
     *
     * @return the vatRate
     */
    public String getVatRate() {
        return vatRate;
    }

    /**
     * <p>Setter method for vatRate.</p>
     *
     * @param vatRate Set for vatRate
     */
    public void setVatRate(String vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * <p>Getter method for totalDiffBaseAmount.</p>
     *
     * @return the totalDiffBaseAmount
     */
    public String getTotalDiffBaseAmount() {
        return totalDiffBaseAmount;
    }

    /**
     * <p>Setter method for totalDiffBaseAmount.</p>
     *
     * @param totalDiffBaseAmount Set for totalDiffBaseAmount
     */
    public void setTotalDiffBaseAmount(String totalDiffBaseAmount) {
        this.totalDiffBaseAmount = totalDiffBaseAmount;
    }

    /**
     * <p>Getter method for totalDiffVatAmount.</p>
     *
     * @return the totalDiffVatAmount
     */
    public String getTotalDiffVatAmount() {
        return totalDiffVatAmount;
    }

    /**
     * <p>Setter method for totalDiffVatAmount.</p>
     *
     * @param totalDiffVatAmount Set for totalDiffVatAmount
     */
    public void setTotalDiffVatAmount(String totalDiffVatAmount) {
        this.totalDiffVatAmount = totalDiffVatAmount;
    }

    /**
     * <p>Getter method for totalDiffAmount.</p>
     *
     * @return the totalDiffAmount
     */
    public String getTotalDiffAmount() {
        return totalDiffAmount;
    }

    /**
     * <p>Setter method for totalDiffAmount.</p>
     *
     * @param totalDiffAmount Set for totalDiffAmount
     */
    public void setTotalDiffAmount(String totalDiffAmount) {
        this.totalDiffAmount = totalDiffAmount;
    }

    /**
     * <p>Getter method for totalBaseAmount.</p>
     *
     * @return the totalBaseAmount
     */
    public String getTotalBaseAmount() {
        return totalBaseAmount;
    }

    /**
     * <p>Setter method for totalBaseAmount.</p>
     *
     * @param totalBaseAmount Set for totalBaseAmount
     */
    public void setTotalBaseAmount(String totalBaseAmount) {
        this.totalBaseAmount = totalBaseAmount;
    }

    /**
     * <p>Getter method for totalVatAmount.</p>
     *
     * @return the totalVatAmount
     */
    public String getTotalVatAmount() {
        return totalVatAmount;
    }

    /**
     * <p>Setter method for totalVatAmount.</p>
     *
     * @param totalVatAmount Set for totalVatAmount
     */
    public void setTotalVatAmount(String totalVatAmount) {
        this.totalVatAmount = totalVatAmount;
    }

    /**
     * <p>Getter method for totalAmount.</p>
     *
     * @return the totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * <p>Setter method for totalAmount.</p>
     *
     * @param totalAmount Set for totalAmount
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * <p>Getter method for strConfirm.</p>
     *
     * @return the strConfirm
     */
    public String getStrConfirm() {
        return strConfirm;
    }

    /**
     * <p>Setter method for strConfirm.</p>
     *
     * @param strConfirm Set for strConfirm
     */
    public void setStrConfirm(String strConfirm) {
        this.strConfirm = strConfirm;
    }

    /**
     * <p>Getter method for strConfirmReturn.</p>
     *
     * @return the strConfirmReturn
     */
    public String getStrConfirmReturn() {
        return strConfirmReturn;
    }

    /**
     * <p>Setter method for strConfirmReturn.</p>
     *
     * @param strConfirmReturn Set for strConfirmReturn
     */
    public void setStrConfirmReturn(String strConfirmReturn) {
        this.strConfirmReturn = strConfirmReturn;
    }

    /**
     * <p>Getter method for asnDetailList.</p>
     *
     * @return the asnDetailList
     */
    public List<AsnDomain> getAsnDetailList() {
        return asnDetailList;
    }

    /**
     * <p>Setter method for asnDetailList.</p>
     *
     * @param asnDetailList Set for asnDetailList
     */
    public void setAsnDetailList(List<AsnDomain> asnDetailList) {
        this.asnDetailList = asnDetailList;
    }

    /**
     * <p>Getter method for cannotDownloadMessage.</p>
     *
     * @return the cannotDownloadMessage
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * <p>Setter method for cannotDownloadMessage.</p>
     *
     * @param cannotDownloadMessage Set for cannotDownloadMessage
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * <p>Getter method for strConfirmCalculate.</p>
     *
     * @return the strConfirmCalculate
     */
    public String getStrConfirmCalculate() {
        return strConfirmCalculate;
    }

    /**
     * <p>Setter method for strConfirmCalculate.</p>
     *
     * @param strConfirmCalculate Set for strConfirmCalculate
     */
    public void setStrConfirmCalculate(String strConfirmCalculate) {
        this.strConfirmCalculate = strConfirmCalculate;
    }

    /**
     * <p>Getter method for errorSupplierInvoiceAmount.</p>
     *
     * @return the errorSupplierInvoiceAmount
     */
    public String getErrorSupplierInvoiceAmount() {
        return errorSupplierInvoiceAmount;
    }

    /**
     * <p>Setter method for errorSupplierInvoiceAmount.</p>
     *
     * @param errorSupplierInvoiceAmount Set for errorSupplierInvoiceAmount
     */
    public void setErrorSupplierInvoiceAmount(String errorSupplierInvoiceAmount) {
        this.errorSupplierInvoiceAmount = errorSupplierInvoiceAmount;
    }

    /**
     * <p>Getter method for errorSupplierUnitPrice.</p>
     *
     * @return the errorSupplierUnitPrice
     */
    public String getErrorSupplierUnitPrice() {
        return errorSupplierUnitPrice;
    }

    /**
     * <p>Setter method for errorSupplierUnitPrice.</p>
     *
     * @param errorSupplierUnitPrice Set for errorSupplierUnitPrice
     */
    public void setErrorSupplierUnitPrice(String errorSupplierUnitPrice) {
        this.errorSupplierUnitPrice = errorSupplierUnitPrice;
    }

    /**
     * <p>Getter method for errorSupplierBaseNull.</p>
     *
     * @return the errorSupplierBaseNull
     */
    public String getErrorSupplierBaseNull() {
        return errorSupplierBaseNull;
    }

    /**
     * <p>Setter method for errorSupplierBaseNull.</p>
     *
     * @param errorSupplierBaseNull Set for errorSupplierBaseNull
     */
    public void setErrorSupplierBaseNull(String errorSupplierBaseNull) {
        this.errorSupplierBaseNull = errorSupplierBaseNull;
    }

    /**
     * <p>Getter method for errorSupplierVatNull.</p>
     *
     * @return the errorSupplierVatNull
     */
    public String getErrorSupplierVatNull() {
        return errorSupplierVatNull;
    }

    /**
     * <p>Setter method for errorSupplierVatNull.</p>
     *
     * @param errorSupplierVatNull Set for errorSupplierVatNull
     */
    public void setErrorSupplierVatNull(String errorSupplierVatNull) {
        this.errorSupplierVatNull = errorSupplierVatNull;
    }

    /**
     * <p>Getter method for errorSupplierTotalNull.</p>
     *
     * @return the errorSupplierTotalNull
     */
    public String getErrorSupplierTotalNull() {
        return errorSupplierTotalNull;
    }

    /**
     * <p>Setter method for errorSupplierTotalNull.</p>
     *
     * @param errorSupplierTotalNull Set for errorSupplierTotalNull
     */
    public void setErrorSupplierTotalNull(String errorSupplierTotalNull) {
        this.errorSupplierTotalNull = errorSupplierTotalNull;
    }

    /**
     * <p>Getter method for coverPageFileId.</p>
     *
     * @return the coverPageFileId
     */
    public String getCoverPageFileId() {
        return coverPageFileId;
    }

    /**
     * <p>Setter method for coverPageFileId.</p>
     *
     * @param coverPageFileId Set for coverPageFileId
     */
    public void setCoverPageFileId(String coverPageFileId) {
        this.coverPageFileId = coverPageFileId;
    }

    /**
     * <p>Getter method for cannotCalculateCnMessage.</p>
     *
     * @return the cannotCalculateCnMessage
     */
    public String getCannotCalculateCnMessage() {
        return cannotCalculateCnMessage;
    }

    /**
     * <p>Setter method for cannotCalculateCnMessage.</p>
     *
     * @param cannotCalculateCnMessage Set for cannotCalculateCnMessage
     */
    public void setCannotCalculateCnMessage(String cannotCalculateCnMessage) {
        this.cannotCalculateCnMessage = cannotCalculateCnMessage;
    }
    
    /**
     * <p>Getter method for diffDPcdFlag.</p>
     *
     * @return the diffDPcdFlag
     */
    public String getDiffDPcdFlag() {
        return diffDPcdFlag;
    }

    /**
     * <p>Setter method for diffDPcdFlag.</p>
     *
     * @param diffDPcdFlag Set for diffDPcdFlag
     */
    public void setDiffDPcdFlag(String diffDPcdFlag) {
        this.diffDPcdFlag = diffDPcdFlag;
    }

    // Start : [IN059] Getter and Setter for defaultVatRate
    /**
     * <p>Getter method for defaultVatRate.</p>
     *
     * @return the defaultVatRate
     */
    public String getDefaultVatRate() {
        return defaultVatRate;
    }

    /**
     * <p>Setter method for defaultVatRate.</p>
     *
     * @param defaultVatRate Set for defaultVatRate
     */
    public void setDefaultVatRate(String defaultVatRate) {
        this.defaultVatRate = defaultVatRate;
    }
    // End : [IN059] Getter and Setter for defaultVatRate
}
