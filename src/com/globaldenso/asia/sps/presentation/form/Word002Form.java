/*
 * ModifyDate Development company     Describe 
 * 2014/03/03 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WORD002Form.
 * @author CSI
 */
public class Word002Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The Supplier Company code. */
    private String sCd;
    
    /** The Vendor Code. */
    private String vendorCd;
       
    /** The Denso Company Code. */
    private String dCd;
   
    /** The SPS PO number. */
    private String spsPoNo;
    
    /** The CIGMA PO */
    private String cigmaPoNo;
    
    /** The Purchase Order ID. */
    private String poId;
    
    /** The Supplier Plant code. */
    private String sPcd;
    
    /** The Denso Plant code. */
    private String dPcd;
    
    /** The issued date from. */
    private String issuedDateFrom;
    
    /** The issued date to. */
    private String issuedDateTo;
    
    /** The Purchase Order Status. */
    private String poStatus;
    
    /** The Purchase Order Type. */
    private String poType;
    
    /** The Period Type. */
    private String periodType;
    
    /** The Period Type Name. */
    private String periodTypeName;
    
    /** The View PDF. */
    private String viewPdf;
    
    /** The PDF Original. */
    private String pdfOriginal;
    
    /** The PDF Change. */
    private String pdfChange;
      
    /** The List of PO Status */
    private List<MiscellaneousDomain> poStatusList;
    
    /** The List of PO Type */
    private List<MiscellaneousDomain> poTypeList;
    
    /** The List of Period Type */
    private List<MiscellaneousDomain> periodTypeList;
    
    /** The List of View PDF */
    private List<MiscellaneousDomain> viewPdfList;

    /** The List of Company Supplier. */
    private List<CompanySupplierDomain> companySupplierList;

    /** The List of Company DENSO. */
    private List<CompanyDensoDomain> companyDensoList;

    /** The List of Plant DENSO. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of Plant Supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of PO domain. */
    private List<PurchaseOrderInformationDomain> poList;
    

    /** The purchase order status Selected*/
    private String poStatusSelected;
    
    /** The purchase order ID Selected*/
    private String poIdSelected;

    /** The file Id that user selected from screen. */
    private String fileIdSelected;
    
    /** PO type of clicked record. */
    private String clickedPoType;

    /** Period type of clicked record. */
    private String clickedPeriodType;

    /** PO type of clicked record. */
    private String clickedSpsPoNo;
    
    /** Error message if user cannot click Reset button. */
    private String cannotResetMessage;
    
    /** Error message if user cannot click Download CSV. */
    private String cannotDownloadCsvMessage;
    
    /** The Data Type. */
    private String dataType;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
//    /** Last update datetime Selected*/
//    private Timestamp lastUpdateDatetimeSelected;
    
    /** The default Constructor */
    public Word002Form() {
        this.poList = new ArrayList<PurchaseOrderInformationDomain>();
        this.poStatusList = new ArrayList<MiscellaneousDomain>();
        this.poTypeList = new ArrayList<MiscellaneousDomain>();
        this.periodTypeList = new ArrayList<MiscellaneousDomain>();
        this.viewPdfList = new ArrayList<MiscellaneousDomain>();
        this.companySupplierList = new ArrayList<CompanySupplierDomain>();
        this.companyDensoList = new ArrayList<CompanyDensoDomain>();
        this.plantDensoList = new ArrayList<PlantDensoDomain>();
        this.plantSupplierList = new ArrayList<PlantSupplierDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        sCd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        spsPoNo = Constants.EMPTY_STRING;
        cigmaPoNo = Constants.EMPTY_STRING;
        issuedDateFrom = Constants.EMPTY_STRING;
        issuedDateTo = Constants.EMPTY_STRING;
        poStatus = Constants.EMPTY_STRING;
        poType = Constants.EMPTY_STRING;
        periodType = Constants.EMPTY_STRING;
        viewPdf = Constants.EMPTY_STRING;
        dataType = Constants.EMPTY_STRING;
        
        poList = new ArrayList<PurchaseOrderInformationDomain>();
    }
    /**
     * Sets the Purchase Order item.
     * 
     * @param items the new Purchase Order item
     */
    public void setPo(PurchaseOrderInformationDomain items) {
        this.poList.add(items);
    }
    
    /**
     * Gets the Purchase Order item.
     * 
     * @param index the index
     * @return the Purchase Order item.
     */
    public PurchaseOrderInformationDomain getPo(int index){
        if (poList == null) {
            poList = new ArrayList<PurchaseOrderInformationDomain>();
        }
        while (poList.size() <= index) {
            poList.add(new PurchaseOrderInformationDomain());
        }
        return poList.get(index);
    }
    
    /**
     * Gets the supplier Company code
     * 
     * @return the supplier Company code
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the supplier Company code
     * 
     * @param sCd the supplier Company code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Gets the denso Company code
     * 
     * @return the denso Company code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso Company code
     * 
     * @param dCd the denso Company code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the SPS Purchase Order number.
     * 
     * @return the SPS Purchase Order number.
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }
    /**
     * Sets the SPS Purchase Order number.
     * 
     * @param spsPoNo the SPS Purchase Order number.
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }

    /**
     * Gets the cigma purchase order number.
     * 
     * @return the cigma purchase order number.
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * Sets the cigma purchase order number.
     * 
     * @param cigmaPoNo the cigma purchase order number.
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso plant code
     * 
     * @return the denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the denso plant code
     * 
     * @param dPcd the denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the issue date.  
     * 
     * @return the issue date.  
     */
    public String getIssuedDateFrom() {
        return issuedDateFrom;
    }
    /**
     * Sets the issue date.  
     * 
     * @param issuedDateFrom the issue date.  
     */
    public void setIssuedDateFrom(String issuedDateFrom) {
        this.issuedDateFrom = issuedDateFrom;
    }

    /**
     * Gets the issue date.  
     * 
     * @return the issue date.  
     */
    public String getIssuedDateTo() {
        return issuedDateTo;
    }
    /**
     * Sets the issue date.  
     * 
     * @param issuedDateTo the issue date.  
     */
    public void setIssuedDateTo(String issuedDateTo) {
        this.issuedDateTo = issuedDateTo;
    }

    /**
     * Gets the list of purchase order status combo box.
     * 
     * @return the list of purchase order status combo box.
     */
    public List<MiscellaneousDomain> getPoStatusList() {
        return poStatusList;
    }
    /**
     * Sets the list of purchase order status combo box.
     * 
     * @param poStatusList the list of purchase order status combo box.
     */
    public void setPoStatusList(List<MiscellaneousDomain> poStatusList) {
        this.poStatusList = poStatusList;
    }

    /**
     * Gets the list of Purchase order type combo box.
     * 
     * @return the list of Purchase order type combo box.
     */
    public List<MiscellaneousDomain> getPoTypeList() {
        return poTypeList;
    }
    /**
     * Sets the list of Purchase order type combo box.
     * 
     * @param poTypeList the list of Purchase order type combo box.
     */
    public void setPoTypeList(List<MiscellaneousDomain> poTypeList) {
        this.poTypeList = poTypeList;
    }

    /**
     * Gets the list of period type combo box.
     * 
     * @return the list of period type combo box.
     */
    public List<MiscellaneousDomain> getPeriodTypeList() {
        return periodTypeList;
    }
    /**
     * Sets the list of period type combo box.
     * 
     * @param periodTypeList the list of period type combo box.
     */
    public void setPeriodTypeList(List<MiscellaneousDomain> periodTypeList) {
        this.periodTypeList = periodTypeList;
    }

    /**
     * Gets the list of View PDF combo box.
     * 
     * @return the list of View PDF combo box.
     */
    public List<MiscellaneousDomain> getViewPdfList() {
        return viewPdfList;
    }
    /**
     * Sets the list of View PDF combo box.
     * 
     * @param viewPdfList the list of View PDF combo box.
     */
    public void setViewPdfList(List<MiscellaneousDomain> viewPdfList) {
        this.viewPdfList = viewPdfList;
    }

    /**
     * Gets the list of purchase order.
     * 
     * @return the list of purchase order.
     */
    public List<PurchaseOrderInformationDomain> getPoList() {
        return poList;
    }
    /**
     * Sets the list of purchase order.
     * 
     * @param poList the list of purchase order.
     */
    public void setPoList(List<PurchaseOrderInformationDomain> poList) {
        this.poList = poList;
    }
    /**
     * Gets the purchase order status.
     * 
     * @return the purchase order status.
     */
    public String getPoStatus() {
        return poStatus;
    }
    /**
     * Sets the purchase order status.
     * 
     * @param poStatus the purchase order status.
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }
    /**
     * Gets the purchase order type.
     * 
     * @return the purchase order type.
     */
    public String getPoType() {
        return poType;
    }
    /**
     * Sets the purchase order type.
     * 
     * @param poType the purchase order type.
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }
    /**
     * Gets the period Type.
     * 
     * @return the period Type.
     */
    public String getPeriodType() {
        return periodType;
    }
    /**
     * Sets the period Type.
     * 
     * @param periodType the period Type.
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }
    /**
     * Gets the View PDF flag.
     * 
     * @return the View PDF flag.
     */
    public String getViewPdf() {
        return viewPdf;
    }
    /**
     * Sets the View PDF flag.
     * 
     * @param viewPdf the View PDF flag.
     */
    public void setViewPdf(String viewPdf) {
        this.viewPdf = viewPdf;
    }
    /**
     * Gets the PDF Original Flag.
     * 
     * @return the PDF Original Flag.
     */
    public String getPdfOriginal() {
        return pdfOriginal;
    }
    /**
     * Sets the PDF Original Flag.
     * 
     * @param pdfOriginal the PDF Original Flag.
     */
    public void setPdfOriginal(String pdfOriginal) {
        this.pdfOriginal = pdfOriginal;
    }
    /**
     * Gets the PDF Change Flag.
     * 
     * @return the PDF Change Flag.
     */
    public String getPdfChange() {
        return pdfChange;
    }
    /**
     * Sets the PDF Change Flag.
     * 
     * @param pdfChange the PDF Change Flag.
     */
    public void setPdfChange(String pdfChange) {
        this.pdfChange = pdfChange;
    }
    /**
     * Gets the Purchase Order ID.
     * 
     * @return the Purchase Order ID.
     */
    public String getPoId() {
        return poId;
    }
    /**
     * Sets the Purchase Order ID.
     * 
     * @param poId the Purchase Order ID.
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }
    /**
     * Gets the Period Type Name.
     * 
     * @return the Period Type Name.
     */
    public String getPeriodTypeName() {
        return periodTypeName;
    }
    /**
     * Sets the Period Type Name.
     * 
     * @param periodTypeName the Period Type Name.
     */
    public void setPeriodTypeName(String periodTypeName) {
        this.periodTypeName = periodTypeName;
    }
    
    /**
     * Gets the purchase order status selected.
     * 
     * @return the purchase order status selected.
     */
    public String getPoStatusSelected() {
        return poStatusSelected;
    }
    /**
     * Sets the purchase order status selected.
     * 
     * @param poStatusSelected the purchase order status selected.
     */
    public void setPoStatusSelected(String poStatusSelected) {
        this.poStatusSelected = poStatusSelected;
    }
    /**
     * Gets the Purchase order ID selected.
     * 
     * @return the Purchase order ID selected.
     */
    public String getPoIdSelected() {
        return poIdSelected;
    }
    /**
     * Sets the Purchase order ID selected.
     * 
     * @param poIdSelected the Purchase order ID selected.
     */
    public void setPoIdSelected(String poIdSelected) {
        this.poIdSelected = poIdSelected;
    }
//    /**
//     * Gets the Update datetime selected.
//     * 
//     * @return the Update datetime selected.
//     */
//    public Timestamp getLastUpdateDatetimeSelected() {
//        return lastUpdateDatetimeSelected;
//    }
//    /**
//     * Sets the Update datetime selected.
//     * 
//     * @param lastUpdateDatetimeSelected the Update datetime selected.
//     */
//    public void setLastUpdateDatetimeSelected(Timestamp lastUpdateDatetimeSelected) {
//        this.lastUpdateDatetimeSelected = lastUpdateDatetimeSelected;
//    }

    /**
     * Get method for companySupplierList.
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Set method for companySupplierList.
     * @param companySupplierList the companySupplierList to set
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Get method for companyDensoList.
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Set method for companyDensoList.
     * @param companyDensoList the companyDensoList to set
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for fileIdSelected.</p>
     *
     * @return the fileIdSelected
     */
    public String getFileIdSelected() {
        return fileIdSelected;
    }

    /**
     * <p>Setter method for fileIdSelected.</p>
     *
     * @param fileIdSelected Set for fileIdSelected
     */
    public void setFileIdSelected(String fileIdSelected) {
        this.fileIdSelected = fileIdSelected;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * Sets the po status.
     * 
     * @param miscDomain the new po status.
     */
    public void setPoStatusItem(MiscellaneousDomain miscDomain) {
        this.poStatusList.add(miscDomain);
    }

    /**
     * Gets the po status.
     * 
     * @param index the index
     * @return the po status.
     */
    public MiscellaneousDomain getPoStatusItem(int index) {
        if (poStatusList == null) {
            poStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while (poStatusList.size() <= index) {
            poStatusList.add(new MiscellaneousDomain());
        }
        return poStatusList.get(index);
    }

    /**
     * Sets the po type.
     * 
     * @param miscDomain the new po type.
     */
    public void setPoTypeItem(MiscellaneousDomain miscDomain) {
        this.poTypeList.add(miscDomain);
    }
    
    /**
     * Gets the po type.
     * 
     * @param index the index
     * @return the po type.
     */
    public MiscellaneousDomain getPoTypeItem(int index) {
        if (poTypeList == null) {
            poTypeList = new ArrayList<MiscellaneousDomain>();
        }
        while (poTypeList.size() <= index) {
            poTypeList.add(new MiscellaneousDomain());
        }
        return poTypeList.get(index);
    }

    /**
     * Sets the period type.
     * 
     * @param miscDomain the new period type.
     */
    public void setPeriodTypeItem(MiscellaneousDomain miscDomain) {
        this.periodTypeList.add(miscDomain);
    }
    
    /**
     * Gets the period type.
     * 
     * @param index the index
     * @return the period type.
     */
    public MiscellaneousDomain getPeriodTypeItem(int index) {
        if (periodTypeList == null) {
            periodTypeList = new ArrayList<MiscellaneousDomain>();
        }
        while (periodTypeList.size() <= index) {
            periodTypeList.add(new MiscellaneousDomain());
        }
        return periodTypeList.get(index);
    }

    /**
     * Sets the view PDF
     * 
     * @param miscDomain the new  view PDF.
     */
    public void setViewPdfItem(MiscellaneousDomain miscDomain) {
        this.viewPdfList.add(miscDomain);
    }
    
    /**
     * Gets the  view PDF.
     * 
     * @param index the index
     * @return the  view PDF.
     */
    public MiscellaneousDomain getViewPdfItem(int index) {
        if (viewPdfList == null) {
            viewPdfList = new ArrayList<MiscellaneousDomain>();
        }
        while (viewPdfList.size() <= index) {
            viewPdfList.add(new MiscellaneousDomain());
        }
        return viewPdfList.get(index);
    }

    /**
     * Sets the company supplier.
     * 
     * @param companySupplier the new company supplier.
     */
    public void setCompanySupplierItem(CompanySupplierDomain companySupplier) {
        this.companySupplierList.add(companySupplier);
    }
    
    /**
     * Gets the company supplier.
     * 
     * @param index the index
     * @return the company supplier.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index) {
        if (companySupplierList == null) {
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while (companySupplierList.size() <= index) {
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }

    /**
     * Sets the company DENSO.
     * 
     * @param companyDenso the new company DENSO.
     */
    public void setCompanyDensoItem(CompanyDensoDomain companyDenso) {
        this.companyDensoList.add(companyDenso);
    }
    
    /**
     * Gets the company DENSO.
     * 
     * @param index the index
     * @return the company DENSO.
     */
    public CompanyDensoDomain getCompanyDensoItem(int index) {
        if (companyDensoList == null) {
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while (companyDensoList.size() <= index) {
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }

    /**
     * Sets the Plant DENSO.
     * 
     * @param plantDenso the new Plant DENSO.
     */
    public void setPlantDensoItem(PlantDensoDomain plantDenso) {
        this.plantDensoList.add(plantDenso);
    }
    
    /**
     * Gets the Plant DENSO.
     * 
     * @param index the index
     * @return the Plant DENSO.
     */
    public PlantDensoDomain getPlantDensoItem(int index) {
        if (plantDensoList == null) {
            plantDensoList = new ArrayList<PlantDensoDomain>();
        }
        while (plantDensoList.size() <= index) {
            plantDensoList.add(new PlantDensoDomain());
        }
        return plantDensoList.get(index);
    }

    /**
     * Sets the Plant Supplier.
     * 
     * @param plantSupplier the new Plant Supplier.
     */
    public void setPlantSupplierItem(PlantSupplierDomain plantSupplier) {
        this.plantSupplierList.add(plantSupplier);
    }
    
    /**
     * Gets the Plant Supplier.
     * 
     * @param index the index
     * @return the Plant Supplier.
     */
    public PlantSupplierDomain getPlantSupplierItem(int index) {
        if (plantSupplierList == null) {
            plantSupplierList = new ArrayList<PlantSupplierDomain>();
        }
        while (plantSupplierList.size() <= index) {
            plantSupplierList.add(new PlantSupplierDomain());
        }
        return plantSupplierList.get(index);
    }

    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
    /**
     * <p>Getter method for clickedPoType.</p>
     *
     * @return the clickedPoType
     */
    public String getClickedPoType() {
        return clickedPoType;
    }

    /**
     * <p>Setter method for clickedPoType.</p>
     *
     * @param clickedPoType Set for clickedPoType
     */
    public void setClickedPoType(String clickedPoType) {
        this.clickedPoType = clickedPoType;
    }

    /**
     * <p>Getter method for clickedPeriodType.</p>
     *
     * @return the clickedPeriodType
     */
    public String getClickedPeriodType() {
        return clickedPeriodType;
    }

    /**
     * <p>Setter method for clickedPeriodType.</p>
     *
     * @param clickedPeriodType Set for clickedPeriodType
     */
    public void setClickedPeriodType(String clickedPeriodType) {
        this.clickedPeriodType = clickedPeriodType;
    }

    /**
     * <p>Getter method for clickedSpsPoNo.</p>
     *
     * @return the clickedSpsPoNo
     */
    public String getClickedSpsPoNo() {
        return clickedSpsPoNo;
    }

    /**
     * <p>Setter method for clickedSpsPoNo.</p>
     *
     * @param clickedSpsPoNo Set for clickedSpsPoNo
     */
    public void setClickedSpsPoNo(String clickedSpsPoNo) {
        this.clickedSpsPoNo = clickedSpsPoNo;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadCsvMessage.</p>
     *
     * @return the cannotDownloadCsvMessage
     */
    public String getCannotDownloadCsvMessage() {
        return cannotDownloadCsvMessage;
    }

    /**
     * <p>Setter method for cannotDownloadCsvMessage.</p>
     *
     * @param cannotDownloadCsvMessage Set for cannotDownloadCsvMessage
     */
    public void setCannotDownloadCsvMessage(String cannotDownloadCsvMessage) {
        this.cannotDownloadCsvMessage = cannotDownloadCsvMessage;
    }

    /**
     * <p>Getter method for dataType.</p>
     *
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * <p>Setter method for dataType.</p>
     *
     * @param dataType Set for dataType
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

}