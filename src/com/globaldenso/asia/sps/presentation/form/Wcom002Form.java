/*
 * ModifyDate Development company     Describe 
 * 2014/07/02 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAnnounceMessageDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.business.domain.AnnounceMessageDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDetailDomain;

/**
 * <p>
 * Action Form for Welcome screen.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class Wcom002Form extends CoreActionForm {

    /** The generated serial version UID. */
    private static final long serialVersionUID = 2068424456910948844L;

    /** The DSC_ID. */
    private String dscId;

    /** The list of menu that menu that user has permission. */
    private List<SpsMMenuDomain> spsMMenuDomainlist;

    /** The size of menu list. */
    private int menuListSize;

    /** The Announce Message Domain List. */
    private List<SpsMAnnounceMessageDomain> spsMAnnounceMessageDomainList;

    // FIX : wrong dateformat
    /** To format effect start date and show in screen */
    private List<AnnounceMessageDomain> announceMessageDomainList;
    
    /** The user type. */
    private String userType;
    
    /** The Language. */
    private String lang;
    
    /**
     * <p>
     * Main Screen Result Domain List.
     * </p>
     */
    private List<MainScreenResultDomain> mainScreenResultDomain;

    /**
     * <p>
     * Task List Detail Domain List.
     * </p>
     */
    private List<TaskListDetailDomain> taskListDetailDomainList;

    /** The default constructor. */
    public Wcom002Form() {

    }

    /**
     * <p>
     * Getter method for dscId.
     * </p>
     * 
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>
     * Setter method for dscId.
     * </p>
     * 
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>
     * Getter method for spsMMenuDomainlist.
     * </p>
     * 
     * @return the spsMMenuDomainlist
     */
    public List<SpsMMenuDomain> getSpsMMenuDomainlist() {
        return spsMMenuDomainlist;
    }

    /**
     * <p>
     * Setter method for spsMMenuDomainlist.
     * </p>
     * 
     * @param spsMMenuDomainlist Set for spsMMenuDomainlist
     */
    public void setSpsMMenuDomainlist(List<SpsMMenuDomain> spsMMenuDomainlist) {
        this.spsMMenuDomainlist = spsMMenuDomainlist;
    }

    /**
     * <p>
     * Getter method for menuListSize.
     * </p>
     * 
     * @return the menuListSize
     */
    public int getMenuListSize() {
        return menuListSize;
    }

    /**
     * <p>
     * Setter method for menuListSize.
     * </p>
     * 
     * @param menuListSize Set for menuListSize
     */
    public void setMenuListSize(int menuListSize) {
        this.menuListSize = menuListSize;
    }

    /**
     * <p>
     * Getter method for spsMAnnounceMessageDomainList.
     * </p>
     * 
     * @return the spsMAnnounceMessageDomainList
     */
    public List<SpsMAnnounceMessageDomain> getSpsMAnnounceMessageDomainList() {
        return spsMAnnounceMessageDomainList;
    }

    /**
     * <p>
     * Setter method for spsMAnnounceMessageDomainList.
     * </p>
     * 
     * @param spsMAnnounceMessageDomainList Set for
     *            spsMAnnounceMessageDomainList
     */
    public void setSpsMAnnounceMessageDomainList(
        List<SpsMAnnounceMessageDomain> spsMAnnounceMessageDomainList) {
        this.spsMAnnounceMessageDomainList = spsMAnnounceMessageDomainList;
    }

    /**
     * <p>
     * Getter method for userType.
     * </p>
     * 
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * <p>
     * Setter method for userType.
     * </p>
     * 
     * @param userType Set for userType
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * <p>
     * Getter method for mainScreenResultDomain.
     * </p>
     * 
     * @return the mainScreenResultDomain
     */
    public List<MainScreenResultDomain> getMainScreenResultDomain() {
        return mainScreenResultDomain;
    }

    /**
     * <p>
     * Setter method for mainScreenResultDomain.
     * </p>
     * 
     * @param mainScreenResultDomain Set for mainScreenResultDomain
     */
    public void setMainScreenResultDomain(
        List<MainScreenResultDomain> mainScreenResultDomain) {
        this.mainScreenResultDomain = mainScreenResultDomain;
    }

    /**
     * <p>
     * Getter method for taskListDetailDomainList.
     * </p>
     * 
     * @return the taskListDetailDomainList
     */
    public List<TaskListDetailDomain> getTaskListDetailDomainList() {
        return taskListDetailDomainList;
    }

    /**
     * <p>
     * Setter method for taskListDetailDomainList.
     * </p>
     * 
     * @param taskListDetailDomainList Set for taskListDetailDomainList
     */
    public void setTaskListDetailDomainList(
        List<TaskListDetailDomain> taskListDetailDomainList) {
        this.taskListDetailDomainList = taskListDetailDomainList;
    }

    /**
     * <p>Getter method for lang.</p>
     *
     * @return the lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * <p>Setter method for lang.</p>
     *
     * @param lang Set for lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * <p>Getter method for announceMessageDomainList.</p>
     *
     * @return the announceMessageDomainList
     */
    public List<AnnounceMessageDomain> getAnnounceMessageDomainList() {
        return announceMessageDomainList;
    }

    /**
     * <p>Setter method for announceMessageDomainList.</p>
     *
     * @param announceMessageDomainList Set for announceMessageDomainList
     */
    public void setAnnounceMessageDomainList(
        List<AnnounceMessageDomain> announceMessageDomainList) {
        this.announceMessageDomainList = announceMessageDomainList;
    }

}
