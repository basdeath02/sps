/*
 * ModifyDate Development company     Describe 
 * 2014/06/07 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WADM002Form.
 * @author CSI
 */
public class Wadm002Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The Company Supplier Code. */
    private String sCd;
   
    /** The Plant Supplier. */
    private String sPcd;
    
    /** The Vendor Code. */
    private String vendorCd;
    
    /** The first name. */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The DENSO owner. */
    private String densoOwner;
    
    /** The Effective start. */
    private String effectiveStart;
    
    /** The Effective end. */
    private String effectiveEnd;
    
    /** The department. */
    private String departmentName; 
    
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The Mode. */
    private String mode;
    
    /** The reset flag. */
    private Boolean isReset = false;
    
    /** The Email urgent order flag. */
    private String emlUrgentFlag;
    
    /** The email supplier info not found.*/
    private String emlInfoFlag;
    
    /** The email allow revise flag.*/
    private String emlAllowReviseFlag;
    
    /** The email cancel invoice flag.*/
    private String emlCancelInvoiceFlag;
    
    /** The company supplier code criteria. */
    private String sCdCriteria;
    
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The List of Company Supplier. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The List of plant supplier. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The List of Vendor Code. */
    private List<SpsMAs400VendorDomain> vendorCdList;
    
    /** The List of company DENSO code. */
    private List<CompanyDensoDomain> dCdList;
    
    /** The max of record vendor code. */
    private int maxVendorCd;
    
    /** The max of record department. */
    private int maxDepartment;
    
    /** Create DSC ID */
    private String createDscId;
    
    /** The isDcompany flag. */
    private Boolean isDCompany;
    
    /** The create date time. */
    private Timestamp createDatetime;
    
    /** The last update dsc id. */
    private String lastUpdateDscId;
    
    /** The last update date time user. */
    private String lastUpdateDatetimeUser;
    
    /** The last update date time supplier. */
    private String lastUpdateDatetimeSupplier;
    
    /** Error message if user cannot click Reset button. */
    private String cannotResetMessage;
    
    /** User domain for keep department. */
    private List<SpsMUserDomain> departmentList;
    
    /** The default Constructor */
    public Wadm002Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        sCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        email = Constants.EMPTY_STRING;
        densoOwner = Constants.EMPTY_STRING;
        telephone = Constants.EMPTY_STRING;
        firstName = Constants.EMPTY_STRING;
        middleName = Constants.EMPTY_STRING;
        lastName = Constants.EMPTY_STRING;
        effectiveStart = Constants.EMPTY_STRING;
        effectiveEnd = Constants.EMPTY_STRING;
        emlUrgentFlag = Constants.STR_ZERO;
        emlInfoFlag = Constants.STR_ZERO;
        emlAllowReviseFlag = Constants.STR_ZERO;
        emlCancelInvoiceFlag = Constants.STR_ZERO;
        dscId = Constants.EMPTY_STRING;
        departmentName = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        companySupplierList  = new ArrayList<CompanySupplierDomain>();
        dCdList = new ArrayList<CompanyDensoDomain>();
        plantSupplierList = new ArrayList<PlantSupplierDomain>();
        vendorCdList = new ArrayList<SpsMAs400VendorDomain>();
        departmentList = new ArrayList<SpsMUserDomain>();
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode(Register & Edit).
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode(Register & Edit).
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the DENSO Owner.
     * 
     * @return the DENSO Owner.
     */
    public String getDensoOwner() {
        return densoOwner;
    }

    /**
     * Sets the DENSO Owner.
     * 
     * @param densoOwner the DENSO Owner.
     */
    public void setDensoOwner(String densoOwner) {
        this.densoOwner = densoOwner;
    }

    /**
     * Gets the Effective Start.
     * 
     * @return the Effective Start.
     */
    public String getEffectiveStart() {
        return effectiveStart;
    }

    /**
     * Sets the Effective Start.
     * 
     * @param effectiveStart the Effective Start.
     */
    public void setEffectiveStart(String effectiveStart) {
        this.effectiveStart = effectiveStart;
    }

    /**
     * Gets the Effective End.
     * 
     * @return the Effective End.
     */
    public String getEffectiveEnd() {
        return effectiveEnd;
    }

    /**
     * Sets the Effective End.
     * 
     * @param effectiveEnd the Effective End.
     */
    public void setEffectiveEnd(String effectiveEnd) {
        this.effectiveEnd = effectiveEnd;
    }

    /**
     * Gets the Reset flag.
     * 
     * @return the Reset flag.
     */
    public Boolean getIsReset() {
        return isReset;
    }

    /**
     * Sets the Reset flag.
     * 
     * @param isReset the Reset flag.
     */
    public void setIsReset(Boolean isReset) {
        this.isReset = isReset;
    }

    /**
     * Gets the email urgent Flag.
     * 
     * @return the email urgent Flag.
     */
    public String getEmlUrgentFlag() {
        return emlUrgentFlag;
    }

    /**
     * Sets the email urgent Flag.
     * 
     * @param emlUrgentFlag the email urgent Flag.
     */
    public void setEmlUrgentFlag(String emlUrgentFlag) {
        this.emlUrgentFlag = emlUrgentFlag;
    }

    /**
     * Gets the email supplier information not found Flag.
     * 
     * @return the email supplier information not found Flag.
     */
    public String getEmlInfoFlag() {
        return emlInfoFlag;
    }

    /**
     * Sets the email supplier information not found Flag.
     * 
     * @param emlInfoFlag the email supplier information not found Flag.
     */
    public void setEmlInfoFlag(String emlInfoFlag) {
        this.emlInfoFlag = emlInfoFlag;
    }
    
    /**
     * Gets the emlAllowReviseFlag.
     * 
     * @return the emlAllowReviseFlag
     */
    public String getEmlAllowReviseFlag() {
        return emlAllowReviseFlag;
    }
    
    /**
     * Sets the emlAllowReviseFlag.
     * 
     * @param emlAllowReviseFlag the emlAllowReviseFlag
     */
    public void setEmlAllowReviseFlag(String emlAllowReviseFlag) {
        this.emlAllowReviseFlag = emlAllowReviseFlag;
    }
    
    /**
     * Gets the emlCancelInvoiceFlag.
     * 
     * @return the emlCancelInvoiceFlag
     */
    public String getEmlCancelInvoiceFlag() {
        return emlCancelInvoiceFlag;
    }

    /**
     * Sets the emlCancelInvoiceFlag.
     * 
     * @param emlCancelInvoiceFlag the emlCancelInvoiceFlag
     */
    public void setEmlCancelInvoiceFlag(String emlCancelInvoiceFlag) {
        this.emlCancelInvoiceFlag = emlCancelInvoiceFlag;
    }
    
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    
    /**
     * Gets the Company supplier Code.
     * 
     * @return the Company supplier Code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Sets the Company supplier Code.
     * 
     * @param sCd the Company supplier Code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Gets the plant supplier.
     * 
     * @return the plant supplier.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the plant supplier.
     * 
     * @param sPcd the plant supplier.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    /**
     * Gets the department name.
     * 
     * @return the department name.
     */
    public String getDepartmentName() {
        return departmentName;
    }
    /**
     * Sets the department name.
     * 
     * @param departmentName the department name.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    /**
     * Gets the list of plant supplier.
     * 
     * @return the list of plant supplier.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the list of plant supplier.
     * 
     * @param plantSupplierList the list of plant supplier.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    
    /**
     * Getter method of "createDscId"
     * 
     * @return the createDscId
     */
    public String getCreateDscId() {
        return createDscId;
    }

    /**
     * Setter method of "createDscId"
     * 
     * @param createDscId Set in "createDscId".
     */
    public void setCreateDscId(String createDscId) {
        this.createDscId = createDscId;
    }

    /**
     * Getter method of "createDatetime"
     * 
     * @return the createDatetime
     */
    public Timestamp getCreateDatetime() {
        return createDatetime;
    }

    /**
     * Setter method of "createDatetime"
     * 
     * @param createDatetime Set in "createDatetime".
     */
    public void setCreateDatetime(Timestamp createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * Getter method of "lastUpdateDscId"
     * 
     * @return the lastUpdateDscId
     */
    public String getLastUpdateDscId() {
        return lastUpdateDscId;
    }

    /**
     * Setter method of "lastUpdateDscId"
     * 
     * @param lastUpdateDscId Set in "lastUpdateDscId".
     */
    public void setLastUpdateDscId(String lastUpdateDscId) {
        this.lastUpdateDscId = lastUpdateDscId;
    }

    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public String getLastUpdateDatetimeUser() {
        return lastUpdateDatetimeUser;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetimeUser Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetimeUser(String lastUpdateDatetimeUser) {
        this.lastUpdateDatetimeUser = lastUpdateDatetimeUser;
    }
    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public String getLastUpdateDatetimeSupplier() {
        return lastUpdateDatetimeSupplier;
    }
    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetimeSupplier Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetimeSupplier(String lastUpdateDatetimeSupplier) {
        this.lastUpdateDatetimeSupplier = lastUpdateDatetimeSupplier;
    }
    /**
     * Gets the Company Supplier list.
     * 
     * @return the Company Supplier list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }
    /**
     * Sets the Company Supplier list.
     * 
     * @param companySupplierList the Company Supplier list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }
    /**
     * Gets the list of company DENSO code.
     * 
     * @return the list of company DENSO code.
     */
    public List<CompanyDensoDomain> getDCdList() {
        return dCdList;
    }

    /**
     * Sets the list of company DENSO code.
     * 
     * @param dCdList the list of company DENSO code.
     */
    public void setDCdList(List<CompanyDensoDomain> dCdList) {
        this.dCdList = dCdList;
    }
    
    /**
     * Gets the company supplier code criteria.
     * 
     * @return the company supplier code criteria.
     */
    public String getSCdCriteria() {
        return sCdCriteria;
    }

    /**
     * Sets the company supplier code criteria.
     * 
     * @param sCdCriteria the company supplier code criteria.
     */
    public void setSCdCriteria(String sCdCriteria) {
        this.sCdCriteria = sCdCriteria;
    }
    /**
     * Gets the supplier plant code
     * 
     * @return the supplier plant code
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }

    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }
    /**
     * Gets the max of record department list.
     * 
     * @return the max of record department list.
     */
    public int getMaxDepartment() {
        return maxDepartment;
    }
    /**
     * Sets the max of record department list.
     * 
     * @param maxDepartment the max of record department list.
     */
    public void setMaxDepartment(int maxDepartment) {
        this.maxDepartment = maxDepartment;
    }
    /**
     * Gets the Vendor Code.
     * 
     * @return the Vendor Code.
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * Sets the Vendor Code.
     * 
     * @param vendorCd the Vendor Code.
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    /**
     * Gets the list of Vendor Code.
     * 
     * @return the list of Vendor Code.
     */
    public List<SpsMAs400VendorDomain> getVendorCdList() {
        return vendorCdList;
    }
    /**
     * Sets the list of Vendor Code.
     * 
     * @param vendorCdList the list of Vendor Code.
     */
    public void setVendorCdList(List<SpsMAs400VendorDomain> vendorCdList) {
        this.vendorCdList = vendorCdList;
    }
    /**
     * Gets the max record of Vendor Code.
     * 
     * @return the max record of Vendor Code.
     */
    public int getMaxVendorCd() {
        return maxVendorCd;
    }
    /**
     * Sets the max record of Vendor Code.
     * 
     * @param maxVendorCd the max record of Vendor Code.
     */
    public void setMaxVendorCd(int maxVendorCd) {
        this.maxVendorCd = maxVendorCd;
    }
    
    /**
     * Gets the isDcompany flag.
     * 
     * @return the isDcompany flag.
     */
    public Boolean getIsDCompany() {
        return isDCompany;
    }
    /**
     * Sets the isDcompany flag.
     * 
     * @param isDCompany the isDcompany flag.
     */
    public void setIsDCompany(Boolean isDCompany) {
        this.isDCompany = isDCompany;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }
    
    /**
     * Sets the Department.
     * 
     * @param department the new Department.
     */
    public void setDepartmentItem(SpsMUserDomain department) {
        this.departmentList.add(department);
    }
    
    /**
     * Gets the Department.
     * 
     * @param index the index
     * @return the Department.
     */
    public SpsMUserDomain getDepartmentItem(int index) {
        if (departmentList == null) {
            departmentList = new ArrayList<SpsMUserDomain>();
        }
        while (departmentList.size() <= index) {
            departmentList.add(new SpsMUserDomain());
        }
        return departmentList.get(index);
    }
    
    /**
     * Sets the Company Supplier.
     * 
     * @param companySupplier the new Company Supplier.
     */
    public void setCompanySupplierItem(CompanySupplierDomain companySupplier) {
        this.companySupplierList.add(companySupplier);
    }
    
    /**
     * Gets the Company Supplier.
     * 
     * @param index the index
     * @return the Company Supplier.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index) {
        if (companySupplierList == null) {
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while (companySupplierList.size() <= index) {
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * Sets the Vendor.
     * 
     * @param vendor the new Vendor.
     */
    public void setVendorCodeItem(SpsMAs400VendorDomain vendor) {
        this.vendorCdList.add(vendor);
    }
    
    /**
     * Gets the Vendor.
     * 
     * @param index the index
     * @return the Vendor.
     */
    public SpsMAs400VendorDomain getVendorCodeItem(int index) {
        if (vendorCdList == null) {
            vendorCdList = new ArrayList<SpsMAs400VendorDomain>();
        }
        while (vendorCdList.size() <= index) {
            vendorCdList.add(new SpsMAs400VendorDomain());
        }
        return vendorCdList.get(index);
    }
    
    /**
     * Sets the Company DENSO.
     * 
     * @param companyDenso the new Company DENSO.
     */
    public void setCompanyDensoItem(CompanyDensoDomain companyDenso) {
        this.dCdList.add(companyDenso);
    }
    
    /**
     * Gets the Company DENSO.
     * 
     * @param index the index
     * @return the Company DENSO.
     */
    public CompanyDensoDomain getCompanyDensoItem(int index) {
        if (dCdList == null) {
            dCdList = new ArrayList<CompanyDensoDomain>();
        }
        while (dCdList.size() <= index) {
            dCdList.add(new CompanyDensoDomain());
        }
        return dCdList.get(index);
    }

    /**
     * <p>Getter method for departmentList.</p>
     *
     * @return the departmentList
     */
    public List<SpsMUserDomain> getDepartmentList() {
        return departmentList;
    }

    /**
     * <p>Setter method for departmentList.</p>
     *
     * @param departmentList Set for departmentList
     */
    public void setDepartmentList(List<SpsMUserDomain> departmentList) {
        this.departmentList = departmentList;
    }
}