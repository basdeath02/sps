/*
 * ModifyDate Development company     Describe 
 * 2014/08/14 CSI Akat                Create
 * 2016/02/23 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;

/**
 * The Class Word004Form.
 * @author CSI
 */
public class Word004Form extends CoreActionForm {

    /** The generated Serial Version UID. */
    private static final long serialVersionUID = -557713121552438178L;

    /** The DSC_ID. */
    private String dscId;
    
    /** The PO ID. */
    private String poId;
    
    /** The Supplier Part No. */
    private String sPn;
    
    /** The DENSO Part No. */
    private String dPn;
    
    /** The Period Type. */
    private String periodType;
    
    /** The Period Type Name. */
    private String periodTypeName;
    
    /** The PO Type. */
    private String poType;
    
    /** The Action Mode. */
    private String actionMode;

    /** The SPS PO number. */
    private String spsPoNo;
    
    /** All ID of CheckPoDue checkbox to enable. */
    private String enableCheckPoDueId;

    /** List of Purchase Order Due Domain. */
    private List<PurchaseOrderDueDomain> purchaseOrderDueList;
    
    /** List of Misc Domain (pending reason code). */
    private List<MiscellaneousDomain> pendingReasonCodeList;

    // [IN054] Add list of Misc Type for new combo box
    /** List of Misc Domain (Accept Reject Code). */
    private List<MiscellaneousDomain> acceptRejectCodeList;
    
    /** The default constructor. */
    public Word004Form() {
        this.purchaseOrderDueList = new ArrayList<PurchaseOrderDueDomain>();
        this.pendingReasonCodeList = new ArrayList<MiscellaneousDomain>();
    }

    /**
     * <p>Getter method for dscId.</p>
     *
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>Setter method for dscId.</p>
     *
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public String getPoId() {
        return poId;
    }

    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }

    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for periodType.</p>
     *
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * <p>Setter method for periodType.</p>
     *
     * @param periodType Set for periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    /**
     * <p>Getter method for periodTypeName.</p>
     *
     * @return the periodTypeName
     */
    public String getPeriodTypeName() {
        return periodTypeName;
    }

    /**
     * <p>Setter method for periodTypeName.</p>
     *
     * @param periodTypeName Set for periodTypeName
     */
    public void setPeriodTypeName(String periodTypeName) {
        this.periodTypeName = periodTypeName;
    }

    /**
     * <p>Getter method for poType.</p>
     *
     * @return the poType
     */
    public String getPoType() {
        return poType;
    }

    /**
     * <p>Setter method for poType.</p>
     *
     * @param poType Set for poType
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }

    /**
     * <p>Getter method for actionMode.</p>
     *
     * @return the actionMode
     */
    public String getActionMode() {
        return actionMode;
    }

    /**
     * <p>Setter method for actionMode.</p>
     *
     * @param actionMode Set for actionMode
     */
    public void setActionMode(String actionMode) {
        this.actionMode = actionMode;
    }

    /**
     * <p>Getter method for purchaseOrderDueList.</p>
     *
     * @return the purchaseOrderDueList
     */
    public List<PurchaseOrderDueDomain> getPurchaseOrderDueList() {
        return purchaseOrderDueList;
    }

    /**
     * <p>Setter method for purchaseOrderDueList.</p>
     *
     * @param purchaseOrderDueList Set for purchaseOrderDueList
     */
    public void setPurchaseOrderDueList(
        List<PurchaseOrderDueDomain> purchaseOrderDueList) {
        this.purchaseOrderDueList = purchaseOrderDueList;
    }

    /**
     * <p>Getter method for pendingReasonCodeList.</p>
     *
     * @return the pendingReasonCodeList
     */
    public List<MiscellaneousDomain> getPendingReasonCodeList() {
        return pendingReasonCodeList;
    }

    /**
     * <p>Setter method for pendingReasonCodeList.</p>
     *
     * @param pendingReasonCodeList Set for pendingReasonCodeList
     */
    public void setPendingReasonCodeList(List<MiscellaneousDomain> pendingReasonCodeList) {
        this.pendingReasonCodeList = pendingReasonCodeList;
    }

    /**
     * <p>Getter method for spsPoNo.</p>
     *
     * @return the spsPoNo
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }

    /**
     * <p>Setter method for spsPoNo.</p>
     *
     * @param spsPoNo Set for spsPoNo
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }
    
    /**
     * Sets the Purchase Order Due
     * 
     * @param purchaseOrderDue the new Purchase Order Due
     */
    public void setPurchaseOrderDue(PurchaseOrderDueDomain purchaseOrderDue) {
        this.purchaseOrderDueList.add(purchaseOrderDue);
    }
    
    /**
     * Gets the Purchase Order Due.
     * 
     * @param index the index
     * @return the Purchase Order Due.
     */
    public PurchaseOrderDueDomain getPurchaseOrderDue(int index) {
        if (purchaseOrderDueList == null) {
            purchaseOrderDueList = new ArrayList<PurchaseOrderDueDomain>();
        }
        while (purchaseOrderDueList.size() <= index) {
            purchaseOrderDueList.add(new PurchaseOrderDueDomain());
        }
        return purchaseOrderDueList.get(index);
    }

    /**
     * Sets the Pending Reason Code
     * 
     * @param pendingReason the new Pending Reason Code
     */
    public void setPendingReasonCode(MiscellaneousDomain pendingReason) {
        this.pendingReasonCodeList.add(pendingReason);
    }
    
    /**
     * Gets the Pending Reason Code.
     * 
     * @param index the index
     * @return the Pending Reason Code.
     */
    public MiscellaneousDomain getPendingReasonCode(int index) {
        if (pendingReasonCodeList == null) {
            pendingReasonCodeList = new ArrayList<MiscellaneousDomain>();
        }
        while (pendingReasonCodeList.size() <= index) {
            pendingReasonCodeList.add(new MiscellaneousDomain());
        }
        return pendingReasonCodeList.get(index);
    }

    // Start : [IN054] To keep Accept Reject combobox item in Action Form
    /**
     * Sets the Accept Reject Code
     * 
     * @param acceptReject the new Accept Reject Code
     */
    public void setAcceptRejectCode(MiscellaneousDomain acceptReject) {
        this.acceptRejectCodeList.add(acceptReject);
    }
    
    /**
     * Gets the Accept Reject Code.
     * 
     * @param index the index
     * @return the Accept Reject Code.
     */
    public MiscellaneousDomain getAcceptRejectCode(int index) {
        if (acceptRejectCodeList == null) {
            acceptRejectCodeList = new ArrayList<MiscellaneousDomain>();
        }
        while (acceptRejectCodeList.size() <= index) {
            acceptRejectCodeList.add(new MiscellaneousDomain());
        }
        return acceptRejectCodeList.get(index);
    }
    // End : [IN054] To keep Accept Reject combobox item in Action Form
    
    
    /**
     * <p>Getter method for enableCheckPoDueId.</p>
     *
     * @return the enableCheckPoDueId
     */
    public String getEnableCheckPoDueId() {
        return enableCheckPoDueId;
    }

    /**
     * <p>Setter method for enableCheckPoDueId.</p>
     *
     * @param enableCheckPoDueId Set for enableCheckPoDueId
     */
    public void setEnableCheckPoDueId(String enableCheckPoDueId) {
        this.enableCheckPoDueId = enableCheckPoDueId;
    }

    // Start : [IN054] Add list of Misc Type for new combo box
    /**
     * <p>Getter method for acceptRejectCodeList.</p>
     *
     * @return the acceptRejectCodeList
     */
    public List<MiscellaneousDomain> getAcceptRejectCodeList() {
        return acceptRejectCodeList;
    }

    /**
     * <p>Setter method for acceptRejectCodeList.</p>
     *
     * @param acceptRejectCodeList Set for acceptRejectCodeList
     */
    public void setAcceptRejectCodeList(
        List<MiscellaneousDomain> acceptRejectCodeList) {
        this.acceptRejectCodeList = acceptRejectCodeList;
    }
    // End : [IN054] Add list of Misc Type for new combo box
}
