/*
 * ModifyDate Development company   Describe 
 * 2014/07/21 CSI Parichat          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Wshp001Form.
 * @author CSI
 */
public class Wshp001Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8056584415037603708L;
    
    /** The mode. */
    private String mode;
    
    /** The delivery date from. */
    private String deliveryDateFrom;
    
    /** The delivery date to. */
    private String deliveryDateTo;
    
    /** The delivery time from. */
    private String deliveryTimeFrom;
    
    /** The delivery time to. */
    private String deliveryTimeTo;
    
    /** The isPrintOneWayKanbanTag */
    private String isPrintOneWayKanbanTag;
    
    /** The vendor code. */
    private String vendorCd;
   
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The shipment status. */
    private String shipmentStatus;
    
    /** The trans. */
    private String trans;
    
    /** The route no. */
    private String routeNo;
    
    /** The del. */
    private String del;
    
    /** The ship date from. */
    private String shipDateFrom;
    
    /** The ship date to. */
    private String shipDateTo;
    
    /** The issue date from. */
    private String issueDateFrom;
    
    /** The issue date to. */
    private String issueDateTo;
    
    /** The order method. */
    private String orderMethod;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The cigma do no. */
    private String cigmaDoNo;
    
    /** The pdf file id. */
    private String pdfFileId;
    
    /** The receiveByScan. */
    private String receiveByScan;
    
    /** The ltFlag. */
    private String ltFlag;
    
    /** The selected do id. */
    private String selectedDoId;
    
    /** The selected dCd. */
    private String selectedDCd;
    
    /** The selected sps do no. */
    private String selectedSpsDoNo;
    
    /** The selected rev. */
    private String selectedRev;
    
    /** The selected route. */
    private String selectedRoute;
    
    /** The selected del. */
    private String selectedDel;
    
    /** The count group asn. */
    private String countGroupAsn;
    
    /** The doId. */
    private String doId;
    
    /** The doId. */
    private String pdfType;
    
    /** The pdfFileKbTagPrintDate. */
    private String pdfFileKbTagPrintDate;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The shipment status list. */
    private List<MiscellaneousDomain> shipmentStatusList;
    
    /** The trans list. */
    private List<MiscellaneousDomain> transList;
    
    /** The order method list. */
    private List<MiscellaneousDomain> orderMethodList;
    
    /** The receive By Scan List. */
    private List<MiscellaneousDomain> receiveByScanList;
    
    /** The Leadtime flag List. */
    private List<MiscellaneousDomain> ltFlagList;
    
    /** The plant denso list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The plant supplier list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The company supplier list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The company denso list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The acknowledged do information list. */
    private List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The default Constructor */
    public Wshp001Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        deliveryDateFrom = Constants.EMPTY_STRING;
        deliveryDateTo = Constants.EMPTY_STRING;
        deliveryTimeFrom = Constants.EMPTY_STRING;
        deliveryTimeTo = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        shipmentStatus = Constants.EMPTY_STRING;
        trans = Constants.EMPTY_STRING;
        routeNo = Constants.EMPTY_STRING;
        del = Constants.EMPTY_STRING;
        shipDateFrom = Constants.EMPTY_STRING;
        shipDateTo = Constants.EMPTY_STRING;
        issueDateFrom = Constants.EMPTY_STRING;
        issueDateTo = Constants.EMPTY_STRING;
        orderMethod = Constants.EMPTY_STRING;
        spsDoNo = Constants.EMPTY_STRING;
        cigmaDoNo = Constants.EMPTY_STRING;
        countGroupAsn = Constants.STR_ZERO;
        receiveByScan = Constants.EMPTY_STRING;
        setLtFlag(Constants.EMPTY_STRING);
        shipmentStatusList = new ArrayList<MiscellaneousDomain>();
        transList = new ArrayList<MiscellaneousDomain>();
        orderMethodList = new ArrayList<MiscellaneousDomain>();
        receiveByScanList = new ArrayList<MiscellaneousDomain>();
        ltFlagList = new ArrayList<MiscellaneousDomain>();
        acknowledgedDoInformationList = new ArrayList<AcknowledgedDoInformationReturnDomain>();
        companySupplierList = new ArrayList<CompanySupplierDomain>();
        companyDensoList = new ArrayList<CompanyDensoDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * <p>Setter method for acknowledged do information.</p>
     * 
     * @param items the new acknowledged do information
     */
    public void setAcknowledgedDoInformationReturn(AcknowledgedDoInformationReturnDomain items) {
        this.acknowledgedDoInformationList.add(items);
    }
    
    /**
     * Gets the acknowledged do information.
     * 
     * @param index the index
     * @return the acknowledged do information return domain.
     */
    public AcknowledgedDoInformationReturnDomain getAcknowledgedDoInformationReturn(int index){
        if (acknowledgedDoInformationList == null) {
            acknowledgedDoInformationList = new ArrayList<AcknowledgedDoInformationReturnDomain>();
        }
        while (acknowledgedDoInformationList.size() <= index) {
            acknowledgedDoInformationList.add(new AcknowledgedDoInformationReturnDomain());
        }
        return acknowledgedDoInformationList.get(index);
    }
    
    /**
     * <p>Setter method for Company Supplier Item.</p>
     * 
     * @param items the new company supplier domain
     */
    public void setCompanySupplierItem(CompanySupplierDomain items) {
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the Company Supplier Item.
     * 
     * @param index the index
     * @return the company supplier domain.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index){
        if(companySupplierList == null){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * <p>Setter method for Company Denso Item.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoItem(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the Company Denso Item.
     * 
     * @param index the index
     * @return the company denso domain.
     */
    public CompanyDensoDomain getCompanyDensoItem(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * <p>Setter method for shipmentStatusItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setShipmentStatusItem(MiscellaneousDomain items) {
        this.shipmentStatusList.add(items);
    }
    
    /**
     * Gets the shipmentStatusItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getShipmentStatusItem(int index){
        if(shipmentStatusList == null){
            shipmentStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(shipmentStatusList.size() <= index){
            shipmentStatusList.add(new MiscellaneousDomain());
        }
        return shipmentStatusList.get(index);
    }
    
    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
    /**
     * <p>Setter method for transItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setTransItem(MiscellaneousDomain items) {
        this.transList.add(items);
    }
    
    /**
     * Gets the transItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getTransItem(int index){
        if(transList == null){
            transList = new ArrayList<MiscellaneousDomain>();
        }
        while(transList.size() <= index){
            transList.add(new MiscellaneousDomain());
        }
        return transList.get(index);
    }
    
    /**
     * <p>Setter method for orderMethodItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setOrderMethodItem(MiscellaneousDomain items) {
        this.orderMethodList.add(items);
    }
    
    /**
     * Gets the orderMethodItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getOrderMethodItem(int index){
        if(orderMethodList == null){
            orderMethodList = new ArrayList<MiscellaneousDomain>();
        }
        while(orderMethodList.size() <= index){
            orderMethodList.add(new MiscellaneousDomain());
        }
        return orderMethodList.get(index);
    }
    
    /**
     * <p>Setter method for setReceiveByScanItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setReceiveByScanItem(MiscellaneousDomain items) {
        this.receiveByScanList.add(items);
    }
    
    /**
     * Gets the orderMethodItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getReceiveByScanItem(int index){
        if(receiveByScanList == null){
            receiveByScanList = new ArrayList<MiscellaneousDomain>();
        }
        while(receiveByScanList.size() <= index){
            receiveByScanList.add(new MiscellaneousDomain());
        }
        return receiveByScanList.get(index);
    }
    
    /**
     * <p>Setter method for setLtFlagItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setLtFlagItem(MiscellaneousDomain items) {
        this.ltFlagList.add(items);
    }
    
    /**
     * Gets the orderMethodItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getLtFlagItem(int index){
        if(ltFlagList == null){
            ltFlagList = new ArrayList<MiscellaneousDomain>();
        }
        while(ltFlagList.size() <= index){
            ltFlagList.add(new MiscellaneousDomain());
        }
        return ltFlagList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the delivery date from.
     * 
     * @return the delivery date from
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * Sets the delivery date from.
     * 
     * @param deliveryDateFrom the delivery date from
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * Gets the delivery date to.
     * 
     * @return the delivery date to
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * Sets the delivery date to.
     * 
     * @param deliveryDateTo the delivery date to
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }
    
    /**
     * Gets the delivery time from.
     * 
     * @return the delivery time from
     */
    public String getDeliveryTimeFrom() {
        return deliveryTimeFrom;
    }

    /**
     * Sets the delivery time from.
     * 
     * @param deliveryTimeFrom the delivery time from
     */
    public void setDeliveryTimeFrom(String deliveryTimeFrom) {
        this.deliveryTimeFrom = deliveryTimeFrom;
    }

    /**
     * Gets the delivery time to.
     * 
     * @return the delivery time to
     */
    public String getDeliveryTimeTo() {
        return deliveryTimeTo;
    }

    /**
     * Sets the delivery time to.
     * 
     * @param deliveryTimeTo the delivery time to
     */
    public void setDeliveryTimeTo(String deliveryTimeTo) {
        this.deliveryTimeTo = deliveryTimeTo;
    }

    /**
     * Gets the vendor code.
     * 
     * @return the vendor code
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Sets the vendor code.
     * 
     * @param vendorCd the vendor code
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant.
     * 
     * @param sPcd the supplier plant
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso code.
     * 
     * @return the denso code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso code.
     * 
     * @param dCd the denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the denso plant code.
     * 
     * @return the denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the denso plant code.
     * 
     * @param dPcd the denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the route no.
     * 
     * @return the route no
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * Sets the route no.
     * 
     * @param routeNo the route no
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * Gets the del.
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * Sets the del.
     * 
     * @param del the del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * Gets the ship date from.
     * 
     * @return the ship date from
     */
    public String getShipDateFrom() {
        return shipDateFrom;
    }

    /**
     * Sets the ship date from.
     * 
     * @param shipDateFrom the ship date from
     */
    public void setShipDateFrom(String shipDateFrom) {
        this.shipDateFrom = shipDateFrom;
    }

    /**
     * Gets the ship date to.
     * 
     * @return the ship date to
     */
    public String getShipDateTo() {
        return shipDateTo;
    }

    /**
     * Sets the ship date to.
     * 
     * @param shipDateTo the ship date to
     */
    public void setShipDateTo(String shipDateTo) {
        this.shipDateTo = shipDateTo;
    }

    /**
     * Gets the issue date from.
     * 
     * @return the issue date from
     */
    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    /**
     * Sets the issue date from.
     * 
     * @param issueDateFrom the issue date from
     */
    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    /**
     * Gets the issue date to.
     * 
     * @return the issue date to
     */
    public String getIssueDateTo() {
        return issueDateTo;
    }

    /**
     * Sets the issue date to.
     * 
     * @param issueDateTo the issue date to
     */
    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    /**
     * Gets the sps do no.
     * 
     * @return the sps do no
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Sets the sps do no.
     * 
     * @param spsDoNo the sps do no
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Gets the cigma do no.
     * 
     * @return the cigma do no
     */
    public String getCigmaDoNo() {
        return cigmaDoNo;
    }

    /**
     * Sets the cigma do no.
     * 
     * @param cigmaDoNo the cigma do no
     */
    public void setCigmaDoNo(String cigmaDoNo) {
        this.cigmaDoNo = cigmaDoNo;
    }

    /**
     * Gets the shipment status list.
     * 
     * @return the shipment status list
     */
    public List<MiscellaneousDomain> getShipmentStatusList() {
        return shipmentStatusList;
    }

    /**
     * Sets the shipment status list.
     * 
     * @param shipmentStatusList the shipment status list
     */
    public void setShipmentStatusList(List<MiscellaneousDomain> shipmentStatusList) {
        this.shipmentStatusList = shipmentStatusList;
    }

    /**
     * Gets the trans list.
     * 
     * @return the trans list
     */
    public List<MiscellaneousDomain> getTransList() {
        return transList;
    }

    /**
     * Sets the trans list.
     * 
     * @param transList the trans list
     */
    public void setTransList(List<MiscellaneousDomain> transList) {
        this.transList = transList;
    }

    /**
     * Gets the order method list.
     * 
     * @return the order method list
     */
    public List<MiscellaneousDomain> getOrderMethodList() {
        return orderMethodList;
    }

    /**
     * Sets the order method list.
     * 
     * @param orderMethodList the order method list
     */
    public void setOrderMethodList(List<MiscellaneousDomain> orderMethodList) {
        this.orderMethodList = orderMethodList;
    }

    /**
     * Gets the shipment status.
     * 
     * @return the shipment status
     */
    public String getShipmentStatus() {
        return shipmentStatus;
    }

    /**
     * Sets the shipment status.
     * 
     * @param shipmentStatus the shipment status
     */
    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    /**
     * Gets the trans.
     * 
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * Sets the trans.
     * 
     * @param trans the trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }

    /**
     * Gets the order method.
     * 
     * @return the order method
     */
    public String getOrderMethod() {
        return orderMethod;
    }

    /**
     * Sets the order method.
     * 
     * @param orderMethod the order method
     */
    public void setOrderMethod(String orderMethod) {
        this.orderMethod = orderMethod;
    }

    /**
     * Gets the acknowledged do information list.
     * 
     * @return the acknowledged do information list
     */
    public List<AcknowledgedDoInformationReturnDomain> getAcknowledgedDoInformationList() {
        return acknowledgedDoInformationList;
    }

    /**
     * Sets the acknowledged do information list.
     * 
     * @param acknowledgedDoInformationList the acknowledged do information list
     */
    public void setAcknowledgedDoInformationList(
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList) {
        this.acknowledgedDoInformationList = acknowledgedDoInformationList;
    }

    /**
     * Gets the pdf file id.
     * 
     * @return the pdf file id.
     */
    public String getPdfFileId() {
        return pdfFileId;
    }
    
    /**
     * Sets the pdf file id.
     * 
     * @param pdfFileId the pdf file id.
     */  
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Gets the selected do id.
     * 
     * @return the selected do id.
     */
    public String getSelectedDoId() {
        return selectedDoId;
    }

    /**
     * Sets the selected do id.
     * 
     * @param selectedDoId the selected do id.
     */
    public void setSelectedDoId(String selectedDoId) {
        this.selectedDoId = selectedDoId;
    }
    
    /**
     * Gets the selected dCd.
     * 
     * @return the selected dCd
     */
    public String getSelectedDCd() {
        return selectedDCd;
    }

    /**
     * Sets the selected dCd.
     * 
     * @param selectedDCd the selected dCd
     */
    public void setSelectedDCd(String selectedDCd) {
        this.selectedDCd = selectedDCd;
    }

    /**
     * Gets the selected sps do no.
     * 
     * @return the selected sps do no.
     */
    public String getSelectedSpsDoNo() {
        return selectedSpsDoNo;
    }

    /**
     * Sets the selected sps do no.
     * 
     * @param selectedSpsDoNo the selected sps do no.
     */
    public void setSelectedSpsDoNo(String selectedSpsDoNo) {
        this.selectedSpsDoNo = selectedSpsDoNo;
    }

    /**
     * Gets the selected rev.
     * 
     * @return the selected rev.
     */
    public String getSelectedRev() {
        return selectedRev;
    }

    /**
     * Sets the selected rev.
     * 
     * @param selectedRev the selected rev.
     */
    public void setSelectedRev(String selectedRev) {
        this.selectedRev = selectedRev;
    }

    /**
     * Gets the selected route.
     * 
     * @return the selected route.
     */
    public String getSelectedRoute() {
        return selectedRoute;
    }

    /**
     * Sets the selected route.
     * 
     * @param selectedRoute the selected route.
     */
    public void setSelectedRoute(String selectedRoute) {
        this.selectedRoute = selectedRoute;
    }

    /**
     * Gets the selected del.
     * 
     * @return the selected del.
     */
    public String getSelectedDel() {
        return selectedDel;
    }

    /**
     * Sets the selected del.
     * 
     * @param selectedDel the selected del.
     */
    public void setSelectedDel(String selectedDel) {
        this.selectedDel = selectedDel;
    }

    /**
     * Gets the company denso plant list.
     * 
     * @return the company denso plant list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the company denso plant list.
     * 
     * @param plantDensoList the company denso plant list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the company supplier plant list.
     * 
     * @return the company supplier plant list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the company supplier plant list.
     * 
     * @param plantSupplierList the company supplier plant list.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * Gets the count group asn.
     * 
     * @return the count group asn.
     */
    public String getCountGroupAsn() {
        return countGroupAsn;
    }

    /**
     * Sets the count group asn.
     * 
     * @param countGroupAsn the count group asn.
     */
    public void setCountGroupAsn(String countGroupAsn) {
        this.countGroupAsn = countGroupAsn;
    }

    /**
     * Gets the company supplier list.
     * 
     * @return the company supplier list
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the company supplier list.
     * 
     * @param companySupplierList the company supplier list
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the company denso list.
     * 
     * @return the company denso list
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the company denso list.
     * 
     * @param companyDensoList the company denso list
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * <p>Getter method for getReceiveByScan.</p>
     *
     * @return the getReceiveByScan
     */
    public String getReceiveByScan() {
        return receiveByScan;
    }

    /**
     * <p>Setter method for receiveByScan.</p>
     *
     * @param receiveByScan Set for receiveByScan
     */
    public void setReceiveByScan(String receiveByScan) {
        this.receiveByScan = receiveByScan;
    }

    /**
     * <p>Getter method for ltFlag.</p>
     *
     * @return the ltFlag
     */
    public String getLtFlag() {
        return ltFlag;
    }

    /**
     * <p>Setter method for ltFlag.</p>
     *
     * @param ltFlag Set for ltFlag
     */
    public void setLtFlag(String ltFlag) {
        this.ltFlag = ltFlag;
    }

    /**
     * <p>Getter method for isPrintOneWayKanbanTag.</p>
     *
     * @return the isPrintOneWayKanbanTag
     */
    public String getIsPrintOneWayKanbanTag() {
        return isPrintOneWayKanbanTag;
    }

    /**
     * <p>Setter method for isPrintOneWayKanbanTag.</p>
     *
     * @param isPrintOneWayKanbanTag Set for isPrintOneWayKanbanTag
     */
    public void setIsPrintOneWayKanbanTag(String isPrintOneWayKanbanTag) {
        this.isPrintOneWayKanbanTag = isPrintOneWayKanbanTag;
    }

    /**
     * <p>Getter method for receiveByScanList.</p>
     *
     * @return the receiveByScanList
     */
    public List<MiscellaneousDomain> getReceiveByScanList() {
        return receiveByScanList;
    }

    /**
     * <p>Setter method for receiveByScanList.</p>
     *
     * @param receiveByScanList Set for receiveByScanList
     */
    public void setReceiveByScanList(List<MiscellaneousDomain> receiveByScanList) {
        this.receiveByScanList = receiveByScanList;
    }

    /**
     * <p>Getter method for ltFlagList.</p>
     *
     * @return the ltFlagList
     */
    public List<MiscellaneousDomain> getLtFlagList() {
        return ltFlagList;
    }

    /**
     * <p>Setter method for ltFlagList.</p>
     *
     * @param ltFlagList Set for ltFlagList
     */
    public void setLtFlagList(List<MiscellaneousDomain> ltFlagList) {
        this.ltFlagList = ltFlagList;
    }

    /**
     * <p>Getter method for doId.</p>
     *
     * @return the doId
     */

    public String getDoId() {
        return doId;
    }

    /**
     * <p>Setter method for doId.</p>
     *
     * @param doId Set for doId
     */

    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * <p>Getter method for pdfFileKbTagPrintDate.</p>
     *
     * @return the pdfFileKbTagPrintDate
     */

    public String getPdfFileKbTagPrintDate() {
        return pdfFileKbTagPrintDate;
    }

    /**
     * <p>Setter method for pdfFileKbTagPrintDate.</p>
     *
     * @param pdfFileKbTagPrintDate Set for pdfFileKbTagPrintDate
     */

    public void setPdfFileKbTagPrintDate(String pdfFileKbTagPrintDate) {
        this.pdfFileKbTagPrintDate = pdfFileKbTagPrintDate;
    }

    /**
     * <p>Getter method for pdfType.</p>
     *
     * @return the pdfType
     */

    public String getPdfType() {
        return pdfType;
    }

    /**
     * <p>Setter method for pdfType.</p>
     *
     * @param pdfType Set for pdfType
     */

    public void setPdfType(String pdfType) {
        this.pdfType = pdfType;
    }
}