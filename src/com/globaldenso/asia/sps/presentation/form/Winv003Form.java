/*
 * ModifyDate Developmentcompany Describe 
 * 2014/08/08 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;


import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceResultDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Winv003Form.
 * @author CSI
 */
public class Winv003Form extends CoreActionForm {
    
    /** Generated serial version UID. */
    private static final long serialVersionUID = 4748376793923671870L;

    /** The file data. */
    private FormFile fileData;
    
    /** The session Id. */
    private String sessionId;
    
    /** The total record. */
    private String totalRecord;
    
    /** The correct record. */
    private String correctRecord;
    
    /** The warning record. */
    private String warningRecord;
    
    /** The error record. */
    private String errorRecord;
    
    /** The invoice record. */
    private String invoiceNo;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant tax ID. */
    private String supplierTaxId;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The file name. */
    private String fileName;
    
    /** The dsc Id. */
    private String dscId;
    
    /** The list of Temp Upload Invoice Result Domain.*/
    private List<TmpUploadInvoiceResultDomain> tmpUploadInvoiceResultList;
    
    /** The Message when cannot click reset. */
    private String cannotResetMessage;
    
    /** The Message when cannot click download. */
    private String cannotDownloadMessage;
    
    /**
     * <p>Getter method for sCdList.</p>
     *
     * @return the sCdList
     */

    /** The default constructor. */
    public Winv003Form() {
        
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        fileName = Constants.EMPTY_STRING;
        this.tmpUploadInvoiceResultList = new ArrayList<TmpUploadInvoiceResultDomain>();
    }
    
    /**
     * <p>Setter method for invoice information.</p>
     * 
     * @param items the new invoice information
     */
    public void setInvoiceInformationUploading(TmpUploadInvoiceResultDomain items) {
        this.tmpUploadInvoiceResultList.add(items);
    }
    
    /**
     * Gets the invoice information item.
     * 
     * @param index the index
     * @return the invoice information item.
     */
    public TmpUploadInvoiceResultDomain getInvoiceInformationUploading(int index){
        if (tmpUploadInvoiceResultList == null) {
            tmpUploadInvoiceResultList = new ArrayList<TmpUploadInvoiceResultDomain>();
        }
        while (tmpUploadInvoiceResultList.size() <= index) {
            tmpUploadInvoiceResultList.add(new TmpUploadInvoiceResultDomain());
        }
        return tmpUploadInvoiceResultList.get(index);
    }
    
    /**
     * <p>Getter method for file data.</p>
     *
     * @return the file data
     */
    public FormFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter method for file data.</p>
     *
     * @param fileData Set for file data
     */
    public void setFileData(FormFile fileData) {
        this.fileData = fileData;
    }

    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * <p>Getter method for totalRecord.</p>
     *
     * @return the totalRecord
     */
    public String getTotalRecord() {
        return totalRecord;
    }

    /**
     * <p>Setter method for totalRecord.</p>
     *
     * @param totalRecord Set for totalRecord
     */
    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    /**
     * <p>Getter method for correctRecord.</p>
     *
     * @return the correctRecord
     */
    public String getCorrectRecord() {
        return correctRecord;
    }

    /**
     * <p>Setter method for correctRecord.</p>
     *
     * @param correctRecord Set for correctRecord
     */
    public void setCorrectRecord(String correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * <p>Getter method for warningRecord.</p>
     *
     * @return the warningRecord
     */
    public String getWarningRecord() {
        return warningRecord;
    }

    /**
     * <p>Setter method for warningRecord.</p>
     *
     * @param warningRecord Set for warningRecord
     */
    public void setWarningRecord(String warningRecord) {
        this.warningRecord = warningRecord;
    }

    /**
     * <p>Getter method for errorRecord.</p>
     *
     * @return the errorRecord
     */
    public String getErrorRecord() {
        return errorRecord;
    }

    /**
     * <p>Setter method for errorRecord.</p>
     *
     * @param errorRecord Set for errorRecord
     */
    public void setErrorRecord(String errorRecord) {
        this.errorRecord = errorRecord;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplierTaxId.</p>
     *
     * @return the supplierTaxId
     */
    public String getSupplierTaxId() {
        return supplierTaxId;
    }

    /**
     * <p>Setter method for supplierTaxId.</p>
     *
     * @param supplierTaxId Set for supplierTaxId
     */
    public void setSupplierTaxId(String supplierTaxId) {
        this.supplierTaxId = supplierTaxId;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for dscId.</p>
     *
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>Setter method for dscId.</p>
     *
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>Getter method for tmpUploadInvoiceResultList.</p>
     *
     * @return the tmpUploadInvoiceResultList
     */
    public List<TmpUploadInvoiceResultDomain> getTmpUploadInvoiceResultList() {
        return tmpUploadInvoiceResultList;
    }

    /**
     * <p>Setter method for tmpUploadInvoiceResultList.</p>
     *
     * @param tmpUploadInvoiceResultList Set for tmpUploadInvoiceResultList
     */
    public void setTmpUploadInvoiceResultList(
        List<TmpUploadInvoiceResultDomain> tmpUploadInvoiceResultList) {
        this.tmpUploadInvoiceResultList = tmpUploadInvoiceResultList;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadMessage.</p>
     *
     * @return the cannotDownloadMessage
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * <p>Setter method for cannotDownloadMessage.</p>
     *
     * @param cannotDownloadMessage Set for cannotDownloadMessage
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }
}
