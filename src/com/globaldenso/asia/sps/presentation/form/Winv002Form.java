/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/09 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WINV002Form.
 * @author CSI
 */
public class Winv002Form extends CoreActionForm {
    
    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 6822795417253195789L;
    
    /** The mode. */
    private String mode;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The supplier plant code. */
    private String dPcd;
    
    /** The plan ETA date from. */
    private String planEtaFrom;
    
    /** The plan ETA date to. */
    private String planEtaTo;
    
    /** The plan ETA time from. */
    private String planEtaTimeFrom;
    
    /** The plan ETA time to. */
    private String planEtaTimeTo;
    
    /** The asn status.*/
    private String asnStatus;
    
    /** The invoice no.*/
    private String asnNo;
    
    /** The actual ETd date from. */
    private String actualEtdFrom;
    
    /** The actual ETd date to. */
    private String actualEtdTo;
    
    /** The supplier plant tax ID. */
    private String supplierPlantTaxId;
    
    /** The session Id. */
    private String sessionId;
    
    /** The list of ASN Information Domain.*/
    private List<AsnInformationDomain> asnInformationForDisplayList;
    
    /** The count group asn. */
    private String countGroupAsn;

    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The asn Selected. */
    private String asnSelected;
    
    /** The dsc id. */
    private String dscId;
    
    /** The screen id. */
    private String screenId;
    
    /** The supplier code list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The denso code list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The company denso plant list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The company supplier plant list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The asn status list. */
    private List<MiscellaneousDomain> asnStatusList;
    
    /** The Message when cannot click reset. */
    private String cannotResetMessage;
    
    /** The Message when cannot click group. */
    private String cannotGroupMessage;
    
    /** The default constructor. */
    public Winv002Form() {
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        planEtaFrom = Constants.EMPTY_STRING;
        planEtaTo = Constants.EMPTY_STRING;
        asnStatus = Constants.EMPTY_STRING;
        asnNo = Constants.EMPTY_STRING;
        actualEtdFrom = Constants.EMPTY_STRING;
        actualEtdTo = Constants.EMPTY_STRING;
        supplierPlantTaxId = Constants.EMPTY_STRING;
        asnInformationForDisplayList = new ArrayList<AsnInformationDomain>();
        companySupplierList = new ArrayList<CompanySupplierDomain>();
        companyDensoList = new ArrayList<CompanyDensoDomain>();
        asnStatusList = new ArrayList<MiscellaneousDomain>();

    }
    
    /**
     * <p>Setter method for ASN Information.</p>
     * 
     * @param items the new ASN Information
     */
    public void setAsnInformation(AsnInformationDomain items) {
        this.asnInformationForDisplayList.add(items);
    }
    
    /**
     * Gets the ASN Information item.
     * 
     * @param index the index
     * @return the ASN Information item.
     */
    public AsnInformationDomain getAsnInformation(int index){
        if (asnInformationForDisplayList == null) {
            asnInformationForDisplayList = new ArrayList<AsnInformationDomain>();
        }
        while (asnInformationForDisplayList.size() <= index) {
            asnInformationForDisplayList.add(new AsnInformationDomain());
        }
        return asnInformationForDisplayList.get(index);
    }

    /**
     * <p>Setter method for supplier code.</p>
     * 
     * @param items the new company supplier domain
     */
    public void setCompanySupplierCode(CompanySupplierDomain items) {
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the supplier code.
     * 
     * @param index the index
     * @return the company supplier domain.
     */
    public CompanySupplierDomain getCompanySupplierCode(int index){
        if(companySupplierList == null){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * <p>Setter method for denso code.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoCode(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the denso code.
     * 
     * @param index the index
     * @return the company denso domain.
     */
    public CompanyDensoDomain getCompanyDensoCode(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * <p>Setter method for asn status.</p>
     * 
     * @param items the new misc domain
     */
    public void setAsnStatus(MiscellaneousDomain items) {
        this.asnStatusList.add(items);
    }
    
    /**
     * Gets the asn status.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public MiscellaneousDomain getAsnStatus(int index){
        if(asnStatusList == null){
            asnStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(asnStatusList.size() <= index){
            asnStatusList.add(new MiscellaneousDomain());
        }
        return asnStatusList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * <p>Setter method for denso code.</p>
     *
     * @param dCd Set for denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getDPcd() {
        return dPcd;
    }
    
    /**
     * <p>Setter method for denso plan code.</p>
     *
     * @param dPcd Set for denso plan code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for plan ETA from.</p>
     *
     * @return the plan ETA from
     */
    public String getPlanEtaFrom() {
        return planEtaFrom;
    }
    
    /**
     * <p>Setter method for plan ETA from.</p>
     *
     * @param planEtaFrom Set for plan ETA from to
     */
    public void setPlanEtaFrom(String planEtaFrom) {
        this.planEtaFrom = planEtaFrom;
    }
    
    /**
     * <p>Getter method for plan ETA to.</p>
     *
     * @return the plan ETA to
     */
    public String getPlanEtaTo() {
        return planEtaTo;
    }

    /**
     * <p>Setter method for plan ETA /p>
     *
     * @param planEtaTo Set for plan ETA to
     */
    public void setPlanEtaTo(String planEtaTo) {
        this.planEtaTo = planEtaTo;
    }
    
    /**
     * <p>Getter method for ASN status.</p>
     *
     * @return the ASN status
     */
    public String getAsnStatus() {
        return asnStatus;
    }
    
    /**
     * <p>Setter method for ASN status.</p>
     *
     * @param asnStatus Set for ASN status
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }
    
    /**
     * <p>Getter method for ASN no.</p>
     *
     * @return the ASN no
     */
    public String getAsnNo() {
        return asnNo;
    }
    
    /**
     * <p>Setter method for ASN no.</p>
     *
     * @param asnNo Set for ASN no
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }
    
    /**
     * <p>Getter method for actual ETD .</p>
     *
     * @return the actual ETD from
     */
    public String getActualEtdFrom() {
        return actualEtdFrom;
    }
    
    /**
     * <p>Setter method for actual ETD from.</p>
     *
     * @param actualEtdFrom Set for actual ETD from
     */
    public void setActualEtdFrom(String actualEtdFrom) {
        this.actualEtdFrom = actualEtdFrom;
    }
    
    /**
     * <p>Getter method for actual ETD /p>
     *
     * @return the actual ETD to
     */
    public String getActualEtdTo() {
        return actualEtdTo;
    }
    
    /**
     * <p>Setter method for actual ETD /p>
     *
     * @param actualEtdTo Set for actual ETD to
     */
    public void setActualEtdTo(String actualEtdTo) {
        this.actualEtdTo = actualEtdTo;
    }
    
    
    /**
     * <p>Getter method for supplier plant tax ID.</p>
     *
     * @return the supplier plant tax ID
     */
    public String getSupplierPlantTaxId() {
        return supplierPlantTaxId;
    }
    
    /**
     * <p>Setter method for supplier plant tax ID.</p>
     *
     * @param supplierPlantTaxId Set for supplier plant tax ID
     */
    public void setSupplierPlantTaxId(String supplierPlantTaxId) {
        this.supplierPlantTaxId = supplierPlantTaxId;
    }
    
    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * <p>Getter method for ASN Information List.</p>
     *
     * @return the ASN Information List
     */
    public List<AsnInformationDomain> getAsnInformationForDisplayList() {
        return asnInformationForDisplayList;
    }
    
    /**
     * <p>Setter method for ASN Information for display list.</p>
     *
     * @param asnInformationForDisplayList Set for ASN Information for display list
     */
    public void setAsnInformationForDisplayList(
        List<AsnInformationDomain> asnInformationForDisplayList) {
        this.asnInformationForDisplayList = asnInformationForDisplayList;
    }
    
    /**
     * Gets the count group asn.
     * 
     * @return the count group asn.
     */
    public String getCountGroupAsn() {
        return countGroupAsn;
    }

    /**
     * Sets the count group asn.
     * 
     * @param countGroupAsn the count group asn.
     */
    public void setCountGroupAsn(String countGroupAsn) {
        this.countGroupAsn = countGroupAsn;
    }
    
    /**
     * Gets the ASN selected.
     * 
     * @return the ASN selected.
     */
    public String getAsnSelected() {
        return asnSelected;
    }
    
    /**
     * Sets the ASN selected.
     * 
     * @param asnSelected the ASN selected.
     */
    public void setAsnSelected(String asnSelected) {
        this.asnSelected = asnSelected;
    }
    

    /**
     * <p>Getter method for dscId.</p>
     *
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>Setter method for dscId.</p>
     *
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>Getter method for screenId.</p>
     *
     * @return the screenId
     */
    public String getScreenId() {
        return screenId;
    }

    /**
     * <p>Setter method for screenId.</p>
     *
     * @param screenId Set for screenId
     */
    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }

    /**
     * Gets the supplier code list.
     * 
     * @return the supplier code list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the supplier code list.
     * 
     * @param companySupplierList the supplier code list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the denso code list.
     * 
     * @return the denso code list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso code list.
     * 
     * @param companyDensoList the denso code list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso plant list.
     * 
     * @return the company denso plant list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the company denso plant list.
     * 
     * @param plantDensoList the company denso plant list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the company supplier plant list.
     * 
     * @return the company supplier plant list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the company supplier plant list.
     * 
     * @param plantSupplierList the company supplier plant list.
     */
    public void setPlantSupplierList(
        List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    
    /**
     * Gets the asn status list.
     * 
     * @return the asn status list.
     */
    public List<MiscellaneousDomain> getAsnStatusList() {
        return asnStatusList;
    }

    /**
     * Sets the asn statusn list.
     * 
     * @param asnStatusList the asn status list.
     */
    public void setAsnStatusList(List<MiscellaneousDomain> asnStatusList) {
        this.asnStatusList = asnStatusList;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotGroupMessage.</p>
     *
     * @return the cannotGroupMessage
     */
    public String getCannotGroupMessage() {
        return cannotGroupMessage;
    }

    /**
     * <p>Setter method for cannotGroupMessage.</p>
     *
     * @param cannotGroupMessage Set for cannotGroupMessage
     */
    public void setCannotGroupMessage(String cannotGroupMessage) {
        this.cannotGroupMessage = cannotGroupMessage;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }

    /**
     * <p>Getter method for planEtaTimeFrom.</p>
     *
     * @return the planEtaTimeFrom
     */
    public String getPlanEtaTimeFrom() {
        return planEtaTimeFrom;
    }

    /**
     * <p>Setter method for planEtaTimeFrom.</p>
     *
     * @param planEtaTimeFrom Set for planEtaTimeFrom
     */
    public void setPlanEtaTimeFrom(String planEtaTimeFrom) {
        this.planEtaTimeFrom = planEtaTimeFrom;
    }

    /**
     * <p>Getter method for planEtaTimeTo.</p>
     *
     * @return the planEtaTimeTo
     */
    public String getPlanEtaTimeTo() {
        return planEtaTimeTo;
    }

    /**
     * <p>Setter method for planEtaTimeTo.</p>
     *
     * @param planEtaTimeTo Set for planEtaTimeTo
     */
    public void setPlanEtaTimeTo(String planEtaTimeTo) {
        this.planEtaTimeTo = planEtaTimeTo;
    }
    
}
