/*
 * ModifyDate Development company   Describe 
 * 2014/07/28 CSI Parichat          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Wshp003Form.
 * @author CSI
 */
public class Wshp003Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4037580030802360018L;

    /** The mode. */
    private String mode;
    
    /** The company denso code. */
    private String dCd;
    
    /** The do id. */
    private String doId;
    
    /** The dsc id. */
    private String dscId;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The revision. */
    private String revision;
    
    /** The route no. */
    private String routeNo;
    
    /** The del. */
    private String del;
    
    /** The screen id. */
    private String screenId;
    
    /** The display result flag. */
    private String displayResultFlag;
    
    /** The selected asn no. */
    private String selectedAsnNo;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The do detail list. */
    private List<InquiryDoDetailReturnDomain> inquiryDoDetailList;
    
    /** The default Constructor */
    public Wshp003Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        dCd = Constants.EMPTY_STRING;
        doId = Constants.EMPTY_STRING;
        dscId = Constants.EMPTY_STRING;
        mode = Constants.EMPTY_STRING;
        spsDoNo = Constants.EMPTY_STRING;
        routeNo = Constants.EMPTY_STRING;
        del = Constants.EMPTY_STRING;
        revision = Constants.EMPTY_STRING;
        screenId = Constants.EMPTY_STRING;
        inquiryDoDetailList = new ArrayList<InquiryDoDetailReturnDomain>();
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    
    /**
     * Gets the dCd.
     * 
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the dCd.
     * 
     * @param dCd the dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the route no.
     * 
     * @return the route no
     */
    public String getRouteNo() {
        return routeNo;
    }

    /**
     * Sets the route no.
     * 
     * @param routeNo the route no
     */
    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    /**
     * Gets the del.
     * 
     * @return the del
     */
    public String getDel() {
        return del;
    }

    /**
     * Sets the del.
     * 
     * @param del the del
     */
    public void setDel(String del) {
        this.del = del;
    }

    /**
     * Gets the do id.
     * 
     * @return the do id
     */
    public String getDoId() {
        return doId;
    }

    /**
     * Sets the do id.
     * 
     * @param doId the do id
     */
    public void setDoId(String doId) {
        this.doId = doId;
    }

    /**
     * Gets the sps do no.
     * 
     * @return the sps do no
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Sets the sps do no.
     * 
     * @param spsDoNo the sps do no
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Gets the revision.
     * 
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Sets the revision.
     * 
     * @param revision the revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }
    
    /**
     * Gets the dsc id.
     * 
     * @return the dsc id
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Sets the dsc id.
     * 
     * @param dscId the dsc id
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Gets the screen id.
     * 
     * @return the screen id
     */
    public String getScreenId() {
        return screenId;
    }

    /**
     * Sets the screen id.
     * 
     * @param screenId the screen id
     */
    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }
    
    /**
     * Gets the display result flag.
     * 
     * @return the display result flag.
     */
    public String getDisplayResultFlag() {
        return displayResultFlag;
    }

    /**
     * Sets the display result flag..
     * 
     * @param displayResultFlag the display result flag.
     */
    public void setDisplayResultFlag(String displayResultFlag) {
        this.displayResultFlag = displayResultFlag;
    }

    /**
     * Gets the selected asn no.
     * 
     * @return the selected asn no.
     */
    public String getSelectedAsnNo() {
        return selectedAsnNo;
    }
    
    /**
     * Sets the selected asn no.
     * 
     * @param selectedAsnNo the selected asn no.
     */
    public void setSelectedAsnNo(String selectedAsnNo) {
        this.selectedAsnNo = selectedAsnNo;
    }
    
    /**
     * Gets the supplierAuthenList.
     * 
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * Sets the supplierAuthenList.
     * 
     * @param supplierAuthenList the supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * Gets the densoAuthenList.
     * 
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * Sets the densoAuthenList.
     * 
     * @param densoAuthenList the densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
    
    /**
     * Gets the inquiry do detail list.
     * 
     * @return the inquiry do detail list
     */
    public List<InquiryDoDetailReturnDomain> getInquiryDoDetailList() {
        return inquiryDoDetailList;
    }
    
    /**
     * Sets the inquiry do detail list.
     * 
     * @param inquiryDoDetailList the inquiry do detail list
     */
    public void setInquiryDoDetailList(
        List<InquiryDoDetailReturnDomain> inquiryDoDetailList) {
        this.inquiryDoDetailList = inquiryDoDetailList;
    }
    
    /**
     * <p>Setter method for do detail information.</p>
     * 
     * @param items the new inquiryDoDetailReturn domain
     */
    public void setDoDetail(InquiryDoDetailReturnDomain items){
        this.inquiryDoDetailList.add(items);
    }
    
    /**
     * Gets the do detail information.
     * 
     * @param index the index
     * @return the inquiryDoDetailReturn domain.
     */
    public InquiryDoDetailReturnDomain getDoDetail(int index){
        if(inquiryDoDetailList == null){
            inquiryDoDetailList = new ArrayList<InquiryDoDetailReturnDomain>();
        }
        while(inquiryDoDetailList.size() <= index){
            inquiryDoDetailList.add(new InquiryDoDetailReturnDomain());
        }
        return inquiryDoDetailList.get(index);
    }
    
    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
}