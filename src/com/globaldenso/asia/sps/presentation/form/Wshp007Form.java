/*
 * ModifyDate Developmentcompany Describe 
 * 2014/08/25 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WSHP007Form.
 * @author CSI
 */
public class Wshp007Form extends CoreActionForm {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 8720900529081611615L;
    
    /** The mode. */
    private String mode;
    
    /** The cover page file ID. */
    private String coverPageFileId;
    
    /** The status index.*/
    private String statusIndex;
    
    /** The invoice date from. */
    private String deliveryDateFrom;
    
    /** The invoice date to. */
    private String deliveryDateTo;
    
    /** The denso code. */
    private String dCd;
    
    /** The supplier plant code. */
    private String dPcd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String spsDoNo;
    
    /** The supplier plant code. */
    private String asnNo;
    
    /** The invoice status.*/
    private String dPn;
    
    /** The invoice no.*/
    private String sPn;
    
    /** The invoice no.*/
    private String transportModeCb;
    
    /** The invoice date. */
    private String deliveryDate;
    
    /** The session Id. */
    private String sessionId;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The error message for operating download button. */
    private String cannotDownloadMessage;
    
    /** The supplier code list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The denso code list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The company denso plant list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The company supplier plant list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The transportation mode list. */
    private List<MiscellaneousDomain> transList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The list of back order Information domain. */
    private List<BackOrderInformationDomain> backOrderInformationList;
    
    /** The default constructor. */
    public Wshp007Form() {
        
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        statusIndex = Constants.EMPTY_STRING;
        deliveryDateFrom = Constants.EMPTY_STRING;
        deliveryDateTo = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        spsDoNo = Constants.EMPTY_STRING;
        asnNo = Constants.EMPTY_STRING;
        dPn = Constants.EMPTY_STRING;
        sPn = Constants.EMPTY_STRING;
        companySupplierList = new ArrayList<CompanySupplierDomain>();
        companyDensoList = new ArrayList<CompanyDensoDomain>();
        plantSupplierList = new ArrayList<PlantSupplierDomain>();
        plantDensoList = new ArrayList<PlantDensoDomain>();
        transList = new ArrayList<MiscellaneousDomain>();
        supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        backOrderInformationList = new ArrayList<BackOrderInformationDomain>();
    }
    
    /**
     * <p>Setter method for back order information.</p>
     * 
     * @param items the new back order information
     */
    public void setBackOrderInformation(BackOrderInformationDomain items) {
        this.backOrderInformationList.add(items);
    }
    
    /**
     * Gets the back order information item.
     * 
     * @param index the index
     * @return the back order information item.
     */
    public BackOrderInformationDomain getBackOrderInformation(int index){
        if (backOrderInformationList == null) {
            backOrderInformationList = new ArrayList<BackOrderInformationDomain>();
        }
        while (backOrderInformationList.size() <= index) {
            backOrderInformationList.add(new BackOrderInformationDomain());
        }
        return backOrderInformationList.get(index);
    }
    
    /**
     * <p>Setter method for supplier code.</p>
     * 
     * @param items the new company supplier domain
     */
    public void setCompanySupplierCode(CompanySupplierDomain items) {
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the supplier code.
     * 
     * @param index the index
     * @return the company supplier domain.
     */
    public CompanySupplierDomain getCompanySupplierCode(int index){
        if(companySupplierList == null){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * <p>Setter method for denso code.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoCode(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the denso code.
     * 
     * @param index the index
     * @return the company denso domain.
     */
    public CompanyDensoDomain getCompanyDensoCode(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * Sets the Transport Mode.
     * 
     * @param items the new Transport Mode
     */
    public void setTransItem(MiscellaneousDomain items) {
        this.transList.add(items);
    }
    
    /**
     * Gets the Transport Mode.
     * 
     * @param index the index
     * @return the misc domain
     */
    public MiscellaneousDomain getTransItem(int index){
        if(transList == null){
            transList = new ArrayList<MiscellaneousDomain>();
        }
        while(transList.size() <= index){
            transList.add(new MiscellaneousDomain());
        }
        return transList.get(index);
    }
    
    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    
    /**
     * Gets the cover page file ID.
     * 
     * @return the cover page file ID.
     */
    public String getCoverPageFileId() {
        return coverPageFileId;
    }
    
    /**
     * Sets the cover page file ID.
     * 
     * @param coverPageFileId the cover page file ID.
     */  
    public void setCoverPageFileId(String coverPageFileId) {
        this.coverPageFileId = coverPageFileId;
    }
    
    /**
     * Gets the status index.
     * 
     * @return the status index.
     */
    public String getStatusIndex() {
        return statusIndex;
    }
    
    /**
     * Sets the status index.
     * 
     * @param statusIndex the status index.
     */  
    public void setStatusIndex(String statusIndex) {
        this.statusIndex = statusIndex;
    }
    
    /**
     * <p>Getter method for deliveryDateFrom.</p>
     *
     * @return the deliveryDateFrom
     */
    public String getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    /**
     * <p>Setter method for deliveryDateFrom.</p>
     *
     * @param deliveryDateFrom Set for deliveryDateFrom
     */
    public void setDeliveryDateFrom(String deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    /**
     * <p>Getter method for deliveryDateTo.</p>
     *
     * @return the deliveryDateTo
     */
    public String getDeliveryDateTo() {
        return deliveryDateTo;
    }

    /**
     * <p>Setter method for deliveryDateTo.</p>
     *
     * @param deliveryDateTo Set for deliveryDateTo
     */
    public void setDeliveryDateTo(String deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * <p>Setter method for denso code.</p>
     *
     * @param dCd Set for denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for denso plan code.</p>
     *
     * @param dPcd Set for denso plan code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for spsDoNo.</p>
     *
     * @return the spsDoNo
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * <p>Setter method for spsDoNo.</p>
     *
     * @param spsDoNo Set for spsDoNo
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for transportModeCb.</p>
     *
     * @return the transportModeCb
     */
    public String getTransportModeCb() {
        return transportModeCb;
    }

    /**
     * <p>Setter method for transportModeCb.</p>
     *
     * @param transportModeCb Set for transportModeCb
     */
    public void setTransportModeCb(String transportModeCb) {
        this.transportModeCb = transportModeCb;
    }

    /**
     * <p>Getter method for transList.</p>
     *
     * @return the transList
     */
    public List<MiscellaneousDomain> getTransList() {
        return transList;
    }

    /**
     * <p>Setter method for transList.</p>
     *
     * @param transList Set for transList
     */
    public void setTransList(List<MiscellaneousDomain> transList) {
        this.transList = transList;
    }
    
    /**
     * Gets the supplier code list.
     * 
     * @return the supplier code list.
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the supplier code list.
     * 
     * @param companySupplierList the supplier code list.
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the denso code list.
     * 
     * @return the denso code list.
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the denso code list.
     * 
     * @param companyDensoList the denso code list.
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    
    /**
     * Gets the company denso plant list.
     * 
     * @return the company denso plant list.
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the company denso plant list.
     * 
     * @param plantDensoList the company denso plant list.
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the company supplier plant list.
     * 
     * @return the company supplier plant list.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the company supplier plant list.
     * 
     * @param plantSupplierList the company supplier plant list.
     */
    public void setPlantSupplierList(
        List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    
    /**
     * <p>Getter method for deliveryDate.</p>
     *
     * @return the deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * <p>Setter method for deliveryDate.</p>
     *
     * @param deliveryDate Set for deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * <p>Getter method for backOrderInformationList.</p>
     *
     * @return the backOrderInformationList
     */
    public List<BackOrderInformationDomain> getBackOrderInformationList() {
        return backOrderInformationList;
    }

    /**
     * <p>Setter method for backOrderInformationList.</p>
     *
     * @param backOrderInformationList Set for backOrderInformationList
     */
    public void setBackOrderInformationList(
        List<BackOrderInformationDomain> backOrderInformationList) {
        this.backOrderInformationList = backOrderInformationList;
    }

    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * Gets the cannot download message.
     * 
     * @return the cannot download message
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * Sets the cannot download message.
     * 
     * @param cannotDownloadMessage the cannot download message
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * Gets the supplierAuthenList.
     * 
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * Sets the supplierAuthenList.
     * 
     * @param supplierAuthenList the supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * Gets the densoAuthenList.
     * 
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * Sets the densoAuthenList.
     * 
     * @param densoAuthenList the densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
}
