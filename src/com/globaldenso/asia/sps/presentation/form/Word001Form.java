/*
 * ModifyDate Development company     Describe 
 * 2014/06/17 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.TmpUploadErrorDomain;
import com.globaldenso.asia.sps.common.constant.Constants;


/**
 * The Class WADM003Form.
 * @author CSI
 */
public class Word001Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The file ID. */
    private String fileId;
    
    /** The file data. */
    private FormFile fileData;
    
    /** The total error Record. */
    private String totalRecord;
    
    /** The correct Record. */
    private String correctRecord;
    
    /** The warning Record. */
    private String warningRecord;
    
    /** The incorrect Record. */
    private String incorrectRecord;
    
    /** The isUpload Flag. */
    private Boolean isUpload = false;
    
    /** The Company Supplier. */
    private String companySupplier;
   
    /** The Plant Supplier. */
    private String plantSupplier;
    
    /** The file name. */
    private String fileName;
    
    /** The List of Temporary Upload Error domain. */
    private List<TmpUploadErrorDomain> tmpUploadErrorList;
    
    /** The List of Group Upload Error domain. */
    private List<GroupUploadErrorDomain> uploadErrorDetailDomain;
    
    /** The List of Plant supplier */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The size of plant supplier list. */
    private int maxPlantSupplierList;

    /** Error message if user cannot click Reset button. */
    private String cannotResetMessage;

    /** Error message if user cannot click Export button. */
    private String cannotExportMessage;
    
    /** The default Constructor */
    public Word001Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        totalRecord = Constants.STR_ZERO;
        correctRecord = Constants.STR_ZERO;
        warningRecord = Constants.STR_ZERO;
        incorrectRecord = Constants.STR_ZERO;
        fileName = Constants.EMPTY_STRING;
        fileData = null;
        companySupplier = Constants.EMPTY_STRING;
        plantSupplier = Constants.EMPTY_STRING;
        plantSupplierList = new ArrayList<PlantSupplierDomain>();
        uploadErrorDetailDomain = new ArrayList<GroupUploadErrorDomain>();
    }

    /**
     * <p>Setter method for file ID.</p>
     *
     * @param fileId Set for file ID
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * <p>Getter method for file ID.</p>
     *
     * @return the file ID.
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Getter method for file data.</p>
     *
     * @return the file data.
     */
    public FormFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter method for file data.</p>
     *
     * @param fileData Set for file data.
     */
    public void setFileData(FormFile fileData) {
        this.fileData = fileData;
    }

    /**
     * Gets the total error Record.
     * 
     * @return the total error Record.
     */
    public String getTotalRecord() {
        return totalRecord;
    }

    /**
     * Sets the total error Record.
     * 
     * @param totalRecord the total error Record.
     */
    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    /**
     * Gets the correct Record.
     * 
     * @return the correct Record.
     */
    public String getCorrectRecord() {
        return correctRecord;
    }

    /**
     * Sets the correct Record.
     * 
     * @param correctRecord the correct Record.
     */
    public void setCorrectRecord(String correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * Gets the warning record.
     * 
     * @return the warning record.
     */
    public String getWarningRecord() {
        return warningRecord;
    }

    /**
     * Sets the warning record.
     * 
     * @param warningRecord the warning record.
     */
    public void setWarningRecord(String warningRecord) {
        this.warningRecord = warningRecord;
    }

    /**
     * Gets the Incorrect record.
     * 
     * @return the Incorrect record.
     */
    public String getIncorrectRecord() {
        return incorrectRecord;
    }

    /**
     * Sets the Incorrect record.
     * 
     * @param incorrectRecord the Incorrect record.
     */
    public void setIncorrectRecord(String incorrectRecord) {
        this.incorrectRecord = incorrectRecord;
    }

    /**
     * Gets the Upload flag.
     * 
     * @return the Upload flag.
     */
    public Boolean getIsUpload() {
        return isUpload;
    }

    /**
     * Sets the Upload flag.
     * 
     * @param isUpload the Upload flag.
     */
    public void setIsUpload(Boolean isUpload) {
        this.isUpload = isUpload;
    }
    
    /**
     * Gets the Temporary of Upload Error List.
     * 
     * @return the Temporary of Upload Error List.
     */
    public List<TmpUploadErrorDomain> getTmpUploadErrorList() {
        return tmpUploadErrorList;
    }

    /**
     * Sets the Temporary of Upload Error List.
     * 
     * @param tmpUploadErrorList the Temporary of Upload Error List.
     */
    public void setTmpUploadErrorList(List<TmpUploadErrorDomain> tmpUploadErrorList) {
        this.tmpUploadErrorList = tmpUploadErrorList;
    }
    /**
     * Gets the list of group error domain.
     * 
     * @return the list of group error domain.
     */
    public List<GroupUploadErrorDomain> getUploadErrorDetailDomain() {
        return uploadErrorDetailDomain;
    }

    /**
     * Sets the list of group error domain.
     * 
     * @param uploadErrorDetailDomain the list of group error domain.
     */
    public void setUploadErrorDetailDomain(List<GroupUploadErrorDomain> uploadErrorDetailDomain) {
        this.uploadErrorDetailDomain = uploadErrorDetailDomain;
    }
    /**
     * Gets the Company Supplier Name.
     * 
     * @return the Company Supplier Name.
     */
    public String getCompanySupplier() {
        return companySupplier;
    }
    /**
     * Sets the Company Supplier Name.
     * 
     * @param companySupplier the Company Supplier Name.
     */
    public void setCompanySupplier(String companySupplier) {
        this.companySupplier = companySupplier;
    }
    /**
     * Gets the Plant Supplier Name.
     * 
     * @return the list of Plant Supplier Name.
     */
    public String getPlantSupplier() {
        return plantSupplier;
    }
    /**
     * Sets the Plant Supplier Name.
     * 
     * @param plantSupplier the Plant Supplier Name.
     */
    public void setPlantSupplier(String plantSupplier) {
        this.plantSupplier = plantSupplier;
    }
    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    /**
     * <p>Getter method for list of plant supplier..</p>
     *
     * @return the list of plant supplier.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }
    /**
     * <p>Setter list of plant supplier.</p>
     *
     * @param plantSupplierList Set for list of plant supplier.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    /**
     * <p>Getter method for size of plant supplier.</p>
     *
     * @return the size of plant supplier
     */
    public int getMaxPlantSupplierList() {
        return maxPlantSupplierList;
    }
    /**
     * <p>Setter size of plant supplier.</p>
     *
     * @param maxPlantSupplierList Set for size of plant supplier.
     */
    public void setMaxPlantSupplierList(int maxPlantSupplierList) {
        this.maxPlantSupplierList = maxPlantSupplierList;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotExportMessage.</p>
     *
     * @return the cannotExportMessage
     */
    public String getCannotExportMessage() {
        return cannotExportMessage;
    }

    /**
     * <p>Setter method for cannotExportMessage.</p>
     *
     * @param cannotExportMessage Set for cannotExportMessage
     */
    public void setCannotExportMessage(String cannotExportMessage) {
        this.cannotExportMessage = cannotExportMessage;
    }
    
}