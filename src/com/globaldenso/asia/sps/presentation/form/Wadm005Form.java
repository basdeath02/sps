/*
 * ModifyDate Development company     Describe 
 * 2014/06/10 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WADM005Form.
 * @author CSI
 */
public class Wadm005Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The DSC ID Selected. */
    private String dscIdSelected;
        
    /** The Company DENSO Code. */
    private String dCd;
   
    /** The Plant DENSO. */
    private String dPcd;
    
    /** The first name. */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The Employee code. */
    private String employeeCode;  
    
    /** The register date from. */
    private String registerDateFrom;
    
    /** The register date to. */
    private String registerDateTo;
    
    /** The Count Delete. */
    private String countDelete;
    
    /** The DENSO Plant Selected. */
    private String dPcdSelected;
    
    /** The Company DENSO Code Selected. */
    private String dCdSelected;
    
    /** The First Name Selected. */
    private String firstNameSelected;
    
    /** The Middle Name Selected. */
    private String middleNameSelected;
    
    /** The last Name Selected. */
    private String lastNameSelected;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The error message for operating download button. */
    private String cannotDownloadMessage;
    
    /** The List of DENSO User Info domain. */
    private List<DensoUserInformationDomain> userDensoInfoList;
    
    /** The List of Company DENSO domain. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of Plant DENSO domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The default Constructor */
    public Wadm005Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        
        dscId = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        countDelete = Constants.STR_ZERO;
        firstName = Constants.EMPTY_STRING;
        middleName = Constants.EMPTY_STRING;
        lastName = Constants.EMPTY_STRING;
        registerDateFrom = Constants.EMPTY_STRING;
        registerDateTo = Constants.EMPTY_STRING;
        employeeCode = Constants.EMPTY_STRING;
        
        userDensoInfoList = new ArrayList<DensoUserInformationDomain>();
    }
    
    /**
     * Sets the DENSO User item.
     * 
     * @param items the new DENSO item
     */
    public void setUserDenso(DensoUserInformationDomain items) {
        this.userDensoInfoList.add(items);
    }
    
    /**
     * Gets the DENSO item.
     * 
     * @param index the index
     * @return the DENSO item.
     */
    public DensoUserInformationDomain getUserDenso(int index){
        if (userDensoInfoList == null) {
            userDensoInfoList = new ArrayList<DensoUserInformationDomain>();
        }
        while (userDensoInfoList.size() <= index) {
            userDensoInfoList.add(new DensoUserInformationDomain());
        }
        return userDensoInfoList.get(index);
    }
    
    /**
     * <p>Setter method for denso company item.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoItem(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the denso company item.
     * 
     * @param index the index
     * @return the company denso domain
     */
    public CompanyDensoDomain getCompanyDensoItem(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
       
    /**
     * Gets the employee Code.
     * 
     * @return the employee Code.
     */
    public String getEmployeeCode() {
        return employeeCode;
    }

    /**
     * Sets the employee Code.
     * 
     * @param employeeCode the employee Code.
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    
    /**
     * Gets the supplier plant code
     * 
     * @return the supplier plant code
     */
    public String getRegisterDateFrom() {
        return registerDateFrom;
    }

    /**
     * Sets the register Date From.
     * 
     * @param registerDateFrom the register Date From.
     */
    public void setRegisterDateFrom(String registerDateFrom) {
        this.registerDateFrom = registerDateFrom;
    }

    /**
     * Gets the register Date To.
     * 
     * @return the  register Date To.
     */
    public String getRegisterDateTo() {
        return registerDateTo;
    }

    /**
     * Sets the register Date To.
     * 
     * @param registerDateTo the register Date To.
     */
    public void setRegisterDateTo(String registerDateTo) {
        this.registerDateTo = registerDateTo;
    }
    

    /**
     * Gets the list of user DENSO information.
     * 
     * @return the list of user DENSO information.
     */
    public List<DensoUserInformationDomain> getUserDensoInfoList() {
        return userDensoInfoList;
    }

    /**
     * Sets the list of user DENSO information.
     * 
     * @param userDensoInfoList the list of user DENSO information.
     */
    public void setUserDensoInfoList(List<DensoUserInformationDomain> userDensoInfoList) {
        this.userDensoInfoList = userDensoInfoList;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the DSC ID Selected.
     * 
     * @return the DSC ID Selected.
     */
    public String getDscIdSelected() {
        return dscIdSelected;
    }
    /**
     * Sets the DSC ID Selected.
     * 
     * @param dscIdSelected the DSC ID Selected.
     */
    public void setDscIdSelected(String dscIdSelected) {
        this.dscIdSelected = dscIdSelected;
    }
    /**
     * Gets the Company DENSO Code.
     * 
     * @return the Company DENSO Code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Sets the Company DENSO Code.
     * 
     * @param dcd the Company DENSO Code.
     */
    public void setDCd(String dcd) {
        this.dCd = dcd;
    }
    /**
     * Gets the Plant DENSO Code
     * 
     * @return the Plant DENSO Code
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the Plant DENSO Code
     * 
     * @param dPcd the Plant DENSO Code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    /**
     * Gets the list of Company DENSO
     * 
     * @return the list of Company DENSO
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }
    /**
     * Sets the list of Company DENSO
     * 
     * @param companyDensoList the list of Company DENSO
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    /**
     * Gets the list of plant DENSO
     * 
     * @return the list of plant DENSO
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }
    /**
     * Sets the list of plant DENSO
     * 
     * @param plantDensoList the list of plant DENSO
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    /**
     * Gets the count delete.
     * 
     * @return the count delete.
     */
    public String getCountDelete() {
        return countDelete;
    }
    /**
     * Sets the count delete.
     * 
     * @param countDelete the count delete.
     */
    public void setCountDelete(String countDelete) {
        this.countDelete = countDelete;
    }
    
    /**
     * Gets the first name selected.
     * 
     * @return the first name selected.
     */
    public String getFirstNameSelected() {
        return firstNameSelected;
    }
    /**
     * Sets the first name selected.
     * 
     * @param firstNameSelected the first name selected.
     */
    public void setFirstNameSelected(String firstNameSelected) {
        this.firstNameSelected = firstNameSelected;
    }
    /**
     * Gets the middle name selected.
     * 
     * @return the middle name selected.
     */
    public String getMiddleNameSelected() {
        return middleNameSelected;
    }
    /**
     * Sets the middle name selected.
     * 
     * @param middleNameSelected the middle name selected.
     */
    public void setMiddleNameSelected(String middleNameSelected) {
        this.middleNameSelected = middleNameSelected;
    }
    /**
     * Gets the last name selected.
     * 
     * @return the last name selected.
     */
    public String getLastNameSelected() {
        return lastNameSelected;
    }
    /**
     * Sets the last name selected.
     * 
     * @param lastNameSelected the last name selected.
     */
    public void setLastNameSelected(String lastNameSelected) {
        this.lastNameSelected = lastNameSelected;
    }
    /**
     * Gets the Plant DENSO code selected.
     * 
     * @return the Plant DENSO code selected.
     */
    public String getDPcdSelected() {
        return dPcdSelected;
    }
    /**
     * Sets the Plant DENSO code selected.
     * 
     * @param dPcdSelected the Plant DENSO code selected.
     */
    public void setDPcdSelected(String dPcdSelected) {
        this.dPcdSelected = dPcdSelected;
    }
    /**
     * Gets the Company DENSO code selected.
     * 
     * @return the Company DENSO code selected.
     */
    public String getDCdSelected() {
        return dCdSelected;
    }
    /**
     * Sets the Company DENSO code selected.
     * 
     * @param dCdSelected the Company DENSO code selected.
     */
    public void setDCdSelected(String dCdSelected) {
        this.dCdSelected = dCdSelected;
    }
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }
    
    /**
     * Gets the cannot download message.
     * 
     * @return the cannot download message
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * Sets the cannot download message.
     * 
     * @param cannotDownloadMessage the cannot download message
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }
    
    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
}